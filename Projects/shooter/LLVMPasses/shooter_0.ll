; ModuleID = 'shooter.c'
source_filename = "shooter.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.timespec = type { i32, i32 }
%struct.Entity_Table = type { i32, i8*, i32 }
%struct.Physics_States = type { i32, i8*, i32, i32*, float*, float*, float*, float*, float* }
%struct.Physics_Balls = type { i32, i8*, i32, i32*, float*, float* }
%struct.Collision_Table = type { i32, i8*, i32, i32*, i32* }
%struct.Proximity_Attack = type { i32, i8*, i32, i32*, float*, float* }
%struct.Hit_Feedback_Table = type { i32, i8*, i32, i32*, float* }
%struct.Sprite_Map = type { i32, i8*, i32, i32*, i32*, float*, float*, float*, i8* }
%struct.AI_Enemy = type { i32, i8*, i32, i32*, i8* }
%struct.Bullet_Table = type { i32, i8*, i32, i32*, float*, %struct.timespec* }
%struct.Health_Table = type { i32, i8*, i32, i32*, float*, %struct.timespec* }
%struct.Weapon_States = type { i32, i32, float*, float* }
%struct.Overlay_Data = type { i32, i32, i32, float }
%struct.Campaign = type { i32, i32, i8* }
%struct.Wave_Completion = type { [4 x i8] }
%struct.Wave_Emitter = type { float, %struct.timespec, i8, [4 x i8] }
%struct.Wave_Rest = type { float }
%struct.Input_State = type { i8, i8, i8, i8, i8, float, float }
%struct.Table = type { i32, i8*, i32, i32* }
%struct.EmscriptenKeyboardEvent = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, [32 x i8], [32 x i8], [32 x i8], [32 x i8] }
%struct.EmscriptenMouseEvent = type { i32, i32, i32, i32, i32, i32, i32, i32, i16, i16, i32, i32, i32, i32, i32, i32, i32 }
%struct.__pthread = type opaque

@paused = hidden global i8 0, align 1
@start_timestamp = hidden global %struct.timespec zeroinitializer, align 4
@prev_time = hidden global %struct.timespec zeroinitializer, align 4
@curr_time = hidden global %struct.timespec zeroinitializer, align 4
@screen_width = hidden global i32 0, align 4
@screen_height = hidden global i32 0, align 4
@entity_table = hidden global %struct.Entity_Table* null, align 4
@physics_states = hidden global %struct.Physics_States* null, align 4
@physics_balls = hidden global %struct.Physics_Balls* null, align 4
@collision_table = hidden global %struct.Collision_Table* null, align 4
@proximity_attack = hidden global %struct.Proximity_Attack* null, align 4
@hit_feedback_table = hidden global %struct.Hit_Feedback_Table* null, align 4
@sprite_map = hidden global %struct.Sprite_Map* null, align 4
@ai_enemy = hidden global %struct.AI_Enemy* null, align 4
@bullets = hidden global %struct.Bullet_Table* null, align 4
@health_table = hidden global %struct.Health_Table* null, align 4
@weapon_states = hidden global %struct.Weapon_States* null, align 4
@curr_weapon = hidden global i32 0, align 4
@overlay_data = hidden global %struct.Overlay_Data* null, align 4
@curr_wave = hidden global i32 0, align 4
@campaign = hidden global %struct.Campaign* null, align 4
@wave_completion = hidden global %struct.Wave_Completion zeroinitializer, align 1
@wave_emitter = hidden global %struct.Wave_Emitter zeroinitializer, align 4
@wave_rest = hidden global %struct.Wave_Rest zeroinitializer, align 4
@.str = private unnamed_addr constant [8 x i8] c"#window\00", align 1
@str_window = hidden global i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str, i32 0, i32 0), align 4
@.str.1 = private unnamed_addr constant [2 x i8] c"w\00", align 1
@input_state = hidden global %struct.Input_State* null, align 4
@.str.2 = private unnamed_addr constant [2 x i8] c"a\00", align 1
@.str.3 = private unnamed_addr constant [2 x i8] c"s\00", align 1
@.str.4 = private unnamed_addr constant [2 x i8] c"d\00", align 1
@.str.5 = private unnamed_addr constant [7 x i8] c"Escape\00", align 1
@score = hidden global i32 0, align 4
@llvm.used = appending global [15 x i8*] [i8* bitcast (float ()* @randf to i8*), i8* bitcast (void ()* @start_time to i8*), i8* bitcast (void ()* @stop_time to i8*), i8* bitcast (void (i32, i32)* @set_screen_size to i8*), i8* bitcast (i32 (i8*, i32)* @find_item_index to i8*), i8* bitcast (%struct.Physics_States* ()* @get_physics_states to i8*), i8* bitcast (%struct.Collision_Table* ()* @get_collision_table to i8*), i8* bitcast (%struct.Hit_Feedback_Table* ()* @get_hit_feedback_table to i8*), i8* bitcast (%struct.Sprite_Map* ()* @get_sprite_map to i8*), i8* bitcast (%struct.AI_Enemy* ()* @get_ai_enemy to i8*), i8* bitcast (%struct.Weapon_States* ()* @get_weapon_states to i8*), i8* bitcast (%struct.Overlay_Data* ()* @get_overlay_data to i8*), i8* bitcast (void (i32, i32)* @init to i8*), i8* bitcast (i32 ()* @get_score to i8*), i8* bitcast (void ()* @step to i8*)], section "llvm.metadata"

; Function Attrs: noinline nounwind optnone
define hidden float @randf() #0 {
entry:
  %call = call i32 @rand()
  %conv = sitofp i32 %call to float
  %div = fdiv float %conv, 0x41E0000000000000
  ret float %div
}

declare i32 @rand() #1

; Function Attrs: noinline nounwind optnone
define hidden void @timespec_diff(%struct.timespec* %stop, %struct.timespec* %start, %struct.timespec* %result) #0 {
entry:
  %stop.addr = alloca %struct.timespec*, align 4
  %start.addr = alloca %struct.timespec*, align 4
  %result.addr = alloca %struct.timespec*, align 4
  store %struct.timespec* %stop, %struct.timespec** %stop.addr, align 4
  store %struct.timespec* %start, %struct.timespec** %start.addr, align 4
  store %struct.timespec* %result, %struct.timespec** %result.addr, align 4
  %0 = load %struct.timespec*, %struct.timespec** %stop.addr, align 4
  %tv_nsec = getelementptr inbounds %struct.timespec, %struct.timespec* %0, i32 0, i32 1
  %1 = load i32, i32* %tv_nsec, align 4
  %2 = load %struct.timespec*, %struct.timespec** %start.addr, align 4
  %tv_nsec1 = getelementptr inbounds %struct.timespec, %struct.timespec* %2, i32 0, i32 1
  %3 = load i32, i32* %tv_nsec1, align 4
  %cmp = icmp slt i32 %1, %3
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %4 = load %struct.timespec*, %struct.timespec** %stop.addr, align 4
  %tv_sec = getelementptr inbounds %struct.timespec, %struct.timespec* %4, i32 0, i32 0
  %5 = load i32, i32* %tv_sec, align 4
  %6 = load %struct.timespec*, %struct.timespec** %start.addr, align 4
  %tv_sec2 = getelementptr inbounds %struct.timespec, %struct.timespec* %6, i32 0, i32 0
  %7 = load i32, i32* %tv_sec2, align 4
  %sub = sub nsw i32 %5, %7
  %sub3 = sub nsw i32 %sub, 1
  %8 = load %struct.timespec*, %struct.timespec** %result.addr, align 4
  %tv_sec4 = getelementptr inbounds %struct.timespec, %struct.timespec* %8, i32 0, i32 0
  store i32 %sub3, i32* %tv_sec4, align 4
  %9 = load %struct.timespec*, %struct.timespec** %stop.addr, align 4
  %tv_nsec5 = getelementptr inbounds %struct.timespec, %struct.timespec* %9, i32 0, i32 1
  %10 = load i32, i32* %tv_nsec5, align 4
  %11 = load %struct.timespec*, %struct.timespec** %start.addr, align 4
  %tv_nsec6 = getelementptr inbounds %struct.timespec, %struct.timespec* %11, i32 0, i32 1
  %12 = load i32, i32* %tv_nsec6, align 4
  %sub7 = sub nsw i32 %10, %12
  %add = add nsw i32 %sub7, 1000000000
  %13 = load %struct.timespec*, %struct.timespec** %result.addr, align 4
  %tv_nsec8 = getelementptr inbounds %struct.timespec, %struct.timespec* %13, i32 0, i32 1
  store i32 %add, i32* %tv_nsec8, align 4
  br label %if.end

if.else:                                          ; preds = %entry
  %14 = load %struct.timespec*, %struct.timespec** %stop.addr, align 4
  %tv_sec9 = getelementptr inbounds %struct.timespec, %struct.timespec* %14, i32 0, i32 0
  %15 = load i32, i32* %tv_sec9, align 4
  %16 = load %struct.timespec*, %struct.timespec** %start.addr, align 4
  %tv_sec10 = getelementptr inbounds %struct.timespec, %struct.timespec* %16, i32 0, i32 0
  %17 = load i32, i32* %tv_sec10, align 4
  %sub11 = sub nsw i32 %15, %17
  %18 = load %struct.timespec*, %struct.timespec** %result.addr, align 4
  %tv_sec12 = getelementptr inbounds %struct.timespec, %struct.timespec* %18, i32 0, i32 0
  store i32 %sub11, i32* %tv_sec12, align 4
  %19 = load %struct.timespec*, %struct.timespec** %stop.addr, align 4
  %tv_nsec13 = getelementptr inbounds %struct.timespec, %struct.timespec* %19, i32 0, i32 1
  %20 = load i32, i32* %tv_nsec13, align 4
  %21 = load %struct.timespec*, %struct.timespec** %start.addr, align 4
  %tv_nsec14 = getelementptr inbounds %struct.timespec, %struct.timespec* %21, i32 0, i32 1
  %22 = load i32, i32* %tv_nsec14, align 4
  %sub15 = sub nsw i32 %20, %22
  %23 = load %struct.timespec*, %struct.timespec** %result.addr, align 4
  %tv_nsec16 = getelementptr inbounds %struct.timespec, %struct.timespec* %23, i32 0, i32 1
  store i32 %sub15, i32* %tv_nsec16, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden float @timespec_to_float(%struct.timespec* %spec) #0 {
entry:
  %spec.addr = alloca %struct.timespec*, align 4
  store %struct.timespec* %spec, %struct.timespec** %spec.addr, align 4
  %0 = load %struct.timespec*, %struct.timespec** %spec.addr, align 4
  %tv_sec = getelementptr inbounds %struct.timespec, %struct.timespec* %0, i32 0, i32 0
  %1 = load i32, i32* %tv_sec, align 4
  %conv = sitofp i32 %1 to float
  %2 = load %struct.timespec*, %struct.timespec** %spec.addr, align 4
  %tv_nsec = getelementptr inbounds %struct.timespec, %struct.timespec* %2, i32 0, i32 1
  %3 = load i32, i32* %tv_nsec, align 4
  %div = sdiv i32 %3, 1000000
  %conv1 = sitofp i32 %div to float
  %div2 = fdiv float %conv1, 1.000000e+03
  %add = fadd float %conv, %div2
  ret float %add
}

; Function Attrs: noinline nounwind optnone
define hidden float @timespec_diff_float(%struct.timespec* %stop, %struct.timespec* %start) #0 {
entry:
  %stop.addr = alloca %struct.timespec*, align 4
  %start.addr = alloca %struct.timespec*, align 4
  %delta = alloca %struct.timespec, align 4
  store %struct.timespec* %stop, %struct.timespec** %stop.addr, align 4
  store %struct.timespec* %start, %struct.timespec** %start.addr, align 4
  %0 = load %struct.timespec*, %struct.timespec** %stop.addr, align 4
  %1 = load %struct.timespec*, %struct.timespec** %start.addr, align 4
  call void @timespec_diff(%struct.timespec* %0, %struct.timespec* %1, %struct.timespec* %delta)
  %call = call float @timespec_to_float(%struct.timespec* %delta)
  ret float %call
}

; Function Attrs: noinline nounwind optnone
define hidden void @start_time() #0 {
entry:
  %call = call i32 @clock_gettime(i32 0, %struct.timespec* @start_timestamp)
  store i32 0, i32* getelementptr inbounds (%struct.timespec, %struct.timespec* @prev_time, i32 0, i32 0), align 4
  store i32 0, i32* getelementptr inbounds (%struct.timespec, %struct.timespec* @prev_time, i32 0, i32 1), align 4
  call void @emscripten_set_main_loop(void ()* @step, i32 0, i32 0)
  ret void
}

declare i32 @clock_gettime(i32, %struct.timespec*) #1

declare void @emscripten_set_main_loop(void ()*, i32, i32) #1

; Function Attrs: noinline nounwind optnone
define hidden void @stop_time() #0 {
entry:
  call void @emscripten_cancel_main_loop()
  ret void
}

declare void @emscripten_cancel_main_loop() #1

; Function Attrs: noinline nounwind optnone
define hidden float @step_time() #0 {
entry:
  %delta = alloca float, align 4
  %call = call i32 @clock_gettime(i32 0, %struct.timespec* @curr_time)
  call void @timespec_diff(%struct.timespec* @curr_time, %struct.timespec* @start_timestamp, %struct.timespec* @curr_time)
  %call1 = call float @timespec_diff_float(%struct.timespec* @curr_time, %struct.timespec* @prev_time)
  store float %call1, float* %delta, align 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 bitcast (%struct.timespec* @prev_time to i8*), i8* align 4 bitcast (%struct.timespec* @curr_time to i8*), i32 8, i1 false)
  %0 = load float, float* %delta, align 4
  ret float %0
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #2

; Function Attrs: noinline nounwind optnone
define hidden void @set_screen_size(i32 %width, i32 %height) #0 {
entry:
  %width.addr = alloca i32, align 4
  %height.addr = alloca i32, align 4
  store i32 %width, i32* %width.addr, align 4
  store i32 %height, i32* %height.addr, align 4
  %0 = load i32, i32* %width.addr, align 4
  store i32 %0, i32* @screen_width, align 4
  %1 = load i32, i32* %height.addr, align 4
  store i32 %1, i32* @screen_height, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @alloc_table(i8* %table_ptr, i32 %max_count) #0 {
entry:
  %table_ptr.addr = alloca i8*, align 4
  %max_count.addr = alloca i32, align 4
  %table = alloca %struct.Table*, align 4
  %i = alloca i32, align 4
  store i8* %table_ptr, i8** %table_ptr.addr, align 4
  store i32 %max_count, i32* %max_count.addr, align 4
  %0 = load i8*, i8** %table_ptr.addr, align 4
  %1 = bitcast i8* %0 to %struct.Table*
  store %struct.Table* %1, %struct.Table** %table, align 4
  %2 = load i32, i32* %max_count.addr, align 4
  %3 = load %struct.Table*, %struct.Table** %table, align 4
  %max_count1 = getelementptr inbounds %struct.Table, %struct.Table* %3, i32 0, i32 0
  store i32 %2, i32* %max_count1, align 4
  %4 = load i32, i32* %max_count.addr, align 4
  %mul = mul i32 %4, 1
  %call = call i8* @malloc(i32 %mul)
  %5 = load %struct.Table*, %struct.Table** %table, align 4
  %used = getelementptr inbounds %struct.Table, %struct.Table* %5, i32 0, i32 1
  store i8* %call, i8** %used, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %6 = load i32, i32* %i, align 4
  %7 = load i32, i32* %max_count.addr, align 4
  %cmp = icmp ult i32 %6, %7
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %8 = load %struct.Table*, %struct.Table** %table, align 4
  %used2 = getelementptr inbounds %struct.Table, %struct.Table* %8, i32 0, i32 1
  %9 = load i8*, i8** %used2, align 4
  %10 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i8, i8* %9, i32 %10
  store i8 0, i8* %arrayidx, align 1
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4
  %add = add i32 %11, 1
  store i32 %add, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %12 = load %struct.Table*, %struct.Table** %table, align 4
  %curr_max = getelementptr inbounds %struct.Table, %struct.Table* %12, i32 0, i32 2
  store i32 0, i32* %curr_max, align 4
  %13 = load i32, i32* %max_count.addr, align 4
  %mul3 = mul i32 %13, 4
  %call4 = call i8* @malloc(i32 %mul3)
  %14 = bitcast i8* %call4 to i32*
  %15 = load %struct.Table*, %struct.Table** %table, align 4
  %entity_id = getelementptr inbounds %struct.Table, %struct.Table* %15, i32 0, i32 3
  store i32* %14, i32** %entity_id, align 4
  ret void
}

declare i8* @malloc(i32) #1

; Function Attrs: noinline nounwind optnone
define hidden i32 @find_item_index(i8* %table_ptr, i32 %entity_id) #0 {
entry:
  %table_ptr.addr = alloca i8*, align 4
  %entity_id.addr = alloca i32, align 4
  %table = alloca %struct.Table*, align 4
  %index = alloca i32, align 4
  store i8* %table_ptr, i8** %table_ptr.addr, align 4
  store i32 %entity_id, i32* %entity_id.addr, align 4
  %0 = load i8*, i8** %table_ptr.addr, align 4
  %1 = bitcast i8* %0 to %struct.Table*
  store %struct.Table* %1, %struct.Table** %table, align 4
  store i32 0, i32* %index, align 4
  br label %while.cond

while.cond:                                       ; preds = %if.end, %entry
  %2 = load i32, i32* %index, align 4
  %3 = load %struct.Table*, %struct.Table** %table, align 4
  %curr_max = getelementptr inbounds %struct.Table, %struct.Table* %3, i32 0, i32 2
  %4 = load i32, i32* %curr_max, align 4
  %cmp = icmp ult i32 %2, %4
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %5 = load %struct.Table*, %struct.Table** %table, align 4
  %entity_id1 = getelementptr inbounds %struct.Table, %struct.Table* %5, i32 0, i32 3
  %6 = load i32*, i32** %entity_id1, align 4
  %7 = load i32, i32* %index, align 4
  %arrayidx = getelementptr inbounds i32, i32* %6, i32 %7
  %8 = load i32, i32* %arrayidx, align 4
  %9 = load i32, i32* %entity_id.addr, align 4
  %cmp2 = icmp eq i32 %8, %9
  br i1 %cmp2, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %while.body
  %10 = load %struct.Table*, %struct.Table** %table, align 4
  %used = getelementptr inbounds %struct.Table, %struct.Table* %10, i32 0, i32 1
  %11 = load i8*, i8** %used, align 4
  %12 = load i32, i32* %index, align 4
  %arrayidx3 = getelementptr inbounds i8, i8* %11, i32 %12
  %13 = load i8, i8* %arrayidx3, align 1
  %conv = zext i8 %13 to i32
  %tobool = icmp ne i32 %conv, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  br label %while.end

if.end:                                           ; preds = %land.lhs.true, %while.body
  %14 = load i32, i32* %index, align 4
  %add = add i32 %14, 1
  store i32 %add, i32* %index, align 4
  br label %while.cond

while.end:                                        ; preds = %if.then, %while.cond
  %15 = load i32, i32* %index, align 4
  ret i32 %15
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @find_first_unused_item(i8* %table_ptr) #0 {
entry:
  %table_ptr.addr = alloca i8*, align 4
  %table = alloca %struct.Table*, align 4
  %index = alloca i32, align 4
  store i8* %table_ptr, i8** %table_ptr.addr, align 4
  %0 = load i8*, i8** %table_ptr.addr, align 4
  %1 = bitcast i8* %0 to %struct.Table*
  store %struct.Table* %1, %struct.Table** %table, align 4
  store i32 0, i32* %index, align 4
  br label %while.cond

while.cond:                                       ; preds = %if.then, %entry
  %2 = load i32, i32* %index, align 4
  %3 = load %struct.Table*, %struct.Table** %table, align 4
  %curr_max = getelementptr inbounds %struct.Table, %struct.Table* %3, i32 0, i32 2
  %4 = load i32, i32* %curr_max, align 4
  %cmp = icmp ule i32 %2, %4
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %5 = load %struct.Table*, %struct.Table** %table, align 4
  %used = getelementptr inbounds %struct.Table, %struct.Table* %5, i32 0, i32 1
  %6 = load i8*, i8** %used, align 4
  %7 = load i32, i32* %index, align 4
  %arrayidx = getelementptr inbounds i8, i8* %6, i32 %7
  %8 = load i8, i8* %arrayidx, align 1
  %tobool = icmp ne i8 %8, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %while.body
  %9 = load i32, i32* %index, align 4
  %add = add i32 %9, 1
  store i32 %add, i32* %index, align 4
  br label %while.cond

if.else:                                          ; preds = %while.body
  br label %while.end

while.end:                                        ; preds = %if.else, %while.cond
  %10 = load i32, i32* %index, align 4
  ret i32 %10
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @add_table_item(i8* %table_ptr, i32 %entity_id) #0 {
entry:
  %table_ptr.addr = alloca i8*, align 4
  %entity_id.addr = alloca i32, align 4
  %table = alloca %struct.Table*, align 4
  %index = alloca i32, align 4
  store i8* %table_ptr, i8** %table_ptr.addr, align 4
  store i32 %entity_id, i32* %entity_id.addr, align 4
  %0 = load i8*, i8** %table_ptr.addr, align 4
  %1 = bitcast i8* %0 to %struct.Table*
  store %struct.Table* %1, %struct.Table** %table, align 4
  %2 = load %struct.Table*, %struct.Table** %table, align 4
  %3 = bitcast %struct.Table* %2 to i8*
  %call = call i32 @find_first_unused_item(i8* %3)
  store i32 %call, i32* %index, align 4
  %4 = load i32, i32* %index, align 4
  %5 = load %struct.Table*, %struct.Table** %table, align 4
  %curr_max = getelementptr inbounds %struct.Table, %struct.Table* %5, i32 0, i32 2
  %6 = load i32, i32* %curr_max, align 4
  %cmp = icmp eq i32 %4, %6
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %7 = load %struct.Table*, %struct.Table** %table, align 4
  %curr_max1 = getelementptr inbounds %struct.Table, %struct.Table* %7, i32 0, i32 2
  %8 = load i32, i32* %curr_max1, align 4
  %add = add i32 %8, 1
  store i32 %add, i32* %curr_max1, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %9 = load i32, i32* %index, align 4
  %10 = load %struct.Table*, %struct.Table** %table, align 4
  %max_count = getelementptr inbounds %struct.Table, %struct.Table* %10, i32 0, i32 0
  %11 = load i32, i32* %max_count, align 4
  %cmp2 = icmp ult i32 %9, %11
  br i1 %cmp2, label %if.then3, label %if.end6

if.then3:                                         ; preds = %if.end
  %12 = load i32, i32* %entity_id.addr, align 4
  %13 = load %struct.Table*, %struct.Table** %table, align 4
  %entity_id4 = getelementptr inbounds %struct.Table, %struct.Table* %13, i32 0, i32 3
  %14 = load i32*, i32** %entity_id4, align 4
  %15 = load i32, i32* %index, align 4
  %arrayidx = getelementptr inbounds i32, i32* %14, i32 %15
  store i32 %12, i32* %arrayidx, align 4
  %16 = load %struct.Table*, %struct.Table** %table, align 4
  %used = getelementptr inbounds %struct.Table, %struct.Table* %16, i32 0, i32 1
  %17 = load i8*, i8** %used, align 4
  %18 = load i32, i32* %index, align 4
  %arrayidx5 = getelementptr inbounds i8, i8* %17, i32 %18
  store i8 1, i8* %arrayidx5, align 1
  br label %if.end6

if.end6:                                          ; preds = %if.then3, %if.end
  %19 = load i32, i32* %index, align 4
  ret i32 %19
}

; Function Attrs: noinline nounwind optnone
define hidden void @remove_table_item(i8* %table_ptr, i32 %entity_id) #0 {
entry:
  %table_ptr.addr = alloca i8*, align 4
  %entity_id.addr = alloca i32, align 4
  %table = alloca %struct.Table*, align 4
  %index = alloca i32, align 4
  store i8* %table_ptr, i8** %table_ptr.addr, align 4
  store i32 %entity_id, i32* %entity_id.addr, align 4
  %0 = load i8*, i8** %table_ptr.addr, align 4
  %1 = bitcast i8* %0 to %struct.Table*
  store %struct.Table* %1, %struct.Table** %table, align 4
  %2 = load i8*, i8** %table_ptr.addr, align 4
  %3 = load i32, i32* %entity_id.addr, align 4
  %call = call i32 @find_item_index(i8* %2, i32 %3)
  store i32 %call, i32* %index, align 4
  %4 = load %struct.Table*, %struct.Table** %table, align 4
  %used = getelementptr inbounds %struct.Table, %struct.Table* %4, i32 0, i32 1
  %5 = load i8*, i8** %used, align 4
  %6 = load i32, i32* %index, align 4
  %arrayidx = getelementptr inbounds i8, i8* %5, i32 %6
  store i8 0, i8* %arrayidx, align 1
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %7 = load %struct.Table*, %struct.Table** %table, align 4
  %used1 = getelementptr inbounds %struct.Table, %struct.Table* %7, i32 0, i32 1
  %8 = load i8*, i8** %used1, align 4
  %9 = load %struct.Table*, %struct.Table** %table, align 4
  %curr_max = getelementptr inbounds %struct.Table, %struct.Table* %9, i32 0, i32 2
  %10 = load i32, i32* %curr_max, align 4
  %sub = sub i32 %10, 1
  %arrayidx2 = getelementptr inbounds i8, i8* %8, i32 %sub
  %11 = load i8, i8* %arrayidx2, align 1
  %conv = zext i8 %11 to i32
  %cmp = icmp eq i32 %conv, 0
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %while.cond
  %12 = load %struct.Table*, %struct.Table** %table, align 4
  %curr_max4 = getelementptr inbounds %struct.Table, %struct.Table* %12, i32 0, i32 2
  %13 = load i32, i32* %curr_max4, align 4
  %cmp5 = icmp ugt i32 %13, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.cond
  %14 = phi i1 [ false, %while.cond ], [ %cmp5, %land.rhs ]
  br i1 %14, label %while.body, label %while.end

while.body:                                       ; preds = %land.end
  %15 = load %struct.Table*, %struct.Table** %table, align 4
  %curr_max7 = getelementptr inbounds %struct.Table, %struct.Table* %15, i32 0, i32 2
  %16 = load i32, i32* %curr_max7, align 4
  %sub8 = sub i32 %16, 1
  store i32 %sub8, i32* %curr_max7, align 4
  br label %while.cond

while.end:                                        ; preds = %land.end
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @alloc_entity_table(i32 %max_count) #0 {
entry:
  %max_count.addr = alloca i32, align 4
  %table = alloca %struct.Entity_Table*, align 4
  %i = alloca i32, align 4
  store i32 %max_count, i32* %max_count.addr, align 4
  %call = call i8* @malloc(i32 12)
  %0 = bitcast i8* %call to %struct.Entity_Table*
  store %struct.Entity_Table* %0, %struct.Entity_Table** %table, align 4
  %1 = load %struct.Entity_Table*, %struct.Entity_Table** %table, align 4
  store %struct.Entity_Table* %1, %struct.Entity_Table** @entity_table, align 4
  %2 = load i32, i32* %max_count.addr, align 4
  %3 = load %struct.Entity_Table*, %struct.Entity_Table** %table, align 4
  %max_count1 = getelementptr inbounds %struct.Entity_Table, %struct.Entity_Table* %3, i32 0, i32 0
  store i32 %2, i32* %max_count1, align 4
  %4 = load i32, i32* %max_count.addr, align 4
  %mul = mul i32 %4, 1
  %call2 = call i8* @malloc(i32 %mul)
  %5 = load %struct.Entity_Table*, %struct.Entity_Table** %table, align 4
  %used = getelementptr inbounds %struct.Entity_Table, %struct.Entity_Table* %5, i32 0, i32 1
  store i8* %call2, i8** %used, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %6 = load i32, i32* %i, align 4
  %7 = load i32, i32* %max_count.addr, align 4
  %cmp = icmp ult i32 %6, %7
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %8 = load %struct.Entity_Table*, %struct.Entity_Table** %table, align 4
  %used3 = getelementptr inbounds %struct.Entity_Table, %struct.Entity_Table* %8, i32 0, i32 1
  %9 = load i8*, i8** %used3, align 4
  %10 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i8, i8* %9, i32 %10
  store i8 0, i8* %arrayidx, align 1
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4
  %add = add i32 %11, 1
  store i32 %add, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %12 = load %struct.Entity_Table*, %struct.Entity_Table** %table, align 4
  %curr_max = getelementptr inbounds %struct.Entity_Table, %struct.Entity_Table* %12, i32 0, i32 2
  store i32 0, i32* %curr_max, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @create_entity() #0 {
entry:
  %entity_id = alloca i32, align 4
  %0 = load %struct.Entity_Table*, %struct.Entity_Table** @entity_table, align 4
  %1 = bitcast %struct.Entity_Table* %0 to i8*
  %call = call i32 @find_first_unused_item(i8* %1)
  store i32 %call, i32* %entity_id, align 4
  %2 = load i32, i32* %entity_id, align 4
  %3 = load %struct.Entity_Table*, %struct.Entity_Table** @entity_table, align 4
  %curr_max = getelementptr inbounds %struct.Entity_Table, %struct.Entity_Table* %3, i32 0, i32 2
  %4 = load i32, i32* %curr_max, align 4
  %cmp = icmp eq i32 %2, %4
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %5 = load %struct.Entity_Table*, %struct.Entity_Table** @entity_table, align 4
  %curr_max1 = getelementptr inbounds %struct.Entity_Table, %struct.Entity_Table* %5, i32 0, i32 2
  %6 = load i32, i32* %curr_max1, align 4
  %add = add i32 %6, 1
  store i32 %add, i32* %curr_max1, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %7 = load i32, i32* %entity_id, align 4
  %8 = load %struct.Entity_Table*, %struct.Entity_Table** @entity_table, align 4
  %max_count = getelementptr inbounds %struct.Entity_Table, %struct.Entity_Table* %8, i32 0, i32 0
  %9 = load i32, i32* %max_count, align 4
  %cmp2 = icmp ult i32 %7, %9
  br i1 %cmp2, label %if.then3, label %if.end4

if.then3:                                         ; preds = %if.end
  %10 = load %struct.Entity_Table*, %struct.Entity_Table** @entity_table, align 4
  %used = getelementptr inbounds %struct.Entity_Table, %struct.Entity_Table* %10, i32 0, i32 1
  %11 = load i8*, i8** %used, align 4
  %12 = load i32, i32* %entity_id, align 4
  %arrayidx = getelementptr inbounds i8, i8* %11, i32 %12
  store i8 1, i8* %arrayidx, align 1
  br label %if.end4

if.end4:                                          ; preds = %if.then3, %if.end
  %13 = load i32, i32* %entity_id, align 4
  ret i32 %13
}

; Function Attrs: noinline nounwind optnone
define hidden void @remove_entity(i32 %entity_id) #0 {
entry:
  %entity_id.addr = alloca i32, align 4
  %table = alloca %struct.Table*, align 4
  store i32 %entity_id, i32* %entity_id.addr, align 4
  %0 = load %struct.Entity_Table*, %struct.Entity_Table** @entity_table, align 4
  %1 = bitcast %struct.Entity_Table* %0 to %struct.Table*
  store %struct.Table* %1, %struct.Table** %table, align 4
  %2 = load %struct.Table*, %struct.Table** %table, align 4
  %used = getelementptr inbounds %struct.Table, %struct.Table* %2, i32 0, i32 1
  %3 = load i8*, i8** %used, align 4
  %4 = load i32, i32* %entity_id.addr, align 4
  %arrayidx = getelementptr inbounds i8, i8* %3, i32 %4
  store i8 0, i8* %arrayidx, align 1
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %5 = load %struct.Table*, %struct.Table** %table, align 4
  %used1 = getelementptr inbounds %struct.Table, %struct.Table* %5, i32 0, i32 1
  %6 = load i8*, i8** %used1, align 4
  %7 = load %struct.Table*, %struct.Table** %table, align 4
  %curr_max = getelementptr inbounds %struct.Table, %struct.Table* %7, i32 0, i32 2
  %8 = load i32, i32* %curr_max, align 4
  %sub = sub i32 %8, 1
  %arrayidx2 = getelementptr inbounds i8, i8* %6, i32 %sub
  %9 = load i8, i8* %arrayidx2, align 1
  %conv = zext i8 %9 to i32
  %cmp = icmp eq i32 %conv, 0
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %while.cond
  %10 = load %struct.Table*, %struct.Table** %table, align 4
  %curr_max4 = getelementptr inbounds %struct.Table, %struct.Table* %10, i32 0, i32 2
  %11 = load i32, i32* %curr_max4, align 4
  %cmp5 = icmp ugt i32 %11, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.cond
  %12 = phi i1 [ false, %while.cond ], [ %cmp5, %land.rhs ]
  br i1 %12, label %while.body, label %while.end

while.body:                                       ; preds = %land.end
  %13 = load %struct.Table*, %struct.Table** %table, align 4
  %curr_max7 = getelementptr inbounds %struct.Table, %struct.Table* %13, i32 0, i32 2
  %14 = load i32, i32* %curr_max7, align 4
  %sub8 = sub i32 %14, 1
  store i32 %sub8, i32* %curr_max7, align 4
  br label %while.cond

while.end:                                        ; preds = %land.end
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @alloc_physics_states(i32 %max_count) #0 {
entry:
  %max_count.addr = alloca i32, align 4
  store i32 %max_count, i32* %max_count.addr, align 4
  %call = call i8* @malloc(i32 36)
  %0 = bitcast i8* %call to %struct.Physics_States*
  store %struct.Physics_States* %0, %struct.Physics_States** @physics_states, align 4
  %1 = load %struct.Physics_States*, %struct.Physics_States** @physics_states, align 4
  %2 = bitcast %struct.Physics_States* %1 to i8*
  %3 = load i32, i32* %max_count.addr, align 4
  call void @alloc_table(i8* %2, i32 %3)
  %4 = load i32, i32* %max_count.addr, align 4
  %mul = mul i32 %4, 4
  %call1 = call i8* @malloc(i32 %mul)
  %5 = bitcast i8* %call1 to float*
  %6 = load %struct.Physics_States*, %struct.Physics_States** @physics_states, align 4
  %x = getelementptr inbounds %struct.Physics_States, %struct.Physics_States* %6, i32 0, i32 4
  store float* %5, float** %x, align 4
  %7 = load i32, i32* %max_count.addr, align 4
  %mul2 = mul i32 %7, 4
  %call3 = call i8* @malloc(i32 %mul2)
  %8 = bitcast i8* %call3 to float*
  %9 = load %struct.Physics_States*, %struct.Physics_States** @physics_states, align 4
  %y = getelementptr inbounds %struct.Physics_States, %struct.Physics_States* %9, i32 0, i32 5
  store float* %8, float** %y, align 4
  %10 = load i32, i32* %max_count.addr, align 4
  %mul4 = mul i32 %10, 4
  %call5 = call i8* @malloc(i32 %mul4)
  %11 = bitcast i8* %call5 to float*
  %12 = load %struct.Physics_States*, %struct.Physics_States** @physics_states, align 4
  %x_speed = getelementptr inbounds %struct.Physics_States, %struct.Physics_States* %12, i32 0, i32 6
  store float* %11, float** %x_speed, align 4
  %13 = load i32, i32* %max_count.addr, align 4
  %mul6 = mul i32 %13, 4
  %call7 = call i8* @malloc(i32 %mul6)
  %14 = bitcast i8* %call7 to float*
  %15 = load %struct.Physics_States*, %struct.Physics_States** @physics_states, align 4
  %y_speed = getelementptr inbounds %struct.Physics_States, %struct.Physics_States* %15, i32 0, i32 7
  store float* %14, float** %y_speed, align 4
  %16 = load i32, i32* %max_count.addr, align 4
  %mul8 = mul i32 %16, 4
  %call9 = call i8* @malloc(i32 %mul8)
  %17 = bitcast i8* %call9 to float*
  %18 = load %struct.Physics_States*, %struct.Physics_States** @physics_states, align 4
  %angle = getelementptr inbounds %struct.Physics_States, %struct.Physics_States* %18, i32 0, i32 8
  store float* %17, float** %angle, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @add_physics_state(i32 %entity_id, float %x, float %y, float %x_speed, float %y_speed) #0 {
entry:
  %entity_id.addr = alloca i32, align 4
  %x.addr = alloca float, align 4
  %y.addr = alloca float, align 4
  %x_speed.addr = alloca float, align 4
  %y_speed.addr = alloca float, align 4
  %index = alloca i32, align 4
  store i32 %entity_id, i32* %entity_id.addr, align 4
  store float %x, float* %x.addr, align 4
  store float %y, float* %y.addr, align 4
  store float %x_speed, float* %x_speed.addr, align 4
  store float %y_speed, float* %y_speed.addr, align 4
  %0 = load %struct.Physics_States*, %struct.Physics_States** @physics_states, align 4
  %1 = bitcast %struct.Physics_States* %0 to i8*
  %2 = load i32, i32* %entity_id.addr, align 4
  %call = call i32 @add_table_item(i8* %1, i32 %2)
  store i32 %call, i32* %index, align 4
  %3 = load i32, i32* %index, align 4
  %4 = load %struct.Physics_States*, %struct.Physics_States** @physics_states, align 4
  %max_count = getelementptr inbounds %struct.Physics_States, %struct.Physics_States* %4, i32 0, i32 0
  %5 = load i32, i32* %max_count, align 4
  %cmp = icmp ult i32 %3, %5
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %6 = load float, float* %x.addr, align 4
  %7 = load %struct.Physics_States*, %struct.Physics_States** @physics_states, align 4
  %x1 = getelementptr inbounds %struct.Physics_States, %struct.Physics_States* %7, i32 0, i32 4
  %8 = load float*, float** %x1, align 4
  %9 = load i32, i32* %index, align 4
  %arrayidx = getelementptr inbounds float, float* %8, i32 %9
  store float %6, float* %arrayidx, align 4
  %10 = load float, float* %y.addr, align 4
  %11 = load %struct.Physics_States*, %struct.Physics_States** @physics_states, align 4
  %y2 = getelementptr inbounds %struct.Physics_States, %struct.Physics_States* %11, i32 0, i32 5
  %12 = load float*, float** %y2, align 4
  %13 = load i32, i32* %index, align 4
  %arrayidx3 = getelementptr inbounds float, float* %12, i32 %13
  store float %10, float* %arrayidx3, align 4
  %14 = load float, float* %x_speed.addr, align 4
  %15 = load %struct.Physics_States*, %struct.Physics_States** @physics_states, align 4
  %x_speed4 = getelementptr inbounds %struct.Physics_States, %struct.Physics_States* %15, i32 0, i32 6
  %16 = load float*, float** %x_speed4, align 4
  %17 = load i32, i32* %index, align 4
  %arrayidx5 = getelementptr inbounds float, float* %16, i32 %17
  store float %14, float* %arrayidx5, align 4
  %18 = load float, float* %y_speed.addr, align 4
  %19 = load %struct.Physics_States*, %struct.Physics_States** @physics_states, align 4
  %y_speed6 = getelementptr inbounds %struct.Physics_States, %struct.Physics_States* %19, i32 0, i32 7
  %20 = load float*, float** %y_speed6, align 4
  %21 = load i32, i32* %index, align 4
  %arrayidx7 = getelementptr inbounds float, float* %20, i32 %21
  store float %18, float* %arrayidx7, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %22 = load i32, i32* %index, align 4
  ret i32 %22
}

; Function Attrs: noinline nounwind optnone
define hidden void @remove_physics_state(i32 %entity_id) #0 {
entry:
  %entity_id.addr = alloca i32, align 4
  store i32 %entity_id, i32* %entity_id.addr, align 4
  %0 = load %struct.Physics_States*, %struct.Physics_States** @physics_states, align 4
  %1 = bitcast %struct.Physics_States* %0 to i8*
  %2 = load i32, i32* %entity_id.addr, align 4
  call void @remove_table_item(i8* %1, i32 %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden %struct.Physics_States* @get_physics_states() #0 {
entry:
  %0 = load %struct.Physics_States*, %struct.Physics_States** @physics_states, align 4
  ret %struct.Physics_States* %0
}

; Function Attrs: noinline nounwind optnone
define hidden void @alloc_physics_balls(i32 %max_count) #0 {
entry:
  %max_count.addr = alloca i32, align 4
  store i32 %max_count, i32* %max_count.addr, align 4
  %call = call i8* @malloc(i32 24)
  %0 = bitcast i8* %call to %struct.Physics_Balls*
  store %struct.Physics_Balls* %0, %struct.Physics_Balls** @physics_balls, align 4
  %1 = load %struct.Physics_Balls*, %struct.Physics_Balls** @physics_balls, align 4
  %2 = bitcast %struct.Physics_Balls* %1 to i8*
  %3 = load i32, i32* %max_count.addr, align 4
  call void @alloc_table(i8* %2, i32 %3)
  %4 = load i32, i32* %max_count.addr, align 4
  %mul = mul i32 %4, 4
  %call1 = call i8* @malloc(i32 %mul)
  %5 = bitcast i8* %call1 to float*
  %6 = load %struct.Physics_Balls*, %struct.Physics_Balls** @physics_balls, align 4
  %radius = getelementptr inbounds %struct.Physics_Balls, %struct.Physics_Balls* %6, i32 0, i32 4
  store float* %5, float** %radius, align 4
  %7 = load i32, i32* %max_count.addr, align 4
  %mul2 = mul i32 %7, 4
  %call3 = call i8* @malloc(i32 %mul2)
  %8 = bitcast i8* %call3 to float*
  %9 = load %struct.Physics_Balls*, %struct.Physics_Balls** @physics_balls, align 4
  %mass = getelementptr inbounds %struct.Physics_Balls, %struct.Physics_Balls* %9, i32 0, i32 5
  store float* %8, float** %mass, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @add_physics_ball(i32 %entity_id, float %radius, float %mass) #0 {
entry:
  %entity_id.addr = alloca i32, align 4
  %radius.addr = alloca float, align 4
  %mass.addr = alloca float, align 4
  %index = alloca i32, align 4
  store i32 %entity_id, i32* %entity_id.addr, align 4
  store float %radius, float* %radius.addr, align 4
  store float %mass, float* %mass.addr, align 4
  %0 = load %struct.Physics_Balls*, %struct.Physics_Balls** @physics_balls, align 4
  %1 = bitcast %struct.Physics_Balls* %0 to i8*
  %2 = load i32, i32* %entity_id.addr, align 4
  %call = call i32 @add_table_item(i8* %1, i32 %2)
  store i32 %call, i32* %index, align 4
  %3 = load i32, i32* %index, align 4
  %4 = load %struct.Physics_Balls*, %struct.Physics_Balls** @physics_balls, align 4
  %max_count = getelementptr inbounds %struct.Physics_Balls, %struct.Physics_Balls* %4, i32 0, i32 0
  %5 = load i32, i32* %max_count, align 4
  %cmp = icmp ult i32 %3, %5
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %6 = load float, float* %radius.addr, align 4
  %7 = load %struct.Physics_Balls*, %struct.Physics_Balls** @physics_balls, align 4
  %radius1 = getelementptr inbounds %struct.Physics_Balls, %struct.Physics_Balls* %7, i32 0, i32 4
  %8 = load float*, float** %radius1, align 4
  %9 = load i32, i32* %index, align 4
  %arrayidx = getelementptr inbounds float, float* %8, i32 %9
  store float %6, float* %arrayidx, align 4
  %10 = load float, float* %mass.addr, align 4
  %11 = load %struct.Physics_Balls*, %struct.Physics_Balls** @physics_balls, align 4
  %mass2 = getelementptr inbounds %struct.Physics_Balls, %struct.Physics_Balls* %11, i32 0, i32 5
  %12 = load float*, float** %mass2, align 4
  %13 = load i32, i32* %index, align 4
  %arrayidx3 = getelementptr inbounds float, float* %12, i32 %13
  store float %10, float* %arrayidx3, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %14 = load i32, i32* %index, align 4
  ret i32 %14
}

; Function Attrs: noinline nounwind optnone
define hidden void @remove_physics_ball(i32 %entity_id) #0 {
entry:
  %entity_id.addr = alloca i32, align 4
  store i32 %entity_id, i32* %entity_id.addr, align 4
  %0 = load %struct.Physics_Balls*, %struct.Physics_Balls** @physics_balls, align 4
  %1 = bitcast %struct.Physics_Balls* %0 to i8*
  %2 = load i32, i32* %entity_id.addr, align 4
  call void @remove_table_item(i8* %1, i32 %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @alloc_collision_table(i32 %max_count) #0 {
entry:
  %max_count.addr = alloca i32, align 4
  store i32 %max_count, i32* %max_count.addr, align 4
  %call = call i8* @malloc(i32 20)
  %0 = bitcast i8* %call to %struct.Collision_Table*
  store %struct.Collision_Table* %0, %struct.Collision_Table** @collision_table, align 4
  %1 = load %struct.Collision_Table*, %struct.Collision_Table** @collision_table, align 4
  %2 = bitcast %struct.Collision_Table* %1 to i8*
  %3 = load i32, i32* %max_count.addr, align 4
  call void @alloc_table(i8* %2, i32 %3)
  %4 = load i32, i32* %max_count.addr, align 4
  %mul = mul i32 %4, 4
  %call1 = call i8* @malloc(i32 %mul)
  %5 = bitcast i8* %call1 to i32*
  %6 = load %struct.Collision_Table*, %struct.Collision_Table** @collision_table, align 4
  %entity_id_2 = getelementptr inbounds %struct.Collision_Table, %struct.Collision_Table* %6, i32 0, i32 4
  store i32* %5, i32** %entity_id_2, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @add_collision_item(i32 %entity_id, i32 %entity_id_2) #0 {
entry:
  %entity_id.addr = alloca i32, align 4
  %entity_id_2.addr = alloca i32, align 4
  %index = alloca i32, align 4
  store i32 %entity_id, i32* %entity_id.addr, align 4
  store i32 %entity_id_2, i32* %entity_id_2.addr, align 4
  %0 = load %struct.Collision_Table*, %struct.Collision_Table** @collision_table, align 4
  %1 = bitcast %struct.Collision_Table* %0 to i8*
  %2 = load i32, i32* %entity_id.addr, align 4
  %call = call i32 @add_table_item(i8* %1, i32 %2)
  store i32 %call, i32* %index, align 4
  %3 = load i32, i32* %index, align 4
  %4 = load %struct.Collision_Table*, %struct.Collision_Table** @collision_table, align 4
  %max_count = getelementptr inbounds %struct.Collision_Table, %struct.Collision_Table* %4, i32 0, i32 0
  %5 = load i32, i32* %max_count, align 4
  %cmp = icmp ult i32 %3, %5
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %6 = load i32, i32* %entity_id_2.addr, align 4
  %7 = load %struct.Collision_Table*, %struct.Collision_Table** @collision_table, align 4
  %entity_id_21 = getelementptr inbounds %struct.Collision_Table, %struct.Collision_Table* %7, i32 0, i32 4
  %8 = load i32*, i32** %entity_id_21, align 4
  %9 = load i32, i32* %index, align 4
  %arrayidx = getelementptr inbounds i32, i32* %8, i32 %9
  store i32 %6, i32* %arrayidx, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %10 = load i32, i32* %index, align 4
  ret i32 %10
}

; Function Attrs: noinline nounwind optnone
define hidden void @clear_collision_table() #0 {
entry:
  %i = alloca i32, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load %struct.Collision_Table*, %struct.Collision_Table** @collision_table, align 4
  %curr_max = getelementptr inbounds %struct.Collision_Table, %struct.Collision_Table* %1, i32 0, i32 2
  %2 = load i32, i32* %curr_max, align 4
  %cmp = icmp ult i32 %0, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %struct.Collision_Table*, %struct.Collision_Table** @collision_table, align 4
  %used = getelementptr inbounds %struct.Collision_Table, %struct.Collision_Table* %3, i32 0, i32 1
  %4 = load i8*, i8** %used, align 4
  %5 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i8, i8* %4, i32 %5
  store i8 0, i8* %arrayidx, align 1
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4
  %add = add i32 %6, 1
  store i32 %add, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = load %struct.Collision_Table*, %struct.Collision_Table** @collision_table, align 4
  %curr_max1 = getelementptr inbounds %struct.Collision_Table, %struct.Collision_Table* %7, i32 0, i32 2
  store i32 0, i32* %curr_max1, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden %struct.Collision_Table* @get_collision_table() #0 {
entry:
  %0 = load %struct.Collision_Table*, %struct.Collision_Table** @collision_table, align 4
  ret %struct.Collision_Table* %0
}

; Function Attrs: noinline nounwind optnone
define hidden void @alloc_proximity_attack(i32 %max_count) #0 {
entry:
  %max_count.addr = alloca i32, align 4
  store i32 %max_count, i32* %max_count.addr, align 4
  %call = call i8* @malloc(i32 24)
  %0 = bitcast i8* %call to %struct.Proximity_Attack*
  store %struct.Proximity_Attack* %0, %struct.Proximity_Attack** @proximity_attack, align 4
  %1 = load %struct.Proximity_Attack*, %struct.Proximity_Attack** @proximity_attack, align 4
  %2 = bitcast %struct.Proximity_Attack* %1 to i8*
  %3 = load i32, i32* %max_count.addr, align 4
  call void @alloc_table(i8* %2, i32 %3)
  %4 = load i32, i32* %max_count.addr, align 4
  %mul = mul i32 %4, 4
  %call1 = call i8* @malloc(i32 %mul)
  %5 = bitcast i8* %call1 to float*
  %6 = load %struct.Proximity_Attack*, %struct.Proximity_Attack** @proximity_attack, align 4
  %attack_state = getelementptr inbounds %struct.Proximity_Attack, %struct.Proximity_Attack* %6, i32 0, i32 4
  store float* %5, float** %attack_state, align 4
  %7 = load i32, i32* %max_count.addr, align 4
  %mul2 = mul i32 %7, 4
  %call3 = call i8* @malloc(i32 %mul2)
  %8 = bitcast i8* %call3 to float*
  %9 = load %struct.Proximity_Attack*, %struct.Proximity_Attack** @proximity_attack, align 4
  %damage = getelementptr inbounds %struct.Proximity_Attack, %struct.Proximity_Attack* %9, i32 0, i32 5
  store float* %8, float** %damage, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @add_proximity_attack(i32 %entity_id, float %attack_state, float %damage) #0 {
entry:
  %entity_id.addr = alloca i32, align 4
  %attack_state.addr = alloca float, align 4
  %damage.addr = alloca float, align 4
  %index = alloca i32, align 4
  store i32 %entity_id, i32* %entity_id.addr, align 4
  store float %attack_state, float* %attack_state.addr, align 4
  store float %damage, float* %damage.addr, align 4
  %0 = load %struct.Proximity_Attack*, %struct.Proximity_Attack** @proximity_attack, align 4
  %1 = bitcast %struct.Proximity_Attack* %0 to i8*
  %2 = load i32, i32* %entity_id.addr, align 4
  %call = call i32 @add_table_item(i8* %1, i32 %2)
  store i32 %call, i32* %index, align 4
  %3 = load i32, i32* %index, align 4
  %4 = load %struct.Proximity_Attack*, %struct.Proximity_Attack** @proximity_attack, align 4
  %max_count = getelementptr inbounds %struct.Proximity_Attack, %struct.Proximity_Attack* %4, i32 0, i32 0
  %5 = load i32, i32* %max_count, align 4
  %cmp = icmp ult i32 %3, %5
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %6 = load float, float* %attack_state.addr, align 4
  %7 = load %struct.Proximity_Attack*, %struct.Proximity_Attack** @proximity_attack, align 4
  %attack_state1 = getelementptr inbounds %struct.Proximity_Attack, %struct.Proximity_Attack* %7, i32 0, i32 4
  %8 = load float*, float** %attack_state1, align 4
  %9 = load i32, i32* %index, align 4
  %arrayidx = getelementptr inbounds float, float* %8, i32 %9
  store float %6, float* %arrayidx, align 4
  %10 = load float, float* %damage.addr, align 4
  %11 = load %struct.Proximity_Attack*, %struct.Proximity_Attack** @proximity_attack, align 4
  %damage2 = getelementptr inbounds %struct.Proximity_Attack, %struct.Proximity_Attack* %11, i32 0, i32 5
  %12 = load float*, float** %damage2, align 4
  %13 = load i32, i32* %index, align 4
  %arrayidx3 = getelementptr inbounds float, float* %12, i32 %13
  store float %10, float* %arrayidx3, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %14 = load i32, i32* %index, align 4
  ret i32 %14
}

; Function Attrs: noinline nounwind optnone
define hidden void @remove_proximity_attack(i32 %entity_id) #0 {
entry:
  %entity_id.addr = alloca i32, align 4
  store i32 %entity_id, i32* %entity_id.addr, align 4
  %0 = load %struct.Proximity_Attack*, %struct.Proximity_Attack** @proximity_attack, align 4
  %1 = bitcast %struct.Proximity_Attack* %0 to i8*
  %2 = load i32, i32* %entity_id.addr, align 4
  call void @remove_table_item(i8* %1, i32 %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @alloc_hit_feedback_table(i32 %max_count) #0 {
entry:
  %max_count.addr = alloca i32, align 4
  store i32 %max_count, i32* %max_count.addr, align 4
  %call = call i8* @malloc(i32 20)
  %0 = bitcast i8* %call to %struct.Hit_Feedback_Table*
  store %struct.Hit_Feedback_Table* %0, %struct.Hit_Feedback_Table** @hit_feedback_table, align 4
  %1 = load %struct.Hit_Feedback_Table*, %struct.Hit_Feedback_Table** @hit_feedback_table, align 4
  %2 = bitcast %struct.Hit_Feedback_Table* %1 to i8*
  %3 = load i32, i32* %max_count.addr, align 4
  call void @alloc_table(i8* %2, i32 %3)
  %4 = load i32, i32* %max_count.addr, align 4
  %mul = mul i32 %4, 4
  %call1 = call i8* @malloc(i32 %mul)
  %5 = bitcast i8* %call1 to float*
  %6 = load %struct.Hit_Feedback_Table*, %struct.Hit_Feedback_Table** @hit_feedback_table, align 4
  %amount = getelementptr inbounds %struct.Hit_Feedback_Table, %struct.Hit_Feedback_Table* %6, i32 0, i32 4
  store float* %5, float** %amount, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @add_hit_feedback_item(i32 %entity_id, float %amount) #0 {
entry:
  %entity_id.addr = alloca i32, align 4
  %amount.addr = alloca float, align 4
  %index = alloca i32, align 4
  store i32 %entity_id, i32* %entity_id.addr, align 4
  store float %amount, float* %amount.addr, align 4
  %0 = load %struct.Hit_Feedback_Table*, %struct.Hit_Feedback_Table** @hit_feedback_table, align 4
  %1 = bitcast %struct.Hit_Feedback_Table* %0 to i8*
  %2 = load i32, i32* %entity_id.addr, align 4
  %call = call i32 @add_table_item(i8* %1, i32 %2)
  store i32 %call, i32* %index, align 4
  %3 = load i32, i32* %index, align 4
  %4 = load %struct.Hit_Feedback_Table*, %struct.Hit_Feedback_Table** @hit_feedback_table, align 4
  %max_count = getelementptr inbounds %struct.Hit_Feedback_Table, %struct.Hit_Feedback_Table* %4, i32 0, i32 0
  %5 = load i32, i32* %max_count, align 4
  %cmp = icmp ult i32 %3, %5
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %6 = load float, float* %amount.addr, align 4
  %7 = load %struct.Hit_Feedback_Table*, %struct.Hit_Feedback_Table** @hit_feedback_table, align 4
  %amount1 = getelementptr inbounds %struct.Hit_Feedback_Table, %struct.Hit_Feedback_Table* %7, i32 0, i32 4
  %8 = load float*, float** %amount1, align 4
  %9 = load i32, i32* %index, align 4
  %arrayidx = getelementptr inbounds float, float* %8, i32 %9
  store float %6, float* %arrayidx, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %10 = load i32, i32* %index, align 4
  ret i32 %10
}

; Function Attrs: noinline nounwind optnone
define hidden void @clear_hit_feedback_table() #0 {
entry:
  %i = alloca i32, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load %struct.Hit_Feedback_Table*, %struct.Hit_Feedback_Table** @hit_feedback_table, align 4
  %curr_max = getelementptr inbounds %struct.Hit_Feedback_Table, %struct.Hit_Feedback_Table* %1, i32 0, i32 2
  %2 = load i32, i32* %curr_max, align 4
  %cmp = icmp ult i32 %0, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %struct.Hit_Feedback_Table*, %struct.Hit_Feedback_Table** @hit_feedback_table, align 4
  %used = getelementptr inbounds %struct.Hit_Feedback_Table, %struct.Hit_Feedback_Table* %3, i32 0, i32 1
  %4 = load i8*, i8** %used, align 4
  %5 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i8, i8* %4, i32 %5
  store i8 0, i8* %arrayidx, align 1
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4
  %add = add i32 %6, 1
  store i32 %add, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = load %struct.Hit_Feedback_Table*, %struct.Hit_Feedback_Table** @hit_feedback_table, align 4
  %curr_max1 = getelementptr inbounds %struct.Hit_Feedback_Table, %struct.Hit_Feedback_Table* %7, i32 0, i32 2
  store i32 0, i32* %curr_max1, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden %struct.Hit_Feedback_Table* @get_hit_feedback_table() #0 {
entry:
  %0 = load %struct.Hit_Feedback_Table*, %struct.Hit_Feedback_Table** @hit_feedback_table, align 4
  ret %struct.Hit_Feedback_Table* %0
}

; Function Attrs: noinline nounwind optnone
define hidden void @alloc_sprite_map(i32 %max_count) #0 {
entry:
  %max_count.addr = alloca i32, align 4
  store i32 %max_count, i32* %max_count.addr, align 4
  %call = call i8* @malloc(i32 36)
  %0 = bitcast i8* %call to %struct.Sprite_Map*
  store %struct.Sprite_Map* %0, %struct.Sprite_Map** @sprite_map, align 4
  %1 = load %struct.Sprite_Map*, %struct.Sprite_Map** @sprite_map, align 4
  %2 = bitcast %struct.Sprite_Map* %1 to i8*
  %3 = load i32, i32* %max_count.addr, align 4
  call void @alloc_table(i8* %2, i32 %3)
  %4 = load i32, i32* %max_count.addr, align 4
  %mul = mul i32 %4, 4
  %call1 = call i8* @malloc(i32 %mul)
  %5 = bitcast i8* %call1 to i32*
  %6 = load %struct.Sprite_Map*, %struct.Sprite_Map** @sprite_map, align 4
  %sprite_id = getelementptr inbounds %struct.Sprite_Map, %struct.Sprite_Map* %6, i32 0, i32 4
  store i32* %5, i32** %sprite_id, align 4
  %7 = load i32, i32* %max_count.addr, align 4
  %mul2 = mul i32 %7, 4
  %call3 = call i8* @malloc(i32 %mul2)
  %8 = bitcast i8* %call3 to float*
  %9 = load %struct.Sprite_Map*, %struct.Sprite_Map** @sprite_map, align 4
  %sprite_origin_x = getelementptr inbounds %struct.Sprite_Map, %struct.Sprite_Map* %9, i32 0, i32 5
  store float* %8, float** %sprite_origin_x, align 4
  %10 = load i32, i32* %max_count.addr, align 4
  %mul4 = mul i32 %10, 4
  %call5 = call i8* @malloc(i32 %mul4)
  %11 = bitcast i8* %call5 to float*
  %12 = load %struct.Sprite_Map*, %struct.Sprite_Map** @sprite_map, align 4
  %sprite_origin_y = getelementptr inbounds %struct.Sprite_Map, %struct.Sprite_Map* %12, i32 0, i32 6
  store float* %11, float** %sprite_origin_y, align 4
  %13 = load i32, i32* %max_count.addr, align 4
  %mul6 = mul i32 %13, 4
  %call7 = call i8* @malloc(i32 %mul6)
  %14 = bitcast i8* %call7 to float*
  %15 = load %struct.Sprite_Map*, %struct.Sprite_Map** @sprite_map, align 4
  %sprite_size = getelementptr inbounds %struct.Sprite_Map, %struct.Sprite_Map* %15, i32 0, i32 7
  store float* %14, float** %sprite_size, align 4
  %16 = load i32, i32* %max_count.addr, align 4
  %mul8 = mul i32 %16, 1
  %call9 = call i8* @malloc(i32 %mul8)
  %17 = load %struct.Sprite_Map*, %struct.Sprite_Map** @sprite_map, align 4
  %sprite_variant = getelementptr inbounds %struct.Sprite_Map, %struct.Sprite_Map* %17, i32 0, i32 8
  store i8* %call9, i8** %sprite_variant, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @add_sprite_map(i32 %entity_id, i32 %sprite_id, float %sprite_origin_x, float %sprite_origin_y, float %sprite_size, i8 zeroext %sprite_variant) #0 {
entry:
  %entity_id.addr = alloca i32, align 4
  %sprite_id.addr = alloca i32, align 4
  %sprite_origin_x.addr = alloca float, align 4
  %sprite_origin_y.addr = alloca float, align 4
  %sprite_size.addr = alloca float, align 4
  %sprite_variant.addr = alloca i8, align 1
  %index = alloca i32, align 4
  store i32 %entity_id, i32* %entity_id.addr, align 4
  store i32 %sprite_id, i32* %sprite_id.addr, align 4
  store float %sprite_origin_x, float* %sprite_origin_x.addr, align 4
  store float %sprite_origin_y, float* %sprite_origin_y.addr, align 4
  store float %sprite_size, float* %sprite_size.addr, align 4
  store i8 %sprite_variant, i8* %sprite_variant.addr, align 1
  %0 = load %struct.Sprite_Map*, %struct.Sprite_Map** @sprite_map, align 4
  %1 = bitcast %struct.Sprite_Map* %0 to i8*
  %2 = load i32, i32* %entity_id.addr, align 4
  %call = call i32 @add_table_item(i8* %1, i32 %2)
  store i32 %call, i32* %index, align 4
  %3 = load i32, i32* %index, align 4
  %4 = load %struct.Sprite_Map*, %struct.Sprite_Map** @sprite_map, align 4
  %max_count = getelementptr inbounds %struct.Sprite_Map, %struct.Sprite_Map* %4, i32 0, i32 0
  %5 = load i32, i32* %max_count, align 4
  %cmp = icmp ult i32 %3, %5
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %6 = load i32, i32* %sprite_id.addr, align 4
  %7 = load %struct.Sprite_Map*, %struct.Sprite_Map** @sprite_map, align 4
  %sprite_id1 = getelementptr inbounds %struct.Sprite_Map, %struct.Sprite_Map* %7, i32 0, i32 4
  %8 = load i32*, i32** %sprite_id1, align 4
  %9 = load i32, i32* %index, align 4
  %arrayidx = getelementptr inbounds i32, i32* %8, i32 %9
  store i32 %6, i32* %arrayidx, align 4
  %10 = load float, float* %sprite_origin_x.addr, align 4
  %11 = load %struct.Sprite_Map*, %struct.Sprite_Map** @sprite_map, align 4
  %sprite_origin_x2 = getelementptr inbounds %struct.Sprite_Map, %struct.Sprite_Map* %11, i32 0, i32 5
  %12 = load float*, float** %sprite_origin_x2, align 4
  %13 = load i32, i32* %index, align 4
  %arrayidx3 = getelementptr inbounds float, float* %12, i32 %13
  store float %10, float* %arrayidx3, align 4
  %14 = load float, float* %sprite_origin_y.addr, align 4
  %15 = load %struct.Sprite_Map*, %struct.Sprite_Map** @sprite_map, align 4
  %sprite_origin_y4 = getelementptr inbounds %struct.Sprite_Map, %struct.Sprite_Map* %15, i32 0, i32 6
  %16 = load float*, float** %sprite_origin_y4, align 4
  %17 = load i32, i32* %index, align 4
  %arrayidx5 = getelementptr inbounds float, float* %16, i32 %17
  store float %14, float* %arrayidx5, align 4
  %18 = load float, float* %sprite_size.addr, align 4
  %19 = load %struct.Sprite_Map*, %struct.Sprite_Map** @sprite_map, align 4
  %sprite_size6 = getelementptr inbounds %struct.Sprite_Map, %struct.Sprite_Map* %19, i32 0, i32 7
  %20 = load float*, float** %sprite_size6, align 4
  %21 = load i32, i32* %index, align 4
  %arrayidx7 = getelementptr inbounds float, float* %20, i32 %21
  store float %18, float* %arrayidx7, align 4
  %22 = load i8, i8* %sprite_variant.addr, align 1
  %23 = load %struct.Sprite_Map*, %struct.Sprite_Map** @sprite_map, align 4
  %sprite_variant8 = getelementptr inbounds %struct.Sprite_Map, %struct.Sprite_Map* %23, i32 0, i32 8
  %24 = load i8*, i8** %sprite_variant8, align 4
  %25 = load i32, i32* %index, align 4
  %arrayidx9 = getelementptr inbounds i8, i8* %24, i32 %25
  store i8 %22, i8* %arrayidx9, align 1
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %26 = load i32, i32* %index, align 4
  ret i32 %26
}

; Function Attrs: noinline nounwind optnone
define hidden void @remove_sprite_map(i32 %entity_id) #0 {
entry:
  %entity_id.addr = alloca i32, align 4
  store i32 %entity_id, i32* %entity_id.addr, align 4
  %0 = load %struct.Sprite_Map*, %struct.Sprite_Map** @sprite_map, align 4
  %1 = bitcast %struct.Sprite_Map* %0 to i8*
  %2 = load i32, i32* %entity_id.addr, align 4
  call void @remove_table_item(i8* %1, i32 %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden %struct.Sprite_Map* @get_sprite_map() #0 {
entry:
  %0 = load %struct.Sprite_Map*, %struct.Sprite_Map** @sprite_map, align 4
  ret %struct.Sprite_Map* %0
}

; Function Attrs: noinline nounwind optnone
define hidden void @alloc_ai_enemy(i32 %max_count) #0 {
entry:
  %max_count.addr = alloca i32, align 4
  store i32 %max_count, i32* %max_count.addr, align 4
  %call = call i8* @malloc(i32 20)
  %0 = bitcast i8* %call to %struct.AI_Enemy*
  store %struct.AI_Enemy* %0, %struct.AI_Enemy** @ai_enemy, align 4
  %1 = load %struct.AI_Enemy*, %struct.AI_Enemy** @ai_enemy, align 4
  %2 = bitcast %struct.AI_Enemy* %1 to i8*
  %3 = load i32, i32* %max_count.addr, align 4
  call void @alloc_table(i8* %2, i32 %3)
  %4 = load i32, i32* %max_count.addr, align 4
  %mul = mul i32 %4, 1
  %call1 = call i8* @malloc(i32 %mul)
  %5 = load %struct.AI_Enemy*, %struct.AI_Enemy** @ai_enemy, align 4
  %enemy_type = getelementptr inbounds %struct.AI_Enemy, %struct.AI_Enemy* %5, i32 0, i32 4
  store i8* %call1, i8** %enemy_type, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @add_ai_enemy(i32 %entity_id, i8 zeroext %enemy_type) #0 {
entry:
  %entity_id.addr = alloca i32, align 4
  %enemy_type.addr = alloca i8, align 1
  %index = alloca i32, align 4
  store i32 %entity_id, i32* %entity_id.addr, align 4
  store i8 %enemy_type, i8* %enemy_type.addr, align 1
  %0 = load %struct.AI_Enemy*, %struct.AI_Enemy** @ai_enemy, align 4
  %1 = bitcast %struct.AI_Enemy* %0 to i8*
  %2 = load i32, i32* %entity_id.addr, align 4
  %call = call i32 @add_table_item(i8* %1, i32 %2)
  store i32 %call, i32* %index, align 4
  %3 = load i32, i32* %index, align 4
  %4 = load %struct.AI_Enemy*, %struct.AI_Enemy** @ai_enemy, align 4
  %max_count = getelementptr inbounds %struct.AI_Enemy, %struct.AI_Enemy* %4, i32 0, i32 0
  %5 = load i32, i32* %max_count, align 4
  %cmp = icmp ult i32 %3, %5
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %6 = load i8, i8* %enemy_type.addr, align 1
  %7 = load %struct.AI_Enemy*, %struct.AI_Enemy** @ai_enemy, align 4
  %enemy_type1 = getelementptr inbounds %struct.AI_Enemy, %struct.AI_Enemy* %7, i32 0, i32 4
  %8 = load i8*, i8** %enemy_type1, align 4
  %9 = load i32, i32* %index, align 4
  %arrayidx = getelementptr inbounds i8, i8* %8, i32 %9
  store i8 %6, i8* %arrayidx, align 1
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %10 = load i32, i32* %index, align 4
  ret i32 %10
}

; Function Attrs: noinline nounwind optnone
define hidden void @remove_ai_enemy(i32 %entity_id) #0 {
entry:
  %entity_id.addr = alloca i32, align 4
  store i32 %entity_id, i32* %entity_id.addr, align 4
  %0 = load %struct.AI_Enemy*, %struct.AI_Enemy** @ai_enemy, align 4
  %1 = bitcast %struct.AI_Enemy* %0 to i8*
  %2 = load i32, i32* %entity_id.addr, align 4
  call void @remove_table_item(i8* %1, i32 %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden %struct.AI_Enemy* @get_ai_enemy() #0 {
entry:
  %0 = load %struct.AI_Enemy*, %struct.AI_Enemy** @ai_enemy, align 4
  ret %struct.AI_Enemy* %0
}

; Function Attrs: noinline nounwind optnone
define hidden void @alloc_bullets(i32 %max_count) #0 {
entry:
  %max_count.addr = alloca i32, align 4
  store i32 %max_count, i32* %max_count.addr, align 4
  %call = call i8* @malloc(i32 24)
  %0 = bitcast i8* %call to %struct.Bullet_Table*
  store %struct.Bullet_Table* %0, %struct.Bullet_Table** @bullets, align 4
  %1 = load %struct.Bullet_Table*, %struct.Bullet_Table** @bullets, align 4
  %2 = bitcast %struct.Bullet_Table* %1 to i8*
  %3 = load i32, i32* %max_count.addr, align 4
  call void @alloc_table(i8* %2, i32 %3)
  %4 = load i32, i32* %max_count.addr, align 4
  %mul = mul i32 %4, 4
  %call1 = call i8* @malloc(i32 %mul)
  %5 = bitcast i8* %call1 to float*
  %6 = load %struct.Bullet_Table*, %struct.Bullet_Table** @bullets, align 4
  %damage = getelementptr inbounds %struct.Bullet_Table, %struct.Bullet_Table* %6, i32 0, i32 4
  store float* %5, float** %damage, align 4
  %7 = load i32, i32* %max_count.addr, align 4
  %mul2 = mul i32 %7, 8
  %call3 = call i8* @malloc(i32 %mul2)
  %8 = bitcast i8* %call3 to %struct.timespec*
  %9 = load %struct.Bullet_Table*, %struct.Bullet_Table** @bullets, align 4
  %created_at = getelementptr inbounds %struct.Bullet_Table, %struct.Bullet_Table* %9, i32 0, i32 5
  store %struct.timespec* %8, %struct.timespec** %created_at, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @add_bullet(i32 %entity_id, float %damage, %struct.timespec* byval(%struct.timespec) align 4 %created_at) #0 {
entry:
  %entity_id.addr = alloca i32, align 4
  %damage.addr = alloca float, align 4
  %index = alloca i32, align 4
  store i32 %entity_id, i32* %entity_id.addr, align 4
  store float %damage, float* %damage.addr, align 4
  %0 = load %struct.Bullet_Table*, %struct.Bullet_Table** @bullets, align 4
  %1 = bitcast %struct.Bullet_Table* %0 to i8*
  %2 = load i32, i32* %entity_id.addr, align 4
  %call = call i32 @add_table_item(i8* %1, i32 %2)
  store i32 %call, i32* %index, align 4
  %3 = load i32, i32* %index, align 4
  %4 = load %struct.Bullet_Table*, %struct.Bullet_Table** @bullets, align 4
  %max_count = getelementptr inbounds %struct.Bullet_Table, %struct.Bullet_Table* %4, i32 0, i32 0
  %5 = load i32, i32* %max_count, align 4
  %cmp = icmp ult i32 %3, %5
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %6 = load float, float* %damage.addr, align 4
  %7 = load %struct.Bullet_Table*, %struct.Bullet_Table** @bullets, align 4
  %damage1 = getelementptr inbounds %struct.Bullet_Table, %struct.Bullet_Table* %7, i32 0, i32 4
  %8 = load float*, float** %damage1, align 4
  %9 = load i32, i32* %index, align 4
  %arrayidx = getelementptr inbounds float, float* %8, i32 %9
  store float %6, float* %arrayidx, align 4
  %10 = load %struct.Bullet_Table*, %struct.Bullet_Table** @bullets, align 4
  %created_at2 = getelementptr inbounds %struct.Bullet_Table, %struct.Bullet_Table* %10, i32 0, i32 5
  %11 = load %struct.timespec*, %struct.timespec** %created_at2, align 4
  %12 = load i32, i32* %index, align 4
  %arrayidx3 = getelementptr inbounds %struct.timespec, %struct.timespec* %11, i32 %12
  %13 = bitcast %struct.timespec* %arrayidx3 to i8*
  %14 = bitcast %struct.timespec* %created_at to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %13, i8* align 4 %14, i32 8, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %15 = load i32, i32* %index, align 4
  ret i32 %15
}

; Function Attrs: noinline nounwind optnone
define hidden void @remove_bullet(i32 %entity_id) #0 {
entry:
  %entity_id.addr = alloca i32, align 4
  store i32 %entity_id, i32* %entity_id.addr, align 4
  %0 = load %struct.Bullet_Table*, %struct.Bullet_Table** @bullets, align 4
  %1 = bitcast %struct.Bullet_Table* %0 to i8*
  %2 = load i32, i32* %entity_id.addr, align 4
  call void @remove_table_item(i8* %1, i32 %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @alloc_health_table(i32 %max_count) #0 {
entry:
  %max_count.addr = alloca i32, align 4
  store i32 %max_count, i32* %max_count.addr, align 4
  %call = call i8* @malloc(i32 24)
  %0 = bitcast i8* %call to %struct.Health_Table*
  store %struct.Health_Table* %0, %struct.Health_Table** @health_table, align 4
  %1 = load %struct.Health_Table*, %struct.Health_Table** @health_table, align 4
  %2 = bitcast %struct.Health_Table* %1 to i8*
  %3 = load i32, i32* %max_count.addr, align 4
  call void @alloc_table(i8* %2, i32 %3)
  %4 = load i32, i32* %max_count.addr, align 4
  %mul = mul i32 %4, 4
  %call1 = call i8* @malloc(i32 %mul)
  %5 = bitcast i8* %call1 to float*
  %6 = load %struct.Health_Table*, %struct.Health_Table** @health_table, align 4
  %health_points = getelementptr inbounds %struct.Health_Table, %struct.Health_Table* %6, i32 0, i32 4
  store float* %5, float** %health_points, align 4
  %7 = load i32, i32* %max_count.addr, align 4
  %mul2 = mul i32 %7, 8
  %call3 = call i8* @malloc(i32 %mul2)
  %8 = bitcast i8* %call3 to %struct.timespec*
  %9 = load %struct.Health_Table*, %struct.Health_Table** @health_table, align 4
  %last_hit_at = getelementptr inbounds %struct.Health_Table, %struct.Health_Table* %9, i32 0, i32 5
  store %struct.timespec* %8, %struct.timespec** %last_hit_at, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @add_health_item(i32 %entity_id, float %health_points, %struct.timespec* byval(%struct.timespec) align 4 %last_hit_at) #0 {
entry:
  %entity_id.addr = alloca i32, align 4
  %health_points.addr = alloca float, align 4
  %index = alloca i32, align 4
  store i32 %entity_id, i32* %entity_id.addr, align 4
  store float %health_points, float* %health_points.addr, align 4
  %0 = load %struct.Health_Table*, %struct.Health_Table** @health_table, align 4
  %1 = bitcast %struct.Health_Table* %0 to i8*
  %2 = load i32, i32* %entity_id.addr, align 4
  %call = call i32 @add_table_item(i8* %1, i32 %2)
  store i32 %call, i32* %index, align 4
  %3 = load i32, i32* %index, align 4
  %4 = load %struct.Health_Table*, %struct.Health_Table** @health_table, align 4
  %max_count = getelementptr inbounds %struct.Health_Table, %struct.Health_Table* %4, i32 0, i32 0
  %5 = load i32, i32* %max_count, align 4
  %cmp = icmp ult i32 %3, %5
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %6 = load float, float* %health_points.addr, align 4
  %7 = load %struct.Health_Table*, %struct.Health_Table** @health_table, align 4
  %health_points1 = getelementptr inbounds %struct.Health_Table, %struct.Health_Table* %7, i32 0, i32 4
  %8 = load float*, float** %health_points1, align 4
  %9 = load i32, i32* %index, align 4
  %arrayidx = getelementptr inbounds float, float* %8, i32 %9
  store float %6, float* %arrayidx, align 4
  %10 = load %struct.Health_Table*, %struct.Health_Table** @health_table, align 4
  %last_hit_at2 = getelementptr inbounds %struct.Health_Table, %struct.Health_Table* %10, i32 0, i32 5
  %11 = load %struct.timespec*, %struct.timespec** %last_hit_at2, align 4
  %12 = load i32, i32* %index, align 4
  %arrayidx3 = getelementptr inbounds %struct.timespec, %struct.timespec* %11, i32 %12
  %13 = bitcast %struct.timespec* %arrayidx3 to i8*
  %14 = bitcast %struct.timespec* %last_hit_at to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %13, i8* align 4 %14, i32 8, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %15 = load i32, i32* %index, align 4
  ret i32 %15
}

; Function Attrs: noinline nounwind optnone
define hidden void @remove_health_item(i32 %entity_id) #0 {
entry:
  %entity_id.addr = alloca i32, align 4
  store i32 %entity_id, i32* %entity_id.addr, align 4
  %0 = load %struct.Health_Table*, %struct.Health_Table** @health_table, align 4
  %1 = bitcast %struct.Health_Table* %0 to i8*
  %2 = load i32, i32* %entity_id.addr, align 4
  call void @remove_table_item(i8* %1, i32 %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @get_distance_to_point(float %x, float %y, float %x2, float %y2, float* %dx, float* %dy, float* %distance) #0 {
entry:
  %x.addr = alloca float, align 4
  %y.addr = alloca float, align 4
  %x2.addr = alloca float, align 4
  %y2.addr = alloca float, align 4
  %dx.addr = alloca float*, align 4
  %dy.addr = alloca float*, align 4
  %distance.addr = alloca float*, align 4
  store float %x, float* %x.addr, align 4
  store float %y, float* %y.addr, align 4
  store float %x2, float* %x2.addr, align 4
  store float %y2, float* %y2.addr, align 4
  store float* %dx, float** %dx.addr, align 4
  store float* %dy, float** %dy.addr, align 4
  store float* %distance, float** %distance.addr, align 4
  %0 = load float, float* %x2.addr, align 4
  %1 = load float, float* %x.addr, align 4
  %sub = fsub float %0, %1
  %2 = load float*, float** %dx.addr, align 4
  store float %sub, float* %2, align 4
  %3 = load float, float* %y2.addr, align 4
  %4 = load float, float* %y.addr, align 4
  %sub1 = fsub float %3, %4
  %5 = load float*, float** %dy.addr, align 4
  store float %sub1, float* %5, align 4
  %6 = load float*, float** %dx.addr, align 4
  %7 = load float, float* %6, align 4
  %8 = load float*, float** %dx.addr, align 4
  %9 = load float, float* %8, align 4
  %mul = fmul float %7, %9
  %10 = load float*, float** %dy.addr, align 4
  %11 = load float, float* %10, align 4
  %12 = load float*, float** %dy.addr, align 4
  %13 = load float, float* %12, align 4
  %mul2 = fmul float %11, %13
  %add = fadd float %mul, %mul2
  %conv = fpext float %add to double
  %14 = call double @llvm.sqrt.f64(double %conv)
  %conv3 = fptrunc double %14 to float
  %15 = load float*, float** %distance.addr, align 4
  store float %conv3, float* %15, align 4
  ret void
}

; Function Attrs: nounwind readnone speculatable willreturn
declare double @llvm.sqrt.f64(double) #3

; Function Attrs: noinline nounwind optnone
define hidden void @get_direction_to_point(float %x, float %y, float %x2, float %y2, float* %dx, float* %dy, float* %distance, float* %dir_x, float* %dir_y) #0 {
entry:
  %x.addr = alloca float, align 4
  %y.addr = alloca float, align 4
  %x2.addr = alloca float, align 4
  %y2.addr = alloca float, align 4
  %dx.addr = alloca float*, align 4
  %dy.addr = alloca float*, align 4
  %distance.addr = alloca float*, align 4
  %dir_x.addr = alloca float*, align 4
  %dir_y.addr = alloca float*, align 4
  store float %x, float* %x.addr, align 4
  store float %y, float* %y.addr, align 4
  store float %x2, float* %x2.addr, align 4
  store float %y2, float* %y2.addr, align 4
  store float* %dx, float** %dx.addr, align 4
  store float* %dy, float** %dy.addr, align 4
  store float* %distance, float** %distance.addr, align 4
  store float* %dir_x, float** %dir_x.addr, align 4
  store float* %dir_y, float** %dir_y.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %1 = load float, float* %y.addr, align 4
  %2 = load float, float* %x2.addr, align 4
  %3 = load float, float* %y2.addr, align 4
  %4 = load float*, float** %dx.addr, align 4
  %5 = load float*, float** %dy.addr, align 4
  %6 = load float*, float** %distance.addr, align 4
  call void @get_distance_to_point(float %0, float %1, float %2, float %3, float* %4, float* %5, float* %6)
  %7 = load float*, float** %dx.addr, align 4
  %8 = load float, float* %7, align 4
  %9 = load float*, float** %distance.addr, align 4
  %10 = load float, float* %9, align 4
  %div = fdiv float %8, %10
  %11 = load float*, float** %dir_x.addr, align 4
  store float %div, float* %11, align 4
  %12 = load float*, float** %dy.addr, align 4
  %13 = load float, float* %12, align 4
  %14 = load float*, float** %distance.addr, align 4
  %15 = load float, float* %14, align 4
  %div1 = fdiv float %13, %15
  %16 = load float*, float** %dir_y.addr, align 4
  store float %div1, float* %16, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @get_angle_to_point(float %x, float %y, float %x2, float %y2, float* %dx, float* %dy, float* %distance, float* %dir_x, float* %dir_y, float* %angle) #0 {
entry:
  %x.addr = alloca float, align 4
  %y.addr = alloca float, align 4
  %x2.addr = alloca float, align 4
  %y2.addr = alloca float, align 4
  %dx.addr = alloca float*, align 4
  %dy.addr = alloca float*, align 4
  %distance.addr = alloca float*, align 4
  %dir_x.addr = alloca float*, align 4
  %dir_y.addr = alloca float*, align 4
  %angle.addr = alloca float*, align 4
  store float %x, float* %x.addr, align 4
  store float %y, float* %y.addr, align 4
  store float %x2, float* %x2.addr, align 4
  store float %y2, float* %y2.addr, align 4
  store float* %dx, float** %dx.addr, align 4
  store float* %dy, float** %dy.addr, align 4
  store float* %distance, float** %distance.addr, align 4
  store float* %dir_x, float** %dir_x.addr, align 4
  store float* %dir_y, float** %dir_y.addr, align 4
  store float* %angle, float** %angle.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %1 = load float, float* %y.addr, align 4
  %2 = load float, float* %x2.addr, align 4
  %3 = load float, float* %y2.addr, align 4
  %4 = load float*, float** %dx.addr, align 4
  %5 = load float*, float** %dy.addr, align 4
  %6 = load float*, float** %distance.addr, align 4
  %7 = load float*, float** %dir_x.addr, align 4
  %8 = load float*, float** %dir_y.addr, align 4
  call void @get_direction_to_point(float %0, float %1, float %2, float %3, float* %4, float* %5, float* %6, float* %7, float* %8)
  %9 = load float*, float** %dir_y.addr, align 4
  %10 = load float, float* %9, align 4
  %conv = fpext float %10 to double
  %11 = load float*, float** %dir_x.addr, align 4
  %12 = load float, float* %11, align 4
  %conv1 = fpext float %12 to double
  %call = call double @atan2(double %conv, double %conv1) #5
  %conv2 = fptrunc double %call to float
  %13 = load float*, float** %angle.addr, align 4
  store float %conv2, float* %13, align 4
  ret void
}

; Function Attrs: nounwind readnone
declare double @atan2(double, double) #4

; Function Attrs: noinline nounwind optnone
define hidden i32 @create_zombie(float %x, float %y) #0 {
entry:
  %x.addr = alloca float, align 4
  %y.addr = alloca float, align 4
  %entity_id = alloca i32, align 4
  store float %x, float* %x.addr, align 4
  store float %y, float* %y.addr, align 4
  %call = call i32 @create_entity()
  store i32 %call, i32* %entity_id, align 4
  %0 = load i32, i32* %entity_id, align 4
  %1 = load float, float* %x.addr, align 4
  %2 = load float, float* %y.addr, align 4
  %call1 = call i32 @add_physics_state(i32 %0, float %1, float %2, float 0.000000e+00, float 0.000000e+00)
  %3 = load i32, i32* %entity_id, align 4
  %call2 = call i32 @add_physics_ball(i32 %3, float 1.500000e+01, float 2.000000e+00)
  %4 = load i32, i32* %entity_id, align 4
  %call3 = call i32 @add_sprite_map(i32 %4, i32 2, float -2.000000e+01, float -2.000000e+01, float 4.000000e+01, i8 zeroext 0)
  %5 = load i32, i32* %entity_id, align 4
  %call4 = call i32 @add_ai_enemy(i32 %5, i8 zeroext 0)
  %6 = load i32, i32* %entity_id, align 4
  %call5 = call i32 @add_health_item(i32 %6, float 2.000000e+00, %struct.timespec* byval(%struct.timespec) align 4 @curr_time)
  %7 = load i32, i32* %entity_id, align 4
  %call6 = call i32 @add_proximity_attack(i32 %7, float 1.000000e+02, float 1.000000e+01)
  %8 = load i32, i32* %entity_id, align 4
  ret i32 %8
}

; Function Attrs: noinline nounwind optnone
define hidden void @destroy_zombie(i32 %entity_id) #0 {
entry:
  %entity_id.addr = alloca i32, align 4
  store i32 %entity_id, i32* %entity_id.addr, align 4
  %0 = load i32, i32* %entity_id.addr, align 4
  call void @remove_entity(i32 %0)
  %1 = load i32, i32* %entity_id.addr, align 4
  call void @remove_physics_state(i32 %1)
  %2 = load i32, i32* %entity_id.addr, align 4
  call void @remove_physics_ball(i32 %2)
  %3 = load i32, i32* %entity_id.addr, align 4
  call void @remove_sprite_map(i32 %3)
  %4 = load i32, i32* %entity_id.addr, align 4
  call void @remove_ai_enemy(i32 %4)
  %5 = load i32, i32* %entity_id.addr, align 4
  call void @remove_health_item(i32 %5)
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @create_player(float %x, float %y) #0 {
entry:
  %x.addr = alloca float, align 4
  %y.addr = alloca float, align 4
  %entity_id = alloca i32, align 4
  store float %x, float* %x.addr, align 4
  store float %y, float* %y.addr, align 4
  %call = call i32 @create_entity()
  store i32 %call, i32* %entity_id, align 4
  %0 = load i32, i32* %entity_id, align 4
  %1 = load float, float* %x.addr, align 4
  %2 = load float, float* %y.addr, align 4
  %call1 = call i32 @add_physics_state(i32 %0, float %1, float %2, float 0.000000e+00, float 0.000000e+00)
  %3 = load i32, i32* %entity_id, align 4
  %call2 = call i32 @add_physics_ball(i32 %3, float 1.500000e+01, float 2.000000e+00)
  %4 = load i32, i32* %entity_id, align 4
  %call3 = call i32 @add_sprite_map(i32 %4, i32 1, float -2.000000e+01, float -2.000000e+01, float 4.000000e+01, i8 zeroext 0)
  %5 = load i32, i32* %entity_id, align 4
  %call4 = call i32 @add_health_item(i32 %5, float 4.000000e+01, %struct.timespec* byval(%struct.timespec) align 4 @curr_time)
  %6 = load i32, i32* %entity_id, align 4
  ret i32 %6
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @create_bullet(float %x, float %y, float %x_speed, float %y_speed) #0 {
entry:
  %x.addr = alloca float, align 4
  %y.addr = alloca float, align 4
  %x_speed.addr = alloca float, align 4
  %y_speed.addr = alloca float, align 4
  %entity_id = alloca i32, align 4
  store float %x, float* %x.addr, align 4
  store float %y, float* %y.addr, align 4
  store float %x_speed, float* %x_speed.addr, align 4
  store float %y_speed, float* %y_speed.addr, align 4
  %call = call i32 @create_entity()
  store i32 %call, i32* %entity_id, align 4
  %0 = load i32, i32* %entity_id, align 4
  %1 = load float, float* %x.addr, align 4
  %2 = load float, float* %y.addr, align 4
  %3 = load float, float* %x_speed.addr, align 4
  %4 = load float, float* %y_speed.addr, align 4
  %call1 = call i32 @add_physics_state(i32 %0, float %1, float %2, float %3, float %4)
  %5 = load i32, i32* %entity_id, align 4
  %call2 = call i32 @add_physics_ball(i32 %5, float 4.000000e+00, float 1.000000e+00)
  %6 = load i32, i32* %entity_id, align 4
  %call3 = call i32 @add_sprite_map(i32 %6, i32 3, float -8.000000e+00, float -8.000000e+00, float 1.600000e+01, i8 zeroext 0)
  %7 = load i32, i32* %entity_id, align 4
  %call4 = call i32 @add_bullet(i32 %7, float 1.000000e+00, %struct.timespec* byval(%struct.timespec) align 4 @curr_time)
  %8 = load i32, i32* %entity_id, align 4
  ret i32 %8
}

; Function Attrs: noinline nounwind optnone
define hidden void @destroy_bullet(i32 %entity_id) #0 {
entry:
  %entity_id.addr = alloca i32, align 4
  store i32 %entity_id, i32* %entity_id.addr, align 4
  %0 = load i32, i32* %entity_id.addr, align 4
  call void @remove_entity(i32 %0)
  %1 = load i32, i32* %entity_id.addr, align 4
  call void @remove_physics_state(i32 %1)
  %2 = load i32, i32* %entity_id.addr, align 4
  call void @remove_physics_ball(i32 %2)
  %3 = load i32, i32* %entity_id.addr, align 4
  call void @remove_sprite_map(i32 %3)
  %4 = load i32, i32* %entity_id.addr, align 4
  call void @remove_bullet(i32 %4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @alloc_weapon_states(i32 %max_count) #0 {
entry:
  %max_count.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store i32 %max_count, i32* %max_count.addr, align 4
  %call = call i8* @malloc(i32 16)
  %0 = bitcast i8* %call to %struct.Weapon_States*
  store %struct.Weapon_States* %0, %struct.Weapon_States** @weapon_states, align 4
  %1 = load i32, i32* %max_count.addr, align 4
  %mul = mul i32 %1, 4
  %call1 = call i8* @malloc(i32 %mul)
  %2 = bitcast i8* %call1 to float*
  %3 = load %struct.Weapon_States*, %struct.Weapon_States** @weapon_states, align 4
  %firing_state = getelementptr inbounds %struct.Weapon_States, %struct.Weapon_States* %3, i32 0, i32 2
  store float* %2, float** %firing_state, align 4
  %4 = load i32, i32* %max_count.addr, align 4
  %mul2 = mul i32 %4, 4
  %call3 = call i8* @malloc(i32 %mul2)
  %5 = bitcast i8* %call3 to float*
  %6 = load %struct.Weapon_States*, %struct.Weapon_States** @weapon_states, align 4
  %firing_speed = getelementptr inbounds %struct.Weapon_States, %struct.Weapon_States* %6, i32 0, i32 3
  store float* %5, float** %firing_speed, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %7 = load i32, i32* %i, align 4
  %8 = load i32, i32* %max_count.addr, align 4
  %cmp = icmp ult i32 %7, %8
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %9 = load %struct.Weapon_States*, %struct.Weapon_States** @weapon_states, align 4
  %firing_state4 = getelementptr inbounds %struct.Weapon_States, %struct.Weapon_States* %9, i32 0, i32 2
  %10 = load float*, float** %firing_state4, align 4
  %11 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds float, float* %10, i32 %11
  store float 1.000000e+00, float* %arrayidx, align 4
  %12 = load %struct.Weapon_States*, %struct.Weapon_States** @weapon_states, align 4
  %firing_speed5 = getelementptr inbounds %struct.Weapon_States, %struct.Weapon_States* %12, i32 0, i32 3
  %13 = load float*, float** %firing_speed5, align 4
  %14 = load i32, i32* %i, align 4
  %arrayidx6 = getelementptr inbounds float, float* %13, i32 %14
  store float 2.000000e+02, float* %arrayidx6, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %15 = load i32, i32* %i, align 4
  %add = add i32 %15, 1
  store i32 %add, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %16 = load i32, i32* %max_count.addr, align 4
  %17 = load %struct.Weapon_States*, %struct.Weapon_States** @weapon_states, align 4
  %max_count7 = getelementptr inbounds %struct.Weapon_States, %struct.Weapon_States* %17, i32 0, i32 0
  store i32 %16, i32* %max_count7, align 4
  %18 = load %struct.Weapon_States*, %struct.Weapon_States** @weapon_states, align 4
  %curr_max = getelementptr inbounds %struct.Weapon_States, %struct.Weapon_States* %18, i32 0, i32 1
  store i32 1, i32* %curr_max, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @step_weapon_states(float %delta) #0 {
entry:
  %delta.addr = alloca float, align 4
  %i = alloca i32, align 4
  store float %delta, float* %delta.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load %struct.Weapon_States*, %struct.Weapon_States** @weapon_states, align 4
  %max_count = getelementptr inbounds %struct.Weapon_States, %struct.Weapon_States* %1, i32 0, i32 0
  %2 = load i32, i32* %max_count, align 4
  %cmp = icmp ult i32 %0, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %struct.Weapon_States*, %struct.Weapon_States** @weapon_states, align 4
  %firing_state = getelementptr inbounds %struct.Weapon_States, %struct.Weapon_States* %3, i32 0, i32 2
  %4 = load float*, float** %firing_state, align 4
  %5 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds float, float* %4, i32 %5
  %6 = load float, float* %arrayidx, align 4
  %cmp1 = fcmp ogt float %6, 0.000000e+00
  br i1 %cmp1, label %if.then, label %if.else

if.then:                                          ; preds = %for.body
  %7 = load %struct.Weapon_States*, %struct.Weapon_States** @weapon_states, align 4
  %firing_speed = getelementptr inbounds %struct.Weapon_States, %struct.Weapon_States* %7, i32 0, i32 3
  %8 = load float*, float** %firing_speed, align 4
  %9 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds float, float* %8, i32 %9
  %10 = load float, float* %arrayidx2, align 4
  %11 = load float, float* %delta.addr, align 4
  %mul = fmul float %10, %11
  %12 = load %struct.Weapon_States*, %struct.Weapon_States** @weapon_states, align 4
  %firing_state3 = getelementptr inbounds %struct.Weapon_States, %struct.Weapon_States* %12, i32 0, i32 2
  %13 = load float*, float** %firing_state3, align 4
  %14 = load i32, i32* %i, align 4
  %arrayidx4 = getelementptr inbounds float, float* %13, i32 %14
  %15 = load float, float* %arrayidx4, align 4
  %sub = fsub float %15, %mul
  store float %sub, float* %arrayidx4, align 4
  br label %if.end

if.else:                                          ; preds = %for.body
  %16 = load %struct.Weapon_States*, %struct.Weapon_States** @weapon_states, align 4
  %firing_state5 = getelementptr inbounds %struct.Weapon_States, %struct.Weapon_States* %16, i32 0, i32 2
  %17 = load float*, float** %firing_state5, align 4
  %18 = load i32, i32* %i, align 4
  %arrayidx6 = getelementptr inbounds float, float* %17, i32 %18
  store float 0.000000e+00, float* %arrayidx6, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %19 = load i32, i32* %i, align 4
  %add = add i32 %19, 1
  store i32 %add, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden %struct.Weapon_States* @get_weapon_states() #0 {
entry:
  %0 = load %struct.Weapon_States*, %struct.Weapon_States** @weapon_states, align 4
  ret %struct.Weapon_States* %0
}

; Function Attrs: noinline nounwind optnone
define hidden void @alloc_overlay_data() #0 {
entry:
  %call = call i8* @malloc(i32 16)
  %0 = bitcast i8* %call to %struct.Overlay_Data*
  store %struct.Overlay_Data* %0, %struct.Overlay_Data** @overlay_data, align 4
  %1 = load %struct.Overlay_Data*, %struct.Overlay_Data** @overlay_data, align 4
  %player_dead = getelementptr inbounds %struct.Overlay_Data, %struct.Overlay_Data* %1, i32 0, i32 0
  store i32 0, i32* %player_dead, align 4
  %2 = load %struct.Overlay_Data*, %struct.Overlay_Data** @overlay_data, align 4
  %wave_start = getelementptr inbounds %struct.Overlay_Data, %struct.Overlay_Data* %2, i32 0, i32 1
  store i32 0, i32* %wave_start, align 4
  %3 = load %struct.Overlay_Data*, %struct.Overlay_Data** @overlay_data, align 4
  %wave_end = getelementptr inbounds %struct.Overlay_Data, %struct.Overlay_Data* %3, i32 0, i32 2
  store i32 0, i32* %wave_end, align 4
  %4 = load %struct.Overlay_Data*, %struct.Overlay_Data** @overlay_data, align 4
  %wave_state = getelementptr inbounds %struct.Overlay_Data, %struct.Overlay_Data* %4, i32 0, i32 3
  store float 0.000000e+00, float* %wave_state, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden %struct.Overlay_Data* @get_overlay_data() #0 {
entry:
  %0 = load %struct.Overlay_Data*, %struct.Overlay_Data** @overlay_data, align 4
  ret %struct.Overlay_Data* %0
}

; Function Attrs: noinline nounwind optnone
define hidden void @alloc_campaign(i32 %max_count) #0 {
entry:
  %max_count.addr = alloca i32, align 4
  store i32 %max_count, i32* %max_count.addr, align 4
  %call = call i8* @malloc(i32 12)
  %0 = bitcast i8* %call to %struct.Campaign*
  store %struct.Campaign* %0, %struct.Campaign** @campaign, align 4
  %1 = load i32, i32* %max_count.addr, align 4
  %mul = mul i32 %1, 4
  %mul1 = mul i32 %mul, 1
  %call2 = call i8* @malloc(i32 %mul1)
  %2 = load %struct.Campaign*, %struct.Campaign** @campaign, align 4
  %remaining = getelementptr inbounds %struct.Campaign, %struct.Campaign* %2, i32 0, i32 2
  store i8* %call2, i8** %remaining, align 4
  %3 = load %struct.Campaign*, %struct.Campaign** @campaign, align 4
  %curr_max = getelementptr inbounds %struct.Campaign, %struct.Campaign* %3, i32 0, i32 1
  store i32 0, i32* %curr_max, align 4
  %4 = load i32, i32* %max_count.addr, align 4
  %5 = load %struct.Campaign*, %struct.Campaign** @campaign, align 4
  %max_count3 = getelementptr inbounds %struct.Campaign, %struct.Campaign* %5, i32 0, i32 0
  store i32 %4, i32* %max_count3, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @add_campaign_wave(i8* %remaining) #0 {
entry:
  %remaining.addr = alloca i8*, align 4
  %i = alloca i32, align 4
  store i8* %remaining, i8** %remaining.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp ult i32 %0, 4
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %1 = load i8*, i8** %remaining.addr, align 4
  %2 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i8, i8* %1, i32 %2
  %3 = load i8, i8* %arrayidx, align 1
  %4 = load %struct.Campaign*, %struct.Campaign** @campaign, align 4
  %remaining1 = getelementptr inbounds %struct.Campaign, %struct.Campaign* %4, i32 0, i32 2
  %5 = load i8*, i8** %remaining1, align 4
  %6 = load %struct.Campaign*, %struct.Campaign** @campaign, align 4
  %curr_max = getelementptr inbounds %struct.Campaign, %struct.Campaign* %6, i32 0, i32 1
  %7 = load i32, i32* %curr_max, align 4
  %mul = mul i32 %7, 4
  %8 = load i32, i32* %i, align 4
  %add = add i32 %mul, %8
  %arrayidx2 = getelementptr inbounds i8, i8* %5, i32 %add
  store i8 %3, i8* %arrayidx2, align 1
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %9 = load i32, i32* %i, align 4
  %add3 = add i32 %9, 1
  store i32 %add3, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %10 = load %struct.Campaign*, %struct.Campaign** @campaign, align 4
  %curr_max4 = getelementptr inbounds %struct.Campaign, %struct.Campaign* %10, i32 0, i32 1
  %11 = load i32, i32* %curr_max4, align 4
  %12 = load %struct.Campaign*, %struct.Campaign** @campaign, align 4
  %max_count = getelementptr inbounds %struct.Campaign, %struct.Campaign* %12, i32 0, i32 0
  %13 = load i32, i32* %max_count, align 4
  %cmp5 = icmp ult i32 %11, %13
  br i1 %cmp5, label %if.then, label %if.end

if.then:                                          ; preds = %for.end
  %14 = load %struct.Campaign*, %struct.Campaign** @campaign, align 4
  %curr_max6 = getelementptr inbounds %struct.Campaign, %struct.Campaign* %14, i32 0, i32 1
  %15 = load i32, i32* %curr_max6, align 4
  %add7 = add i32 %15, 1
  store i32 %add7, i32* %curr_max6, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %for.end
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @generate_campaign_waves() #0 {
entry:
  %remaining = alloca [4 x i8], align 1
  %i = alloca i32, align 4
  %i1 = alloca i32, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp ult i32 %0, 4
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %1 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [4 x i8], [4 x i8]* %remaining, i32 0, i32 %1
  store i8 0, i8* %arrayidx, align 1
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %2 = load i32, i32* %i, align 4
  %add = add i32 %2, 1
  store i32 %add, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %i1, align 4
  br label %for.cond2

for.cond2:                                        ; preds = %for.inc7, %for.end
  %3 = load i32, i32* %i1, align 4
  %cmp3 = icmp ult i32 %3, 5
  br i1 %cmp3, label %for.body4, label %for.end9

for.body4:                                        ; preds = %for.cond2
  %4 = load i32, i32* %i1, align 4
  %mul = mul i32 %4, 3
  %add5 = add i32 1, %mul
  %conv = trunc i32 %add5 to i8
  %arrayidx6 = getelementptr inbounds [4 x i8], [4 x i8]* %remaining, i32 0, i32 0
  store i8 %conv, i8* %arrayidx6, align 1
  %arraydecay = getelementptr inbounds [4 x i8], [4 x i8]* %remaining, i32 0, i32 0
  call void @add_campaign_wave(i8* %arraydecay)
  br label %for.inc7

for.inc7:                                         ; preds = %for.body4
  %5 = load i32, i32* %i1, align 4
  %add8 = add i32 %5, 1
  store i32 %add8, i32* %i1, align 4
  br label %for.cond2

for.end9:                                         ; preds = %for.cond2
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @step_wave_completion() #0 {
entry:
  %i = alloca i32, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp ult i32 %0, 4
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %1 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [4 x i8], [4 x i8]* getelementptr inbounds (%struct.Wave_Completion, %struct.Wave_Completion* @wave_completion, i32 0, i32 0), i32 0, i32 %1
  %2 = load i8, i8* %arrayidx, align 1
  %conv = zext i8 %2 to i32
  %cmp1 = icmp sgt i32 %conv, 0
  br i1 %cmp1, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  br label %return

if.end:                                           ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %3 = load i32, i32* %i, align 4
  %add = add i32 %3, 1
  store i32 %add, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  call void @end_wave()
  br label %return

return:                                           ; preds = %for.end, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @step_wave_emitter() #0 {
entry:
  %emit_id = alloca i8, align 1
  %emit_x = alloca float, align 4
  %emit_y = alloca float, align 4
  %0 = load i8, i8* getelementptr inbounds (%struct.Wave_Emitter, %struct.Wave_Emitter* @wave_emitter, i32 0, i32 2), align 4
  %conv = zext i8 %0 to i32
  %add = add nsw i32 %conv, 1
  %conv1 = trunc i32 %add to i8
  store i8 %conv1, i8* %emit_id, align 1
  br label %while.cond

while.cond:                                       ; preds = %if.end27, %entry
  %1 = load i8, i8* %emit_id, align 1
  %conv2 = zext i8 %1 to i32
  %2 = load i8, i8* getelementptr inbounds (%struct.Wave_Emitter, %struct.Wave_Emitter* @wave_emitter, i32 0, i32 2), align 4
  %conv3 = zext i8 %2 to i32
  %cmp = icmp sgt i32 %conv2, %conv3
  br i1 %cmp, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %while.cond
  %3 = load i8, i8* %emit_id, align 1
  %conv5 = zext i8 %3 to i32
  %4 = load i8, i8* getelementptr inbounds (%struct.Wave_Emitter, %struct.Wave_Emitter* @wave_emitter, i32 0, i32 2), align 4
  %conv6 = zext i8 %4 to i32
  %cmp7 = icmp slt i32 %conv5, %conv6
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %while.cond
  %5 = phi i1 [ true, %while.cond ], [ %cmp7, %lor.rhs ]
  br i1 %5, label %while.body, label %while.end

while.body:                                       ; preds = %lor.end
  %6 = load i8, i8* %emit_id, align 1
  %conv9 = zext i8 %6 to i32
  %cmp10 = icmp sge i32 %conv9, 4
  br i1 %cmp10, label %if.then, label %if.end

if.then:                                          ; preds = %while.body
  store i8 0, i8* %emit_id, align 1
  br label %if.end

if.end:                                           ; preds = %if.then, %while.body
  %7 = load i8, i8* %emit_id, align 1
  %idxprom = zext i8 %7 to i32
  %arrayidx = getelementptr inbounds [4 x i8], [4 x i8]* getelementptr inbounds (%struct.Wave_Emitter, %struct.Wave_Emitter* @wave_emitter, i32 0, i32 3), i32 0, i32 %idxprom
  %8 = load i8, i8* %arrayidx, align 1
  %conv12 = zext i8 %8 to i32
  %cmp13 = icmp sgt i32 %conv12, 0
  br i1 %cmp13, label %if.then15, label %if.end16

if.then15:                                        ; preds = %if.end
  br label %while.end

if.end16:                                         ; preds = %if.end
  %9 = load i8, i8* %emit_id, align 1
  %conv17 = zext i8 %9 to i32
  %cmp18 = icmp eq i32 %conv17, 0
  br i1 %cmp18, label %land.lhs.true, label %if.then23

land.lhs.true:                                    ; preds = %if.end16
  %10 = load i8, i8* getelementptr inbounds (%struct.Wave_Emitter, %struct.Wave_Emitter* @wave_emitter, i32 0, i32 2), align 4
  %conv20 = zext i8 %10 to i32
  %cmp21 = icmp eq i32 %conv20, 0
  br i1 %cmp21, label %if.end27, label %if.then23

if.then23:                                        ; preds = %land.lhs.true, %if.end16
  %11 = load i8, i8* %emit_id, align 1
  %conv24 = zext i8 %11 to i32
  %add25 = add nsw i32 %conv24, 1
  %conv26 = trunc i32 %add25 to i8
  store i8 %conv26, i8* %emit_id, align 1
  br label %if.end27

if.end27:                                         ; preds = %if.then23, %land.lhs.true
  br label %while.cond

while.end:                                        ; preds = %if.then15, %lor.end
  %12 = load i8, i8* %emit_id, align 1
  %idxprom28 = zext i8 %12 to i32
  %arrayidx29 = getelementptr inbounds [4 x i8], [4 x i8]* getelementptr inbounds (%struct.Wave_Emitter, %struct.Wave_Emitter* @wave_emitter, i32 0, i32 3), i32 0, i32 %idxprom28
  %13 = load i8, i8* %arrayidx29, align 1
  %conv30 = zext i8 %13 to i32
  %cmp31 = icmp sgt i32 %conv30, 0
  br i1 %cmp31, label %land.lhs.true33, label %if.end51

land.lhs.true33:                                  ; preds = %while.end
  %call = call float @timespec_diff_float(%struct.timespec* @curr_time, %struct.timespec* getelementptr inbounds (%struct.Wave_Emitter, %struct.Wave_Emitter* @wave_emitter, i32 0, i32 1))
  %14 = load float, float* getelementptr inbounds (%struct.Wave_Emitter, %struct.Wave_Emitter* @wave_emitter, i32 0, i32 0), align 4
  %cmp34 = fcmp ogt float %call, %14
  br i1 %cmp34, label %if.then36, label %if.end51

if.then36:                                        ; preds = %land.lhs.true33
  %call37 = call float @randf()
  %conv38 = fpext float %call37 to double
  %cmp39 = fcmp olt double %conv38, 5.000000e-01
  br i1 %cmp39, label %if.then41, label %if.else

if.then41:                                        ; preds = %if.then36
  store float 0.000000e+00, float* %emit_x, align 4
  br label %if.end43

if.else:                                          ; preds = %if.then36
  %15 = load i32, i32* @screen_width, align 4
  %conv42 = sitofp i32 %15 to float
  store float %conv42, float* %emit_x, align 4
  br label %if.end43

if.end43:                                         ; preds = %if.else, %if.then41
  %16 = load i32, i32* @screen_height, align 4
  %conv44 = sitofp i32 %16 to float
  %call45 = call float @randf()
  %mul = fmul float %conv44, %call45
  store float %mul, float* %emit_y, align 4
  %17 = load float, float* %emit_x, align 4
  %18 = load float, float* %emit_y, align 4
  %call46 = call i32 @create_zombie(float %17, float %18)
  %19 = load i8, i8* %emit_id, align 1
  %idxprom47 = zext i8 %19 to i32
  %arrayidx48 = getelementptr inbounds [4 x i8], [4 x i8]* getelementptr inbounds (%struct.Wave_Emitter, %struct.Wave_Emitter* @wave_emitter, i32 0, i32 3), i32 0, i32 %idxprom47
  %20 = load i8, i8* %arrayidx48, align 1
  %conv49 = zext i8 %20 to i32
  %sub = sub nsw i32 %conv49, 1
  %conv50 = trunc i32 %sub to i8
  store i8 %conv50, i8* %arrayidx48, align 1
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 bitcast (%struct.timespec* getelementptr inbounds (%struct.Wave_Emitter, %struct.Wave_Emitter* @wave_emitter, i32 0, i32 1) to i8*), i8* align 4 bitcast (%struct.timespec* @curr_time to i8*), i32 8, i1 false)
  %21 = load i8, i8* %emit_id, align 1
  store i8 %21, i8* getelementptr inbounds (%struct.Wave_Emitter, %struct.Wave_Emitter* @wave_emitter, i32 0, i32 2), align 4
  br label %if.end51

if.end51:                                         ; preds = %if.end43, %land.lhs.true33, %while.end
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @start_wave() #0 {
entry:
  %0 = load %struct.Campaign*, %struct.Campaign** @campaign, align 4
  %remaining = getelementptr inbounds %struct.Campaign, %struct.Campaign* %0, i32 0, i32 2
  %1 = load i8*, i8** %remaining, align 4
  %2 = load i32, i32* @curr_wave, align 4
  %mul = mul i32 %2, 4
  %arrayidx = getelementptr inbounds i8, i8* %1, i32 %mul
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 getelementptr inbounds (%struct.Wave_Completion, %struct.Wave_Completion* @wave_completion, i32 0, i32 0, i32 0), i8* align 1 %arrayidx, i32 4, i1 false)
  %3 = load %struct.Campaign*, %struct.Campaign** @campaign, align 4
  %remaining1 = getelementptr inbounds %struct.Campaign, %struct.Campaign* %3, i32 0, i32 2
  %4 = load i8*, i8** %remaining1, align 4
  %5 = load i32, i32* @curr_wave, align 4
  %mul2 = mul i32 %5, 4
  %arrayidx3 = getelementptr inbounds i8, i8* %4, i32 %mul2
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 getelementptr inbounds (%struct.Wave_Emitter, %struct.Wave_Emitter* @wave_emitter, i32 0, i32 3, i32 0), i8* align 1 %arrayidx3, i32 4, i1 false)
  store float 1.000000e+00, float* getelementptr inbounds (%struct.Wave_Emitter, %struct.Wave_Emitter* @wave_emitter, i32 0, i32 0), align 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 bitcast (%struct.timespec* getelementptr inbounds (%struct.Wave_Emitter, %struct.Wave_Emitter* @wave_emitter, i32 0, i32 1) to i8*), i8* align 4 bitcast (%struct.timespec* @curr_time to i8*), i32 8, i1 false)
  store i8 0, i8* getelementptr inbounds (%struct.Wave_Emitter, %struct.Wave_Emitter* @wave_emitter, i32 0, i32 2), align 4
  %6 = load i32, i32* @curr_wave, align 4
  %7 = load %struct.Overlay_Data*, %struct.Overlay_Data** @overlay_data, align 4
  %wave_start = getelementptr inbounds %struct.Overlay_Data, %struct.Overlay_Data* %7, i32 0, i32 1
  store i32 %6, i32* %wave_start, align 4
  %8 = load %struct.Overlay_Data*, %struct.Overlay_Data** @overlay_data, align 4
  %wave_state = getelementptr inbounds %struct.Overlay_Data, %struct.Overlay_Data* %8, i32 0, i32 3
  store float 2.000000e+00, float* %wave_state, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @end_wave() #0 {
entry:
  store float 3.000000e+00, float* getelementptr inbounds (%struct.Wave_Rest, %struct.Wave_Rest* @wave_rest, i32 0, i32 0), align 4
  %0 = load i32, i32* @curr_wave, align 4
  %1 = load %struct.Overlay_Data*, %struct.Overlay_Data** @overlay_data, align 4
  %wave_end = getelementptr inbounds %struct.Overlay_Data, %struct.Overlay_Data* %1, i32 0, i32 2
  store i32 %0, i32* %wave_end, align 4
  %2 = load %struct.Overlay_Data*, %struct.Overlay_Data** @overlay_data, align 4
  %wave_state = getelementptr inbounds %struct.Overlay_Data, %struct.Overlay_Data* %2, i32 0, i32 3
  store float 2.000000e+00, float* %wave_state, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @keydown(i32 %event_type, %struct.EmscriptenKeyboardEvent* %event, i8* %user_data) #0 {
entry:
  %event_type.addr = alloca i32, align 4
  %event.addr = alloca %struct.EmscriptenKeyboardEvent*, align 4
  %user_data.addr = alloca i8*, align 4
  %consumed = alloca i32, align 4
  store i32 %event_type, i32* %event_type.addr, align 4
  store %struct.EmscriptenKeyboardEvent* %event, %struct.EmscriptenKeyboardEvent** %event.addr, align 4
  store i8* %user_data, i8** %user_data.addr, align 4
  store i32 0, i32* %consumed, align 4
  %0 = load %struct.EmscriptenKeyboardEvent*, %struct.EmscriptenKeyboardEvent** %event.addr, align 4
  %key = getelementptr inbounds %struct.EmscriptenKeyboardEvent, %struct.EmscriptenKeyboardEvent* %0, i32 0, i32 9
  %arraydecay = getelementptr inbounds [32 x i8], [32 x i8]* %key, i32 0, i32 0
  %call = call i32 @strcmp(i8* %arraydecay, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.1, i32 0, i32 0))
  %cmp = icmp eq i32 %call, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.Input_State*, %struct.Input_State** @input_state, align 4
  %move_up = getelementptr inbounds %struct.Input_State, %struct.Input_State* %1, i32 0, i32 0
  store i8 1, i8* %move_up, align 4
  store i32 1, i32* %consumed, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %struct.EmscriptenKeyboardEvent*, %struct.EmscriptenKeyboardEvent** %event.addr, align 4
  %key1 = getelementptr inbounds %struct.EmscriptenKeyboardEvent, %struct.EmscriptenKeyboardEvent* %2, i32 0, i32 9
  %arraydecay2 = getelementptr inbounds [32 x i8], [32 x i8]* %key1, i32 0, i32 0
  %call3 = call i32 @strcmp(i8* %arraydecay2, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.2, i32 0, i32 0))
  %cmp4 = icmp eq i32 %call3, 0
  br i1 %cmp4, label %if.then5, label %if.end6

if.then5:                                         ; preds = %if.end
  %3 = load %struct.Input_State*, %struct.Input_State** @input_state, align 4
  %move_left = getelementptr inbounds %struct.Input_State, %struct.Input_State* %3, i32 0, i32 1
  store i8 1, i8* %move_left, align 1
  store i32 1, i32* %consumed, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.then5, %if.end
  %4 = load %struct.EmscriptenKeyboardEvent*, %struct.EmscriptenKeyboardEvent** %event.addr, align 4
  %key7 = getelementptr inbounds %struct.EmscriptenKeyboardEvent, %struct.EmscriptenKeyboardEvent* %4, i32 0, i32 9
  %arraydecay8 = getelementptr inbounds [32 x i8], [32 x i8]* %key7, i32 0, i32 0
  %call9 = call i32 @strcmp(i8* %arraydecay8, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.3, i32 0, i32 0))
  %cmp10 = icmp eq i32 %call9, 0
  br i1 %cmp10, label %if.then11, label %if.end12

if.then11:                                        ; preds = %if.end6
  %5 = load %struct.Input_State*, %struct.Input_State** @input_state, align 4
  %move_down = getelementptr inbounds %struct.Input_State, %struct.Input_State* %5, i32 0, i32 2
  store i8 1, i8* %move_down, align 2
  store i32 1, i32* %consumed, align 4
  br label %if.end12

if.end12:                                         ; preds = %if.then11, %if.end6
  %6 = load %struct.EmscriptenKeyboardEvent*, %struct.EmscriptenKeyboardEvent** %event.addr, align 4
  %key13 = getelementptr inbounds %struct.EmscriptenKeyboardEvent, %struct.EmscriptenKeyboardEvent* %6, i32 0, i32 9
  %arraydecay14 = getelementptr inbounds [32 x i8], [32 x i8]* %key13, i32 0, i32 0
  %call15 = call i32 @strcmp(i8* %arraydecay14, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.4, i32 0, i32 0))
  %cmp16 = icmp eq i32 %call15, 0
  br i1 %cmp16, label %if.then17, label %if.end18

if.then17:                                        ; preds = %if.end12
  %7 = load %struct.Input_State*, %struct.Input_State** @input_state, align 4
  %move_right = getelementptr inbounds %struct.Input_State, %struct.Input_State* %7, i32 0, i32 3
  store i8 1, i8* %move_right, align 1
  store i32 1, i32* %consumed, align 4
  br label %if.end18

if.end18:                                         ; preds = %if.then17, %if.end12
  %8 = load %struct.EmscriptenKeyboardEvent*, %struct.EmscriptenKeyboardEvent** %event.addr, align 4
  %key19 = getelementptr inbounds %struct.EmscriptenKeyboardEvent, %struct.EmscriptenKeyboardEvent* %8, i32 0, i32 9
  %arraydecay20 = getelementptr inbounds [32 x i8], [32 x i8]* %key19, i32 0, i32 0
  %call21 = call i32 @strcmp(i8* %arraydecay20, i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.5, i32 0, i32 0))
  %cmp22 = icmp eq i32 %call21, 0
  br i1 %cmp22, label %if.then23, label %if.end26

if.then23:                                        ; preds = %if.end18
  %9 = load i8, i8* @paused, align 1
  %tobool = icmp ne i8 %9, 0
  br i1 %tobool, label %if.then24, label %if.else

if.then24:                                        ; preds = %if.then23
  store i8 0, i8* @paused, align 1
  call void @start_time()
  br label %if.end25

if.else:                                          ; preds = %if.then23
  store i8 1, i8* @paused, align 1
  call void @stop_time()
  br label %if.end25

if.end25:                                         ; preds = %if.else, %if.then24
  store i32 1, i32* %consumed, align 4
  br label %if.end26

if.end26:                                         ; preds = %if.end25, %if.end18
  %10 = load i32, i32* %consumed, align 4
  ret i32 %10
}

declare i32 @strcmp(i8*, i8*) #1

; Function Attrs: noinline nounwind optnone
define hidden i32 @keyup(i32 %event_type, %struct.EmscriptenKeyboardEvent* %event, i8* %user_data) #0 {
entry:
  %event_type.addr = alloca i32, align 4
  %event.addr = alloca %struct.EmscriptenKeyboardEvent*, align 4
  %user_data.addr = alloca i8*, align 4
  %consumed = alloca i32, align 4
  store i32 %event_type, i32* %event_type.addr, align 4
  store %struct.EmscriptenKeyboardEvent* %event, %struct.EmscriptenKeyboardEvent** %event.addr, align 4
  store i8* %user_data, i8** %user_data.addr, align 4
  store i32 0, i32* %consumed, align 4
  %0 = load %struct.EmscriptenKeyboardEvent*, %struct.EmscriptenKeyboardEvent** %event.addr, align 4
  %key = getelementptr inbounds %struct.EmscriptenKeyboardEvent, %struct.EmscriptenKeyboardEvent* %0, i32 0, i32 9
  %arraydecay = getelementptr inbounds [32 x i8], [32 x i8]* %key, i32 0, i32 0
  %call = call i32 @strcmp(i8* %arraydecay, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.1, i32 0, i32 0))
  %cmp = icmp eq i32 %call, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.Input_State*, %struct.Input_State** @input_state, align 4
  %move_up = getelementptr inbounds %struct.Input_State, %struct.Input_State* %1, i32 0, i32 0
  store i8 0, i8* %move_up, align 4
  store i32 1, i32* %consumed, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %struct.EmscriptenKeyboardEvent*, %struct.EmscriptenKeyboardEvent** %event.addr, align 4
  %key1 = getelementptr inbounds %struct.EmscriptenKeyboardEvent, %struct.EmscriptenKeyboardEvent* %2, i32 0, i32 9
  %arraydecay2 = getelementptr inbounds [32 x i8], [32 x i8]* %key1, i32 0, i32 0
  %call3 = call i32 @strcmp(i8* %arraydecay2, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.2, i32 0, i32 0))
  %cmp4 = icmp eq i32 %call3, 0
  br i1 %cmp4, label %if.then5, label %if.end6

if.then5:                                         ; preds = %if.end
  %3 = load %struct.Input_State*, %struct.Input_State** @input_state, align 4
  %move_left = getelementptr inbounds %struct.Input_State, %struct.Input_State* %3, i32 0, i32 1
  store i8 0, i8* %move_left, align 1
  store i32 1, i32* %consumed, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.then5, %if.end
  %4 = load %struct.EmscriptenKeyboardEvent*, %struct.EmscriptenKeyboardEvent** %event.addr, align 4
  %key7 = getelementptr inbounds %struct.EmscriptenKeyboardEvent, %struct.EmscriptenKeyboardEvent* %4, i32 0, i32 9
  %arraydecay8 = getelementptr inbounds [32 x i8], [32 x i8]* %key7, i32 0, i32 0
  %call9 = call i32 @strcmp(i8* %arraydecay8, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.3, i32 0, i32 0))
  %cmp10 = icmp eq i32 %call9, 0
  br i1 %cmp10, label %if.then11, label %if.end12

if.then11:                                        ; preds = %if.end6
  %5 = load %struct.Input_State*, %struct.Input_State** @input_state, align 4
  %move_down = getelementptr inbounds %struct.Input_State, %struct.Input_State* %5, i32 0, i32 2
  store i8 0, i8* %move_down, align 2
  store i32 1, i32* %consumed, align 4
  br label %if.end12

if.end12:                                         ; preds = %if.then11, %if.end6
  %6 = load %struct.EmscriptenKeyboardEvent*, %struct.EmscriptenKeyboardEvent** %event.addr, align 4
  %key13 = getelementptr inbounds %struct.EmscriptenKeyboardEvent, %struct.EmscriptenKeyboardEvent* %6, i32 0, i32 9
  %arraydecay14 = getelementptr inbounds [32 x i8], [32 x i8]* %key13, i32 0, i32 0
  %call15 = call i32 @strcmp(i8* %arraydecay14, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.4, i32 0, i32 0))
  %cmp16 = icmp eq i32 %call15, 0
  br i1 %cmp16, label %if.then17, label %if.end18

if.then17:                                        ; preds = %if.end12
  %7 = load %struct.Input_State*, %struct.Input_State** @input_state, align 4
  %move_right = getelementptr inbounds %struct.Input_State, %struct.Input_State* %7, i32 0, i32 3
  store i8 0, i8* %move_right, align 1
  store i32 1, i32* %consumed, align 4
  br label %if.end18

if.end18:                                         ; preds = %if.then17, %if.end12
  %8 = load i32, i32* %consumed, align 4
  ret i32 %8
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @mousedown(i32 %event_type, %struct.EmscriptenMouseEvent* %event, i8* %user_data) #0 {
entry:
  %event_type.addr = alloca i32, align 4
  %event.addr = alloca %struct.EmscriptenMouseEvent*, align 4
  %user_data.addr = alloca i8*, align 4
  %consumed = alloca i32, align 4
  store i32 %event_type, i32* %event_type.addr, align 4
  store %struct.EmscriptenMouseEvent* %event, %struct.EmscriptenMouseEvent** %event.addr, align 4
  store i8* %user_data, i8** %user_data.addr, align 4
  store i32 0, i32* %consumed, align 4
  %0 = load %struct.EmscriptenMouseEvent*, %struct.EmscriptenMouseEvent** %event.addr, align 4
  %button = getelementptr inbounds %struct.EmscriptenMouseEvent, %struct.EmscriptenMouseEvent* %0, i32 0, i32 8
  %1 = load i16, i16* %button, align 4
  %conv = zext i16 %1 to i32
  %cmp = icmp eq i32 %conv, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load %struct.Input_State*, %struct.Input_State** @input_state, align 4
  %shoot = getelementptr inbounds %struct.Input_State, %struct.Input_State* %2, i32 0, i32 4
  store i8 1, i8* %shoot, align 4
  store i32 1, i32* %consumed, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %3 = load i32, i32* %consumed, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @mousemove(i32 %event_type, %struct.EmscriptenMouseEvent* %event, i8* %user_data) #0 {
entry:
  %event_type.addr = alloca i32, align 4
  %event.addr = alloca %struct.EmscriptenMouseEvent*, align 4
  %user_data.addr = alloca i8*, align 4
  store i32 %event_type, i32* %event_type.addr, align 4
  store %struct.EmscriptenMouseEvent* %event, %struct.EmscriptenMouseEvent** %event.addr, align 4
  store i8* %user_data, i8** %user_data.addr, align 4
  %0 = load %struct.EmscriptenMouseEvent*, %struct.EmscriptenMouseEvent** %event.addr, align 4
  %clientX = getelementptr inbounds %struct.EmscriptenMouseEvent, %struct.EmscriptenMouseEvent* %0, i32 0, i32 2
  %1 = load i32, i32* %clientX, align 4
  %conv = sitofp i32 %1 to float
  %2 = load %struct.Input_State*, %struct.Input_State** @input_state, align 4
  %mouse_x = getelementptr inbounds %struct.Input_State, %struct.Input_State* %2, i32 0, i32 5
  store float %conv, float* %mouse_x, align 4
  %3 = load %struct.EmscriptenMouseEvent*, %struct.EmscriptenMouseEvent** %event.addr, align 4
  %clientY = getelementptr inbounds %struct.EmscriptenMouseEvent, %struct.EmscriptenMouseEvent* %3, i32 0, i32 3
  %4 = load i32, i32* %clientY, align 4
  %conv1 = sitofp i32 %4 to float
  %5 = load %struct.Input_State*, %struct.Input_State** @input_state, align 4
  %mouse_y = getelementptr inbounds %struct.Input_State, %struct.Input_State* %5, i32 0, i32 6
  store float %conv1, float* %mouse_y, align 4
  ret i32 1
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @mouseup(i32 %event_type, %struct.EmscriptenMouseEvent* %event, i8* %user_data) #0 {
entry:
  %event_type.addr = alloca i32, align 4
  %event.addr = alloca %struct.EmscriptenMouseEvent*, align 4
  %user_data.addr = alloca i8*, align 4
  %consumed = alloca i32, align 4
  store i32 %event_type, i32* %event_type.addr, align 4
  store %struct.EmscriptenMouseEvent* %event, %struct.EmscriptenMouseEvent** %event.addr, align 4
  store i8* %user_data, i8** %user_data.addr, align 4
  store i32 0, i32* %consumed, align 4
  %0 = load %struct.EmscriptenMouseEvent*, %struct.EmscriptenMouseEvent** %event.addr, align 4
  %button = getelementptr inbounds %struct.EmscriptenMouseEvent, %struct.EmscriptenMouseEvent* %0, i32 0, i32 8
  %1 = load i16, i16* %button, align 4
  %conv = zext i16 %1 to i32
  %cmp = icmp eq i32 %conv, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load %struct.Input_State*, %struct.Input_State** @input_state, align 4
  %shoot = getelementptr inbounds %struct.Input_State, %struct.Input_State* %2, i32 0, i32 4
  store i8 0, i8* %shoot, align 4
  store i32 1, i32* %consumed, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %3 = load i32, i32* %consumed, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden void @init(i32 %width, i32 %height) #0 {
entry:
  %width.addr = alloca i32, align 4
  %height.addr = alloca i32, align 4
  store i32 %width, i32* %width.addr, align 4
  store i32 %height, i32* %height.addr, align 4
  %0 = load i32, i32* %width.addr, align 4
  %1 = load i32, i32* %height.addr, align 4
  call void @set_screen_size(i32 %0, i32 %1)
  call void @start_time()
  store float 0xBF847AE140000000, float* getelementptr inbounds (%struct.Wave_Rest, %struct.Wave_Rest* @wave_rest, i32 0, i32 0), align 4
  call void @alloc_proximity_attack(i32 2000)
  call void @alloc_hit_feedback_table(i32 2000)
  call void @alloc_collision_table(i32 2000)
  call void @alloc_entity_table(i32 2000)
  call void @alloc_physics_states(i32 2000)
  call void @alloc_physics_balls(i32 2000)
  call void @alloc_sprite_map(i32 2000)
  call void @alloc_ai_enemy(i32 2000)
  call void @alloc_bullets(i32 2000)
  call void @alloc_health_table(i32 2000)
  call void @alloc_weapon_states(i32 8)
  call void @alloc_campaign(i32 20)
  call void @generate_campaign_waves()
  call void @alloc_overlay_data()
  %call = call i8* @malloc(i32 16)
  %2 = bitcast i8* %call to %struct.Input_State*
  store %struct.Input_State* %2, %struct.Input_State** @input_state, align 4
  %3 = load i8*, i8** @str_window, align 4
  %call1 = call i32 @emscripten_set_keydown_callback_on_thread(i8* %3, i8* null, i32 0, i32 (i32, %struct.EmscriptenKeyboardEvent*, i8*)* @keydown, %struct.__pthread* inttoptr (i32 2 to %struct.__pthread*))
  %4 = load i8*, i8** @str_window, align 4
  %call2 = call i32 @emscripten_set_keyup_callback_on_thread(i8* %4, i8* null, i32 0, i32 (i32, %struct.EmscriptenKeyboardEvent*, i8*)* @keyup, %struct.__pthread* inttoptr (i32 2 to %struct.__pthread*))
  %5 = load i8*, i8** @str_window, align 4
  %call3 = call i32 @emscripten_set_mousemove_callback_on_thread(i8* %5, i8* null, i32 0, i32 (i32, %struct.EmscriptenMouseEvent*, i8*)* @mousemove, %struct.__pthread* inttoptr (i32 2 to %struct.__pthread*))
  %6 = load i8*, i8** @str_window, align 4
  %call4 = call i32 @emscripten_set_mousedown_callback_on_thread(i8* %6, i8* null, i32 0, i32 (i32, %struct.EmscriptenMouseEvent*, i8*)* @mousedown, %struct.__pthread* inttoptr (i32 2 to %struct.__pthread*))
  %7 = load i8*, i8** @str_window, align 4
  %call5 = call i32 @emscripten_set_mouseup_callback_on_thread(i8* %7, i8* null, i32 0, i32 (i32, %struct.EmscriptenMouseEvent*, i8*)* @mouseup, %struct.__pthread* inttoptr (i32 2 to %struct.__pthread*))
  %8 = load i32, i32* @screen_width, align 4
  %conv = sitofp i32 %8 to double
  %div = fdiv double %conv, 2.000000e+00
  %conv6 = fptrunc double %div to float
  %9 = load i32, i32* @screen_height, align 4
  %conv7 = sitofp i32 %9 to double
  %div8 = fdiv double %conv7, 2.000000e+00
  %conv9 = fptrunc double %div8 to float
  %call10 = call i32 @create_player(float %conv6, float %conv9)
  call void @start_wave()
  ret void
}

declare i32 @emscripten_set_keydown_callback_on_thread(i8*, i8*, i32, i32 (i32, %struct.EmscriptenKeyboardEvent*, i8*)*, %struct.__pthread*) #1

declare i32 @emscripten_set_keyup_callback_on_thread(i8*, i8*, i32, i32 (i32, %struct.EmscriptenKeyboardEvent*, i8*)*, %struct.__pthread*) #1

declare i32 @emscripten_set_mousemove_callback_on_thread(i8*, i8*, i32, i32 (i32, %struct.EmscriptenMouseEvent*, i8*)*, %struct.__pthread*) #1

declare i32 @emscripten_set_mousedown_callback_on_thread(i8*, i8*, i32, i32 (i32, %struct.EmscriptenMouseEvent*, i8*)*, %struct.__pthread*) #1

declare i32 @emscripten_set_mouseup_callback_on_thread(i8*, i8*, i32, i32 (i32, %struct.EmscriptenMouseEvent*, i8*)*, %struct.__pthread*) #1

; Function Attrs: noinline nounwind optnone
define hidden i32 @get_score() #0 {
entry:
  %0 = load i32, i32* @score, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define hidden void @step_player(float %delta) #0 {
entry:
  %delta.addr = alloca float, align 4
  %x = alloca float, align 4
  %y = alloca float, align 4
  %x_speed29 = alloca float, align 4
  %y_speed32 = alloca float, align 4
  %dx = alloca float, align 4
  %dy = alloca float, align 4
  %distance = alloca float, align 4
  %dir_x = alloca float, align 4
  %dir_y = alloca float, align 4
  store float %delta, float* %delta.addr, align 4
  %0 = load %struct.Health_Table*, %struct.Health_Table** @health_table, align 4
  %health_points = getelementptr inbounds %struct.Health_Table, %struct.Health_Table* %0, i32 0, i32 4
  %1 = load float*, float** %health_points, align 4
  %arrayidx = getelementptr inbounds float, float* %1, i32 0
  %2 = load float, float* %arrayidx, align 4
  %cmp = fcmp olt float %2, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  br label %if.end50

if.end:                                           ; preds = %entry
  %3 = load %struct.Physics_States*, %struct.Physics_States** @physics_states, align 4
  %x1 = getelementptr inbounds %struct.Physics_States, %struct.Physics_States* %3, i32 0, i32 4
  %4 = load float*, float** %x1, align 4
  %arrayidx2 = getelementptr inbounds float, float* %4, i32 0
  %5 = load float, float* %arrayidx2, align 4
  store float %5, float* %x, align 4
  %6 = load %struct.Physics_States*, %struct.Physics_States** @physics_states, align 4
  %y3 = getelementptr inbounds %struct.Physics_States, %struct.Physics_States* %6, i32 0, i32 5
  %7 = load float*, float** %y3, align 4
  %arrayidx4 = getelementptr inbounds float, float* %7, i32 0
  %8 = load float, float* %arrayidx4, align 4
  store float %8, float* %y, align 4
  %9 = load %struct.Input_State*, %struct.Input_State** @input_state, align 4
  %move_up = getelementptr inbounds %struct.Input_State, %struct.Input_State* %9, i32 0, i32 0
  %10 = load i8, i8* %move_up, align 4
  %tobool = icmp ne i8 %10, 0
  br i1 %tobool, label %if.then5, label %if.else

if.then5:                                         ; preds = %if.end
  %11 = load %struct.Physics_States*, %struct.Physics_States** @physics_states, align 4
  %y_speed = getelementptr inbounds %struct.Physics_States, %struct.Physics_States* %11, i32 0, i32 7
  %12 = load float*, float** %y_speed, align 4
  %arrayidx6 = getelementptr inbounds float, float* %12, i32 0
  store float -2.000000e+02, float* %arrayidx6, align 4
  br label %if.end15

if.else:                                          ; preds = %if.end
  %13 = load %struct.Input_State*, %struct.Input_State** @input_state, align 4
  %move_down = getelementptr inbounds %struct.Input_State, %struct.Input_State* %13, i32 0, i32 2
  %14 = load i8, i8* %move_down, align 2
  %tobool7 = icmp ne i8 %14, 0
  br i1 %tobool7, label %if.then8, label %if.else11

if.then8:                                         ; preds = %if.else
  %15 = load %struct.Physics_States*, %struct.Physics_States** @physics_states, align 4
  %y_speed9 = getelementptr inbounds %struct.Physics_States, %struct.Physics_States* %15, i32 0, i32 7
  %16 = load float*, float** %y_speed9, align 4
  %arrayidx10 = getelementptr inbounds float, float* %16, i32 0
  store float 2.000000e+02, float* %arrayidx10, align 4
  br label %if.end14

if.else11:                                        ; preds = %if.else
  %17 = load %struct.Physics_States*, %struct.Physics_States** @physics_states, align 4
  %y_speed12 = getelementptr inbounds %struct.Physics_States, %struct.Physics_States* %17, i32 0, i32 7
  %18 = load float*, float** %y_speed12, align 4
  %arrayidx13 = getelementptr inbounds float, float* %18, i32 0
  store float 0.000000e+00, float* %arrayidx13, align 4
  br label %if.end14

if.end14:                                         ; preds = %if.else11, %if.then8
  br label %if.end15

if.end15:                                         ; preds = %if.end14, %if.then5
  %19 = load %struct.Input_State*, %struct.Input_State** @input_state, align 4
  %move_left = getelementptr inbounds %struct.Input_State, %struct.Input_State* %19, i32 0, i32 1
  %20 = load i8, i8* %move_left, align 1
  %tobool16 = icmp ne i8 %20, 0
  br i1 %tobool16, label %if.then17, label %if.else19

if.then17:                                        ; preds = %if.end15
  %21 = load %struct.Physics_States*, %struct.Physics_States** @physics_states, align 4
  %x_speed = getelementptr inbounds %struct.Physics_States, %struct.Physics_States* %21, i32 0, i32 6
  %22 = load float*, float** %x_speed, align 4
  %arrayidx18 = getelementptr inbounds float, float* %22, i32 0
  store float -2.000000e+02, float* %arrayidx18, align 4
  br label %if.end28

if.else19:                                        ; preds = %if.end15
  %23 = load %struct.Input_State*, %struct.Input_State** @input_state, align 4
  %move_right = getelementptr inbounds %struct.Input_State, %struct.Input_State* %23, i32 0, i32 3
  %24 = load i8, i8* %move_right, align 1
  %tobool20 = icmp ne i8 %24, 0
  br i1 %tobool20, label %if.then21, label %if.else24

if.then21:                                        ; preds = %if.else19
  %25 = load %struct.Physics_States*, %struct.Physics_States** @physics_states, align 4
  %x_speed22 = getelementptr inbounds %struct.Physics_States, %struct.Physics_States* %25, i32 0, i32 6
  %26 = load float*, float** %x_speed22, align 4
  %arrayidx23 = getelementptr inbounds float, float* %26, i32 0
  store float 2.000000e+02, float* %arrayidx23, align 4
  br label %if.end27

if.else24:                                        ; preds = %if.else19
  %27 = load %struct.Physics_States*, %struct.Physics_States** @physics_states, align 4
  %x_speed25 = getelementptr inbounds %struct.Physics_States, %struct.Physics_States* %27, i32 0, i32 6
  %28 = load float*, float** %x_speed25, align 4
  %arrayidx26 = getelementptr inbounds float, float* %28, i32 0
  store float 0.000000e+00, float* %arrayidx26, align 4
  br label %if.end27

if.end27:                                         ; preds = %if.else24, %if.then21
  br label %if.end28

if.end28:                                         ; preds = %if.end27, %if.then17
  %29 = load %struct.Physics_States*, %struct.Physics_States** @physics_states, align 4
  %x_speed30 = getelementptr inbounds %struct.Physics_States, %struct.Physics_States* %29, i32 0, i32 6
  %30 = load float*, float** %x_speed30, align 4
  %arrayidx31 = getelementptr inbounds float, float* %30, i32 0
  %31 = load float, float* %arrayidx31, align 4
  store float %31, float* %x_speed29, align 4
  %32 = load %struct.Physics_States*, %struct.Physics_States** @physics_states, align 4
  %y_speed33 = getelementptr inbounds %struct.Physics_States, %struct.Physics_States* %32, i32 0, i32 7
  %33 = load float*, float** %y_speed33, align 4
  %arrayidx34 = getelementptr inbounds float, float* %33, i32 0
  %34 = load float, float* %arrayidx34, align 4
  store float %34, float* %y_speed32, align 4
  %35 = load %struct.Input_State*, %struct.Input_State** @input_state, align 4
  %mouse_x = getelementptr inbounds %struct.Input_State, %struct.Input_State* %35, i32 0, i32 5
  %36 = load float, float* %mouse_x, align 4
  %37 = load %struct.Input_State*, %struct.Input_State** @input_state, align 4
  %mouse_y = getelementptr inbounds %struct.Input_State, %struct.Input_State* %37, i32 0, i32 6
  %38 = load float, float* %mouse_y, align 4
  %39 = load float, float* %x, align 4
  %40 = load float, float* %y, align 4
  %41 = load %struct.Physics_States*, %struct.Physics_States** @physics_states, align 4
  %angle = getelementptr inbounds %struct.Physics_States, %struct.Physics_States* %41, i32 0, i32 8
  %42 = load float*, float** %angle, align 4
  %arrayidx35 = getelementptr inbounds float, float* %42, i32 0
  call void @get_angle_to_point(float %36, float %38, float %39, float %40, float* %dx, float* %dy, float* %distance, float* %dir_x, float* %dir_y, float* %arrayidx35)
  %43 = load %struct.Input_State*, %struct.Input_State** @input_state, align 4
  %shoot = getelementptr inbounds %struct.Input_State, %struct.Input_State* %43, i32 0, i32 4
  %44 = load i8, i8* %shoot, align 4
  %conv = zext i8 %44 to i32
  %tobool36 = icmp ne i32 %conv, 0
  br i1 %tobool36, label %land.lhs.true, label %if.end50

land.lhs.true:                                    ; preds = %if.end28
  %45 = load %struct.Weapon_States*, %struct.Weapon_States** @weapon_states, align 4
  %firing_state = getelementptr inbounds %struct.Weapon_States, %struct.Weapon_States* %45, i32 0, i32 2
  %46 = load float*, float** %firing_state, align 4
  %47 = load i32, i32* @curr_weapon, align 4
  %arrayidx37 = getelementptr inbounds float, float* %46, i32 %47
  %48 = load float, float* %arrayidx37, align 4
  %conv38 = fpext float %48 to double
  %cmp39 = fcmp olt double %conv38, 1.000000e-02
  br i1 %cmp39, label %if.then41, label %if.end50

if.then41:                                        ; preds = %land.lhs.true
  %49 = load float, float* %x, align 4
  %50 = load float, float* %dir_x, align 4
  %mul = fmul float %50, 4.000000e+01
  %sub = fsub float %49, %mul
  %51 = load float, float* %y, align 4
  %52 = load float, float* %dir_y, align 4
  %mul42 = fmul float %52, 4.000000e+01
  %sub43 = fsub float %51, %mul42
  %53 = load float, float* %dir_x, align 4
  %fneg = fneg float %53
  %mul44 = fmul float %fneg, 1.000000e+03
  %54 = load float, float* %x_speed29, align 4
  %add = fadd float %mul44, %54
  %55 = load float, float* %dir_y, align 4
  %fneg45 = fneg float %55
  %mul46 = fmul float %fneg45, 1.000000e+03
  %56 = load float, float* %y_speed32, align 4
  %add47 = fadd float %mul46, %56
  %call = call i32 @create_bullet(float %sub, float %sub43, float %add, float %add47)
  %57 = load %struct.Weapon_States*, %struct.Weapon_States** @weapon_states, align 4
  %firing_state48 = getelementptr inbounds %struct.Weapon_States, %struct.Weapon_States* %57, i32 0, i32 2
  %58 = load float*, float** %firing_state48, align 4
  %59 = load i32, i32* @curr_weapon, align 4
  %arrayidx49 = getelementptr inbounds float, float* %58, i32 %59
  store float 1.000000e+02, float* %arrayidx49, align 4
  br label %if.end50

if.end50:                                         ; preds = %if.then, %if.then41, %land.lhs.true, %if.end28
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @step_bullets(float %delta) #0 {
entry:
  %delta.addr = alloca float, align 4
  %i = alloca i32, align 4
  %entity_id = alloca i32, align 4
  %created_at = alloca %struct.timespec, align 4
  %collision_id = alloca i32, align 4
  %entity_id_2 = alloca i32, align 4
  %ai_enemy_id = alloca i32, align 4
  %enemy_health_id = alloca i32, align 4
  %damage = alloca float, align 4
  %physics_id = alloca i32, align 4
  %x = alloca float, align 4
  %y = alloca float, align 4
  store float %delta, float* %delta.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load %struct.Bullet_Table*, %struct.Bullet_Table** @bullets, align 4
  %curr_max = getelementptr inbounds %struct.Bullet_Table, %struct.Bullet_Table* %1, i32 0, i32 2
  %2 = load i32, i32* %curr_max, align 4
  %cmp = icmp ult i32 %0, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %struct.Bullet_Table*, %struct.Bullet_Table** @bullets, align 4
  %used = getelementptr inbounds %struct.Bullet_Table, %struct.Bullet_Table* %3, i32 0, i32 1
  %4 = load i8*, i8** %used, align 4
  %5 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i8, i8* %4, i32 %5
  %6 = load i8, i8* %arrayidx, align 1
  %tobool = icmp ne i8 %6, 0
  br i1 %tobool, label %if.then, label %if.end45

if.then:                                          ; preds = %for.body
  %7 = load %struct.Bullet_Table*, %struct.Bullet_Table** @bullets, align 4
  %entity_id1 = getelementptr inbounds %struct.Bullet_Table, %struct.Bullet_Table* %7, i32 0, i32 3
  %8 = load i32*, i32** %entity_id1, align 4
  %9 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds i32, i32* %8, i32 %9
  %10 = load i32, i32* %arrayidx2, align 4
  store i32 %10, i32* %entity_id, align 4
  %11 = load %struct.Bullet_Table*, %struct.Bullet_Table** @bullets, align 4
  %created_at3 = getelementptr inbounds %struct.Bullet_Table, %struct.Bullet_Table* %11, i32 0, i32 5
  %12 = load %struct.timespec*, %struct.timespec** %created_at3, align 4
  %13 = load i32, i32* %i, align 4
  %arrayidx4 = getelementptr inbounds %struct.timespec, %struct.timespec* %12, i32 %13
  %14 = bitcast %struct.timespec* %created_at to i8*
  %15 = bitcast %struct.timespec* %arrayidx4 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %14, i8* align 4 %15, i32 8, i1 false)
  %call = call float @timespec_diff_float(%struct.timespec* @curr_time, %struct.timespec* %created_at)
  %cmp5 = fcmp ogt float %call, 5.000000e+00
  br i1 %cmp5, label %if.then6, label %if.end

if.then6:                                         ; preds = %if.then
  %16 = load i32, i32* %entity_id, align 4
  call void @destroy_bullet(i32 %16)
  br label %for.end

if.end:                                           ; preds = %if.then
  %17 = load %struct.Collision_Table*, %struct.Collision_Table** @collision_table, align 4
  %18 = bitcast %struct.Collision_Table* %17 to i8*
  %19 = load i32, i32* %entity_id, align 4
  %call7 = call i32 @find_item_index(i8* %18, i32 %19)
  store i32 %call7, i32* %collision_id, align 4
  %20 = load i32, i32* %collision_id, align 4
  %21 = load %struct.Collision_Table*, %struct.Collision_Table** @collision_table, align 4
  %curr_max8 = getelementptr inbounds %struct.Collision_Table, %struct.Collision_Table* %21, i32 0, i32 2
  %22 = load i32, i32* %curr_max8, align 4
  %cmp9 = icmp ult i32 %20, %22
  br i1 %cmp9, label %if.then10, label %if.end27

if.then10:                                        ; preds = %if.end
  %23 = load %struct.Collision_Table*, %struct.Collision_Table** @collision_table, align 4
  %entity_id_211 = getelementptr inbounds %struct.Collision_Table, %struct.Collision_Table* %23, i32 0, i32 4
  %24 = load i32*, i32** %entity_id_211, align 4
  %25 = load i32, i32* %collision_id, align 4
  %arrayidx12 = getelementptr inbounds i32, i32* %24, i32 %25
  %26 = load i32, i32* %arrayidx12, align 4
  store i32 %26, i32* %entity_id_2, align 4
  %27 = load %struct.AI_Enemy*, %struct.AI_Enemy** @ai_enemy, align 4
  %28 = bitcast %struct.AI_Enemy* %27 to i8*
  %29 = load i32, i32* %entity_id_2, align 4
  %call13 = call i32 @find_item_index(i8* %28, i32 %29)
  store i32 %call13, i32* %ai_enemy_id, align 4
  %30 = load i32, i32* %ai_enemy_id, align 4
  %31 = load %struct.AI_Enemy*, %struct.AI_Enemy** @ai_enemy, align 4
  %curr_max14 = getelementptr inbounds %struct.AI_Enemy, %struct.AI_Enemy* %31, i32 0, i32 2
  %32 = load i32, i32* %curr_max14, align 4
  %cmp15 = icmp ult i32 %30, %32
  br i1 %cmp15, label %if.then16, label %if.end26

if.then16:                                        ; preds = %if.then10
  %33 = load %struct.Health_Table*, %struct.Health_Table** @health_table, align 4
  %34 = bitcast %struct.Health_Table* %33 to i8*
  %35 = load i32, i32* %entity_id_2, align 4
  %call17 = call i32 @find_item_index(i8* %34, i32 %35)
  store i32 %call17, i32* %enemy_health_id, align 4
  %36 = load i32, i32* %enemy_health_id, align 4
  %37 = load %struct.Health_Table*, %struct.Health_Table** @health_table, align 4
  %curr_max18 = getelementptr inbounds %struct.Health_Table, %struct.Health_Table* %37, i32 0, i32 2
  %38 = load i32, i32* %curr_max18, align 4
  %cmp19 = icmp ult i32 %36, %38
  br i1 %cmp19, label %if.then20, label %if.end24

if.then20:                                        ; preds = %if.then16
  %39 = load %struct.Bullet_Table*, %struct.Bullet_Table** @bullets, align 4
  %damage21 = getelementptr inbounds %struct.Bullet_Table, %struct.Bullet_Table* %39, i32 0, i32 4
  %40 = load float*, float** %damage21, align 4
  %41 = load i32, i32* %i, align 4
  %arrayidx22 = getelementptr inbounds float, float* %40, i32 %41
  %42 = load float, float* %arrayidx22, align 4
  store float %42, float* %damage, align 4
  %43 = load float, float* %damage, align 4
  %44 = load %struct.Health_Table*, %struct.Health_Table** @health_table, align 4
  %health_points = getelementptr inbounds %struct.Health_Table, %struct.Health_Table* %44, i32 0, i32 4
  %45 = load float*, float** %health_points, align 4
  %46 = load i32, i32* %enemy_health_id, align 4
  %arrayidx23 = getelementptr inbounds float, float* %45, i32 %46
  %47 = load float, float* %arrayidx23, align 4
  %sub = fsub float %47, %43
  store float %sub, float* %arrayidx23, align 4
  br label %if.end24

if.end24:                                         ; preds = %if.then20, %if.then16
  %48 = load i32, i32* %entity_id_2, align 4
  %call25 = call i32 @add_hit_feedback_item(i32 %48, float 1.000000e+02)
  %49 = load i32, i32* %entity_id, align 4
  call void @destroy_bullet(i32 %49)
  br label %for.end

if.end26:                                         ; preds = %if.then10
  br label %if.end27

if.end27:                                         ; preds = %if.end26, %if.end
  %50 = load %struct.Physics_States*, %struct.Physics_States** @physics_states, align 4
  %51 = bitcast %struct.Physics_States* %50 to i8*
  %52 = load i32, i32* %entity_id, align 4
  %call28 = call i32 @find_item_index(i8* %51, i32 %52)
  store i32 %call28, i32* %physics_id, align 4
  %53 = load %struct.Physics_States*, %struct.Physics_States** @physics_states, align 4
  %x29 = getelementptr inbounds %struct.Physics_States, %struct.Physics_States* %53, i32 0, i32 4
  %54 = load float*, float** %x29, align 4
  %55 = load i32, i32* %physics_id, align 4
  %arrayidx30 = getelementptr inbounds float, float* %54, i32 %55
  %56 = load float, float* %arrayidx30, align 4
  store float %56, float* %x, align 4
  %57 = load %struct.Physics_States*, %struct.Physics_States** @physics_states, align 4
  %y31 = getelementptr inbounds %struct.Physics_States, %struct.Physics_States* %57, i32 0, i32 5
  %58 = load float*, float** %y31, align 4
  %59 = load i32, i32* %physics_id, align 4
  %arrayidx32 = getelementptr inbounds float, float* %58, i32 %59
  %60 = load float, float* %arrayidx32, align 4
  store float %60, float* %y, align 4
  %61 = load float, float* %x, align 4
  %cmp33 = fcmp olt float %61, 0.000000e+00
  br i1 %cmp33, label %if.then43, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end27
  %62 = load float, float* %x, align 4
  %63 = load i32, i32* @screen_width, align 4
  %conv = sitofp i32 %63 to float
  %cmp34 = fcmp ogt float %62, %conv
  br i1 %cmp34, label %if.then43, label %lor.lhs.false36

lor.lhs.false36:                                  ; preds = %lor.lhs.false
  %64 = load float, float* %y, align 4
  %cmp37 = fcmp olt float %64, 0.000000e+00
  br i1 %cmp37, label %if.then43, label %lor.lhs.false39

lor.lhs.false39:                                  ; preds = %lor.lhs.false36
  %65 = load float, float* %y, align 4
  %66 = load i32, i32* @screen_height, align 4
  %conv40 = sitofp i32 %66 to float
  %cmp41 = fcmp ogt float %65, %conv40
  br i1 %cmp41, label %if.then43, label %if.end44

if.then43:                                        ; preds = %lor.lhs.false39, %lor.lhs.false36, %lor.lhs.false, %if.end27
  %67 = load i32, i32* %entity_id, align 4
  call void @destroy_bullet(i32 %67)
  br label %for.end

if.end44:                                         ; preds = %lor.lhs.false39
  br label %if.end45

if.end45:                                         ; preds = %if.end44, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end45
  %68 = load i32, i32* %i, align 4
  %add = add i32 %68, 1
  store i32 %add, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %if.then43, %if.end24, %if.then6, %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @step_physics_balls(float %delta) #0 {
entry:
  %delta.addr = alloca float, align 4
  %delta_iter = alloca float, align 4
  %iter = alloca i32, align 4
  %i = alloca i32, align 4
  %entity_id = alloca i32, align 4
  %physics_id = alloca i32, align 4
  %is_bullet = alloca i8, align 1
  %is_enemy = alloca i8, align 1
  %x = alloca float, align 4
  %y = alloca float, align 4
  %radius = alloca float, align 4
  %mass = alloca float, align 4
  %j = alloca i32, align 4
  %j_entity_id = alloca i32, align 4
  %j_physics_id = alloca i32, align 4
  %j_is_bullet = alloca i8, align 1
  %j_is_enemy = alloca i8, align 1
  %j_x = alloca float, align 4
  %j_y = alloca float, align 4
  %j_radius = alloca float, align 4
  %j_mass = alloca float, align 4
  %dx = alloca float, align 4
  %dy = alloca float, align 4
  %distance = alloca float, align 4
  %mid_x = alloca float, align 4
  %mid_y = alloca float, align 4
  %dir_x = alloca float, align 4
  %dir_y = alloca float, align 4
  %power = alloca float, align 4
  %push_x = alloca float, align 4
  %push_y = alloca float, align 4
  store float %delta, float* %delta.addr, align 4
  %0 = load float, float* %delta.addr, align 4
  %div = fdiv float %0, 2.000000e+00
  store float %div, float* %delta_iter, align 4
  store i32 0, i32* %iter, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc112, %entry
  %1 = load i32, i32* %iter, align 4
  %cmp = icmp ult i32 %1, 2
  br i1 %cmp, label %for.body, label %for.end114

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %i, align 4
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc109, %for.body
  %2 = load i32, i32* %i, align 4
  %3 = load %struct.Physics_Balls*, %struct.Physics_Balls** @physics_balls, align 4
  %curr_max = getelementptr inbounds %struct.Physics_Balls, %struct.Physics_Balls* %3, i32 0, i32 2
  %4 = load i32, i32* %curr_max, align 4
  %cmp2 = icmp ult i32 %2, %4
  br i1 %cmp2, label %for.body3, label %for.end111

for.body3:                                        ; preds = %for.cond1
  %5 = load %struct.Physics_Balls*, %struct.Physics_Balls** @physics_balls, align 4
  %used = getelementptr inbounds %struct.Physics_Balls, %struct.Physics_Balls* %5, i32 0, i32 1
  %6 = load i8*, i8** %used, align 4
  %7 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i8, i8* %6, i32 %7
  %8 = load i8, i8* %arrayidx, align 1
  %tobool = icmp ne i8 %8, 0
  br i1 %tobool, label %if.then, label %if.end108

if.then:                                          ; preds = %for.body3
  %9 = load %struct.Physics_Balls*, %struct.Physics_Balls** @physics_balls, align 4
  %entity_id4 = getelementptr inbounds %struct.Physics_Balls, %struct.Physics_Balls* %9, i32 0, i32 3
  %10 = load i32*, i32** %entity_id4, align 4
  %11 = load i32, i32* %i, align 4
  %arrayidx5 = getelementptr inbounds i32, i32* %10, i32 %11
  %12 = load i32, i32* %arrayidx5, align 4
  store i32 %12, i32* %entity_id, align 4
  %13 = load %struct.Physics_States*, %struct.Physics_States** @physics_states, align 4
  %14 = bitcast %struct.Physics_States* %13 to i8*
  %15 = load i32, i32* %entity_id, align 4
  %call = call i32 @find_item_index(i8* %14, i32 %15)
  store i32 %call, i32* %physics_id, align 4
  %16 = load %struct.Bullet_Table*, %struct.Bullet_Table** @bullets, align 4
  %17 = bitcast %struct.Bullet_Table* %16 to i8*
  %18 = load i32, i32* %entity_id, align 4
  %call6 = call i32 @find_item_index(i8* %17, i32 %18)
  %19 = load %struct.Bullet_Table*, %struct.Bullet_Table** @bullets, align 4
  %curr_max7 = getelementptr inbounds %struct.Bullet_Table, %struct.Bullet_Table* %19, i32 0, i32 2
  %20 = load i32, i32* %curr_max7, align 4
  %cmp8 = icmp ult i32 %call6, %20
  %conv = zext i1 %cmp8 to i32
  %conv9 = trunc i32 %conv to i8
  store i8 %conv9, i8* %is_bullet, align 1
  %21 = load %struct.AI_Enemy*, %struct.AI_Enemy** @ai_enemy, align 4
  %22 = bitcast %struct.AI_Enemy* %21 to i8*
  %23 = load i32, i32* %entity_id, align 4
  %call10 = call i32 @find_item_index(i8* %22, i32 %23)
  %24 = load %struct.AI_Enemy*, %struct.AI_Enemy** @ai_enemy, align 4
  %curr_max11 = getelementptr inbounds %struct.AI_Enemy, %struct.AI_Enemy* %24, i32 0, i32 2
  %25 = load i32, i32* %curr_max11, align 4
  %cmp12 = icmp ult i32 %call10, %25
  %conv13 = zext i1 %cmp12 to i32
  %conv14 = trunc i32 %conv13 to i8
  store i8 %conv14, i8* %is_enemy, align 1
  %26 = load %struct.Physics_States*, %struct.Physics_States** @physics_states, align 4
  %x15 = getelementptr inbounds %struct.Physics_States, %struct.Physics_States* %26, i32 0, i32 4
  %27 = load float*, float** %x15, align 4
  %28 = load i32, i32* %physics_id, align 4
  %arrayidx16 = getelementptr inbounds float, float* %27, i32 %28
  %29 = load float, float* %arrayidx16, align 4
  store float %29, float* %x, align 4
  %30 = load %struct.Physics_States*, %struct.Physics_States** @physics_states, align 4
  %y17 = getelementptr inbounds %struct.Physics_States, %struct.Physics_States* %30, i32 0, i32 5
  %31 = load float*, float** %y17, align 4
  %32 = load i32, i32* %physics_id, align 4
  %arrayidx18 = getelementptr inbounds float, float* %31, i32 %32
  %33 = load float, float* %arrayidx18, align 4
  store float %33, float* %y, align 4
  %34 = load %struct.Physics_Balls*, %struct.Physics_Balls** @physics_balls, align 4
  %radius19 = getelementptr inbounds %struct.Physics_Balls, %struct.Physics_Balls* %34, i32 0, i32 4
  %35 = load float*, float** %radius19, align 4
  %36 = load i32, i32* %i, align 4
  %arrayidx20 = getelementptr inbounds float, float* %35, i32 %36
  %37 = load float, float* %arrayidx20, align 4
  store float %37, float* %radius, align 4
  %38 = load %struct.Physics_Balls*, %struct.Physics_Balls** @physics_balls, align 4
  %mass21 = getelementptr inbounds %struct.Physics_Balls, %struct.Physics_Balls* %38, i32 0, i32 5
  %39 = load float*, float** %mass21, align 4
  %40 = load i32, i32* %i, align 4
  %arrayidx22 = getelementptr inbounds float, float* %39, i32 %40
  %41 = load float, float* %arrayidx22, align 4
  store float %41, float* %mass, align 4
  store i32 0, i32* %j, align 4
  br label %for.cond23

for.cond23:                                       ; preds = %for.inc, %if.then
  %42 = load i32, i32* %j, align 4
  %43 = load %struct.Physics_Balls*, %struct.Physics_Balls** @physics_balls, align 4
  %curr_max24 = getelementptr inbounds %struct.Physics_Balls, %struct.Physics_Balls* %43, i32 0, i32 2
  %44 = load i32, i32* %curr_max24, align 4
  %cmp25 = icmp ult i32 %42, %44
  br i1 %cmp25, label %for.body27, label %for.end

for.body27:                                       ; preds = %for.cond23
  %45 = load %struct.Physics_Balls*, %struct.Physics_Balls** @physics_balls, align 4
  %used28 = getelementptr inbounds %struct.Physics_Balls, %struct.Physics_Balls* %45, i32 0, i32 1
  %46 = load i8*, i8** %used28, align 4
  %47 = load i32, i32* %j, align 4
  %arrayidx29 = getelementptr inbounds i8, i8* %46, i32 %47
  %48 = load i8, i8* %arrayidx29, align 1
  %tobool30 = icmp ne i8 %48, 0
  br i1 %tobool30, label %if.then31, label %if.end106

if.then31:                                        ; preds = %for.body27
  %49 = load i32, i32* %j, align 4
  %50 = load i32, i32* %i, align 4
  %cmp32 = icmp ne i32 %49, %50
  br i1 %cmp32, label %if.then34, label %if.end105

if.then34:                                        ; preds = %if.then31
  %51 = load %struct.Physics_Balls*, %struct.Physics_Balls** @physics_balls, align 4
  %entity_id35 = getelementptr inbounds %struct.Physics_Balls, %struct.Physics_Balls* %51, i32 0, i32 3
  %52 = load i32*, i32** %entity_id35, align 4
  %53 = load i32, i32* %j, align 4
  %arrayidx36 = getelementptr inbounds i32, i32* %52, i32 %53
  %54 = load i32, i32* %arrayidx36, align 4
  store i32 %54, i32* %j_entity_id, align 4
  %55 = load %struct.Physics_States*, %struct.Physics_States** @physics_states, align 4
  %56 = bitcast %struct.Physics_States* %55 to i8*
  %57 = load i32, i32* %j_entity_id, align 4
  %call37 = call i32 @find_item_index(i8* %56, i32 %57)
  store i32 %call37, i32* %j_physics_id, align 4
  %58 = load %struct.Bullet_Table*, %struct.Bullet_Table** @bullets, align 4
  %59 = bitcast %struct.Bullet_Table* %58 to i8*
  %60 = load i32, i32* %j_entity_id, align 4
  %call38 = call i32 @find_item_index(i8* %59, i32 %60)
  %61 = load %struct.Bullet_Table*, %struct.Bullet_Table** @bullets, align 4
  %curr_max39 = getelementptr inbounds %struct.Bullet_Table, %struct.Bullet_Table* %61, i32 0, i32 2
  %62 = load i32, i32* %curr_max39, align 4
  %cmp40 = icmp ult i32 %call38, %62
  %conv41 = zext i1 %cmp40 to i32
  %conv42 = trunc i32 %conv41 to i8
  store i8 %conv42, i8* %j_is_bullet, align 1
  %63 = load %struct.AI_Enemy*, %struct.AI_Enemy** @ai_enemy, align 4
  %64 = bitcast %struct.AI_Enemy* %63 to i8*
  %65 = load i32, i32* %j_entity_id, align 4
  %call43 = call i32 @find_item_index(i8* %64, i32 %65)
  %66 = load %struct.AI_Enemy*, %struct.AI_Enemy** @ai_enemy, align 4
  %curr_max44 = getelementptr inbounds %struct.AI_Enemy, %struct.AI_Enemy* %66, i32 0, i32 2
  %67 = load i32, i32* %curr_max44, align 4
  %cmp45 = icmp ult i32 %call43, %67
  %conv46 = zext i1 %cmp45 to i32
  %conv47 = trunc i32 %conv46 to i8
  store i8 %conv47, i8* %j_is_enemy, align 1
  %68 = load %struct.Physics_States*, %struct.Physics_States** @physics_states, align 4
  %x48 = getelementptr inbounds %struct.Physics_States, %struct.Physics_States* %68, i32 0, i32 4
  %69 = load float*, float** %x48, align 4
  %70 = load i32, i32* %j_physics_id, align 4
  %arrayidx49 = getelementptr inbounds float, float* %69, i32 %70
  %71 = load float, float* %arrayidx49, align 4
  store float %71, float* %j_x, align 4
  %72 = load %struct.Physics_States*, %struct.Physics_States** @physics_states, align 4
  %y50 = getelementptr inbounds %struct.Physics_States, %struct.Physics_States* %72, i32 0, i32 5
  %73 = load float*, float** %y50, align 4
  %74 = load i32, i32* %j_physics_id, align 4
  %arrayidx51 = getelementptr inbounds float, float* %73, i32 %74
  %75 = load float, float* %arrayidx51, align 4
  store float %75, float* %j_y, align 4
  %76 = load %struct.Physics_Balls*, %struct.Physics_Balls** @physics_balls, align 4
  %radius52 = getelementptr inbounds %struct.Physics_Balls, %struct.Physics_Balls* %76, i32 0, i32 4
  %77 = load float*, float** %radius52, align 4
  %78 = load i32, i32* %j, align 4
  %arrayidx53 = getelementptr inbounds float, float* %77, i32 %78
  %79 = load float, float* %arrayidx53, align 4
  store float %79, float* %j_radius, align 4
  %80 = load %struct.Physics_Balls*, %struct.Physics_Balls** @physics_balls, align 4
  %mass54 = getelementptr inbounds %struct.Physics_Balls, %struct.Physics_Balls* %80, i32 0, i32 5
  %81 = load float*, float** %mass54, align 4
  %82 = load i32, i32* %j, align 4
  %arrayidx55 = getelementptr inbounds float, float* %81, i32 %82
  %83 = load float, float* %arrayidx55, align 4
  store float %83, float* %j_mass, align 4
  %84 = load float, float* %x, align 4
  %85 = load float, float* %y, align 4
  %86 = load float, float* %j_x, align 4
  %87 = load float, float* %j_y, align 4
  call void @get_distance_to_point(float %84, float %85, float %86, float %87, float* %dx, float* %dy, float* %distance)
  %88 = load float, float* %distance, align 4
  %89 = load float, float* %radius, align 4
  %90 = load float, float* %j_radius, align 4
  %add = fadd float %89, %90
  %cmp56 = fcmp olt float %88, %add
  br i1 %cmp56, label %if.then58, label %if.end104

if.then58:                                        ; preds = %if.then34
  %91 = load i8, i8* %is_enemy, align 1
  %conv59 = zext i8 %91 to i32
  %tobool60 = icmp ne i32 %conv59, 0
  br i1 %tobool60, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %if.then58
  %92 = load i8, i8* %j_is_bullet, align 1
  %conv61 = zext i8 %92 to i32
  %tobool62 = icmp ne i32 %conv61, 0
  br i1 %tobool62, label %if.then63, label %if.end

if.then63:                                        ; preds = %land.lhs.true
  %93 = load %struct.Physics_States*, %struct.Physics_States** @physics_states, align 4
  %x_speed = getelementptr inbounds %struct.Physics_States, %struct.Physics_States* %93, i32 0, i32 6
  %94 = load float*, float** %x_speed, align 4
  %95 = load i32, i32* %i, align 4
  %arrayidx64 = getelementptr inbounds float, float* %94, i32 %95
  %96 = load float, float* %arrayidx64, align 4
  %97 = load float, float* %delta.addr, align 4
  %mul = fmul float %96, %97
  %mul65 = fmul float %mul, 1.000000e+01
  %98 = load %struct.Physics_States*, %struct.Physics_States** @physics_states, align 4
  %x66 = getelementptr inbounds %struct.Physics_States, %struct.Physics_States* %98, i32 0, i32 4
  %99 = load float*, float** %x66, align 4
  %100 = load i32, i32* %i, align 4
  %arrayidx67 = getelementptr inbounds float, float* %99, i32 %100
  %101 = load float, float* %arrayidx67, align 4
  %sub = fsub float %101, %mul65
  store float %sub, float* %arrayidx67, align 4
  %102 = load %struct.Physics_States*, %struct.Physics_States** @physics_states, align 4
  %y_speed = getelementptr inbounds %struct.Physics_States, %struct.Physics_States* %102, i32 0, i32 7
  %103 = load float*, float** %y_speed, align 4
  %104 = load i32, i32* %i, align 4
  %arrayidx68 = getelementptr inbounds float, float* %103, i32 %104
  %105 = load float, float* %arrayidx68, align 4
  %106 = load float, float* %delta.addr, align 4
  %mul69 = fmul float %105, %106
  %mul70 = fmul float %mul69, 1.000000e+01
  %107 = load %struct.Physics_States*, %struct.Physics_States** @physics_states, align 4
  %y71 = getelementptr inbounds %struct.Physics_States, %struct.Physics_States* %107, i32 0, i32 5
  %108 = load float*, float** %y71, align 4
  %109 = load i32, i32* %i, align 4
  %arrayidx72 = getelementptr inbounds float, float* %108, i32 %109
  %110 = load float, float* %arrayidx72, align 4
  %sub73 = fsub float %110, %mul70
  store float %sub73, float* %arrayidx72, align 4
  %111 = load i32, i32* %j_entity_id, align 4
  %112 = load i32, i32* %entity_id, align 4
  %call74 = call i32 @add_collision_item(i32 %111, i32 %112)
  %113 = load i32, i32* %j_entity_id, align 4
  call void @remove_physics_state(i32 %113)
  %114 = load i32, i32* %j_entity_id, align 4
  call void @remove_sprite_map(i32 %114)
  %115 = load i32, i32* %j_entity_id, align 4
  call void @remove_physics_ball(i32 %115)
  br label %for.end

if.end:                                           ; preds = %land.lhs.true, %if.then58
  %116 = load float, float* %dx, align 4
  %div75 = fdiv float %116, 2.000000e+00
  store float %div75, float* %mid_x, align 4
  %117 = load float, float* %dy, align 4
  %div76 = fdiv float %117, 2.000000e+00
  store float %div76, float* %mid_y, align 4
  %118 = load float, float* %dx, align 4
  %119 = load float, float* %distance, align 4
  %div77 = fdiv float %118, %119
  store float %div77, float* %dir_x, align 4
  %120 = load float, float* %dy, align 4
  %121 = load float, float* %distance, align 4
  %div78 = fdiv float %120, %121
  store float %div78, float* %dir_y, align 4
  %122 = load float, float* %radius, align 4
  %123 = load float, float* %j_radius, align 4
  %add79 = fadd float %122, %123
  %124 = load float, float* %distance, align 4
  %sub80 = fsub float %add79, %124
  store float %sub80, float* %power, align 4
  %125 = load float, float* %mid_x, align 4
  %126 = load float, float* %dir_x, align 4
  %127 = load float, float* %radius, align 4
  %mul81 = fmul float %126, %127
  %add82 = fadd float %125, %mul81
  %128 = load float, float* %power, align 4
  %mul83 = fmul float %add82, %128
  store float %mul83, float* %push_x, align 4
  %129 = load float, float* %mid_y, align 4
  %130 = load float, float* %dir_y, align 4
  %131 = load float, float* %radius, align 4
  %mul84 = fmul float %130, %131
  %add85 = fadd float %129, %mul84
  %132 = load float, float* %power, align 4
  %mul86 = fmul float %add85, %132
  store float %mul86, float* %push_y, align 4
  %133 = load float, float* %push_x, align 4
  %134 = load float, float* %mass, align 4
  %div87 = fdiv float %133, %134
  %135 = load %struct.Physics_States*, %struct.Physics_States** @physics_states, align 4
  %x_speed88 = getelementptr inbounds %struct.Physics_States, %struct.Physics_States* %135, i32 0, i32 6
  %136 = load float*, float** %x_speed88, align 4
  %137 = load i32, i32* %physics_id, align 4
  %arrayidx89 = getelementptr inbounds float, float* %136, i32 %137
  %138 = load float, float* %arrayidx89, align 4
  %sub90 = fsub float %138, %div87
  store float %sub90, float* %arrayidx89, align 4
  %139 = load float, float* %push_y, align 4
  %140 = load float, float* %mass, align 4
  %div91 = fdiv float %139, %140
  %141 = load %struct.Physics_States*, %struct.Physics_States** @physics_states, align 4
  %y_speed92 = getelementptr inbounds %struct.Physics_States, %struct.Physics_States* %141, i32 0, i32 7
  %142 = load float*, float** %y_speed92, align 4
  %143 = load i32, i32* %physics_id, align 4
  %arrayidx93 = getelementptr inbounds float, float* %142, i32 %143
  %144 = load float, float* %arrayidx93, align 4
  %sub94 = fsub float %144, %div91
  store float %sub94, float* %arrayidx93, align 4
  %145 = load float, float* %push_x, align 4
  %146 = load float, float* %j_mass, align 4
  %div95 = fdiv float %145, %146
  %147 = load %struct.Physics_States*, %struct.Physics_States** @physics_states, align 4
  %x_speed96 = getelementptr inbounds %struct.Physics_States, %struct.Physics_States* %147, i32 0, i32 6
  %148 = load float*, float** %x_speed96, align 4
  %149 = load i32, i32* %j_physics_id, align 4
  %arrayidx97 = getelementptr inbounds float, float* %148, i32 %149
  %150 = load float, float* %arrayidx97, align 4
  %add98 = fadd float %150, %div95
  store float %add98, float* %arrayidx97, align 4
  %151 = load float, float* %push_y, align 4
  %152 = load float, float* %j_mass, align 4
  %div99 = fdiv float %151, %152
  %153 = load %struct.Physics_States*, %struct.Physics_States** @physics_states, align 4
  %y_speed100 = getelementptr inbounds %struct.Physics_States, %struct.Physics_States* %153, i32 0, i32 7
  %154 = load float*, float** %y_speed100, align 4
  %155 = load i32, i32* %j_physics_id, align 4
  %arrayidx101 = getelementptr inbounds float, float* %154, i32 %155
  %156 = load float, float* %arrayidx101, align 4
  %add102 = fadd float %156, %div99
  store float %add102, float* %arrayidx101, align 4
  %157 = load i32, i32* %entity_id, align 4
  %158 = load i32, i32* %j_entity_id, align 4
  %call103 = call i32 @add_collision_item(i32 %157, i32 %158)
  br label %if.end104

if.end104:                                        ; preds = %if.end, %if.then34
  br label %if.end105

if.end105:                                        ; preds = %if.end104, %if.then31
  br label %if.end106

if.end106:                                        ; preds = %if.end105, %for.body27
  br label %for.inc

for.inc:                                          ; preds = %if.end106
  %159 = load i32, i32* %j, align 4
  %add107 = add i32 %159, 1
  store i32 %add107, i32* %j, align 4
  br label %for.cond23

for.end:                                          ; preds = %if.then63, %for.cond23
  br label %if.end108

if.end108:                                        ; preds = %for.end, %for.body3
  br label %for.inc109

for.inc109:                                       ; preds = %if.end108
  %160 = load i32, i32* %i, align 4
  %add110 = add i32 %160, 1
  store i32 %add110, i32* %i, align 4
  br label %for.cond1

for.end111:                                       ; preds = %for.cond1
  br label %for.inc112

for.inc112:                                       ; preds = %for.end111
  %161 = load i32, i32* %iter, align 4
  %add113 = add i32 %161, 1
  store i32 %add113, i32* %iter, align 4
  br label %for.cond

for.end114:                                       ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @step_physics(float %delta) #0 {
entry:
  %delta.addr = alloca float, align 4
  %delta_iter = alloca float, align 4
  %iter = alloca i32, align 4
  %i = alloca i32, align 4
  store float %delta, float* %delta.addr, align 4
  %0 = load float, float* %delta.addr, align 4
  %div = fdiv float %0, 2.000000e+00
  store float %div, float* %delta_iter, align 4
  store i32 0, i32* %iter, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc16, %entry
  %1 = load i32, i32* %iter, align 4
  %cmp = icmp ult i32 %1, 2
  br i1 %cmp, label %for.body, label %for.end18

for.body:                                         ; preds = %for.cond
  %2 = load float, float* %delta_iter, align 4
  call void @step_physics_balls(float %2)
  store i32 0, i32* %i, align 4
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc, %for.body
  %3 = load i32, i32* %i, align 4
  %4 = load %struct.Physics_States*, %struct.Physics_States** @physics_states, align 4
  %curr_max = getelementptr inbounds %struct.Physics_States, %struct.Physics_States* %4, i32 0, i32 2
  %5 = load i32, i32* %curr_max, align 4
  %cmp2 = icmp ult i32 %3, %5
  br i1 %cmp2, label %for.body3, label %for.end

for.body3:                                        ; preds = %for.cond1
  %6 = load %struct.Physics_States*, %struct.Physics_States** @physics_states, align 4
  %used = getelementptr inbounds %struct.Physics_States, %struct.Physics_States* %6, i32 0, i32 1
  %7 = load i8*, i8** %used, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i8, i8* %7, i32 %8
  %9 = load i8, i8* %arrayidx, align 1
  %tobool = icmp ne i8 %9, 0
  br i1 %tobool, label %if.then, label %if.end14

if.then:                                          ; preds = %for.body3
  %10 = load i32, i32* %i, align 4
  %cmp4 = icmp eq i32 %10, 0
  br i1 %cmp4, label %land.lhs.true, label %if.then7

land.lhs.true:                                    ; preds = %if.then
  %11 = load %struct.Health_Table*, %struct.Health_Table** @health_table, align 4
  %health_points = getelementptr inbounds %struct.Health_Table, %struct.Health_Table* %11, i32 0, i32 4
  %12 = load float*, float** %health_points, align 4
  %arrayidx5 = getelementptr inbounds float, float* %12, i32 0
  %13 = load float, float* %arrayidx5, align 4
  %cmp6 = fcmp olt float %13, 0.000000e+00
  br i1 %cmp6, label %if.end, label %if.then7

if.then7:                                         ; preds = %land.lhs.true, %if.then
  %14 = load %struct.Physics_States*, %struct.Physics_States** @physics_states, align 4
  %x_speed = getelementptr inbounds %struct.Physics_States, %struct.Physics_States* %14, i32 0, i32 6
  %15 = load float*, float** %x_speed, align 4
  %16 = load i32, i32* %i, align 4
  %arrayidx8 = getelementptr inbounds float, float* %15, i32 %16
  %17 = load float, float* %arrayidx8, align 4
  %18 = load float, float* %delta_iter, align 4
  %mul = fmul float %17, %18
  %19 = load %struct.Physics_States*, %struct.Physics_States** @physics_states, align 4
  %x = getelementptr inbounds %struct.Physics_States, %struct.Physics_States* %19, i32 0, i32 4
  %20 = load float*, float** %x, align 4
  %21 = load i32, i32* %i, align 4
  %arrayidx9 = getelementptr inbounds float, float* %20, i32 %21
  %22 = load float, float* %arrayidx9, align 4
  %add = fadd float %22, %mul
  store float %add, float* %arrayidx9, align 4
  %23 = load %struct.Physics_States*, %struct.Physics_States** @physics_states, align 4
  %y_speed = getelementptr inbounds %struct.Physics_States, %struct.Physics_States* %23, i32 0, i32 7
  %24 = load float*, float** %y_speed, align 4
  %25 = load i32, i32* %i, align 4
  %arrayidx10 = getelementptr inbounds float, float* %24, i32 %25
  %26 = load float, float* %arrayidx10, align 4
  %27 = load float, float* %delta_iter, align 4
  %mul11 = fmul float %26, %27
  %28 = load %struct.Physics_States*, %struct.Physics_States** @physics_states, align 4
  %y = getelementptr inbounds %struct.Physics_States, %struct.Physics_States* %28, i32 0, i32 5
  %29 = load float*, float** %y, align 4
  %30 = load i32, i32* %i, align 4
  %arrayidx12 = getelementptr inbounds float, float* %29, i32 %30
  %31 = load float, float* %arrayidx12, align 4
  %add13 = fadd float %31, %mul11
  store float %add13, float* %arrayidx12, align 4
  br label %if.end

if.end:                                           ; preds = %if.then7, %land.lhs.true
  br label %if.end14

if.end14:                                         ; preds = %if.end, %for.body3
  br label %for.inc

for.inc:                                          ; preds = %if.end14
  %32 = load i32, i32* %i, align 4
  %add15 = add i32 %32, 1
  store i32 %add15, i32* %i, align 4
  br label %for.cond1

for.end:                                          ; preds = %for.cond1
  br label %for.inc16

for.inc16:                                        ; preds = %for.end
  %33 = load i32, i32* %iter, align 4
  %add17 = add i32 %33, 1
  store i32 %add17, i32* %iter, align 4
  br label %for.cond

for.end18:                                        ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @step_collision_resolve(float %delta) #0 {
entry:
  %delta.addr = alloca float, align 4
  %i = alloca i32, align 4
  %entity_id = alloca i32, align 4
  %entity_id_2 = alloca i32, align 4
  %is_enemy = alloca i8, align 1
  %with_player = alloca i8, align 1
  %proximity_attack_id = alloca i32, align 4
  %proximity_attack_state = alloca float, align 4
  %sprite_map_id = alloca i32, align 4
  %proximity_attack_damage = alloca float, align 4
  store float %delta, float* %delta.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load %struct.Collision_Table*, %struct.Collision_Table** @collision_table, align 4
  %curr_max = getelementptr inbounds %struct.Collision_Table, %struct.Collision_Table* %1, i32 0, i32 2
  %2 = load i32, i32* %curr_max, align 4
  %cmp = icmp ult i32 %0, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %struct.Collision_Table*, %struct.Collision_Table** @collision_table, align 4
  %entity_id1 = getelementptr inbounds %struct.Collision_Table, %struct.Collision_Table* %3, i32 0, i32 3
  %4 = load i32*, i32** %entity_id1, align 4
  %5 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %4, i32 %5
  %6 = load i32, i32* %arrayidx, align 4
  store i32 %6, i32* %entity_id, align 4
  %7 = load %struct.Collision_Table*, %struct.Collision_Table** @collision_table, align 4
  %entity_id_22 = getelementptr inbounds %struct.Collision_Table, %struct.Collision_Table* %7, i32 0, i32 4
  %8 = load i32*, i32** %entity_id_22, align 4
  %9 = load i32, i32* %i, align 4
  %arrayidx3 = getelementptr inbounds i32, i32* %8, i32 %9
  %10 = load i32, i32* %arrayidx3, align 4
  store i32 %10, i32* %entity_id_2, align 4
  %11 = load %struct.AI_Enemy*, %struct.AI_Enemy** @ai_enemy, align 4
  %12 = bitcast %struct.AI_Enemy* %11 to i8*
  %13 = load i32, i32* %entity_id, align 4
  %call = call i32 @find_item_index(i8* %12, i32 %13)
  %14 = load %struct.AI_Enemy*, %struct.AI_Enemy** @ai_enemy, align 4
  %curr_max4 = getelementptr inbounds %struct.AI_Enemy, %struct.AI_Enemy* %14, i32 0, i32 2
  %15 = load i32, i32* %curr_max4, align 4
  %cmp5 = icmp ult i32 %call, %15
  %conv = zext i1 %cmp5 to i32
  %conv6 = trunc i32 %conv to i8
  store i8 %conv6, i8* %is_enemy, align 1
  %16 = load i32, i32* %entity_id_2, align 4
  %cmp7 = icmp eq i32 %16, 0
  %conv8 = zext i1 %cmp7 to i32
  %conv9 = trunc i32 %conv8 to i8
  store i8 %conv9, i8* %with_player, align 1
  %17 = load i8, i8* %is_enemy, align 1
  %conv10 = zext i8 %17 to i32
  %tobool = icmp ne i32 %conv10, 0
  br i1 %tobool, label %land.lhs.true, label %if.end31

land.lhs.true:                                    ; preds = %for.body
  %18 = load i8, i8* %with_player, align 1
  %conv11 = zext i8 %18 to i32
  %tobool12 = icmp ne i32 %conv11, 0
  br i1 %tobool12, label %if.then, label %if.end31

if.then:                                          ; preds = %land.lhs.true
  %19 = load %struct.Proximity_Attack*, %struct.Proximity_Attack** @proximity_attack, align 4
  %20 = bitcast %struct.Proximity_Attack* %19 to i8*
  %21 = load i32, i32* %entity_id, align 4
  %call13 = call i32 @find_item_index(i8* %20, i32 %21)
  store i32 %call13, i32* %proximity_attack_id, align 4
  %22 = load %struct.Proximity_Attack*, %struct.Proximity_Attack** @proximity_attack, align 4
  %attack_state = getelementptr inbounds %struct.Proximity_Attack, %struct.Proximity_Attack* %22, i32 0, i32 4
  %23 = load float*, float** %attack_state, align 4
  %24 = load i32, i32* %proximity_attack_id, align 4
  %arrayidx14 = getelementptr inbounds float, float* %23, i32 %24
  %25 = load float, float* %arrayidx14, align 4
  store float %25, float* %proximity_attack_state, align 4
  %26 = load float, float* %proximity_attack_state, align 4
  %cmp15 = fcmp olt float %26, 0.000000e+00
  br i1 %cmp15, label %if.then17, label %if.end30

if.then17:                                        ; preds = %if.then
  %call18 = call i32 @add_hit_feedback_item(i32 0, float 1.000000e+02)
  %27 = load %struct.Sprite_Map*, %struct.Sprite_Map** @sprite_map, align 4
  %28 = bitcast %struct.Sprite_Map* %27 to i8*
  %29 = load i32, i32* %entity_id, align 4
  %call19 = call i32 @find_item_index(i8* %28, i32 %29)
  store i32 %call19, i32* %sprite_map_id, align 4
  %30 = load %struct.Sprite_Map*, %struct.Sprite_Map** @sprite_map, align 4
  %sprite_variant = getelementptr inbounds %struct.Sprite_Map, %struct.Sprite_Map* %30, i32 0, i32 8
  %31 = load i8*, i8** %sprite_variant, align 4
  %32 = load i32, i32* %sprite_map_id, align 4
  %arrayidx20 = getelementptr inbounds i8, i8* %31, i32 %32
  store i8 0, i8* %arrayidx20, align 1
  %33 = load %struct.Proximity_Attack*, %struct.Proximity_Attack** @proximity_attack, align 4
  %damage = getelementptr inbounds %struct.Proximity_Attack, %struct.Proximity_Attack* %33, i32 0, i32 5
  %34 = load float*, float** %damage, align 4
  %35 = load i32, i32* %proximity_attack_id, align 4
  %arrayidx21 = getelementptr inbounds float, float* %34, i32 %35
  %36 = load float, float* %arrayidx21, align 4
  store float %36, float* %proximity_attack_damage, align 4
  %37 = load %struct.Proximity_Attack*, %struct.Proximity_Attack** @proximity_attack, align 4
  %attack_state22 = getelementptr inbounds %struct.Proximity_Attack, %struct.Proximity_Attack* %37, i32 0, i32 4
  %38 = load float*, float** %attack_state22, align 4
  %39 = load i32, i32* %proximity_attack_id, align 4
  %arrayidx23 = getelementptr inbounds float, float* %38, i32 %39
  store float 1.000000e+02, float* %arrayidx23, align 4
  %40 = load %struct.Health_Table*, %struct.Health_Table** @health_table, align 4
  %health_points = getelementptr inbounds %struct.Health_Table, %struct.Health_Table* %40, i32 0, i32 4
  %41 = load float*, float** %health_points, align 4
  %arrayidx24 = getelementptr inbounds float, float* %41, i32 0
  %42 = load float, float* %arrayidx24, align 4
  %cmp25 = fcmp oge float %42, 0.000000e+00
  br i1 %cmp25, label %if.then27, label %if.end

if.then27:                                        ; preds = %if.then17
  %43 = load float, float* %proximity_attack_damage, align 4
  %44 = load %struct.Health_Table*, %struct.Health_Table** @health_table, align 4
  %health_points28 = getelementptr inbounds %struct.Health_Table, %struct.Health_Table* %44, i32 0, i32 4
  %45 = load float*, float** %health_points28, align 4
  %arrayidx29 = getelementptr inbounds float, float* %45, i32 0
  %46 = load float, float* %arrayidx29, align 4
  %sub = fsub float %46, %43
  store float %sub, float* %arrayidx29, align 4
  br label %if.end

if.end:                                           ; preds = %if.then27, %if.then17
  br label %if.end30

if.end30:                                         ; preds = %if.end, %if.then
  br label %if.end31

if.end31:                                         ; preds = %if.end30, %land.lhs.true, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end31
  %47 = load i32, i32* %i, align 4
  %add = add i32 %47, 1
  store i32 %add, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @step_ai_enemy(float %delta) #0 {
entry:
  %delta.addr = alloca float, align 4
  %delta_iter = alloca float, align 4
  %player_x = alloca float, align 4
  %player_y = alloca float, align 4
  %iter = alloca i32, align 4
  %i = alloca i32, align 4
  %entity_id = alloca i32, align 4
  %enemy_type = alloca i8, align 1
  %health_id = alloca i32, align 4
  %health_points = alloca float, align 4
  %physics_id = alloca i32, align 4
  %x24 = alloca float, align 4
  %y27 = alloca float, align 4
  %player_dx = alloca float, align 4
  %player_dy = alloca float, align 4
  %player_distance = alloca float, align 4
  %player_dir_x = alloca float, align 4
  %player_dir_y = alloca float, align 4
  %player_angle = alloca float, align 4
  %j = alloca i32, align 4
  %j_entity_id = alloca i32, align 4
  %j_physics_id = alloca i32, align 4
  %j_x = alloca float, align 4
  %j_y = alloca float, align 4
  %dx = alloca float, align 4
  %dy = alloca float, align 4
  %distance = alloca float, align 4
  %distance_diff = alloca float, align 4
  store float %delta, float* %delta.addr, align 4
  %0 = load float, float* %delta.addr, align 4
  %div = fdiv float %0, 3.000000e+00
  store float %div, float* %delta_iter, align 4
  %1 = load %struct.Physics_States*, %struct.Physics_States** @physics_states, align 4
  %x = getelementptr inbounds %struct.Physics_States, %struct.Physics_States* %1, i32 0, i32 4
  %2 = load float*, float** %x, align 4
  %arrayidx = getelementptr inbounds float, float* %2, i32 0
  %3 = load float, float* %arrayidx, align 4
  store float %3, float* %player_x, align 4
  %4 = load %struct.Physics_States*, %struct.Physics_States** @physics_states, align 4
  %y = getelementptr inbounds %struct.Physics_States, %struct.Physics_States* %4, i32 0, i32 5
  %5 = load float*, float** %y, align 4
  %arrayidx1 = getelementptr inbounds float, float* %5, i32 0
  %6 = load float, float* %arrayidx1, align 4
  store float %6, float* %player_y, align 4
  store i32 0, i32* %iter, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc88, %entry
  %7 = load i32, i32* %iter, align 4
  %cmp = icmp ult i32 %7, 3
  br i1 %cmp, label %for.body, label %for.end90

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %i, align 4
  br label %for.cond2

for.cond2:                                        ; preds = %for.inc85, %for.body
  %8 = load i32, i32* %i, align 4
  %9 = load %struct.AI_Enemy*, %struct.AI_Enemy** @ai_enemy, align 4
  %curr_max = getelementptr inbounds %struct.AI_Enemy, %struct.AI_Enemy* %9, i32 0, i32 2
  %10 = load i32, i32* %curr_max, align 4
  %cmp3 = icmp ult i32 %8, %10
  br i1 %cmp3, label %for.body4, label %for.end87

for.body4:                                        ; preds = %for.cond2
  %11 = load %struct.AI_Enemy*, %struct.AI_Enemy** @ai_enemy, align 4
  %used = getelementptr inbounds %struct.AI_Enemy, %struct.AI_Enemy* %11, i32 0, i32 1
  %12 = load i8*, i8** %used, align 4
  %13 = load i32, i32* %i, align 4
  %arrayidx5 = getelementptr inbounds i8, i8* %12, i32 %13
  %14 = load i8, i8* %arrayidx5, align 1
  %tobool = icmp ne i8 %14, 0
  br i1 %tobool, label %if.then, label %if.end84

if.then:                                          ; preds = %for.body4
  %15 = load %struct.AI_Enemy*, %struct.AI_Enemy** @ai_enemy, align 4
  %entity_id6 = getelementptr inbounds %struct.AI_Enemy, %struct.AI_Enemy* %15, i32 0, i32 3
  %16 = load i32*, i32** %entity_id6, align 4
  %17 = load i32, i32* %i, align 4
  %arrayidx7 = getelementptr inbounds i32, i32* %16, i32 %17
  %18 = load i32, i32* %arrayidx7, align 4
  store i32 %18, i32* %entity_id, align 4
  %19 = load %struct.AI_Enemy*, %struct.AI_Enemy** @ai_enemy, align 4
  %enemy_type8 = getelementptr inbounds %struct.AI_Enemy, %struct.AI_Enemy* %19, i32 0, i32 4
  %20 = load i8*, i8** %enemy_type8, align 4
  %21 = load i32, i32* %i, align 4
  %arrayidx9 = getelementptr inbounds i8, i8* %20, i32 %21
  %22 = load i8, i8* %arrayidx9, align 1
  store i8 %22, i8* %enemy_type, align 1
  %23 = load %struct.Health_Table*, %struct.Health_Table** @health_table, align 4
  %24 = bitcast %struct.Health_Table* %23 to i8*
  %25 = load i32, i32* %entity_id, align 4
  %call = call i32 @find_item_index(i8* %24, i32 %25)
  store i32 %call, i32* %health_id, align 4
  %26 = load %struct.Health_Table*, %struct.Health_Table** @health_table, align 4
  %health_points10 = getelementptr inbounds %struct.Health_Table, %struct.Health_Table* %26, i32 0, i32 4
  %27 = load float*, float** %health_points10, align 4
  %28 = load i32, i32* %health_id, align 4
  %arrayidx11 = getelementptr inbounds float, float* %27, i32 %28
  %29 = load float, float* %arrayidx11, align 4
  store float %29, float* %health_points, align 4
  %30 = load float, float* %health_points, align 4
  %conv = fpext float %30 to double
  %cmp12 = fcmp olt double %conv, 1.000000e-01
  br i1 %cmp12, label %if.then14, label %if.end22

if.then14:                                        ; preds = %if.then
  %31 = load i8, i8* %enemy_type, align 1
  %conv15 = zext i8 %31 to i32
  %cmp16 = icmp eq i32 %conv15, 0
  br i1 %cmp16, label %if.then18, label %if.end

if.then18:                                        ; preds = %if.then14
  %32 = load i32, i32* %entity_id, align 4
  call void @destroy_zombie(i32 %32)
  %33 = load i32, i32* @curr_wave, align 4
  %mul = mul i32 200, %33
  %34 = load i32, i32* @score, align 4
  %add = add i32 %34, %mul
  store i32 %add, i32* @score, align 4
  br label %if.end

if.end:                                           ; preds = %if.then18, %if.then14
  %35 = load i8, i8* %enemy_type, align 1
  %idxprom = zext i8 %35 to i32
  %arrayidx19 = getelementptr inbounds [4 x i8], [4 x i8]* getelementptr inbounds (%struct.Wave_Completion, %struct.Wave_Completion* @wave_completion, i32 0, i32 0), i32 0, i32 %idxprom
  %36 = load i8, i8* %arrayidx19, align 1
  %conv20 = zext i8 %36 to i32
  %sub = sub nsw i32 %conv20, 1
  %conv21 = trunc i32 %sub to i8
  store i8 %conv21, i8* %arrayidx19, align 1
  br label %for.end87

if.end22:                                         ; preds = %if.then
  %37 = load %struct.Physics_States*, %struct.Physics_States** @physics_states, align 4
  %38 = bitcast %struct.Physics_States* %37 to i8*
  %39 = load i32, i32* %entity_id, align 4
  %call23 = call i32 @find_item_index(i8* %38, i32 %39)
  store i32 %call23, i32* %physics_id, align 4
  %40 = load %struct.Physics_States*, %struct.Physics_States** @physics_states, align 4
  %x25 = getelementptr inbounds %struct.Physics_States, %struct.Physics_States* %40, i32 0, i32 4
  %41 = load float*, float** %x25, align 4
  %42 = load i32, i32* %physics_id, align 4
  %arrayidx26 = getelementptr inbounds float, float* %41, i32 %42
  %43 = load float, float* %arrayidx26, align 4
  store float %43, float* %x24, align 4
  %44 = load %struct.Physics_States*, %struct.Physics_States** @physics_states, align 4
  %y28 = getelementptr inbounds %struct.Physics_States, %struct.Physics_States* %44, i32 0, i32 5
  %45 = load float*, float** %y28, align 4
  %46 = load i32, i32* %physics_id, align 4
  %arrayidx29 = getelementptr inbounds float, float* %45, i32 %46
  %47 = load float, float* %arrayidx29, align 4
  store float %47, float* %y27, align 4
  %48 = load float, float* %player_x, align 4
  %49 = load float, float* %player_y, align 4
  %50 = load float, float* %x24, align 4
  %51 = load float, float* %y27, align 4
  call void @get_angle_to_point(float %48, float %49, float %50, float %51, float* %player_dx, float* %player_dy, float* %player_distance, float* %player_dir_x, float* %player_dir_y, float* %player_angle)
  %52 = load float, float* %player_dir_x, align 4
  %fneg = fneg float %52
  %mul30 = fmul float %fneg, 2.000000e+02
  %conv31 = fpext float %mul30 to double
  %call32 = call float @timespec_to_float(%struct.timespec* @curr_time)
  %mul33 = fmul float %call32, 5.000000e+00
  %conv34 = fpext float %mul33 to double
  %53 = call double @llvm.sin.f64(double %conv34)
  %54 = call double @llvm.fabs.f64(double %53)
  %mul35 = fmul double %conv31, %54
  %conv36 = fptrunc double %mul35 to float
  %55 = load %struct.Physics_States*, %struct.Physics_States** @physics_states, align 4
  %x_speed = getelementptr inbounds %struct.Physics_States, %struct.Physics_States* %55, i32 0, i32 6
  %56 = load float*, float** %x_speed, align 4
  %57 = load i32, i32* %physics_id, align 4
  %arrayidx37 = getelementptr inbounds float, float* %56, i32 %57
  store float %conv36, float* %arrayidx37, align 4
  %58 = load float, float* %player_dir_y, align 4
  %fneg38 = fneg float %58
  %mul39 = fmul float %fneg38, 2.000000e+02
  %conv40 = fpext float %mul39 to double
  %call41 = call float @timespec_to_float(%struct.timespec* @curr_time)
  %mul42 = fmul float %call41, 5.000000e+00
  %conv43 = fpext float %mul42 to double
  %59 = call double @llvm.sin.f64(double %conv43)
  %60 = call double @llvm.fabs.f64(double %59)
  %mul44 = fmul double %conv40, %60
  %conv45 = fptrunc double %mul44 to float
  %61 = load %struct.Physics_States*, %struct.Physics_States** @physics_states, align 4
  %y_speed = getelementptr inbounds %struct.Physics_States, %struct.Physics_States* %61, i32 0, i32 7
  %62 = load float*, float** %y_speed, align 4
  %63 = load i32, i32* %physics_id, align 4
  %arrayidx46 = getelementptr inbounds float, float* %62, i32 %63
  store float %conv45, float* %arrayidx46, align 4
  %64 = load float, float* %player_angle, align 4
  %65 = load %struct.Physics_States*, %struct.Physics_States** @physics_states, align 4
  %angle = getelementptr inbounds %struct.Physics_States, %struct.Physics_States* %65, i32 0, i32 8
  %66 = load float*, float** %angle, align 4
  %67 = load i32, i32* %physics_id, align 4
  %arrayidx47 = getelementptr inbounds float, float* %66, i32 %67
  store float %64, float* %arrayidx47, align 4
  store i32 0, i32* %j, align 4
  br label %for.cond48

for.cond48:                                       ; preds = %for.inc, %if.end22
  %68 = load i32, i32* %j, align 4
  %69 = load %struct.AI_Enemy*, %struct.AI_Enemy** @ai_enemy, align 4
  %curr_max49 = getelementptr inbounds %struct.AI_Enemy, %struct.AI_Enemy* %69, i32 0, i32 2
  %70 = load i32, i32* %curr_max49, align 4
  %cmp50 = icmp ult i32 %68, %70
  br i1 %cmp50, label %for.body52, label %for.end

for.body52:                                       ; preds = %for.cond48
  %71 = load i32, i32* %j, align 4
  %72 = load i32, i32* %i, align 4
  %cmp53 = icmp ne i32 %71, %72
  br i1 %cmp53, label %land.lhs.true, label %if.end82

land.lhs.true:                                    ; preds = %for.body52
  %73 = load %struct.AI_Enemy*, %struct.AI_Enemy** @ai_enemy, align 4
  %used55 = getelementptr inbounds %struct.AI_Enemy, %struct.AI_Enemy* %73, i32 0, i32 1
  %74 = load i8*, i8** %used55, align 4
  %75 = load i32, i32* %j, align 4
  %arrayidx56 = getelementptr inbounds i8, i8* %74, i32 %75
  %76 = load i8, i8* %arrayidx56, align 1
  %conv57 = zext i8 %76 to i32
  %tobool58 = icmp ne i32 %conv57, 0
  br i1 %tobool58, label %if.then59, label %if.end82

if.then59:                                        ; preds = %land.lhs.true
  %77 = load %struct.AI_Enemy*, %struct.AI_Enemy** @ai_enemy, align 4
  %entity_id60 = getelementptr inbounds %struct.AI_Enemy, %struct.AI_Enemy* %77, i32 0, i32 3
  %78 = load i32*, i32** %entity_id60, align 4
  %79 = load i32, i32* %j, align 4
  %arrayidx61 = getelementptr inbounds i32, i32* %78, i32 %79
  %80 = load i32, i32* %arrayidx61, align 4
  store i32 %80, i32* %j_entity_id, align 4
  %81 = load %struct.Physics_States*, %struct.Physics_States** @physics_states, align 4
  %82 = bitcast %struct.Physics_States* %81 to i8*
  %83 = load i32, i32* %j_entity_id, align 4
  %call62 = call i32 @find_item_index(i8* %82, i32 %83)
  store i32 %call62, i32* %j_physics_id, align 4
  %84 = load %struct.Physics_States*, %struct.Physics_States** @physics_states, align 4
  %x63 = getelementptr inbounds %struct.Physics_States, %struct.Physics_States* %84, i32 0, i32 4
  %85 = load float*, float** %x63, align 4
  %86 = load i32, i32* %j_physics_id, align 4
  %arrayidx64 = getelementptr inbounds float, float* %85, i32 %86
  %87 = load float, float* %arrayidx64, align 4
  store float %87, float* %j_x, align 4
  %88 = load %struct.Physics_States*, %struct.Physics_States** @physics_states, align 4
  %y65 = getelementptr inbounds %struct.Physics_States, %struct.Physics_States* %88, i32 0, i32 5
  %89 = load float*, float** %y65, align 4
  %90 = load i32, i32* %j_physics_id, align 4
  %arrayidx66 = getelementptr inbounds float, float* %89, i32 %90
  %91 = load float, float* %arrayidx66, align 4
  store float %91, float* %j_y, align 4
  %92 = load float, float* %x24, align 4
  %93 = load float, float* %y27, align 4
  %94 = load float, float* %j_x, align 4
  %95 = load float, float* %j_y, align 4
  call void @get_distance_to_point(float %92, float %93, float %94, float %95, float* %dx, float* %dy, float* %distance)
  %96 = load float, float* %distance, align 4
  %sub67 = fsub float %96, 4.000000e+01
  store float %sub67, float* %distance_diff, align 4
  %97 = load float, float* %distance_diff, align 4
  %cmp68 = fcmp olt float %97, 0.000000e+00
  br i1 %cmp68, label %if.then70, label %if.end81

if.then70:                                        ; preds = %if.then59
  %98 = load float, float* %distance_diff, align 4
  %99 = load float, float* %dx, align 4
  %mul71 = fmul float %98, %99
  %100 = load float, float* %delta_iter, align 4
  %mul72 = fmul float %mul71, %100
  %101 = load %struct.Physics_States*, %struct.Physics_States** @physics_states, align 4
  %x73 = getelementptr inbounds %struct.Physics_States, %struct.Physics_States* %101, i32 0, i32 4
  %102 = load float*, float** %x73, align 4
  %103 = load i32, i32* %physics_id, align 4
  %arrayidx74 = getelementptr inbounds float, float* %102, i32 %103
  %104 = load float, float* %arrayidx74, align 4
  %add75 = fadd float %104, %mul72
  store float %add75, float* %arrayidx74, align 4
  %105 = load float, float* %distance_diff, align 4
  %106 = load float, float* %dy, align 4
  %mul76 = fmul float %105, %106
  %107 = load float, float* %delta_iter, align 4
  %mul77 = fmul float %mul76, %107
  %108 = load %struct.Physics_States*, %struct.Physics_States** @physics_states, align 4
  %y78 = getelementptr inbounds %struct.Physics_States, %struct.Physics_States* %108, i32 0, i32 5
  %109 = load float*, float** %y78, align 4
  %110 = load i32, i32* %physics_id, align 4
  %arrayidx79 = getelementptr inbounds float, float* %109, i32 %110
  %111 = load float, float* %arrayidx79, align 4
  %add80 = fadd float %111, %mul77
  store float %add80, float* %arrayidx79, align 4
  br label %if.end81

if.end81:                                         ; preds = %if.then70, %if.then59
  br label %if.end82

if.end82:                                         ; preds = %if.end81, %land.lhs.true, %for.body52
  br label %for.inc

for.inc:                                          ; preds = %if.end82
  %112 = load i32, i32* %j, align 4
  %add83 = add i32 %112, 1
  store i32 %add83, i32* %j, align 4
  br label %for.cond48

for.end:                                          ; preds = %for.cond48
  br label %if.end84

if.end84:                                         ; preds = %for.end, %for.body4
  br label %for.inc85

for.inc85:                                        ; preds = %if.end84
  %113 = load i32, i32* %i, align 4
  %add86 = add i32 %113, 1
  store i32 %add86, i32* %i, align 4
  br label %for.cond2

for.end87:                                        ; preds = %if.end, %for.cond2
  br label %for.inc88

for.inc88:                                        ; preds = %for.end87
  %114 = load i32, i32* %iter, align 4
  %add89 = add i32 %114, 1
  store i32 %add89, i32* %iter, align 4
  br label %for.cond

for.end90:                                        ; preds = %for.cond
  ret void
}

; Function Attrs: nounwind readnone speculatable willreturn
declare double @llvm.sin.f64(double) #3

; Function Attrs: nounwind readnone speculatable willreturn
declare double @llvm.fabs.f64(double) #3

; Function Attrs: noinline nounwind optnone
define hidden void @step_proximity_attack(float %delta) #0 {
entry:
  %delta.addr = alloca float, align 4
  %i = alloca i32, align 4
  %entity_id = alloca i32, align 4
  %sprite_map_id = alloca i32, align 4
  store float %delta, float* %delta.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load %struct.Proximity_Attack*, %struct.Proximity_Attack** @proximity_attack, align 4
  %curr_max = getelementptr inbounds %struct.Proximity_Attack, %struct.Proximity_Attack* %1, i32 0, i32 2
  %2 = load i32, i32* %curr_max, align 4
  %cmp = icmp ult i32 %0, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %struct.Proximity_Attack*, %struct.Proximity_Attack** @proximity_attack, align 4
  %used = getelementptr inbounds %struct.Proximity_Attack, %struct.Proximity_Attack* %3, i32 0, i32 1
  %4 = load i8*, i8** %used, align 4
  %5 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i8, i8* %4, i32 %5
  %6 = load i8, i8* %arrayidx, align 1
  %tobool = icmp ne i8 %6, 0
  br i1 %tobool, label %if.then, label %if.end14

if.then:                                          ; preds = %for.body
  %7 = load %struct.Proximity_Attack*, %struct.Proximity_Attack** @proximity_attack, align 4
  %attack_state = getelementptr inbounds %struct.Proximity_Attack, %struct.Proximity_Attack* %7, i32 0, i32 4
  %8 = load float*, float** %attack_state, align 4
  %9 = load i32, i32* %i, align 4
  %arrayidx1 = getelementptr inbounds float, float* %8, i32 %9
  %10 = load float, float* %arrayidx1, align 4
  %cmp2 = fcmp ogt float %10, 0.000000e+00
  br i1 %cmp2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %11 = load float, float* %delta.addr, align 4
  %mul = fmul float 1.000000e+02, %11
  %12 = load %struct.Proximity_Attack*, %struct.Proximity_Attack** @proximity_attack, align 4
  %attack_state4 = getelementptr inbounds %struct.Proximity_Attack, %struct.Proximity_Attack* %12, i32 0, i32 4
  %13 = load float*, float** %attack_state4, align 4
  %14 = load i32, i32* %i, align 4
  %arrayidx5 = getelementptr inbounds float, float* %13, i32 %14
  %15 = load float, float* %arrayidx5, align 4
  %sub = fsub float %15, %mul
  store float %sub, float* %arrayidx5, align 4
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %16 = load %struct.Proximity_Attack*, %struct.Proximity_Attack** @proximity_attack, align 4
  %attack_state6 = getelementptr inbounds %struct.Proximity_Attack, %struct.Proximity_Attack* %16, i32 0, i32 4
  %17 = load float*, float** %attack_state6, align 4
  %18 = load i32, i32* %i, align 4
  %arrayidx7 = getelementptr inbounds float, float* %17, i32 %18
  %19 = load float, float* %arrayidx7, align 4
  %cmp8 = fcmp olt float %19, 2.000000e+01
  br i1 %cmp8, label %if.then9, label %if.end13

if.then9:                                         ; preds = %if.end
  %20 = load %struct.Proximity_Attack*, %struct.Proximity_Attack** @proximity_attack, align 4
  %entity_id10 = getelementptr inbounds %struct.Proximity_Attack, %struct.Proximity_Attack* %20, i32 0, i32 3
  %21 = load i32*, i32** %entity_id10, align 4
  %22 = load i32, i32* %i, align 4
  %arrayidx11 = getelementptr inbounds i32, i32* %21, i32 %22
  %23 = load i32, i32* %arrayidx11, align 4
  store i32 %23, i32* %entity_id, align 4
  %24 = load %struct.Sprite_Map*, %struct.Sprite_Map** @sprite_map, align 4
  %25 = bitcast %struct.Sprite_Map* %24 to i8*
  %26 = load i32, i32* %entity_id, align 4
  %call = call i32 @find_item_index(i8* %25, i32 %26)
  store i32 %call, i32* %sprite_map_id, align 4
  %27 = load %struct.Sprite_Map*, %struct.Sprite_Map** @sprite_map, align 4
  %sprite_variant = getelementptr inbounds %struct.Sprite_Map, %struct.Sprite_Map* %27, i32 0, i32 8
  %28 = load i8*, i8** %sprite_variant, align 4
  %29 = load i32, i32* %sprite_map_id, align 4
  %arrayidx12 = getelementptr inbounds i8, i8* %28, i32 %29
  store i8 2, i8* %arrayidx12, align 1
  br label %if.end13

if.end13:                                         ; preds = %if.then9, %if.end
  br label %if.end14

if.end14:                                         ; preds = %if.end13, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end14
  %30 = load i32, i32* %i, align 4
  %add = add i32 %30, 1
  store i32 %add, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @step_hit_feedback_table(float %delta) #0 {
entry:
  %delta.addr = alloca float, align 4
  %i = alloca i32, align 4
  %entity_id = alloca i32, align 4
  %sprite_map_id = alloca i32, align 4
  %health_id = alloca i32, align 4
  %health_points = alloca float, align 4
  store float %delta, float* %delta.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load %struct.Hit_Feedback_Table*, %struct.Hit_Feedback_Table** @hit_feedback_table, align 4
  %curr_max = getelementptr inbounds %struct.Hit_Feedback_Table, %struct.Hit_Feedback_Table* %1, i32 0, i32 2
  %2 = load i32, i32* %curr_max, align 4
  %cmp = icmp ult i32 %0, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load float, float* %delta.addr, align 4
  %mul = fmul float 4.000000e+02, %3
  %4 = load %struct.Hit_Feedback_Table*, %struct.Hit_Feedback_Table** @hit_feedback_table, align 4
  %amount = getelementptr inbounds %struct.Hit_Feedback_Table, %struct.Hit_Feedback_Table* %4, i32 0, i32 4
  %5 = load float*, float** %amount, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds float, float* %5, i32 %6
  %7 = load float, float* %arrayidx, align 4
  %sub = fsub float %7, %mul
  store float %sub, float* %arrayidx, align 4
  %8 = load %struct.Hit_Feedback_Table*, %struct.Hit_Feedback_Table** @hit_feedback_table, align 4
  %entity_id1 = getelementptr inbounds %struct.Hit_Feedback_Table, %struct.Hit_Feedback_Table* %8, i32 0, i32 3
  %9 = load i32*, i32** %entity_id1, align 4
  %10 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds i32, i32* %9, i32 %10
  %11 = load i32, i32* %arrayidx2, align 4
  store i32 %11, i32* %entity_id, align 4
  %12 = load %struct.Sprite_Map*, %struct.Sprite_Map** @sprite_map, align 4
  %13 = bitcast %struct.Sprite_Map* %12 to i8*
  %14 = load i32, i32* %entity_id, align 4
  %call = call i32 @find_item_index(i8* %13, i32 %14)
  store i32 %call, i32* %sprite_map_id, align 4
  %15 = load %struct.Hit_Feedback_Table*, %struct.Hit_Feedback_Table** @hit_feedback_table, align 4
  %amount3 = getelementptr inbounds %struct.Hit_Feedback_Table, %struct.Hit_Feedback_Table* %15, i32 0, i32 4
  %16 = load float*, float** %amount3, align 4
  %17 = load i32, i32* %i, align 4
  %arrayidx4 = getelementptr inbounds float, float* %16, i32 %17
  %18 = load float, float* %arrayidx4, align 4
  %cmp5 = fcmp olt float %18, 0.000000e+00
  br i1 %cmp5, label %if.then, label %if.else

if.then:                                          ; preds = %for.body
  %19 = load %struct.Hit_Feedback_Table*, %struct.Hit_Feedback_Table** @hit_feedback_table, align 4
  %20 = bitcast %struct.Hit_Feedback_Table* %19 to i8*
  %21 = load %struct.Hit_Feedback_Table*, %struct.Hit_Feedback_Table** @hit_feedback_table, align 4
  %entity_id6 = getelementptr inbounds %struct.Hit_Feedback_Table, %struct.Hit_Feedback_Table* %21, i32 0, i32 3
  %22 = load i32*, i32** %entity_id6, align 4
  %23 = load i32, i32* %i, align 4
  %arrayidx7 = getelementptr inbounds i32, i32* %22, i32 %23
  %24 = load i32, i32* %arrayidx7, align 4
  call void @remove_table_item(i8* %20, i32 %24)
  %25 = load %struct.Sprite_Map*, %struct.Sprite_Map** @sprite_map, align 4
  %26 = bitcast %struct.Sprite_Map* %25 to i8*
  %27 = load i32, i32* %entity_id, align 4
  %call8 = call i32 @find_item_index(i8* %26, i32 %27)
  store i32 %call8, i32* %health_id, align 4
  %28 = load %struct.Health_Table*, %struct.Health_Table** @health_table, align 4
  %health_points9 = getelementptr inbounds %struct.Health_Table, %struct.Health_Table* %28, i32 0, i32 4
  %29 = load float*, float** %health_points9, align 4
  %30 = load i32, i32* %health_id, align 4
  %arrayidx10 = getelementptr inbounds float, float* %29, i32 %30
  %31 = load float, float* %arrayidx10, align 4
  store float %31, float* %health_points, align 4
  %32 = load float, float* %health_points, align 4
  %cmp11 = fcmp ogt float %32, 0.000000e+00
  br i1 %cmp11, label %if.then12, label %if.end

if.then12:                                        ; preds = %if.then
  %33 = load %struct.Sprite_Map*, %struct.Sprite_Map** @sprite_map, align 4
  %sprite_variant = getelementptr inbounds %struct.Sprite_Map, %struct.Sprite_Map* %33, i32 0, i32 8
  %34 = load i8*, i8** %sprite_variant, align 4
  %35 = load i32, i32* %sprite_map_id, align 4
  %arrayidx13 = getelementptr inbounds i8, i8* %34, i32 %35
  store i8 0, i8* %arrayidx13, align 1
  br label %if.end

if.end:                                           ; preds = %if.then12, %if.then
  br label %if.end16

if.else:                                          ; preds = %for.body
  %36 = load %struct.Sprite_Map*, %struct.Sprite_Map** @sprite_map, align 4
  %sprite_variant14 = getelementptr inbounds %struct.Sprite_Map, %struct.Sprite_Map* %36, i32 0, i32 8
  %37 = load i8*, i8** %sprite_variant14, align 4
  %38 = load i32, i32* %sprite_map_id, align 4
  %arrayidx15 = getelementptr inbounds i8, i8* %37, i32 %38
  store i8 1, i8* %arrayidx15, align 1
  br label %if.end16

if.end16:                                         ; preds = %if.else, %if.end
  br label %for.inc

for.inc:                                          ; preds = %if.end16
  %39 = load i32, i32* %i, align 4
  %add = add i32 %39, 1
  store i32 %add, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @step_wave_rest(float %delta) #0 {
entry:
  %delta.addr = alloca float, align 4
  store float %delta, float* %delta.addr, align 4
  %0 = load float, float* getelementptr inbounds (%struct.Wave_Rest, %struct.Wave_Rest* @wave_rest, i32 0, i32 0), align 4
  %cmp = fcmp ogt float %0, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.end3

if.then:                                          ; preds = %entry
  %1 = load float, float* %delta.addr, align 4
  %2 = load float, float* getelementptr inbounds (%struct.Wave_Rest, %struct.Wave_Rest* @wave_rest, i32 0, i32 0), align 4
  %sub = fsub float %2, %1
  store float %sub, float* getelementptr inbounds (%struct.Wave_Rest, %struct.Wave_Rest* @wave_rest, i32 0, i32 0), align 4
  %3 = load float, float* getelementptr inbounds (%struct.Wave_Rest, %struct.Wave_Rest* @wave_rest, i32 0, i32 0), align 4
  %cmp1 = fcmp olt float %3, 0.000000e+00
  br i1 %cmp1, label %if.then2, label %if.end

if.then2:                                         ; preds = %if.then
  %4 = load i32, i32* @curr_wave, align 4
  %add = add i32 %4, 1
  store i32 %add, i32* @curr_wave, align 4
  call void @start_wave()
  br label %if.end

if.end:                                           ; preds = %if.then2, %if.then
  br label %if.end3

if.end3:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @step_overlay_data(float %delta) #0 {
entry:
  %delta.addr = alloca float, align 4
  store float %delta, float* %delta.addr, align 4
  %0 = load %struct.Health_Table*, %struct.Health_Table** @health_table, align 4
  %health_points = getelementptr inbounds %struct.Health_Table, %struct.Health_Table* %0, i32 0, i32 4
  %1 = load float*, float** %health_points, align 4
  %arrayidx = getelementptr inbounds float, float* %1, i32 0
  %2 = load float, float* %arrayidx, align 4
  %cmp = fcmp olt float %2, 0.000000e+00
  %conv = zext i1 %cmp to i32
  %3 = load %struct.Overlay_Data*, %struct.Overlay_Data** @overlay_data, align 4
  %player_dead = getelementptr inbounds %struct.Overlay_Data, %struct.Overlay_Data* %3, i32 0, i32 0
  store i32 %conv, i32* %player_dead, align 4
  %4 = load %struct.Overlay_Data*, %struct.Overlay_Data** @overlay_data, align 4
  %wave_state = getelementptr inbounds %struct.Overlay_Data, %struct.Overlay_Data* %4, i32 0, i32 3
  %5 = load float, float* %wave_state, align 4
  %cmp1 = fcmp ogt float %5, 0.000000e+00
  br i1 %cmp1, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %6 = load float, float* %delta.addr, align 4
  %7 = load %struct.Overlay_Data*, %struct.Overlay_Data** @overlay_data, align 4
  %wave_state3 = getelementptr inbounds %struct.Overlay_Data, %struct.Overlay_Data* %7, i32 0, i32 3
  %8 = load float, float* %wave_state3, align 4
  %sub = fsub float %8, %6
  store float %sub, float* %wave_state3, align 4
  br label %if.end

if.else:                                          ; preds = %entry
  %9 = load %struct.Overlay_Data*, %struct.Overlay_Data** @overlay_data, align 4
  %wave_start = getelementptr inbounds %struct.Overlay_Data, %struct.Overlay_Data* %9, i32 0, i32 1
  store i32 0, i32* %wave_start, align 4
  %10 = load %struct.Overlay_Data*, %struct.Overlay_Data** @overlay_data, align 4
  %wave_end = getelementptr inbounds %struct.Overlay_Data, %struct.Overlay_Data* %10, i32 0, i32 2
  store i32 0, i32* %wave_end, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @step() #0 {
entry:
  %delta = alloca float, align 4
  %call = call float @step_time()
  store float %call, float* %delta, align 4
  call void @clear_collision_table()
  %0 = load float, float* %delta, align 4
  call void @step_physics(float %0)
  %1 = load float, float* %delta, align 4
  call void @step_collision_resolve(float %1)
  %2 = load float, float* %delta, align 4
  call void @step_proximity_attack(float %2)
  %3 = load float, float* %delta, align 4
  call void @step_hit_feedback_table(float %3)
  %4 = load float, float* %delta, align 4
  call void @step_weapon_states(float %4)
  %5 = load float, float* %delta, align 4
  call void @step_player(float %5)
  %6 = load float, float* %delta, align 4
  call void @step_ai_enemy(float %6)
  %7 = load float, float* %delta, align 4
  call void @step_bullets(float %7)
  call void @step_wave_emitter()
  %8 = load float, float* %delta, align 4
  call void @step_wave_rest(float %8)
  %9 = load float, float* getelementptr inbounds (%struct.Wave_Rest, %struct.Wave_Rest* @wave_rest, i32 0, i32 0), align 4
  %cmp = fcmp olt float %9, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @step_wave_completion()
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %10 = load float, float* %delta, align 4
  call void @step_overlay_data(float %10)
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @main(i32 %argc, i8** %argv) #0 {
entry:
  %argc.addr = alloca i32, align 4
  %argv.addr = alloca i8**, align 4
  store i32 %argc, i32* %argc.addr, align 4
  store i8** %argv, i8*** %argv.addr, align 4
  ret i32 0
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn }
attributes #3 = { nounwind readnone speculatable willreturn }
attributes #4 = { nounwind readnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nounwind readnone }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
