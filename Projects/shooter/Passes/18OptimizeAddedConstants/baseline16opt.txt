[PassRunner] running passes...
[PassRunner]   running pass: strip-dwarf...                    5.737e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: post-emscripten...                0.000176171 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inline-main...                    1.1035e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: no-exit-runtime...                0.000351336 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 0.000727305 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: memory-packing...                 2.7048e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dce...                            0.00307163 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.0045374 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.000400178 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.0018262 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: pick-load-signs...                0.000885662 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.00188471 seconds.
[PassRunner]   (validating)
[PassRunner] passes took 0.0139044 seconds.
[PassRunner] (final validation)
