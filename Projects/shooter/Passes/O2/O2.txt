[PassRunner] running passes...
[PassRunner]   running pass: duplicate-function-elimination... 0.00118707 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: memory-packing...                 2.3886e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dce...                            0.00309097 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.00458729 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.000402597 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.00184835 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: pick-load-signs...                0.000691811 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.0018667 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: code-pushing...                   0.00172775 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals-nostructure...    0.0081655 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.00374734 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.00107949 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.00195332 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.00235822 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals...                0.00560086 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.00274489 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.000586674 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.00226609 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.000588523 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.00266497 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.00175839 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.00259012 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.000306955 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.00156021 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.00137631 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.00139548 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: rse...                            0.00174313 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0025185 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dae-optimizing...                 0.0104829 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inlining-optimizing...            0.0186205 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 0.000436575 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-import-elimination...   1.2458e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-globals-optimizing...    0.000601503 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-module-elements...  0.000433795 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: directize...                      3.83e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: generate-stack-ir...              0.000309687 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-stack-ir...              0.00308446 seconds.
[PassRunner]   (validating)
[PassRunner] passes took 0.0944171 seconds.
[PassRunner] (final validation)
