[PassRunner] running passes...
[PassRunner]   running pass: strip-dwarf...                    9.958e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: post-emscripten...                0.000179578 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inline-main...                    8.307e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: no-exit-runtime...                0.000352833 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 0.000715482 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: memory-packing...                 2.7178e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dce...                            0.00310102 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.00453286 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.000398387 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.00183136 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: pick-load-signs...                0.000701635 seconds.
[PassRunner]   (validating)
[PassRunner] passes took 0.0118586 seconds.
[PassRunner] (final validation)
