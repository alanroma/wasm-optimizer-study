; ModuleID = 'src/gol-wasm.c'
source_filename = "src/gol-wasm.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32"

%struct.RGBA = type { i8, i8, i8, i8 }
%struct.Canvas = type { i32, i32, %struct.RGBA* }
%struct.Mouse = type { i32, i32, i32 }

@canvas_data = hidden global [921600 x %struct.RGBA] zeroinitializer, align 16
@canvas = hidden global %struct.Canvas { i32 1280, i32 720, %struct.RGBA* getelementptr inbounds ([921600 x %struct.RGBA], [921600 x %struct.RGBA]* @canvas_data, i32 0, i32 0) }, align 4
@mouse = hidden global %struct.Mouse zeroinitializer, align 4
@background = hidden constant %struct.RGBA { i8 0, i8 0, i8 100, i8 -1 }, align 1
@light_blue = hidden constant %struct.RGBA { i8 -66, i8 -66, i8 -1, i8 -1 }, align 1
@blue = hidden constant %struct.RGBA { i8 80, i8 80, i8 -56, i8 -1 }, align 1
@dark_blue = hidden constant %struct.RGBA { i8 0, i8 0, i8 70, i8 -1 }, align 1
@world_1 = hidden global [921600 x i32] zeroinitializer, align 16
@world_pointer = hidden global i32* getelementptr inbounds ([921600 x i32], [921600 x i32]* @world_1, i32 0, i32 0), align 4
@world_2 = hidden global [921600 x i32] zeroinitializer, align 16
@llvm.used = appending global [2 x i8*] [i8* bitcast (%struct.Canvas* @canvas to i8*), i8* bitcast (%struct.Mouse* @mouse to i8*)], section "llvm.metadata"

; Function Attrs: noinline nounwind optnone
define hidden void @restart() #0 {
entry:
  %i = alloca i32, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp ult i32 %0, 921600
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %call = call i32 @rand(i32 2)
  %tobool = icmp ne i32 %call, 0
  %1 = zext i1 %tobool to i64
  %cond = select i1 %tobool, i32 3, i32 0
  %2 = load i32*, i32** @world_pointer, align 4
  %3 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %2, i32 %3
  store i32 %cond, i32* %arrayidx, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %4 = load i32, i32* %i, align 4
  %inc = add i32 %4, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

declare i32 @rand(i32) #1

; Function Attrs: noinline nounwind optnone
define hidden void @tic(i32* %to) #0 {
entry:
  %to.addr = alloca i32*, align 4
  %y = alloca i32, align 4
  %i = alloca i32, align 4
  %x = alloca i32, align 4
  %neighbours = alloca i8, align 1
  store i32* %to, i32** %to.addr, align 4
  store i32 0, i32* %y, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc125, %entry
  %0 = load i32, i32* %y, align 4
  %cmp = icmp slt i32 %0, 720
  br i1 %cmp, label %for.body, label %for.end127

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %x, align 4
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc, %for.body
  %1 = load i32, i32* %x, align 4
  %cmp2 = icmp slt i32 %1, 1280
  br i1 %cmp2, label %for.body3, label %for.end

for.body3:                                        ; preds = %for.cond1
  store i8 0, i8* %neighbours, align 1
  %2 = load i32, i32* %x, align 4
  %cmp4 = icmp sgt i32 %2, 0
  br i1 %cmp4, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %for.body3
  %3 = load i32*, i32** @world_pointer, align 4
  %4 = load i32, i32* %i, align 4
  %sub = sub nsw i32 %4, 1
  %arrayidx = getelementptr inbounds i32, i32* %3, i32 %sub
  %5 = load i32, i32* %arrayidx, align 4
  %cmp5 = icmp ugt i32 %5, 1
  br i1 %cmp5, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %6 = load i8, i8* %neighbours, align 1
  %conv = zext i8 %6 to i32
  %add = add nsw i32 %conv, 1
  %conv6 = trunc i32 %add to i8
  store i8 %conv6, i8* %neighbours, align 1
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true, %for.body3
  %7 = load i32, i32* %x, align 4
  %cmp7 = icmp slt i32 %7, 1279
  br i1 %cmp7, label %land.lhs.true9, label %if.end18

land.lhs.true9:                                   ; preds = %if.end
  %8 = load i32*, i32** @world_pointer, align 4
  %9 = load i32, i32* %i, align 4
  %add10 = add nsw i32 %9, 1
  %arrayidx11 = getelementptr inbounds i32, i32* %8, i32 %add10
  %10 = load i32, i32* %arrayidx11, align 4
  %cmp12 = icmp ugt i32 %10, 1
  br i1 %cmp12, label %if.then14, label %if.end18

if.then14:                                        ; preds = %land.lhs.true9
  %11 = load i8, i8* %neighbours, align 1
  %conv15 = zext i8 %11 to i32
  %add16 = add nsw i32 %conv15, 1
  %conv17 = trunc i32 %add16 to i8
  store i8 %conv17, i8* %neighbours, align 1
  br label %if.end18

if.end18:                                         ; preds = %if.then14, %land.lhs.true9, %if.end
  %12 = load i32, i32* %y, align 4
  %cmp19 = icmp sgt i32 %12, 0
  br i1 %cmp19, label %land.lhs.true21, label %if.end30

land.lhs.true21:                                  ; preds = %if.end18
  %13 = load i32*, i32** @world_pointer, align 4
  %14 = load i32, i32* %i, align 4
  %sub22 = sub nsw i32 %14, 1280
  %arrayidx23 = getelementptr inbounds i32, i32* %13, i32 %sub22
  %15 = load i32, i32* %arrayidx23, align 4
  %cmp24 = icmp ugt i32 %15, 1
  br i1 %cmp24, label %if.then26, label %if.end30

if.then26:                                        ; preds = %land.lhs.true21
  %16 = load i8, i8* %neighbours, align 1
  %conv27 = zext i8 %16 to i32
  %add28 = add nsw i32 %conv27, 1
  %conv29 = trunc i32 %add28 to i8
  store i8 %conv29, i8* %neighbours, align 1
  br label %if.end30

if.end30:                                         ; preds = %if.then26, %land.lhs.true21, %if.end18
  %17 = load i32, i32* %y, align 4
  %cmp31 = icmp slt i32 %17, 719
  br i1 %cmp31, label %land.lhs.true33, label %if.end42

land.lhs.true33:                                  ; preds = %if.end30
  %18 = load i32*, i32** @world_pointer, align 4
  %19 = load i32, i32* %i, align 4
  %add34 = add nsw i32 %19, 1280
  %arrayidx35 = getelementptr inbounds i32, i32* %18, i32 %add34
  %20 = load i32, i32* %arrayidx35, align 4
  %cmp36 = icmp ugt i32 %20, 1
  br i1 %cmp36, label %if.then38, label %if.end42

if.then38:                                        ; preds = %land.lhs.true33
  %21 = load i8, i8* %neighbours, align 1
  %conv39 = zext i8 %21 to i32
  %add40 = add nsw i32 %conv39, 1
  %conv41 = trunc i32 %add40 to i8
  store i8 %conv41, i8* %neighbours, align 1
  br label %if.end42

if.end42:                                         ; preds = %if.then38, %land.lhs.true33, %if.end30
  %22 = load i32, i32* %x, align 4
  %cmp43 = icmp sgt i32 %22, 0
  br i1 %cmp43, label %land.lhs.true45, label %if.end58

land.lhs.true45:                                  ; preds = %if.end42
  %23 = load i32, i32* %y, align 4
  %cmp46 = icmp sgt i32 %23, 0
  br i1 %cmp46, label %land.lhs.true48, label %if.end58

land.lhs.true48:                                  ; preds = %land.lhs.true45
  %24 = load i32*, i32** @world_pointer, align 4
  %25 = load i32, i32* %i, align 4
  %sub49 = sub nsw i32 %25, 1280
  %sub50 = sub nsw i32 %sub49, 1
  %arrayidx51 = getelementptr inbounds i32, i32* %24, i32 %sub50
  %26 = load i32, i32* %arrayidx51, align 4
  %cmp52 = icmp ugt i32 %26, 1
  br i1 %cmp52, label %if.then54, label %if.end58

if.then54:                                        ; preds = %land.lhs.true48
  %27 = load i8, i8* %neighbours, align 1
  %conv55 = zext i8 %27 to i32
  %add56 = add nsw i32 %conv55, 1
  %conv57 = trunc i32 %add56 to i8
  store i8 %conv57, i8* %neighbours, align 1
  br label %if.end58

if.end58:                                         ; preds = %if.then54, %land.lhs.true48, %land.lhs.true45, %if.end42
  %28 = load i32, i32* %x, align 4
  %cmp59 = icmp slt i32 %28, 1279
  br i1 %cmp59, label %land.lhs.true61, label %if.end74

land.lhs.true61:                                  ; preds = %if.end58
  %29 = load i32, i32* %y, align 4
  %cmp62 = icmp sgt i32 %29, 0
  br i1 %cmp62, label %land.lhs.true64, label %if.end74

land.lhs.true64:                                  ; preds = %land.lhs.true61
  %30 = load i32*, i32** @world_pointer, align 4
  %31 = load i32, i32* %i, align 4
  %sub65 = sub nsw i32 %31, 1280
  %add66 = add nsw i32 %sub65, 1
  %arrayidx67 = getelementptr inbounds i32, i32* %30, i32 %add66
  %32 = load i32, i32* %arrayidx67, align 4
  %cmp68 = icmp ugt i32 %32, 1
  br i1 %cmp68, label %if.then70, label %if.end74

if.then70:                                        ; preds = %land.lhs.true64
  %33 = load i8, i8* %neighbours, align 1
  %conv71 = zext i8 %33 to i32
  %add72 = add nsw i32 %conv71, 1
  %conv73 = trunc i32 %add72 to i8
  store i8 %conv73, i8* %neighbours, align 1
  br label %if.end74

if.end74:                                         ; preds = %if.then70, %land.lhs.true64, %land.lhs.true61, %if.end58
  %34 = load i32, i32* %x, align 4
  %cmp75 = icmp sgt i32 %34, 0
  br i1 %cmp75, label %land.lhs.true77, label %if.end90

land.lhs.true77:                                  ; preds = %if.end74
  %35 = load i32, i32* %y, align 4
  %cmp78 = icmp slt i32 %35, 719
  br i1 %cmp78, label %land.lhs.true80, label %if.end90

land.lhs.true80:                                  ; preds = %land.lhs.true77
  %36 = load i32*, i32** @world_pointer, align 4
  %37 = load i32, i32* %i, align 4
  %add81 = add nsw i32 %37, 1280
  %sub82 = sub nsw i32 %add81, 1
  %arrayidx83 = getelementptr inbounds i32, i32* %36, i32 %sub82
  %38 = load i32, i32* %arrayidx83, align 4
  %cmp84 = icmp ugt i32 %38, 1
  br i1 %cmp84, label %if.then86, label %if.end90

if.then86:                                        ; preds = %land.lhs.true80
  %39 = load i8, i8* %neighbours, align 1
  %conv87 = zext i8 %39 to i32
  %add88 = add nsw i32 %conv87, 1
  %conv89 = trunc i32 %add88 to i8
  store i8 %conv89, i8* %neighbours, align 1
  br label %if.end90

if.end90:                                         ; preds = %if.then86, %land.lhs.true80, %land.lhs.true77, %if.end74
  %40 = load i32, i32* %x, align 4
  %cmp91 = icmp slt i32 %40, 1279
  br i1 %cmp91, label %land.lhs.true93, label %if.end106

land.lhs.true93:                                  ; preds = %if.end90
  %41 = load i32, i32* %y, align 4
  %cmp94 = icmp slt i32 %41, 719
  br i1 %cmp94, label %land.lhs.true96, label %if.end106

land.lhs.true96:                                  ; preds = %land.lhs.true93
  %42 = load i32*, i32** @world_pointer, align 4
  %43 = load i32, i32* %i, align 4
  %add97 = add nsw i32 %43, 1280
  %add98 = add nsw i32 %add97, 1
  %arrayidx99 = getelementptr inbounds i32, i32* %42, i32 %add98
  %44 = load i32, i32* %arrayidx99, align 4
  %cmp100 = icmp ugt i32 %44, 1
  br i1 %cmp100, label %if.then102, label %if.end106

if.then102:                                       ; preds = %land.lhs.true96
  %45 = load i8, i8* %neighbours, align 1
  %conv103 = zext i8 %45 to i32
  %add104 = add nsw i32 %conv103, 1
  %conv105 = trunc i32 %add104 to i8
  store i8 %conv105, i8* %neighbours, align 1
  br label %if.end106

if.end106:                                        ; preds = %if.then102, %land.lhs.true96, %land.lhs.true93, %if.end90
  %46 = load i32*, i32** @world_pointer, align 4
  %47 = load i32, i32* %i, align 4
  %arrayidx107 = getelementptr inbounds i32, i32* %46, i32 %47
  %48 = load i32, i32* %arrayidx107, align 4
  %cmp108 = icmp ugt i32 %48, 1
  br i1 %cmp108, label %if.then110, label %if.else

if.then110:                                       ; preds = %if.end106
  %49 = load i8, i8* %neighbours, align 1
  %conv111 = zext i8 %49 to i32
  %cmp112 = icmp eq i32 %conv111, 2
  br i1 %cmp112, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %if.then110
  %50 = load i8, i8* %neighbours, align 1
  %conv114 = zext i8 %50 to i32
  %cmp115 = icmp eq i32 %conv114, 3
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %if.then110
  %51 = phi i1 [ true, %if.then110 ], [ %cmp115, %lor.rhs ]
  %52 = zext i1 %51 to i64
  %cond = select i1 %51, i32 2, i32 1
  %53 = load i32*, i32** %to.addr, align 4
  %54 = load i32, i32* %i, align 4
  %arrayidx117 = getelementptr inbounds i32, i32* %53, i32 %54
  store i32 %cond, i32* %arrayidx117, align 4
  br label %if.end123

if.else:                                          ; preds = %if.end106
  %55 = load i8, i8* %neighbours, align 1
  %conv118 = zext i8 %55 to i32
  %cmp119 = icmp eq i32 %conv118, 3
  %56 = zext i1 %cmp119 to i64
  %cond121 = select i1 %cmp119, i32 3, i32 0
  %57 = load i32*, i32** %to.addr, align 4
  %58 = load i32, i32* %i, align 4
  %arrayidx122 = getelementptr inbounds i32, i32* %57, i32 %58
  store i32 %cond121, i32* %arrayidx122, align 4
  br label %if.end123

if.end123:                                        ; preds = %if.else, %lor.end
  br label %for.inc

for.inc:                                          ; preds = %if.end123
  %59 = load i32, i32* %x, align 4
  %inc = add nsw i32 %59, 1
  store i32 %inc, i32* %x, align 4
  %60 = load i32, i32* %i, align 4
  %inc124 = add nsw i32 %60, 1
  store i32 %inc124, i32* %i, align 4
  br label %for.cond1

for.end:                                          ; preds = %for.cond1
  br label %for.inc125

for.inc125:                                       ; preds = %for.end
  %61 = load i32, i32* %y, align 4
  %inc126 = add nsw i32 %61, 1
  store i32 %inc126, i32* %y, align 4
  br label %for.cond

for.end127:                                       ; preds = %for.cond
  %62 = load i32*, i32** %to.addr, align 4
  store i32* %62, i32** @world_pointer, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @start() #0 {
entry:
  %i = alloca i32, align 4
  %col = alloca %struct.RGBA*, align 4
  call void @restart()
  br label %while.body

while.body:                                       ; preds = %entry, %if.end
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %while.body
  %0 = load i32, i32* %i, align 4
  %cmp = icmp ult i32 %0, 921600
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %1 = load i32*, i32** @world_pointer, align 4
  %2 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %1, i32 %2
  %3 = load i32, i32* %arrayidx, align 4
  switch i32 %3, label %sw.epilog [
    i32 0, label %sw.bb
    i32 1, label %sw.bb1
    i32 2, label %sw.bb3
    i32 3, label %sw.bb4
  ]

sw.bb:                                            ; preds = %for.body
  store %struct.RGBA* @background, %struct.RGBA** %col, align 4
  br label %sw.epilog

sw.bb1:                                           ; preds = %for.body
  store %struct.RGBA* @dark_blue, %struct.RGBA** %col, align 4
  %4 = load i32*, i32** @world_pointer, align 4
  %5 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds i32, i32* %4, i32 %5
  store i32 0, i32* %arrayidx2, align 4
  br label %sw.epilog

sw.bb3:                                           ; preds = %for.body
  store %struct.RGBA* @blue, %struct.RGBA** %col, align 4
  br label %sw.epilog

sw.bb4:                                           ; preds = %for.body
  store %struct.RGBA* @light_blue, %struct.RGBA** %col, align 4
  %6 = load i32*, i32** @world_pointer, align 4
  %7 = load i32, i32* %i, align 4
  %arrayidx5 = getelementptr inbounds i32, i32* %6, i32 %7
  store i32 2, i32* %arrayidx5, align 4
  br label %sw.epilog

sw.epilog:                                        ; preds = %for.body, %sw.bb4, %sw.bb3, %sw.bb1, %sw.bb
  %8 = load %struct.RGBA*, %struct.RGBA** %col, align 4
  %9 = bitcast %struct.RGBA* %8 to i32*
  %10 = load i32, i32* %9, align 4
  %11 = load %struct.RGBA*, %struct.RGBA** getelementptr inbounds (%struct.Canvas, %struct.Canvas* @canvas, i32 0, i32 2), align 4
  %12 = bitcast %struct.RGBA* %11 to i32*
  %13 = load i32, i32* %i, align 4
  %add.ptr = getelementptr inbounds i32, i32* %12, i32 %13
  store i32 %10, i32* %add.ptr, align 4
  br label %for.inc

for.inc:                                          ; preds = %sw.epilog
  %14 = load i32, i32* %i, align 4
  %inc = add i32 %14, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %15 = load i32*, i32** @world_pointer, align 4
  %cmp6 = icmp eq i32* %15, getelementptr inbounds ([921600 x i32], [921600 x i32]* @world_1, i32 0, i32 0)
  %16 = zext i1 %cmp6 to i64
  %cond = select i1 %cmp6, i32* getelementptr inbounds ([921600 x i32], [921600 x i32]* @world_2, i32 0, i32 0), i32* getelementptr inbounds ([921600 x i32], [921600 x i32]* @world_1, i32 0, i32 0)
  call void @tic(i32* %cond)
  call void bitcast (void (...)* @next_frame to void ()*)()
  %17 = load i32, i32* getelementptr inbounds (%struct.Mouse, %struct.Mouse* @mouse, i32 0, i32 2), align 4
  %cmp7 = icmp eq i32 %17, 1
  br i1 %cmp7, label %if.then, label %if.end

if.then:                                          ; preds = %for.end
  store i32 0, i32* getelementptr inbounds (%struct.Mouse, %struct.Mouse* @mouse, i32 0, i32 2), align 4
  call void @restart()
  br label %if.end

if.end:                                           ; preds = %if.then, %for.end
  br label %while.body
}

declare void @next_frame(...) #2

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-prototype" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
