[PassRunner] running passes...
[PassRunner]   running pass: strip-dwarf...                        4.125e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: post-emscripten...                    0.000127579 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inline-main...                        6.018e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: no-exit-runtime...                    5.833e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination...     0.000339038 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: memory-packing...                     1.2079e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: ssa-nomerge...                        0.00259604 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dce...                                0.000752145 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...                  0.0011463 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...                7.1956e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...              0.000272456 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: pick-load-signs...                    0.00013863 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute-propagate...               0.00718519 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-added-constants-propagate... 0.00250456 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: code-pushing...                       0.000451313 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals-nostructure...        0.00475798 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                             0.000810322 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                     0.000319351 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...                  0.000204524 seconds.
[PassRunner]   (validating)
[PassRunner] passes took 0.0217579 seconds.
[PassRunner] (final validation)
