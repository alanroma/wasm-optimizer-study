[PassRunner] running passes...
[PassRunner]   running pass: strip-dwarf...                    3.86e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: post-emscripten...                0.000113234 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inline-main...                    5.14e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: no-exit-runtime...                5.7887e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 0.000358384 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: memory-packing...                 7.874e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dce...                            0.000750803 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.00113355 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            7.0541e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.000271794 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: pick-load-signs...                0.000138161 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.000414537 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-added-constants...       6.7677e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: code-pushing...                   0.000471494 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals-nostructure...    0.00360788 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.000782934 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.000317827 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.000227472 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.00096719 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals...                0.00504903 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.00046968 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.000103301 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.00104346 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.000106189 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.000475153 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.000178578 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.000575119 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            3.7032e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.000133901 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.000205762 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.000159351 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: rse...                            0.000397591 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.000397551 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dae-optimizing...                 0.000176356 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inlining-optimizing...            0.000149506 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 0.000109421 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-import-elimination...   4.178e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-globals-optimizing...    0.00034372 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-module-elements...  5.4188e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: directize...                      9.88e-07 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: generate-stack-ir...              4.5585e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-stack-ir...              0.000264594 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: strip-debug...                    0.000122818 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: strip-producers...                1.247e-06 seconds.
[PassRunner]   (validating)
[PassRunner] passes took 0.0203725 seconds.
[PassRunner] (final validation)
