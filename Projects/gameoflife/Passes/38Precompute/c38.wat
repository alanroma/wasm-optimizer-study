(module
  (type (;0;) (func))
  (type (;1;) (func (param i32)))
  (type (;2;) (func (result i32)))
  (type (;3;) (func (param i32) (result i32)))
  (import "env" "rand" (func (;0;) (type 3)))
  (import "env" "next_frame" (func (;1;) (type 0)))
  (func (;2;) (type 0)
    (local i32 i32)
    global.get 0
    i32.const 16
    i32.sub
    local.tee 0
    global.set 0
    local.get 0
    i32.const 0
    i32.store offset=12
    loop  ;; label = @1
      local.get 0
      i32.load offset=12
      i32.const 921600
      i32.lt_u
      i32.const 1
      i32.and
      i32.eqz
      if  ;; label = @2
        nop
      else
        i32.const 3
        i32.const 0
        i32.const 2
        call 0
        select
        local.set 1
        i32.const 0
        i32.load offset=1052
        local.get 0
        i32.load offset=12
        i32.const 2
        i32.shl
        i32.add
        local.get 1
        i32.store
        local.get 0
        local.get 0
        i32.load offset=12
        i32.const 1
        i32.add
        i32.store offset=12
        br 1 (;@1;)
      end
    end
    local.get 0
    i32.const 16
    i32.add
    global.set 0)
  (func (;3;) (type 1) (param i32)
    (local i32)
    global.get 0
    i32.const 32
    i32.sub
    local.tee 1
    local.get 0
    i32.store offset=28
    local.get 1
    i32.const 0
    local.tee 0
    i32.store offset=24
    local.get 1
    local.get 0
    i32.store offset=20
    loop  ;; label = @1
      local.get 1
      i32.load offset=24
      i32.const 720
      i32.lt_s
      i32.const 1
      i32.and
      i32.eqz
      if  ;; label = @2
        nop
      else
        local.get 1
        i32.const 0
        i32.store offset=16
        loop  ;; label = @3
          local.get 1
          i32.load offset=16
          i32.const 1280
          i32.lt_s
          i32.const 1
          i32.and
          i32.eqz
          if  ;; label = @4
            nop
          else
            local.get 1
            i32.const 0
            i32.store8 offset=15
            block  ;; label = @5
              local.get 1
              i32.load offset=16
              i32.const 0
              i32.gt_s
              i32.const 1
              i32.and
              i32.eqz
              br_if 0 (;@5;)
              i32.const 0
              i32.load offset=1052
              local.get 1
              i32.load offset=20
              i32.const 1
              i32.sub
              i32.const 2
              i32.shl
              i32.add
              i32.load
              i32.const 1
              i32.gt_u
              i32.const 1
              i32.and
              i32.eqz
              br_if 0 (;@5;)
              local.get 1
              local.get 1
              i32.load8_u offset=15
              i32.const 255
              i32.and
              i32.const 1
              i32.add
              i32.store8 offset=15
            end
            block  ;; label = @5
              local.get 1
              i32.load offset=16
              i32.const 1279
              i32.lt_s
              i32.const 1
              i32.and
              i32.eqz
              br_if 0 (;@5;)
              i32.const 0
              i32.load offset=1052
              local.get 1
              i32.load offset=20
              i32.const 1
              i32.add
              i32.const 2
              i32.shl
              i32.add
              i32.load
              i32.const 1
              i32.gt_u
              i32.const 1
              i32.and
              i32.eqz
              br_if 0 (;@5;)
              local.get 1
              local.get 1
              i32.load8_u offset=15
              i32.const 255
              i32.and
              i32.const 1
              i32.add
              i32.store8 offset=15
            end
            block  ;; label = @5
              local.get 1
              i32.load offset=24
              i32.const 0
              i32.gt_s
              i32.const 1
              i32.and
              i32.eqz
              br_if 0 (;@5;)
              i32.const 0
              i32.load offset=1052
              local.get 1
              i32.load offset=20
              i32.const 1280
              i32.sub
              i32.const 2
              i32.shl
              i32.add
              i32.load
              i32.const 1
              i32.gt_u
              i32.const 1
              i32.and
              i32.eqz
              br_if 0 (;@5;)
              local.get 1
              local.get 1
              i32.load8_u offset=15
              i32.const 255
              i32.and
              i32.const 1
              i32.add
              i32.store8 offset=15
            end
            block  ;; label = @5
              local.get 1
              i32.load offset=24
              i32.const 719
              i32.lt_s
              i32.const 1
              i32.and
              i32.eqz
              br_if 0 (;@5;)
              i32.const 0
              i32.load offset=1052
              local.get 1
              i32.load offset=20
              i32.const 1280
              i32.add
              i32.const 2
              i32.shl
              i32.add
              i32.load
              i32.const 1
              i32.gt_u
              i32.const 1
              i32.and
              i32.eqz
              br_if 0 (;@5;)
              local.get 1
              local.get 1
              i32.load8_u offset=15
              i32.const 255
              i32.and
              i32.const 1
              i32.add
              i32.store8 offset=15
            end
            block  ;; label = @5
              local.get 1
              i32.load offset=16
              i32.const 0
              i32.gt_s
              i32.const 1
              i32.and
              i32.eqz
              br_if 0 (;@5;)
              local.get 1
              i32.load offset=24
              i32.const 0
              i32.gt_s
              i32.const 1
              i32.and
              i32.eqz
              br_if 0 (;@5;)
              i32.const 0
              i32.load offset=1052
              local.get 1
              i32.load offset=20
              i32.const 1280
              i32.sub
              i32.const 1
              i32.sub
              i32.const 2
              i32.shl
              i32.add
              i32.load
              i32.const 1
              i32.gt_u
              i32.const 1
              i32.and
              i32.eqz
              br_if 0 (;@5;)
              local.get 1
              local.get 1
              i32.load8_u offset=15
              i32.const 255
              i32.and
              i32.const 1
              i32.add
              i32.store8 offset=15
            end
            block  ;; label = @5
              local.get 1
              i32.load offset=16
              i32.const 1279
              i32.lt_s
              i32.const 1
              i32.and
              i32.eqz
              br_if 0 (;@5;)
              local.get 1
              i32.load offset=24
              i32.const 0
              i32.gt_s
              i32.const 1
              i32.and
              i32.eqz
              br_if 0 (;@5;)
              i32.const 0
              i32.load offset=1052
              local.get 1
              i32.load offset=20
              i32.const 1280
              i32.sub
              i32.const 1
              i32.add
              i32.const 2
              i32.shl
              i32.add
              i32.load
              i32.const 1
              i32.gt_u
              i32.const 1
              i32.and
              i32.eqz
              br_if 0 (;@5;)
              local.get 1
              local.get 1
              i32.load8_u offset=15
              i32.const 255
              i32.and
              i32.const 1
              i32.add
              i32.store8 offset=15
            end
            block  ;; label = @5
              local.get 1
              i32.load offset=16
              i32.const 0
              i32.gt_s
              i32.const 1
              i32.and
              i32.eqz
              br_if 0 (;@5;)
              local.get 1
              i32.load offset=24
              i32.const 719
              i32.lt_s
              i32.const 1
              i32.and
              i32.eqz
              br_if 0 (;@5;)
              i32.const 0
              i32.load offset=1052
              local.get 1
              i32.load offset=20
              i32.const 1280
              i32.add
              i32.const 1
              i32.sub
              i32.const 2
              i32.shl
              i32.add
              i32.load
              i32.const 1
              i32.gt_u
              i32.const 1
              i32.and
              i32.eqz
              br_if 0 (;@5;)
              local.get 1
              local.get 1
              i32.load8_u offset=15
              i32.const 255
              i32.and
              i32.const 1
              i32.add
              i32.store8 offset=15
            end
            block  ;; label = @5
              local.get 1
              i32.load offset=16
              i32.const 1279
              i32.lt_s
              i32.const 1
              i32.and
              i32.eqz
              br_if 0 (;@5;)
              local.get 1
              i32.load offset=24
              i32.const 719
              i32.lt_s
              i32.const 1
              i32.and
              i32.eqz
              br_if 0 (;@5;)
              i32.const 0
              i32.load offset=1052
              local.get 1
              i32.load offset=20
              i32.const 1280
              i32.add
              i32.const 1
              i32.add
              i32.const 2
              i32.shl
              i32.add
              i32.load
              i32.const 1
              i32.gt_u
              i32.const 1
              i32.and
              i32.eqz
              br_if 0 (;@5;)
              local.get 1
              local.get 1
              i32.load8_u offset=15
              i32.const 255
              i32.and
              i32.const 1
              i32.add
              i32.store8 offset=15
            end
            block  ;; label = @5
              i32.const 0
              i32.load offset=1052
              local.get 1
              i32.load offset=20
              i32.const 2
              i32.shl
              i32.add
              i32.load
              i32.const 1
              i32.gt_u
              i32.const 1
              i32.and
              i32.eqz
              i32.eqz
              if  ;; label = @6
                i32.const 1
                local.set 0
                local.get 1
                i32.load offset=28
                local.get 1
                i32.load offset=20
                i32.const 2
                i32.shl
                i32.add
                i32.const 2
                i32.const 1
                local.get 1
                i32.load8_u offset=15
                i32.const 255
                i32.and
                i32.const 2
                i32.eq
                i32.const 1
                i32.and
                i32.eqz
                if (result i32)  ;; label = @7
                  local.get 1
                  i32.load8_u offset=15
                  i32.const 255
                  i32.and
                  i32.const 3
                  i32.eq
                else
                  local.get 0
                end
                i32.const 1
                i32.and
                select
                i32.store
                br 1 (;@5;)
              end
              local.get 1
              i32.load offset=28
              local.get 1
              i32.load offset=20
              i32.const 2
              i32.shl
              i32.add
              i32.const 3
              local.tee 0
              i32.const 0
              local.get 1
              i32.load8_u offset=15
              i32.const 255
              i32.and
              local.get 0
              i32.eq
              i32.const 1
              i32.and
              select
              i32.store
            end
            local.get 1
            local.get 1
            i32.load offset=16
            i32.const 1
            i32.add
            i32.store offset=16
            local.get 1
            local.get 1
            i32.load offset=20
            i32.const 1
            i32.add
            i32.store offset=20
            br 1 (;@3;)
          end
        end
        local.get 1
        local.get 1
        i32.load offset=24
        i32.const 1
        i32.add
        i32.store offset=24
        br 1 (;@1;)
      end
    end
    i32.const 0
    local.get 1
    i32.load offset=28
    i32.store offset=1052)
  (func (;4;) (type 0)
    (local i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32)
    global.get 1
    i32.const 2
    i32.eq
    if  ;; label = @1
      global.get 2
      global.get 2
      i32.load
      i32.const -756
      i32.add
      i32.store
      global.get 2
      i32.load
      local.tee 1
      i32.load
      local.set 7
      local.get 1
      i32.load offset=4
      local.set 8
      local.get 1
      i32.load offset=8
      local.set 2
      local.get 1
      i32.load offset=12
      local.set 9
      local.get 1
      i32.load offset=16
      local.set 10
      local.get 1
      i32.load offset=20
      local.set 11
      local.get 1
      i32.load offset=24
      local.set 12
      local.get 1
      i32.load offset=28
      local.set 13
      local.get 1
      i32.load offset=32
      local.set 14
      local.get 1
      i32.load offset=36
      local.set 15
      local.get 1
      i32.load offset=40
      local.set 16
      local.get 1
      i32.load offset=44
      local.set 17
      local.get 1
      i32.load offset=48
      local.set 18
      local.get 1
      i32.load offset=52
      local.set 19
      local.get 1
      i32.load offset=56
      local.set 20
      local.get 1
      i32.load offset=60
      local.set 21
      local.get 1
      i32.load offset=64
      local.set 22
      local.get 1
      i32.load offset=68
      local.set 4
      local.get 1
      i32.load offset=72
      local.set 23
      local.get 1
      i32.load offset=76
      local.set 24
      local.get 1
      i32.load offset=80
      local.set 25
      local.get 1
      i32.load offset=84
      local.set 26
      local.get 1
      i32.load offset=88
      local.set 27
      local.get 1
      i32.load offset=92
      local.set 28
      local.get 1
      i32.load offset=96
      local.set 29
      local.get 1
      i32.load offset=100
      local.set 30
      local.get 1
      i32.load offset=104
      local.set 31
      local.get 1
      i32.load offset=108
      local.set 32
      local.get 1
      i32.load offset=112
      local.set 33
      local.get 1
      i32.load offset=116
      local.set 34
      local.get 1
      i32.load offset=120
      local.set 35
      local.get 1
      i32.load offset=124
      local.set 36
      local.get 1
      i32.load offset=128
      local.set 37
      local.get 1
      i32.load offset=132
      local.set 38
      local.get 1
      i32.load offset=136
      local.set 39
      local.get 1
      i32.load offset=140
      local.set 40
      local.get 1
      i32.load offset=144
      local.set 41
      local.get 1
      i32.load offset=148
      local.set 42
      local.get 1
      i32.load offset=152
      local.set 43
      local.get 1
      i32.load offset=156
      local.set 44
      local.get 1
      i32.load offset=160
      local.set 45
      local.get 1
      i32.load offset=164
      local.set 46
      local.get 1
      i32.load offset=168
      local.set 47
      local.get 1
      i32.load offset=172
      local.set 48
      local.get 1
      i32.load offset=176
      local.set 49
      local.get 1
      i32.load offset=180
      local.set 50
      local.get 1
      i32.load offset=184
      local.set 51
      local.get 1
      i32.load offset=188
      local.set 52
      local.get 1
      i32.load offset=192
      local.set 53
      local.get 1
      i32.load offset=196
      local.set 3
      local.get 1
      i32.load offset=200
      local.set 54
      local.get 1
      i32.load offset=204
      local.set 5
      local.get 1
      i32.load offset=208
      local.set 55
      local.get 1
      i32.load offset=212
      local.set 56
      local.get 1
      i32.load offset=216
      local.set 57
      local.get 1
      i32.load offset=220
      local.set 58
      local.get 1
      i32.load offset=224
      local.set 59
      local.get 1
      i32.load offset=228
      local.set 60
      local.get 1
      i32.load offset=232
      local.set 6
      local.get 1
      i32.load offset=236
      local.set 61
      local.get 1
      i32.load offset=240
      local.set 62
      local.get 1
      i32.load offset=244
      local.set 63
      local.get 1
      i32.load offset=248
      local.set 64
      local.get 1
      i32.load offset=252
      local.set 65
      local.get 1
      i32.load offset=256
      local.set 66
      local.get 1
      i32.load offset=260
      local.set 67
      local.get 1
      i32.load offset=264
      local.set 68
      local.get 1
      i32.load offset=268
      local.set 69
      local.get 1
      i32.load offset=272
      local.set 70
      local.get 1
      i32.load offset=276
      local.set 71
      local.get 1
      i32.load offset=280
      local.set 72
      local.get 1
      i32.load offset=284
      local.set 73
      local.get 1
      i32.load offset=288
      local.set 74
      local.get 1
      i32.load offset=292
      local.set 75
      local.get 1
      i32.load offset=296
      local.set 76
      local.get 1
      i32.load offset=300
      local.set 77
      local.get 1
      i32.load offset=304
      local.set 78
      local.get 1
      i32.load offset=308
      local.set 79
      local.get 1
      i32.load offset=312
      local.set 80
      local.get 1
      i32.load offset=316
      local.set 81
      local.get 1
      i32.load offset=320
      local.set 82
      local.get 1
      i32.load offset=324
      local.set 83
      local.get 1
      i32.load offset=328
      local.set 84
      local.get 1
      i32.load offset=332
      local.set 85
      local.get 1
      i32.load offset=336
      local.set 86
      local.get 1
      i32.load offset=340
      local.set 87
      local.get 1
      i32.load offset=344
      local.set 88
      local.get 1
      i32.load offset=348
      local.set 89
      local.get 1
      i32.load offset=352
      local.set 90
      local.get 1
      i32.load offset=356
      local.set 91
      local.get 1
      i32.load offset=360
      local.set 92
      local.get 1
      i32.load offset=364
      local.set 93
      local.get 1
      i32.load offset=368
      local.set 94
      local.get 1
      i32.load offset=372
      local.set 95
      local.get 1
      i32.load offset=376
      local.set 96
      local.get 1
      i32.load offset=380
      local.set 97
      local.get 1
      i32.load offset=384
      local.set 98
      local.get 1
      i32.load offset=388
      local.set 99
      local.get 1
      i32.load offset=392
      local.set 100
      local.get 1
      i32.load offset=396
      local.set 101
      local.get 1
      i32.load offset=400
      local.set 102
      local.get 1
      i32.load offset=404
      local.set 103
      local.get 1
      i32.load offset=408
      local.set 104
      local.get 1
      i32.load offset=412
      local.set 105
      local.get 1
      i32.load offset=416
      local.set 106
      local.get 1
      i32.load offset=420
      local.set 107
      local.get 1
      i32.load offset=424
      local.set 108
      local.get 1
      i32.load offset=428
      local.set 109
      local.get 1
      i32.load offset=432
      local.set 110
      local.get 1
      i32.load offset=436
      local.set 111
      local.get 1
      i32.load offset=440
      local.set 112
      local.get 1
      i32.load offset=444
      local.set 113
      local.get 1
      i32.load offset=448
      local.set 114
      local.get 1
      i32.load offset=452
      local.set 115
      local.get 1
      i32.load offset=456
      local.set 116
      local.get 1
      i32.load offset=460
      local.set 117
      local.get 1
      i32.load offset=464
      local.set 118
      local.get 1
      i32.load offset=468
      local.set 119
      local.get 1
      i32.load offset=472
      local.set 120
      local.get 1
      i32.load offset=476
      local.set 121
      local.get 1
      i32.load offset=480
      local.set 122
      local.get 1
      i32.load offset=484
      local.set 123
      local.get 1
      i32.load offset=488
      local.set 124
      local.get 1
      i32.load offset=492
      local.set 125
      local.get 1
      i32.load offset=496
      local.set 126
      local.get 1
      i32.load offset=500
      local.set 127
      local.get 1
      i32.load offset=504
      local.set 128
      local.get 1
      i32.load offset=508
      local.set 129
      local.get 1
      i32.load offset=512
      local.set 130
      local.get 1
      i32.load offset=516
      local.set 131
      local.get 1
      i32.load offset=520
      local.set 132
      local.get 1
      i32.load offset=524
      local.set 133
      local.get 1
      i32.load offset=528
      local.set 134
      local.get 1
      i32.load offset=532
      local.set 135
      local.get 1
      i32.load offset=536
      local.set 136
      local.get 1
      i32.load offset=540
      local.set 137
      local.get 1
      i32.load offset=544
      local.set 138
      local.get 1
      i32.load offset=548
      local.set 139
      local.get 1
      i32.load offset=552
      local.set 140
      local.get 1
      i32.load offset=556
      local.set 141
      local.get 1
      i32.load offset=560
      local.set 142
      local.get 1
      i32.load offset=564
      local.set 143
      local.get 1
      i32.load offset=568
      local.set 144
      local.get 1
      i32.load offset=572
      local.set 145
      local.get 1
      i32.load offset=576
      local.set 146
      local.get 1
      i32.load offset=580
      local.set 147
      local.get 1
      i32.load offset=584
      local.set 148
      local.get 1
      i32.load offset=588
      local.set 149
      local.get 1
      i32.load offset=592
      local.set 150
      local.get 1
      i32.load offset=596
      local.set 151
      local.get 1
      i32.load offset=600
      local.set 152
      local.get 1
      i32.load offset=604
      local.set 153
      local.get 1
      i32.load offset=608
      local.set 154
      local.get 1
      i32.load offset=612
      local.set 155
      local.get 1
      i32.load offset=616
      local.set 156
      local.get 1
      i32.load offset=620
      local.set 157
      local.get 1
      i32.load offset=624
      local.set 158
      local.get 1
      i32.load offset=628
      local.set 159
      local.get 1
      i32.load offset=632
      local.set 160
      local.get 1
      i32.load offset=636
      local.set 161
      local.get 1
      i32.load offset=640
      local.set 162
      local.get 1
      i32.load offset=644
      local.set 163
      local.get 1
      i32.load offset=648
      local.set 164
      local.get 1
      i32.load offset=652
      local.set 165
      local.get 1
      i32.load offset=656
      local.set 166
      local.get 1
      i32.load offset=660
      local.set 167
      local.get 1
      i32.load offset=664
      local.set 168
      local.get 1
      i32.load offset=668
      local.set 169
      local.get 1
      i32.load offset=672
      local.set 170
      local.get 1
      i32.load offset=676
      local.set 171
      local.get 1
      i32.load offset=680
      local.set 172
      local.get 1
      i32.load offset=684
      local.set 173
      local.get 1
      i32.load offset=688
      local.set 174
      local.get 1
      i32.load offset=692
      local.set 175
      local.get 1
      i32.load offset=696
      local.set 176
      local.get 1
      i32.load offset=700
      local.set 177
      local.get 1
      i32.load offset=704
      local.set 178
      local.get 1
      i32.load offset=708
      local.set 179
      local.get 1
      i32.load offset=712
      local.set 180
      local.get 1
      i32.load offset=716
      local.set 181
      local.get 1
      i32.load offset=720
      local.set 182
      local.get 1
      i32.load offset=724
      local.set 183
      local.get 1
      i32.load offset=728
      local.set 184
      local.get 1
      i32.load offset=732
      local.set 185
      local.get 1
      i32.load offset=736
      local.set 186
      local.get 1
      i32.load offset=740
      local.set 187
      local.get 1
      i32.load offset=744
      local.set 188
      local.get 1
      i32.load offset=748
      local.set 189
      local.get 1
      i32.load offset=752
      local.set 1
    end
    block (result i32)  ;; label = @1
      global.get 1
      i32.const 2
      i32.eq
      if  ;; label = @2
        global.get 2
        global.get 2
        i32.load
        i32.const -4
        i32.add
        i32.store
        global.get 2
        i32.load
        i32.load
        local.set 0
      end
      global.get 1
      i32.eqz
      if  ;; label = @2
        global.get 0
        local.tee 71
        local.tee 7
        local.tee 72
        i32.const 16
        local.tee 8
        local.tee 73
        i32.sub
        local.tee 74
        local.tee 2
        local.tee 75
        global.set 0
        call 2
      end
      loop  ;; label = @2
        global.get 1
        i32.eqz
        if  ;; label = @3
          local.get 2
          local.tee 76
          i32.const 0
          local.tee 9
          local.tee 77
          i32.store offset=12
          loop  ;; label = @4
            local.get 2
            local.tee 78
            i32.load offset=12
            local.tee 79
            local.tee 11
            local.tee 80
            local.tee 12
            local.tee 82
            i32.const 921600
            local.tee 10
            local.tee 81
            local.tee 13
            local.tee 83
            i32.lt_u
            local.tee 84
            local.tee 14
            local.tee 85
            i32.const 1
            local.tee 15
            local.tee 86
            i32.and
            local.tee 87
            local.tee 16
            local.tee 88
            i32.eqz
            local.tee 89
            if  ;; label = @5
              nop
            else
              block  ;; label = @6
                i32.const 0
                local.tee 17
                local.tee 90
                i32.load offset=1052
                local.tee 91
                local.tee 18
                local.tee 97
                local.get 2
                local.tee 92
                i32.load offset=12
                local.tee 93
                local.tee 19
                local.tee 94
                i32.const 2
                local.tee 20
                local.tee 95
                i32.shl
                local.tee 96
                local.tee 21
                local.tee 98
                i32.add
                local.tee 99
                local.tee 22
                local.tee 100
                i32.load
                local.tee 101
                local.tee 4
                local.tee 102
                i32.const 3
                local.tee 23
                local.tee 103
                i32.gt_u
                local.tee 104
                local.tee 24
                local.tee 105
                br_if 0 (;@6;)
                block  ;; label = @7
                  block  ;; label = @8
                    block  ;; label = @9
                      block  ;; label = @10
                        local.get 4
                        local.tee 106
                        i32.const 1
                        i32.sub
                        br_table 1 (;@9;) 2 (;@8;) 3 (;@7;) 0 (;@10;)
                      end
                      local.get 2
                      local.tee 107
                      i32.const 1024
                      local.tee 25
                      local.tee 108
                      i32.store offset=8
                      br 3 (;@6;)
                    end
                    local.get 2
                    local.tee 109
                    i32.const 1036
                    local.tee 27
                    local.tee 110
                    i32.store offset=8
                    i32.const 0
                    local.tee 28
                    local.tee 111
                    i32.load offset=1052
                    local.tee 112
                    local.tee 29
                    local.tee 118
                    local.get 2
                    local.tee 113
                    i32.load offset=12
                    local.tee 114
                    local.tee 30
                    local.tee 115
                    i32.const 2
                    local.tee 31
                    local.tee 116
                    i32.shl
                    local.tee 117
                    local.tee 32
                    local.tee 119
                    i32.add
                    local.tee 120
                    local.tee 33
                    local.tee 121
                    i32.const 0
                    local.tee 26
                    local.tee 122
                    i32.store
                    br 2 (;@6;)
                  end
                  local.get 2
                  local.tee 123
                  i32.const 1032
                  local.tee 34
                  local.tee 124
                  i32.store offset=8
                  br 1 (;@6;)
                end
                local.get 2
                local.tee 125
                i32.const 1028
                local.tee 36
                local.tee 126
                i32.store offset=8
                i32.const 0
                local.tee 37
                local.tee 127
                i32.load offset=1052
                local.tee 128
                local.tee 38
                local.tee 134
                local.get 2
                local.tee 129
                i32.load offset=12
                local.tee 130
                local.tee 39
                local.tee 131
                i32.const 2
                local.tee 40
                local.tee 132
                i32.shl
                local.tee 133
                local.tee 41
                local.tee 135
                i32.add
                local.tee 136
                local.tee 42
                local.tee 137
                i32.const 2
                local.tee 35
                local.tee 138
                i32.store
              end
              i32.const 0
              local.tee 45
              local.tee 143
              i32.load offset=1048
              local.tee 144
              local.tee 46
              local.tee 150
              local.get 2
              local.tee 145
              i32.load offset=12
              local.tee 146
              local.tee 47
              local.tee 147
              i32.const 2
              local.tee 48
              local.tee 148
              i32.shl
              local.tee 149
              local.tee 49
              local.tee 151
              i32.add
              local.tee 152
              local.tee 50
              local.tee 153
              local.get 2
              local.tee 139
              i32.load offset=8
              local.tee 140
              local.tee 43
              local.tee 141
              i32.load
              local.tee 142
              local.tee 44
              local.tee 154
              i32.store
              local.get 2
              local.tee 160
              local.get 2
              local.tee 155
              i32.load offset=12
              local.tee 156
              local.tee 51
              local.tee 157
              i32.const 1
              local.tee 52
              local.tee 158
              i32.add
              local.tee 159
              local.tee 53
              local.tee 161
              i32.store offset=12
              br 1 (;@4;)
            end
          end
          i32.const 0
          local.tee 55
          local.tee 162
          i32.load offset=1052
          local.tee 163
          local.tee 56
          local.tee 164
          local.tee 57
          local.tee 166
          i32.const 3687472
          local.tee 5
          local.tee 165
          local.tee 58
          local.tee 167
          i32.eq
          local.tee 168
          local.tee 59
          local.tee 169
          i32.const 1
          local.tee 60
          local.tee 170
          i32.and
          local.tee 171
          local.set 6
          i32.const 7373872
          local.tee 54
          local.tee 172
          local.get 5
          local.tee 173
          local.get 6
          local.tee 174
          select
          local.tee 175
          local.tee 61
          local.tee 176
          call 3
          i32.const 1
          local.set 3
        end
        local.get 0
        i32.eqz
        i32.const 1
        global.get 1
        select
        if  ;; label = @3
          call 1
          i32.const 0
          global.get 1
          i32.const 1
          i32.eq
          br_if 2 (;@1;)
          drop
        end
        global.get 1
        i32.eqz
        if  ;; label = @3
          i32.const 0
          local.tee 62
          local.tee 177
          i32.load offset=3687464
          local.tee 178
          local.tee 63
          local.tee 179
          local.tee 64
          local.tee 181
          local.get 3
          local.tee 180
          local.tee 65
          local.tee 182
          i32.eq
          local.tee 183
          local.tee 66
          local.tee 184
          i32.const 1
          local.tee 67
          local.tee 185
          i32.and
          local.tee 186
          local.tee 68
          local.tee 187
          i32.eqz
          local.tee 188
          br_if 1 (;@2;)
          i32.const 0
          local.tee 70
          local.tee 189
          i32.const 0
          local.tee 69
          local.tee 1
          i32.store offset=3687464
          call 2
          br 1 (;@2;)
        end
      end
      return
    end
    local.set 0
    global.get 2
    i32.load
    local.get 0
    i32.store
    global.get 2
    global.get 2
    i32.load
    i32.const 4
    i32.add
    i32.store
    global.get 2
    i32.load
    local.tee 0
    local.get 7
    i32.store
    local.get 0
    local.get 8
    i32.store offset=4
    local.get 0
    local.get 2
    i32.store offset=8
    local.get 0
    local.get 9
    i32.store offset=12
    local.get 0
    local.get 10
    i32.store offset=16
    local.get 0
    local.get 11
    i32.store offset=20
    local.get 0
    local.get 12
    i32.store offset=24
    local.get 0
    local.get 13
    i32.store offset=28
    local.get 0
    local.get 14
    i32.store offset=32
    local.get 0
    local.get 15
    i32.store offset=36
    local.get 0
    local.get 16
    i32.store offset=40
    local.get 0
    local.get 17
    i32.store offset=44
    local.get 0
    local.get 18
    i32.store offset=48
    local.get 0
    local.get 19
    i32.store offset=52
    local.get 0
    local.get 20
    i32.store offset=56
    local.get 0
    local.get 21
    i32.store offset=60
    local.get 0
    local.get 22
    i32.store offset=64
    local.get 0
    local.get 4
    i32.store offset=68
    local.get 0
    local.get 23
    i32.store offset=72
    local.get 0
    local.get 24
    i32.store offset=76
    local.get 0
    local.get 25
    i32.store offset=80
    local.get 0
    local.get 26
    i32.store offset=84
    local.get 0
    local.get 27
    i32.store offset=88
    local.get 0
    local.get 28
    i32.store offset=92
    local.get 0
    local.get 29
    i32.store offset=96
    local.get 0
    local.get 30
    i32.store offset=100
    local.get 0
    local.get 31
    i32.store offset=104
    local.get 0
    local.get 32
    i32.store offset=108
    local.get 0
    local.get 33
    i32.store offset=112
    local.get 0
    local.get 34
    i32.store offset=116
    local.get 0
    local.get 35
    i32.store offset=120
    local.get 0
    local.get 36
    i32.store offset=124
    local.get 0
    local.get 37
    i32.store offset=128
    local.get 0
    local.get 38
    i32.store offset=132
    local.get 0
    local.get 39
    i32.store offset=136
    local.get 0
    local.get 40
    i32.store offset=140
    local.get 0
    local.get 41
    i32.store offset=144
    local.get 0
    local.get 42
    i32.store offset=148
    local.get 0
    local.get 43
    i32.store offset=152
    local.get 0
    local.get 44
    i32.store offset=156
    local.get 0
    local.get 45
    i32.store offset=160
    local.get 0
    local.get 46
    i32.store offset=164
    local.get 0
    local.get 47
    i32.store offset=168
    local.get 0
    local.get 48
    i32.store offset=172
    local.get 0
    local.get 49
    i32.store offset=176
    local.get 0
    local.get 50
    i32.store offset=180
    local.get 0
    local.get 51
    i32.store offset=184
    local.get 0
    local.get 52
    i32.store offset=188
    local.get 0
    local.get 53
    i32.store offset=192
    local.get 0
    local.get 3
    i32.store offset=196
    local.get 0
    local.get 54
    i32.store offset=200
    local.get 0
    local.get 5
    i32.store offset=204
    local.get 0
    local.get 55
    i32.store offset=208
    local.get 0
    local.get 56
    i32.store offset=212
    local.get 0
    local.get 57
    i32.store offset=216
    local.get 0
    local.get 58
    i32.store offset=220
    local.get 0
    local.get 59
    i32.store offset=224
    local.get 0
    local.get 60
    i32.store offset=228
    local.get 0
    local.get 6
    i32.store offset=232
    local.get 0
    local.get 61
    i32.store offset=236
    local.get 0
    local.get 62
    i32.store offset=240
    local.get 0
    local.get 63
    i32.store offset=244
    local.get 0
    local.get 64
    i32.store offset=248
    local.get 0
    local.get 65
    i32.store offset=252
    local.get 0
    local.get 66
    i32.store offset=256
    local.get 0
    local.get 67
    i32.store offset=260
    local.get 0
    local.get 68
    i32.store offset=264
    local.get 0
    local.get 69
    i32.store offset=268
    local.get 0
    local.get 70
    i32.store offset=272
    local.get 0
    local.get 71
    i32.store offset=276
    local.get 0
    local.get 72
    i32.store offset=280
    local.get 0
    local.get 73
    i32.store offset=284
    local.get 0
    local.get 74
    i32.store offset=288
    local.get 0
    local.get 75
    i32.store offset=292
    local.get 0
    local.get 76
    i32.store offset=296
    local.get 0
    local.get 77
    i32.store offset=300
    local.get 0
    local.get 78
    i32.store offset=304
    local.get 0
    local.get 79
    i32.store offset=308
    local.get 0
    local.get 80
    i32.store offset=312
    local.get 0
    local.get 81
    i32.store offset=316
    local.get 0
    local.get 82
    i32.store offset=320
    local.get 0
    local.get 83
    i32.store offset=324
    local.get 0
    local.get 84
    i32.store offset=328
    local.get 0
    local.get 85
    i32.store offset=332
    local.get 0
    local.get 86
    i32.store offset=336
    local.get 0
    local.get 87
    i32.store offset=340
    local.get 0
    local.get 88
    i32.store offset=344
    local.get 0
    local.get 89
    i32.store offset=348
    local.get 0
    local.get 90
    i32.store offset=352
    local.get 0
    local.get 91
    i32.store offset=356
    local.get 0
    local.get 92
    i32.store offset=360
    local.get 0
    local.get 93
    i32.store offset=364
    local.get 0
    local.get 94
    i32.store offset=368
    local.get 0
    local.get 95
    i32.store offset=372
    local.get 0
    local.get 96
    i32.store offset=376
    local.get 0
    local.get 97
    i32.store offset=380
    local.get 0
    local.get 98
    i32.store offset=384
    local.get 0
    local.get 99
    i32.store offset=388
    local.get 0
    local.get 100
    i32.store offset=392
    local.get 0
    local.get 101
    i32.store offset=396
    local.get 0
    local.get 102
    i32.store offset=400
    local.get 0
    local.get 103
    i32.store offset=404
    local.get 0
    local.get 104
    i32.store offset=408
    local.get 0
    local.get 105
    i32.store offset=412
    local.get 0
    local.get 106
    i32.store offset=416
    local.get 0
    local.get 107
    i32.store offset=420
    local.get 0
    local.get 108
    i32.store offset=424
    local.get 0
    local.get 109
    i32.store offset=428
    local.get 0
    local.get 110
    i32.store offset=432
    local.get 0
    local.get 111
    i32.store offset=436
    local.get 0
    local.get 112
    i32.store offset=440
    local.get 0
    local.get 113
    i32.store offset=444
    local.get 0
    local.get 114
    i32.store offset=448
    local.get 0
    local.get 115
    i32.store offset=452
    local.get 0
    local.get 116
    i32.store offset=456
    local.get 0
    local.get 117
    i32.store offset=460
    local.get 0
    local.get 118
    i32.store offset=464
    local.get 0
    local.get 119
    i32.store offset=468
    local.get 0
    local.get 120
    i32.store offset=472
    local.get 0
    local.get 121
    i32.store offset=476
    local.get 0
    local.get 122
    i32.store offset=480
    local.get 0
    local.get 123
    i32.store offset=484
    local.get 0
    local.get 124
    i32.store offset=488
    local.get 0
    local.get 125
    i32.store offset=492
    local.get 0
    local.get 126
    i32.store offset=496
    local.get 0
    local.get 127
    i32.store offset=500
    local.get 0
    local.get 128
    i32.store offset=504
    local.get 0
    local.get 129
    i32.store offset=508
    local.get 0
    local.get 130
    i32.store offset=512
    local.get 0
    local.get 131
    i32.store offset=516
    local.get 0
    local.get 132
    i32.store offset=520
    local.get 0
    local.get 133
    i32.store offset=524
    local.get 0
    local.get 134
    i32.store offset=528
    local.get 0
    local.get 135
    i32.store offset=532
    local.get 0
    local.get 136
    i32.store offset=536
    local.get 0
    local.get 137
    i32.store offset=540
    local.get 0
    local.get 138
    i32.store offset=544
    local.get 0
    local.get 139
    i32.store offset=548
    local.get 0
    local.get 140
    i32.store offset=552
    local.get 0
    local.get 141
    i32.store offset=556
    local.get 0
    local.get 142
    i32.store offset=560
    local.get 0
    local.get 143
    i32.store offset=564
    local.get 0
    local.get 144
    i32.store offset=568
    local.get 0
    local.get 145
    i32.store offset=572
    local.get 0
    local.get 146
    i32.store offset=576
    local.get 0
    local.get 147
    i32.store offset=580
    local.get 0
    local.get 148
    i32.store offset=584
    local.get 0
    local.get 149
    i32.store offset=588
    local.get 0
    local.get 150
    i32.store offset=592
    local.get 0
    local.get 151
    i32.store offset=596
    local.get 0
    local.get 152
    i32.store offset=600
    local.get 0
    local.get 153
    i32.store offset=604
    local.get 0
    local.get 154
    i32.store offset=608
    local.get 0
    local.get 155
    i32.store offset=612
    local.get 0
    local.get 156
    i32.store offset=616
    local.get 0
    local.get 157
    i32.store offset=620
    local.get 0
    local.get 158
    i32.store offset=624
    local.get 0
    local.get 159
    i32.store offset=628
    local.get 0
    local.get 160
    i32.store offset=632
    local.get 0
    local.get 161
    i32.store offset=636
    local.get 0
    local.get 162
    i32.store offset=640
    local.get 0
    local.get 163
    i32.store offset=644
    local.get 0
    local.get 164
    i32.store offset=648
    local.get 0
    local.get 165
    i32.store offset=652
    local.get 0
    local.get 166
    i32.store offset=656
    local.get 0
    local.get 167
    i32.store offset=660
    local.get 0
    local.get 168
    i32.store offset=664
    local.get 0
    local.get 169
    i32.store offset=668
    local.get 0
    local.get 170
    i32.store offset=672
    local.get 0
    local.get 171
    i32.store offset=676
    local.get 0
    local.get 172
    i32.store offset=680
    local.get 0
    local.get 173
    i32.store offset=684
    local.get 0
    local.get 174
    i32.store offset=688
    local.get 0
    local.get 175
    i32.store offset=692
    local.get 0
    local.get 176
    i32.store offset=696
    local.get 0
    local.get 177
    i32.store offset=700
    local.get 0
    local.get 178
    i32.store offset=704
    local.get 0
    local.get 179
    i32.store offset=708
    local.get 0
    local.get 180
    i32.store offset=712
    local.get 0
    local.get 181
    i32.store offset=716
    local.get 0
    local.get 182
    i32.store offset=720
    local.get 0
    local.get 183
    i32.store offset=724
    local.get 0
    local.get 184
    i32.store offset=728
    local.get 0
    local.get 185
    i32.store offset=732
    local.get 0
    local.get 186
    i32.store offset=736
    local.get 0
    local.get 187
    i32.store offset=740
    local.get 0
    local.get 188
    i32.store offset=744
    local.get 0
    local.get 189
    i32.store offset=748
    local.get 0
    local.get 1
    i32.store offset=752
    global.get 2
    global.get 2
    i32.load
    i32.const 756
    i32.add
    i32.store)
  (func (;5;) (type 1) (param i32)
    i32.const 1
    global.set 1
    local.get 0
    global.set 2
    global.get 2
    i32.load
    global.get 2
    i32.load offset=4
    i32.gt_u
    if  ;; label = @1
      unreachable
    end)
  (func (;6;) (type 0)
    i32.const 0
    global.set 1
    global.get 2
    i32.load
    global.get 2
    i32.load offset=4
    i32.gt_u
    if  ;; label = @1
      unreachable
    end)
  (func (;7;) (type 1) (param i32)
    i32.const 2
    global.set 1
    local.get 0
    global.set 2
    global.get 2
    i32.load
    global.get 2
    i32.load offset=4
    i32.gt_u
    if  ;; label = @1
      unreachable
    end)
  (func (;8;) (type 2) (result i32)
    global.get 1)
  (table (;0;) 1 1 funcref)
  (memory (;0;) 170)
  (global (;0;) (mut i32) (i32.const 11125808))
  (global (;1;) (mut i32) (i32.const 0))
  (global (;2;) (mut i32) (i32.const 0))
  (export "memory" (memory 0))
  (export "start" (func 4))
  (export "asyncify_start_unwind" (func 5))
  (export "asyncify_stop_unwind" (func 6))
  (export "asyncify_start_rewind" (func 7))
  (export "asyncify_stop_rewind" (func 6))
  (export "asyncify_get_state" (func 8))
  (data (;0;) (i32.const 1026) "d\ff\be\be\ff\ffPP\c8\ff\00\00F\ff")
  (data (;1;) (i32.const 1041) "\05\00\00\d0\02\00\00 \04\00\000D8"))
