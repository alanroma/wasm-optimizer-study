[PassRunner] running passes...
[PassRunner]   running pass: strip-dwarf...                        3.811e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: post-emscripten...                    8.8303e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inline-main...                        5.784e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: no-exit-runtime...                    5.7422e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination...     0.000291441 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: memory-packing...                     7.486e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: ssa-nomerge...                        0.00262381 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dce...                                0.000753252 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...                  0.0011354 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...                7.2289e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...              0.000264099 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: pick-load-signs...                    0.000137763 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute-propagate...               0.00718163 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-added-constants-propagate... 0.00247818 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: code-pushing...                       0.000457504 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals-nostructure...        0.00473976 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                             0.000819139 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                     0.000323903 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...                  0.000206596 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-locals...                       0.00115971 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                    0.00118853 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals...                    0.00680379 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                             0.000737976 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                     0.000175984 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                    0.00131082 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                     0.000167056 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                             0.000719268 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: code-folding...                       0.000115163 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                       0.000296513 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...                  0.000758147 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...                6.4884e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                       0.000134918 seconds.
[PassRunner]   (validating)
[PassRunner] passes took 0.0352803 seconds.
[PassRunner] (final validation)
