[PassRunner] running passes...
[PassRunner]   running pass: strip-dwarf...                    3.742e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: post-emscripten...                9.165e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inline-main...                    5.594e-06 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: no-exit-runtime...                5.7323e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 0.000371653 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: memory-packing...                 1.4556e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dce...                            0.00101078 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.00112572 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.000125152 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.00042526 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: pick-load-signs...                0.000138883 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.000410265 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-added-constants...       6.7502e-05 seconds.
[PassRunner]   (validating)
[PassRunner] passes took 0.00384808 seconds.
[PassRunner] (final validation)
