(module
  (type (;0;) (func))
  (type (;1;) (func (param i32)))
  (type (;2;) (func (param i32) (result i32)))
  (import "env" "rand" (func (;0;) (type 2)))
  (import "env" "next_frame" (func (;1;) (type 0)))
  (func (;2;) (type 0)
    nop)
  (func (;3;) (type 0)
    (local i32 i32 i32 i32)
    loop  ;; label = @1
      i32.const 2
      call 0
      local.set 1
      i32.const 1052
      i32.load
      local.get 0
      i32.const 2
      i32.shl
      i32.add
      i32.const 3
      i32.const 0
      local.get 1
      select
      i32.store
      local.get 0
      i32.const 1
      i32.add
      local.tee 0
      i32.const 921600
      i32.ne
      br_if 0 (;@1;)
    end
    loop  ;; label = @1
      i32.const 1052
      i32.load
      local.set 1
      i32.const 0
      local.set 0
      loop  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            block  ;; label = @5
              block  ;; label = @6
                block  ;; label = @7
                  local.get 1
                  local.get 0
                  i32.const 2
                  i32.shl
                  local.tee 3
                  i32.add
                  local.tee 1
                  i32.load
                  br_table 0 (;@7;) 1 (;@6;) 2 (;@5;) 3 (;@4;) 4 (;@3;)
                end
                i32.const 1024
                local.set 2
                br 3 (;@3;)
              end
              local.get 1
              i32.const 0
              i32.store
              i32.const 1036
              local.set 2
              br 2 (;@3;)
            end
            i32.const 1032
            local.set 2
            br 1 (;@3;)
          end
          local.get 1
          i32.const 2
          i32.store
          i32.const 1028
          local.set 2
        end
        i32.const 1048
        i32.load
        local.get 3
        i32.add
        local.get 2
        i32.load
        i32.store
        i32.const 1052
        i32.load
        local.set 1
        local.get 0
        i32.const 1
        i32.add
        local.tee 0
        i32.const 921600
        i32.ne
        br_if 0 (;@2;)
      end
      i32.const 7373872
      i32.const 3687472
      local.get 1
      i32.const 3687472
      i32.eq
      select
      call 4
      call 1
      i32.const 3687464
      i32.load
      i32.const 1
      i32.ne
      br_if 0 (;@1;)
      i32.const 0
      local.set 0
      i32.const 3687464
      i32.const 0
      i32.store
      loop  ;; label = @2
        i32.const 2
        call 0
        local.set 1
        i32.const 1052
        i32.load
        local.get 0
        i32.const 2
        i32.shl
        i32.add
        i32.const 3
        i32.const 0
        local.get 1
        select
        i32.store
        local.get 0
        i32.const 1
        i32.add
        local.tee 0
        i32.const 921600
        i32.ne
        br_if 0 (;@2;)
      end
      br 0 (;@1;)
      unreachable
    end
    unreachable)
  (func (;4;) (type 1) (param i32)
    (local i32 i32 i32 i32 i32 i32 i32 i32 i32)
    loop  ;; label = @1
      local.get 4
      i32.const 2
      i32.shl
      local.tee 3
      i32.const 1052
      i32.load
      i32.add
      local.tee 1
      i32.load offset=4
      i32.const 1
      i32.gt_u
      local.set 2
      block  ;; label = @2
        local.get 6
        if  ;; label = @3
          local.get 2
          local.get 1
          i32.const -5120
          i32.add
          i32.load
          i32.const 1
          i32.gt_u
          i32.add
          local.set 2
          i32.const 0
          local.set 5
          local.get 6
          i32.const 718
          i32.gt_u
          local.tee 7
          if (result i32)  ;; label = @4
            local.get 2
          else
            i32.const 1
            local.set 5
            local.get 2
            local.get 1
            i32.const 5120
            i32.add
            i32.load
            i32.const 1
            i32.gt_u
            i32.add
          end
          local.get 1
          i32.const -5116
          i32.add
          i32.load
          i32.const 1
          i32.gt_u
          i32.add
          local.set 2
          local.get 5
          if  ;; label = @4
            local.get 2
            local.get 1
            i32.const 5124
            i32.add
            i32.load
            i32.const 1
            i32.gt_u
            i32.add
            local.set 2
          end
          local.get 0
          local.get 3
          i32.add
          block (result i32)  ;; label = @4
            i32.const 3
            i32.const 0
            local.get 2
            i32.const 3
            i32.eq
            select
            local.get 1
            i32.load
            i32.const 1
            i32.le_u
            br_if 0 (;@4;)
            drop
            i32.const 2
            i32.const 1
            local.get 2
            i32.const 30
            i32.and
            i32.const 2
            i32.eq
            select
          end
          i32.store
          i32.const 1
          local.set 5
          local.get 4
          i32.const 1
          i32.add
          local.set 4
          loop  ;; label = @4
            i32.const 0
            local.set 2
            local.get 4
            i32.const 2
            i32.shl
            local.tee 9
            i32.const 1052
            i32.load
            i32.add
            local.tee 3
            i32.const -4
            i32.add
            i32.load
            i32.const 1
            i32.gt_u
            local.set 1
            local.get 5
            i32.const 1278
            i32.gt_u
            if (result i32)  ;; label = @5
              i32.const 0
            else
              local.get 1
              local.get 3
              i32.load offset=4
              i32.const 1
              i32.gt_u
              i32.add
              local.set 1
              i32.const 1
            end
            local.set 8
            local.get 1
            local.get 3
            i32.const -5120
            i32.add
            i32.load
            i32.const 1
            i32.gt_u
            i32.add
            local.set 1
            local.get 7
            if (result i32)  ;; label = @5
              local.get 1
            else
              i32.const 1
              local.set 2
              local.get 1
              local.get 3
              i32.const 5120
              i32.add
              i32.load
              i32.const 1
              i32.gt_u
              i32.add
            end
            local.get 3
            i32.const -5124
            i32.add
            i32.load
            i32.const 1
            i32.gt_u
            i32.add
            local.set 1
            local.get 8
            if  ;; label = @5
              local.get 1
              local.get 3
              i32.const -5116
              i32.add
              i32.load
              i32.const 1
              i32.gt_u
              i32.add
              local.set 1
            end
            local.get 2
            if  ;; label = @5
              local.get 1
              local.get 3
              i32.const 5116
              i32.add
              i32.load
              i32.const 1
              i32.gt_u
              i32.add
              local.set 1
            end
            local.get 2
            local.get 8
            i32.and
            i32.const 1
            i32.eq
            if  ;; label = @5
              local.get 1
              local.get 3
              i32.const 5124
              i32.add
              i32.load
              i32.const 1
              i32.gt_u
              i32.add
              local.set 1
            end
            local.get 0
            local.get 9
            i32.add
            block (result i32)  ;; label = @5
              i32.const 3
              i32.const 0
              local.get 1
              i32.const 3
              i32.eq
              select
              local.get 3
              i32.load
              i32.const 1
              i32.le_u
              br_if 0 (;@5;)
              drop
              i32.const 2
              i32.const 1
              local.get 1
              i32.const 254
              i32.and
              i32.const 2
              i32.eq
              select
            end
            i32.store
            local.get 4
            i32.const 1
            i32.add
            local.set 4
            local.get 5
            i32.const 1
            i32.add
            local.tee 5
            i32.const 1280
            i32.ne
            br_if 0 (;@4;)
          end
          br 1 (;@2;)
        end
        local.get 2
        local.get 1
        i32.const 5120
        i32.add
        i32.load
        i32.const 1
        i32.gt_u
        i32.add
        local.get 1
        i32.const 5124
        i32.add
        i32.load
        i32.const 1
        i32.gt_u
        i32.add
        local.set 2
        local.get 0
        local.get 3
        i32.add
        block (result i32)  ;; label = @3
          i32.const 3
          i32.const 0
          local.get 2
          i32.const 3
          i32.eq
          select
          local.get 1
          i32.load
          i32.const 1
          i32.le_u
          br_if 0 (;@3;)
          drop
          i32.const 2
          i32.const 1
          local.get 2
          i32.const 6
          i32.and
          i32.const 2
          i32.eq
          select
        end
        i32.store
        i32.const 1
        local.set 2
        local.get 4
        i32.const 1
        i32.add
        local.set 4
        loop  ;; label = @3
          local.get 4
          i32.const 2
          i32.shl
          local.tee 9
          i32.const 1052
          i32.load
          local.tee 3
          i32.add
          local.tee 1
          i32.const -4
          i32.add
          i32.load
          i32.const 1
          i32.gt_u
          local.set 5
          block (result i32)  ;; label = @4
            local.get 2
            i32.const 1278
            i32.gt_u
            if  ;; label = @5
              i32.const 1280
              local.set 7
              i32.const 1279
              br 1 (;@4;)
            end
            local.get 5
            local.get 1
            i32.load offset=4
            i32.const 1
            i32.gt_u
            i32.add
            local.get 1
            i32.const 5120
            i32.add
            i32.load
            i32.const 1
            i32.gt_u
            i32.add
            local.set 5
            i32.const 1279
            local.set 7
            i32.const 1281
          end
          local.set 8
          local.get 5
          local.get 3
          local.get 4
          local.get 7
          i32.add
          i32.const 2
          i32.shl
          i32.add
          i32.load
          i32.const 1
          i32.gt_u
          i32.add
          local.get 3
          local.get 4
          local.get 8
          i32.add
          i32.const 2
          i32.shl
          i32.add
          i32.load
          i32.const 1
          i32.gt_u
          i32.add
          local.set 5
          local.get 0
          local.get 9
          i32.add
          block (result i32)  ;; label = @4
            i32.const 2
            i32.const 1
            local.get 5
            i32.const 30
            i32.and
            i32.const 2
            i32.eq
            select
            local.get 1
            i32.load
            i32.const 2
            i32.ge_u
            br_if 0 (;@4;)
            drop
            i32.const 3
            i32.const 0
            local.get 5
            i32.const 3
            i32.eq
            select
          end
          i32.store
          local.get 4
          i32.const 1
          i32.add
          local.set 4
          local.get 2
          i32.const 1
          i32.add
          local.tee 2
          i32.const 1280
          i32.ne
          br_if 0 (;@3;)
        end
      end
      local.get 6
      i32.const 1
      i32.add
      local.tee 6
      i32.const 720
      i32.ne
      br_if 0 (;@1;)
    end
    i32.const 1052
    local.get 0
    i32.store)
  (memory (;0;) 256 256)
  (export "memory" (memory 0))
  (export "__wasm_call_ctors" (func 2))
  (export "start" (func 3))
  (data (;0;) (i32.const 1026) "d\ff\be\be\ff\ffPP\c8\ff\00\00F\ff\00\05\00\00\d0\02\00\00 \04\00\000D8")
  (data (;1;) (i32.const 11060272) "\d0\c4\f8"))
