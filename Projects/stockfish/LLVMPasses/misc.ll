; ModuleID = 'misc.cpp'
source_filename = "misc.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%"class.std::__2::vector" = type { %"class.std::__2::__vector_base" }
%"class.std::__2::__vector_base" = type { %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"*, %"class.std::__2::__compressed_pair.1" }
%"class.std::__2::basic_string" = type { %"class.std::__2::__compressed_pair" }
%"class.std::__2::__compressed_pair" = type { %"struct.std::__2::__compressed_pair_elem" }
%"struct.std::__2::__compressed_pair_elem" = type { %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep" }
%"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep" = type { %union.anon }
%union.anon = type { %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long" }
%"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long" = type { i8*, i32, i32 }
%"class.std::__2::__compressed_pair.1" = type { %"struct.std::__2::__compressed_pair_elem.2" }
%"struct.std::__2::__compressed_pair_elem.2" = type { %"class.std::__2::basic_string"* }
%"class.std::__2::basic_ostream" = type { i32 (...)**, %"class.std::__2::basic_ios" }
%"class.std::__2::basic_ios" = type { %"class.std::__2::ios_base", %"class.std::__2::basic_ostream"*, i32 }
%"class.std::__2::ios_base" = type { i32 (...)**, i32, i32, i32, i32, i32, i8*, i8*, void (i32, %"class.std::__2::ios_base"*, i32)**, i32*, i32, i32, i32*, i32, i32, i8**, i32, i32 }
%"class.std::__2::mutex" = type { %struct.pthread_mutex_t }
%struct.pthread_mutex_t = type { %union.anon.6 }
%union.anon.6 = type { [7 x i32] }
%"class.(anonymous namespace)::Logger" = type { %"class.std::__2::basic_ofstream", %"struct.(anonymous namespace)::Tie", %"struct.(anonymous namespace)::Tie" }
%"class.std::__2::basic_ofstream" = type { %"class.std::__2::basic_ostream.base", %"class.std::__2::basic_filebuf", %"class.std::__2::basic_ios" }
%"class.std::__2::basic_ostream.base" = type { i32 (...)** }
%"class.std::__2::basic_filebuf" = type <{ %"class.std::__2::basic_streambuf", i8*, i8*, i8*, [8 x i8], i32, i8*, i32, %struct._IO_FILE*, %"class.std::__2::codecvt"*, %struct.__mbstate_t, %struct.__mbstate_t, i32, i32, i8, i8, i8, i8 }>
%"class.std::__2::basic_streambuf" = type { i32 (...)**, %"class.std::__2::locale", i8*, i8*, i8*, i8*, i8*, i8* }
%"class.std::__2::locale" = type { %"class.std::__2::locale::__imp"* }
%"class.std::__2::locale::__imp" = type opaque
%struct._IO_FILE = type opaque
%"class.std::__2::codecvt" = type { %"class.std::__2::locale::facet" }
%"class.std::__2::locale::facet" = type { %"class.std::__2::__shared_count" }
%"class.std::__2::__shared_count" = type { i32 (...)**, i32 }
%struct.__mbstate_t = type { i32, i32 }
%"struct.(anonymous namespace)::Tie" = type { %"class.std::__2::basic_streambuf", %"class.std::__2::basic_streambuf"*, %"class.std::__2::basic_streambuf"* }
%"class.std::__2::basic_istream" = type { i32 (...)**, i32, %"class.std::__2::basic_ios" }
%"class.std::__2::locale::id" = type { %"struct.std::__2::once_flag", i32 }
%"struct.std::__2::once_flag" = type { i32 }
%"class.std::initializer_list" = type { %"class.std::__2::basic_string"*, i32 }
%"struct.std::__2::__default_init_tag" = type { i8 }
%"class.std::__2::__basic_string_common" = type { i8 }
%"class.std::__2::basic_stringstream" = type { %"class.std::__2::basic_iostream.base", %"class.std::__2::basic_stringbuf", %"class.std::__2::basic_ios" }
%"class.std::__2::basic_iostream.base" = type { %"class.std::__2::basic_istream.base", %"class.std::__2::basic_ostream.base" }
%"class.std::__2::basic_istream.base" = type { i32 (...)**, i32 }
%"class.std::__2::basic_stringbuf" = type { %"class.std::__2::basic_streambuf", %"class.std::__2::basic_string", i8*, i32 }
%"class.std::__2::__iom_t4" = type { i8 }
%"class.std::__2::basic_iostream" = type { %"class.std::__2::basic_istream.base", %"class.std::__2::basic_ostream.base", %"class.std::__2::basic_ios" }
%"class.std::__2::fpos" = type { %struct.__mbstate_t, i64 }
%"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short" = type { [11 x i8], %struct.anon }
%struct.anon = type { i8 }
%"struct.std::__2::__less" = type { i8 }
%"class.std::__2::unique_ptr" = type { %"class.std::__2::__compressed_pair.7" }
%"class.std::__2::__compressed_pair.7" = type { %"struct.std::__2::__compressed_pair_elem.8", %"struct.std::__2::__compressed_pair_elem.9" }
%"struct.std::__2::__compressed_pair_elem.8" = type { %struct._IO_FILE* }
%"struct.std::__2::__compressed_pair_elem.9" = type { i32 (%struct._IO_FILE*)* }
%"struct.std::__2::__less.10" = type { i8 }
%"struct.std::__2::__less.11" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.0" = type { i8 }
%"class.std::__2::allocator" = type { i8 }
%"class.std::__2::__vector_base_common" = type { i8 }
%"class.std::__2::allocator.4" = type { i8 }
%"struct.std::__2::vector<std::__2::basic_string<char>, std::__2::allocator<std::__2::basic_string<char>>>::_ConstructTransaction" = type { %"class.std::__2::vector"*, %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"* }
%"struct.std::__2::__compressed_pair_elem.3" = type { i8 }
%"struct.std::__2::integral_constant" = type { i8 }
%"struct.std::__2::__has_max_size" = type { i8 }
%"struct.std::__2::__has_construct" = type { i8 }
%"struct.std::__2::__has_destroy" = type { i8 }
%"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__raw" = type { [3 x i32] }
%"class.std::__2::basic_ostream<char, std::__2::char_traits<char>>::sentry" = type { i8, %"class.std::__2::basic_ostream"* }
%"class.std::__2::ostreambuf_iterator" = type { %"class.std::__2::basic_streambuf"* }
%"struct.std::__2::iterator" = type { i8 }
%"class.std::__2::ctype" = type <{ %"class.std::__2::locale::facet", i16*, i8, [3 x i8] }>
%"struct.std::__2::random_access_iterator_tag" = type { i8 }
%"struct.std::__2::__has_max_size.12" = type { i8 }

$_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2IDnEEPKc = comdat any

$_ZNSt3__26vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEEC2ESt16initializer_listIS6_E = comdat any

$_ZNSt3__26vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEED2Ev = comdat any

$_ZNSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEC1Ej = comdat any

$_ZNSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKNS_12basic_stringIcS2_S4_EEj = comdat any

$_ZNSt3__2lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_RKNS_8__iom_t4IcEE = comdat any

$_ZNSt3__2lsIcNS_11char_traitsIcEENS_9allocatorIcEEEERNS_13basic_ostreamIT_T0_EES9_RKNS_12basic_stringIS6_S7_T1_EE = comdat any

$_ZNSt3__2lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc = comdat any

$_ZNSt3__27setfillIcEENS_8__iom_t4IT_EES2_ = comdat any

$_ZNKSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEE3strEv = comdat any

$_ZNSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev = comdat any

$_ZNSt3__213basic_ostreamIcNS_11char_traitsIcEEElsEPFRS3_S4_E = comdat any

$_ZNSt3__24endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_ = comdat any

$_ZNSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED2Ev = comdat any

$_ZThn8_NSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev = comdat any

$_ZTv0_n12_NSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev = comdat any

$_ZNSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev = comdat any

$_ZThn8_NSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev = comdat any

$_ZTv0_n12_NSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev = comdat any

$_ZNSt3__215basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED2Ev = comdat any

$_ZNSt3__215basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev = comdat any

$_ZNSt3__215basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekoffExNS_8ios_base7seekdirEj = comdat any

$_ZNSt3__215basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekposENS_4fposI11__mbstate_tEEj = comdat any

$_ZNSt3__215basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9underflowEv = comdat any

$_ZNSt3__215basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9pbackfailEi = comdat any

$_ZNSt3__215basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE8overflowEi = comdat any

$_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE4pptrEv = comdat any

$_ZNSt3__24fposI11__mbstate_tEC2Ex = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv = comdat any

$_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE4gptrEv = comdat any

$_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE5ebackEv = comdat any

$_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE5pbaseEv = comdat any

$_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE4setgEPcS4_S4_ = comdat any

$_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE4setpEPcS4_ = comdat any

$_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE5epptrEv = comdat any

$_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE5pbumpEi = comdat any

$_ZNSt3__212__to_addressIKcEEPT_S3_ = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE13__get_pointerEv = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE9__is_longEv = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE18__get_long_pointerEv = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE19__get_short_pointerEv = comdat any

$_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EE5__getEv = comdat any

$_ZNSt3__214pointer_traitsIPKcE10pointer_toERS1_ = comdat any

$_ZNSt3__29addressofIKcEEPT_RS2_ = comdat any

$_ZNKSt3__24fposI11__mbstate_tEcvxEv = comdat any

$_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE5egptrEv = comdat any

$_ZNSt3__211char_traitsIcE11to_int_typeEc = comdat any

$_ZNSt3__211char_traitsIcE3eofEv = comdat any

$_ZNSt3__211char_traitsIcE11eq_int_typeEii = comdat any

$_ZNSt3__211char_traitsIcE7not_eofEi = comdat any

$_ZNSt3__211char_traitsIcE2eqEcc = comdat any

$_ZNSt3__211char_traitsIcE12to_char_typeEi = comdat any

$_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6resizeEm = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE8capacityEv = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4sizeEv = comdat any

$_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE7__pbumpEl = comdat any

$_ZNSt3__23maxIPcEERKT_S4_S4_ = comdat any

$_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE5sputcEc = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE14__get_long_capEv = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE15__get_long_sizeEv = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE16__get_short_sizeEv = comdat any

$_ZNSt3__23maxIPcNS_6__lessIS1_S1_EEEERKT_S6_S6_T0_ = comdat any

$_ZNKSt3__26__lessIPcS1_EclERKS1_S4_ = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5emptyEv = comdat any

$_ZNKSt3__214basic_ofstreamIcNS_11char_traitsIcEEE7is_openEv = comdat any

$_ZNSt3__214basic_ofstreamIcNS_11char_traitsIcEEE4openERKNS_12basic_stringIcS2_NS_9allocatorIcEEEEj = comdat any

$_ZNSt3__29basic_iosIcNS_11char_traitsIcEEE5rdbufEPNS_15basic_streambufIcS2_EE = comdat any

$_ZNSt3__214basic_ofstreamIcNS_11char_traitsIcEEE5closeEv = comdat any

$_ZNSt3__214basic_ofstreamIcNS_11char_traitsIcEEEC1Ev = comdat any

$_ZNKSt3__29basic_iosIcNS_11char_traitsIcEEE5rdbufEv = comdat any

$_ZNKSt3__214basic_ofstreamIcNS_11char_traitsIcEEE5rdbufEv = comdat any

$_ZNSt3__29basic_iosIcNS_11char_traitsIcEEEC2Ev = comdat any

$_ZNSt3__213basic_ostreamIcNS_11char_traitsIcEEEC2EPNS_15basic_streambufIcS2_EE = comdat any

$_ZNSt3__213basic_filebufIcNS_11char_traitsIcEEEC2Ev = comdat any

$_ZNSt3__214basic_ofstreamIcNS_11char_traitsIcEEED1Ev = comdat any

$_ZNSt3__214basic_ofstreamIcNS_11char_traitsIcEEED0Ev = comdat any

$_ZTv0_n12_NSt3__214basic_ofstreamIcNS_11char_traitsIcEEED1Ev = comdat any

$_ZTv0_n12_NSt3__214basic_ofstreamIcNS_11char_traitsIcEEED0Ev = comdat any

$_ZNSt3__28ios_baseC2Ev = comdat any

$_ZNSt3__29basic_iosIcNS_11char_traitsIcEEE4initEPNS_15basic_streambufIcS2_EE = comdat any

$_ZNSt3__29has_facetINS_7codecvtIcc11__mbstate_tEEEEbRKNS_6localeE = comdat any

$_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE6getlocEv = comdat any

$_ZNSt3__29use_facetINS_7codecvtIcc11__mbstate_tEEEERKT_RKNS_6localeE = comdat any

$_ZNKSt3__27codecvtIcc11__mbstate_tE13always_noconvEv = comdat any

$_ZNSt3__213basic_filebufIcNS_11char_traitsIcEEED2Ev = comdat any

$_ZNSt3__213basic_filebufIcNS_11char_traitsIcEEED0Ev = comdat any

$_ZNSt3__213basic_filebufIcNS_11char_traitsIcEEE5imbueERKNS_6localeE = comdat any

$_ZNSt3__213basic_filebufIcNS_11char_traitsIcEEE6setbufEPcl = comdat any

$_ZNSt3__213basic_filebufIcNS_11char_traitsIcEEE7seekoffExNS_8ios_base7seekdirEj = comdat any

$_ZNSt3__213basic_filebufIcNS_11char_traitsIcEEE7seekposENS_4fposI11__mbstate_tEEj = comdat any

$_ZNSt3__213basic_filebufIcNS_11char_traitsIcEEE4syncEv = comdat any

$_ZNSt3__213basic_filebufIcNS_11char_traitsIcEEE9underflowEv = comdat any

$_ZNSt3__213basic_filebufIcNS_11char_traitsIcEEE9pbackfailEi = comdat any

$_ZNSt3__213basic_filebufIcNS_11char_traitsIcEEE8overflowEi = comdat any

$_ZNSt3__213basic_filebufIcNS_11char_traitsIcEEE5closeEv = comdat any

$_ZNSt3__210unique_ptrI8_IO_FILEPFiPS1_EEC2ILb1EvEES2_NS_16__dependent_typeINS_27__unique_ptr_deleter_sfinaeIS4_EEXT_EE20__good_rval_ref_typeE = comdat any

$_ZNSt3__210unique_ptrI8_IO_FILEPFiPS1_EE7releaseEv = comdat any

$_ZNSt3__210unique_ptrI8_IO_FILEPFiPS1_EED2Ev = comdat any

$_ZNSt3__24moveIRPFiP8_IO_FILEEEEONS_16remove_referenceIT_E4typeEOS7_ = comdat any

$_ZNSt3__217__compressed_pairIP8_IO_FILEPFiS2_EEC2IRS2_S4_EEOT_OT0_ = comdat any

$_ZNSt3__27forwardIRP8_IO_FILEEEOT_RNS_16remove_referenceIS4_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIP8_IO_FILELi0ELb0EEC2IRS2_vEEOT_ = comdat any

$_ZNSt3__27forwardIPFiP8_IO_FILEEEEOT_RNS_16remove_referenceIS5_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPFiP8_IO_FILEELi1ELb0EEC2IS4_vEEOT_ = comdat any

$_ZNSt3__217__compressed_pairIP8_IO_FILEPFiS2_EE5firstEv = comdat any

$_ZNSt3__222__compressed_pair_elemIP8_IO_FILELi0ELb0EE5__getEv = comdat any

$_ZNSt3__210unique_ptrI8_IO_FILEPFiPS1_EE5resetES2_ = comdat any

$_ZNSt3__217__compressed_pairIP8_IO_FILEPFiS2_EE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemIPFiP8_IO_FILEELi1ELb0EE5__getEv = comdat any

$_ZNSt3__23maxIlEERKT_S3_S3_ = comdat any

$_ZNSt3__23maxIlNS_6__lessIllEEEERKT_S5_S5_T0_ = comdat any

$_ZNKSt3__26__lessIllEclERKlS3_ = comdat any

$_ZNSt3__216__throw_bad_castEv = comdat any

$_ZNKSt3__27codecvtIcc11__mbstate_tE8encodingEv = comdat any

$_ZNSt3__24fposI11__mbstate_tE5stateES1_ = comdat any

$_ZNKSt3__24fposI11__mbstate_tE5stateEv = comdat any

$_ZNKSt3__27codecvtIcc11__mbstate_tE7unshiftERS1_PcS4_RS4_ = comdat any

$_ZNKSt3__27codecvtIcc11__mbstate_tE6lengthERS1_PKcS5_m = comdat any

$_ZNSt3__213basic_filebufIcNS_11char_traitsIcEEE11__read_modeEv = comdat any

$_ZNSt3__23minImEERKT_S3_S3_ = comdat any

$_ZNKSt3__27codecvtIcc11__mbstate_tE2inERS1_PKcS5_RS5_PcS7_RS7_ = comdat any

$_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_ = comdat any

$_ZNKSt3__26__lessImmEclERKmS3_ = comdat any

$_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE5gbumpEi = comdat any

$_ZNSt3__213basic_filebufIcNS_11char_traitsIcEEE12__write_modeEv = comdat any

$_ZNKSt3__27codecvtIcc11__mbstate_tE3outERS1_PKcS5_RS5_PcS7_RS7_ = comdat any

$_ZNSt3__214basic_ofstreamIcNS_11char_traitsIcEEED2Ev = comdat any

$_ZNKSt3__28ios_base5rdbufEv = comdat any

$_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE7pubsyncEv = comdat any

$_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE5sgetcEv = comdat any

$_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE6sbumpcEv = comdat any

$_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE5sputnEPKcl = comdat any

$_ZNKSt3__213basic_filebufIcNS_11char_traitsIcEEE7is_openEv = comdat any

$_ZNSt3__213basic_filebufIcNS_11char_traitsIcEEE4openERKNS_12basic_stringIcS2_NS_9allocatorIcEEEEj = comdat any

$_ZNSt3__29basic_iosIcNS_11char_traitsIcEEE5clearEj = comdat any

$_ZNSt3__29basic_iosIcNS_11char_traitsIcEEE8setstateEj = comdat any

$_ZNSt3__213basic_filebufIcNS_11char_traitsIcEEE4openEPKcj = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5c_strEv = comdat any

$_ZNSt3__213basic_filebufIcNS_11char_traitsIcEEE15__make_mdstringEj = comdat any

$_ZNSt3__28ios_base8setstateEj = comdat any

$_ZNSt3__28ios_base5rdbufEPv = comdat any

$_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_EC2INS_18__default_init_tagESA_EEOT_OT0_ = comdat any

$_ZNSt3__211char_traitsIcE6lengthEPKc = comdat any

$_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIcEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__29allocatorIcEC2Ev = comdat any

$_ZNSt3__213__vector_baseINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEEC2Ev = comdat any

$_ZNKSt16initializer_listINSt3__212basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEEE4sizeEv = comdat any

$_ZNSt3__26vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE11__vallocateEm = comdat any

$_ZNSt3__26vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE18__construct_at_endIPKS6_EENS_9enable_ifIXsr27__is_cpp17_forward_iteratorIT_EE5valueEvE4typeESD_SD_m = comdat any

$_ZNKSt16initializer_listINSt3__212basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEEE5beginEv = comdat any

$_ZNKSt16initializer_listINSt3__212basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEEE3endEv = comdat any

$_ZNSt3__220__vector_base_commonILb1EEC2Ev = comdat any

$_ZNSt3__217__compressed_pairIPNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEEC2IDnNS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEELi0ELb0EEC2IDnvEEOT_ = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__29allocatorINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEEEC2Ev = comdat any

$_ZNKSt3__26vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE8max_sizeEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEEEE8allocateERS7_m = comdat any

$_ZNSt3__213__vector_baseINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE7__allocEv = comdat any

$_ZNSt3__213__vector_baseINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE9__end_capEv = comdat any

$_ZNKSt3__26vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE14__annotate_newEm = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEEEE8max_sizeERKS7_ = comdat any

$_ZNKSt3__213__vector_baseINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE7__allocEv = comdat any

$_ZNSt3__214numeric_limitsIlE3maxEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS7_ = comdat any

$_ZNKSt3__29allocatorINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEEE8max_sizeEv = comdat any

$_ZNKSt3__217__compressed_pairIPNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE6secondEv = comdat any

$_ZNKSt3__222__compressed_pair_elemINS_9allocatorINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv = comdat any

$_ZNSt3__29allocatorINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEEE8allocateEmPKv = comdat any

$_ZNSt3__220__throw_length_errorEPKc = comdat any

$_ZNSt3__217__libcpp_allocateEmm = comdat any

$_ZNSt3__217__compressed_pairIPNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE5firstEv = comdat any

$_ZNSt3__222__compressed_pair_elemIPNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEELi0ELb0EE5__getEv = comdat any

$_ZNKSt3__26vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE31__annotate_contiguous_containerEPKvSA_SA_SA_ = comdat any

$_ZNKSt3__26vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE4dataEv = comdat any

$_ZNKSt3__26vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE8capacityEv = comdat any

$_ZNSt3__212__to_addressINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEEPT_S8_ = comdat any

$_ZNKSt3__213__vector_baseINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__26vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE21_ConstructTransactionC2ERS8_m = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEEEE25__construct_range_forwardIPKS6_PS6_EEvRS7_T_SE_RT0_ = comdat any

$_ZNSt3__26vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE21_ConstructTransactionD2Ev = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEEEE9constructIS6_JRKS6_EEEvRS7_PT_DpOT0_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEEEE11__constructIS6_JRKS6_EEEvNS_17integral_constantIbLb1EEERS7_PT_DpOT0_ = comdat any

$_ZNSt3__27forwardIRKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEEOT_RNS_16remove_referenceIS9_E4typeE = comdat any

$_ZNSt3__29allocatorINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEEE9constructIS5_JRKS5_EEEvPT_DpOT0_ = comdat any

$_ZNKSt3__26vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE17__annotate_deleteEv = comdat any

$_ZNSt3__213__vector_baseINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEED2Ev = comdat any

$_ZNKSt3__26vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE4sizeEv = comdat any

$_ZNSt3__213__vector_baseINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE5clearEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEEEE10deallocateERS7_PS6_m = comdat any

$_ZNSt3__213__vector_baseINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE17__destruct_at_endEPS6_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEEEE7destroyIS6_EEvRS7_PT_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEEEE9__destroyIS6_EEvNS_17integral_constantIbLb1EEERS7_PT_ = comdat any

$_ZNSt3__29allocatorINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEEE7destroyEPS5_ = comdat any

$_ZNSt3__29allocatorINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEEE10deallocateEPS5_m = comdat any

$_ZNSt3__219__libcpp_deallocateEPvmm = comdat any

$_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm = comdat any

$_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm = comdat any

$_ZNSt3__217_DeallocateCaller9__do_callEPv = comdat any

$_ZNSt3__214basic_iostreamIcNS_11char_traitsIcEEEC2EPNS_15basic_streambufIcS2_EE = comdat any

$_ZNSt3__215basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEC2Ej = comdat any

$_ZNSt3__213basic_istreamIcNS_11char_traitsIcEEEC2EPNS_15basic_streambufIcS2_EE = comdat any

$_ZNSt3__213basic_ostreamIcNS_11char_traitsIcEEEC2Ev = comdat any

$_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2Ev = comdat any

$_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__zeroEv = comdat any

$_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EE5__getEv = comdat any

$_ZNSt3__215basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEC2ERKNS_12basic_stringIcS2_S4_EEj = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE13get_allocatorEv = comdat any

$_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2ERKS4_ = comdat any

$_ZNSt3__215basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE3strERKNS_12basic_stringIcS2_S4_EE = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE7__allocEv = comdat any

$_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E6secondEv = comdat any

$_ZNKSt3__222__compressed_pair_elemINS_9allocatorIcEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_EC2INS_18__default_init_tagERKS5_EEOT_OT0_ = comdat any

$_ZNSt3__27forwardIRKNS_9allocatorIcEEEEOT_RNS_16remove_referenceIS5_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIcEELi1ELb1EEC2IRKS2_vEEOT_ = comdat any

$_ZNSt3__224__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m = comdat any

$_ZNKSt3__213basic_ostreamIcNS_11char_traitsIcEEE6sentrycvbEv = comdat any

$_ZNSt3__216__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_ = comdat any

$_ZNSt3__219ostreambuf_iteratorIcNS_11char_traitsIcEEEC2ERNS_13basic_ostreamIcS2_EE = comdat any

$_ZNKSt3__28ios_base5flagsEv = comdat any

$_ZNKSt3__29basic_iosIcNS_11char_traitsIcEEE4fillEv = comdat any

$_ZNKSt3__219ostreambuf_iteratorIcNS_11char_traitsIcEEE6failedEv = comdat any

$_ZNKSt3__28ios_base5widthEv = comdat any

$_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2Emc = comdat any

$_ZNSt3__28ios_base5widthEl = comdat any

$_ZNKSt3__29basic_iosIcNS_11char_traitsIcEEE5widenEc = comdat any

$_ZNSt3__29use_facetINS_5ctypeIcEEEERKT_RKNS_6localeE = comdat any

$_ZNKSt3__25ctypeIcE5widenEc = comdat any

$_ZNSt3__28__iom_t4IcEC2Ec = comdat any

$_ZNSt3__29basic_iosIcNS_11char_traitsIcEEE4fillEc = comdat any

$_ZNKSt3__215basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE3strEv = comdat any

$_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2IPcvEET_S8_RKS4_ = comdat any

$_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initIPcEENS_9enable_ifIXsr27__is_cpp17_forward_iteratorIT_EE5valueEvE4typeES9_S9_ = comdat any

$_ZNSt3__28distanceIPcEENS_15iterator_traitsIT_E15difference_typeES3_S3_ = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE8max_sizeEv = comdat any

$_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE16__set_short_sizeEm = comdat any

$_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE19__get_short_pointerEv = comdat any

$_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE11__recommendEm = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIcEEE8allocateERS2_m = comdat any

$_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE7__allocEv = comdat any

$_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE18__set_long_pointerEPc = comdat any

$_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE14__set_long_capEm = comdat any

$_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE15__set_long_sizeEm = comdat any

$_ZNSt3__211char_traitsIcE6assignERcRKc = comdat any

$_ZNSt3__210__distanceIPcEENS_15iterator_traitsIT_E15difference_typeES3_S3_NS_26random_access_iterator_tagE = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIcEEE8max_sizeERKS2_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIcEEE10__max_sizeENS_17integral_constantIbLb1EEERKS2_ = comdat any

$_ZNKSt3__29allocatorIcE8max_sizeEv = comdat any

$_ZNSt3__214pointer_traitsIPcE10pointer_toERc = comdat any

$_ZNSt3__29addressofIcEEPT_RS1_ = comdat any

$_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE10__align_itILm16EEEmm = comdat any

$_ZNSt3__29allocatorIcE8allocateEmPKv = comdat any

$_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIcEELi1ELb1EE5__getEv = comdat any

$_ZTVNSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE = comdat any

$_ZTTNSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE = comdat any

$_ZTCNSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE0_NS_14basic_iostreamIcS2_EE = comdat any

$_ZTCNSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE0_NS_13basic_istreamIcS2_EE = comdat any

$_ZTCNSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE8_NS_13basic_ostreamIcS2_EE = comdat any

$_ZTSNSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE = comdat any

$_ZTINSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE = comdat any

$_ZTVNSt3__215basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE = comdat any

$_ZTSNSt3__215basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE = comdat any

$_ZTINSt3__215basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE = comdat any

$_ZTVNSt3__214basic_ofstreamIcNS_11char_traitsIcEEEE = comdat any

$_ZTTNSt3__214basic_ofstreamIcNS_11char_traitsIcEEEE = comdat any

$_ZTCNSt3__214basic_ofstreamIcNS_11char_traitsIcEEEE0_NS_13basic_ostreamIcS2_EE = comdat any

$_ZTSNSt3__214basic_ofstreamIcNS_11char_traitsIcEEEE = comdat any

$_ZTINSt3__214basic_ofstreamIcNS_11char_traitsIcEEEE = comdat any

$_ZTVNSt3__213basic_filebufIcNS_11char_traitsIcEEEE = comdat any

$_ZTSNSt3__213basic_filebufIcNS_11char_traitsIcEEEE = comdat any

$_ZTINSt3__213basic_filebufIcNS_11char_traitsIcEEEE = comdat any

@_ZL8variants = internal global %"class.std::__2::vector" zeroinitializer, align 4
@.str = private unnamed_addr constant [6 x i8] c"chess\00", align 1
@__dso_handle = external hidden global i8
@_ZN12_GLOBAL__N_17VersionE = internal global %"class.std::__2::basic_string" zeroinitializer, align 4
@.str.2 = private unnamed_addr constant [1 x i8] zeroinitializer, align 1
@.str.4 = private unnamed_addr constant [12 x i8] c"Nov  5 2020\00", align 1
@.str.5 = private unnamed_addr constant [14 x i8] c"Stockfish.js \00", align 1
@.str.6 = private unnamed_addr constant [95 x i8] c" by T. Romstad, M. Costalba, J. Kiiski, G. Linscott, D. Dugovic, F. Fichter, N. Fiekas, et al.\00", align 1
@_ZL4hits = internal global [2 x i64] zeroinitializer, align 16
@_ZL5means = internal global [2 x i64] zeroinitializer, align 16
@_ZNSt3__24cerrE = external global %"class.std::__2::basic_ostream", align 4
@.str.7 = private unnamed_addr constant [7 x i8] c"Total \00", align 1
@.str.8 = private unnamed_addr constant [7 x i8] c" Hits \00", align 1
@.str.9 = private unnamed_addr constant [15 x i8] c" hit rate (%) \00", align 1
@.str.10 = private unnamed_addr constant [7 x i8] c" Mean \00", align 1
@_ZZlsRNSt3__213basic_ostreamIcNS_11char_traitsIcEEEE8SyncCoutE1m = internal global %"class.std::__2::mutex" zeroinitializer, align 4
@_ZGVZlsRNSt3__213basic_ostreamIcNS_11char_traitsIcEEEE8SyncCoutE1m = internal global i32 0, align 4
@_ZTVNSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE = linkonce_odr unnamed_addr constant { [5 x i8*], [5 x i8*], [5 x i8*] } { [5 x i8*] [i8* inttoptr (i32 64 to i8*), i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTINSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE to i8*), i8* bitcast (%"class.std::__2::basic_stringstream"* (%"class.std::__2::basic_stringstream"*)* @_ZNSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev to i8*), i8* bitcast (void (%"class.std::__2::basic_stringstream"*)* @_ZNSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev to i8*)], [5 x i8*] [i8* inttoptr (i32 56 to i8*), i8* inttoptr (i32 -8 to i8*), i8* bitcast ({ i8*, i8*, i8* }* @_ZTINSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE to i8*), i8* bitcast (%"class.std::__2::basic_stringstream"* (%"class.std::__2::basic_stringstream"*)* @_ZThn8_NSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev to i8*), i8* bitcast (void (%"class.std::__2::basic_stringstream"*)* @_ZThn8_NSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev to i8*)], [5 x i8*] [i8* inttoptr (i32 -64 to i8*), i8* inttoptr (i32 -64 to i8*), i8* bitcast ({ i8*, i8*, i8* }* @_ZTINSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE to i8*), i8* bitcast (%"class.std::__2::basic_stringstream"* (%"class.std::__2::basic_stringstream"*)* @_ZTv0_n12_NSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev to i8*), i8* bitcast (void (%"class.std::__2::basic_stringstream"*)* @_ZTv0_n12_NSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev to i8*)] }, comdat, align 4
@_ZTTNSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE = linkonce_odr unnamed_addr constant [10 x i8*] [i8* bitcast (i8** getelementptr inbounds ({ [5 x i8*], [5 x i8*], [5 x i8*] }, { [5 x i8*], [5 x i8*], [5 x i8*] }* @_ZTVNSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE, i32 0, inrange i32 0, i32 3) to i8*), i8* bitcast (i8** getelementptr inbounds ({ [5 x i8*], [5 x i8*], [5 x i8*] }, { [5 x i8*], [5 x i8*], [5 x i8*] }* @_ZTCNSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE0_NS_14basic_iostreamIcS2_EE, i32 0, inrange i32 0, i32 3) to i8*), i8* bitcast (i8** getelementptr inbounds ({ [5 x i8*], [5 x i8*] }, { [5 x i8*], [5 x i8*] }* @_ZTCNSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE0_NS_13basic_istreamIcS2_EE, i32 0, inrange i32 0, i32 3) to i8*), i8* bitcast (i8** getelementptr inbounds ({ [5 x i8*], [5 x i8*] }, { [5 x i8*], [5 x i8*] }* @_ZTCNSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE0_NS_13basic_istreamIcS2_EE, i32 0, inrange i32 1, i32 3) to i8*), i8* bitcast (i8** getelementptr inbounds ({ [5 x i8*], [5 x i8*] }, { [5 x i8*], [5 x i8*] }* @_ZTCNSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE8_NS_13basic_ostreamIcS2_EE, i32 0, inrange i32 0, i32 3) to i8*), i8* bitcast (i8** getelementptr inbounds ({ [5 x i8*], [5 x i8*] }, { [5 x i8*], [5 x i8*] }* @_ZTCNSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE8_NS_13basic_ostreamIcS2_EE, i32 0, inrange i32 1, i32 3) to i8*), i8* bitcast (i8** getelementptr inbounds ({ [5 x i8*], [5 x i8*], [5 x i8*] }, { [5 x i8*], [5 x i8*], [5 x i8*] }* @_ZTCNSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE0_NS_14basic_iostreamIcS2_EE, i32 0, inrange i32 2, i32 3) to i8*), i8* bitcast (i8** getelementptr inbounds ({ [5 x i8*], [5 x i8*], [5 x i8*] }, { [5 x i8*], [5 x i8*], [5 x i8*] }* @_ZTCNSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE0_NS_14basic_iostreamIcS2_EE, i32 0, inrange i32 1, i32 3) to i8*), i8* bitcast (i8** getelementptr inbounds ({ [5 x i8*], [5 x i8*], [5 x i8*] }, { [5 x i8*], [5 x i8*], [5 x i8*] }* @_ZTVNSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE, i32 0, inrange i32 2, i32 3) to i8*), i8* bitcast (i8** getelementptr inbounds ({ [5 x i8*], [5 x i8*], [5 x i8*] }, { [5 x i8*], [5 x i8*], [5 x i8*] }* @_ZTVNSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE, i32 0, inrange i32 1, i32 3) to i8*)], comdat, align 4
@_ZTCNSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE0_NS_14basic_iostreamIcS2_EE = linkonce_odr unnamed_addr constant { [5 x i8*], [5 x i8*], [5 x i8*] } { [5 x i8*] [i8* inttoptr (i32 64 to i8*), i8* null, i8* bitcast (i8** @_ZTINSt3__214basic_iostreamIcNS_11char_traitsIcEEEE to i8*), i8* bitcast (%"class.std::__2::basic_iostream"* (%"class.std::__2::basic_iostream"*)* @_ZNSt3__214basic_iostreamIcNS_11char_traitsIcEEED1Ev to i8*), i8* bitcast (void (%"class.std::__2::basic_iostream"*)* @_ZNSt3__214basic_iostreamIcNS_11char_traitsIcEEED0Ev to i8*)], [5 x i8*] [i8* inttoptr (i32 56 to i8*), i8* inttoptr (i32 -8 to i8*), i8* bitcast (i8** @_ZTINSt3__214basic_iostreamIcNS_11char_traitsIcEEEE to i8*), i8* bitcast (%"class.std::__2::basic_iostream"* (%"class.std::__2::basic_iostream"*)* @_ZThn8_NSt3__214basic_iostreamIcNS_11char_traitsIcEEED1Ev to i8*), i8* bitcast (void (%"class.std::__2::basic_iostream"*)* @_ZThn8_NSt3__214basic_iostreamIcNS_11char_traitsIcEEED0Ev to i8*)], [5 x i8*] [i8* inttoptr (i32 -64 to i8*), i8* inttoptr (i32 -64 to i8*), i8* bitcast (i8** @_ZTINSt3__214basic_iostreamIcNS_11char_traitsIcEEEE to i8*), i8* bitcast (%"class.std::__2::basic_iostream"* (%"class.std::__2::basic_iostream"*)* @_ZTv0_n12_NSt3__214basic_iostreamIcNS_11char_traitsIcEEED1Ev to i8*), i8* bitcast (void (%"class.std::__2::basic_iostream"*)* @_ZTv0_n12_NSt3__214basic_iostreamIcNS_11char_traitsIcEEED0Ev to i8*)] }, comdat, align 4
@_ZTINSt3__214basic_iostreamIcNS_11char_traitsIcEEEE = external constant i8*
@_ZTCNSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE0_NS_13basic_istreamIcS2_EE = linkonce_odr unnamed_addr constant { [5 x i8*], [5 x i8*] } { [5 x i8*] [i8* inttoptr (i32 64 to i8*), i8* null, i8* bitcast (i8** @_ZTINSt3__213basic_istreamIcNS_11char_traitsIcEEEE to i8*), i8* bitcast (%"class.std::__2::basic_istream"* (%"class.std::__2::basic_istream"*)* @_ZNSt3__213basic_istreamIcNS_11char_traitsIcEEED1Ev to i8*), i8* bitcast (void (%"class.std::__2::basic_istream"*)* @_ZNSt3__213basic_istreamIcNS_11char_traitsIcEEED0Ev to i8*)], [5 x i8*] [i8* inttoptr (i32 -64 to i8*), i8* inttoptr (i32 -64 to i8*), i8* bitcast (i8** @_ZTINSt3__213basic_istreamIcNS_11char_traitsIcEEEE to i8*), i8* bitcast (%"class.std::__2::basic_istream"* (%"class.std::__2::basic_istream"*)* @_ZTv0_n12_NSt3__213basic_istreamIcNS_11char_traitsIcEEED1Ev to i8*), i8* bitcast (void (%"class.std::__2::basic_istream"*)* @_ZTv0_n12_NSt3__213basic_istreamIcNS_11char_traitsIcEEED0Ev to i8*)] }, comdat, align 4
@_ZTINSt3__213basic_istreamIcNS_11char_traitsIcEEEE = external constant i8*
@_ZTCNSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE8_NS_13basic_ostreamIcS2_EE = linkonce_odr unnamed_addr constant { [5 x i8*], [5 x i8*] } { [5 x i8*] [i8* inttoptr (i32 56 to i8*), i8* null, i8* bitcast (i8** @_ZTINSt3__213basic_ostreamIcNS_11char_traitsIcEEEE to i8*), i8* bitcast (%"class.std::__2::basic_ostream"* (%"class.std::__2::basic_ostream"*)* @_ZNSt3__213basic_ostreamIcNS_11char_traitsIcEEED1Ev to i8*), i8* bitcast (void (%"class.std::__2::basic_ostream"*)* @_ZNSt3__213basic_ostreamIcNS_11char_traitsIcEEED0Ev to i8*)], [5 x i8*] [i8* inttoptr (i32 -56 to i8*), i8* inttoptr (i32 -56 to i8*), i8* bitcast (i8** @_ZTINSt3__213basic_ostreamIcNS_11char_traitsIcEEEE to i8*), i8* bitcast (%"class.std::__2::basic_ostream"* (%"class.std::__2::basic_ostream"*)* @_ZTv0_n12_NSt3__213basic_ostreamIcNS_11char_traitsIcEEED1Ev to i8*), i8* bitcast (void (%"class.std::__2::basic_ostream"*)* @_ZTv0_n12_NSt3__213basic_ostreamIcNS_11char_traitsIcEEED0Ev to i8*)] }, comdat, align 4
@_ZTINSt3__213basic_ostreamIcNS_11char_traitsIcEEEE = external constant i8*
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTSNSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE = linkonce_odr constant [69 x i8] c"NSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE\00", comdat, align 1
@_ZTINSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE = linkonce_odr constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([69 x i8], [69 x i8]* @_ZTSNSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE, i32 0, i32 0), i8* bitcast (i8** @_ZTINSt3__214basic_iostreamIcNS_11char_traitsIcEEEE to i8*) }, comdat, align 4
@_ZTVNSt3__215basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE = linkonce_odr unnamed_addr constant { [16 x i8*] } { [16 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTINSt3__215basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE to i8*), i8* bitcast (%"class.std::__2::basic_stringbuf"* (%"class.std::__2::basic_stringbuf"*)* @_ZNSt3__215basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED2Ev to i8*), i8* bitcast (void (%"class.std::__2::basic_stringbuf"*)* @_ZNSt3__215basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev to i8*), i8* bitcast (void (%"class.std::__2::basic_streambuf"*, %"class.std::__2::locale"*)* @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE5imbueERKNS_6localeE to i8*), i8* bitcast (%"class.std::__2::basic_streambuf"* (%"class.std::__2::basic_streambuf"*, i8*, i32)* @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE6setbufEPcl to i8*), i8* bitcast (void (%"class.std::__2::fpos"*, %"class.std::__2::basic_stringbuf"*, i64, i32, i32)* @_ZNSt3__215basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekoffExNS_8ios_base7seekdirEj to i8*), i8* bitcast (void (%"class.std::__2::fpos"*, %"class.std::__2::basic_stringbuf"*, %"class.std::__2::fpos"*, i32)* @_ZNSt3__215basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekposENS_4fposI11__mbstate_tEEj to i8*), i8* bitcast (i32 (%"class.std::__2::basic_streambuf"*)* @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE4syncEv to i8*), i8* bitcast (i32 (%"class.std::__2::basic_streambuf"*)* @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE9showmanycEv to i8*), i8* bitcast (i32 (%"class.std::__2::basic_streambuf"*, i8*, i32)* @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE6xsgetnEPcl to i8*), i8* bitcast (i32 (%"class.std::__2::basic_stringbuf"*)* @_ZNSt3__215basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9underflowEv to i8*), i8* bitcast (i32 (%"class.std::__2::basic_streambuf"*)* @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE5uflowEv to i8*), i8* bitcast (i32 (%"class.std::__2::basic_stringbuf"*, i32)* @_ZNSt3__215basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9pbackfailEi to i8*), i8* bitcast (i32 (%"class.std::__2::basic_streambuf"*, i8*, i32)* @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE6xsputnEPKcl to i8*), i8* bitcast (i32 (%"class.std::__2::basic_stringbuf"*, i32)* @_ZNSt3__215basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE8overflowEi to i8*)] }, comdat, align 4
@_ZTSNSt3__215basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE = linkonce_odr constant [66 x i8] c"NSt3__215basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE\00", comdat, align 1
@_ZTINSt3__215basic_streambufIcNS_11char_traitsIcEEEE = external constant i8*
@_ZTINSt3__215basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE = linkonce_odr constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([66 x i8], [66 x i8]* @_ZTSNSt3__215basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE, i32 0, i32 0), i8* bitcast (i8** @_ZTINSt3__215basic_streambufIcNS_11char_traitsIcEEEE to i8*) }, comdat, align 4
@_ZZN12_GLOBAL__N_16Logger5startERKNSt3__212basic_stringIcNS1_11char_traitsIcEENS1_9allocatorIcEEEEE1l = internal global %"class.(anonymous namespace)::Logger" zeroinitializer, align 4
@_ZGVZN12_GLOBAL__N_16Logger5startERKNSt3__212basic_stringIcNS1_11char_traitsIcEENS1_9allocatorIcEEEEE1l = internal global i32 0, align 4
@_ZNSt3__23cinE = external global %"class.std::__2::basic_istream", align 4
@_ZNSt3__24coutE = external global %"class.std::__2::basic_ostream", align 4
@_ZTVNSt3__214basic_ofstreamIcNS_11char_traitsIcEEEE = linkonce_odr unnamed_addr constant { [5 x i8*], [5 x i8*] } { [5 x i8*] [i8* inttoptr (i32 104 to i8*), i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTINSt3__214basic_ofstreamIcNS_11char_traitsIcEEEE to i8*), i8* bitcast (%"class.std::__2::basic_ofstream"* (%"class.std::__2::basic_ofstream"*)* @_ZNSt3__214basic_ofstreamIcNS_11char_traitsIcEEED1Ev to i8*), i8* bitcast (void (%"class.std::__2::basic_ofstream"*)* @_ZNSt3__214basic_ofstreamIcNS_11char_traitsIcEEED0Ev to i8*)], [5 x i8*] [i8* inttoptr (i32 -104 to i8*), i8* inttoptr (i32 -104 to i8*), i8* bitcast ({ i8*, i8*, i8* }* @_ZTINSt3__214basic_ofstreamIcNS_11char_traitsIcEEEE to i8*), i8* bitcast (%"class.std::__2::basic_ofstream"* (%"class.std::__2::basic_ofstream"*)* @_ZTv0_n12_NSt3__214basic_ofstreamIcNS_11char_traitsIcEEED1Ev to i8*), i8* bitcast (void (%"class.std::__2::basic_ofstream"*)* @_ZTv0_n12_NSt3__214basic_ofstreamIcNS_11char_traitsIcEEED0Ev to i8*)] }, comdat, align 4
@_ZTTNSt3__214basic_ofstreamIcNS_11char_traitsIcEEEE = linkonce_odr unnamed_addr constant [4 x i8*] [i8* bitcast (i8** getelementptr inbounds ({ [5 x i8*], [5 x i8*] }, { [5 x i8*], [5 x i8*] }* @_ZTVNSt3__214basic_ofstreamIcNS_11char_traitsIcEEEE, i32 0, inrange i32 0, i32 3) to i8*), i8* bitcast (i8** getelementptr inbounds ({ [5 x i8*], [5 x i8*] }, { [5 x i8*], [5 x i8*] }* @_ZTCNSt3__214basic_ofstreamIcNS_11char_traitsIcEEEE0_NS_13basic_ostreamIcS2_EE, i32 0, inrange i32 0, i32 3) to i8*), i8* bitcast (i8** getelementptr inbounds ({ [5 x i8*], [5 x i8*] }, { [5 x i8*], [5 x i8*] }* @_ZTCNSt3__214basic_ofstreamIcNS_11char_traitsIcEEEE0_NS_13basic_ostreamIcS2_EE, i32 0, inrange i32 1, i32 3) to i8*), i8* bitcast (i8** getelementptr inbounds ({ [5 x i8*], [5 x i8*] }, { [5 x i8*], [5 x i8*] }* @_ZTVNSt3__214basic_ofstreamIcNS_11char_traitsIcEEEE, i32 0, inrange i32 1, i32 3) to i8*)], comdat, align 4
@_ZTCNSt3__214basic_ofstreamIcNS_11char_traitsIcEEEE0_NS_13basic_ostreamIcS2_EE = linkonce_odr unnamed_addr constant { [5 x i8*], [5 x i8*] } { [5 x i8*] [i8* inttoptr (i32 104 to i8*), i8* null, i8* bitcast (i8** @_ZTINSt3__213basic_ostreamIcNS_11char_traitsIcEEEE to i8*), i8* bitcast (%"class.std::__2::basic_ostream"* (%"class.std::__2::basic_ostream"*)* @_ZNSt3__213basic_ostreamIcNS_11char_traitsIcEEED1Ev to i8*), i8* bitcast (void (%"class.std::__2::basic_ostream"*)* @_ZNSt3__213basic_ostreamIcNS_11char_traitsIcEEED0Ev to i8*)], [5 x i8*] [i8* inttoptr (i32 -104 to i8*), i8* inttoptr (i32 -104 to i8*), i8* bitcast (i8** @_ZTINSt3__213basic_ostreamIcNS_11char_traitsIcEEEE to i8*), i8* bitcast (%"class.std::__2::basic_ostream"* (%"class.std::__2::basic_ostream"*)* @_ZTv0_n12_NSt3__213basic_ostreamIcNS_11char_traitsIcEEED1Ev to i8*), i8* bitcast (void (%"class.std::__2::basic_ostream"*)* @_ZTv0_n12_NSt3__213basic_ostreamIcNS_11char_traitsIcEEED0Ev to i8*)] }, comdat, align 4
@_ZTSNSt3__214basic_ofstreamIcNS_11char_traitsIcEEEE = linkonce_odr constant [48 x i8] c"NSt3__214basic_ofstreamIcNS_11char_traitsIcEEEE\00", comdat, align 1
@_ZTINSt3__214basic_ofstreamIcNS_11char_traitsIcEEEE = linkonce_odr constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([48 x i8], [48 x i8]* @_ZTSNSt3__214basic_ofstreamIcNS_11char_traitsIcEEEE, i32 0, i32 0), i8* bitcast (i8** @_ZTINSt3__213basic_ostreamIcNS_11char_traitsIcEEEE to i8*) }, comdat, align 4
@_ZTVNSt3__29basic_iosIcNS_11char_traitsIcEEEE = external unnamed_addr constant { [4 x i8*] }, align 4
@_ZTVNSt3__28ios_baseE = external unnamed_addr constant { [4 x i8*] }, align 4
@_ZTVNSt3__213basic_filebufIcNS_11char_traitsIcEEEE = linkonce_odr unnamed_addr constant { [16 x i8*] } { [16 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTINSt3__213basic_filebufIcNS_11char_traitsIcEEEE to i8*), i8* bitcast (%"class.std::__2::basic_filebuf"* (%"class.std::__2::basic_filebuf"*)* @_ZNSt3__213basic_filebufIcNS_11char_traitsIcEEED2Ev to i8*), i8* bitcast (void (%"class.std::__2::basic_filebuf"*)* @_ZNSt3__213basic_filebufIcNS_11char_traitsIcEEED0Ev to i8*), i8* bitcast (void (%"class.std::__2::basic_filebuf"*, %"class.std::__2::locale"*)* @_ZNSt3__213basic_filebufIcNS_11char_traitsIcEEE5imbueERKNS_6localeE to i8*), i8* bitcast (%"class.std::__2::basic_streambuf"* (%"class.std::__2::basic_filebuf"*, i8*, i32)* @_ZNSt3__213basic_filebufIcNS_11char_traitsIcEEE6setbufEPcl to i8*), i8* bitcast (void (%"class.std::__2::fpos"*, %"class.std::__2::basic_filebuf"*, i64, i32, i32)* @_ZNSt3__213basic_filebufIcNS_11char_traitsIcEEE7seekoffExNS_8ios_base7seekdirEj to i8*), i8* bitcast (void (%"class.std::__2::fpos"*, %"class.std::__2::basic_filebuf"*, %"class.std::__2::fpos"*, i32)* @_ZNSt3__213basic_filebufIcNS_11char_traitsIcEEE7seekposENS_4fposI11__mbstate_tEEj to i8*), i8* bitcast (i32 (%"class.std::__2::basic_filebuf"*)* @_ZNSt3__213basic_filebufIcNS_11char_traitsIcEEE4syncEv to i8*), i8* bitcast (i32 (%"class.std::__2::basic_streambuf"*)* @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE9showmanycEv to i8*), i8* bitcast (i32 (%"class.std::__2::basic_streambuf"*, i8*, i32)* @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE6xsgetnEPcl to i8*), i8* bitcast (i32 (%"class.std::__2::basic_filebuf"*)* @_ZNSt3__213basic_filebufIcNS_11char_traitsIcEEE9underflowEv to i8*), i8* bitcast (i32 (%"class.std::__2::basic_streambuf"*)* @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE5uflowEv to i8*), i8* bitcast (i32 (%"class.std::__2::basic_filebuf"*, i32)* @_ZNSt3__213basic_filebufIcNS_11char_traitsIcEEE9pbackfailEi to i8*), i8* bitcast (i32 (%"class.std::__2::basic_streambuf"*, i8*, i32)* @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE6xsputnEPKcl to i8*), i8* bitcast (i32 (%"class.std::__2::basic_filebuf"*, i32)* @_ZNSt3__213basic_filebufIcNS_11char_traitsIcEEE8overflowEi to i8*)] }, comdat, align 4
@_ZTSNSt3__213basic_filebufIcNS_11char_traitsIcEEEE = linkonce_odr constant [47 x i8] c"NSt3__213basic_filebufIcNS_11char_traitsIcEEEE\00", comdat, align 1
@_ZTINSt3__213basic_filebufIcNS_11char_traitsIcEEEE = linkonce_odr constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([47 x i8], [47 x i8]* @_ZTSNSt3__213basic_filebufIcNS_11char_traitsIcEEEE, i32 0, i32 0), i8* bitcast (i8** @_ZTINSt3__215basic_streambufIcNS_11char_traitsIcEEEE to i8*) }, comdat, align 4
@_ZNSt3__27codecvtIcc11__mbstate_tE2idE = external global %"class.std::__2::locale::id", align 4
@_ZTVN12_GLOBAL__N_13TieE = internal unnamed_addr constant { [16 x i8*] } { [16 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN12_GLOBAL__N_13TieE to i8*), i8* bitcast (%"struct.(anonymous namespace)::Tie"* (%"struct.(anonymous namespace)::Tie"*)* @_ZN12_GLOBAL__N_13TieD2Ev to i8*), i8* bitcast (void (%"struct.(anonymous namespace)::Tie"*)* @_ZN12_GLOBAL__N_13TieD0Ev to i8*), i8* bitcast (void (%"class.std::__2::basic_streambuf"*, %"class.std::__2::locale"*)* @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE5imbueERKNS_6localeE to i8*), i8* bitcast (%"class.std::__2::basic_streambuf"* (%"class.std::__2::basic_streambuf"*, i8*, i32)* @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE6setbufEPcl to i8*), i8* bitcast (void (%"class.std::__2::fpos"*, %"class.std::__2::basic_streambuf"*, i64, i32, i32)* @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE7seekoffExNS_8ios_base7seekdirEj to i8*), i8* bitcast (void (%"class.std::__2::fpos"*, %"class.std::__2::basic_streambuf"*, %"class.std::__2::fpos"*, i32)* @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE7seekposENS_4fposI11__mbstate_tEEj to i8*), i8* bitcast (i32 (%"struct.(anonymous namespace)::Tie"*)* @_ZN12_GLOBAL__N_13Tie4syncEv to i8*), i8* bitcast (i32 (%"class.std::__2::basic_streambuf"*)* @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE9showmanycEv to i8*), i8* bitcast (i32 (%"class.std::__2::basic_streambuf"*, i8*, i32)* @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE6xsgetnEPcl to i8*), i8* bitcast (i32 (%"struct.(anonymous namespace)::Tie"*)* @_ZN12_GLOBAL__N_13Tie9underflowEv to i8*), i8* bitcast (i32 (%"struct.(anonymous namespace)::Tie"*)* @_ZN12_GLOBAL__N_13Tie5uflowEv to i8*), i8* bitcast (i32 (%"class.std::__2::basic_streambuf"*, i32)* @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE9pbackfailEi to i8*), i8* bitcast (i32 (%"class.std::__2::basic_streambuf"*, i8*, i32)* @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE6xsputnEPKcl to i8*), i8* bitcast (i32 (%"struct.(anonymous namespace)::Tie"*, i32)* @_ZN12_GLOBAL__N_13Tie8overflowEi to i8*)] }, align 4
@_ZTSN12_GLOBAL__N_13TieE = internal constant [21 x i8] c"N12_GLOBAL__N_13TieE\00", align 1
@_ZTIN12_GLOBAL__N_13TieE = internal constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([21 x i8], [21 x i8]* @_ZTSN12_GLOBAL__N_13TieE, i32 0, i32 0), i8* bitcast (i8** @_ZTINSt3__215basic_streambufIcNS_11char_traitsIcEEEE to i8*) }, align 4
@.str.13 = private unnamed_addr constant [4 x i8] c">> \00", align 1
@_ZZN12_GLOBAL__N_13Tie3logEiPKcE4last = internal global i32 10, align 4
@.str.14 = private unnamed_addr constant [4 x i8] c"<< \00", align 1
@.str.15 = private unnamed_addr constant [2 x i8] c"w\00", align 1
@.str.16 = private unnamed_addr constant [2 x i8] c"a\00", align 1
@.str.17 = private unnamed_addr constant [2 x i8] c"r\00", align 1
@.str.18 = private unnamed_addr constant [3 x i8] c"r+\00", align 1
@.str.19 = private unnamed_addr constant [3 x i8] c"w+\00", align 1
@.str.20 = private unnamed_addr constant [3 x i8] c"a+\00", align 1
@.str.21 = private unnamed_addr constant [3 x i8] c"wb\00", align 1
@.str.22 = private unnamed_addr constant [3 x i8] c"ab\00", align 1
@.str.23 = private unnamed_addr constant [3 x i8] c"rb\00", align 1
@.str.24 = private unnamed_addr constant [4 x i8] c"r+b\00", align 1
@.str.25 = private unnamed_addr constant [4 x i8] c"w+b\00", align 1
@.str.26 = private unnamed_addr constant [4 x i8] c"a+b\00", align 1
@.str.27 = private unnamed_addr constant [68 x i8] c"allocator<T>::allocate(size_t n) 'n' exceeds maximum supported size\00", align 1
@_ZNSt3__25ctypeIcE2idE = external global %"class.std::__2::locale::id", align 4
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_misc.cpp, i8* null }]

; Function Attrs: noinline nounwind
define internal void @__cxx_global_var_init() #0 {
entry:
  %agg.tmp = alloca %"class.std::initializer_list", align 4
  %ref.tmp = alloca [1 x %"class.std::__2::basic_string"], align 4
  %arrayinit.begin = getelementptr inbounds [1 x %"class.std::__2::basic_string"], [1 x %"class.std::__2::basic_string"]* %ref.tmp, i32 0, i32 0
  %call = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2IDnEEPKc(%"class.std::__2::basic_string"* %arrayinit.begin, i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str, i32 0, i32 0))
  %__begin_ = getelementptr inbounds %"class.std::initializer_list", %"class.std::initializer_list"* %agg.tmp, i32 0, i32 0
  %arraystart = getelementptr inbounds [1 x %"class.std::__2::basic_string"], [1 x %"class.std::__2::basic_string"]* %ref.tmp, i32 0, i32 0
  store %"class.std::__2::basic_string"* %arraystart, %"class.std::__2::basic_string"** %__begin_, align 4
  %__size_ = getelementptr inbounds %"class.std::initializer_list", %"class.std::initializer_list"* %agg.tmp, i32 0, i32 1
  store i32 1, i32* %__size_, align 4
  %call1 = call %"class.std::__2::vector"* @_ZNSt3__26vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEEC2ESt16initializer_listIS6_E(%"class.std::__2::vector"* @_ZL8variants, %"class.std::initializer_list"* byval(%"class.std::initializer_list") align 4 %agg.tmp)
  %array.begin = getelementptr inbounds [1 x %"class.std::__2::basic_string"], [1 x %"class.std::__2::basic_string"]* %ref.tmp, i32 0, i32 0
  %0 = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %array.begin, i32 1
  br label %arraydestroy.body

arraydestroy.body:                                ; preds = %arraydestroy.body, %entry
  %arraydestroy.elementPast = phi %"class.std::__2::basic_string"* [ %0, %entry ], [ %arraydestroy.element, %arraydestroy.body ]
  %arraydestroy.element = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %arraydestroy.elementPast, i32 -1
  %call2 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_string"* %arraydestroy.element) #3
  %arraydestroy.done = icmp eq %"class.std::__2::basic_string"* %arraydestroy.element, %array.begin
  br i1 %arraydestroy.done, label %arraydestroy.done3, label %arraydestroy.body

arraydestroy.done3:                               ; preds = %arraydestroy.body
  %1 = call i32 @__cxa_atexit(void (i8*)* @__cxx_global_array_dtor, i8* null, i8* @__dso_handle) #3
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2IDnEEPKc(%"class.std::__2::basic_string"* returned %this, i8* %__s) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  %__s.addr = alloca i8*, align 4
  %ref.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  %ref.tmp2 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  store i8* %__s, i8** %__s.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::basic_string"* %this1 to %"class.std::__2::__basic_string_common"*
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::__compressed_pair"* @_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_EC2INS_18__default_init_tagESA_EEOT_OT0_(%"class.std::__2::__compressed_pair"* %__r_, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  %1 = load i8*, i8** %__s.addr, align 4
  %2 = load i8*, i8** %__s.addr, align 4
  %call3 = call i32 @_ZNSt3__211char_traitsIcE6lengthEPKc(i8* %2) #3
  call void @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEPKcm(%"class.std::__2::basic_string"* %this1, i8* %1, i32 %call3)
  ret %"class.std::__2::basic_string"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector"* @_ZNSt3__26vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEEC2ESt16initializer_listIS6_E(%"class.std::__2::vector"* returned %this, %"class.std::initializer_list"* byval(%"class.std::initializer_list") align 4 %__il) unnamed_addr #1 comdat {
entry:
  %retval = alloca %"class.std::__2::vector"*, align 4
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  store %"class.std::__2::vector"* %this1, %"class.std::__2::vector"** %retval, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call %"class.std::__2::__vector_base"* @_ZNSt3__213__vector_baseINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEEC2Ev(%"class.std::__2::__vector_base"* %0) #3
  %call2 = call i32 @_ZNKSt16initializer_listINSt3__212basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEEE4sizeEv(%"class.std::initializer_list"* %__il) #3
  %cmp = icmp ugt i32 %call2, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNKSt16initializer_listINSt3__212basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEEE4sizeEv(%"class.std::initializer_list"* %__il) #3
  call void @_ZNSt3__26vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE11__vallocateEm(%"class.std::__2::vector"* %this1, i32 %call3)
  %call4 = call %"class.std::__2::basic_string"* @_ZNKSt16initializer_listINSt3__212basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEEE5beginEv(%"class.std::initializer_list"* %__il) #3
  %call5 = call %"class.std::__2::basic_string"* @_ZNKSt16initializer_listINSt3__212basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEEE3endEv(%"class.std::initializer_list"* %__il) #3
  %call6 = call i32 @_ZNKSt16initializer_listINSt3__212basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEEE4sizeEv(%"class.std::initializer_list"* %__il) #3
  call void @_ZNSt3__26vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE18__construct_at_endIPKS6_EENS_9enable_ifIXsr27__is_cpp17_forward_iteratorIT_EE5valueEvE4typeESD_SD_m(%"class.std::__2::vector"* %this1, %"class.std::__2::basic_string"* %call4, %"class.std::__2::basic_string"* %call5, i32 %call6)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %retval, align 4
  ret %"class.std::__2::vector"* %1
}

; Function Attrs: nounwind
declare %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_string"* returned) unnamed_addr #2

; Function Attrs: noinline nounwind
define internal void @__cxx_global_array_dtor(i8* %0) #0 {
entry:
  %.addr = alloca i8*, align 4
  store i8* %0, i8** %.addr, align 4
  %call = call %"class.std::__2::vector"* @_ZNSt3__26vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEED2Ev(%"class.std::__2::vector"* @_ZL8variants) #3
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector"* @_ZNSt3__26vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEED2Ev(%"class.std::__2::vector"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  call void @_ZNKSt3__26vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE17__annotate_deleteEv(%"class.std::__2::vector"* %this1) #3
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call %"class.std::__2::__vector_base"* @_ZNSt3__213__vector_baseINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEED2Ev(%"class.std::__2::__vector_base"* %0) #3
  ret %"class.std::__2::vector"* %this1
}

; Function Attrs: nounwind
declare i32 @__cxa_atexit(void (i8*)*, i8*, i8*) #3

; Function Attrs: noinline nounwind
define internal void @__cxx_global_var_init.1() #0 {
entry:
  %call = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2IDnEEPKc(%"class.std::__2::basic_string"* @_ZN12_GLOBAL__N_17VersionE, i8* getelementptr inbounds ([1 x i8], [1 x i8]* @.str.2, i32 0, i32 0))
  %0 = call i32 @__cxa_atexit(void (i8*)* @__cxx_global_array_dtor.3, i8* null, i8* @__dso_handle) #3
  ret void
}

; Function Attrs: noinline nounwind
define internal void @__cxx_global_array_dtor.3(i8* %0) #0 {
entry:
  %.addr = alloca i8*, align 4
  store i8* %0, i8** %.addr, align 4
  %call = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_string"* @_ZN12_GLOBAL__N_17VersionE) #3
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_Z11engine_infob(%"class.std::__2::basic_string"* noalias sret align 4 %agg.result, i1 zeroext %to_uci) #1 {
entry:
  %result.ptr = alloca i8*, align 4
  %to_uci.addr = alloca i8, align 1
  %ss = alloca %"class.std::__2::basic_stringstream", align 4
  %date = alloca %"class.std::__2::basic_stringstream", align 4
  %ref.tmp = alloca %"class.std::__2::basic_string", align 4
  %ref.tmp6 = alloca %"class.std::__2::__iom_t4", align 1
  %0 = bitcast %"class.std::__2::basic_string"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  %frombool = zext i1 %to_uci to i8
  store i8 %frombool, i8* %to_uci.addr, align 1
  %call = call %"class.std::__2::basic_stringstream"* @_ZNSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEC1Ej(%"class.std::__2::basic_stringstream"* %ss, i32 24)
  %call1 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2IDnEEPKc(%"class.std::__2::basic_string"* %ref.tmp, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.4, i32 0, i32 0))
  %call2 = call %"class.std::__2::basic_stringstream"* @_ZNSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKNS_12basic_stringIcS2_S4_EEj(%"class.std::__2::basic_stringstream"* %date, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %ref.tmp, i32 24)
  %call3 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_string"* %ref.tmp) #3
  %1 = bitcast %"class.std::__2::basic_stringstream"* %ss to i8*
  %add.ptr = getelementptr inbounds i8, i8* %1, i32 8
  %2 = bitcast i8* %add.ptr to %"class.std::__2::basic_ostream"*
  %call4 = call nonnull align 4 dereferenceable(4) %"class.std::__2::basic_ostream"* @_ZNSt3__2lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc(%"class.std::__2::basic_ostream"* nonnull align 4 dereferenceable(4) %2, i8* getelementptr inbounds ([14 x i8], [14 x i8]* @.str.5, i32 0, i32 0))
  %call5 = call nonnull align 4 dereferenceable(4) %"class.std::__2::basic_ostream"* @_ZNSt3__2lsIcNS_11char_traitsIcEENS_9allocatorIcEEEERNS_13basic_ostreamIT_T0_EES9_RKNS_12basic_stringIS6_S7_T1_EE(%"class.std::__2::basic_ostream"* nonnull align 4 dereferenceable(4) %call4, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) @_ZN12_GLOBAL__N_17VersionE)
  %call7 = call i8 @_ZNSt3__27setfillIcEENS_8__iom_t4IT_EES2_(i8 signext 48)
  %coerce.dive = getelementptr inbounds %"class.std::__2::__iom_t4", %"class.std::__2::__iom_t4"* %ref.tmp6, i32 0, i32 0
  store i8 %call7, i8* %coerce.dive, align 1
  %call8 = call nonnull align 4 dereferenceable(4) %"class.std::__2::basic_ostream"* @_ZNSt3__2lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_RKNS_8__iom_t4IcEE(%"class.std::__2::basic_ostream"* nonnull align 4 dereferenceable(4) %call5, %"class.std::__2::__iom_t4"* nonnull align 1 dereferenceable(1) %ref.tmp6)
  %3 = bitcast %"class.std::__2::basic_stringstream"* %ss to i8*
  %add.ptr9 = getelementptr inbounds i8, i8* %3, i32 8
  %4 = bitcast i8* %add.ptr9 to %"class.std::__2::basic_ostream"*
  %call10 = call nonnull align 4 dereferenceable(4) %"class.std::__2::basic_ostream"* @_ZNSt3__2lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc(%"class.std::__2::basic_ostream"* nonnull align 4 dereferenceable(4) %4, i8* getelementptr inbounds ([1 x i8], [1 x i8]* @.str.2, i32 0, i32 0))
  %call11 = call nonnull align 4 dereferenceable(4) %"class.std::__2::basic_ostream"* @_ZNSt3__2lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc(%"class.std::__2::basic_ostream"* nonnull align 4 dereferenceable(4) %call10, i8* getelementptr inbounds ([1 x i8], [1 x i8]* @.str.2, i32 0, i32 0))
  %call12 = call nonnull align 4 dereferenceable(4) %"class.std::__2::basic_ostream"* @_ZNSt3__2lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc(%"class.std::__2::basic_ostream"* nonnull align 4 dereferenceable(4) %call11, i8* getelementptr inbounds ([95 x i8], [95 x i8]* @.str.6, i32 0, i32 0))
  call void @_ZNKSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEE3strEv(%"class.std::__2::basic_string"* sret align 4 %agg.result, %"class.std::__2::basic_stringstream"* %ss)
  %call13 = call %"class.std::__2::basic_stringstream"* @_ZNSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_stringstream"* %date) #3
  %call14 = call %"class.std::__2::basic_stringstream"* @_ZNSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_stringstream"* %ss) #3
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::basic_stringstream"* @_ZNSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEC1Ej(%"class.std::__2::basic_stringstream"* returned %this, i32 %__wch) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_stringstream"*, align 4
  %__wch.addr = alloca i32, align 4
  store %"class.std::__2::basic_stringstream"* %this, %"class.std::__2::basic_stringstream"** %this.addr, align 4
  store i32 %__wch, i32* %__wch.addr, align 4
  %this1 = load %"class.std::__2::basic_stringstream"*, %"class.std::__2::basic_stringstream"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::basic_stringstream"* %this1 to i8*
  %1 = getelementptr inbounds i8, i8* %0, i32 64
  %2 = bitcast i8* %1 to %"class.std::__2::basic_ios"*
  %call = call %"class.std::__2::basic_ios"* @_ZNSt3__29basic_iosIcNS_11char_traitsIcEEEC2Ev(%"class.std::__2::basic_ios"* %2)
  %3 = bitcast %"class.std::__2::basic_stringstream"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*], [5 x i8*], [5 x i8*] }, { [5 x i8*], [5 x i8*], [5 x i8*] }* @_ZTVNSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE, i32 0, inrange i32 0, i32 3) to i32 (...)**), i32 (...)*** %3, align 4
  %4 = bitcast %"class.std::__2::basic_stringstream"* %this1 to i8*
  %add.ptr = getelementptr inbounds i8, i8* %4, i32 64
  %5 = bitcast i8* %add.ptr to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*], [5 x i8*], [5 x i8*] }, { [5 x i8*], [5 x i8*], [5 x i8*] }* @_ZTVNSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE, i32 0, inrange i32 2, i32 3) to i32 (...)**), i32 (...)*** %5, align 4
  %6 = bitcast %"class.std::__2::basic_stringstream"* %this1 to i8*
  %add.ptr2 = getelementptr inbounds i8, i8* %6, i32 8
  %7 = bitcast i8* %add.ptr2 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*], [5 x i8*], [5 x i8*] }, { [5 x i8*], [5 x i8*], [5 x i8*] }* @_ZTVNSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE, i32 0, inrange i32 1, i32 3) to i32 (...)**), i32 (...)*** %7, align 4
  %8 = bitcast %"class.std::__2::basic_stringstream"* %this1 to %"class.std::__2::basic_iostream"*
  %__sb_ = getelementptr inbounds %"class.std::__2::basic_stringstream", %"class.std::__2::basic_stringstream"* %this1, i32 0, i32 1
  %9 = bitcast %"class.std::__2::basic_stringbuf"* %__sb_ to %"class.std::__2::basic_streambuf"*
  %call3 = call %"class.std::__2::basic_iostream"* @_ZNSt3__214basic_iostreamIcNS_11char_traitsIcEEEC2EPNS_15basic_streambufIcS2_EE(%"class.std::__2::basic_iostream"* %8, i8** getelementptr inbounds ([10 x i8*], [10 x i8*]* @_ZTTNSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE, i64 0, i64 1), %"class.std::__2::basic_streambuf"* %9)
  %10 = bitcast %"class.std::__2::basic_stringstream"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*], [5 x i8*], [5 x i8*] }, { [5 x i8*], [5 x i8*], [5 x i8*] }* @_ZTVNSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE, i32 0, inrange i32 0, i32 3) to i32 (...)**), i32 (...)*** %10, align 4
  %11 = bitcast %"class.std::__2::basic_stringstream"* %this1 to i8*
  %add.ptr4 = getelementptr inbounds i8, i8* %11, i32 64
  %12 = bitcast i8* %add.ptr4 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*], [5 x i8*], [5 x i8*] }, { [5 x i8*], [5 x i8*], [5 x i8*] }* @_ZTVNSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE, i32 0, inrange i32 2, i32 3) to i32 (...)**), i32 (...)*** %12, align 4
  %13 = bitcast %"class.std::__2::basic_stringstream"* %this1 to i8*
  %add.ptr5 = getelementptr inbounds i8, i8* %13, i32 8
  %14 = bitcast i8* %add.ptr5 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*], [5 x i8*], [5 x i8*] }, { [5 x i8*], [5 x i8*], [5 x i8*] }* @_ZTVNSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE, i32 0, inrange i32 1, i32 3) to i32 (...)**), i32 (...)*** %14, align 4
  %__sb_6 = getelementptr inbounds %"class.std::__2::basic_stringstream", %"class.std::__2::basic_stringstream"* %this1, i32 0, i32 1
  %15 = load i32, i32* %__wch.addr, align 4
  %call7 = call %"class.std::__2::basic_stringbuf"* @_ZNSt3__215basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEC2Ej(%"class.std::__2::basic_stringbuf"* %__sb_6, i32 %15)
  ret %"class.std::__2::basic_stringstream"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::basic_stringstream"* @_ZNSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKNS_12basic_stringIcS2_S4_EEj(%"class.std::__2::basic_stringstream"* returned %this, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %__s, i32 %__wch) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_stringstream"*, align 4
  %__s.addr = alloca %"class.std::__2::basic_string"*, align 4
  %__wch.addr = alloca i32, align 4
  store %"class.std::__2::basic_stringstream"* %this, %"class.std::__2::basic_stringstream"** %this.addr, align 4
  store %"class.std::__2::basic_string"* %__s, %"class.std::__2::basic_string"** %__s.addr, align 4
  store i32 %__wch, i32* %__wch.addr, align 4
  %this1 = load %"class.std::__2::basic_stringstream"*, %"class.std::__2::basic_stringstream"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::basic_stringstream"* %this1 to i8*
  %1 = getelementptr inbounds i8, i8* %0, i32 64
  %2 = bitcast i8* %1 to %"class.std::__2::basic_ios"*
  %call = call %"class.std::__2::basic_ios"* @_ZNSt3__29basic_iosIcNS_11char_traitsIcEEEC2Ev(%"class.std::__2::basic_ios"* %2)
  %3 = bitcast %"class.std::__2::basic_stringstream"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*], [5 x i8*], [5 x i8*] }, { [5 x i8*], [5 x i8*], [5 x i8*] }* @_ZTVNSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE, i32 0, inrange i32 0, i32 3) to i32 (...)**), i32 (...)*** %3, align 4
  %4 = bitcast %"class.std::__2::basic_stringstream"* %this1 to i8*
  %add.ptr = getelementptr inbounds i8, i8* %4, i32 64
  %5 = bitcast i8* %add.ptr to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*], [5 x i8*], [5 x i8*] }, { [5 x i8*], [5 x i8*], [5 x i8*] }* @_ZTVNSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE, i32 0, inrange i32 2, i32 3) to i32 (...)**), i32 (...)*** %5, align 4
  %6 = bitcast %"class.std::__2::basic_stringstream"* %this1 to i8*
  %add.ptr2 = getelementptr inbounds i8, i8* %6, i32 8
  %7 = bitcast i8* %add.ptr2 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*], [5 x i8*], [5 x i8*] }, { [5 x i8*], [5 x i8*], [5 x i8*] }* @_ZTVNSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE, i32 0, inrange i32 1, i32 3) to i32 (...)**), i32 (...)*** %7, align 4
  %8 = bitcast %"class.std::__2::basic_stringstream"* %this1 to %"class.std::__2::basic_iostream"*
  %__sb_ = getelementptr inbounds %"class.std::__2::basic_stringstream", %"class.std::__2::basic_stringstream"* %this1, i32 0, i32 1
  %9 = bitcast %"class.std::__2::basic_stringbuf"* %__sb_ to %"class.std::__2::basic_streambuf"*
  %call3 = call %"class.std::__2::basic_iostream"* @_ZNSt3__214basic_iostreamIcNS_11char_traitsIcEEEC2EPNS_15basic_streambufIcS2_EE(%"class.std::__2::basic_iostream"* %8, i8** getelementptr inbounds ([10 x i8*], [10 x i8*]* @_ZTTNSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE, i64 0, i64 1), %"class.std::__2::basic_streambuf"* %9)
  %10 = bitcast %"class.std::__2::basic_stringstream"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*], [5 x i8*], [5 x i8*] }, { [5 x i8*], [5 x i8*], [5 x i8*] }* @_ZTVNSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE, i32 0, inrange i32 0, i32 3) to i32 (...)**), i32 (...)*** %10, align 4
  %11 = bitcast %"class.std::__2::basic_stringstream"* %this1 to i8*
  %add.ptr4 = getelementptr inbounds i8, i8* %11, i32 64
  %12 = bitcast i8* %add.ptr4 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*], [5 x i8*], [5 x i8*] }, { [5 x i8*], [5 x i8*], [5 x i8*] }* @_ZTVNSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE, i32 0, inrange i32 2, i32 3) to i32 (...)**), i32 (...)*** %12, align 4
  %13 = bitcast %"class.std::__2::basic_stringstream"* %this1 to i8*
  %add.ptr5 = getelementptr inbounds i8, i8* %13, i32 8
  %14 = bitcast i8* %add.ptr5 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*], [5 x i8*], [5 x i8*] }, { [5 x i8*], [5 x i8*], [5 x i8*] }* @_ZTVNSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE, i32 0, inrange i32 1, i32 3) to i32 (...)**), i32 (...)*** %14, align 4
  %__sb_6 = getelementptr inbounds %"class.std::__2::basic_stringstream", %"class.std::__2::basic_stringstream"* %this1, i32 0, i32 1
  %15 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__s.addr, align 4
  %16 = load i32, i32* %__wch.addr, align 4
  %call7 = call %"class.std::__2::basic_stringbuf"* @_ZNSt3__215basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEC2ERKNS_12basic_stringIcS2_S4_EEj(%"class.std::__2::basic_stringbuf"* %__sb_6, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %15, i32 %16)
  ret %"class.std::__2::basic_stringstream"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::basic_ostream"* @_ZNSt3__2lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_RKNS_8__iom_t4IcEE(%"class.std::__2::basic_ostream"* nonnull align 4 dereferenceable(4) %__os, %"class.std::__2::__iom_t4"* nonnull align 1 dereferenceable(1) %__x) #1 comdat {
entry:
  %__os.addr = alloca %"class.std::__2::basic_ostream"*, align 4
  %__x.addr = alloca %"class.std::__2::__iom_t4"*, align 4
  store %"class.std::__2::basic_ostream"* %__os, %"class.std::__2::basic_ostream"** %__os.addr, align 4
  store %"class.std::__2::__iom_t4"* %__x, %"class.std::__2::__iom_t4"** %__x.addr, align 4
  %0 = load %"class.std::__2::basic_ostream"*, %"class.std::__2::basic_ostream"** %__os.addr, align 4
  %1 = bitcast %"class.std::__2::basic_ostream"* %0 to i8**
  %vtable = load i8*, i8** %1, align 4
  %vbase.offset.ptr = getelementptr i8, i8* %vtable, i64 -12
  %2 = bitcast i8* %vbase.offset.ptr to i32*
  %vbase.offset = load i32, i32* %2, align 4
  %3 = bitcast %"class.std::__2::basic_ostream"* %0 to i8*
  %add.ptr = getelementptr inbounds i8, i8* %3, i32 %vbase.offset
  %4 = bitcast i8* %add.ptr to %"class.std::__2::basic_ios"*
  %5 = load %"class.std::__2::__iom_t4"*, %"class.std::__2::__iom_t4"** %__x.addr, align 4
  %__fill_ = getelementptr inbounds %"class.std::__2::__iom_t4", %"class.std::__2::__iom_t4"* %5, i32 0, i32 0
  %6 = load i8, i8* %__fill_, align 1
  %call = call signext i8 @_ZNSt3__29basic_iosIcNS_11char_traitsIcEEE4fillEc(%"class.std::__2::basic_ios"* %4, i8 signext %6)
  %7 = load %"class.std::__2::basic_ostream"*, %"class.std::__2::basic_ostream"** %__os.addr, align 4
  ret %"class.std::__2::basic_ostream"* %7
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::basic_ostream"* @_ZNSt3__2lsIcNS_11char_traitsIcEENS_9allocatorIcEEEERNS_13basic_ostreamIT_T0_EES9_RKNS_12basic_stringIS6_S7_T1_EE(%"class.std::__2::basic_ostream"* nonnull align 4 dereferenceable(4) %__os, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %__str) #1 comdat {
entry:
  %__os.addr = alloca %"class.std::__2::basic_ostream"*, align 4
  %__str.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_ostream"* %__os, %"class.std::__2::basic_ostream"** %__os.addr, align 4
  store %"class.std::__2::basic_string"* %__str, %"class.std::__2::basic_string"** %__str.addr, align 4
  %0 = load %"class.std::__2::basic_ostream"*, %"class.std::__2::basic_ostream"** %__os.addr, align 4
  %1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__str.addr, align 4
  %call = call i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv(%"class.std::__2::basic_string"* %1) #3
  %2 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__str.addr, align 4
  %call1 = call i32 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4sizeEv(%"class.std::__2::basic_string"* %2) #3
  %call2 = call nonnull align 4 dereferenceable(4) %"class.std::__2::basic_ostream"* @_ZNSt3__224__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m(%"class.std::__2::basic_ostream"* nonnull align 4 dereferenceable(4) %0, i8* %call, i32 %call1)
  ret %"class.std::__2::basic_ostream"* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::basic_ostream"* @_ZNSt3__2lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc(%"class.std::__2::basic_ostream"* nonnull align 4 dereferenceable(4) %__os, i8* %__str) #1 comdat {
entry:
  %__os.addr = alloca %"class.std::__2::basic_ostream"*, align 4
  %__str.addr = alloca i8*, align 4
  store %"class.std::__2::basic_ostream"* %__os, %"class.std::__2::basic_ostream"** %__os.addr, align 4
  store i8* %__str, i8** %__str.addr, align 4
  %0 = load %"class.std::__2::basic_ostream"*, %"class.std::__2::basic_ostream"** %__os.addr, align 4
  %1 = load i8*, i8** %__str.addr, align 4
  %2 = load i8*, i8** %__str.addr, align 4
  %call = call i32 @_ZNSt3__211char_traitsIcE6lengthEPKc(i8* %2) #3
  %call1 = call nonnull align 4 dereferenceable(4) %"class.std::__2::basic_ostream"* @_ZNSt3__224__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m(%"class.std::__2::basic_ostream"* nonnull align 4 dereferenceable(4) %0, i8* %1, i32 %call)
  ret %"class.std::__2::basic_ostream"* %call1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8 @_ZNSt3__27setfillIcEENS_8__iom_t4IT_EES2_(i8 signext %__c) #1 comdat {
entry:
  %retval = alloca %"class.std::__2::__iom_t4", align 1
  %__c.addr = alloca i8, align 1
  store i8 %__c, i8* %__c.addr, align 1
  %0 = load i8, i8* %__c.addr, align 1
  %call = call %"class.std::__2::__iom_t4"* @_ZNSt3__28__iom_t4IcEC2Ec(%"class.std::__2::__iom_t4"* %retval, i8 signext %0)
  %coerce.dive = getelementptr inbounds %"class.std::__2::__iom_t4", %"class.std::__2::__iom_t4"* %retval, i32 0, i32 0
  %1 = load i8, i8* %coerce.dive, align 1
  ret i8 %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEE3strEv(%"class.std::__2::basic_string"* noalias sret align 4 %agg.result, %"class.std::__2::basic_stringstream"* %this) #1 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %"class.std::__2::basic_stringstream"*, align 4
  %0 = bitcast %"class.std::__2::basic_string"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %"class.std::__2::basic_stringstream"* %this, %"class.std::__2::basic_stringstream"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_stringstream"*, %"class.std::__2::basic_stringstream"** %this.addr, align 4
  %__sb_ = getelementptr inbounds %"class.std::__2::basic_stringstream", %"class.std::__2::basic_stringstream"* %this1, i32 0, i32 1
  call void @_ZNKSt3__215basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE3strEv(%"class.std::__2::basic_string"* sret align 4 %agg.result, %"class.std::__2::basic_stringbuf"* %__sb_)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::basic_stringstream"* @_ZNSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_stringstream"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_stringstream"*, align 4
  store %"class.std::__2::basic_stringstream"* %this, %"class.std::__2::basic_stringstream"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_stringstream"*, %"class.std::__2::basic_stringstream"** %this.addr, align 4
  %call = call %"class.std::__2::basic_stringstream"* @_ZNSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED2Ev(%"class.std::__2::basic_stringstream"* %this1, i8** getelementptr inbounds ([10 x i8*], [10 x i8*]* @_ZTTNSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE, i64 0, i64 0)) #3
  %0 = bitcast %"class.std::__2::basic_stringstream"* %this1 to i8*
  %1 = getelementptr inbounds i8, i8* %0, i32 64
  %2 = bitcast i8* %1 to %"class.std::__2::basic_ios"*
  %call2 = call %"class.std::__2::basic_ios"* @_ZNSt3__29basic_iosIcNS_11char_traitsIcEEED2Ev(%"class.std::__2::basic_ios"* %2) #3
  ret %"class.std::__2::basic_stringstream"* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden void @_Z10dbg_hit_onb(i1 zeroext %b) #1 {
entry:
  %b.addr = alloca i8, align 1
  %frombool = zext i1 %b to i8
  store i8 %frombool, i8* %b.addr, align 1
  %0 = load i64, i64* getelementptr inbounds ([2 x i64], [2 x i64]* @_ZL4hits, i32 0, i32 0), align 16
  %inc = add nsw i64 %0, 1
  store i64 %inc, i64* getelementptr inbounds ([2 x i64], [2 x i64]* @_ZL4hits, i32 0, i32 0), align 16
  %1 = load i8, i8* %b.addr, align 1
  %tobool = trunc i8 %1 to i1
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load i64, i64* getelementptr inbounds ([2 x i64], [2 x i64]* @_ZL4hits, i32 0, i32 1), align 8
  %inc1 = add nsw i64 %2, 1
  store i64 %inc1, i64* getelementptr inbounds ([2 x i64], [2 x i64]* @_ZL4hits, i32 0, i32 1), align 8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_Z10dbg_hit_onbb(i1 zeroext %c, i1 zeroext %b) #1 {
entry:
  %c.addr = alloca i8, align 1
  %b.addr = alloca i8, align 1
  %frombool = zext i1 %c to i8
  store i8 %frombool, i8* %c.addr, align 1
  %frombool1 = zext i1 %b to i8
  store i8 %frombool1, i8* %b.addr, align 1
  %0 = load i8, i8* %c.addr, align 1
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i8, i8* %b.addr, align 1
  %tobool2 = trunc i8 %1 to i1
  call void @_Z10dbg_hit_onb(i1 zeroext %tobool2)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_Z11dbg_mean_ofi(i32 %v) #1 {
entry:
  %v.addr = alloca i32, align 4
  store i32 %v, i32* %v.addr, align 4
  %0 = load i64, i64* getelementptr inbounds ([2 x i64], [2 x i64]* @_ZL5means, i32 0, i32 0), align 16
  %inc = add nsw i64 %0, 1
  store i64 %inc, i64* getelementptr inbounds ([2 x i64], [2 x i64]* @_ZL5means, i32 0, i32 0), align 16
  %1 = load i32, i32* %v.addr, align 4
  %conv = sext i32 %1 to i64
  %2 = load i64, i64* getelementptr inbounds ([2 x i64], [2 x i64]* @_ZL5means, i32 0, i32 1), align 8
  %add = add nsw i64 %2, %conv
  store i64 %add, i64* getelementptr inbounds ([2 x i64], [2 x i64]* @_ZL5means, i32 0, i32 1), align 8
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_Z9dbg_printv() #1 {
entry:
  %0 = load i64, i64* getelementptr inbounds ([2 x i64], [2 x i64]* @_ZL4hits, i32 0, i32 0), align 16
  %tobool = icmp ne i64 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::basic_ostream"* @_ZNSt3__2lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc(%"class.std::__2::basic_ostream"* nonnull align 4 dereferenceable(4) @_ZNSt3__24cerrE, i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.7, i32 0, i32 0))
  %1 = load i64, i64* getelementptr inbounds ([2 x i64], [2 x i64]* @_ZL4hits, i32 0, i32 0), align 16
  %call1 = call nonnull align 4 dereferenceable(4) %"class.std::__2::basic_ostream"* @_ZNSt3__213basic_ostreamIcNS_11char_traitsIcEEElsEx(%"class.std::__2::basic_ostream"* %call, i64 %1)
  %call2 = call nonnull align 4 dereferenceable(4) %"class.std::__2::basic_ostream"* @_ZNSt3__2lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc(%"class.std::__2::basic_ostream"* nonnull align 4 dereferenceable(4) %call1, i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.8, i32 0, i32 0))
  %2 = load i64, i64* getelementptr inbounds ([2 x i64], [2 x i64]* @_ZL4hits, i32 0, i32 1), align 8
  %call3 = call nonnull align 4 dereferenceable(4) %"class.std::__2::basic_ostream"* @_ZNSt3__213basic_ostreamIcNS_11char_traitsIcEEElsEx(%"class.std::__2::basic_ostream"* %call2, i64 %2)
  %call4 = call nonnull align 4 dereferenceable(4) %"class.std::__2::basic_ostream"* @_ZNSt3__2lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc(%"class.std::__2::basic_ostream"* nonnull align 4 dereferenceable(4) %call3, i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str.9, i32 0, i32 0))
  %3 = load i64, i64* getelementptr inbounds ([2 x i64], [2 x i64]* @_ZL4hits, i32 0, i32 1), align 8
  %mul = mul nsw i64 100, %3
  %4 = load i64, i64* getelementptr inbounds ([2 x i64], [2 x i64]* @_ZL4hits, i32 0, i32 0), align 16
  %div = sdiv i64 %mul, %4
  %call5 = call nonnull align 4 dereferenceable(4) %"class.std::__2::basic_ostream"* @_ZNSt3__213basic_ostreamIcNS_11char_traitsIcEEElsEx(%"class.std::__2::basic_ostream"* %call4, i64 %div)
  %call6 = call nonnull align 4 dereferenceable(4) %"class.std::__2::basic_ostream"* @_ZNSt3__213basic_ostreamIcNS_11char_traitsIcEEElsEPFRS3_S4_E(%"class.std::__2::basic_ostream"* %call5, %"class.std::__2::basic_ostream"* (%"class.std::__2::basic_ostream"*)* @_ZNSt3__24endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %5 = load i64, i64* getelementptr inbounds ([2 x i64], [2 x i64]* @_ZL5means, i32 0, i32 0), align 16
  %tobool7 = icmp ne i64 %5, 0
  br i1 %tobool7, label %if.then8, label %if.end16

if.then8:                                         ; preds = %if.end
  %call9 = call nonnull align 4 dereferenceable(4) %"class.std::__2::basic_ostream"* @_ZNSt3__2lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc(%"class.std::__2::basic_ostream"* nonnull align 4 dereferenceable(4) @_ZNSt3__24cerrE, i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.7, i32 0, i32 0))
  %6 = load i64, i64* getelementptr inbounds ([2 x i64], [2 x i64]* @_ZL5means, i32 0, i32 0), align 16
  %call10 = call nonnull align 4 dereferenceable(4) %"class.std::__2::basic_ostream"* @_ZNSt3__213basic_ostreamIcNS_11char_traitsIcEEElsEx(%"class.std::__2::basic_ostream"* %call9, i64 %6)
  %call11 = call nonnull align 4 dereferenceable(4) %"class.std::__2::basic_ostream"* @_ZNSt3__2lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc(%"class.std::__2::basic_ostream"* nonnull align 4 dereferenceable(4) %call10, i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.10, i32 0, i32 0))
  %7 = load i64, i64* getelementptr inbounds ([2 x i64], [2 x i64]* @_ZL5means, i32 0, i32 1), align 8
  %conv = sitofp i64 %7 to double
  %8 = load i64, i64* getelementptr inbounds ([2 x i64], [2 x i64]* @_ZL5means, i32 0, i32 0), align 16
  %conv12 = sitofp i64 %8 to double
  %div13 = fdiv double %conv, %conv12
  %call14 = call nonnull align 4 dereferenceable(4) %"class.std::__2::basic_ostream"* @_ZNSt3__213basic_ostreamIcNS_11char_traitsIcEEElsEd(%"class.std::__2::basic_ostream"* %call11, double %div13)
  %call15 = call nonnull align 4 dereferenceable(4) %"class.std::__2::basic_ostream"* @_ZNSt3__213basic_ostreamIcNS_11char_traitsIcEEElsEPFRS3_S4_E(%"class.std::__2::basic_ostream"* %call14, %"class.std::__2::basic_ostream"* (%"class.std::__2::basic_ostream"*)* @_ZNSt3__24endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_)
  br label %if.end16

if.end16:                                         ; preds = %if.then8, %if.end
  ret void
}

declare nonnull align 4 dereferenceable(4) %"class.std::__2::basic_ostream"* @_ZNSt3__213basic_ostreamIcNS_11char_traitsIcEEElsEx(%"class.std::__2::basic_ostream"*, i64) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::basic_ostream"* @_ZNSt3__213basic_ostreamIcNS_11char_traitsIcEEElsEPFRS3_S4_E(%"class.std::__2::basic_ostream"* %this, %"class.std::__2::basic_ostream"* (%"class.std::__2::basic_ostream"*)* %__pf) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_ostream"*, align 4
  %__pf.addr = alloca %"class.std::__2::basic_ostream"* (%"class.std::__2::basic_ostream"*)*, align 4
  store %"class.std::__2::basic_ostream"* %this, %"class.std::__2::basic_ostream"** %this.addr, align 4
  store %"class.std::__2::basic_ostream"* (%"class.std::__2::basic_ostream"*)* %__pf, %"class.std::__2::basic_ostream"* (%"class.std::__2::basic_ostream"*)** %__pf.addr, align 4
  %this1 = load %"class.std::__2::basic_ostream"*, %"class.std::__2::basic_ostream"** %this.addr, align 4
  %0 = load %"class.std::__2::basic_ostream"* (%"class.std::__2::basic_ostream"*)*, %"class.std::__2::basic_ostream"* (%"class.std::__2::basic_ostream"*)** %__pf.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::basic_ostream"* %0(%"class.std::__2::basic_ostream"* nonnull align 4 dereferenceable(4) %this1)
  ret %"class.std::__2::basic_ostream"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::basic_ostream"* @_ZNSt3__24endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_(%"class.std::__2::basic_ostream"* nonnull align 4 dereferenceable(4) %__os) #1 comdat {
entry:
  %__os.addr = alloca %"class.std::__2::basic_ostream"*, align 4
  store %"class.std::__2::basic_ostream"* %__os, %"class.std::__2::basic_ostream"** %__os.addr, align 4
  %0 = load %"class.std::__2::basic_ostream"*, %"class.std::__2::basic_ostream"** %__os.addr, align 4
  %1 = load %"class.std::__2::basic_ostream"*, %"class.std::__2::basic_ostream"** %__os.addr, align 4
  %2 = bitcast %"class.std::__2::basic_ostream"* %1 to i8**
  %vtable = load i8*, i8** %2, align 4
  %vbase.offset.ptr = getelementptr i8, i8* %vtable, i64 -12
  %3 = bitcast i8* %vbase.offset.ptr to i32*
  %vbase.offset = load i32, i32* %3, align 4
  %4 = bitcast %"class.std::__2::basic_ostream"* %1 to i8*
  %add.ptr = getelementptr inbounds i8, i8* %4, i32 %vbase.offset
  %5 = bitcast i8* %add.ptr to %"class.std::__2::basic_ios"*
  %call = call signext i8 @_ZNKSt3__29basic_iosIcNS_11char_traitsIcEEE5widenEc(%"class.std::__2::basic_ios"* %5, i8 signext 10)
  %call1 = call nonnull align 4 dereferenceable(4) %"class.std::__2::basic_ostream"* @_ZNSt3__213basic_ostreamIcNS_11char_traitsIcEEE3putEc(%"class.std::__2::basic_ostream"* %0, i8 signext %call)
  %6 = load %"class.std::__2::basic_ostream"*, %"class.std::__2::basic_ostream"** %__os.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) %"class.std::__2::basic_ostream"* @_ZNSt3__213basic_ostreamIcNS_11char_traitsIcEEE5flushEv(%"class.std::__2::basic_ostream"* %6)
  %7 = load %"class.std::__2::basic_ostream"*, %"class.std::__2::basic_ostream"** %__os.addr, align 4
  ret %"class.std::__2::basic_ostream"* %7
}

declare nonnull align 4 dereferenceable(4) %"class.std::__2::basic_ostream"* @_ZNSt3__213basic_ostreamIcNS_11char_traitsIcEEElsEd(%"class.std::__2::basic_ostream"*, double) #4

; Function Attrs: noinline nounwind optnone
define hidden nonnull align 4 dereferenceable(4) %"class.std::__2::basic_ostream"* @_ZlsRNSt3__213basic_ostreamIcNS_11char_traitsIcEEEE8SyncCout(%"class.std::__2::basic_ostream"* nonnull align 4 dereferenceable(4) %os, i32 %sc) #1 {
entry:
  %os.addr = alloca %"class.std::__2::basic_ostream"*, align 4
  %sc.addr = alloca i32, align 4
  store %"class.std::__2::basic_ostream"* %os, %"class.std::__2::basic_ostream"** %os.addr, align 4
  store i32 %sc, i32* %sc.addr, align 4
  %0 = load atomic i8, i8* bitcast (i32* @_ZGVZlsRNSt3__213basic_ostreamIcNS_11char_traitsIcEEEE8SyncCoutE1m to i8*) acquire, align 4
  %1 = and i8 %0, 1
  %guard.uninitialized = icmp eq i8 %1, 0
  br i1 %guard.uninitialized, label %init.check, label %init.end, !prof !2

init.check:                                       ; preds = %entry
  %2 = call i32 @__cxa_guard_acquire(i32* @_ZGVZlsRNSt3__213basic_ostreamIcNS_11char_traitsIcEEEE8SyncCoutE1m) #3
  %tobool = icmp ne i32 %2, 0
  br i1 %tobool, label %init, label %init.end

init:                                             ; preds = %init.check
  %3 = call i32 @__cxa_atexit(void (i8*)* @__cxx_global_array_dtor.11, i8* null, i8* @__dso_handle) #3
  call void @__cxa_guard_release(i32* @_ZGVZlsRNSt3__213basic_ostreamIcNS_11char_traitsIcEEEE8SyncCoutE1m) #3
  br label %init.end

init.end:                                         ; preds = %init, %init.check, %entry
  %4 = load i32, i32* %sc.addr, align 4
  %cmp = icmp eq i32 %4, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %init.end
  call void @_ZNSt3__25mutex4lockEv(%"class.std::__2::mutex"* @_ZZlsRNSt3__213basic_ostreamIcNS_11char_traitsIcEEEE8SyncCoutE1m)
  br label %if.end

if.end:                                           ; preds = %if.then, %init.end
  %5 = load i32, i32* %sc.addr, align 4
  %cmp1 = icmp eq i32 %5, 1
  br i1 %cmp1, label %if.then2, label %if.end3

if.then2:                                         ; preds = %if.end
  call void @_ZNSt3__25mutex6unlockEv(%"class.std::__2::mutex"* @_ZZlsRNSt3__213basic_ostreamIcNS_11char_traitsIcEEEE8SyncCoutE1m) #3
  br label %if.end3

if.end3:                                          ; preds = %if.then2, %if.end
  %6 = load %"class.std::__2::basic_ostream"*, %"class.std::__2::basic_ostream"** %os.addr, align 4
  ret %"class.std::__2::basic_ostream"* %6
}

; Function Attrs: nounwind
declare i32 @__cxa_guard_acquire(i32*) #3

; Function Attrs: noinline nounwind
define internal void @__cxx_global_array_dtor.11(i8* %0) #0 {
entry:
  %.addr = alloca i8*, align 4
  store i8* %0, i8** %.addr, align 4
  %call = call %"class.std::__2::mutex"* @_ZNSt3__25mutexD1Ev(%"class.std::__2::mutex"* @_ZZlsRNSt3__213basic_ostreamIcNS_11char_traitsIcEEEE8SyncCoutE1m) #3
  ret void
}

; Function Attrs: nounwind
declare %"class.std::__2::mutex"* @_ZNSt3__25mutexD1Ev(%"class.std::__2::mutex"* returned) unnamed_addr #2

; Function Attrs: nounwind
declare void @__cxa_guard_release(i32*) #3

declare void @_ZNSt3__25mutex4lockEv(%"class.std::__2::mutex"*) #4

; Function Attrs: nounwind
declare void @_ZNSt3__25mutex6unlockEv(%"class.std::__2::mutex"*) #2

; Function Attrs: noinline nounwind optnone
define hidden void @_Z12start_loggerRKNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE(%"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %fname) #1 {
entry:
  %fname.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %fname, %"class.std::__2::basic_string"** %fname.addr, align 4
  %0 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %fname.addr, align 4
  call void @_ZN12_GLOBAL__N_16Logger5startERKNSt3__212basic_stringIcNS1_11char_traitsIcEENS1_9allocatorIcEEEE(%"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %0)
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @_ZN12_GLOBAL__N_16Logger5startERKNSt3__212basic_stringIcNS1_11char_traitsIcEENS1_9allocatorIcEEEE(%"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %fname) #1 {
entry:
  %fname.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %fname, %"class.std::__2::basic_string"** %fname.addr, align 4
  %0 = load atomic i8, i8* bitcast (i32* @_ZGVZN12_GLOBAL__N_16Logger5startERKNSt3__212basic_stringIcNS1_11char_traitsIcEENS1_9allocatorIcEEEEE1l to i8*) acquire, align 4
  %1 = and i8 %0, 1
  %guard.uninitialized = icmp eq i8 %1, 0
  br i1 %guard.uninitialized, label %init.check, label %init.end, !prof !2

init.check:                                       ; preds = %entry
  %2 = call i32 @__cxa_guard_acquire(i32* @_ZGVZN12_GLOBAL__N_16Logger5startERKNSt3__212basic_stringIcNS1_11char_traitsIcEENS1_9allocatorIcEEEEE1l) #3
  %tobool = icmp ne i32 %2, 0
  br i1 %tobool, label %init, label %init.end

init:                                             ; preds = %init.check
  %call = call %"class.(anonymous namespace)::Logger"* @_ZN12_GLOBAL__N_16LoggerC2Ev(%"class.(anonymous namespace)::Logger"* @_ZZN12_GLOBAL__N_16Logger5startERKNSt3__212basic_stringIcNS1_11char_traitsIcEENS1_9allocatorIcEEEEE1l)
  %3 = call i32 @__cxa_atexit(void (i8*)* @__cxx_global_array_dtor.12, i8* null, i8* @__dso_handle) #3
  call void @__cxa_guard_release(i32* @_ZGVZN12_GLOBAL__N_16Logger5startERKNSt3__212basic_stringIcNS1_11char_traitsIcEENS1_9allocatorIcEEEEE1l) #3
  br label %init.end

init.end:                                         ; preds = %init, %init.check, %entry
  %4 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %fname.addr, align 4
  %call1 = call zeroext i1 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5emptyEv(%"class.std::__2::basic_string"* %4) #3
  br i1 %call1, label %if.else, label %land.lhs.true

land.lhs.true:                                    ; preds = %init.end
  %call2 = call zeroext i1 @_ZNKSt3__214basic_ofstreamIcNS_11char_traitsIcEEE7is_openEv(%"class.std::__2::basic_ofstream"* getelementptr inbounds (%"class.(anonymous namespace)::Logger", %"class.(anonymous namespace)::Logger"* @_ZZN12_GLOBAL__N_16Logger5startERKNSt3__212basic_stringIcNS1_11char_traitsIcEENS1_9allocatorIcEEEEE1l, i32 0, i32 0))
  br i1 %call2, label %if.else, label %if.then

if.then:                                          ; preds = %land.lhs.true
  %5 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %fname.addr, align 4
  call void @_ZNSt3__214basic_ofstreamIcNS_11char_traitsIcEEE4openERKNS_12basic_stringIcS2_NS_9allocatorIcEEEEj(%"class.std::__2::basic_ofstream"* getelementptr inbounds (%"class.(anonymous namespace)::Logger", %"class.(anonymous namespace)::Logger"* @_ZZN12_GLOBAL__N_16Logger5startERKNSt3__212basic_stringIcNS1_11char_traitsIcEENS1_9allocatorIcEEEEE1l, i32 0, i32 0), %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %5, i32 16)
  %vtable = load i8*, i8** bitcast (%"class.std::__2::basic_istream"* @_ZNSt3__23cinE to i8**), align 4
  %vbase.offset.ptr = getelementptr i8, i8* %vtable, i64 -12
  %6 = bitcast i8* %vbase.offset.ptr to i32*
  %vbase.offset = load i32, i32* %6, align 4
  %add.ptr = getelementptr inbounds i8, i8* bitcast (%"class.std::__2::basic_istream"* @_ZNSt3__23cinE to i8*), i32 %vbase.offset
  %7 = bitcast i8* %add.ptr to %"class.std::__2::basic_ios"*
  %call3 = call %"class.std::__2::basic_streambuf"* @_ZNSt3__29basic_iosIcNS_11char_traitsIcEEE5rdbufEPNS_15basic_streambufIcS2_EE(%"class.std::__2::basic_ios"* %7, %"class.std::__2::basic_streambuf"* getelementptr inbounds (%"class.(anonymous namespace)::Logger", %"class.(anonymous namespace)::Logger"* @_ZZN12_GLOBAL__N_16Logger5startERKNSt3__212basic_stringIcNS1_11char_traitsIcEENS1_9allocatorIcEEEEE1l, i32 0, i32 1, i32 0))
  %vtable4 = load i8*, i8** bitcast (%"class.std::__2::basic_ostream"* @_ZNSt3__24coutE to i8**), align 4
  %vbase.offset.ptr5 = getelementptr i8, i8* %vtable4, i64 -12
  %8 = bitcast i8* %vbase.offset.ptr5 to i32*
  %vbase.offset6 = load i32, i32* %8, align 4
  %add.ptr7 = getelementptr inbounds i8, i8* bitcast (%"class.std::__2::basic_ostream"* @_ZNSt3__24coutE to i8*), i32 %vbase.offset6
  %9 = bitcast i8* %add.ptr7 to %"class.std::__2::basic_ios"*
  %call8 = call %"class.std::__2::basic_streambuf"* @_ZNSt3__29basic_iosIcNS_11char_traitsIcEEE5rdbufEPNS_15basic_streambufIcS2_EE(%"class.std::__2::basic_ios"* %9, %"class.std::__2::basic_streambuf"* getelementptr inbounds (%"class.(anonymous namespace)::Logger", %"class.(anonymous namespace)::Logger"* @_ZZN12_GLOBAL__N_16Logger5startERKNSt3__212basic_stringIcNS1_11char_traitsIcEENS1_9allocatorIcEEEEE1l, i32 0, i32 2, i32 0))
  br label %if.end23

if.else:                                          ; preds = %land.lhs.true, %init.end
  %10 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %fname.addr, align 4
  %call9 = call zeroext i1 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5emptyEv(%"class.std::__2::basic_string"* %10) #3
  br i1 %call9, label %land.lhs.true10, label %if.end

land.lhs.true10:                                  ; preds = %if.else
  %call11 = call zeroext i1 @_ZNKSt3__214basic_ofstreamIcNS_11char_traitsIcEEE7is_openEv(%"class.std::__2::basic_ofstream"* getelementptr inbounds (%"class.(anonymous namespace)::Logger", %"class.(anonymous namespace)::Logger"* @_ZZN12_GLOBAL__N_16Logger5startERKNSt3__212basic_stringIcNS1_11char_traitsIcEENS1_9allocatorIcEEEEE1l, i32 0, i32 0))
  br i1 %call11, label %if.then12, label %if.end

if.then12:                                        ; preds = %land.lhs.true10
  %vtable13 = load i8*, i8** bitcast (%"class.std::__2::basic_ostream"* @_ZNSt3__24coutE to i8**), align 4
  %vbase.offset.ptr14 = getelementptr i8, i8* %vtable13, i64 -12
  %11 = bitcast i8* %vbase.offset.ptr14 to i32*
  %vbase.offset15 = load i32, i32* %11, align 4
  %add.ptr16 = getelementptr inbounds i8, i8* bitcast (%"class.std::__2::basic_ostream"* @_ZNSt3__24coutE to i8*), i32 %vbase.offset15
  %12 = bitcast i8* %add.ptr16 to %"class.std::__2::basic_ios"*
  %13 = load %"class.std::__2::basic_streambuf"*, %"class.std::__2::basic_streambuf"** getelementptr inbounds (%"class.(anonymous namespace)::Logger", %"class.(anonymous namespace)::Logger"* @_ZZN12_GLOBAL__N_16Logger5startERKNSt3__212basic_stringIcNS1_11char_traitsIcEENS1_9allocatorIcEEEEE1l, i32 0, i32 2, i32 1), align 4
  %call17 = call %"class.std::__2::basic_streambuf"* @_ZNSt3__29basic_iosIcNS_11char_traitsIcEEE5rdbufEPNS_15basic_streambufIcS2_EE(%"class.std::__2::basic_ios"* %12, %"class.std::__2::basic_streambuf"* %13)
  %vtable18 = load i8*, i8** bitcast (%"class.std::__2::basic_istream"* @_ZNSt3__23cinE to i8**), align 4
  %vbase.offset.ptr19 = getelementptr i8, i8* %vtable18, i64 -12
  %14 = bitcast i8* %vbase.offset.ptr19 to i32*
  %vbase.offset20 = load i32, i32* %14, align 4
  %add.ptr21 = getelementptr inbounds i8, i8* bitcast (%"class.std::__2::basic_istream"* @_ZNSt3__23cinE to i8*), i32 %vbase.offset20
  %15 = bitcast i8* %add.ptr21 to %"class.std::__2::basic_ios"*
  %16 = load %"class.std::__2::basic_streambuf"*, %"class.std::__2::basic_streambuf"** getelementptr inbounds (%"class.(anonymous namespace)::Logger", %"class.(anonymous namespace)::Logger"* @_ZZN12_GLOBAL__N_16Logger5startERKNSt3__212basic_stringIcNS1_11char_traitsIcEENS1_9allocatorIcEEEEE1l, i32 0, i32 1, i32 1), align 4
  %call22 = call %"class.std::__2::basic_streambuf"* @_ZNSt3__29basic_iosIcNS_11char_traitsIcEEE5rdbufEPNS_15basic_streambufIcS2_EE(%"class.std::__2::basic_ios"* %15, %"class.std::__2::basic_streambuf"* %16)
  call void @_ZNSt3__214basic_ofstreamIcNS_11char_traitsIcEEE5closeEv(%"class.std::__2::basic_ofstream"* getelementptr inbounds (%"class.(anonymous namespace)::Logger", %"class.(anonymous namespace)::Logger"* @_ZZN12_GLOBAL__N_16Logger5startERKNSt3__212basic_stringIcNS1_11char_traitsIcEENS1_9allocatorIcEEEEE1l, i32 0, i32 0))
  br label %if.end

if.end:                                           ; preds = %if.then12, %land.lhs.true10, %if.else
  br label %if.end23

if.end23:                                         ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_Z8prefetchPv(i8* %0) #1 {
entry:
  %.addr = alloca i8*, align 4
  store i8* %0, i8** %.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_Z9prefetch2Pv(i8* %addr) #1 {
entry:
  %addr.addr = alloca i8*, align 4
  store i8* %addr, i8** %addr.addr, align 4
  %0 = load i8*, i8** %addr.addr, align 4
  call void @_Z8prefetchPv(i8* %0)
  %1 = load i8*, i8** %addr.addr, align 4
  %add.ptr = getelementptr inbounds i8, i8* %1, i32 64
  call void @_Z8prefetchPv(i8* %add.ptr)
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN12WinProcGroup14bindThisThreadEm(i32 %0) #1 {
entry:
  %.addr = alloca i32, align 4
  store i32 %0, i32* %.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::basic_stringstream"* @_ZNSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED2Ev(%"class.std::__2::basic_stringstream"* returned %this, i8** %vtt) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_stringstream"*, align 4
  %vtt.addr = alloca i8**, align 4
  store %"class.std::__2::basic_stringstream"* %this, %"class.std::__2::basic_stringstream"** %this.addr, align 4
  store i8** %vtt, i8*** %vtt.addr, align 4
  %this1 = load %"class.std::__2::basic_stringstream"*, %"class.std::__2::basic_stringstream"** %this.addr, align 4
  %vtt2 = load i8**, i8*** %vtt.addr, align 4
  %0 = load i8*, i8** %vtt2, align 4
  %1 = bitcast %"class.std::__2::basic_stringstream"* %this1 to i32 (...)***
  %2 = bitcast i8* %0 to i32 (...)**
  store i32 (...)** %2, i32 (...)*** %1, align 4
  %3 = getelementptr inbounds i8*, i8** %vtt2, i64 8
  %4 = load i8*, i8** %3, align 4
  %5 = bitcast %"class.std::__2::basic_stringstream"* %this1 to i8**
  %vtable = load i8*, i8** %5, align 4
  %vbase.offset.ptr = getelementptr i8, i8* %vtable, i64 -12
  %6 = bitcast i8* %vbase.offset.ptr to i32*
  %vbase.offset = load i32, i32* %6, align 4
  %7 = bitcast %"class.std::__2::basic_stringstream"* %this1 to i8*
  %add.ptr = getelementptr inbounds i8, i8* %7, i32 %vbase.offset
  %8 = bitcast i8* %add.ptr to i32 (...)***
  %9 = bitcast i8* %4 to i32 (...)**
  store i32 (...)** %9, i32 (...)*** %8, align 4
  %10 = getelementptr inbounds i8*, i8** %vtt2, i64 9
  %11 = load i8*, i8** %10, align 4
  %12 = bitcast %"class.std::__2::basic_stringstream"* %this1 to i8*
  %add.ptr3 = getelementptr inbounds i8, i8* %12, i32 8
  %13 = bitcast i8* %add.ptr3 to i32 (...)***
  %14 = bitcast i8* %11 to i32 (...)**
  store i32 (...)** %14, i32 (...)*** %13, align 4
  %__sb_ = getelementptr inbounds %"class.std::__2::basic_stringstream", %"class.std::__2::basic_stringstream"* %this1, i32 0, i32 1
  %call = call %"class.std::__2::basic_stringbuf"* @_ZNSt3__215basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED2Ev(%"class.std::__2::basic_stringbuf"* %__sb_) #3
  %15 = bitcast %"class.std::__2::basic_stringstream"* %this1 to %"class.std::__2::basic_iostream"*
  %16 = getelementptr inbounds i8*, i8** %vtt2, i64 1
  %call4 = call %"class.std::__2::basic_iostream"* @_ZNSt3__214basic_iostreamIcNS_11char_traitsIcEEED2Ev(%"class.std::__2::basic_iostream"* %15, i8** %16) #3
  ret %"class.std::__2::basic_stringstream"* %this1
}

; Function Attrs: nounwind
declare %"class.std::__2::basic_ios"* @_ZNSt3__29basic_iosIcNS_11char_traitsIcEEED2Ev(%"class.std::__2::basic_ios"* returned) unnamed_addr #2

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::basic_stringstream"* @_ZThn8_NSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_stringstream"* %this) unnamed_addr #1 comdat {
entry:
  %retval = alloca %"class.std::__2::basic_stringstream"*, align 4
  %this.addr = alloca %"class.std::__2::basic_stringstream"*, align 4
  store %"class.std::__2::basic_stringstream"* %this, %"class.std::__2::basic_stringstream"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_stringstream"*, %"class.std::__2::basic_stringstream"** %this.addr, align 4
  store %"class.std::__2::basic_stringstream"* %this1, %"class.std::__2::basic_stringstream"** %retval, align 4
  %0 = bitcast %"class.std::__2::basic_stringstream"* %this1 to i8*
  %1 = getelementptr inbounds i8, i8* %0, i32 -8
  %2 = bitcast i8* %1 to %"class.std::__2::basic_stringstream"*
  %call = tail call %"class.std::__2::basic_stringstream"* @_ZNSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_stringstream"* %2) #3
  ret %"class.std::__2::basic_stringstream"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::basic_stringstream"* @_ZTv0_n12_NSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_stringstream"* %this) unnamed_addr #1 comdat {
entry:
  %retval = alloca %"class.std::__2::basic_stringstream"*, align 4
  %this.addr = alloca %"class.std::__2::basic_stringstream"*, align 4
  store %"class.std::__2::basic_stringstream"* %this, %"class.std::__2::basic_stringstream"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_stringstream"*, %"class.std::__2::basic_stringstream"** %this.addr, align 4
  store %"class.std::__2::basic_stringstream"* %this1, %"class.std::__2::basic_stringstream"** %retval, align 4
  %0 = bitcast %"class.std::__2::basic_stringstream"* %this1 to i8*
  %1 = bitcast i8* %0 to i8**
  %2 = load i8*, i8** %1, align 4
  %3 = getelementptr inbounds i8, i8* %2, i64 -12
  %4 = bitcast i8* %3 to i32*
  %5 = load i32, i32* %4, align 4
  %6 = getelementptr inbounds i8, i8* %0, i32 %5
  %7 = bitcast i8* %6 to %"class.std::__2::basic_stringstream"*
  %call = tail call %"class.std::__2::basic_stringstream"* @_ZNSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_stringstream"* %7) #3
  ret %"class.std::__2::basic_stringstream"* %call
}

; Function Attrs: nounwind
declare %"class.std::__2::basic_iostream"* @_ZNSt3__214basic_iostreamIcNS_11char_traitsIcEEED1Ev(%"class.std::__2::basic_iostream"* returned) unnamed_addr #2

; Function Attrs: nounwind
declare void @_ZNSt3__214basic_iostreamIcNS_11char_traitsIcEEED0Ev(%"class.std::__2::basic_iostream"*) unnamed_addr #2

; Function Attrs: nounwind
declare %"class.std::__2::basic_iostream"* @_ZThn8_NSt3__214basic_iostreamIcNS_11char_traitsIcEEED1Ev(%"class.std::__2::basic_iostream"*) unnamed_addr #2

; Function Attrs: nounwind
declare void @_ZThn8_NSt3__214basic_iostreamIcNS_11char_traitsIcEEED0Ev(%"class.std::__2::basic_iostream"*) unnamed_addr #2

; Function Attrs: nounwind
declare %"class.std::__2::basic_iostream"* @_ZTv0_n12_NSt3__214basic_iostreamIcNS_11char_traitsIcEEED1Ev(%"class.std::__2::basic_iostream"*) unnamed_addr #2

; Function Attrs: nounwind
declare void @_ZTv0_n12_NSt3__214basic_iostreamIcNS_11char_traitsIcEEED0Ev(%"class.std::__2::basic_iostream"*) unnamed_addr #2

; Function Attrs: nounwind
declare %"class.std::__2::basic_istream"* @_ZNSt3__213basic_istreamIcNS_11char_traitsIcEEED1Ev(%"class.std::__2::basic_istream"* returned) unnamed_addr #2

; Function Attrs: nounwind
declare void @_ZNSt3__213basic_istreamIcNS_11char_traitsIcEEED0Ev(%"class.std::__2::basic_istream"*) unnamed_addr #2

; Function Attrs: nounwind
declare %"class.std::__2::basic_istream"* @_ZTv0_n12_NSt3__213basic_istreamIcNS_11char_traitsIcEEED1Ev(%"class.std::__2::basic_istream"*) unnamed_addr #2

; Function Attrs: nounwind
declare void @_ZTv0_n12_NSt3__213basic_istreamIcNS_11char_traitsIcEEED0Ev(%"class.std::__2::basic_istream"*) unnamed_addr #2

; Function Attrs: nounwind
declare %"class.std::__2::basic_ostream"* @_ZNSt3__213basic_ostreamIcNS_11char_traitsIcEEED1Ev(%"class.std::__2::basic_ostream"* returned) unnamed_addr #2

; Function Attrs: nounwind
declare void @_ZNSt3__213basic_ostreamIcNS_11char_traitsIcEEED0Ev(%"class.std::__2::basic_ostream"*) unnamed_addr #2

; Function Attrs: nounwind
declare %"class.std::__2::basic_ostream"* @_ZTv0_n12_NSt3__213basic_ostreamIcNS_11char_traitsIcEEED1Ev(%"class.std::__2::basic_ostream"*) unnamed_addr #2

; Function Attrs: nounwind
declare void @_ZTv0_n12_NSt3__213basic_ostreamIcNS_11char_traitsIcEEED0Ev(%"class.std::__2::basic_ostream"*) unnamed_addr #2

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev(%"class.std::__2::basic_stringstream"* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_stringstream"*, align 4
  store %"class.std::__2::basic_stringstream"* %this, %"class.std::__2::basic_stringstream"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_stringstream"*, %"class.std::__2::basic_stringstream"** %this.addr, align 4
  %call = call %"class.std::__2::basic_stringstream"* @_ZNSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_stringstream"* %this1) #3
  %0 = bitcast %"class.std::__2::basic_stringstream"* %this1 to i8*
  call void @_ZdlPv(i8* %0) #11
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZThn8_NSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev(%"class.std::__2::basic_stringstream"* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_stringstream"*, align 4
  store %"class.std::__2::basic_stringstream"* %this, %"class.std::__2::basic_stringstream"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_stringstream"*, %"class.std::__2::basic_stringstream"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::basic_stringstream"* %this1 to i8*
  %1 = getelementptr inbounds i8, i8* %0, i32 -8
  %2 = bitcast i8* %1 to %"class.std::__2::basic_stringstream"*
  tail call void @_ZNSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev(%"class.std::__2::basic_stringstream"* %2) #3
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZTv0_n12_NSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev(%"class.std::__2::basic_stringstream"* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_stringstream"*, align 4
  store %"class.std::__2::basic_stringstream"* %this, %"class.std::__2::basic_stringstream"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_stringstream"*, %"class.std::__2::basic_stringstream"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::basic_stringstream"* %this1 to i8*
  %1 = bitcast i8* %0 to i8**
  %2 = load i8*, i8** %1, align 4
  %3 = getelementptr inbounds i8, i8* %2, i64 -12
  %4 = bitcast i8* %3 to i32*
  %5 = load i32, i32* %4, align 4
  %6 = getelementptr inbounds i8, i8* %0, i32 %5
  %7 = bitcast i8* %6 to %"class.std::__2::basic_stringstream"*
  tail call void @_ZNSt3__218basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev(%"class.std::__2::basic_stringstream"* %7) #3
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::basic_stringbuf"* @_ZNSt3__215basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED2Ev(%"class.std::__2::basic_stringbuf"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_stringbuf"*, align 4
  store %"class.std::__2::basic_stringbuf"* %this, %"class.std::__2::basic_stringbuf"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_stringbuf"*, %"class.std::__2::basic_stringbuf"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::basic_stringbuf"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [16 x i8*] }, { [16 x i8*] }* @_ZTVNSt3__215basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %__str_ = getelementptr inbounds %"class.std::__2::basic_stringbuf", %"class.std::__2::basic_stringbuf"* %this1, i32 0, i32 1
  %call = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_string"* %__str_) #3
  %1 = bitcast %"class.std::__2::basic_stringbuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call2 = call %"class.std::__2::basic_streambuf"* @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEED2Ev(%"class.std::__2::basic_streambuf"* %1) #3
  ret %"class.std::__2::basic_stringbuf"* %this1
}

; Function Attrs: nounwind
declare %"class.std::__2::basic_iostream"* @_ZNSt3__214basic_iostreamIcNS_11char_traitsIcEEED2Ev(%"class.std::__2::basic_iostream"* returned, i8**) unnamed_addr #2

; Function Attrs: nounwind
declare %"class.std::__2::basic_streambuf"* @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEED2Ev(%"class.std::__2::basic_streambuf"* returned) unnamed_addr #2

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__215basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev(%"class.std::__2::basic_stringbuf"* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_stringbuf"*, align 4
  store %"class.std::__2::basic_stringbuf"* %this, %"class.std::__2::basic_stringbuf"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_stringbuf"*, %"class.std::__2::basic_stringbuf"** %this.addr, align 4
  %call = call %"class.std::__2::basic_stringbuf"* @_ZNSt3__215basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED2Ev(%"class.std::__2::basic_stringbuf"* %this1) #3
  %0 = bitcast %"class.std::__2::basic_stringbuf"* %this1 to i8*
  call void @_ZdlPv(i8* %0) #11
  ret void
}

declare void @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE5imbueERKNS_6localeE(%"class.std::__2::basic_streambuf"*, %"class.std::__2::locale"* nonnull align 4 dereferenceable(4)) unnamed_addr #4

declare %"class.std::__2::basic_streambuf"* @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE6setbufEPcl(%"class.std::__2::basic_streambuf"*, i8*, i32) unnamed_addr #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__215basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekoffExNS_8ios_base7seekdirEj(%"class.std::__2::fpos"* noalias sret align 8 %agg.result, %"class.std::__2::basic_stringbuf"* %this, i64 %__off, i32 %__way, i32 %__wch) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_stringbuf"*, align 4
  %__off.addr = alloca i64, align 8
  %__way.addr = alloca i32, align 4
  %__wch.addr = alloca i32, align 4
  %__hm = alloca i32, align 4
  %__noff = alloca i64, align 8
  store %"class.std::__2::basic_stringbuf"* %this, %"class.std::__2::basic_stringbuf"** %this.addr, align 4
  store i64 %__off, i64* %__off.addr, align 8
  store i32 %__way, i32* %__way.addr, align 4
  store i32 %__wch, i32* %__wch.addr, align 4
  %this1 = load %"class.std::__2::basic_stringbuf"*, %"class.std::__2::basic_stringbuf"** %this.addr, align 4
  %__hm_ = getelementptr inbounds %"class.std::__2::basic_stringbuf", %"class.std::__2::basic_stringbuf"* %this1, i32 0, i32 2
  %0 = load i8*, i8** %__hm_, align 4
  %1 = bitcast %"class.std::__2::basic_stringbuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE4pptrEv(%"class.std::__2::basic_streambuf"* %1)
  %cmp = icmp ult i8* %0, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = bitcast %"class.std::__2::basic_stringbuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call2 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE4pptrEv(%"class.std::__2::basic_streambuf"* %2)
  %__hm_3 = getelementptr inbounds %"class.std::__2::basic_stringbuf", %"class.std::__2::basic_stringbuf"* %this1, i32 0, i32 2
  store i8* %call2, i8** %__hm_3, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %3 = load i32, i32* %__wch.addr, align 4
  %and = and i32 %3, 24
  %cmp4 = icmp eq i32 %and, 0
  br i1 %cmp4, label %if.then5, label %if.end7

if.then5:                                         ; preds = %if.end
  %call6 = call %"class.std::__2::fpos"* @_ZNSt3__24fposI11__mbstate_tEC2Ex(%"class.std::__2::fpos"* %agg.result, i64 -1)
  br label %return

if.end7:                                          ; preds = %if.end
  %4 = load i32, i32* %__wch.addr, align 4
  %and8 = and i32 %4, 24
  %cmp9 = icmp eq i32 %and8, 24
  br i1 %cmp9, label %land.lhs.true, label %if.end13

land.lhs.true:                                    ; preds = %if.end7
  %5 = load i32, i32* %__way.addr, align 4
  %cmp10 = icmp eq i32 %5, 1
  br i1 %cmp10, label %if.then11, label %if.end13

if.then11:                                        ; preds = %land.lhs.true
  %call12 = call %"class.std::__2::fpos"* @_ZNSt3__24fposI11__mbstate_tEC2Ex(%"class.std::__2::fpos"* %agg.result, i64 -1)
  br label %return

if.end13:                                         ; preds = %land.lhs.true, %if.end7
  %__hm_14 = getelementptr inbounds %"class.std::__2::basic_stringbuf", %"class.std::__2::basic_stringbuf"* %this1, i32 0, i32 2
  %6 = load i8*, i8** %__hm_14, align 4
  %cmp15 = icmp eq i8* %6, null
  br i1 %cmp15, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end13
  br label %cond.end

cond.false:                                       ; preds = %if.end13
  %__hm_16 = getelementptr inbounds %"class.std::__2::basic_stringbuf", %"class.std::__2::basic_stringbuf"* %this1, i32 0, i32 2
  %7 = load i8*, i8** %__hm_16, align 4
  %__str_ = getelementptr inbounds %"class.std::__2::basic_stringbuf", %"class.std::__2::basic_stringbuf"* %this1, i32 0, i32 1
  %call17 = call i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv(%"class.std::__2::basic_string"* %__str_) #3
  %sub.ptr.lhs.cast = ptrtoint i8* %7 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %call17 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ 0, %cond.true ], [ %sub.ptr.sub, %cond.false ]
  store i32 %cond, i32* %__hm, align 4
  %8 = load i32, i32* %__way.addr, align 4
  switch i32 %8, label %sw.default [
    i32 0, label %sw.bb
    i32 1, label %sw.bb18
    i32 2, label %sw.bb33
  ]

sw.bb:                                            ; preds = %cond.end
  store i64 0, i64* %__noff, align 8
  br label %sw.epilog

sw.bb18:                                          ; preds = %cond.end
  %9 = load i32, i32* %__wch.addr, align 4
  %and19 = and i32 %9, 8
  %tobool = icmp ne i32 %and19, 0
  br i1 %tobool, label %if.then20, label %if.else

if.then20:                                        ; preds = %sw.bb18
  %10 = bitcast %"class.std::__2::basic_stringbuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call21 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE4gptrEv(%"class.std::__2::basic_streambuf"* %10)
  %11 = bitcast %"class.std::__2::basic_stringbuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call22 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE5ebackEv(%"class.std::__2::basic_streambuf"* %11)
  %sub.ptr.lhs.cast23 = ptrtoint i8* %call21 to i32
  %sub.ptr.rhs.cast24 = ptrtoint i8* %call22 to i32
  %sub.ptr.sub25 = sub i32 %sub.ptr.lhs.cast23, %sub.ptr.rhs.cast24
  %conv = sext i32 %sub.ptr.sub25 to i64
  store i64 %conv, i64* %__noff, align 8
  br label %if.end32

if.else:                                          ; preds = %sw.bb18
  %12 = bitcast %"class.std::__2::basic_stringbuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call26 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE4pptrEv(%"class.std::__2::basic_streambuf"* %12)
  %13 = bitcast %"class.std::__2::basic_stringbuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call27 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE5pbaseEv(%"class.std::__2::basic_streambuf"* %13)
  %sub.ptr.lhs.cast28 = ptrtoint i8* %call26 to i32
  %sub.ptr.rhs.cast29 = ptrtoint i8* %call27 to i32
  %sub.ptr.sub30 = sub i32 %sub.ptr.lhs.cast28, %sub.ptr.rhs.cast29
  %conv31 = sext i32 %sub.ptr.sub30 to i64
  store i64 %conv31, i64* %__noff, align 8
  br label %if.end32

if.end32:                                         ; preds = %if.else, %if.then20
  br label %sw.epilog

sw.bb33:                                          ; preds = %cond.end
  %14 = load i32, i32* %__hm, align 4
  %conv34 = sext i32 %14 to i64
  store i64 %conv34, i64* %__noff, align 8
  br label %sw.epilog

sw.default:                                       ; preds = %cond.end
  %call35 = call %"class.std::__2::fpos"* @_ZNSt3__24fposI11__mbstate_tEC2Ex(%"class.std::__2::fpos"* %agg.result, i64 -1)
  br label %return

sw.epilog:                                        ; preds = %sw.bb33, %if.end32, %sw.bb
  %15 = load i64, i64* %__off.addr, align 8
  %16 = load i64, i64* %__noff, align 8
  %add = add nsw i64 %16, %15
  store i64 %add, i64* %__noff, align 8
  %17 = load i64, i64* %__noff, align 8
  %cmp36 = icmp slt i64 %17, 0
  br i1 %cmp36, label %if.then39, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %sw.epilog
  %18 = load i32, i32* %__hm, align 4
  %conv37 = sext i32 %18 to i64
  %19 = load i64, i64* %__noff, align 8
  %cmp38 = icmp slt i64 %conv37, %19
  br i1 %cmp38, label %if.then39, label %if.end41

if.then39:                                        ; preds = %lor.lhs.false, %sw.epilog
  %call40 = call %"class.std::__2::fpos"* @_ZNSt3__24fposI11__mbstate_tEC2Ex(%"class.std::__2::fpos"* %agg.result, i64 -1)
  br label %return

if.end41:                                         ; preds = %lor.lhs.false
  %20 = load i64, i64* %__noff, align 8
  %cmp42 = icmp ne i64 %20, 0
  br i1 %cmp42, label %if.then43, label %if.end60

if.then43:                                        ; preds = %if.end41
  %21 = load i32, i32* %__wch.addr, align 4
  %and44 = and i32 %21, 8
  %tobool45 = icmp ne i32 %and44, 0
  br i1 %tobool45, label %land.lhs.true46, label %if.end51

land.lhs.true46:                                  ; preds = %if.then43
  %22 = bitcast %"class.std::__2::basic_stringbuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call47 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE4gptrEv(%"class.std::__2::basic_streambuf"* %22)
  %cmp48 = icmp eq i8* %call47, null
  br i1 %cmp48, label %if.then49, label %if.end51

if.then49:                                        ; preds = %land.lhs.true46
  %call50 = call %"class.std::__2::fpos"* @_ZNSt3__24fposI11__mbstate_tEC2Ex(%"class.std::__2::fpos"* %agg.result, i64 -1)
  br label %return

if.end51:                                         ; preds = %land.lhs.true46, %if.then43
  %23 = load i32, i32* %__wch.addr, align 4
  %and52 = and i32 %23, 16
  %tobool53 = icmp ne i32 %and52, 0
  br i1 %tobool53, label %land.lhs.true54, label %if.end59

land.lhs.true54:                                  ; preds = %if.end51
  %24 = bitcast %"class.std::__2::basic_stringbuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call55 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE4pptrEv(%"class.std::__2::basic_streambuf"* %24)
  %cmp56 = icmp eq i8* %call55, null
  br i1 %cmp56, label %if.then57, label %if.end59

if.then57:                                        ; preds = %land.lhs.true54
  %call58 = call %"class.std::__2::fpos"* @_ZNSt3__24fposI11__mbstate_tEC2Ex(%"class.std::__2::fpos"* %agg.result, i64 -1)
  br label %return

if.end59:                                         ; preds = %land.lhs.true54, %if.end51
  br label %if.end60

if.end60:                                         ; preds = %if.end59, %if.end41
  %25 = load i32, i32* %__wch.addr, align 4
  %and61 = and i32 %25, 8
  %tobool62 = icmp ne i32 %and61, 0
  br i1 %tobool62, label %if.then63, label %if.end67

if.then63:                                        ; preds = %if.end60
  %26 = bitcast %"class.std::__2::basic_stringbuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %27 = bitcast %"class.std::__2::basic_stringbuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call64 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE5ebackEv(%"class.std::__2::basic_streambuf"* %27)
  %28 = bitcast %"class.std::__2::basic_stringbuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call65 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE5ebackEv(%"class.std::__2::basic_streambuf"* %28)
  %29 = load i64, i64* %__noff, align 8
  %idx.ext = trunc i64 %29 to i32
  %add.ptr = getelementptr inbounds i8, i8* %call65, i32 %idx.ext
  %__hm_66 = getelementptr inbounds %"class.std::__2::basic_stringbuf", %"class.std::__2::basic_stringbuf"* %this1, i32 0, i32 2
  %30 = load i8*, i8** %__hm_66, align 4
  call void @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE4setgEPcS4_S4_(%"class.std::__2::basic_streambuf"* %26, i8* %call64, i8* %add.ptr, i8* %30)
  br label %if.end67

if.end67:                                         ; preds = %if.then63, %if.end60
  %31 = load i32, i32* %__wch.addr, align 4
  %and68 = and i32 %31, 16
  %tobool69 = icmp ne i32 %and68, 0
  br i1 %tobool69, label %if.then70, label %if.end74

if.then70:                                        ; preds = %if.end67
  %32 = bitcast %"class.std::__2::basic_stringbuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %33 = bitcast %"class.std::__2::basic_stringbuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call71 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE5pbaseEv(%"class.std::__2::basic_streambuf"* %33)
  %34 = bitcast %"class.std::__2::basic_stringbuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call72 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE5epptrEv(%"class.std::__2::basic_streambuf"* %34)
  call void @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE4setpEPcS4_(%"class.std::__2::basic_streambuf"* %32, i8* %call71, i8* %call72)
  %35 = bitcast %"class.std::__2::basic_stringbuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %36 = load i64, i64* %__noff, align 8
  %conv73 = trunc i64 %36 to i32
  call void @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE5pbumpEi(%"class.std::__2::basic_streambuf"* %35, i32 %conv73)
  br label %if.end74

if.end74:                                         ; preds = %if.then70, %if.end67
  %37 = load i64, i64* %__noff, align 8
  %call75 = call %"class.std::__2::fpos"* @_ZNSt3__24fposI11__mbstate_tEC2Ex(%"class.std::__2::fpos"* %agg.result, i64 %37)
  br label %return

return:                                           ; preds = %if.end74, %if.then57, %if.then49, %if.then39, %sw.default, %if.then11, %if.then5
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__215basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekposENS_4fposI11__mbstate_tEEj(%"class.std::__2::fpos"* noalias sret align 8 %agg.result, %"class.std::__2::basic_stringbuf"* %this, %"class.std::__2::fpos"* byval(%"class.std::__2::fpos") align 8 %__sp, i32 %__wch) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_stringbuf"*, align 4
  %__wch.addr = alloca i32, align 4
  store %"class.std::__2::basic_stringbuf"* %this, %"class.std::__2::basic_stringbuf"** %this.addr, align 4
  store i32 %__wch, i32* %__wch.addr, align 4
  %this1 = load %"class.std::__2::basic_stringbuf"*, %"class.std::__2::basic_stringbuf"** %this.addr, align 4
  %call = call i64 @_ZNKSt3__24fposI11__mbstate_tEcvxEv(%"class.std::__2::fpos"* %__sp)
  %0 = load i32, i32* %__wch.addr, align 4
  %1 = bitcast %"class.std::__2::basic_stringbuf"* %this1 to void (%"class.std::__2::fpos"*, %"class.std::__2::basic_stringbuf"*, i64, i32, i32)***
  %vtable = load void (%"class.std::__2::fpos"*, %"class.std::__2::basic_stringbuf"*, i64, i32, i32)**, void (%"class.std::__2::fpos"*, %"class.std::__2::basic_stringbuf"*, i64, i32, i32)*** %1, align 4
  %vfn = getelementptr inbounds void (%"class.std::__2::fpos"*, %"class.std::__2::basic_stringbuf"*, i64, i32, i32)*, void (%"class.std::__2::fpos"*, %"class.std::__2::basic_stringbuf"*, i64, i32, i32)** %vtable, i64 4
  %2 = load void (%"class.std::__2::fpos"*, %"class.std::__2::basic_stringbuf"*, i64, i32, i32)*, void (%"class.std::__2::fpos"*, %"class.std::__2::basic_stringbuf"*, i64, i32, i32)** %vfn, align 4
  call void %2(%"class.std::__2::fpos"* sret align 8 %agg.result, %"class.std::__2::basic_stringbuf"* %this1, i64 %call, i32 0, i32 %0)
  ret void
}

declare i32 @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE4syncEv(%"class.std::__2::basic_streambuf"*) unnamed_addr #4

declare i32 @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE9showmanycEv(%"class.std::__2::basic_streambuf"*) unnamed_addr #4

declare i32 @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE6xsgetnEPcl(%"class.std::__2::basic_streambuf"*, i8*, i32) unnamed_addr #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__215basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9underflowEv(%"class.std::__2::basic_stringbuf"* %this) unnamed_addr #1 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %"class.std::__2::basic_stringbuf"*, align 4
  store %"class.std::__2::basic_stringbuf"* %this, %"class.std::__2::basic_stringbuf"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_stringbuf"*, %"class.std::__2::basic_stringbuf"** %this.addr, align 4
  %__hm_ = getelementptr inbounds %"class.std::__2::basic_stringbuf", %"class.std::__2::basic_stringbuf"* %this1, i32 0, i32 2
  %0 = load i8*, i8** %__hm_, align 4
  %1 = bitcast %"class.std::__2::basic_stringbuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE4pptrEv(%"class.std::__2::basic_streambuf"* %1)
  %cmp = icmp ult i8* %0, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = bitcast %"class.std::__2::basic_stringbuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call2 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE4pptrEv(%"class.std::__2::basic_streambuf"* %2)
  %__hm_3 = getelementptr inbounds %"class.std::__2::basic_stringbuf", %"class.std::__2::basic_stringbuf"* %this1, i32 0, i32 2
  store i8* %call2, i8** %__hm_3, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %__mode_ = getelementptr inbounds %"class.std::__2::basic_stringbuf", %"class.std::__2::basic_stringbuf"* %this1, i32 0, i32 3
  %3 = load i32, i32* %__mode_, align 4
  %and = and i32 %3, 8
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then4, label %if.end20

if.then4:                                         ; preds = %if.end
  %4 = bitcast %"class.std::__2::basic_stringbuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call5 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE5egptrEv(%"class.std::__2::basic_streambuf"* %4)
  %__hm_6 = getelementptr inbounds %"class.std::__2::basic_stringbuf", %"class.std::__2::basic_stringbuf"* %this1, i32 0, i32 2
  %5 = load i8*, i8** %__hm_6, align 4
  %cmp7 = icmp ult i8* %call5, %5
  br i1 %cmp7, label %if.then8, label %if.end12

if.then8:                                         ; preds = %if.then4
  %6 = bitcast %"class.std::__2::basic_stringbuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %7 = bitcast %"class.std::__2::basic_stringbuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call9 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE5ebackEv(%"class.std::__2::basic_streambuf"* %7)
  %8 = bitcast %"class.std::__2::basic_stringbuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call10 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE4gptrEv(%"class.std::__2::basic_streambuf"* %8)
  %__hm_11 = getelementptr inbounds %"class.std::__2::basic_stringbuf", %"class.std::__2::basic_stringbuf"* %this1, i32 0, i32 2
  %9 = load i8*, i8** %__hm_11, align 4
  call void @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE4setgEPcS4_S4_(%"class.std::__2::basic_streambuf"* %6, i8* %call9, i8* %call10, i8* %9)
  br label %if.end12

if.end12:                                         ; preds = %if.then8, %if.then4
  %10 = bitcast %"class.std::__2::basic_stringbuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call13 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE4gptrEv(%"class.std::__2::basic_streambuf"* %10)
  %11 = bitcast %"class.std::__2::basic_stringbuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call14 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE5egptrEv(%"class.std::__2::basic_streambuf"* %11)
  %cmp15 = icmp ult i8* %call13, %call14
  br i1 %cmp15, label %if.then16, label %if.end19

if.then16:                                        ; preds = %if.end12
  %12 = bitcast %"class.std::__2::basic_stringbuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call17 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE4gptrEv(%"class.std::__2::basic_streambuf"* %12)
  %13 = load i8, i8* %call17, align 1
  %call18 = call i32 @_ZNSt3__211char_traitsIcE11to_int_typeEc(i8 signext %13) #3
  store i32 %call18, i32* %retval, align 4
  br label %return

if.end19:                                         ; preds = %if.end12
  br label %if.end20

if.end20:                                         ; preds = %if.end19, %if.end
  %call21 = call i32 @_ZNSt3__211char_traitsIcE3eofEv() #3
  store i32 %call21, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end20, %if.then16
  %14 = load i32, i32* %retval, align 4
  ret i32 %14
}

declare i32 @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE5uflowEv(%"class.std::__2::basic_streambuf"*) unnamed_addr #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__215basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9pbackfailEi(%"class.std::__2::basic_stringbuf"* %this, i32 %__c) unnamed_addr #1 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %"class.std::__2::basic_stringbuf"*, align 4
  %__c.addr = alloca i32, align 4
  store %"class.std::__2::basic_stringbuf"* %this, %"class.std::__2::basic_stringbuf"** %this.addr, align 4
  store i32 %__c, i32* %__c.addr, align 4
  %this1 = load %"class.std::__2::basic_stringbuf"*, %"class.std::__2::basic_stringbuf"** %this.addr, align 4
  %__hm_ = getelementptr inbounds %"class.std::__2::basic_stringbuf", %"class.std::__2::basic_stringbuf"* %this1, i32 0, i32 2
  %0 = load i8*, i8** %__hm_, align 4
  %1 = bitcast %"class.std::__2::basic_stringbuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE4pptrEv(%"class.std::__2::basic_streambuf"* %1)
  %cmp = icmp ult i8* %0, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = bitcast %"class.std::__2::basic_stringbuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call2 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE4pptrEv(%"class.std::__2::basic_streambuf"* %2)
  %__hm_3 = getelementptr inbounds %"class.std::__2::basic_stringbuf", %"class.std::__2::basic_stringbuf"* %this1, i32 0, i32 2
  store i8* %call2, i8** %__hm_3, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %3 = bitcast %"class.std::__2::basic_stringbuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call4 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE5ebackEv(%"class.std::__2::basic_streambuf"* %3)
  %4 = bitcast %"class.std::__2::basic_stringbuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call5 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE4gptrEv(%"class.std::__2::basic_streambuf"* %4)
  %cmp6 = icmp ult i8* %call4, %call5
  br i1 %cmp6, label %if.then7, label %if.end27

if.then7:                                         ; preds = %if.end
  %5 = load i32, i32* %__c.addr, align 4
  %call8 = call i32 @_ZNSt3__211char_traitsIcE3eofEv() #3
  %call9 = call zeroext i1 @_ZNSt3__211char_traitsIcE11eq_int_typeEii(i32 %5, i32 %call8) #3
  br i1 %call9, label %if.then10, label %if.end15

if.then10:                                        ; preds = %if.then7
  %6 = bitcast %"class.std::__2::basic_stringbuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %7 = bitcast %"class.std::__2::basic_stringbuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call11 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE5ebackEv(%"class.std::__2::basic_streambuf"* %7)
  %8 = bitcast %"class.std::__2::basic_stringbuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call12 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE4gptrEv(%"class.std::__2::basic_streambuf"* %8)
  %add.ptr = getelementptr inbounds i8, i8* %call12, i32 -1
  %__hm_13 = getelementptr inbounds %"class.std::__2::basic_stringbuf", %"class.std::__2::basic_stringbuf"* %this1, i32 0, i32 2
  %9 = load i8*, i8** %__hm_13, align 4
  call void @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE4setgEPcS4_S4_(%"class.std::__2::basic_streambuf"* %6, i8* %call11, i8* %add.ptr, i8* %9)
  %10 = load i32, i32* %__c.addr, align 4
  %call14 = call i32 @_ZNSt3__211char_traitsIcE7not_eofEi(i32 %10) #3
  store i32 %call14, i32* %retval, align 4
  br label %return

if.end15:                                         ; preds = %if.then7
  %__mode_ = getelementptr inbounds %"class.std::__2::basic_stringbuf", %"class.std::__2::basic_stringbuf"* %this1, i32 0, i32 3
  %11 = load i32, i32* %__mode_, align 4
  %and = and i32 %11, 16
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then19, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end15
  %12 = load i32, i32* %__c.addr, align 4
  %call16 = call signext i8 @_ZNSt3__211char_traitsIcE12to_char_typeEi(i32 %12) #3
  %13 = bitcast %"class.std::__2::basic_stringbuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call17 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE4gptrEv(%"class.std::__2::basic_streambuf"* %13)
  %arrayidx = getelementptr inbounds i8, i8* %call17, i32 -1
  %14 = load i8, i8* %arrayidx, align 1
  %call18 = call zeroext i1 @_ZNSt3__211char_traitsIcE2eqEcc(i8 signext %call16, i8 signext %14) #3
  br i1 %call18, label %if.then19, label %if.end26

if.then19:                                        ; preds = %lor.lhs.false, %if.end15
  %15 = bitcast %"class.std::__2::basic_stringbuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %16 = bitcast %"class.std::__2::basic_stringbuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call20 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE5ebackEv(%"class.std::__2::basic_streambuf"* %16)
  %17 = bitcast %"class.std::__2::basic_stringbuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call21 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE4gptrEv(%"class.std::__2::basic_streambuf"* %17)
  %add.ptr22 = getelementptr inbounds i8, i8* %call21, i32 -1
  %__hm_23 = getelementptr inbounds %"class.std::__2::basic_stringbuf", %"class.std::__2::basic_stringbuf"* %this1, i32 0, i32 2
  %18 = load i8*, i8** %__hm_23, align 4
  call void @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE4setgEPcS4_S4_(%"class.std::__2::basic_streambuf"* %15, i8* %call20, i8* %add.ptr22, i8* %18)
  %19 = load i32, i32* %__c.addr, align 4
  %call24 = call signext i8 @_ZNSt3__211char_traitsIcE12to_char_typeEi(i32 %19) #3
  %20 = bitcast %"class.std::__2::basic_stringbuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call25 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE4gptrEv(%"class.std::__2::basic_streambuf"* %20)
  store i8 %call24, i8* %call25, align 1
  %21 = load i32, i32* %__c.addr, align 4
  store i32 %21, i32* %retval, align 4
  br label %return

if.end26:                                         ; preds = %lor.lhs.false
  br label %if.end27

if.end27:                                         ; preds = %if.end26, %if.end
  %call28 = call i32 @_ZNSt3__211char_traitsIcE3eofEv() #3
  store i32 %call28, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end27, %if.then19, %if.then10
  %22 = load i32, i32* %retval, align 4
  ret i32 %22
}

declare i32 @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE6xsputnEPKcl(%"class.std::__2::basic_streambuf"*, i8*, i32) unnamed_addr #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__215basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE8overflowEi(%"class.std::__2::basic_stringbuf"* %this, i32 %__c) unnamed_addr #1 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %"class.std::__2::basic_stringbuf"*, align 4
  %__c.addr = alloca i32, align 4
  %__ninp = alloca i32, align 4
  %__nout = alloca i32, align 4
  %__hm = alloca i32, align 4
  %__p = alloca i8*, align 4
  %ref.tmp = alloca i8*, align 4
  %__p39 = alloca i8*, align 4
  store %"class.std::__2::basic_stringbuf"* %this, %"class.std::__2::basic_stringbuf"** %this.addr, align 4
  store i32 %__c, i32* %__c.addr, align 4
  %this1 = load %"class.std::__2::basic_stringbuf"*, %"class.std::__2::basic_stringbuf"** %this.addr, align 4
  %0 = load i32, i32* %__c.addr, align 4
  %call = call i32 @_ZNSt3__211char_traitsIcE3eofEv() #3
  %call2 = call zeroext i1 @_ZNSt3__211char_traitsIcE11eq_int_typeEii(i32 %0, i32 %call) #3
  br i1 %call2, label %if.end47, label %if.then

if.then:                                          ; preds = %entry
  %1 = bitcast %"class.std::__2::basic_stringbuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call3 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE4gptrEv(%"class.std::__2::basic_streambuf"* %1)
  %2 = bitcast %"class.std::__2::basic_stringbuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call4 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE5ebackEv(%"class.std::__2::basic_streambuf"* %2)
  %sub.ptr.lhs.cast = ptrtoint i8* %call3 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %call4 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  store i32 %sub.ptr.sub, i32* %__ninp, align 4
  %3 = bitcast %"class.std::__2::basic_stringbuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call5 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE4pptrEv(%"class.std::__2::basic_streambuf"* %3)
  %4 = bitcast %"class.std::__2::basic_stringbuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call6 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE5epptrEv(%"class.std::__2::basic_streambuf"* %4)
  %cmp = icmp eq i8* %call5, %call6
  br i1 %cmp, label %if.then7, label %if.end29

if.then7:                                         ; preds = %if.then
  %__mode_ = getelementptr inbounds %"class.std::__2::basic_stringbuf", %"class.std::__2::basic_stringbuf"* %this1, i32 0, i32 3
  %5 = load i32, i32* %__mode_, align 4
  %and = and i32 %5, 16
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.end, label %if.then8

if.then8:                                         ; preds = %if.then7
  %call9 = call i32 @_ZNSt3__211char_traitsIcE3eofEv() #3
  store i32 %call9, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %if.then7
  %6 = bitcast %"class.std::__2::basic_stringbuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call10 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE4pptrEv(%"class.std::__2::basic_streambuf"* %6)
  %7 = bitcast %"class.std::__2::basic_stringbuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call11 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE5pbaseEv(%"class.std::__2::basic_streambuf"* %7)
  %sub.ptr.lhs.cast12 = ptrtoint i8* %call10 to i32
  %sub.ptr.rhs.cast13 = ptrtoint i8* %call11 to i32
  %sub.ptr.sub14 = sub i32 %sub.ptr.lhs.cast12, %sub.ptr.rhs.cast13
  store i32 %sub.ptr.sub14, i32* %__nout, align 4
  %__hm_ = getelementptr inbounds %"class.std::__2::basic_stringbuf", %"class.std::__2::basic_stringbuf"* %this1, i32 0, i32 2
  %8 = load i8*, i8** %__hm_, align 4
  %9 = bitcast %"class.std::__2::basic_stringbuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call15 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE5pbaseEv(%"class.std::__2::basic_streambuf"* %9)
  %sub.ptr.lhs.cast16 = ptrtoint i8* %8 to i32
  %sub.ptr.rhs.cast17 = ptrtoint i8* %call15 to i32
  %sub.ptr.sub18 = sub i32 %sub.ptr.lhs.cast16, %sub.ptr.rhs.cast17
  store i32 %sub.ptr.sub18, i32* %__hm, align 4
  %__str_ = getelementptr inbounds %"class.std::__2::basic_stringbuf", %"class.std::__2::basic_stringbuf"* %this1, i32 0, i32 1
  call void @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE9push_backEc(%"class.std::__2::basic_string"* %__str_, i8 signext 0)
  %__str_19 = getelementptr inbounds %"class.std::__2::basic_stringbuf", %"class.std::__2::basic_stringbuf"* %this1, i32 0, i32 1
  %__str_20 = getelementptr inbounds %"class.std::__2::basic_stringbuf", %"class.std::__2::basic_stringbuf"* %this1, i32 0, i32 1
  %call21 = call i32 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE8capacityEv(%"class.std::__2::basic_string"* %__str_20) #3
  call void @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6resizeEm(%"class.std::__2::basic_string"* %__str_19, i32 %call21)
  %__str_22 = getelementptr inbounds %"class.std::__2::basic_stringbuf", %"class.std::__2::basic_stringbuf"* %this1, i32 0, i32 1
  %call23 = call i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv(%"class.std::__2::basic_string"* %__str_22) #3
  store i8* %call23, i8** %__p, align 4
  %10 = bitcast %"class.std::__2::basic_stringbuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %11 = load i8*, i8** %__p, align 4
  %12 = load i8*, i8** %__p, align 4
  %__str_24 = getelementptr inbounds %"class.std::__2::basic_stringbuf", %"class.std::__2::basic_stringbuf"* %this1, i32 0, i32 1
  %call25 = call i32 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4sizeEv(%"class.std::__2::basic_string"* %__str_24) #3
  %add.ptr = getelementptr inbounds i8, i8* %12, i32 %call25
  call void @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE4setpEPcS4_(%"class.std::__2::basic_streambuf"* %10, i8* %11, i8* %add.ptr)
  %13 = bitcast %"class.std::__2::basic_stringbuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %14 = load i32, i32* %__nout, align 4
  call void @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE7__pbumpEl(%"class.std::__2::basic_streambuf"* %13, i32 %14)
  %15 = bitcast %"class.std::__2::basic_stringbuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call26 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE5pbaseEv(%"class.std::__2::basic_streambuf"* %15)
  %16 = load i32, i32* %__hm, align 4
  %add.ptr27 = getelementptr inbounds i8, i8* %call26, i32 %16
  %__hm_28 = getelementptr inbounds %"class.std::__2::basic_stringbuf", %"class.std::__2::basic_stringbuf"* %this1, i32 0, i32 2
  store i8* %add.ptr27, i8** %__hm_28, align 4
  br label %if.end29

if.end29:                                         ; preds = %if.end, %if.then
  %17 = bitcast %"class.std::__2::basic_stringbuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call30 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE4pptrEv(%"class.std::__2::basic_streambuf"* %17)
  %add.ptr31 = getelementptr inbounds i8, i8* %call30, i32 1
  store i8* %add.ptr31, i8** %ref.tmp, align 4
  %__hm_32 = getelementptr inbounds %"class.std::__2::basic_stringbuf", %"class.std::__2::basic_stringbuf"* %this1, i32 0, i32 2
  %call33 = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__23maxIPcEERKT_S4_S4_(i8** nonnull align 4 dereferenceable(4) %ref.tmp, i8** nonnull align 4 dereferenceable(4) %__hm_32)
  %18 = load i8*, i8** %call33, align 4
  %__hm_34 = getelementptr inbounds %"class.std::__2::basic_stringbuf", %"class.std::__2::basic_stringbuf"* %this1, i32 0, i32 2
  store i8* %18, i8** %__hm_34, align 4
  %__mode_35 = getelementptr inbounds %"class.std::__2::basic_stringbuf", %"class.std::__2::basic_stringbuf"* %this1, i32 0, i32 3
  %19 = load i32, i32* %__mode_35, align 4
  %and36 = and i32 %19, 8
  %tobool37 = icmp ne i32 %and36, 0
  br i1 %tobool37, label %if.then38, label %if.end44

if.then38:                                        ; preds = %if.end29
  %__str_40 = getelementptr inbounds %"class.std::__2::basic_stringbuf", %"class.std::__2::basic_stringbuf"* %this1, i32 0, i32 1
  %call41 = call i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv(%"class.std::__2::basic_string"* %__str_40) #3
  store i8* %call41, i8** %__p39, align 4
  %20 = bitcast %"class.std::__2::basic_stringbuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %21 = load i8*, i8** %__p39, align 4
  %22 = load i8*, i8** %__p39, align 4
  %23 = load i32, i32* %__ninp, align 4
  %add.ptr42 = getelementptr inbounds i8, i8* %22, i32 %23
  %__hm_43 = getelementptr inbounds %"class.std::__2::basic_stringbuf", %"class.std::__2::basic_stringbuf"* %this1, i32 0, i32 2
  %24 = load i8*, i8** %__hm_43, align 4
  call void @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE4setgEPcS4_S4_(%"class.std::__2::basic_streambuf"* %20, i8* %21, i8* %add.ptr42, i8* %24)
  br label %if.end44

if.end44:                                         ; preds = %if.then38, %if.end29
  %25 = bitcast %"class.std::__2::basic_stringbuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %26 = load i32, i32* %__c.addr, align 4
  %call45 = call signext i8 @_ZNSt3__211char_traitsIcE12to_char_typeEi(i32 %26) #3
  %call46 = call i32 @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE5sputcEc(%"class.std::__2::basic_streambuf"* %25, i8 signext %call45)
  store i32 %call46, i32* %retval, align 4
  br label %return

if.end47:                                         ; preds = %entry
  %27 = load i32, i32* %__c.addr, align 4
  %call48 = call i32 @_ZNSt3__211char_traitsIcE7not_eofEi(i32 %27) #3
  store i32 %call48, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end47, %if.end44, %if.then8
  %28 = load i32, i32* %retval, align 4
  ret i32 %28
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #5

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE4pptrEv(%"class.std::__2::basic_streambuf"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_streambuf"*, align 4
  store %"class.std::__2::basic_streambuf"* %this, %"class.std::__2::basic_streambuf"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_streambuf"*, %"class.std::__2::basic_streambuf"** %this.addr, align 4
  %__nout_ = getelementptr inbounds %"class.std::__2::basic_streambuf", %"class.std::__2::basic_streambuf"* %this1, i32 0, i32 6
  %0 = load i8*, i8** %__nout_, align 4
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::fpos"* @_ZNSt3__24fposI11__mbstate_tEC2Ex(%"class.std::__2::fpos"* returned %this, i64 %__off) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::fpos"*, align 4
  %__off.addr = alloca i64, align 8
  store %"class.std::__2::fpos"* %this, %"class.std::__2::fpos"** %this.addr, align 4
  store i64 %__off, i64* %__off.addr, align 8
  %this1 = load %"class.std::__2::fpos"*, %"class.std::__2::fpos"** %this.addr, align 4
  %__st_ = getelementptr inbounds %"class.std::__2::fpos", %"class.std::__2::fpos"* %this1, i32 0, i32 0
  %0 = bitcast %struct.__mbstate_t* %__st_ to i8*
  call void @llvm.memset.p0i8.i32(i8* align 8 %0, i8 0, i32 8, i1 false)
  %__off_ = getelementptr inbounds %"class.std::__2::fpos", %"class.std::__2::fpos"* %this1, i32 0, i32 1
  %1 = load i64, i64* %__off.addr, align 8
  store i64 %1, i64* %__off_, align 8
  ret %"class.std::__2::fpos"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv(%"class.std::__2::basic_string"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %call = call i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE13__get_pointerEv(%"class.std::__2::basic_string"* %this1) #3
  %call2 = call i8* @_ZNSt3__212__to_addressIKcEEPT_S3_(i8* %call) #3
  ret i8* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE4gptrEv(%"class.std::__2::basic_streambuf"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_streambuf"*, align 4
  store %"class.std::__2::basic_streambuf"* %this, %"class.std::__2::basic_streambuf"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_streambuf"*, %"class.std::__2::basic_streambuf"** %this.addr, align 4
  %__ninp_ = getelementptr inbounds %"class.std::__2::basic_streambuf", %"class.std::__2::basic_streambuf"* %this1, i32 0, i32 3
  %0 = load i8*, i8** %__ninp_, align 4
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE5ebackEv(%"class.std::__2::basic_streambuf"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_streambuf"*, align 4
  store %"class.std::__2::basic_streambuf"* %this, %"class.std::__2::basic_streambuf"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_streambuf"*, %"class.std::__2::basic_streambuf"** %this.addr, align 4
  %__binp_ = getelementptr inbounds %"class.std::__2::basic_streambuf", %"class.std::__2::basic_streambuf"* %this1, i32 0, i32 2
  %0 = load i8*, i8** %__binp_, align 4
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE5pbaseEv(%"class.std::__2::basic_streambuf"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_streambuf"*, align 4
  store %"class.std::__2::basic_streambuf"* %this, %"class.std::__2::basic_streambuf"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_streambuf"*, %"class.std::__2::basic_streambuf"** %this.addr, align 4
  %__bout_ = getelementptr inbounds %"class.std::__2::basic_streambuf", %"class.std::__2::basic_streambuf"* %this1, i32 0, i32 5
  %0 = load i8*, i8** %__bout_, align 4
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE4setgEPcS4_S4_(%"class.std::__2::basic_streambuf"* %this, i8* %__gbeg, i8* %__gnext, i8* %__gend) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_streambuf"*, align 4
  %__gbeg.addr = alloca i8*, align 4
  %__gnext.addr = alloca i8*, align 4
  %__gend.addr = alloca i8*, align 4
  store %"class.std::__2::basic_streambuf"* %this, %"class.std::__2::basic_streambuf"** %this.addr, align 4
  store i8* %__gbeg, i8** %__gbeg.addr, align 4
  store i8* %__gnext, i8** %__gnext.addr, align 4
  store i8* %__gend, i8** %__gend.addr, align 4
  %this1 = load %"class.std::__2::basic_streambuf"*, %"class.std::__2::basic_streambuf"** %this.addr, align 4
  %0 = load i8*, i8** %__gbeg.addr, align 4
  %__binp_ = getelementptr inbounds %"class.std::__2::basic_streambuf", %"class.std::__2::basic_streambuf"* %this1, i32 0, i32 2
  store i8* %0, i8** %__binp_, align 4
  %1 = load i8*, i8** %__gnext.addr, align 4
  %__ninp_ = getelementptr inbounds %"class.std::__2::basic_streambuf", %"class.std::__2::basic_streambuf"* %this1, i32 0, i32 3
  store i8* %1, i8** %__ninp_, align 4
  %2 = load i8*, i8** %__gend.addr, align 4
  %__einp_ = getelementptr inbounds %"class.std::__2::basic_streambuf", %"class.std::__2::basic_streambuf"* %this1, i32 0, i32 4
  store i8* %2, i8** %__einp_, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE4setpEPcS4_(%"class.std::__2::basic_streambuf"* %this, i8* %__pbeg, i8* %__pend) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_streambuf"*, align 4
  %__pbeg.addr = alloca i8*, align 4
  %__pend.addr = alloca i8*, align 4
  store %"class.std::__2::basic_streambuf"* %this, %"class.std::__2::basic_streambuf"** %this.addr, align 4
  store i8* %__pbeg, i8** %__pbeg.addr, align 4
  store i8* %__pend, i8** %__pend.addr, align 4
  %this1 = load %"class.std::__2::basic_streambuf"*, %"class.std::__2::basic_streambuf"** %this.addr, align 4
  %0 = load i8*, i8** %__pbeg.addr, align 4
  %__nout_ = getelementptr inbounds %"class.std::__2::basic_streambuf", %"class.std::__2::basic_streambuf"* %this1, i32 0, i32 6
  store i8* %0, i8** %__nout_, align 4
  %__bout_ = getelementptr inbounds %"class.std::__2::basic_streambuf", %"class.std::__2::basic_streambuf"* %this1, i32 0, i32 5
  store i8* %0, i8** %__bout_, align 4
  %1 = load i8*, i8** %__pend.addr, align 4
  %__eout_ = getelementptr inbounds %"class.std::__2::basic_streambuf", %"class.std::__2::basic_streambuf"* %this1, i32 0, i32 7
  store i8* %1, i8** %__eout_, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE5epptrEv(%"class.std::__2::basic_streambuf"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_streambuf"*, align 4
  store %"class.std::__2::basic_streambuf"* %this, %"class.std::__2::basic_streambuf"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_streambuf"*, %"class.std::__2::basic_streambuf"** %this.addr, align 4
  %__eout_ = getelementptr inbounds %"class.std::__2::basic_streambuf", %"class.std::__2::basic_streambuf"* %this1, i32 0, i32 7
  %0 = load i8*, i8** %__eout_, align 4
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE5pbumpEi(%"class.std::__2::basic_streambuf"* %this, i32 %__n) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_streambuf"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::basic_streambuf"* %this, %"class.std::__2::basic_streambuf"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::basic_streambuf"*, %"class.std::__2::basic_streambuf"** %this.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %__nout_ = getelementptr inbounds %"class.std::__2::basic_streambuf", %"class.std::__2::basic_streambuf"* %this1, i32 0, i32 6
  %1 = load i8*, i8** %__nout_, align 4
  %add.ptr = getelementptr inbounds i8, i8* %1, i32 %0
  store i8* %add.ptr, i8** %__nout_, align 4
  ret void
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #6

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__212__to_addressIKcEEPT_S3_(i8* %__p) #1 comdat {
entry:
  %__p.addr = alloca i8*, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %0 = load i8*, i8** %__p.addr, align 4
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE13__get_pointerEv(%"class.std::__2::basic_string"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %call = call zeroext i1 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE9__is_longEv(%"class.std::__2::basic_string"* %this1) #3
  br i1 %call, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %call2 = call i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE18__get_long_pointerEv(%"class.std::__2::basic_string"* %this1) #3
  br label %cond.end

cond.false:                                       ; preds = %entry
  %call3 = call i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE19__get_short_pointerEv(%"class.std::__2::basic_string"* %this1) #3
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i8* [ %call2, %cond.true ], [ %call3, %cond.false ]
  ret i8* %cond
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE9__is_longEv(%"class.std::__2::basic_string"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair"* %__r_) #3
  %0 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call, i32 0, i32 0
  %__s = bitcast %union.anon* %0 to %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short"*
  %1 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short"* %__s, i32 0, i32 1
  %__size_ = getelementptr inbounds %struct.anon, %struct.anon* %1, i32 0, i32 0
  %2 = load i8, i8* %__size_, align 1
  %conv = zext i8 %2 to i32
  %and = and i32 %conv, 128
  %tobool = icmp ne i32 %and, 0
  ret i1 %tobool
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE18__get_long_pointerEv(%"class.std::__2::basic_string"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair"* %__r_) #3
  %0 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call, i32 0, i32 0
  %__l = bitcast %union.anon* %0 to %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long"*
  %__data_ = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long"* %__l, i32 0, i32 0
  %1 = load i8*, i8** %__data_, align 4
  ret i8* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE19__get_short_pointerEv(%"class.std::__2::basic_string"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair"* %__r_) #3
  %0 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call, i32 0, i32 0
  %__s = bitcast %union.anon* %0 to %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short"*
  %__data_ = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short"* %__s, i32 0, i32 0
  %arrayidx = getelementptr inbounds [11 x i8], [11 x i8]* %__data_, i32 0, i32 0
  %call2 = call i8* @_ZNSt3__214pointer_traitsIPKcE10pointer_toERS1_(i8* nonnull align 1 dereferenceable(1) %arrayidx) #3
  ret i8* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %0) #3
  ret %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %this) #1 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem"*, align 4
  store %"struct.std::__2::__compressed_pair_elem"* %this, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem"*, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem"* %this1, i32 0, i32 0
  ret %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__214pointer_traitsIPKcE10pointer_toERS1_(i8* nonnull align 1 dereferenceable(1) %__r) #1 comdat {
entry:
  %__r.addr = alloca i8*, align 4
  store i8* %__r, i8** %__r.addr, align 4
  %0 = load i8*, i8** %__r.addr, align 4
  %call = call i8* @_ZNSt3__29addressofIKcEEPT_RS2_(i8* nonnull align 1 dereferenceable(1) %0) #3
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__29addressofIKcEEPT_RS2_(i8* nonnull align 1 dereferenceable(1) %__x) #1 comdat {
entry:
  %__x.addr = alloca i8*, align 4
  store i8* %__x, i8** %__x.addr, align 4
  %0 = load i8*, i8** %__x.addr, align 4
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i64 @_ZNKSt3__24fposI11__mbstate_tEcvxEv(%"class.std::__2::fpos"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::fpos"*, align 4
  store %"class.std::__2::fpos"* %this, %"class.std::__2::fpos"** %this.addr, align 4
  %this1 = load %"class.std::__2::fpos"*, %"class.std::__2::fpos"** %this.addr, align 4
  %__off_ = getelementptr inbounds %"class.std::__2::fpos", %"class.std::__2::fpos"* %this1, i32 0, i32 1
  %0 = load i64, i64* %__off_, align 8
  ret i64 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE5egptrEv(%"class.std::__2::basic_streambuf"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_streambuf"*, align 4
  store %"class.std::__2::basic_streambuf"* %this, %"class.std::__2::basic_streambuf"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_streambuf"*, %"class.std::__2::basic_streambuf"** %this.addr, align 4
  %__einp_ = getelementptr inbounds %"class.std::__2::basic_streambuf", %"class.std::__2::basic_streambuf"* %this1, i32 0, i32 4
  %0 = load i8*, i8** %__einp_, align 4
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__211char_traitsIcE11to_int_typeEc(i8 signext %__c) #1 comdat {
entry:
  %__c.addr = alloca i8, align 1
  store i8 %__c, i8* %__c.addr, align 1
  %0 = load i8, i8* %__c.addr, align 1
  %conv = zext i8 %0 to i32
  ret i32 %conv
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__211char_traitsIcE3eofEv() #1 comdat {
entry:
  ret i32 -1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNSt3__211char_traitsIcE11eq_int_typeEii(i32 %__c1, i32 %__c2) #1 comdat {
entry:
  %__c1.addr = alloca i32, align 4
  %__c2.addr = alloca i32, align 4
  store i32 %__c1, i32* %__c1.addr, align 4
  store i32 %__c2, i32* %__c2.addr, align 4
  %0 = load i32, i32* %__c1.addr, align 4
  %1 = load i32, i32* %__c2.addr, align 4
  %cmp = icmp eq i32 %0, %1
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__211char_traitsIcE7not_eofEi(i32 %__c) #1 comdat {
entry:
  %__c.addr = alloca i32, align 4
  store i32 %__c, i32* %__c.addr, align 4
  %0 = load i32, i32* %__c.addr, align 4
  %call = call i32 @_ZNSt3__211char_traitsIcE3eofEv() #3
  %call1 = call zeroext i1 @_ZNSt3__211char_traitsIcE11eq_int_typeEii(i32 %0, i32 %call) #3
  br i1 %call1, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %call2 = call i32 @_ZNSt3__211char_traitsIcE3eofEv() #3
  %neg = xor i32 %call2, -1
  br label %cond.end

cond.false:                                       ; preds = %entry
  %1 = load i32, i32* %__c.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %neg, %cond.true ], [ %1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNSt3__211char_traitsIcE2eqEcc(i8 signext %__c1, i8 signext %__c2) #1 comdat {
entry:
  %__c1.addr = alloca i8, align 1
  %__c2.addr = alloca i8, align 1
  store i8 %__c1, i8* %__c1.addr, align 1
  store i8 %__c2, i8* %__c2.addr, align 1
  %0 = load i8, i8* %__c1.addr, align 1
  %conv = sext i8 %0 to i32
  %1 = load i8, i8* %__c2.addr, align 1
  %conv1 = sext i8 %1 to i32
  %cmp = icmp eq i32 %conv, %conv1
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden signext i8 @_ZNSt3__211char_traitsIcE12to_char_typeEi(i32 %__c) #1 comdat {
entry:
  %__c.addr = alloca i32, align 4
  store i32 %__c, i32* %__c.addr, align 4
  %0 = load i32, i32* %__c.addr, align 4
  %conv = trunc i32 %0 to i8
  ret i8 %conv
}

declare void @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE9push_backEc(%"class.std::__2::basic_string"*, i8 signext) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6resizeEm(%"class.std::__2::basic_string"* %this, i32 %__n) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6resizeEmc(%"class.std::__2::basic_string"* %this1, i32 %0, i8 signext 0)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE8capacityEv(%"class.std::__2::basic_string"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %call = call zeroext i1 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE9__is_longEv(%"class.std::__2::basic_string"* %this1) #3
  br i1 %call, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %call2 = call i32 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE14__get_long_capEv(%"class.std::__2::basic_string"* %this1) #3
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %call2, %cond.true ], [ 11, %cond.false ]
  %sub = sub i32 %cond, 1
  ret i32 %sub
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4sizeEv(%"class.std::__2::basic_string"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %call = call zeroext i1 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE9__is_longEv(%"class.std::__2::basic_string"* %this1) #3
  br i1 %call, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %call2 = call i32 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE15__get_long_sizeEv(%"class.std::__2::basic_string"* %this1) #3
  br label %cond.end

cond.false:                                       ; preds = %entry
  %call3 = call i32 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE16__get_short_sizeEv(%"class.std::__2::basic_string"* %this1) #3
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %call2, %cond.true ], [ %call3, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE7__pbumpEl(%"class.std::__2::basic_streambuf"* %this, i32 %__n) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_streambuf"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::basic_streambuf"* %this, %"class.std::__2::basic_streambuf"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::basic_streambuf"*, %"class.std::__2::basic_streambuf"** %this.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %__nout_ = getelementptr inbounds %"class.std::__2::basic_streambuf", %"class.std::__2::basic_streambuf"* %this1, i32 0, i32 6
  %1 = load i8*, i8** %__nout_, align 4
  %add.ptr = getelementptr inbounds i8, i8* %1, i32 %0
  store i8* %add.ptr, i8** %__nout_, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNSt3__23maxIPcEERKT_S4_S4_(i8** nonnull align 4 dereferenceable(4) %__a, i8** nonnull align 4 dereferenceable(4) %__b) #1 comdat {
entry:
  %__a.addr = alloca i8**, align 4
  %__b.addr = alloca i8**, align 4
  %agg.tmp = alloca %"struct.std::__2::__less", align 1
  store i8** %__a, i8*** %__a.addr, align 4
  store i8** %__b, i8*** %__b.addr, align 4
  %0 = load i8**, i8*** %__a.addr, align 4
  %1 = load i8**, i8*** %__b.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__23maxIPcNS_6__lessIS1_S1_EEEERKT_S6_S6_T0_(i8** nonnull align 4 dereferenceable(4) %0, i8** nonnull align 4 dereferenceable(4) %1)
  ret i8** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE5sputcEc(%"class.std::__2::basic_streambuf"* %this, i8 signext %__c) #1 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %"class.std::__2::basic_streambuf"*, align 4
  %__c.addr = alloca i8, align 1
  store %"class.std::__2::basic_streambuf"* %this, %"class.std::__2::basic_streambuf"** %this.addr, align 4
  store i8 %__c, i8* %__c.addr, align 1
  %this1 = load %"class.std::__2::basic_streambuf"*, %"class.std::__2::basic_streambuf"** %this.addr, align 4
  %__nout_ = getelementptr inbounds %"class.std::__2::basic_streambuf", %"class.std::__2::basic_streambuf"* %this1, i32 0, i32 6
  %0 = load i8*, i8** %__nout_, align 4
  %__eout_ = getelementptr inbounds %"class.std::__2::basic_streambuf", %"class.std::__2::basic_streambuf"* %this1, i32 0, i32 7
  %1 = load i8*, i8** %__eout_, align 4
  %cmp = icmp eq i8* %0, %1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load i8, i8* %__c.addr, align 1
  %call = call i32 @_ZNSt3__211char_traitsIcE11to_int_typeEc(i8 signext %2) #3
  %3 = bitcast %"class.std::__2::basic_streambuf"* %this1 to i32 (%"class.std::__2::basic_streambuf"*, i32)***
  %vtable = load i32 (%"class.std::__2::basic_streambuf"*, i32)**, i32 (%"class.std::__2::basic_streambuf"*, i32)*** %3, align 4
  %vfn = getelementptr inbounds i32 (%"class.std::__2::basic_streambuf"*, i32)*, i32 (%"class.std::__2::basic_streambuf"*, i32)** %vtable, i64 13
  %4 = load i32 (%"class.std::__2::basic_streambuf"*, i32)*, i32 (%"class.std::__2::basic_streambuf"*, i32)** %vfn, align 4
  %call2 = call i32 %4(%"class.std::__2::basic_streambuf"* %this1, i32 %call)
  store i32 %call2, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %5 = load i8, i8* %__c.addr, align 1
  %__nout_3 = getelementptr inbounds %"class.std::__2::basic_streambuf", %"class.std::__2::basic_streambuf"* %this1, i32 0, i32 6
  %6 = load i8*, i8** %__nout_3, align 4
  %incdec.ptr = getelementptr inbounds i8, i8* %6, i32 1
  store i8* %incdec.ptr, i8** %__nout_3, align 4
  store i8 %5, i8* %6, align 1
  %7 = load i8, i8* %__c.addr, align 1
  %call4 = call i32 @_ZNSt3__211char_traitsIcE11to_int_typeEc(i8 signext %7) #3
  store i32 %call4, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %8 = load i32, i32* %retval, align 4
  ret i32 %8
}

declare void @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6resizeEmc(%"class.std::__2::basic_string"*, i32, i8 signext) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE14__get_long_capEv(%"class.std::__2::basic_string"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair"* %__r_) #3
  %0 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call, i32 0, i32 0
  %__l = bitcast %union.anon* %0 to %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long"*
  %__cap_ = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long"* %__l, i32 0, i32 2
  %1 = load i32, i32* %__cap_, align 4
  %and = and i32 %1, 2147483647
  ret i32 %and
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE15__get_long_sizeEv(%"class.std::__2::basic_string"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair"* %__r_) #3
  %0 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call, i32 0, i32 0
  %__l = bitcast %union.anon* %0 to %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long"*
  %__size_ = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long"* %__l, i32 0, i32 1
  %1 = load i32, i32* %__size_, align 4
  ret i32 %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE16__get_short_sizeEv(%"class.std::__2::basic_string"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair"* %__r_) #3
  %0 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call, i32 0, i32 0
  %__s = bitcast %union.anon* %0 to %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short"*
  %1 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short"* %__s, i32 0, i32 1
  %__size_ = getelementptr inbounds %struct.anon, %struct.anon* %1, i32 0, i32 0
  %2 = load i8, i8* %__size_, align 1
  %conv = zext i8 %2 to i32
  ret i32 %conv
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNSt3__23maxIPcNS_6__lessIS1_S1_EEEERKT_S6_S6_T0_(i8** nonnull align 4 dereferenceable(4) %__a, i8** nonnull align 4 dereferenceable(4) %__b) #1 comdat {
entry:
  %__comp = alloca %"struct.std::__2::__less", align 1
  %__a.addr = alloca i8**, align 4
  %__b.addr = alloca i8**, align 4
  store i8** %__a, i8*** %__a.addr, align 4
  store i8** %__b, i8*** %__b.addr, align 4
  %0 = load i8**, i8*** %__a.addr, align 4
  %1 = load i8**, i8*** %__b.addr, align 4
  %call = call zeroext i1 @_ZNKSt3__26__lessIPcS1_EclERKS1_S4_(%"struct.std::__2::__less"* %__comp, i8** nonnull align 4 dereferenceable(4) %0, i8** nonnull align 4 dereferenceable(4) %1)
  br i1 %call, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i8**, i8*** %__b.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %3 = load i8**, i8*** %__a.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi i8** [ %2, %cond.true ], [ %3, %cond.false ]
  ret i8** %cond-lvalue
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__26__lessIPcS1_EclERKS1_S4_(%"struct.std::__2::__less"* %this, i8** nonnull align 4 dereferenceable(4) %__x, i8** nonnull align 4 dereferenceable(4) %__y) #1 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__less"*, align 4
  %__x.addr = alloca i8**, align 4
  %__y.addr = alloca i8**, align 4
  store %"struct.std::__2::__less"* %this, %"struct.std::__2::__less"** %this.addr, align 4
  store i8** %__x, i8*** %__x.addr, align 4
  store i8** %__y, i8*** %__y.addr, align 4
  %this1 = load %"struct.std::__2::__less"*, %"struct.std::__2::__less"** %this.addr, align 4
  %0 = load i8**, i8*** %__x.addr, align 4
  %1 = load i8*, i8** %0, align 4
  %2 = load i8**, i8*** %__y.addr, align 4
  %3 = load i8*, i8** %2, align 4
  %cmp = icmp ult i8* %1, %3
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define internal %"class.(anonymous namespace)::Logger"* @_ZN12_GLOBAL__N_16LoggerC2Ev(%"class.(anonymous namespace)::Logger"* returned %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %"class.(anonymous namespace)::Logger"*, align 4
  store %"class.(anonymous namespace)::Logger"* %this, %"class.(anonymous namespace)::Logger"** %this.addr, align 4
  %this1 = load %"class.(anonymous namespace)::Logger"*, %"class.(anonymous namespace)::Logger"** %this.addr, align 4
  %file = getelementptr inbounds %"class.(anonymous namespace)::Logger", %"class.(anonymous namespace)::Logger"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::basic_ofstream"* @_ZNSt3__214basic_ofstreamIcNS_11char_traitsIcEEEC1Ev(%"class.std::__2::basic_ofstream"* %file)
  %in = getelementptr inbounds %"class.(anonymous namespace)::Logger", %"class.(anonymous namespace)::Logger"* %this1, i32 0, i32 1
  %vtable = load i8*, i8** bitcast (%"class.std::__2::basic_istream"* @_ZNSt3__23cinE to i8**), align 4
  %vbase.offset.ptr = getelementptr i8, i8* %vtable, i64 -12
  %0 = bitcast i8* %vbase.offset.ptr to i32*
  %vbase.offset = load i32, i32* %0, align 4
  %add.ptr = getelementptr inbounds i8, i8* bitcast (%"class.std::__2::basic_istream"* @_ZNSt3__23cinE to i8*), i32 %vbase.offset
  %1 = bitcast i8* %add.ptr to %"class.std::__2::basic_ios"*
  %call2 = call %"class.std::__2::basic_streambuf"* @_ZNKSt3__29basic_iosIcNS_11char_traitsIcEEE5rdbufEv(%"class.std::__2::basic_ios"* %1)
  %file3 = getelementptr inbounds %"class.(anonymous namespace)::Logger", %"class.(anonymous namespace)::Logger"* %this1, i32 0, i32 0
  %call4 = call %"class.std::__2::basic_filebuf"* @_ZNKSt3__214basic_ofstreamIcNS_11char_traitsIcEEE5rdbufEv(%"class.std::__2::basic_ofstream"* %file3)
  %2 = bitcast %"class.std::__2::basic_filebuf"* %call4 to %"class.std::__2::basic_streambuf"*
  %call5 = call %"struct.(anonymous namespace)::Tie"* @_ZN12_GLOBAL__N_13TieC2EPNSt3__215basic_streambufIcNS1_11char_traitsIcEEEES6_(%"struct.(anonymous namespace)::Tie"* %in, %"class.std::__2::basic_streambuf"* %call2, %"class.std::__2::basic_streambuf"* %2)
  %out = getelementptr inbounds %"class.(anonymous namespace)::Logger", %"class.(anonymous namespace)::Logger"* %this1, i32 0, i32 2
  %vtable6 = load i8*, i8** bitcast (%"class.std::__2::basic_ostream"* @_ZNSt3__24coutE to i8**), align 4
  %vbase.offset.ptr7 = getelementptr i8, i8* %vtable6, i64 -12
  %3 = bitcast i8* %vbase.offset.ptr7 to i32*
  %vbase.offset8 = load i32, i32* %3, align 4
  %add.ptr9 = getelementptr inbounds i8, i8* bitcast (%"class.std::__2::basic_ostream"* @_ZNSt3__24coutE to i8*), i32 %vbase.offset8
  %4 = bitcast i8* %add.ptr9 to %"class.std::__2::basic_ios"*
  %call10 = call %"class.std::__2::basic_streambuf"* @_ZNKSt3__29basic_iosIcNS_11char_traitsIcEEE5rdbufEv(%"class.std::__2::basic_ios"* %4)
  %file11 = getelementptr inbounds %"class.(anonymous namespace)::Logger", %"class.(anonymous namespace)::Logger"* %this1, i32 0, i32 0
  %call12 = call %"class.std::__2::basic_filebuf"* @_ZNKSt3__214basic_ofstreamIcNS_11char_traitsIcEEE5rdbufEv(%"class.std::__2::basic_ofstream"* %file11)
  %5 = bitcast %"class.std::__2::basic_filebuf"* %call12 to %"class.std::__2::basic_streambuf"*
  %call13 = call %"struct.(anonymous namespace)::Tie"* @_ZN12_GLOBAL__N_13TieC2EPNSt3__215basic_streambufIcNS1_11char_traitsIcEEEES6_(%"struct.(anonymous namespace)::Tie"* %out, %"class.std::__2::basic_streambuf"* %call10, %"class.std::__2::basic_streambuf"* %5)
  ret %"class.(anonymous namespace)::Logger"* %this1
}

; Function Attrs: noinline nounwind
define internal void @__cxx_global_array_dtor.12(i8* %0) #0 {
entry:
  %.addr = alloca i8*, align 4
  store i8* %0, i8** %.addr, align 4
  %call = call %"class.(anonymous namespace)::Logger"* @_ZN12_GLOBAL__N_16LoggerD2Ev(%"class.(anonymous namespace)::Logger"* @_ZZN12_GLOBAL__N_16Logger5startERKNSt3__212basic_stringIcNS1_11char_traitsIcEENS1_9allocatorIcEEEEE1l) #3
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal %"class.(anonymous namespace)::Logger"* @_ZN12_GLOBAL__N_16LoggerD2Ev(%"class.(anonymous namespace)::Logger"* returned %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %"class.(anonymous namespace)::Logger"*, align 4
  %ref.tmp = alloca %"class.std::__2::basic_string", align 4
  store %"class.(anonymous namespace)::Logger"* %this, %"class.(anonymous namespace)::Logger"** %this.addr, align 4
  %this1 = load %"class.(anonymous namespace)::Logger"*, %"class.(anonymous namespace)::Logger"** %this.addr, align 4
  %call = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2IDnEEPKc(%"class.std::__2::basic_string"* %ref.tmp, i8* getelementptr inbounds ([1 x i8], [1 x i8]* @.str.2, i32 0, i32 0))
  call void @_ZN12_GLOBAL__N_16Logger5startERKNSt3__212basic_stringIcNS1_11char_traitsIcEENS1_9allocatorIcEEEE(%"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %ref.tmp)
  %call2 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_string"* %ref.tmp) #3
  %out = getelementptr inbounds %"class.(anonymous namespace)::Logger", %"class.(anonymous namespace)::Logger"* %this1, i32 0, i32 2
  %call3 = call %"struct.(anonymous namespace)::Tie"* @_ZN12_GLOBAL__N_13TieD2Ev(%"struct.(anonymous namespace)::Tie"* %out) #3
  %in = getelementptr inbounds %"class.(anonymous namespace)::Logger", %"class.(anonymous namespace)::Logger"* %this1, i32 0, i32 1
  %call4 = call %"struct.(anonymous namespace)::Tie"* @_ZN12_GLOBAL__N_13TieD2Ev(%"struct.(anonymous namespace)::Tie"* %in) #3
  %file = getelementptr inbounds %"class.(anonymous namespace)::Logger", %"class.(anonymous namespace)::Logger"* %this1, i32 0, i32 0
  %call5 = call %"class.std::__2::basic_ofstream"* @_ZNSt3__214basic_ofstreamIcNS_11char_traitsIcEEED1Ev(%"class.std::__2::basic_ofstream"* %file) #3
  ret %"class.(anonymous namespace)::Logger"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5emptyEv(%"class.std::__2::basic_string"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4sizeEv(%"class.std::__2::basic_string"* %this1) #3
  %cmp = icmp eq i32 %call, 0
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__214basic_ofstreamIcNS_11char_traitsIcEEE7is_openEv(%"class.std::__2::basic_ofstream"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_ofstream"*, align 4
  store %"class.std::__2::basic_ofstream"* %this, %"class.std::__2::basic_ofstream"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_ofstream"*, %"class.std::__2::basic_ofstream"** %this.addr, align 4
  %__sb_ = getelementptr inbounds %"class.std::__2::basic_ofstream", %"class.std::__2::basic_ofstream"* %this1, i32 0, i32 1
  %call = call zeroext i1 @_ZNKSt3__213basic_filebufIcNS_11char_traitsIcEEE7is_openEv(%"class.std::__2::basic_filebuf"* %__sb_)
  ret i1 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214basic_ofstreamIcNS_11char_traitsIcEEE4openERKNS_12basic_stringIcS2_NS_9allocatorIcEEEEj(%"class.std::__2::basic_ofstream"* %this, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %__s, i32 %__mode) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_ofstream"*, align 4
  %__s.addr = alloca %"class.std::__2::basic_string"*, align 4
  %__mode.addr = alloca i32, align 4
  store %"class.std::__2::basic_ofstream"* %this, %"class.std::__2::basic_ofstream"** %this.addr, align 4
  store %"class.std::__2::basic_string"* %__s, %"class.std::__2::basic_string"** %__s.addr, align 4
  store i32 %__mode, i32* %__mode.addr, align 4
  %this1 = load %"class.std::__2::basic_ofstream"*, %"class.std::__2::basic_ofstream"** %this.addr, align 4
  %__sb_ = getelementptr inbounds %"class.std::__2::basic_ofstream", %"class.std::__2::basic_ofstream"* %this1, i32 0, i32 1
  %0 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__s.addr, align 4
  %1 = load i32, i32* %__mode.addr, align 4
  %or = or i32 %1, 16
  %call = call %"class.std::__2::basic_filebuf"* @_ZNSt3__213basic_filebufIcNS_11char_traitsIcEEE4openERKNS_12basic_stringIcS2_NS_9allocatorIcEEEEj(%"class.std::__2::basic_filebuf"* %__sb_, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %0, i32 %or)
  %tobool = icmp ne %"class.std::__2::basic_filebuf"* %call, null
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = bitcast %"class.std::__2::basic_ofstream"* %this1 to i8**
  %vtable = load i8*, i8** %2, align 4
  %vbase.offset.ptr = getelementptr i8, i8* %vtable, i64 -12
  %3 = bitcast i8* %vbase.offset.ptr to i32*
  %vbase.offset = load i32, i32* %3, align 4
  %4 = bitcast %"class.std::__2::basic_ofstream"* %this1 to i8*
  %add.ptr = getelementptr inbounds i8, i8* %4, i32 %vbase.offset
  %5 = bitcast i8* %add.ptr to %"class.std::__2::basic_ios"*
  call void @_ZNSt3__29basic_iosIcNS_11char_traitsIcEEE5clearEj(%"class.std::__2::basic_ios"* %5, i32 0)
  br label %if.end

if.else:                                          ; preds = %entry
  %6 = bitcast %"class.std::__2::basic_ofstream"* %this1 to i8**
  %vtable2 = load i8*, i8** %6, align 4
  %vbase.offset.ptr3 = getelementptr i8, i8* %vtable2, i64 -12
  %7 = bitcast i8* %vbase.offset.ptr3 to i32*
  %vbase.offset4 = load i32, i32* %7, align 4
  %8 = bitcast %"class.std::__2::basic_ofstream"* %this1 to i8*
  %add.ptr5 = getelementptr inbounds i8, i8* %8, i32 %vbase.offset4
  %9 = bitcast i8* %add.ptr5 to %"class.std::__2::basic_ios"*
  call void @_ZNSt3__29basic_iosIcNS_11char_traitsIcEEE8setstateEj(%"class.std::__2::basic_ios"* %9, i32 4)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::basic_streambuf"* @_ZNSt3__29basic_iosIcNS_11char_traitsIcEEE5rdbufEPNS_15basic_streambufIcS2_EE(%"class.std::__2::basic_ios"* %this, %"class.std::__2::basic_streambuf"* %__sb) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_ios"*, align 4
  %__sb.addr = alloca %"class.std::__2::basic_streambuf"*, align 4
  %__r = alloca %"class.std::__2::basic_streambuf"*, align 4
  store %"class.std::__2::basic_ios"* %this, %"class.std::__2::basic_ios"** %this.addr, align 4
  store %"class.std::__2::basic_streambuf"* %__sb, %"class.std::__2::basic_streambuf"** %__sb.addr, align 4
  %this1 = load %"class.std::__2::basic_ios"*, %"class.std::__2::basic_ios"** %this.addr, align 4
  %call = call %"class.std::__2::basic_streambuf"* @_ZNKSt3__29basic_iosIcNS_11char_traitsIcEEE5rdbufEv(%"class.std::__2::basic_ios"* %this1)
  store %"class.std::__2::basic_streambuf"* %call, %"class.std::__2::basic_streambuf"** %__r, align 4
  %0 = bitcast %"class.std::__2::basic_ios"* %this1 to %"class.std::__2::ios_base"*
  %1 = load %"class.std::__2::basic_streambuf"*, %"class.std::__2::basic_streambuf"** %__sb.addr, align 4
  %2 = bitcast %"class.std::__2::basic_streambuf"* %1 to i8*
  call void @_ZNSt3__28ios_base5rdbufEPv(%"class.std::__2::ios_base"* %0, i8* %2)
  %3 = load %"class.std::__2::basic_streambuf"*, %"class.std::__2::basic_streambuf"** %__r, align 4
  ret %"class.std::__2::basic_streambuf"* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214basic_ofstreamIcNS_11char_traitsIcEEE5closeEv(%"class.std::__2::basic_ofstream"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_ofstream"*, align 4
  store %"class.std::__2::basic_ofstream"* %this, %"class.std::__2::basic_ofstream"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_ofstream"*, %"class.std::__2::basic_ofstream"** %this.addr, align 4
  %__sb_ = getelementptr inbounds %"class.std::__2::basic_ofstream", %"class.std::__2::basic_ofstream"* %this1, i32 0, i32 1
  %call = call %"class.std::__2::basic_filebuf"* @_ZNSt3__213basic_filebufIcNS_11char_traitsIcEEE5closeEv(%"class.std::__2::basic_filebuf"* %__sb_)
  %cmp = icmp eq %"class.std::__2::basic_filebuf"* %call, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %0 = bitcast %"class.std::__2::basic_ofstream"* %this1 to i8**
  %vtable = load i8*, i8** %0, align 4
  %vbase.offset.ptr = getelementptr i8, i8* %vtable, i64 -12
  %1 = bitcast i8* %vbase.offset.ptr to i32*
  %vbase.offset = load i32, i32* %1, align 4
  %2 = bitcast %"class.std::__2::basic_ofstream"* %this1 to i8*
  %add.ptr = getelementptr inbounds i8, i8* %2, i32 %vbase.offset
  %3 = bitcast i8* %add.ptr to %"class.std::__2::basic_ios"*
  call void @_ZNSt3__29basic_iosIcNS_11char_traitsIcEEE8setstateEj(%"class.std::__2::basic_ios"* %3, i32 4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::basic_ofstream"* @_ZNSt3__214basic_ofstreamIcNS_11char_traitsIcEEEC1Ev(%"class.std::__2::basic_ofstream"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_ofstream"*, align 4
  store %"class.std::__2::basic_ofstream"* %this, %"class.std::__2::basic_ofstream"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_ofstream"*, %"class.std::__2::basic_ofstream"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::basic_ofstream"* %this1 to i8*
  %1 = getelementptr inbounds i8, i8* %0, i32 104
  %2 = bitcast i8* %1 to %"class.std::__2::basic_ios"*
  %call = call %"class.std::__2::basic_ios"* @_ZNSt3__29basic_iosIcNS_11char_traitsIcEEEC2Ev(%"class.std::__2::basic_ios"* %2)
  %3 = bitcast %"class.std::__2::basic_ofstream"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*], [5 x i8*] }, { [5 x i8*], [5 x i8*] }* @_ZTVNSt3__214basic_ofstreamIcNS_11char_traitsIcEEEE, i32 0, inrange i32 0, i32 3) to i32 (...)**), i32 (...)*** %3, align 4
  %4 = bitcast %"class.std::__2::basic_ofstream"* %this1 to i8*
  %add.ptr = getelementptr inbounds i8, i8* %4, i32 104
  %5 = bitcast i8* %add.ptr to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*], [5 x i8*] }, { [5 x i8*], [5 x i8*] }* @_ZTVNSt3__214basic_ofstreamIcNS_11char_traitsIcEEEE, i32 0, inrange i32 1, i32 3) to i32 (...)**), i32 (...)*** %5, align 4
  %6 = bitcast %"class.std::__2::basic_ofstream"* %this1 to %"class.std::__2::basic_ostream"*
  %__sb_ = getelementptr inbounds %"class.std::__2::basic_ofstream", %"class.std::__2::basic_ofstream"* %this1, i32 0, i32 1
  %7 = bitcast %"class.std::__2::basic_filebuf"* %__sb_ to %"class.std::__2::basic_streambuf"*
  %call2 = call %"class.std::__2::basic_ostream"* @_ZNSt3__213basic_ostreamIcNS_11char_traitsIcEEEC2EPNS_15basic_streambufIcS2_EE(%"class.std::__2::basic_ostream"* %6, i8** getelementptr inbounds ([4 x i8*], [4 x i8*]* @_ZTTNSt3__214basic_ofstreamIcNS_11char_traitsIcEEEE, i64 0, i64 1), %"class.std::__2::basic_streambuf"* %7)
  %8 = bitcast %"class.std::__2::basic_ofstream"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*], [5 x i8*] }, { [5 x i8*], [5 x i8*] }* @_ZTVNSt3__214basic_ofstreamIcNS_11char_traitsIcEEEE, i32 0, inrange i32 0, i32 3) to i32 (...)**), i32 (...)*** %8, align 4
  %9 = bitcast %"class.std::__2::basic_ofstream"* %this1 to i8*
  %add.ptr3 = getelementptr inbounds i8, i8* %9, i32 104
  %10 = bitcast i8* %add.ptr3 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*], [5 x i8*] }, { [5 x i8*], [5 x i8*] }* @_ZTVNSt3__214basic_ofstreamIcNS_11char_traitsIcEEEE, i32 0, inrange i32 1, i32 3) to i32 (...)**), i32 (...)*** %10, align 4
  %__sb_4 = getelementptr inbounds %"class.std::__2::basic_ofstream", %"class.std::__2::basic_ofstream"* %this1, i32 0, i32 1
  %call5 = call %"class.std::__2::basic_filebuf"* @_ZNSt3__213basic_filebufIcNS_11char_traitsIcEEEC2Ev(%"class.std::__2::basic_filebuf"* %__sb_4)
  ret %"class.std::__2::basic_ofstream"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::basic_streambuf"* @_ZNKSt3__29basic_iosIcNS_11char_traitsIcEEE5rdbufEv(%"class.std::__2::basic_ios"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_ios"*, align 4
  store %"class.std::__2::basic_ios"* %this, %"class.std::__2::basic_ios"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_ios"*, %"class.std::__2::basic_ios"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::basic_ios"* %this1 to %"class.std::__2::ios_base"*
  %call = call i8* @_ZNKSt3__28ios_base5rdbufEv(%"class.std::__2::ios_base"* %0)
  %1 = bitcast i8* %call to %"class.std::__2::basic_streambuf"*
  ret %"class.std::__2::basic_streambuf"* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::basic_filebuf"* @_ZNKSt3__214basic_ofstreamIcNS_11char_traitsIcEEE5rdbufEv(%"class.std::__2::basic_ofstream"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_ofstream"*, align 4
  store %"class.std::__2::basic_ofstream"* %this, %"class.std::__2::basic_ofstream"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_ofstream"*, %"class.std::__2::basic_ofstream"** %this.addr, align 4
  %__sb_ = getelementptr inbounds %"class.std::__2::basic_ofstream", %"class.std::__2::basic_ofstream"* %this1, i32 0, i32 1
  ret %"class.std::__2::basic_filebuf"* %__sb_
}

; Function Attrs: noinline nounwind optnone
define internal %"struct.(anonymous namespace)::Tie"* @_ZN12_GLOBAL__N_13TieC2EPNSt3__215basic_streambufIcNS1_11char_traitsIcEEEES6_(%"struct.(anonymous namespace)::Tie"* returned %this, %"class.std::__2::basic_streambuf"* %b, %"class.std::__2::basic_streambuf"* %l) unnamed_addr #1 {
entry:
  %this.addr = alloca %"struct.(anonymous namespace)::Tie"*, align 4
  %b.addr = alloca %"class.std::__2::basic_streambuf"*, align 4
  %l.addr = alloca %"class.std::__2::basic_streambuf"*, align 4
  store %"struct.(anonymous namespace)::Tie"* %this, %"struct.(anonymous namespace)::Tie"** %this.addr, align 4
  store %"class.std::__2::basic_streambuf"* %b, %"class.std::__2::basic_streambuf"** %b.addr, align 4
  store %"class.std::__2::basic_streambuf"* %l, %"class.std::__2::basic_streambuf"** %l.addr, align 4
  %this1 = load %"struct.(anonymous namespace)::Tie"*, %"struct.(anonymous namespace)::Tie"** %this.addr, align 4
  %0 = bitcast %"struct.(anonymous namespace)::Tie"* %this1 to %"class.std::__2::basic_streambuf"*
  %call = call %"class.std::__2::basic_streambuf"* @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEEC2Ev(%"class.std::__2::basic_streambuf"* %0)
  %1 = bitcast %"struct.(anonymous namespace)::Tie"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [16 x i8*] }, { [16 x i8*] }* @_ZTVN12_GLOBAL__N_13TieE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %buf = getelementptr inbounds %"struct.(anonymous namespace)::Tie", %"struct.(anonymous namespace)::Tie"* %this1, i32 0, i32 1
  %2 = load %"class.std::__2::basic_streambuf"*, %"class.std::__2::basic_streambuf"** %b.addr, align 4
  store %"class.std::__2::basic_streambuf"* %2, %"class.std::__2::basic_streambuf"** %buf, align 4
  %logBuf = getelementptr inbounds %"struct.(anonymous namespace)::Tie", %"struct.(anonymous namespace)::Tie"* %this1, i32 0, i32 2
  %3 = load %"class.std::__2::basic_streambuf"*, %"class.std::__2::basic_streambuf"** %l.addr, align 4
  store %"class.std::__2::basic_streambuf"* %3, %"class.std::__2::basic_streambuf"** %logBuf, align 4
  ret %"struct.(anonymous namespace)::Tie"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::basic_ios"* @_ZNSt3__29basic_iosIcNS_11char_traitsIcEEEC2Ev(%"class.std::__2::basic_ios"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_ios"*, align 4
  store %"class.std::__2::basic_ios"* %this, %"class.std::__2::basic_ios"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_ios"*, %"class.std::__2::basic_ios"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::basic_ios"* %this1 to %"class.std::__2::ios_base"*
  %call = call %"class.std::__2::ios_base"* @_ZNSt3__28ios_baseC2Ev(%"class.std::__2::ios_base"* %0)
  %1 = bitcast %"class.std::__2::basic_ios"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [4 x i8*] }, { [4 x i8*] }* @_ZTVNSt3__29basic_iosIcNS_11char_traitsIcEEEE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  ret %"class.std::__2::basic_ios"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::basic_ostream"* @_ZNSt3__213basic_ostreamIcNS_11char_traitsIcEEEC2EPNS_15basic_streambufIcS2_EE(%"class.std::__2::basic_ostream"* returned %this, i8** %vtt, %"class.std::__2::basic_streambuf"* %__sb) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_ostream"*, align 4
  %vtt.addr = alloca i8**, align 4
  %__sb.addr = alloca %"class.std::__2::basic_streambuf"*, align 4
  store %"class.std::__2::basic_ostream"* %this, %"class.std::__2::basic_ostream"** %this.addr, align 4
  store i8** %vtt, i8*** %vtt.addr, align 4
  store %"class.std::__2::basic_streambuf"* %__sb, %"class.std::__2::basic_streambuf"** %__sb.addr, align 4
  %this1 = load %"class.std::__2::basic_ostream"*, %"class.std::__2::basic_ostream"** %this.addr, align 4
  %vtt2 = load i8**, i8*** %vtt.addr, align 4
  %0 = load i8*, i8** %vtt2, align 4
  %1 = bitcast %"class.std::__2::basic_ostream"* %this1 to i32 (...)***
  %2 = bitcast i8* %0 to i32 (...)**
  store i32 (...)** %2, i32 (...)*** %1, align 4
  %3 = getelementptr inbounds i8*, i8** %vtt2, i64 1
  %4 = load i8*, i8** %3, align 4
  %5 = bitcast %"class.std::__2::basic_ostream"* %this1 to i8**
  %vtable = load i8*, i8** %5, align 4
  %vbase.offset.ptr = getelementptr i8, i8* %vtable, i64 -12
  %6 = bitcast i8* %vbase.offset.ptr to i32*
  %vbase.offset = load i32, i32* %6, align 4
  %7 = bitcast %"class.std::__2::basic_ostream"* %this1 to i8*
  %add.ptr = getelementptr inbounds i8, i8* %7, i32 %vbase.offset
  %8 = bitcast i8* %add.ptr to i32 (...)***
  %9 = bitcast i8* %4 to i32 (...)**
  store i32 (...)** %9, i32 (...)*** %8, align 4
  %10 = bitcast %"class.std::__2::basic_ostream"* %this1 to i8**
  %vtable3 = load i8*, i8** %10, align 4
  %vbase.offset.ptr4 = getelementptr i8, i8* %vtable3, i64 -12
  %11 = bitcast i8* %vbase.offset.ptr4 to i32*
  %vbase.offset5 = load i32, i32* %11, align 4
  %12 = bitcast %"class.std::__2::basic_ostream"* %this1 to i8*
  %add.ptr6 = getelementptr inbounds i8, i8* %12, i32 %vbase.offset5
  %13 = bitcast i8* %add.ptr6 to %"class.std::__2::basic_ios"*
  %14 = load %"class.std::__2::basic_streambuf"*, %"class.std::__2::basic_streambuf"** %__sb.addr, align 4
  call void @_ZNSt3__29basic_iosIcNS_11char_traitsIcEEE4initEPNS_15basic_streambufIcS2_EE(%"class.std::__2::basic_ios"* %13, %"class.std::__2::basic_streambuf"* %14)
  ret %"class.std::__2::basic_ostream"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::basic_filebuf"* @_ZNSt3__213basic_filebufIcNS_11char_traitsIcEEEC2Ev(%"class.std::__2::basic_filebuf"* returned %this) unnamed_addr #1 comdat {
entry:
  %retval = alloca %"class.std::__2::basic_filebuf"*, align 4
  %this.addr = alloca %"class.std::__2::basic_filebuf"*, align 4
  %ref.tmp = alloca %"class.std::__2::locale", align 4
  %ref.tmp4 = alloca %"class.std::__2::locale", align 4
  store %"class.std::__2::basic_filebuf"* %this, %"class.std::__2::basic_filebuf"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_filebuf"*, %"class.std::__2::basic_filebuf"** %this.addr, align 4
  store %"class.std::__2::basic_filebuf"* %this1, %"class.std::__2::basic_filebuf"** %retval, align 4
  %0 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call = call %"class.std::__2::basic_streambuf"* @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEEC2Ev(%"class.std::__2::basic_streambuf"* %0)
  %1 = bitcast %"class.std::__2::basic_filebuf"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [16 x i8*] }, { [16 x i8*] }* @_ZTVNSt3__213basic_filebufIcNS_11char_traitsIcEEEE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %__extbuf_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 1
  store i8* null, i8** %__extbuf_, align 4
  %__extbufnext_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 2
  store i8* null, i8** %__extbufnext_, align 4
  %__extbufend_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 3
  store i8* null, i8** %__extbufend_, align 4
  %__ebs_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 5
  store i32 0, i32* %__ebs_, align 4
  %__intbuf_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 6
  store i8* null, i8** %__intbuf_, align 4
  %__ibs_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 7
  store i32 0, i32* %__ibs_, align 4
  %__file_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 8
  store %struct._IO_FILE* null, %struct._IO_FILE** %__file_, align 4
  %__cv_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 9
  store %"class.std::__2::codecvt"* null, %"class.std::__2::codecvt"** %__cv_, align 4
  %__st_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 10
  %2 = bitcast %struct.__mbstate_t* %__st_ to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %2, i8 0, i32 8, i1 false)
  %__st_last_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 11
  %3 = bitcast %struct.__mbstate_t* %__st_last_ to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %3, i8 0, i32 8, i1 false)
  %__om_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 12
  store i32 0, i32* %__om_, align 4
  %__cm_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 13
  store i32 0, i32* %__cm_, align 4
  %__owns_eb_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 14
  store i8 0, i8* %__owns_eb_, align 4
  %__owns_ib_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 15
  store i8 0, i8* %__owns_ib_, align 1
  %__always_noconv_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 16
  store i8 0, i8* %__always_noconv_, align 2
  %4 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  call void @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE6getlocEv(%"class.std::__2::locale"* sret align 4 %ref.tmp, %"class.std::__2::basic_streambuf"* %4)
  %call2 = call zeroext i1 @_ZNSt3__29has_facetINS_7codecvtIcc11__mbstate_tEEEEbRKNS_6localeE(%"class.std::__2::locale"* nonnull align 4 dereferenceable(4) %ref.tmp) #3
  %call3 = call %"class.std::__2::locale"* @_ZNSt3__26localeD1Ev(%"class.std::__2::locale"* %ref.tmp) #3
  br i1 %call2, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %5 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  call void @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE6getlocEv(%"class.std::__2::locale"* sret align 4 %ref.tmp4, %"class.std::__2::basic_streambuf"* %5)
  %call5 = call nonnull align 4 dereferenceable(8) %"class.std::__2::codecvt"* @_ZNSt3__29use_facetINS_7codecvtIcc11__mbstate_tEEEERKT_RKNS_6localeE(%"class.std::__2::locale"* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %__cv_6 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 9
  store %"class.std::__2::codecvt"* %call5, %"class.std::__2::codecvt"** %__cv_6, align 4
  %call7 = call %"class.std::__2::locale"* @_ZNSt3__26localeD1Ev(%"class.std::__2::locale"* %ref.tmp4) #3
  %__cv_8 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 9
  %6 = load %"class.std::__2::codecvt"*, %"class.std::__2::codecvt"** %__cv_8, align 4
  %call9 = call zeroext i1 @_ZNKSt3__27codecvtIcc11__mbstate_tE13always_noconvEv(%"class.std::__2::codecvt"* %6) #3
  %__always_noconv_10 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 16
  %frombool = zext i1 %call9 to i8
  store i8 %frombool, i8* %__always_noconv_10, align 2
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %7 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"* (%"class.std::__2::basic_filebuf"*, i8*, i32)***
  %vtable = load %"class.std::__2::basic_streambuf"* (%"class.std::__2::basic_filebuf"*, i8*, i32)**, %"class.std::__2::basic_streambuf"* (%"class.std::__2::basic_filebuf"*, i8*, i32)*** %7, align 4
  %vfn = getelementptr inbounds %"class.std::__2::basic_streambuf"* (%"class.std::__2::basic_filebuf"*, i8*, i32)*, %"class.std::__2::basic_streambuf"* (%"class.std::__2::basic_filebuf"*, i8*, i32)** %vtable, i64 3
  %8 = load %"class.std::__2::basic_streambuf"* (%"class.std::__2::basic_filebuf"*, i8*, i32)*, %"class.std::__2::basic_streambuf"* (%"class.std::__2::basic_filebuf"*, i8*, i32)** %vfn, align 4
  %call11 = call %"class.std::__2::basic_streambuf"* %8(%"class.std::__2::basic_filebuf"* %this1, i8* null, i32 4096)
  %9 = load %"class.std::__2::basic_filebuf"*, %"class.std::__2::basic_filebuf"** %retval, align 4
  ret %"class.std::__2::basic_filebuf"* %9
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::basic_ofstream"* @_ZNSt3__214basic_ofstreamIcNS_11char_traitsIcEEED1Ev(%"class.std::__2::basic_ofstream"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_ofstream"*, align 4
  store %"class.std::__2::basic_ofstream"* %this, %"class.std::__2::basic_ofstream"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_ofstream"*, %"class.std::__2::basic_ofstream"** %this.addr, align 4
  %call = call %"class.std::__2::basic_ofstream"* @_ZNSt3__214basic_ofstreamIcNS_11char_traitsIcEEED2Ev(%"class.std::__2::basic_ofstream"* %this1, i8** getelementptr inbounds ([4 x i8*], [4 x i8*]* @_ZTTNSt3__214basic_ofstreamIcNS_11char_traitsIcEEEE, i64 0, i64 0)) #3
  %0 = bitcast %"class.std::__2::basic_ofstream"* %this1 to i8*
  %1 = getelementptr inbounds i8, i8* %0, i32 104
  %2 = bitcast i8* %1 to %"class.std::__2::basic_ios"*
  %call2 = call %"class.std::__2::basic_ios"* @_ZNSt3__29basic_iosIcNS_11char_traitsIcEEED2Ev(%"class.std::__2::basic_ios"* %2) #3
  ret %"class.std::__2::basic_ofstream"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214basic_ofstreamIcNS_11char_traitsIcEEED0Ev(%"class.std::__2::basic_ofstream"* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_ofstream"*, align 4
  store %"class.std::__2::basic_ofstream"* %this, %"class.std::__2::basic_ofstream"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_ofstream"*, %"class.std::__2::basic_ofstream"** %this.addr, align 4
  %call = call %"class.std::__2::basic_ofstream"* @_ZNSt3__214basic_ofstreamIcNS_11char_traitsIcEEED1Ev(%"class.std::__2::basic_ofstream"* %this1) #3
  %0 = bitcast %"class.std::__2::basic_ofstream"* %this1 to i8*
  call void @_ZdlPv(i8* %0) #11
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::basic_ofstream"* @_ZTv0_n12_NSt3__214basic_ofstreamIcNS_11char_traitsIcEEED1Ev(%"class.std::__2::basic_ofstream"* %this) unnamed_addr #1 comdat {
entry:
  %retval = alloca %"class.std::__2::basic_ofstream"*, align 4
  %this.addr = alloca %"class.std::__2::basic_ofstream"*, align 4
  store %"class.std::__2::basic_ofstream"* %this, %"class.std::__2::basic_ofstream"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_ofstream"*, %"class.std::__2::basic_ofstream"** %this.addr, align 4
  store %"class.std::__2::basic_ofstream"* %this1, %"class.std::__2::basic_ofstream"** %retval, align 4
  %0 = bitcast %"class.std::__2::basic_ofstream"* %this1 to i8*
  %1 = bitcast i8* %0 to i8**
  %2 = load i8*, i8** %1, align 4
  %3 = getelementptr inbounds i8, i8* %2, i64 -12
  %4 = bitcast i8* %3 to i32*
  %5 = load i32, i32* %4, align 4
  %6 = getelementptr inbounds i8, i8* %0, i32 %5
  %7 = bitcast i8* %6 to %"class.std::__2::basic_ofstream"*
  %call = tail call %"class.std::__2::basic_ofstream"* @_ZNSt3__214basic_ofstreamIcNS_11char_traitsIcEEED1Ev(%"class.std::__2::basic_ofstream"* %7) #3
  ret %"class.std::__2::basic_ofstream"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZTv0_n12_NSt3__214basic_ofstreamIcNS_11char_traitsIcEEED0Ev(%"class.std::__2::basic_ofstream"* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_ofstream"*, align 4
  store %"class.std::__2::basic_ofstream"* %this, %"class.std::__2::basic_ofstream"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_ofstream"*, %"class.std::__2::basic_ofstream"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::basic_ofstream"* %this1 to i8*
  %1 = bitcast i8* %0 to i8**
  %2 = load i8*, i8** %1, align 4
  %3 = getelementptr inbounds i8, i8* %2, i64 -12
  %4 = bitcast i8* %3 to i32*
  %5 = load i32, i32* %4, align 4
  %6 = getelementptr inbounds i8, i8* %0, i32 %5
  %7 = bitcast i8* %6 to %"class.std::__2::basic_ofstream"*
  tail call void @_ZNSt3__214basic_ofstreamIcNS_11char_traitsIcEEED0Ev(%"class.std::__2::basic_ofstream"* %7) #3
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::ios_base"* @_ZNSt3__28ios_baseC2Ev(%"class.std::__2::ios_base"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::ios_base"*, align 4
  store %"class.std::__2::ios_base"* %this, %"class.std::__2::ios_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::ios_base"*, %"class.std::__2::ios_base"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::ios_base"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [4 x i8*] }, { [4 x i8*] }* @_ZTVNSt3__28ios_baseE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  ret %"class.std::__2::ios_base"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29basic_iosIcNS_11char_traitsIcEEE4initEPNS_15basic_streambufIcS2_EE(%"class.std::__2::basic_ios"* %this, %"class.std::__2::basic_streambuf"* %__sb) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_ios"*, align 4
  %__sb.addr = alloca %"class.std::__2::basic_streambuf"*, align 4
  store %"class.std::__2::basic_ios"* %this, %"class.std::__2::basic_ios"** %this.addr, align 4
  store %"class.std::__2::basic_streambuf"* %__sb, %"class.std::__2::basic_streambuf"** %__sb.addr, align 4
  %this1 = load %"class.std::__2::basic_ios"*, %"class.std::__2::basic_ios"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::basic_ios"* %this1 to %"class.std::__2::ios_base"*
  %1 = load %"class.std::__2::basic_streambuf"*, %"class.std::__2::basic_streambuf"** %__sb.addr, align 4
  %2 = bitcast %"class.std::__2::basic_streambuf"* %1 to i8*
  call void @_ZNSt3__28ios_base4initEPv(%"class.std::__2::ios_base"* %0, i8* %2)
  %__tie_ = getelementptr inbounds %"class.std::__2::basic_ios", %"class.std::__2::basic_ios"* %this1, i32 0, i32 1
  store %"class.std::__2::basic_ostream"* null, %"class.std::__2::basic_ostream"** %__tie_, align 4
  %call = call i32 @_ZNSt3__211char_traitsIcE3eofEv() #3
  %__fill_ = getelementptr inbounds %"class.std::__2::basic_ios", %"class.std::__2::basic_ios"* %this1, i32 0, i32 2
  store i32 %call, i32* %__fill_, align 4
  ret void
}

declare void @_ZNSt3__28ios_base4initEPv(%"class.std::__2::ios_base"*, i8*) #4

declare %"class.std::__2::basic_streambuf"* @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEEC2Ev(%"class.std::__2::basic_streambuf"* returned) unnamed_addr #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNSt3__29has_facetINS_7codecvtIcc11__mbstate_tEEEEbRKNS_6localeE(%"class.std::__2::locale"* nonnull align 4 dereferenceable(4) %__l) #1 comdat {
entry:
  %__l.addr = alloca %"class.std::__2::locale"*, align 4
  store %"class.std::__2::locale"* %__l, %"class.std::__2::locale"** %__l.addr, align 4
  %0 = load %"class.std::__2::locale"*, %"class.std::__2::locale"** %__l.addr, align 4
  %call = call zeroext i1 @_ZNKSt3__26locale9has_facetERNS0_2idE(%"class.std::__2::locale"* %0, %"class.std::__2::locale::id"* nonnull align 4 dereferenceable(8) @_ZNSt3__27codecvtIcc11__mbstate_tE2idE)
  ret i1 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE6getlocEv(%"class.std::__2::locale"* noalias sret align 4 %agg.result, %"class.std::__2::basic_streambuf"* %this) #1 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %"class.std::__2::basic_streambuf"*, align 4
  %0 = bitcast %"class.std::__2::locale"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %"class.std::__2::basic_streambuf"* %this, %"class.std::__2::basic_streambuf"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_streambuf"*, %"class.std::__2::basic_streambuf"** %this.addr, align 4
  %__loc_ = getelementptr inbounds %"class.std::__2::basic_streambuf", %"class.std::__2::basic_streambuf"* %this1, i32 0, i32 1
  %call = call %"class.std::__2::locale"* @_ZNSt3__26localeC1ERKS0_(%"class.std::__2::locale"* %agg.result, %"class.std::__2::locale"* nonnull align 4 dereferenceable(4) %__loc_) #3
  ret void
}

; Function Attrs: nounwind
declare %"class.std::__2::locale"* @_ZNSt3__26localeD1Ev(%"class.std::__2::locale"* returned) unnamed_addr #2

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(8) %"class.std::__2::codecvt"* @_ZNSt3__29use_facetINS_7codecvtIcc11__mbstate_tEEEERKT_RKNS_6localeE(%"class.std::__2::locale"* nonnull align 4 dereferenceable(4) %__l) #1 comdat {
entry:
  %__l.addr = alloca %"class.std::__2::locale"*, align 4
  store %"class.std::__2::locale"* %__l, %"class.std::__2::locale"** %__l.addr, align 4
  %0 = load %"class.std::__2::locale"*, %"class.std::__2::locale"** %__l.addr, align 4
  %call = call %"class.std::__2::locale::facet"* @_ZNKSt3__26locale9use_facetERNS0_2idE(%"class.std::__2::locale"* %0, %"class.std::__2::locale::id"* nonnull align 4 dereferenceable(8) @_ZNSt3__27codecvtIcc11__mbstate_tE2idE)
  %1 = bitcast %"class.std::__2::locale::facet"* %call to %"class.std::__2::codecvt"*
  ret %"class.std::__2::codecvt"* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__27codecvtIcc11__mbstate_tE13always_noconvEv(%"class.std::__2::codecvt"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::codecvt"*, align 4
  store %"class.std::__2::codecvt"* %this, %"class.std::__2::codecvt"** %this.addr, align 4
  %this1 = load %"class.std::__2::codecvt"*, %"class.std::__2::codecvt"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::codecvt"* %this1 to i1 (%"class.std::__2::codecvt"*)***
  %vtable = load i1 (%"class.std::__2::codecvt"*)**, i1 (%"class.std::__2::codecvt"*)*** %0, align 4
  %vfn = getelementptr inbounds i1 (%"class.std::__2::codecvt"*)*, i1 (%"class.std::__2::codecvt"*)** %vtable, i64 7
  %1 = load i1 (%"class.std::__2::codecvt"*)*, i1 (%"class.std::__2::codecvt"*)** %vfn, align 4
  %call = call zeroext i1 %1(%"class.std::__2::codecvt"* %this1) #3
  ret i1 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::basic_filebuf"* @_ZNSt3__213basic_filebufIcNS_11char_traitsIcEEED2Ev(%"class.std::__2::basic_filebuf"* returned %this) unnamed_addr #1 comdat {
entry:
  %retval = alloca %"class.std::__2::basic_filebuf"*, align 4
  %this.addr = alloca %"class.std::__2::basic_filebuf"*, align 4
  store %"class.std::__2::basic_filebuf"* %this, %"class.std::__2::basic_filebuf"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_filebuf"*, %"class.std::__2::basic_filebuf"** %this.addr, align 4
  store %"class.std::__2::basic_filebuf"* %this1, %"class.std::__2::basic_filebuf"** %retval, align 4
  %0 = bitcast %"class.std::__2::basic_filebuf"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [16 x i8*] }, { [16 x i8*] }* @_ZTVNSt3__213basic_filebufIcNS_11char_traitsIcEEEE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %call = call %"class.std::__2::basic_filebuf"* @_ZNSt3__213basic_filebufIcNS_11char_traitsIcEEE5closeEv(%"class.std::__2::basic_filebuf"* %this1)
  %__owns_eb_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 14
  %1 = load i8, i8* %__owns_eb_, align 4
  %tobool = trunc i8 %1 to i1
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__extbuf_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 1
  %2 = load i8*, i8** %__extbuf_, align 4
  %isnull = icmp eq i8* %2, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %if.then
  call void @_ZdaPv(i8* %2) #11
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %if.then
  br label %if.end

if.end:                                           ; preds = %delete.end, %entry
  %__owns_ib_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 15
  %3 = load i8, i8* %__owns_ib_, align 1
  %tobool2 = trunc i8 %3 to i1
  br i1 %tobool2, label %if.then3, label %if.end7

if.then3:                                         ; preds = %if.end
  %__intbuf_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 6
  %4 = load i8*, i8** %__intbuf_, align 4
  %isnull4 = icmp eq i8* %4, null
  br i1 %isnull4, label %delete.end6, label %delete.notnull5

delete.notnull5:                                  ; preds = %if.then3
  call void @_ZdaPv(i8* %4) #11
  br label %delete.end6

delete.end6:                                      ; preds = %delete.notnull5, %if.then3
  br label %if.end7

if.end7:                                          ; preds = %delete.end6, %if.end
  %5 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call8 = call %"class.std::__2::basic_streambuf"* @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEED2Ev(%"class.std::__2::basic_streambuf"* %5) #3
  %6 = load %"class.std::__2::basic_filebuf"*, %"class.std::__2::basic_filebuf"** %retval, align 4
  ret %"class.std::__2::basic_filebuf"* %6
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213basic_filebufIcNS_11char_traitsIcEEED0Ev(%"class.std::__2::basic_filebuf"* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_filebuf"*, align 4
  store %"class.std::__2::basic_filebuf"* %this, %"class.std::__2::basic_filebuf"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_filebuf"*, %"class.std::__2::basic_filebuf"** %this.addr, align 4
  %call = call %"class.std::__2::basic_filebuf"* @_ZNSt3__213basic_filebufIcNS_11char_traitsIcEEED2Ev(%"class.std::__2::basic_filebuf"* %this1) #3
  %0 = bitcast %"class.std::__2::basic_filebuf"* %this1 to i8*
  call void @_ZdlPv(i8* %0) #11
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213basic_filebufIcNS_11char_traitsIcEEE5imbueERKNS_6localeE(%"class.std::__2::basic_filebuf"* %this, %"class.std::__2::locale"* nonnull align 4 dereferenceable(4) %__loc) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_filebuf"*, align 4
  %__loc.addr = alloca %"class.std::__2::locale"*, align 4
  %__old_anc = alloca i8, align 1
  store %"class.std::__2::basic_filebuf"* %this, %"class.std::__2::basic_filebuf"** %this.addr, align 4
  store %"class.std::__2::locale"* %__loc, %"class.std::__2::locale"** %__loc.addr, align 4
  %this1 = load %"class.std::__2::basic_filebuf"*, %"class.std::__2::basic_filebuf"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::basic_filebuf"* %this1 to i32 (%"class.std::__2::basic_filebuf"*)***
  %vtable = load i32 (%"class.std::__2::basic_filebuf"*)**, i32 (%"class.std::__2::basic_filebuf"*)*** %0, align 4
  %vfn = getelementptr inbounds i32 (%"class.std::__2::basic_filebuf"*)*, i32 (%"class.std::__2::basic_filebuf"*)** %vtable, i64 6
  %1 = load i32 (%"class.std::__2::basic_filebuf"*)*, i32 (%"class.std::__2::basic_filebuf"*)** %vfn, align 4
  %call = call i32 %1(%"class.std::__2::basic_filebuf"* %this1)
  %2 = load %"class.std::__2::locale"*, %"class.std::__2::locale"** %__loc.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(8) %"class.std::__2::codecvt"* @_ZNSt3__29use_facetINS_7codecvtIcc11__mbstate_tEEEERKT_RKNS_6localeE(%"class.std::__2::locale"* nonnull align 4 dereferenceable(4) %2)
  %__cv_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 9
  store %"class.std::__2::codecvt"* %call2, %"class.std::__2::codecvt"** %__cv_, align 4
  %__always_noconv_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 16
  %3 = load i8, i8* %__always_noconv_, align 2
  %tobool = trunc i8 %3 to i1
  %frombool = zext i1 %tobool to i8
  store i8 %frombool, i8* %__old_anc, align 1
  %__cv_3 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 9
  %4 = load %"class.std::__2::codecvt"*, %"class.std::__2::codecvt"** %__cv_3, align 4
  %call4 = call zeroext i1 @_ZNKSt3__27codecvtIcc11__mbstate_tE13always_noconvEv(%"class.std::__2::codecvt"* %4) #3
  %__always_noconv_5 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 16
  %frombool6 = zext i1 %call4 to i8
  store i8 %frombool6, i8* %__always_noconv_5, align 2
  %5 = load i8, i8* %__old_anc, align 1
  %tobool7 = trunc i8 %5 to i1
  %conv = zext i1 %tobool7 to i32
  %__always_noconv_8 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 16
  %6 = load i8, i8* %__always_noconv_8, align 2
  %tobool9 = trunc i8 %6 to i1
  %conv10 = zext i1 %tobool9 to i32
  %cmp = icmp ne i32 %conv, %conv10
  br i1 %cmp, label %if.then, label %if.end46

if.then:                                          ; preds = %entry
  %7 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  call void @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE4setgEPcS4_S4_(%"class.std::__2::basic_streambuf"* %7, i8* null, i8* null, i8* null)
  %8 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  call void @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE4setpEPcS4_(%"class.std::__2::basic_streambuf"* %8, i8* null, i8* null)
  %__always_noconv_11 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 16
  %9 = load i8, i8* %__always_noconv_11, align 2
  %tobool12 = trunc i8 %9 to i1
  br i1 %tobool12, label %if.then13, label %if.else

if.then13:                                        ; preds = %if.then
  %__owns_eb_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 14
  %10 = load i8, i8* %__owns_eb_, align 4
  %tobool14 = trunc i8 %10 to i1
  br i1 %tobool14, label %if.then15, label %if.end

if.then15:                                        ; preds = %if.then13
  %__extbuf_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 1
  %11 = load i8*, i8** %__extbuf_, align 4
  %isnull = icmp eq i8* %11, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %if.then15
  call void @_ZdaPv(i8* %11) #11
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %if.then15
  br label %if.end

if.end:                                           ; preds = %delete.end, %if.then13
  %__owns_ib_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 15
  %12 = load i8, i8* %__owns_ib_, align 1
  %tobool16 = trunc i8 %12 to i1
  %__owns_eb_17 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 14
  %frombool18 = zext i1 %tobool16 to i8
  store i8 %frombool18, i8* %__owns_eb_17, align 4
  %__ibs_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 7
  %13 = load i32, i32* %__ibs_, align 4
  %__ebs_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 5
  store i32 %13, i32* %__ebs_, align 4
  %__intbuf_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 6
  %14 = load i8*, i8** %__intbuf_, align 4
  %__extbuf_19 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 1
  store i8* %14, i8** %__extbuf_19, align 4
  %__ibs_20 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 7
  store i32 0, i32* %__ibs_20, align 4
  %__intbuf_21 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 6
  store i8* null, i8** %__intbuf_21, align 4
  %__owns_ib_22 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 15
  store i8 0, i8* %__owns_ib_22, align 1
  br label %if.end45

if.else:                                          ; preds = %if.then
  %__owns_eb_23 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 14
  %15 = load i8, i8* %__owns_eb_23, align 4
  %tobool24 = trunc i8 %15 to i1
  br i1 %tobool24, label %if.else37, label %land.lhs.true

land.lhs.true:                                    ; preds = %if.else
  %__extbuf_25 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 1
  %16 = load i8*, i8** %__extbuf_25, align 4
  %__extbuf_min_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 4
  %arraydecay = getelementptr inbounds [8 x i8], [8 x i8]* %__extbuf_min_, i32 0, i32 0
  %cmp26 = icmp ne i8* %16, %arraydecay
  br i1 %cmp26, label %if.then27, label %if.else37

if.then27:                                        ; preds = %land.lhs.true
  %__ebs_28 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 5
  %17 = load i32, i32* %__ebs_28, align 4
  %__ibs_29 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 7
  store i32 %17, i32* %__ibs_29, align 4
  %__extbuf_30 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 1
  %18 = load i8*, i8** %__extbuf_30, align 4
  %__intbuf_31 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 6
  store i8* %18, i8** %__intbuf_31, align 4
  %__owns_ib_32 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 15
  store i8 0, i8* %__owns_ib_32, align 1
  %__ebs_33 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 5
  %19 = load i32, i32* %__ebs_33, align 4
  %call34 = call noalias nonnull i8* @_Znam(i32 %19) #12
  %__extbuf_35 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 1
  store i8* %call34, i8** %__extbuf_35, align 4
  %__owns_eb_36 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 14
  store i8 1, i8* %__owns_eb_36, align 4
  br label %if.end44

if.else37:                                        ; preds = %land.lhs.true, %if.else
  %__ebs_38 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 5
  %20 = load i32, i32* %__ebs_38, align 4
  %__ibs_39 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 7
  store i32 %20, i32* %__ibs_39, align 4
  %__ibs_40 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 7
  %21 = load i32, i32* %__ibs_40, align 4
  %call41 = call noalias nonnull i8* @_Znam(i32 %21) #12
  %__intbuf_42 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 6
  store i8* %call41, i8** %__intbuf_42, align 4
  %__owns_ib_43 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 15
  store i8 1, i8* %__owns_ib_43, align 1
  br label %if.end44

if.end44:                                         ; preds = %if.else37, %if.then27
  br label %if.end45

if.end45:                                         ; preds = %if.end44, %if.end
  br label %if.end46

if.end46:                                         ; preds = %if.end45, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::basic_streambuf"* @_ZNSt3__213basic_filebufIcNS_11char_traitsIcEEE6setbufEPcl(%"class.std::__2::basic_filebuf"* %this, i8* %__s, i32 %__n) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_filebuf"*, align 4
  %__s.addr = alloca i8*, align 4
  %__n.addr = alloca i32, align 4
  %ref.tmp = alloca i32, align 4
  store %"class.std::__2::basic_filebuf"* %this, %"class.std::__2::basic_filebuf"** %this.addr, align 4
  store i8* %__s, i8** %__s.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::basic_filebuf"*, %"class.std::__2::basic_filebuf"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  call void @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE4setgEPcS4_S4_(%"class.std::__2::basic_streambuf"* %0, i8* null, i8* null, i8* null)
  %1 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  call void @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE4setpEPcS4_(%"class.std::__2::basic_streambuf"* %1, i8* null, i8* null)
  %__owns_eb_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 14
  %2 = load i8, i8* %__owns_eb_, align 4
  %tobool = trunc i8 %2 to i1
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__extbuf_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 1
  %3 = load i8*, i8** %__extbuf_, align 4
  %isnull = icmp eq i8* %3, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %if.then
  call void @_ZdaPv(i8* %3) #11
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %if.then
  br label %if.end

if.end:                                           ; preds = %delete.end, %entry
  %__owns_ib_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 15
  %4 = load i8, i8* %__owns_ib_, align 1
  %tobool2 = trunc i8 %4 to i1
  br i1 %tobool2, label %if.then3, label %if.end7

if.then3:                                         ; preds = %if.end
  %__intbuf_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 6
  %5 = load i8*, i8** %__intbuf_, align 4
  %isnull4 = icmp eq i8* %5, null
  br i1 %isnull4, label %delete.end6, label %delete.notnull5

delete.notnull5:                                  ; preds = %if.then3
  call void @_ZdaPv(i8* %5) #11
  br label %delete.end6

delete.end6:                                      ; preds = %delete.notnull5, %if.then3
  br label %if.end7

if.end7:                                          ; preds = %delete.end6, %if.end
  %6 = load i32, i32* %__n.addr, align 4
  %__ebs_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 5
  store i32 %6, i32* %__ebs_, align 4
  %__ebs_8 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 5
  %7 = load i32, i32* %__ebs_8, align 4
  %cmp = icmp ugt i32 %7, 8
  br i1 %cmp, label %if.then9, label %if.else19

if.then9:                                         ; preds = %if.end7
  %__always_noconv_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 16
  %8 = load i8, i8* %__always_noconv_, align 2
  %tobool10 = trunc i8 %8 to i1
  br i1 %tobool10, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %if.then9
  %9 = load i8*, i8** %__s.addr, align 4
  %tobool11 = icmp ne i8* %9, null
  br i1 %tobool11, label %if.then12, label %if.else

if.then12:                                        ; preds = %land.lhs.true
  %10 = load i8*, i8** %__s.addr, align 4
  %__extbuf_13 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 1
  store i8* %10, i8** %__extbuf_13, align 4
  %__owns_eb_14 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 14
  store i8 0, i8* %__owns_eb_14, align 4
  br label %if.end18

if.else:                                          ; preds = %land.lhs.true, %if.then9
  %__ebs_15 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 5
  %11 = load i32, i32* %__ebs_15, align 4
  %call = call noalias nonnull i8* @_Znam(i32 %11) #12
  %__extbuf_16 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 1
  store i8* %call, i8** %__extbuf_16, align 4
  %__owns_eb_17 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 14
  store i8 1, i8* %__owns_eb_17, align 4
  br label %if.end18

if.end18:                                         ; preds = %if.else, %if.then12
  br label %if.end23

if.else19:                                        ; preds = %if.end7
  %__extbuf_min_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 4
  %arraydecay = getelementptr inbounds [8 x i8], [8 x i8]* %__extbuf_min_, i32 0, i32 0
  %__extbuf_20 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 1
  store i8* %arraydecay, i8** %__extbuf_20, align 4
  %__ebs_21 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 5
  store i32 8, i32* %__ebs_21, align 4
  %__owns_eb_22 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 14
  store i8 0, i8* %__owns_eb_22, align 4
  br label %if.end23

if.end23:                                         ; preds = %if.else19, %if.end18
  %__always_noconv_24 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 16
  %12 = load i8, i8* %__always_noconv_24, align 2
  %tobool25 = trunc i8 %12 to i1
  br i1 %tobool25, label %if.else41, label %if.then26

if.then26:                                        ; preds = %if.end23
  store i32 8, i32* %ref.tmp, align 4
  %call27 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxIlEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %__n.addr, i32* nonnull align 4 dereferenceable(4) %ref.tmp)
  %13 = load i32, i32* %call27, align 4
  %__ibs_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 7
  store i32 %13, i32* %__ibs_, align 4
  %14 = load i8*, i8** %__s.addr, align 4
  %tobool28 = icmp ne i8* %14, null
  br i1 %tobool28, label %land.lhs.true29, label %if.else35

land.lhs.true29:                                  ; preds = %if.then26
  %__ibs_30 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 7
  %15 = load i32, i32* %__ibs_30, align 4
  %cmp31 = icmp uge i32 %15, 8
  br i1 %cmp31, label %if.then32, label %if.else35

if.then32:                                        ; preds = %land.lhs.true29
  %16 = load i8*, i8** %__s.addr, align 4
  %__intbuf_33 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 6
  store i8* %16, i8** %__intbuf_33, align 4
  %__owns_ib_34 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 15
  store i8 0, i8* %__owns_ib_34, align 1
  br label %if.end40

if.else35:                                        ; preds = %land.lhs.true29, %if.then26
  %__ibs_36 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 7
  %17 = load i32, i32* %__ibs_36, align 4
  %call37 = call noalias nonnull i8* @_Znam(i32 %17) #12
  %__intbuf_38 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 6
  store i8* %call37, i8** %__intbuf_38, align 4
  %__owns_ib_39 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 15
  store i8 1, i8* %__owns_ib_39, align 1
  br label %if.end40

if.end40:                                         ; preds = %if.else35, %if.then32
  br label %if.end45

if.else41:                                        ; preds = %if.end23
  %__ibs_42 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 7
  store i32 0, i32* %__ibs_42, align 4
  %__intbuf_43 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 6
  store i8* null, i8** %__intbuf_43, align 4
  %__owns_ib_44 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 15
  store i8 0, i8* %__owns_ib_44, align 1
  br label %if.end45

if.end45:                                         ; preds = %if.else41, %if.end40
  %18 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  ret %"class.std::__2::basic_streambuf"* %18
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213basic_filebufIcNS_11char_traitsIcEEE7seekoffExNS_8ios_base7seekdirEj(%"class.std::__2::fpos"* noalias sret align 8 %agg.result, %"class.std::__2::basic_filebuf"* %this, i64 %__off, i32 %__way, i32 %0) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_filebuf"*, align 4
  %__off.addr = alloca i64, align 8
  %__way.addr = alloca i32, align 4
  %.addr = alloca i32, align 4
  %__width = alloca i32, align 4
  %__whence = alloca i32, align 4
  %__r = alloca %"class.std::__2::fpos", align 8
  %agg.tmp = alloca %struct.__mbstate_t, align 4
  store %"class.std::__2::basic_filebuf"* %this, %"class.std::__2::basic_filebuf"** %this.addr, align 4
  store i64 %__off, i64* %__off.addr, align 8
  store i32 %__way, i32* %__way.addr, align 4
  store i32 %0, i32* %.addr, align 4
  %this1 = load %"class.std::__2::basic_filebuf"*, %"class.std::__2::basic_filebuf"** %this.addr, align 4
  %__cv_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 9
  %1 = load %"class.std::__2::codecvt"*, %"class.std::__2::codecvt"** %__cv_, align 4
  %tobool = icmp ne %"class.std::__2::codecvt"* %1, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  call void @_ZNSt3__216__throw_bad_castEv() #13
  unreachable

if.end:                                           ; preds = %entry
  %__cv_2 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 9
  %2 = load %"class.std::__2::codecvt"*, %"class.std::__2::codecvt"** %__cv_2, align 4
  %call = call i32 @_ZNKSt3__27codecvtIcc11__mbstate_tE8encodingEv(%"class.std::__2::codecvt"* %2) #3
  store i32 %call, i32* %__width, align 4
  %__file_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 8
  %3 = load %struct._IO_FILE*, %struct._IO_FILE** %__file_, align 4
  %cmp = icmp eq %struct._IO_FILE* %3, null
  br i1 %cmp, label %if.then8, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end
  %4 = load i32, i32* %__width, align 4
  %cmp3 = icmp sle i32 %4, 0
  br i1 %cmp3, label %land.lhs.true, label %lor.lhs.false5

land.lhs.true:                                    ; preds = %lor.lhs.false
  %5 = load i64, i64* %__off.addr, align 8
  %cmp4 = icmp ne i64 %5, 0
  br i1 %cmp4, label %if.then8, label %lor.lhs.false5

lor.lhs.false5:                                   ; preds = %land.lhs.true, %lor.lhs.false
  %6 = bitcast %"class.std::__2::basic_filebuf"* %this1 to i32 (%"class.std::__2::basic_filebuf"*)***
  %vtable = load i32 (%"class.std::__2::basic_filebuf"*)**, i32 (%"class.std::__2::basic_filebuf"*)*** %6, align 4
  %vfn = getelementptr inbounds i32 (%"class.std::__2::basic_filebuf"*)*, i32 (%"class.std::__2::basic_filebuf"*)** %vtable, i64 6
  %7 = load i32 (%"class.std::__2::basic_filebuf"*)*, i32 (%"class.std::__2::basic_filebuf"*)** %vfn, align 4
  %call6 = call i32 %7(%"class.std::__2::basic_filebuf"* %this1)
  %tobool7 = icmp ne i32 %call6, 0
  br i1 %tobool7, label %if.then8, label %if.end10

if.then8:                                         ; preds = %lor.lhs.false5, %land.lhs.true, %if.end
  %call9 = call %"class.std::__2::fpos"* @_ZNSt3__24fposI11__mbstate_tEC2Ex(%"class.std::__2::fpos"* %agg.result, i64 -1)
  br label %return

if.end10:                                         ; preds = %lor.lhs.false5
  %8 = load i32, i32* %__way.addr, align 4
  switch i32 %8, label %sw.default [
    i32 0, label %sw.bb
    i32 1, label %sw.bb11
    i32 2, label %sw.bb12
  ]

sw.bb:                                            ; preds = %if.end10
  store i32 0, i32* %__whence, align 4
  br label %sw.epilog

sw.bb11:                                          ; preds = %if.end10
  store i32 1, i32* %__whence, align 4
  br label %sw.epilog

sw.bb12:                                          ; preds = %if.end10
  store i32 2, i32* %__whence, align 4
  br label %sw.epilog

sw.default:                                       ; preds = %if.end10
  %call13 = call %"class.std::__2::fpos"* @_ZNSt3__24fposI11__mbstate_tEC2Ex(%"class.std::__2::fpos"* %agg.result, i64 -1)
  br label %return

sw.epilog:                                        ; preds = %sw.bb12, %sw.bb11, %sw.bb
  %__file_14 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 8
  %9 = load %struct._IO_FILE*, %struct._IO_FILE** %__file_14, align 4
  %10 = load i32, i32* %__width, align 4
  %cmp15 = icmp sgt i32 %10, 0
  br i1 %cmp15, label %cond.true, label %cond.false

cond.true:                                        ; preds = %sw.epilog
  %11 = load i32, i32* %__width, align 4
  %conv = sext i32 %11 to i64
  %12 = load i64, i64* %__off.addr, align 8
  %mul = mul nsw i64 %conv, %12
  br label %cond.end

cond.false:                                       ; preds = %sw.epilog
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i64 [ %mul, %cond.true ], [ 0, %cond.false ]
  %13 = load i32, i32* %__whence, align 4
  %call16 = call i32 @fseeko(%struct._IO_FILE* %9, i64 %cond, i32 %13)
  %tobool17 = icmp ne i32 %call16, 0
  br i1 %tobool17, label %if.then18, label %if.end20

if.then18:                                        ; preds = %cond.end
  %call19 = call %"class.std::__2::fpos"* @_ZNSt3__24fposI11__mbstate_tEC2Ex(%"class.std::__2::fpos"* %agg.result, i64 -1)
  br label %return

if.end20:                                         ; preds = %cond.end
  %__file_21 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 8
  %14 = load %struct._IO_FILE*, %struct._IO_FILE** %__file_21, align 4
  %call22 = call i64 @ftello(%struct._IO_FILE* %14)
  %call23 = call %"class.std::__2::fpos"* @_ZNSt3__24fposI11__mbstate_tEC2Ex(%"class.std::__2::fpos"* %__r, i64 %call22)
  %__st_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 10
  %15 = bitcast %struct.__mbstate_t* %agg.tmp to i8*
  %16 = bitcast %struct.__mbstate_t* %__st_ to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %15, i8* align 4 %16, i32 8, i1 false)
  call void @_ZNSt3__24fposI11__mbstate_tE5stateES1_(%"class.std::__2::fpos"* %__r, %struct.__mbstate_t* byval(%struct.__mbstate_t) align 4 %agg.tmp)
  %17 = bitcast %"class.std::__2::fpos"* %agg.result to i8*
  %18 = bitcast %"class.std::__2::fpos"* %__r to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %17, i8* align 8 %18, i32 16, i1 false)
  br label %return

return:                                           ; preds = %if.end20, %if.then18, %sw.default, %if.then8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213basic_filebufIcNS_11char_traitsIcEEE7seekposENS_4fposI11__mbstate_tEEj(%"class.std::__2::fpos"* noalias sret align 8 %agg.result, %"class.std::__2::basic_filebuf"* %this, %"class.std::__2::fpos"* byval(%"class.std::__2::fpos") align 8 %__sp, i32 %0) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_filebuf"*, align 4
  %.addr = alloca i32, align 4
  %ref.tmp = alloca %struct.__mbstate_t, align 4
  store %"class.std::__2::basic_filebuf"* %this, %"class.std::__2::basic_filebuf"** %this.addr, align 4
  store i32 %0, i32* %.addr, align 4
  %this1 = load %"class.std::__2::basic_filebuf"*, %"class.std::__2::basic_filebuf"** %this.addr, align 4
  %__file_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 8
  %1 = load %struct._IO_FILE*, %struct._IO_FILE** %__file_, align 4
  %cmp = icmp eq %struct._IO_FILE* %1, null
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %2 = bitcast %"class.std::__2::basic_filebuf"* %this1 to i32 (%"class.std::__2::basic_filebuf"*)***
  %vtable = load i32 (%"class.std::__2::basic_filebuf"*)**, i32 (%"class.std::__2::basic_filebuf"*)*** %2, align 4
  %vfn = getelementptr inbounds i32 (%"class.std::__2::basic_filebuf"*)*, i32 (%"class.std::__2::basic_filebuf"*)** %vtable, i64 6
  %3 = load i32 (%"class.std::__2::basic_filebuf"*)*, i32 (%"class.std::__2::basic_filebuf"*)** %vfn, align 4
  %call = call i32 %3(%"class.std::__2::basic_filebuf"* %this1)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  %call2 = call %"class.std::__2::fpos"* @_ZNSt3__24fposI11__mbstate_tEC2Ex(%"class.std::__2::fpos"* %agg.result, i64 -1)
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  %__file_3 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 8
  %4 = load %struct._IO_FILE*, %struct._IO_FILE** %__file_3, align 4
  %call4 = call i64 @_ZNKSt3__24fposI11__mbstate_tEcvxEv(%"class.std::__2::fpos"* %__sp)
  %call5 = call i32 @fseeko(%struct._IO_FILE* %4, i64 %call4, i32 0)
  %tobool6 = icmp ne i32 %call5, 0
  br i1 %tobool6, label %if.then7, label %if.end9

if.then7:                                         ; preds = %if.end
  %call8 = call %"class.std::__2::fpos"* @_ZNSt3__24fposI11__mbstate_tEC2Ex(%"class.std::__2::fpos"* %agg.result, i64 -1)
  br label %return

if.end9:                                          ; preds = %if.end
  call void @_ZNKSt3__24fposI11__mbstate_tE5stateEv(%struct.__mbstate_t* sret align 4 %ref.tmp, %"class.std::__2::fpos"* %__sp)
  %__st_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 10
  %5 = bitcast %struct.__mbstate_t* %__st_ to i8*
  %6 = bitcast %struct.__mbstate_t* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %5, i8* align 4 %6, i32 8, i1 false)
  %7 = bitcast %"class.std::__2::fpos"* %agg.result to i8*
  %8 = bitcast %"class.std::__2::fpos"* %__sp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %7, i8* align 8 %8, i32 16, i1 false)
  br label %return

return:                                           ; preds = %if.end9, %if.then7, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__213basic_filebufIcNS_11char_traitsIcEEE4syncEv(%"class.std::__2::basic_filebuf"* %this) unnamed_addr #1 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %"class.std::__2::basic_filebuf"*, align 4
  %__r = alloca i32, align 4
  %__extbe = alloca i8*, align 4
  %__nmemb = alloca i32, align 4
  %__c = alloca i64, align 8
  %__state = alloca %struct.__mbstate_t, align 4
  %__update_st = alloca i8, align 1
  %__width = alloca i32, align 4
  %__off = alloca i32, align 4
  store %"class.std::__2::basic_filebuf"* %this, %"class.std::__2::basic_filebuf"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_filebuf"*, %"class.std::__2::basic_filebuf"** %this.addr, align 4
  %__file_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 8
  %0 = load %struct._IO_FILE*, %struct._IO_FILE** %__file_, align 4
  %cmp = icmp eq %struct._IO_FILE* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %__cv_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 9
  %1 = load %"class.std::__2::codecvt"*, %"class.std::__2::codecvt"** %__cv_, align 4
  %tobool = icmp ne %"class.std::__2::codecvt"* %1, null
  br i1 %tobool, label %if.end3, label %if.then2

if.then2:                                         ; preds = %if.end
  call void @_ZNSt3__216__throw_bad_castEv() #13
  unreachable

if.end3:                                          ; preds = %if.end
  %__cm_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 13
  %2 = load i32, i32* %__cm_, align 4
  %and = and i32 %2, 16
  %tobool4 = icmp ne i32 %and, 0
  br i1 %tobool4, label %if.then5, label %if.else

if.then5:                                         ; preds = %if.end3
  %3 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE4pptrEv(%"class.std::__2::basic_streambuf"* %3)
  %4 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call6 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE5pbaseEv(%"class.std::__2::basic_streambuf"* %4)
  %cmp7 = icmp ne i8* %call, %call6
  br i1 %cmp7, label %if.then8, label %if.end15

if.then8:                                         ; preds = %if.then5
  %call9 = call i32 @_ZNSt3__211char_traitsIcE3eofEv() #3
  %5 = bitcast %"class.std::__2::basic_filebuf"* %this1 to i32 (%"class.std::__2::basic_filebuf"*, i32)***
  %vtable = load i32 (%"class.std::__2::basic_filebuf"*, i32)**, i32 (%"class.std::__2::basic_filebuf"*, i32)*** %5, align 4
  %vfn = getelementptr inbounds i32 (%"class.std::__2::basic_filebuf"*, i32)*, i32 (%"class.std::__2::basic_filebuf"*, i32)** %vtable, i64 13
  %6 = load i32 (%"class.std::__2::basic_filebuf"*, i32)*, i32 (%"class.std::__2::basic_filebuf"*, i32)** %vfn, align 4
  %call10 = call i32 %6(%"class.std::__2::basic_filebuf"* %this1, i32 %call9)
  %call11 = call i32 @_ZNSt3__211char_traitsIcE3eofEv() #3
  %cmp12 = icmp eq i32 %call10, %call11
  br i1 %cmp12, label %if.then13, label %if.end14

if.then13:                                        ; preds = %if.then8
  store i32 -1, i32* %retval, align 4
  br label %return

if.end14:                                         ; preds = %if.then8
  br label %if.end15

if.end15:                                         ; preds = %if.end14, %if.then5
  br label %do.body

do.body:                                          ; preds = %do.cond, %if.end15
  %__cv_16 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 9
  %7 = load %"class.std::__2::codecvt"*, %"class.std::__2::codecvt"** %__cv_16, align 4
  %__st_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 10
  %__extbuf_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 1
  %8 = load i8*, i8** %__extbuf_, align 4
  %__extbuf_17 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 1
  %9 = load i8*, i8** %__extbuf_17, align 4
  %__ebs_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 5
  %10 = load i32, i32* %__ebs_, align 4
  %add.ptr = getelementptr inbounds i8, i8* %9, i32 %10
  %call18 = call i32 @_ZNKSt3__27codecvtIcc11__mbstate_tE7unshiftERS1_PcS4_RS4_(%"class.std::__2::codecvt"* %7, %struct.__mbstate_t* nonnull align 4 dereferenceable(8) %__st_, i8* %8, i8* %add.ptr, i8** nonnull align 4 dereferenceable(4) %__extbe)
  store i32 %call18, i32* %__r, align 4
  %11 = load i8*, i8** %__extbe, align 4
  %__extbuf_19 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 1
  %12 = load i8*, i8** %__extbuf_19, align 4
  %sub.ptr.lhs.cast = ptrtoint i8* %11 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %12 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  store i32 %sub.ptr.sub, i32* %__nmemb, align 4
  %__extbuf_20 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 1
  %13 = load i8*, i8** %__extbuf_20, align 4
  %14 = load i32, i32* %__nmemb, align 4
  %__file_21 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 8
  %15 = load %struct._IO_FILE*, %struct._IO_FILE** %__file_21, align 4
  %call22 = call i32 @fwrite(i8* %13, i32 1, i32 %14, %struct._IO_FILE* %15)
  %16 = load i32, i32* %__nmemb, align 4
  %cmp23 = icmp ne i32 %call22, %16
  br i1 %cmp23, label %if.then24, label %if.end25

if.then24:                                        ; preds = %do.body
  store i32 -1, i32* %retval, align 4
  br label %return

if.end25:                                         ; preds = %do.body
  br label %do.cond

do.cond:                                          ; preds = %if.end25
  %17 = load i32, i32* %__r, align 4
  %cmp26 = icmp eq i32 %17, 1
  br i1 %cmp26, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond
  %18 = load i32, i32* %__r, align 4
  %cmp27 = icmp eq i32 %18, 2
  br i1 %cmp27, label %if.then28, label %if.end29

if.then28:                                        ; preds = %do.end
  store i32 -1, i32* %retval, align 4
  br label %return

if.end29:                                         ; preds = %do.end
  %__file_30 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 8
  %19 = load %struct._IO_FILE*, %struct._IO_FILE** %__file_30, align 4
  %call31 = call i32 @fflush(%struct._IO_FILE* %19)
  %tobool32 = icmp ne i32 %call31, 0
  br i1 %tobool32, label %if.then33, label %if.end34

if.then33:                                        ; preds = %if.end29
  store i32 -1, i32* %retval, align 4
  br label %return

if.end34:                                         ; preds = %if.end29
  br label %if.end100

if.else:                                          ; preds = %if.end3
  %__cm_35 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 13
  %20 = load i32, i32* %__cm_35, align 4
  %and36 = and i32 %20, 8
  %tobool37 = icmp ne i32 %and36, 0
  br i1 %tobool37, label %if.then38, label %if.end99

if.then38:                                        ; preds = %if.else
  %__st_last_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 11
  %21 = bitcast %struct.__mbstate_t* %__state to i8*
  %22 = bitcast %struct.__mbstate_t* %__st_last_ to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %21, i8* align 4 %22, i32 8, i1 false)
  store i8 0, i8* %__update_st, align 1
  %__always_noconv_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 16
  %23 = load i8, i8* %__always_noconv_, align 2
  %tobool39 = trunc i8 %23 to i1
  br i1 %tobool39, label %if.then40, label %if.else46

if.then40:                                        ; preds = %if.then38
  %24 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call41 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE5egptrEv(%"class.std::__2::basic_streambuf"* %24)
  %25 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call42 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE4gptrEv(%"class.std::__2::basic_streambuf"* %25)
  %sub.ptr.lhs.cast43 = ptrtoint i8* %call41 to i32
  %sub.ptr.rhs.cast44 = ptrtoint i8* %call42 to i32
  %sub.ptr.sub45 = sub i32 %sub.ptr.lhs.cast43, %sub.ptr.rhs.cast44
  %conv = sext i32 %sub.ptr.sub45 to i64
  store i64 %conv, i64* %__c, align 8
  br label %if.end84

if.else46:                                        ; preds = %if.then38
  %__cv_47 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 9
  %26 = load %"class.std::__2::codecvt"*, %"class.std::__2::codecvt"** %__cv_47, align 4
  %call48 = call i32 @_ZNKSt3__27codecvtIcc11__mbstate_tE8encodingEv(%"class.std::__2::codecvt"* %26) #3
  store i32 %call48, i32* %__width, align 4
  %__extbufend_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 3
  %27 = load i8*, i8** %__extbufend_, align 4
  %__extbufnext_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 2
  %28 = load i8*, i8** %__extbufnext_, align 4
  %sub.ptr.lhs.cast49 = ptrtoint i8* %27 to i32
  %sub.ptr.rhs.cast50 = ptrtoint i8* %28 to i32
  %sub.ptr.sub51 = sub i32 %sub.ptr.lhs.cast49, %sub.ptr.rhs.cast50
  %conv52 = sext i32 %sub.ptr.sub51 to i64
  store i64 %conv52, i64* %__c, align 8
  %29 = load i32, i32* %__width, align 4
  %cmp53 = icmp sgt i32 %29, 0
  br i1 %cmp53, label %if.then54, label %if.else61

if.then54:                                        ; preds = %if.else46
  %30 = load i32, i32* %__width, align 4
  %31 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call55 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE5egptrEv(%"class.std::__2::basic_streambuf"* %31)
  %32 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call56 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE4gptrEv(%"class.std::__2::basic_streambuf"* %32)
  %sub.ptr.lhs.cast57 = ptrtoint i8* %call55 to i32
  %sub.ptr.rhs.cast58 = ptrtoint i8* %call56 to i32
  %sub.ptr.sub59 = sub i32 %sub.ptr.lhs.cast57, %sub.ptr.rhs.cast58
  %mul = mul nsw i32 %30, %sub.ptr.sub59
  %conv60 = sext i32 %mul to i64
  %33 = load i64, i64* %__c, align 8
  %add = add nsw i64 %33, %conv60
  store i64 %add, i64* %__c, align 8
  br label %if.end83

if.else61:                                        ; preds = %if.else46
  %34 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call62 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE4gptrEv(%"class.std::__2::basic_streambuf"* %34)
  %35 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call63 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE5egptrEv(%"class.std::__2::basic_streambuf"* %35)
  %cmp64 = icmp ne i8* %call62, %call63
  br i1 %cmp64, label %if.then65, label %if.end82

if.then65:                                        ; preds = %if.else61
  %__cv_66 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 9
  %36 = load %"class.std::__2::codecvt"*, %"class.std::__2::codecvt"** %__cv_66, align 4
  %__extbuf_67 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 1
  %37 = load i8*, i8** %__extbuf_67, align 4
  %__extbufnext_68 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 2
  %38 = load i8*, i8** %__extbufnext_68, align 4
  %39 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call69 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE4gptrEv(%"class.std::__2::basic_streambuf"* %39)
  %40 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call70 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE5ebackEv(%"class.std::__2::basic_streambuf"* %40)
  %sub.ptr.lhs.cast71 = ptrtoint i8* %call69 to i32
  %sub.ptr.rhs.cast72 = ptrtoint i8* %call70 to i32
  %sub.ptr.sub73 = sub i32 %sub.ptr.lhs.cast71, %sub.ptr.rhs.cast72
  %call74 = call i32 @_ZNKSt3__27codecvtIcc11__mbstate_tE6lengthERS1_PKcS5_m(%"class.std::__2::codecvt"* %36, %struct.__mbstate_t* nonnull align 4 dereferenceable(8) %__state, i8* %37, i8* %38, i32 %sub.ptr.sub73)
  store i32 %call74, i32* %__off, align 4
  %__extbufnext_75 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 2
  %41 = load i8*, i8** %__extbufnext_75, align 4
  %__extbuf_76 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 1
  %42 = load i8*, i8** %__extbuf_76, align 4
  %sub.ptr.lhs.cast77 = ptrtoint i8* %41 to i32
  %sub.ptr.rhs.cast78 = ptrtoint i8* %42 to i32
  %sub.ptr.sub79 = sub i32 %sub.ptr.lhs.cast77, %sub.ptr.rhs.cast78
  %43 = load i32, i32* %__off, align 4
  %sub = sub nsw i32 %sub.ptr.sub79, %43
  %conv80 = sext i32 %sub to i64
  %44 = load i64, i64* %__c, align 8
  %add81 = add nsw i64 %44, %conv80
  store i64 %add81, i64* %__c, align 8
  store i8 1, i8* %__update_st, align 1
  br label %if.end82

if.end82:                                         ; preds = %if.then65, %if.else61
  br label %if.end83

if.end83:                                         ; preds = %if.end82, %if.then54
  br label %if.end84

if.end84:                                         ; preds = %if.end83, %if.then40
  %__file_85 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 8
  %45 = load %struct._IO_FILE*, %struct._IO_FILE** %__file_85, align 4
  %46 = load i64, i64* %__c, align 8
  %sub86 = sub nsw i64 0, %46
  %call87 = call i32 @fseeko(%struct._IO_FILE* %45, i64 %sub86, i32 1)
  %tobool88 = icmp ne i32 %call87, 0
  br i1 %tobool88, label %if.then89, label %if.end90

if.then89:                                        ; preds = %if.end84
  store i32 -1, i32* %retval, align 4
  br label %return

if.end90:                                         ; preds = %if.end84
  %47 = load i8, i8* %__update_st, align 1
  %tobool91 = trunc i8 %47 to i1
  br i1 %tobool91, label %if.then92, label %if.end94

if.then92:                                        ; preds = %if.end90
  %__st_93 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 10
  %48 = bitcast %struct.__mbstate_t* %__st_93 to i8*
  %49 = bitcast %struct.__mbstate_t* %__state to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %48, i8* align 4 %49, i32 8, i1 false)
  br label %if.end94

if.end94:                                         ; preds = %if.then92, %if.end90
  %__extbuf_95 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 1
  %50 = load i8*, i8** %__extbuf_95, align 4
  %__extbufend_96 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 3
  store i8* %50, i8** %__extbufend_96, align 4
  %__extbufnext_97 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 2
  store i8* %50, i8** %__extbufnext_97, align 4
  %51 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  call void @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE4setgEPcS4_S4_(%"class.std::__2::basic_streambuf"* %51, i8* null, i8* null, i8* null)
  %__cm_98 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 13
  store i32 0, i32* %__cm_98, align 4
  br label %if.end99

if.end99:                                         ; preds = %if.end94, %if.else
  br label %if.end100

if.end100:                                        ; preds = %if.end99, %if.end34
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end100, %if.then89, %if.then33, %if.then28, %if.then24, %if.then13, %if.then
  %52 = load i32, i32* %retval, align 4
  ret i32 %52
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__213basic_filebufIcNS_11char_traitsIcEEE9underflowEv(%"class.std::__2::basic_filebuf"* %this) unnamed_addr #1 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %"class.std::__2::basic_filebuf"*, align 4
  %__initial = alloca i8, align 1
  %__1buf = alloca i8, align 1
  %__unget_sz = alloca i32, align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp10 = alloca i32, align 4
  %__c = alloca i32, align 4
  %__nmemb = alloca i32, align 4
  %__nmemb68 = alloca i32, align 4
  %ref.tmp69 = alloca i32, align 4
  %ref.tmp71 = alloca i32, align 4
  %__r = alloca i32, align 4
  %__nr = alloca i32, align 4
  %__inext = alloca i8*, align 4
  store %"class.std::__2::basic_filebuf"* %this, %"class.std::__2::basic_filebuf"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_filebuf"*, %"class.std::__2::basic_filebuf"** %this.addr, align 4
  %__file_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 8
  %0 = load %struct._IO_FILE*, %struct._IO_FILE** %__file_, align 4
  %cmp = icmp eq %struct._IO_FILE* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call = call i32 @_ZNSt3__211char_traitsIcE3eofEv() #3
  store i32 %call, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %call2 = call zeroext i1 @_ZNSt3__213basic_filebufIcNS_11char_traitsIcEEE11__read_modeEv(%"class.std::__2::basic_filebuf"* %this1)
  %frombool = zext i1 %call2 to i8
  store i8 %frombool, i8* %__initial, align 1
  %1 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call3 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE4gptrEv(%"class.std::__2::basic_streambuf"* %1)
  %cmp4 = icmp eq i8* %call3, null
  br i1 %cmp4, label %if.then5, label %if.end7

if.then5:                                         ; preds = %if.end
  %2 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %add.ptr = getelementptr inbounds i8, i8* %__1buf, i32 1
  %add.ptr6 = getelementptr inbounds i8, i8* %__1buf, i32 1
  call void @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE4setgEPcS4_S4_(%"class.std::__2::basic_streambuf"* %2, i8* %__1buf, i8* %add.ptr, i8* %add.ptr6)
  br label %if.end7

if.end7:                                          ; preds = %if.then5, %if.end
  %3 = load i8, i8* %__initial, align 1
  %tobool = trunc i8 %3 to i1
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end7
  br label %cond.end

cond.false:                                       ; preds = %if.end7
  %4 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call8 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE5egptrEv(%"class.std::__2::basic_streambuf"* %4)
  %5 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call9 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE5ebackEv(%"class.std::__2::basic_streambuf"* %5)
  %sub.ptr.lhs.cast = ptrtoint i8* %call8 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %call9 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %div = sdiv i32 %sub.ptr.sub, 2
  store i32 %div, i32* %ref.tmp, align 4
  store i32 4, i32* %ref.tmp10, align 4
  %call11 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %ref.tmp10)
  %6 = load i32, i32* %call11, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ 0, %cond.true ], [ %6, %cond.false ]
  store i32 %cond, i32* %__unget_sz, align 4
  %call12 = call i32 @_ZNSt3__211char_traitsIcE3eofEv() #3
  store i32 %call12, i32* %__c, align 4
  %7 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call13 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE4gptrEv(%"class.std::__2::basic_streambuf"* %7)
  %8 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call14 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE5egptrEv(%"class.std::__2::basic_streambuf"* %8)
  %cmp15 = icmp eq i8* %call13, %call14
  br i1 %cmp15, label %if.then16, label %if.else121

if.then16:                                        ; preds = %cond.end
  %9 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call17 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE5ebackEv(%"class.std::__2::basic_streambuf"* %9)
  %10 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call18 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE5egptrEv(%"class.std::__2::basic_streambuf"* %10)
  %11 = load i32, i32* %__unget_sz, align 4
  %idx.neg = sub i32 0, %11
  %add.ptr19 = getelementptr inbounds i8, i8* %call18, i32 %idx.neg
  %12 = load i32, i32* %__unget_sz, align 4
  %mul = mul i32 %12, 1
  call void @llvm.memmove.p0i8.p0i8.i32(i8* align 1 %call17, i8* align 1 %add.ptr19, i32 %mul, i1 false)
  %__always_noconv_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 16
  %13 = load i8, i8* %__always_noconv_, align 2
  %tobool20 = trunc i8 %13 to i1
  br i1 %tobool20, label %if.then21, label %if.else

if.then21:                                        ; preds = %if.then16
  %14 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call22 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE5egptrEv(%"class.std::__2::basic_streambuf"* %14)
  %15 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call23 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE5ebackEv(%"class.std::__2::basic_streambuf"* %15)
  %sub.ptr.lhs.cast24 = ptrtoint i8* %call22 to i32
  %sub.ptr.rhs.cast25 = ptrtoint i8* %call23 to i32
  %sub.ptr.sub26 = sub i32 %sub.ptr.lhs.cast24, %sub.ptr.rhs.cast25
  %16 = load i32, i32* %__unget_sz, align 4
  %sub = sub i32 %sub.ptr.sub26, %16
  store i32 %sub, i32* %__nmemb, align 4
  %17 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call27 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE5ebackEv(%"class.std::__2::basic_streambuf"* %17)
  %18 = load i32, i32* %__unget_sz, align 4
  %add.ptr28 = getelementptr inbounds i8, i8* %call27, i32 %18
  %19 = load i32, i32* %__nmemb, align 4
  %__file_29 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 8
  %20 = load %struct._IO_FILE*, %struct._IO_FILE** %__file_29, align 4
  %call30 = call i32 @fread(i8* %add.ptr28, i32 1, i32 %19, %struct._IO_FILE* %20)
  store i32 %call30, i32* %__nmemb, align 4
  %21 = load i32, i32* %__nmemb, align 4
  %cmp31 = icmp ne i32 %21, 0
  br i1 %cmp31, label %if.then32, label %if.end41

if.then32:                                        ; preds = %if.then21
  %22 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %23 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call33 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE5ebackEv(%"class.std::__2::basic_streambuf"* %23)
  %24 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call34 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE5ebackEv(%"class.std::__2::basic_streambuf"* %24)
  %25 = load i32, i32* %__unget_sz, align 4
  %add.ptr35 = getelementptr inbounds i8, i8* %call34, i32 %25
  %26 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call36 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE5ebackEv(%"class.std::__2::basic_streambuf"* %26)
  %27 = load i32, i32* %__unget_sz, align 4
  %add.ptr37 = getelementptr inbounds i8, i8* %call36, i32 %27
  %28 = load i32, i32* %__nmemb, align 4
  %add.ptr38 = getelementptr inbounds i8, i8* %add.ptr37, i32 %28
  call void @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE4setgEPcS4_S4_(%"class.std::__2::basic_streambuf"* %22, i8* %call33, i8* %add.ptr35, i8* %add.ptr38)
  %29 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call39 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE4gptrEv(%"class.std::__2::basic_streambuf"* %29)
  %30 = load i8, i8* %call39, align 1
  %call40 = call i32 @_ZNSt3__211char_traitsIcE11to_int_typeEc(i8 signext %30) #3
  store i32 %call40, i32* %__c, align 4
  br label %if.end41

if.end41:                                         ; preds = %if.then32, %if.then21
  br label %if.end120

if.else:                                          ; preds = %if.then16
  %__extbufend_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 3
  %31 = load i8*, i8** %__extbufend_, align 4
  %__extbufnext_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 2
  %32 = load i8*, i8** %__extbufnext_, align 4
  %cmp42 = icmp ne i8* %31, %32
  br i1 %cmp42, label %if.then43, label %if.end50

if.then43:                                        ; preds = %if.else
  %__extbuf_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 1
  %33 = load i8*, i8** %__extbuf_, align 4
  %__extbufnext_44 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 2
  %34 = load i8*, i8** %__extbufnext_44, align 4
  %__extbufend_45 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 3
  %35 = load i8*, i8** %__extbufend_45, align 4
  %__extbufnext_46 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 2
  %36 = load i8*, i8** %__extbufnext_46, align 4
  %sub.ptr.lhs.cast47 = ptrtoint i8* %35 to i32
  %sub.ptr.rhs.cast48 = ptrtoint i8* %36 to i32
  %sub.ptr.sub49 = sub i32 %sub.ptr.lhs.cast47, %sub.ptr.rhs.cast48
  call void @llvm.memmove.p0i8.p0i8.i32(i8* align 1 %33, i8* align 1 %34, i32 %sub.ptr.sub49, i1 false)
  br label %if.end50

if.end50:                                         ; preds = %if.then43, %if.else
  %__extbuf_51 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 1
  %37 = load i8*, i8** %__extbuf_51, align 4
  %__extbufend_52 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 3
  %38 = load i8*, i8** %__extbufend_52, align 4
  %__extbufnext_53 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 2
  %39 = load i8*, i8** %__extbufnext_53, align 4
  %sub.ptr.lhs.cast54 = ptrtoint i8* %38 to i32
  %sub.ptr.rhs.cast55 = ptrtoint i8* %39 to i32
  %sub.ptr.sub56 = sub i32 %sub.ptr.lhs.cast54, %sub.ptr.rhs.cast55
  %add.ptr57 = getelementptr inbounds i8, i8* %37, i32 %sub.ptr.sub56
  %__extbufnext_58 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 2
  store i8* %add.ptr57, i8** %__extbufnext_58, align 4
  %__extbuf_59 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 1
  %40 = load i8*, i8** %__extbuf_59, align 4
  %__extbuf_60 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 1
  %41 = load i8*, i8** %__extbuf_60, align 4
  %__extbuf_min_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 4
  %arraydecay = getelementptr inbounds [8 x i8], [8 x i8]* %__extbuf_min_, i32 0, i32 0
  %cmp61 = icmp eq i8* %41, %arraydecay
  br i1 %cmp61, label %cond.true62, label %cond.false63

cond.true62:                                      ; preds = %if.end50
  br label %cond.end64

cond.false63:                                     ; preds = %if.end50
  %__ebs_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 5
  %42 = load i32, i32* %__ebs_, align 4
  br label %cond.end64

cond.end64:                                       ; preds = %cond.false63, %cond.true62
  %cond65 = phi i32 [ 8, %cond.true62 ], [ %42, %cond.false63 ]
  %add.ptr66 = getelementptr inbounds i8, i8* %40, i32 %cond65
  %__extbufend_67 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 3
  store i8* %add.ptr66, i8** %__extbufend_67, align 4
  %__ibs_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 7
  %43 = load i32, i32* %__ibs_, align 4
  %44 = load i32, i32* %__unget_sz, align 4
  %sub70 = sub i32 %43, %44
  store i32 %sub70, i32* %ref.tmp69, align 4
  %__extbufend_72 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 3
  %45 = load i8*, i8** %__extbufend_72, align 4
  %__extbufnext_73 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 2
  %46 = load i8*, i8** %__extbufnext_73, align 4
  %sub.ptr.lhs.cast74 = ptrtoint i8* %45 to i32
  %sub.ptr.rhs.cast75 = ptrtoint i8* %46 to i32
  %sub.ptr.sub76 = sub i32 %sub.ptr.lhs.cast74, %sub.ptr.rhs.cast75
  store i32 %sub.ptr.sub76, i32* %ref.tmp71, align 4
  %call77 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp69, i32* nonnull align 4 dereferenceable(4) %ref.tmp71)
  %47 = load i32, i32* %call77, align 4
  store i32 %47, i32* %__nmemb68, align 4
  %__st_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 10
  %__st_last_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 11
  %48 = bitcast %struct.__mbstate_t* %__st_last_ to i8*
  %49 = bitcast %struct.__mbstate_t* %__st_ to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %48, i8* align 4 %49, i32 8, i1 false)
  %__extbufnext_78 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 2
  %50 = load i8*, i8** %__extbufnext_78, align 4
  %51 = load i32, i32* %__nmemb68, align 4
  %__file_79 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 8
  %52 = load %struct._IO_FILE*, %struct._IO_FILE** %__file_79, align 4
  %call80 = call i32 @fread(i8* %50, i32 1, i32 %51, %struct._IO_FILE* %52)
  store i32 %call80, i32* %__nr, align 4
  %53 = load i32, i32* %__nr, align 4
  %cmp81 = icmp ne i32 %53, 0
  br i1 %cmp81, label %if.then82, label %if.end119

if.then82:                                        ; preds = %cond.end64
  %__cv_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 9
  %54 = load %"class.std::__2::codecvt"*, %"class.std::__2::codecvt"** %__cv_, align 4
  %tobool83 = icmp ne %"class.std::__2::codecvt"* %54, null
  br i1 %tobool83, label %if.end85, label %if.then84

if.then84:                                        ; preds = %if.then82
  call void @_ZNSt3__216__throw_bad_castEv() #13
  unreachable

if.end85:                                         ; preds = %if.then82
  %__extbufnext_86 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 2
  %55 = load i8*, i8** %__extbufnext_86, align 4
  %56 = load i32, i32* %__nr, align 4
  %add.ptr87 = getelementptr inbounds i8, i8* %55, i32 %56
  %__extbufend_88 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 3
  store i8* %add.ptr87, i8** %__extbufend_88, align 4
  %__cv_89 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 9
  %57 = load %"class.std::__2::codecvt"*, %"class.std::__2::codecvt"** %__cv_89, align 4
  %__st_90 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 10
  %__extbuf_91 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 1
  %58 = load i8*, i8** %__extbuf_91, align 4
  %__extbufend_92 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 3
  %59 = load i8*, i8** %__extbufend_92, align 4
  %__extbufnext_93 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 2
  %60 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call94 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE5ebackEv(%"class.std::__2::basic_streambuf"* %60)
  %61 = load i32, i32* %__unget_sz, align 4
  %add.ptr95 = getelementptr inbounds i8, i8* %call94, i32 %61
  %62 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call96 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE5ebackEv(%"class.std::__2::basic_streambuf"* %62)
  %__ibs_97 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 7
  %63 = load i32, i32* %__ibs_97, align 4
  %add.ptr98 = getelementptr inbounds i8, i8* %call96, i32 %63
  %call99 = call i32 @_ZNKSt3__27codecvtIcc11__mbstate_tE2inERS1_PKcS5_RS5_PcS7_RS7_(%"class.std::__2::codecvt"* %57, %struct.__mbstate_t* nonnull align 4 dereferenceable(8) %__st_90, i8* %58, i8* %59, i8** nonnull align 4 dereferenceable(4) %__extbufnext_93, i8* %add.ptr95, i8* %add.ptr98, i8** nonnull align 4 dereferenceable(4) %__inext)
  store i32 %call99, i32* %__r, align 4
  %64 = load i32, i32* %__r, align 4
  %cmp100 = icmp eq i32 %64, 3
  br i1 %cmp100, label %if.then101, label %if.else107

if.then101:                                       ; preds = %if.end85
  %65 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %__extbuf_102 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 1
  %66 = load i8*, i8** %__extbuf_102, align 4
  %__extbuf_103 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 1
  %67 = load i8*, i8** %__extbuf_103, align 4
  %__extbufend_104 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 3
  %68 = load i8*, i8** %__extbufend_104, align 4
  call void @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE4setgEPcS4_S4_(%"class.std::__2::basic_streambuf"* %65, i8* %66, i8* %67, i8* %68)
  %69 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call105 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE4gptrEv(%"class.std::__2::basic_streambuf"* %69)
  %70 = load i8, i8* %call105, align 1
  %call106 = call i32 @_ZNSt3__211char_traitsIcE11to_int_typeEc(i8 signext %70) #3
  store i32 %call106, i32* %__c, align 4
  br label %if.end118

if.else107:                                       ; preds = %if.end85
  %71 = load i8*, i8** %__inext, align 4
  %72 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call108 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE5ebackEv(%"class.std::__2::basic_streambuf"* %72)
  %73 = load i32, i32* %__unget_sz, align 4
  %add.ptr109 = getelementptr inbounds i8, i8* %call108, i32 %73
  %cmp110 = icmp ne i8* %71, %add.ptr109
  br i1 %cmp110, label %if.then111, label %if.end117

if.then111:                                       ; preds = %if.else107
  %74 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %75 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call112 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE5ebackEv(%"class.std::__2::basic_streambuf"* %75)
  %76 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call113 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE5ebackEv(%"class.std::__2::basic_streambuf"* %76)
  %77 = load i32, i32* %__unget_sz, align 4
  %add.ptr114 = getelementptr inbounds i8, i8* %call113, i32 %77
  %78 = load i8*, i8** %__inext, align 4
  call void @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE4setgEPcS4_S4_(%"class.std::__2::basic_streambuf"* %74, i8* %call112, i8* %add.ptr114, i8* %78)
  %79 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call115 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE4gptrEv(%"class.std::__2::basic_streambuf"* %79)
  %80 = load i8, i8* %call115, align 1
  %call116 = call i32 @_ZNSt3__211char_traitsIcE11to_int_typeEc(i8 signext %80) #3
  store i32 %call116, i32* %__c, align 4
  br label %if.end117

if.end117:                                        ; preds = %if.then111, %if.else107
  br label %if.end118

if.end118:                                        ; preds = %if.end117, %if.then101
  br label %if.end119

if.end119:                                        ; preds = %if.end118, %cond.end64
  br label %if.end120

if.end120:                                        ; preds = %if.end119, %if.end41
  br label %if.end124

if.else121:                                       ; preds = %cond.end
  %81 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call122 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE4gptrEv(%"class.std::__2::basic_streambuf"* %81)
  %82 = load i8, i8* %call122, align 1
  %call123 = call i32 @_ZNSt3__211char_traitsIcE11to_int_typeEc(i8 signext %82) #3
  store i32 %call123, i32* %__c, align 4
  br label %if.end124

if.end124:                                        ; preds = %if.else121, %if.end120
  %83 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call125 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE5ebackEv(%"class.std::__2::basic_streambuf"* %83)
  %cmp126 = icmp eq i8* %call125, %__1buf
  br i1 %cmp126, label %if.then127, label %if.end128

if.then127:                                       ; preds = %if.end124
  %84 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  call void @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE4setgEPcS4_S4_(%"class.std::__2::basic_streambuf"* %84, i8* null, i8* null, i8* null)
  br label %if.end128

if.end128:                                        ; preds = %if.then127, %if.end124
  %85 = load i32, i32* %__c, align 4
  store i32 %85, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end128, %if.then
  %86 = load i32, i32* %retval, align 4
  ret i32 %86
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__213basic_filebufIcNS_11char_traitsIcEEE9pbackfailEi(%"class.std::__2::basic_filebuf"* %this, i32 %__c) unnamed_addr #1 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %"class.std::__2::basic_filebuf"*, align 4
  %__c.addr = alloca i32, align 4
  store %"class.std::__2::basic_filebuf"* %this, %"class.std::__2::basic_filebuf"** %this.addr, align 4
  store i32 %__c, i32* %__c.addr, align 4
  %this1 = load %"class.std::__2::basic_filebuf"*, %"class.std::__2::basic_filebuf"** %this.addr, align 4
  %__file_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 8
  %0 = load %struct._IO_FILE*, %struct._IO_FILE** %__file_, align 4
  %tobool = icmp ne %struct._IO_FILE* %0, null
  br i1 %tobool, label %land.lhs.true, label %if.end15

land.lhs.true:                                    ; preds = %entry
  %1 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE5ebackEv(%"class.std::__2::basic_streambuf"* %1)
  %2 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call2 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE4gptrEv(%"class.std::__2::basic_streambuf"* %2)
  %cmp = icmp ult i8* %call, %call2
  br i1 %cmp, label %if.then, label %if.end15

if.then:                                          ; preds = %land.lhs.true
  %3 = load i32, i32* %__c.addr, align 4
  %call3 = call i32 @_ZNSt3__211char_traitsIcE3eofEv() #3
  %call4 = call zeroext i1 @_ZNSt3__211char_traitsIcE11eq_int_typeEii(i32 %3, i32 %call3) #3
  br i1 %call4, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.then
  %4 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  call void @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE5gbumpEi(%"class.std::__2::basic_streambuf"* %4, i32 -1)
  %5 = load i32, i32* %__c.addr, align 4
  %call6 = call i32 @_ZNSt3__211char_traitsIcE7not_eofEi(i32 %5) #3
  store i32 %call6, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %if.then
  %__om_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 12
  %6 = load i32, i32* %__om_, align 4
  %and = and i32 %6, 16
  %tobool7 = icmp ne i32 %and, 0
  br i1 %tobool7, label %if.then11, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end
  %7 = load i32, i32* %__c.addr, align 4
  %call8 = call signext i8 @_ZNSt3__211char_traitsIcE12to_char_typeEi(i32 %7) #3
  %8 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call9 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE4gptrEv(%"class.std::__2::basic_streambuf"* %8)
  %arrayidx = getelementptr inbounds i8, i8* %call9, i32 -1
  %9 = load i8, i8* %arrayidx, align 1
  %call10 = call zeroext i1 @_ZNSt3__211char_traitsIcE2eqEcc(i8 signext %call8, i8 signext %9) #3
  br i1 %call10, label %if.then11, label %if.end14

if.then11:                                        ; preds = %lor.lhs.false, %if.end
  %10 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  call void @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE5gbumpEi(%"class.std::__2::basic_streambuf"* %10, i32 -1)
  %11 = load i32, i32* %__c.addr, align 4
  %call12 = call signext i8 @_ZNSt3__211char_traitsIcE12to_char_typeEi(i32 %11) #3
  %12 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call13 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE4gptrEv(%"class.std::__2::basic_streambuf"* %12)
  store i8 %call12, i8* %call13, align 1
  %13 = load i32, i32* %__c.addr, align 4
  store i32 %13, i32* %retval, align 4
  br label %return

if.end14:                                         ; preds = %lor.lhs.false
  br label %if.end15

if.end15:                                         ; preds = %if.end14, %land.lhs.true, %entry
  %call16 = call i32 @_ZNSt3__211char_traitsIcE3eofEv() #3
  store i32 %call16, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end15, %if.then11, %if.then5
  %14 = load i32, i32* %retval, align 4
  ret i32 %14
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__213basic_filebufIcNS_11char_traitsIcEEE8overflowEi(%"class.std::__2::basic_filebuf"* %this, i32 %__c) unnamed_addr #1 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %"class.std::__2::basic_filebuf"*, align 4
  %__c.addr = alloca i32, align 4
  %__1buf = alloca i8, align 1
  %__pb_save = alloca i8*, align 4
  %__epb_save = alloca i8*, align 4
  %__nmemb = alloca i32, align 4
  %__extbe = alloca i8*, align 4
  %__r = alloca i32, align 4
  %__e = alloca i8*, align 4
  %__nmemb45 = alloca i32, align 4
  %__nmemb62 = alloca i32, align 4
  store %"class.std::__2::basic_filebuf"* %this, %"class.std::__2::basic_filebuf"** %this.addr, align 4
  store i32 %__c, i32* %__c.addr, align 4
  %this1 = load %"class.std::__2::basic_filebuf"*, %"class.std::__2::basic_filebuf"** %this.addr, align 4
  %__file_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 8
  %0 = load %struct._IO_FILE*, %struct._IO_FILE** %__file_, align 4
  %cmp = icmp eq %struct._IO_FILE* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call = call i32 @_ZNSt3__211char_traitsIcE3eofEv() #3
  store i32 %call, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  call void @_ZNSt3__213basic_filebufIcNS_11char_traitsIcEEE12__write_modeEv(%"class.std::__2::basic_filebuf"* %this1)
  %1 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call2 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE5pbaseEv(%"class.std::__2::basic_streambuf"* %1)
  store i8* %call2, i8** %__pb_save, align 4
  %2 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call3 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE5epptrEv(%"class.std::__2::basic_streambuf"* %2)
  store i8* %call3, i8** %__epb_save, align 4
  %3 = load i32, i32* %__c.addr, align 4
  %call4 = call i32 @_ZNSt3__211char_traitsIcE3eofEv() #3
  %call5 = call zeroext i1 @_ZNSt3__211char_traitsIcE11eq_int_typeEii(i32 %3, i32 %call4) #3
  br i1 %call5, label %if.end13, label %if.then6

if.then6:                                         ; preds = %if.end
  %4 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call7 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE4pptrEv(%"class.std::__2::basic_streambuf"* %4)
  %cmp8 = icmp eq i8* %call7, null
  br i1 %cmp8, label %if.then9, label %if.end10

if.then9:                                         ; preds = %if.then6
  %5 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %add.ptr = getelementptr inbounds i8, i8* %__1buf, i32 1
  call void @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE4setpEPcS4_(%"class.std::__2::basic_streambuf"* %5, i8* %__1buf, i8* %add.ptr)
  br label %if.end10

if.end10:                                         ; preds = %if.then9, %if.then6
  %6 = load i32, i32* %__c.addr, align 4
  %call11 = call signext i8 @_ZNSt3__211char_traitsIcE12to_char_typeEi(i32 %6) #3
  %7 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call12 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE4pptrEv(%"class.std::__2::basic_streambuf"* %7)
  store i8 %call11, i8* %call12, align 1
  %8 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  call void @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE5pbumpEi(%"class.std::__2::basic_streambuf"* %8, i32 1)
  br label %if.end13

if.end13:                                         ; preds = %if.end10, %if.end
  %9 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call14 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE4pptrEv(%"class.std::__2::basic_streambuf"* %9)
  %10 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call15 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE5pbaseEv(%"class.std::__2::basic_streambuf"* %10)
  %cmp16 = icmp ne i8* %call14, %call15
  br i1 %cmp16, label %if.then17, label %if.end89

if.then17:                                        ; preds = %if.end13
  %__always_noconv_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 16
  %11 = load i8, i8* %__always_noconv_, align 2
  %tobool = trunc i8 %11 to i1
  br i1 %tobool, label %if.then18, label %if.else

if.then18:                                        ; preds = %if.then17
  %12 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call19 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE4pptrEv(%"class.std::__2::basic_streambuf"* %12)
  %13 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call20 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE5pbaseEv(%"class.std::__2::basic_streambuf"* %13)
  %sub.ptr.lhs.cast = ptrtoint i8* %call19 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %call20 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  store i32 %sub.ptr.sub, i32* %__nmemb, align 4
  %14 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call21 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE5pbaseEv(%"class.std::__2::basic_streambuf"* %14)
  %15 = load i32, i32* %__nmemb, align 4
  %__file_22 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 8
  %16 = load %struct._IO_FILE*, %struct._IO_FILE** %__file_22, align 4
  %call23 = call i32 @fwrite(i8* %call21, i32 1, i32 %15, %struct._IO_FILE* %16)
  %17 = load i32, i32* %__nmemb, align 4
  %cmp24 = icmp ne i32 %call23, %17
  br i1 %cmp24, label %if.then25, label %if.end27

if.then25:                                        ; preds = %if.then18
  %call26 = call i32 @_ZNSt3__211char_traitsIcE3eofEv() #3
  store i32 %call26, i32* %retval, align 4
  br label %return

if.end27:                                         ; preds = %if.then18
  br label %if.end88

if.else:                                          ; preds = %if.then17
  %__extbuf_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 1
  %18 = load i8*, i8** %__extbuf_, align 4
  store i8* %18, i8** %__extbe, align 4
  br label %do.body

do.body:                                          ; preds = %do.cond, %if.else
  %__cv_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 9
  %19 = load %"class.std::__2::codecvt"*, %"class.std::__2::codecvt"** %__cv_, align 4
  %tobool28 = icmp ne %"class.std::__2::codecvt"* %19, null
  br i1 %tobool28, label %if.end30, label %if.then29

if.then29:                                        ; preds = %do.body
  call void @_ZNSt3__216__throw_bad_castEv() #13
  unreachable

if.end30:                                         ; preds = %do.body
  %__cv_31 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 9
  %20 = load %"class.std::__2::codecvt"*, %"class.std::__2::codecvt"** %__cv_31, align 4
  %__st_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 10
  %21 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call32 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE5pbaseEv(%"class.std::__2::basic_streambuf"* %21)
  %22 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call33 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE4pptrEv(%"class.std::__2::basic_streambuf"* %22)
  %__extbuf_34 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 1
  %23 = load i8*, i8** %__extbuf_34, align 4
  %__extbuf_35 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 1
  %24 = load i8*, i8** %__extbuf_35, align 4
  %__ebs_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 5
  %25 = load i32, i32* %__ebs_, align 4
  %add.ptr36 = getelementptr inbounds i8, i8* %24, i32 %25
  %call37 = call i32 @_ZNKSt3__27codecvtIcc11__mbstate_tE3outERS1_PKcS5_RS5_PcS7_RS7_(%"class.std::__2::codecvt"* %20, %struct.__mbstate_t* nonnull align 4 dereferenceable(8) %__st_, i8* %call32, i8* %call33, i8** nonnull align 4 dereferenceable(4) %__e, i8* %23, i8* %add.ptr36, i8** nonnull align 4 dereferenceable(4) %__extbe)
  store i32 %call37, i32* %__r, align 4
  %26 = load i8*, i8** %__e, align 4
  %27 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call38 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE5pbaseEv(%"class.std::__2::basic_streambuf"* %27)
  %cmp39 = icmp eq i8* %26, %call38
  br i1 %cmp39, label %if.then40, label %if.end42

if.then40:                                        ; preds = %if.end30
  %call41 = call i32 @_ZNSt3__211char_traitsIcE3eofEv() #3
  store i32 %call41, i32* %retval, align 4
  br label %return

if.end42:                                         ; preds = %if.end30
  %28 = load i32, i32* %__r, align 4
  %cmp43 = icmp eq i32 %28, 3
  br i1 %cmp43, label %if.then44, label %if.else58

if.then44:                                        ; preds = %if.end42
  %29 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call46 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE4pptrEv(%"class.std::__2::basic_streambuf"* %29)
  %30 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call47 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE5pbaseEv(%"class.std::__2::basic_streambuf"* %30)
  %sub.ptr.lhs.cast48 = ptrtoint i8* %call46 to i32
  %sub.ptr.rhs.cast49 = ptrtoint i8* %call47 to i32
  %sub.ptr.sub50 = sub i32 %sub.ptr.lhs.cast48, %sub.ptr.rhs.cast49
  store i32 %sub.ptr.sub50, i32* %__nmemb45, align 4
  %31 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call51 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE5pbaseEv(%"class.std::__2::basic_streambuf"* %31)
  %32 = load i32, i32* %__nmemb45, align 4
  %__file_52 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 8
  %33 = load %struct._IO_FILE*, %struct._IO_FILE** %__file_52, align 4
  %call53 = call i32 @fwrite(i8* %call51, i32 1, i32 %32, %struct._IO_FILE* %33)
  %34 = load i32, i32* %__nmemb45, align 4
  %cmp54 = icmp ne i32 %call53, %34
  br i1 %cmp54, label %if.then55, label %if.end57

if.then55:                                        ; preds = %if.then44
  %call56 = call i32 @_ZNSt3__211char_traitsIcE3eofEv() #3
  store i32 %call56, i32* %retval, align 4
  br label %return

if.end57:                                         ; preds = %if.then44
  br label %if.end86

if.else58:                                        ; preds = %if.end42
  %35 = load i32, i32* %__r, align 4
  %cmp59 = icmp eq i32 %35, 0
  br i1 %cmp59, label %if.then61, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.else58
  %36 = load i32, i32* %__r, align 4
  %cmp60 = icmp eq i32 %36, 1
  br i1 %cmp60, label %if.then61, label %if.else83

if.then61:                                        ; preds = %lor.lhs.false, %if.else58
  %37 = load i8*, i8** %__extbe, align 4
  %__extbuf_63 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 1
  %38 = load i8*, i8** %__extbuf_63, align 4
  %sub.ptr.lhs.cast64 = ptrtoint i8* %37 to i32
  %sub.ptr.rhs.cast65 = ptrtoint i8* %38 to i32
  %sub.ptr.sub66 = sub i32 %sub.ptr.lhs.cast64, %sub.ptr.rhs.cast65
  store i32 %sub.ptr.sub66, i32* %__nmemb62, align 4
  %__extbuf_67 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 1
  %39 = load i8*, i8** %__extbuf_67, align 4
  %40 = load i32, i32* %__nmemb62, align 4
  %__file_68 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 8
  %41 = load %struct._IO_FILE*, %struct._IO_FILE** %__file_68, align 4
  %call69 = call i32 @fwrite(i8* %39, i32 1, i32 %40, %struct._IO_FILE* %41)
  %42 = load i32, i32* %__nmemb62, align 4
  %cmp70 = icmp ne i32 %call69, %42
  br i1 %cmp70, label %if.then71, label %if.end73

if.then71:                                        ; preds = %if.then61
  %call72 = call i32 @_ZNSt3__211char_traitsIcE3eofEv() #3
  store i32 %call72, i32* %retval, align 4
  br label %return

if.end73:                                         ; preds = %if.then61
  %43 = load i32, i32* %__r, align 4
  %cmp74 = icmp eq i32 %43, 1
  br i1 %cmp74, label %if.then75, label %if.end82

if.then75:                                        ; preds = %if.end73
  %44 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %45 = load i8*, i8** %__e, align 4
  %46 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call76 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE4pptrEv(%"class.std::__2::basic_streambuf"* %46)
  call void @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE4setpEPcS4_(%"class.std::__2::basic_streambuf"* %44, i8* %45, i8* %call76)
  %47 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %48 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call77 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE5epptrEv(%"class.std::__2::basic_streambuf"* %48)
  %49 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call78 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE5pbaseEv(%"class.std::__2::basic_streambuf"* %49)
  %sub.ptr.lhs.cast79 = ptrtoint i8* %call77 to i32
  %sub.ptr.rhs.cast80 = ptrtoint i8* %call78 to i32
  %sub.ptr.sub81 = sub i32 %sub.ptr.lhs.cast79, %sub.ptr.rhs.cast80
  call void @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE7__pbumpEl(%"class.std::__2::basic_streambuf"* %47, i32 %sub.ptr.sub81)
  br label %if.end82

if.end82:                                         ; preds = %if.then75, %if.end73
  br label %if.end85

if.else83:                                        ; preds = %lor.lhs.false
  %call84 = call i32 @_ZNSt3__211char_traitsIcE3eofEv() #3
  store i32 %call84, i32* %retval, align 4
  br label %return

if.end85:                                         ; preds = %if.end82
  br label %if.end86

if.end86:                                         ; preds = %if.end85, %if.end57
  br label %do.cond

do.cond:                                          ; preds = %if.end86
  %50 = load i32, i32* %__r, align 4
  %cmp87 = icmp eq i32 %50, 1
  br i1 %cmp87, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond
  br label %if.end88

if.end88:                                         ; preds = %do.end, %if.end27
  %51 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %52 = load i8*, i8** %__pb_save, align 4
  %53 = load i8*, i8** %__epb_save, align 4
  call void @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE4setpEPcS4_(%"class.std::__2::basic_streambuf"* %51, i8* %52, i8* %53)
  br label %if.end89

if.end89:                                         ; preds = %if.end88, %if.end13
  %54 = load i32, i32* %__c.addr, align 4
  %call90 = call i32 @_ZNSt3__211char_traitsIcE7not_eofEi(i32 %54) #3
  store i32 %call90, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end89, %if.else83, %if.then71, %if.then55, %if.then40, %if.then25, %if.then
  %55 = load i32, i32* %retval, align 4
  ret i32 %55
}

declare zeroext i1 @_ZNKSt3__26locale9has_facetERNS0_2idE(%"class.std::__2::locale"*, %"class.std::__2::locale::id"* nonnull align 4 dereferenceable(8)) #4

; Function Attrs: nounwind
declare %"class.std::__2::locale"* @_ZNSt3__26localeC1ERKS0_(%"class.std::__2::locale"* returned, %"class.std::__2::locale"* nonnull align 4 dereferenceable(4)) unnamed_addr #2

declare %"class.std::__2::locale::facet"* @_ZNKSt3__26locale9use_facetERNS0_2idE(%"class.std::__2::locale"*, %"class.std::__2::locale::id"* nonnull align 4 dereferenceable(8)) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::basic_filebuf"* @_ZNSt3__213basic_filebufIcNS_11char_traitsIcEEE5closeEv(%"class.std::__2::basic_filebuf"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_filebuf"*, align 4
  %__rt = alloca %"class.std::__2::basic_filebuf"*, align 4
  %__h = alloca %"class.std::__2::unique_ptr", align 4
  %ref.tmp = alloca i32 (%struct._IO_FILE*)*, align 4
  store %"class.std::__2::basic_filebuf"* %this, %"class.std::__2::basic_filebuf"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_filebuf"*, %"class.std::__2::basic_filebuf"** %this.addr, align 4
  store %"class.std::__2::basic_filebuf"* null, %"class.std::__2::basic_filebuf"** %__rt, align 4
  %__file_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 8
  %0 = load %struct._IO_FILE*, %struct._IO_FILE** %__file_, align 4
  %tobool = icmp ne %struct._IO_FILE* %0, null
  br i1 %tobool, label %if.then, label %if.end16

if.then:                                          ; preds = %entry
  store %"class.std::__2::basic_filebuf"* %this1, %"class.std::__2::basic_filebuf"** %__rt, align 4
  %__file_2 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 8
  %1 = load %struct._IO_FILE*, %struct._IO_FILE** %__file_2, align 4
  store i32 (%struct._IO_FILE*)* @fclose, i32 (%struct._IO_FILE*)** %ref.tmp, align 4
  %call = call %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrI8_IO_FILEPFiPS1_EEC2ILb1EvEES2_NS_16__dependent_typeINS_27__unique_ptr_deleter_sfinaeIS4_EEXT_EE20__good_rval_ref_typeE(%"class.std::__2::unique_ptr"* %__h, %struct._IO_FILE* %1, i32 (%struct._IO_FILE*)** nonnull align 4 dereferenceable(4) %ref.tmp) #3
  %2 = bitcast %"class.std::__2::basic_filebuf"* %this1 to i32 (%"class.std::__2::basic_filebuf"*)***
  %vtable = load i32 (%"class.std::__2::basic_filebuf"*)**, i32 (%"class.std::__2::basic_filebuf"*)*** %2, align 4
  %vfn = getelementptr inbounds i32 (%"class.std::__2::basic_filebuf"*)*, i32 (%"class.std::__2::basic_filebuf"*)** %vtable, i64 6
  %3 = load i32 (%"class.std::__2::basic_filebuf"*)*, i32 (%"class.std::__2::basic_filebuf"*)** %vfn, align 4
  %call3 = call i32 %3(%"class.std::__2::basic_filebuf"* %this1)
  %tobool4 = icmp ne i32 %call3, 0
  br i1 %tobool4, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.then
  store %"class.std::__2::basic_filebuf"* null, %"class.std::__2::basic_filebuf"** %__rt, align 4
  br label %if.end

if.end:                                           ; preds = %if.then5, %if.then
  %call6 = call %struct._IO_FILE* @_ZNSt3__210unique_ptrI8_IO_FILEPFiPS1_EE7releaseEv(%"class.std::__2::unique_ptr"* %__h) #3
  %call7 = call i32 @fclose(%struct._IO_FILE* %call6)
  %tobool8 = icmp ne i32 %call7, 0
  br i1 %tobool8, label %if.then9, label %if.end10

if.then9:                                         ; preds = %if.end
  store %"class.std::__2::basic_filebuf"* null, %"class.std::__2::basic_filebuf"** %__rt, align 4
  br label %if.end10

if.end10:                                         ; preds = %if.then9, %if.end
  %__file_11 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 8
  store %struct._IO_FILE* null, %struct._IO_FILE** %__file_11, align 4
  %4 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"* (%"class.std::__2::basic_filebuf"*, i8*, i32)***
  %vtable12 = load %"class.std::__2::basic_streambuf"* (%"class.std::__2::basic_filebuf"*, i8*, i32)**, %"class.std::__2::basic_streambuf"* (%"class.std::__2::basic_filebuf"*, i8*, i32)*** %4, align 4
  %vfn13 = getelementptr inbounds %"class.std::__2::basic_streambuf"* (%"class.std::__2::basic_filebuf"*, i8*, i32)*, %"class.std::__2::basic_streambuf"* (%"class.std::__2::basic_filebuf"*, i8*, i32)** %vtable12, i64 3
  %5 = load %"class.std::__2::basic_streambuf"* (%"class.std::__2::basic_filebuf"*, i8*, i32)*, %"class.std::__2::basic_streambuf"* (%"class.std::__2::basic_filebuf"*, i8*, i32)** %vfn13, align 4
  %call14 = call %"class.std::__2::basic_streambuf"* %5(%"class.std::__2::basic_filebuf"* %this1, i8* null, i32 0)
  %call15 = call %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrI8_IO_FILEPFiPS1_EED2Ev(%"class.std::__2::unique_ptr"* %__h) #3
  br label %if.end16

if.end16:                                         ; preds = %if.end10, %entry
  %6 = load %"class.std::__2::basic_filebuf"*, %"class.std::__2::basic_filebuf"** %__rt, align 4
  ret %"class.std::__2::basic_filebuf"* %6
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdaPv(i8*) #5

declare i32 @fclose(%struct._IO_FILE*) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrI8_IO_FILEPFiPS1_EEC2ILb1EvEES2_NS_16__dependent_typeINS_27__unique_ptr_deleter_sfinaeIS4_EEXT_EE20__good_rval_ref_typeE(%"class.std::__2::unique_ptr"* returned %this, %struct._IO_FILE* %__p, i32 (%struct._IO_FILE*)** nonnull align 4 dereferenceable(4) %__d) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  %__p.addr = alloca %struct._IO_FILE*, align 4
  %__d.addr = alloca i32 (%struct._IO_FILE*)**, align 4
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  store %struct._IO_FILE* %__p, %struct._IO_FILE** %__p.addr, align 4
  store i32 (%struct._IO_FILE*)** %__d, i32 (%struct._IO_FILE*)*** %__d.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %0 = load i32 (%struct._IO_FILE*)**, i32 (%struct._IO_FILE*)*** %__d.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32 (%struct._IO_FILE*)** @_ZNSt3__24moveIRPFiP8_IO_FILEEEEONS_16remove_referenceIT_E4typeEOS7_(i32 (%struct._IO_FILE*)** nonnull align 4 dereferenceable(4) %0) #3
  %call2 = call %"class.std::__2::__compressed_pair.7"* @_ZNSt3__217__compressed_pairIP8_IO_FILEPFiS2_EEC2IRS2_S4_EEOT_OT0_(%"class.std::__2::__compressed_pair.7"* %__ptr_, %struct._IO_FILE** nonnull align 4 dereferenceable(4) %__p.addr, i32 (%struct._IO_FILE*)** nonnull align 4 dereferenceable(4) %call)
  ret %"class.std::__2::unique_ptr"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct._IO_FILE* @_ZNSt3__210unique_ptrI8_IO_FILEPFiPS1_EE7releaseEv(%"class.std::__2::unique_ptr"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  %__t = alloca %struct._IO_FILE*, align 4
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %struct._IO_FILE** @_ZNSt3__217__compressed_pairIP8_IO_FILEPFiS2_EE5firstEv(%"class.std::__2::__compressed_pair.7"* %__ptr_) #3
  %0 = load %struct._IO_FILE*, %struct._IO_FILE** %call, align 4
  store %struct._IO_FILE* %0, %struct._IO_FILE** %__t, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %struct._IO_FILE** @_ZNSt3__217__compressed_pairIP8_IO_FILEPFiS2_EE5firstEv(%"class.std::__2::__compressed_pair.7"* %__ptr_2) #3
  store %struct._IO_FILE* null, %struct._IO_FILE** %call3, align 4
  %1 = load %struct._IO_FILE*, %struct._IO_FILE** %__t, align 4
  ret %struct._IO_FILE* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrI8_IO_FILEPFiPS1_EED2Ev(%"class.std::__2::unique_ptr"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  call void @_ZNSt3__210unique_ptrI8_IO_FILEPFiPS1_EE5resetES2_(%"class.std::__2::unique_ptr"* %this1, %struct._IO_FILE* null) #3
  ret %"class.std::__2::unique_ptr"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32 (%struct._IO_FILE*)** @_ZNSt3__24moveIRPFiP8_IO_FILEEEEONS_16remove_referenceIT_E4typeEOS7_(i32 (%struct._IO_FILE*)** nonnull align 4 dereferenceable(4) %__t) #1 comdat {
entry:
  %__t.addr = alloca i32 (%struct._IO_FILE*)**, align 4
  store i32 (%struct._IO_FILE*)** %__t, i32 (%struct._IO_FILE*)*** %__t.addr, align 4
  %0 = load i32 (%struct._IO_FILE*)**, i32 (%struct._IO_FILE*)*** %__t.addr, align 4
  ret i32 (%struct._IO_FILE*)** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.7"* @_ZNSt3__217__compressed_pairIP8_IO_FILEPFiS2_EEC2IRS2_S4_EEOT_OT0_(%"class.std::__2::__compressed_pair.7"* returned %this, %struct._IO_FILE** nonnull align 4 dereferenceable(4) %__t1, i32 (%struct._IO_FILE*)** nonnull align 4 dereferenceable(4) %__t2) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.7"*, align 4
  %__t1.addr = alloca %struct._IO_FILE**, align 4
  %__t2.addr = alloca i32 (%struct._IO_FILE*)**, align 4
  store %"class.std::__2::__compressed_pair.7"* %this, %"class.std::__2::__compressed_pair.7"** %this.addr, align 4
  store %struct._IO_FILE** %__t1, %struct._IO_FILE*** %__t1.addr, align 4
  store i32 (%struct._IO_FILE*)** %__t2, i32 (%struct._IO_FILE*)*** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.7"*, %"class.std::__2::__compressed_pair.7"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.7"* %this1 to %"struct.std::__2::__compressed_pair_elem.8"*
  %1 = load %struct._IO_FILE**, %struct._IO_FILE*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %struct._IO_FILE** @_ZNSt3__27forwardIRP8_IO_FILEEEOT_RNS_16remove_referenceIS4_E4typeE(%struct._IO_FILE** nonnull align 4 dereferenceable(4) %1) #3
  %call2 = call %"struct.std::__2::__compressed_pair_elem.8"* @_ZNSt3__222__compressed_pair_elemIP8_IO_FILELi0ELb0EEC2IRS2_vEEOT_(%"struct.std::__2::__compressed_pair_elem.8"* %0, %struct._IO_FILE** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.7"* %this1 to i8*
  %3 = getelementptr inbounds i8, i8* %2, i32 4
  %4 = bitcast i8* %3 to %"struct.std::__2::__compressed_pair_elem.9"*
  %5 = load i32 (%struct._IO_FILE*)**, i32 (%struct._IO_FILE*)*** %__t2.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(4) i32 (%struct._IO_FILE*)** @_ZNSt3__27forwardIPFiP8_IO_FILEEEEOT_RNS_16remove_referenceIS5_E4typeE(i32 (%struct._IO_FILE*)** nonnull align 4 dereferenceable(4) %5) #3
  %call4 = call %"struct.std::__2::__compressed_pair_elem.9"* @_ZNSt3__222__compressed_pair_elemIPFiP8_IO_FILEELi1ELb0EEC2IS4_vEEOT_(%"struct.std::__2::__compressed_pair_elem.9"* %4, i32 (%struct._IO_FILE*)** nonnull align 4 dereferenceable(4) %call3)
  ret %"class.std::__2::__compressed_pair.7"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %struct._IO_FILE** @_ZNSt3__27forwardIRP8_IO_FILEEEOT_RNS_16remove_referenceIS4_E4typeE(%struct._IO_FILE** nonnull align 4 dereferenceable(4) %__t) #1 comdat {
entry:
  %__t.addr = alloca %struct._IO_FILE**, align 4
  store %struct._IO_FILE** %__t, %struct._IO_FILE*** %__t.addr, align 4
  %0 = load %struct._IO_FILE**, %struct._IO_FILE*** %__t.addr, align 4
  ret %struct._IO_FILE** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.8"* @_ZNSt3__222__compressed_pair_elemIP8_IO_FILELi0ELb0EEC2IRS2_vEEOT_(%"struct.std::__2::__compressed_pair_elem.8"* returned %this, %struct._IO_FILE** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.8"*, align 4
  %__u.addr = alloca %struct._IO_FILE**, align 4
  store %"struct.std::__2::__compressed_pair_elem.8"* %this, %"struct.std::__2::__compressed_pair_elem.8"** %this.addr, align 4
  store %struct._IO_FILE** %__u, %struct._IO_FILE*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.8"*, %"struct.std::__2::__compressed_pair_elem.8"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.8", %"struct.std::__2::__compressed_pair_elem.8"* %this1, i32 0, i32 0
  %0 = load %struct._IO_FILE**, %struct._IO_FILE*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %struct._IO_FILE** @_ZNSt3__27forwardIRP8_IO_FILEEEOT_RNS_16remove_referenceIS4_E4typeE(%struct._IO_FILE** nonnull align 4 dereferenceable(4) %0) #3
  %1 = load %struct._IO_FILE*, %struct._IO_FILE** %call, align 4
  store %struct._IO_FILE* %1, %struct._IO_FILE** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.8"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32 (%struct._IO_FILE*)** @_ZNSt3__27forwardIPFiP8_IO_FILEEEEOT_RNS_16remove_referenceIS5_E4typeE(i32 (%struct._IO_FILE*)** nonnull align 4 dereferenceable(4) %__t) #1 comdat {
entry:
  %__t.addr = alloca i32 (%struct._IO_FILE*)**, align 4
  store i32 (%struct._IO_FILE*)** %__t, i32 (%struct._IO_FILE*)*** %__t.addr, align 4
  %0 = load i32 (%struct._IO_FILE*)**, i32 (%struct._IO_FILE*)*** %__t.addr, align 4
  ret i32 (%struct._IO_FILE*)** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.9"* @_ZNSt3__222__compressed_pair_elemIPFiP8_IO_FILEELi1ELb0EEC2IS4_vEEOT_(%"struct.std::__2::__compressed_pair_elem.9"* returned %this, i32 (%struct._IO_FILE*)** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.9"*, align 4
  %__u.addr = alloca i32 (%struct._IO_FILE*)**, align 4
  store %"struct.std::__2::__compressed_pair_elem.9"* %this, %"struct.std::__2::__compressed_pair_elem.9"** %this.addr, align 4
  store i32 (%struct._IO_FILE*)** %__u, i32 (%struct._IO_FILE*)*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.9"*, %"struct.std::__2::__compressed_pair_elem.9"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.9", %"struct.std::__2::__compressed_pair_elem.9"* %this1, i32 0, i32 0
  %0 = load i32 (%struct._IO_FILE*)**, i32 (%struct._IO_FILE*)*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32 (%struct._IO_FILE*)** @_ZNSt3__27forwardIPFiP8_IO_FILEEEEOT_RNS_16remove_referenceIS5_E4typeE(i32 (%struct._IO_FILE*)** nonnull align 4 dereferenceable(4) %0) #3
  %1 = load i32 (%struct._IO_FILE*)*, i32 (%struct._IO_FILE*)** %call, align 4
  store i32 (%struct._IO_FILE*)* %1, i32 (%struct._IO_FILE*)** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.9"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %struct._IO_FILE** @_ZNSt3__217__compressed_pairIP8_IO_FILEPFiS2_EE5firstEv(%"class.std::__2::__compressed_pair.7"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.7"*, align 4
  store %"class.std::__2::__compressed_pair.7"* %this, %"class.std::__2::__compressed_pair.7"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.7"*, %"class.std::__2::__compressed_pair.7"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.7"* %this1 to %"struct.std::__2::__compressed_pair_elem.8"*
  %call = call nonnull align 4 dereferenceable(4) %struct._IO_FILE** @_ZNSt3__222__compressed_pair_elemIP8_IO_FILELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.8"* %0) #3
  ret %struct._IO_FILE** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %struct._IO_FILE** @_ZNSt3__222__compressed_pair_elemIP8_IO_FILELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.8"* %this) #1 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.8"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.8"* %this, %"struct.std::__2::__compressed_pair_elem.8"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.8"*, %"struct.std::__2::__compressed_pair_elem.8"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.8", %"struct.std::__2::__compressed_pair_elem.8"* %this1, i32 0, i32 0
  ret %struct._IO_FILE** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__210unique_ptrI8_IO_FILEPFiPS1_EE5resetES2_(%"class.std::__2::unique_ptr"* %this, %struct._IO_FILE* %__p) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  %__p.addr = alloca %struct._IO_FILE*, align 4
  %__tmp = alloca %struct._IO_FILE*, align 4
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  store %struct._IO_FILE* %__p, %struct._IO_FILE** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %struct._IO_FILE** @_ZNSt3__217__compressed_pairIP8_IO_FILEPFiS2_EE5firstEv(%"class.std::__2::__compressed_pair.7"* %__ptr_) #3
  %0 = load %struct._IO_FILE*, %struct._IO_FILE** %call, align 4
  store %struct._IO_FILE* %0, %struct._IO_FILE** %__tmp, align 4
  %1 = load %struct._IO_FILE*, %struct._IO_FILE** %__p.addr, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %struct._IO_FILE** @_ZNSt3__217__compressed_pairIP8_IO_FILEPFiS2_EE5firstEv(%"class.std::__2::__compressed_pair.7"* %__ptr_2) #3
  store %struct._IO_FILE* %1, %struct._IO_FILE** %call3, align 4
  %2 = load %struct._IO_FILE*, %struct._IO_FILE** %__tmp, align 4
  %tobool = icmp ne %struct._IO_FILE* %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__ptr_4 = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call5 = call nonnull align 4 dereferenceable(4) i32 (%struct._IO_FILE*)** @_ZNSt3__217__compressed_pairIP8_IO_FILEPFiS2_EE6secondEv(%"class.std::__2::__compressed_pair.7"* %__ptr_4) #3
  %3 = load i32 (%struct._IO_FILE*)*, i32 (%struct._IO_FILE*)** %call5, align 4
  %4 = load %struct._IO_FILE*, %struct._IO_FILE** %__tmp, align 4
  %call6 = call i32 %3(%struct._IO_FILE* %4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32 (%struct._IO_FILE*)** @_ZNSt3__217__compressed_pairIP8_IO_FILEPFiS2_EE6secondEv(%"class.std::__2::__compressed_pair.7"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.7"*, align 4
  store %"class.std::__2::__compressed_pair.7"* %this, %"class.std::__2::__compressed_pair.7"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.7"*, %"class.std::__2::__compressed_pair.7"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.7"* %this1 to i8*
  %add.ptr = getelementptr inbounds i8, i8* %0, i32 4
  %1 = bitcast i8* %add.ptr to %"struct.std::__2::__compressed_pair_elem.9"*
  %call = call nonnull align 4 dereferenceable(4) i32 (%struct._IO_FILE*)** @_ZNSt3__222__compressed_pair_elemIPFiP8_IO_FILEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.9"* %1) #3
  ret i32 (%struct._IO_FILE*)** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32 (%struct._IO_FILE*)** @_ZNSt3__222__compressed_pair_elemIPFiP8_IO_FILEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.9"* %this) #1 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.9"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.9"* %this, %"struct.std::__2::__compressed_pair_elem.9"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.9"*, %"struct.std::__2::__compressed_pair_elem.9"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.9", %"struct.std::__2::__compressed_pair_elem.9"* %this1, i32 0, i32 0
  ret i32 (%struct._IO_FILE*)** %__value_
}

; Function Attrs: nobuiltin allocsize(0)
declare nonnull i8* @_Znam(i32) #7

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxIlEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #1 comdat {
entry:
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::__less.10", align 1
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__a.addr, align 4
  %1 = load i32*, i32** %__b.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxIlNS_6__lessIllEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxIlNS_6__lessIllEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #1 comdat {
entry:
  %__comp = alloca %"struct.std::__2::__less.10", align 1
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__a.addr, align 4
  %1 = load i32*, i32** %__b.addr, align 4
  %call = call zeroext i1 @_ZNKSt3__26__lessIllEclERKlS3_(%"struct.std::__2::__less.10"* %__comp, i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  br i1 %call, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32*, i32** %__b.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %3 = load i32*, i32** %__a.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi i32* [ %2, %cond.true ], [ %3, %cond.false ]
  ret i32* %cond-lvalue
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__26__lessIllEclERKlS3_(%"struct.std::__2::__less.10"* %this, i32* nonnull align 4 dereferenceable(4) %__x, i32* nonnull align 4 dereferenceable(4) %__y) #1 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__less.10"*, align 4
  %__x.addr = alloca i32*, align 4
  %__y.addr = alloca i32*, align 4
  store %"struct.std::__2::__less.10"* %this, %"struct.std::__2::__less.10"** %this.addr, align 4
  store i32* %__x, i32** %__x.addr, align 4
  store i32* %__y, i32** %__y.addr, align 4
  %this1 = load %"struct.std::__2::__less.10"*, %"struct.std::__2::__less.10"** %this.addr, align 4
  %0 = load i32*, i32** %__x.addr, align 4
  %1 = load i32, i32* %0, align 4
  %2 = load i32*, i32** %__y.addr, align 4
  %3 = load i32, i32* %2, align 4
  %cmp = icmp slt i32 %1, %3
  ret i1 %cmp
}

; Function Attrs: noinline noreturn nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216__throw_bad_castEv() #8 comdat {
entry:
  call void @abort() #13
  unreachable
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__27codecvtIcc11__mbstate_tE8encodingEv(%"class.std::__2::codecvt"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::codecvt"*, align 4
  store %"class.std::__2::codecvt"* %this, %"class.std::__2::codecvt"** %this.addr, align 4
  %this1 = load %"class.std::__2::codecvt"*, %"class.std::__2::codecvt"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::codecvt"* %this1 to i32 (%"class.std::__2::codecvt"*)***
  %vtable = load i32 (%"class.std::__2::codecvt"*)**, i32 (%"class.std::__2::codecvt"*)*** %0, align 4
  %vfn = getelementptr inbounds i32 (%"class.std::__2::codecvt"*)*, i32 (%"class.std::__2::codecvt"*)** %vtable, i64 6
  %1 = load i32 (%"class.std::__2::codecvt"*)*, i32 (%"class.std::__2::codecvt"*)** %vfn, align 4
  %call = call i32 %1(%"class.std::__2::codecvt"* %this1) #3
  ret i32 %call
}

declare i32 @fseeko(%struct._IO_FILE*, i64, i32) #4

declare i64 @ftello(%struct._IO_FILE*) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__24fposI11__mbstate_tE5stateES1_(%"class.std::__2::fpos"* %this, %struct.__mbstate_t* byval(%struct.__mbstate_t) align 4 %__st) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::fpos"*, align 4
  store %"class.std::__2::fpos"* %this, %"class.std::__2::fpos"** %this.addr, align 4
  %this1 = load %"class.std::__2::fpos"*, %"class.std::__2::fpos"** %this.addr, align 4
  %__st_ = getelementptr inbounds %"class.std::__2::fpos", %"class.std::__2::fpos"* %this1, i32 0, i32 0
  %0 = bitcast %struct.__mbstate_t* %__st_ to i8*
  %1 = bitcast %struct.__mbstate_t* %__st to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %0, i8* align 4 %1, i32 8, i1 false)
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #9

; Function Attrs: noreturn
declare void @abort() #10

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__24fposI11__mbstate_tE5stateEv(%struct.__mbstate_t* noalias sret align 4 %agg.result, %"class.std::__2::fpos"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::fpos"*, align 4
  store %"class.std::__2::fpos"* %this, %"class.std::__2::fpos"** %this.addr, align 4
  %this1 = load %"class.std::__2::fpos"*, %"class.std::__2::fpos"** %this.addr, align 4
  %__st_ = getelementptr inbounds %"class.std::__2::fpos", %"class.std::__2::fpos"* %this1, i32 0, i32 0
  %0 = bitcast %struct.__mbstate_t* %agg.result to i8*
  %1 = bitcast %struct.__mbstate_t* %__st_ to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %0, i8* align 8 %1, i32 8, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__27codecvtIcc11__mbstate_tE7unshiftERS1_PcS4_RS4_(%"class.std::__2::codecvt"* %this, %struct.__mbstate_t* nonnull align 4 dereferenceable(8) %__st, i8* %__to, i8* %__to_end, i8** nonnull align 4 dereferenceable(4) %__to_nxt) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::codecvt"*, align 4
  %__st.addr = alloca %struct.__mbstate_t*, align 4
  %__to.addr = alloca i8*, align 4
  %__to_end.addr = alloca i8*, align 4
  %__to_nxt.addr = alloca i8**, align 4
  store %"class.std::__2::codecvt"* %this, %"class.std::__2::codecvt"** %this.addr, align 4
  store %struct.__mbstate_t* %__st, %struct.__mbstate_t** %__st.addr, align 4
  store i8* %__to, i8** %__to.addr, align 4
  store i8* %__to_end, i8** %__to_end.addr, align 4
  store i8** %__to_nxt, i8*** %__to_nxt.addr, align 4
  %this1 = load %"class.std::__2::codecvt"*, %"class.std::__2::codecvt"** %this.addr, align 4
  %0 = load %struct.__mbstate_t*, %struct.__mbstate_t** %__st.addr, align 4
  %1 = load i8*, i8** %__to.addr, align 4
  %2 = load i8*, i8** %__to_end.addr, align 4
  %3 = load i8**, i8*** %__to_nxt.addr, align 4
  %4 = bitcast %"class.std::__2::codecvt"* %this1 to i32 (%"class.std::__2::codecvt"*, %struct.__mbstate_t*, i8*, i8*, i8**)***
  %vtable = load i32 (%"class.std::__2::codecvt"*, %struct.__mbstate_t*, i8*, i8*, i8**)**, i32 (%"class.std::__2::codecvt"*, %struct.__mbstate_t*, i8*, i8*, i8**)*** %4, align 4
  %vfn = getelementptr inbounds i32 (%"class.std::__2::codecvt"*, %struct.__mbstate_t*, i8*, i8*, i8**)*, i32 (%"class.std::__2::codecvt"*, %struct.__mbstate_t*, i8*, i8*, i8**)** %vtable, i64 5
  %5 = load i32 (%"class.std::__2::codecvt"*, %struct.__mbstate_t*, i8*, i8*, i8**)*, i32 (%"class.std::__2::codecvt"*, %struct.__mbstate_t*, i8*, i8*, i8**)** %vfn, align 4
  %call = call i32 %5(%"class.std::__2::codecvt"* %this1, %struct.__mbstate_t* nonnull align 4 dereferenceable(8) %0, i8* %1, i8* %2, i8** nonnull align 4 dereferenceable(4) %3)
  ret i32 %call
}

declare i32 @fwrite(i8*, i32, i32, %struct._IO_FILE*) #4

declare i32 @fflush(%struct._IO_FILE*) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__27codecvtIcc11__mbstate_tE6lengthERS1_PKcS5_m(%"class.std::__2::codecvt"* %this, %struct.__mbstate_t* nonnull align 4 dereferenceable(8) %__st, i8* %__frm, i8* %__end, i32 %__mx) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::codecvt"*, align 4
  %__st.addr = alloca %struct.__mbstate_t*, align 4
  %__frm.addr = alloca i8*, align 4
  %__end.addr = alloca i8*, align 4
  %__mx.addr = alloca i32, align 4
  store %"class.std::__2::codecvt"* %this, %"class.std::__2::codecvt"** %this.addr, align 4
  store %struct.__mbstate_t* %__st, %struct.__mbstate_t** %__st.addr, align 4
  store i8* %__frm, i8** %__frm.addr, align 4
  store i8* %__end, i8** %__end.addr, align 4
  store i32 %__mx, i32* %__mx.addr, align 4
  %this1 = load %"class.std::__2::codecvt"*, %"class.std::__2::codecvt"** %this.addr, align 4
  %0 = load %struct.__mbstate_t*, %struct.__mbstate_t** %__st.addr, align 4
  %1 = load i8*, i8** %__frm.addr, align 4
  %2 = load i8*, i8** %__end.addr, align 4
  %3 = load i32, i32* %__mx.addr, align 4
  %4 = bitcast %"class.std::__2::codecvt"* %this1 to i32 (%"class.std::__2::codecvt"*, %struct.__mbstate_t*, i8*, i8*, i32)***
  %vtable = load i32 (%"class.std::__2::codecvt"*, %struct.__mbstate_t*, i8*, i8*, i32)**, i32 (%"class.std::__2::codecvt"*, %struct.__mbstate_t*, i8*, i8*, i32)*** %4, align 4
  %vfn = getelementptr inbounds i32 (%"class.std::__2::codecvt"*, %struct.__mbstate_t*, i8*, i8*, i32)*, i32 (%"class.std::__2::codecvt"*, %struct.__mbstate_t*, i8*, i8*, i32)** %vtable, i64 8
  %5 = load i32 (%"class.std::__2::codecvt"*, %struct.__mbstate_t*, i8*, i8*, i32)*, i32 (%"class.std::__2::codecvt"*, %struct.__mbstate_t*, i8*, i8*, i32)** %vfn, align 4
  %call = call i32 %5(%"class.std::__2::codecvt"* %this1, %struct.__mbstate_t* nonnull align 4 dereferenceable(8) %0, i8* %1, i8* %2, i32 %3)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNSt3__213basic_filebufIcNS_11char_traitsIcEEE11__read_modeEv(%"class.std::__2::basic_filebuf"* %this) #1 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.std::__2::basic_filebuf"*, align 4
  store %"class.std::__2::basic_filebuf"* %this, %"class.std::__2::basic_filebuf"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_filebuf"*, %"class.std::__2::basic_filebuf"** %this.addr, align 4
  %__cm_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 13
  %0 = load i32, i32* %__cm_, align 4
  %and = and i32 %0, 8
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.end14, label %if.then

if.then:                                          ; preds = %entry
  %1 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  call void @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE4setpEPcS4_(%"class.std::__2::basic_streambuf"* %1, i8* null, i8* null)
  %__always_noconv_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 16
  %2 = load i8, i8* %__always_noconv_, align 2
  %tobool2 = trunc i8 %2 to i1
  br i1 %tobool2, label %if.then3, label %if.else

if.then3:                                         ; preds = %if.then
  %3 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %__extbuf_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 1
  %4 = load i8*, i8** %__extbuf_, align 4
  %__extbuf_4 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 1
  %5 = load i8*, i8** %__extbuf_4, align 4
  %__ebs_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 5
  %6 = load i32, i32* %__ebs_, align 4
  %add.ptr = getelementptr inbounds i8, i8* %5, i32 %6
  %__extbuf_5 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 1
  %7 = load i8*, i8** %__extbuf_5, align 4
  %__ebs_6 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 5
  %8 = load i32, i32* %__ebs_6, align 4
  %add.ptr7 = getelementptr inbounds i8, i8* %7, i32 %8
  call void @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE4setgEPcS4_S4_(%"class.std::__2::basic_streambuf"* %3, i8* %4, i8* %add.ptr, i8* %add.ptr7)
  br label %if.end

if.else:                                          ; preds = %if.then
  %9 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %__intbuf_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 6
  %10 = load i8*, i8** %__intbuf_, align 4
  %__intbuf_8 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 6
  %11 = load i8*, i8** %__intbuf_8, align 4
  %__ibs_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 7
  %12 = load i32, i32* %__ibs_, align 4
  %add.ptr9 = getelementptr inbounds i8, i8* %11, i32 %12
  %__intbuf_10 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 6
  %13 = load i8*, i8** %__intbuf_10, align 4
  %__ibs_11 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 7
  %14 = load i32, i32* %__ibs_11, align 4
  %add.ptr12 = getelementptr inbounds i8, i8* %13, i32 %14
  call void @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE4setgEPcS4_S4_(%"class.std::__2::basic_streambuf"* %9, i8* %10, i8* %add.ptr9, i8* %add.ptr12)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then3
  %__cm_13 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 13
  store i32 8, i32* %__cm_13, align 4
  store i1 true, i1* %retval, align 1
  br label %return

if.end14:                                         ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end14, %if.end
  %15 = load i1, i1* %retval, align 1
  ret i1 %15
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #1 comdat {
entry:
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::__less.11", align 1
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__a.addr, align 4
  %1 = load i32*, i32** %__b.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  ret i32* %call
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memmove.p0i8.p0i8.i32(i8* nocapture, i8* nocapture readonly, i32, i1 immarg) #9

declare i32 @fread(i8*, i32, i32, %struct._IO_FILE*) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__27codecvtIcc11__mbstate_tE2inERS1_PKcS5_RS5_PcS7_RS7_(%"class.std::__2::codecvt"* %this, %struct.__mbstate_t* nonnull align 4 dereferenceable(8) %__st, i8* %__frm, i8* %__frm_end, i8** nonnull align 4 dereferenceable(4) %__frm_nxt, i8* %__to, i8* %__to_end, i8** nonnull align 4 dereferenceable(4) %__to_nxt) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::codecvt"*, align 4
  %__st.addr = alloca %struct.__mbstate_t*, align 4
  %__frm.addr = alloca i8*, align 4
  %__frm_end.addr = alloca i8*, align 4
  %__frm_nxt.addr = alloca i8**, align 4
  %__to.addr = alloca i8*, align 4
  %__to_end.addr = alloca i8*, align 4
  %__to_nxt.addr = alloca i8**, align 4
  store %"class.std::__2::codecvt"* %this, %"class.std::__2::codecvt"** %this.addr, align 4
  store %struct.__mbstate_t* %__st, %struct.__mbstate_t** %__st.addr, align 4
  store i8* %__frm, i8** %__frm.addr, align 4
  store i8* %__frm_end, i8** %__frm_end.addr, align 4
  store i8** %__frm_nxt, i8*** %__frm_nxt.addr, align 4
  store i8* %__to, i8** %__to.addr, align 4
  store i8* %__to_end, i8** %__to_end.addr, align 4
  store i8** %__to_nxt, i8*** %__to_nxt.addr, align 4
  %this1 = load %"class.std::__2::codecvt"*, %"class.std::__2::codecvt"** %this.addr, align 4
  %0 = load %struct.__mbstate_t*, %struct.__mbstate_t** %__st.addr, align 4
  %1 = load i8*, i8** %__frm.addr, align 4
  %2 = load i8*, i8** %__frm_end.addr, align 4
  %3 = load i8**, i8*** %__frm_nxt.addr, align 4
  %4 = load i8*, i8** %__to.addr, align 4
  %5 = load i8*, i8** %__to_end.addr, align 4
  %6 = load i8**, i8*** %__to_nxt.addr, align 4
  %7 = bitcast %"class.std::__2::codecvt"* %this1 to i32 (%"class.std::__2::codecvt"*, %struct.__mbstate_t*, i8*, i8*, i8**, i8*, i8*, i8**)***
  %vtable = load i32 (%"class.std::__2::codecvt"*, %struct.__mbstate_t*, i8*, i8*, i8**, i8*, i8*, i8**)**, i32 (%"class.std::__2::codecvt"*, %struct.__mbstate_t*, i8*, i8*, i8**, i8*, i8*, i8**)*** %7, align 4
  %vfn = getelementptr inbounds i32 (%"class.std::__2::codecvt"*, %struct.__mbstate_t*, i8*, i8*, i8**, i8*, i8*, i8**)*, i32 (%"class.std::__2::codecvt"*, %struct.__mbstate_t*, i8*, i8*, i8**, i8*, i8*, i8**)** %vtable, i64 4
  %8 = load i32 (%"class.std::__2::codecvt"*, %struct.__mbstate_t*, i8*, i8*, i8**, i8*, i8*, i8**)*, i32 (%"class.std::__2::codecvt"*, %struct.__mbstate_t*, i8*, i8*, i8**, i8*, i8*, i8**)** %vfn, align 4
  %call = call i32 %8(%"class.std::__2::codecvt"* %this1, %struct.__mbstate_t* nonnull align 4 dereferenceable(8) %0, i8* %1, i8* %2, i8** nonnull align 4 dereferenceable(4) %3, i8* %4, i8* %5, i8** nonnull align 4 dereferenceable(4) %6)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #1 comdat {
entry:
  %__comp = alloca %"struct.std::__2::__less.11", align 1
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__b.addr, align 4
  %1 = load i32*, i32** %__a.addr, align 4
  %call = call zeroext i1 @_ZNKSt3__26__lessImmEclERKmS3_(%"struct.std::__2::__less.11"* %__comp, i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  br i1 %call, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32*, i32** %__b.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %3 = load i32*, i32** %__a.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi i32* [ %2, %cond.true ], [ %3, %cond.false ]
  ret i32* %cond-lvalue
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__26__lessImmEclERKmS3_(%"struct.std::__2::__less.11"* %this, i32* nonnull align 4 dereferenceable(4) %__x, i32* nonnull align 4 dereferenceable(4) %__y) #1 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__less.11"*, align 4
  %__x.addr = alloca i32*, align 4
  %__y.addr = alloca i32*, align 4
  store %"struct.std::__2::__less.11"* %this, %"struct.std::__2::__less.11"** %this.addr, align 4
  store i32* %__x, i32** %__x.addr, align 4
  store i32* %__y, i32** %__y.addr, align 4
  %this1 = load %"struct.std::__2::__less.11"*, %"struct.std::__2::__less.11"** %this.addr, align 4
  %0 = load i32*, i32** %__x.addr, align 4
  %1 = load i32, i32* %0, align 4
  %2 = load i32*, i32** %__y.addr, align 4
  %3 = load i32, i32* %2, align 4
  %cmp = icmp ult i32 %1, %3
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE5gbumpEi(%"class.std::__2::basic_streambuf"* %this, i32 %__n) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_streambuf"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::basic_streambuf"* %this, %"class.std::__2::basic_streambuf"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::basic_streambuf"*, %"class.std::__2::basic_streambuf"** %this.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %__ninp_ = getelementptr inbounds %"class.std::__2::basic_streambuf", %"class.std::__2::basic_streambuf"* %this1, i32 0, i32 3
  %1 = load i8*, i8** %__ninp_, align 4
  %add.ptr = getelementptr inbounds i8, i8* %1, i32 %0
  store i8* %add.ptr, i8** %__ninp_, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213basic_filebufIcNS_11char_traitsIcEEE12__write_modeEv(%"class.std::__2::basic_filebuf"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_filebuf"*, align 4
  store %"class.std::__2::basic_filebuf"* %this, %"class.std::__2::basic_filebuf"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_filebuf"*, %"class.std::__2::basic_filebuf"** %this.addr, align 4
  %__cm_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 13
  %0 = load i32, i32* %__cm_, align 4
  %and = and i32 %0, 16
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.end13, label %if.then

if.then:                                          ; preds = %entry
  %1 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  call void @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE4setgEPcS4_S4_(%"class.std::__2::basic_streambuf"* %1, i8* null, i8* null, i8* null)
  %__ebs_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 5
  %2 = load i32, i32* %__ebs_, align 4
  %cmp = icmp ugt i32 %2, 8
  br i1 %cmp, label %if.then2, label %if.else10

if.then2:                                         ; preds = %if.then
  %__always_noconv_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 16
  %3 = load i8, i8* %__always_noconv_, align 2
  %tobool3 = trunc i8 %3 to i1
  br i1 %tobool3, label %if.then4, label %if.else

if.then4:                                         ; preds = %if.then2
  %4 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %__extbuf_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 1
  %5 = load i8*, i8** %__extbuf_, align 4
  %__extbuf_5 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 1
  %6 = load i8*, i8** %__extbuf_5, align 4
  %__ebs_6 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 5
  %7 = load i32, i32* %__ebs_6, align 4
  %sub = sub i32 %7, 1
  %add.ptr = getelementptr inbounds i8, i8* %6, i32 %sub
  call void @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE4setpEPcS4_(%"class.std::__2::basic_streambuf"* %4, i8* %5, i8* %add.ptr)
  br label %if.end

if.else:                                          ; preds = %if.then2
  %8 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %__intbuf_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 6
  %9 = load i8*, i8** %__intbuf_, align 4
  %__intbuf_7 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 6
  %10 = load i8*, i8** %__intbuf_7, align 4
  %__ibs_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 7
  %11 = load i32, i32* %__ibs_, align 4
  %sub8 = sub i32 %11, 1
  %add.ptr9 = getelementptr inbounds i8, i8* %10, i32 %sub8
  call void @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE4setpEPcS4_(%"class.std::__2::basic_streambuf"* %8, i8* %9, i8* %add.ptr9)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then4
  br label %if.end11

if.else10:                                        ; preds = %if.then
  %12 = bitcast %"class.std::__2::basic_filebuf"* %this1 to %"class.std::__2::basic_streambuf"*
  call void @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE4setpEPcS4_(%"class.std::__2::basic_streambuf"* %12, i8* null, i8* null)
  br label %if.end11

if.end11:                                         ; preds = %if.else10, %if.end
  %__cm_12 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 13
  store i32 16, i32* %__cm_12, align 4
  br label %if.end13

if.end13:                                         ; preds = %if.end11, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__27codecvtIcc11__mbstate_tE3outERS1_PKcS5_RS5_PcS7_RS7_(%"class.std::__2::codecvt"* %this, %struct.__mbstate_t* nonnull align 4 dereferenceable(8) %__st, i8* %__frm, i8* %__frm_end, i8** nonnull align 4 dereferenceable(4) %__frm_nxt, i8* %__to, i8* %__to_end, i8** nonnull align 4 dereferenceable(4) %__to_nxt) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::codecvt"*, align 4
  %__st.addr = alloca %struct.__mbstate_t*, align 4
  %__frm.addr = alloca i8*, align 4
  %__frm_end.addr = alloca i8*, align 4
  %__frm_nxt.addr = alloca i8**, align 4
  %__to.addr = alloca i8*, align 4
  %__to_end.addr = alloca i8*, align 4
  %__to_nxt.addr = alloca i8**, align 4
  store %"class.std::__2::codecvt"* %this, %"class.std::__2::codecvt"** %this.addr, align 4
  store %struct.__mbstate_t* %__st, %struct.__mbstate_t** %__st.addr, align 4
  store i8* %__frm, i8** %__frm.addr, align 4
  store i8* %__frm_end, i8** %__frm_end.addr, align 4
  store i8** %__frm_nxt, i8*** %__frm_nxt.addr, align 4
  store i8* %__to, i8** %__to.addr, align 4
  store i8* %__to_end, i8** %__to_end.addr, align 4
  store i8** %__to_nxt, i8*** %__to_nxt.addr, align 4
  %this1 = load %"class.std::__2::codecvt"*, %"class.std::__2::codecvt"** %this.addr, align 4
  %0 = load %struct.__mbstate_t*, %struct.__mbstate_t** %__st.addr, align 4
  %1 = load i8*, i8** %__frm.addr, align 4
  %2 = load i8*, i8** %__frm_end.addr, align 4
  %3 = load i8**, i8*** %__frm_nxt.addr, align 4
  %4 = load i8*, i8** %__to.addr, align 4
  %5 = load i8*, i8** %__to_end.addr, align 4
  %6 = load i8**, i8*** %__to_nxt.addr, align 4
  %7 = bitcast %"class.std::__2::codecvt"* %this1 to i32 (%"class.std::__2::codecvt"*, %struct.__mbstate_t*, i8*, i8*, i8**, i8*, i8*, i8**)***
  %vtable = load i32 (%"class.std::__2::codecvt"*, %struct.__mbstate_t*, i8*, i8*, i8**, i8*, i8*, i8**)**, i32 (%"class.std::__2::codecvt"*, %struct.__mbstate_t*, i8*, i8*, i8**, i8*, i8*, i8**)*** %7, align 4
  %vfn = getelementptr inbounds i32 (%"class.std::__2::codecvt"*, %struct.__mbstate_t*, i8*, i8*, i8**, i8*, i8*, i8**)*, i32 (%"class.std::__2::codecvt"*, %struct.__mbstate_t*, i8*, i8*, i8**, i8*, i8*, i8**)** %vtable, i64 3
  %8 = load i32 (%"class.std::__2::codecvt"*, %struct.__mbstate_t*, i8*, i8*, i8**, i8*, i8*, i8**)*, i32 (%"class.std::__2::codecvt"*, %struct.__mbstate_t*, i8*, i8*, i8**, i8*, i8*, i8**)** %vfn, align 4
  %call = call i32 %8(%"class.std::__2::codecvt"* %this1, %struct.__mbstate_t* nonnull align 4 dereferenceable(8) %0, i8* %1, i8* %2, i8** nonnull align 4 dereferenceable(4) %3, i8* %4, i8* %5, i8** nonnull align 4 dereferenceable(4) %6)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::basic_ofstream"* @_ZNSt3__214basic_ofstreamIcNS_11char_traitsIcEEED2Ev(%"class.std::__2::basic_ofstream"* returned %this, i8** %vtt) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_ofstream"*, align 4
  %vtt.addr = alloca i8**, align 4
  store %"class.std::__2::basic_ofstream"* %this, %"class.std::__2::basic_ofstream"** %this.addr, align 4
  store i8** %vtt, i8*** %vtt.addr, align 4
  %this1 = load %"class.std::__2::basic_ofstream"*, %"class.std::__2::basic_ofstream"** %this.addr, align 4
  %vtt2 = load i8**, i8*** %vtt.addr, align 4
  %0 = load i8*, i8** %vtt2, align 4
  %1 = bitcast %"class.std::__2::basic_ofstream"* %this1 to i32 (...)***
  %2 = bitcast i8* %0 to i32 (...)**
  store i32 (...)** %2, i32 (...)*** %1, align 4
  %3 = getelementptr inbounds i8*, i8** %vtt2, i64 3
  %4 = load i8*, i8** %3, align 4
  %5 = bitcast %"class.std::__2::basic_ofstream"* %this1 to i8**
  %vtable = load i8*, i8** %5, align 4
  %vbase.offset.ptr = getelementptr i8, i8* %vtable, i64 -12
  %6 = bitcast i8* %vbase.offset.ptr to i32*
  %vbase.offset = load i32, i32* %6, align 4
  %7 = bitcast %"class.std::__2::basic_ofstream"* %this1 to i8*
  %add.ptr = getelementptr inbounds i8, i8* %7, i32 %vbase.offset
  %8 = bitcast i8* %add.ptr to i32 (...)***
  %9 = bitcast i8* %4 to i32 (...)**
  store i32 (...)** %9, i32 (...)*** %8, align 4
  %__sb_ = getelementptr inbounds %"class.std::__2::basic_ofstream", %"class.std::__2::basic_ofstream"* %this1, i32 0, i32 1
  %call = call %"class.std::__2::basic_filebuf"* @_ZNSt3__213basic_filebufIcNS_11char_traitsIcEEED2Ev(%"class.std::__2::basic_filebuf"* %__sb_) #3
  %10 = bitcast %"class.std::__2::basic_ofstream"* %this1 to %"class.std::__2::basic_ostream"*
  %11 = getelementptr inbounds i8*, i8** %vtt2, i64 1
  %call3 = call %"class.std::__2::basic_ostream"* @_ZNSt3__213basic_ostreamIcNS_11char_traitsIcEEED2Ev(%"class.std::__2::basic_ostream"* %10, i8** %11) #3
  ret %"class.std::__2::basic_ofstream"* %this1
}

; Function Attrs: nounwind
declare %"class.std::__2::basic_ostream"* @_ZNSt3__213basic_ostreamIcNS_11char_traitsIcEEED2Ev(%"class.std::__2::basic_ostream"* returned, i8**) unnamed_addr #2

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__28ios_base5rdbufEv(%"class.std::__2::ios_base"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::ios_base"*, align 4
  store %"class.std::__2::ios_base"* %this, %"class.std::__2::ios_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::ios_base"*, %"class.std::__2::ios_base"** %this.addr, align 4
  %__rdbuf_ = getelementptr inbounds %"class.std::__2::ios_base", %"class.std::__2::ios_base"* %this1, i32 0, i32 6
  %0 = load i8*, i8** %__rdbuf_, align 4
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define internal %"struct.(anonymous namespace)::Tie"* @_ZN12_GLOBAL__N_13TieD2Ev(%"struct.(anonymous namespace)::Tie"* returned %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %"struct.(anonymous namespace)::Tie"*, align 4
  store %"struct.(anonymous namespace)::Tie"* %this, %"struct.(anonymous namespace)::Tie"** %this.addr, align 4
  %this1 = load %"struct.(anonymous namespace)::Tie"*, %"struct.(anonymous namespace)::Tie"** %this.addr, align 4
  %0 = bitcast %"struct.(anonymous namespace)::Tie"* %this1 to %"class.std::__2::basic_streambuf"*
  %call = call %"class.std::__2::basic_streambuf"* @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEED2Ev(%"class.std::__2::basic_streambuf"* %0) #3
  ret %"struct.(anonymous namespace)::Tie"* %this1
}

; Function Attrs: noinline nounwind optnone
define internal void @_ZN12_GLOBAL__N_13TieD0Ev(%"struct.(anonymous namespace)::Tie"* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %"struct.(anonymous namespace)::Tie"*, align 4
  store %"struct.(anonymous namespace)::Tie"* %this, %"struct.(anonymous namespace)::Tie"** %this.addr, align 4
  %this1 = load %"struct.(anonymous namespace)::Tie"*, %"struct.(anonymous namespace)::Tie"** %this.addr, align 4
  %call = call %"struct.(anonymous namespace)::Tie"* @_ZN12_GLOBAL__N_13TieD2Ev(%"struct.(anonymous namespace)::Tie"* %this1) #3
  %0 = bitcast %"struct.(anonymous namespace)::Tie"* %this1 to i8*
  call void @_ZdlPv(i8* %0) #11
  ret void
}

declare void @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE7seekoffExNS_8ios_base7seekdirEj(%"class.std::__2::fpos"* sret align 8, %"class.std::__2::basic_streambuf"*, i64, i32, i32) unnamed_addr #4

declare void @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE7seekposENS_4fposI11__mbstate_tEEj(%"class.std::__2::fpos"* sret align 8, %"class.std::__2::basic_streambuf"*, %"class.std::__2::fpos"* byval(%"class.std::__2::fpos") align 8, i32) unnamed_addr #4

; Function Attrs: noinline nounwind optnone
define internal i32 @_ZN12_GLOBAL__N_13Tie4syncEv(%"struct.(anonymous namespace)::Tie"* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %"struct.(anonymous namespace)::Tie"*, align 4
  store %"struct.(anonymous namespace)::Tie"* %this, %"struct.(anonymous namespace)::Tie"** %this.addr, align 4
  %this1 = load %"struct.(anonymous namespace)::Tie"*, %"struct.(anonymous namespace)::Tie"** %this.addr, align 4
  %logBuf = getelementptr inbounds %"struct.(anonymous namespace)::Tie", %"struct.(anonymous namespace)::Tie"* %this1, i32 0, i32 2
  %0 = load %"class.std::__2::basic_streambuf"*, %"class.std::__2::basic_streambuf"** %logBuf, align 4
  %call = call i32 @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE7pubsyncEv(%"class.std::__2::basic_streambuf"* %0)
  %buf = getelementptr inbounds %"struct.(anonymous namespace)::Tie", %"struct.(anonymous namespace)::Tie"* %this1, i32 0, i32 1
  %1 = load %"class.std::__2::basic_streambuf"*, %"class.std::__2::basic_streambuf"** %buf, align 4
  %call2 = call i32 @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE7pubsyncEv(%"class.std::__2::basic_streambuf"* %1)
  ret i32 %call2
}

; Function Attrs: noinline nounwind optnone
define internal i32 @_ZN12_GLOBAL__N_13Tie9underflowEv(%"struct.(anonymous namespace)::Tie"* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %"struct.(anonymous namespace)::Tie"*, align 4
  store %"struct.(anonymous namespace)::Tie"* %this, %"struct.(anonymous namespace)::Tie"** %this.addr, align 4
  %this1 = load %"struct.(anonymous namespace)::Tie"*, %"struct.(anonymous namespace)::Tie"** %this.addr, align 4
  %buf = getelementptr inbounds %"struct.(anonymous namespace)::Tie", %"struct.(anonymous namespace)::Tie"* %this1, i32 0, i32 1
  %0 = load %"class.std::__2::basic_streambuf"*, %"class.std::__2::basic_streambuf"** %buf, align 4
  %call = call i32 @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE5sgetcEv(%"class.std::__2::basic_streambuf"* %0)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define internal i32 @_ZN12_GLOBAL__N_13Tie5uflowEv(%"struct.(anonymous namespace)::Tie"* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %"struct.(anonymous namespace)::Tie"*, align 4
  store %"struct.(anonymous namespace)::Tie"* %this, %"struct.(anonymous namespace)::Tie"** %this.addr, align 4
  %this1 = load %"struct.(anonymous namespace)::Tie"*, %"struct.(anonymous namespace)::Tie"** %this.addr, align 4
  %buf = getelementptr inbounds %"struct.(anonymous namespace)::Tie", %"struct.(anonymous namespace)::Tie"* %this1, i32 0, i32 1
  %0 = load %"class.std::__2::basic_streambuf"*, %"class.std::__2::basic_streambuf"** %buf, align 4
  %call = call i32 @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE6sbumpcEv(%"class.std::__2::basic_streambuf"* %0)
  %call2 = call i32 @_ZN12_GLOBAL__N_13Tie3logEiPKc(%"struct.(anonymous namespace)::Tie"* %this1, i32 %call, i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.13, i32 0, i32 0))
  ret i32 %call2
}

declare i32 @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE9pbackfailEi(%"class.std::__2::basic_streambuf"*, i32) unnamed_addr #4

; Function Attrs: noinline nounwind optnone
define internal i32 @_ZN12_GLOBAL__N_13Tie8overflowEi(%"struct.(anonymous namespace)::Tie"* %this, i32 %c) unnamed_addr #1 {
entry:
  %this.addr = alloca %"struct.(anonymous namespace)::Tie"*, align 4
  %c.addr = alloca i32, align 4
  store %"struct.(anonymous namespace)::Tie"* %this, %"struct.(anonymous namespace)::Tie"** %this.addr, align 4
  store i32 %c, i32* %c.addr, align 4
  %this1 = load %"struct.(anonymous namespace)::Tie"*, %"struct.(anonymous namespace)::Tie"** %this.addr, align 4
  %buf = getelementptr inbounds %"struct.(anonymous namespace)::Tie", %"struct.(anonymous namespace)::Tie"* %this1, i32 0, i32 1
  %0 = load %"class.std::__2::basic_streambuf"*, %"class.std::__2::basic_streambuf"** %buf, align 4
  %1 = load i32, i32* %c.addr, align 4
  %conv = trunc i32 %1 to i8
  %call = call i32 @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE5sputcEc(%"class.std::__2::basic_streambuf"* %0, i8 signext %conv)
  %call2 = call i32 @_ZN12_GLOBAL__N_13Tie3logEiPKc(%"struct.(anonymous namespace)::Tie"* %this1, i32 %call, i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.14, i32 0, i32 0))
  ret i32 %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE7pubsyncEv(%"class.std::__2::basic_streambuf"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_streambuf"*, align 4
  store %"class.std::__2::basic_streambuf"* %this, %"class.std::__2::basic_streambuf"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_streambuf"*, %"class.std::__2::basic_streambuf"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::basic_streambuf"* %this1 to i32 (%"class.std::__2::basic_streambuf"*)***
  %vtable = load i32 (%"class.std::__2::basic_streambuf"*)**, i32 (%"class.std::__2::basic_streambuf"*)*** %0, align 4
  %vfn = getelementptr inbounds i32 (%"class.std::__2::basic_streambuf"*)*, i32 (%"class.std::__2::basic_streambuf"*)** %vtable, i64 6
  %1 = load i32 (%"class.std::__2::basic_streambuf"*)*, i32 (%"class.std::__2::basic_streambuf"*)** %vfn, align 4
  %call = call i32 %1(%"class.std::__2::basic_streambuf"* %this1)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE5sgetcEv(%"class.std::__2::basic_streambuf"* %this) #1 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %"class.std::__2::basic_streambuf"*, align 4
  store %"class.std::__2::basic_streambuf"* %this, %"class.std::__2::basic_streambuf"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_streambuf"*, %"class.std::__2::basic_streambuf"** %this.addr, align 4
  %__ninp_ = getelementptr inbounds %"class.std::__2::basic_streambuf", %"class.std::__2::basic_streambuf"* %this1, i32 0, i32 3
  %0 = load i8*, i8** %__ninp_, align 4
  %__einp_ = getelementptr inbounds %"class.std::__2::basic_streambuf", %"class.std::__2::basic_streambuf"* %this1, i32 0, i32 4
  %1 = load i8*, i8** %__einp_, align 4
  %cmp = icmp eq i8* %0, %1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = bitcast %"class.std::__2::basic_streambuf"* %this1 to i32 (%"class.std::__2::basic_streambuf"*)***
  %vtable = load i32 (%"class.std::__2::basic_streambuf"*)**, i32 (%"class.std::__2::basic_streambuf"*)*** %2, align 4
  %vfn = getelementptr inbounds i32 (%"class.std::__2::basic_streambuf"*)*, i32 (%"class.std::__2::basic_streambuf"*)** %vtable, i64 9
  %3 = load i32 (%"class.std::__2::basic_streambuf"*)*, i32 (%"class.std::__2::basic_streambuf"*)** %vfn, align 4
  %call = call i32 %3(%"class.std::__2::basic_streambuf"* %this1)
  store i32 %call, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %__ninp_2 = getelementptr inbounds %"class.std::__2::basic_streambuf", %"class.std::__2::basic_streambuf"* %this1, i32 0, i32 3
  %4 = load i8*, i8** %__ninp_2, align 4
  %5 = load i8, i8* %4, align 1
  %call3 = call i32 @_ZNSt3__211char_traitsIcE11to_int_typeEc(i8 signext %5) #3
  store i32 %call3, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %6 = load i32, i32* %retval, align 4
  ret i32 %6
}

; Function Attrs: noinline nounwind optnone
define internal i32 @_ZN12_GLOBAL__N_13Tie3logEiPKc(%"struct.(anonymous namespace)::Tie"* %this, i32 %c, i8* %prefix) #1 {
entry:
  %this.addr = alloca %"struct.(anonymous namespace)::Tie"*, align 4
  %c.addr = alloca i32, align 4
  %prefix.addr = alloca i8*, align 4
  store %"struct.(anonymous namespace)::Tie"* %this, %"struct.(anonymous namespace)::Tie"** %this.addr, align 4
  store i32 %c, i32* %c.addr, align 4
  store i8* %prefix, i8** %prefix.addr, align 4
  %this1 = load %"struct.(anonymous namespace)::Tie"*, %"struct.(anonymous namespace)::Tie"** %this.addr, align 4
  %0 = load i32, i32* @_ZZN12_GLOBAL__N_13Tie3logEiPKcE4last, align 4
  %cmp = icmp eq i32 %0, 10
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %logBuf = getelementptr inbounds %"struct.(anonymous namespace)::Tie", %"struct.(anonymous namespace)::Tie"* %this1, i32 0, i32 2
  %1 = load %"class.std::__2::basic_streambuf"*, %"class.std::__2::basic_streambuf"** %logBuf, align 4
  %2 = load i8*, i8** %prefix.addr, align 4
  %call = call i32 @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE5sputnEPKcl(%"class.std::__2::basic_streambuf"* %1, i8* %2, i32 3)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %logBuf2 = getelementptr inbounds %"struct.(anonymous namespace)::Tie", %"struct.(anonymous namespace)::Tie"* %this1, i32 0, i32 2
  %3 = load %"class.std::__2::basic_streambuf"*, %"class.std::__2::basic_streambuf"** %logBuf2, align 4
  %4 = load i32, i32* %c.addr, align 4
  %conv = trunc i32 %4 to i8
  %call3 = call i32 @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE5sputcEc(%"class.std::__2::basic_streambuf"* %3, i8 signext %conv)
  store i32 %call3, i32* @_ZZN12_GLOBAL__N_13Tie3logEiPKcE4last, align 4
  ret i32 %call3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE6sbumpcEv(%"class.std::__2::basic_streambuf"* %this) #1 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %"class.std::__2::basic_streambuf"*, align 4
  store %"class.std::__2::basic_streambuf"* %this, %"class.std::__2::basic_streambuf"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_streambuf"*, %"class.std::__2::basic_streambuf"** %this.addr, align 4
  %__ninp_ = getelementptr inbounds %"class.std::__2::basic_streambuf", %"class.std::__2::basic_streambuf"* %this1, i32 0, i32 3
  %0 = load i8*, i8** %__ninp_, align 4
  %__einp_ = getelementptr inbounds %"class.std::__2::basic_streambuf", %"class.std::__2::basic_streambuf"* %this1, i32 0, i32 4
  %1 = load i8*, i8** %__einp_, align 4
  %cmp = icmp eq i8* %0, %1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = bitcast %"class.std::__2::basic_streambuf"* %this1 to i32 (%"class.std::__2::basic_streambuf"*)***
  %vtable = load i32 (%"class.std::__2::basic_streambuf"*)**, i32 (%"class.std::__2::basic_streambuf"*)*** %2, align 4
  %vfn = getelementptr inbounds i32 (%"class.std::__2::basic_streambuf"*)*, i32 (%"class.std::__2::basic_streambuf"*)** %vtable, i64 10
  %3 = load i32 (%"class.std::__2::basic_streambuf"*)*, i32 (%"class.std::__2::basic_streambuf"*)** %vfn, align 4
  %call = call i32 %3(%"class.std::__2::basic_streambuf"* %this1)
  store i32 %call, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %__ninp_2 = getelementptr inbounds %"class.std::__2::basic_streambuf", %"class.std::__2::basic_streambuf"* %this1, i32 0, i32 3
  %4 = load i8*, i8** %__ninp_2, align 4
  %incdec.ptr = getelementptr inbounds i8, i8* %4, i32 1
  store i8* %incdec.ptr, i8** %__ninp_2, align 4
  %5 = load i8, i8* %4, align 1
  %call3 = call i32 @_ZNSt3__211char_traitsIcE11to_int_typeEc(i8 signext %5) #3
  store i32 %call3, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %6 = load i32, i32* %retval, align 4
  ret i32 %6
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE5sputnEPKcl(%"class.std::__2::basic_streambuf"* %this, i8* %__s, i32 %__n) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_streambuf"*, align 4
  %__s.addr = alloca i8*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::basic_streambuf"* %this, %"class.std::__2::basic_streambuf"** %this.addr, align 4
  store i8* %__s, i8** %__s.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::basic_streambuf"*, %"class.std::__2::basic_streambuf"** %this.addr, align 4
  %0 = load i8*, i8** %__s.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %2 = bitcast %"class.std::__2::basic_streambuf"* %this1 to i32 (%"class.std::__2::basic_streambuf"*, i8*, i32)***
  %vtable = load i32 (%"class.std::__2::basic_streambuf"*, i8*, i32)**, i32 (%"class.std::__2::basic_streambuf"*, i8*, i32)*** %2, align 4
  %vfn = getelementptr inbounds i32 (%"class.std::__2::basic_streambuf"*, i8*, i32)*, i32 (%"class.std::__2::basic_streambuf"*, i8*, i32)** %vtable, i64 12
  %3 = load i32 (%"class.std::__2::basic_streambuf"*, i8*, i32)*, i32 (%"class.std::__2::basic_streambuf"*, i8*, i32)** %vfn, align 4
  %call = call i32 %3(%"class.std::__2::basic_streambuf"* %this1, i8* %0, i32 %1)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__213basic_filebufIcNS_11char_traitsIcEEE7is_openEv(%"class.std::__2::basic_filebuf"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_filebuf"*, align 4
  store %"class.std::__2::basic_filebuf"* %this, %"class.std::__2::basic_filebuf"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_filebuf"*, %"class.std::__2::basic_filebuf"** %this.addr, align 4
  %__file_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 8
  %0 = load %struct._IO_FILE*, %struct._IO_FILE** %__file_, align 4
  %cmp = icmp ne %struct._IO_FILE* %0, null
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::basic_filebuf"* @_ZNSt3__213basic_filebufIcNS_11char_traitsIcEEE4openERKNS_12basic_stringIcS2_NS_9allocatorIcEEEEj(%"class.std::__2::basic_filebuf"* %this, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %__s, i32 %__mode) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_filebuf"*, align 4
  %__s.addr = alloca %"class.std::__2::basic_string"*, align 4
  %__mode.addr = alloca i32, align 4
  store %"class.std::__2::basic_filebuf"* %this, %"class.std::__2::basic_filebuf"** %this.addr, align 4
  store %"class.std::__2::basic_string"* %__s, %"class.std::__2::basic_string"** %__s.addr, align 4
  store i32 %__mode, i32* %__mode.addr, align 4
  %this1 = load %"class.std::__2::basic_filebuf"*, %"class.std::__2::basic_filebuf"** %this.addr, align 4
  %0 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__s.addr, align 4
  %call = call i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5c_strEv(%"class.std::__2::basic_string"* %0) #3
  %1 = load i32, i32* %__mode.addr, align 4
  %call2 = call %"class.std::__2::basic_filebuf"* @_ZNSt3__213basic_filebufIcNS_11char_traitsIcEEE4openEPKcj(%"class.std::__2::basic_filebuf"* %this1, i8* %call, i32 %1)
  ret %"class.std::__2::basic_filebuf"* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29basic_iosIcNS_11char_traitsIcEEE5clearEj(%"class.std::__2::basic_ios"* %this, i32 %__state) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_ios"*, align 4
  %__state.addr = alloca i32, align 4
  store %"class.std::__2::basic_ios"* %this, %"class.std::__2::basic_ios"** %this.addr, align 4
  store i32 %__state, i32* %__state.addr, align 4
  %this1 = load %"class.std::__2::basic_ios"*, %"class.std::__2::basic_ios"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::basic_ios"* %this1 to %"class.std::__2::ios_base"*
  %1 = load i32, i32* %__state.addr, align 4
  call void @_ZNSt3__28ios_base5clearEj(%"class.std::__2::ios_base"* %0, i32 %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29basic_iosIcNS_11char_traitsIcEEE8setstateEj(%"class.std::__2::basic_ios"* %this, i32 %__state) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_ios"*, align 4
  %__state.addr = alloca i32, align 4
  store %"class.std::__2::basic_ios"* %this, %"class.std::__2::basic_ios"** %this.addr, align 4
  store i32 %__state, i32* %__state.addr, align 4
  %this1 = load %"class.std::__2::basic_ios"*, %"class.std::__2::basic_ios"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::basic_ios"* %this1 to %"class.std::__2::ios_base"*
  %1 = load i32, i32* %__state.addr, align 4
  call void @_ZNSt3__28ios_base8setstateEj(%"class.std::__2::ios_base"* %0, i32 %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::basic_filebuf"* @_ZNSt3__213basic_filebufIcNS_11char_traitsIcEEE4openEPKcj(%"class.std::__2::basic_filebuf"* %this, i8* %__s, i32 %__mode) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_filebuf"*, align 4
  %__s.addr = alloca i8*, align 4
  %__mode.addr = alloca i32, align 4
  %__rt = alloca %"class.std::__2::basic_filebuf"*, align 4
  %__mdstr = alloca i8*, align 4
  store %"class.std::__2::basic_filebuf"* %this, %"class.std::__2::basic_filebuf"** %this.addr, align 4
  store i8* %__s, i8** %__s.addr, align 4
  store i32 %__mode, i32* %__mode.addr, align 4
  %this1 = load %"class.std::__2::basic_filebuf"*, %"class.std::__2::basic_filebuf"** %this.addr, align 4
  store %"class.std::__2::basic_filebuf"* null, %"class.std::__2::basic_filebuf"** %__rt, align 4
  %__file_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 8
  %0 = load %struct._IO_FILE*, %struct._IO_FILE** %__file_, align 4
  %cmp = icmp eq %struct._IO_FILE* %0, null
  br i1 %cmp, label %if.then, label %if.end20

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %__mode.addr, align 4
  %call = call i8* @_ZNSt3__213basic_filebufIcNS_11char_traitsIcEEE15__make_mdstringEj(i32 %1) #3
  store i8* %call, i8** %__mdstr, align 4
  %2 = load i8*, i8** %__mdstr, align 4
  %tobool = icmp ne i8* %2, null
  br i1 %tobool, label %if.then2, label %if.end19

if.then2:                                         ; preds = %if.then
  store %"class.std::__2::basic_filebuf"* %this1, %"class.std::__2::basic_filebuf"** %__rt, align 4
  %3 = load i8*, i8** %__s.addr, align 4
  %4 = load i8*, i8** %__mdstr, align 4
  %call3 = call %struct._IO_FILE* @fopen(i8* %3, i8* %4)
  %__file_4 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 8
  store %struct._IO_FILE* %call3, %struct._IO_FILE** %__file_4, align 4
  %__file_5 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 8
  %5 = load %struct._IO_FILE*, %struct._IO_FILE** %__file_5, align 4
  %tobool6 = icmp ne %struct._IO_FILE* %5, null
  br i1 %tobool6, label %if.then7, label %if.else

if.then7:                                         ; preds = %if.then2
  %6 = load i32, i32* %__mode.addr, align 4
  %__om_ = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 12
  store i32 %6, i32* %__om_, align 4
  %7 = load i32, i32* %__mode.addr, align 4
  %and = and i32 %7, 2
  %tobool8 = icmp ne i32 %and, 0
  br i1 %tobool8, label %if.then9, label %if.end17

if.then9:                                         ; preds = %if.then7
  %__file_10 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 8
  %8 = load %struct._IO_FILE*, %struct._IO_FILE** %__file_10, align 4
  %call11 = call i32 @fseek(%struct._IO_FILE* %8, i32 0, i32 2)
  %tobool12 = icmp ne i32 %call11, 0
  br i1 %tobool12, label %if.then13, label %if.end

if.then13:                                        ; preds = %if.then9
  %__file_14 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 8
  %9 = load %struct._IO_FILE*, %struct._IO_FILE** %__file_14, align 4
  %call15 = call i32 @fclose(%struct._IO_FILE* %9)
  %__file_16 = getelementptr inbounds %"class.std::__2::basic_filebuf", %"class.std::__2::basic_filebuf"* %this1, i32 0, i32 8
  store %struct._IO_FILE* null, %struct._IO_FILE** %__file_16, align 4
  store %"class.std::__2::basic_filebuf"* null, %"class.std::__2::basic_filebuf"** %__rt, align 4
  br label %if.end

if.end:                                           ; preds = %if.then13, %if.then9
  br label %if.end17

if.end17:                                         ; preds = %if.end, %if.then7
  br label %if.end18

if.else:                                          ; preds = %if.then2
  store %"class.std::__2::basic_filebuf"* null, %"class.std::__2::basic_filebuf"** %__rt, align 4
  br label %if.end18

if.end18:                                         ; preds = %if.else, %if.end17
  br label %if.end19

if.end19:                                         ; preds = %if.end18, %if.then
  br label %if.end20

if.end20:                                         ; preds = %if.end19, %entry
  %10 = load %"class.std::__2::basic_filebuf"*, %"class.std::__2::basic_filebuf"** %__rt, align 4
  ret %"class.std::__2::basic_filebuf"* %10
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5c_strEv(%"class.std::__2::basic_string"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %call = call i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv(%"class.std::__2::basic_string"* %this1) #3
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__213basic_filebufIcNS_11char_traitsIcEEE15__make_mdstringEj(i32 %__mode) #1 comdat {
entry:
  %retval = alloca i8*, align 4
  %__mode.addr = alloca i32, align 4
  store i32 %__mode, i32* %__mode.addr, align 4
  %0 = load i32, i32* %__mode.addr, align 4
  %and = and i32 %0, -3
  switch i32 %and, label %sw.default [
    i32 16, label %sw.bb
    i32 48, label %sw.bb
    i32 17, label %sw.bb1
    i32 1, label %sw.bb1
    i32 8, label %sw.bb2
    i32 24, label %sw.bb3
    i32 56, label %sw.bb4
    i32 25, label %sw.bb5
    i32 9, label %sw.bb5
    i32 20, label %sw.bb6
    i32 52, label %sw.bb6
    i32 21, label %sw.bb7
    i32 5, label %sw.bb7
    i32 12, label %sw.bb8
    i32 28, label %sw.bb9
    i32 60, label %sw.bb10
    i32 29, label %sw.bb11
    i32 13, label %sw.bb11
  ]

sw.bb:                                            ; preds = %entry, %entry
  store i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.15, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb1:                                           ; preds = %entry, %entry
  store i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.16, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb2:                                           ; preds = %entry
  store i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.17, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb3:                                           ; preds = %entry
  store i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.18, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb4:                                           ; preds = %entry
  store i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.19, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb5:                                           ; preds = %entry, %entry
  store i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.20, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb6:                                           ; preds = %entry, %entry
  store i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.21, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb7:                                           ; preds = %entry, %entry
  store i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.22, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb8:                                           ; preds = %entry
  store i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.23, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb9:                                           ; preds = %entry
  store i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.24, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb10:                                          ; preds = %entry
  store i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.25, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.bb11:                                          ; preds = %entry, %entry
  store i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.26, i32 0, i32 0), i8** %retval, align 4
  br label %return

sw.default:                                       ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %sw.default, %sw.bb11, %sw.bb10, %sw.bb9, %sw.bb8, %sw.bb7, %sw.bb6, %sw.bb5, %sw.bb4, %sw.bb3, %sw.bb2, %sw.bb1, %sw.bb
  %1 = load i8*, i8** %retval, align 4
  ret i8* %1
}

declare %struct._IO_FILE* @fopen(i8*, i8*) #4

declare i32 @fseek(%struct._IO_FILE*, i32, i32) #4

declare void @_ZNSt3__28ios_base5clearEj(%"class.std::__2::ios_base"*, i32) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__28ios_base8setstateEj(%"class.std::__2::ios_base"* %this, i32 %__state) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::ios_base"*, align 4
  %__state.addr = alloca i32, align 4
  store %"class.std::__2::ios_base"* %this, %"class.std::__2::ios_base"** %this.addr, align 4
  store i32 %__state, i32* %__state.addr, align 4
  %this1 = load %"class.std::__2::ios_base"*, %"class.std::__2::ios_base"** %this.addr, align 4
  %__rdstate_ = getelementptr inbounds %"class.std::__2::ios_base", %"class.std::__2::ios_base"* %this1, i32 0, i32 4
  %0 = load i32, i32* %__rdstate_, align 4
  %1 = load i32, i32* %__state.addr, align 4
  %or = or i32 %0, %1
  call void @_ZNSt3__28ios_base5clearEj(%"class.std::__2::ios_base"* %this1, i32 %or)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__28ios_base5rdbufEPv(%"class.std::__2::ios_base"* %this, i8* %__sb) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::ios_base"*, align 4
  %__sb.addr = alloca i8*, align 4
  store %"class.std::__2::ios_base"* %this, %"class.std::__2::ios_base"** %this.addr, align 4
  store i8* %__sb, i8** %__sb.addr, align 4
  %this1 = load %"class.std::__2::ios_base"*, %"class.std::__2::ios_base"** %this.addr, align 4
  %0 = load i8*, i8** %__sb.addr, align 4
  %__rdbuf_ = getelementptr inbounds %"class.std::__2::ios_base", %"class.std::__2::ios_base"* %this1, i32 0, i32 6
  store i8* %0, i8** %__rdbuf_, align 4
  call void @_ZNSt3__28ios_base5clearEj(%"class.std::__2::ios_base"* %this1, i32 0)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair"* @_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_EC2INS_18__default_init_tagESA_EEOT_OT0_(%"class.std::__2::__compressed_pair"* returned %this, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  %__t1.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  %agg.tmp3 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t1, %"struct.std::__2::__default_init_tag"** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %1 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t1.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %1) #3
  %call2 = call %"struct.std::__2::__compressed_pair_elem"* @_ZNSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem"* %0)
  %2 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem.0"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call4 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #3
  %call5 = call %"struct.std::__2::__compressed_pair_elem.0"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIcEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.0"* %2)
  ret %"class.std::__2::__compressed_pair"* %this1
}

declare void @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEPKcm(%"class.std::__2::basic_string"*, i8*, i32) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__211char_traitsIcE6lengthEPKc(i8* %__s) #1 comdat {
entry:
  %__s.addr = alloca i8*, align 4
  store i8* %__s, i8** %__s.addr, align 4
  %0 = load i8*, i8** %__s.addr, align 4
  %call = call i32 @strlen(i8* %0) #3
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t) #1 comdat {
entry:
  %__t.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  store %"struct.std::__2::__default_init_tag"* %__t, %"struct.std::__2::__default_init_tag"** %__t.addr, align 4
  %0 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t.addr, align 4
  ret %"struct.std::__2::__default_init_tag"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem"* @_ZNSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem"* returned %this) unnamed_addr #1 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem"*, align 4
  store %"struct.std::__2::__compressed_pair_elem"* %this, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem"*, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem"* %this1, i32 0, i32 0
  ret %"struct.std::__2::__compressed_pair_elem"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.0"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIcEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.0"* returned %this) unnamed_addr #1 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.0"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.0"* %this, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.0"*, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.0"* %this1 to %"class.std::__2::allocator"*
  %call = call %"class.std::__2::allocator"* @_ZNSt3__29allocatorIcEC2Ev(%"class.std::__2::allocator"* %1) #3
  ret %"struct.std::__2::__compressed_pair_elem.0"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::allocator"* @_ZNSt3__29allocatorIcEC2Ev(%"class.std::__2::allocator"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  ret %"class.std::__2::allocator"* %this1
}

; Function Attrs: nounwind
declare i32 @strlen(i8*) #2

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base"* @_ZNSt3__213__vector_baseINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEEC2Ev(%"class.std::__2::__vector_base"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  %ref.tmp = alloca i8*, align 4
  %ref.tmp2 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__vector_base"* %this1 to %"class.std::__2::__vector_base_common"*
  %call = call %"class.std::__2::__vector_base_common"* @_ZNSt3__220__vector_base_commonILb1EEC2Ev(%"class.std::__2::__vector_base_common"* %0)
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  store %"class.std::__2::basic_string"* null, %"class.std::__2::basic_string"** %__begin_, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 1
  store %"class.std::__2::basic_string"* null, %"class.std::__2::basic_string"** %__end_, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  store i8* null, i8** %ref.tmp, align 4
  %call3 = call %"class.std::__2::__compressed_pair.1"* @_ZNSt3__217__compressed_pairIPNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEEC2IDnNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.1"* %__end_cap_, i8** nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  ret %"class.std::__2::__vector_base"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt16initializer_listINSt3__212basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEEE4sizeEv(%"class.std::initializer_list"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.std::initializer_list"*, align 4
  store %"class.std::initializer_list"* %this, %"class.std::initializer_list"** %this.addr, align 4
  %this1 = load %"class.std::initializer_list"*, %"class.std::initializer_list"** %this.addr, align 4
  %__size_ = getelementptr inbounds %"class.std::initializer_list", %"class.std::initializer_list"* %this1, i32 0, i32 1
  %0 = load i32, i32* %__size_, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE11__vallocateEm(%"class.std::__2::vector"* %this, i32 %__n) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE8max_sizeEv(%"class.std::__2::vector"* %this1) #3
  %cmp = icmp ugt i32 %0, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base_common"*
  call void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"* %1) #13
  unreachable

if.end:                                           ; preds = %entry
  %2 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.4"* @_ZNSt3__213__vector_baseINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE7__allocEv(%"class.std::__2::__vector_base"* %2) #3
  %3 = load i32, i32* %__n.addr, align 4
  %call3 = call %"class.std::__2::basic_string"* @_ZNSt3__216allocator_traitsINS_9allocatorINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEEEE8allocateERS7_m(%"class.std::__2::allocator.4"* nonnull align 1 dereferenceable(1) %call2, i32 %3)
  %4 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %4, i32 0, i32 1
  store %"class.std::__2::basic_string"* %call3, %"class.std::__2::basic_string"** %__end_, align 4
  %5 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %5, i32 0, i32 0
  store %"class.std::__2::basic_string"* %call3, %"class.std::__2::basic_string"** %__begin_, align 4
  %6 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_4 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %6, i32 0, i32 0
  %7 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__begin_4, align 4
  %8 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %7, i32 %8
  %9 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call5 = call nonnull align 4 dereferenceable(4) %"class.std::__2::basic_string"** @_ZNSt3__213__vector_baseINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE9__end_capEv(%"class.std::__2::__vector_base"* %9) #3
  store %"class.std::__2::basic_string"* %add.ptr, %"class.std::__2::basic_string"** %call5, align 4
  call void @_ZNKSt3__26vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE14__annotate_newEm(%"class.std::__2::vector"* %this1, i32 0) #3
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE18__construct_at_endIPKS6_EENS_9enable_ifIXsr27__is_cpp17_forward_iteratorIT_EE5valueEvE4typeESD_SD_m(%"class.std::__2::vector"* %this, %"class.std::__2::basic_string"* %__first, %"class.std::__2::basic_string"* %__last, i32 %__n) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__first.addr = alloca %"class.std::__2::basic_string"*, align 4
  %__last.addr = alloca %"class.std::__2::basic_string"*, align 4
  %__n.addr = alloca i32, align 4
  %__tx = alloca %"struct.std::__2::vector<std::__2::basic_string<char>, std::__2::allocator<std::__2::basic_string<char>>>::_ConstructTransaction", align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store %"class.std::__2::basic_string"* %__first, %"class.std::__2::basic_string"** %__first.addr, align 4
  store %"class.std::__2::basic_string"* %__last, %"class.std::__2::basic_string"** %__last.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %call = call %"struct.std::__2::vector<std::__2::basic_string<char>, std::__2::allocator<std::__2::basic_string<char>>>::_ConstructTransaction"* @_ZNSt3__26vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE21_ConstructTransactionC2ERS8_m(%"struct.std::__2::vector<std::__2::basic_string<char>, std::__2::allocator<std::__2::basic_string<char>>>::_ConstructTransaction"* %__tx, %"class.std::__2::vector"* nonnull align 4 dereferenceable(12) %this1, i32 %0)
  %1 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.4"* @_ZNSt3__213__vector_baseINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE7__allocEv(%"class.std::__2::__vector_base"* %1) #3
  %2 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__first.addr, align 4
  %3 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__last.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<std::__2::basic_string<char>, std::__2::allocator<std::__2::basic_string<char>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::basic_string<char>, std::__2::allocator<std::__2::basic_string<char>>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEEEE25__construct_range_forwardIPKS6_PS6_EEvRS7_T_SE_RT0_(%"class.std::__2::allocator.4"* nonnull align 1 dereferenceable(1) %call2, %"class.std::__2::basic_string"* %2, %"class.std::__2::basic_string"* %3, %"class.std::__2::basic_string"** nonnull align 4 dereferenceable(4) %__pos_)
  %call3 = call %"struct.std::__2::vector<std::__2::basic_string<char>, std::__2::allocator<std::__2::basic_string<char>>>::_ConstructTransaction"* @_ZNSt3__26vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<std::__2::basic_string<char>, std::__2::allocator<std::__2::basic_string<char>>>::_ConstructTransaction"* %__tx) #3
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::basic_string"* @_ZNKSt16initializer_listINSt3__212basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEEE5beginEv(%"class.std::initializer_list"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.std::initializer_list"*, align 4
  store %"class.std::initializer_list"* %this, %"class.std::initializer_list"** %this.addr, align 4
  %this1 = load %"class.std::initializer_list"*, %"class.std::initializer_list"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::initializer_list", %"class.std::initializer_list"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__begin_, align 4
  ret %"class.std::__2::basic_string"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::basic_string"* @_ZNKSt16initializer_listINSt3__212basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEEE3endEv(%"class.std::initializer_list"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.std::initializer_list"*, align 4
  store %"class.std::initializer_list"* %this, %"class.std::initializer_list"** %this.addr, align 4
  %this1 = load %"class.std::initializer_list"*, %"class.std::initializer_list"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::initializer_list", %"class.std::initializer_list"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__begin_, align 4
  %__size_ = getelementptr inbounds %"class.std::initializer_list", %"class.std::initializer_list"* %this1, i32 0, i32 1
  %1 = load i32, i32* %__size_, align 4
  %add.ptr = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %0, i32 %1
  ret %"class.std::__2::basic_string"* %add.ptr
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base_common"* @_ZNSt3__220__vector_base_commonILb1EEC2Ev(%"class.std::__2::__vector_base_common"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base_common"*, align 4
  store %"class.std::__2::__vector_base_common"* %this, %"class.std::__2::__vector_base_common"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base_common"*, %"class.std::__2::__vector_base_common"** %this.addr, align 4
  ret %"class.std::__2::__vector_base_common"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.1"* @_ZNSt3__217__compressed_pairIPNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEEC2IDnNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.1"* returned %this, i8** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.1"*, align 4
  %__t1.addr = alloca i8**, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.1"* %this, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  store i8** %__t1, i8*** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.1"*, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.1"* %this1 to %"struct.std::__2::__compressed_pair_elem.2"*
  %1 = load i8**, i8*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %1) #3
  %call2 = call %"struct.std::__2::__compressed_pair_elem.2"* @_ZNSt3__222__compressed_pair_elemIPNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEELi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.2"* %0, i8** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.1"* %this1 to %"struct.std::__2::__compressed_pair_elem.3"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #3
  %call4 = call %"struct.std::__2::__compressed_pair_elem.3"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.3"* %2)
  ret %"class.std::__2::__compressed_pair.1"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %__t) #1 comdat {
entry:
  %__t.addr = alloca i8**, align 4
  store i8** %__t, i8*** %__t.addr, align 4
  %0 = load i8**, i8*** %__t.addr, align 4
  ret i8** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.2"* @_ZNSt3__222__compressed_pair_elemIPNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEELi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.2"* returned %this, i8** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.2"*, align 4
  %__u.addr = alloca i8**, align 4
  store %"struct.std::__2::__compressed_pair_elem.2"* %this, %"struct.std::__2::__compressed_pair_elem.2"** %this.addr, align 4
  store i8** %__u, i8*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.2"*, %"struct.std::__2::__compressed_pair_elem.2"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.2", %"struct.std::__2::__compressed_pair_elem.2"* %this1, i32 0, i32 0
  %0 = load i8**, i8*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %0) #3
  store %"class.std::__2::basic_string"* null, %"class.std::__2::basic_string"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.2"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.3"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.3"* returned %this) unnamed_addr #1 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.3"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.3"* %this, %"struct.std::__2::__compressed_pair_elem.3"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.3"*, %"struct.std::__2::__compressed_pair_elem.3"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.3"* %this1 to %"class.std::__2::allocator.4"*
  %call = call %"class.std::__2::allocator.4"* @_ZNSt3__29allocatorINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEEEC2Ev(%"class.std::__2::allocator.4"* %1) #3
  ret %"struct.std::__2::__compressed_pair_elem.3"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::allocator.4"* @_ZNSt3__29allocatorINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEEEC2Ev(%"class.std::__2::allocator.4"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.4"*, align 4
  store %"class.std::__2::allocator.4"* %this, %"class.std::__2::allocator.4"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.4"*, %"class.std::__2::allocator.4"** %this.addr, align 4
  ret %"class.std::__2::allocator.4"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE8max_sizeEv(%"class.std::__2::vector"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp3 = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.4"* @_ZNKSt3__213__vector_baseINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE7__allocEv(%"class.std::__2::__vector_base"* %0) #3
  %call2 = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEEEE8max_sizeERKS7_(%"class.std::__2::allocator.4"* nonnull align 1 dereferenceable(1) %call) #3
  store i32 %call2, i32* %ref.tmp, align 4
  %call4 = call i32 @_ZNSt3__214numeric_limitsIlE3maxEv() #3
  store i32 %call4, i32* %ref.tmp3, align 4
  %call5 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %1 = load i32, i32* %call5, align 4
  ret i32 %1
}

; Function Attrs: noreturn
declare void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"*) #10

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::basic_string"* @_ZNSt3__216allocator_traitsINS_9allocatorINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEEEE8allocateERS7_m(%"class.std::__2::allocator.4"* nonnull align 1 dereferenceable(1) %__a, i32 %__n) #1 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.4"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.4"* %__a, %"class.std::__2::allocator.4"** %__a.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.4"*, %"class.std::__2::allocator.4"** %__a.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call %"class.std::__2::basic_string"* @_ZNSt3__29allocatorINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEEE8allocateEmPKv(%"class.std::__2::allocator.4"* %0, i32 %1, i8* null)
  ret %"class.std::__2::basic_string"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.4"* @_ZNSt3__213__vector_baseINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE7__allocEv(%"class.std::__2::__vector_base"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.4"* @_ZNSt3__217__compressed_pairIPNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE6secondEv(%"class.std::__2::__compressed_pair.1"* %__end_cap_) #3
  ret %"class.std::__2::allocator.4"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::basic_string"** @_ZNSt3__213__vector_baseINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE9__end_capEv(%"class.std::__2::__vector_base"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::basic_string"** @_ZNSt3__217__compressed_pairIPNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE5firstEv(%"class.std::__2::__compressed_pair.1"* %__end_cap_) #3
  ret %"class.std::__2::basic_string"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE14__annotate_newEm(%"class.std::__2::vector"* %this, i32 %__current_size) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__current_size.addr = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__current_size, i32* %__current_size.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %call = call %"class.std::__2::basic_string"* @_ZNKSt3__26vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE4dataEv(%"class.std::__2::vector"* %this1) #3
  %0 = bitcast %"class.std::__2::basic_string"* %call to i8*
  %call2 = call %"class.std::__2::basic_string"* @_ZNKSt3__26vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE4dataEv(%"class.std::__2::vector"* %this1) #3
  %call3 = call i32 @_ZNKSt3__26vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE8capacityEv(%"class.std::__2::vector"* %this1) #3
  %add.ptr = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %call2, i32 %call3
  %1 = bitcast %"class.std::__2::basic_string"* %add.ptr to i8*
  %call4 = call %"class.std::__2::basic_string"* @_ZNKSt3__26vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE4dataEv(%"class.std::__2::vector"* %this1) #3
  %call5 = call i32 @_ZNKSt3__26vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE8capacityEv(%"class.std::__2::vector"* %this1) #3
  %add.ptr6 = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %call4, i32 %call5
  %2 = bitcast %"class.std::__2::basic_string"* %add.ptr6 to i8*
  %call7 = call %"class.std::__2::basic_string"* @_ZNKSt3__26vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE4dataEv(%"class.std::__2::vector"* %this1) #3
  %3 = load i32, i32* %__current_size.addr, align 4
  %add.ptr8 = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %call7, i32 %3
  %4 = bitcast %"class.std::__2::basic_string"* %add.ptr8 to i8*
  call void @_ZNKSt3__26vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE31__annotate_contiguous_containerEPKvSA_SA_SA_(%"class.std::__2::vector"* %this1, i8* %0, i8* %1, i8* %2, i8* %4) #3
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEEEE8max_sizeERKS7_(%"class.std::__2::allocator.4"* nonnull align 1 dereferenceable(1) %__a) #1 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.4"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_max_size", align 1
  store %"class.std::__2::allocator.4"* %__a, %"class.std::__2::allocator.4"** %__a.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_max_size"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.4"*, %"class.std::__2::allocator.4"** %__a.addr, align 4
  %call = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS7_(%"class.std::__2::allocator.4"* nonnull align 1 dereferenceable(1) %1) #3
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.4"* @_ZNKSt3__213__vector_baseINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE7__allocEv(%"class.std::__2::__vector_base"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.4"* @_ZNKSt3__217__compressed_pairIPNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE6secondEv(%"class.std::__2::__compressed_pair.1"* %__end_cap_) #3
  ret %"class.std::__2::allocator.4"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__214numeric_limitsIlE3maxEv() #1 comdat {
entry:
  %call = call i32 @_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv() #3
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS7_(%"class.std::__2::allocator.4"* nonnull align 1 dereferenceable(1) %__a) #1 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.4"*, align 4
  store %"class.std::__2::allocator.4"* %__a, %"class.std::__2::allocator.4"** %__a.addr, align 4
  %1 = load %"class.std::__2::allocator.4"*, %"class.std::__2::allocator.4"** %__a.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEEE8max_sizeEv(%"class.std::__2::allocator.4"* %1) #3
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__29allocatorINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEEE8max_sizeEv(%"class.std::__2::allocator.4"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.4"*, align 4
  store %"class.std::__2::allocator.4"* %this, %"class.std::__2::allocator.4"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.4"*, %"class.std::__2::allocator.4"** %this.addr, align 4
  ret i32 357913941
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.4"* @_ZNKSt3__217__compressed_pairIPNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE6secondEv(%"class.std::__2::__compressed_pair.1"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.1"*, align 4
  store %"class.std::__2::__compressed_pair.1"* %this, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.1"*, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.1"* %this1 to %"struct.std::__2::__compressed_pair_elem.3"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.4"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.3"* %0) #3
  ret %"class.std::__2::allocator.4"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.4"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.3"* %this) #1 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.3"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.3"* %this, %"struct.std::__2::__compressed_pair_elem.3"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.3"*, %"struct.std::__2::__compressed_pair_elem.3"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.3"* %this1 to %"class.std::__2::allocator.4"*
  ret %"class.std::__2::allocator.4"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv() #1 comdat {
entry:
  ret i32 2147483647
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::basic_string"* @_ZNSt3__29allocatorINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEEE8allocateEmPKv(%"class.std::__2::allocator.4"* %this, i32 %__n, i8* %0) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.4"*, align 4
  %__n.addr = alloca i32, align 4
  %.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.4"* %this, %"class.std::__2::allocator.4"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::allocator.4"*, %"class.std::__2::allocator.4"** %this.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEEE8max_sizeEv(%"class.std::__2::allocator.4"* %this1) #3
  %cmp = icmp ugt i32 %1, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__220__throw_length_errorEPKc(i8* getelementptr inbounds ([68 x i8], [68 x i8]* @.str.27, i32 0, i32 0)) #13
  unreachable

if.end:                                           ; preds = %entry
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 12
  %call2 = call i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %mul, i32 4)
  %3 = bitcast i8* %call2 to %"class.std::__2::basic_string"*
  ret %"class.std::__2::basic_string"* %3
}

; Function Attrs: noinline noreturn nounwind optnone
define linkonce_odr hidden void @_ZNSt3__220__throw_length_errorEPKc(i8* %__msg) #8 comdat {
entry:
  %__msg.addr = alloca i8*, align 4
  store i8* %__msg, i8** %__msg.addr, align 4
  call void @abort() #13
  unreachable
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %__size, i32 %__align) #1 comdat {
entry:
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i32, i32* %__size.addr, align 4
  %call = call noalias nonnull i8* @_Znwm(i32 %0) #12
  ret i8* %call
}

; Function Attrs: nobuiltin allocsize(0)
declare nonnull i8* @_Znwm(i32) #7

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.4"* @_ZNSt3__217__compressed_pairIPNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE6secondEv(%"class.std::__2::__compressed_pair.1"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.1"*, align 4
  store %"class.std::__2::__compressed_pair.1"* %this, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.1"*, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.1"* %this1 to %"struct.std::__2::__compressed_pair_elem.3"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.4"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.3"* %0) #3
  ret %"class.std::__2::allocator.4"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.4"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.3"* %this) #1 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.3"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.3"* %this, %"struct.std::__2::__compressed_pair_elem.3"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.3"*, %"struct.std::__2::__compressed_pair_elem.3"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.3"* %this1 to %"class.std::__2::allocator.4"*
  ret %"class.std::__2::allocator.4"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::basic_string"** @_ZNSt3__217__compressed_pairIPNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE5firstEv(%"class.std::__2::__compressed_pair.1"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.1"*, align 4
  store %"class.std::__2::__compressed_pair.1"* %this, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.1"*, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.1"* %this1 to %"struct.std::__2::__compressed_pair_elem.2"*
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::basic_string"** @_ZNSt3__222__compressed_pair_elemIPNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.2"* %0) #3
  ret %"class.std::__2::basic_string"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::basic_string"** @_ZNSt3__222__compressed_pair_elemIPNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.2"* %this) #1 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.2"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.2"* %this, %"struct.std::__2::__compressed_pair_elem.2"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.2"*, %"struct.std::__2::__compressed_pair_elem.2"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.2", %"struct.std::__2::__compressed_pair_elem.2"* %this1, i32 0, i32 0
  ret %"class.std::__2::basic_string"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE31__annotate_contiguous_containerEPKvSA_SA_SA_(%"class.std::__2::vector"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::basic_string"* @_ZNKSt3__26vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE4dataEv(%"class.std::__2::vector"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 0
  %1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__begin_, align 4
  %call = call %"class.std::__2::basic_string"* @_ZNSt3__212__to_addressINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEEPT_S8_(%"class.std::__2::basic_string"* %1) #3
  ret %"class.std::__2::basic_string"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE8capacityEv(%"class.std::__2::vector"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call i32 @_ZNKSt3__213__vector_baseINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE8capacityEv(%"class.std::__2::__vector_base"* %0) #3
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::basic_string"* @_ZNSt3__212__to_addressINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEEPT_S8_(%"class.std::__2::basic_string"* %__p) #1 comdat {
entry:
  %__p.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %__p, %"class.std::__2::basic_string"** %__p.addr, align 4
  %0 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__p.addr, align 4
  ret %"class.std::__2::basic_string"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE8capacityEv(%"class.std::__2::__vector_base"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::basic_string"** @_ZNKSt3__213__vector_baseINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE9__end_capEv(%"class.std::__2::__vector_base"* %this1) #3
  %0 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.std::__2::basic_string"* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.std::__2::basic_string"* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 12
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::basic_string"** @_ZNKSt3__213__vector_baseINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE9__end_capEv(%"class.std::__2::__vector_base"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::basic_string"** @_ZNKSt3__217__compressed_pairIPNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE5firstEv(%"class.std::__2::__compressed_pair.1"* %__end_cap_) #3
  ret %"class.std::__2::basic_string"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::basic_string"** @_ZNKSt3__217__compressed_pairIPNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE5firstEv(%"class.std::__2::__compressed_pair.1"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.1"*, align 4
  store %"class.std::__2::__compressed_pair.1"* %this, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.1"*, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.1"* %this1 to %"struct.std::__2::__compressed_pair_elem.2"*
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::basic_string"** @_ZNKSt3__222__compressed_pair_elemIPNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.2"* %0) #3
  ret %"class.std::__2::basic_string"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::basic_string"** @_ZNKSt3__222__compressed_pair_elemIPNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.2"* %this) #1 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.2"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.2"* %this, %"struct.std::__2::__compressed_pair_elem.2"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.2"*, %"struct.std::__2::__compressed_pair_elem.2"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.2", %"struct.std::__2::__compressed_pair_elem.2"* %this1, i32 0, i32 0
  ret %"class.std::__2::basic_string"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<std::__2::basic_string<char>, std::__2::allocator<std::__2::basic_string<char>>>::_ConstructTransaction"* @_ZNSt3__26vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE21_ConstructTransactionC2ERS8_m(%"struct.std::__2::vector<std::__2::basic_string<char>, std::__2::allocator<std::__2::basic_string<char>>>::_ConstructTransaction"* returned %this, %"class.std::__2::vector"* nonnull align 4 dereferenceable(12) %__v, i32 %__n) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<std::__2::basic_string<char>, std::__2::allocator<std::__2::basic_string<char>>>::_ConstructTransaction"*, align 4
  %__v.addr = alloca %"class.std::__2::vector"*, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::vector<std::__2::basic_string<char>, std::__2::allocator<std::__2::basic_string<char>>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<std::__2::basic_string<char>, std::__2::allocator<std::__2::basic_string<char>>>::_ConstructTransaction"** %this.addr, align 4
  store %"class.std::__2::vector"* %__v, %"class.std::__2::vector"** %__v.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::vector<std::__2::basic_string<char>, std::__2::allocator<std::__2::basic_string<char>>>::_ConstructTransaction"*, %"struct.std::__2::vector<std::__2::basic_string<char>, std::__2::allocator<std::__2::basic_string<char>>>::_ConstructTransaction"** %this.addr, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<std::__2::basic_string<char>, std::__2::allocator<std::__2::basic_string<char>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::basic_string<char>, std::__2::allocator<std::__2::basic_string<char>>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__v.addr, align 4
  store %"class.std::__2::vector"* %0, %"class.std::__2::vector"** %__v_, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<std::__2::basic_string<char>, std::__2::allocator<std::__2::basic_string<char>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::basic_string<char>, std::__2::allocator<std::__2::basic_string<char>>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__v.addr, align 4
  %2 = bitcast %"class.std::__2::vector"* %1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %2, i32 0, i32 1
  %3 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__end_, align 4
  store %"class.std::__2::basic_string"* %3, %"class.std::__2::basic_string"** %__pos_, align 4
  %__new_end_ = getelementptr inbounds %"struct.std::__2::vector<std::__2::basic_string<char>, std::__2::allocator<std::__2::basic_string<char>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::basic_string<char>, std::__2::allocator<std::__2::basic_string<char>>>::_ConstructTransaction"* %this1, i32 0, i32 2
  %4 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__v.addr, align 4
  %5 = bitcast %"class.std::__2::vector"* %4 to %"class.std::__2::__vector_base"*
  %__end_2 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %5, i32 0, i32 1
  %6 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__end_2, align 4
  %7 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %6, i32 %7
  store %"class.std::__2::basic_string"* %add.ptr, %"class.std::__2::basic_string"** %__new_end_, align 4
  ret %"struct.std::__2::vector<std::__2::basic_string<char>, std::__2::allocator<std::__2::basic_string<char>>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEEEE25__construct_range_forwardIPKS6_PS6_EEvRS7_T_SE_RT0_(%"class.std::__2::allocator.4"* nonnull align 1 dereferenceable(1) %__a, %"class.std::__2::basic_string"* %__begin1, %"class.std::__2::basic_string"* %__end1, %"class.std::__2::basic_string"** nonnull align 4 dereferenceable(4) %__begin2) #1 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.4"*, align 4
  %__begin1.addr = alloca %"class.std::__2::basic_string"*, align 4
  %__end1.addr = alloca %"class.std::__2::basic_string"*, align 4
  %__begin2.addr = alloca %"class.std::__2::basic_string"**, align 4
  store %"class.std::__2::allocator.4"* %__a, %"class.std::__2::allocator.4"** %__a.addr, align 4
  store %"class.std::__2::basic_string"* %__begin1, %"class.std::__2::basic_string"** %__begin1.addr, align 4
  store %"class.std::__2::basic_string"* %__end1, %"class.std::__2::basic_string"** %__end1.addr, align 4
  store %"class.std::__2::basic_string"** %__begin2, %"class.std::__2::basic_string"*** %__begin2.addr, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__begin1.addr, align 4
  %1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__end1.addr, align 4
  %cmp = icmp ne %"class.std::__2::basic_string"* %0, %1
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load %"class.std::__2::allocator.4"*, %"class.std::__2::allocator.4"** %__a.addr, align 4
  %3 = load %"class.std::__2::basic_string"**, %"class.std::__2::basic_string"*** %__begin2.addr, align 4
  %4 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %3, align 4
  %call = call %"class.std::__2::basic_string"* @_ZNSt3__212__to_addressINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEEPT_S8_(%"class.std::__2::basic_string"* %4) #3
  %5 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__begin1.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEEEE9constructIS6_JRKS6_EEEvRS7_PT_DpOT0_(%"class.std::__2::allocator.4"* nonnull align 1 dereferenceable(1) %2, %"class.std::__2::basic_string"* %call, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %5)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__begin1.addr, align 4
  %incdec.ptr = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %6, i32 1
  store %"class.std::__2::basic_string"* %incdec.ptr, %"class.std::__2::basic_string"** %__begin1.addr, align 4
  %7 = load %"class.std::__2::basic_string"**, %"class.std::__2::basic_string"*** %__begin2.addr, align 4
  %8 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %7, align 4
  %incdec.ptr1 = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %8, i32 1
  store %"class.std::__2::basic_string"* %incdec.ptr1, %"class.std::__2::basic_string"** %7, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<std::__2::basic_string<char>, std::__2::allocator<std::__2::basic_string<char>>>::_ConstructTransaction"* @_ZNSt3__26vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<std::__2::basic_string<char>, std::__2::allocator<std::__2::basic_string<char>>>::_ConstructTransaction"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<std::__2::basic_string<char>, std::__2::allocator<std::__2::basic_string<char>>>::_ConstructTransaction"*, align 4
  store %"struct.std::__2::vector<std::__2::basic_string<char>, std::__2::allocator<std::__2::basic_string<char>>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<std::__2::basic_string<char>, std::__2::allocator<std::__2::basic_string<char>>>::_ConstructTransaction"** %this.addr, align 4
  %this1 = load %"struct.std::__2::vector<std::__2::basic_string<char>, std::__2::allocator<std::__2::basic_string<char>>>::_ConstructTransaction"*, %"struct.std::__2::vector<std::__2::basic_string<char>, std::__2::allocator<std::__2::basic_string<char>>>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<std::__2::basic_string<char>, std::__2::allocator<std::__2::basic_string<char>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::basic_string<char>, std::__2::allocator<std::__2::basic_string<char>>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %0 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__pos_, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<std::__2::basic_string<char>, std::__2::allocator<std::__2::basic_string<char>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::basic_string<char>, std::__2::allocator<std::__2::basic_string<char>>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__v_, align 4
  %2 = bitcast %"class.std::__2::vector"* %1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %2, i32 0, i32 1
  store %"class.std::__2::basic_string"* %0, %"class.std::__2::basic_string"** %__end_, align 4
  ret %"struct.std::__2::vector<std::__2::basic_string<char>, std::__2::allocator<std::__2::basic_string<char>>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEEEE9constructIS6_JRKS6_EEEvRS7_PT_DpOT0_(%"class.std::__2::allocator.4"* nonnull align 1 dereferenceable(1) %__a, %"class.std::__2::basic_string"* %__p, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %__args) #1 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.4"*, align 4
  %__p.addr = alloca %"class.std::__2::basic_string"*, align 4
  %__args.addr = alloca %"class.std::__2::basic_string"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_construct", align 1
  store %"class.std::__2::allocator.4"* %__a, %"class.std::__2::allocator.4"** %__a.addr, align 4
  store %"class.std::__2::basic_string"* %__p, %"class.std::__2::basic_string"** %__p.addr, align 4
  store %"class.std::__2::basic_string"* %__args, %"class.std::__2::basic_string"** %__args.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_construct"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.4"*, %"class.std::__2::allocator.4"** %__a.addr, align 4
  %2 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__p.addr, align 4
  %3 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(12) %"class.std::__2::basic_string"* @_ZNSt3__27forwardIRKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEEOT_RNS_16remove_referenceIS9_E4typeE(%"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %3) #3
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEEEE11__constructIS6_JRKS6_EEEvNS_17integral_constantIbLb1EEERS7_PT_DpOT0_(%"class.std::__2::allocator.4"* nonnull align 1 dereferenceable(1) %1, %"class.std::__2::basic_string"* %2, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEEEE11__constructIS6_JRKS6_EEEvNS_17integral_constantIbLb1EEERS7_PT_DpOT0_(%"class.std::__2::allocator.4"* nonnull align 1 dereferenceable(1) %__a, %"class.std::__2::basic_string"* %__p, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %__args) #1 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.4"*, align 4
  %__p.addr = alloca %"class.std::__2::basic_string"*, align 4
  %__args.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::allocator.4"* %__a, %"class.std::__2::allocator.4"** %__a.addr, align 4
  store %"class.std::__2::basic_string"* %__p, %"class.std::__2::basic_string"** %__p.addr, align 4
  store %"class.std::__2::basic_string"* %__args, %"class.std::__2::basic_string"** %__args.addr, align 4
  %1 = load %"class.std::__2::allocator.4"*, %"class.std::__2::allocator.4"** %__a.addr, align 4
  %2 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__p.addr, align 4
  %3 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(12) %"class.std::__2::basic_string"* @_ZNSt3__27forwardIRKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEEOT_RNS_16remove_referenceIS9_E4typeE(%"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %3) #3
  call void @_ZNSt3__29allocatorINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEEE9constructIS5_JRKS5_EEEvPT_DpOT0_(%"class.std::__2::allocator.4"* %1, %"class.std::__2::basic_string"* %2, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"class.std::__2::basic_string"* @_ZNSt3__27forwardIRKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEEOT_RNS_16remove_referenceIS9_E4typeE(%"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %__t) #1 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %__t, %"class.std::__2::basic_string"** %__t.addr, align 4
  %0 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__t.addr, align 4
  ret %"class.std::__2::basic_string"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEEE9constructIS5_JRKS5_EEEvPT_DpOT0_(%"class.std::__2::allocator.4"* %this, %"class.std::__2::basic_string"* %__p, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %__args) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.4"*, align 4
  %__p.addr = alloca %"class.std::__2::basic_string"*, align 4
  %__args.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::allocator.4"* %this, %"class.std::__2::allocator.4"** %this.addr, align 4
  store %"class.std::__2::basic_string"* %__p, %"class.std::__2::basic_string"** %__p.addr, align 4
  store %"class.std::__2::basic_string"* %__args, %"class.std::__2::basic_string"** %__args.addr, align 4
  %this1 = load %"class.std::__2::allocator.4"*, %"class.std::__2::allocator.4"** %this.addr, align 4
  %0 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__p.addr, align 4
  %1 = bitcast %"class.std::__2::basic_string"* %0 to i8*
  %2 = bitcast i8* %1 to %"class.std::__2::basic_string"*
  %3 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(12) %"class.std::__2::basic_string"* @_ZNSt3__27forwardIRKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEEOT_RNS_16remove_referenceIS9_E4typeE(%"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %3) #3
  %call2 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKS5_(%"class.std::__2::basic_string"* %2, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %call)
  ret void
}

declare %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKS5_(%"class.std::__2::basic_string"* returned, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12)) unnamed_addr #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE17__annotate_deleteEv(%"class.std::__2::vector"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %call = call %"class.std::__2::basic_string"* @_ZNKSt3__26vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE4dataEv(%"class.std::__2::vector"* %this1) #3
  %0 = bitcast %"class.std::__2::basic_string"* %call to i8*
  %call2 = call %"class.std::__2::basic_string"* @_ZNKSt3__26vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE4dataEv(%"class.std::__2::vector"* %this1) #3
  %call3 = call i32 @_ZNKSt3__26vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE8capacityEv(%"class.std::__2::vector"* %this1) #3
  %add.ptr = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %call2, i32 %call3
  %1 = bitcast %"class.std::__2::basic_string"* %add.ptr to i8*
  %call4 = call %"class.std::__2::basic_string"* @_ZNKSt3__26vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE4dataEv(%"class.std::__2::vector"* %this1) #3
  %call5 = call i32 @_ZNKSt3__26vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE4sizeEv(%"class.std::__2::vector"* %this1) #3
  %add.ptr6 = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %call4, i32 %call5
  %2 = bitcast %"class.std::__2::basic_string"* %add.ptr6 to i8*
  %call7 = call %"class.std::__2::basic_string"* @_ZNKSt3__26vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE4dataEv(%"class.std::__2::vector"* %this1) #3
  %call8 = call i32 @_ZNKSt3__26vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE8capacityEv(%"class.std::__2::vector"* %this1) #3
  %add.ptr9 = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %call7, i32 %call8
  %3 = bitcast %"class.std::__2::basic_string"* %add.ptr9 to i8*
  call void @_ZNKSt3__26vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE31__annotate_contiguous_containerEPKvSA_SA_SA_(%"class.std::__2::vector"* %this1, i8* %0, i8* %1, i8* %2, i8* %3) #3
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base"* @_ZNSt3__213__vector_baseINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEED2Ev(%"class.std::__2::__vector_base"* returned %this) unnamed_addr #1 comdat {
entry:
  %retval = alloca %"class.std::__2::__vector_base"*, align 4
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  store %"class.std::__2::__vector_base"* %this1, %"class.std::__2::__vector_base"** %retval, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__begin_, align 4
  %cmp = icmp ne %"class.std::__2::basic_string"* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__213__vector_baseINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE5clearEv(%"class.std::__2::__vector_base"* %this1) #3
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.4"* @_ZNSt3__213__vector_baseINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE7__allocEv(%"class.std::__2::__vector_base"* %this1) #3
  %__begin_2 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__begin_2, align 4
  %call3 = call i32 @_ZNKSt3__213__vector_baseINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE8capacityEv(%"class.std::__2::__vector_base"* %this1) #3
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEEEE10deallocateERS7_PS6_m(%"class.std::__2::allocator.4"* nonnull align 1 dereferenceable(1) %call, %"class.std::__2::basic_string"* %1, i32 %call3) #3
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %retval, align 4
  ret %"class.std::__2::__vector_base"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE4sizeEv(%"class.std::__2::vector"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 1
  %1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %2, i32 0, i32 0
  %3 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.std::__2::basic_string"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.std::__2::basic_string"* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 12
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE5clearEv(%"class.std::__2::__vector_base"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__begin_, align 4
  call void @_ZNSt3__213__vector_baseINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE17__destruct_at_endEPS6_(%"class.std::__2::__vector_base"* %this1, %"class.std::__2::basic_string"* %0) #3
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEEEE10deallocateERS7_PS6_m(%"class.std::__2::allocator.4"* nonnull align 1 dereferenceable(1) %__a, %"class.std::__2::basic_string"* %__p, i32 %__n) #1 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.4"*, align 4
  %__p.addr = alloca %"class.std::__2::basic_string"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.4"* %__a, %"class.std::__2::allocator.4"** %__a.addr, align 4
  store %"class.std::__2::basic_string"* %__p, %"class.std::__2::basic_string"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.4"*, %"class.std::__2::allocator.4"** %__a.addr, align 4
  %1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEEE10deallocateEPS5_m(%"class.std::__2::allocator.4"* %0, %"class.std::__2::basic_string"* %1, i32 %2) #3
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE17__destruct_at_endEPS6_(%"class.std::__2::__vector_base"* %this, %"class.std::__2::basic_string"* %__new_last) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  %__new_last.addr = alloca %"class.std::__2::basic_string"*, align 4
  %__soon_to_be_end = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  store %"class.std::__2::basic_string"* %__new_last, %"class.std::__2::basic_string"** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 1
  %0 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__end_, align 4
  store %"class.std::__2::basic_string"* %0, %"class.std::__2::basic_string"** %__soon_to_be_end, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__new_last.addr, align 4
  %2 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__soon_to_be_end, align 4
  %cmp = icmp ne %"class.std::__2::basic_string"* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.4"* @_ZNSt3__213__vector_baseINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE7__allocEv(%"class.std::__2::__vector_base"* %this1) #3
  %3 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__soon_to_be_end, align 4
  %incdec.ptr = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %3, i32 -1
  store %"class.std::__2::basic_string"* %incdec.ptr, %"class.std::__2::basic_string"** %__soon_to_be_end, align 4
  %call2 = call %"class.std::__2::basic_string"* @_ZNSt3__212__to_addressINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEEPT_S8_(%"class.std::__2::basic_string"* %incdec.ptr) #3
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEEEE7destroyIS6_EEvRS7_PT_(%"class.std::__2::allocator.4"* nonnull align 1 dereferenceable(1) %call, %"class.std::__2::basic_string"* %call2)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %4 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__new_last.addr, align 4
  %__end_3 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 1
  store %"class.std::__2::basic_string"* %4, %"class.std::__2::basic_string"** %__end_3, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEEEE7destroyIS6_EEvRS7_PT_(%"class.std::__2::allocator.4"* nonnull align 1 dereferenceable(1) %__a, %"class.std::__2::basic_string"* %__p) #1 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.4"*, align 4
  %__p.addr = alloca %"class.std::__2::basic_string"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy", align 1
  store %"class.std::__2::allocator.4"* %__a, %"class.std::__2::allocator.4"** %__a.addr, align 4
  store %"class.std::__2::basic_string"* %__p, %"class.std::__2::basic_string"** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.4"*, %"class.std::__2::allocator.4"** %__a.addr, align 4
  %2 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEEEE9__destroyIS6_EEvNS_17integral_constantIbLb1EEERS7_PT_(%"class.std::__2::allocator.4"* nonnull align 1 dereferenceable(1) %1, %"class.std::__2::basic_string"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEEEE9__destroyIS6_EEvNS_17integral_constantIbLb1EEERS7_PT_(%"class.std::__2::allocator.4"* nonnull align 1 dereferenceable(1) %__a, %"class.std::__2::basic_string"* %__p) #1 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.4"*, align 4
  %__p.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::allocator.4"* %__a, %"class.std::__2::allocator.4"** %__a.addr, align 4
  store %"class.std::__2::basic_string"* %__p, %"class.std::__2::basic_string"** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.4"*, %"class.std::__2::allocator.4"** %__a.addr, align 4
  %2 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__p.addr, align 4
  call void @_ZNSt3__29allocatorINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEEE7destroyEPS5_(%"class.std::__2::allocator.4"* %1, %"class.std::__2::basic_string"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEEE7destroyEPS5_(%"class.std::__2::allocator.4"* %this, %"class.std::__2::basic_string"* %__p) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.4"*, align 4
  %__p.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::allocator.4"* %this, %"class.std::__2::allocator.4"** %this.addr, align 4
  store %"class.std::__2::basic_string"* %__p, %"class.std::__2::basic_string"** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.4"*, %"class.std::__2::allocator.4"** %this.addr, align 4
  %0 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__p.addr, align 4
  %call = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_string"* %0) #3
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEEE10deallocateEPS5_m(%"class.std::__2::allocator.4"* %this, %"class.std::__2::basic_string"* %__p, i32 %__n) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.4"*, align 4
  %__p.addr = alloca %"class.std::__2::basic_string"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.4"* %this, %"class.std::__2::allocator.4"** %this.addr, align 4
  store %"class.std::__2::basic_string"* %__p, %"class.std::__2::basic_string"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.4"*, %"class.std::__2::allocator.4"** %this.addr, align 4
  %0 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__p.addr, align 4
  %1 = bitcast %"class.std::__2::basic_string"* %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 12
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %__ptr, i32 %__size, i32 %__align) #1 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  %1 = load i32, i32* %__size.addr, align 4
  %2 = load i32, i32* %__align.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm(i8* %0, i32 %1, i32 %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm(i8* %__ptr, i32 %__size, i32 %__align) #1 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  %1 = load i32, i32* %__size.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm(i8* %0, i32 %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm(i8* %__ptr, i32 %__size) #1 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller9__do_callEPv(i8* %0)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller9__do_callEPv(i8* %__ptr) #1 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  call void @_ZdlPv(i8* %0) #11
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::basic_iostream"* @_ZNSt3__214basic_iostreamIcNS_11char_traitsIcEEEC2EPNS_15basic_streambufIcS2_EE(%"class.std::__2::basic_iostream"* returned %this, i8** %vtt, %"class.std::__2::basic_streambuf"* %__sb) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_iostream"*, align 4
  %vtt.addr = alloca i8**, align 4
  %__sb.addr = alloca %"class.std::__2::basic_streambuf"*, align 4
  store %"class.std::__2::basic_iostream"* %this, %"class.std::__2::basic_iostream"** %this.addr, align 4
  store i8** %vtt, i8*** %vtt.addr, align 4
  store %"class.std::__2::basic_streambuf"* %__sb, %"class.std::__2::basic_streambuf"** %__sb.addr, align 4
  %this1 = load %"class.std::__2::basic_iostream"*, %"class.std::__2::basic_iostream"** %this.addr, align 4
  %vtt2 = load i8**, i8*** %vtt.addr, align 4
  %0 = bitcast %"class.std::__2::basic_iostream"* %this1 to %"class.std::__2::basic_istream"*
  %1 = load %"class.std::__2::basic_streambuf"*, %"class.std::__2::basic_streambuf"** %__sb.addr, align 4
  %2 = getelementptr inbounds i8*, i8** %vtt2, i64 1
  %call = call %"class.std::__2::basic_istream"* @_ZNSt3__213basic_istreamIcNS_11char_traitsIcEEEC2EPNS_15basic_streambufIcS2_EE(%"class.std::__2::basic_istream"* %0, i8** %2, %"class.std::__2::basic_streambuf"* %1)
  %3 = bitcast %"class.std::__2::basic_iostream"* %this1 to i8*
  %4 = getelementptr inbounds i8, i8* %3, i32 8
  %5 = bitcast i8* %4 to %"class.std::__2::basic_ostream"*
  %6 = getelementptr inbounds i8*, i8** %vtt2, i64 3
  %call3 = call %"class.std::__2::basic_ostream"* @_ZNSt3__213basic_ostreamIcNS_11char_traitsIcEEEC2Ev(%"class.std::__2::basic_ostream"* %5, i8** %6)
  %7 = load i8*, i8** %vtt2, align 4
  %8 = bitcast %"class.std::__2::basic_iostream"* %this1 to i32 (...)***
  %9 = bitcast i8* %7 to i32 (...)**
  store i32 (...)** %9, i32 (...)*** %8, align 4
  %10 = getelementptr inbounds i8*, i8** %vtt2, i64 5
  %11 = load i8*, i8** %10, align 4
  %12 = bitcast %"class.std::__2::basic_iostream"* %this1 to i8**
  %vtable = load i8*, i8** %12, align 4
  %vbase.offset.ptr = getelementptr i8, i8* %vtable, i64 -12
  %13 = bitcast i8* %vbase.offset.ptr to i32*
  %vbase.offset = load i32, i32* %13, align 4
  %14 = bitcast %"class.std::__2::basic_iostream"* %this1 to i8*
  %add.ptr = getelementptr inbounds i8, i8* %14, i32 %vbase.offset
  %15 = bitcast i8* %add.ptr to i32 (...)***
  %16 = bitcast i8* %11 to i32 (...)**
  store i32 (...)** %16, i32 (...)*** %15, align 4
  %17 = getelementptr inbounds i8*, i8** %vtt2, i64 6
  %18 = load i8*, i8** %17, align 4
  %19 = bitcast %"class.std::__2::basic_iostream"* %this1 to i8*
  %add.ptr4 = getelementptr inbounds i8, i8* %19, i32 8
  %20 = bitcast i8* %add.ptr4 to i32 (...)***
  %21 = bitcast i8* %18 to i32 (...)**
  store i32 (...)** %21, i32 (...)*** %20, align 4
  ret %"class.std::__2::basic_iostream"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::basic_stringbuf"* @_ZNSt3__215basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEC2Ej(%"class.std::__2::basic_stringbuf"* returned %this, i32 %__wch) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_stringbuf"*, align 4
  %__wch.addr = alloca i32, align 4
  store %"class.std::__2::basic_stringbuf"* %this, %"class.std::__2::basic_stringbuf"** %this.addr, align 4
  store i32 %__wch, i32* %__wch.addr, align 4
  %this1 = load %"class.std::__2::basic_stringbuf"*, %"class.std::__2::basic_stringbuf"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::basic_stringbuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call = call %"class.std::__2::basic_streambuf"* @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEEC2Ev(%"class.std::__2::basic_streambuf"* %0)
  %1 = bitcast %"class.std::__2::basic_stringbuf"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [16 x i8*] }, { [16 x i8*] }* @_ZTVNSt3__215basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %__str_ = getelementptr inbounds %"class.std::__2::basic_stringbuf", %"class.std::__2::basic_stringbuf"* %this1, i32 0, i32 1
  %call2 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2Ev(%"class.std::__2::basic_string"* %__str_) #3
  %__hm_ = getelementptr inbounds %"class.std::__2::basic_stringbuf", %"class.std::__2::basic_stringbuf"* %this1, i32 0, i32 2
  store i8* null, i8** %__hm_, align 4
  %__mode_ = getelementptr inbounds %"class.std::__2::basic_stringbuf", %"class.std::__2::basic_stringbuf"* %this1, i32 0, i32 3
  %2 = load i32, i32* %__wch.addr, align 4
  store i32 %2, i32* %__mode_, align 4
  ret %"class.std::__2::basic_stringbuf"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::basic_istream"* @_ZNSt3__213basic_istreamIcNS_11char_traitsIcEEEC2EPNS_15basic_streambufIcS2_EE(%"class.std::__2::basic_istream"* returned %this, i8** %vtt, %"class.std::__2::basic_streambuf"* %__sb) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_istream"*, align 4
  %vtt.addr = alloca i8**, align 4
  %__sb.addr = alloca %"class.std::__2::basic_streambuf"*, align 4
  store %"class.std::__2::basic_istream"* %this, %"class.std::__2::basic_istream"** %this.addr, align 4
  store i8** %vtt, i8*** %vtt.addr, align 4
  store %"class.std::__2::basic_streambuf"* %__sb, %"class.std::__2::basic_streambuf"** %__sb.addr, align 4
  %this1 = load %"class.std::__2::basic_istream"*, %"class.std::__2::basic_istream"** %this.addr, align 4
  %vtt2 = load i8**, i8*** %vtt.addr, align 4
  %0 = load i8*, i8** %vtt2, align 4
  %1 = bitcast %"class.std::__2::basic_istream"* %this1 to i32 (...)***
  %2 = bitcast i8* %0 to i32 (...)**
  store i32 (...)** %2, i32 (...)*** %1, align 4
  %3 = getelementptr inbounds i8*, i8** %vtt2, i64 1
  %4 = load i8*, i8** %3, align 4
  %5 = bitcast %"class.std::__2::basic_istream"* %this1 to i8**
  %vtable = load i8*, i8** %5, align 4
  %vbase.offset.ptr = getelementptr i8, i8* %vtable, i64 -12
  %6 = bitcast i8* %vbase.offset.ptr to i32*
  %vbase.offset = load i32, i32* %6, align 4
  %7 = bitcast %"class.std::__2::basic_istream"* %this1 to i8*
  %add.ptr = getelementptr inbounds i8, i8* %7, i32 %vbase.offset
  %8 = bitcast i8* %add.ptr to i32 (...)***
  %9 = bitcast i8* %4 to i32 (...)**
  store i32 (...)** %9, i32 (...)*** %8, align 4
  %__gc_ = getelementptr inbounds %"class.std::__2::basic_istream", %"class.std::__2::basic_istream"* %this1, i32 0, i32 1
  store i32 0, i32* %__gc_, align 4
  %10 = bitcast %"class.std::__2::basic_istream"* %this1 to i8**
  %vtable3 = load i8*, i8** %10, align 4
  %vbase.offset.ptr4 = getelementptr i8, i8* %vtable3, i64 -12
  %11 = bitcast i8* %vbase.offset.ptr4 to i32*
  %vbase.offset5 = load i32, i32* %11, align 4
  %12 = bitcast %"class.std::__2::basic_istream"* %this1 to i8*
  %add.ptr6 = getelementptr inbounds i8, i8* %12, i32 %vbase.offset5
  %13 = bitcast i8* %add.ptr6 to %"class.std::__2::basic_ios"*
  %14 = load %"class.std::__2::basic_streambuf"*, %"class.std::__2::basic_streambuf"** %__sb.addr, align 4
  call void @_ZNSt3__29basic_iosIcNS_11char_traitsIcEEE4initEPNS_15basic_streambufIcS2_EE(%"class.std::__2::basic_ios"* %13, %"class.std::__2::basic_streambuf"* %14)
  ret %"class.std::__2::basic_istream"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::basic_ostream"* @_ZNSt3__213basic_ostreamIcNS_11char_traitsIcEEEC2Ev(%"class.std::__2::basic_ostream"* returned %this, i8** %vtt) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_ostream"*, align 4
  %vtt.addr = alloca i8**, align 4
  store %"class.std::__2::basic_ostream"* %this, %"class.std::__2::basic_ostream"** %this.addr, align 4
  store i8** %vtt, i8*** %vtt.addr, align 4
  %this1 = load %"class.std::__2::basic_ostream"*, %"class.std::__2::basic_ostream"** %this.addr, align 4
  %vtt2 = load i8**, i8*** %vtt.addr, align 4
  %0 = load i8*, i8** %vtt2, align 4
  %1 = bitcast %"class.std::__2::basic_ostream"* %this1 to i32 (...)***
  %2 = bitcast i8* %0 to i32 (...)**
  store i32 (...)** %2, i32 (...)*** %1, align 4
  %3 = getelementptr inbounds i8*, i8** %vtt2, i64 1
  %4 = load i8*, i8** %3, align 4
  %5 = bitcast %"class.std::__2::basic_ostream"* %this1 to i8**
  %vtable = load i8*, i8** %5, align 4
  %vbase.offset.ptr = getelementptr i8, i8* %vtable, i64 -12
  %6 = bitcast i8* %vbase.offset.ptr to i32*
  %vbase.offset = load i32, i32* %6, align 4
  %7 = bitcast %"class.std::__2::basic_ostream"* %this1 to i8*
  %add.ptr = getelementptr inbounds i8, i8* %7, i32 %vbase.offset
  %8 = bitcast i8* %add.ptr to i32 (...)***
  %9 = bitcast i8* %4 to i32 (...)**
  store i32 (...)** %9, i32 (...)*** %8, align 4
  ret %"class.std::__2::basic_ostream"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2Ev(%"class.std::__2::basic_string"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  %ref.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  %ref.tmp2 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::basic_string"* %this1 to %"class.std::__2::__basic_string_common"*
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::__compressed_pair"* @_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_EC2INS_18__default_init_tagESA_EEOT_OT0_(%"class.std::__2::__compressed_pair"* %__r_, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  call void @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__zeroEv(%"class.std::__2::basic_string"* %this1) #3
  ret %"class.std::__2::basic_string"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__zeroEv(%"class.std::__2::basic_string"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  %__a = alloca [3 x i32]*, align 4
  %__i = alloca i32, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair"* %__r_) #3
  %0 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call, i32 0, i32 0
  %__r = bitcast %union.anon* %0 to %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__raw"*
  %__words = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__raw", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__raw"* %__r, i32 0, i32 0
  store [3 x i32]* %__words, [3 x i32]** %__a, align 4
  store i32 0, i32* %__i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %__i, align 4
  %cmp = icmp ult i32 %1, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load [3 x i32]*, [3 x i32]** %__a, align 4
  %3 = load i32, i32* %__i, align 4
  %arrayidx = getelementptr inbounds [3 x i32], [3 x i32]* %2, i32 0, i32 %3
  store i32 0, i32* %arrayidx, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %4 = load i32, i32* %__i, align 4
  %inc = add i32 %4, 1
  store i32 %inc, i32* %__i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %0) #3
  ret %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %this) #1 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem"*, align 4
  store %"struct.std::__2::__compressed_pair_elem"* %this, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem"*, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem"* %this1, i32 0, i32 0
  ret %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::basic_stringbuf"* @_ZNSt3__215basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEC2ERKNS_12basic_stringIcS2_S4_EEj(%"class.std::__2::basic_stringbuf"* returned %this, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %__s, i32 %__wch) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_stringbuf"*, align 4
  %__s.addr = alloca %"class.std::__2::basic_string"*, align 4
  %__wch.addr = alloca i32, align 4
  %ref.tmp = alloca %"class.std::__2::allocator", align 1
  %undef.agg.tmp = alloca %"class.std::__2::allocator", align 1
  store %"class.std::__2::basic_stringbuf"* %this, %"class.std::__2::basic_stringbuf"** %this.addr, align 4
  store %"class.std::__2::basic_string"* %__s, %"class.std::__2::basic_string"** %__s.addr, align 4
  store i32 %__wch, i32* %__wch.addr, align 4
  %this1 = load %"class.std::__2::basic_stringbuf"*, %"class.std::__2::basic_stringbuf"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::basic_stringbuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call = call %"class.std::__2::basic_streambuf"* @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEEC2Ev(%"class.std::__2::basic_streambuf"* %0)
  %1 = bitcast %"class.std::__2::basic_stringbuf"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [16 x i8*] }, { [16 x i8*] }* @_ZTVNSt3__215basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %__str_ = getelementptr inbounds %"class.std::__2::basic_stringbuf", %"class.std::__2::basic_stringbuf"* %this1, i32 0, i32 1
  %2 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__s.addr, align 4
  call void @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE13get_allocatorEv(%"class.std::__2::basic_string"* %2) #3
  %call2 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2ERKS4_(%"class.std::__2::basic_string"* %__str_, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %ref.tmp) #3
  %__hm_ = getelementptr inbounds %"class.std::__2::basic_stringbuf", %"class.std::__2::basic_stringbuf"* %this1, i32 0, i32 2
  store i8* null, i8** %__hm_, align 4
  %__mode_ = getelementptr inbounds %"class.std::__2::basic_stringbuf", %"class.std::__2::basic_stringbuf"* %this1, i32 0, i32 3
  %3 = load i32, i32* %__wch.addr, align 4
  store i32 %3, i32* %__mode_, align 4
  %4 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__s.addr, align 4
  call void @_ZNSt3__215basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE3strERKNS_12basic_stringIcS2_S4_EE(%"class.std::__2::basic_stringbuf"* %this1, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %4)
  ret %"class.std::__2::basic_stringbuf"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE13get_allocatorEv(%"class.std::__2::basic_string"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE7__allocEv(%"class.std::__2::basic_string"* %this1) #3
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2ERKS4_(%"class.std::__2::basic_string"* returned %this, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %ref.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::basic_string"* %this1 to %"class.std::__2::__basic_string_common"*
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %call = call %"class.std::__2::__compressed_pair"* @_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_EC2INS_18__default_init_tagERKS5_EEOT_OT0_(%"class.std::__2::__compressed_pair"* %__r_, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %1)
  call void @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__zeroEv(%"class.std::__2::basic_string"* %this1) #3
  ret %"class.std::__2::basic_string"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__215basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE3strERKNS_12basic_stringIcS2_S4_EE(%"class.std::__2::basic_stringbuf"* %this, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %__s) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_stringbuf"*, align 4
  %__s.addr = alloca %"class.std::__2::basic_string"*, align 4
  %__sz = alloca i32, align 4
  store %"class.std::__2::basic_stringbuf"* %this, %"class.std::__2::basic_stringbuf"** %this.addr, align 4
  store %"class.std::__2::basic_string"* %__s, %"class.std::__2::basic_string"** %__s.addr, align 4
  %this1 = load %"class.std::__2::basic_stringbuf"*, %"class.std::__2::basic_stringbuf"** %this.addr, align 4
  %0 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__s.addr, align 4
  %__str_ = getelementptr inbounds %"class.std::__2::basic_stringbuf", %"class.std::__2::basic_stringbuf"* %this1, i32 0, i32 1
  %call = call nonnull align 4 dereferenceable(12) %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEaSERKS5_(%"class.std::__2::basic_string"* %__str_, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %0)
  %__hm_ = getelementptr inbounds %"class.std::__2::basic_stringbuf", %"class.std::__2::basic_stringbuf"* %this1, i32 0, i32 2
  store i8* null, i8** %__hm_, align 4
  %__mode_ = getelementptr inbounds %"class.std::__2::basic_stringbuf", %"class.std::__2::basic_stringbuf"* %this1, i32 0, i32 3
  %1 = load i32, i32* %__mode_, align 4
  %and = and i32 %1, 8
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__str_2 = getelementptr inbounds %"class.std::__2::basic_stringbuf", %"class.std::__2::basic_stringbuf"* %this1, i32 0, i32 1
  %call3 = call i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv(%"class.std::__2::basic_string"* %__str_2) #3
  %__str_4 = getelementptr inbounds %"class.std::__2::basic_stringbuf", %"class.std::__2::basic_stringbuf"* %this1, i32 0, i32 1
  %call5 = call i32 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4sizeEv(%"class.std::__2::basic_string"* %__str_4) #3
  %add.ptr = getelementptr inbounds i8, i8* %call3, i32 %call5
  %__hm_6 = getelementptr inbounds %"class.std::__2::basic_stringbuf", %"class.std::__2::basic_stringbuf"* %this1, i32 0, i32 2
  store i8* %add.ptr, i8** %__hm_6, align 4
  %2 = bitcast %"class.std::__2::basic_stringbuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %__str_7 = getelementptr inbounds %"class.std::__2::basic_stringbuf", %"class.std::__2::basic_stringbuf"* %this1, i32 0, i32 1
  %call8 = call i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv(%"class.std::__2::basic_string"* %__str_7) #3
  %__str_9 = getelementptr inbounds %"class.std::__2::basic_stringbuf", %"class.std::__2::basic_stringbuf"* %this1, i32 0, i32 1
  %call10 = call i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv(%"class.std::__2::basic_string"* %__str_9) #3
  %__hm_11 = getelementptr inbounds %"class.std::__2::basic_stringbuf", %"class.std::__2::basic_stringbuf"* %this1, i32 0, i32 2
  %3 = load i8*, i8** %__hm_11, align 4
  call void @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE4setgEPcS4_S4_(%"class.std::__2::basic_streambuf"* %2, i8* %call8, i8* %call10, i8* %3)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %__mode_12 = getelementptr inbounds %"class.std::__2::basic_stringbuf", %"class.std::__2::basic_stringbuf"* %this1, i32 0, i32 3
  %4 = load i32, i32* %__mode_12, align 4
  %and13 = and i32 %4, 16
  %tobool14 = icmp ne i32 %and13, 0
  br i1 %tobool14, label %if.then15, label %if.end40

if.then15:                                        ; preds = %if.end
  %__str_16 = getelementptr inbounds %"class.std::__2::basic_stringbuf", %"class.std::__2::basic_stringbuf"* %this1, i32 0, i32 1
  %call17 = call i32 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4sizeEv(%"class.std::__2::basic_string"* %__str_16) #3
  store i32 %call17, i32* %__sz, align 4
  %__str_18 = getelementptr inbounds %"class.std::__2::basic_stringbuf", %"class.std::__2::basic_stringbuf"* %this1, i32 0, i32 1
  %call19 = call i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv(%"class.std::__2::basic_string"* %__str_18) #3
  %5 = load i32, i32* %__sz, align 4
  %add.ptr20 = getelementptr inbounds i8, i8* %call19, i32 %5
  %__hm_21 = getelementptr inbounds %"class.std::__2::basic_stringbuf", %"class.std::__2::basic_stringbuf"* %this1, i32 0, i32 2
  store i8* %add.ptr20, i8** %__hm_21, align 4
  %__str_22 = getelementptr inbounds %"class.std::__2::basic_stringbuf", %"class.std::__2::basic_stringbuf"* %this1, i32 0, i32 1
  %__str_23 = getelementptr inbounds %"class.std::__2::basic_stringbuf", %"class.std::__2::basic_stringbuf"* %this1, i32 0, i32 1
  %call24 = call i32 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE8capacityEv(%"class.std::__2::basic_string"* %__str_23) #3
  call void @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6resizeEm(%"class.std::__2::basic_string"* %__str_22, i32 %call24)
  %6 = bitcast %"class.std::__2::basic_stringbuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %__str_25 = getelementptr inbounds %"class.std::__2::basic_stringbuf", %"class.std::__2::basic_stringbuf"* %this1, i32 0, i32 1
  %call26 = call i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv(%"class.std::__2::basic_string"* %__str_25) #3
  %__str_27 = getelementptr inbounds %"class.std::__2::basic_stringbuf", %"class.std::__2::basic_stringbuf"* %this1, i32 0, i32 1
  %call28 = call i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv(%"class.std::__2::basic_string"* %__str_27) #3
  %__str_29 = getelementptr inbounds %"class.std::__2::basic_stringbuf", %"class.std::__2::basic_stringbuf"* %this1, i32 0, i32 1
  %call30 = call i32 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4sizeEv(%"class.std::__2::basic_string"* %__str_29) #3
  %add.ptr31 = getelementptr inbounds i8, i8* %call28, i32 %call30
  call void @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE4setpEPcS4_(%"class.std::__2::basic_streambuf"* %6, i8* %call26, i8* %add.ptr31)
  %__mode_32 = getelementptr inbounds %"class.std::__2::basic_stringbuf", %"class.std::__2::basic_stringbuf"* %this1, i32 0, i32 3
  %7 = load i32, i32* %__mode_32, align 4
  %and33 = and i32 %7, 3
  %tobool34 = icmp ne i32 %and33, 0
  br i1 %tobool34, label %if.then35, label %if.end39

if.then35:                                        ; preds = %if.then15
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.then35
  %8 = load i32, i32* %__sz, align 4
  %cmp = icmp ugt i32 %8, 2147483647
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %9 = bitcast %"class.std::__2::basic_stringbuf"* %this1 to %"class.std::__2::basic_streambuf"*
  call void @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE5pbumpEi(%"class.std::__2::basic_streambuf"* %9, i32 2147483647)
  %10 = load i32, i32* %__sz, align 4
  %sub = sub i32 %10, 2147483647
  store i32 %sub, i32* %__sz, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %11 = load i32, i32* %__sz, align 4
  %cmp36 = icmp ugt i32 %11, 0
  br i1 %cmp36, label %if.then37, label %if.end38

if.then37:                                        ; preds = %while.end
  %12 = bitcast %"class.std::__2::basic_stringbuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %13 = load i32, i32* %__sz, align 4
  call void @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE5pbumpEi(%"class.std::__2::basic_streambuf"* %12, i32 %13)
  br label %if.end38

if.end38:                                         ; preds = %if.then37, %while.end
  br label %if.end39

if.end39:                                         ; preds = %if.end38, %if.then15
  br label %if.end40

if.end40:                                         ; preds = %if.end39, %if.end
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE7__allocEv(%"class.std::__2::basic_string"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E6secondEv(%"class.std::__2::__compressed_pair"* %__r_) #3
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E6secondEv(%"class.std::__2::__compressed_pair"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem.0"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorIcEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.0"* %0) #3
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorIcEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.0"* %this) #1 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.0"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.0"* %this, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.0"*, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.0"* %this1 to %"class.std::__2::allocator"*
  ret %"class.std::__2::allocator"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair"* @_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_EC2INS_18__default_init_tagERKS5_EEOT_OT0_(%"class.std::__2::__compressed_pair"* returned %this, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t1, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  %__t1.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %__t2.addr = alloca %"class.std::__2::allocator"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t1, %"struct.std::__2::__default_init_tag"** %__t1.addr, align 4
  store %"class.std::__2::allocator"* %__t2, %"class.std::__2::allocator"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %1 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t1.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %1) #3
  %call2 = call %"struct.std::__2::__compressed_pair_elem"* @_ZNSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem"* %0)
  %2 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem.0"*
  %3 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__27forwardIRKNS_9allocatorIcEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %3) #3
  %call4 = call %"struct.std::__2::__compressed_pair_elem.0"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIcEELi1ELb1EEC2IRKS2_vEEOT_(%"struct.std::__2::__compressed_pair_elem.0"* %2, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::__compressed_pair"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__27forwardIRKNS_9allocatorIcEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__t) #1 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"class.std::__2::allocator"* %__t, %"class.std::__2::allocator"** %__t.addr, align 4
  %0 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__t.addr, align 4
  ret %"class.std::__2::allocator"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.0"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIcEELi1ELb1EEC2IRKS2_vEEOT_(%"struct.std::__2::__compressed_pair_elem.0"* returned %this, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__u) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.0"*, align 4
  %__u.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.0"* %this, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  store %"class.std::__2::allocator"* %__u, %"class.std::__2::allocator"** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.0"*, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.0"* %this1 to %"class.std::__2::allocator"*
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__u.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__27forwardIRKNS_9allocatorIcEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %1) #3
  ret %"struct.std::__2::__compressed_pair_elem.0"* %this1
}

declare nonnull align 4 dereferenceable(12) %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEaSERKS5_(%"class.std::__2::basic_string"*, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12)) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::basic_ostream"* @_ZNSt3__224__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m(%"class.std::__2::basic_ostream"* nonnull align 4 dereferenceable(4) %__os, i8* %__str, i32 %__len) #1 comdat {
entry:
  %__os.addr = alloca %"class.std::__2::basic_ostream"*, align 4
  %__str.addr = alloca i8*, align 4
  %__len.addr = alloca i32, align 4
  %__s = alloca %"class.std::__2::basic_ostream<char, std::__2::char_traits<char>>::sentry", align 4
  %ref.tmp = alloca %"class.std::__2::ostreambuf_iterator", align 4
  %agg.tmp = alloca %"class.std::__2::ostreambuf_iterator", align 4
  store %"class.std::__2::basic_ostream"* %__os, %"class.std::__2::basic_ostream"** %__os.addr, align 4
  store i8* %__str, i8** %__str.addr, align 4
  store i32 %__len, i32* %__len.addr, align 4
  %0 = load %"class.std::__2::basic_ostream"*, %"class.std::__2::basic_ostream"** %__os.addr, align 4
  %call = call %"class.std::__2::basic_ostream<char, std::__2::char_traits<char>>::sentry"* @_ZNSt3__213basic_ostreamIcNS_11char_traitsIcEEE6sentryC1ERS3_(%"class.std::__2::basic_ostream<char, std::__2::char_traits<char>>::sentry"* %__s, %"class.std::__2::basic_ostream"* nonnull align 4 dereferenceable(4) %0)
  %call1 = call zeroext i1 @_ZNKSt3__213basic_ostreamIcNS_11char_traitsIcEEE6sentrycvbEv(%"class.std::__2::basic_ostream<char, std::__2::char_traits<char>>::sentry"* %__s)
  br i1 %call1, label %if.then, label %if.end23

if.then:                                          ; preds = %entry
  %1 = load %"class.std::__2::basic_ostream"*, %"class.std::__2::basic_ostream"** %__os.addr, align 4
  %call2 = call %"class.std::__2::ostreambuf_iterator"* @_ZNSt3__219ostreambuf_iteratorIcNS_11char_traitsIcEEEC2ERNS_13basic_ostreamIcS2_EE(%"class.std::__2::ostreambuf_iterator"* %agg.tmp, %"class.std::__2::basic_ostream"* nonnull align 4 dereferenceable(4) %1) #3
  %2 = load i8*, i8** %__str.addr, align 4
  %3 = load %"class.std::__2::basic_ostream"*, %"class.std::__2::basic_ostream"** %__os.addr, align 4
  %4 = bitcast %"class.std::__2::basic_ostream"* %3 to i8**
  %vtable = load i8*, i8** %4, align 4
  %vbase.offset.ptr = getelementptr i8, i8* %vtable, i64 -12
  %5 = bitcast i8* %vbase.offset.ptr to i32*
  %vbase.offset = load i32, i32* %5, align 4
  %6 = bitcast %"class.std::__2::basic_ostream"* %3 to i8*
  %add.ptr = getelementptr inbounds i8, i8* %6, i32 %vbase.offset
  %7 = bitcast i8* %add.ptr to %"class.std::__2::ios_base"*
  %call3 = call i32 @_ZNKSt3__28ios_base5flagsEv(%"class.std::__2::ios_base"* %7)
  %and = and i32 %call3, 176
  %cmp = icmp eq i32 %and, 32
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then
  %8 = load i8*, i8** %__str.addr, align 4
  %9 = load i32, i32* %__len.addr, align 4
  %add.ptr4 = getelementptr inbounds i8, i8* %8, i32 %9
  br label %cond.end

cond.false:                                       ; preds = %if.then
  %10 = load i8*, i8** %__str.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i8* [ %add.ptr4, %cond.true ], [ %10, %cond.false ]
  %11 = load i8*, i8** %__str.addr, align 4
  %12 = load i32, i32* %__len.addr, align 4
  %add.ptr5 = getelementptr inbounds i8, i8* %11, i32 %12
  %13 = load %"class.std::__2::basic_ostream"*, %"class.std::__2::basic_ostream"** %__os.addr, align 4
  %14 = bitcast %"class.std::__2::basic_ostream"* %13 to i8**
  %vtable6 = load i8*, i8** %14, align 4
  %vbase.offset.ptr7 = getelementptr i8, i8* %vtable6, i64 -12
  %15 = bitcast i8* %vbase.offset.ptr7 to i32*
  %vbase.offset8 = load i32, i32* %15, align 4
  %16 = bitcast %"class.std::__2::basic_ostream"* %13 to i8*
  %add.ptr9 = getelementptr inbounds i8, i8* %16, i32 %vbase.offset8
  %17 = bitcast i8* %add.ptr9 to %"class.std::__2::ios_base"*
  %18 = load %"class.std::__2::basic_ostream"*, %"class.std::__2::basic_ostream"** %__os.addr, align 4
  %19 = bitcast %"class.std::__2::basic_ostream"* %18 to i8**
  %vtable10 = load i8*, i8** %19, align 4
  %vbase.offset.ptr11 = getelementptr i8, i8* %vtable10, i64 -12
  %20 = bitcast i8* %vbase.offset.ptr11 to i32*
  %vbase.offset12 = load i32, i32* %20, align 4
  %21 = bitcast %"class.std::__2::basic_ostream"* %18 to i8*
  %add.ptr13 = getelementptr inbounds i8, i8* %21, i32 %vbase.offset12
  %22 = bitcast i8* %add.ptr13 to %"class.std::__2::basic_ios"*
  %call14 = call signext i8 @_ZNKSt3__29basic_iosIcNS_11char_traitsIcEEE4fillEv(%"class.std::__2::basic_ios"* %22)
  %coerce.dive = getelementptr inbounds %"class.std::__2::ostreambuf_iterator", %"class.std::__2::ostreambuf_iterator"* %agg.tmp, i32 0, i32 0
  %23 = load %"class.std::__2::basic_streambuf"*, %"class.std::__2::basic_streambuf"** %coerce.dive, align 4
  %call15 = call %"class.std::__2::basic_streambuf"* @_ZNSt3__216__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_(%"class.std::__2::basic_streambuf"* %23, i8* %2, i8* %cond, i8* %add.ptr5, %"class.std::__2::ios_base"* nonnull align 4 dereferenceable(72) %17, i8 signext %call14)
  %coerce.dive16 = getelementptr inbounds %"class.std::__2::ostreambuf_iterator", %"class.std::__2::ostreambuf_iterator"* %ref.tmp, i32 0, i32 0
  store %"class.std::__2::basic_streambuf"* %call15, %"class.std::__2::basic_streambuf"** %coerce.dive16, align 4
  %call17 = call zeroext i1 @_ZNKSt3__219ostreambuf_iteratorIcNS_11char_traitsIcEEE6failedEv(%"class.std::__2::ostreambuf_iterator"* %ref.tmp) #3
  br i1 %call17, label %if.then18, label %if.end

if.then18:                                        ; preds = %cond.end
  %24 = load %"class.std::__2::basic_ostream"*, %"class.std::__2::basic_ostream"** %__os.addr, align 4
  %25 = bitcast %"class.std::__2::basic_ostream"* %24 to i8**
  %vtable19 = load i8*, i8** %25, align 4
  %vbase.offset.ptr20 = getelementptr i8, i8* %vtable19, i64 -12
  %26 = bitcast i8* %vbase.offset.ptr20 to i32*
  %vbase.offset21 = load i32, i32* %26, align 4
  %27 = bitcast %"class.std::__2::basic_ostream"* %24 to i8*
  %add.ptr22 = getelementptr inbounds i8, i8* %27, i32 %vbase.offset21
  %28 = bitcast i8* %add.ptr22 to %"class.std::__2::basic_ios"*
  call void @_ZNSt3__29basic_iosIcNS_11char_traitsIcEEE8setstateEj(%"class.std::__2::basic_ios"* %28, i32 5)
  br label %if.end

if.end:                                           ; preds = %if.then18, %cond.end
  br label %if.end23

if.end23:                                         ; preds = %if.end, %entry
  %29 = load %"class.std::__2::basic_ostream"*, %"class.std::__2::basic_ostream"** %__os.addr, align 4
  %call24 = call %"class.std::__2::basic_ostream<char, std::__2::char_traits<char>>::sentry"* @_ZNSt3__213basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev(%"class.std::__2::basic_ostream<char, std::__2::char_traits<char>>::sentry"* %__s) #3
  ret %"class.std::__2::basic_ostream"* %29
}

declare %"class.std::__2::basic_ostream<char, std::__2::char_traits<char>>::sentry"* @_ZNSt3__213basic_ostreamIcNS_11char_traitsIcEEE6sentryC1ERS3_(%"class.std::__2::basic_ostream<char, std::__2::char_traits<char>>::sentry"* returned, %"class.std::__2::basic_ostream"* nonnull align 4 dereferenceable(4)) unnamed_addr #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__213basic_ostreamIcNS_11char_traitsIcEEE6sentrycvbEv(%"class.std::__2::basic_ostream<char, std::__2::char_traits<char>>::sentry"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_ostream<char, std::__2::char_traits<char>>::sentry"*, align 4
  store %"class.std::__2::basic_ostream<char, std::__2::char_traits<char>>::sentry"* %this, %"class.std::__2::basic_ostream<char, std::__2::char_traits<char>>::sentry"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_ostream<char, std::__2::char_traits<char>>::sentry"*, %"class.std::__2::basic_ostream<char, std::__2::char_traits<char>>::sentry"** %this.addr, align 4
  %__ok_ = getelementptr inbounds %"class.std::__2::basic_ostream<char, std::__2::char_traits<char>>::sentry", %"class.std::__2::basic_ostream<char, std::__2::char_traits<char>>::sentry"* %this1, i32 0, i32 0
  %0 = load i8, i8* %__ok_, align 4
  %tobool = trunc i8 %0 to i1
  ret i1 %tobool
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::basic_streambuf"* @_ZNSt3__216__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_(%"class.std::__2::basic_streambuf"* %__s.coerce, i8* %__ob, i8* %__op, i8* %__oe, %"class.std::__2::ios_base"* nonnull align 4 dereferenceable(72) %__iob, i8 signext %__fl) #1 comdat {
entry:
  %retval = alloca %"class.std::__2::ostreambuf_iterator", align 4
  %__s = alloca %"class.std::__2::ostreambuf_iterator", align 4
  %__ob.addr = alloca i8*, align 4
  %__op.addr = alloca i8*, align 4
  %__oe.addr = alloca i8*, align 4
  %__iob.addr = alloca %"class.std::__2::ios_base"*, align 4
  %__fl.addr = alloca i8, align 1
  %__sz = alloca i32, align 4
  %__ns = alloca i32, align 4
  %__np = alloca i32, align 4
  %__sp = alloca %"class.std::__2::basic_string", align 4
  %cleanup.dest.slot = alloca i32, align 4
  %coerce.dive = getelementptr inbounds %"class.std::__2::ostreambuf_iterator", %"class.std::__2::ostreambuf_iterator"* %__s, i32 0, i32 0
  store %"class.std::__2::basic_streambuf"* %__s.coerce, %"class.std::__2::basic_streambuf"** %coerce.dive, align 4
  store i8* %__ob, i8** %__ob.addr, align 4
  store i8* %__op, i8** %__op.addr, align 4
  store i8* %__oe, i8** %__oe.addr, align 4
  store %"class.std::__2::ios_base"* %__iob, %"class.std::__2::ios_base"** %__iob.addr, align 4
  store i8 %__fl, i8* %__fl.addr, align 1
  %__sbuf_ = getelementptr inbounds %"class.std::__2::ostreambuf_iterator", %"class.std::__2::ostreambuf_iterator"* %__s, i32 0, i32 0
  %0 = load %"class.std::__2::basic_streambuf"*, %"class.std::__2::basic_streambuf"** %__sbuf_, align 4
  %cmp = icmp eq %"class.std::__2::basic_streambuf"* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %"class.std::__2::ostreambuf_iterator"* %retval to i8*
  %2 = bitcast %"class.std::__2::ostreambuf_iterator"* %__s to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 4, i1 false)
  br label %return

if.end:                                           ; preds = %entry
  %3 = load i8*, i8** %__oe.addr, align 4
  %4 = load i8*, i8** %__ob.addr, align 4
  %sub.ptr.lhs.cast = ptrtoint i8* %3 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %4 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  store i32 %sub.ptr.sub, i32* %__sz, align 4
  %5 = load %"class.std::__2::ios_base"*, %"class.std::__2::ios_base"** %__iob.addr, align 4
  %call = call i32 @_ZNKSt3__28ios_base5widthEv(%"class.std::__2::ios_base"* %5)
  store i32 %call, i32* %__ns, align 4
  %6 = load i32, i32* %__ns, align 4
  %7 = load i32, i32* %__sz, align 4
  %cmp1 = icmp sgt i32 %6, %7
  br i1 %cmp1, label %if.then2, label %if.else

if.then2:                                         ; preds = %if.end
  %8 = load i32, i32* %__sz, align 4
  %9 = load i32, i32* %__ns, align 4
  %sub = sub nsw i32 %9, %8
  store i32 %sub, i32* %__ns, align 4
  br label %if.end3

if.else:                                          ; preds = %if.end
  store i32 0, i32* %__ns, align 4
  br label %if.end3

if.end3:                                          ; preds = %if.else, %if.then2
  %10 = load i8*, i8** %__op.addr, align 4
  %11 = load i8*, i8** %__ob.addr, align 4
  %sub.ptr.lhs.cast4 = ptrtoint i8* %10 to i32
  %sub.ptr.rhs.cast5 = ptrtoint i8* %11 to i32
  %sub.ptr.sub6 = sub i32 %sub.ptr.lhs.cast4, %sub.ptr.rhs.cast5
  store i32 %sub.ptr.sub6, i32* %__np, align 4
  %12 = load i32, i32* %__np, align 4
  %cmp7 = icmp sgt i32 %12, 0
  br i1 %cmp7, label %if.then8, label %if.end15

if.then8:                                         ; preds = %if.end3
  %__sbuf_9 = getelementptr inbounds %"class.std::__2::ostreambuf_iterator", %"class.std::__2::ostreambuf_iterator"* %__s, i32 0, i32 0
  %13 = load %"class.std::__2::basic_streambuf"*, %"class.std::__2::basic_streambuf"** %__sbuf_9, align 4
  %14 = load i8*, i8** %__ob.addr, align 4
  %15 = load i32, i32* %__np, align 4
  %call10 = call i32 @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE5sputnEPKcl(%"class.std::__2::basic_streambuf"* %13, i8* %14, i32 %15)
  %16 = load i32, i32* %__np, align 4
  %cmp11 = icmp ne i32 %call10, %16
  br i1 %cmp11, label %if.then12, label %if.end14

if.then12:                                        ; preds = %if.then8
  %__sbuf_13 = getelementptr inbounds %"class.std::__2::ostreambuf_iterator", %"class.std::__2::ostreambuf_iterator"* %__s, i32 0, i32 0
  store %"class.std::__2::basic_streambuf"* null, %"class.std::__2::basic_streambuf"** %__sbuf_13, align 4
  %17 = bitcast %"class.std::__2::ostreambuf_iterator"* %retval to i8*
  %18 = bitcast %"class.std::__2::ostreambuf_iterator"* %__s to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %17, i8* align 4 %18, i32 4, i1 false)
  br label %return

if.end14:                                         ; preds = %if.then8
  br label %if.end15

if.end15:                                         ; preds = %if.end14, %if.end3
  %19 = load i32, i32* %__ns, align 4
  %cmp16 = icmp sgt i32 %19, 0
  br i1 %cmp16, label %if.then17, label %if.end27

if.then17:                                        ; preds = %if.end15
  %20 = load i32, i32* %__ns, align 4
  %21 = load i8, i8* %__fl.addr, align 1
  %call18 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2Emc(%"class.std::__2::basic_string"* %__sp, i32 %20, i8 signext %21)
  %__sbuf_19 = getelementptr inbounds %"class.std::__2::ostreambuf_iterator", %"class.std::__2::ostreambuf_iterator"* %__s, i32 0, i32 0
  %22 = load %"class.std::__2::basic_streambuf"*, %"class.std::__2::basic_streambuf"** %__sbuf_19, align 4
  %call20 = call i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv(%"class.std::__2::basic_string"* %__sp) #3
  %23 = load i32, i32* %__ns, align 4
  %call21 = call i32 @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE5sputnEPKcl(%"class.std::__2::basic_streambuf"* %22, i8* %call20, i32 %23)
  %24 = load i32, i32* %__ns, align 4
  %cmp22 = icmp ne i32 %call21, %24
  br i1 %cmp22, label %if.then23, label %if.end25

if.then23:                                        ; preds = %if.then17
  %__sbuf_24 = getelementptr inbounds %"class.std::__2::ostreambuf_iterator", %"class.std::__2::ostreambuf_iterator"* %__s, i32 0, i32 0
  store %"class.std::__2::basic_streambuf"* null, %"class.std::__2::basic_streambuf"** %__sbuf_24, align 4
  %25 = bitcast %"class.std::__2::ostreambuf_iterator"* %retval to i8*
  %26 = bitcast %"class.std::__2::ostreambuf_iterator"* %__s to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %25, i8* align 4 %26, i32 4, i1 false)
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end25:                                         ; preds = %if.then17
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end25, %if.then23
  %call26 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_string"* %__sp) #3
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %return
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %if.end27

if.end27:                                         ; preds = %cleanup.cont, %if.end15
  %27 = load i8*, i8** %__oe.addr, align 4
  %28 = load i8*, i8** %__op.addr, align 4
  %sub.ptr.lhs.cast28 = ptrtoint i8* %27 to i32
  %sub.ptr.rhs.cast29 = ptrtoint i8* %28 to i32
  %sub.ptr.sub30 = sub i32 %sub.ptr.lhs.cast28, %sub.ptr.rhs.cast29
  store i32 %sub.ptr.sub30, i32* %__np, align 4
  %29 = load i32, i32* %__np, align 4
  %cmp31 = icmp sgt i32 %29, 0
  br i1 %cmp31, label %if.then32, label %if.end39

if.then32:                                        ; preds = %if.end27
  %__sbuf_33 = getelementptr inbounds %"class.std::__2::ostreambuf_iterator", %"class.std::__2::ostreambuf_iterator"* %__s, i32 0, i32 0
  %30 = load %"class.std::__2::basic_streambuf"*, %"class.std::__2::basic_streambuf"** %__sbuf_33, align 4
  %31 = load i8*, i8** %__op.addr, align 4
  %32 = load i32, i32* %__np, align 4
  %call34 = call i32 @_ZNSt3__215basic_streambufIcNS_11char_traitsIcEEE5sputnEPKcl(%"class.std::__2::basic_streambuf"* %30, i8* %31, i32 %32)
  %33 = load i32, i32* %__np, align 4
  %cmp35 = icmp ne i32 %call34, %33
  br i1 %cmp35, label %if.then36, label %if.end38

if.then36:                                        ; preds = %if.then32
  %__sbuf_37 = getelementptr inbounds %"class.std::__2::ostreambuf_iterator", %"class.std::__2::ostreambuf_iterator"* %__s, i32 0, i32 0
  store %"class.std::__2::basic_streambuf"* null, %"class.std::__2::basic_streambuf"** %__sbuf_37, align 4
  %34 = bitcast %"class.std::__2::ostreambuf_iterator"* %retval to i8*
  %35 = bitcast %"class.std::__2::ostreambuf_iterator"* %__s to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %34, i8* align 4 %35, i32 4, i1 false)
  br label %return

if.end38:                                         ; preds = %if.then32
  br label %if.end39

if.end39:                                         ; preds = %if.end38, %if.end27
  %36 = load %"class.std::__2::ios_base"*, %"class.std::__2::ios_base"** %__iob.addr, align 4
  %call40 = call i32 @_ZNSt3__28ios_base5widthEl(%"class.std::__2::ios_base"* %36, i32 0)
  %37 = bitcast %"class.std::__2::ostreambuf_iterator"* %retval to i8*
  %38 = bitcast %"class.std::__2::ostreambuf_iterator"* %__s to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %37, i8* align 4 %38, i32 4, i1 false)
  br label %return

return:                                           ; preds = %if.end39, %if.then36, %cleanup, %if.then12, %if.then
  %coerce.dive41 = getelementptr inbounds %"class.std::__2::ostreambuf_iterator", %"class.std::__2::ostreambuf_iterator"* %retval, i32 0, i32 0
  %39 = load %"class.std::__2::basic_streambuf"*, %"class.std::__2::basic_streambuf"** %coerce.dive41, align 4
  ret %"class.std::__2::basic_streambuf"* %39

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::ostreambuf_iterator"* @_ZNSt3__219ostreambuf_iteratorIcNS_11char_traitsIcEEEC2ERNS_13basic_ostreamIcS2_EE(%"class.std::__2::ostreambuf_iterator"* returned %this, %"class.std::__2::basic_ostream"* nonnull align 4 dereferenceable(4) %__s) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::ostreambuf_iterator"*, align 4
  %__s.addr = alloca %"class.std::__2::basic_ostream"*, align 4
  store %"class.std::__2::ostreambuf_iterator"* %this, %"class.std::__2::ostreambuf_iterator"** %this.addr, align 4
  store %"class.std::__2::basic_ostream"* %__s, %"class.std::__2::basic_ostream"** %__s.addr, align 4
  %this1 = load %"class.std::__2::ostreambuf_iterator"*, %"class.std::__2::ostreambuf_iterator"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::ostreambuf_iterator"* %this1 to %"struct.std::__2::iterator"*
  %__sbuf_ = getelementptr inbounds %"class.std::__2::ostreambuf_iterator", %"class.std::__2::ostreambuf_iterator"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::basic_ostream"*, %"class.std::__2::basic_ostream"** %__s.addr, align 4
  %2 = bitcast %"class.std::__2::basic_ostream"* %1 to i8**
  %vtable = load i8*, i8** %2, align 4
  %vbase.offset.ptr = getelementptr i8, i8* %vtable, i64 -12
  %3 = bitcast i8* %vbase.offset.ptr to i32*
  %vbase.offset = load i32, i32* %3, align 4
  %4 = bitcast %"class.std::__2::basic_ostream"* %1 to i8*
  %add.ptr = getelementptr inbounds i8, i8* %4, i32 %vbase.offset
  %5 = bitcast i8* %add.ptr to %"class.std::__2::basic_ios"*
  %call = call %"class.std::__2::basic_streambuf"* @_ZNKSt3__29basic_iosIcNS_11char_traitsIcEEE5rdbufEv(%"class.std::__2::basic_ios"* %5)
  store %"class.std::__2::basic_streambuf"* %call, %"class.std::__2::basic_streambuf"** %__sbuf_, align 4
  ret %"class.std::__2::ostreambuf_iterator"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__28ios_base5flagsEv(%"class.std::__2::ios_base"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::ios_base"*, align 4
  store %"class.std::__2::ios_base"* %this, %"class.std::__2::ios_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::ios_base"*, %"class.std::__2::ios_base"** %this.addr, align 4
  %__fmtflags_ = getelementptr inbounds %"class.std::__2::ios_base", %"class.std::__2::ios_base"* %this1, i32 0, i32 1
  %0 = load i32, i32* %__fmtflags_, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden signext i8 @_ZNKSt3__29basic_iosIcNS_11char_traitsIcEEE4fillEv(%"class.std::__2::basic_ios"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_ios"*, align 4
  store %"class.std::__2::basic_ios"* %this, %"class.std::__2::basic_ios"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_ios"*, %"class.std::__2::basic_ios"** %this.addr, align 4
  %call = call i32 @_ZNSt3__211char_traitsIcE3eofEv() #3
  %__fill_ = getelementptr inbounds %"class.std::__2::basic_ios", %"class.std::__2::basic_ios"* %this1, i32 0, i32 2
  %0 = load i32, i32* %__fill_, align 4
  %call2 = call zeroext i1 @_ZNSt3__211char_traitsIcE11eq_int_typeEii(i32 %call, i32 %0) #3
  br i1 %call2, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call signext i8 @_ZNKSt3__29basic_iosIcNS_11char_traitsIcEEE5widenEc(%"class.std::__2::basic_ios"* %this1, i8 signext 32)
  %conv = sext i8 %call3 to i32
  %__fill_4 = getelementptr inbounds %"class.std::__2::basic_ios", %"class.std::__2::basic_ios"* %this1, i32 0, i32 2
  store i32 %conv, i32* %__fill_4, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %__fill_5 = getelementptr inbounds %"class.std::__2::basic_ios", %"class.std::__2::basic_ios"* %this1, i32 0, i32 2
  %1 = load i32, i32* %__fill_5, align 4
  %conv6 = trunc i32 %1 to i8
  ret i8 %conv6
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__219ostreambuf_iteratorIcNS_11char_traitsIcEEE6failedEv(%"class.std::__2::ostreambuf_iterator"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::ostreambuf_iterator"*, align 4
  store %"class.std::__2::ostreambuf_iterator"* %this, %"class.std::__2::ostreambuf_iterator"** %this.addr, align 4
  %this1 = load %"class.std::__2::ostreambuf_iterator"*, %"class.std::__2::ostreambuf_iterator"** %this.addr, align 4
  %__sbuf_ = getelementptr inbounds %"class.std::__2::ostreambuf_iterator", %"class.std::__2::ostreambuf_iterator"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::basic_streambuf"*, %"class.std::__2::basic_streambuf"** %__sbuf_, align 4
  %cmp = icmp eq %"class.std::__2::basic_streambuf"* %0, null
  ret i1 %cmp
}

; Function Attrs: nounwind
declare %"class.std::__2::basic_ostream<char, std::__2::char_traits<char>>::sentry"* @_ZNSt3__213basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev(%"class.std::__2::basic_ostream<char, std::__2::char_traits<char>>::sentry"* returned) unnamed_addr #2

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__28ios_base5widthEv(%"class.std::__2::ios_base"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::ios_base"*, align 4
  store %"class.std::__2::ios_base"* %this, %"class.std::__2::ios_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::ios_base"*, %"class.std::__2::ios_base"** %this.addr, align 4
  %__width_ = getelementptr inbounds %"class.std::__2::ios_base", %"class.std::__2::ios_base"* %this1, i32 0, i32 3
  %0 = load i32, i32* %__width_, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2Emc(%"class.std::__2::basic_string"* returned %this, i32 %__n, i8 signext %__c) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  %__n.addr = alloca i32, align 4
  %__c.addr = alloca i8, align 1
  %ref.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  %ref.tmp2 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store i8 %__c, i8* %__c.addr, align 1
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::basic_string"* %this1 to %"class.std::__2::__basic_string_common"*
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::__compressed_pair"* @_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_EC2INS_18__default_init_tagESA_EEOT_OT0_(%"class.std::__2::__compressed_pair"* %__r_, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  %1 = load i32, i32* %__n.addr, align 4
  %2 = load i8, i8* %__c.addr, align 1
  call void @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEmc(%"class.std::__2::basic_string"* %this1, i32 %1, i8 signext %2)
  ret %"class.std::__2::basic_string"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__28ios_base5widthEl(%"class.std::__2::ios_base"* %this, i32 %__wide) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::ios_base"*, align 4
  %__wide.addr = alloca i32, align 4
  %__r = alloca i32, align 4
  store %"class.std::__2::ios_base"* %this, %"class.std::__2::ios_base"** %this.addr, align 4
  store i32 %__wide, i32* %__wide.addr, align 4
  %this1 = load %"class.std::__2::ios_base"*, %"class.std::__2::ios_base"** %this.addr, align 4
  %__width_ = getelementptr inbounds %"class.std::__2::ios_base", %"class.std::__2::ios_base"* %this1, i32 0, i32 3
  %0 = load i32, i32* %__width_, align 4
  store i32 %0, i32* %__r, align 4
  %1 = load i32, i32* %__wide.addr, align 4
  %__width_2 = getelementptr inbounds %"class.std::__2::ios_base", %"class.std::__2::ios_base"* %this1, i32 0, i32 3
  store i32 %1, i32* %__width_2, align 4
  %2 = load i32, i32* %__r, align 4
  ret i32 %2
}

declare void @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEmc(%"class.std::__2::basic_string"*, i32, i8 signext) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden signext i8 @_ZNKSt3__29basic_iosIcNS_11char_traitsIcEEE5widenEc(%"class.std::__2::basic_ios"* %this, i8 signext %__c) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_ios"*, align 4
  %__c.addr = alloca i8, align 1
  %ref.tmp = alloca %"class.std::__2::locale", align 4
  store %"class.std::__2::basic_ios"* %this, %"class.std::__2::basic_ios"** %this.addr, align 4
  store i8 %__c, i8* %__c.addr, align 1
  %this1 = load %"class.std::__2::basic_ios"*, %"class.std::__2::basic_ios"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::basic_ios"* %this1 to %"class.std::__2::ios_base"*
  call void @_ZNKSt3__28ios_base6getlocEv(%"class.std::__2::locale"* sret align 4 %ref.tmp, %"class.std::__2::ios_base"* %0)
  %call = call nonnull align 4 dereferenceable(13) %"class.std::__2::ctype"* @_ZNSt3__29use_facetINS_5ctypeIcEEEERKT_RKNS_6localeE(%"class.std::__2::locale"* nonnull align 4 dereferenceable(4) %ref.tmp)
  %1 = load i8, i8* %__c.addr, align 1
  %call2 = call signext i8 @_ZNKSt3__25ctypeIcE5widenEc(%"class.std::__2::ctype"* %call, i8 signext %1)
  %call3 = call %"class.std::__2::locale"* @_ZNSt3__26localeD1Ev(%"class.std::__2::locale"* %ref.tmp) #3
  ret i8 %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(13) %"class.std::__2::ctype"* @_ZNSt3__29use_facetINS_5ctypeIcEEEERKT_RKNS_6localeE(%"class.std::__2::locale"* nonnull align 4 dereferenceable(4) %__l) #1 comdat {
entry:
  %__l.addr = alloca %"class.std::__2::locale"*, align 4
  store %"class.std::__2::locale"* %__l, %"class.std::__2::locale"** %__l.addr, align 4
  %0 = load %"class.std::__2::locale"*, %"class.std::__2::locale"** %__l.addr, align 4
  %call = call %"class.std::__2::locale::facet"* @_ZNKSt3__26locale9use_facetERNS0_2idE(%"class.std::__2::locale"* %0, %"class.std::__2::locale::id"* nonnull align 4 dereferenceable(8) @_ZNSt3__25ctypeIcE2idE)
  %1 = bitcast %"class.std::__2::locale::facet"* %call to %"class.std::__2::ctype"*
  ret %"class.std::__2::ctype"* %1
}

declare void @_ZNKSt3__28ios_base6getlocEv(%"class.std::__2::locale"* sret align 4, %"class.std::__2::ios_base"*) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden signext i8 @_ZNKSt3__25ctypeIcE5widenEc(%"class.std::__2::ctype"* %this, i8 signext %__c) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::ctype"*, align 4
  %__c.addr = alloca i8, align 1
  store %"class.std::__2::ctype"* %this, %"class.std::__2::ctype"** %this.addr, align 4
  store i8 %__c, i8* %__c.addr, align 1
  %this1 = load %"class.std::__2::ctype"*, %"class.std::__2::ctype"** %this.addr, align 4
  %0 = load i8, i8* %__c.addr, align 1
  %1 = bitcast %"class.std::__2::ctype"* %this1 to i8 (%"class.std::__2::ctype"*, i8)***
  %vtable = load i8 (%"class.std::__2::ctype"*, i8)**, i8 (%"class.std::__2::ctype"*, i8)*** %1, align 4
  %vfn = getelementptr inbounds i8 (%"class.std::__2::ctype"*, i8)*, i8 (%"class.std::__2::ctype"*, i8)** %vtable, i64 7
  %2 = load i8 (%"class.std::__2::ctype"*, i8)*, i8 (%"class.std::__2::ctype"*, i8)** %vfn, align 4
  %call = call signext i8 %2(%"class.std::__2::ctype"* %this1, i8 signext %0)
  ret i8 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__iom_t4"* @_ZNSt3__28__iom_t4IcEC2Ec(%"class.std::__2::__iom_t4"* returned %this, i8 signext %__c) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__iom_t4"*, align 4
  %__c.addr = alloca i8, align 1
  store %"class.std::__2::__iom_t4"* %this, %"class.std::__2::__iom_t4"** %this.addr, align 4
  store i8 %__c, i8* %__c.addr, align 1
  %this1 = load %"class.std::__2::__iom_t4"*, %"class.std::__2::__iom_t4"** %this.addr, align 4
  %__fill_ = getelementptr inbounds %"class.std::__2::__iom_t4", %"class.std::__2::__iom_t4"* %this1, i32 0, i32 0
  %0 = load i8, i8* %__c.addr, align 1
  store i8 %0, i8* %__fill_, align 1
  ret %"class.std::__2::__iom_t4"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden signext i8 @_ZNSt3__29basic_iosIcNS_11char_traitsIcEEE4fillEc(%"class.std::__2::basic_ios"* %this, i8 signext %__ch) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_ios"*, align 4
  %__ch.addr = alloca i8, align 1
  %__r = alloca i8, align 1
  store %"class.std::__2::basic_ios"* %this, %"class.std::__2::basic_ios"** %this.addr, align 4
  store i8 %__ch, i8* %__ch.addr, align 1
  %this1 = load %"class.std::__2::basic_ios"*, %"class.std::__2::basic_ios"** %this.addr, align 4
  %__fill_ = getelementptr inbounds %"class.std::__2::basic_ios", %"class.std::__2::basic_ios"* %this1, i32 0, i32 2
  %0 = load i32, i32* %__fill_, align 4
  %conv = trunc i32 %0 to i8
  store i8 %conv, i8* %__r, align 1
  %1 = load i8, i8* %__ch.addr, align 1
  %conv2 = sext i8 %1 to i32
  %__fill_3 = getelementptr inbounds %"class.std::__2::basic_ios", %"class.std::__2::basic_ios"* %this1, i32 0, i32 2
  store i32 %conv2, i32* %__fill_3, align 4
  %2 = load i8, i8* %__r, align 1
  ret i8 %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__215basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE3strEv(%"class.std::__2::basic_string"* noalias sret align 4 %agg.result, %"class.std::__2::basic_stringbuf"* %this) #1 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %"class.std::__2::basic_stringbuf"*, align 4
  %ref.tmp = alloca %"class.std::__2::allocator", align 1
  %undef.agg.tmp = alloca %"class.std::__2::allocator", align 1
  %ref.tmp14 = alloca %"class.std::__2::allocator", align 1
  %undef.agg.tmp16 = alloca %"class.std::__2::allocator", align 1
  %ref.tmp20 = alloca %"class.std::__2::allocator", align 1
  %undef.agg.tmp22 = alloca %"class.std::__2::allocator", align 1
  %0 = bitcast %"class.std::__2::basic_string"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %"class.std::__2::basic_stringbuf"* %this, %"class.std::__2::basic_stringbuf"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_stringbuf"*, %"class.std::__2::basic_stringbuf"** %this.addr, align 4
  %__mode_ = getelementptr inbounds %"class.std::__2::basic_stringbuf", %"class.std::__2::basic_stringbuf"* %this1, i32 0, i32 3
  %1 = load i32, i32* %__mode_, align 4
  %and = and i32 %1, 16
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %__hm_ = getelementptr inbounds %"class.std::__2::basic_stringbuf", %"class.std::__2::basic_stringbuf"* %this1, i32 0, i32 2
  %2 = load i8*, i8** %__hm_, align 4
  %3 = bitcast %"class.std::__2::basic_stringbuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE4pptrEv(%"class.std::__2::basic_streambuf"* %3)
  %cmp = icmp ult i8* %2, %call
  br i1 %cmp, label %if.then2, label %if.end

if.then2:                                         ; preds = %if.then
  %4 = bitcast %"class.std::__2::basic_stringbuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call3 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE4pptrEv(%"class.std::__2::basic_streambuf"* %4)
  %__hm_4 = getelementptr inbounds %"class.std::__2::basic_stringbuf", %"class.std::__2::basic_stringbuf"* %this1, i32 0, i32 2
  store i8* %call3, i8** %__hm_4, align 4
  br label %if.end

if.end:                                           ; preds = %if.then2, %if.then
  %5 = bitcast %"class.std::__2::basic_stringbuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call5 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE5pbaseEv(%"class.std::__2::basic_streambuf"* %5)
  %__hm_6 = getelementptr inbounds %"class.std::__2::basic_stringbuf", %"class.std::__2::basic_stringbuf"* %this1, i32 0, i32 2
  %6 = load i8*, i8** %__hm_6, align 4
  %__str_ = getelementptr inbounds %"class.std::__2::basic_stringbuf", %"class.std::__2::basic_stringbuf"* %this1, i32 0, i32 1
  call void @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE13get_allocatorEv(%"class.std::__2::basic_string"* %__str_) #3
  %call7 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2IPcvEET_S8_RKS4_(%"class.std::__2::basic_string"* %agg.result, i8* %call5, i8* %6, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %ref.tmp)
  br label %return

if.else:                                          ; preds = %entry
  %__mode_8 = getelementptr inbounds %"class.std::__2::basic_stringbuf", %"class.std::__2::basic_stringbuf"* %this1, i32 0, i32 3
  %7 = load i32, i32* %__mode_8, align 4
  %and9 = and i32 %7, 8
  %tobool10 = icmp ne i32 %and9, 0
  br i1 %tobool10, label %if.then11, label %if.end18

if.then11:                                        ; preds = %if.else
  %8 = bitcast %"class.std::__2::basic_stringbuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call12 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE5ebackEv(%"class.std::__2::basic_streambuf"* %8)
  %9 = bitcast %"class.std::__2::basic_stringbuf"* %this1 to %"class.std::__2::basic_streambuf"*
  %call13 = call i8* @_ZNKSt3__215basic_streambufIcNS_11char_traitsIcEEE5egptrEv(%"class.std::__2::basic_streambuf"* %9)
  %__str_15 = getelementptr inbounds %"class.std::__2::basic_stringbuf", %"class.std::__2::basic_stringbuf"* %this1, i32 0, i32 1
  call void @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE13get_allocatorEv(%"class.std::__2::basic_string"* %__str_15) #3
  %call17 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2IPcvEET_S8_RKS4_(%"class.std::__2::basic_string"* %agg.result, i8* %call12, i8* %call13, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %ref.tmp14)
  br label %return

if.end18:                                         ; preds = %if.else
  br label %if.end19

if.end19:                                         ; preds = %if.end18
  %__str_21 = getelementptr inbounds %"class.std::__2::basic_stringbuf", %"class.std::__2::basic_stringbuf"* %this1, i32 0, i32 1
  call void @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE13get_allocatorEv(%"class.std::__2::basic_string"* %__str_21) #3
  %call23 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2ERKS4_(%"class.std::__2::basic_string"* %agg.result, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %ref.tmp20) #3
  br label %return

return:                                           ; preds = %if.end19, %if.then11, %if.end
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2IPcvEET_S8_RKS4_(%"class.std::__2::basic_string"* returned %this, i8* %__first, i8* %__last, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  %__first.addr = alloca i8*, align 4
  %__last.addr = alloca i8*, align 4
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %ref.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  store i8* %__first, i8** %__first.addr, align 4
  store i8* %__last, i8** %__last.addr, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::basic_string"* %this1 to %"class.std::__2::__basic_string_common"*
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %call = call %"class.std::__2::__compressed_pair"* @_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_EC2INS_18__default_init_tagERKS5_EEOT_OT0_(%"class.std::__2::__compressed_pair"* %__r_, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %1)
  %2 = load i8*, i8** %__first.addr, align 4
  %3 = load i8*, i8** %__last.addr, align 4
  call void @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initIPcEENS_9enable_ifIXsr27__is_cpp17_forward_iteratorIT_EE5valueEvE4typeES9_S9_(%"class.std::__2::basic_string"* %this1, i8* %2, i8* %3)
  ret %"class.std::__2::basic_string"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr void @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initIPcEENS_9enable_ifIXsr27__is_cpp17_forward_iteratorIT_EE5valueEvE4typeES9_S9_(%"class.std::__2::basic_string"* %this, i8* %__first, i8* %__last) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  %__first.addr = alloca i8*, align 4
  %__last.addr = alloca i8*, align 4
  %__sz = alloca i32, align 4
  %__p = alloca i8*, align 4
  %__cap = alloca i32, align 4
  %ref.tmp = alloca i8, align 1
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  store i8* %__first, i8** %__first.addr, align 4
  store i8* %__last, i8** %__last.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %0 = load i8*, i8** %__first.addr, align 4
  %1 = load i8*, i8** %__last.addr, align 4
  %call = call i32 @_ZNSt3__28distanceIPcEENS_15iterator_traitsIT_E15difference_typeES3_S3_(i8* %0, i8* %1)
  store i32 %call, i32* %__sz, align 4
  %2 = load i32, i32* %__sz, align 4
  %call2 = call i32 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE8max_sizeEv(%"class.std::__2::basic_string"* %this1) #3
  %cmp = icmp ugt i32 %2, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %3 = bitcast %"class.std::__2::basic_string"* %this1 to %"class.std::__2::__basic_string_common"*
  call void @_ZNKSt3__221__basic_string_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__basic_string_common"* %3) #13
  unreachable

if.end:                                           ; preds = %entry
  %4 = load i32, i32* %__sz, align 4
  %cmp3 = icmp ult i32 %4, 11
  br i1 %cmp3, label %if.then4, label %if.else

if.then4:                                         ; preds = %if.end
  %5 = load i32, i32* %__sz, align 4
  call void @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE16__set_short_sizeEm(%"class.std::__2::basic_string"* %this1, i32 %5) #3
  %call5 = call i8* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE19__get_short_pointerEv(%"class.std::__2::basic_string"* %this1) #3
  store i8* %call5, i8** %__p, align 4
  br label %if.end10

if.else:                                          ; preds = %if.end
  %6 = load i32, i32* %__sz, align 4
  %call6 = call i32 @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE11__recommendEm(i32 %6) #3
  store i32 %call6, i32* %__cap, align 4
  %call7 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE7__allocEv(%"class.std::__2::basic_string"* %this1) #3
  %7 = load i32, i32* %__cap, align 4
  %add = add i32 %7, 1
  %call8 = call i8* @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE8allocateERS2_m(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call7, i32 %add)
  store i8* %call8, i8** %__p, align 4
  %8 = load i8*, i8** %__p, align 4
  call void @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE18__set_long_pointerEPc(%"class.std::__2::basic_string"* %this1, i8* %8) #3
  %9 = load i32, i32* %__cap, align 4
  %add9 = add i32 %9, 1
  call void @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE14__set_long_capEm(%"class.std::__2::basic_string"* %this1, i32 %add9) #3
  %10 = load i32, i32* %__sz, align 4
  call void @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE15__set_long_sizeEm(%"class.std::__2::basic_string"* %this1, i32 %10) #3
  br label %if.end10

if.end10:                                         ; preds = %if.else, %if.then4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end10
  %11 = load i8*, i8** %__first.addr, align 4
  %12 = load i8*, i8** %__last.addr, align 4
  %cmp11 = icmp ne i8* %11, %12
  br i1 %cmp11, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %13 = load i8*, i8** %__p, align 4
  %14 = load i8*, i8** %__first.addr, align 4
  call void @_ZNSt3__211char_traitsIcE6assignERcRKc(i8* nonnull align 1 dereferenceable(1) %13, i8* nonnull align 1 dereferenceable(1) %14) #3
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %15 = load i8*, i8** %__first.addr, align 4
  %incdec.ptr = getelementptr inbounds i8, i8* %15, i32 1
  store i8* %incdec.ptr, i8** %__first.addr, align 4
  %16 = load i8*, i8** %__p, align 4
  %incdec.ptr12 = getelementptr inbounds i8, i8* %16, i32 1
  store i8* %incdec.ptr12, i8** %__p, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %17 = load i8*, i8** %__p, align 4
  store i8 0, i8* %ref.tmp, align 1
  call void @_ZNSt3__211char_traitsIcE6assignERcRKc(i8* nonnull align 1 dereferenceable(1) %17, i8* nonnull align 1 dereferenceable(1) %ref.tmp) #3
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__28distanceIPcEENS_15iterator_traitsIT_E15difference_typeES3_S3_(i8* %__first, i8* %__last) #1 comdat {
entry:
  %__first.addr = alloca i8*, align 4
  %__last.addr = alloca i8*, align 4
  %agg.tmp = alloca %"struct.std::__2::random_access_iterator_tag", align 1
  store i8* %__first, i8** %__first.addr, align 4
  store i8* %__last, i8** %__last.addr, align 4
  %0 = load i8*, i8** %__first.addr, align 4
  %1 = load i8*, i8** %__last.addr, align 4
  %call = call i32 @_ZNSt3__210__distanceIPcEENS_15iterator_traitsIT_E15difference_typeES3_S3_NS_26random_access_iterator_tagE(i8* %0, i8* %1)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE8max_sizeEv(%"class.std::__2::basic_string"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  %__m = alloca i32, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE7__allocEv(%"class.std::__2::basic_string"* %this1) #3
  %call2 = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE8max_sizeERKS2_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call) #3
  store i32 %call2, i32* %__m, align 4
  %0 = load i32, i32* %__m, align 4
  %sub = sub i32 %0, 16
  ret i32 %sub
}

; Function Attrs: noreturn
declare void @_ZNKSt3__221__basic_string_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__basic_string_common"*) #10

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE16__set_short_sizeEm(%"class.std::__2::basic_string"* %this, i32 %__s) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  %__s.addr = alloca i32, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  store i32 %__s, i32* %__s.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %0 = load i32, i32* %__s.addr, align 4
  %conv = trunc i32 %0 to i8
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair"* %__r_) #3
  %1 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call, i32 0, i32 0
  %__s2 = bitcast %union.anon* %1 to %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short"*
  %2 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short"* %__s2, i32 0, i32 1
  %__size_ = getelementptr inbounds %struct.anon, %struct.anon* %2, i32 0, i32 0
  store i8 %conv, i8* %__size_, align 1
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE19__get_short_pointerEv(%"class.std::__2::basic_string"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair"* %__r_) #3
  %0 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call, i32 0, i32 0
  %__s = bitcast %union.anon* %0 to %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short"*
  %__data_ = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short"* %__s, i32 0, i32 0
  %arrayidx = getelementptr inbounds [11 x i8], [11 x i8]* %__data_, i32 0, i32 0
  %call2 = call i8* @_ZNSt3__214pointer_traitsIPcE10pointer_toERc(i8* nonnull align 1 dereferenceable(1) %arrayidx) #3
  ret i8* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE11__recommendEm(i32 %__s) #1 comdat {
entry:
  %retval = alloca i32, align 4
  %__s.addr = alloca i32, align 4
  %__guess = alloca i32, align 4
  store i32 %__s, i32* %__s.addr, align 4
  %0 = load i32, i32* %__s.addr, align 4
  %cmp = icmp ult i32 %0, 11
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 10, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load i32, i32* %__s.addr, align 4
  %add = add i32 %1, 1
  %call = call i32 @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE10__align_itILm16EEEmm(i32 %add) #3
  %sub = sub i32 %call, 1
  store i32 %sub, i32* %__guess, align 4
  %2 = load i32, i32* %__guess, align 4
  %cmp1 = icmp eq i32 %2, 11
  br i1 %cmp1, label %if.then2, label %if.end3

if.then2:                                         ; preds = %if.end
  %3 = load i32, i32* %__guess, align 4
  %inc = add i32 %3, 1
  store i32 %inc, i32* %__guess, align 4
  br label %if.end3

if.end3:                                          ; preds = %if.then2, %if.end
  %4 = load i32, i32* %__guess, align 4
  store i32 %4, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end3, %if.then
  %5 = load i32, i32* %retval, align 4
  ret i32 %5
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE8allocateERS2_m(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, i32 %__n) #1 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call i8* @_ZNSt3__29allocatorIcE8allocateEmPKv(%"class.std::__2::allocator"* %0, i32 %1, i8* null)
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE7__allocEv(%"class.std::__2::basic_string"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E6secondEv(%"class.std::__2::__compressed_pair"* %__r_) #3
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE18__set_long_pointerEPc(%"class.std::__2::basic_string"* %this, i8* %__p) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  %__p.addr = alloca i8*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %0 = load i8*, i8** %__p.addr, align 4
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair"* %__r_) #3
  %1 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call, i32 0, i32 0
  %__l = bitcast %union.anon* %1 to %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long"*
  %__data_ = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long"* %__l, i32 0, i32 0
  store i8* %0, i8** %__data_, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE14__set_long_capEm(%"class.std::__2::basic_string"* %this, i32 %__s) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  %__s.addr = alloca i32, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  store i32 %__s, i32* %__s.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %0 = load i32, i32* %__s.addr, align 4
  %or = or i32 -2147483648, %0
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair"* %__r_) #3
  %1 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call, i32 0, i32 0
  %__l = bitcast %union.anon* %1 to %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long"*
  %__cap_ = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long"* %__l, i32 0, i32 2
  store i32 %or, i32* %__cap_, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE15__set_long_sizeEm(%"class.std::__2::basic_string"* %this, i32 %__s) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  %__s.addr = alloca i32, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  store i32 %__s, i32* %__s.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %0 = load i32, i32* %__s.addr, align 4
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair"* %__r_) #3
  %1 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call, i32 0, i32 0
  %__l = bitcast %union.anon* %1 to %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long"*
  %__size_ = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long"* %__l, i32 0, i32 1
  store i32 %0, i32* %__size_, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__211char_traitsIcE6assignERcRKc(i8* nonnull align 1 dereferenceable(1) %__c1, i8* nonnull align 1 dereferenceable(1) %__c2) #1 comdat {
entry:
  %__c1.addr = alloca i8*, align 4
  %__c2.addr = alloca i8*, align 4
  store i8* %__c1, i8** %__c1.addr, align 4
  store i8* %__c2, i8** %__c2.addr, align 4
  %0 = load i8*, i8** %__c2.addr, align 4
  %1 = load i8, i8* %0, align 1
  %2 = load i8*, i8** %__c1.addr, align 4
  store i8 %1, i8* %2, align 1
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__210__distanceIPcEENS_15iterator_traitsIT_E15difference_typeES3_S3_NS_26random_access_iterator_tagE(i8* %__first, i8* %__last) #1 comdat {
entry:
  %0 = alloca %"struct.std::__2::random_access_iterator_tag", align 1
  %__first.addr = alloca i8*, align 4
  %__last.addr = alloca i8*, align 4
  store i8* %__first, i8** %__first.addr, align 4
  store i8* %__last, i8** %__last.addr, align 4
  %1 = load i8*, i8** %__last.addr, align 4
  %2 = load i8*, i8** %__first.addr, align 4
  %sub.ptr.lhs.cast = ptrtoint i8* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %2 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  ret i32 %sub.ptr.sub
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE8max_sizeERKS2_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a) #1 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_max_size.12", align 1
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_max_size.12"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %call = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE10__max_sizeENS_17integral_constantIbLb1EEERKS2_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %1) #3
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE10__max_sizeENS_17integral_constantIbLb1EEERKS2_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a) #1 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorIcE8max_sizeEv(%"class.std::__2::allocator"* %1) #3
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__29allocatorIcE8max_sizeEv(%"class.std::__2::allocator"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  ret i32 -1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__214pointer_traitsIPcE10pointer_toERc(i8* nonnull align 1 dereferenceable(1) %__r) #1 comdat {
entry:
  %__r.addr = alloca i8*, align 4
  store i8* %__r, i8** %__r.addr, align 4
  %0 = load i8*, i8** %__r.addr, align 4
  %call = call i8* @_ZNSt3__29addressofIcEEPT_RS1_(i8* nonnull align 1 dereferenceable(1) %0) #3
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__29addressofIcEEPT_RS1_(i8* nonnull align 1 dereferenceable(1) %__x) #1 comdat {
entry:
  %__x.addr = alloca i8*, align 4
  store i8* %__x, i8** %__x.addr, align 4
  %0 = load i8*, i8** %__x.addr, align 4
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr i32 @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE10__align_itILm16EEEmm(i32 %__s) #1 comdat {
entry:
  %__s.addr = alloca i32, align 4
  store i32 %__s, i32* %__s.addr, align 4
  %0 = load i32, i32* %__s.addr, align 4
  %add = add i32 %0, 15
  %and = and i32 %add, -16
  ret i32 %and
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__29allocatorIcE8allocateEmPKv(%"class.std::__2::allocator"* %this, i32 %__n, i8* %0) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  %__n.addr = alloca i32, align 4
  %.addr = alloca i8*, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorIcE8max_sizeEv(%"class.std::__2::allocator"* %this1) #3
  %cmp = icmp ugt i32 %1, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__220__throw_length_errorEPKc(i8* getelementptr inbounds ([68 x i8], [68 x i8]* @.str.27, i32 0, i32 0)) #13
  unreachable

if.end:                                           ; preds = %entry
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 1
  %call2 = call i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %mul, i32 1)
  ret i8* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E6secondEv(%"class.std::__2::__compressed_pair"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem.0"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIcEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.0"* %0) #3
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIcEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.0"* %this) #1 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.0"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.0"* %this, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.0"*, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.0"* %this1 to %"class.std::__2::allocator"*
  ret %"class.std::__2::allocator"* %0
}

declare nonnull align 4 dereferenceable(4) %"class.std::__2::basic_ostream"* @_ZNSt3__213basic_ostreamIcNS_11char_traitsIcEEE3putEc(%"class.std::__2::basic_ostream"*, i8 signext) #4

declare nonnull align 4 dereferenceable(4) %"class.std::__2::basic_ostream"* @_ZNSt3__213basic_ostreamIcNS_11char_traitsIcEEE5flushEv(%"class.std::__2::basic_ostream"*) #4

; Function Attrs: noinline nounwind
define internal void @_GLOBAL__sub_I_misc.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  call void @__cxx_global_var_init.1()
  ret void
}

attributes #0 = { noinline nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind }
attributes #4 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { argmemonly nounwind willreturn writeonly }
attributes #7 = { nobuiltin allocsize(0) "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #8 = { noinline noreturn nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #9 = { argmemonly nounwind willreturn }
attributes #10 = { noreturn "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #11 = { builtin nounwind }
attributes #12 = { builtin allocsize(0) }
attributes #13 = { noreturn }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!"branch_weights", i32 1, i32 1048575}
