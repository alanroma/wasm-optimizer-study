[PassRunner] running passes...
[PassRunner]   running pass: strip-dwarf...                    0.000125822 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: post-emscripten...                0.00175279 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inline-main...                    0.000136969 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: no-exit-runtime...                0.00847057 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 0.0107383 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: memory-packing...                 0.000922039 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dce...                            0.0509759 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.0672186 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.00694135 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.0266585 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: pick-load-signs...                0.0123415 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.0318027 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-added-constants...       0.00693224 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: code-pushing...                   0.0296982 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals-nostructure...    0.131306 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0613575 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.0215755 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.028661 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.036646 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals...                0.0816166 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.043315 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.0112758 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.0358077 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.0109329 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0416291 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.0210269 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.0355439 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.00588147 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.0189123 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.0229455 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.0210725 seconds.
[PassRunner]   (validating)
[PassRunner] passes took 0.884221 seconds.
[PassRunner] (final validation)
