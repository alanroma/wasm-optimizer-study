[PassRunner] running passes...
[PassRunner]   running pass: strip-dwarf...                    0.000124671 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: post-emscripten...                0.00172575 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inline-main...                    0.000132018 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: no-exit-runtime...                0.00858039 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 0.0107481 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: memory-packing...                 0.000852234 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dce...                            0.0506429 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.0679015 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.00707754 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.0266918 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: pick-load-signs...                0.0121963 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.0313767 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-added-constants...       0.00686381 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: code-pushing...                   0.0298742 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals-nostructure...    0.13065 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0608581 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.0213749 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.0285262 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.0366736 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals...                0.0817069 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0425462 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.0113226 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.0361024 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.0110482 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0413006 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.0211104 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.0362208 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.00591313 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.0187918 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.0229761 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.0213505 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: rse...                            0.0236009 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0375867 seconds.
[PassRunner]   (validating)
[PassRunner] passes took 0.944448 seconds.
[PassRunner] (final validation)
