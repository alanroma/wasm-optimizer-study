[PassRunner] running passes...
[PassRunner]   running pass: duplicate-function-elimination... 0.0903064 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: memory-packing...                 0.000904218 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: ssa-nomerge...                    0.148716 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dce...                            0.046948 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.0609999 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.00659979 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.0245262 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: pick-load-signs...                0.0111362 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.0289252 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: code-pushing...                   0.028706 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals-nostructure...    0.118971 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0569992 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.0200526 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.0265935 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.0333953 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals...                0.0757677 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0392428 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.0101482 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.0320219 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.00992709 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0381615 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: code-folding...                   0.0111053 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.01983 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.0344616 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.00544405 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.0177158 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.0208008 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.0195924 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: rse...                            0.021428 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0345344 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dae-optimizing...                 0.113585 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inlining-optimizing...            0.0766724 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 0.0164052 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-import-elimination...   7.1386e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-globals-optimizing...    0.0037287 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-module-elements...  0.0087124 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: directize...                      5.338e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: generate-stack-ir...              0.00577419 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-stack-ir...              0.0900377 seconds.
[PassRunner]   (validating)
[PassRunner] passes took 1.409 seconds.
[PassRunner] (final validation)
