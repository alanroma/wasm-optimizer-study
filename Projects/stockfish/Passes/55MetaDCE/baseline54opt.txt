[PassRunner] running passes...
[PassRunner]   running pass: strip-dwarf...                    0.000122172 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: post-emscripten...                0.00176213 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inline-main...                    0.000143878 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: no-exit-runtime...                0.00873277 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 0.0106545 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: memory-packing...                 0.000772066 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dce...                            0.0512236 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.0669862 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.00702225 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.0266596 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: pick-load-signs...                0.0123472 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.0316822 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-added-constants...       0.00695253 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: code-pushing...                   0.0297806 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals-nostructure...    0.131584 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0605461 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.0211063 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.0286705 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.0369369 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals...                0.0808206 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0425543 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.0113036 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.0361406 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.0108966 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0415023 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.0210529 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.0362576 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.00597035 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.018993 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.0228831 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.0222176 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: rse...                            0.0238276 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0372255 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dae-optimizing...                 0.145376 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inlining-optimizing...            0.071452 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 0.00697243 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-import-elimination...   8.2369e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-globals-optimizing...    0.00395663 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-module-elements...  0.00932333 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: directize...                      5.9538e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: generate-stack-ir...              0.0059999 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-stack-ir...              0.0360062 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: strip-debug...                    0.0156565 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: strip-producers...                6.1279e-05 seconds.
[PassRunner]   (validating)
[PassRunner] passes took 1.24028 seconds.
[PassRunner] (final validation)
