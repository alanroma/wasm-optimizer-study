[PassRunner] running passes...
[PassRunner]   running pass: strip-dwarf...                    0.000122086 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: post-emscripten...                0.0016851 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inline-main...                    0.000128032 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: no-exit-runtime...                0.00877454 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 0.0107498 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: memory-packing...                 0.000829054 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dce...                            0.0509627 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.0673365 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.00696721 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.0270368 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: pick-load-signs...                0.012339 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.0315659 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-added-constants...       0.00690873 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: code-pushing...                   0.02983 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals-nostructure...    0.130665 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0615819 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.0215766 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.0285887 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.037089 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals...                0.0809635 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.042895 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.0113386 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.0360687 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.011169 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0410907 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.0208081 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.036221 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.00601142 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.0188936 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.0239768 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.0214949 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: rse...                            0.0239146 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0374803 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dae-optimizing...                 0.145878 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inlining-optimizing...            0.0719305 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 0.00688737 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-import-elimination...   8.1308e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-globals-optimizing...    0.00396567 seconds.
[PassRunner]   (validating)
[PassRunner] passes took 1.17581 seconds.
[PassRunner] (final validation)
