[PassRunner] running passes...
[PassRunner]   running pass: strip-dwarf...                    0.000119712 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: post-emscripten...                0.00187859 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inline-main...                    0.000202848 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: no-exit-runtime...                0.00883525 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 0.0105526 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: memory-packing...                 0.000755788 seconds.
[PassRunner]   (validating)
[PassRunner] passes took 0.0223448 seconds.
[PassRunner] (final validation)
