[PassRunner] running passes...
[PassRunner]   running pass: strip-dwarf...                    0.00015431 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: post-emscripten...                0.00180229 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inline-main...                    0.000131656 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: no-exit-runtime...                0.00875579 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 0.0107322 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: memory-packing...                 0.000817792 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dce...                            0.0507913 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.0675816 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.00694003 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.0266858 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: pick-load-signs...                0.0123416 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.0316766 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-added-constants...       0.00793296 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: code-pushing...                   0.0302976 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals-nostructure...    0.130991 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0611599 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.0214222 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.028482 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.0370327 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals...                0.0816065 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.042869 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.0112128 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.0358864 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.0110702 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0409371 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.0210501 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.0361631 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.006886 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.0190171 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.0231477 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.0212703 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: rse...                            0.0238647 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0377429 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dae-optimizing...                 0.145298 seconds.
[PassRunner]   (validating)
[PassRunner] passes took 1.09375 seconds.
[PassRunner] (final validation)
