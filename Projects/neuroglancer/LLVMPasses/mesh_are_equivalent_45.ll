; ModuleID = './draco/src/draco/mesh/mesh_are_equivalent.cc'
source_filename = "./draco/src/draco/mesh/mesh_are_equivalent.cc"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct._IO_FILE = type opaque
%"class.draco::MeshAreEquivalent" = type { %"class.std::__2::vector", i32 }
%"class.std::__2::vector" = type { %"class.std::__2::__vector_base" }
%"class.std::__2::__vector_base" = type { %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"class.std::__2::__compressed_pair.121" }
%"struct.draco::MeshAreEquivalent::MeshInfo" = type { %"class.draco::Mesh"*, %"class.std::__2::vector.112", %"class.draco::IndexTypeVector.120" }
%"class.std::__2::vector.112" = type { %"class.std::__2::__vector_base.113" }
%"class.std::__2::__vector_base.113" = type { %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"*, %"class.std::__2::__compressed_pair.115" }
%"class.draco::IndexType.114" = type { i32 }
%"class.std::__2::__compressed_pair.115" = type { %"struct.std::__2::__compressed_pair_elem.116" }
%"struct.std::__2::__compressed_pair_elem.116" = type { %"class.draco::IndexType.114"* }
%"class.draco::IndexTypeVector.120" = type { %"class.std::__2::vector.89" }
%"class.std::__2::vector.89" = type { %"class.std::__2::__vector_base.90" }
%"class.std::__2::__vector_base.90" = type { i32*, i32*, %"class.std::__2::__compressed_pair.91" }
%"class.std::__2::__compressed_pair.91" = type { %"struct.std::__2::__compressed_pair_elem.92" }
%"struct.std::__2::__compressed_pair_elem.92" = type { i32* }
%"class.std::__2::__compressed_pair.121" = type { %"struct.std::__2::__compressed_pair_elem.122" }
%"struct.std::__2::__compressed_pair_elem.122" = type { %"struct.draco::MeshAreEquivalent::MeshInfo"* }
%"class.draco::Mesh" = type { %"class.draco::PointCloud", %"class.std::__2::vector.96", %"class.draco::IndexTypeVector.103" }
%"class.draco::PointCloud" = type { i32 (...)**, %"class.std::__2::unique_ptr", %"class.std::__2::vector.53", [5 x %"class.std::__2::vector.89"], i32 }
%"class.std::__2::unique_ptr" = type { %"class.std::__2::__compressed_pair" }
%"class.std::__2::__compressed_pair" = type { %"struct.std::__2::__compressed_pair_elem" }
%"struct.std::__2::__compressed_pair_elem" = type { %"class.draco::GeometryMetadata"* }
%"class.draco::GeometryMetadata" = type { %"class.draco::Metadata", %"class.std::__2::vector.40" }
%"class.draco::Metadata" = type { %"class.std::__2::unordered_map", %"class.std::__2::unordered_map.17" }
%"class.std::__2::unordered_map" = type { %"class.std::__2::__hash_table" }
%"class.std::__2::__hash_table" = type { %"class.std::__2::unique_ptr.0", %"class.std::__2::__compressed_pair.7", %"class.std::__2::__compressed_pair.12", %"class.std::__2::__compressed_pair.14" }
%"class.std::__2::unique_ptr.0" = type { %"class.std::__2::__compressed_pair.1" }
%"class.std::__2::__compressed_pair.1" = type { %"struct.std::__2::__compressed_pair_elem.2", %"struct.std::__2::__compressed_pair_elem.3" }
%"struct.std::__2::__compressed_pair_elem.2" = type { %"struct.std::__2::__hash_node_base"** }
%"struct.std::__2::__hash_node_base" = type { %"struct.std::__2::__hash_node_base"* }
%"struct.std::__2::__compressed_pair_elem.3" = type { %"class.std::__2::__bucket_list_deallocator" }
%"class.std::__2::__bucket_list_deallocator" = type { %"class.std::__2::__compressed_pair.4" }
%"class.std::__2::__compressed_pair.4" = type { %"struct.std::__2::__compressed_pair_elem.5" }
%"struct.std::__2::__compressed_pair_elem.5" = type { i32 }
%"class.std::__2::__compressed_pair.7" = type { %"struct.std::__2::__compressed_pair_elem.8" }
%"struct.std::__2::__compressed_pair_elem.8" = type { %"struct.std::__2::__hash_node_base" }
%"class.std::__2::__compressed_pair.12" = type { %"struct.std::__2::__compressed_pair_elem.5" }
%"class.std::__2::__compressed_pair.14" = type { %"struct.std::__2::__compressed_pair_elem.15" }
%"struct.std::__2::__compressed_pair_elem.15" = type { float }
%"class.std::__2::unordered_map.17" = type { %"class.std::__2::__hash_table.18" }
%"class.std::__2::__hash_table.18" = type { %"class.std::__2::unique_ptr.19", %"class.std::__2::__compressed_pair.29", %"class.std::__2::__compressed_pair.34", %"class.std::__2::__compressed_pair.37" }
%"class.std::__2::unique_ptr.19" = type { %"class.std::__2::__compressed_pair.20" }
%"class.std::__2::__compressed_pair.20" = type { %"struct.std::__2::__compressed_pair_elem.21", %"struct.std::__2::__compressed_pair_elem.23" }
%"struct.std::__2::__compressed_pair_elem.21" = type { %"struct.std::__2::__hash_node_base.22"** }
%"struct.std::__2::__hash_node_base.22" = type { %"struct.std::__2::__hash_node_base.22"* }
%"struct.std::__2::__compressed_pair_elem.23" = type { %"class.std::__2::__bucket_list_deallocator.24" }
%"class.std::__2::__bucket_list_deallocator.24" = type { %"class.std::__2::__compressed_pair.25" }
%"class.std::__2::__compressed_pair.25" = type { %"struct.std::__2::__compressed_pair_elem.5" }
%"class.std::__2::__compressed_pair.29" = type { %"struct.std::__2::__compressed_pair_elem.30" }
%"struct.std::__2::__compressed_pair_elem.30" = type { %"struct.std::__2::__hash_node_base.22" }
%"class.std::__2::__compressed_pair.34" = type { %"struct.std::__2::__compressed_pair_elem.5" }
%"class.std::__2::__compressed_pair.37" = type { %"struct.std::__2::__compressed_pair_elem.15" }
%"class.std::__2::vector.40" = type { %"class.std::__2::__vector_base.41" }
%"class.std::__2::__vector_base.41" = type { %"class.std::__2::unique_ptr.42"*, %"class.std::__2::unique_ptr.42"*, %"class.std::__2::__compressed_pair.46" }
%"class.std::__2::unique_ptr.42" = type { %"class.std::__2::__compressed_pair.43" }
%"class.std::__2::__compressed_pair.43" = type { %"struct.std::__2::__compressed_pair_elem.44" }
%"struct.std::__2::__compressed_pair_elem.44" = type { %"class.draco::AttributeMetadata"* }
%"class.draco::AttributeMetadata" = type { %"class.draco::Metadata", i32 }
%"class.std::__2::__compressed_pair.46" = type { %"struct.std::__2::__compressed_pair_elem.47" }
%"struct.std::__2::__compressed_pair_elem.47" = type { %"class.std::__2::unique_ptr.42"* }
%"class.std::__2::vector.53" = type { %"class.std::__2::__vector_base.54" }
%"class.std::__2::__vector_base.54" = type { %"class.std::__2::unique_ptr.55"*, %"class.std::__2::unique_ptr.55"*, %"class.std::__2::__compressed_pair.84" }
%"class.std::__2::unique_ptr.55" = type { %"class.std::__2::__compressed_pair.56" }
%"class.std::__2::__compressed_pair.56" = type { %"struct.std::__2::__compressed_pair_elem.57" }
%"struct.std::__2::__compressed_pair_elem.57" = type { %"class.draco::PointAttribute"* }
%"class.draco::PointAttribute" = type <{ %"class.draco::GeometryAttribute", %"class.std::__2::unique_ptr.65", %"class.draco::IndexTypeVector", i32, i8, [3 x i8], %"class.std::__2::unique_ptr.77", [4 x i8] }>
%"class.draco::GeometryAttribute" = type { %"class.draco::DataBuffer"*, %"struct.draco::DataBufferDescriptor", i8, i32, i8, i64, i64, i32, i32 }
%"class.draco::DataBuffer" = type { %"class.std::__2::vector.58", %"struct.draco::DataBufferDescriptor" }
%"class.std::__2::vector.58" = type { %"class.std::__2::__vector_base.59" }
%"class.std::__2::__vector_base.59" = type { i8*, i8*, %"class.std::__2::__compressed_pair.60" }
%"class.std::__2::__compressed_pair.60" = type { %"struct.std::__2::__compressed_pair_elem.61" }
%"struct.std::__2::__compressed_pair_elem.61" = type { i8* }
%"struct.draco::DataBufferDescriptor" = type { i64, i64 }
%"class.std::__2::unique_ptr.65" = type { %"class.std::__2::__compressed_pair.66" }
%"class.std::__2::__compressed_pair.66" = type { %"struct.std::__2::__compressed_pair_elem.67" }
%"struct.std::__2::__compressed_pair_elem.67" = type { %"class.draco::DataBuffer"* }
%"class.draco::IndexTypeVector" = type { %"class.std::__2::vector.70" }
%"class.std::__2::vector.70" = type { %"class.std::__2::__vector_base.71" }
%"class.std::__2::__vector_base.71" = type { %"class.draco::IndexType"*, %"class.draco::IndexType"*, %"class.std::__2::__compressed_pair.72" }
%"class.draco::IndexType" = type { i32 }
%"class.std::__2::__compressed_pair.72" = type { %"struct.std::__2::__compressed_pair_elem.73" }
%"struct.std::__2::__compressed_pair_elem.73" = type { %"class.draco::IndexType"* }
%"class.std::__2::unique_ptr.77" = type { %"class.std::__2::__compressed_pair.78" }
%"class.std::__2::__compressed_pair.78" = type { %"struct.std::__2::__compressed_pair_elem.79" }
%"struct.std::__2::__compressed_pair_elem.79" = type { %"class.draco::AttributeTransformData"* }
%"class.draco::AttributeTransformData" = type { i32, %"class.draco::DataBuffer" }
%"class.std::__2::__compressed_pair.84" = type { %"struct.std::__2::__compressed_pair_elem.85" }
%"struct.std::__2::__compressed_pair_elem.85" = type { %"class.std::__2::unique_ptr.55"* }
%"class.std::__2::vector.96" = type { %"class.std::__2::__vector_base.97" }
%"class.std::__2::__vector_base.97" = type { %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"*, %"class.std::__2::__compressed_pair.98" }
%"struct.draco::Mesh::AttributeData" = type { i32 }
%"class.std::__2::__compressed_pair.98" = type { %"struct.std::__2::__compressed_pair_elem.99" }
%"struct.std::__2::__compressed_pair_elem.99" = type { %"struct.draco::Mesh::AttributeData"* }
%"class.draco::IndexTypeVector.103" = type { %"class.std::__2::vector.104" }
%"class.std::__2::vector.104" = type { %"class.std::__2::__vector_base.105" }
%"class.std::__2::__vector_base.105" = type { %"struct.std::__2::array"*, %"struct.std::__2::array"*, %"class.std::__2::__compressed_pair.107" }
%"struct.std::__2::array" = type { [3 x %"class.draco::IndexType.106"] }
%"class.draco::IndexType.106" = type { i32 }
%"class.std::__2::__compressed_pair.107" = type { %"struct.std::__2::__compressed_pair_elem.108" }
%"struct.std::__2::__compressed_pair_elem.108" = type { %"struct.std::__2::array"* }
%"struct.std::__2::array.126" = type { [3 x float] }
%"class.draco::VectorD" = type { %"struct.std::__2::array.126" }
%"struct.draco::MeshAreEquivalent::FaceIndexLess" = type { %"struct.draco::MeshAreEquivalent::MeshInfo"* }
%"class.std::__2::__wrap_iter" = type { %"class.draco::IndexType.114"* }
%"class.std::__2::allocator.118" = type { i8 }
%"struct.std::__2::__split_buffer.136" = type { %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"*, %"class.std::__2::__compressed_pair.137" }
%"class.std::__2::__compressed_pair.137" = type { %"struct.std::__2::__compressed_pair_elem.116", %"struct.std::__2::__compressed_pair_elem.138" }
%"struct.std::__2::__compressed_pair_elem.138" = type { %"class.std::__2::allocator.118"* }
%"struct.std::__2::__less" = type { i8 }
%"class.std::__2::unique_ptr.127" = type { %"class.std::__2::__compressed_pair.128" }
%"class.std::__2::__compressed_pair.128" = type { %"struct.std::__2::__compressed_pair_elem.61" }
%"struct.std::__2::__default_init_tag" = type { i8 }
%"class.std::__2::__vector_base_common" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.117" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.93" = type { i8 }
%"class.std::__2::allocator.94" = type { i8 }
%"struct.std::__2::integral_constant" = type { i8 }
%"struct.std::__2::__has_destroy" = type { i8 }
%"struct.std::__2::__has_destroy.131" = type { i8 }
%"struct.std::__2::__split_buffer" = type { i32*, i32*, i32*, %"class.std::__2::__compressed_pair.132" }
%"class.std::__2::__compressed_pair.132" = type { %"struct.std::__2::__compressed_pair_elem.92", %"struct.std::__2::__compressed_pair_elem.133" }
%"struct.std::__2::__compressed_pair_elem.133" = type { %"class.std::__2::allocator.94"* }
%"class.std::__2::__split_buffer_common" = type { i8 }
%"struct.std::__2::integral_constant.134" = type { i8 }
%"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction" = type { %"class.std::__2::vector.89"*, i32*, i32* }
%"struct.std::__2::__has_construct" = type { i8 }
%"struct.std::__2::__less.135" = type { i8 }
%"struct.std::__2::__has_max_size" = type { i8 }
%"struct.std::__2::vector<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>>>::_ConstructTransaction" = type { %"class.std::__2::vector.112"*, %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"* }
%"struct.std::__2::__has_construct.139" = type { i8 }
%"struct.std::__2::__has_max_size.140" = type { i8 }
%"class.std::__2::allocator.124" = type { i8 }
%"struct.std::__2::__has_destroy.141" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.123" = type { i8 }
%"struct.std::__2::vector<draco::MeshAreEquivalent::MeshInfo, std::__2::allocator<draco::MeshAreEquivalent::MeshInfo>>::_ConstructTransaction" = type { %"class.std::__2::vector"*, %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"* }
%"struct.std::__2::__split_buffer.143" = type { %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"class.std::__2::__compressed_pair.144" }
%"class.std::__2::__compressed_pair.144" = type { %"struct.std::__2::__compressed_pair_elem.122", %"struct.std::__2::__compressed_pair_elem.145" }
%"struct.std::__2::__compressed_pair_elem.145" = type { %"class.std::__2::allocator.124"* }
%"struct.std::__2::__has_construct.142" = type { i8 }
%"struct.std::__2::__has_max_size.146" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.129" = type { i8 }
%"struct.std::__2::default_delete.130" = type { i8 }

$_ZNK5draco9IndexTypeIjNS_19FaceIndex_tag_type_EE5valueEv = comdat any

$_ZNK5draco4Mesh4faceENS_9IndexTypeIjNS_19FaceIndex_tag_type_EEE = comdat any

$_ZNKSt3__25arrayIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELm3EEixEm = comdat any

$_ZNK5draco14PointAttribute12mapped_indexENS_9IndexTypeIjNS_20PointIndex_tag_type_EEE = comdat any

$_ZNK5draco17GeometryAttribute8GetValueIfLi3EEENSt3__25arrayIT_XT0_EEENS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEE = comdat any

$_ZNKSt3__25arrayIfLm3EEixEm = comdat any

$_ZN5draco7VectorDIfLi3EEC2ERKfS3_S3_ = comdat any

$_ZNSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEEixEm = comdat any

$_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEEiE7reserveEm = comdat any

$_ZN5draco9IndexTypeIjNS_19FaceIndex_tag_type_EEC2Ej = comdat any

$_ZNK5draco9IndexTypeIjNS_19FaceIndex_tag_type_EEltERKj = comdat any

$_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEEiE9push_backEOi = comdat any

$_ZN5draco9IndexTypeIjNS_19FaceIndex_tag_type_EEppEv = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE7reserveEm = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE9push_backERKS4_ = comdat any

$_ZN5draco17MeshAreEquivalent13FaceIndexLessC2ERKNS0_8MeshInfoE = comdat any

$_ZNSt3__24sortIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS1_17MeshAreEquivalent13FaceIndexLessEEEvNS_11__wrap_iterIPT_EESA_T0_ = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE5beginEv = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE3endEv = comdat any

$_ZN5draco7VectorDIfLi3EEC2Ev = comdat any

$_ZNSt3__211min_elementIPN5draco7VectorDIfLi3EEEEET_S5_S5_ = comdat any

$_ZNSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE5clearEv = comdat any

$_ZNK5draco4Mesh9num_facesEv = comdat any

$_ZNSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE9push_backEOS3_ = comdat any

$_ZN5draco17MeshAreEquivalent8MeshInfoC2ERKNS_4MeshE = comdat any

$_ZN5draco17MeshAreEquivalent8MeshInfoD2Ev = comdat any

$_ZNK5draco10PointCloud14num_attributesEv = comdat any

$_ZNK5draco17GeometryAttribute9data_typeEv = comdat any

$_ZNK5draco17GeometryAttribute14num_componentsEv = comdat any

$_ZNK5draco17GeometryAttribute10normalizedEv = comdat any

$_ZNK5draco17GeometryAttribute11byte_strideEv = comdat any

$_ZNSt3__210unique_ptrIA_hNS_14default_deleteIS1_EEEC2IPhLb1EvvEET_ = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEEixEm = comdat any

$_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEEiEixERKS3_ = comdat any

$_ZNK5draco17GeometryAttribute8GetValueENS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEEPv = comdat any

$_ZNKSt3__210unique_ptrIA_hNS_14default_deleteIS1_EEE3getEv = comdat any

$_ZNSt3__210unique_ptrIA_hNS_14default_deleteIS1_EEED2Ev = comdat any

$_ZNK5draco9IndexTypeIjNS_19FaceIndex_tag_type_EEeqERKS2_ = comdat any

$_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEEiEixERKS3_ = comdat any

$_ZNK5draco7VectorDIfLi3EEltERKS1_ = comdat any

$_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEENSt3__25arrayINS1_IjNS_20PointIndex_tag_type_EEELm3EEEEixERKS3_ = comdat any

$_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEEixEm = comdat any

$_ZNK5draco9IndexTypeIjNS_20PointIndex_tag_type_EE5valueEv = comdat any

$_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEC2Ej = comdat any

$_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEEEixERKS3_ = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEEixEm = comdat any

$_ZNSt3__211min_elementIPN5draco7VectorDIfLi3EEENS_6__lessIS3_S3_EEEET_S7_S7_T0_ = comdat any

$_ZNKSt3__26__lessIN5draco7VectorDIfLi3EEES3_EclERKS3_S6_ = comdat any

$_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEENSt3__25arrayINS1_IjNS_20PointIndex_tag_type_EEELm3EEEE4sizeEv = comdat any

$_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4sizeEv = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEEC2Ev = comdat any

$_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEEiEC2Ev = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEEC2Ev = comdat any

$_ZNSt3__220__vector_base_commonILb1EEC2Ev = comdat any

$_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEEC2IDnNS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEELi0ELb0EEC2IDnvEEOT_ = comdat any

$_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEC2Ev = comdat any

$_ZNSt3__26vectorIiNS_9allocatorIiEEEC2Ev = comdat any

$_ZNSt3__213__vector_baseIiNS_9allocatorIiEEEC2Ev = comdat any

$_ZNSt3__217__compressed_pairIPiNS_9allocatorIiEEEC2IDnNS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__222__compressed_pair_elemIPiLi0ELb0EEC2IDnvEEOT_ = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIiEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__29allocatorIiEC2Ev = comdat any

$_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEEiED2Ev = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEED2Ev = comdat any

$_ZNSt3__26vectorIiNS_9allocatorIiEEED2Ev = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEE17__annotate_deleteEv = comdat any

$_ZNSt3__213__vector_baseIiNS_9allocatorIiEEED2Ev = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEE31__annotate_contiguous_containerEPKvS5_S5_S5_ = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEE8capacityEv = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEE4sizeEv = comdat any

$_ZNSt3__212__to_addressIiEEPT_S2_ = comdat any

$_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPiNS_9allocatorIiEEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPiLi0ELb0EE5__getEv = comdat any

$_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE5clearEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIiEEE10deallocateERS2_Pim = comdat any

$_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE7__allocEv = comdat any

$_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE17__destruct_at_endEPi = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIiEEE7destroyIiEEvRS2_PT_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIiEEE9__destroyIiEEvNS_17integral_constantIbLb1EEERS2_PT_ = comdat any

$_ZNSt3__29allocatorIiE7destroyEPi = comdat any

$_ZNSt3__29allocatorIiE10deallocateEPim = comdat any

$_ZNSt3__219__libcpp_deallocateEPvmm = comdat any

$_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm = comdat any

$_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm = comdat any

$_ZNSt3__217_DeallocateCaller9__do_callEPv = comdat any

$_ZNSt3__217__compressed_pairIPiNS_9allocatorIiEEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIiEELi1ELb1EE5__getEv = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_deleteEv = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEED2Ev = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_ = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv = comdat any

$_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEPT_S6_ = comdat any

$_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE5clearEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEEE10deallocateERS6_PS5_m = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEEE7destroyIS5_EEvRS6_PT_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEEE9__destroyIS5_EEvNS_17integral_constantIbLb1EEERS6_PT_ = comdat any

$_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEE7destroyEPS4_ = comdat any

$_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEE10deallocateEPS4_m = comdat any

$_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEELi1ELb1EE5__getEv = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv = comdat any

$_ZNK5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EE5valueEv = comdat any

$_ZNK5draco10DataBuffer4ReadExPvm = comdat any

$_ZNK5draco10DataBuffer4dataEv = comdat any

$_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv = comdat any

$_ZNSt3__212__to_addressIhEEPT_S2_ = comdat any

$_ZNSt3__25arrayIfLm3EEixEm = comdat any

$_ZNSt3__26vectorIiNS_9allocatorIiEEE7reserveEm = comdat any

$_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEEC2EmmS3_ = comdat any

$_ZNSt3__26vectorIiNS_9allocatorIiEEE26__swap_out_circular_bufferERNS_14__split_bufferIiRS2_EE = comdat any

$_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEED2Ev = comdat any

$_ZNSt3__217__compressed_pairIPiRNS_9allocatorIiEEEC2IDnS4_EEOT_OT0_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIiEEE8allocateERS2_m = comdat any

$_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE7__allocEv = comdat any

$_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE9__end_capEv = comdat any

$_ZNSt3__27forwardIRNS_9allocatorIiEEEEOT_RNS_16remove_referenceIS4_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIiEELi1ELb0EEC2IS3_vEEOT_ = comdat any

$_ZNSt3__29allocatorIiE8allocateEmPKv = comdat any

$_ZNKSt3__29allocatorIiE8max_sizeEv = comdat any

$_ZNSt3__220__throw_length_errorEPKc = comdat any

$_ZNSt3__217__libcpp_allocateEmm = comdat any

$_ZNSt3__217__compressed_pairIPiRNS_9allocatorIiEEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIiEELi1ELb0EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPiRNS_9allocatorIiEEE5firstEv = comdat any

$_ZNSt3__222__compressed_pair_elemIPiLi0ELb0EE5__getEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIiEEE46__construct_backward_with_exception_guaranteesIiEENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS2_PT_S8_EE5valuesr31is_trivially_move_constructibleIS8_EE5valueEvE4typeERS2_S9_S9_RS9_ = comdat any

$_ZNSt3__24swapIPiEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_ = comdat any

$_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE9__end_capEv = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEE14__annotate_newEm = comdat any

$_ZNSt3__26vectorIiNS_9allocatorIiEEE26__invalidate_all_iteratorsEv = comdat any

$_ZNSt3__24moveIRPiEEONS_16remove_referenceIT_E4typeEOS4_ = comdat any

$_ZNSt3__217__compressed_pairIPiNS_9allocatorIiEEE5firstEv = comdat any

$_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE5clearEv = comdat any

$_ZNKSt3__214__split_bufferIiRNS_9allocatorIiEEE8capacityEv = comdat any

$_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE17__destruct_at_endEPi = comdat any

$_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE17__destruct_at_endEPiNS_17integral_constantIbLb0EEE = comdat any

$_ZNKSt3__214__split_bufferIiRNS_9allocatorIiEEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPiRNS_9allocatorIiEEE5firstEv = comdat any

$_ZNSt3__26vectorIiNS_9allocatorIiEEE9push_backEOi = comdat any

$_ZNSt3__24moveIRiEEONS_16remove_referenceIT_E4typeEOS3_ = comdat any

$_ZNSt3__26vectorIiNS_9allocatorIiEEE22__construct_one_at_endIJiEEEvDpOT_ = comdat any

$_ZNSt3__26vectorIiNS_9allocatorIiEEE21__push_back_slow_pathIiEEvOT_ = comdat any

$_ZNSt3__26vectorIiNS_9allocatorIiEEE21_ConstructTransactionC2ERS3_m = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIiEEE9constructIiJiEEEvRS2_PT_DpOT0_ = comdat any

$_ZNSt3__27forwardIiEEOT_RNS_16remove_referenceIS1_E4typeE = comdat any

$_ZNSt3__26vectorIiNS_9allocatorIiEEE21_ConstructTransactionD2Ev = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIiEEE11__constructIiJiEEEvNS_17integral_constantIbLb1EEERS2_PT_DpOT0_ = comdat any

$_ZNSt3__29allocatorIiE9constructIiJiEEEvPT_DpOT0_ = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEE11__recommendEm = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEE8max_sizeEv = comdat any

$_ZNSt3__23maxImEERKT_S3_S3_ = comdat any

$_ZNSt3__23minImEERKT_S3_S3_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIiEEE8max_sizeERKS2_ = comdat any

$_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE7__allocEv = comdat any

$_ZNSt3__214numeric_limitsIlE3maxEv = comdat any

$_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_ = comdat any

$_ZNKSt3__26__lessImmEclERKmS3_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIiEEE10__max_sizeENS_17integral_constantIbLb1EEERKS2_ = comdat any

$_ZNKSt3__217__compressed_pairIPiNS_9allocatorIiEEE6secondEv = comdat any

$_ZNKSt3__222__compressed_pair_elemINS_9allocatorIiEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv = comdat any

$_ZNSt3__23maxImNS_6__lessImmEEEERKT_S5_S5_T0_ = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEEC2EmmS7_ = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS4_RS6_EE = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEED2Ev = comdat any

$_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEEC2IDnS8_EEOT_OT0_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEEE8allocateERS6_m = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEE7__allocEv = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEE9__end_capEv = comdat any

$_ZNSt3__27forwardIRNS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEEEEOT_RNS_16remove_referenceIS8_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEELi1ELb0EEC2IS7_vEEOT_ = comdat any

$_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEE8allocateEmPKv = comdat any

$_ZNKSt3__29allocatorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEE8max_sizeEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEELi1ELb0EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEE5firstEv = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEEE46__construct_backward_with_exception_guaranteesIS5_EENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS6_PT_SC_EE5valuesr31is_trivially_move_constructibleISC_EE5valueEvE4typeERS6_SD_SD_RSD_ = comdat any

$_ZNSt3__24swapIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS7_EE5valueEvE4typeERS7_SA_ = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE14__annotate_newEm = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE26__invalidate_all_iteratorsEv = comdat any

$_ZNSt3__24moveIRPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEONS_16remove_referenceIT_E4typeEOS8_ = comdat any

$_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEE5clearEv = comdat any

$_ZNKSt3__214__split_bufferIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEE8capacityEv = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEE17__destruct_at_endEPS4_ = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEE17__destruct_at_endEPS4_NS_17integral_constantIbLb0EEE = comdat any

$_ZNKSt3__214__split_bufferIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEE5firstEv = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE22__construct_one_at_endIJRKS4_EEEvDpOT_ = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE21__push_back_slow_pathIRKS4_EEvOT_ = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionC2ERS7_m = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEEE9constructIS5_JRKS5_EEEvRS6_PT_DpOT0_ = comdat any

$_ZNSt3__27forwardIRKN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEOT_RNS_16remove_referenceIS7_E4typeE = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionD2Ev = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEEE11__constructIS5_JRKS5_EEEvNS_17integral_constantIbLb1EEERS6_PT_DpOT0_ = comdat any

$_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEE9constructIS4_JRKS4_EEEvPT_DpOT0_ = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE11__recommendEm = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE8max_sizeEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEEE8max_sizeERKS6_ = comdat any

$_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS6_ = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv = comdat any

$_ZNKSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE11__make_iterEPS4_ = comdat any

$_ZNSt3__211__wrap_iterIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEC2ES5_ = comdat any

$_ZNSt3__24sortIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS1_17MeshAreEquivalent13FaceIndexLessEEEvT_S9_T0_ = comdat any

$_ZNKSt3__211__wrap_iterIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEE4baseEv = comdat any

$_ZNSt3__26__sortIRN5draco17MeshAreEquivalent13FaceIndexLessEPNS1_9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEvT0_S9_T_ = comdat any

$_ZNSt3__24swapIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS6_EE5valueEvE4typeERS6_S9_ = comdat any

$_ZNSt3__27__sort3IRN5draco17MeshAreEquivalent13FaceIndexLessEPNS1_9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEjT0_S9_S9_T_ = comdat any

$_ZNSt3__27__sort4IRN5draco17MeshAreEquivalent13FaceIndexLessEPNS1_9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEjT0_S9_S9_S9_T_ = comdat any

$_ZNSt3__27__sort5IRN5draco17MeshAreEquivalent13FaceIndexLessEPNS1_9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEjT0_S9_S9_S9_S9_T_ = comdat any

$_ZNSt3__218__insertion_sort_3IRN5draco17MeshAreEquivalent13FaceIndexLessEPNS1_9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEvT0_S9_T_ = comdat any

$_ZNSt3__227__insertion_sort_incompleteIRN5draco17MeshAreEquivalent13FaceIndexLessEPNS1_9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEbT0_S9_T_ = comdat any

$_ZNSt3__24moveIRN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEONS_16remove_referenceIT_E4typeEOS7_ = comdat any

$_ZN5draco9IndexTypeIjNS_19FaceIndex_tag_type_EEaSERKS2_ = comdat any

$_ZN5draco7VectorDIfLi3EEixEi = comdat any

$_ZNKSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE4sizeEv = comdat any

$_ZNSt3__213__vector_baseIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE5clearEv = comdat any

$_ZNKSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE17__annotate_shrinkEm = comdat any

$_ZNSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE26__invalidate_all_iteratorsEv = comdat any

$_ZNSt3__213__vector_baseIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE17__destruct_at_endEPS3_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco17MeshAreEquivalent8MeshInfoEEEE7destroyIS4_EEvRS5_PT_ = comdat any

$_ZNSt3__213__vector_baseIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE7__allocEv = comdat any

$_ZNSt3__212__to_addressIN5draco17MeshAreEquivalent8MeshInfoEEEPT_S5_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco17MeshAreEquivalent8MeshInfoEEEE9__destroyIS4_EEvNS_17integral_constantIbLb1EEERS5_PT_ = comdat any

$_ZNSt3__29allocatorIN5draco17MeshAreEquivalent8MeshInfoEE7destroyEPS3_ = comdat any

$_ZNSt3__217__compressed_pairIPN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco17MeshAreEquivalent8MeshInfoEEELi1ELb1EE5__getEv = comdat any

$_ZNKSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE31__annotate_contiguous_containerEPKvS8_S8_S8_ = comdat any

$_ZNKSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE4dataEv = comdat any

$_ZNKSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPN5draco17MeshAreEquivalent8MeshInfoELi0ELb0EE5__getEv = comdat any

$_ZNSt3__213__vector_baseIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE9__end_capEv = comdat any

$_ZNSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE22__construct_one_at_endIJS3_EEEvDpOT_ = comdat any

$_ZNSt3__24moveIRN5draco17MeshAreEquivalent8MeshInfoEEEONS_16remove_referenceIT_E4typeEOS6_ = comdat any

$_ZNSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE21__push_back_slow_pathIS3_EEvOT_ = comdat any

$_ZNSt3__217__compressed_pairIPN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE5firstEv = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco17MeshAreEquivalent8MeshInfoELi0ELb0EE5__getEv = comdat any

$_ZNSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE21_ConstructTransactionC2ERS6_m = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco17MeshAreEquivalent8MeshInfoEEEE9constructIS4_JS4_EEEvRS5_PT_DpOT0_ = comdat any

$_ZNSt3__27forwardIN5draco17MeshAreEquivalent8MeshInfoEEEOT_RNS_16remove_referenceIS4_E4typeE = comdat any

$_ZNSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE21_ConstructTransactionD2Ev = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco17MeshAreEquivalent8MeshInfoEEEE11__constructIS4_JS4_EEEvNS_17integral_constantIbLb1EEERS5_PT_DpOT0_ = comdat any

$_ZNSt3__29allocatorIN5draco17MeshAreEquivalent8MeshInfoEE9constructIS3_JS3_EEEvPT_DpOT0_ = comdat any

$_ZN5draco17MeshAreEquivalent8MeshInfoC2EOS1_ = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEEC2EOS7_ = comdat any

$_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEEiEC2EOS4_ = comdat any

$_ZNSt3__24moveIRNS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEEEEONS_16remove_referenceIT_E4typeEOS9_ = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEEC2EOS6_ = comdat any

$_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEEC2IDnS7_EEOT_OT0_ = comdat any

$_ZNSt3__27forwardINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEEEEOT_RNS_16remove_referenceIS7_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEELi1ELb1EEC2IS6_vEEOT_ = comdat any

$_ZNSt3__26vectorIiNS_9allocatorIiEEEC2EOS3_ = comdat any

$_ZNSt3__24moveIRNS_9allocatorIiEEEEONS_16remove_referenceIT_E4typeEOS5_ = comdat any

$_ZNSt3__213__vector_baseIiNS_9allocatorIiEEEC2EOS2_ = comdat any

$_ZNSt3__217__compressed_pairIPiNS_9allocatorIiEEEC2IDnS3_EEOT_OT0_ = comdat any

$_ZNSt3__27forwardINS_9allocatorIiEEEEOT_RNS_16remove_referenceIS3_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIiEELi1ELb1EEC2IS2_vEEOT_ = comdat any

$_ZNKSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE11__recommendEm = comdat any

$_ZNSt3__214__split_bufferIN5draco17MeshAreEquivalent8MeshInfoERNS_9allocatorIS3_EEEC2EmmS6_ = comdat any

$_ZNSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS3_RS5_EE = comdat any

$_ZNSt3__214__split_bufferIN5draco17MeshAreEquivalent8MeshInfoERNS_9allocatorIS3_EEED2Ev = comdat any

$_ZNKSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE8max_sizeEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco17MeshAreEquivalent8MeshInfoEEEE8max_sizeERKS5_ = comdat any

$_ZNKSt3__213__vector_baseIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE7__allocEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco17MeshAreEquivalent8MeshInfoEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS5_ = comdat any

$_ZNKSt3__29allocatorIN5draco17MeshAreEquivalent8MeshInfoEE8max_sizeEv = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE6secondEv = comdat any

$_ZNKSt3__222__compressed_pair_elemINS_9allocatorIN5draco17MeshAreEquivalent8MeshInfoEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco17MeshAreEquivalent8MeshInfoERNS_9allocatorIS3_EEEC2IDnS7_EEOT_OT0_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco17MeshAreEquivalent8MeshInfoEEEE8allocateERS5_m = comdat any

$_ZNSt3__214__split_bufferIN5draco17MeshAreEquivalent8MeshInfoERNS_9allocatorIS3_EEE7__allocEv = comdat any

$_ZNSt3__214__split_bufferIN5draco17MeshAreEquivalent8MeshInfoERNS_9allocatorIS3_EEE9__end_capEv = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco17MeshAreEquivalent8MeshInfoELi0ELb0EEC2IDnvEEOT_ = comdat any

$_ZNSt3__27forwardIRNS_9allocatorIN5draco17MeshAreEquivalent8MeshInfoEEEEEOT_RNS_16remove_referenceIS7_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIN5draco17MeshAreEquivalent8MeshInfoEEELi1ELb0EEC2IS6_vEEOT_ = comdat any

$_ZNSt3__29allocatorIN5draco17MeshAreEquivalent8MeshInfoEE8allocateEmPKv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco17MeshAreEquivalent8MeshInfoERNS_9allocatorIS3_EEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIN5draco17MeshAreEquivalent8MeshInfoEEELi1ELb0EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco17MeshAreEquivalent8MeshInfoERNS_9allocatorIS3_EEE5firstEv = comdat any

$_ZNKSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE17__annotate_deleteEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco17MeshAreEquivalent8MeshInfoEEEE46__construct_backward_with_exception_guaranteesIPS4_EEvRS5_T_SA_RSA_ = comdat any

$_ZNSt3__24swapIPN5draco17MeshAreEquivalent8MeshInfoEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS6_EE5valueEvE4typeERS6_S9_ = comdat any

$_ZNKSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE14__annotate_newEm = comdat any

$_ZNSt3__24moveIRPN5draco17MeshAreEquivalent8MeshInfoEEEONS_16remove_referenceIT_E4typeEOS7_ = comdat any

$_ZNSt3__214__split_bufferIN5draco17MeshAreEquivalent8MeshInfoERNS_9allocatorIS3_EEE5clearEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco17MeshAreEquivalent8MeshInfoEEEE10deallocateERS5_PS4_m = comdat any

$_ZNKSt3__214__split_bufferIN5draco17MeshAreEquivalent8MeshInfoERNS_9allocatorIS3_EEE8capacityEv = comdat any

$_ZNSt3__214__split_bufferIN5draco17MeshAreEquivalent8MeshInfoERNS_9allocatorIS3_EEE17__destruct_at_endEPS3_ = comdat any

$_ZNSt3__214__split_bufferIN5draco17MeshAreEquivalent8MeshInfoERNS_9allocatorIS3_EEE17__destruct_at_endEPS3_NS_17integral_constantIbLb0EEE = comdat any

$_ZNSt3__29allocatorIN5draco17MeshAreEquivalent8MeshInfoEE10deallocateEPS3_m = comdat any

$_ZNKSt3__214__split_bufferIN5draco17MeshAreEquivalent8MeshInfoERNS_9allocatorIS3_EEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco17MeshAreEquivalent8MeshInfoERNS_9allocatorIS3_EEE5firstEv = comdat any

$_ZNSt3__217__compressed_pairIPhNS_14default_deleteIA_hEEEC2IRS1_NS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__27forwardIRPhEEOT_RNS_16remove_referenceIS3_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPhLi0ELb0EEC2IRS1_vEEOT_ = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIA_hEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__210unique_ptrIA_hNS_14default_deleteIS1_EEE5resetEDn = comdat any

$_ZNSt3__217__compressed_pairIPhNS_14default_deleteIA_hEEE5firstEv = comdat any

$_ZNSt3__217__compressed_pairIPhNS_14default_deleteIA_hEEE6secondEv = comdat any

$_ZNKSt3__214default_deleteIA_hEclIhEENS2_20_EnableIfConvertibleIT_E4typeEPS5_ = comdat any

$_ZNSt3__222__compressed_pair_elemIPhLi0ELb0EE5__getEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIA_hEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__26vectorIiNS_9allocatorIiEEEixEm = comdat any

$_ZNKSt3__217__compressed_pairIPhNS_14default_deleteIA_hEEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPhLi0ELb0EE5__getEv = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEEixEm = comdat any

@stderr = external constant %struct._IO_FILE*, align 4
@.str = private unnamed_addr constant [31 x i8] c"Printing position for (%i,%i)\0A\00", align 1
@.str.1 = private unnamed_addr constant [21 x i8] c"Position (%f,%f,%f)\0A\00", align 1
@.str.2 = private unnamed_addr constant [68 x i8] c"allocator<T>::allocate(size_t n) 'n' exceeds maximum supported size\00", align 1

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN5draco17MeshAreEquivalent13PrintPositionERKNS_4MeshENS_9IndexTypeIjNS_19FaceIndex_tag_type_EEEi(%"class.draco::MeshAreEquivalent"* %this, %"class.draco::Mesh"* nonnull align 4 dereferenceable(108) %mesh, i32 %f.coerce, i32 %c) #0 {
entry:
  %f = alloca %"class.draco::IndexType.114", align 4
  %this.addr = alloca %"class.draco::MeshAreEquivalent"*, align 4
  %mesh.addr = alloca %"class.draco::Mesh"*, align 4
  %c.addr = alloca i32, align 4
  %pos_att = alloca %"class.draco::PointAttribute"*, align 4
  %ver_index = alloca %"class.draco::IndexType.106", align 4
  %agg.tmp = alloca %"class.draco::IndexType.114", align 4
  %pos_index = alloca %"class.draco::IndexType", align 4
  %agg.tmp7 = alloca %"class.draco::IndexType.106", align 4
  %pos = alloca %"struct.std::__2::array.126", align 4
  %agg.tmp11 = alloca %"class.draco::IndexType", align 4
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %f, i32 0, i32 0
  store i32 %f.coerce, i32* %coerce.dive, align 4
  store %"class.draco::MeshAreEquivalent"* %this, %"class.draco::MeshAreEquivalent"** %this.addr, align 4
  store %"class.draco::Mesh"* %mesh, %"class.draco::Mesh"** %mesh.addr, align 4
  store i32 %c, i32* %c.addr, align 4
  %this1 = load %"class.draco::MeshAreEquivalent"*, %"class.draco::MeshAreEquivalent"** %this.addr, align 4
  %0 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4
  %call = call i32 @_ZNK5draco9IndexTypeIjNS_19FaceIndex_tag_type_EE5valueEv(%"class.draco::IndexType.114"* %f)
  %1 = load i32, i32* %c.addr, align 4
  %call2 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %0, i8* getelementptr inbounds ([31 x i8], [31 x i8]* @.str, i32 0, i32 0), i32 %call, i32 %1)
  %2 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %mesh.addr, align 4
  %3 = bitcast %"class.draco::Mesh"* %2 to %"class.draco::PointCloud"*
  %call3 = call %"class.draco::PointAttribute"* @_ZNK5draco10PointCloud17GetNamedAttributeENS_17GeometryAttribute4TypeE(%"class.draco::PointCloud"* %3, i32 0)
  store %"class.draco::PointAttribute"* %call3, %"class.draco::PointAttribute"** %pos_att, align 4
  %4 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %mesh.addr, align 4
  %5 = bitcast %"class.draco::IndexType.114"* %agg.tmp to i8*
  %6 = bitcast %"class.draco::IndexType.114"* %f to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %5, i8* align 4 %6, i32 4, i1 false)
  %coerce.dive4 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %agg.tmp, i32 0, i32 0
  %7 = load i32, i32* %coerce.dive4, align 4
  %call5 = call nonnull align 4 dereferenceable(12) %"struct.std::__2::array"* @_ZNK5draco4Mesh4faceENS_9IndexTypeIjNS_19FaceIndex_tag_type_EEE(%"class.draco::Mesh"* %4, i32 %7)
  %8 = load i32, i32* %c.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.106"* @_ZNKSt3__25arrayIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELm3EEixEm(%"struct.std::__2::array"* %call5, i32 %8) #7
  %9 = bitcast %"class.draco::IndexType.106"* %ver_index to i8*
  %10 = bitcast %"class.draco::IndexType.106"* %call6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 4, i1 false)
  %11 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %pos_att, align 4
  %12 = bitcast %"class.draco::IndexType.106"* %agg.tmp7 to i8*
  %13 = bitcast %"class.draco::IndexType.106"* %ver_index to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %12, i8* align 4 %13, i32 4, i1 false)
  %coerce.dive8 = getelementptr inbounds %"class.draco::IndexType.106", %"class.draco::IndexType.106"* %agg.tmp7, i32 0, i32 0
  %14 = load i32, i32* %coerce.dive8, align 4
  %call9 = call i32 @_ZNK5draco14PointAttribute12mapped_indexENS_9IndexTypeIjNS_20PointIndex_tag_type_EEE(%"class.draco::PointAttribute"* %11, i32 %14)
  %coerce.dive10 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %pos_index, i32 0, i32 0
  store i32 %call9, i32* %coerce.dive10, align 4
  %15 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %pos_att, align 4
  %16 = bitcast %"class.draco::PointAttribute"* %15 to %"class.draco::GeometryAttribute"*
  %17 = bitcast %"class.draco::IndexType"* %agg.tmp11 to i8*
  %18 = bitcast %"class.draco::IndexType"* %pos_index to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %17, i8* align 4 %18, i32 4, i1 false)
  %coerce.dive12 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %agg.tmp11, i32 0, i32 0
  %19 = load i32, i32* %coerce.dive12, align 4
  call void @_ZNK5draco17GeometryAttribute8GetValueIfLi3EEENSt3__25arrayIT_XT0_EEENS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEE(%"struct.std::__2::array.126"* sret align 4 %pos, %"class.draco::GeometryAttribute"* %16, i32 %19)
  %20 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 4
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNKSt3__25arrayIfLm3EEixEm(%"struct.std::__2::array.126"* %pos, i32 0) #7
  %21 = load float, float* %call13, align 4
  %conv = fpext float %21 to double
  %call14 = call nonnull align 4 dereferenceable(4) float* @_ZNKSt3__25arrayIfLm3EEixEm(%"struct.std::__2::array.126"* %pos, i32 1) #7
  %22 = load float, float* %call14, align 4
  %conv15 = fpext float %22 to double
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNKSt3__25arrayIfLm3EEixEm(%"struct.std::__2::array.126"* %pos, i32 2) #7
  %23 = load float, float* %call16, align 4
  %conv17 = fpext float %23 to double
  %call18 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %20, i8* getelementptr inbounds ([21 x i8], [21 x i8]* @.str.1, i32 0, i32 0), double %conv, double %conv15, double %conv17)
  ret void
}

declare i32 @fprintf(%struct._IO_FILE*, i8*, ...) #1

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco9IndexTypeIjNS_19FaceIndex_tag_type_EE5valueEv(%"class.draco::IndexType.114"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.114"*, align 4
  store %"class.draco::IndexType.114"* %this, %"class.draco::IndexType.114"** %this.addr, align 4
  %this1 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value_, align 4
  ret i32 %0
}

declare %"class.draco::PointAttribute"* @_ZNK5draco10PointCloud17GetNamedAttributeENS_17GeometryAttribute4TypeE(%"class.draco::PointCloud"*, i32) #1

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"struct.std::__2::array"* @_ZNK5draco4Mesh4faceENS_9IndexTypeIjNS_19FaceIndex_tag_type_EEE(%"class.draco::Mesh"* %this, i32 %face_id.coerce) #0 comdat {
entry:
  %face_id = alloca %"class.draco::IndexType.114", align 4
  %this.addr = alloca %"class.draco::Mesh"*, align 4
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %face_id, i32 0, i32 0
  store i32 %face_id.coerce, i32* %coerce.dive, align 4
  store %"class.draco::Mesh"* %this, %"class.draco::Mesh"** %this.addr, align 4
  %this1 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %this.addr, align 4
  %faces_ = getelementptr inbounds %"class.draco::Mesh", %"class.draco::Mesh"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::array"* @_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEENSt3__25arrayINS1_IjNS_20PointIndex_tag_type_EEELm3EEEEixERKS3_(%"class.draco::IndexTypeVector.103"* %faces_, %"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %face_id)
  ret %"struct.std::__2::array"* %call
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #2

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.106"* @_ZNKSt3__25arrayIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELm3EEixEm(%"struct.std::__2::array"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::array"*, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::array"* %this, %"struct.std::__2::array"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %this.addr, align 4
  %__elems_ = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %this1, i32 0, i32 0
  %0 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %"class.draco::IndexType.106"], [3 x %"class.draco::IndexType.106"]* %__elems_, i32 0, i32 %0
  ret %"class.draco::IndexType.106"* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco14PointAttribute12mapped_indexENS_9IndexTypeIjNS_20PointIndex_tag_type_EEE(%"class.draco::PointAttribute"* %this, i32 %point_index.coerce) #0 comdat {
entry:
  %retval = alloca %"class.draco::IndexType", align 4
  %point_index = alloca %"class.draco::IndexType.106", align 4
  %this.addr = alloca %"class.draco::PointAttribute"*, align 4
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType.106", %"class.draco::IndexType.106"* %point_index, i32 0, i32 0
  store i32 %point_index.coerce, i32* %coerce.dive, align 4
  store %"class.draco::PointAttribute"* %this, %"class.draco::PointAttribute"** %this.addr, align 4
  %this1 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %this.addr, align 4
  %identity_mapping_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 4
  %0 = load i8, i8* %identity_mapping_, align 4
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call = call i32 @_ZNK5draco9IndexTypeIjNS_20PointIndex_tag_type_EE5valueEv(%"class.draco::IndexType.106"* %point_index)
  %call2 = call %"class.draco::IndexType"* @_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEC2Ej(%"class.draco::IndexType"* %retval, i32 %call)
  br label %return

if.end:                                           ; preds = %entry
  %indices_map_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 2
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEEEixERKS3_(%"class.draco::IndexTypeVector"* %indices_map_, %"class.draco::IndexType.106"* nonnull align 4 dereferenceable(4) %point_index)
  %1 = bitcast %"class.draco::IndexType"* %retval to i8*
  %2 = bitcast %"class.draco::IndexType"* %call3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 4, i1 false)
  br label %return

return:                                           ; preds = %if.end, %if.then
  %coerce.dive4 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %retval, i32 0, i32 0
  %3 = load i32, i32* %coerce.dive4, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK5draco17GeometryAttribute8GetValueIfLi3EEENSt3__25arrayIT_XT0_EEENS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEE(%"struct.std::__2::array.126"* noalias sret align 4 %agg.result, %"class.draco::GeometryAttribute"* %this, i32 %att_index.coerce) #0 comdat {
entry:
  %att_index = alloca %"class.draco::IndexType", align 4
  %this.addr = alloca %"class.draco::GeometryAttribute"*, align 4
  %byte_pos = alloca i64, align 8
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %att_index, i32 0, i32 0
  store i32 %att_index.coerce, i32* %coerce.dive, align 4
  store %"class.draco::GeometryAttribute"* %this, %"class.draco::GeometryAttribute"** %this.addr, align 4
  %this1 = load %"class.draco::GeometryAttribute"*, %"class.draco::GeometryAttribute"** %this.addr, align 4
  %byte_offset_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 6
  %0 = load i64, i64* %byte_offset_, align 8
  %byte_stride_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 5
  %1 = load i64, i64* %byte_stride_, align 8
  %call = call i32 @_ZNK5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EE5valueEv(%"class.draco::IndexType"* %att_index)
  %conv = zext i32 %call to i64
  %mul = mul nsw i64 %1, %conv
  %add = add nsw i64 %0, %mul
  store i64 %add, i64* %byte_pos, align 8
  %buffer_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 0
  %2 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %buffer_, align 8
  %3 = load i64, i64* %byte_pos, align 8
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNSt3__25arrayIfLm3EEixEm(%"struct.std::__2::array.126"* %agg.result, i32 0) #7
  %4 = bitcast float* %call2 to i8*
  call void @_ZNK5draco10DataBuffer4ReadExPvm(%"class.draco::DataBuffer"* %2, i64 %3, i8* %4, i32 12)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNKSt3__25arrayIfLm3EEixEm(%"struct.std::__2::array.126"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::array.126"*, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::array.126"* %this, %"struct.std::__2::array.126"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::array.126"*, %"struct.std::__2::array.126"** %this.addr, align 4
  %__elems_ = getelementptr inbounds %"struct.std::__2::array.126", %"struct.std::__2::array.126"* %this1, i32 0, i32 0
  %0 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds [3 x float], [3 x float]* %__elems_, i32 0, i32 %0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN5draco17MeshAreEquivalent11GetPositionERKNS_4MeshENS_9IndexTypeIjNS_19FaceIndex_tag_type_EEEi(%"class.draco::VectorD"* noalias sret align 4 %agg.result, %"class.draco::Mesh"* nonnull align 4 dereferenceable(108) %mesh, i32 %f.coerce, i32 %c) #0 {
entry:
  %result.ptr = alloca i8*, align 4
  %f = alloca %"class.draco::IndexType.114", align 4
  %mesh.addr = alloca %"class.draco::Mesh"*, align 4
  %c.addr = alloca i32, align 4
  %pos_att = alloca %"class.draco::PointAttribute"*, align 4
  %ver_index = alloca %"class.draco::IndexType.106", align 4
  %agg.tmp = alloca %"class.draco::IndexType.114", align 4
  %pos_index = alloca %"class.draco::IndexType", align 4
  %agg.tmp4 = alloca %"class.draco::IndexType.106", align 4
  %pos = alloca %"struct.std::__2::array.126", align 4
  %agg.tmp8 = alloca %"class.draco::IndexType", align 4
  %0 = bitcast %"class.draco::VectorD"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %f, i32 0, i32 0
  store i32 %f.coerce, i32* %coerce.dive, align 4
  store %"class.draco::Mesh"* %mesh, %"class.draco::Mesh"** %mesh.addr, align 4
  store i32 %c, i32* %c.addr, align 4
  %1 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %mesh.addr, align 4
  %2 = bitcast %"class.draco::Mesh"* %1 to %"class.draco::PointCloud"*
  %call = call %"class.draco::PointAttribute"* @_ZNK5draco10PointCloud17GetNamedAttributeENS_17GeometryAttribute4TypeE(%"class.draco::PointCloud"* %2, i32 0)
  store %"class.draco::PointAttribute"* %call, %"class.draco::PointAttribute"** %pos_att, align 4
  %3 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %mesh.addr, align 4
  %4 = bitcast %"class.draco::IndexType.114"* %agg.tmp to i8*
  %5 = bitcast %"class.draco::IndexType.114"* %f to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 4, i1 false)
  %coerce.dive1 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %agg.tmp, i32 0, i32 0
  %6 = load i32, i32* %coerce.dive1, align 4
  %call2 = call nonnull align 4 dereferenceable(12) %"struct.std::__2::array"* @_ZNK5draco4Mesh4faceENS_9IndexTypeIjNS_19FaceIndex_tag_type_EEE(%"class.draco::Mesh"* %3, i32 %6)
  %7 = load i32, i32* %c.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.106"* @_ZNKSt3__25arrayIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELm3EEixEm(%"struct.std::__2::array"* %call2, i32 %7) #7
  %8 = bitcast %"class.draco::IndexType.106"* %ver_index to i8*
  %9 = bitcast %"class.draco::IndexType.106"* %call3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %8, i8* align 4 %9, i32 4, i1 false)
  %10 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %pos_att, align 4
  %11 = bitcast %"class.draco::IndexType.106"* %agg.tmp4 to i8*
  %12 = bitcast %"class.draco::IndexType.106"* %ver_index to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %11, i8* align 4 %12, i32 4, i1 false)
  %coerce.dive5 = getelementptr inbounds %"class.draco::IndexType.106", %"class.draco::IndexType.106"* %agg.tmp4, i32 0, i32 0
  %13 = load i32, i32* %coerce.dive5, align 4
  %call6 = call i32 @_ZNK5draco14PointAttribute12mapped_indexENS_9IndexTypeIjNS_20PointIndex_tag_type_EEE(%"class.draco::PointAttribute"* %10, i32 %13)
  %coerce.dive7 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %pos_index, i32 0, i32 0
  store i32 %call6, i32* %coerce.dive7, align 4
  %14 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %pos_att, align 4
  %15 = bitcast %"class.draco::PointAttribute"* %14 to %"class.draco::GeometryAttribute"*
  %16 = bitcast %"class.draco::IndexType"* %agg.tmp8 to i8*
  %17 = bitcast %"class.draco::IndexType"* %pos_index to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %16, i8* align 4 %17, i32 4, i1 false)
  %coerce.dive9 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %agg.tmp8, i32 0, i32 0
  %18 = load i32, i32* %coerce.dive9, align 4
  call void @_ZNK5draco17GeometryAttribute8GetValueIfLi3EEENSt3__25arrayIT_XT0_EEENS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEE(%"struct.std::__2::array.126"* sret align 4 %pos, %"class.draco::GeometryAttribute"* %15, i32 %18)
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNKSt3__25arrayIfLm3EEixEm(%"struct.std::__2::array.126"* %pos, i32 0) #7
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNKSt3__25arrayIfLm3EEixEm(%"struct.std::__2::array.126"* %pos, i32 1) #7
  %call12 = call nonnull align 4 dereferenceable(4) float* @_ZNKSt3__25arrayIfLm3EEixEm(%"struct.std::__2::array.126"* %pos, i32 2) #7
  %call13 = call %"class.draco::VectorD"* @_ZN5draco7VectorDIfLi3EEC2ERKfS3_S3_(%"class.draco::VectorD"* %agg.result, float* nonnull align 4 dereferenceable(4) %call10, float* nonnull align 4 dereferenceable(4) %call11, float* nonnull align 4 dereferenceable(4) %call12)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::VectorD"* @_ZN5draco7VectorDIfLi3EEC2ERKfS3_S3_(%"class.draco::VectorD"* returned %this, float* nonnull align 4 dereferenceable(4) %c0, float* nonnull align 4 dereferenceable(4) %c1, float* nonnull align 4 dereferenceable(4) %c2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::VectorD"*, align 4
  %c0.addr = alloca float*, align 4
  %c1.addr = alloca float*, align 4
  %c2.addr = alloca float*, align 4
  store %"class.draco::VectorD"* %this, %"class.draco::VectorD"** %this.addr, align 4
  store float* %c0, float** %c0.addr, align 4
  store float* %c1, float** %c1.addr, align 4
  store float* %c2, float** %c2.addr, align 4
  %this1 = load %"class.draco::VectorD"*, %"class.draco::VectorD"** %this.addr, align 4
  %v_ = getelementptr inbounds %"class.draco::VectorD", %"class.draco::VectorD"* %this1, i32 0, i32 0
  %__elems_ = getelementptr inbounds %"struct.std::__2::array.126", %"struct.std::__2::array.126"* %v_, i32 0, i32 0
  %arrayinit.begin = getelementptr inbounds [3 x float], [3 x float]* %__elems_, i32 0, i32 0
  %0 = load float*, float** %c0.addr, align 4
  %1 = load float, float* %0, align 4
  store float %1, float* %arrayinit.begin, align 4
  %arrayinit.element = getelementptr inbounds float, float* %arrayinit.begin, i32 1
  %2 = load float*, float** %c1.addr, align 4
  %3 = load float, float* %2, align 4
  store float %3, float* %arrayinit.element, align 4
  %arrayinit.element2 = getelementptr inbounds float, float* %arrayinit.element, i32 1
  %4 = load float*, float** %c2.addr, align 4
  %5 = load float, float* %4, align 4
  store float %5, float* %arrayinit.element2, align 4
  ret %"class.draco::VectorD"* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN5draco17MeshAreEquivalent33InitCornerIndexOfSmallestPointXYZEv(%"class.draco::MeshAreEquivalent"* %this) #0 {
entry:
  %this.addr = alloca %"class.draco::MeshAreEquivalent"*, align 4
  %i = alloca i32, align 4
  %f = alloca %"class.draco::IndexType.114", align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp10 = alloca i32, align 4
  %agg.tmp = alloca %"class.draco::IndexType.114", align 4
  store %"class.draco::MeshAreEquivalent"* %this, %"class.draco::MeshAreEquivalent"** %this.addr, align 4
  %this1 = load %"class.draco::MeshAreEquivalent"*, %"class.draco::MeshAreEquivalent"** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc15, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 2
  br i1 %cmp, label %for.body, label %for.end16

for.body:                                         ; preds = %for.cond
  %mesh_infos_ = getelementptr inbounds %"class.draco::MeshAreEquivalent", %"class.draco::MeshAreEquivalent"* %this1, i32 0, i32 0
  %1 = load i32, i32* %i, align 4
  %call = call nonnull align 4 dereferenceable(28) %"struct.draco::MeshAreEquivalent::MeshInfo"* @_ZNSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEEixEm(%"class.std::__2::vector"* %mesh_infos_, i32 %1) #7
  %corner_index_of_smallest_vertex = getelementptr inbounds %"struct.draco::MeshAreEquivalent::MeshInfo", %"struct.draco::MeshAreEquivalent::MeshInfo"* %call, i32 0, i32 2
  %num_faces_ = getelementptr inbounds %"class.draco::MeshAreEquivalent", %"class.draco::MeshAreEquivalent"* %this1, i32 0, i32 1
  %2 = load i32, i32* %num_faces_, align 4
  call void @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEEiE7reserveEm(%"class.draco::IndexTypeVector.120"* %corner_index_of_smallest_vertex, i32 %2)
  %call2 = call %"class.draco::IndexType.114"* @_ZN5draco9IndexTypeIjNS_19FaceIndex_tag_type_EEC2Ej(%"class.draco::IndexType.114"* %f, i32 0)
  br label %for.cond3

for.cond3:                                        ; preds = %for.inc, %for.body
  %num_faces_4 = getelementptr inbounds %"class.draco::MeshAreEquivalent", %"class.draco::MeshAreEquivalent"* %this1, i32 0, i32 1
  %3 = load i32, i32* %num_faces_4, align 4
  store i32 %3, i32* %ref.tmp, align 4
  %call5 = call zeroext i1 @_ZNK5draco9IndexTypeIjNS_19FaceIndex_tag_type_EEltERKj(%"class.draco::IndexType.114"* %f, i32* nonnull align 4 dereferenceable(4) %ref.tmp)
  br i1 %call5, label %for.body6, label %for.end

for.body6:                                        ; preds = %for.cond3
  %mesh_infos_7 = getelementptr inbounds %"class.draco::MeshAreEquivalent", %"class.draco::MeshAreEquivalent"* %this1, i32 0, i32 0
  %4 = load i32, i32* %i, align 4
  %call8 = call nonnull align 4 dereferenceable(28) %"struct.draco::MeshAreEquivalent::MeshInfo"* @_ZNSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEEixEm(%"class.std::__2::vector"* %mesh_infos_7, i32 %4) #7
  %corner_index_of_smallest_vertex9 = getelementptr inbounds %"struct.draco::MeshAreEquivalent::MeshInfo", %"struct.draco::MeshAreEquivalent::MeshInfo"* %call8, i32 0, i32 2
  %mesh_infos_11 = getelementptr inbounds %"class.draco::MeshAreEquivalent", %"class.draco::MeshAreEquivalent"* %this1, i32 0, i32 0
  %5 = load i32, i32* %i, align 4
  %call12 = call nonnull align 4 dereferenceable(28) %"struct.draco::MeshAreEquivalent::MeshInfo"* @_ZNSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEEixEm(%"class.std::__2::vector"* %mesh_infos_11, i32 %5) #7
  %mesh = getelementptr inbounds %"struct.draco::MeshAreEquivalent::MeshInfo", %"struct.draco::MeshAreEquivalent::MeshInfo"* %call12, i32 0, i32 0
  %6 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %mesh, align 4
  %7 = bitcast %"class.draco::IndexType.114"* %agg.tmp to i8*
  %8 = bitcast %"class.draco::IndexType.114"* %f to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 4, i1 false)
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %agg.tmp, i32 0, i32 0
  %9 = load i32, i32* %coerce.dive, align 4
  %call13 = call i32 @_ZN5draco17MeshAreEquivalent36ComputeCornerIndexOfSmallestPointXYZERKNS_4MeshENS_9IndexTypeIjNS_19FaceIndex_tag_type_EEE(%"class.draco::Mesh"* nonnull align 4 dereferenceable(108) %6, i32 %9)
  store i32 %call13, i32* %ref.tmp10, align 4
  call void @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEEiE9push_backEOi(%"class.draco::IndexTypeVector.120"* %corner_index_of_smallest_vertex9, i32* nonnull align 4 dereferenceable(4) %ref.tmp10)
  br label %for.inc

for.inc:                                          ; preds = %for.body6
  %call14 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.114"* @_ZN5draco9IndexTypeIjNS_19FaceIndex_tag_type_EEppEv(%"class.draco::IndexType.114"* %f)
  br label %for.cond3

for.end:                                          ; preds = %for.cond3
  br label %for.inc15

for.inc15:                                        ; preds = %for.end
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end16:                                        ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(28) %"struct.draco::MeshAreEquivalent::MeshInfo"* @_ZNSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEEixEm(%"class.std::__2::vector"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 0
  %1 = load %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__begin_, align 4
  %2 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds %"struct.draco::MeshAreEquivalent::MeshInfo", %"struct.draco::MeshAreEquivalent::MeshInfo"* %1, i32 %2
  ret %"struct.draco::MeshAreEquivalent::MeshInfo"* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEEiE7reserveEm(%"class.draco::IndexTypeVector.120"* %this, i32 %size) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector.120"*, align 4
  %size.addr = alloca i32, align 4
  store %"class.draco::IndexTypeVector.120"* %this, %"class.draco::IndexTypeVector.120"** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector.120"*, %"class.draco::IndexTypeVector.120"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector.120", %"class.draco::IndexTypeVector.120"* %this1, i32 0, i32 0
  %0 = load i32, i32* %size.addr, align 4
  call void @_ZNSt3__26vectorIiNS_9allocatorIiEEE7reserveEm(%"class.std::__2::vector.89"* %vector_, i32 %0)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType.114"* @_ZN5draco9IndexTypeIjNS_19FaceIndex_tag_type_EEC2Ej(%"class.draco::IndexType.114"* returned %this, i32 %value) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.114"*, align 4
  %value.addr = alloca i32, align 4
  store %"class.draco::IndexType.114"* %this, %"class.draco::IndexType.114"** %this.addr, align 4
  store i32 %value, i32* %value.addr, align 4
  %this1 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value.addr, align 4
  store i32 %0, i32* %value_, align 4
  ret %"class.draco::IndexType.114"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK5draco9IndexTypeIjNS_19FaceIndex_tag_type_EEltERKj(%"class.draco::IndexType.114"* %this, i32* nonnull align 4 dereferenceable(4) %val) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.114"*, align 4
  %val.addr = alloca i32*, align 4
  store %"class.draco::IndexType.114"* %this, %"class.draco::IndexType.114"** %this.addr, align 4
  store i32* %val, i32** %val.addr, align 4
  %this1 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value_, align 4
  %1 = load i32*, i32** %val.addr, align 4
  %2 = load i32, i32* %1, align 4
  %cmp = icmp ult i32 %0, %2
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEEiE9push_backEOi(%"class.draco::IndexTypeVector.120"* %this, i32* nonnull align 4 dereferenceable(4) %val) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector.120"*, align 4
  %val.addr = alloca i32*, align 4
  store %"class.draco::IndexTypeVector.120"* %this, %"class.draco::IndexTypeVector.120"** %this.addr, align 4
  store i32* %val, i32** %val.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector.120"*, %"class.draco::IndexTypeVector.120"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector.120", %"class.draco::IndexTypeVector.120"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %val.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__24moveIRiEEONS_16remove_referenceIT_E4typeEOS3_(i32* nonnull align 4 dereferenceable(4) %0) #7
  call void @_ZNSt3__26vectorIiNS_9allocatorIiEEE9push_backEOi(%"class.std::__2::vector.89"* %vector_, i32* nonnull align 4 dereferenceable(4) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @_ZN5draco17MeshAreEquivalent36ComputeCornerIndexOfSmallestPointXYZERKNS_4MeshENS_9IndexTypeIjNS_19FaceIndex_tag_type_EEE(%"class.draco::Mesh"* nonnull align 4 dereferenceable(108) %mesh, i32 %f.coerce) #0 {
entry:
  %f = alloca %"class.draco::IndexType.114", align 4
  %mesh.addr = alloca %"class.draco::Mesh"*, align 4
  %pos = alloca [3 x %"class.draco::VectorD"], align 16
  %i = alloca i32, align 4
  %ref.tmp = alloca %"class.draco::VectorD", align 4
  %agg.tmp = alloca %"class.draco::IndexType.114", align 4
  %min_it = alloca %"class.draco::VectorD"*, align 4
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %f, i32 0, i32 0
  store i32 %f.coerce, i32* %coerce.dive, align 4
  store %"class.draco::Mesh"* %mesh, %"class.draco::Mesh"** %mesh.addr, align 4
  %array.begin = getelementptr inbounds [3 x %"class.draco::VectorD"], [3 x %"class.draco::VectorD"]* %pos, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %"class.draco::VectorD", %"class.draco::VectorD"* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %"class.draco::VectorD"* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %"class.draco::VectorD"* @_ZN5draco7VectorDIfLi3EEC2Ev(%"class.draco::VectorD"* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %"class.draco::VectorD", %"class.draco::VectorD"* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %"class.draco::VectorD"* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %arrayctor.cont
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %1 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %mesh.addr, align 4
  %2 = bitcast %"class.draco::IndexType.114"* %agg.tmp to i8*
  %3 = bitcast %"class.draco::IndexType.114"* %f to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 4, i1 false)
  %4 = load i32, i32* %i, align 4
  %coerce.dive1 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %agg.tmp, i32 0, i32 0
  %5 = load i32, i32* %coerce.dive1, align 4
  call void @_ZN5draco17MeshAreEquivalent11GetPositionERKNS_4MeshENS_9IndexTypeIjNS_19FaceIndex_tag_type_EEEi(%"class.draco::VectorD"* sret align 4 %ref.tmp, %"class.draco::Mesh"* nonnull align 4 dereferenceable(108) %1, i32 %5, i32 %4)
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [3 x %"class.draco::VectorD"], [3 x %"class.draco::VectorD"]* %pos, i32 0, i32 %6
  %7 = bitcast %"class.draco::VectorD"* %arrayidx to i8*
  %8 = bitcast %"class.draco::VectorD"* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 12, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %9 = load i32, i32* %i, align 4
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %arraydecay = getelementptr inbounds [3 x %"class.draco::VectorD"], [3 x %"class.draco::VectorD"]* %pos, i32 0, i32 0
  %arraydecay2 = getelementptr inbounds [3 x %"class.draco::VectorD"], [3 x %"class.draco::VectorD"]* %pos, i32 0, i32 0
  %add.ptr = getelementptr inbounds %"class.draco::VectorD", %"class.draco::VectorD"* %arraydecay2, i32 3
  %call3 = call %"class.draco::VectorD"* @_ZNSt3__211min_elementIPN5draco7VectorDIfLi3EEEEET_S5_S5_(%"class.draco::VectorD"* %arraydecay, %"class.draco::VectorD"* %add.ptr)
  store %"class.draco::VectorD"* %call3, %"class.draco::VectorD"** %min_it, align 4
  %10 = load %"class.draco::VectorD"*, %"class.draco::VectorD"** %min_it, align 4
  %arraydecay4 = getelementptr inbounds [3 x %"class.draco::VectorD"], [3 x %"class.draco::VectorD"]* %pos, i32 0, i32 0
  %sub.ptr.lhs.cast = ptrtoint %"class.draco::VectorD"* %10 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.draco::VectorD"* %arraydecay4 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 12
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.114"* @_ZN5draco9IndexTypeIjNS_19FaceIndex_tag_type_EEppEv(%"class.draco::IndexType.114"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.114"*, align 4
  store %"class.draco::IndexType.114"* %this, %"class.draco::IndexType.114"** %this.addr, align 4
  %this1 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value_, align 4
  %inc = add i32 %0, 1
  store i32 %inc, i32* %value_, align 4
  ret %"class.draco::IndexType.114"* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN5draco17MeshAreEquivalent20InitOrderedFaceIndexEv(%"class.draco::MeshAreEquivalent"* %this) #0 {
entry:
  %this.addr = alloca %"class.draco::MeshAreEquivalent"*, align 4
  %i = alloca i32, align 4
  %j = alloca %"class.draco::IndexType.114", align 4
  %ref.tmp = alloca i32, align 4
  %less = alloca %"struct.draco::MeshAreEquivalent::FaceIndexLess", align 4
  %agg.tmp = alloca %"class.std::__2::__wrap_iter", align 4
  %agg.tmp18 = alloca %"class.std::__2::__wrap_iter", align 4
  %agg.tmp24 = alloca %"struct.draco::MeshAreEquivalent::FaceIndexLess", align 4
  store %"class.draco::MeshAreEquivalent"* %this, %"class.draco::MeshAreEquivalent"** %this.addr, align 4
  %this1 = load %"class.draco::MeshAreEquivalent"*, %"class.draco::MeshAreEquivalent"** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc28, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 2
  br i1 %cmp, label %for.body, label %for.end29

for.body:                                         ; preds = %for.cond
  %mesh_infos_ = getelementptr inbounds %"class.draco::MeshAreEquivalent", %"class.draco::MeshAreEquivalent"* %this1, i32 0, i32 0
  %1 = load i32, i32* %i, align 4
  %call = call nonnull align 4 dereferenceable(28) %"struct.draco::MeshAreEquivalent::MeshInfo"* @_ZNSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEEixEm(%"class.std::__2::vector"* %mesh_infos_, i32 %1) #7
  %ordered_index_of_face = getelementptr inbounds %"struct.draco::MeshAreEquivalent::MeshInfo", %"struct.draco::MeshAreEquivalent::MeshInfo"* %call, i32 0, i32 1
  %num_faces_ = getelementptr inbounds %"class.draco::MeshAreEquivalent", %"class.draco::MeshAreEquivalent"* %this1, i32 0, i32 1
  %2 = load i32, i32* %num_faces_, align 4
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE7reserveEm(%"class.std::__2::vector.112"* %ordered_index_of_face, i32 %2)
  %call2 = call %"class.draco::IndexType.114"* @_ZN5draco9IndexTypeIjNS_19FaceIndex_tag_type_EEC2Ej(%"class.draco::IndexType.114"* %j, i32 0)
  br label %for.cond3

for.cond3:                                        ; preds = %for.inc, %for.body
  %num_faces_4 = getelementptr inbounds %"class.draco::MeshAreEquivalent", %"class.draco::MeshAreEquivalent"* %this1, i32 0, i32 1
  %3 = load i32, i32* %num_faces_4, align 4
  store i32 %3, i32* %ref.tmp, align 4
  %call5 = call zeroext i1 @_ZNK5draco9IndexTypeIjNS_19FaceIndex_tag_type_EEltERKj(%"class.draco::IndexType.114"* %j, i32* nonnull align 4 dereferenceable(4) %ref.tmp)
  br i1 %call5, label %for.body6, label %for.end

for.body6:                                        ; preds = %for.cond3
  %mesh_infos_7 = getelementptr inbounds %"class.draco::MeshAreEquivalent", %"class.draco::MeshAreEquivalent"* %this1, i32 0, i32 0
  %4 = load i32, i32* %i, align 4
  %call8 = call nonnull align 4 dereferenceable(28) %"struct.draco::MeshAreEquivalent::MeshInfo"* @_ZNSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEEixEm(%"class.std::__2::vector"* %mesh_infos_7, i32 %4) #7
  %ordered_index_of_face9 = getelementptr inbounds %"struct.draco::MeshAreEquivalent::MeshInfo", %"struct.draco::MeshAreEquivalent::MeshInfo"* %call8, i32 0, i32 1
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE9push_backERKS4_(%"class.std::__2::vector.112"* %ordered_index_of_face9, %"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %j)
  br label %for.inc

for.inc:                                          ; preds = %for.body6
  %call10 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.114"* @_ZN5draco9IndexTypeIjNS_19FaceIndex_tag_type_EEppEv(%"class.draco::IndexType.114"* %j)
  br label %for.cond3

for.end:                                          ; preds = %for.cond3
  %mesh_infos_11 = getelementptr inbounds %"class.draco::MeshAreEquivalent", %"class.draco::MeshAreEquivalent"* %this1, i32 0, i32 0
  %5 = load i32, i32* %i, align 4
  %call12 = call nonnull align 4 dereferenceable(28) %"struct.draco::MeshAreEquivalent::MeshInfo"* @_ZNSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEEixEm(%"class.std::__2::vector"* %mesh_infos_11, i32 %5) #7
  %call13 = call %"struct.draco::MeshAreEquivalent::FaceIndexLess"* @_ZN5draco17MeshAreEquivalent13FaceIndexLessC2ERKNS0_8MeshInfoE(%"struct.draco::MeshAreEquivalent::FaceIndexLess"* %less, %"struct.draco::MeshAreEquivalent::MeshInfo"* nonnull align 4 dereferenceable(28) %call12)
  %mesh_infos_14 = getelementptr inbounds %"class.draco::MeshAreEquivalent", %"class.draco::MeshAreEquivalent"* %this1, i32 0, i32 0
  %6 = load i32, i32* %i, align 4
  %call15 = call nonnull align 4 dereferenceable(28) %"struct.draco::MeshAreEquivalent::MeshInfo"* @_ZNSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEEixEm(%"class.std::__2::vector"* %mesh_infos_14, i32 %6) #7
  %ordered_index_of_face16 = getelementptr inbounds %"struct.draco::MeshAreEquivalent::MeshInfo", %"struct.draco::MeshAreEquivalent::MeshInfo"* %call15, i32 0, i32 1
  %call17 = call %"class.draco::IndexType.114"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE5beginEv(%"class.std::__2::vector.112"* %ordered_index_of_face16) #7
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %agg.tmp, i32 0, i32 0
  store %"class.draco::IndexType.114"* %call17, %"class.draco::IndexType.114"** %coerce.dive, align 4
  %mesh_infos_19 = getelementptr inbounds %"class.draco::MeshAreEquivalent", %"class.draco::MeshAreEquivalent"* %this1, i32 0, i32 0
  %7 = load i32, i32* %i, align 4
  %call20 = call nonnull align 4 dereferenceable(28) %"struct.draco::MeshAreEquivalent::MeshInfo"* @_ZNSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEEixEm(%"class.std::__2::vector"* %mesh_infos_19, i32 %7) #7
  %ordered_index_of_face21 = getelementptr inbounds %"struct.draco::MeshAreEquivalent::MeshInfo", %"struct.draco::MeshAreEquivalent::MeshInfo"* %call20, i32 0, i32 1
  %call22 = call %"class.draco::IndexType.114"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE3endEv(%"class.std::__2::vector.112"* %ordered_index_of_face21) #7
  %coerce.dive23 = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %agg.tmp18, i32 0, i32 0
  store %"class.draco::IndexType.114"* %call22, %"class.draco::IndexType.114"** %coerce.dive23, align 4
  %8 = bitcast %"struct.draco::MeshAreEquivalent::FaceIndexLess"* %agg.tmp24 to i8*
  %9 = bitcast %"struct.draco::MeshAreEquivalent::FaceIndexLess"* %less to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %8, i8* align 4 %9, i32 4, i1 false)
  %coerce.dive25 = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %agg.tmp, i32 0, i32 0
  %10 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %coerce.dive25, align 4
  %coerce.dive26 = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %agg.tmp18, i32 0, i32 0
  %11 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %coerce.dive26, align 4
  %coerce.dive27 = getelementptr inbounds %"struct.draco::MeshAreEquivalent::FaceIndexLess", %"struct.draco::MeshAreEquivalent::FaceIndexLess"* %agg.tmp24, i32 0, i32 0
  %12 = load %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"** %coerce.dive27, align 4
  call void @_ZNSt3__24sortIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS1_17MeshAreEquivalent13FaceIndexLessEEEvNS_11__wrap_iterIPT_EESA_T0_(%"class.draco::IndexType.114"* %10, %"class.draco::IndexType.114"* %11, %"struct.draco::MeshAreEquivalent::MeshInfo"* %12)
  br label %for.inc28

for.inc28:                                        ; preds = %for.end
  %13 = load i32, i32* %i, align 4
  %inc = add nsw i32 %13, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end29:                                        ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE7reserveEm(%"class.std::__2::vector.112"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.112"*, align 4
  %__n.addr = alloca i32, align 4
  %__a = alloca %"class.std::__2::allocator.118"*, align 4
  %__v = alloca %"struct.std::__2::__split_buffer.136", align 4
  store %"class.std::__2::vector.112"* %this, %"class.std::__2::vector.112"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.112"*, %"class.std::__2::vector.112"** %this.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.112"* %this1) #7
  %cmp = icmp ugt i32 %0, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %"class.std::__2::vector.112"* %this1 to %"class.std::__2::__vector_base.113"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.118"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.113"* %1) #7
  store %"class.std::__2::allocator.118"* %call2, %"class.std::__2::allocator.118"** %__a, align 4
  %2 = load i32, i32* %__n.addr, align 4
  %call3 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.112"* %this1) #7
  %3 = load %"class.std::__2::allocator.118"*, %"class.std::__2::allocator.118"** %__a, align 4
  %call4 = call %"struct.std::__2::__split_buffer.136"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEEC2EmmS7_(%"struct.std::__2::__split_buffer.136"* %__v, i32 %2, i32 %call3, %"class.std::__2::allocator.118"* nonnull align 1 dereferenceable(1) %3)
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS4_RS6_EE(%"class.std::__2::vector.112"* %this1, %"struct.std::__2::__split_buffer.136"* nonnull align 4 dereferenceable(20) %__v)
  %call5 = call %"struct.std::__2::__split_buffer.136"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEED2Ev(%"struct.std::__2::__split_buffer.136"* %__v) #7
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE9push_backERKS4_(%"class.std::__2::vector.112"* %this, %"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %__x) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.112"*, align 4
  %__x.addr = alloca %"class.draco::IndexType.114"*, align 4
  store %"class.std::__2::vector.112"* %this, %"class.std::__2::vector.112"** %this.addr, align 4
  store %"class.draco::IndexType.114"* %__x, %"class.draco::IndexType.114"** %__x.addr, align 4
  %this1 = load %"class.std::__2::vector.112"*, %"class.std::__2::vector.112"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.112"* %this1 to %"class.std::__2::__vector_base.113"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.113", %"class.std::__2::__vector_base.113"* %0, i32 0, i32 1
  %1 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector.112"* %this1 to %"class.std::__2::__vector_base.113"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.114"** @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base.113"* %2) #7
  %3 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %call, align 4
  %cmp = icmp ne %"class.draco::IndexType.114"* %1, %3
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %4 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__x.addr, align 4
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE22__construct_one_at_endIJRKS4_EEEvDpOT_(%"class.std::__2::vector.112"* %this1, %"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %4)
  br label %if.end

if.else:                                          ; preds = %entry
  %5 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__x.addr, align 4
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE21__push_back_slow_pathIRKS4_EEvOT_(%"class.std::__2::vector.112"* %this1, %"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %5)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.draco::MeshAreEquivalent::FaceIndexLess"* @_ZN5draco17MeshAreEquivalent13FaceIndexLessC2ERKNS0_8MeshInfoE(%"struct.draco::MeshAreEquivalent::FaceIndexLess"* returned %this, %"struct.draco::MeshAreEquivalent::MeshInfo"* nonnull align 4 dereferenceable(28) %in_mesh_info) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.draco::MeshAreEquivalent::FaceIndexLess"*, align 4
  %in_mesh_info.addr = alloca %"struct.draco::MeshAreEquivalent::MeshInfo"*, align 4
  store %"struct.draco::MeshAreEquivalent::FaceIndexLess"* %this, %"struct.draco::MeshAreEquivalent::FaceIndexLess"** %this.addr, align 4
  store %"struct.draco::MeshAreEquivalent::MeshInfo"* %in_mesh_info, %"struct.draco::MeshAreEquivalent::MeshInfo"** %in_mesh_info.addr, align 4
  %this1 = load %"struct.draco::MeshAreEquivalent::FaceIndexLess"*, %"struct.draco::MeshAreEquivalent::FaceIndexLess"** %this.addr, align 4
  %mesh_info = getelementptr inbounds %"struct.draco::MeshAreEquivalent::FaceIndexLess", %"struct.draco::MeshAreEquivalent::FaceIndexLess"* %this1, i32 0, i32 0
  %0 = load %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"** %in_mesh_info.addr, align 4
  store %"struct.draco::MeshAreEquivalent::MeshInfo"* %0, %"struct.draco::MeshAreEquivalent::MeshInfo"** %mesh_info, align 4
  ret %"struct.draco::MeshAreEquivalent::FaceIndexLess"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__24sortIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS1_17MeshAreEquivalent13FaceIndexLessEEEvNS_11__wrap_iterIPT_EESA_T0_(%"class.draco::IndexType.114"* %__first.coerce, %"class.draco::IndexType.114"* %__last.coerce, %"struct.draco::MeshAreEquivalent::MeshInfo"* %__comp.coerce) #0 comdat {
entry:
  %__first = alloca %"class.std::__2::__wrap_iter", align 4
  %__last = alloca %"class.std::__2::__wrap_iter", align 4
  %__comp = alloca %"struct.draco::MeshAreEquivalent::FaceIndexLess", align 4
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %__first, i32 0, i32 0
  store %"class.draco::IndexType.114"* %__first.coerce, %"class.draco::IndexType.114"** %coerce.dive, align 4
  %coerce.dive1 = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %__last, i32 0, i32 0
  store %"class.draco::IndexType.114"* %__last.coerce, %"class.draco::IndexType.114"** %coerce.dive1, align 4
  %coerce.dive2 = getelementptr inbounds %"struct.draco::MeshAreEquivalent::FaceIndexLess", %"struct.draco::MeshAreEquivalent::FaceIndexLess"* %__comp, i32 0, i32 0
  store %"struct.draco::MeshAreEquivalent::MeshInfo"* %__comp.coerce, %"struct.draco::MeshAreEquivalent::MeshInfo"** %coerce.dive2, align 4
  %call = call %"class.draco::IndexType.114"* @_ZNKSt3__211__wrap_iterIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEE4baseEv(%"class.std::__2::__wrap_iter"* %__first) #7
  %call3 = call %"class.draco::IndexType.114"* @_ZNKSt3__211__wrap_iterIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEE4baseEv(%"class.std::__2::__wrap_iter"* %__last) #7
  call void @_ZNSt3__24sortIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS1_17MeshAreEquivalent13FaceIndexLessEEEvT_S9_T0_(%"class.draco::IndexType.114"* %call, %"class.draco::IndexType.114"* %call3, %"struct.draco::MeshAreEquivalent::FaceIndexLess"* nonnull align 4 dereferenceable(4) %__comp)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType.114"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE5beginEv(%"class.std::__2::vector.112"* %this) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__wrap_iter", align 4
  %this.addr = alloca %"class.std::__2::vector.112"*, align 4
  store %"class.std::__2::vector.112"* %this, %"class.std::__2::vector.112"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.112"*, %"class.std::__2::vector.112"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.112"* %this1 to %"class.std::__2::__vector_base.113"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.113", %"class.std::__2::__vector_base.113"* %0, i32 0, i32 0
  %1 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__begin_, align 4
  %call = call %"class.draco::IndexType.114"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE11__make_iterEPS4_(%"class.std::__2::vector.112"* %this1, %"class.draco::IndexType.114"* %1) #7
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %retval, i32 0, i32 0
  store %"class.draco::IndexType.114"* %call, %"class.draco::IndexType.114"** %coerce.dive, align 4
  %coerce.dive2 = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %retval, i32 0, i32 0
  %2 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %coerce.dive2, align 4
  ret %"class.draco::IndexType.114"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType.114"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE3endEv(%"class.std::__2::vector.112"* %this) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__wrap_iter", align 4
  %this.addr = alloca %"class.std::__2::vector.112"*, align 4
  store %"class.std::__2::vector.112"* %this, %"class.std::__2::vector.112"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.112"*, %"class.std::__2::vector.112"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.112"* %this1 to %"class.std::__2::__vector_base.113"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.113", %"class.std::__2::__vector_base.113"* %0, i32 0, i32 1
  %1 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__end_, align 4
  %call = call %"class.draco::IndexType.114"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE11__make_iterEPS4_(%"class.std::__2::vector.112"* %this1, %"class.draco::IndexType.114"* %1) #7
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %retval, i32 0, i32 0
  store %"class.draco::IndexType.114"* %call, %"class.draco::IndexType.114"** %coerce.dive, align 4
  %coerce.dive2 = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %retval, i32 0, i32 0
  %2 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %coerce.dive2, align 4
  ret %"class.draco::IndexType.114"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::VectorD"* @_ZN5draco7VectorDIfLi3EEC2Ev(%"class.draco::VectorD"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"class.draco::VectorD"*, align 4
  %this.addr = alloca %"class.draco::VectorD"*, align 4
  %i = alloca i32, align 4
  store %"class.draco::VectorD"* %this, %"class.draco::VectorD"** %this.addr, align 4
  %this1 = load %"class.draco::VectorD"*, %"class.draco::VectorD"** %this.addr, align 4
  store %"class.draco::VectorD"* %this1, %"class.draco::VectorD"** %retval, align 4
  %v_ = getelementptr inbounds %"class.draco::VectorD", %"class.draco::VectorD"* %this1, i32 0, i32 0
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %1 = load i32, i32* %i, align 4
  %call = call nonnull align 4 dereferenceable(4) float* @_ZN5draco7VectorDIfLi3EEixEi(%"class.draco::VectorD"* %this1, i32 %1)
  store float 0.000000e+00, float* %call, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %2 = load i32, i32* %i, align 4
  %inc = add nsw i32 %2, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %3 = load %"class.draco::VectorD"*, %"class.draco::VectorD"** %retval, align 4
  ret %"class.draco::VectorD"* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::VectorD"* @_ZNSt3__211min_elementIPN5draco7VectorDIfLi3EEEEET_S5_S5_(%"class.draco::VectorD"* %__first, %"class.draco::VectorD"* %__last) #0 comdat {
entry:
  %__first.addr = alloca %"class.draco::VectorD"*, align 4
  %__last.addr = alloca %"class.draco::VectorD"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__less", align 1
  store %"class.draco::VectorD"* %__first, %"class.draco::VectorD"** %__first.addr, align 4
  store %"class.draco::VectorD"* %__last, %"class.draco::VectorD"** %__last.addr, align 4
  %0 = load %"class.draco::VectorD"*, %"class.draco::VectorD"** %__first.addr, align 4
  %1 = load %"class.draco::VectorD"*, %"class.draco::VectorD"** %__last.addr, align 4
  %call = call %"class.draco::VectorD"* @_ZNSt3__211min_elementIPN5draco7VectorDIfLi3EEENS_6__lessIS3_S3_EEEET_S7_S7_T0_(%"class.draco::VectorD"* %0, %"class.draco::VectorD"* %1)
  ret %"class.draco::VectorD"* %call
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN5draco17MeshAreEquivalent4InitERKNS_4MeshES3_(%"class.draco::MeshAreEquivalent"* %this, %"class.draco::Mesh"* nonnull align 4 dereferenceable(108) %mesh0, %"class.draco::Mesh"* nonnull align 4 dereferenceable(108) %mesh1) #0 {
entry:
  %this.addr = alloca %"class.draco::MeshAreEquivalent"*, align 4
  %mesh0.addr = alloca %"class.draco::Mesh"*, align 4
  %mesh1.addr = alloca %"class.draco::Mesh"*, align 4
  %ref.tmp = alloca %"struct.draco::MeshAreEquivalent::MeshInfo", align 4
  %ref.tmp6 = alloca %"struct.draco::MeshAreEquivalent::MeshInfo", align 4
  store %"class.draco::MeshAreEquivalent"* %this, %"class.draco::MeshAreEquivalent"** %this.addr, align 4
  store %"class.draco::Mesh"* %mesh0, %"class.draco::Mesh"** %mesh0.addr, align 4
  store %"class.draco::Mesh"* %mesh1, %"class.draco::Mesh"** %mesh1.addr, align 4
  %this1 = load %"class.draco::MeshAreEquivalent"*, %"class.draco::MeshAreEquivalent"** %this.addr, align 4
  %mesh_infos_ = getelementptr inbounds %"class.draco::MeshAreEquivalent", %"class.draco::MeshAreEquivalent"* %this1, i32 0, i32 0
  call void @_ZNSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE5clearEv(%"class.std::__2::vector"* %mesh_infos_) #7
  %0 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %mesh1.addr, align 4
  %call = call i32 @_ZNK5draco4Mesh9num_facesEv(%"class.draco::Mesh"* %0)
  %num_faces_ = getelementptr inbounds %"class.draco::MeshAreEquivalent", %"class.draco::MeshAreEquivalent"* %this1, i32 0, i32 1
  store i32 %call, i32* %num_faces_, align 4
  %mesh_infos_2 = getelementptr inbounds %"class.draco::MeshAreEquivalent", %"class.draco::MeshAreEquivalent"* %this1, i32 0, i32 0
  %1 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %mesh0.addr, align 4
  %call3 = call %"struct.draco::MeshAreEquivalent::MeshInfo"* @_ZN5draco17MeshAreEquivalent8MeshInfoC2ERKNS_4MeshE(%"struct.draco::MeshAreEquivalent::MeshInfo"* %ref.tmp, %"class.draco::Mesh"* nonnull align 4 dereferenceable(108) %1)
  call void @_ZNSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE9push_backEOS3_(%"class.std::__2::vector"* %mesh_infos_2, %"struct.draco::MeshAreEquivalent::MeshInfo"* nonnull align 4 dereferenceable(28) %ref.tmp)
  %call4 = call %"struct.draco::MeshAreEquivalent::MeshInfo"* @_ZN5draco17MeshAreEquivalent8MeshInfoD2Ev(%"struct.draco::MeshAreEquivalent::MeshInfo"* %ref.tmp) #7
  %mesh_infos_5 = getelementptr inbounds %"class.draco::MeshAreEquivalent", %"class.draco::MeshAreEquivalent"* %this1, i32 0, i32 0
  %2 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %mesh1.addr, align 4
  %call7 = call %"struct.draco::MeshAreEquivalent::MeshInfo"* @_ZN5draco17MeshAreEquivalent8MeshInfoC2ERKNS_4MeshE(%"struct.draco::MeshAreEquivalent::MeshInfo"* %ref.tmp6, %"class.draco::Mesh"* nonnull align 4 dereferenceable(108) %2)
  call void @_ZNSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE9push_backEOS3_(%"class.std::__2::vector"* %mesh_infos_5, %"struct.draco::MeshAreEquivalent::MeshInfo"* nonnull align 4 dereferenceable(28) %ref.tmp6)
  %call8 = call %"struct.draco::MeshAreEquivalent::MeshInfo"* @_ZN5draco17MeshAreEquivalent8MeshInfoD2Ev(%"struct.draco::MeshAreEquivalent::MeshInfo"* %ref.tmp6) #7
  call void @_ZN5draco17MeshAreEquivalent33InitCornerIndexOfSmallestPointXYZEv(%"class.draco::MeshAreEquivalent"* %this1)
  call void @_ZN5draco17MeshAreEquivalent20InitOrderedFaceIndexEv(%"class.draco::MeshAreEquivalent"* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE5clearEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__old_size = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE4sizeEv(%"class.std::__2::vector"* %this1) #7
  store i32 %call, i32* %__old_size, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  call void @_ZNSt3__213__vector_baseIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE5clearEv(%"class.std::__2::__vector_base"* %0) #7
  %1 = load i32, i32* %__old_size, align 4
  call void @_ZNKSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE17__annotate_shrinkEm(%"class.std::__2::vector"* %this1, i32 %1) #7
  call void @_ZNSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector"* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco4Mesh9num_facesEv(%"class.draco::Mesh"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::Mesh"*, align 4
  store %"class.draco::Mesh"* %this, %"class.draco::Mesh"** %this.addr, align 4
  %this1 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %this.addr, align 4
  %faces_ = getelementptr inbounds %"class.draco::Mesh", %"class.draco::Mesh"* %this1, i32 0, i32 2
  %call = call i32 @_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEENSt3__25arrayINS1_IjNS_20PointIndex_tag_type_EEELm3EEEE4sizeEv(%"class.draco::IndexTypeVector.103"* %faces_)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE9push_backEOS3_(%"class.std::__2::vector"* %this, %"struct.draco::MeshAreEquivalent::MeshInfo"* nonnull align 4 dereferenceable(28) %__x) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__x.addr = alloca %"struct.draco::MeshAreEquivalent::MeshInfo"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store %"struct.draco::MeshAreEquivalent::MeshInfo"* %__x, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__x.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 1
  %1 = load %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call nonnull align 4 dereferenceable(4) %"struct.draco::MeshAreEquivalent::MeshInfo"** @_ZNSt3__213__vector_baseIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE9__end_capEv(%"class.std::__2::__vector_base"* %2) #7
  %3 = load %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"** %call, align 4
  %cmp = icmp ult %"struct.draco::MeshAreEquivalent::MeshInfo"* %1, %3
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %4 = load %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__x.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(28) %"struct.draco::MeshAreEquivalent::MeshInfo"* @_ZNSt3__24moveIRN5draco17MeshAreEquivalent8MeshInfoEEEONS_16remove_referenceIT_E4typeEOS6_(%"struct.draco::MeshAreEquivalent::MeshInfo"* nonnull align 4 dereferenceable(28) %4) #7
  call void @_ZNSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE22__construct_one_at_endIJS3_EEEvDpOT_(%"class.std::__2::vector"* %this1, %"struct.draco::MeshAreEquivalent::MeshInfo"* nonnull align 4 dereferenceable(28) %call2)
  br label %if.end

if.else:                                          ; preds = %entry
  %5 = load %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__x.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(28) %"struct.draco::MeshAreEquivalent::MeshInfo"* @_ZNSt3__24moveIRN5draco17MeshAreEquivalent8MeshInfoEEEONS_16remove_referenceIT_E4typeEOS6_(%"struct.draco::MeshAreEquivalent::MeshInfo"* nonnull align 4 dereferenceable(28) %5) #7
  call void @_ZNSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE21__push_back_slow_pathIS3_EEvOT_(%"class.std::__2::vector"* %this1, %"struct.draco::MeshAreEquivalent::MeshInfo"* nonnull align 4 dereferenceable(28) %call3)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.draco::MeshAreEquivalent::MeshInfo"* @_ZN5draco17MeshAreEquivalent8MeshInfoC2ERKNS_4MeshE(%"struct.draco::MeshAreEquivalent::MeshInfo"* returned %this, %"class.draco::Mesh"* nonnull align 4 dereferenceable(108) %in_mesh) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.draco::MeshAreEquivalent::MeshInfo"*, align 4
  %in_mesh.addr = alloca %"class.draco::Mesh"*, align 4
  store %"struct.draco::MeshAreEquivalent::MeshInfo"* %this, %"struct.draco::MeshAreEquivalent::MeshInfo"** %this.addr, align 4
  store %"class.draco::Mesh"* %in_mesh, %"class.draco::Mesh"** %in_mesh.addr, align 4
  %this1 = load %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"** %this.addr, align 4
  %mesh = getelementptr inbounds %"struct.draco::MeshAreEquivalent::MeshInfo", %"struct.draco::MeshAreEquivalent::MeshInfo"* %this1, i32 0, i32 0
  %0 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %in_mesh.addr, align 4
  store %"class.draco::Mesh"* %0, %"class.draco::Mesh"** %mesh, align 4
  %ordered_index_of_face = getelementptr inbounds %"struct.draco::MeshAreEquivalent::MeshInfo", %"struct.draco::MeshAreEquivalent::MeshInfo"* %this1, i32 0, i32 1
  %call = call %"class.std::__2::vector.112"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEEC2Ev(%"class.std::__2::vector.112"* %ordered_index_of_face) #7
  %corner_index_of_smallest_vertex = getelementptr inbounds %"struct.draco::MeshAreEquivalent::MeshInfo", %"struct.draco::MeshAreEquivalent::MeshInfo"* %this1, i32 0, i32 2
  %call2 = call %"class.draco::IndexTypeVector.120"* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEEiEC2Ev(%"class.draco::IndexTypeVector.120"* %corner_index_of_smallest_vertex)
  ret %"struct.draco::MeshAreEquivalent::MeshInfo"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.draco::MeshAreEquivalent::MeshInfo"* @_ZN5draco17MeshAreEquivalent8MeshInfoD2Ev(%"struct.draco::MeshAreEquivalent::MeshInfo"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.draco::MeshAreEquivalent::MeshInfo"*, align 4
  store %"struct.draco::MeshAreEquivalent::MeshInfo"* %this, %"struct.draco::MeshAreEquivalent::MeshInfo"** %this.addr, align 4
  %this1 = load %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"** %this.addr, align 4
  %corner_index_of_smallest_vertex = getelementptr inbounds %"struct.draco::MeshAreEquivalent::MeshInfo", %"struct.draco::MeshAreEquivalent::MeshInfo"* %this1, i32 0, i32 2
  %call = call %"class.draco::IndexTypeVector.120"* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEEiED2Ev(%"class.draco::IndexTypeVector.120"* %corner_index_of_smallest_vertex) #7
  %ordered_index_of_face = getelementptr inbounds %"struct.draco::MeshAreEquivalent::MeshInfo", %"struct.draco::MeshAreEquivalent::MeshInfo"* %this1, i32 0, i32 1
  %call2 = call %"class.std::__2::vector.112"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEED2Ev(%"class.std::__2::vector.112"* %ordered_index_of_face) #7
  ret %"struct.draco::MeshAreEquivalent::MeshInfo"* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden zeroext i1 @_ZN5draco17MeshAreEquivalentclERKNS_4MeshES3_(%"class.draco::MeshAreEquivalent"* %this, %"class.draco::Mesh"* nonnull align 4 dereferenceable(108) %mesh0, %"class.draco::Mesh"* nonnull align 4 dereferenceable(108) %mesh1) #0 {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::MeshAreEquivalent"*, align 4
  %mesh0.addr = alloca %"class.draco::Mesh"*, align 4
  %mesh1.addr = alloca %"class.draco::Mesh"*, align 4
  %att_max = alloca i32, align 4
  %att_id = alloca i32, align 4
  %att0 = alloca %"class.draco::PointAttribute"*, align 4
  %att1 = alloca %"class.draco::PointAttribute"*, align 4
  %data0 = alloca %"class.std::__2::unique_ptr.127", align 4
  %data1 = alloca %"class.std::__2::unique_ptr.127", align 4
  %i = alloca i32, align 4
  %f0 = alloca %"class.draco::IndexType.114", align 4
  %f1 = alloca %"class.draco::IndexType.114", align 4
  %c0_off = alloca i32, align 4
  %c1_off = alloca i32, align 4
  %c = alloca i32, align 4
  %corner0 = alloca %"class.draco::IndexType.106", align 4
  %agg.tmp = alloca %"class.draco::IndexType.114", align 4
  %corner1 = alloca %"class.draco::IndexType.106", align 4
  %agg.tmp73 = alloca %"class.draco::IndexType.114", align 4
  %index0 = alloca %"class.draco::IndexType", align 4
  %agg.tmp79 = alloca %"class.draco::IndexType.106", align 4
  %index1 = alloca %"class.draco::IndexType", align 4
  %agg.tmp83 = alloca %"class.draco::IndexType.106", align 4
  %agg.tmp87 = alloca %"class.draco::IndexType", align 4
  %agg.tmp90 = alloca %"class.draco::IndexType", align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %"class.draco::MeshAreEquivalent"* %this, %"class.draco::MeshAreEquivalent"** %this.addr, align 4
  store %"class.draco::Mesh"* %mesh0, %"class.draco::Mesh"** %mesh0.addr, align 4
  store %"class.draco::Mesh"* %mesh1, %"class.draco::Mesh"** %mesh1.addr, align 4
  %this1 = load %"class.draco::MeshAreEquivalent"*, %"class.draco::MeshAreEquivalent"** %this.addr, align 4
  %0 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %mesh0.addr, align 4
  %call = call i32 @_ZNK5draco4Mesh9num_facesEv(%"class.draco::Mesh"* %0)
  %1 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %mesh1.addr, align 4
  %call2 = call i32 @_ZNK5draco4Mesh9num_facesEv(%"class.draco::Mesh"* %1)
  %cmp = icmp ne i32 %call, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %2 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %mesh0.addr, align 4
  %3 = bitcast %"class.draco::Mesh"* %2 to %"class.draco::PointCloud"*
  %call3 = call i32 @_ZNK5draco10PointCloud14num_attributesEv(%"class.draco::PointCloud"* %3)
  %4 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %mesh1.addr, align 4
  %5 = bitcast %"class.draco::Mesh"* %4 to %"class.draco::PointCloud"*
  %call4 = call i32 @_ZNK5draco10PointCloud14num_attributesEv(%"class.draco::PointCloud"* %5)
  %cmp5 = icmp ne i32 %call3, %call4
  br i1 %cmp5, label %if.then6, label %if.end7

if.then6:                                         ; preds = %if.end
  store i1 false, i1* %retval, align 1
  br label %return

if.end7:                                          ; preds = %if.end
  %6 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %mesh0.addr, align 4
  %7 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %mesh1.addr, align 4
  call void @_ZN5draco17MeshAreEquivalent4InitERKNS_4MeshES3_(%"class.draco::MeshAreEquivalent"* %this1, %"class.draco::Mesh"* nonnull align 4 dereferenceable(108) %6, %"class.draco::Mesh"* nonnull align 4 dereferenceable(108) %7)
  store i32 5, i32* %att_max, align 4
  store i32 0, i32* %att_id, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc107, %if.end7
  %8 = load i32, i32* %att_id, align 4
  %cmp8 = icmp slt i32 %8, 5
  br i1 %cmp8, label %for.body, label %for.end109

for.body:                                         ; preds = %for.cond
  %9 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %mesh0.addr, align 4
  %10 = bitcast %"class.draco::Mesh"* %9 to %"class.draco::PointCloud"*
  %11 = load i32, i32* %att_id, align 4
  %call9 = call %"class.draco::PointAttribute"* @_ZNK5draco10PointCloud17GetNamedAttributeENS_17GeometryAttribute4TypeE(%"class.draco::PointCloud"* %10, i32 %11)
  store %"class.draco::PointAttribute"* %call9, %"class.draco::PointAttribute"** %att0, align 4
  %12 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %mesh1.addr, align 4
  %13 = bitcast %"class.draco::Mesh"* %12 to %"class.draco::PointCloud"*
  %14 = load i32, i32* %att_id, align 4
  %call10 = call %"class.draco::PointAttribute"* @_ZNK5draco10PointCloud17GetNamedAttributeENS_17GeometryAttribute4TypeE(%"class.draco::PointCloud"* %13, i32 %14)
  store %"class.draco::PointAttribute"* %call10, %"class.draco::PointAttribute"** %att1, align 4
  %15 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %att0, align 4
  %cmp11 = icmp eq %"class.draco::PointAttribute"* %15, null
  br i1 %cmp11, label %land.lhs.true, label %if.end14

land.lhs.true:                                    ; preds = %for.body
  %16 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %att1, align 4
  %cmp12 = icmp eq %"class.draco::PointAttribute"* %16, null
  br i1 %cmp12, label %if.then13, label %if.end14

if.then13:                                        ; preds = %land.lhs.true
  br label %for.inc107

if.end14:                                         ; preds = %land.lhs.true, %for.body
  %17 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %att0, align 4
  %cmp15 = icmp eq %"class.draco::PointAttribute"* %17, null
  br i1 %cmp15, label %if.then16, label %if.end17

if.then16:                                        ; preds = %if.end14
  store i1 false, i1* %retval, align 1
  br label %return

if.end17:                                         ; preds = %if.end14
  %18 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %att1, align 4
  %cmp18 = icmp eq %"class.draco::PointAttribute"* %18, null
  br i1 %cmp18, label %if.then19, label %if.end20

if.then19:                                        ; preds = %if.end17
  store i1 false, i1* %retval, align 1
  br label %return

if.end20:                                         ; preds = %if.end17
  %19 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %att0, align 4
  %20 = bitcast %"class.draco::PointAttribute"* %19 to %"class.draco::GeometryAttribute"*
  %call21 = call i32 @_ZNK5draco17GeometryAttribute9data_typeEv(%"class.draco::GeometryAttribute"* %20)
  %21 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %att1, align 4
  %22 = bitcast %"class.draco::PointAttribute"* %21 to %"class.draco::GeometryAttribute"*
  %call22 = call i32 @_ZNK5draco17GeometryAttribute9data_typeEv(%"class.draco::GeometryAttribute"* %22)
  %cmp23 = icmp ne i32 %call21, %call22
  br i1 %cmp23, label %if.then24, label %if.end25

if.then24:                                        ; preds = %if.end20
  store i1 false, i1* %retval, align 1
  br label %return

if.end25:                                         ; preds = %if.end20
  %23 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %att0, align 4
  %24 = bitcast %"class.draco::PointAttribute"* %23 to %"class.draco::GeometryAttribute"*
  %call26 = call signext i8 @_ZNK5draco17GeometryAttribute14num_componentsEv(%"class.draco::GeometryAttribute"* %24)
  %conv = sext i8 %call26 to i32
  %25 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %att1, align 4
  %26 = bitcast %"class.draco::PointAttribute"* %25 to %"class.draco::GeometryAttribute"*
  %call27 = call signext i8 @_ZNK5draco17GeometryAttribute14num_componentsEv(%"class.draco::GeometryAttribute"* %26)
  %conv28 = sext i8 %call27 to i32
  %cmp29 = icmp ne i32 %conv, %conv28
  br i1 %cmp29, label %if.then30, label %if.end31

if.then30:                                        ; preds = %if.end25
  store i1 false, i1* %retval, align 1
  br label %return

if.end31:                                         ; preds = %if.end25
  %27 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %att0, align 4
  %28 = bitcast %"class.draco::PointAttribute"* %27 to %"class.draco::GeometryAttribute"*
  %call32 = call zeroext i1 @_ZNK5draco17GeometryAttribute10normalizedEv(%"class.draco::GeometryAttribute"* %28)
  %conv33 = zext i1 %call32 to i32
  %29 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %att1, align 4
  %30 = bitcast %"class.draco::PointAttribute"* %29 to %"class.draco::GeometryAttribute"*
  %call34 = call zeroext i1 @_ZNK5draco17GeometryAttribute10normalizedEv(%"class.draco::GeometryAttribute"* %30)
  %conv35 = zext i1 %call34 to i32
  %cmp36 = icmp ne i32 %conv33, %conv35
  br i1 %cmp36, label %if.then37, label %if.end38

if.then37:                                        ; preds = %if.end31
  store i1 false, i1* %retval, align 1
  br label %return

if.end38:                                         ; preds = %if.end31
  %31 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %att0, align 4
  %32 = bitcast %"class.draco::PointAttribute"* %31 to %"class.draco::GeometryAttribute"*
  %call39 = call i64 @_ZNK5draco17GeometryAttribute11byte_strideEv(%"class.draco::GeometryAttribute"* %32)
  %33 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %att1, align 4
  %34 = bitcast %"class.draco::PointAttribute"* %33 to %"class.draco::GeometryAttribute"*
  %call40 = call i64 @_ZNK5draco17GeometryAttribute11byte_strideEv(%"class.draco::GeometryAttribute"* %34)
  %cmp41 = icmp ne i64 %call39, %call40
  br i1 %cmp41, label %if.then42, label %if.end43

if.then42:                                        ; preds = %if.end38
  store i1 false, i1* %retval, align 1
  br label %return

if.end43:                                         ; preds = %if.end38
  %35 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %att0, align 4
  %36 = bitcast %"class.draco::PointAttribute"* %35 to %"class.draco::GeometryAttribute"*
  %call44 = call i64 @_ZNK5draco17GeometryAttribute11byte_strideEv(%"class.draco::GeometryAttribute"* %36)
  %conv45 = trunc i64 %call44 to i32
  %call46 = call noalias nonnull i8* @_Znam(i32 %conv45) #8
  %call47 = call %"class.std::__2::unique_ptr.127"* @_ZNSt3__210unique_ptrIA_hNS_14default_deleteIS1_EEEC2IPhLb1EvvEET_(%"class.std::__2::unique_ptr.127"* %data0, i8* %call46) #7
  %37 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %att0, align 4
  %38 = bitcast %"class.draco::PointAttribute"* %37 to %"class.draco::GeometryAttribute"*
  %call48 = call i64 @_ZNK5draco17GeometryAttribute11byte_strideEv(%"class.draco::GeometryAttribute"* %38)
  %conv49 = trunc i64 %call48 to i32
  %call50 = call noalias nonnull i8* @_Znam(i32 %conv49) #8
  %call51 = call %"class.std::__2::unique_ptr.127"* @_ZNSt3__210unique_ptrIA_hNS_14default_deleteIS1_EEEC2IPhLb1EvvEET_(%"class.std::__2::unique_ptr.127"* %data1, i8* %call50) #7
  store i32 0, i32* %i, align 4
  br label %for.cond52

for.cond52:                                       ; preds = %for.inc101, %if.end43
  %39 = load i32, i32* %i, align 4
  %num_faces_ = getelementptr inbounds %"class.draco::MeshAreEquivalent", %"class.draco::MeshAreEquivalent"* %this1, i32 0, i32 1
  %40 = load i32, i32* %num_faces_, align 4
  %cmp53 = icmp slt i32 %39, %40
  br i1 %cmp53, label %for.body54, label %for.end103

for.body54:                                       ; preds = %for.cond52
  %mesh_infos_ = getelementptr inbounds %"class.draco::MeshAreEquivalent", %"class.draco::MeshAreEquivalent"* %this1, i32 0, i32 0
  %call55 = call nonnull align 4 dereferenceable(28) %"struct.draco::MeshAreEquivalent::MeshInfo"* @_ZNSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEEixEm(%"class.std::__2::vector"* %mesh_infos_, i32 0) #7
  %ordered_index_of_face = getelementptr inbounds %"struct.draco::MeshAreEquivalent::MeshInfo", %"struct.draco::MeshAreEquivalent::MeshInfo"* %call55, i32 0, i32 1
  %41 = load i32, i32* %i, align 4
  %call56 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.114"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEEixEm(%"class.std::__2::vector.112"* %ordered_index_of_face, i32 %41) #7
  %42 = bitcast %"class.draco::IndexType.114"* %f0 to i8*
  %43 = bitcast %"class.draco::IndexType.114"* %call56 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %42, i8* align 4 %43, i32 4, i1 false)
  %mesh_infos_57 = getelementptr inbounds %"class.draco::MeshAreEquivalent", %"class.draco::MeshAreEquivalent"* %this1, i32 0, i32 0
  %call58 = call nonnull align 4 dereferenceable(28) %"struct.draco::MeshAreEquivalent::MeshInfo"* @_ZNSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEEixEm(%"class.std::__2::vector"* %mesh_infos_57, i32 1) #7
  %ordered_index_of_face59 = getelementptr inbounds %"struct.draco::MeshAreEquivalent::MeshInfo", %"struct.draco::MeshAreEquivalent::MeshInfo"* %call58, i32 0, i32 1
  %44 = load i32, i32* %i, align 4
  %call60 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.114"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEEixEm(%"class.std::__2::vector.112"* %ordered_index_of_face59, i32 %44) #7
  %45 = bitcast %"class.draco::IndexType.114"* %f1 to i8*
  %46 = bitcast %"class.draco::IndexType.114"* %call60 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %45, i8* align 4 %46, i32 4, i1 false)
  %mesh_infos_61 = getelementptr inbounds %"class.draco::MeshAreEquivalent", %"class.draco::MeshAreEquivalent"* %this1, i32 0, i32 0
  %call62 = call nonnull align 4 dereferenceable(28) %"struct.draco::MeshAreEquivalent::MeshInfo"* @_ZNSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEEixEm(%"class.std::__2::vector"* %mesh_infos_61, i32 0) #7
  %corner_index_of_smallest_vertex = getelementptr inbounds %"struct.draco::MeshAreEquivalent::MeshInfo", %"struct.draco::MeshAreEquivalent::MeshInfo"* %call62, i32 0, i32 2
  %call63 = call nonnull align 4 dereferenceable(4) i32* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEEiEixERKS3_(%"class.draco::IndexTypeVector.120"* %corner_index_of_smallest_vertex, %"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %f0)
  %47 = load i32, i32* %call63, align 4
  store i32 %47, i32* %c0_off, align 4
  %mesh_infos_64 = getelementptr inbounds %"class.draco::MeshAreEquivalent", %"class.draco::MeshAreEquivalent"* %this1, i32 0, i32 0
  %call65 = call nonnull align 4 dereferenceable(28) %"struct.draco::MeshAreEquivalent::MeshInfo"* @_ZNSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEEixEm(%"class.std::__2::vector"* %mesh_infos_64, i32 1) #7
  %corner_index_of_smallest_vertex66 = getelementptr inbounds %"struct.draco::MeshAreEquivalent::MeshInfo", %"struct.draco::MeshAreEquivalent::MeshInfo"* %call65, i32 0, i32 2
  %call67 = call nonnull align 4 dereferenceable(4) i32* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEEiEixERKS3_(%"class.draco::IndexTypeVector.120"* %corner_index_of_smallest_vertex66, %"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %f1)
  %48 = load i32, i32* %call67, align 4
  store i32 %48, i32* %c1_off, align 4
  store i32 0, i32* %c, align 4
  br label %for.cond68

for.cond68:                                       ; preds = %for.inc, %for.body54
  %49 = load i32, i32* %c, align 4
  %cmp69 = icmp slt i32 %49, 3
  br i1 %cmp69, label %for.body70, label %for.end

for.body70:                                       ; preds = %for.cond68
  %50 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %mesh0.addr, align 4
  %51 = bitcast %"class.draco::IndexType.114"* %agg.tmp to i8*
  %52 = bitcast %"class.draco::IndexType.114"* %f0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %51, i8* align 4 %52, i32 4, i1 false)
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %agg.tmp, i32 0, i32 0
  %53 = load i32, i32* %coerce.dive, align 4
  %call71 = call nonnull align 4 dereferenceable(12) %"struct.std::__2::array"* @_ZNK5draco4Mesh4faceENS_9IndexTypeIjNS_19FaceIndex_tag_type_EEE(%"class.draco::Mesh"* %50, i32 %53)
  %54 = load i32, i32* %c0_off, align 4
  %55 = load i32, i32* %c, align 4
  %add = add nsw i32 %54, %55
  %rem = srem i32 %add, 3
  %call72 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.106"* @_ZNKSt3__25arrayIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELm3EEixEm(%"struct.std::__2::array"* %call71, i32 %rem) #7
  %56 = bitcast %"class.draco::IndexType.106"* %corner0 to i8*
  %57 = bitcast %"class.draco::IndexType.106"* %call72 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %56, i8* align 4 %57, i32 4, i1 false)
  %58 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %mesh1.addr, align 4
  %59 = bitcast %"class.draco::IndexType.114"* %agg.tmp73 to i8*
  %60 = bitcast %"class.draco::IndexType.114"* %f1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %59, i8* align 4 %60, i32 4, i1 false)
  %coerce.dive74 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %agg.tmp73, i32 0, i32 0
  %61 = load i32, i32* %coerce.dive74, align 4
  %call75 = call nonnull align 4 dereferenceable(12) %"struct.std::__2::array"* @_ZNK5draco4Mesh4faceENS_9IndexTypeIjNS_19FaceIndex_tag_type_EEE(%"class.draco::Mesh"* %58, i32 %61)
  %62 = load i32, i32* %c1_off, align 4
  %63 = load i32, i32* %c, align 4
  %add76 = add nsw i32 %62, %63
  %rem77 = srem i32 %add76, 3
  %call78 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.106"* @_ZNKSt3__25arrayIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELm3EEixEm(%"struct.std::__2::array"* %call75, i32 %rem77) #7
  %64 = bitcast %"class.draco::IndexType.106"* %corner1 to i8*
  %65 = bitcast %"class.draco::IndexType.106"* %call78 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %64, i8* align 4 %65, i32 4, i1 false)
  %66 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %att0, align 4
  %67 = bitcast %"class.draco::IndexType.106"* %agg.tmp79 to i8*
  %68 = bitcast %"class.draco::IndexType.106"* %corner0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %67, i8* align 4 %68, i32 4, i1 false)
  %coerce.dive80 = getelementptr inbounds %"class.draco::IndexType.106", %"class.draco::IndexType.106"* %agg.tmp79, i32 0, i32 0
  %69 = load i32, i32* %coerce.dive80, align 4
  %call81 = call i32 @_ZNK5draco14PointAttribute12mapped_indexENS_9IndexTypeIjNS_20PointIndex_tag_type_EEE(%"class.draco::PointAttribute"* %66, i32 %69)
  %coerce.dive82 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %index0, i32 0, i32 0
  store i32 %call81, i32* %coerce.dive82, align 4
  %70 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %att1, align 4
  %71 = bitcast %"class.draco::IndexType.106"* %agg.tmp83 to i8*
  %72 = bitcast %"class.draco::IndexType.106"* %corner1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %71, i8* align 4 %72, i32 4, i1 false)
  %coerce.dive84 = getelementptr inbounds %"class.draco::IndexType.106", %"class.draco::IndexType.106"* %agg.tmp83, i32 0, i32 0
  %73 = load i32, i32* %coerce.dive84, align 4
  %call85 = call i32 @_ZNK5draco14PointAttribute12mapped_indexENS_9IndexTypeIjNS_20PointIndex_tag_type_EEE(%"class.draco::PointAttribute"* %70, i32 %73)
  %coerce.dive86 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %index1, i32 0, i32 0
  store i32 %call85, i32* %coerce.dive86, align 4
  %74 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %att0, align 4
  %75 = bitcast %"class.draco::PointAttribute"* %74 to %"class.draco::GeometryAttribute"*
  %76 = bitcast %"class.draco::IndexType"* %agg.tmp87 to i8*
  %77 = bitcast %"class.draco::IndexType"* %index0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %76, i8* align 4 %77, i32 4, i1 false)
  %call88 = call i8* @_ZNKSt3__210unique_ptrIA_hNS_14default_deleteIS1_EEE3getEv(%"class.std::__2::unique_ptr.127"* %data0) #7
  %coerce.dive89 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %agg.tmp87, i32 0, i32 0
  %78 = load i32, i32* %coerce.dive89, align 4
  call void @_ZNK5draco17GeometryAttribute8GetValueENS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEEPv(%"class.draco::GeometryAttribute"* %75, i32 %78, i8* %call88)
  %79 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %att1, align 4
  %80 = bitcast %"class.draco::PointAttribute"* %79 to %"class.draco::GeometryAttribute"*
  %81 = bitcast %"class.draco::IndexType"* %agg.tmp90 to i8*
  %82 = bitcast %"class.draco::IndexType"* %index1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %81, i8* align 4 %82, i32 4, i1 false)
  %call91 = call i8* @_ZNKSt3__210unique_ptrIA_hNS_14default_deleteIS1_EEE3getEv(%"class.std::__2::unique_ptr.127"* %data1) #7
  %coerce.dive92 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %agg.tmp90, i32 0, i32 0
  %83 = load i32, i32* %coerce.dive92, align 4
  call void @_ZNK5draco17GeometryAttribute8GetValueENS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEEPv(%"class.draco::GeometryAttribute"* %80, i32 %83, i8* %call91)
  %call93 = call i8* @_ZNKSt3__210unique_ptrIA_hNS_14default_deleteIS1_EEE3getEv(%"class.std::__2::unique_ptr.127"* %data0) #7
  %call94 = call i8* @_ZNKSt3__210unique_ptrIA_hNS_14default_deleteIS1_EEE3getEv(%"class.std::__2::unique_ptr.127"* %data1) #7
  %84 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %att0, align 4
  %85 = bitcast %"class.draco::PointAttribute"* %84 to %"class.draco::GeometryAttribute"*
  %call95 = call i64 @_ZNK5draco17GeometryAttribute11byte_strideEv(%"class.draco::GeometryAttribute"* %85)
  %conv96 = trunc i64 %call95 to i32
  %call97 = call i32 @memcmp(i8* %call93, i8* %call94, i32 %conv96)
  %cmp98 = icmp ne i32 %call97, 0
  br i1 %cmp98, label %if.then99, label %if.end100

if.then99:                                        ; preds = %for.body70
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end100:                                        ; preds = %for.body70
  br label %for.inc

for.inc:                                          ; preds = %if.end100
  %86 = load i32, i32* %c, align 4
  %inc = add nsw i32 %86, 1
  store i32 %inc, i32* %c, align 4
  br label %for.cond68

for.end:                                          ; preds = %for.cond68
  br label %for.inc101

for.inc101:                                       ; preds = %for.end
  %87 = load i32, i32* %i, align 4
  %inc102 = add nsw i32 %87, 1
  store i32 %inc102, i32* %i, align 4
  br label %for.cond52

for.end103:                                       ; preds = %for.cond52
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end103, %if.then99
  %call104 = call %"class.std::__2::unique_ptr.127"* @_ZNSt3__210unique_ptrIA_hNS_14default_deleteIS1_EEED2Ev(%"class.std::__2::unique_ptr.127"* %data1) #7
  %call106 = call %"class.std::__2::unique_ptr.127"* @_ZNSt3__210unique_ptrIA_hNS_14default_deleteIS1_EEED2Ev(%"class.std::__2::unique_ptr.127"* %data0) #7
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %return
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc107

for.inc107:                                       ; preds = %cleanup.cont, %if.then13
  %88 = load i32, i32* %att_id, align 4
  %inc108 = add nsw i32 %88, 1
  store i32 %inc108, i32* %att_id, align 4
  br label %for.cond

for.end109:                                       ; preds = %for.cond
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %for.end109, %cleanup, %if.then42, %if.then37, %if.then30, %if.then24, %if.then19, %if.then16, %if.then6, %if.then
  %89 = load i1, i1* %retval, align 1
  ret i1 %89

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco10PointCloud14num_attributesEv(%"class.draco::PointCloud"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointCloud"*, align 4
  store %"class.draco::PointCloud"* %this, %"class.draco::PointCloud"** %this.addr, align 4
  %this1 = load %"class.draco::PointCloud"*, %"class.draco::PointCloud"** %this.addr, align 4
  %attributes_ = getelementptr inbounds %"class.draco::PointCloud", %"class.draco::PointCloud"* %this1, i32 0, i32 2
  %call = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.53"* %attributes_) #7
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco17GeometryAttribute9data_typeEv(%"class.draco::GeometryAttribute"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::GeometryAttribute"*, align 4
  store %"class.draco::GeometryAttribute"* %this, %"class.draco::GeometryAttribute"** %this.addr, align 4
  %this1 = load %"class.draco::GeometryAttribute"*, %"class.draco::GeometryAttribute"** %this.addr, align 4
  %data_type_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 3
  %0 = load i32, i32* %data_type_, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden signext i8 @_ZNK5draco17GeometryAttribute14num_componentsEv(%"class.draco::GeometryAttribute"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::GeometryAttribute"*, align 4
  store %"class.draco::GeometryAttribute"* %this, %"class.draco::GeometryAttribute"** %this.addr, align 4
  %this1 = load %"class.draco::GeometryAttribute"*, %"class.draco::GeometryAttribute"** %this.addr, align 4
  %num_components_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 2
  %0 = load i8, i8* %num_components_, align 8
  ret i8 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK5draco17GeometryAttribute10normalizedEv(%"class.draco::GeometryAttribute"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::GeometryAttribute"*, align 4
  store %"class.draco::GeometryAttribute"* %this, %"class.draco::GeometryAttribute"** %this.addr, align 4
  %this1 = load %"class.draco::GeometryAttribute"*, %"class.draco::GeometryAttribute"** %this.addr, align 4
  %normalized_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 4
  %0 = load i8, i8* %normalized_, align 8
  %tobool = trunc i8 %0 to i1
  ret i1 %tobool
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i64 @_ZNK5draco17GeometryAttribute11byte_strideEv(%"class.draco::GeometryAttribute"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::GeometryAttribute"*, align 4
  store %"class.draco::GeometryAttribute"* %this, %"class.draco::GeometryAttribute"** %this.addr, align 4
  %this1 = load %"class.draco::GeometryAttribute"*, %"class.draco::GeometryAttribute"** %this.addr, align 4
  %byte_stride_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 5
  %0 = load i64, i64* %byte_stride_, align 8
  ret i64 %0
}

; Function Attrs: nobuiltin allocsize(0)
declare nonnull i8* @_Znam(i32) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.127"* @_ZNSt3__210unique_ptrIA_hNS_14default_deleteIS1_EEEC2IPhLb1EvvEET_(%"class.std::__2::unique_ptr.127"* returned %this, i8* %__p) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.127"*, align 4
  %__p.addr = alloca i8*, align 4
  %ref.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::unique_ptr.127"* %this, %"class.std::__2::unique_ptr.127"** %this.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.127"*, %"class.std::__2::unique_ptr.127"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.127", %"class.std::__2::unique_ptr.127"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::__compressed_pair.128"* @_ZNSt3__217__compressed_pairIPhNS_14default_deleteIA_hEEEC2IRS1_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.128"* %__ptr_, i8** nonnull align 4 dereferenceable(4) %__p.addr, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp)
  ret %"class.std::__2::unique_ptr.127"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.114"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEEixEm(%"class.std::__2::vector.112"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.112"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector.112"* %this, %"class.std::__2::vector.112"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.112"*, %"class.std::__2::vector.112"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.112"* %this1 to %"class.std::__2::__vector_base.113"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.113", %"class.std::__2::__vector_base.113"* %0, i32 0, i32 0
  %1 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__begin_, align 4
  %2 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %1, i32 %2
  ret %"class.draco::IndexType.114"* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEEiEixERKS3_(%"class.draco::IndexTypeVector.120"* %this, %"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %index) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector.120"*, align 4
  %index.addr = alloca %"class.draco::IndexType.114"*, align 4
  store %"class.draco::IndexTypeVector.120"* %this, %"class.draco::IndexTypeVector.120"** %this.addr, align 4
  store %"class.draco::IndexType.114"* %index, %"class.draco::IndexType.114"** %index.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector.120"*, %"class.draco::IndexTypeVector.120"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector.120", %"class.draco::IndexTypeVector.120"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %index.addr, align 4
  %call = call i32 @_ZNK5draco9IndexTypeIjNS_19FaceIndex_tag_type_EE5valueEv(%"class.draco::IndexType.114"* %0)
  %call2 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIiNS_9allocatorIiEEEixEm(%"class.std::__2::vector.89"* %vector_, i32 %call) #7
  ret i32* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK5draco17GeometryAttribute8GetValueENS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEEPv(%"class.draco::GeometryAttribute"* %this, i32 %att_index.coerce, i8* %out_data) #0 comdat {
entry:
  %att_index = alloca %"class.draco::IndexType", align 4
  %this.addr = alloca %"class.draco::GeometryAttribute"*, align 4
  %out_data.addr = alloca i8*, align 4
  %byte_pos = alloca i64, align 8
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %att_index, i32 0, i32 0
  store i32 %att_index.coerce, i32* %coerce.dive, align 4
  store %"class.draco::GeometryAttribute"* %this, %"class.draco::GeometryAttribute"** %this.addr, align 4
  store i8* %out_data, i8** %out_data.addr, align 4
  %this1 = load %"class.draco::GeometryAttribute"*, %"class.draco::GeometryAttribute"** %this.addr, align 4
  %byte_offset_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 6
  %0 = load i64, i64* %byte_offset_, align 8
  %byte_stride_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 5
  %1 = load i64, i64* %byte_stride_, align 8
  %call = call i32 @_ZNK5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EE5valueEv(%"class.draco::IndexType"* %att_index)
  %conv = zext i32 %call to i64
  %mul = mul nsw i64 %1, %conv
  %add = add nsw i64 %0, %mul
  store i64 %add, i64* %byte_pos, align 8
  %buffer_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 0
  %2 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %buffer_, align 8
  %3 = load i64, i64* %byte_pos, align 8
  %4 = load i8*, i8** %out_data.addr, align 4
  %byte_stride_2 = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 5
  %5 = load i64, i64* %byte_stride_2, align 8
  %conv3 = trunc i64 %5 to i32
  call void @_ZNK5draco10DataBuffer4ReadExPvm(%"class.draco::DataBuffer"* %2, i64 %3, i8* %4, i32 %conv3)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__210unique_ptrIA_hNS_14default_deleteIS1_EEE3getEv(%"class.std::__2::unique_ptr.127"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.127"*, align 4
  store %"class.std::__2::unique_ptr.127"* %this, %"class.std::__2::unique_ptr.127"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.127"*, %"class.std::__2::unique_ptr.127"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.127", %"class.std::__2::unique_ptr.127"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__217__compressed_pairIPhNS_14default_deleteIA_hEEE5firstEv(%"class.std::__2::__compressed_pair.128"* %__ptr_) #7
  %0 = load i8*, i8** %call, align 4
  ret i8* %0
}

declare i32 @memcmp(i8*, i8*, i32) #1

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.127"* @_ZNSt3__210unique_ptrIA_hNS_14default_deleteIS1_EEED2Ev(%"class.std::__2::unique_ptr.127"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.127"*, align 4
  store %"class.std::__2::unique_ptr.127"* %this, %"class.std::__2::unique_ptr.127"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.127"*, %"class.std::__2::unique_ptr.127"** %this.addr, align 4
  call void @_ZNSt3__210unique_ptrIA_hNS_14default_deleteIS1_EEE5resetEDn(%"class.std::__2::unique_ptr.127"* %this1, i8* null) #7
  ret %"class.std::__2::unique_ptr.127"* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden zeroext i1 @_ZNK5draco17MeshAreEquivalent13FaceIndexLessclENS_9IndexTypeIjNS_19FaceIndex_tag_type_EEES4_(%"struct.draco::MeshAreEquivalent::FaceIndexLess"* %this, i32 %f0.coerce, i32 %f1.coerce) #0 {
entry:
  %retval = alloca i1, align 1
  %f0 = alloca %"class.draco::IndexType.114", align 4
  %f1 = alloca %"class.draco::IndexType.114", align 4
  %this.addr = alloca %"struct.draco::MeshAreEquivalent::FaceIndexLess"*, align 4
  %c0 = alloca i32, align 4
  %c1 = alloca i32, align 4
  %i = alloca i32, align 4
  %vf0 = alloca %"class.draco::VectorD", align 4
  %agg.tmp = alloca %"class.draco::IndexType.114", align 4
  %vf1 = alloca %"class.draco::VectorD", align 4
  %agg.tmp11 = alloca %"class.draco::IndexType.114", align 4
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %f0, i32 0, i32 0
  store i32 %f0.coerce, i32* %coerce.dive, align 4
  %coerce.dive1 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %f1, i32 0, i32 0
  store i32 %f1.coerce, i32* %coerce.dive1, align 4
  store %"struct.draco::MeshAreEquivalent::FaceIndexLess"* %this, %"struct.draco::MeshAreEquivalent::FaceIndexLess"** %this.addr, align 4
  %this2 = load %"struct.draco::MeshAreEquivalent::FaceIndexLess"*, %"struct.draco::MeshAreEquivalent::FaceIndexLess"** %this.addr, align 4
  %call = call zeroext i1 @_ZNK5draco9IndexTypeIjNS_19FaceIndex_tag_type_EEeqERKS2_(%"class.draco::IndexType.114"* %f0, %"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %f1)
  br i1 %call, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %mesh_info = getelementptr inbounds %"struct.draco::MeshAreEquivalent::FaceIndexLess", %"struct.draco::MeshAreEquivalent::FaceIndexLess"* %this2, i32 0, i32 0
  %0 = load %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"** %mesh_info, align 4
  %corner_index_of_smallest_vertex = getelementptr inbounds %"struct.draco::MeshAreEquivalent::MeshInfo", %"struct.draco::MeshAreEquivalent::MeshInfo"* %0, i32 0, i32 2
  %call3 = call nonnull align 4 dereferenceable(4) i32* @_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEEiEixERKS3_(%"class.draco::IndexTypeVector.120"* %corner_index_of_smallest_vertex, %"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %f0)
  %1 = load i32, i32* %call3, align 4
  store i32 %1, i32* %c0, align 4
  %mesh_info4 = getelementptr inbounds %"struct.draco::MeshAreEquivalent::FaceIndexLess", %"struct.draco::MeshAreEquivalent::FaceIndexLess"* %this2, i32 0, i32 0
  %2 = load %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"** %mesh_info4, align 4
  %corner_index_of_smallest_vertex5 = getelementptr inbounds %"struct.draco::MeshAreEquivalent::MeshInfo", %"struct.draco::MeshAreEquivalent::MeshInfo"* %2, i32 0, i32 2
  %call6 = call nonnull align 4 dereferenceable(4) i32* @_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEEiEixERKS3_(%"class.draco::IndexTypeVector.120"* %corner_index_of_smallest_vertex5, %"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %f1)
  %3 = load i32, i32* %call6, align 4
  store i32 %3, i32* %c1, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %4 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %4, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %mesh_info7 = getelementptr inbounds %"struct.draco::MeshAreEquivalent::FaceIndexLess", %"struct.draco::MeshAreEquivalent::FaceIndexLess"* %this2, i32 0, i32 0
  %5 = load %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"** %mesh_info7, align 4
  %mesh = getelementptr inbounds %"struct.draco::MeshAreEquivalent::MeshInfo", %"struct.draco::MeshAreEquivalent::MeshInfo"* %5, i32 0, i32 0
  %6 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %mesh, align 4
  %7 = bitcast %"class.draco::IndexType.114"* %agg.tmp to i8*
  %8 = bitcast %"class.draco::IndexType.114"* %f0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 4, i1 false)
  %9 = load i32, i32* %c0, align 4
  %10 = load i32, i32* %i, align 4
  %add = add nsw i32 %9, %10
  %rem = srem i32 %add, 3
  %coerce.dive8 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %agg.tmp, i32 0, i32 0
  %11 = load i32, i32* %coerce.dive8, align 4
  call void @_ZN5draco17MeshAreEquivalent11GetPositionERKNS_4MeshENS_9IndexTypeIjNS_19FaceIndex_tag_type_EEEi(%"class.draco::VectorD"* sret align 4 %vf0, %"class.draco::Mesh"* nonnull align 4 dereferenceable(108) %6, i32 %11, i32 %rem)
  %mesh_info9 = getelementptr inbounds %"struct.draco::MeshAreEquivalent::FaceIndexLess", %"struct.draco::MeshAreEquivalent::FaceIndexLess"* %this2, i32 0, i32 0
  %12 = load %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"** %mesh_info9, align 4
  %mesh10 = getelementptr inbounds %"struct.draco::MeshAreEquivalent::MeshInfo", %"struct.draco::MeshAreEquivalent::MeshInfo"* %12, i32 0, i32 0
  %13 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %mesh10, align 4
  %14 = bitcast %"class.draco::IndexType.114"* %agg.tmp11 to i8*
  %15 = bitcast %"class.draco::IndexType.114"* %f1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %14, i8* align 4 %15, i32 4, i1 false)
  %16 = load i32, i32* %c1, align 4
  %17 = load i32, i32* %i, align 4
  %add12 = add nsw i32 %16, %17
  %rem13 = srem i32 %add12, 3
  %coerce.dive14 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %agg.tmp11, i32 0, i32 0
  %18 = load i32, i32* %coerce.dive14, align 4
  call void @_ZN5draco17MeshAreEquivalent11GetPositionERKNS_4MeshENS_9IndexTypeIjNS_19FaceIndex_tag_type_EEEi(%"class.draco::VectorD"* sret align 4 %vf1, %"class.draco::Mesh"* nonnull align 4 dereferenceable(108) %13, i32 %18, i32 %rem13)
  %call15 = call zeroext i1 @_ZNK5draco7VectorDIfLi3EEltERKS1_(%"class.draco::VectorD"* %vf0, %"class.draco::VectorD"* nonnull align 4 dereferenceable(12) %vf1)
  br i1 %call15, label %if.then16, label %if.end17

if.then16:                                        ; preds = %for.body
  store i1 true, i1* %retval, align 1
  br label %return

if.end17:                                         ; preds = %for.body
  %call18 = call zeroext i1 @_ZNK5draco7VectorDIfLi3EEltERKS1_(%"class.draco::VectorD"* %vf1, %"class.draco::VectorD"* nonnull align 4 dereferenceable(12) %vf0)
  br i1 %call18, label %if.then19, label %if.end20

if.then19:                                        ; preds = %if.end17
  store i1 false, i1* %retval, align 1
  br label %return

if.end20:                                         ; preds = %if.end17
  br label %for.inc

for.inc:                                          ; preds = %if.end20
  %19 = load i32, i32* %i, align 4
  %inc = add nsw i32 %19, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i1 false, i1* %retval, align 1
  br label %return

return:                                           ; preds = %for.end, %if.then19, %if.then16, %if.then
  %20 = load i1, i1* %retval, align 1
  ret i1 %20
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK5draco9IndexTypeIjNS_19FaceIndex_tag_type_EEeqERKS2_(%"class.draco::IndexType.114"* %this, %"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %i) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.114"*, align 4
  %i.addr = alloca %"class.draco::IndexType.114"*, align 4
  store %"class.draco::IndexType.114"* %this, %"class.draco::IndexType.114"** %this.addr, align 4
  store %"class.draco::IndexType.114"* %i, %"class.draco::IndexType.114"** %i.addr, align 4
  %this1 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value_, align 4
  %1 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %i.addr, align 4
  %value_2 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %1, i32 0, i32 0
  %2 = load i32, i32* %value_2, align 4
  %cmp = icmp eq i32 %0, %2
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEEiEixERKS3_(%"class.draco::IndexTypeVector.120"* %this, %"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %index) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector.120"*, align 4
  %index.addr = alloca %"class.draco::IndexType.114"*, align 4
  store %"class.draco::IndexTypeVector.120"* %this, %"class.draco::IndexTypeVector.120"** %this.addr, align 4
  store %"class.draco::IndexType.114"* %index, %"class.draco::IndexType.114"** %index.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector.120"*, %"class.draco::IndexTypeVector.120"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector.120", %"class.draco::IndexTypeVector.120"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %index.addr, align 4
  %call = call i32 @_ZNK5draco9IndexTypeIjNS_19FaceIndex_tag_type_EE5valueEv(%"class.draco::IndexType.114"* %0)
  %call2 = call nonnull align 4 dereferenceable(4) i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEEixEm(%"class.std::__2::vector.89"* %vector_, i32 %call) #7
  ret i32* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK5draco7VectorDIfLi3EEltERKS1_(%"class.draco::VectorD"* %this, %"class.draco::VectorD"* nonnull align 4 dereferenceable(12) %x) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::VectorD"*, align 4
  %x.addr = alloca %"class.draco::VectorD"*, align 4
  %i = alloca i32, align 4
  store %"class.draco::VectorD"* %this, %"class.draco::VectorD"** %this.addr, align 4
  store %"class.draco::VectorD"* %x, %"class.draco::VectorD"** %x.addr, align 4
  %this1 = load %"class.draco::VectorD"*, %"class.draco::VectorD"** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %v_ = getelementptr inbounds %"class.draco::VectorD", %"class.draco::VectorD"* %this1, i32 0, i32 0
  %1 = load i32, i32* %i, align 4
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNKSt3__25arrayIfLm3EEixEm(%"struct.std::__2::array.126"* %v_, i32 %1) #7
  %2 = load float, float* %call, align 4
  %3 = load %"class.draco::VectorD"*, %"class.draco::VectorD"** %x.addr, align 4
  %v_2 = getelementptr inbounds %"class.draco::VectorD", %"class.draco::VectorD"* %3, i32 0, i32 0
  %4 = load i32, i32* %i, align 4
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNKSt3__25arrayIfLm3EEixEm(%"struct.std::__2::array.126"* %v_2, i32 %4) #7
  %5 = load float, float* %call3, align 4
  %cmp4 = fcmp olt float %2, %5
  br i1 %cmp4, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  store i1 true, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %for.body
  %v_5 = getelementptr inbounds %"class.draco::VectorD", %"class.draco::VectorD"* %this1, i32 0, i32 0
  %6 = load i32, i32* %i, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNKSt3__25arrayIfLm3EEixEm(%"struct.std::__2::array.126"* %v_5, i32 %6) #7
  %7 = load float, float* %call6, align 4
  %8 = load %"class.draco::VectorD"*, %"class.draco::VectorD"** %x.addr, align 4
  %v_7 = getelementptr inbounds %"class.draco::VectorD", %"class.draco::VectorD"* %8, i32 0, i32 0
  %9 = load i32, i32* %i, align 4
  %call8 = call nonnull align 4 dereferenceable(4) float* @_ZNKSt3__25arrayIfLm3EEixEm(%"struct.std::__2::array.126"* %v_7, i32 %9) #7
  %10 = load float, float* %call8, align 4
  %cmp9 = fcmp ogt float %7, %10
  br i1 %cmp9, label %if.then10, label %if.end11

if.then10:                                        ; preds = %if.end
  store i1 false, i1* %retval, align 1
  br label %return

if.end11:                                         ; preds = %if.end
  br label %for.inc

for.inc:                                          ; preds = %if.end11
  %11 = load i32, i32* %i, align 4
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %v_12 = getelementptr inbounds %"class.draco::VectorD", %"class.draco::VectorD"* %this1, i32 0, i32 0
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNKSt3__25arrayIfLm3EEixEm(%"struct.std::__2::array.126"* %v_12, i32 2) #7
  %12 = load float, float* %call13, align 4
  %13 = load %"class.draco::VectorD"*, %"class.draco::VectorD"** %x.addr, align 4
  %v_14 = getelementptr inbounds %"class.draco::VectorD", %"class.draco::VectorD"* %13, i32 0, i32 0
  %call15 = call nonnull align 4 dereferenceable(4) float* @_ZNKSt3__25arrayIfLm3EEixEm(%"struct.std::__2::array.126"* %v_14, i32 2) #7
  %14 = load float, float* %call15, align 4
  %cmp16 = fcmp olt float %12, %14
  br i1 %cmp16, label %if.then17, label %if.end18

if.then17:                                        ; preds = %for.end
  store i1 true, i1* %retval, align 1
  br label %return

if.end18:                                         ; preds = %for.end
  store i1 false, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end18, %if.then17, %if.then10, %if.then
  %15 = load i1, i1* %retval, align 1
  ret i1 %15
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"struct.std::__2::array"* @_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEENSt3__25arrayINS1_IjNS_20PointIndex_tag_type_EEELm3EEEEixERKS3_(%"class.draco::IndexTypeVector.103"* %this, %"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %index) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector.103"*, align 4
  %index.addr = alloca %"class.draco::IndexType.114"*, align 4
  store %"class.draco::IndexTypeVector.103"* %this, %"class.draco::IndexTypeVector.103"** %this.addr, align 4
  store %"class.draco::IndexType.114"* %index, %"class.draco::IndexType.114"** %index.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector.103"*, %"class.draco::IndexTypeVector.103"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector.103", %"class.draco::IndexTypeVector.103"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %index.addr, align 4
  %call = call i32 @_ZNK5draco9IndexTypeIjNS_19FaceIndex_tag_type_EE5valueEv(%"class.draco::IndexType.114"* %0)
  %call2 = call nonnull align 4 dereferenceable(12) %"struct.std::__2::array"* @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEEixEm(%"class.std::__2::vector.104"* %vector_, i32 %call) #7
  ret %"struct.std::__2::array"* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"struct.std::__2::array"* @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEEixEm(%"class.std::__2::vector.104"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.104"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector.104"* %this, %"class.std::__2::vector.104"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.104"*, %"class.std::__2::vector.104"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.104"* %this1 to %"class.std::__2::__vector_base.105"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.105", %"class.std::__2::__vector_base.105"* %0, i32 0, i32 0
  %1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__begin_, align 4
  %2 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %1, i32 %2
  ret %"struct.std::__2::array"* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco9IndexTypeIjNS_20PointIndex_tag_type_EE5valueEv(%"class.draco::IndexType.106"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.106"*, align 4
  store %"class.draco::IndexType.106"* %this, %"class.draco::IndexType.106"** %this.addr, align 4
  %this1 = load %"class.draco::IndexType.106"*, %"class.draco::IndexType.106"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.106", %"class.draco::IndexType.106"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value_, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType"* @_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEC2Ej(%"class.draco::IndexType"* returned %this, i32 %value) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType"*, align 4
  %value.addr = alloca i32, align 4
  store %"class.draco::IndexType"* %this, %"class.draco::IndexType"** %this.addr, align 4
  store i32 %value, i32* %value.addr, align 4
  %this1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value.addr, align 4
  store i32 %0, i32* %value_, align 4
  ret %"class.draco::IndexType"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEEEixERKS3_(%"class.draco::IndexTypeVector"* %this, %"class.draco::IndexType.106"* nonnull align 4 dereferenceable(4) %index) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector"*, align 4
  %index.addr = alloca %"class.draco::IndexType.106"*, align 4
  store %"class.draco::IndexTypeVector"* %this, %"class.draco::IndexTypeVector"** %this.addr, align 4
  store %"class.draco::IndexType.106"* %index, %"class.draco::IndexType.106"** %index.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector"*, %"class.draco::IndexTypeVector"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector", %"class.draco::IndexTypeVector"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexType.106"*, %"class.draco::IndexType.106"** %index.addr, align 4
  %call = call i32 @_ZNK5draco9IndexTypeIjNS_20PointIndex_tag_type_EE5valueEv(%"class.draco::IndexType.106"* %0)
  %call2 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEEixEm(%"class.std::__2::vector.70"* %vector_, i32 %call) #7
  ret %"class.draco::IndexType"* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEEixEm(%"class.std::__2::vector.70"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.70"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector.70"* %this, %"class.std::__2::vector.70"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.70"*, %"class.std::__2::vector.70"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.70"* %this1 to %"class.std::__2::__vector_base.71"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.71", %"class.std::__2::__vector_base.71"* %0, i32 0, i32 0
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  %2 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %1, i32 %2
  ret %"class.draco::IndexType"* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::VectorD"* @_ZNSt3__211min_elementIPN5draco7VectorDIfLi3EEENS_6__lessIS3_S3_EEEET_S7_S7_T0_(%"class.draco::VectorD"* %__first, %"class.draco::VectorD"* %__last) #0 comdat {
entry:
  %__comp = alloca %"struct.std::__2::__less", align 1
  %__first.addr = alloca %"class.draco::VectorD"*, align 4
  %__last.addr = alloca %"class.draco::VectorD"*, align 4
  %__i = alloca %"class.draco::VectorD"*, align 4
  store %"class.draco::VectorD"* %__first, %"class.draco::VectorD"** %__first.addr, align 4
  store %"class.draco::VectorD"* %__last, %"class.draco::VectorD"** %__last.addr, align 4
  %0 = load %"class.draco::VectorD"*, %"class.draco::VectorD"** %__first.addr, align 4
  %1 = load %"class.draco::VectorD"*, %"class.draco::VectorD"** %__last.addr, align 4
  %cmp = icmp ne %"class.draco::VectorD"* %0, %1
  br i1 %cmp, label %if.then, label %if.end3

if.then:                                          ; preds = %entry
  %2 = load %"class.draco::VectorD"*, %"class.draco::VectorD"** %__first.addr, align 4
  store %"class.draco::VectorD"* %2, %"class.draco::VectorD"** %__i, align 4
  br label %while.cond

while.cond:                                       ; preds = %if.end, %if.then
  %3 = load %"class.draco::VectorD"*, %"class.draco::VectorD"** %__i, align 4
  %incdec.ptr = getelementptr inbounds %"class.draco::VectorD", %"class.draco::VectorD"* %3, i32 1
  store %"class.draco::VectorD"* %incdec.ptr, %"class.draco::VectorD"** %__i, align 4
  %4 = load %"class.draco::VectorD"*, %"class.draco::VectorD"** %__last.addr, align 4
  %cmp1 = icmp ne %"class.draco::VectorD"* %incdec.ptr, %4
  br i1 %cmp1, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %5 = load %"class.draco::VectorD"*, %"class.draco::VectorD"** %__i, align 4
  %6 = load %"class.draco::VectorD"*, %"class.draco::VectorD"** %__first.addr, align 4
  %call = call zeroext i1 @_ZNKSt3__26__lessIN5draco7VectorDIfLi3EEES3_EclERKS3_S6_(%"struct.std::__2::__less"* %__comp, %"class.draco::VectorD"* nonnull align 4 dereferenceable(12) %5, %"class.draco::VectorD"* nonnull align 4 dereferenceable(12) %6)
  br i1 %call, label %if.then2, label %if.end

if.then2:                                         ; preds = %while.body
  %7 = load %"class.draco::VectorD"*, %"class.draco::VectorD"** %__i, align 4
  store %"class.draco::VectorD"* %7, %"class.draco::VectorD"** %__first.addr, align 4
  br label %if.end

if.end:                                           ; preds = %if.then2, %while.body
  br label %while.cond

while.end:                                        ; preds = %while.cond
  br label %if.end3

if.end3:                                          ; preds = %while.end, %entry
  %8 = load %"class.draco::VectorD"*, %"class.draco::VectorD"** %__first.addr, align 4
  ret %"class.draco::VectorD"* %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__26__lessIN5draco7VectorDIfLi3EEES3_EclERKS3_S6_(%"struct.std::__2::__less"* %this, %"class.draco::VectorD"* nonnull align 4 dereferenceable(12) %__x, %"class.draco::VectorD"* nonnull align 4 dereferenceable(12) %__y) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__less"*, align 4
  %__x.addr = alloca %"class.draco::VectorD"*, align 4
  %__y.addr = alloca %"class.draco::VectorD"*, align 4
  store %"struct.std::__2::__less"* %this, %"struct.std::__2::__less"** %this.addr, align 4
  store %"class.draco::VectorD"* %__x, %"class.draco::VectorD"** %__x.addr, align 4
  store %"class.draco::VectorD"* %__y, %"class.draco::VectorD"** %__y.addr, align 4
  %this1 = load %"struct.std::__2::__less"*, %"struct.std::__2::__less"** %this.addr, align 4
  %0 = load %"class.draco::VectorD"*, %"class.draco::VectorD"** %__x.addr, align 4
  %1 = load %"class.draco::VectorD"*, %"class.draco::VectorD"** %__y.addr, align 4
  %call = call zeroext i1 @_ZNK5draco7VectorDIfLi3EEltERKS1_(%"class.draco::VectorD"* %0, %"class.draco::VectorD"* nonnull align 4 dereferenceable(12) %1)
  ret i1 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEENSt3__25arrayINS1_IjNS_20PointIndex_tag_type_EEELm3EEEE4sizeEv(%"class.draco::IndexTypeVector.103"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector.103"*, align 4
  store %"class.draco::IndexTypeVector.103"* %this, %"class.draco::IndexTypeVector.103"** %this.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector.103"*, %"class.draco::IndexTypeVector.103"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector.103", %"class.draco::IndexTypeVector.103"* %this1, i32 0, i32 0
  %call = call i32 @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.104"* %vector_) #7
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.104"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.104"*, align 4
  store %"class.std::__2::vector.104"* %this, %"class.std::__2::vector.104"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.104"*, %"class.std::__2::vector.104"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.104"* %this1 to %"class.std::__2::__vector_base.105"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.105", %"class.std::__2::__vector_base.105"* %0, i32 0, i32 1
  %1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector.104"* %this1 to %"class.std::__2::__vector_base.105"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.105", %"class.std::__2::__vector_base.105"* %2, i32 0, i32 0
  %3 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"struct.std::__2::array"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"struct.std::__2::array"* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 12
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector.112"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEEC2Ev(%"class.std::__2::vector.112"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.112"*, align 4
  store %"class.std::__2::vector.112"* %this, %"class.std::__2::vector.112"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.112"*, %"class.std::__2::vector.112"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.112"* %this1 to %"class.std::__2::__vector_base.113"*
  %call = call %"class.std::__2::__vector_base.113"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEEC2Ev(%"class.std::__2::__vector_base.113"* %0) #7
  ret %"class.std::__2::vector.112"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexTypeVector.120"* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEEiEC2Ev(%"class.draco::IndexTypeVector.120"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector.120"*, align 4
  store %"class.draco::IndexTypeVector.120"* %this, %"class.draco::IndexTypeVector.120"** %this.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector.120"*, %"class.draco::IndexTypeVector.120"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector.120", %"class.draco::IndexTypeVector.120"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::vector.89"* @_ZNSt3__26vectorIiNS_9allocatorIiEEEC2Ev(%"class.std::__2::vector.89"* %vector_) #7
  ret %"class.draco::IndexTypeVector.120"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base.113"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEEC2Ev(%"class.std::__2::__vector_base.113"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.113"*, align 4
  %ref.tmp = alloca i8*, align 4
  %ref.tmp2 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__vector_base.113"* %this, %"class.std::__2::__vector_base.113"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.113"*, %"class.std::__2::__vector_base.113"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__vector_base.113"* %this1 to %"class.std::__2::__vector_base_common"*
  %call = call %"class.std::__2::__vector_base_common"* @_ZNSt3__220__vector_base_commonILb1EEC2Ev(%"class.std::__2::__vector_base_common"* %0)
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.113", %"class.std::__2::__vector_base.113"* %this1, i32 0, i32 0
  store %"class.draco::IndexType.114"* null, %"class.draco::IndexType.114"** %__begin_, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.113", %"class.std::__2::__vector_base.113"* %this1, i32 0, i32 1
  store %"class.draco::IndexType.114"* null, %"class.draco::IndexType.114"** %__end_, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.113", %"class.std::__2::__vector_base.113"* %this1, i32 0, i32 2
  store i8* null, i8** %ref.tmp, align 4
  %call3 = call %"class.std::__2::__compressed_pair.115"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEEC2IDnNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.115"* %__end_cap_, i8** nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  ret %"class.std::__2::__vector_base.113"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base_common"* @_ZNSt3__220__vector_base_commonILb1EEC2Ev(%"class.std::__2::__vector_base_common"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base_common"*, align 4
  store %"class.std::__2::__vector_base_common"* %this, %"class.std::__2::__vector_base_common"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base_common"*, %"class.std::__2::__vector_base_common"** %this.addr, align 4
  ret %"class.std::__2::__vector_base_common"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.115"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEEC2IDnNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.115"* returned %this, i8** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.115"*, align 4
  %__t1.addr = alloca i8**, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.115"* %this, %"class.std::__2::__compressed_pair.115"** %this.addr, align 4
  store i8** %__t1, i8*** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.115"*, %"class.std::__2::__compressed_pair.115"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.115"* %this1 to %"struct.std::__2::__compressed_pair_elem.116"*
  %1 = load i8**, i8*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %1) #7
  %call2 = call %"struct.std::__2::__compressed_pair_elem.116"* @_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEELi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.116"* %0, i8** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.115"* %this1 to %"struct.std::__2::__compressed_pair_elem.117"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #7
  %call4 = call %"struct.std::__2::__compressed_pair_elem.117"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.117"* %2)
  ret %"class.std::__2::__compressed_pair.115"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca i8**, align 4
  store i8** %__t, i8*** %__t.addr, align 4
  %0 = load i8**, i8*** %__t.addr, align 4
  ret i8** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.116"* @_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEELi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.116"* returned %this, i8** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.116"*, align 4
  %__u.addr = alloca i8**, align 4
  store %"struct.std::__2::__compressed_pair_elem.116"* %this, %"struct.std::__2::__compressed_pair_elem.116"** %this.addr, align 4
  store i8** %__u, i8*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.116"*, %"struct.std::__2::__compressed_pair_elem.116"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.116", %"struct.std::__2::__compressed_pair_elem.116"* %this1, i32 0, i32 0
  %0 = load i8**, i8*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %0) #7
  store %"class.draco::IndexType.114"* null, %"class.draco::IndexType.114"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.116"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  store %"struct.std::__2::__default_init_tag"* %__t, %"struct.std::__2::__default_init_tag"** %__t.addr, align 4
  %0 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t.addr, align 4
  ret %"struct.std::__2::__default_init_tag"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.117"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.117"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.117"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.117"* %this, %"struct.std::__2::__compressed_pair_elem.117"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.117"*, %"struct.std::__2::__compressed_pair_elem.117"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.117"* %this1 to %"class.std::__2::allocator.118"*
  %call = call %"class.std::__2::allocator.118"* @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEC2Ev(%"class.std::__2::allocator.118"* %1) #7
  ret %"struct.std::__2::__compressed_pair_elem.117"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::allocator.118"* @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEC2Ev(%"class.std::__2::allocator.118"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.118"*, align 4
  store %"class.std::__2::allocator.118"* %this, %"class.std::__2::allocator.118"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.118"*, %"class.std::__2::allocator.118"** %this.addr, align 4
  ret %"class.std::__2::allocator.118"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector.89"* @_ZNSt3__26vectorIiNS_9allocatorIiEEEC2Ev(%"class.std::__2::vector.89"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.89"*, align 4
  store %"class.std::__2::vector.89"* %this, %"class.std::__2::vector.89"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.89"*, %"class.std::__2::vector.89"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.89"* %this1 to %"class.std::__2::__vector_base.90"*
  %call = call %"class.std::__2::__vector_base.90"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEEC2Ev(%"class.std::__2::__vector_base.90"* %0) #7
  ret %"class.std::__2::vector.89"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base.90"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEEC2Ev(%"class.std::__2::__vector_base.90"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.90"*, align 4
  %ref.tmp = alloca i8*, align 4
  %ref.tmp2 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__vector_base.90"* %this, %"class.std::__2::__vector_base.90"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.90"*, %"class.std::__2::__vector_base.90"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__vector_base.90"* %this1 to %"class.std::__2::__vector_base_common"*
  %call = call %"class.std::__2::__vector_base_common"* @_ZNSt3__220__vector_base_commonILb1EEC2Ev(%"class.std::__2::__vector_base_common"* %0)
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.90", %"class.std::__2::__vector_base.90"* %this1, i32 0, i32 0
  store i32* null, i32** %__begin_, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.90", %"class.std::__2::__vector_base.90"* %this1, i32 0, i32 1
  store i32* null, i32** %__end_, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.90", %"class.std::__2::__vector_base.90"* %this1, i32 0, i32 2
  store i8* null, i8** %ref.tmp, align 4
  %call3 = call %"class.std::__2::__compressed_pair.91"* @_ZNSt3__217__compressed_pairIPiNS_9allocatorIiEEEC2IDnNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.91"* %__end_cap_, i8** nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  ret %"class.std::__2::__vector_base.90"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.91"* @_ZNSt3__217__compressed_pairIPiNS_9allocatorIiEEEC2IDnNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.91"* returned %this, i8** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.91"*, align 4
  %__t1.addr = alloca i8**, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.91"* %this, %"class.std::__2::__compressed_pair.91"** %this.addr, align 4
  store i8** %__t1, i8*** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.91"*, %"class.std::__2::__compressed_pair.91"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.91"* %this1 to %"struct.std::__2::__compressed_pair_elem.92"*
  %1 = load i8**, i8*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %1) #7
  %call2 = call %"struct.std::__2::__compressed_pair_elem.92"* @_ZNSt3__222__compressed_pair_elemIPiLi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.92"* %0, i8** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.91"* %this1 to %"struct.std::__2::__compressed_pair_elem.93"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #7
  %call4 = call %"struct.std::__2::__compressed_pair_elem.93"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIiEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.93"* %2)
  ret %"class.std::__2::__compressed_pair.91"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.92"* @_ZNSt3__222__compressed_pair_elemIPiLi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.92"* returned %this, i8** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.92"*, align 4
  %__u.addr = alloca i8**, align 4
  store %"struct.std::__2::__compressed_pair_elem.92"* %this, %"struct.std::__2::__compressed_pair_elem.92"** %this.addr, align 4
  store i8** %__u, i8*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.92"*, %"struct.std::__2::__compressed_pair_elem.92"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.92", %"struct.std::__2::__compressed_pair_elem.92"* %this1, i32 0, i32 0
  %0 = load i8**, i8*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %0) #7
  store i32* null, i32** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.92"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.93"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIiEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.93"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.93"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.93"* %this, %"struct.std::__2::__compressed_pair_elem.93"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.93"*, %"struct.std::__2::__compressed_pair_elem.93"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.93"* %this1 to %"class.std::__2::allocator.94"*
  %call = call %"class.std::__2::allocator.94"* @_ZNSt3__29allocatorIiEC2Ev(%"class.std::__2::allocator.94"* %1) #7
  ret %"struct.std::__2::__compressed_pair_elem.93"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::allocator.94"* @_ZNSt3__29allocatorIiEC2Ev(%"class.std::__2::allocator.94"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.94"*, align 4
  store %"class.std::__2::allocator.94"* %this, %"class.std::__2::allocator.94"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.94"*, %"class.std::__2::allocator.94"** %this.addr, align 4
  ret %"class.std::__2::allocator.94"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexTypeVector.120"* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEEiED2Ev(%"class.draco::IndexTypeVector.120"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector.120"*, align 4
  store %"class.draco::IndexTypeVector.120"* %this, %"class.draco::IndexTypeVector.120"** %this.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector.120"*, %"class.draco::IndexTypeVector.120"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector.120", %"class.draco::IndexTypeVector.120"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::vector.89"* @_ZNSt3__26vectorIiNS_9allocatorIiEEED2Ev(%"class.std::__2::vector.89"* %vector_) #7
  ret %"class.draco::IndexTypeVector.120"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector.112"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEED2Ev(%"class.std::__2::vector.112"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.112"*, align 4
  store %"class.std::__2::vector.112"* %this, %"class.std::__2::vector.112"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.112"*, %"class.std::__2::vector.112"** %this.addr, align 4
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_deleteEv(%"class.std::__2::vector.112"* %this1) #7
  %0 = bitcast %"class.std::__2::vector.112"* %this1 to %"class.std::__2::__vector_base.113"*
  %call = call %"class.std::__2::__vector_base.113"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEED2Ev(%"class.std::__2::__vector_base.113"* %0) #7
  ret %"class.std::__2::vector.112"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector.89"* @_ZNSt3__26vectorIiNS_9allocatorIiEEED2Ev(%"class.std::__2::vector.89"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.89"*, align 4
  store %"class.std::__2::vector.89"* %this, %"class.std::__2::vector.89"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.89"*, %"class.std::__2::vector.89"** %this.addr, align 4
  call void @_ZNKSt3__26vectorIiNS_9allocatorIiEEE17__annotate_deleteEv(%"class.std::__2::vector.89"* %this1) #7
  %0 = bitcast %"class.std::__2::vector.89"* %this1 to %"class.std::__2::__vector_base.90"*
  %call = call %"class.std::__2::__vector_base.90"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEED2Ev(%"class.std::__2::__vector_base.90"* %0) #7
  ret %"class.std::__2::vector.89"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIiNS_9allocatorIiEEE17__annotate_deleteEv(%"class.std::__2::vector.89"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.89"*, align 4
  store %"class.std::__2::vector.89"* %this, %"class.std::__2::vector.89"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.89"*, %"class.std::__2::vector.89"** %this.addr, align 4
  %call = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector.89"* %this1) #7
  %0 = bitcast i32* %call to i8*
  %call2 = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector.89"* %this1) #7
  %call3 = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::vector.89"* %this1) #7
  %add.ptr = getelementptr inbounds i32, i32* %call2, i32 %call3
  %1 = bitcast i32* %add.ptr to i8*
  %call4 = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector.89"* %this1) #7
  %call5 = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4sizeEv(%"class.std::__2::vector.89"* %this1) #7
  %add.ptr6 = getelementptr inbounds i32, i32* %call4, i32 %call5
  %2 = bitcast i32* %add.ptr6 to i8*
  %call7 = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector.89"* %this1) #7
  %call8 = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::vector.89"* %this1) #7
  %add.ptr9 = getelementptr inbounds i32, i32* %call7, i32 %call8
  %3 = bitcast i32* %add.ptr9 to i8*
  call void @_ZNKSt3__26vectorIiNS_9allocatorIiEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector.89"* %this1, i8* %0, i8* %1, i8* %2, i8* %3) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base.90"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEED2Ev(%"class.std::__2::__vector_base.90"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__vector_base.90"*, align 4
  %this.addr = alloca %"class.std::__2::__vector_base.90"*, align 4
  store %"class.std::__2::__vector_base.90"* %this, %"class.std::__2::__vector_base.90"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.90"*, %"class.std::__2::__vector_base.90"** %this.addr, align 4
  store %"class.std::__2::__vector_base.90"* %this1, %"class.std::__2::__vector_base.90"** %retval, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.90", %"class.std::__2::__vector_base.90"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__begin_, align 4
  %cmp = icmp ne i32* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE5clearEv(%"class.std::__2::__vector_base.90"* %this1) #7
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.94"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE7__allocEv(%"class.std::__2::__vector_base.90"* %this1) #7
  %__begin_2 = getelementptr inbounds %"class.std::__2::__vector_base.90", %"class.std::__2::__vector_base.90"* %this1, i32 0, i32 0
  %1 = load i32*, i32** %__begin_2, align 4
  %call3 = call i32 @_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::__vector_base.90"* %this1) #7
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE10deallocateERS2_Pim(%"class.std::__2::allocator.94"* nonnull align 1 dereferenceable(1) %call, i32* %1, i32 %call3) #7
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"class.std::__2::__vector_base.90"*, %"class.std::__2::__vector_base.90"** %retval, align 4
  ret %"class.std::__2::__vector_base.90"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIiNS_9allocatorIiEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector.89"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.89"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector.89"* %this, %"class.std::__2::vector.89"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector.89"*, %"class.std::__2::vector.89"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector.89"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.89"*, align 4
  store %"class.std::__2::vector.89"* %this, %"class.std::__2::vector.89"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.89"*, %"class.std::__2::vector.89"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.89"* %this1 to %"class.std::__2::__vector_base.90"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.90", %"class.std::__2::__vector_base.90"* %0, i32 0, i32 0
  %1 = load i32*, i32** %__begin_, align 4
  %call = call i32* @_ZNSt3__212__to_addressIiEEPT_S2_(i32* %1) #7
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::vector.89"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.89"*, align 4
  store %"class.std::__2::vector.89"* %this, %"class.std::__2::vector.89"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.89"*, %"class.std::__2::vector.89"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.89"* %this1 to %"class.std::__2::__vector_base.90"*
  %call = call i32 @_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::__vector_base.90"* %0) #7
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4sizeEv(%"class.std::__2::vector.89"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.89"*, align 4
  store %"class.std::__2::vector.89"* %this, %"class.std::__2::vector.89"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.89"*, %"class.std::__2::vector.89"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.89"* %this1 to %"class.std::__2::__vector_base.90"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.90", %"class.std::__2::__vector_base.90"* %0, i32 0, i32 1
  %1 = load i32*, i32** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector.89"* %this1 to %"class.std::__2::__vector_base.90"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.90", %"class.std::__2::__vector_base.90"* %2, i32 0, i32 0
  %3 = load i32*, i32** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint i32* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint i32* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNSt3__212__to_addressIiEEPT_S2_(i32* %__p) #0 comdat {
entry:
  %__p.addr = alloca i32*, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %0 = load i32*, i32** %__p.addr, align 4
  ret i32* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::__vector_base.90"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.90"*, align 4
  store %"class.std::__2::__vector_base.90"* %this, %"class.std::__2::__vector_base.90"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.90"*, %"class.std::__2::__vector_base.90"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE9__end_capEv(%"class.std::__2::__vector_base.90"* %this1) #7
  %0 = load i32*, i32** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.90", %"class.std::__2::__vector_base.90"* %this1, i32 0, i32 0
  %1 = load i32*, i32** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint i32* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint i32* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE9__end_capEv(%"class.std::__2::__vector_base.90"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.90"*, align 4
  store %"class.std::__2::__vector_base.90"* %this, %"class.std::__2::__vector_base.90"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.90"*, %"class.std::__2::__vector_base.90"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.90", %"class.std::__2::__vector_base.90"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__217__compressed_pairIPiNS_9allocatorIiEEE5firstEv(%"class.std::__2::__compressed_pair.91"* %__end_cap_) #7
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__217__compressed_pairIPiNS_9allocatorIiEEE5firstEv(%"class.std::__2::__compressed_pair.91"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.91"*, align 4
  store %"class.std::__2::__compressed_pair.91"* %this, %"class.std::__2::__compressed_pair.91"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.91"*, %"class.std::__2::__compressed_pair.91"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.91"* %this1 to %"struct.std::__2::__compressed_pair_elem.92"*
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__222__compressed_pair_elemIPiLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.92"* %0) #7
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__222__compressed_pair_elemIPiLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.92"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.92"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.92"* %this, %"struct.std::__2::__compressed_pair_elem.92"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.92"*, %"struct.std::__2::__compressed_pair_elem.92"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.92", %"struct.std::__2::__compressed_pair_elem.92"* %this1, i32 0, i32 0
  ret i32** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE5clearEv(%"class.std::__2::__vector_base.90"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.90"*, align 4
  store %"class.std::__2::__vector_base.90"* %this, %"class.std::__2::__vector_base.90"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.90"*, %"class.std::__2::__vector_base.90"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.90", %"class.std::__2::__vector_base.90"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__begin_, align 4
  call void @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE17__destruct_at_endEPi(%"class.std::__2::__vector_base.90"* %this1, i32* %0) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE10deallocateERS2_Pim(%"class.std::__2::allocator.94"* nonnull align 1 dereferenceable(1) %__a, i32* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.94"*, align 4
  %__p.addr = alloca i32*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.94"* %__a, %"class.std::__2::allocator.94"** %__a.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.94"*, %"class.std::__2::allocator.94"** %__a.addr, align 4
  %1 = load i32*, i32** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorIiE10deallocateEPim(%"class.std::__2::allocator.94"* %0, i32* %1, i32 %2) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.94"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE7__allocEv(%"class.std::__2::__vector_base.90"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.90"*, align 4
  store %"class.std::__2::__vector_base.90"* %this, %"class.std::__2::__vector_base.90"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.90"*, %"class.std::__2::__vector_base.90"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.90", %"class.std::__2::__vector_base.90"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.94"* @_ZNSt3__217__compressed_pairIPiNS_9allocatorIiEEE6secondEv(%"class.std::__2::__compressed_pair.91"* %__end_cap_) #7
  ret %"class.std::__2::allocator.94"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE17__destruct_at_endEPi(%"class.std::__2::__vector_base.90"* %this, i32* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.90"*, align 4
  %__new_last.addr = alloca i32*, align 4
  %__soon_to_be_end = alloca i32*, align 4
  store %"class.std::__2::__vector_base.90"* %this, %"class.std::__2::__vector_base.90"** %this.addr, align 4
  store i32* %__new_last, i32** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.90"*, %"class.std::__2::__vector_base.90"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.90", %"class.std::__2::__vector_base.90"* %this1, i32 0, i32 1
  %0 = load i32*, i32** %__end_, align 4
  store i32* %0, i32** %__soon_to_be_end, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load i32*, i32** %__new_last.addr, align 4
  %2 = load i32*, i32** %__soon_to_be_end, align 4
  %cmp = icmp ne i32* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.94"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE7__allocEv(%"class.std::__2::__vector_base.90"* %this1) #7
  %3 = load i32*, i32** %__soon_to_be_end, align 4
  %incdec.ptr = getelementptr inbounds i32, i32* %3, i32 -1
  store i32* %incdec.ptr, i32** %__soon_to_be_end, align 4
  %call2 = call i32* @_ZNSt3__212__to_addressIiEEPT_S2_(i32* %incdec.ptr) #7
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE7destroyIiEEvRS2_PT_(%"class.std::__2::allocator.94"* nonnull align 1 dereferenceable(1) %call, i32* %call2)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %4 = load i32*, i32** %__new_last.addr, align 4
  %__end_3 = getelementptr inbounds %"class.std::__2::__vector_base.90", %"class.std::__2::__vector_base.90"* %this1, i32 0, i32 1
  store i32* %4, i32** %__end_3, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE7destroyIiEEvRS2_PT_(%"class.std::__2::allocator.94"* nonnull align 1 dereferenceable(1) %__a, i32* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.94"*, align 4
  %__p.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy", align 1
  store %"class.std::__2::allocator.94"* %__a, %"class.std::__2::allocator.94"** %__a.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.94"*, %"class.std::__2::allocator.94"** %__a.addr, align 4
  %2 = load i32*, i32** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE9__destroyIiEEvNS_17integral_constantIbLb1EEERS2_PT_(%"class.std::__2::allocator.94"* nonnull align 1 dereferenceable(1) %1, i32* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE9__destroyIiEEvNS_17integral_constantIbLb1EEERS2_PT_(%"class.std::__2::allocator.94"* nonnull align 1 dereferenceable(1) %__a, i32* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.94"*, align 4
  %__p.addr = alloca i32*, align 4
  store %"class.std::__2::allocator.94"* %__a, %"class.std::__2::allocator.94"** %__a.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.94"*, %"class.std::__2::allocator.94"** %__a.addr, align 4
  %2 = load i32*, i32** %__p.addr, align 4
  call void @_ZNSt3__29allocatorIiE7destroyEPi(%"class.std::__2::allocator.94"* %1, i32* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIiE7destroyEPi(%"class.std::__2::allocator.94"* %this, i32* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.94"*, align 4
  %__p.addr = alloca i32*, align 4
  store %"class.std::__2::allocator.94"* %this, %"class.std::__2::allocator.94"** %this.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.94"*, %"class.std::__2::allocator.94"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIiE10deallocateEPim(%"class.std::__2::allocator.94"* %this, i32* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.94"*, align 4
  %__p.addr = alloca i32*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.94"* %this, %"class.std::__2::allocator.94"** %this.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.94"*, %"class.std::__2::allocator.94"** %this.addr, align 4
  %0 = load i32*, i32** %__p.addr, align 4
  %1 = bitcast i32* %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %__ptr, i32 %__size, i32 %__align) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  %1 = load i32, i32* %__size.addr, align 4
  %2 = load i32, i32* %__align.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm(i8* %0, i32 %1, i32 %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm(i8* %__ptr, i32 %__size, i32 %__align) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  %1 = load i32, i32* %__size.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm(i8* %0, i32 %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm(i8* %__ptr, i32 %__size) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller9__do_callEPv(i8* %0)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller9__do_callEPv(i8* %__ptr) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  call void @_ZdlPv(i8* %0) #9
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.94"* @_ZNSt3__217__compressed_pairIPiNS_9allocatorIiEEE6secondEv(%"class.std::__2::__compressed_pair.91"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.91"*, align 4
  store %"class.std::__2::__compressed_pair.91"* %this, %"class.std::__2::__compressed_pair.91"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.91"*, %"class.std::__2::__compressed_pair.91"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.91"* %this1 to %"struct.std::__2::__compressed_pair_elem.93"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.94"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIiEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.93"* %0) #7
  ret %"class.std::__2::allocator.94"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.94"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIiEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.93"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.93"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.93"* %this, %"struct.std::__2::__compressed_pair_elem.93"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.93"*, %"struct.std::__2::__compressed_pair_elem.93"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.93"* %this1 to %"class.std::__2::allocator.94"*
  ret %"class.std::__2::allocator.94"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_deleteEv(%"class.std::__2::vector.112"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.112"*, align 4
  store %"class.std::__2::vector.112"* %this, %"class.std::__2::vector.112"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.112"*, %"class.std::__2::vector.112"** %this.addr, align 4
  %call = call %"class.draco::IndexType.114"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.112"* %this1) #7
  %0 = bitcast %"class.draco::IndexType.114"* %call to i8*
  %call2 = call %"class.draco::IndexType.114"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.112"* %this1) #7
  %call3 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.112"* %this1) #7
  %add.ptr = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %call2, i32 %call3
  %1 = bitcast %"class.draco::IndexType.114"* %add.ptr to i8*
  %call4 = call %"class.draco::IndexType.114"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.112"* %this1) #7
  %call5 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.112"* %this1) #7
  %add.ptr6 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %call4, i32 %call5
  %2 = bitcast %"class.draco::IndexType.114"* %add.ptr6 to i8*
  %call7 = call %"class.draco::IndexType.114"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.112"* %this1) #7
  %call8 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.112"* %this1) #7
  %add.ptr9 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %call7, i32 %call8
  %3 = bitcast %"class.draco::IndexType.114"* %add.ptr9 to i8*
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_(%"class.std::__2::vector.112"* %this1, i8* %0, i8* %1, i8* %2, i8* %3) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base.113"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEED2Ev(%"class.std::__2::__vector_base.113"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__vector_base.113"*, align 4
  %this.addr = alloca %"class.std::__2::__vector_base.113"*, align 4
  store %"class.std::__2::__vector_base.113"* %this, %"class.std::__2::__vector_base.113"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.113"*, %"class.std::__2::__vector_base.113"** %this.addr, align 4
  store %"class.std::__2::__vector_base.113"* %this1, %"class.std::__2::__vector_base.113"** %retval, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.113", %"class.std::__2::__vector_base.113"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__begin_, align 4
  %cmp = icmp ne %"class.draco::IndexType.114"* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE5clearEv(%"class.std::__2::__vector_base.113"* %this1) #7
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.118"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.113"* %this1) #7
  %__begin_2 = getelementptr inbounds %"class.std::__2::__vector_base.113", %"class.std::__2::__vector_base.113"* %this1, i32 0, i32 0
  %1 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__begin_2, align 4
  %call3 = call i32 @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::__vector_base.113"* %this1) #7
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEEE10deallocateERS6_PS5_m(%"class.std::__2::allocator.118"* nonnull align 1 dereferenceable(1) %call, %"class.draco::IndexType.114"* %1, i32 %call3) #7
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"class.std::__2::__vector_base.113"*, %"class.std::__2::__vector_base.113"** %retval, align 4
  ret %"class.std::__2::__vector_base.113"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_(%"class.std::__2::vector.112"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.112"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector.112"* %this, %"class.std::__2::vector.112"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector.112"*, %"class.std::__2::vector.112"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType.114"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.112"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.112"*, align 4
  store %"class.std::__2::vector.112"* %this, %"class.std::__2::vector.112"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.112"*, %"class.std::__2::vector.112"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.112"* %this1 to %"class.std::__2::__vector_base.113"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.113", %"class.std::__2::__vector_base.113"* %0, i32 0, i32 0
  %1 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__begin_, align 4
  %call = call %"class.draco::IndexType.114"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType.114"* %1) #7
  ret %"class.draco::IndexType.114"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.112"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.112"*, align 4
  store %"class.std::__2::vector.112"* %this, %"class.std::__2::vector.112"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.112"*, %"class.std::__2::vector.112"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.112"* %this1 to %"class.std::__2::__vector_base.113"*
  %call = call i32 @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::__vector_base.113"* %0) #7
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.112"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.112"*, align 4
  store %"class.std::__2::vector.112"* %this, %"class.std::__2::vector.112"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.112"*, %"class.std::__2::vector.112"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.112"* %this1 to %"class.std::__2::__vector_base.113"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.113", %"class.std::__2::__vector_base.113"* %0, i32 0, i32 1
  %1 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector.112"* %this1 to %"class.std::__2::__vector_base.113"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.113", %"class.std::__2::__vector_base.113"* %2, i32 0, i32 0
  %3 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.draco::IndexType.114"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.draco::IndexType.114"* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType.114"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType.114"* %__p) #0 comdat {
entry:
  %__p.addr = alloca %"class.draco::IndexType.114"*, align 4
  store %"class.draco::IndexType.114"* %__p, %"class.draco::IndexType.114"** %__p.addr, align 4
  %0 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__p.addr, align 4
  ret %"class.draco::IndexType.114"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::__vector_base.113"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.113"*, align 4
  store %"class.std::__2::__vector_base.113"* %this, %"class.std::__2::__vector_base.113"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.113"*, %"class.std::__2::__vector_base.113"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.114"** @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base.113"* %this1) #7
  %0 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.113", %"class.std::__2::__vector_base.113"* %this1, i32 0, i32 0
  %1 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.draco::IndexType.114"* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.draco::IndexType.114"* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.114"** @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base.113"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.113"*, align 4
  store %"class.std::__2::__vector_base.113"* %this, %"class.std::__2::__vector_base.113"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.113"*, %"class.std::__2::__vector_base.113"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.113", %"class.std::__2::__vector_base.113"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.114"** @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.115"* %__end_cap_) #7
  ret %"class.draco::IndexType.114"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.114"** @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.115"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.115"*, align 4
  store %"class.std::__2::__compressed_pair.115"* %this, %"class.std::__2::__compressed_pair.115"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.115"*, %"class.std::__2::__compressed_pair.115"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.115"* %this1 to %"struct.std::__2::__compressed_pair_elem.116"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.114"** @_ZNKSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.116"* %0) #7
  ret %"class.draco::IndexType.114"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.114"** @_ZNKSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.116"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.116"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.116"* %this, %"struct.std::__2::__compressed_pair_elem.116"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.116"*, %"struct.std::__2::__compressed_pair_elem.116"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.116", %"struct.std::__2::__compressed_pair_elem.116"* %this1, i32 0, i32 0
  ret %"class.draco::IndexType.114"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE5clearEv(%"class.std::__2::__vector_base.113"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.113"*, align 4
  store %"class.std::__2::__vector_base.113"* %this, %"class.std::__2::__vector_base.113"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.113"*, %"class.std::__2::__vector_base.113"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.113", %"class.std::__2::__vector_base.113"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__begin_, align 4
  call void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_(%"class.std::__2::__vector_base.113"* %this1, %"class.draco::IndexType.114"* %0) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEEE10deallocateERS6_PS5_m(%"class.std::__2::allocator.118"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType.114"* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.118"*, align 4
  %__p.addr = alloca %"class.draco::IndexType.114"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.118"* %__a, %"class.std::__2::allocator.118"** %__a.addr, align 4
  store %"class.draco::IndexType.114"* %__p, %"class.draco::IndexType.114"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.118"*, %"class.std::__2::allocator.118"** %__a.addr, align 4
  %1 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEE10deallocateEPS4_m(%"class.std::__2::allocator.118"* %0, %"class.draco::IndexType.114"* %1, i32 %2) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.118"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.113"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.113"*, align 4
  store %"class.std::__2::__vector_base.113"* %this, %"class.std::__2::__vector_base.113"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.113"*, %"class.std::__2::__vector_base.113"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.113", %"class.std::__2::__vector_base.113"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.118"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.115"* %__end_cap_) #7
  ret %"class.std::__2::allocator.118"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_(%"class.std::__2::__vector_base.113"* %this, %"class.draco::IndexType.114"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.113"*, align 4
  %__new_last.addr = alloca %"class.draco::IndexType.114"*, align 4
  %__soon_to_be_end = alloca %"class.draco::IndexType.114"*, align 4
  store %"class.std::__2::__vector_base.113"* %this, %"class.std::__2::__vector_base.113"** %this.addr, align 4
  store %"class.draco::IndexType.114"* %__new_last, %"class.draco::IndexType.114"** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.113"*, %"class.std::__2::__vector_base.113"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.113", %"class.std::__2::__vector_base.113"* %this1, i32 0, i32 1
  %0 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__end_, align 4
  store %"class.draco::IndexType.114"* %0, %"class.draco::IndexType.114"** %__soon_to_be_end, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__new_last.addr, align 4
  %2 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__soon_to_be_end, align 4
  %cmp = icmp ne %"class.draco::IndexType.114"* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.118"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.113"* %this1) #7
  %3 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__soon_to_be_end, align 4
  %incdec.ptr = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %3, i32 -1
  store %"class.draco::IndexType.114"* %incdec.ptr, %"class.draco::IndexType.114"** %__soon_to_be_end, align 4
  %call2 = call %"class.draco::IndexType.114"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType.114"* %incdec.ptr) #7
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEEE7destroyIS5_EEvRS6_PT_(%"class.std::__2::allocator.118"* nonnull align 1 dereferenceable(1) %call, %"class.draco::IndexType.114"* %call2)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %4 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__new_last.addr, align 4
  %__end_3 = getelementptr inbounds %"class.std::__2::__vector_base.113", %"class.std::__2::__vector_base.113"* %this1, i32 0, i32 1
  store %"class.draco::IndexType.114"* %4, %"class.draco::IndexType.114"** %__end_3, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEEE7destroyIS5_EEvRS6_PT_(%"class.std::__2::allocator.118"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType.114"* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.118"*, align 4
  %__p.addr = alloca %"class.draco::IndexType.114"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy.131", align 1
  store %"class.std::__2::allocator.118"* %__a, %"class.std::__2::allocator.118"** %__a.addr, align 4
  store %"class.draco::IndexType.114"* %__p, %"class.draco::IndexType.114"** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy.131"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.118"*, %"class.std::__2::allocator.118"** %__a.addr, align 4
  %2 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEEE9__destroyIS5_EEvNS_17integral_constantIbLb1EEERS6_PT_(%"class.std::__2::allocator.118"* nonnull align 1 dereferenceable(1) %1, %"class.draco::IndexType.114"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEEE9__destroyIS5_EEvNS_17integral_constantIbLb1EEERS6_PT_(%"class.std::__2::allocator.118"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType.114"* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.118"*, align 4
  %__p.addr = alloca %"class.draco::IndexType.114"*, align 4
  store %"class.std::__2::allocator.118"* %__a, %"class.std::__2::allocator.118"** %__a.addr, align 4
  store %"class.draco::IndexType.114"* %__p, %"class.draco::IndexType.114"** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.118"*, %"class.std::__2::allocator.118"** %__a.addr, align 4
  %2 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__p.addr, align 4
  call void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEE7destroyEPS4_(%"class.std::__2::allocator.118"* %1, %"class.draco::IndexType.114"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEE7destroyEPS4_(%"class.std::__2::allocator.118"* %this, %"class.draco::IndexType.114"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.118"*, align 4
  %__p.addr = alloca %"class.draco::IndexType.114"*, align 4
  store %"class.std::__2::allocator.118"* %this, %"class.std::__2::allocator.118"** %this.addr, align 4
  store %"class.draco::IndexType.114"* %__p, %"class.draco::IndexType.114"** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.118"*, %"class.std::__2::allocator.118"** %this.addr, align 4
  %0 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__p.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEE10deallocateEPS4_m(%"class.std::__2::allocator.118"* %this, %"class.draco::IndexType.114"* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.118"*, align 4
  %__p.addr = alloca %"class.draco::IndexType.114"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.118"* %this, %"class.std::__2::allocator.118"** %this.addr, align 4
  store %"class.draco::IndexType.114"* %__p, %"class.draco::IndexType.114"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.118"*, %"class.std::__2::allocator.118"** %this.addr, align 4
  %0 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__p.addr, align 4
  %1 = bitcast %"class.draco::IndexType.114"* %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.118"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.115"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.115"*, align 4
  store %"class.std::__2::__compressed_pair.115"* %this, %"class.std::__2::__compressed_pair.115"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.115"*, %"class.std::__2::__compressed_pair.115"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.115"* %this1 to %"struct.std::__2::__compressed_pair_elem.117"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.118"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.117"* %0) #7
  ret %"class.std::__2::allocator.118"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.118"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.117"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.117"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.117"* %this, %"struct.std::__2::__compressed_pair_elem.117"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.117"*, %"struct.std::__2::__compressed_pair_elem.117"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.117"* %this1 to %"class.std::__2::allocator.118"*
  ret %"class.std::__2::allocator.118"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.53"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.53"*, align 4
  store %"class.std::__2::vector.53"* %this, %"class.std::__2::vector.53"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.53"*, %"class.std::__2::vector.53"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.53"* %this1 to %"class.std::__2::__vector_base.54"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.54", %"class.std::__2::__vector_base.54"* %0, i32 0, i32 1
  %1 = load %"class.std::__2::unique_ptr.55"*, %"class.std::__2::unique_ptr.55"** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector.53"* %this1 to %"class.std::__2::__vector_base.54"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.54", %"class.std::__2::__vector_base.54"* %2, i32 0, i32 0
  %3 = load %"class.std::__2::unique_ptr.55"*, %"class.std::__2::unique_ptr.55"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.std::__2::unique_ptr.55"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.std::__2::unique_ptr.55"* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EE5valueEv(%"class.draco::IndexType"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.draco::IndexType"* %this, %"class.draco::IndexType"** %this.addr, align 4
  %this1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value_, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK5draco10DataBuffer4ReadExPvm(%"class.draco::DataBuffer"* %this, i64 %byte_pos, i8* %out_data, i32 %data_size) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::DataBuffer"*, align 4
  %byte_pos.addr = alloca i64, align 8
  %out_data.addr = alloca i8*, align 4
  %data_size.addr = alloca i32, align 4
  store %"class.draco::DataBuffer"* %this, %"class.draco::DataBuffer"** %this.addr, align 4
  store i64 %byte_pos, i64* %byte_pos.addr, align 8
  store i8* %out_data, i8** %out_data.addr, align 4
  store i32 %data_size, i32* %data_size.addr, align 4
  %this1 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %this.addr, align 4
  %0 = load i8*, i8** %out_data.addr, align 4
  %call = call i8* @_ZNK5draco10DataBuffer4dataEv(%"class.draco::DataBuffer"* %this1)
  %1 = load i64, i64* %byte_pos.addr, align 8
  %idx.ext = trunc i64 %1 to i32
  %add.ptr = getelementptr inbounds i8, i8* %call, i32 %idx.ext
  %2 = load i32, i32* %data_size.addr, align 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %0, i8* align 1 %add.ptr, i32 %2, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNK5draco10DataBuffer4dataEv(%"class.draco::DataBuffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::DataBuffer"*, align 4
  store %"class.draco::DataBuffer"* %this, %"class.draco::DataBuffer"** %this.addr, align 4
  %this1 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %this.addr, align 4
  %data_ = getelementptr inbounds %"class.draco::DataBuffer", %"class.draco::DataBuffer"* %this1, i32 0, i32 0
  %call = call i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector.58"* %data_) #7
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector.58"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.58"*, align 4
  store %"class.std::__2::vector.58"* %this, %"class.std::__2::vector.58"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.58"*, %"class.std::__2::vector.58"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.58"* %this1 to %"class.std::__2::__vector_base.59"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.59", %"class.std::__2::__vector_base.59"* %0, i32 0, i32 0
  %1 = load i8*, i8** %__begin_, align 4
  %call = call i8* @_ZNSt3__212__to_addressIhEEPT_S2_(i8* %1) #7
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__212__to_addressIhEEPT_S2_(i8* %__p) #0 comdat {
entry:
  %__p.addr = alloca i8*, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %0 = load i8*, i8** %__p.addr, align 4
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNSt3__25arrayIfLm3EEixEm(%"struct.std::__2::array.126"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::array.126"*, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::array.126"* %this, %"struct.std::__2::array.126"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::array.126"*, %"struct.std::__2::array.126"** %this.addr, align 4
  %__elems_ = getelementptr inbounds %"struct.std::__2::array.126", %"struct.std::__2::array.126"* %this1, i32 0, i32 0
  %0 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds [3 x float], [3 x float]* %__elems_, i32 0, i32 %0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIiNS_9allocatorIiEEE7reserveEm(%"class.std::__2::vector.89"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.89"*, align 4
  %__n.addr = alloca i32, align 4
  %__a = alloca %"class.std::__2::allocator.94"*, align 4
  %__v = alloca %"struct.std::__2::__split_buffer", align 4
  store %"class.std::__2::vector.89"* %this, %"class.std::__2::vector.89"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.89"*, %"class.std::__2::vector.89"** %this.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::vector.89"* %this1) #7
  %cmp = icmp ugt i32 %0, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %"class.std::__2::vector.89"* %this1 to %"class.std::__2::__vector_base.90"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.94"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE7__allocEv(%"class.std::__2::__vector_base.90"* %1) #7
  store %"class.std::__2::allocator.94"* %call2, %"class.std::__2::allocator.94"** %__a, align 4
  %2 = load i32, i32* %__n.addr, align 4
  %call3 = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4sizeEv(%"class.std::__2::vector.89"* %this1) #7
  %3 = load %"class.std::__2::allocator.94"*, %"class.std::__2::allocator.94"** %__a, align 4
  %call4 = call %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEEC2EmmS3_(%"struct.std::__2::__split_buffer"* %__v, i32 %2, i32 %call3, %"class.std::__2::allocator.94"* nonnull align 1 dereferenceable(1) %3)
  call void @_ZNSt3__26vectorIiNS_9allocatorIiEEE26__swap_out_circular_bufferERNS_14__split_bufferIiRS2_EE(%"class.std::__2::vector.89"* %this1, %"struct.std::__2::__split_buffer"* nonnull align 4 dereferenceable(20) %__v)
  %call5 = call %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEED2Ev(%"struct.std::__2::__split_buffer"* %__v) #7
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEEC2EmmS3_(%"struct.std::__2::__split_buffer"* returned %this, i32 %__cap, i32 %__start, %"class.std::__2::allocator.94"* nonnull align 1 dereferenceable(1) %__a) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"struct.std::__2::__split_buffer"*, align 4
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__cap.addr = alloca i32, align 4
  %__start.addr = alloca i32, align 4
  %__a.addr = alloca %"class.std::__2::allocator.94"*, align 4
  %ref.tmp = alloca i8*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store i32 %__cap, i32* %__cap.addr, align 4
  store i32 %__start, i32* %__start.addr, align 4
  store %"class.std::__2::allocator.94"* %__a, %"class.std::__2::allocator.94"** %__a.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer"* %this1, %"struct.std::__2::__split_buffer"** %retval, align 4
  %0 = bitcast %"struct.std::__2::__split_buffer"* %this1 to %"class.std::__2::__split_buffer_common"*
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  store i8* null, i8** %ref.tmp, align 4
  %1 = load %"class.std::__2::allocator.94"*, %"class.std::__2::allocator.94"** %__a.addr, align 4
  %call = call %"class.std::__2::__compressed_pair.132"* @_ZNSt3__217__compressed_pairIPiRNS_9allocatorIiEEEC2IDnS4_EEOT_OT0_(%"class.std::__2::__compressed_pair.132"* %__end_cap_, i8** nonnull align 4 dereferenceable(4) %ref.tmp, %"class.std::__2::allocator.94"* nonnull align 1 dereferenceable(1) %1)
  %2 = load i32, i32* %__cap.addr, align 4
  %cmp = icmp ne i32 %2, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.94"* @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE7__allocEv(%"struct.std::__2::__split_buffer"* %this1) #7
  %3 = load i32, i32* %__cap.addr, align 4
  %call3 = call i32* @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE8allocateERS2_m(%"class.std::__2::allocator.94"* nonnull align 1 dereferenceable(1) %call2, i32 %3)
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32* [ %call3, %cond.true ], [ null, %cond.false ]
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  store i32* %cond, i32** %__first_, align 4
  %__first_4 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %4 = load i32*, i32** %__first_4, align 4
  %5 = load i32, i32* %__start.addr, align 4
  %add.ptr = getelementptr inbounds i32, i32* %4, i32 %5
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 2
  store i32* %add.ptr, i32** %__end_, align 4
  %__begin_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 1
  store i32* %add.ptr, i32** %__begin_, align 4
  %__first_5 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %6 = load i32*, i32** %__first_5, align 4
  %7 = load i32, i32* %__cap.addr, align 4
  %add.ptr6 = getelementptr inbounds i32, i32* %6, i32 %7
  %call7 = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this1) #7
  store i32* %add.ptr6, i32** %call7, align 4
  %8 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %retval, align 4
  ret %"struct.std::__2::__split_buffer"* %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIiNS_9allocatorIiEEE26__swap_out_circular_bufferERNS_14__split_bufferIiRS2_EE(%"class.std::__2::vector.89"* %this, %"struct.std::__2::__split_buffer"* nonnull align 4 dereferenceable(20) %__v) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.89"*, align 4
  %__v.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"class.std::__2::vector.89"* %this, %"class.std::__2::vector.89"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer"* %__v, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %this1 = load %"class.std::__2::vector.89"*, %"class.std::__2::vector.89"** %this.addr, align 4
  call void @_ZNKSt3__26vectorIiNS_9allocatorIiEEE17__annotate_deleteEv(%"class.std::__2::vector.89"* %this1) #7
  %0 = bitcast %"class.std::__2::vector.89"* %this1 to %"class.std::__2::__vector_base.90"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.94"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE7__allocEv(%"class.std::__2::__vector_base.90"* %0) #7
  %1 = bitcast %"class.std::__2::vector.89"* %this1 to %"class.std::__2::__vector_base.90"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.90", %"class.std::__2::__vector_base.90"* %1, i32 0, i32 0
  %2 = load i32*, i32** %__begin_, align 4
  %3 = bitcast %"class.std::__2::vector.89"* %this1 to %"class.std::__2::__vector_base.90"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.90", %"class.std::__2::__vector_base.90"* %3, i32 0, i32 1
  %4 = load i32*, i32** %__end_, align 4
  %5 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__begin_2 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %5, i32 0, i32 1
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE46__construct_backward_with_exception_guaranteesIiEENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS2_PT_S8_EE5valuesr31is_trivially_move_constructibleIS8_EE5valueEvE4typeERS2_S9_S9_RS9_(%"class.std::__2::allocator.94"* nonnull align 1 dereferenceable(1) %call, i32* %2, i32* %4, i32** nonnull align 4 dereferenceable(4) %__begin_2)
  %6 = bitcast %"class.std::__2::vector.89"* %this1 to %"class.std::__2::__vector_base.90"*
  %__begin_3 = getelementptr inbounds %"class.std::__2::__vector_base.90", %"class.std::__2::__vector_base.90"* %6, i32 0, i32 0
  %7 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__begin_4 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %7, i32 0, i32 1
  call void @_ZNSt3__24swapIPiEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_(i32** nonnull align 4 dereferenceable(4) %__begin_3, i32** nonnull align 4 dereferenceable(4) %__begin_4) #7
  %8 = bitcast %"class.std::__2::vector.89"* %this1 to %"class.std::__2::__vector_base.90"*
  %__end_5 = getelementptr inbounds %"class.std::__2::__vector_base.90", %"class.std::__2::__vector_base.90"* %8, i32 0, i32 1
  %9 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__end_6 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %9, i32 0, i32 2
  call void @_ZNSt3__24swapIPiEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_(i32** nonnull align 4 dereferenceable(4) %__end_5, i32** nonnull align 4 dereferenceable(4) %__end_6) #7
  %10 = bitcast %"class.std::__2::vector.89"* %this1 to %"class.std::__2::__vector_base.90"*
  %call7 = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE9__end_capEv(%"class.std::__2::__vector_base.90"* %10) #7
  %11 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %call8 = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE9__end_capEv(%"struct.std::__2::__split_buffer"* %11) #7
  call void @_ZNSt3__24swapIPiEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_(i32** nonnull align 4 dereferenceable(4) %call7, i32** nonnull align 4 dereferenceable(4) %call8) #7
  %12 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__begin_9 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %12, i32 0, i32 1
  %13 = load i32*, i32** %__begin_9, align 4
  %14 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %14, i32 0, i32 0
  store i32* %13, i32** %__first_, align 4
  %call10 = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4sizeEv(%"class.std::__2::vector.89"* %this1) #7
  call void @_ZNKSt3__26vectorIiNS_9allocatorIiEEE14__annotate_newEm(%"class.std::__2::vector.89"* %this1, i32 %call10) #7
  call void @_ZNSt3__26vectorIiNS_9allocatorIiEEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector.89"* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEED2Ev(%"struct.std::__2::__split_buffer"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"struct.std::__2::__split_buffer"*, align 4
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer"* %this1, %"struct.std::__2::__split_buffer"** %retval, align 4
  call void @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE5clearEv(%"struct.std::__2::__split_buffer"* %this1) #7
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__first_, align 4
  %tobool = icmp ne i32* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.94"* @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE7__allocEv(%"struct.std::__2::__split_buffer"* %this1) #7
  %__first_2 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %1 = load i32*, i32** %__first_2, align 4
  %call3 = call i32 @_ZNKSt3__214__split_bufferIiRNS_9allocatorIiEEE8capacityEv(%"struct.std::__2::__split_buffer"* %this1)
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE10deallocateERS2_Pim(%"class.std::__2::allocator.94"* nonnull align 1 dereferenceable(1) %call, i32* %1, i32 %call3) #7
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %retval, align 4
  ret %"struct.std::__2::__split_buffer"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.132"* @_ZNSt3__217__compressed_pairIPiRNS_9allocatorIiEEEC2IDnS4_EEOT_OT0_(%"class.std::__2::__compressed_pair.132"* returned %this, i8** nonnull align 4 dereferenceable(4) %__t1, %"class.std::__2::allocator.94"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.132"*, align 4
  %__t1.addr = alloca i8**, align 4
  %__t2.addr = alloca %"class.std::__2::allocator.94"*, align 4
  store %"class.std::__2::__compressed_pair.132"* %this, %"class.std::__2::__compressed_pair.132"** %this.addr, align 4
  store i8** %__t1, i8*** %__t1.addr, align 4
  store %"class.std::__2::allocator.94"* %__t2, %"class.std::__2::allocator.94"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.132"*, %"class.std::__2::__compressed_pair.132"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.132"* %this1 to %"struct.std::__2::__compressed_pair_elem.92"*
  %1 = load i8**, i8*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %1) #7
  %call2 = call %"struct.std::__2::__compressed_pair_elem.92"* @_ZNSt3__222__compressed_pair_elemIPiLi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.92"* %0, i8** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.132"* %this1 to i8*
  %3 = getelementptr inbounds i8, i8* %2, i32 4
  %4 = bitcast i8* %3 to %"struct.std::__2::__compressed_pair_elem.133"*
  %5 = load %"class.std::__2::allocator.94"*, %"class.std::__2::allocator.94"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.94"* @_ZNSt3__27forwardIRNS_9allocatorIiEEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.std::__2::allocator.94"* nonnull align 1 dereferenceable(1) %5) #7
  %call4 = call %"struct.std::__2::__compressed_pair_elem.133"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIiEELi1ELb0EEC2IS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.133"* %4, %"class.std::__2::allocator.94"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::__compressed_pair.132"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE8allocateERS2_m(%"class.std::__2::allocator.94"* nonnull align 1 dereferenceable(1) %__a, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.94"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.94"* %__a, %"class.std::__2::allocator.94"** %__a.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.94"*, %"class.std::__2::allocator.94"** %__a.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call i32* @_ZNSt3__29allocatorIiE8allocateEmPKv(%"class.std::__2::allocator.94"* %0, i32 %1, i8* null)
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.94"* @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE7__allocEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.94"* @_ZNSt3__217__compressed_pairIPiRNS_9allocatorIiEEE6secondEv(%"class.std::__2::__compressed_pair.132"* %__end_cap_) #7
  ret %"class.std::__2::allocator.94"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__217__compressed_pairIPiRNS_9allocatorIiEEE5firstEv(%"class.std::__2::__compressed_pair.132"* %__end_cap_) #7
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.94"* @_ZNSt3__27forwardIRNS_9allocatorIiEEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.std::__2::allocator.94"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::allocator.94"*, align 4
  store %"class.std::__2::allocator.94"* %__t, %"class.std::__2::allocator.94"** %__t.addr, align 4
  %0 = load %"class.std::__2::allocator.94"*, %"class.std::__2::allocator.94"** %__t.addr, align 4
  ret %"class.std::__2::allocator.94"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.133"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIiEELi1ELb0EEC2IS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.133"* returned %this, %"class.std::__2::allocator.94"* nonnull align 1 dereferenceable(1) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.133"*, align 4
  %__u.addr = alloca %"class.std::__2::allocator.94"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.133"* %this, %"struct.std::__2::__compressed_pair_elem.133"** %this.addr, align 4
  store %"class.std::__2::allocator.94"* %__u, %"class.std::__2::allocator.94"** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.133"*, %"struct.std::__2::__compressed_pair_elem.133"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.133", %"struct.std::__2::__compressed_pair_elem.133"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::allocator.94"*, %"class.std::__2::allocator.94"** %__u.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.94"* @_ZNSt3__27forwardIRNS_9allocatorIiEEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.std::__2::allocator.94"* nonnull align 1 dereferenceable(1) %0) #7
  store %"class.std::__2::allocator.94"* %call, %"class.std::__2::allocator.94"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.133"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNSt3__29allocatorIiE8allocateEmPKv(%"class.std::__2::allocator.94"* %this, i32 %__n, i8* %0) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.94"*, align 4
  %__n.addr = alloca i32, align 4
  %.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.94"* %this, %"class.std::__2::allocator.94"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::allocator.94"*, %"class.std::__2::allocator.94"** %this.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorIiE8max_sizeEv(%"class.std::__2::allocator.94"* %this1) #7
  %cmp = icmp ugt i32 %1, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__220__throw_length_errorEPKc(i8* getelementptr inbounds ([68 x i8], [68 x i8]* @.str.2, i32 0, i32 0)) #10
  unreachable

if.end:                                           ; preds = %entry
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  %call2 = call i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %mul, i32 4)
  %3 = bitcast i8* %call2 to i32*
  ret i32* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__29allocatorIiE8max_sizeEv(%"class.std::__2::allocator.94"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.94"*, align 4
  store %"class.std::__2::allocator.94"* %this, %"class.std::__2::allocator.94"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.94"*, %"class.std::__2::allocator.94"** %this.addr, align 4
  ret i32 1073741823
}

; Function Attrs: noinline noreturn nounwind optnone
define linkonce_odr hidden void @_ZNSt3__220__throw_length_errorEPKc(i8* %__msg) #5 comdat {
entry:
  %__msg.addr = alloca i8*, align 4
  store i8* %__msg, i8** %__msg.addr, align 4
  call void @abort() #10
  unreachable
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %__size, i32 %__align) #0 comdat {
entry:
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i32, i32* %__size.addr, align 4
  %call = call noalias nonnull i8* @_Znwm(i32 %0) #8
  ret i8* %call
}

; Function Attrs: noreturn
declare void @abort() #6

; Function Attrs: nobuiltin allocsize(0)
declare nonnull i8* @_Znwm(i32) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.94"* @_ZNSt3__217__compressed_pairIPiRNS_9allocatorIiEEE6secondEv(%"class.std::__2::__compressed_pair.132"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.132"*, align 4
  store %"class.std::__2::__compressed_pair.132"* %this, %"class.std::__2::__compressed_pair.132"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.132"*, %"class.std::__2::__compressed_pair.132"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.132"* %this1 to i8*
  %add.ptr = getelementptr inbounds i8, i8* %0, i32 4
  %1 = bitcast i8* %add.ptr to %"struct.std::__2::__compressed_pair_elem.133"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.94"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIiEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.133"* %1) #7
  ret %"class.std::__2::allocator.94"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.94"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIiEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.133"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.133"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.133"* %this, %"struct.std::__2::__compressed_pair_elem.133"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.133"*, %"struct.std::__2::__compressed_pair_elem.133"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.133", %"struct.std::__2::__compressed_pair_elem.133"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::allocator.94"*, %"class.std::__2::allocator.94"** %__value_, align 4
  ret %"class.std::__2::allocator.94"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNSt3__217__compressed_pairIPiRNS_9allocatorIiEEE5firstEv(%"class.std::__2::__compressed_pair.132"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.132"*, align 4
  store %"class.std::__2::__compressed_pair.132"* %this, %"class.std::__2::__compressed_pair.132"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.132"*, %"class.std::__2::__compressed_pair.132"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.132"* %this1 to %"struct.std::__2::__compressed_pair_elem.92"*
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__222__compressed_pair_elemIPiLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.92"* %0) #7
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNSt3__222__compressed_pair_elemIPiLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.92"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.92"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.92"* %this, %"struct.std::__2::__compressed_pair_elem.92"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.92"*, %"struct.std::__2::__compressed_pair_elem.92"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.92", %"struct.std::__2::__compressed_pair_elem.92"* %this1, i32 0, i32 0
  ret i32** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE46__construct_backward_with_exception_guaranteesIiEENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS2_PT_S8_EE5valuesr31is_trivially_move_constructibleIS8_EE5valueEvE4typeERS2_S9_S9_RS9_(%"class.std::__2::allocator.94"* nonnull align 1 dereferenceable(1) %0, i32* %__begin1, i32* %__end1, i32** nonnull align 4 dereferenceable(4) %__end2) #0 comdat {
entry:
  %.addr = alloca %"class.std::__2::allocator.94"*, align 4
  %__begin1.addr = alloca i32*, align 4
  %__end1.addr = alloca i32*, align 4
  %__end2.addr = alloca i32**, align 4
  %_Np = alloca i32, align 4
  store %"class.std::__2::allocator.94"* %0, %"class.std::__2::allocator.94"** %.addr, align 4
  store i32* %__begin1, i32** %__begin1.addr, align 4
  store i32* %__end1, i32** %__end1.addr, align 4
  store i32** %__end2, i32*** %__end2.addr, align 4
  %1 = load i32*, i32** %__end1.addr, align 4
  %2 = load i32*, i32** %__begin1.addr, align 4
  %sub.ptr.lhs.cast = ptrtoint i32* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint i32* %2 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  store i32 %sub.ptr.div, i32* %_Np, align 4
  %3 = load i32, i32* %_Np, align 4
  %4 = load i32**, i32*** %__end2.addr, align 4
  %5 = load i32*, i32** %4, align 4
  %idx.neg = sub i32 0, %3
  %add.ptr = getelementptr inbounds i32, i32* %5, i32 %idx.neg
  store i32* %add.ptr, i32** %4, align 4
  %6 = load i32, i32* %_Np, align 4
  %cmp = icmp sgt i32 %6, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %7 = load i32**, i32*** %__end2.addr, align 4
  %8 = load i32*, i32** %7, align 4
  %9 = bitcast i32* %8 to i8*
  %10 = load i32*, i32** %__begin1.addr, align 4
  %11 = bitcast i32* %10 to i8*
  %12 = load i32, i32* %_Np, align 4
  %mul = mul i32 %12, 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %11, i32 %mul, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__24swapIPiEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_(i32** nonnull align 4 dereferenceable(4) %__x, i32** nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %__x.addr = alloca i32**, align 4
  %__y.addr = alloca i32**, align 4
  %__t = alloca i32*, align 4
  store i32** %__x, i32*** %__x.addr, align 4
  store i32** %__y, i32*** %__y.addr, align 4
  %0 = load i32**, i32*** %__x.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__24moveIRPiEEONS_16remove_referenceIT_E4typeEOS4_(i32** nonnull align 4 dereferenceable(4) %0) #7
  %1 = load i32*, i32** %call, align 4
  store i32* %1, i32** %__t, align 4
  %2 = load i32**, i32*** %__y.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__24moveIRPiEEONS_16remove_referenceIT_E4typeEOS4_(i32** nonnull align 4 dereferenceable(4) %2) #7
  %3 = load i32*, i32** %call1, align 4
  %4 = load i32**, i32*** %__x.addr, align 4
  store i32* %3, i32** %4, align 4
  %call2 = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__24moveIRPiEEONS_16remove_referenceIT_E4typeEOS4_(i32** nonnull align 4 dereferenceable(4) %__t) #7
  %5 = load i32*, i32** %call2, align 4
  %6 = load i32**, i32*** %__y.addr, align 4
  store i32* %5, i32** %6, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE9__end_capEv(%"class.std::__2::__vector_base.90"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.90"*, align 4
  store %"class.std::__2::__vector_base.90"* %this, %"class.std::__2::__vector_base.90"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.90"*, %"class.std::__2::__vector_base.90"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.90", %"class.std::__2::__vector_base.90"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__217__compressed_pairIPiNS_9allocatorIiEEE5firstEv(%"class.std::__2::__compressed_pair.91"* %__end_cap_) #7
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIiNS_9allocatorIiEEE14__annotate_newEm(%"class.std::__2::vector.89"* %this, i32 %__current_size) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.89"*, align 4
  %__current_size.addr = alloca i32, align 4
  store %"class.std::__2::vector.89"* %this, %"class.std::__2::vector.89"** %this.addr, align 4
  store i32 %__current_size, i32* %__current_size.addr, align 4
  %this1 = load %"class.std::__2::vector.89"*, %"class.std::__2::vector.89"** %this.addr, align 4
  %call = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector.89"* %this1) #7
  %0 = bitcast i32* %call to i8*
  %call2 = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector.89"* %this1) #7
  %call3 = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::vector.89"* %this1) #7
  %add.ptr = getelementptr inbounds i32, i32* %call2, i32 %call3
  %1 = bitcast i32* %add.ptr to i8*
  %call4 = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector.89"* %this1) #7
  %call5 = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::vector.89"* %this1) #7
  %add.ptr6 = getelementptr inbounds i32, i32* %call4, i32 %call5
  %2 = bitcast i32* %add.ptr6 to i8*
  %call7 = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector.89"* %this1) #7
  %3 = load i32, i32* %__current_size.addr, align 4
  %add.ptr8 = getelementptr inbounds i32, i32* %call7, i32 %3
  %4 = bitcast i32* %add.ptr8 to i8*
  call void @_ZNKSt3__26vectorIiNS_9allocatorIiEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector.89"* %this1, i8* %0, i8* %1, i8* %2, i8* %4) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIiNS_9allocatorIiEEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector.89"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.89"*, align 4
  store %"class.std::__2::vector.89"* %this, %"class.std::__2::vector.89"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.89"*, %"class.std::__2::vector.89"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNSt3__24moveIRPiEEONS_16remove_referenceIT_E4typeEOS4_(i32** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca i32**, align 4
  store i32** %__t, i32*** %__t.addr, align 4
  %0 = load i32**, i32*** %__t.addr, align 4
  ret i32** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNSt3__217__compressed_pairIPiNS_9allocatorIiEEE5firstEv(%"class.std::__2::__compressed_pair.91"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.91"*, align 4
  store %"class.std::__2::__compressed_pair.91"* %this, %"class.std::__2::__compressed_pair.91"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.91"*, %"class.std::__2::__compressed_pair.91"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.91"* %this1 to %"struct.std::__2::__compressed_pair_elem.92"*
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__222__compressed_pair_elemIPiLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.92"* %0) #7
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE5clearEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 1
  %0 = load i32*, i32** %__begin_, align 4
  call void @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE17__destruct_at_endEPi(%"struct.std::__2::__split_buffer"* %this1, i32* %0) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__214__split_bufferIiRNS_9allocatorIiEEE8capacityEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__214__split_bufferIiRNS_9allocatorIiEEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this1) #7
  %0 = load i32*, i32** %call, align 4
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %1 = load i32*, i32** %__first_, align 4
  %sub.ptr.lhs.cast = ptrtoint i32* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint i32* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE17__destruct_at_endEPi(%"struct.std::__2::__split_buffer"* %this, i32* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__new_last.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant.134", align 1
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store i32* %__new_last, i32** %__new_last.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %0 = load i32*, i32** %__new_last.addr, align 4
  call void @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE17__destruct_at_endEPiNS_17integral_constantIbLb0EEE(%"struct.std::__2::__split_buffer"* %this1, i32* %0) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE17__destruct_at_endEPiNS_17integral_constantIbLb0EEE(%"struct.std::__2::__split_buffer"* %this, i32* %__new_last) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant.134", align 1
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__new_last.addr = alloca i32*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store i32* %__new_last, i32** %__new_last.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load i32*, i32** %__new_last.addr, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 2
  %2 = load i32*, i32** %__end_, align 4
  %cmp = icmp ne i32* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.94"* @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE7__allocEv(%"struct.std::__2::__split_buffer"* %this1) #7
  %__end_2 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 2
  %3 = load i32*, i32** %__end_2, align 4
  %incdec.ptr = getelementptr inbounds i32, i32* %3, i32 -1
  store i32* %incdec.ptr, i32** %__end_2, align 4
  %call3 = call i32* @_ZNSt3__212__to_addressIiEEPT_S2_(i32* %incdec.ptr) #7
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE7destroyIiEEvRS2_PT_(%"class.std::__2::allocator.94"* nonnull align 1 dereferenceable(1) %call, i32* %call3)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__214__split_bufferIiRNS_9allocatorIiEEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__217__compressed_pairIPiRNS_9allocatorIiEEE5firstEv(%"class.std::__2::__compressed_pair.132"* %__end_cap_) #7
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__217__compressed_pairIPiRNS_9allocatorIiEEE5firstEv(%"class.std::__2::__compressed_pair.132"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.132"*, align 4
  store %"class.std::__2::__compressed_pair.132"* %this, %"class.std::__2::__compressed_pair.132"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.132"*, %"class.std::__2::__compressed_pair.132"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.132"* %this1 to %"struct.std::__2::__compressed_pair_elem.92"*
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__222__compressed_pair_elemIPiLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.92"* %0) #7
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIiNS_9allocatorIiEEE9push_backEOi(%"class.std::__2::vector.89"* %this, i32* nonnull align 4 dereferenceable(4) %__x) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.89"*, align 4
  %__x.addr = alloca i32*, align 4
  store %"class.std::__2::vector.89"* %this, %"class.std::__2::vector.89"** %this.addr, align 4
  store i32* %__x, i32** %__x.addr, align 4
  %this1 = load %"class.std::__2::vector.89"*, %"class.std::__2::vector.89"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.89"* %this1 to %"class.std::__2::__vector_base.90"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.90", %"class.std::__2::__vector_base.90"* %0, i32 0, i32 1
  %1 = load i32*, i32** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector.89"* %this1 to %"class.std::__2::__vector_base.90"*
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE9__end_capEv(%"class.std::__2::__vector_base.90"* %2) #7
  %3 = load i32*, i32** %call, align 4
  %cmp = icmp ult i32* %1, %3
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %4 = load i32*, i32** %__x.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__24moveIRiEEONS_16remove_referenceIT_E4typeEOS3_(i32* nonnull align 4 dereferenceable(4) %4) #7
  call void @_ZNSt3__26vectorIiNS_9allocatorIiEEE22__construct_one_at_endIJiEEEvDpOT_(%"class.std::__2::vector.89"* %this1, i32* nonnull align 4 dereferenceable(4) %call2)
  br label %if.end

if.else:                                          ; preds = %entry
  %5 = load i32*, i32** %__x.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__24moveIRiEEONS_16remove_referenceIT_E4typeEOS3_(i32* nonnull align 4 dereferenceable(4) %5) #7
  call void @_ZNSt3__26vectorIiNS_9allocatorIiEEE21__push_back_slow_pathIiEEvOT_(%"class.std::__2::vector.89"* %this1, i32* nonnull align 4 dereferenceable(4) %call3)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__24moveIRiEEONS_16remove_referenceIT_E4typeEOS3_(i32* nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca i32*, align 4
  store i32* %__t, i32** %__t.addr, align 4
  %0 = load i32*, i32** %__t.addr, align 4
  ret i32* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIiNS_9allocatorIiEEE22__construct_one_at_endIJiEEEvDpOT_(%"class.std::__2::vector.89"* %this, i32* nonnull align 4 dereferenceable(4) %__args) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.89"*, align 4
  %__args.addr = alloca i32*, align 4
  %__tx = alloca %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction", align 4
  store %"class.std::__2::vector.89"* %this, %"class.std::__2::vector.89"** %this.addr, align 4
  store i32* %__args, i32** %__args.addr, align 4
  %this1 = load %"class.std::__2::vector.89"*, %"class.std::__2::vector.89"** %this.addr, align 4
  %call = call %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* @_ZNSt3__26vectorIiNS_9allocatorIiEEE21_ConstructTransactionC2ERS3_m(%"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* %__tx, %"class.std::__2::vector.89"* nonnull align 4 dereferenceable(12) %this1, i32 1)
  %0 = bitcast %"class.std::__2::vector.89"* %this1 to %"class.std::__2::__vector_base.90"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.94"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE7__allocEv(%"class.std::__2::__vector_base.90"* %0) #7
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction", %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %1 = load i32*, i32** %__pos_, align 4
  %call3 = call i32* @_ZNSt3__212__to_addressIiEEPT_S2_(i32* %1) #7
  %2 = load i32*, i32** %__args.addr, align 4
  %call4 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__27forwardIiEEOT_RNS_16remove_referenceIS1_E4typeE(i32* nonnull align 4 dereferenceable(4) %2) #7
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE9constructIiJiEEEvRS2_PT_DpOT0_(%"class.std::__2::allocator.94"* nonnull align 1 dereferenceable(1) %call2, i32* %call3, i32* nonnull align 4 dereferenceable(4) %call4)
  %__pos_5 = getelementptr inbounds %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction", %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %3 = load i32*, i32** %__pos_5, align 4
  %incdec.ptr = getelementptr inbounds i32, i32* %3, i32 1
  store i32* %incdec.ptr, i32** %__pos_5, align 4
  %call6 = call %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* @_ZNSt3__26vectorIiNS_9allocatorIiEEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* %__tx) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIiNS_9allocatorIiEEE21__push_back_slow_pathIiEEvOT_(%"class.std::__2::vector.89"* %this, i32* nonnull align 4 dereferenceable(4) %__x) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.89"*, align 4
  %__x.addr = alloca i32*, align 4
  %__a = alloca %"class.std::__2::allocator.94"*, align 4
  %__v = alloca %"struct.std::__2::__split_buffer", align 4
  store %"class.std::__2::vector.89"* %this, %"class.std::__2::vector.89"** %this.addr, align 4
  store i32* %__x, i32** %__x.addr, align 4
  %this1 = load %"class.std::__2::vector.89"*, %"class.std::__2::vector.89"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.89"* %this1 to %"class.std::__2::__vector_base.90"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.94"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE7__allocEv(%"class.std::__2::__vector_base.90"* %0) #7
  store %"class.std::__2::allocator.94"* %call, %"class.std::__2::allocator.94"** %__a, align 4
  %call2 = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4sizeEv(%"class.std::__2::vector.89"* %this1) #7
  %add = add i32 %call2, 1
  %call3 = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE11__recommendEm(%"class.std::__2::vector.89"* %this1, i32 %add)
  %call4 = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4sizeEv(%"class.std::__2::vector.89"* %this1) #7
  %1 = load %"class.std::__2::allocator.94"*, %"class.std::__2::allocator.94"** %__a, align 4
  %call5 = call %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEEC2EmmS3_(%"struct.std::__2::__split_buffer"* %__v, i32 %call3, i32 %call4, %"class.std::__2::allocator.94"* nonnull align 1 dereferenceable(1) %1)
  %2 = load %"class.std::__2::allocator.94"*, %"class.std::__2::allocator.94"** %__a, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %__v, i32 0, i32 2
  %3 = load i32*, i32** %__end_, align 4
  %call6 = call i32* @_ZNSt3__212__to_addressIiEEPT_S2_(i32* %3) #7
  %4 = load i32*, i32** %__x.addr, align 4
  %call7 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__27forwardIiEEOT_RNS_16remove_referenceIS1_E4typeE(i32* nonnull align 4 dereferenceable(4) %4) #7
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE9constructIiJiEEEvRS2_PT_DpOT0_(%"class.std::__2::allocator.94"* nonnull align 1 dereferenceable(1) %2, i32* %call6, i32* nonnull align 4 dereferenceable(4) %call7)
  %__end_8 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %__v, i32 0, i32 2
  %5 = load i32*, i32** %__end_8, align 4
  %incdec.ptr = getelementptr inbounds i32, i32* %5, i32 1
  store i32* %incdec.ptr, i32** %__end_8, align 4
  call void @_ZNSt3__26vectorIiNS_9allocatorIiEEE26__swap_out_circular_bufferERNS_14__split_bufferIiRS2_EE(%"class.std::__2::vector.89"* %this1, %"struct.std::__2::__split_buffer"* nonnull align 4 dereferenceable(20) %__v)
  %call9 = call %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEED2Ev(%"struct.std::__2::__split_buffer"* %__v) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* @_ZNSt3__26vectorIiNS_9allocatorIiEEE21_ConstructTransactionC2ERS3_m(%"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* returned %this, %"class.std::__2::vector.89"* nonnull align 4 dereferenceable(12) %__v, i32 %__n) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"*, align 4
  %__v.addr = alloca %"class.std::__2::vector.89"*, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"** %this.addr, align 4
  store %"class.std::__2::vector.89"* %__v, %"class.std::__2::vector.89"** %__v.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"*, %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"** %this.addr, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction", %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::vector.89"*, %"class.std::__2::vector.89"** %__v.addr, align 4
  store %"class.std::__2::vector.89"* %0, %"class.std::__2::vector.89"** %__v_, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction", %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %1 = load %"class.std::__2::vector.89"*, %"class.std::__2::vector.89"** %__v.addr, align 4
  %2 = bitcast %"class.std::__2::vector.89"* %1 to %"class.std::__2::__vector_base.90"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.90", %"class.std::__2::__vector_base.90"* %2, i32 0, i32 1
  %3 = load i32*, i32** %__end_, align 4
  store i32* %3, i32** %__pos_, align 4
  %__new_end_ = getelementptr inbounds %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction", %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* %this1, i32 0, i32 2
  %4 = load %"class.std::__2::vector.89"*, %"class.std::__2::vector.89"** %__v.addr, align 4
  %5 = bitcast %"class.std::__2::vector.89"* %4 to %"class.std::__2::__vector_base.90"*
  %__end_2 = getelementptr inbounds %"class.std::__2::__vector_base.90", %"class.std::__2::__vector_base.90"* %5, i32 0, i32 1
  %6 = load i32*, i32** %__end_2, align 4
  %7 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds i32, i32* %6, i32 %7
  store i32* %add.ptr, i32** %__new_end_, align 4
  ret %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE9constructIiJiEEEvRS2_PT_DpOT0_(%"class.std::__2::allocator.94"* nonnull align 1 dereferenceable(1) %__a, i32* %__p, i32* nonnull align 4 dereferenceable(4) %__args) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.94"*, align 4
  %__p.addr = alloca i32*, align 4
  %__args.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_construct", align 1
  store %"class.std::__2::allocator.94"* %__a, %"class.std::__2::allocator.94"** %__a.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  store i32* %__args, i32** %__args.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_construct"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.94"*, %"class.std::__2::allocator.94"** %__a.addr, align 4
  %2 = load i32*, i32** %__p.addr, align 4
  %3 = load i32*, i32** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__27forwardIiEEOT_RNS_16remove_referenceIS1_E4typeE(i32* nonnull align 4 dereferenceable(4) %3) #7
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE11__constructIiJiEEEvNS_17integral_constantIbLb1EEERS2_PT_DpOT0_(%"class.std::__2::allocator.94"* nonnull align 1 dereferenceable(1) %1, i32* %2, i32* nonnull align 4 dereferenceable(4) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__27forwardIiEEOT_RNS_16remove_referenceIS1_E4typeE(i32* nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca i32*, align 4
  store i32* %__t, i32** %__t.addr, align 4
  %0 = load i32*, i32** %__t.addr, align 4
  ret i32* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* @_ZNSt3__26vectorIiNS_9allocatorIiEEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"*, align 4
  store %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"** %this.addr, align 4
  %this1 = load %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"*, %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction", %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %0 = load i32*, i32** %__pos_, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction", %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::vector.89"*, %"class.std::__2::vector.89"** %__v_, align 4
  %2 = bitcast %"class.std::__2::vector.89"* %1 to %"class.std::__2::__vector_base.90"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.90", %"class.std::__2::__vector_base.90"* %2, i32 0, i32 1
  store i32* %0, i32** %__end_, align 4
  ret %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE11__constructIiJiEEEvNS_17integral_constantIbLb1EEERS2_PT_DpOT0_(%"class.std::__2::allocator.94"* nonnull align 1 dereferenceable(1) %__a, i32* %__p, i32* nonnull align 4 dereferenceable(4) %__args) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.94"*, align 4
  %__p.addr = alloca i32*, align 4
  %__args.addr = alloca i32*, align 4
  store %"class.std::__2::allocator.94"* %__a, %"class.std::__2::allocator.94"** %__a.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  store i32* %__args, i32** %__args.addr, align 4
  %1 = load %"class.std::__2::allocator.94"*, %"class.std::__2::allocator.94"** %__a.addr, align 4
  %2 = load i32*, i32** %__p.addr, align 4
  %3 = load i32*, i32** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__27forwardIiEEOT_RNS_16remove_referenceIS1_E4typeE(i32* nonnull align 4 dereferenceable(4) %3) #7
  call void @_ZNSt3__29allocatorIiE9constructIiJiEEEvPT_DpOT0_(%"class.std::__2::allocator.94"* %1, i32* %2, i32* nonnull align 4 dereferenceable(4) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIiE9constructIiJiEEEvPT_DpOT0_(%"class.std::__2::allocator.94"* %this, i32* %__p, i32* nonnull align 4 dereferenceable(4) %__args) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.94"*, align 4
  %__p.addr = alloca i32*, align 4
  %__args.addr = alloca i32*, align 4
  store %"class.std::__2::allocator.94"* %this, %"class.std::__2::allocator.94"** %this.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  store i32* %__args, i32** %__args.addr, align 4
  %this1 = load %"class.std::__2::allocator.94"*, %"class.std::__2::allocator.94"** %this.addr, align 4
  %0 = load i32*, i32** %__p.addr, align 4
  %1 = bitcast i32* %0 to i8*
  %2 = bitcast i8* %1 to i32*
  %3 = load i32*, i32** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__27forwardIiEEOT_RNS_16remove_referenceIS1_E4typeE(i32* nonnull align 4 dereferenceable(4) %3) #7
  %4 = load i32, i32* %call, align 4
  store i32 %4, i32* %2, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE11__recommendEm(%"class.std::__2::vector.89"* %this, i32 %__new_size) #0 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %"class.std::__2::vector.89"*, align 4
  %__new_size.addr = alloca i32, align 4
  %__ms = alloca i32, align 4
  %__cap = alloca i32, align 4
  %ref.tmp = alloca i32, align 4
  store %"class.std::__2::vector.89"* %this, %"class.std::__2::vector.89"** %this.addr, align 4
  store i32 %__new_size, i32* %__new_size.addr, align 4
  %this1 = load %"class.std::__2::vector.89"*, %"class.std::__2::vector.89"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE8max_sizeEv(%"class.std::__2::vector.89"* %this1) #7
  store i32 %call, i32* %__ms, align 4
  %0 = load i32, i32* %__new_size.addr, align 4
  %1 = load i32, i32* %__ms, align 4
  %cmp = icmp ugt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = bitcast %"class.std::__2::vector.89"* %this1 to %"class.std::__2::__vector_base_common"*
  call void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"* %2) #10
  unreachable

if.end:                                           ; preds = %entry
  %call2 = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::vector.89"* %this1) #7
  store i32 %call2, i32* %__cap, align 4
  %3 = load i32, i32* %__cap, align 4
  %4 = load i32, i32* %__ms, align 4
  %div = udiv i32 %4, 2
  %cmp3 = icmp uge i32 %3, %div
  br i1 %cmp3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %if.end
  %5 = load i32, i32* %__ms, align 4
  store i32 %5, i32* %retval, align 4
  br label %return

if.end5:                                          ; preds = %if.end
  %6 = load i32, i32* %__cap, align 4
  %mul = mul i32 2, %6
  store i32 %mul, i32* %ref.tmp, align 4
  %call6 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %__new_size.addr)
  %7 = load i32, i32* %call6, align 4
  store i32 %7, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end5, %if.then4
  %8 = load i32, i32* %retval, align 4
  ret i32 %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE8max_sizeEv(%"class.std::__2::vector.89"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.89"*, align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp3 = alloca i32, align 4
  store %"class.std::__2::vector.89"* %this, %"class.std::__2::vector.89"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.89"*, %"class.std::__2::vector.89"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.89"* %this1 to %"class.std::__2::__vector_base.90"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.94"* @_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE7__allocEv(%"class.std::__2::__vector_base.90"* %0) #7
  %call2 = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE8max_sizeERKS2_(%"class.std::__2::allocator.94"* nonnull align 1 dereferenceable(1) %call) #7
  store i32 %call2, i32* %ref.tmp, align 4
  %call4 = call i32 @_ZNSt3__214numeric_limitsIlE3maxEv() #7
  store i32 %call4, i32* %ref.tmp3, align 4
  %call5 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %1 = load i32, i32* %call5, align 4
  ret i32 %1
}

; Function Attrs: noreturn
declare void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"*) #6

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::__less.135", align 1
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__a.addr, align 4
  %1 = load i32*, i32** %__b.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::__less.135", align 1
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__a.addr, align 4
  %1 = load i32*, i32** %__b.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE8max_sizeERKS2_(%"class.std::__2::allocator.94"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.94"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_max_size", align 1
  store %"class.std::__2::allocator.94"* %__a, %"class.std::__2::allocator.94"** %__a.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_max_size"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.94"*, %"class.std::__2::allocator.94"** %__a.addr, align 4
  %call = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE10__max_sizeENS_17integral_constantIbLb1EEERKS2_(%"class.std::__2::allocator.94"* nonnull align 1 dereferenceable(1) %1) #7
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.94"* @_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE7__allocEv(%"class.std::__2::__vector_base.90"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.90"*, align 4
  store %"class.std::__2::__vector_base.90"* %this, %"class.std::__2::__vector_base.90"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.90"*, %"class.std::__2::__vector_base.90"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.90", %"class.std::__2::__vector_base.90"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.94"* @_ZNKSt3__217__compressed_pairIPiNS_9allocatorIiEEE6secondEv(%"class.std::__2::__compressed_pair.91"* %__end_cap_) #7
  ret %"class.std::__2::allocator.94"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__214numeric_limitsIlE3maxEv() #0 comdat {
entry:
  %call = call i32 @_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv() #7
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__comp = alloca %"struct.std::__2::__less.135", align 1
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__b.addr, align 4
  %1 = load i32*, i32** %__a.addr, align 4
  %call = call zeroext i1 @_ZNKSt3__26__lessImmEclERKmS3_(%"struct.std::__2::__less.135"* %__comp, i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  br i1 %call, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32*, i32** %__b.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %3 = load i32*, i32** %__a.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi i32* [ %2, %cond.true ], [ %3, %cond.false ]
  ret i32* %cond-lvalue
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__26__lessImmEclERKmS3_(%"struct.std::__2::__less.135"* %this, i32* nonnull align 4 dereferenceable(4) %__x, i32* nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__less.135"*, align 4
  %__x.addr = alloca i32*, align 4
  %__y.addr = alloca i32*, align 4
  store %"struct.std::__2::__less.135"* %this, %"struct.std::__2::__less.135"** %this.addr, align 4
  store i32* %__x, i32** %__x.addr, align 4
  store i32* %__y, i32** %__y.addr, align 4
  %this1 = load %"struct.std::__2::__less.135"*, %"struct.std::__2::__less.135"** %this.addr, align 4
  %0 = load i32*, i32** %__x.addr, align 4
  %1 = load i32, i32* %0, align 4
  %2 = load i32*, i32** %__y.addr, align 4
  %3 = load i32, i32* %2, align 4
  %cmp = icmp ult i32 %1, %3
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE10__max_sizeENS_17integral_constantIbLb1EEERKS2_(%"class.std::__2::allocator.94"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.94"*, align 4
  store %"class.std::__2::allocator.94"* %__a, %"class.std::__2::allocator.94"** %__a.addr, align 4
  %1 = load %"class.std::__2::allocator.94"*, %"class.std::__2::allocator.94"** %__a.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorIiE8max_sizeEv(%"class.std::__2::allocator.94"* %1) #7
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.94"* @_ZNKSt3__217__compressed_pairIPiNS_9allocatorIiEEE6secondEv(%"class.std::__2::__compressed_pair.91"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.91"*, align 4
  store %"class.std::__2::__compressed_pair.91"* %this, %"class.std::__2::__compressed_pair.91"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.91"*, %"class.std::__2::__compressed_pair.91"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.91"* %this1 to %"struct.std::__2::__compressed_pair_elem.93"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.94"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorIiEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.93"* %0) #7
  ret %"class.std::__2::allocator.94"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.94"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorIiEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.93"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.93"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.93"* %this, %"struct.std::__2::__compressed_pair_elem.93"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.93"*, %"struct.std::__2::__compressed_pair_elem.93"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.93"* %this1 to %"class.std::__2::allocator.94"*
  ret %"class.std::__2::allocator.94"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv() #0 comdat {
entry:
  ret i32 2147483647
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__comp = alloca %"struct.std::__2::__less.135", align 1
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__a.addr, align 4
  %1 = load i32*, i32** %__b.addr, align 4
  %call = call zeroext i1 @_ZNKSt3__26__lessImmEclERKmS3_(%"struct.std::__2::__less.135"* %__comp, i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  br i1 %call, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32*, i32** %__b.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %3 = load i32*, i32** %__a.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi i32* [ %2, %cond.true ], [ %3, %cond.false ]
  ret i32* %cond-lvalue
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer.136"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEEC2EmmS7_(%"struct.std::__2::__split_buffer.136"* returned %this, i32 %__cap, i32 %__start, %"class.std::__2::allocator.118"* nonnull align 1 dereferenceable(1) %__a) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"struct.std::__2::__split_buffer.136"*, align 4
  %this.addr = alloca %"struct.std::__2::__split_buffer.136"*, align 4
  %__cap.addr = alloca i32, align 4
  %__start.addr = alloca i32, align 4
  %__a.addr = alloca %"class.std::__2::allocator.118"*, align 4
  %ref.tmp = alloca i8*, align 4
  store %"struct.std::__2::__split_buffer.136"* %this, %"struct.std::__2::__split_buffer.136"** %this.addr, align 4
  store i32 %__cap, i32* %__cap.addr, align 4
  store i32 %__start, i32* %__start.addr, align 4
  store %"class.std::__2::allocator.118"* %__a, %"class.std::__2::allocator.118"** %__a.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.136"*, %"struct.std::__2::__split_buffer.136"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer.136"* %this1, %"struct.std::__2::__split_buffer.136"** %retval, align 4
  %0 = bitcast %"struct.std::__2::__split_buffer.136"* %this1 to %"class.std::__2::__split_buffer_common"*
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer.136", %"struct.std::__2::__split_buffer.136"* %this1, i32 0, i32 3
  store i8* null, i8** %ref.tmp, align 4
  %1 = load %"class.std::__2::allocator.118"*, %"class.std::__2::allocator.118"** %__a.addr, align 4
  %call = call %"class.std::__2::__compressed_pair.137"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEEC2IDnS8_EEOT_OT0_(%"class.std::__2::__compressed_pair.137"* %__end_cap_, i8** nonnull align 4 dereferenceable(4) %ref.tmp, %"class.std::__2::allocator.118"* nonnull align 1 dereferenceable(1) %1)
  %2 = load i32, i32* %__cap.addr, align 4
  %cmp = icmp ne i32 %2, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.118"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEE7__allocEv(%"struct.std::__2::__split_buffer.136"* %this1) #7
  %3 = load i32, i32* %__cap.addr, align 4
  %call3 = call %"class.draco::IndexType.114"* @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEEE8allocateERS6_m(%"class.std::__2::allocator.118"* nonnull align 1 dereferenceable(1) %call2, i32 %3)
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %"class.draco::IndexType.114"* [ %call3, %cond.true ], [ null, %cond.false ]
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer.136", %"struct.std::__2::__split_buffer.136"* %this1, i32 0, i32 0
  store %"class.draco::IndexType.114"* %cond, %"class.draco::IndexType.114"** %__first_, align 4
  %__first_4 = getelementptr inbounds %"struct.std::__2::__split_buffer.136", %"struct.std::__2::__split_buffer.136"* %this1, i32 0, i32 0
  %4 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__first_4, align 4
  %5 = load i32, i32* %__start.addr, align 4
  %add.ptr = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %4, i32 %5
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer.136", %"struct.std::__2::__split_buffer.136"* %this1, i32 0, i32 2
  store %"class.draco::IndexType.114"* %add.ptr, %"class.draco::IndexType.114"** %__end_, align 4
  %__begin_ = getelementptr inbounds %"struct.std::__2::__split_buffer.136", %"struct.std::__2::__split_buffer.136"* %this1, i32 0, i32 1
  store %"class.draco::IndexType.114"* %add.ptr, %"class.draco::IndexType.114"** %__begin_, align 4
  %__first_5 = getelementptr inbounds %"struct.std::__2::__split_buffer.136", %"struct.std::__2::__split_buffer.136"* %this1, i32 0, i32 0
  %6 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__first_5, align 4
  %7 = load i32, i32* %__cap.addr, align 4
  %add.ptr6 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %6, i32 %7
  %call7 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.114"** @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEE9__end_capEv(%"struct.std::__2::__split_buffer.136"* %this1) #7
  store %"class.draco::IndexType.114"* %add.ptr6, %"class.draco::IndexType.114"** %call7, align 4
  %8 = load %"struct.std::__2::__split_buffer.136"*, %"struct.std::__2::__split_buffer.136"** %retval, align 4
  ret %"struct.std::__2::__split_buffer.136"* %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS4_RS6_EE(%"class.std::__2::vector.112"* %this, %"struct.std::__2::__split_buffer.136"* nonnull align 4 dereferenceable(20) %__v) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.112"*, align 4
  %__v.addr = alloca %"struct.std::__2::__split_buffer.136"*, align 4
  store %"class.std::__2::vector.112"* %this, %"class.std::__2::vector.112"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer.136"* %__v, %"struct.std::__2::__split_buffer.136"** %__v.addr, align 4
  %this1 = load %"class.std::__2::vector.112"*, %"class.std::__2::vector.112"** %this.addr, align 4
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_deleteEv(%"class.std::__2::vector.112"* %this1) #7
  %0 = bitcast %"class.std::__2::vector.112"* %this1 to %"class.std::__2::__vector_base.113"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.118"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.113"* %0) #7
  %1 = bitcast %"class.std::__2::vector.112"* %this1 to %"class.std::__2::__vector_base.113"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.113", %"class.std::__2::__vector_base.113"* %1, i32 0, i32 0
  %2 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__begin_, align 4
  %3 = bitcast %"class.std::__2::vector.112"* %this1 to %"class.std::__2::__vector_base.113"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.113", %"class.std::__2::__vector_base.113"* %3, i32 0, i32 1
  %4 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__end_, align 4
  %5 = load %"struct.std::__2::__split_buffer.136"*, %"struct.std::__2::__split_buffer.136"** %__v.addr, align 4
  %__begin_2 = getelementptr inbounds %"struct.std::__2::__split_buffer.136", %"struct.std::__2::__split_buffer.136"* %5, i32 0, i32 1
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEEE46__construct_backward_with_exception_guaranteesIS5_EENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS6_PT_SC_EE5valuesr31is_trivially_move_constructibleISC_EE5valueEvE4typeERS6_SD_SD_RSD_(%"class.std::__2::allocator.118"* nonnull align 1 dereferenceable(1) %call, %"class.draco::IndexType.114"* %2, %"class.draco::IndexType.114"* %4, %"class.draco::IndexType.114"** nonnull align 4 dereferenceable(4) %__begin_2)
  %6 = bitcast %"class.std::__2::vector.112"* %this1 to %"class.std::__2::__vector_base.113"*
  %__begin_3 = getelementptr inbounds %"class.std::__2::__vector_base.113", %"class.std::__2::__vector_base.113"* %6, i32 0, i32 0
  %7 = load %"struct.std::__2::__split_buffer.136"*, %"struct.std::__2::__split_buffer.136"** %__v.addr, align 4
  %__begin_4 = getelementptr inbounds %"struct.std::__2::__split_buffer.136", %"struct.std::__2::__split_buffer.136"* %7, i32 0, i32 1
  call void @_ZNSt3__24swapIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS7_EE5valueEvE4typeERS7_SA_(%"class.draco::IndexType.114"** nonnull align 4 dereferenceable(4) %__begin_3, %"class.draco::IndexType.114"** nonnull align 4 dereferenceable(4) %__begin_4) #7
  %8 = bitcast %"class.std::__2::vector.112"* %this1 to %"class.std::__2::__vector_base.113"*
  %__end_5 = getelementptr inbounds %"class.std::__2::__vector_base.113", %"class.std::__2::__vector_base.113"* %8, i32 0, i32 1
  %9 = load %"struct.std::__2::__split_buffer.136"*, %"struct.std::__2::__split_buffer.136"** %__v.addr, align 4
  %__end_6 = getelementptr inbounds %"struct.std::__2::__split_buffer.136", %"struct.std::__2::__split_buffer.136"* %9, i32 0, i32 2
  call void @_ZNSt3__24swapIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS7_EE5valueEvE4typeERS7_SA_(%"class.draco::IndexType.114"** nonnull align 4 dereferenceable(4) %__end_5, %"class.draco::IndexType.114"** nonnull align 4 dereferenceable(4) %__end_6) #7
  %10 = bitcast %"class.std::__2::vector.112"* %this1 to %"class.std::__2::__vector_base.113"*
  %call7 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.114"** @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base.113"* %10) #7
  %11 = load %"struct.std::__2::__split_buffer.136"*, %"struct.std::__2::__split_buffer.136"** %__v.addr, align 4
  %call8 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.114"** @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEE9__end_capEv(%"struct.std::__2::__split_buffer.136"* %11) #7
  call void @_ZNSt3__24swapIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS7_EE5valueEvE4typeERS7_SA_(%"class.draco::IndexType.114"** nonnull align 4 dereferenceable(4) %call7, %"class.draco::IndexType.114"** nonnull align 4 dereferenceable(4) %call8) #7
  %12 = load %"struct.std::__2::__split_buffer.136"*, %"struct.std::__2::__split_buffer.136"** %__v.addr, align 4
  %__begin_9 = getelementptr inbounds %"struct.std::__2::__split_buffer.136", %"struct.std::__2::__split_buffer.136"* %12, i32 0, i32 1
  %13 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__begin_9, align 4
  %14 = load %"struct.std::__2::__split_buffer.136"*, %"struct.std::__2::__split_buffer.136"** %__v.addr, align 4
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer.136", %"struct.std::__2::__split_buffer.136"* %14, i32 0, i32 0
  store %"class.draco::IndexType.114"* %13, %"class.draco::IndexType.114"** %__first_, align 4
  %call10 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.112"* %this1) #7
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE14__annotate_newEm(%"class.std::__2::vector.112"* %this1, i32 %call10) #7
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector.112"* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer.136"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEED2Ev(%"struct.std::__2::__split_buffer.136"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"struct.std::__2::__split_buffer.136"*, align 4
  %this.addr = alloca %"struct.std::__2::__split_buffer.136"*, align 4
  store %"struct.std::__2::__split_buffer.136"* %this, %"struct.std::__2::__split_buffer.136"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.136"*, %"struct.std::__2::__split_buffer.136"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer.136"* %this1, %"struct.std::__2::__split_buffer.136"** %retval, align 4
  call void @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEE5clearEv(%"struct.std::__2::__split_buffer.136"* %this1) #7
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer.136", %"struct.std::__2::__split_buffer.136"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__first_, align 4
  %tobool = icmp ne %"class.draco::IndexType.114"* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.118"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEE7__allocEv(%"struct.std::__2::__split_buffer.136"* %this1) #7
  %__first_2 = getelementptr inbounds %"struct.std::__2::__split_buffer.136", %"struct.std::__2::__split_buffer.136"* %this1, i32 0, i32 0
  %1 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__first_2, align 4
  %call3 = call i32 @_ZNKSt3__214__split_bufferIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEE8capacityEv(%"struct.std::__2::__split_buffer.136"* %this1)
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEEE10deallocateERS6_PS5_m(%"class.std::__2::allocator.118"* nonnull align 1 dereferenceable(1) %call, %"class.draco::IndexType.114"* %1, i32 %call3) #7
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"struct.std::__2::__split_buffer.136"*, %"struct.std::__2::__split_buffer.136"** %retval, align 4
  ret %"struct.std::__2::__split_buffer.136"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.137"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEEC2IDnS8_EEOT_OT0_(%"class.std::__2::__compressed_pair.137"* returned %this, i8** nonnull align 4 dereferenceable(4) %__t1, %"class.std::__2::allocator.118"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.137"*, align 4
  %__t1.addr = alloca i8**, align 4
  %__t2.addr = alloca %"class.std::__2::allocator.118"*, align 4
  store %"class.std::__2::__compressed_pair.137"* %this, %"class.std::__2::__compressed_pair.137"** %this.addr, align 4
  store i8** %__t1, i8*** %__t1.addr, align 4
  store %"class.std::__2::allocator.118"* %__t2, %"class.std::__2::allocator.118"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.137"*, %"class.std::__2::__compressed_pair.137"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.137"* %this1 to %"struct.std::__2::__compressed_pair_elem.116"*
  %1 = load i8**, i8*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %1) #7
  %call2 = call %"struct.std::__2::__compressed_pair_elem.116"* @_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEELi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.116"* %0, i8** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.137"* %this1 to i8*
  %3 = getelementptr inbounds i8, i8* %2, i32 4
  %4 = bitcast i8* %3 to %"struct.std::__2::__compressed_pair_elem.138"*
  %5 = load %"class.std::__2::allocator.118"*, %"class.std::__2::allocator.118"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.118"* @_ZNSt3__27forwardIRNS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEEEEOT_RNS_16remove_referenceIS8_E4typeE(%"class.std::__2::allocator.118"* nonnull align 1 dereferenceable(1) %5) #7
  %call4 = call %"struct.std::__2::__compressed_pair_elem.138"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEELi1ELb0EEC2IS7_vEEOT_(%"struct.std::__2::__compressed_pair_elem.138"* %4, %"class.std::__2::allocator.118"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::__compressed_pair.137"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType.114"* @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEEE8allocateERS6_m(%"class.std::__2::allocator.118"* nonnull align 1 dereferenceable(1) %__a, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.118"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.118"* %__a, %"class.std::__2::allocator.118"** %__a.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.118"*, %"class.std::__2::allocator.118"** %__a.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call %"class.draco::IndexType.114"* @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEE8allocateEmPKv(%"class.std::__2::allocator.118"* %0, i32 %1, i8* null)
  ret %"class.draco::IndexType.114"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.118"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEE7__allocEv(%"struct.std::__2::__split_buffer.136"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.136"*, align 4
  store %"struct.std::__2::__split_buffer.136"* %this, %"struct.std::__2::__split_buffer.136"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.136"*, %"struct.std::__2::__split_buffer.136"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer.136", %"struct.std::__2::__split_buffer.136"* %this1, i32 0, i32 3
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.118"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.137"* %__end_cap_) #7
  ret %"class.std::__2::allocator.118"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.114"** @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEE9__end_capEv(%"struct.std::__2::__split_buffer.136"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.136"*, align 4
  store %"struct.std::__2::__split_buffer.136"* %this, %"struct.std::__2::__split_buffer.136"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.136"*, %"struct.std::__2::__split_buffer.136"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer.136", %"struct.std::__2::__split_buffer.136"* %this1, i32 0, i32 3
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.114"** @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.137"* %__end_cap_) #7
  ret %"class.draco::IndexType.114"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.118"* @_ZNSt3__27forwardIRNS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEEEEOT_RNS_16remove_referenceIS8_E4typeE(%"class.std::__2::allocator.118"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::allocator.118"*, align 4
  store %"class.std::__2::allocator.118"* %__t, %"class.std::__2::allocator.118"** %__t.addr, align 4
  %0 = load %"class.std::__2::allocator.118"*, %"class.std::__2::allocator.118"** %__t.addr, align 4
  ret %"class.std::__2::allocator.118"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.138"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEELi1ELb0EEC2IS7_vEEOT_(%"struct.std::__2::__compressed_pair_elem.138"* returned %this, %"class.std::__2::allocator.118"* nonnull align 1 dereferenceable(1) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.138"*, align 4
  %__u.addr = alloca %"class.std::__2::allocator.118"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.138"* %this, %"struct.std::__2::__compressed_pair_elem.138"** %this.addr, align 4
  store %"class.std::__2::allocator.118"* %__u, %"class.std::__2::allocator.118"** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.138"*, %"struct.std::__2::__compressed_pair_elem.138"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.138", %"struct.std::__2::__compressed_pair_elem.138"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::allocator.118"*, %"class.std::__2::allocator.118"** %__u.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.118"* @_ZNSt3__27forwardIRNS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEEEEOT_RNS_16remove_referenceIS8_E4typeE(%"class.std::__2::allocator.118"* nonnull align 1 dereferenceable(1) %0) #7
  store %"class.std::__2::allocator.118"* %call, %"class.std::__2::allocator.118"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.138"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType.114"* @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEE8allocateEmPKv(%"class.std::__2::allocator.118"* %this, i32 %__n, i8* %0) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.118"*, align 4
  %__n.addr = alloca i32, align 4
  %.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.118"* %this, %"class.std::__2::allocator.118"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::allocator.118"*, %"class.std::__2::allocator.118"** %this.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEE8max_sizeEv(%"class.std::__2::allocator.118"* %this1) #7
  %cmp = icmp ugt i32 %1, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__220__throw_length_errorEPKc(i8* getelementptr inbounds ([68 x i8], [68 x i8]* @.str.2, i32 0, i32 0)) #10
  unreachable

if.end:                                           ; preds = %entry
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  %call2 = call i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %mul, i32 4)
  %3 = bitcast i8* %call2 to %"class.draco::IndexType.114"*
  ret %"class.draco::IndexType.114"* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__29allocatorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEE8max_sizeEv(%"class.std::__2::allocator.118"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.118"*, align 4
  store %"class.std::__2::allocator.118"* %this, %"class.std::__2::allocator.118"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.118"*, %"class.std::__2::allocator.118"** %this.addr, align 4
  ret i32 1073741823
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.118"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.137"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.137"*, align 4
  store %"class.std::__2::__compressed_pair.137"* %this, %"class.std::__2::__compressed_pair.137"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.137"*, %"class.std::__2::__compressed_pair.137"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.137"* %this1 to i8*
  %add.ptr = getelementptr inbounds i8, i8* %0, i32 4
  %1 = bitcast i8* %add.ptr to %"struct.std::__2::__compressed_pair_elem.138"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.118"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.138"* %1) #7
  ret %"class.std::__2::allocator.118"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.118"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.138"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.138"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.138"* %this, %"struct.std::__2::__compressed_pair_elem.138"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.138"*, %"struct.std::__2::__compressed_pair_elem.138"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.138", %"struct.std::__2::__compressed_pair_elem.138"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::allocator.118"*, %"class.std::__2::allocator.118"** %__value_, align 4
  ret %"class.std::__2::allocator.118"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.114"** @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.137"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.137"*, align 4
  store %"class.std::__2::__compressed_pair.137"* %this, %"class.std::__2::__compressed_pair.137"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.137"*, %"class.std::__2::__compressed_pair.137"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.137"* %this1 to %"struct.std::__2::__compressed_pair_elem.116"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.114"** @_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.116"* %0) #7
  ret %"class.draco::IndexType.114"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.114"** @_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.116"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.116"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.116"* %this, %"struct.std::__2::__compressed_pair_elem.116"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.116"*, %"struct.std::__2::__compressed_pair_elem.116"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.116", %"struct.std::__2::__compressed_pair_elem.116"* %this1, i32 0, i32 0
  ret %"class.draco::IndexType.114"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEEE46__construct_backward_with_exception_guaranteesIS5_EENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS6_PT_SC_EE5valuesr31is_trivially_move_constructibleISC_EE5valueEvE4typeERS6_SD_SD_RSD_(%"class.std::__2::allocator.118"* nonnull align 1 dereferenceable(1) %0, %"class.draco::IndexType.114"* %__begin1, %"class.draco::IndexType.114"* %__end1, %"class.draco::IndexType.114"** nonnull align 4 dereferenceable(4) %__end2) #0 comdat {
entry:
  %.addr = alloca %"class.std::__2::allocator.118"*, align 4
  %__begin1.addr = alloca %"class.draco::IndexType.114"*, align 4
  %__end1.addr = alloca %"class.draco::IndexType.114"*, align 4
  %__end2.addr = alloca %"class.draco::IndexType.114"**, align 4
  %_Np = alloca i32, align 4
  store %"class.std::__2::allocator.118"* %0, %"class.std::__2::allocator.118"** %.addr, align 4
  store %"class.draco::IndexType.114"* %__begin1, %"class.draco::IndexType.114"** %__begin1.addr, align 4
  store %"class.draco::IndexType.114"* %__end1, %"class.draco::IndexType.114"** %__end1.addr, align 4
  store %"class.draco::IndexType.114"** %__end2, %"class.draco::IndexType.114"*** %__end2.addr, align 4
  %1 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__end1.addr, align 4
  %2 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__begin1.addr, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.draco::IndexType.114"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.draco::IndexType.114"* %2 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  store i32 %sub.ptr.div, i32* %_Np, align 4
  %3 = load i32, i32* %_Np, align 4
  %4 = load %"class.draco::IndexType.114"**, %"class.draco::IndexType.114"*** %__end2.addr, align 4
  %5 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %4, align 4
  %idx.neg = sub i32 0, %3
  %add.ptr = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %5, i32 %idx.neg
  store %"class.draco::IndexType.114"* %add.ptr, %"class.draco::IndexType.114"** %4, align 4
  %6 = load i32, i32* %_Np, align 4
  %cmp = icmp sgt i32 %6, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %7 = load %"class.draco::IndexType.114"**, %"class.draco::IndexType.114"*** %__end2.addr, align 4
  %8 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %7, align 4
  %9 = bitcast %"class.draco::IndexType.114"* %8 to i8*
  %10 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__begin1.addr, align 4
  %11 = bitcast %"class.draco::IndexType.114"* %10 to i8*
  %12 = load i32, i32* %_Np, align 4
  %mul = mul i32 %12, 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %11, i32 %mul, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__24swapIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS7_EE5valueEvE4typeERS7_SA_(%"class.draco::IndexType.114"** nonnull align 4 dereferenceable(4) %__x, %"class.draco::IndexType.114"** nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %__x.addr = alloca %"class.draco::IndexType.114"**, align 4
  %__y.addr = alloca %"class.draco::IndexType.114"**, align 4
  %__t = alloca %"class.draco::IndexType.114"*, align 4
  store %"class.draco::IndexType.114"** %__x, %"class.draco::IndexType.114"*** %__x.addr, align 4
  store %"class.draco::IndexType.114"** %__y, %"class.draco::IndexType.114"*** %__y.addr, align 4
  %0 = load %"class.draco::IndexType.114"**, %"class.draco::IndexType.114"*** %__x.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.114"** @_ZNSt3__24moveIRPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEONS_16remove_referenceIT_E4typeEOS8_(%"class.draco::IndexType.114"** nonnull align 4 dereferenceable(4) %0) #7
  %1 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %call, align 4
  store %"class.draco::IndexType.114"* %1, %"class.draco::IndexType.114"** %__t, align 4
  %2 = load %"class.draco::IndexType.114"**, %"class.draco::IndexType.114"*** %__y.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.114"** @_ZNSt3__24moveIRPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEONS_16remove_referenceIT_E4typeEOS8_(%"class.draco::IndexType.114"** nonnull align 4 dereferenceable(4) %2) #7
  %3 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %call1, align 4
  %4 = load %"class.draco::IndexType.114"**, %"class.draco::IndexType.114"*** %__x.addr, align 4
  store %"class.draco::IndexType.114"* %3, %"class.draco::IndexType.114"** %4, align 4
  %call2 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.114"** @_ZNSt3__24moveIRPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEONS_16remove_referenceIT_E4typeEOS8_(%"class.draco::IndexType.114"** nonnull align 4 dereferenceable(4) %__t) #7
  %5 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %call2, align 4
  %6 = load %"class.draco::IndexType.114"**, %"class.draco::IndexType.114"*** %__y.addr, align 4
  store %"class.draco::IndexType.114"* %5, %"class.draco::IndexType.114"** %6, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.114"** @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base.113"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.113"*, align 4
  store %"class.std::__2::__vector_base.113"* %this, %"class.std::__2::__vector_base.113"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.113"*, %"class.std::__2::__vector_base.113"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.113", %"class.std::__2::__vector_base.113"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.114"** @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.115"* %__end_cap_) #7
  ret %"class.draco::IndexType.114"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE14__annotate_newEm(%"class.std::__2::vector.112"* %this, i32 %__current_size) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.112"*, align 4
  %__current_size.addr = alloca i32, align 4
  store %"class.std::__2::vector.112"* %this, %"class.std::__2::vector.112"** %this.addr, align 4
  store i32 %__current_size, i32* %__current_size.addr, align 4
  %this1 = load %"class.std::__2::vector.112"*, %"class.std::__2::vector.112"** %this.addr, align 4
  %call = call %"class.draco::IndexType.114"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.112"* %this1) #7
  %0 = bitcast %"class.draco::IndexType.114"* %call to i8*
  %call2 = call %"class.draco::IndexType.114"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.112"* %this1) #7
  %call3 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.112"* %this1) #7
  %add.ptr = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %call2, i32 %call3
  %1 = bitcast %"class.draco::IndexType.114"* %add.ptr to i8*
  %call4 = call %"class.draco::IndexType.114"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.112"* %this1) #7
  %call5 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.112"* %this1) #7
  %add.ptr6 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %call4, i32 %call5
  %2 = bitcast %"class.draco::IndexType.114"* %add.ptr6 to i8*
  %call7 = call %"class.draco::IndexType.114"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.112"* %this1) #7
  %3 = load i32, i32* %__current_size.addr, align 4
  %add.ptr8 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %call7, i32 %3
  %4 = bitcast %"class.draco::IndexType.114"* %add.ptr8 to i8*
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_(%"class.std::__2::vector.112"* %this1, i8* %0, i8* %1, i8* %2, i8* %4) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector.112"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.112"*, align 4
  store %"class.std::__2::vector.112"* %this, %"class.std::__2::vector.112"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.112"*, %"class.std::__2::vector.112"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.114"** @_ZNSt3__24moveIRPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEONS_16remove_referenceIT_E4typeEOS8_(%"class.draco::IndexType.114"** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.draco::IndexType.114"**, align 4
  store %"class.draco::IndexType.114"** %__t, %"class.draco::IndexType.114"*** %__t.addr, align 4
  %0 = load %"class.draco::IndexType.114"**, %"class.draco::IndexType.114"*** %__t.addr, align 4
  ret %"class.draco::IndexType.114"** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.114"** @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.115"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.115"*, align 4
  store %"class.std::__2::__compressed_pair.115"* %this, %"class.std::__2::__compressed_pair.115"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.115"*, %"class.std::__2::__compressed_pair.115"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.115"* %this1 to %"struct.std::__2::__compressed_pair_elem.116"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.114"** @_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.116"* %0) #7
  ret %"class.draco::IndexType.114"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEE5clearEv(%"struct.std::__2::__split_buffer.136"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.136"*, align 4
  store %"struct.std::__2::__split_buffer.136"* %this, %"struct.std::__2::__split_buffer.136"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.136"*, %"struct.std::__2::__split_buffer.136"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"struct.std::__2::__split_buffer.136", %"struct.std::__2::__split_buffer.136"* %this1, i32 0, i32 1
  %0 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__begin_, align 4
  call void @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEE17__destruct_at_endEPS4_(%"struct.std::__2::__split_buffer.136"* %this1, %"class.draco::IndexType.114"* %0) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__214__split_bufferIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEE8capacityEv(%"struct.std::__2::__split_buffer.136"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.136"*, align 4
  store %"struct.std::__2::__split_buffer.136"* %this, %"struct.std::__2::__split_buffer.136"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.136"*, %"struct.std::__2::__split_buffer.136"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.114"** @_ZNKSt3__214__split_bufferIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEE9__end_capEv(%"struct.std::__2::__split_buffer.136"* %this1) #7
  %0 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %call, align 4
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer.136", %"struct.std::__2::__split_buffer.136"* %this1, i32 0, i32 0
  %1 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__first_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.draco::IndexType.114"* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.draco::IndexType.114"* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEE17__destruct_at_endEPS4_(%"struct.std::__2::__split_buffer.136"* %this, %"class.draco::IndexType.114"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.136"*, align 4
  %__new_last.addr = alloca %"class.draco::IndexType.114"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant.134", align 1
  store %"struct.std::__2::__split_buffer.136"* %this, %"struct.std::__2::__split_buffer.136"** %this.addr, align 4
  store %"class.draco::IndexType.114"* %__new_last, %"class.draco::IndexType.114"** %__new_last.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.136"*, %"struct.std::__2::__split_buffer.136"** %this.addr, align 4
  %0 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__new_last.addr, align 4
  call void @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEE17__destruct_at_endEPS4_NS_17integral_constantIbLb0EEE(%"struct.std::__2::__split_buffer.136"* %this1, %"class.draco::IndexType.114"* %0) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEE17__destruct_at_endEPS4_NS_17integral_constantIbLb0EEE(%"struct.std::__2::__split_buffer.136"* %this, %"class.draco::IndexType.114"* %__new_last) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant.134", align 1
  %this.addr = alloca %"struct.std::__2::__split_buffer.136"*, align 4
  %__new_last.addr = alloca %"class.draco::IndexType.114"*, align 4
  store %"struct.std::__2::__split_buffer.136"* %this, %"struct.std::__2::__split_buffer.136"** %this.addr, align 4
  store %"class.draco::IndexType.114"* %__new_last, %"class.draco::IndexType.114"** %__new_last.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.136"*, %"struct.std::__2::__split_buffer.136"** %this.addr, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__new_last.addr, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer.136", %"struct.std::__2::__split_buffer.136"* %this1, i32 0, i32 2
  %2 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__end_, align 4
  %cmp = icmp ne %"class.draco::IndexType.114"* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.118"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEE7__allocEv(%"struct.std::__2::__split_buffer.136"* %this1) #7
  %__end_2 = getelementptr inbounds %"struct.std::__2::__split_buffer.136", %"struct.std::__2::__split_buffer.136"* %this1, i32 0, i32 2
  %3 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__end_2, align 4
  %incdec.ptr = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %3, i32 -1
  store %"class.draco::IndexType.114"* %incdec.ptr, %"class.draco::IndexType.114"** %__end_2, align 4
  %call3 = call %"class.draco::IndexType.114"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType.114"* %incdec.ptr) #7
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEEE7destroyIS5_EEvRS6_PT_(%"class.std::__2::allocator.118"* nonnull align 1 dereferenceable(1) %call, %"class.draco::IndexType.114"* %call3)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.114"** @_ZNKSt3__214__split_bufferIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEE9__end_capEv(%"struct.std::__2::__split_buffer.136"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.136"*, align 4
  store %"struct.std::__2::__split_buffer.136"* %this, %"struct.std::__2::__split_buffer.136"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.136"*, %"struct.std::__2::__split_buffer.136"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer.136", %"struct.std::__2::__split_buffer.136"* %this1, i32 0, i32 3
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.114"** @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.137"* %__end_cap_) #7
  ret %"class.draco::IndexType.114"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.114"** @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.137"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.137"*, align 4
  store %"class.std::__2::__compressed_pair.137"* %this, %"class.std::__2::__compressed_pair.137"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.137"*, %"class.std::__2::__compressed_pair.137"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.137"* %this1 to %"struct.std::__2::__compressed_pair_elem.116"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.114"** @_ZNKSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.116"* %0) #7
  ret %"class.draco::IndexType.114"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE22__construct_one_at_endIJRKS4_EEEvDpOT_(%"class.std::__2::vector.112"* %this, %"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %__args) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.112"*, align 4
  %__args.addr = alloca %"class.draco::IndexType.114"*, align 4
  %__tx = alloca %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>>>::_ConstructTransaction", align 4
  store %"class.std::__2::vector.112"* %this, %"class.std::__2::vector.112"** %this.addr, align 4
  store %"class.draco::IndexType.114"* %__args, %"class.draco::IndexType.114"** %__args.addr, align 4
  %this1 = load %"class.std::__2::vector.112"*, %"class.std::__2::vector.112"** %this.addr, align 4
  %call = call %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>>>::_ConstructTransaction"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionC2ERS7_m(%"struct.std::__2::vector<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>>>::_ConstructTransaction"* %__tx, %"class.std::__2::vector.112"* nonnull align 4 dereferenceable(12) %this1, i32 1)
  %0 = bitcast %"class.std::__2::vector.112"* %this1 to %"class.std::__2::__vector_base.113"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.118"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.113"* %0) #7
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %1 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__pos_, align 4
  %call3 = call %"class.draco::IndexType.114"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType.114"* %1) #7
  %2 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__args.addr, align 4
  %call4 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.114"* @_ZNSt3__27forwardIRKN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %2) #7
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEEE9constructIS5_JRKS5_EEEvRS6_PT_DpOT0_(%"class.std::__2::allocator.118"* nonnull align 1 dereferenceable(1) %call2, %"class.draco::IndexType.114"* %call3, %"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %call4)
  %__pos_5 = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %3 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__pos_5, align 4
  %incdec.ptr = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %3, i32 1
  store %"class.draco::IndexType.114"* %incdec.ptr, %"class.draco::IndexType.114"** %__pos_5, align 4
  %call6 = call %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>>>::_ConstructTransaction"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>>>::_ConstructTransaction"* %__tx) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE21__push_back_slow_pathIRKS4_EEvOT_(%"class.std::__2::vector.112"* %this, %"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %__x) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.112"*, align 4
  %__x.addr = alloca %"class.draco::IndexType.114"*, align 4
  %__a = alloca %"class.std::__2::allocator.118"*, align 4
  %__v = alloca %"struct.std::__2::__split_buffer.136", align 4
  store %"class.std::__2::vector.112"* %this, %"class.std::__2::vector.112"** %this.addr, align 4
  store %"class.draco::IndexType.114"* %__x, %"class.draco::IndexType.114"** %__x.addr, align 4
  %this1 = load %"class.std::__2::vector.112"*, %"class.std::__2::vector.112"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.112"* %this1 to %"class.std::__2::__vector_base.113"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.118"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.113"* %0) #7
  store %"class.std::__2::allocator.118"* %call, %"class.std::__2::allocator.118"** %__a, align 4
  %call2 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.112"* %this1) #7
  %add = add i32 %call2, 1
  %call3 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE11__recommendEm(%"class.std::__2::vector.112"* %this1, i32 %add)
  %call4 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.112"* %this1) #7
  %1 = load %"class.std::__2::allocator.118"*, %"class.std::__2::allocator.118"** %__a, align 4
  %call5 = call %"struct.std::__2::__split_buffer.136"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEEC2EmmS7_(%"struct.std::__2::__split_buffer.136"* %__v, i32 %call3, i32 %call4, %"class.std::__2::allocator.118"* nonnull align 1 dereferenceable(1) %1)
  %2 = load %"class.std::__2::allocator.118"*, %"class.std::__2::allocator.118"** %__a, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer.136", %"struct.std::__2::__split_buffer.136"* %__v, i32 0, i32 2
  %3 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__end_, align 4
  %call6 = call %"class.draco::IndexType.114"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType.114"* %3) #7
  %4 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__x.addr, align 4
  %call7 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.114"* @_ZNSt3__27forwardIRKN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %4) #7
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEEE9constructIS5_JRKS5_EEEvRS6_PT_DpOT0_(%"class.std::__2::allocator.118"* nonnull align 1 dereferenceable(1) %2, %"class.draco::IndexType.114"* %call6, %"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %call7)
  %__end_8 = getelementptr inbounds %"struct.std::__2::__split_buffer.136", %"struct.std::__2::__split_buffer.136"* %__v, i32 0, i32 2
  %5 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__end_8, align 4
  %incdec.ptr = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %5, i32 1
  store %"class.draco::IndexType.114"* %incdec.ptr, %"class.draco::IndexType.114"** %__end_8, align 4
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS4_RS6_EE(%"class.std::__2::vector.112"* %this1, %"struct.std::__2::__split_buffer.136"* nonnull align 4 dereferenceable(20) %__v)
  %call9 = call %"struct.std::__2::__split_buffer.136"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS_9allocatorIS4_EEED2Ev(%"struct.std::__2::__split_buffer.136"* %__v) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>>>::_ConstructTransaction"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionC2ERS7_m(%"struct.std::__2::vector<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>>>::_ConstructTransaction"* returned %this, %"class.std::__2::vector.112"* nonnull align 4 dereferenceable(12) %__v, i32 %__n) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>>>::_ConstructTransaction"*, align 4
  %__v.addr = alloca %"class.std::__2::vector.112"*, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>>>::_ConstructTransaction"** %this.addr, align 4
  store %"class.std::__2::vector.112"* %__v, %"class.std::__2::vector.112"** %__v.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>>>::_ConstructTransaction"*, %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>>>::_ConstructTransaction"** %this.addr, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::vector.112"*, %"class.std::__2::vector.112"** %__v.addr, align 4
  store %"class.std::__2::vector.112"* %0, %"class.std::__2::vector.112"** %__v_, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %1 = load %"class.std::__2::vector.112"*, %"class.std::__2::vector.112"** %__v.addr, align 4
  %2 = bitcast %"class.std::__2::vector.112"* %1 to %"class.std::__2::__vector_base.113"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.113", %"class.std::__2::__vector_base.113"* %2, i32 0, i32 1
  %3 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__end_, align 4
  store %"class.draco::IndexType.114"* %3, %"class.draco::IndexType.114"** %__pos_, align 4
  %__new_end_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>>>::_ConstructTransaction"* %this1, i32 0, i32 2
  %4 = load %"class.std::__2::vector.112"*, %"class.std::__2::vector.112"** %__v.addr, align 4
  %5 = bitcast %"class.std::__2::vector.112"* %4 to %"class.std::__2::__vector_base.113"*
  %__end_2 = getelementptr inbounds %"class.std::__2::__vector_base.113", %"class.std::__2::__vector_base.113"* %5, i32 0, i32 1
  %6 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__end_2, align 4
  %7 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %6, i32 %7
  store %"class.draco::IndexType.114"* %add.ptr, %"class.draco::IndexType.114"** %__new_end_, align 4
  ret %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEEE9constructIS5_JRKS5_EEEvRS6_PT_DpOT0_(%"class.std::__2::allocator.118"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType.114"* %__p, %"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %__args) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.118"*, align 4
  %__p.addr = alloca %"class.draco::IndexType.114"*, align 4
  %__args.addr = alloca %"class.draco::IndexType.114"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_construct.139", align 1
  store %"class.std::__2::allocator.118"* %__a, %"class.std::__2::allocator.118"** %__a.addr, align 4
  store %"class.draco::IndexType.114"* %__p, %"class.draco::IndexType.114"** %__p.addr, align 4
  store %"class.draco::IndexType.114"* %__args, %"class.draco::IndexType.114"** %__args.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_construct.139"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.118"*, %"class.std::__2::allocator.118"** %__a.addr, align 4
  %2 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__p.addr, align 4
  %3 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.114"* @_ZNSt3__27forwardIRKN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %3) #7
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEEE11__constructIS5_JRKS5_EEEvNS_17integral_constantIbLb1EEERS6_PT_DpOT0_(%"class.std::__2::allocator.118"* nonnull align 1 dereferenceable(1) %1, %"class.draco::IndexType.114"* %2, %"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.114"* @_ZNSt3__27forwardIRKN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.draco::IndexType.114"*, align 4
  store %"class.draco::IndexType.114"* %__t, %"class.draco::IndexType.114"** %__t.addr, align 4
  %0 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__t.addr, align 4
  ret %"class.draco::IndexType.114"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>>>::_ConstructTransaction"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>>>::_ConstructTransaction"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>>>::_ConstructTransaction"*, align 4
  store %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>>>::_ConstructTransaction"** %this.addr, align 4
  %this1 = load %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>>>::_ConstructTransaction"*, %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>>>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %0 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__pos_, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::vector.112"*, %"class.std::__2::vector.112"** %__v_, align 4
  %2 = bitcast %"class.std::__2::vector.112"* %1 to %"class.std::__2::__vector_base.113"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.113", %"class.std::__2::__vector_base.113"* %2, i32 0, i32 1
  store %"class.draco::IndexType.114"* %0, %"class.draco::IndexType.114"** %__end_, align 4
  ret %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::FaceIndex_tag_type_>>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEEE11__constructIS5_JRKS5_EEEvNS_17integral_constantIbLb1EEERS6_PT_DpOT0_(%"class.std::__2::allocator.118"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType.114"* %__p, %"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %__args) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.118"*, align 4
  %__p.addr = alloca %"class.draco::IndexType.114"*, align 4
  %__args.addr = alloca %"class.draco::IndexType.114"*, align 4
  store %"class.std::__2::allocator.118"* %__a, %"class.std::__2::allocator.118"** %__a.addr, align 4
  store %"class.draco::IndexType.114"* %__p, %"class.draco::IndexType.114"** %__p.addr, align 4
  store %"class.draco::IndexType.114"* %__args, %"class.draco::IndexType.114"** %__args.addr, align 4
  %1 = load %"class.std::__2::allocator.118"*, %"class.std::__2::allocator.118"** %__a.addr, align 4
  %2 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__p.addr, align 4
  %3 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.114"* @_ZNSt3__27forwardIRKN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %3) #7
  call void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEE9constructIS4_JRKS4_EEEvPT_DpOT0_(%"class.std::__2::allocator.118"* %1, %"class.draco::IndexType.114"* %2, %"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEE9constructIS4_JRKS4_EEEvPT_DpOT0_(%"class.std::__2::allocator.118"* %this, %"class.draco::IndexType.114"* %__p, %"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %__args) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.118"*, align 4
  %__p.addr = alloca %"class.draco::IndexType.114"*, align 4
  %__args.addr = alloca %"class.draco::IndexType.114"*, align 4
  store %"class.std::__2::allocator.118"* %this, %"class.std::__2::allocator.118"** %this.addr, align 4
  store %"class.draco::IndexType.114"* %__p, %"class.draco::IndexType.114"** %__p.addr, align 4
  store %"class.draco::IndexType.114"* %__args, %"class.draco::IndexType.114"** %__args.addr, align 4
  %this1 = load %"class.std::__2::allocator.118"*, %"class.std::__2::allocator.118"** %this.addr, align 4
  %0 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__p.addr, align 4
  %1 = bitcast %"class.draco::IndexType.114"* %0 to i8*
  %2 = bitcast i8* %1 to %"class.draco::IndexType.114"*
  %3 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.114"* @_ZNSt3__27forwardIRKN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %3) #7
  %4 = bitcast %"class.draco::IndexType.114"* %2 to i8*
  %5 = bitcast %"class.draco::IndexType.114"* %call to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 4, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE11__recommendEm(%"class.std::__2::vector.112"* %this, i32 %__new_size) #0 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %"class.std::__2::vector.112"*, align 4
  %__new_size.addr = alloca i32, align 4
  %__ms = alloca i32, align 4
  %__cap = alloca i32, align 4
  %ref.tmp = alloca i32, align 4
  store %"class.std::__2::vector.112"* %this, %"class.std::__2::vector.112"** %this.addr, align 4
  store i32 %__new_size, i32* %__new_size.addr, align 4
  %this1 = load %"class.std::__2::vector.112"*, %"class.std::__2::vector.112"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE8max_sizeEv(%"class.std::__2::vector.112"* %this1) #7
  store i32 %call, i32* %__ms, align 4
  %0 = load i32, i32* %__new_size.addr, align 4
  %1 = load i32, i32* %__ms, align 4
  %cmp = icmp ugt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = bitcast %"class.std::__2::vector.112"* %this1 to %"class.std::__2::__vector_base_common"*
  call void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"* %2) #10
  unreachable

if.end:                                           ; preds = %entry
  %call2 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.112"* %this1) #7
  store i32 %call2, i32* %__cap, align 4
  %3 = load i32, i32* %__cap, align 4
  %4 = load i32, i32* %__ms, align 4
  %div = udiv i32 %4, 2
  %cmp3 = icmp uge i32 %3, %div
  br i1 %cmp3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %if.end
  %5 = load i32, i32* %__ms, align 4
  store i32 %5, i32* %retval, align 4
  br label %return

if.end5:                                          ; preds = %if.end
  %6 = load i32, i32* %__cap, align 4
  %mul = mul i32 2, %6
  store i32 %mul, i32* %ref.tmp, align 4
  %call6 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %__new_size.addr)
  %7 = load i32, i32* %call6, align 4
  store i32 %7, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end5, %if.then4
  %8 = load i32, i32* %retval, align 4
  ret i32 %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE8max_sizeEv(%"class.std::__2::vector.112"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.112"*, align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp3 = alloca i32, align 4
  store %"class.std::__2::vector.112"* %this, %"class.std::__2::vector.112"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.112"*, %"class.std::__2::vector.112"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.112"* %this1 to %"class.std::__2::__vector_base.113"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.118"* @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.113"* %0) #7
  %call2 = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEEE8max_sizeERKS6_(%"class.std::__2::allocator.118"* nonnull align 1 dereferenceable(1) %call) #7
  store i32 %call2, i32* %ref.tmp, align 4
  %call4 = call i32 @_ZNSt3__214numeric_limitsIlE3maxEv() #7
  store i32 %call4, i32* %ref.tmp3, align 4
  %call5 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %1 = load i32, i32* %call5, align 4
  ret i32 %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEEE8max_sizeERKS6_(%"class.std::__2::allocator.118"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.118"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_max_size.140", align 1
  store %"class.std::__2::allocator.118"* %__a, %"class.std::__2::allocator.118"** %__a.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_max_size.140"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.118"*, %"class.std::__2::allocator.118"** %__a.addr, align 4
  %call = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS6_(%"class.std::__2::allocator.118"* nonnull align 1 dereferenceable(1) %1) #7
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.118"* @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.113"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.113"*, align 4
  store %"class.std::__2::__vector_base.113"* %this, %"class.std::__2::__vector_base.113"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.113"*, %"class.std::__2::__vector_base.113"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.113", %"class.std::__2::__vector_base.113"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.118"* @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.115"* %__end_cap_) #7
  ret %"class.std::__2::allocator.118"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS6_(%"class.std::__2::allocator.118"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.118"*, align 4
  store %"class.std::__2::allocator.118"* %__a, %"class.std::__2::allocator.118"** %__a.addr, align 4
  %1 = load %"class.std::__2::allocator.118"*, %"class.std::__2::allocator.118"** %__a.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEE8max_sizeEv(%"class.std::__2::allocator.118"* %1) #7
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.118"* @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.115"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.115"*, align 4
  store %"class.std::__2::__compressed_pair.115"* %this, %"class.std::__2::__compressed_pair.115"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.115"*, %"class.std::__2::__compressed_pair.115"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.115"* %this1 to %"struct.std::__2::__compressed_pair_elem.117"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.118"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.117"* %0) #7
  ret %"class.std::__2::allocator.118"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.118"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.117"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.117"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.117"* %this, %"struct.std::__2::__compressed_pair_elem.117"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.117"*, %"struct.std::__2::__compressed_pair_elem.117"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.117"* %this1 to %"class.std::__2::allocator.118"*
  ret %"class.std::__2::allocator.118"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType.114"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE11__make_iterEPS4_(%"class.std::__2::vector.112"* %this, %"class.draco::IndexType.114"* %__p) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__wrap_iter", align 4
  %this.addr = alloca %"class.std::__2::vector.112"*, align 4
  %__p.addr = alloca %"class.draco::IndexType.114"*, align 4
  store %"class.std::__2::vector.112"* %this, %"class.std::__2::vector.112"** %this.addr, align 4
  store %"class.draco::IndexType.114"* %__p, %"class.draco::IndexType.114"** %__p.addr, align 4
  %this1 = load %"class.std::__2::vector.112"*, %"class.std::__2::vector.112"** %this.addr, align 4
  %0 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__p.addr, align 4
  %call = call %"class.std::__2::__wrap_iter"* @_ZNSt3__211__wrap_iterIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEC2ES5_(%"class.std::__2::__wrap_iter"* %retval, %"class.draco::IndexType.114"* %0) #7
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %retval, i32 0, i32 0
  %1 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %coerce.dive, align 4
  ret %"class.draco::IndexType.114"* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__wrap_iter"* @_ZNSt3__211__wrap_iterIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEC2ES5_(%"class.std::__2::__wrap_iter"* returned %this, %"class.draco::IndexType.114"* %__x) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__wrap_iter"*, align 4
  %__x.addr = alloca %"class.draco::IndexType.114"*, align 4
  store %"class.std::__2::__wrap_iter"* %this, %"class.std::__2::__wrap_iter"** %this.addr, align 4
  store %"class.draco::IndexType.114"* %__x, %"class.draco::IndexType.114"** %__x.addr, align 4
  %this1 = load %"class.std::__2::__wrap_iter"*, %"class.std::__2::__wrap_iter"** %this.addr, align 4
  %__i = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__x.addr, align 4
  store %"class.draco::IndexType.114"* %0, %"class.draco::IndexType.114"** %__i, align 4
  ret %"class.std::__2::__wrap_iter"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__24sortIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEERNS1_17MeshAreEquivalent13FaceIndexLessEEEvT_S9_T0_(%"class.draco::IndexType.114"* %__first, %"class.draco::IndexType.114"* %__last, %"struct.draco::MeshAreEquivalent::FaceIndexLess"* nonnull align 4 dereferenceable(4) %__comp) #0 comdat {
entry:
  %__first.addr = alloca %"class.draco::IndexType.114"*, align 4
  %__last.addr = alloca %"class.draco::IndexType.114"*, align 4
  %__comp.addr = alloca %"struct.draco::MeshAreEquivalent::FaceIndexLess"*, align 4
  store %"class.draco::IndexType.114"* %__first, %"class.draco::IndexType.114"** %__first.addr, align 4
  store %"class.draco::IndexType.114"* %__last, %"class.draco::IndexType.114"** %__last.addr, align 4
  store %"struct.draco::MeshAreEquivalent::FaceIndexLess"* %__comp, %"struct.draco::MeshAreEquivalent::FaceIndexLess"** %__comp.addr, align 4
  %0 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__first.addr, align 4
  %1 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__last.addr, align 4
  %2 = load %"struct.draco::MeshAreEquivalent::FaceIndexLess"*, %"struct.draco::MeshAreEquivalent::FaceIndexLess"** %__comp.addr, align 4
  call void @_ZNSt3__26__sortIRN5draco17MeshAreEquivalent13FaceIndexLessEPNS1_9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEvT0_S9_T_(%"class.draco::IndexType.114"* %0, %"class.draco::IndexType.114"* %1, %"struct.draco::MeshAreEquivalent::FaceIndexLess"* nonnull align 4 dereferenceable(4) %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType.114"* @_ZNKSt3__211__wrap_iterIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEE4baseEv(%"class.std::__2::__wrap_iter"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__wrap_iter"*, align 4
  store %"class.std::__2::__wrap_iter"* %this, %"class.std::__2::__wrap_iter"** %this.addr, align 4
  %this1 = load %"class.std::__2::__wrap_iter"*, %"class.std::__2::__wrap_iter"** %this.addr, align 4
  %__i = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__i, align 4
  ret %"class.draco::IndexType.114"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26__sortIRN5draco17MeshAreEquivalent13FaceIndexLessEPNS1_9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEvT0_S9_T_(%"class.draco::IndexType.114"* %__first, %"class.draco::IndexType.114"* %__last, %"struct.draco::MeshAreEquivalent::FaceIndexLess"* nonnull align 4 dereferenceable(4) %__comp) #0 comdat {
entry:
  %__first.addr = alloca %"class.draco::IndexType.114"*, align 4
  %__last.addr = alloca %"class.draco::IndexType.114"*, align 4
  %__comp.addr = alloca %"struct.draco::MeshAreEquivalent::FaceIndexLess"*, align 4
  %__limit = alloca i32, align 4
  %__len = alloca i32, align 4
  %agg.tmp = alloca %"class.draco::IndexType.114", align 4
  %agg.tmp2 = alloca %"class.draco::IndexType.114", align 4
  %__m = alloca %"class.draco::IndexType.114"*, align 4
  %__lm1 = alloca %"class.draco::IndexType.114"*, align 4
  %__n_swaps = alloca i32, align 4
  %__delta = alloca i32, align 4
  %__i = alloca %"class.draco::IndexType.114"*, align 4
  %__j = alloca %"class.draco::IndexType.114"*, align 4
  %agg.tmp32 = alloca %"class.draco::IndexType.114", align 4
  %agg.tmp33 = alloca %"class.draco::IndexType.114", align 4
  %agg.tmp44 = alloca %"class.draco::IndexType.114", align 4
  %agg.tmp45 = alloca %"class.draco::IndexType.114", align 4
  %agg.tmp56 = alloca %"class.draco::IndexType.114", align 4
  %agg.tmp57 = alloca %"class.draco::IndexType.114", align 4
  %agg.tmp72 = alloca %"class.draco::IndexType.114", align 4
  %agg.tmp73 = alloca %"class.draco::IndexType.114", align 4
  %agg.tmp81 = alloca %"class.draco::IndexType.114", align 4
  %agg.tmp82 = alloca %"class.draco::IndexType.114", align 4
  %agg.tmp96 = alloca %"class.draco::IndexType.114", align 4
  %agg.tmp97 = alloca %"class.draco::IndexType.114", align 4
  %agg.tmp112 = alloca %"class.draco::IndexType.114", align 4
  %agg.tmp113 = alloca %"class.draco::IndexType.114", align 4
  %agg.tmp121 = alloca %"class.draco::IndexType.114", align 4
  %agg.tmp123 = alloca %"class.draco::IndexType.114", align 4
  %agg.tmp141 = alloca %"class.draco::IndexType.114", align 4
  %agg.tmp142 = alloca %"class.draco::IndexType.114", align 4
  %__fs = alloca i8, align 1
  store %"class.draco::IndexType.114"* %__first, %"class.draco::IndexType.114"** %__first.addr, align 4
  store %"class.draco::IndexType.114"* %__last, %"class.draco::IndexType.114"** %__last.addr, align 4
  store %"struct.draco::MeshAreEquivalent::FaceIndexLess"* %__comp, %"struct.draco::MeshAreEquivalent::FaceIndexLess"** %__comp.addr, align 4
  store i32 6, i32* %__limit, align 4
  br label %while.body

while.body:                                       ; preds = %entry, %if.end156, %if.then159, %if.end177
  br label %__restart

__restart:                                        ; preds = %while.end94, %while.body
  %0 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__last.addr, align 4
  %1 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__first.addr, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.draco::IndexType.114"* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.draco::IndexType.114"* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  store i32 %sub.ptr.div, i32* %__len, align 4
  %2 = load i32, i32* %__len, align 4
  switch i32 %2, label %sw.epilog [
    i32 0, label %sw.bb
    i32 1, label %sw.bb
    i32 2, label %sw.bb1
    i32 3, label %sw.bb4
    i32 4, label %sw.bb7
    i32 5, label %sw.bb12
  ]

sw.bb:                                            ; preds = %__restart, %__restart
  br label %return

sw.bb1:                                           ; preds = %__restart
  %3 = load %"struct.draco::MeshAreEquivalent::FaceIndexLess"*, %"struct.draco::MeshAreEquivalent::FaceIndexLess"** %__comp.addr, align 4
  %4 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__last.addr, align 4
  %incdec.ptr = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %4, i32 -1
  store %"class.draco::IndexType.114"* %incdec.ptr, %"class.draco::IndexType.114"** %__last.addr, align 4
  %5 = bitcast %"class.draco::IndexType.114"* %agg.tmp to i8*
  %6 = bitcast %"class.draco::IndexType.114"* %incdec.ptr to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %5, i8* align 4 %6, i32 4, i1 false)
  %7 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__first.addr, align 4
  %8 = bitcast %"class.draco::IndexType.114"* %agg.tmp2 to i8*
  %9 = bitcast %"class.draco::IndexType.114"* %7 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %8, i8* align 4 %9, i32 4, i1 false)
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %agg.tmp, i32 0, i32 0
  %10 = load i32, i32* %coerce.dive, align 4
  %coerce.dive3 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %agg.tmp2, i32 0, i32 0
  %11 = load i32, i32* %coerce.dive3, align 4
  %call = call zeroext i1 @_ZNK5draco17MeshAreEquivalent13FaceIndexLessclENS_9IndexTypeIjNS_19FaceIndex_tag_type_EEES4_(%"struct.draco::MeshAreEquivalent::FaceIndexLess"* %3, i32 %10, i32 %11)
  br i1 %call, label %if.then, label %if.end

if.then:                                          ; preds = %sw.bb1
  %12 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__first.addr, align 4
  %13 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__last.addr, align 4
  call void @_ZNSt3__24swapIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS6_EE5valueEvE4typeERS6_S9_(%"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %12, %"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %13)
  br label %if.end

if.end:                                           ; preds = %if.then, %sw.bb1
  br label %return

sw.bb4:                                           ; preds = %__restart
  %14 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__first.addr, align 4
  %15 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__first.addr, align 4
  %add.ptr = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %15, i32 1
  %16 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__last.addr, align 4
  %incdec.ptr5 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %16, i32 -1
  store %"class.draco::IndexType.114"* %incdec.ptr5, %"class.draco::IndexType.114"** %__last.addr, align 4
  %17 = load %"struct.draco::MeshAreEquivalent::FaceIndexLess"*, %"struct.draco::MeshAreEquivalent::FaceIndexLess"** %__comp.addr, align 4
  %call6 = call i32 @_ZNSt3__27__sort3IRN5draco17MeshAreEquivalent13FaceIndexLessEPNS1_9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEjT0_S9_S9_T_(%"class.draco::IndexType.114"* %14, %"class.draco::IndexType.114"* %add.ptr, %"class.draco::IndexType.114"* %incdec.ptr5, %"struct.draco::MeshAreEquivalent::FaceIndexLess"* nonnull align 4 dereferenceable(4) %17)
  br label %return

sw.bb7:                                           ; preds = %__restart
  %18 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__first.addr, align 4
  %19 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__first.addr, align 4
  %add.ptr8 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %19, i32 1
  %20 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__first.addr, align 4
  %add.ptr9 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %20, i32 2
  %21 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__last.addr, align 4
  %incdec.ptr10 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %21, i32 -1
  store %"class.draco::IndexType.114"* %incdec.ptr10, %"class.draco::IndexType.114"** %__last.addr, align 4
  %22 = load %"struct.draco::MeshAreEquivalent::FaceIndexLess"*, %"struct.draco::MeshAreEquivalent::FaceIndexLess"** %__comp.addr, align 4
  %call11 = call i32 @_ZNSt3__27__sort4IRN5draco17MeshAreEquivalent13FaceIndexLessEPNS1_9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEjT0_S9_S9_S9_T_(%"class.draco::IndexType.114"* %18, %"class.draco::IndexType.114"* %add.ptr8, %"class.draco::IndexType.114"* %add.ptr9, %"class.draco::IndexType.114"* %incdec.ptr10, %"struct.draco::MeshAreEquivalent::FaceIndexLess"* nonnull align 4 dereferenceable(4) %22)
  br label %return

sw.bb12:                                          ; preds = %__restart
  %23 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__first.addr, align 4
  %24 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__first.addr, align 4
  %add.ptr13 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %24, i32 1
  %25 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__first.addr, align 4
  %add.ptr14 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %25, i32 2
  %26 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__first.addr, align 4
  %add.ptr15 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %26, i32 3
  %27 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__last.addr, align 4
  %incdec.ptr16 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %27, i32 -1
  store %"class.draco::IndexType.114"* %incdec.ptr16, %"class.draco::IndexType.114"** %__last.addr, align 4
  %28 = load %"struct.draco::MeshAreEquivalent::FaceIndexLess"*, %"struct.draco::MeshAreEquivalent::FaceIndexLess"** %__comp.addr, align 4
  %call17 = call i32 @_ZNSt3__27__sort5IRN5draco17MeshAreEquivalent13FaceIndexLessEPNS1_9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEjT0_S9_S9_S9_S9_T_(%"class.draco::IndexType.114"* %23, %"class.draco::IndexType.114"* %add.ptr13, %"class.draco::IndexType.114"* %add.ptr14, %"class.draco::IndexType.114"* %add.ptr15, %"class.draco::IndexType.114"* %incdec.ptr16, %"struct.draco::MeshAreEquivalent::FaceIndexLess"* nonnull align 4 dereferenceable(4) %28)
  br label %return

sw.epilog:                                        ; preds = %__restart
  %29 = load i32, i32* %__len, align 4
  %cmp = icmp sle i32 %29, 6
  br i1 %cmp, label %if.then18, label %if.end19

if.then18:                                        ; preds = %sw.epilog
  %30 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__first.addr, align 4
  %31 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__last.addr, align 4
  %32 = load %"struct.draco::MeshAreEquivalent::FaceIndexLess"*, %"struct.draco::MeshAreEquivalent::FaceIndexLess"** %__comp.addr, align 4
  call void @_ZNSt3__218__insertion_sort_3IRN5draco17MeshAreEquivalent13FaceIndexLessEPNS1_9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEvT0_S9_T_(%"class.draco::IndexType.114"* %30, %"class.draco::IndexType.114"* %31, %"struct.draco::MeshAreEquivalent::FaceIndexLess"* nonnull align 4 dereferenceable(4) %32)
  br label %return

if.end19:                                         ; preds = %sw.epilog
  %33 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__first.addr, align 4
  store %"class.draco::IndexType.114"* %33, %"class.draco::IndexType.114"** %__m, align 4
  %34 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__last.addr, align 4
  store %"class.draco::IndexType.114"* %34, %"class.draco::IndexType.114"** %__lm1, align 4
  %35 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__lm1, align 4
  %incdec.ptr20 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %35, i32 -1
  store %"class.draco::IndexType.114"* %incdec.ptr20, %"class.draco::IndexType.114"** %__lm1, align 4
  %36 = load i32, i32* %__len, align 4
  %cmp21 = icmp sge i32 %36, 1000
  br i1 %cmp21, label %if.then22, label %if.else

if.then22:                                        ; preds = %if.end19
  %37 = load i32, i32* %__len, align 4
  %div = sdiv i32 %37, 2
  store i32 %div, i32* %__delta, align 4
  %38 = load i32, i32* %__delta, align 4
  %39 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__m, align 4
  %add.ptr23 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %39, i32 %38
  store %"class.draco::IndexType.114"* %add.ptr23, %"class.draco::IndexType.114"** %__m, align 4
  %40 = load i32, i32* %__delta, align 4
  %div24 = sdiv i32 %40, 2
  store i32 %div24, i32* %__delta, align 4
  %41 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__first.addr, align 4
  %42 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__first.addr, align 4
  %43 = load i32, i32* %__delta, align 4
  %add.ptr25 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %42, i32 %43
  %44 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__m, align 4
  %45 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__m, align 4
  %46 = load i32, i32* %__delta, align 4
  %add.ptr26 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %45, i32 %46
  %47 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__lm1, align 4
  %48 = load %"struct.draco::MeshAreEquivalent::FaceIndexLess"*, %"struct.draco::MeshAreEquivalent::FaceIndexLess"** %__comp.addr, align 4
  %call27 = call i32 @_ZNSt3__27__sort5IRN5draco17MeshAreEquivalent13FaceIndexLessEPNS1_9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEjT0_S9_S9_S9_S9_T_(%"class.draco::IndexType.114"* %41, %"class.draco::IndexType.114"* %add.ptr25, %"class.draco::IndexType.114"* %44, %"class.draco::IndexType.114"* %add.ptr26, %"class.draco::IndexType.114"* %47, %"struct.draco::MeshAreEquivalent::FaceIndexLess"* nonnull align 4 dereferenceable(4) %48)
  store i32 %call27, i32* %__n_swaps, align 4
  br label %if.end31

if.else:                                          ; preds = %if.end19
  %49 = load i32, i32* %__len, align 4
  %div28 = sdiv i32 %49, 2
  store i32 %div28, i32* %__delta, align 4
  %50 = load i32, i32* %__delta, align 4
  %51 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__m, align 4
  %add.ptr29 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %51, i32 %50
  store %"class.draco::IndexType.114"* %add.ptr29, %"class.draco::IndexType.114"** %__m, align 4
  %52 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__first.addr, align 4
  %53 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__m, align 4
  %54 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__lm1, align 4
  %55 = load %"struct.draco::MeshAreEquivalent::FaceIndexLess"*, %"struct.draco::MeshAreEquivalent::FaceIndexLess"** %__comp.addr, align 4
  %call30 = call i32 @_ZNSt3__27__sort3IRN5draco17MeshAreEquivalent13FaceIndexLessEPNS1_9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEjT0_S9_S9_T_(%"class.draco::IndexType.114"* %52, %"class.draco::IndexType.114"* %53, %"class.draco::IndexType.114"* %54, %"struct.draco::MeshAreEquivalent::FaceIndexLess"* nonnull align 4 dereferenceable(4) %55)
  store i32 %call30, i32* %__n_swaps, align 4
  br label %if.end31

if.end31:                                         ; preds = %if.else, %if.then22
  %56 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__first.addr, align 4
  store %"class.draco::IndexType.114"* %56, %"class.draco::IndexType.114"** %__i, align 4
  %57 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__lm1, align 4
  store %"class.draco::IndexType.114"* %57, %"class.draco::IndexType.114"** %__j, align 4
  %58 = load %"struct.draco::MeshAreEquivalent::FaceIndexLess"*, %"struct.draco::MeshAreEquivalent::FaceIndexLess"** %__comp.addr, align 4
  %59 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__i, align 4
  %60 = bitcast %"class.draco::IndexType.114"* %agg.tmp32 to i8*
  %61 = bitcast %"class.draco::IndexType.114"* %59 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %60, i8* align 4 %61, i32 4, i1 false)
  %62 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__m, align 4
  %63 = bitcast %"class.draco::IndexType.114"* %agg.tmp33 to i8*
  %64 = bitcast %"class.draco::IndexType.114"* %62 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %63, i8* align 4 %64, i32 4, i1 false)
  %coerce.dive34 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %agg.tmp32, i32 0, i32 0
  %65 = load i32, i32* %coerce.dive34, align 4
  %coerce.dive35 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %agg.tmp33, i32 0, i32 0
  %66 = load i32, i32* %coerce.dive35, align 4
  %call36 = call zeroext i1 @_ZNK5draco17MeshAreEquivalent13FaceIndexLessclENS_9IndexTypeIjNS_19FaceIndex_tag_type_EEES4_(%"struct.draco::MeshAreEquivalent::FaceIndexLess"* %58, i32 %65, i32 %66)
  br i1 %call36, label %if.end105, label %if.then37

if.then37:                                        ; preds = %if.end31
  br label %while.body39

while.body39:                                     ; preds = %if.then37, %if.end103
  %67 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__i, align 4
  %68 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__j, align 4
  %incdec.ptr40 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %68, i32 -1
  store %"class.draco::IndexType.114"* %incdec.ptr40, %"class.draco::IndexType.114"** %__j, align 4
  %cmp41 = icmp eq %"class.draco::IndexType.114"* %67, %incdec.ptr40
  br i1 %cmp41, label %if.then42, label %if.end95

if.then42:                                        ; preds = %while.body39
  %69 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__i, align 4
  %incdec.ptr43 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %69, i32 1
  store %"class.draco::IndexType.114"* %incdec.ptr43, %"class.draco::IndexType.114"** %__i, align 4
  %70 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__last.addr, align 4
  store %"class.draco::IndexType.114"* %70, %"class.draco::IndexType.114"** %__j, align 4
  %71 = load %"struct.draco::MeshAreEquivalent::FaceIndexLess"*, %"struct.draco::MeshAreEquivalent::FaceIndexLess"** %__comp.addr, align 4
  %72 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__first.addr, align 4
  %73 = bitcast %"class.draco::IndexType.114"* %agg.tmp44 to i8*
  %74 = bitcast %"class.draco::IndexType.114"* %72 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %73, i8* align 4 %74, i32 4, i1 false)
  %75 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__j, align 4
  %incdec.ptr46 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %75, i32 -1
  store %"class.draco::IndexType.114"* %incdec.ptr46, %"class.draco::IndexType.114"** %__j, align 4
  %76 = bitcast %"class.draco::IndexType.114"* %agg.tmp45 to i8*
  %77 = bitcast %"class.draco::IndexType.114"* %incdec.ptr46 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %76, i8* align 4 %77, i32 4, i1 false)
  %coerce.dive47 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %agg.tmp44, i32 0, i32 0
  %78 = load i32, i32* %coerce.dive47, align 4
  %coerce.dive48 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %agg.tmp45, i32 0, i32 0
  %79 = load i32, i32* %coerce.dive48, align 4
  %call49 = call zeroext i1 @_ZNK5draco17MeshAreEquivalent13FaceIndexLessclENS_9IndexTypeIjNS_19FaceIndex_tag_type_EEES4_(%"struct.draco::MeshAreEquivalent::FaceIndexLess"* %71, i32 %78, i32 %79)
  br i1 %call49, label %if.end65, label %if.then50

if.then50:                                        ; preds = %if.then42
  br label %while.body52

while.body52:                                     ; preds = %if.then50, %if.end63
  %80 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__i, align 4
  %81 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__j, align 4
  %cmp53 = icmp eq %"class.draco::IndexType.114"* %80, %81
  br i1 %cmp53, label %if.then54, label %if.end55

if.then54:                                        ; preds = %while.body52
  br label %return

if.end55:                                         ; preds = %while.body52
  %82 = load %"struct.draco::MeshAreEquivalent::FaceIndexLess"*, %"struct.draco::MeshAreEquivalent::FaceIndexLess"** %__comp.addr, align 4
  %83 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__first.addr, align 4
  %84 = bitcast %"class.draco::IndexType.114"* %agg.tmp56 to i8*
  %85 = bitcast %"class.draco::IndexType.114"* %83 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %84, i8* align 4 %85, i32 4, i1 false)
  %86 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__i, align 4
  %87 = bitcast %"class.draco::IndexType.114"* %agg.tmp57 to i8*
  %88 = bitcast %"class.draco::IndexType.114"* %86 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %87, i8* align 4 %88, i32 4, i1 false)
  %coerce.dive58 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %agg.tmp56, i32 0, i32 0
  %89 = load i32, i32* %coerce.dive58, align 4
  %coerce.dive59 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %agg.tmp57, i32 0, i32 0
  %90 = load i32, i32* %coerce.dive59, align 4
  %call60 = call zeroext i1 @_ZNK5draco17MeshAreEquivalent13FaceIndexLessclENS_9IndexTypeIjNS_19FaceIndex_tag_type_EEES4_(%"struct.draco::MeshAreEquivalent::FaceIndexLess"* %82, i32 %89, i32 %90)
  br i1 %call60, label %if.then61, label %if.end63

if.then61:                                        ; preds = %if.end55
  %91 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__i, align 4
  %92 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__j, align 4
  call void @_ZNSt3__24swapIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS6_EE5valueEvE4typeERS6_S9_(%"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %91, %"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %92)
  %93 = load i32, i32* %__n_swaps, align 4
  %inc = add i32 %93, 1
  store i32 %inc, i32* %__n_swaps, align 4
  %94 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__i, align 4
  %incdec.ptr62 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %94, i32 1
  store %"class.draco::IndexType.114"* %incdec.ptr62, %"class.draco::IndexType.114"** %__i, align 4
  br label %while.end

if.end63:                                         ; preds = %if.end55
  %95 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__i, align 4
  %incdec.ptr64 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %95, i32 1
  store %"class.draco::IndexType.114"* %incdec.ptr64, %"class.draco::IndexType.114"** %__i, align 4
  br label %while.body52

while.end:                                        ; preds = %if.then61
  br label %if.end65

if.end65:                                         ; preds = %while.end, %if.then42
  %96 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__i, align 4
  %97 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__j, align 4
  %cmp66 = icmp eq %"class.draco::IndexType.114"* %96, %97
  br i1 %cmp66, label %if.then67, label %if.end68

if.then67:                                        ; preds = %if.end65
  br label %return

if.end68:                                         ; preds = %if.end65
  br label %while.body70

while.body70:                                     ; preds = %if.end68, %if.end91
  br label %while.cond71

while.cond71:                                     ; preds = %while.body77, %while.body70
  %98 = load %"struct.draco::MeshAreEquivalent::FaceIndexLess"*, %"struct.draco::MeshAreEquivalent::FaceIndexLess"** %__comp.addr, align 4
  %99 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__first.addr, align 4
  %100 = bitcast %"class.draco::IndexType.114"* %agg.tmp72 to i8*
  %101 = bitcast %"class.draco::IndexType.114"* %99 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %100, i8* align 4 %101, i32 4, i1 false)
  %102 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__i, align 4
  %103 = bitcast %"class.draco::IndexType.114"* %agg.tmp73 to i8*
  %104 = bitcast %"class.draco::IndexType.114"* %102 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %103, i8* align 4 %104, i32 4, i1 false)
  %coerce.dive74 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %agg.tmp72, i32 0, i32 0
  %105 = load i32, i32* %coerce.dive74, align 4
  %coerce.dive75 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %agg.tmp73, i32 0, i32 0
  %106 = load i32, i32* %coerce.dive75, align 4
  %call76 = call zeroext i1 @_ZNK5draco17MeshAreEquivalent13FaceIndexLessclENS_9IndexTypeIjNS_19FaceIndex_tag_type_EEES4_(%"struct.draco::MeshAreEquivalent::FaceIndexLess"* %98, i32 %105, i32 %106)
  %lnot = xor i1 %call76, true
  br i1 %lnot, label %while.body77, label %while.end79

while.body77:                                     ; preds = %while.cond71
  %107 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__i, align 4
  %incdec.ptr78 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %107, i32 1
  store %"class.draco::IndexType.114"* %incdec.ptr78, %"class.draco::IndexType.114"** %__i, align 4
  br label %while.cond71

while.end79:                                      ; preds = %while.cond71
  br label %while.cond80

while.cond80:                                     ; preds = %while.body87, %while.end79
  %108 = load %"struct.draco::MeshAreEquivalent::FaceIndexLess"*, %"struct.draco::MeshAreEquivalent::FaceIndexLess"** %__comp.addr, align 4
  %109 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__first.addr, align 4
  %110 = bitcast %"class.draco::IndexType.114"* %agg.tmp81 to i8*
  %111 = bitcast %"class.draco::IndexType.114"* %109 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %110, i8* align 4 %111, i32 4, i1 false)
  %112 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__j, align 4
  %incdec.ptr83 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %112, i32 -1
  store %"class.draco::IndexType.114"* %incdec.ptr83, %"class.draco::IndexType.114"** %__j, align 4
  %113 = bitcast %"class.draco::IndexType.114"* %agg.tmp82 to i8*
  %114 = bitcast %"class.draco::IndexType.114"* %incdec.ptr83 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %113, i8* align 4 %114, i32 4, i1 false)
  %coerce.dive84 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %agg.tmp81, i32 0, i32 0
  %115 = load i32, i32* %coerce.dive84, align 4
  %coerce.dive85 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %agg.tmp82, i32 0, i32 0
  %116 = load i32, i32* %coerce.dive85, align 4
  %call86 = call zeroext i1 @_ZNK5draco17MeshAreEquivalent13FaceIndexLessclENS_9IndexTypeIjNS_19FaceIndex_tag_type_EEES4_(%"struct.draco::MeshAreEquivalent::FaceIndexLess"* %108, i32 %115, i32 %116)
  br i1 %call86, label %while.body87, label %while.end88

while.body87:                                     ; preds = %while.cond80
  br label %while.cond80

while.end88:                                      ; preds = %while.cond80
  %117 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__i, align 4
  %118 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__j, align 4
  %cmp89 = icmp uge %"class.draco::IndexType.114"* %117, %118
  br i1 %cmp89, label %if.then90, label %if.end91

if.then90:                                        ; preds = %while.end88
  br label %while.end94

if.end91:                                         ; preds = %while.end88
  %119 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__i, align 4
  %120 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__j, align 4
  call void @_ZNSt3__24swapIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS6_EE5valueEvE4typeERS6_S9_(%"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %119, %"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %120)
  %121 = load i32, i32* %__n_swaps, align 4
  %inc92 = add i32 %121, 1
  store i32 %inc92, i32* %__n_swaps, align 4
  %122 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__i, align 4
  %incdec.ptr93 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %122, i32 1
  store %"class.draco::IndexType.114"* %incdec.ptr93, %"class.draco::IndexType.114"** %__i, align 4
  br label %while.body70

while.end94:                                      ; preds = %if.then90
  %123 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__i, align 4
  store %"class.draco::IndexType.114"* %123, %"class.draco::IndexType.114"** %__first.addr, align 4
  br label %__restart

if.end95:                                         ; preds = %while.body39
  %124 = load %"struct.draco::MeshAreEquivalent::FaceIndexLess"*, %"struct.draco::MeshAreEquivalent::FaceIndexLess"** %__comp.addr, align 4
  %125 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__j, align 4
  %126 = bitcast %"class.draco::IndexType.114"* %agg.tmp96 to i8*
  %127 = bitcast %"class.draco::IndexType.114"* %125 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %126, i8* align 4 %127, i32 4, i1 false)
  %128 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__m, align 4
  %129 = bitcast %"class.draco::IndexType.114"* %agg.tmp97 to i8*
  %130 = bitcast %"class.draco::IndexType.114"* %128 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %129, i8* align 4 %130, i32 4, i1 false)
  %coerce.dive98 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %agg.tmp96, i32 0, i32 0
  %131 = load i32, i32* %coerce.dive98, align 4
  %coerce.dive99 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %agg.tmp97, i32 0, i32 0
  %132 = load i32, i32* %coerce.dive99, align 4
  %call100 = call zeroext i1 @_ZNK5draco17MeshAreEquivalent13FaceIndexLessclENS_9IndexTypeIjNS_19FaceIndex_tag_type_EEES4_(%"struct.draco::MeshAreEquivalent::FaceIndexLess"* %124, i32 %131, i32 %132)
  br i1 %call100, label %if.then101, label %if.end103

if.then101:                                       ; preds = %if.end95
  %133 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__i, align 4
  %134 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__j, align 4
  call void @_ZNSt3__24swapIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS6_EE5valueEvE4typeERS6_S9_(%"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %133, %"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %134)
  %135 = load i32, i32* %__n_swaps, align 4
  %inc102 = add i32 %135, 1
  store i32 %inc102, i32* %__n_swaps, align 4
  br label %while.end104

if.end103:                                        ; preds = %if.end95
  br label %while.body39

while.end104:                                     ; preds = %if.then101
  br label %if.end105

if.end105:                                        ; preds = %while.end104, %if.end31
  %136 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__i, align 4
  %incdec.ptr106 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %136, i32 1
  store %"class.draco::IndexType.114"* %incdec.ptr106, %"class.draco::IndexType.114"** %__i, align 4
  %137 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__i, align 4
  %138 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__j, align 4
  %cmp107 = icmp ult %"class.draco::IndexType.114"* %137, %138
  br i1 %cmp107, label %if.then108, label %if.end139

if.then108:                                       ; preds = %if.end105
  br label %while.body110

while.body110:                                    ; preds = %if.then108, %if.end136
  br label %while.cond111

while.cond111:                                    ; preds = %while.body117, %while.body110
  %139 = load %"struct.draco::MeshAreEquivalent::FaceIndexLess"*, %"struct.draco::MeshAreEquivalent::FaceIndexLess"** %__comp.addr, align 4
  %140 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__i, align 4
  %141 = bitcast %"class.draco::IndexType.114"* %agg.tmp112 to i8*
  %142 = bitcast %"class.draco::IndexType.114"* %140 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %141, i8* align 4 %142, i32 4, i1 false)
  %143 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__m, align 4
  %144 = bitcast %"class.draco::IndexType.114"* %agg.tmp113 to i8*
  %145 = bitcast %"class.draco::IndexType.114"* %143 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %144, i8* align 4 %145, i32 4, i1 false)
  %coerce.dive114 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %agg.tmp112, i32 0, i32 0
  %146 = load i32, i32* %coerce.dive114, align 4
  %coerce.dive115 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %agg.tmp113, i32 0, i32 0
  %147 = load i32, i32* %coerce.dive115, align 4
  %call116 = call zeroext i1 @_ZNK5draco17MeshAreEquivalent13FaceIndexLessclENS_9IndexTypeIjNS_19FaceIndex_tag_type_EEES4_(%"struct.draco::MeshAreEquivalent::FaceIndexLess"* %139, i32 %146, i32 %147)
  br i1 %call116, label %while.body117, label %while.end119

while.body117:                                    ; preds = %while.cond111
  %148 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__i, align 4
  %incdec.ptr118 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %148, i32 1
  store %"class.draco::IndexType.114"* %incdec.ptr118, %"class.draco::IndexType.114"** %__i, align 4
  br label %while.cond111

while.end119:                                     ; preds = %while.cond111
  br label %while.cond120

while.cond120:                                    ; preds = %while.body128, %while.end119
  %149 = load %"struct.draco::MeshAreEquivalent::FaceIndexLess"*, %"struct.draco::MeshAreEquivalent::FaceIndexLess"** %__comp.addr, align 4
  %150 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__j, align 4
  %incdec.ptr122 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %150, i32 -1
  store %"class.draco::IndexType.114"* %incdec.ptr122, %"class.draco::IndexType.114"** %__j, align 4
  %151 = bitcast %"class.draco::IndexType.114"* %agg.tmp121 to i8*
  %152 = bitcast %"class.draco::IndexType.114"* %incdec.ptr122 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %151, i8* align 4 %152, i32 4, i1 false)
  %153 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__m, align 4
  %154 = bitcast %"class.draco::IndexType.114"* %agg.tmp123 to i8*
  %155 = bitcast %"class.draco::IndexType.114"* %153 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %154, i8* align 4 %155, i32 4, i1 false)
  %coerce.dive124 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %agg.tmp121, i32 0, i32 0
  %156 = load i32, i32* %coerce.dive124, align 4
  %coerce.dive125 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %agg.tmp123, i32 0, i32 0
  %157 = load i32, i32* %coerce.dive125, align 4
  %call126 = call zeroext i1 @_ZNK5draco17MeshAreEquivalent13FaceIndexLessclENS_9IndexTypeIjNS_19FaceIndex_tag_type_EEES4_(%"struct.draco::MeshAreEquivalent::FaceIndexLess"* %149, i32 %156, i32 %157)
  %lnot127 = xor i1 %call126, true
  br i1 %lnot127, label %while.body128, label %while.end129

while.body128:                                    ; preds = %while.cond120
  br label %while.cond120

while.end129:                                     ; preds = %while.cond120
  %158 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__i, align 4
  %159 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__j, align 4
  %cmp130 = icmp ugt %"class.draco::IndexType.114"* %158, %159
  br i1 %cmp130, label %if.then131, label %if.end132

if.then131:                                       ; preds = %while.end129
  br label %while.end138

if.end132:                                        ; preds = %while.end129
  %160 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__i, align 4
  %161 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__j, align 4
  call void @_ZNSt3__24swapIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS6_EE5valueEvE4typeERS6_S9_(%"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %160, %"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %161)
  %162 = load i32, i32* %__n_swaps, align 4
  %inc133 = add i32 %162, 1
  store i32 %inc133, i32* %__n_swaps, align 4
  %163 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__m, align 4
  %164 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__i, align 4
  %cmp134 = icmp eq %"class.draco::IndexType.114"* %163, %164
  br i1 %cmp134, label %if.then135, label %if.end136

if.then135:                                       ; preds = %if.end132
  %165 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__j, align 4
  store %"class.draco::IndexType.114"* %165, %"class.draco::IndexType.114"** %__m, align 4
  br label %if.end136

if.end136:                                        ; preds = %if.then135, %if.end132
  %166 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__i, align 4
  %incdec.ptr137 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %166, i32 1
  store %"class.draco::IndexType.114"* %incdec.ptr137, %"class.draco::IndexType.114"** %__i, align 4
  br label %while.body110

while.end138:                                     ; preds = %if.then131
  br label %if.end139

if.end139:                                        ; preds = %while.end138, %if.end105
  %167 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__i, align 4
  %168 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__m, align 4
  %cmp140 = icmp ne %"class.draco::IndexType.114"* %167, %168
  br i1 %cmp140, label %land.lhs.true, label %if.end148

land.lhs.true:                                    ; preds = %if.end139
  %169 = load %"struct.draco::MeshAreEquivalent::FaceIndexLess"*, %"struct.draco::MeshAreEquivalent::FaceIndexLess"** %__comp.addr, align 4
  %170 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__m, align 4
  %171 = bitcast %"class.draco::IndexType.114"* %agg.tmp141 to i8*
  %172 = bitcast %"class.draco::IndexType.114"* %170 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %171, i8* align 4 %172, i32 4, i1 false)
  %173 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__i, align 4
  %174 = bitcast %"class.draco::IndexType.114"* %agg.tmp142 to i8*
  %175 = bitcast %"class.draco::IndexType.114"* %173 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %174, i8* align 4 %175, i32 4, i1 false)
  %coerce.dive143 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %agg.tmp141, i32 0, i32 0
  %176 = load i32, i32* %coerce.dive143, align 4
  %coerce.dive144 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %agg.tmp142, i32 0, i32 0
  %177 = load i32, i32* %coerce.dive144, align 4
  %call145 = call zeroext i1 @_ZNK5draco17MeshAreEquivalent13FaceIndexLessclENS_9IndexTypeIjNS_19FaceIndex_tag_type_EEES4_(%"struct.draco::MeshAreEquivalent::FaceIndexLess"* %169, i32 %176, i32 %177)
  br i1 %call145, label %if.then146, label %if.end148

if.then146:                                       ; preds = %land.lhs.true
  %178 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__i, align 4
  %179 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__m, align 4
  call void @_ZNSt3__24swapIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS6_EE5valueEvE4typeERS6_S9_(%"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %178, %"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %179)
  %180 = load i32, i32* %__n_swaps, align 4
  %inc147 = add i32 %180, 1
  store i32 %inc147, i32* %__n_swaps, align 4
  br label %if.end148

if.end148:                                        ; preds = %if.then146, %land.lhs.true, %if.end139
  %181 = load i32, i32* %__n_swaps, align 4
  %cmp149 = icmp eq i32 %181, 0
  br i1 %cmp149, label %if.then150, label %if.end163

if.then150:                                       ; preds = %if.end148
  %182 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__first.addr, align 4
  %183 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__i, align 4
  %184 = load %"struct.draco::MeshAreEquivalent::FaceIndexLess"*, %"struct.draco::MeshAreEquivalent::FaceIndexLess"** %__comp.addr, align 4
  %call151 = call zeroext i1 @_ZNSt3__227__insertion_sort_incompleteIRN5draco17MeshAreEquivalent13FaceIndexLessEPNS1_9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEbT0_S9_T_(%"class.draco::IndexType.114"* %182, %"class.draco::IndexType.114"* %183, %"struct.draco::MeshAreEquivalent::FaceIndexLess"* nonnull align 4 dereferenceable(4) %184)
  %frombool = zext i1 %call151 to i8
  store i8 %frombool, i8* %__fs, align 1
  %185 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__i, align 4
  %add.ptr152 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %185, i32 1
  %186 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__last.addr, align 4
  %187 = load %"struct.draco::MeshAreEquivalent::FaceIndexLess"*, %"struct.draco::MeshAreEquivalent::FaceIndexLess"** %__comp.addr, align 4
  %call153 = call zeroext i1 @_ZNSt3__227__insertion_sort_incompleteIRN5draco17MeshAreEquivalent13FaceIndexLessEPNS1_9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEbT0_S9_T_(%"class.draco::IndexType.114"* %add.ptr152, %"class.draco::IndexType.114"* %186, %"struct.draco::MeshAreEquivalent::FaceIndexLess"* nonnull align 4 dereferenceable(4) %187)
  br i1 %call153, label %if.then154, label %if.else157

if.then154:                                       ; preds = %if.then150
  %188 = load i8, i8* %__fs, align 1
  %tobool = trunc i8 %188 to i1
  br i1 %tobool, label %if.then155, label %if.end156

if.then155:                                       ; preds = %if.then154
  br label %return

if.end156:                                        ; preds = %if.then154
  %189 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__i, align 4
  store %"class.draco::IndexType.114"* %189, %"class.draco::IndexType.114"** %__last.addr, align 4
  br label %while.body

if.else157:                                       ; preds = %if.then150
  %190 = load i8, i8* %__fs, align 1
  %tobool158 = trunc i8 %190 to i1
  br i1 %tobool158, label %if.then159, label %if.end161

if.then159:                                       ; preds = %if.else157
  %191 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__i, align 4
  %incdec.ptr160 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %191, i32 1
  store %"class.draco::IndexType.114"* %incdec.ptr160, %"class.draco::IndexType.114"** %__i, align 4
  store %"class.draco::IndexType.114"* %incdec.ptr160, %"class.draco::IndexType.114"** %__first.addr, align 4
  br label %while.body

if.end161:                                        ; preds = %if.else157
  br label %if.end162

if.end162:                                        ; preds = %if.end161
  br label %if.end163

if.end163:                                        ; preds = %if.end162, %if.end148
  %192 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__i, align 4
  %193 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__first.addr, align 4
  %sub.ptr.lhs.cast164 = ptrtoint %"class.draco::IndexType.114"* %192 to i32
  %sub.ptr.rhs.cast165 = ptrtoint %"class.draco::IndexType.114"* %193 to i32
  %sub.ptr.sub166 = sub i32 %sub.ptr.lhs.cast164, %sub.ptr.rhs.cast165
  %sub.ptr.div167 = sdiv exact i32 %sub.ptr.sub166, 4
  %194 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__last.addr, align 4
  %195 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__i, align 4
  %sub.ptr.lhs.cast168 = ptrtoint %"class.draco::IndexType.114"* %194 to i32
  %sub.ptr.rhs.cast169 = ptrtoint %"class.draco::IndexType.114"* %195 to i32
  %sub.ptr.sub170 = sub i32 %sub.ptr.lhs.cast168, %sub.ptr.rhs.cast169
  %sub.ptr.div171 = sdiv exact i32 %sub.ptr.sub170, 4
  %cmp172 = icmp slt i32 %sub.ptr.div167, %sub.ptr.div171
  br i1 %cmp172, label %if.then173, label %if.else175

if.then173:                                       ; preds = %if.end163
  %196 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__first.addr, align 4
  %197 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__i, align 4
  %198 = load %"struct.draco::MeshAreEquivalent::FaceIndexLess"*, %"struct.draco::MeshAreEquivalent::FaceIndexLess"** %__comp.addr, align 4
  call void @_ZNSt3__26__sortIRN5draco17MeshAreEquivalent13FaceIndexLessEPNS1_9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEvT0_S9_T_(%"class.draco::IndexType.114"* %196, %"class.draco::IndexType.114"* %197, %"struct.draco::MeshAreEquivalent::FaceIndexLess"* nonnull align 4 dereferenceable(4) %198)
  %199 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__i, align 4
  %incdec.ptr174 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %199, i32 1
  store %"class.draco::IndexType.114"* %incdec.ptr174, %"class.draco::IndexType.114"** %__i, align 4
  store %"class.draco::IndexType.114"* %incdec.ptr174, %"class.draco::IndexType.114"** %__first.addr, align 4
  br label %if.end177

if.else175:                                       ; preds = %if.end163
  %200 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__i, align 4
  %add.ptr176 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %200, i32 1
  %201 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__last.addr, align 4
  %202 = load %"struct.draco::MeshAreEquivalent::FaceIndexLess"*, %"struct.draco::MeshAreEquivalent::FaceIndexLess"** %__comp.addr, align 4
  call void @_ZNSt3__26__sortIRN5draco17MeshAreEquivalent13FaceIndexLessEPNS1_9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEvT0_S9_T_(%"class.draco::IndexType.114"* %add.ptr176, %"class.draco::IndexType.114"* %201, %"struct.draco::MeshAreEquivalent::FaceIndexLess"* nonnull align 4 dereferenceable(4) %202)
  %203 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__i, align 4
  store %"class.draco::IndexType.114"* %203, %"class.draco::IndexType.114"** %__last.addr, align 4
  br label %if.end177

if.end177:                                        ; preds = %if.else175, %if.then173
  br label %while.body

return:                                           ; preds = %if.then155, %if.then67, %if.then54, %if.then18, %sw.bb12, %sw.bb7, %sw.bb4, %if.end, %sw.bb
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__24swapIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS6_EE5valueEvE4typeERS6_S9_(%"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %__x, %"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %__x.addr = alloca %"class.draco::IndexType.114"*, align 4
  %__y.addr = alloca %"class.draco::IndexType.114"*, align 4
  %__t = alloca %"class.draco::IndexType.114", align 4
  store %"class.draco::IndexType.114"* %__x, %"class.draco::IndexType.114"** %__x.addr, align 4
  store %"class.draco::IndexType.114"* %__y, %"class.draco::IndexType.114"** %__y.addr, align 4
  %0 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__x.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.114"* @_ZNSt3__24moveIRN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEONS_16remove_referenceIT_E4typeEOS7_(%"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %0) #7
  %1 = bitcast %"class.draco::IndexType.114"* %__t to i8*
  %2 = bitcast %"class.draco::IndexType.114"* %call to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 4, i1 false)
  %3 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__y.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.114"* @_ZNSt3__24moveIRN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEONS_16remove_referenceIT_E4typeEOS7_(%"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %3) #7
  %4 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__x.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.114"* @_ZN5draco9IndexTypeIjNS_19FaceIndex_tag_type_EEaSERKS2_(%"class.draco::IndexType.114"* %4, %"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %call1)
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.114"* @_ZNSt3__24moveIRN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEONS_16remove_referenceIT_E4typeEOS7_(%"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %__t) #7
  %5 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__y.addr, align 4
  %call4 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.114"* @_ZN5draco9IndexTypeIjNS_19FaceIndex_tag_type_EEaSERKS2_(%"class.draco::IndexType.114"* %5, %"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %call3)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__27__sort3IRN5draco17MeshAreEquivalent13FaceIndexLessEPNS1_9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEjT0_S9_S9_T_(%"class.draco::IndexType.114"* %__x, %"class.draco::IndexType.114"* %__y, %"class.draco::IndexType.114"* %__z, %"struct.draco::MeshAreEquivalent::FaceIndexLess"* nonnull align 4 dereferenceable(4) %__c) #0 comdat {
entry:
  %retval = alloca i32, align 4
  %__x.addr = alloca %"class.draco::IndexType.114"*, align 4
  %__y.addr = alloca %"class.draco::IndexType.114"*, align 4
  %__z.addr = alloca %"class.draco::IndexType.114"*, align 4
  %__c.addr = alloca %"struct.draco::MeshAreEquivalent::FaceIndexLess"*, align 4
  %__r = alloca i32, align 4
  %agg.tmp = alloca %"class.draco::IndexType.114", align 4
  %agg.tmp1 = alloca %"class.draco::IndexType.114", align 4
  %agg.tmp3 = alloca %"class.draco::IndexType.114", align 4
  %agg.tmp4 = alloca %"class.draco::IndexType.114", align 4
  %agg.tmp9 = alloca %"class.draco::IndexType.114", align 4
  %agg.tmp10 = alloca %"class.draco::IndexType.114", align 4
  %agg.tmp17 = alloca %"class.draco::IndexType.114", align 4
  %agg.tmp18 = alloca %"class.draco::IndexType.114", align 4
  %agg.tmp24 = alloca %"class.draco::IndexType.114", align 4
  %agg.tmp25 = alloca %"class.draco::IndexType.114", align 4
  store %"class.draco::IndexType.114"* %__x, %"class.draco::IndexType.114"** %__x.addr, align 4
  store %"class.draco::IndexType.114"* %__y, %"class.draco::IndexType.114"** %__y.addr, align 4
  store %"class.draco::IndexType.114"* %__z, %"class.draco::IndexType.114"** %__z.addr, align 4
  store %"struct.draco::MeshAreEquivalent::FaceIndexLess"* %__c, %"struct.draco::MeshAreEquivalent::FaceIndexLess"** %__c.addr, align 4
  store i32 0, i32* %__r, align 4
  %0 = load %"struct.draco::MeshAreEquivalent::FaceIndexLess"*, %"struct.draco::MeshAreEquivalent::FaceIndexLess"** %__c.addr, align 4
  %1 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__y.addr, align 4
  %2 = bitcast %"class.draco::IndexType.114"* %agg.tmp to i8*
  %3 = bitcast %"class.draco::IndexType.114"* %1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 4, i1 false)
  %4 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__x.addr, align 4
  %5 = bitcast %"class.draco::IndexType.114"* %agg.tmp1 to i8*
  %6 = bitcast %"class.draco::IndexType.114"* %4 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %5, i8* align 4 %6, i32 4, i1 false)
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %agg.tmp, i32 0, i32 0
  %7 = load i32, i32* %coerce.dive, align 4
  %coerce.dive2 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %agg.tmp1, i32 0, i32 0
  %8 = load i32, i32* %coerce.dive2, align 4
  %call = call zeroext i1 @_ZNK5draco17MeshAreEquivalent13FaceIndexLessclENS_9IndexTypeIjNS_19FaceIndex_tag_type_EEES4_(%"struct.draco::MeshAreEquivalent::FaceIndexLess"* %0, i32 %7, i32 %8)
  br i1 %call, label %if.end16, label %if.then

if.then:                                          ; preds = %entry
  %9 = load %"struct.draco::MeshAreEquivalent::FaceIndexLess"*, %"struct.draco::MeshAreEquivalent::FaceIndexLess"** %__c.addr, align 4
  %10 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__z.addr, align 4
  %11 = bitcast %"class.draco::IndexType.114"* %agg.tmp3 to i8*
  %12 = bitcast %"class.draco::IndexType.114"* %10 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %11, i8* align 4 %12, i32 4, i1 false)
  %13 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__y.addr, align 4
  %14 = bitcast %"class.draco::IndexType.114"* %agg.tmp4 to i8*
  %15 = bitcast %"class.draco::IndexType.114"* %13 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %14, i8* align 4 %15, i32 4, i1 false)
  %coerce.dive5 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %agg.tmp3, i32 0, i32 0
  %16 = load i32, i32* %coerce.dive5, align 4
  %coerce.dive6 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %agg.tmp4, i32 0, i32 0
  %17 = load i32, i32* %coerce.dive6, align 4
  %call7 = call zeroext i1 @_ZNK5draco17MeshAreEquivalent13FaceIndexLessclENS_9IndexTypeIjNS_19FaceIndex_tag_type_EEES4_(%"struct.draco::MeshAreEquivalent::FaceIndexLess"* %9, i32 %16, i32 %17)
  br i1 %call7, label %if.end, label %if.then8

if.then8:                                         ; preds = %if.then
  %18 = load i32, i32* %__r, align 4
  store i32 %18, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %if.then
  %19 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__y.addr, align 4
  %20 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__z.addr, align 4
  call void @_ZNSt3__24swapIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS6_EE5valueEvE4typeERS6_S9_(%"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %19, %"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %20)
  store i32 1, i32* %__r, align 4
  %21 = load %"struct.draco::MeshAreEquivalent::FaceIndexLess"*, %"struct.draco::MeshAreEquivalent::FaceIndexLess"** %__c.addr, align 4
  %22 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__y.addr, align 4
  %23 = bitcast %"class.draco::IndexType.114"* %agg.tmp9 to i8*
  %24 = bitcast %"class.draco::IndexType.114"* %22 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %23, i8* align 4 %24, i32 4, i1 false)
  %25 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__x.addr, align 4
  %26 = bitcast %"class.draco::IndexType.114"* %agg.tmp10 to i8*
  %27 = bitcast %"class.draco::IndexType.114"* %25 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %26, i8* align 4 %27, i32 4, i1 false)
  %coerce.dive11 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %agg.tmp9, i32 0, i32 0
  %28 = load i32, i32* %coerce.dive11, align 4
  %coerce.dive12 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %agg.tmp10, i32 0, i32 0
  %29 = load i32, i32* %coerce.dive12, align 4
  %call13 = call zeroext i1 @_ZNK5draco17MeshAreEquivalent13FaceIndexLessclENS_9IndexTypeIjNS_19FaceIndex_tag_type_EEES4_(%"struct.draco::MeshAreEquivalent::FaceIndexLess"* %21, i32 %28, i32 %29)
  br i1 %call13, label %if.then14, label %if.end15

if.then14:                                        ; preds = %if.end
  %30 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__x.addr, align 4
  %31 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__y.addr, align 4
  call void @_ZNSt3__24swapIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS6_EE5valueEvE4typeERS6_S9_(%"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %30, %"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %31)
  store i32 2, i32* %__r, align 4
  br label %if.end15

if.end15:                                         ; preds = %if.then14, %if.end
  %32 = load i32, i32* %__r, align 4
  store i32 %32, i32* %retval, align 4
  br label %return

if.end16:                                         ; preds = %entry
  %33 = load %"struct.draco::MeshAreEquivalent::FaceIndexLess"*, %"struct.draco::MeshAreEquivalent::FaceIndexLess"** %__c.addr, align 4
  %34 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__z.addr, align 4
  %35 = bitcast %"class.draco::IndexType.114"* %agg.tmp17 to i8*
  %36 = bitcast %"class.draco::IndexType.114"* %34 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %35, i8* align 4 %36, i32 4, i1 false)
  %37 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__y.addr, align 4
  %38 = bitcast %"class.draco::IndexType.114"* %agg.tmp18 to i8*
  %39 = bitcast %"class.draco::IndexType.114"* %37 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %38, i8* align 4 %39, i32 4, i1 false)
  %coerce.dive19 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %agg.tmp17, i32 0, i32 0
  %40 = load i32, i32* %coerce.dive19, align 4
  %coerce.dive20 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %agg.tmp18, i32 0, i32 0
  %41 = load i32, i32* %coerce.dive20, align 4
  %call21 = call zeroext i1 @_ZNK5draco17MeshAreEquivalent13FaceIndexLessclENS_9IndexTypeIjNS_19FaceIndex_tag_type_EEES4_(%"struct.draco::MeshAreEquivalent::FaceIndexLess"* %33, i32 %40, i32 %41)
  br i1 %call21, label %if.then22, label %if.end23

if.then22:                                        ; preds = %if.end16
  %42 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__x.addr, align 4
  %43 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__z.addr, align 4
  call void @_ZNSt3__24swapIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS6_EE5valueEvE4typeERS6_S9_(%"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %42, %"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %43)
  store i32 1, i32* %__r, align 4
  %44 = load i32, i32* %__r, align 4
  store i32 %44, i32* %retval, align 4
  br label %return

if.end23:                                         ; preds = %if.end16
  %45 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__x.addr, align 4
  %46 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__y.addr, align 4
  call void @_ZNSt3__24swapIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS6_EE5valueEvE4typeERS6_S9_(%"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %45, %"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %46)
  store i32 1, i32* %__r, align 4
  %47 = load %"struct.draco::MeshAreEquivalent::FaceIndexLess"*, %"struct.draco::MeshAreEquivalent::FaceIndexLess"** %__c.addr, align 4
  %48 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__z.addr, align 4
  %49 = bitcast %"class.draco::IndexType.114"* %agg.tmp24 to i8*
  %50 = bitcast %"class.draco::IndexType.114"* %48 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %49, i8* align 4 %50, i32 4, i1 false)
  %51 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__y.addr, align 4
  %52 = bitcast %"class.draco::IndexType.114"* %agg.tmp25 to i8*
  %53 = bitcast %"class.draco::IndexType.114"* %51 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %52, i8* align 4 %53, i32 4, i1 false)
  %coerce.dive26 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %agg.tmp24, i32 0, i32 0
  %54 = load i32, i32* %coerce.dive26, align 4
  %coerce.dive27 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %agg.tmp25, i32 0, i32 0
  %55 = load i32, i32* %coerce.dive27, align 4
  %call28 = call zeroext i1 @_ZNK5draco17MeshAreEquivalent13FaceIndexLessclENS_9IndexTypeIjNS_19FaceIndex_tag_type_EEES4_(%"struct.draco::MeshAreEquivalent::FaceIndexLess"* %47, i32 %54, i32 %55)
  br i1 %call28, label %if.then29, label %if.end30

if.then29:                                        ; preds = %if.end23
  %56 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__y.addr, align 4
  %57 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__z.addr, align 4
  call void @_ZNSt3__24swapIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS6_EE5valueEvE4typeERS6_S9_(%"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %56, %"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %57)
  store i32 2, i32* %__r, align 4
  br label %if.end30

if.end30:                                         ; preds = %if.then29, %if.end23
  %58 = load i32, i32* %__r, align 4
  store i32 %58, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end30, %if.then22, %if.end15, %if.then8
  %59 = load i32, i32* %retval, align 4
  ret i32 %59
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__27__sort4IRN5draco17MeshAreEquivalent13FaceIndexLessEPNS1_9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEjT0_S9_S9_S9_T_(%"class.draco::IndexType.114"* %__x1, %"class.draco::IndexType.114"* %__x2, %"class.draco::IndexType.114"* %__x3, %"class.draco::IndexType.114"* %__x4, %"struct.draco::MeshAreEquivalent::FaceIndexLess"* nonnull align 4 dereferenceable(4) %__c) #0 comdat {
entry:
  %__x1.addr = alloca %"class.draco::IndexType.114"*, align 4
  %__x2.addr = alloca %"class.draco::IndexType.114"*, align 4
  %__x3.addr = alloca %"class.draco::IndexType.114"*, align 4
  %__x4.addr = alloca %"class.draco::IndexType.114"*, align 4
  %__c.addr = alloca %"struct.draco::MeshAreEquivalent::FaceIndexLess"*, align 4
  %__r = alloca i32, align 4
  %agg.tmp = alloca %"class.draco::IndexType.114", align 4
  %agg.tmp1 = alloca %"class.draco::IndexType.114", align 4
  %agg.tmp4 = alloca %"class.draco::IndexType.114", align 4
  %agg.tmp5 = alloca %"class.draco::IndexType.114", align 4
  %agg.tmp11 = alloca %"class.draco::IndexType.114", align 4
  %agg.tmp12 = alloca %"class.draco::IndexType.114", align 4
  store %"class.draco::IndexType.114"* %__x1, %"class.draco::IndexType.114"** %__x1.addr, align 4
  store %"class.draco::IndexType.114"* %__x2, %"class.draco::IndexType.114"** %__x2.addr, align 4
  store %"class.draco::IndexType.114"* %__x3, %"class.draco::IndexType.114"** %__x3.addr, align 4
  store %"class.draco::IndexType.114"* %__x4, %"class.draco::IndexType.114"** %__x4.addr, align 4
  store %"struct.draco::MeshAreEquivalent::FaceIndexLess"* %__c, %"struct.draco::MeshAreEquivalent::FaceIndexLess"** %__c.addr, align 4
  %0 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__x1.addr, align 4
  %1 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__x2.addr, align 4
  %2 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__x3.addr, align 4
  %3 = load %"struct.draco::MeshAreEquivalent::FaceIndexLess"*, %"struct.draco::MeshAreEquivalent::FaceIndexLess"** %__c.addr, align 4
  %call = call i32 @_ZNSt3__27__sort3IRN5draco17MeshAreEquivalent13FaceIndexLessEPNS1_9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEjT0_S9_S9_T_(%"class.draco::IndexType.114"* %0, %"class.draco::IndexType.114"* %1, %"class.draco::IndexType.114"* %2, %"struct.draco::MeshAreEquivalent::FaceIndexLess"* nonnull align 4 dereferenceable(4) %3)
  store i32 %call, i32* %__r, align 4
  %4 = load %"struct.draco::MeshAreEquivalent::FaceIndexLess"*, %"struct.draco::MeshAreEquivalent::FaceIndexLess"** %__c.addr, align 4
  %5 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__x4.addr, align 4
  %6 = bitcast %"class.draco::IndexType.114"* %agg.tmp to i8*
  %7 = bitcast %"class.draco::IndexType.114"* %5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 4, i1 false)
  %8 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__x3.addr, align 4
  %9 = bitcast %"class.draco::IndexType.114"* %agg.tmp1 to i8*
  %10 = bitcast %"class.draco::IndexType.114"* %8 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 4, i1 false)
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %agg.tmp, i32 0, i32 0
  %11 = load i32, i32* %coerce.dive, align 4
  %coerce.dive2 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %agg.tmp1, i32 0, i32 0
  %12 = load i32, i32* %coerce.dive2, align 4
  %call3 = call zeroext i1 @_ZNK5draco17MeshAreEquivalent13FaceIndexLessclENS_9IndexTypeIjNS_19FaceIndex_tag_type_EEES4_(%"struct.draco::MeshAreEquivalent::FaceIndexLess"* %4, i32 %11, i32 %12)
  br i1 %call3, label %if.then, label %if.end19

if.then:                                          ; preds = %entry
  %13 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__x3.addr, align 4
  %14 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__x4.addr, align 4
  call void @_ZNSt3__24swapIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS6_EE5valueEvE4typeERS6_S9_(%"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %13, %"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %14)
  %15 = load i32, i32* %__r, align 4
  %inc = add i32 %15, 1
  store i32 %inc, i32* %__r, align 4
  %16 = load %"struct.draco::MeshAreEquivalent::FaceIndexLess"*, %"struct.draco::MeshAreEquivalent::FaceIndexLess"** %__c.addr, align 4
  %17 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__x3.addr, align 4
  %18 = bitcast %"class.draco::IndexType.114"* %agg.tmp4 to i8*
  %19 = bitcast %"class.draco::IndexType.114"* %17 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %18, i8* align 4 %19, i32 4, i1 false)
  %20 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__x2.addr, align 4
  %21 = bitcast %"class.draco::IndexType.114"* %agg.tmp5 to i8*
  %22 = bitcast %"class.draco::IndexType.114"* %20 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %21, i8* align 4 %22, i32 4, i1 false)
  %coerce.dive6 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %agg.tmp4, i32 0, i32 0
  %23 = load i32, i32* %coerce.dive6, align 4
  %coerce.dive7 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %agg.tmp5, i32 0, i32 0
  %24 = load i32, i32* %coerce.dive7, align 4
  %call8 = call zeroext i1 @_ZNK5draco17MeshAreEquivalent13FaceIndexLessclENS_9IndexTypeIjNS_19FaceIndex_tag_type_EEES4_(%"struct.draco::MeshAreEquivalent::FaceIndexLess"* %16, i32 %23, i32 %24)
  br i1 %call8, label %if.then9, label %if.end18

if.then9:                                         ; preds = %if.then
  %25 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__x2.addr, align 4
  %26 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__x3.addr, align 4
  call void @_ZNSt3__24swapIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS6_EE5valueEvE4typeERS6_S9_(%"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %25, %"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %26)
  %27 = load i32, i32* %__r, align 4
  %inc10 = add i32 %27, 1
  store i32 %inc10, i32* %__r, align 4
  %28 = load %"struct.draco::MeshAreEquivalent::FaceIndexLess"*, %"struct.draco::MeshAreEquivalent::FaceIndexLess"** %__c.addr, align 4
  %29 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__x2.addr, align 4
  %30 = bitcast %"class.draco::IndexType.114"* %agg.tmp11 to i8*
  %31 = bitcast %"class.draco::IndexType.114"* %29 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %30, i8* align 4 %31, i32 4, i1 false)
  %32 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__x1.addr, align 4
  %33 = bitcast %"class.draco::IndexType.114"* %agg.tmp12 to i8*
  %34 = bitcast %"class.draco::IndexType.114"* %32 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %33, i8* align 4 %34, i32 4, i1 false)
  %coerce.dive13 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %agg.tmp11, i32 0, i32 0
  %35 = load i32, i32* %coerce.dive13, align 4
  %coerce.dive14 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %agg.tmp12, i32 0, i32 0
  %36 = load i32, i32* %coerce.dive14, align 4
  %call15 = call zeroext i1 @_ZNK5draco17MeshAreEquivalent13FaceIndexLessclENS_9IndexTypeIjNS_19FaceIndex_tag_type_EEES4_(%"struct.draco::MeshAreEquivalent::FaceIndexLess"* %28, i32 %35, i32 %36)
  br i1 %call15, label %if.then16, label %if.end

if.then16:                                        ; preds = %if.then9
  %37 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__x1.addr, align 4
  %38 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__x2.addr, align 4
  call void @_ZNSt3__24swapIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS6_EE5valueEvE4typeERS6_S9_(%"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %37, %"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %38)
  %39 = load i32, i32* %__r, align 4
  %inc17 = add i32 %39, 1
  store i32 %inc17, i32* %__r, align 4
  br label %if.end

if.end:                                           ; preds = %if.then16, %if.then9
  br label %if.end18

if.end18:                                         ; preds = %if.end, %if.then
  br label %if.end19

if.end19:                                         ; preds = %if.end18, %entry
  %40 = load i32, i32* %__r, align 4
  ret i32 %40
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__27__sort5IRN5draco17MeshAreEquivalent13FaceIndexLessEPNS1_9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEjT0_S9_S9_S9_S9_T_(%"class.draco::IndexType.114"* %__x1, %"class.draco::IndexType.114"* %__x2, %"class.draco::IndexType.114"* %__x3, %"class.draco::IndexType.114"* %__x4, %"class.draco::IndexType.114"* %__x5, %"struct.draco::MeshAreEquivalent::FaceIndexLess"* nonnull align 4 dereferenceable(4) %__c) #0 comdat {
entry:
  %__x1.addr = alloca %"class.draco::IndexType.114"*, align 4
  %__x2.addr = alloca %"class.draco::IndexType.114"*, align 4
  %__x3.addr = alloca %"class.draco::IndexType.114"*, align 4
  %__x4.addr = alloca %"class.draco::IndexType.114"*, align 4
  %__x5.addr = alloca %"class.draco::IndexType.114"*, align 4
  %__c.addr = alloca %"struct.draco::MeshAreEquivalent::FaceIndexLess"*, align 4
  %__r = alloca i32, align 4
  %agg.tmp = alloca %"class.draco::IndexType.114", align 4
  %agg.tmp1 = alloca %"class.draco::IndexType.114", align 4
  %agg.tmp4 = alloca %"class.draco::IndexType.114", align 4
  %agg.tmp5 = alloca %"class.draco::IndexType.114", align 4
  %agg.tmp11 = alloca %"class.draco::IndexType.114", align 4
  %agg.tmp12 = alloca %"class.draco::IndexType.114", align 4
  %agg.tmp18 = alloca %"class.draco::IndexType.114", align 4
  %agg.tmp19 = alloca %"class.draco::IndexType.114", align 4
  store %"class.draco::IndexType.114"* %__x1, %"class.draco::IndexType.114"** %__x1.addr, align 4
  store %"class.draco::IndexType.114"* %__x2, %"class.draco::IndexType.114"** %__x2.addr, align 4
  store %"class.draco::IndexType.114"* %__x3, %"class.draco::IndexType.114"** %__x3.addr, align 4
  store %"class.draco::IndexType.114"* %__x4, %"class.draco::IndexType.114"** %__x4.addr, align 4
  store %"class.draco::IndexType.114"* %__x5, %"class.draco::IndexType.114"** %__x5.addr, align 4
  store %"struct.draco::MeshAreEquivalent::FaceIndexLess"* %__c, %"struct.draco::MeshAreEquivalent::FaceIndexLess"** %__c.addr, align 4
  %0 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__x1.addr, align 4
  %1 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__x2.addr, align 4
  %2 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__x3.addr, align 4
  %3 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__x4.addr, align 4
  %4 = load %"struct.draco::MeshAreEquivalent::FaceIndexLess"*, %"struct.draco::MeshAreEquivalent::FaceIndexLess"** %__c.addr, align 4
  %call = call i32 @_ZNSt3__27__sort4IRN5draco17MeshAreEquivalent13FaceIndexLessEPNS1_9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEjT0_S9_S9_S9_T_(%"class.draco::IndexType.114"* %0, %"class.draco::IndexType.114"* %1, %"class.draco::IndexType.114"* %2, %"class.draco::IndexType.114"* %3, %"struct.draco::MeshAreEquivalent::FaceIndexLess"* nonnull align 4 dereferenceable(4) %4)
  store i32 %call, i32* %__r, align 4
  %5 = load %"struct.draco::MeshAreEquivalent::FaceIndexLess"*, %"struct.draco::MeshAreEquivalent::FaceIndexLess"** %__c.addr, align 4
  %6 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__x5.addr, align 4
  %7 = bitcast %"class.draco::IndexType.114"* %agg.tmp to i8*
  %8 = bitcast %"class.draco::IndexType.114"* %6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 4, i1 false)
  %9 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__x4.addr, align 4
  %10 = bitcast %"class.draco::IndexType.114"* %agg.tmp1 to i8*
  %11 = bitcast %"class.draco::IndexType.114"* %9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 4, i1 false)
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %agg.tmp, i32 0, i32 0
  %12 = load i32, i32* %coerce.dive, align 4
  %coerce.dive2 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %agg.tmp1, i32 0, i32 0
  %13 = load i32, i32* %coerce.dive2, align 4
  %call3 = call zeroext i1 @_ZNK5draco17MeshAreEquivalent13FaceIndexLessclENS_9IndexTypeIjNS_19FaceIndex_tag_type_EEES4_(%"struct.draco::MeshAreEquivalent::FaceIndexLess"* %5, i32 %12, i32 %13)
  br i1 %call3, label %if.then, label %if.end27

if.then:                                          ; preds = %entry
  %14 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__x4.addr, align 4
  %15 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__x5.addr, align 4
  call void @_ZNSt3__24swapIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS6_EE5valueEvE4typeERS6_S9_(%"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %14, %"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %15)
  %16 = load i32, i32* %__r, align 4
  %inc = add i32 %16, 1
  store i32 %inc, i32* %__r, align 4
  %17 = load %"struct.draco::MeshAreEquivalent::FaceIndexLess"*, %"struct.draco::MeshAreEquivalent::FaceIndexLess"** %__c.addr, align 4
  %18 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__x4.addr, align 4
  %19 = bitcast %"class.draco::IndexType.114"* %agg.tmp4 to i8*
  %20 = bitcast %"class.draco::IndexType.114"* %18 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %19, i8* align 4 %20, i32 4, i1 false)
  %21 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__x3.addr, align 4
  %22 = bitcast %"class.draco::IndexType.114"* %agg.tmp5 to i8*
  %23 = bitcast %"class.draco::IndexType.114"* %21 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %22, i8* align 4 %23, i32 4, i1 false)
  %coerce.dive6 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %agg.tmp4, i32 0, i32 0
  %24 = load i32, i32* %coerce.dive6, align 4
  %coerce.dive7 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %agg.tmp5, i32 0, i32 0
  %25 = load i32, i32* %coerce.dive7, align 4
  %call8 = call zeroext i1 @_ZNK5draco17MeshAreEquivalent13FaceIndexLessclENS_9IndexTypeIjNS_19FaceIndex_tag_type_EEES4_(%"struct.draco::MeshAreEquivalent::FaceIndexLess"* %17, i32 %24, i32 %25)
  br i1 %call8, label %if.then9, label %if.end26

if.then9:                                         ; preds = %if.then
  %26 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__x3.addr, align 4
  %27 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__x4.addr, align 4
  call void @_ZNSt3__24swapIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS6_EE5valueEvE4typeERS6_S9_(%"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %26, %"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %27)
  %28 = load i32, i32* %__r, align 4
  %inc10 = add i32 %28, 1
  store i32 %inc10, i32* %__r, align 4
  %29 = load %"struct.draco::MeshAreEquivalent::FaceIndexLess"*, %"struct.draco::MeshAreEquivalent::FaceIndexLess"** %__c.addr, align 4
  %30 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__x3.addr, align 4
  %31 = bitcast %"class.draco::IndexType.114"* %agg.tmp11 to i8*
  %32 = bitcast %"class.draco::IndexType.114"* %30 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %31, i8* align 4 %32, i32 4, i1 false)
  %33 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__x2.addr, align 4
  %34 = bitcast %"class.draco::IndexType.114"* %agg.tmp12 to i8*
  %35 = bitcast %"class.draco::IndexType.114"* %33 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %34, i8* align 4 %35, i32 4, i1 false)
  %coerce.dive13 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %agg.tmp11, i32 0, i32 0
  %36 = load i32, i32* %coerce.dive13, align 4
  %coerce.dive14 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %agg.tmp12, i32 0, i32 0
  %37 = load i32, i32* %coerce.dive14, align 4
  %call15 = call zeroext i1 @_ZNK5draco17MeshAreEquivalent13FaceIndexLessclENS_9IndexTypeIjNS_19FaceIndex_tag_type_EEES4_(%"struct.draco::MeshAreEquivalent::FaceIndexLess"* %29, i32 %36, i32 %37)
  br i1 %call15, label %if.then16, label %if.end25

if.then16:                                        ; preds = %if.then9
  %38 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__x2.addr, align 4
  %39 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__x3.addr, align 4
  call void @_ZNSt3__24swapIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS6_EE5valueEvE4typeERS6_S9_(%"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %38, %"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %39)
  %40 = load i32, i32* %__r, align 4
  %inc17 = add i32 %40, 1
  store i32 %inc17, i32* %__r, align 4
  %41 = load %"struct.draco::MeshAreEquivalent::FaceIndexLess"*, %"struct.draco::MeshAreEquivalent::FaceIndexLess"** %__c.addr, align 4
  %42 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__x2.addr, align 4
  %43 = bitcast %"class.draco::IndexType.114"* %agg.tmp18 to i8*
  %44 = bitcast %"class.draco::IndexType.114"* %42 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %43, i8* align 4 %44, i32 4, i1 false)
  %45 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__x1.addr, align 4
  %46 = bitcast %"class.draco::IndexType.114"* %agg.tmp19 to i8*
  %47 = bitcast %"class.draco::IndexType.114"* %45 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %46, i8* align 4 %47, i32 4, i1 false)
  %coerce.dive20 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %agg.tmp18, i32 0, i32 0
  %48 = load i32, i32* %coerce.dive20, align 4
  %coerce.dive21 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %agg.tmp19, i32 0, i32 0
  %49 = load i32, i32* %coerce.dive21, align 4
  %call22 = call zeroext i1 @_ZNK5draco17MeshAreEquivalent13FaceIndexLessclENS_9IndexTypeIjNS_19FaceIndex_tag_type_EEES4_(%"struct.draco::MeshAreEquivalent::FaceIndexLess"* %41, i32 %48, i32 %49)
  br i1 %call22, label %if.then23, label %if.end

if.then23:                                        ; preds = %if.then16
  %50 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__x1.addr, align 4
  %51 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__x2.addr, align 4
  call void @_ZNSt3__24swapIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS6_EE5valueEvE4typeERS6_S9_(%"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %50, %"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %51)
  %52 = load i32, i32* %__r, align 4
  %inc24 = add i32 %52, 1
  store i32 %inc24, i32* %__r, align 4
  br label %if.end

if.end:                                           ; preds = %if.then23, %if.then16
  br label %if.end25

if.end25:                                         ; preds = %if.end, %if.then9
  br label %if.end26

if.end26:                                         ; preds = %if.end25, %if.then
  br label %if.end27

if.end27:                                         ; preds = %if.end26, %entry
  %53 = load i32, i32* %__r, align 4
  ret i32 %53
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__218__insertion_sort_3IRN5draco17MeshAreEquivalent13FaceIndexLessEPNS1_9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEvT0_S9_T_(%"class.draco::IndexType.114"* %__first, %"class.draco::IndexType.114"* %__last, %"struct.draco::MeshAreEquivalent::FaceIndexLess"* nonnull align 4 dereferenceable(4) %__comp) #0 comdat {
entry:
  %__first.addr = alloca %"class.draco::IndexType.114"*, align 4
  %__last.addr = alloca %"class.draco::IndexType.114"*, align 4
  %__comp.addr = alloca %"struct.draco::MeshAreEquivalent::FaceIndexLess"*, align 4
  %__j = alloca %"class.draco::IndexType.114"*, align 4
  %__i = alloca %"class.draco::IndexType.114"*, align 4
  %agg.tmp = alloca %"class.draco::IndexType.114", align 4
  %agg.tmp3 = alloca %"class.draco::IndexType.114", align 4
  %__t = alloca %"class.draco::IndexType.114", align 4
  %__k = alloca %"class.draco::IndexType.114"*, align 4
  %agg.tmp10 = alloca %"class.draco::IndexType.114", align 4
  %agg.tmp11 = alloca %"class.draco::IndexType.114", align 4
  store %"class.draco::IndexType.114"* %__first, %"class.draco::IndexType.114"** %__first.addr, align 4
  store %"class.draco::IndexType.114"* %__last, %"class.draco::IndexType.114"** %__last.addr, align 4
  store %"struct.draco::MeshAreEquivalent::FaceIndexLess"* %__comp, %"struct.draco::MeshAreEquivalent::FaceIndexLess"** %__comp.addr, align 4
  %0 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__first.addr, align 4
  %add.ptr = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %0, i32 2
  store %"class.draco::IndexType.114"* %add.ptr, %"class.draco::IndexType.114"** %__j, align 4
  %1 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__first.addr, align 4
  %2 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__first.addr, align 4
  %add.ptr1 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %2, i32 1
  %3 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__j, align 4
  %4 = load %"struct.draco::MeshAreEquivalent::FaceIndexLess"*, %"struct.draco::MeshAreEquivalent::FaceIndexLess"** %__comp.addr, align 4
  %call = call i32 @_ZNSt3__27__sort3IRN5draco17MeshAreEquivalent13FaceIndexLessEPNS1_9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEjT0_S9_S9_T_(%"class.draco::IndexType.114"* %1, %"class.draco::IndexType.114"* %add.ptr1, %"class.draco::IndexType.114"* %3, %"struct.draco::MeshAreEquivalent::FaceIndexLess"* nonnull align 4 dereferenceable(4) %4)
  %5 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__j, align 4
  %add.ptr2 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %5, i32 1
  store %"class.draco::IndexType.114"* %add.ptr2, %"class.draco::IndexType.114"** %__i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %6 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__i, align 4
  %7 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__last.addr, align 4
  %cmp = icmp ne %"class.draco::IndexType.114"* %6, %7
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %8 = load %"struct.draco::MeshAreEquivalent::FaceIndexLess"*, %"struct.draco::MeshAreEquivalent::FaceIndexLess"** %__comp.addr, align 4
  %9 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__i, align 4
  %10 = bitcast %"class.draco::IndexType.114"* %agg.tmp to i8*
  %11 = bitcast %"class.draco::IndexType.114"* %9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 4, i1 false)
  %12 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__j, align 4
  %13 = bitcast %"class.draco::IndexType.114"* %agg.tmp3 to i8*
  %14 = bitcast %"class.draco::IndexType.114"* %12 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %13, i8* align 4 %14, i32 4, i1 false)
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %agg.tmp, i32 0, i32 0
  %15 = load i32, i32* %coerce.dive, align 4
  %coerce.dive4 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %agg.tmp3, i32 0, i32 0
  %16 = load i32, i32* %coerce.dive4, align 4
  %call5 = call zeroext i1 @_ZNK5draco17MeshAreEquivalent13FaceIndexLessclENS_9IndexTypeIjNS_19FaceIndex_tag_type_EEES4_(%"struct.draco::MeshAreEquivalent::FaceIndexLess"* %8, i32 %15, i32 %16)
  br i1 %call5, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %17 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__i, align 4
  %call6 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.114"* @_ZNSt3__24moveIRN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEONS_16remove_referenceIT_E4typeEOS7_(%"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %17) #7
  %18 = bitcast %"class.draco::IndexType.114"* %__t to i8*
  %19 = bitcast %"class.draco::IndexType.114"* %call6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %18, i8* align 4 %19, i32 4, i1 false)
  %20 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__j, align 4
  store %"class.draco::IndexType.114"* %20, %"class.draco::IndexType.114"** %__k, align 4
  %21 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__i, align 4
  store %"class.draco::IndexType.114"* %21, %"class.draco::IndexType.114"** %__j, align 4
  br label %do.body

do.body:                                          ; preds = %land.end, %if.then
  %22 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__k, align 4
  %call7 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.114"* @_ZNSt3__24moveIRN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEONS_16remove_referenceIT_E4typeEOS7_(%"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %22) #7
  %23 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__j, align 4
  %call8 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.114"* @_ZN5draco9IndexTypeIjNS_19FaceIndex_tag_type_EEaSERKS2_(%"class.draco::IndexType.114"* %23, %"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %call7)
  %24 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__k, align 4
  store %"class.draco::IndexType.114"* %24, %"class.draco::IndexType.114"** %__j, align 4
  br label %do.cond

do.cond:                                          ; preds = %do.body
  %25 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__j, align 4
  %26 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__first.addr, align 4
  %cmp9 = icmp ne %"class.draco::IndexType.114"* %25, %26
  br i1 %cmp9, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %do.cond
  %27 = load %"struct.draco::MeshAreEquivalent::FaceIndexLess"*, %"struct.draco::MeshAreEquivalent::FaceIndexLess"** %__comp.addr, align 4
  %28 = bitcast %"class.draco::IndexType.114"* %agg.tmp10 to i8*
  %29 = bitcast %"class.draco::IndexType.114"* %__t to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %28, i8* align 4 %29, i32 4, i1 false)
  %30 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__k, align 4
  %incdec.ptr = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %30, i32 -1
  store %"class.draco::IndexType.114"* %incdec.ptr, %"class.draco::IndexType.114"** %__k, align 4
  %31 = bitcast %"class.draco::IndexType.114"* %agg.tmp11 to i8*
  %32 = bitcast %"class.draco::IndexType.114"* %incdec.ptr to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %31, i8* align 4 %32, i32 4, i1 false)
  %coerce.dive12 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %agg.tmp10, i32 0, i32 0
  %33 = load i32, i32* %coerce.dive12, align 4
  %coerce.dive13 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %agg.tmp11, i32 0, i32 0
  %34 = load i32, i32* %coerce.dive13, align 4
  %call14 = call zeroext i1 @_ZNK5draco17MeshAreEquivalent13FaceIndexLessclENS_9IndexTypeIjNS_19FaceIndex_tag_type_EEES4_(%"struct.draco::MeshAreEquivalent::FaceIndexLess"* %27, i32 %33, i32 %34)
  br label %land.end

land.end:                                         ; preds = %land.rhs, %do.cond
  %35 = phi i1 [ false, %do.cond ], [ %call14, %land.rhs ]
  br i1 %35, label %do.body, label %do.end

do.end:                                           ; preds = %land.end
  %call15 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.114"* @_ZNSt3__24moveIRN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEONS_16remove_referenceIT_E4typeEOS7_(%"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %__t) #7
  %36 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__j, align 4
  %call16 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.114"* @_ZN5draco9IndexTypeIjNS_19FaceIndex_tag_type_EEaSERKS2_(%"class.draco::IndexType.114"* %36, %"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %call15)
  br label %if.end

if.end:                                           ; preds = %do.end, %for.body
  %37 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__i, align 4
  store %"class.draco::IndexType.114"* %37, %"class.draco::IndexType.114"** %__j, align 4
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %38 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__i, align 4
  %incdec.ptr17 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %38, i32 1
  store %"class.draco::IndexType.114"* %incdec.ptr17, %"class.draco::IndexType.114"** %__i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNSt3__227__insertion_sort_incompleteIRN5draco17MeshAreEquivalent13FaceIndexLessEPNS1_9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEbT0_S9_T_(%"class.draco::IndexType.114"* %__first, %"class.draco::IndexType.114"* %__last, %"struct.draco::MeshAreEquivalent::FaceIndexLess"* nonnull align 4 dereferenceable(4) %__comp) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %__first.addr = alloca %"class.draco::IndexType.114"*, align 4
  %__last.addr = alloca %"class.draco::IndexType.114"*, align 4
  %__comp.addr = alloca %"struct.draco::MeshAreEquivalent::FaceIndexLess"*, align 4
  %agg.tmp = alloca %"class.draco::IndexType.114", align 4
  %agg.tmp2 = alloca %"class.draco::IndexType.114", align 4
  %__j = alloca %"class.draco::IndexType.114"*, align 4
  %__limit = alloca i32, align 4
  %__count = alloca i32, align 4
  %__i = alloca %"class.draco::IndexType.114"*, align 4
  %agg.tmp22 = alloca %"class.draco::IndexType.114", align 4
  %agg.tmp23 = alloca %"class.draco::IndexType.114", align 4
  %__t = alloca %"class.draco::IndexType.114", align 4
  %__k = alloca %"class.draco::IndexType.114"*, align 4
  %agg.tmp32 = alloca %"class.draco::IndexType.114", align 4
  %agg.tmp33 = alloca %"class.draco::IndexType.114", align 4
  store %"class.draco::IndexType.114"* %__first, %"class.draco::IndexType.114"** %__first.addr, align 4
  store %"class.draco::IndexType.114"* %__last, %"class.draco::IndexType.114"** %__last.addr, align 4
  store %"struct.draco::MeshAreEquivalent::FaceIndexLess"* %__comp, %"struct.draco::MeshAreEquivalent::FaceIndexLess"** %__comp.addr, align 4
  %0 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__last.addr, align 4
  %1 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__first.addr, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.draco::IndexType.114"* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.draco::IndexType.114"* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  switch i32 %sub.ptr.div, label %sw.epilog [
    i32 0, label %sw.bb
    i32 1, label %sw.bb
    i32 2, label %sw.bb1
    i32 3, label %sw.bb4
    i32 4, label %sw.bb7
    i32 5, label %sw.bb12
  ]

sw.bb:                                            ; preds = %entry, %entry
  store i1 true, i1* %retval, align 1
  br label %return

sw.bb1:                                           ; preds = %entry
  %2 = load %"struct.draco::MeshAreEquivalent::FaceIndexLess"*, %"struct.draco::MeshAreEquivalent::FaceIndexLess"** %__comp.addr, align 4
  %3 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__last.addr, align 4
  %incdec.ptr = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %3, i32 -1
  store %"class.draco::IndexType.114"* %incdec.ptr, %"class.draco::IndexType.114"** %__last.addr, align 4
  %4 = bitcast %"class.draco::IndexType.114"* %agg.tmp to i8*
  %5 = bitcast %"class.draco::IndexType.114"* %incdec.ptr to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 4, i1 false)
  %6 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__first.addr, align 4
  %7 = bitcast %"class.draco::IndexType.114"* %agg.tmp2 to i8*
  %8 = bitcast %"class.draco::IndexType.114"* %6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 4, i1 false)
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %agg.tmp, i32 0, i32 0
  %9 = load i32, i32* %coerce.dive, align 4
  %coerce.dive3 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %agg.tmp2, i32 0, i32 0
  %10 = load i32, i32* %coerce.dive3, align 4
  %call = call zeroext i1 @_ZNK5draco17MeshAreEquivalent13FaceIndexLessclENS_9IndexTypeIjNS_19FaceIndex_tag_type_EEES4_(%"struct.draco::MeshAreEquivalent::FaceIndexLess"* %2, i32 %9, i32 %10)
  br i1 %call, label %if.then, label %if.end

if.then:                                          ; preds = %sw.bb1
  %11 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__first.addr, align 4
  %12 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__last.addr, align 4
  call void @_ZNSt3__24swapIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS6_EE5valueEvE4typeERS6_S9_(%"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %11, %"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %12)
  br label %if.end

if.end:                                           ; preds = %if.then, %sw.bb1
  store i1 true, i1* %retval, align 1
  br label %return

sw.bb4:                                           ; preds = %entry
  %13 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__first.addr, align 4
  %14 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__first.addr, align 4
  %add.ptr = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %14, i32 1
  %15 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__last.addr, align 4
  %incdec.ptr5 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %15, i32 -1
  store %"class.draco::IndexType.114"* %incdec.ptr5, %"class.draco::IndexType.114"** %__last.addr, align 4
  %16 = load %"struct.draco::MeshAreEquivalent::FaceIndexLess"*, %"struct.draco::MeshAreEquivalent::FaceIndexLess"** %__comp.addr, align 4
  %call6 = call i32 @_ZNSt3__27__sort3IRN5draco17MeshAreEquivalent13FaceIndexLessEPNS1_9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEjT0_S9_S9_T_(%"class.draco::IndexType.114"* %13, %"class.draco::IndexType.114"* %add.ptr, %"class.draco::IndexType.114"* %incdec.ptr5, %"struct.draco::MeshAreEquivalent::FaceIndexLess"* nonnull align 4 dereferenceable(4) %16)
  store i1 true, i1* %retval, align 1
  br label %return

sw.bb7:                                           ; preds = %entry
  %17 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__first.addr, align 4
  %18 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__first.addr, align 4
  %add.ptr8 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %18, i32 1
  %19 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__first.addr, align 4
  %add.ptr9 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %19, i32 2
  %20 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__last.addr, align 4
  %incdec.ptr10 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %20, i32 -1
  store %"class.draco::IndexType.114"* %incdec.ptr10, %"class.draco::IndexType.114"** %__last.addr, align 4
  %21 = load %"struct.draco::MeshAreEquivalent::FaceIndexLess"*, %"struct.draco::MeshAreEquivalent::FaceIndexLess"** %__comp.addr, align 4
  %call11 = call i32 @_ZNSt3__27__sort4IRN5draco17MeshAreEquivalent13FaceIndexLessEPNS1_9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEjT0_S9_S9_S9_T_(%"class.draco::IndexType.114"* %17, %"class.draco::IndexType.114"* %add.ptr8, %"class.draco::IndexType.114"* %add.ptr9, %"class.draco::IndexType.114"* %incdec.ptr10, %"struct.draco::MeshAreEquivalent::FaceIndexLess"* nonnull align 4 dereferenceable(4) %21)
  store i1 true, i1* %retval, align 1
  br label %return

sw.bb12:                                          ; preds = %entry
  %22 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__first.addr, align 4
  %23 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__first.addr, align 4
  %add.ptr13 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %23, i32 1
  %24 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__first.addr, align 4
  %add.ptr14 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %24, i32 2
  %25 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__first.addr, align 4
  %add.ptr15 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %25, i32 3
  %26 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__last.addr, align 4
  %incdec.ptr16 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %26, i32 -1
  store %"class.draco::IndexType.114"* %incdec.ptr16, %"class.draco::IndexType.114"** %__last.addr, align 4
  %27 = load %"struct.draco::MeshAreEquivalent::FaceIndexLess"*, %"struct.draco::MeshAreEquivalent::FaceIndexLess"** %__comp.addr, align 4
  %call17 = call i32 @_ZNSt3__27__sort5IRN5draco17MeshAreEquivalent13FaceIndexLessEPNS1_9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEjT0_S9_S9_S9_S9_T_(%"class.draco::IndexType.114"* %22, %"class.draco::IndexType.114"* %add.ptr13, %"class.draco::IndexType.114"* %add.ptr14, %"class.draco::IndexType.114"* %add.ptr15, %"class.draco::IndexType.114"* %incdec.ptr16, %"struct.draco::MeshAreEquivalent::FaceIndexLess"* nonnull align 4 dereferenceable(4) %27)
  store i1 true, i1* %retval, align 1
  br label %return

sw.epilog:                                        ; preds = %entry
  %28 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__first.addr, align 4
  %add.ptr18 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %28, i32 2
  store %"class.draco::IndexType.114"* %add.ptr18, %"class.draco::IndexType.114"** %__j, align 4
  %29 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__first.addr, align 4
  %30 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__first.addr, align 4
  %add.ptr19 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %30, i32 1
  %31 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__j, align 4
  %32 = load %"struct.draco::MeshAreEquivalent::FaceIndexLess"*, %"struct.draco::MeshAreEquivalent::FaceIndexLess"** %__comp.addr, align 4
  %call20 = call i32 @_ZNSt3__27__sort3IRN5draco17MeshAreEquivalent13FaceIndexLessEPNS1_9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEjT0_S9_S9_T_(%"class.draco::IndexType.114"* %29, %"class.draco::IndexType.114"* %add.ptr19, %"class.draco::IndexType.114"* %31, %"struct.draco::MeshAreEquivalent::FaceIndexLess"* nonnull align 4 dereferenceable(4) %32)
  store i32 8, i32* %__limit, align 4
  store i32 0, i32* %__count, align 4
  %33 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__j, align 4
  %add.ptr21 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %33, i32 1
  store %"class.draco::IndexType.114"* %add.ptr21, %"class.draco::IndexType.114"** %__i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %sw.epilog
  %34 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__i, align 4
  %35 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__last.addr, align 4
  %cmp = icmp ne %"class.draco::IndexType.114"* %34, %35
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %36 = load %"struct.draco::MeshAreEquivalent::FaceIndexLess"*, %"struct.draco::MeshAreEquivalent::FaceIndexLess"** %__comp.addr, align 4
  %37 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__i, align 4
  %38 = bitcast %"class.draco::IndexType.114"* %agg.tmp22 to i8*
  %39 = bitcast %"class.draco::IndexType.114"* %37 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %38, i8* align 4 %39, i32 4, i1 false)
  %40 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__j, align 4
  %41 = bitcast %"class.draco::IndexType.114"* %agg.tmp23 to i8*
  %42 = bitcast %"class.draco::IndexType.114"* %40 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %41, i8* align 4 %42, i32 4, i1 false)
  %coerce.dive24 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %agg.tmp22, i32 0, i32 0
  %43 = load i32, i32* %coerce.dive24, align 4
  %coerce.dive25 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %agg.tmp23, i32 0, i32 0
  %44 = load i32, i32* %coerce.dive25, align 4
  %call26 = call zeroext i1 @_ZNK5draco17MeshAreEquivalent13FaceIndexLessclENS_9IndexTypeIjNS_19FaceIndex_tag_type_EEES4_(%"struct.draco::MeshAreEquivalent::FaceIndexLess"* %36, i32 %43, i32 %44)
  br i1 %call26, label %if.then27, label %if.end45

if.then27:                                        ; preds = %for.body
  %45 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__i, align 4
  %call28 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.114"* @_ZNSt3__24moveIRN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEONS_16remove_referenceIT_E4typeEOS7_(%"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %45) #7
  %46 = bitcast %"class.draco::IndexType.114"* %__t to i8*
  %47 = bitcast %"class.draco::IndexType.114"* %call28 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %46, i8* align 4 %47, i32 4, i1 false)
  %48 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__j, align 4
  store %"class.draco::IndexType.114"* %48, %"class.draco::IndexType.114"** %__k, align 4
  %49 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__i, align 4
  store %"class.draco::IndexType.114"* %49, %"class.draco::IndexType.114"** %__j, align 4
  br label %do.body

do.body:                                          ; preds = %land.end, %if.then27
  %50 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__k, align 4
  %call29 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.114"* @_ZNSt3__24moveIRN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEONS_16remove_referenceIT_E4typeEOS7_(%"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %50) #7
  %51 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__j, align 4
  %call30 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.114"* @_ZN5draco9IndexTypeIjNS_19FaceIndex_tag_type_EEaSERKS2_(%"class.draco::IndexType.114"* %51, %"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %call29)
  %52 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__k, align 4
  store %"class.draco::IndexType.114"* %52, %"class.draco::IndexType.114"** %__j, align 4
  br label %do.cond

do.cond:                                          ; preds = %do.body
  %53 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__j, align 4
  %54 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__first.addr, align 4
  %cmp31 = icmp ne %"class.draco::IndexType.114"* %53, %54
  br i1 %cmp31, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %do.cond
  %55 = load %"struct.draco::MeshAreEquivalent::FaceIndexLess"*, %"struct.draco::MeshAreEquivalent::FaceIndexLess"** %__comp.addr, align 4
  %56 = bitcast %"class.draco::IndexType.114"* %agg.tmp32 to i8*
  %57 = bitcast %"class.draco::IndexType.114"* %__t to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %56, i8* align 4 %57, i32 4, i1 false)
  %58 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__k, align 4
  %incdec.ptr34 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %58, i32 -1
  store %"class.draco::IndexType.114"* %incdec.ptr34, %"class.draco::IndexType.114"** %__k, align 4
  %59 = bitcast %"class.draco::IndexType.114"* %agg.tmp33 to i8*
  %60 = bitcast %"class.draco::IndexType.114"* %incdec.ptr34 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %59, i8* align 4 %60, i32 4, i1 false)
  %coerce.dive35 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %agg.tmp32, i32 0, i32 0
  %61 = load i32, i32* %coerce.dive35, align 4
  %coerce.dive36 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %agg.tmp33, i32 0, i32 0
  %62 = load i32, i32* %coerce.dive36, align 4
  %call37 = call zeroext i1 @_ZNK5draco17MeshAreEquivalent13FaceIndexLessclENS_9IndexTypeIjNS_19FaceIndex_tag_type_EEES4_(%"struct.draco::MeshAreEquivalent::FaceIndexLess"* %55, i32 %61, i32 %62)
  br label %land.end

land.end:                                         ; preds = %land.rhs, %do.cond
  %63 = phi i1 [ false, %do.cond ], [ %call37, %land.rhs ]
  br i1 %63, label %do.body, label %do.end

do.end:                                           ; preds = %land.end
  %call38 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.114"* @_ZNSt3__24moveIRN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEONS_16remove_referenceIT_E4typeEOS7_(%"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %__t) #7
  %64 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__j, align 4
  %call39 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.114"* @_ZN5draco9IndexTypeIjNS_19FaceIndex_tag_type_EEaSERKS2_(%"class.draco::IndexType.114"* %64, %"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %call38)
  %65 = load i32, i32* %__count, align 4
  %inc = add i32 %65, 1
  store i32 %inc, i32* %__count, align 4
  %cmp40 = icmp eq i32 %inc, 8
  br i1 %cmp40, label %if.then41, label %if.end44

if.then41:                                        ; preds = %do.end
  %66 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__i, align 4
  %incdec.ptr42 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %66, i32 1
  store %"class.draco::IndexType.114"* %incdec.ptr42, %"class.draco::IndexType.114"** %__i, align 4
  %67 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__last.addr, align 4
  %cmp43 = icmp eq %"class.draco::IndexType.114"* %incdec.ptr42, %67
  store i1 %cmp43, i1* %retval, align 1
  br label %return

if.end44:                                         ; preds = %do.end
  br label %if.end45

if.end45:                                         ; preds = %if.end44, %for.body
  %68 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__i, align 4
  store %"class.draco::IndexType.114"* %68, %"class.draco::IndexType.114"** %__j, align 4
  br label %for.inc

for.inc:                                          ; preds = %if.end45
  %69 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__i, align 4
  %incdec.ptr46 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %69, i32 1
  store %"class.draco::IndexType.114"* %incdec.ptr46, %"class.draco::IndexType.114"** %__i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %for.end, %if.then41, %sw.bb12, %sw.bb7, %sw.bb4, %if.end, %sw.bb
  %70 = load i1, i1* %retval, align 1
  ret i1 %70
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.114"* @_ZNSt3__24moveIRN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEEEEONS_16remove_referenceIT_E4typeEOS7_(%"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.draco::IndexType.114"*, align 4
  store %"class.draco::IndexType.114"* %__t, %"class.draco::IndexType.114"** %__t.addr, align 4
  %0 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__t.addr, align 4
  ret %"class.draco::IndexType.114"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.114"* @_ZN5draco9IndexTypeIjNS_19FaceIndex_tag_type_EEaSERKS2_(%"class.draco::IndexType.114"* %this, %"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %i) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.114"*, align 4
  %i.addr = alloca %"class.draco::IndexType.114"*, align 4
  store %"class.draco::IndexType.114"* %this, %"class.draco::IndexType.114"** %this.addr, align 4
  store %"class.draco::IndexType.114"* %i, %"class.draco::IndexType.114"** %i.addr, align 4
  %this1 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %this.addr, align 4
  %0 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %i.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %0, i32 0, i32 0
  %1 = load i32, i32* %value_, align 4
  %value_2 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %this1, i32 0, i32 0
  store i32 %1, i32* %value_2, align 4
  ret %"class.draco::IndexType.114"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZN5draco7VectorDIfLi3EEixEi(%"class.draco::VectorD"* %this, i32 %i) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::VectorD"*, align 4
  %i.addr = alloca i32, align 4
  store %"class.draco::VectorD"* %this, %"class.draco::VectorD"** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %"class.draco::VectorD"*, %"class.draco::VectorD"** %this.addr, align 4
  %v_ = getelementptr inbounds %"class.draco::VectorD", %"class.draco::VectorD"* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNSt3__25arrayIfLm3EEixEm(%"struct.std::__2::array.126"* %v_, i32 %0) #7
  ret float* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE4sizeEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 1
  %1 = load %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %2, i32 0, i32 0
  %3 = load %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"struct.draco::MeshAreEquivalent::MeshInfo"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"struct.draco::MeshAreEquivalent::MeshInfo"* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 28
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE5clearEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  %0 = load %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__begin_, align 4
  call void @_ZNSt3__213__vector_baseIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE17__destruct_at_endEPS3_(%"class.std::__2::__vector_base"* %this1, %"struct.draco::MeshAreEquivalent::MeshInfo"* %0) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE17__annotate_shrinkEm(%"class.std::__2::vector"* %this, i32 %__old_size) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__old_size.addr = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__old_size, i32* %__old_size.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %call = call %"struct.draco::MeshAreEquivalent::MeshInfo"* @_ZNKSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE4dataEv(%"class.std::__2::vector"* %this1) #7
  %0 = bitcast %"struct.draco::MeshAreEquivalent::MeshInfo"* %call to i8*
  %call2 = call %"struct.draco::MeshAreEquivalent::MeshInfo"* @_ZNKSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE4dataEv(%"class.std::__2::vector"* %this1) #7
  %call3 = call i32 @_ZNKSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE8capacityEv(%"class.std::__2::vector"* %this1) #7
  %add.ptr = getelementptr inbounds %"struct.draco::MeshAreEquivalent::MeshInfo", %"struct.draco::MeshAreEquivalent::MeshInfo"* %call2, i32 %call3
  %1 = bitcast %"struct.draco::MeshAreEquivalent::MeshInfo"* %add.ptr to i8*
  %call4 = call %"struct.draco::MeshAreEquivalent::MeshInfo"* @_ZNKSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE4dataEv(%"class.std::__2::vector"* %this1) #7
  %2 = load i32, i32* %__old_size.addr, align 4
  %add.ptr5 = getelementptr inbounds %"struct.draco::MeshAreEquivalent::MeshInfo", %"struct.draco::MeshAreEquivalent::MeshInfo"* %call4, i32 %2
  %3 = bitcast %"struct.draco::MeshAreEquivalent::MeshInfo"* %add.ptr5 to i8*
  %call6 = call %"struct.draco::MeshAreEquivalent::MeshInfo"* @_ZNKSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE4dataEv(%"class.std::__2::vector"* %this1) #7
  %call7 = call i32 @_ZNKSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE4sizeEv(%"class.std::__2::vector"* %this1) #7
  %add.ptr8 = getelementptr inbounds %"struct.draco::MeshAreEquivalent::MeshInfo", %"struct.draco::MeshAreEquivalent::MeshInfo"* %call6, i32 %call7
  %4 = bitcast %"struct.draco::MeshAreEquivalent::MeshInfo"* %add.ptr8 to i8*
  call void @_ZNKSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE31__annotate_contiguous_containerEPKvS8_S8_S8_(%"class.std::__2::vector"* %this1, i8* %0, i8* %1, i8* %3, i8* %4) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE17__destruct_at_endEPS3_(%"class.std::__2::__vector_base"* %this, %"struct.draco::MeshAreEquivalent::MeshInfo"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  %__new_last.addr = alloca %"struct.draco::MeshAreEquivalent::MeshInfo"*, align 4
  %__soon_to_be_end = alloca %"struct.draco::MeshAreEquivalent::MeshInfo"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  store %"struct.draco::MeshAreEquivalent::MeshInfo"* %__new_last, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 1
  %0 = load %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__end_, align 4
  store %"struct.draco::MeshAreEquivalent::MeshInfo"* %0, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__soon_to_be_end, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__new_last.addr, align 4
  %2 = load %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__soon_to_be_end, align 4
  %cmp = icmp ne %"struct.draco::MeshAreEquivalent::MeshInfo"* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.124"* @_ZNSt3__213__vector_baseIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE7__allocEv(%"class.std::__2::__vector_base"* %this1) #7
  %3 = load %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__soon_to_be_end, align 4
  %incdec.ptr = getelementptr inbounds %"struct.draco::MeshAreEquivalent::MeshInfo", %"struct.draco::MeshAreEquivalent::MeshInfo"* %3, i32 -1
  store %"struct.draco::MeshAreEquivalent::MeshInfo"* %incdec.ptr, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__soon_to_be_end, align 4
  %call2 = call %"struct.draco::MeshAreEquivalent::MeshInfo"* @_ZNSt3__212__to_addressIN5draco17MeshAreEquivalent8MeshInfoEEEPT_S5_(%"struct.draco::MeshAreEquivalent::MeshInfo"* %incdec.ptr) #7
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco17MeshAreEquivalent8MeshInfoEEEE7destroyIS4_EEvRS5_PT_(%"class.std::__2::allocator.124"* nonnull align 1 dereferenceable(1) %call, %"struct.draco::MeshAreEquivalent::MeshInfo"* %call2)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %4 = load %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__new_last.addr, align 4
  %__end_3 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 1
  store %"struct.draco::MeshAreEquivalent::MeshInfo"* %4, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__end_3, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco17MeshAreEquivalent8MeshInfoEEEE7destroyIS4_EEvRS5_PT_(%"class.std::__2::allocator.124"* nonnull align 1 dereferenceable(1) %__a, %"struct.draco::MeshAreEquivalent::MeshInfo"* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.124"*, align 4
  %__p.addr = alloca %"struct.draco::MeshAreEquivalent::MeshInfo"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy.141", align 1
  store %"class.std::__2::allocator.124"* %__a, %"class.std::__2::allocator.124"** %__a.addr, align 4
  store %"struct.draco::MeshAreEquivalent::MeshInfo"* %__p, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy.141"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.124"*, %"class.std::__2::allocator.124"** %__a.addr, align 4
  %2 = load %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco17MeshAreEquivalent8MeshInfoEEEE9__destroyIS4_EEvNS_17integral_constantIbLb1EEERS5_PT_(%"class.std::__2::allocator.124"* nonnull align 1 dereferenceable(1) %1, %"struct.draco::MeshAreEquivalent::MeshInfo"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.124"* @_ZNSt3__213__vector_baseIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE7__allocEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.124"* @_ZNSt3__217__compressed_pairIPN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE6secondEv(%"class.std::__2::__compressed_pair.121"* %__end_cap_) #7
  ret %"class.std::__2::allocator.124"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.draco::MeshAreEquivalent::MeshInfo"* @_ZNSt3__212__to_addressIN5draco17MeshAreEquivalent8MeshInfoEEEPT_S5_(%"struct.draco::MeshAreEquivalent::MeshInfo"* %__p) #0 comdat {
entry:
  %__p.addr = alloca %"struct.draco::MeshAreEquivalent::MeshInfo"*, align 4
  store %"struct.draco::MeshAreEquivalent::MeshInfo"* %__p, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__p.addr, align 4
  %0 = load %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__p.addr, align 4
  ret %"struct.draco::MeshAreEquivalent::MeshInfo"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco17MeshAreEquivalent8MeshInfoEEEE9__destroyIS4_EEvNS_17integral_constantIbLb1EEERS5_PT_(%"class.std::__2::allocator.124"* nonnull align 1 dereferenceable(1) %__a, %"struct.draco::MeshAreEquivalent::MeshInfo"* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.124"*, align 4
  %__p.addr = alloca %"struct.draco::MeshAreEquivalent::MeshInfo"*, align 4
  store %"class.std::__2::allocator.124"* %__a, %"class.std::__2::allocator.124"** %__a.addr, align 4
  store %"struct.draco::MeshAreEquivalent::MeshInfo"* %__p, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.124"*, %"class.std::__2::allocator.124"** %__a.addr, align 4
  %2 = load %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__p.addr, align 4
  call void @_ZNSt3__29allocatorIN5draco17MeshAreEquivalent8MeshInfoEE7destroyEPS3_(%"class.std::__2::allocator.124"* %1, %"struct.draco::MeshAreEquivalent::MeshInfo"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIN5draco17MeshAreEquivalent8MeshInfoEE7destroyEPS3_(%"class.std::__2::allocator.124"* %this, %"struct.draco::MeshAreEquivalent::MeshInfo"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.124"*, align 4
  %__p.addr = alloca %"struct.draco::MeshAreEquivalent::MeshInfo"*, align 4
  store %"class.std::__2::allocator.124"* %this, %"class.std::__2::allocator.124"** %this.addr, align 4
  store %"struct.draco::MeshAreEquivalent::MeshInfo"* %__p, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.124"*, %"class.std::__2::allocator.124"** %this.addr, align 4
  %0 = load %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__p.addr, align 4
  %call = call %"struct.draco::MeshAreEquivalent::MeshInfo"* @_ZN5draco17MeshAreEquivalent8MeshInfoD2Ev(%"struct.draco::MeshAreEquivalent::MeshInfo"* %0) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.124"* @_ZNSt3__217__compressed_pairIPN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE6secondEv(%"class.std::__2::__compressed_pair.121"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.121"*, align 4
  store %"class.std::__2::__compressed_pair.121"* %this, %"class.std::__2::__compressed_pair.121"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.121"*, %"class.std::__2::__compressed_pair.121"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.121"* %this1 to %"struct.std::__2::__compressed_pair_elem.123"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.124"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco17MeshAreEquivalent8MeshInfoEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.123"* %0) #7
  ret %"class.std::__2::allocator.124"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.124"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco17MeshAreEquivalent8MeshInfoEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.123"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.123"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.123"* %this, %"struct.std::__2::__compressed_pair_elem.123"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.123"*, %"struct.std::__2::__compressed_pair_elem.123"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.123"* %this1 to %"class.std::__2::allocator.124"*
  ret %"class.std::__2::allocator.124"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE31__annotate_contiguous_containerEPKvS8_S8_S8_(%"class.std::__2::vector"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.draco::MeshAreEquivalent::MeshInfo"* @_ZNKSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE4dataEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 0
  %1 = load %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__begin_, align 4
  %call = call %"struct.draco::MeshAreEquivalent::MeshInfo"* @_ZNSt3__212__to_addressIN5draco17MeshAreEquivalent8MeshInfoEEEPT_S5_(%"struct.draco::MeshAreEquivalent::MeshInfo"* %1) #7
  ret %"struct.draco::MeshAreEquivalent::MeshInfo"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE8capacityEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call i32 @_ZNKSt3__213__vector_baseIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE8capacityEv(%"class.std::__2::__vector_base"* %0) #7
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE8capacityEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"struct.draco::MeshAreEquivalent::MeshInfo"** @_ZNKSt3__213__vector_baseIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE9__end_capEv(%"class.std::__2::__vector_base"* %this1) #7
  %0 = load %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  %1 = load %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"struct.draco::MeshAreEquivalent::MeshInfo"* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint %"struct.draco::MeshAreEquivalent::MeshInfo"* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 28
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.draco::MeshAreEquivalent::MeshInfo"** @_ZNKSt3__213__vector_baseIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE9__end_capEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) %"struct.draco::MeshAreEquivalent::MeshInfo"** @_ZNKSt3__217__compressed_pairIPN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE5firstEv(%"class.std::__2::__compressed_pair.121"* %__end_cap_) #7
  ret %"struct.draco::MeshAreEquivalent::MeshInfo"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.draco::MeshAreEquivalent::MeshInfo"** @_ZNKSt3__217__compressed_pairIPN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE5firstEv(%"class.std::__2::__compressed_pair.121"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.121"*, align 4
  store %"class.std::__2::__compressed_pair.121"* %this, %"class.std::__2::__compressed_pair.121"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.121"*, %"class.std::__2::__compressed_pair.121"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.121"* %this1 to %"struct.std::__2::__compressed_pair_elem.122"*
  %call = call nonnull align 4 dereferenceable(4) %"struct.draco::MeshAreEquivalent::MeshInfo"** @_ZNKSt3__222__compressed_pair_elemIPN5draco17MeshAreEquivalent8MeshInfoELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.122"* %0) #7
  ret %"struct.draco::MeshAreEquivalent::MeshInfo"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.draco::MeshAreEquivalent::MeshInfo"** @_ZNKSt3__222__compressed_pair_elemIPN5draco17MeshAreEquivalent8MeshInfoELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.122"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.122"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.122"* %this, %"struct.std::__2::__compressed_pair_elem.122"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.122"*, %"struct.std::__2::__compressed_pair_elem.122"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.122", %"struct.std::__2::__compressed_pair_elem.122"* %this1, i32 0, i32 0
  ret %"struct.draco::MeshAreEquivalent::MeshInfo"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.draco::MeshAreEquivalent::MeshInfo"** @_ZNSt3__213__vector_baseIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE9__end_capEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) %"struct.draco::MeshAreEquivalent::MeshInfo"** @_ZNSt3__217__compressed_pairIPN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE5firstEv(%"class.std::__2::__compressed_pair.121"* %__end_cap_) #7
  ret %"struct.draco::MeshAreEquivalent::MeshInfo"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE22__construct_one_at_endIJS3_EEEvDpOT_(%"class.std::__2::vector"* %this, %"struct.draco::MeshAreEquivalent::MeshInfo"* nonnull align 4 dereferenceable(28) %__args) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__args.addr = alloca %"struct.draco::MeshAreEquivalent::MeshInfo"*, align 4
  %__tx = alloca %"struct.std::__2::vector<draco::MeshAreEquivalent::MeshInfo, std::__2::allocator<draco::MeshAreEquivalent::MeshInfo>>::_ConstructTransaction", align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store %"struct.draco::MeshAreEquivalent::MeshInfo"* %__args, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__args.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %call = call %"struct.std::__2::vector<draco::MeshAreEquivalent::MeshInfo, std::__2::allocator<draco::MeshAreEquivalent::MeshInfo>>::_ConstructTransaction"* @_ZNSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE21_ConstructTransactionC2ERS6_m(%"struct.std::__2::vector<draco::MeshAreEquivalent::MeshInfo, std::__2::allocator<draco::MeshAreEquivalent::MeshInfo>>::_ConstructTransaction"* %__tx, %"class.std::__2::vector"* nonnull align 4 dereferenceable(12) %this1, i32 1)
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.124"* @_ZNSt3__213__vector_baseIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE7__allocEv(%"class.std::__2::__vector_base"* %0) #7
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<draco::MeshAreEquivalent::MeshInfo, std::__2::allocator<draco::MeshAreEquivalent::MeshInfo>>::_ConstructTransaction", %"struct.std::__2::vector<draco::MeshAreEquivalent::MeshInfo, std::__2::allocator<draco::MeshAreEquivalent::MeshInfo>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %1 = load %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__pos_, align 4
  %call3 = call %"struct.draco::MeshAreEquivalent::MeshInfo"* @_ZNSt3__212__to_addressIN5draco17MeshAreEquivalent8MeshInfoEEEPT_S5_(%"struct.draco::MeshAreEquivalent::MeshInfo"* %1) #7
  %2 = load %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__args.addr, align 4
  %call4 = call nonnull align 4 dereferenceable(28) %"struct.draco::MeshAreEquivalent::MeshInfo"* @_ZNSt3__27forwardIN5draco17MeshAreEquivalent8MeshInfoEEEOT_RNS_16remove_referenceIS4_E4typeE(%"struct.draco::MeshAreEquivalent::MeshInfo"* nonnull align 4 dereferenceable(28) %2) #7
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco17MeshAreEquivalent8MeshInfoEEEE9constructIS4_JS4_EEEvRS5_PT_DpOT0_(%"class.std::__2::allocator.124"* nonnull align 1 dereferenceable(1) %call2, %"struct.draco::MeshAreEquivalent::MeshInfo"* %call3, %"struct.draco::MeshAreEquivalent::MeshInfo"* nonnull align 4 dereferenceable(28) %call4)
  %__pos_5 = getelementptr inbounds %"struct.std::__2::vector<draco::MeshAreEquivalent::MeshInfo, std::__2::allocator<draco::MeshAreEquivalent::MeshInfo>>::_ConstructTransaction", %"struct.std::__2::vector<draco::MeshAreEquivalent::MeshInfo, std::__2::allocator<draco::MeshAreEquivalent::MeshInfo>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %3 = load %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__pos_5, align 4
  %incdec.ptr = getelementptr inbounds %"struct.draco::MeshAreEquivalent::MeshInfo", %"struct.draco::MeshAreEquivalent::MeshInfo"* %3, i32 1
  store %"struct.draco::MeshAreEquivalent::MeshInfo"* %incdec.ptr, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__pos_5, align 4
  %call6 = call %"struct.std::__2::vector<draco::MeshAreEquivalent::MeshInfo, std::__2::allocator<draco::MeshAreEquivalent::MeshInfo>>::_ConstructTransaction"* @_ZNSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<draco::MeshAreEquivalent::MeshInfo, std::__2::allocator<draco::MeshAreEquivalent::MeshInfo>>::_ConstructTransaction"* %__tx) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(28) %"struct.draco::MeshAreEquivalent::MeshInfo"* @_ZNSt3__24moveIRN5draco17MeshAreEquivalent8MeshInfoEEEONS_16remove_referenceIT_E4typeEOS6_(%"struct.draco::MeshAreEquivalent::MeshInfo"* nonnull align 4 dereferenceable(28) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"struct.draco::MeshAreEquivalent::MeshInfo"*, align 4
  store %"struct.draco::MeshAreEquivalent::MeshInfo"* %__t, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__t.addr, align 4
  %0 = load %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__t.addr, align 4
  ret %"struct.draco::MeshAreEquivalent::MeshInfo"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE21__push_back_slow_pathIS3_EEvOT_(%"class.std::__2::vector"* %this, %"struct.draco::MeshAreEquivalent::MeshInfo"* nonnull align 4 dereferenceable(28) %__x) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__x.addr = alloca %"struct.draco::MeshAreEquivalent::MeshInfo"*, align 4
  %__a = alloca %"class.std::__2::allocator.124"*, align 4
  %__v = alloca %"struct.std::__2::__split_buffer.143", align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store %"struct.draco::MeshAreEquivalent::MeshInfo"* %__x, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__x.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.124"* @_ZNSt3__213__vector_baseIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE7__allocEv(%"class.std::__2::__vector_base"* %0) #7
  store %"class.std::__2::allocator.124"* %call, %"class.std::__2::allocator.124"** %__a, align 4
  %call2 = call i32 @_ZNKSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE4sizeEv(%"class.std::__2::vector"* %this1) #7
  %add = add i32 %call2, 1
  %call3 = call i32 @_ZNKSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE11__recommendEm(%"class.std::__2::vector"* %this1, i32 %add)
  %call4 = call i32 @_ZNKSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE4sizeEv(%"class.std::__2::vector"* %this1) #7
  %1 = load %"class.std::__2::allocator.124"*, %"class.std::__2::allocator.124"** %__a, align 4
  %call5 = call %"struct.std::__2::__split_buffer.143"* @_ZNSt3__214__split_bufferIN5draco17MeshAreEquivalent8MeshInfoERNS_9allocatorIS3_EEEC2EmmS6_(%"struct.std::__2::__split_buffer.143"* %__v, i32 %call3, i32 %call4, %"class.std::__2::allocator.124"* nonnull align 1 dereferenceable(1) %1)
  %2 = load %"class.std::__2::allocator.124"*, %"class.std::__2::allocator.124"** %__a, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer.143", %"struct.std::__2::__split_buffer.143"* %__v, i32 0, i32 2
  %3 = load %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__end_, align 4
  %call6 = call %"struct.draco::MeshAreEquivalent::MeshInfo"* @_ZNSt3__212__to_addressIN5draco17MeshAreEquivalent8MeshInfoEEEPT_S5_(%"struct.draco::MeshAreEquivalent::MeshInfo"* %3) #7
  %4 = load %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__x.addr, align 4
  %call7 = call nonnull align 4 dereferenceable(28) %"struct.draco::MeshAreEquivalent::MeshInfo"* @_ZNSt3__27forwardIN5draco17MeshAreEquivalent8MeshInfoEEEOT_RNS_16remove_referenceIS4_E4typeE(%"struct.draco::MeshAreEquivalent::MeshInfo"* nonnull align 4 dereferenceable(28) %4) #7
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco17MeshAreEquivalent8MeshInfoEEEE9constructIS4_JS4_EEEvRS5_PT_DpOT0_(%"class.std::__2::allocator.124"* nonnull align 1 dereferenceable(1) %2, %"struct.draco::MeshAreEquivalent::MeshInfo"* %call6, %"struct.draco::MeshAreEquivalent::MeshInfo"* nonnull align 4 dereferenceable(28) %call7)
  %__end_8 = getelementptr inbounds %"struct.std::__2::__split_buffer.143", %"struct.std::__2::__split_buffer.143"* %__v, i32 0, i32 2
  %5 = load %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__end_8, align 4
  %incdec.ptr = getelementptr inbounds %"struct.draco::MeshAreEquivalent::MeshInfo", %"struct.draco::MeshAreEquivalent::MeshInfo"* %5, i32 1
  store %"struct.draco::MeshAreEquivalent::MeshInfo"* %incdec.ptr, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__end_8, align 4
  call void @_ZNSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS3_RS5_EE(%"class.std::__2::vector"* %this1, %"struct.std::__2::__split_buffer.143"* nonnull align 4 dereferenceable(20) %__v)
  %call9 = call %"struct.std::__2::__split_buffer.143"* @_ZNSt3__214__split_bufferIN5draco17MeshAreEquivalent8MeshInfoERNS_9allocatorIS3_EEED2Ev(%"struct.std::__2::__split_buffer.143"* %__v) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.draco::MeshAreEquivalent::MeshInfo"** @_ZNSt3__217__compressed_pairIPN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE5firstEv(%"class.std::__2::__compressed_pair.121"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.121"*, align 4
  store %"class.std::__2::__compressed_pair.121"* %this, %"class.std::__2::__compressed_pair.121"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.121"*, %"class.std::__2::__compressed_pair.121"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.121"* %this1 to %"struct.std::__2::__compressed_pair_elem.122"*
  %call = call nonnull align 4 dereferenceable(4) %"struct.draco::MeshAreEquivalent::MeshInfo"** @_ZNSt3__222__compressed_pair_elemIPN5draco17MeshAreEquivalent8MeshInfoELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.122"* %0) #7
  ret %"struct.draco::MeshAreEquivalent::MeshInfo"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.draco::MeshAreEquivalent::MeshInfo"** @_ZNSt3__222__compressed_pair_elemIPN5draco17MeshAreEquivalent8MeshInfoELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.122"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.122"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.122"* %this, %"struct.std::__2::__compressed_pair_elem.122"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.122"*, %"struct.std::__2::__compressed_pair_elem.122"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.122", %"struct.std::__2::__compressed_pair_elem.122"* %this1, i32 0, i32 0
  ret %"struct.draco::MeshAreEquivalent::MeshInfo"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<draco::MeshAreEquivalent::MeshInfo, std::__2::allocator<draco::MeshAreEquivalent::MeshInfo>>::_ConstructTransaction"* @_ZNSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE21_ConstructTransactionC2ERS6_m(%"struct.std::__2::vector<draco::MeshAreEquivalent::MeshInfo, std::__2::allocator<draco::MeshAreEquivalent::MeshInfo>>::_ConstructTransaction"* returned %this, %"class.std::__2::vector"* nonnull align 4 dereferenceable(12) %__v, i32 %__n) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<draco::MeshAreEquivalent::MeshInfo, std::__2::allocator<draco::MeshAreEquivalent::MeshInfo>>::_ConstructTransaction"*, align 4
  %__v.addr = alloca %"class.std::__2::vector"*, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::vector<draco::MeshAreEquivalent::MeshInfo, std::__2::allocator<draco::MeshAreEquivalent::MeshInfo>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<draco::MeshAreEquivalent::MeshInfo, std::__2::allocator<draco::MeshAreEquivalent::MeshInfo>>::_ConstructTransaction"** %this.addr, align 4
  store %"class.std::__2::vector"* %__v, %"class.std::__2::vector"** %__v.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::vector<draco::MeshAreEquivalent::MeshInfo, std::__2::allocator<draco::MeshAreEquivalent::MeshInfo>>::_ConstructTransaction"*, %"struct.std::__2::vector<draco::MeshAreEquivalent::MeshInfo, std::__2::allocator<draco::MeshAreEquivalent::MeshInfo>>::_ConstructTransaction"** %this.addr, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<draco::MeshAreEquivalent::MeshInfo, std::__2::allocator<draco::MeshAreEquivalent::MeshInfo>>::_ConstructTransaction", %"struct.std::__2::vector<draco::MeshAreEquivalent::MeshInfo, std::__2::allocator<draco::MeshAreEquivalent::MeshInfo>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__v.addr, align 4
  store %"class.std::__2::vector"* %0, %"class.std::__2::vector"** %__v_, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<draco::MeshAreEquivalent::MeshInfo, std::__2::allocator<draco::MeshAreEquivalent::MeshInfo>>::_ConstructTransaction", %"struct.std::__2::vector<draco::MeshAreEquivalent::MeshInfo, std::__2::allocator<draco::MeshAreEquivalent::MeshInfo>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__v.addr, align 4
  %2 = bitcast %"class.std::__2::vector"* %1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %2, i32 0, i32 1
  %3 = load %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__end_, align 4
  store %"struct.draco::MeshAreEquivalent::MeshInfo"* %3, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__pos_, align 4
  %__new_end_ = getelementptr inbounds %"struct.std::__2::vector<draco::MeshAreEquivalent::MeshInfo, std::__2::allocator<draco::MeshAreEquivalent::MeshInfo>>::_ConstructTransaction", %"struct.std::__2::vector<draco::MeshAreEquivalent::MeshInfo, std::__2::allocator<draco::MeshAreEquivalent::MeshInfo>>::_ConstructTransaction"* %this1, i32 0, i32 2
  %4 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__v.addr, align 4
  %5 = bitcast %"class.std::__2::vector"* %4 to %"class.std::__2::__vector_base"*
  %__end_2 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %5, i32 0, i32 1
  %6 = load %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__end_2, align 4
  %7 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds %"struct.draco::MeshAreEquivalent::MeshInfo", %"struct.draco::MeshAreEquivalent::MeshInfo"* %6, i32 %7
  store %"struct.draco::MeshAreEquivalent::MeshInfo"* %add.ptr, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__new_end_, align 4
  ret %"struct.std::__2::vector<draco::MeshAreEquivalent::MeshInfo, std::__2::allocator<draco::MeshAreEquivalent::MeshInfo>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco17MeshAreEquivalent8MeshInfoEEEE9constructIS4_JS4_EEEvRS5_PT_DpOT0_(%"class.std::__2::allocator.124"* nonnull align 1 dereferenceable(1) %__a, %"struct.draco::MeshAreEquivalent::MeshInfo"* %__p, %"struct.draco::MeshAreEquivalent::MeshInfo"* nonnull align 4 dereferenceable(28) %__args) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.124"*, align 4
  %__p.addr = alloca %"struct.draco::MeshAreEquivalent::MeshInfo"*, align 4
  %__args.addr = alloca %"struct.draco::MeshAreEquivalent::MeshInfo"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_construct.142", align 1
  store %"class.std::__2::allocator.124"* %__a, %"class.std::__2::allocator.124"** %__a.addr, align 4
  store %"struct.draco::MeshAreEquivalent::MeshInfo"* %__p, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__p.addr, align 4
  store %"struct.draco::MeshAreEquivalent::MeshInfo"* %__args, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__args.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_construct.142"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.124"*, %"class.std::__2::allocator.124"** %__a.addr, align 4
  %2 = load %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__p.addr, align 4
  %3 = load %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(28) %"struct.draco::MeshAreEquivalent::MeshInfo"* @_ZNSt3__27forwardIN5draco17MeshAreEquivalent8MeshInfoEEEOT_RNS_16remove_referenceIS4_E4typeE(%"struct.draco::MeshAreEquivalent::MeshInfo"* nonnull align 4 dereferenceable(28) %3) #7
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco17MeshAreEquivalent8MeshInfoEEEE11__constructIS4_JS4_EEEvNS_17integral_constantIbLb1EEERS5_PT_DpOT0_(%"class.std::__2::allocator.124"* nonnull align 1 dereferenceable(1) %1, %"struct.draco::MeshAreEquivalent::MeshInfo"* %2, %"struct.draco::MeshAreEquivalent::MeshInfo"* nonnull align 4 dereferenceable(28) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(28) %"struct.draco::MeshAreEquivalent::MeshInfo"* @_ZNSt3__27forwardIN5draco17MeshAreEquivalent8MeshInfoEEEOT_RNS_16remove_referenceIS4_E4typeE(%"struct.draco::MeshAreEquivalent::MeshInfo"* nonnull align 4 dereferenceable(28) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"struct.draco::MeshAreEquivalent::MeshInfo"*, align 4
  store %"struct.draco::MeshAreEquivalent::MeshInfo"* %__t, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__t.addr, align 4
  %0 = load %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__t.addr, align 4
  ret %"struct.draco::MeshAreEquivalent::MeshInfo"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<draco::MeshAreEquivalent::MeshInfo, std::__2::allocator<draco::MeshAreEquivalent::MeshInfo>>::_ConstructTransaction"* @_ZNSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<draco::MeshAreEquivalent::MeshInfo, std::__2::allocator<draco::MeshAreEquivalent::MeshInfo>>::_ConstructTransaction"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<draco::MeshAreEquivalent::MeshInfo, std::__2::allocator<draco::MeshAreEquivalent::MeshInfo>>::_ConstructTransaction"*, align 4
  store %"struct.std::__2::vector<draco::MeshAreEquivalent::MeshInfo, std::__2::allocator<draco::MeshAreEquivalent::MeshInfo>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<draco::MeshAreEquivalent::MeshInfo, std::__2::allocator<draco::MeshAreEquivalent::MeshInfo>>::_ConstructTransaction"** %this.addr, align 4
  %this1 = load %"struct.std::__2::vector<draco::MeshAreEquivalent::MeshInfo, std::__2::allocator<draco::MeshAreEquivalent::MeshInfo>>::_ConstructTransaction"*, %"struct.std::__2::vector<draco::MeshAreEquivalent::MeshInfo, std::__2::allocator<draco::MeshAreEquivalent::MeshInfo>>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<draco::MeshAreEquivalent::MeshInfo, std::__2::allocator<draco::MeshAreEquivalent::MeshInfo>>::_ConstructTransaction", %"struct.std::__2::vector<draco::MeshAreEquivalent::MeshInfo, std::__2::allocator<draco::MeshAreEquivalent::MeshInfo>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %0 = load %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__pos_, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<draco::MeshAreEquivalent::MeshInfo, std::__2::allocator<draco::MeshAreEquivalent::MeshInfo>>::_ConstructTransaction", %"struct.std::__2::vector<draco::MeshAreEquivalent::MeshInfo, std::__2::allocator<draco::MeshAreEquivalent::MeshInfo>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__v_, align 4
  %2 = bitcast %"class.std::__2::vector"* %1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %2, i32 0, i32 1
  store %"struct.draco::MeshAreEquivalent::MeshInfo"* %0, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__end_, align 4
  ret %"struct.std::__2::vector<draco::MeshAreEquivalent::MeshInfo, std::__2::allocator<draco::MeshAreEquivalent::MeshInfo>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco17MeshAreEquivalent8MeshInfoEEEE11__constructIS4_JS4_EEEvNS_17integral_constantIbLb1EEERS5_PT_DpOT0_(%"class.std::__2::allocator.124"* nonnull align 1 dereferenceable(1) %__a, %"struct.draco::MeshAreEquivalent::MeshInfo"* %__p, %"struct.draco::MeshAreEquivalent::MeshInfo"* nonnull align 4 dereferenceable(28) %__args) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.124"*, align 4
  %__p.addr = alloca %"struct.draco::MeshAreEquivalent::MeshInfo"*, align 4
  %__args.addr = alloca %"struct.draco::MeshAreEquivalent::MeshInfo"*, align 4
  store %"class.std::__2::allocator.124"* %__a, %"class.std::__2::allocator.124"** %__a.addr, align 4
  store %"struct.draco::MeshAreEquivalent::MeshInfo"* %__p, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__p.addr, align 4
  store %"struct.draco::MeshAreEquivalent::MeshInfo"* %__args, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__args.addr, align 4
  %1 = load %"class.std::__2::allocator.124"*, %"class.std::__2::allocator.124"** %__a.addr, align 4
  %2 = load %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__p.addr, align 4
  %3 = load %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(28) %"struct.draco::MeshAreEquivalent::MeshInfo"* @_ZNSt3__27forwardIN5draco17MeshAreEquivalent8MeshInfoEEEOT_RNS_16remove_referenceIS4_E4typeE(%"struct.draco::MeshAreEquivalent::MeshInfo"* nonnull align 4 dereferenceable(28) %3) #7
  call void @_ZNSt3__29allocatorIN5draco17MeshAreEquivalent8MeshInfoEE9constructIS3_JS3_EEEvPT_DpOT0_(%"class.std::__2::allocator.124"* %1, %"struct.draco::MeshAreEquivalent::MeshInfo"* %2, %"struct.draco::MeshAreEquivalent::MeshInfo"* nonnull align 4 dereferenceable(28) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIN5draco17MeshAreEquivalent8MeshInfoEE9constructIS3_JS3_EEEvPT_DpOT0_(%"class.std::__2::allocator.124"* %this, %"struct.draco::MeshAreEquivalent::MeshInfo"* %__p, %"struct.draco::MeshAreEquivalent::MeshInfo"* nonnull align 4 dereferenceable(28) %__args) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.124"*, align 4
  %__p.addr = alloca %"struct.draco::MeshAreEquivalent::MeshInfo"*, align 4
  %__args.addr = alloca %"struct.draco::MeshAreEquivalent::MeshInfo"*, align 4
  store %"class.std::__2::allocator.124"* %this, %"class.std::__2::allocator.124"** %this.addr, align 4
  store %"struct.draco::MeshAreEquivalent::MeshInfo"* %__p, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__p.addr, align 4
  store %"struct.draco::MeshAreEquivalent::MeshInfo"* %__args, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__args.addr, align 4
  %this1 = load %"class.std::__2::allocator.124"*, %"class.std::__2::allocator.124"** %this.addr, align 4
  %0 = load %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__p.addr, align 4
  %1 = bitcast %"struct.draco::MeshAreEquivalent::MeshInfo"* %0 to i8*
  %2 = bitcast i8* %1 to %"struct.draco::MeshAreEquivalent::MeshInfo"*
  %3 = load %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(28) %"struct.draco::MeshAreEquivalent::MeshInfo"* @_ZNSt3__27forwardIN5draco17MeshAreEquivalent8MeshInfoEEEOT_RNS_16remove_referenceIS4_E4typeE(%"struct.draco::MeshAreEquivalent::MeshInfo"* nonnull align 4 dereferenceable(28) %3) #7
  %call2 = call %"struct.draco::MeshAreEquivalent::MeshInfo"* @_ZN5draco17MeshAreEquivalent8MeshInfoC2EOS1_(%"struct.draco::MeshAreEquivalent::MeshInfo"* %2, %"struct.draco::MeshAreEquivalent::MeshInfo"* nonnull align 4 dereferenceable(28) %call) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.draco::MeshAreEquivalent::MeshInfo"* @_ZN5draco17MeshAreEquivalent8MeshInfoC2EOS1_(%"struct.draco::MeshAreEquivalent::MeshInfo"* returned %this, %"struct.draco::MeshAreEquivalent::MeshInfo"* nonnull align 4 dereferenceable(28) %0) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.draco::MeshAreEquivalent::MeshInfo"*, align 4
  %.addr = alloca %"struct.draco::MeshAreEquivalent::MeshInfo"*, align 4
  store %"struct.draco::MeshAreEquivalent::MeshInfo"* %this, %"struct.draco::MeshAreEquivalent::MeshInfo"** %this.addr, align 4
  store %"struct.draco::MeshAreEquivalent::MeshInfo"* %0, %"struct.draco::MeshAreEquivalent::MeshInfo"** %.addr, align 4
  %this1 = load %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"** %this.addr, align 4
  %mesh = getelementptr inbounds %"struct.draco::MeshAreEquivalent::MeshInfo", %"struct.draco::MeshAreEquivalent::MeshInfo"* %this1, i32 0, i32 0
  %1 = load %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"** %.addr, align 4
  %mesh2 = getelementptr inbounds %"struct.draco::MeshAreEquivalent::MeshInfo", %"struct.draco::MeshAreEquivalent::MeshInfo"* %1, i32 0, i32 0
  %2 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %mesh2, align 4
  store %"class.draco::Mesh"* %2, %"class.draco::Mesh"** %mesh, align 4
  %ordered_index_of_face = getelementptr inbounds %"struct.draco::MeshAreEquivalent::MeshInfo", %"struct.draco::MeshAreEquivalent::MeshInfo"* %this1, i32 0, i32 1
  %3 = load %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"** %.addr, align 4
  %ordered_index_of_face3 = getelementptr inbounds %"struct.draco::MeshAreEquivalent::MeshInfo", %"struct.draco::MeshAreEquivalent::MeshInfo"* %3, i32 0, i32 1
  %call = call %"class.std::__2::vector.112"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEEC2EOS7_(%"class.std::__2::vector.112"* %ordered_index_of_face, %"class.std::__2::vector.112"* nonnull align 4 dereferenceable(12) %ordered_index_of_face3) #7
  %corner_index_of_smallest_vertex = getelementptr inbounds %"struct.draco::MeshAreEquivalent::MeshInfo", %"struct.draco::MeshAreEquivalent::MeshInfo"* %this1, i32 0, i32 2
  %4 = load %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"** %.addr, align 4
  %corner_index_of_smallest_vertex4 = getelementptr inbounds %"struct.draco::MeshAreEquivalent::MeshInfo", %"struct.draco::MeshAreEquivalent::MeshInfo"* %4, i32 0, i32 2
  %call5 = call %"class.draco::IndexTypeVector.120"* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEEiEC2EOS4_(%"class.draco::IndexTypeVector.120"* %corner_index_of_smallest_vertex, %"class.draco::IndexTypeVector.120"* nonnull align 4 dereferenceable(12) %corner_index_of_smallest_vertex4) #7
  ret %"struct.draco::MeshAreEquivalent::MeshInfo"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector.112"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEEC2EOS7_(%"class.std::__2::vector.112"* returned %this, %"class.std::__2::vector.112"* nonnull align 4 dereferenceable(12) %__x) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.112"*, align 4
  %__x.addr = alloca %"class.std::__2::vector.112"*, align 4
  store %"class.std::__2::vector.112"* %this, %"class.std::__2::vector.112"** %this.addr, align 4
  store %"class.std::__2::vector.112"* %__x, %"class.std::__2::vector.112"** %__x.addr, align 4
  %this1 = load %"class.std::__2::vector.112"*, %"class.std::__2::vector.112"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.112"* %this1 to %"class.std::__2::__vector_base.113"*
  %1 = load %"class.std::__2::vector.112"*, %"class.std::__2::vector.112"** %__x.addr, align 4
  %2 = bitcast %"class.std::__2::vector.112"* %1 to %"class.std::__2::__vector_base.113"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.118"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.113"* %2) #7
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.118"* @_ZNSt3__24moveIRNS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEEEEONS_16remove_referenceIT_E4typeEOS9_(%"class.std::__2::allocator.118"* nonnull align 1 dereferenceable(1) %call) #7
  %call3 = call %"class.std::__2::__vector_base.113"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEEC2EOS6_(%"class.std::__2::__vector_base.113"* %0, %"class.std::__2::allocator.118"* nonnull align 1 dereferenceable(1) %call2) #7
  %3 = load %"class.std::__2::vector.112"*, %"class.std::__2::vector.112"** %__x.addr, align 4
  %4 = bitcast %"class.std::__2::vector.112"* %3 to %"class.std::__2::__vector_base.113"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.113", %"class.std::__2::__vector_base.113"* %4, i32 0, i32 0
  %5 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__begin_, align 4
  %6 = bitcast %"class.std::__2::vector.112"* %this1 to %"class.std::__2::__vector_base.113"*
  %__begin_4 = getelementptr inbounds %"class.std::__2::__vector_base.113", %"class.std::__2::__vector_base.113"* %6, i32 0, i32 0
  store %"class.draco::IndexType.114"* %5, %"class.draco::IndexType.114"** %__begin_4, align 4
  %7 = load %"class.std::__2::vector.112"*, %"class.std::__2::vector.112"** %__x.addr, align 4
  %8 = bitcast %"class.std::__2::vector.112"* %7 to %"class.std::__2::__vector_base.113"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.113", %"class.std::__2::__vector_base.113"* %8, i32 0, i32 1
  %9 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %__end_, align 4
  %10 = bitcast %"class.std::__2::vector.112"* %this1 to %"class.std::__2::__vector_base.113"*
  %__end_5 = getelementptr inbounds %"class.std::__2::__vector_base.113", %"class.std::__2::__vector_base.113"* %10, i32 0, i32 1
  store %"class.draco::IndexType.114"* %9, %"class.draco::IndexType.114"** %__end_5, align 4
  %11 = load %"class.std::__2::vector.112"*, %"class.std::__2::vector.112"** %__x.addr, align 4
  %12 = bitcast %"class.std::__2::vector.112"* %11 to %"class.std::__2::__vector_base.113"*
  %call6 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.114"** @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base.113"* %12) #7
  %13 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %call6, align 4
  %14 = bitcast %"class.std::__2::vector.112"* %this1 to %"class.std::__2::__vector_base.113"*
  %call7 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.114"** @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base.113"* %14) #7
  store %"class.draco::IndexType.114"* %13, %"class.draco::IndexType.114"** %call7, align 4
  %15 = load %"class.std::__2::vector.112"*, %"class.std::__2::vector.112"** %__x.addr, align 4
  %16 = bitcast %"class.std::__2::vector.112"* %15 to %"class.std::__2::__vector_base.113"*
  %call8 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.114"** @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base.113"* %16) #7
  store %"class.draco::IndexType.114"* null, %"class.draco::IndexType.114"** %call8, align 4
  %17 = load %"class.std::__2::vector.112"*, %"class.std::__2::vector.112"** %__x.addr, align 4
  %18 = bitcast %"class.std::__2::vector.112"* %17 to %"class.std::__2::__vector_base.113"*
  %__end_9 = getelementptr inbounds %"class.std::__2::__vector_base.113", %"class.std::__2::__vector_base.113"* %18, i32 0, i32 1
  store %"class.draco::IndexType.114"* null, %"class.draco::IndexType.114"** %__end_9, align 4
  %19 = load %"class.std::__2::vector.112"*, %"class.std::__2::vector.112"** %__x.addr, align 4
  %20 = bitcast %"class.std::__2::vector.112"* %19 to %"class.std::__2::__vector_base.113"*
  %__begin_10 = getelementptr inbounds %"class.std::__2::__vector_base.113", %"class.std::__2::__vector_base.113"* %20, i32 0, i32 0
  store %"class.draco::IndexType.114"* null, %"class.draco::IndexType.114"** %__begin_10, align 4
  ret %"class.std::__2::vector.112"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexTypeVector.120"* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEEiEC2EOS4_(%"class.draco::IndexTypeVector.120"* returned %this, %"class.draco::IndexTypeVector.120"* nonnull align 4 dereferenceable(12) %0) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector.120"*, align 4
  %.addr = alloca %"class.draco::IndexTypeVector.120"*, align 4
  store %"class.draco::IndexTypeVector.120"* %this, %"class.draco::IndexTypeVector.120"** %this.addr, align 4
  store %"class.draco::IndexTypeVector.120"* %0, %"class.draco::IndexTypeVector.120"** %.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector.120"*, %"class.draco::IndexTypeVector.120"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector.120", %"class.draco::IndexTypeVector.120"* %this1, i32 0, i32 0
  %1 = load %"class.draco::IndexTypeVector.120"*, %"class.draco::IndexTypeVector.120"** %.addr, align 4
  %vector_2 = getelementptr inbounds %"class.draco::IndexTypeVector.120", %"class.draco::IndexTypeVector.120"* %1, i32 0, i32 0
  %call = call %"class.std::__2::vector.89"* @_ZNSt3__26vectorIiNS_9allocatorIiEEEC2EOS3_(%"class.std::__2::vector.89"* %vector_, %"class.std::__2::vector.89"* nonnull align 4 dereferenceable(12) %vector_2) #7
  ret %"class.draco::IndexTypeVector.120"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.118"* @_ZNSt3__24moveIRNS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEEEEONS_16remove_referenceIT_E4typeEOS9_(%"class.std::__2::allocator.118"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::allocator.118"*, align 4
  store %"class.std::__2::allocator.118"* %__t, %"class.std::__2::allocator.118"** %__t.addr, align 4
  %0 = load %"class.std::__2::allocator.118"*, %"class.std::__2::allocator.118"** %__t.addr, align 4
  ret %"class.std::__2::allocator.118"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base.113"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEEC2EOS6_(%"class.std::__2::__vector_base.113"* returned %this, %"class.std::__2::allocator.118"* nonnull align 1 dereferenceable(1) %__a) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.113"*, align 4
  %__a.addr = alloca %"class.std::__2::allocator.118"*, align 4
  %ref.tmp = alloca i8*, align 4
  store %"class.std::__2::__vector_base.113"* %this, %"class.std::__2::__vector_base.113"** %this.addr, align 4
  store %"class.std::__2::allocator.118"* %__a, %"class.std::__2::allocator.118"** %__a.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.113"*, %"class.std::__2::__vector_base.113"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__vector_base.113"* %this1 to %"class.std::__2::__vector_base_common"*
  %call = call %"class.std::__2::__vector_base_common"* @_ZNSt3__220__vector_base_commonILb1EEC2Ev(%"class.std::__2::__vector_base_common"* %0)
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.113", %"class.std::__2::__vector_base.113"* %this1, i32 0, i32 0
  store %"class.draco::IndexType.114"* null, %"class.draco::IndexType.114"** %__begin_, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.113", %"class.std::__2::__vector_base.113"* %this1, i32 0, i32 1
  store %"class.draco::IndexType.114"* null, %"class.draco::IndexType.114"** %__end_, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.113", %"class.std::__2::__vector_base.113"* %this1, i32 0, i32 2
  store i8* null, i8** %ref.tmp, align 4
  %1 = load %"class.std::__2::allocator.118"*, %"class.std::__2::allocator.118"** %__a.addr, align 4
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.118"* @_ZNSt3__24moveIRNS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEEEEONS_16remove_referenceIT_E4typeEOS9_(%"class.std::__2::allocator.118"* nonnull align 1 dereferenceable(1) %1) #7
  %call3 = call %"class.std::__2::__compressed_pair.115"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEEC2IDnS7_EEOT_OT0_(%"class.std::__2::__compressed_pair.115"* %__end_cap_, i8** nonnull align 4 dereferenceable(4) %ref.tmp, %"class.std::__2::allocator.118"* nonnull align 1 dereferenceable(1) %call2)
  ret %"class.std::__2::__vector_base.113"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.115"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEENS_9allocatorIS4_EEEC2IDnS7_EEOT_OT0_(%"class.std::__2::__compressed_pair.115"* returned %this, i8** nonnull align 4 dereferenceable(4) %__t1, %"class.std::__2::allocator.118"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.115"*, align 4
  %__t1.addr = alloca i8**, align 4
  %__t2.addr = alloca %"class.std::__2::allocator.118"*, align 4
  store %"class.std::__2::__compressed_pair.115"* %this, %"class.std::__2::__compressed_pair.115"** %this.addr, align 4
  store i8** %__t1, i8*** %__t1.addr, align 4
  store %"class.std::__2::allocator.118"* %__t2, %"class.std::__2::allocator.118"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.115"*, %"class.std::__2::__compressed_pair.115"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.115"* %this1 to %"struct.std::__2::__compressed_pair_elem.116"*
  %1 = load i8**, i8*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %1) #7
  %call2 = call %"struct.std::__2::__compressed_pair_elem.116"* @_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_19FaceIndex_tag_type_EEELi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.116"* %0, i8** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.115"* %this1 to %"struct.std::__2::__compressed_pair_elem.117"*
  %3 = load %"class.std::__2::allocator.118"*, %"class.std::__2::allocator.118"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.118"* @_ZNSt3__27forwardINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"class.std::__2::allocator.118"* nonnull align 1 dereferenceable(1) %3) #7
  %call4 = call %"struct.std::__2::__compressed_pair_elem.117"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEELi1ELb1EEC2IS6_vEEOT_(%"struct.std::__2::__compressed_pair_elem.117"* %2, %"class.std::__2::allocator.118"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::__compressed_pair.115"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.118"* @_ZNSt3__27forwardINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"class.std::__2::allocator.118"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::allocator.118"*, align 4
  store %"class.std::__2::allocator.118"* %__t, %"class.std::__2::allocator.118"** %__t.addr, align 4
  %0 = load %"class.std::__2::allocator.118"*, %"class.std::__2::allocator.118"** %__t.addr, align 4
  ret %"class.std::__2::allocator.118"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.117"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEELi1ELb1EEC2IS6_vEEOT_(%"struct.std::__2::__compressed_pair_elem.117"* returned %this, %"class.std::__2::allocator.118"* nonnull align 1 dereferenceable(1) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.117"*, align 4
  %__u.addr = alloca %"class.std::__2::allocator.118"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.117"* %this, %"struct.std::__2::__compressed_pair_elem.117"** %this.addr, align 4
  store %"class.std::__2::allocator.118"* %__u, %"class.std::__2::allocator.118"** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.117"*, %"struct.std::__2::__compressed_pair_elem.117"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.117"* %this1 to %"class.std::__2::allocator.118"*
  %1 = load %"class.std::__2::allocator.118"*, %"class.std::__2::allocator.118"** %__u.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.118"* @_ZNSt3__27forwardINS_9allocatorIN5draco9IndexTypeIjNS2_19FaceIndex_tag_type_EEEEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"class.std::__2::allocator.118"* nonnull align 1 dereferenceable(1) %1) #7
  ret %"struct.std::__2::__compressed_pair_elem.117"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector.89"* @_ZNSt3__26vectorIiNS_9allocatorIiEEEC2EOS3_(%"class.std::__2::vector.89"* returned %this, %"class.std::__2::vector.89"* nonnull align 4 dereferenceable(12) %__x) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.89"*, align 4
  %__x.addr = alloca %"class.std::__2::vector.89"*, align 4
  store %"class.std::__2::vector.89"* %this, %"class.std::__2::vector.89"** %this.addr, align 4
  store %"class.std::__2::vector.89"* %__x, %"class.std::__2::vector.89"** %__x.addr, align 4
  %this1 = load %"class.std::__2::vector.89"*, %"class.std::__2::vector.89"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.89"* %this1 to %"class.std::__2::__vector_base.90"*
  %1 = load %"class.std::__2::vector.89"*, %"class.std::__2::vector.89"** %__x.addr, align 4
  %2 = bitcast %"class.std::__2::vector.89"* %1 to %"class.std::__2::__vector_base.90"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.94"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE7__allocEv(%"class.std::__2::__vector_base.90"* %2) #7
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.94"* @_ZNSt3__24moveIRNS_9allocatorIiEEEEONS_16remove_referenceIT_E4typeEOS5_(%"class.std::__2::allocator.94"* nonnull align 1 dereferenceable(1) %call) #7
  %call3 = call %"class.std::__2::__vector_base.90"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEEC2EOS2_(%"class.std::__2::__vector_base.90"* %0, %"class.std::__2::allocator.94"* nonnull align 1 dereferenceable(1) %call2) #7
  %3 = load %"class.std::__2::vector.89"*, %"class.std::__2::vector.89"** %__x.addr, align 4
  %4 = bitcast %"class.std::__2::vector.89"* %3 to %"class.std::__2::__vector_base.90"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.90", %"class.std::__2::__vector_base.90"* %4, i32 0, i32 0
  %5 = load i32*, i32** %__begin_, align 4
  %6 = bitcast %"class.std::__2::vector.89"* %this1 to %"class.std::__2::__vector_base.90"*
  %__begin_4 = getelementptr inbounds %"class.std::__2::__vector_base.90", %"class.std::__2::__vector_base.90"* %6, i32 0, i32 0
  store i32* %5, i32** %__begin_4, align 4
  %7 = load %"class.std::__2::vector.89"*, %"class.std::__2::vector.89"** %__x.addr, align 4
  %8 = bitcast %"class.std::__2::vector.89"* %7 to %"class.std::__2::__vector_base.90"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.90", %"class.std::__2::__vector_base.90"* %8, i32 0, i32 1
  %9 = load i32*, i32** %__end_, align 4
  %10 = bitcast %"class.std::__2::vector.89"* %this1 to %"class.std::__2::__vector_base.90"*
  %__end_5 = getelementptr inbounds %"class.std::__2::__vector_base.90", %"class.std::__2::__vector_base.90"* %10, i32 0, i32 1
  store i32* %9, i32** %__end_5, align 4
  %11 = load %"class.std::__2::vector.89"*, %"class.std::__2::vector.89"** %__x.addr, align 4
  %12 = bitcast %"class.std::__2::vector.89"* %11 to %"class.std::__2::__vector_base.90"*
  %call6 = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE9__end_capEv(%"class.std::__2::__vector_base.90"* %12) #7
  %13 = load i32*, i32** %call6, align 4
  %14 = bitcast %"class.std::__2::vector.89"* %this1 to %"class.std::__2::__vector_base.90"*
  %call7 = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE9__end_capEv(%"class.std::__2::__vector_base.90"* %14) #7
  store i32* %13, i32** %call7, align 4
  %15 = load %"class.std::__2::vector.89"*, %"class.std::__2::vector.89"** %__x.addr, align 4
  %16 = bitcast %"class.std::__2::vector.89"* %15 to %"class.std::__2::__vector_base.90"*
  %call8 = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE9__end_capEv(%"class.std::__2::__vector_base.90"* %16) #7
  store i32* null, i32** %call8, align 4
  %17 = load %"class.std::__2::vector.89"*, %"class.std::__2::vector.89"** %__x.addr, align 4
  %18 = bitcast %"class.std::__2::vector.89"* %17 to %"class.std::__2::__vector_base.90"*
  %__end_9 = getelementptr inbounds %"class.std::__2::__vector_base.90", %"class.std::__2::__vector_base.90"* %18, i32 0, i32 1
  store i32* null, i32** %__end_9, align 4
  %19 = load %"class.std::__2::vector.89"*, %"class.std::__2::vector.89"** %__x.addr, align 4
  %20 = bitcast %"class.std::__2::vector.89"* %19 to %"class.std::__2::__vector_base.90"*
  %__begin_10 = getelementptr inbounds %"class.std::__2::__vector_base.90", %"class.std::__2::__vector_base.90"* %20, i32 0, i32 0
  store i32* null, i32** %__begin_10, align 4
  ret %"class.std::__2::vector.89"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.94"* @_ZNSt3__24moveIRNS_9allocatorIiEEEEONS_16remove_referenceIT_E4typeEOS5_(%"class.std::__2::allocator.94"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::allocator.94"*, align 4
  store %"class.std::__2::allocator.94"* %__t, %"class.std::__2::allocator.94"** %__t.addr, align 4
  %0 = load %"class.std::__2::allocator.94"*, %"class.std::__2::allocator.94"** %__t.addr, align 4
  ret %"class.std::__2::allocator.94"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base.90"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEEC2EOS2_(%"class.std::__2::__vector_base.90"* returned %this, %"class.std::__2::allocator.94"* nonnull align 1 dereferenceable(1) %__a) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.90"*, align 4
  %__a.addr = alloca %"class.std::__2::allocator.94"*, align 4
  %ref.tmp = alloca i8*, align 4
  store %"class.std::__2::__vector_base.90"* %this, %"class.std::__2::__vector_base.90"** %this.addr, align 4
  store %"class.std::__2::allocator.94"* %__a, %"class.std::__2::allocator.94"** %__a.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.90"*, %"class.std::__2::__vector_base.90"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__vector_base.90"* %this1 to %"class.std::__2::__vector_base_common"*
  %call = call %"class.std::__2::__vector_base_common"* @_ZNSt3__220__vector_base_commonILb1EEC2Ev(%"class.std::__2::__vector_base_common"* %0)
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.90", %"class.std::__2::__vector_base.90"* %this1, i32 0, i32 0
  store i32* null, i32** %__begin_, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.90", %"class.std::__2::__vector_base.90"* %this1, i32 0, i32 1
  store i32* null, i32** %__end_, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.90", %"class.std::__2::__vector_base.90"* %this1, i32 0, i32 2
  store i8* null, i8** %ref.tmp, align 4
  %1 = load %"class.std::__2::allocator.94"*, %"class.std::__2::allocator.94"** %__a.addr, align 4
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.94"* @_ZNSt3__24moveIRNS_9allocatorIiEEEEONS_16remove_referenceIT_E4typeEOS5_(%"class.std::__2::allocator.94"* nonnull align 1 dereferenceable(1) %1) #7
  %call3 = call %"class.std::__2::__compressed_pair.91"* @_ZNSt3__217__compressed_pairIPiNS_9allocatorIiEEEC2IDnS3_EEOT_OT0_(%"class.std::__2::__compressed_pair.91"* %__end_cap_, i8** nonnull align 4 dereferenceable(4) %ref.tmp, %"class.std::__2::allocator.94"* nonnull align 1 dereferenceable(1) %call2)
  ret %"class.std::__2::__vector_base.90"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.91"* @_ZNSt3__217__compressed_pairIPiNS_9allocatorIiEEEC2IDnS3_EEOT_OT0_(%"class.std::__2::__compressed_pair.91"* returned %this, i8** nonnull align 4 dereferenceable(4) %__t1, %"class.std::__2::allocator.94"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.91"*, align 4
  %__t1.addr = alloca i8**, align 4
  %__t2.addr = alloca %"class.std::__2::allocator.94"*, align 4
  store %"class.std::__2::__compressed_pair.91"* %this, %"class.std::__2::__compressed_pair.91"** %this.addr, align 4
  store i8** %__t1, i8*** %__t1.addr, align 4
  store %"class.std::__2::allocator.94"* %__t2, %"class.std::__2::allocator.94"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.91"*, %"class.std::__2::__compressed_pair.91"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.91"* %this1 to %"struct.std::__2::__compressed_pair_elem.92"*
  %1 = load i8**, i8*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %1) #7
  %call2 = call %"struct.std::__2::__compressed_pair_elem.92"* @_ZNSt3__222__compressed_pair_elemIPiLi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.92"* %0, i8** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.91"* %this1 to %"struct.std::__2::__compressed_pair_elem.93"*
  %3 = load %"class.std::__2::allocator.94"*, %"class.std::__2::allocator.94"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.94"* @_ZNSt3__27forwardINS_9allocatorIiEEEEOT_RNS_16remove_referenceIS3_E4typeE(%"class.std::__2::allocator.94"* nonnull align 1 dereferenceable(1) %3) #7
  %call4 = call %"struct.std::__2::__compressed_pair_elem.93"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIiEELi1ELb1EEC2IS2_vEEOT_(%"struct.std::__2::__compressed_pair_elem.93"* %2, %"class.std::__2::allocator.94"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::__compressed_pair.91"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.94"* @_ZNSt3__27forwardINS_9allocatorIiEEEEOT_RNS_16remove_referenceIS3_E4typeE(%"class.std::__2::allocator.94"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::allocator.94"*, align 4
  store %"class.std::__2::allocator.94"* %__t, %"class.std::__2::allocator.94"** %__t.addr, align 4
  %0 = load %"class.std::__2::allocator.94"*, %"class.std::__2::allocator.94"** %__t.addr, align 4
  ret %"class.std::__2::allocator.94"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.93"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIiEELi1ELb1EEC2IS2_vEEOT_(%"struct.std::__2::__compressed_pair_elem.93"* returned %this, %"class.std::__2::allocator.94"* nonnull align 1 dereferenceable(1) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.93"*, align 4
  %__u.addr = alloca %"class.std::__2::allocator.94"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.93"* %this, %"struct.std::__2::__compressed_pair_elem.93"** %this.addr, align 4
  store %"class.std::__2::allocator.94"* %__u, %"class.std::__2::allocator.94"** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.93"*, %"struct.std::__2::__compressed_pair_elem.93"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.93"* %this1 to %"class.std::__2::allocator.94"*
  %1 = load %"class.std::__2::allocator.94"*, %"class.std::__2::allocator.94"** %__u.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.94"* @_ZNSt3__27forwardINS_9allocatorIiEEEEOT_RNS_16remove_referenceIS3_E4typeE(%"class.std::__2::allocator.94"* nonnull align 1 dereferenceable(1) %1) #7
  ret %"struct.std::__2::__compressed_pair_elem.93"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE11__recommendEm(%"class.std::__2::vector"* %this, i32 %__new_size) #0 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__new_size.addr = alloca i32, align 4
  %__ms = alloca i32, align 4
  %__cap = alloca i32, align 4
  %ref.tmp = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__new_size, i32* %__new_size.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE8max_sizeEv(%"class.std::__2::vector"* %this1) #7
  store i32 %call, i32* %__ms, align 4
  %0 = load i32, i32* %__new_size.addr, align 4
  %1 = load i32, i32* %__ms, align 4
  %cmp = icmp ugt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base_common"*
  call void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"* %2) #10
  unreachable

if.end:                                           ; preds = %entry
  %call2 = call i32 @_ZNKSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE8capacityEv(%"class.std::__2::vector"* %this1) #7
  store i32 %call2, i32* %__cap, align 4
  %3 = load i32, i32* %__cap, align 4
  %4 = load i32, i32* %__ms, align 4
  %div = udiv i32 %4, 2
  %cmp3 = icmp uge i32 %3, %div
  br i1 %cmp3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %if.end
  %5 = load i32, i32* %__ms, align 4
  store i32 %5, i32* %retval, align 4
  br label %return

if.end5:                                          ; preds = %if.end
  %6 = load i32, i32* %__cap, align 4
  %mul = mul i32 2, %6
  store i32 %mul, i32* %ref.tmp, align 4
  %call6 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %__new_size.addr)
  %7 = load i32, i32* %call6, align 4
  store i32 %7, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end5, %if.then4
  %8 = load i32, i32* %retval, align 4
  ret i32 %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer.143"* @_ZNSt3__214__split_bufferIN5draco17MeshAreEquivalent8MeshInfoERNS_9allocatorIS3_EEEC2EmmS6_(%"struct.std::__2::__split_buffer.143"* returned %this, i32 %__cap, i32 %__start, %"class.std::__2::allocator.124"* nonnull align 1 dereferenceable(1) %__a) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"struct.std::__2::__split_buffer.143"*, align 4
  %this.addr = alloca %"struct.std::__2::__split_buffer.143"*, align 4
  %__cap.addr = alloca i32, align 4
  %__start.addr = alloca i32, align 4
  %__a.addr = alloca %"class.std::__2::allocator.124"*, align 4
  %ref.tmp = alloca i8*, align 4
  store %"struct.std::__2::__split_buffer.143"* %this, %"struct.std::__2::__split_buffer.143"** %this.addr, align 4
  store i32 %__cap, i32* %__cap.addr, align 4
  store i32 %__start, i32* %__start.addr, align 4
  store %"class.std::__2::allocator.124"* %__a, %"class.std::__2::allocator.124"** %__a.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.143"*, %"struct.std::__2::__split_buffer.143"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer.143"* %this1, %"struct.std::__2::__split_buffer.143"** %retval, align 4
  %0 = bitcast %"struct.std::__2::__split_buffer.143"* %this1 to %"class.std::__2::__split_buffer_common"*
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer.143", %"struct.std::__2::__split_buffer.143"* %this1, i32 0, i32 3
  store i8* null, i8** %ref.tmp, align 4
  %1 = load %"class.std::__2::allocator.124"*, %"class.std::__2::allocator.124"** %__a.addr, align 4
  %call = call %"class.std::__2::__compressed_pair.144"* @_ZNSt3__217__compressed_pairIPN5draco17MeshAreEquivalent8MeshInfoERNS_9allocatorIS3_EEEC2IDnS7_EEOT_OT0_(%"class.std::__2::__compressed_pair.144"* %__end_cap_, i8** nonnull align 4 dereferenceable(4) %ref.tmp, %"class.std::__2::allocator.124"* nonnull align 1 dereferenceable(1) %1)
  %2 = load i32, i32* %__cap.addr, align 4
  %cmp = icmp ne i32 %2, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.124"* @_ZNSt3__214__split_bufferIN5draco17MeshAreEquivalent8MeshInfoERNS_9allocatorIS3_EEE7__allocEv(%"struct.std::__2::__split_buffer.143"* %this1) #7
  %3 = load i32, i32* %__cap.addr, align 4
  %call3 = call %"struct.draco::MeshAreEquivalent::MeshInfo"* @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco17MeshAreEquivalent8MeshInfoEEEE8allocateERS5_m(%"class.std::__2::allocator.124"* nonnull align 1 dereferenceable(1) %call2, i32 %3)
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %"struct.draco::MeshAreEquivalent::MeshInfo"* [ %call3, %cond.true ], [ null, %cond.false ]
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer.143", %"struct.std::__2::__split_buffer.143"* %this1, i32 0, i32 0
  store %"struct.draco::MeshAreEquivalent::MeshInfo"* %cond, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__first_, align 4
  %__first_4 = getelementptr inbounds %"struct.std::__2::__split_buffer.143", %"struct.std::__2::__split_buffer.143"* %this1, i32 0, i32 0
  %4 = load %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__first_4, align 4
  %5 = load i32, i32* %__start.addr, align 4
  %add.ptr = getelementptr inbounds %"struct.draco::MeshAreEquivalent::MeshInfo", %"struct.draco::MeshAreEquivalent::MeshInfo"* %4, i32 %5
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer.143", %"struct.std::__2::__split_buffer.143"* %this1, i32 0, i32 2
  store %"struct.draco::MeshAreEquivalent::MeshInfo"* %add.ptr, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__end_, align 4
  %__begin_ = getelementptr inbounds %"struct.std::__2::__split_buffer.143", %"struct.std::__2::__split_buffer.143"* %this1, i32 0, i32 1
  store %"struct.draco::MeshAreEquivalent::MeshInfo"* %add.ptr, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__begin_, align 4
  %__first_5 = getelementptr inbounds %"struct.std::__2::__split_buffer.143", %"struct.std::__2::__split_buffer.143"* %this1, i32 0, i32 0
  %6 = load %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__first_5, align 4
  %7 = load i32, i32* %__cap.addr, align 4
  %add.ptr6 = getelementptr inbounds %"struct.draco::MeshAreEquivalent::MeshInfo", %"struct.draco::MeshAreEquivalent::MeshInfo"* %6, i32 %7
  %call7 = call nonnull align 4 dereferenceable(4) %"struct.draco::MeshAreEquivalent::MeshInfo"** @_ZNSt3__214__split_bufferIN5draco17MeshAreEquivalent8MeshInfoERNS_9allocatorIS3_EEE9__end_capEv(%"struct.std::__2::__split_buffer.143"* %this1) #7
  store %"struct.draco::MeshAreEquivalent::MeshInfo"* %add.ptr6, %"struct.draco::MeshAreEquivalent::MeshInfo"** %call7, align 4
  %8 = load %"struct.std::__2::__split_buffer.143"*, %"struct.std::__2::__split_buffer.143"** %retval, align 4
  ret %"struct.std::__2::__split_buffer.143"* %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS3_RS5_EE(%"class.std::__2::vector"* %this, %"struct.std::__2::__split_buffer.143"* nonnull align 4 dereferenceable(20) %__v) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__v.addr = alloca %"struct.std::__2::__split_buffer.143"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer.143"* %__v, %"struct.std::__2::__split_buffer.143"** %__v.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  call void @_ZNKSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE17__annotate_deleteEv(%"class.std::__2::vector"* %this1) #7
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.124"* @_ZNSt3__213__vector_baseIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE7__allocEv(%"class.std::__2::__vector_base"* %0) #7
  %1 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %1, i32 0, i32 0
  %2 = load %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__begin_, align 4
  %3 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %3, i32 0, i32 1
  %4 = load %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__end_, align 4
  %5 = load %"struct.std::__2::__split_buffer.143"*, %"struct.std::__2::__split_buffer.143"** %__v.addr, align 4
  %__begin_2 = getelementptr inbounds %"struct.std::__2::__split_buffer.143", %"struct.std::__2::__split_buffer.143"* %5, i32 0, i32 1
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco17MeshAreEquivalent8MeshInfoEEEE46__construct_backward_with_exception_guaranteesIPS4_EEvRS5_T_SA_RSA_(%"class.std::__2::allocator.124"* nonnull align 1 dereferenceable(1) %call, %"struct.draco::MeshAreEquivalent::MeshInfo"* %2, %"struct.draco::MeshAreEquivalent::MeshInfo"* %4, %"struct.draco::MeshAreEquivalent::MeshInfo"** nonnull align 4 dereferenceable(4) %__begin_2)
  %6 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_3 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %6, i32 0, i32 0
  %7 = load %"struct.std::__2::__split_buffer.143"*, %"struct.std::__2::__split_buffer.143"** %__v.addr, align 4
  %__begin_4 = getelementptr inbounds %"struct.std::__2::__split_buffer.143", %"struct.std::__2::__split_buffer.143"* %7, i32 0, i32 1
  call void @_ZNSt3__24swapIPN5draco17MeshAreEquivalent8MeshInfoEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS6_EE5valueEvE4typeERS6_S9_(%"struct.draco::MeshAreEquivalent::MeshInfo"** nonnull align 4 dereferenceable(4) %__begin_3, %"struct.draco::MeshAreEquivalent::MeshInfo"** nonnull align 4 dereferenceable(4) %__begin_4) #7
  %8 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__end_5 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %8, i32 0, i32 1
  %9 = load %"struct.std::__2::__split_buffer.143"*, %"struct.std::__2::__split_buffer.143"** %__v.addr, align 4
  %__end_6 = getelementptr inbounds %"struct.std::__2::__split_buffer.143", %"struct.std::__2::__split_buffer.143"* %9, i32 0, i32 2
  call void @_ZNSt3__24swapIPN5draco17MeshAreEquivalent8MeshInfoEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS6_EE5valueEvE4typeERS6_S9_(%"struct.draco::MeshAreEquivalent::MeshInfo"** nonnull align 4 dereferenceable(4) %__end_5, %"struct.draco::MeshAreEquivalent::MeshInfo"** nonnull align 4 dereferenceable(4) %__end_6) #7
  %10 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call7 = call nonnull align 4 dereferenceable(4) %"struct.draco::MeshAreEquivalent::MeshInfo"** @_ZNSt3__213__vector_baseIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE9__end_capEv(%"class.std::__2::__vector_base"* %10) #7
  %11 = load %"struct.std::__2::__split_buffer.143"*, %"struct.std::__2::__split_buffer.143"** %__v.addr, align 4
  %call8 = call nonnull align 4 dereferenceable(4) %"struct.draco::MeshAreEquivalent::MeshInfo"** @_ZNSt3__214__split_bufferIN5draco17MeshAreEquivalent8MeshInfoERNS_9allocatorIS3_EEE9__end_capEv(%"struct.std::__2::__split_buffer.143"* %11) #7
  call void @_ZNSt3__24swapIPN5draco17MeshAreEquivalent8MeshInfoEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS6_EE5valueEvE4typeERS6_S9_(%"struct.draco::MeshAreEquivalent::MeshInfo"** nonnull align 4 dereferenceable(4) %call7, %"struct.draco::MeshAreEquivalent::MeshInfo"** nonnull align 4 dereferenceable(4) %call8) #7
  %12 = load %"struct.std::__2::__split_buffer.143"*, %"struct.std::__2::__split_buffer.143"** %__v.addr, align 4
  %__begin_9 = getelementptr inbounds %"struct.std::__2::__split_buffer.143", %"struct.std::__2::__split_buffer.143"* %12, i32 0, i32 1
  %13 = load %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__begin_9, align 4
  %14 = load %"struct.std::__2::__split_buffer.143"*, %"struct.std::__2::__split_buffer.143"** %__v.addr, align 4
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer.143", %"struct.std::__2::__split_buffer.143"* %14, i32 0, i32 0
  store %"struct.draco::MeshAreEquivalent::MeshInfo"* %13, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__first_, align 4
  %call10 = call i32 @_ZNKSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE4sizeEv(%"class.std::__2::vector"* %this1) #7
  call void @_ZNKSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE14__annotate_newEm(%"class.std::__2::vector"* %this1, i32 %call10) #7
  call void @_ZNSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector"* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer.143"* @_ZNSt3__214__split_bufferIN5draco17MeshAreEquivalent8MeshInfoERNS_9allocatorIS3_EEED2Ev(%"struct.std::__2::__split_buffer.143"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"struct.std::__2::__split_buffer.143"*, align 4
  %this.addr = alloca %"struct.std::__2::__split_buffer.143"*, align 4
  store %"struct.std::__2::__split_buffer.143"* %this, %"struct.std::__2::__split_buffer.143"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.143"*, %"struct.std::__2::__split_buffer.143"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer.143"* %this1, %"struct.std::__2::__split_buffer.143"** %retval, align 4
  call void @_ZNSt3__214__split_bufferIN5draco17MeshAreEquivalent8MeshInfoERNS_9allocatorIS3_EEE5clearEv(%"struct.std::__2::__split_buffer.143"* %this1) #7
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer.143", %"struct.std::__2::__split_buffer.143"* %this1, i32 0, i32 0
  %0 = load %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__first_, align 4
  %tobool = icmp ne %"struct.draco::MeshAreEquivalent::MeshInfo"* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.124"* @_ZNSt3__214__split_bufferIN5draco17MeshAreEquivalent8MeshInfoERNS_9allocatorIS3_EEE7__allocEv(%"struct.std::__2::__split_buffer.143"* %this1) #7
  %__first_2 = getelementptr inbounds %"struct.std::__2::__split_buffer.143", %"struct.std::__2::__split_buffer.143"* %this1, i32 0, i32 0
  %1 = load %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__first_2, align 4
  %call3 = call i32 @_ZNKSt3__214__split_bufferIN5draco17MeshAreEquivalent8MeshInfoERNS_9allocatorIS3_EEE8capacityEv(%"struct.std::__2::__split_buffer.143"* %this1)
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco17MeshAreEquivalent8MeshInfoEEEE10deallocateERS5_PS4_m(%"class.std::__2::allocator.124"* nonnull align 1 dereferenceable(1) %call, %"struct.draco::MeshAreEquivalent::MeshInfo"* %1, i32 %call3) #7
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"struct.std::__2::__split_buffer.143"*, %"struct.std::__2::__split_buffer.143"** %retval, align 4
  ret %"struct.std::__2::__split_buffer.143"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE8max_sizeEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp3 = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.124"* @_ZNKSt3__213__vector_baseIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE7__allocEv(%"class.std::__2::__vector_base"* %0) #7
  %call2 = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco17MeshAreEquivalent8MeshInfoEEEE8max_sizeERKS5_(%"class.std::__2::allocator.124"* nonnull align 1 dereferenceable(1) %call) #7
  store i32 %call2, i32* %ref.tmp, align 4
  %call4 = call i32 @_ZNSt3__214numeric_limitsIlE3maxEv() #7
  store i32 %call4, i32* %ref.tmp3, align 4
  %call5 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %1 = load i32, i32* %call5, align 4
  ret i32 %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco17MeshAreEquivalent8MeshInfoEEEE8max_sizeERKS5_(%"class.std::__2::allocator.124"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.124"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_max_size.146", align 1
  store %"class.std::__2::allocator.124"* %__a, %"class.std::__2::allocator.124"** %__a.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_max_size.146"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.124"*, %"class.std::__2::allocator.124"** %__a.addr, align 4
  %call = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco17MeshAreEquivalent8MeshInfoEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS5_(%"class.std::__2::allocator.124"* nonnull align 1 dereferenceable(1) %1) #7
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.124"* @_ZNKSt3__213__vector_baseIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE7__allocEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.124"* @_ZNKSt3__217__compressed_pairIPN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE6secondEv(%"class.std::__2::__compressed_pair.121"* %__end_cap_) #7
  ret %"class.std::__2::allocator.124"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco17MeshAreEquivalent8MeshInfoEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS5_(%"class.std::__2::allocator.124"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.124"*, align 4
  store %"class.std::__2::allocator.124"* %__a, %"class.std::__2::allocator.124"** %__a.addr, align 4
  %1 = load %"class.std::__2::allocator.124"*, %"class.std::__2::allocator.124"** %__a.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorIN5draco17MeshAreEquivalent8MeshInfoEE8max_sizeEv(%"class.std::__2::allocator.124"* %1) #7
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__29allocatorIN5draco17MeshAreEquivalent8MeshInfoEE8max_sizeEv(%"class.std::__2::allocator.124"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.124"*, align 4
  store %"class.std::__2::allocator.124"* %this, %"class.std::__2::allocator.124"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.124"*, %"class.std::__2::allocator.124"** %this.addr, align 4
  ret i32 153391689
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.124"* @_ZNKSt3__217__compressed_pairIPN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE6secondEv(%"class.std::__2::__compressed_pair.121"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.121"*, align 4
  store %"class.std::__2::__compressed_pair.121"* %this, %"class.std::__2::__compressed_pair.121"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.121"*, %"class.std::__2::__compressed_pair.121"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.121"* %this1 to %"struct.std::__2::__compressed_pair_elem.123"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.124"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorIN5draco17MeshAreEquivalent8MeshInfoEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.123"* %0) #7
  ret %"class.std::__2::allocator.124"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.124"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorIN5draco17MeshAreEquivalent8MeshInfoEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.123"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.123"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.123"* %this, %"struct.std::__2::__compressed_pair_elem.123"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.123"*, %"struct.std::__2::__compressed_pair_elem.123"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.123"* %this1 to %"class.std::__2::allocator.124"*
  ret %"class.std::__2::allocator.124"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.144"* @_ZNSt3__217__compressed_pairIPN5draco17MeshAreEquivalent8MeshInfoERNS_9allocatorIS3_EEEC2IDnS7_EEOT_OT0_(%"class.std::__2::__compressed_pair.144"* returned %this, i8** nonnull align 4 dereferenceable(4) %__t1, %"class.std::__2::allocator.124"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.144"*, align 4
  %__t1.addr = alloca i8**, align 4
  %__t2.addr = alloca %"class.std::__2::allocator.124"*, align 4
  store %"class.std::__2::__compressed_pair.144"* %this, %"class.std::__2::__compressed_pair.144"** %this.addr, align 4
  store i8** %__t1, i8*** %__t1.addr, align 4
  store %"class.std::__2::allocator.124"* %__t2, %"class.std::__2::allocator.124"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.144"*, %"class.std::__2::__compressed_pair.144"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.144"* %this1 to %"struct.std::__2::__compressed_pair_elem.122"*
  %1 = load i8**, i8*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %1) #7
  %call2 = call %"struct.std::__2::__compressed_pair_elem.122"* @_ZNSt3__222__compressed_pair_elemIPN5draco17MeshAreEquivalent8MeshInfoELi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.122"* %0, i8** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.144"* %this1 to i8*
  %3 = getelementptr inbounds i8, i8* %2, i32 4
  %4 = bitcast i8* %3 to %"struct.std::__2::__compressed_pair_elem.145"*
  %5 = load %"class.std::__2::allocator.124"*, %"class.std::__2::allocator.124"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.124"* @_ZNSt3__27forwardIRNS_9allocatorIN5draco17MeshAreEquivalent8MeshInfoEEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"class.std::__2::allocator.124"* nonnull align 1 dereferenceable(1) %5) #7
  %call4 = call %"struct.std::__2::__compressed_pair_elem.145"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIN5draco17MeshAreEquivalent8MeshInfoEEELi1ELb0EEC2IS6_vEEOT_(%"struct.std::__2::__compressed_pair_elem.145"* %4, %"class.std::__2::allocator.124"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::__compressed_pair.144"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.draco::MeshAreEquivalent::MeshInfo"* @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco17MeshAreEquivalent8MeshInfoEEEE8allocateERS5_m(%"class.std::__2::allocator.124"* nonnull align 1 dereferenceable(1) %__a, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.124"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.124"* %__a, %"class.std::__2::allocator.124"** %__a.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.124"*, %"class.std::__2::allocator.124"** %__a.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call %"struct.draco::MeshAreEquivalent::MeshInfo"* @_ZNSt3__29allocatorIN5draco17MeshAreEquivalent8MeshInfoEE8allocateEmPKv(%"class.std::__2::allocator.124"* %0, i32 %1, i8* null)
  ret %"struct.draco::MeshAreEquivalent::MeshInfo"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.124"* @_ZNSt3__214__split_bufferIN5draco17MeshAreEquivalent8MeshInfoERNS_9allocatorIS3_EEE7__allocEv(%"struct.std::__2::__split_buffer.143"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.143"*, align 4
  store %"struct.std::__2::__split_buffer.143"* %this, %"struct.std::__2::__split_buffer.143"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.143"*, %"struct.std::__2::__split_buffer.143"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer.143", %"struct.std::__2::__split_buffer.143"* %this1, i32 0, i32 3
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.124"* @_ZNSt3__217__compressed_pairIPN5draco17MeshAreEquivalent8MeshInfoERNS_9allocatorIS3_EEE6secondEv(%"class.std::__2::__compressed_pair.144"* %__end_cap_) #7
  ret %"class.std::__2::allocator.124"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.draco::MeshAreEquivalent::MeshInfo"** @_ZNSt3__214__split_bufferIN5draco17MeshAreEquivalent8MeshInfoERNS_9allocatorIS3_EEE9__end_capEv(%"struct.std::__2::__split_buffer.143"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.143"*, align 4
  store %"struct.std::__2::__split_buffer.143"* %this, %"struct.std::__2::__split_buffer.143"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.143"*, %"struct.std::__2::__split_buffer.143"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer.143", %"struct.std::__2::__split_buffer.143"* %this1, i32 0, i32 3
  %call = call nonnull align 4 dereferenceable(4) %"struct.draco::MeshAreEquivalent::MeshInfo"** @_ZNSt3__217__compressed_pairIPN5draco17MeshAreEquivalent8MeshInfoERNS_9allocatorIS3_EEE5firstEv(%"class.std::__2::__compressed_pair.144"* %__end_cap_) #7
  ret %"struct.draco::MeshAreEquivalent::MeshInfo"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.122"* @_ZNSt3__222__compressed_pair_elemIPN5draco17MeshAreEquivalent8MeshInfoELi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.122"* returned %this, i8** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.122"*, align 4
  %__u.addr = alloca i8**, align 4
  store %"struct.std::__2::__compressed_pair_elem.122"* %this, %"struct.std::__2::__compressed_pair_elem.122"** %this.addr, align 4
  store i8** %__u, i8*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.122"*, %"struct.std::__2::__compressed_pair_elem.122"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.122", %"struct.std::__2::__compressed_pair_elem.122"* %this1, i32 0, i32 0
  %0 = load i8**, i8*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %0) #7
  store %"struct.draco::MeshAreEquivalent::MeshInfo"* null, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.122"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.124"* @_ZNSt3__27forwardIRNS_9allocatorIN5draco17MeshAreEquivalent8MeshInfoEEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"class.std::__2::allocator.124"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::allocator.124"*, align 4
  store %"class.std::__2::allocator.124"* %__t, %"class.std::__2::allocator.124"** %__t.addr, align 4
  %0 = load %"class.std::__2::allocator.124"*, %"class.std::__2::allocator.124"** %__t.addr, align 4
  ret %"class.std::__2::allocator.124"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.145"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIN5draco17MeshAreEquivalent8MeshInfoEEELi1ELb0EEC2IS6_vEEOT_(%"struct.std::__2::__compressed_pair_elem.145"* returned %this, %"class.std::__2::allocator.124"* nonnull align 1 dereferenceable(1) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.145"*, align 4
  %__u.addr = alloca %"class.std::__2::allocator.124"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.145"* %this, %"struct.std::__2::__compressed_pair_elem.145"** %this.addr, align 4
  store %"class.std::__2::allocator.124"* %__u, %"class.std::__2::allocator.124"** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.145"*, %"struct.std::__2::__compressed_pair_elem.145"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.145", %"struct.std::__2::__compressed_pair_elem.145"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::allocator.124"*, %"class.std::__2::allocator.124"** %__u.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.124"* @_ZNSt3__27forwardIRNS_9allocatorIN5draco17MeshAreEquivalent8MeshInfoEEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"class.std::__2::allocator.124"* nonnull align 1 dereferenceable(1) %0) #7
  store %"class.std::__2::allocator.124"* %call, %"class.std::__2::allocator.124"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.145"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.draco::MeshAreEquivalent::MeshInfo"* @_ZNSt3__29allocatorIN5draco17MeshAreEquivalent8MeshInfoEE8allocateEmPKv(%"class.std::__2::allocator.124"* %this, i32 %__n, i8* %0) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.124"*, align 4
  %__n.addr = alloca i32, align 4
  %.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.124"* %this, %"class.std::__2::allocator.124"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::allocator.124"*, %"class.std::__2::allocator.124"** %this.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorIN5draco17MeshAreEquivalent8MeshInfoEE8max_sizeEv(%"class.std::__2::allocator.124"* %this1) #7
  %cmp = icmp ugt i32 %1, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__220__throw_length_errorEPKc(i8* getelementptr inbounds ([68 x i8], [68 x i8]* @.str.2, i32 0, i32 0)) #10
  unreachable

if.end:                                           ; preds = %entry
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 28
  %call2 = call i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %mul, i32 4)
  %3 = bitcast i8* %call2 to %"struct.draco::MeshAreEquivalent::MeshInfo"*
  ret %"struct.draco::MeshAreEquivalent::MeshInfo"* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.124"* @_ZNSt3__217__compressed_pairIPN5draco17MeshAreEquivalent8MeshInfoERNS_9allocatorIS3_EEE6secondEv(%"class.std::__2::__compressed_pair.144"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.144"*, align 4
  store %"class.std::__2::__compressed_pair.144"* %this, %"class.std::__2::__compressed_pair.144"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.144"*, %"class.std::__2::__compressed_pair.144"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.144"* %this1 to i8*
  %add.ptr = getelementptr inbounds i8, i8* %0, i32 4
  %1 = bitcast i8* %add.ptr to %"struct.std::__2::__compressed_pair_elem.145"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.124"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIN5draco17MeshAreEquivalent8MeshInfoEEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.145"* %1) #7
  ret %"class.std::__2::allocator.124"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.124"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIN5draco17MeshAreEquivalent8MeshInfoEEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.145"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.145"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.145"* %this, %"struct.std::__2::__compressed_pair_elem.145"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.145"*, %"struct.std::__2::__compressed_pair_elem.145"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.145", %"struct.std::__2::__compressed_pair_elem.145"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::allocator.124"*, %"class.std::__2::allocator.124"** %__value_, align 4
  ret %"class.std::__2::allocator.124"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.draco::MeshAreEquivalent::MeshInfo"** @_ZNSt3__217__compressed_pairIPN5draco17MeshAreEquivalent8MeshInfoERNS_9allocatorIS3_EEE5firstEv(%"class.std::__2::__compressed_pair.144"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.144"*, align 4
  store %"class.std::__2::__compressed_pair.144"* %this, %"class.std::__2::__compressed_pair.144"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.144"*, %"class.std::__2::__compressed_pair.144"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.144"* %this1 to %"struct.std::__2::__compressed_pair_elem.122"*
  %call = call nonnull align 4 dereferenceable(4) %"struct.draco::MeshAreEquivalent::MeshInfo"** @_ZNSt3__222__compressed_pair_elemIPN5draco17MeshAreEquivalent8MeshInfoELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.122"* %0) #7
  ret %"struct.draco::MeshAreEquivalent::MeshInfo"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE17__annotate_deleteEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %call = call %"struct.draco::MeshAreEquivalent::MeshInfo"* @_ZNKSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE4dataEv(%"class.std::__2::vector"* %this1) #7
  %0 = bitcast %"struct.draco::MeshAreEquivalent::MeshInfo"* %call to i8*
  %call2 = call %"struct.draco::MeshAreEquivalent::MeshInfo"* @_ZNKSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE4dataEv(%"class.std::__2::vector"* %this1) #7
  %call3 = call i32 @_ZNKSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE8capacityEv(%"class.std::__2::vector"* %this1) #7
  %add.ptr = getelementptr inbounds %"struct.draco::MeshAreEquivalent::MeshInfo", %"struct.draco::MeshAreEquivalent::MeshInfo"* %call2, i32 %call3
  %1 = bitcast %"struct.draco::MeshAreEquivalent::MeshInfo"* %add.ptr to i8*
  %call4 = call %"struct.draco::MeshAreEquivalent::MeshInfo"* @_ZNKSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE4dataEv(%"class.std::__2::vector"* %this1) #7
  %call5 = call i32 @_ZNKSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE4sizeEv(%"class.std::__2::vector"* %this1) #7
  %add.ptr6 = getelementptr inbounds %"struct.draco::MeshAreEquivalent::MeshInfo", %"struct.draco::MeshAreEquivalent::MeshInfo"* %call4, i32 %call5
  %2 = bitcast %"struct.draco::MeshAreEquivalent::MeshInfo"* %add.ptr6 to i8*
  %call7 = call %"struct.draco::MeshAreEquivalent::MeshInfo"* @_ZNKSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE4dataEv(%"class.std::__2::vector"* %this1) #7
  %call8 = call i32 @_ZNKSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE8capacityEv(%"class.std::__2::vector"* %this1) #7
  %add.ptr9 = getelementptr inbounds %"struct.draco::MeshAreEquivalent::MeshInfo", %"struct.draco::MeshAreEquivalent::MeshInfo"* %call7, i32 %call8
  %3 = bitcast %"struct.draco::MeshAreEquivalent::MeshInfo"* %add.ptr9 to i8*
  call void @_ZNKSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE31__annotate_contiguous_containerEPKvS8_S8_S8_(%"class.std::__2::vector"* %this1, i8* %0, i8* %1, i8* %2, i8* %3) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco17MeshAreEquivalent8MeshInfoEEEE46__construct_backward_with_exception_guaranteesIPS4_EEvRS5_T_SA_RSA_(%"class.std::__2::allocator.124"* nonnull align 1 dereferenceable(1) %__a, %"struct.draco::MeshAreEquivalent::MeshInfo"* %__begin1, %"struct.draco::MeshAreEquivalent::MeshInfo"* %__end1, %"struct.draco::MeshAreEquivalent::MeshInfo"** nonnull align 4 dereferenceable(4) %__end2) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.124"*, align 4
  %__begin1.addr = alloca %"struct.draco::MeshAreEquivalent::MeshInfo"*, align 4
  %__end1.addr = alloca %"struct.draco::MeshAreEquivalent::MeshInfo"*, align 4
  %__end2.addr = alloca %"struct.draco::MeshAreEquivalent::MeshInfo"**, align 4
  store %"class.std::__2::allocator.124"* %__a, %"class.std::__2::allocator.124"** %__a.addr, align 4
  store %"struct.draco::MeshAreEquivalent::MeshInfo"* %__begin1, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__begin1.addr, align 4
  store %"struct.draco::MeshAreEquivalent::MeshInfo"* %__end1, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__end1.addr, align 4
  store %"struct.draco::MeshAreEquivalent::MeshInfo"** %__end2, %"struct.draco::MeshAreEquivalent::MeshInfo"*** %__end2.addr, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %0 = load %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__end1.addr, align 4
  %1 = load %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__begin1.addr, align 4
  %cmp = icmp ne %"struct.draco::MeshAreEquivalent::MeshInfo"* %0, %1
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %2 = load %"class.std::__2::allocator.124"*, %"class.std::__2::allocator.124"** %__a.addr, align 4
  %3 = load %"struct.draco::MeshAreEquivalent::MeshInfo"**, %"struct.draco::MeshAreEquivalent::MeshInfo"*** %__end2.addr, align 4
  %4 = load %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"** %3, align 4
  %add.ptr = getelementptr inbounds %"struct.draco::MeshAreEquivalent::MeshInfo", %"struct.draco::MeshAreEquivalent::MeshInfo"* %4, i32 -1
  %call = call %"struct.draco::MeshAreEquivalent::MeshInfo"* @_ZNSt3__212__to_addressIN5draco17MeshAreEquivalent8MeshInfoEEEPT_S5_(%"struct.draco::MeshAreEquivalent::MeshInfo"* %add.ptr) #7
  %5 = load %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__end1.addr, align 4
  %incdec.ptr = getelementptr inbounds %"struct.draco::MeshAreEquivalent::MeshInfo", %"struct.draco::MeshAreEquivalent::MeshInfo"* %5, i32 -1
  store %"struct.draco::MeshAreEquivalent::MeshInfo"* %incdec.ptr, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__end1.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(28) %"struct.draco::MeshAreEquivalent::MeshInfo"* @_ZNSt3__24moveIRN5draco17MeshAreEquivalent8MeshInfoEEEONS_16remove_referenceIT_E4typeEOS6_(%"struct.draco::MeshAreEquivalent::MeshInfo"* nonnull align 4 dereferenceable(28) %incdec.ptr) #7
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco17MeshAreEquivalent8MeshInfoEEEE9constructIS4_JS4_EEEvRS5_PT_DpOT0_(%"class.std::__2::allocator.124"* nonnull align 1 dereferenceable(1) %2, %"struct.draco::MeshAreEquivalent::MeshInfo"* %call, %"struct.draco::MeshAreEquivalent::MeshInfo"* nonnull align 4 dereferenceable(28) %call1)
  %6 = load %"struct.draco::MeshAreEquivalent::MeshInfo"**, %"struct.draco::MeshAreEquivalent::MeshInfo"*** %__end2.addr, align 4
  %7 = load %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"** %6, align 4
  %incdec.ptr2 = getelementptr inbounds %"struct.draco::MeshAreEquivalent::MeshInfo", %"struct.draco::MeshAreEquivalent::MeshInfo"* %7, i32 -1
  store %"struct.draco::MeshAreEquivalent::MeshInfo"* %incdec.ptr2, %"struct.draco::MeshAreEquivalent::MeshInfo"** %6, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__24swapIPN5draco17MeshAreEquivalent8MeshInfoEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS6_EE5valueEvE4typeERS6_S9_(%"struct.draco::MeshAreEquivalent::MeshInfo"** nonnull align 4 dereferenceable(4) %__x, %"struct.draco::MeshAreEquivalent::MeshInfo"** nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %__x.addr = alloca %"struct.draco::MeshAreEquivalent::MeshInfo"**, align 4
  %__y.addr = alloca %"struct.draco::MeshAreEquivalent::MeshInfo"**, align 4
  %__t = alloca %"struct.draco::MeshAreEquivalent::MeshInfo"*, align 4
  store %"struct.draco::MeshAreEquivalent::MeshInfo"** %__x, %"struct.draco::MeshAreEquivalent::MeshInfo"*** %__x.addr, align 4
  store %"struct.draco::MeshAreEquivalent::MeshInfo"** %__y, %"struct.draco::MeshAreEquivalent::MeshInfo"*** %__y.addr, align 4
  %0 = load %"struct.draco::MeshAreEquivalent::MeshInfo"**, %"struct.draco::MeshAreEquivalent::MeshInfo"*** %__x.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"struct.draco::MeshAreEquivalent::MeshInfo"** @_ZNSt3__24moveIRPN5draco17MeshAreEquivalent8MeshInfoEEEONS_16remove_referenceIT_E4typeEOS7_(%"struct.draco::MeshAreEquivalent::MeshInfo"** nonnull align 4 dereferenceable(4) %0) #7
  %1 = load %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"** %call, align 4
  store %"struct.draco::MeshAreEquivalent::MeshInfo"* %1, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__t, align 4
  %2 = load %"struct.draco::MeshAreEquivalent::MeshInfo"**, %"struct.draco::MeshAreEquivalent::MeshInfo"*** %__y.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(4) %"struct.draco::MeshAreEquivalent::MeshInfo"** @_ZNSt3__24moveIRPN5draco17MeshAreEquivalent8MeshInfoEEEONS_16remove_referenceIT_E4typeEOS7_(%"struct.draco::MeshAreEquivalent::MeshInfo"** nonnull align 4 dereferenceable(4) %2) #7
  %3 = load %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"** %call1, align 4
  %4 = load %"struct.draco::MeshAreEquivalent::MeshInfo"**, %"struct.draco::MeshAreEquivalent::MeshInfo"*** %__x.addr, align 4
  store %"struct.draco::MeshAreEquivalent::MeshInfo"* %3, %"struct.draco::MeshAreEquivalent::MeshInfo"** %4, align 4
  %call2 = call nonnull align 4 dereferenceable(4) %"struct.draco::MeshAreEquivalent::MeshInfo"** @_ZNSt3__24moveIRPN5draco17MeshAreEquivalent8MeshInfoEEEONS_16remove_referenceIT_E4typeEOS7_(%"struct.draco::MeshAreEquivalent::MeshInfo"** nonnull align 4 dereferenceable(4) %__t) #7
  %5 = load %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"** %call2, align 4
  %6 = load %"struct.draco::MeshAreEquivalent::MeshInfo"**, %"struct.draco::MeshAreEquivalent::MeshInfo"*** %__y.addr, align 4
  store %"struct.draco::MeshAreEquivalent::MeshInfo"* %5, %"struct.draco::MeshAreEquivalent::MeshInfo"** %6, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE14__annotate_newEm(%"class.std::__2::vector"* %this, i32 %__current_size) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__current_size.addr = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__current_size, i32* %__current_size.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %call = call %"struct.draco::MeshAreEquivalent::MeshInfo"* @_ZNKSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE4dataEv(%"class.std::__2::vector"* %this1) #7
  %0 = bitcast %"struct.draco::MeshAreEquivalent::MeshInfo"* %call to i8*
  %call2 = call %"struct.draco::MeshAreEquivalent::MeshInfo"* @_ZNKSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE4dataEv(%"class.std::__2::vector"* %this1) #7
  %call3 = call i32 @_ZNKSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE8capacityEv(%"class.std::__2::vector"* %this1) #7
  %add.ptr = getelementptr inbounds %"struct.draco::MeshAreEquivalent::MeshInfo", %"struct.draco::MeshAreEquivalent::MeshInfo"* %call2, i32 %call3
  %1 = bitcast %"struct.draco::MeshAreEquivalent::MeshInfo"* %add.ptr to i8*
  %call4 = call %"struct.draco::MeshAreEquivalent::MeshInfo"* @_ZNKSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE4dataEv(%"class.std::__2::vector"* %this1) #7
  %call5 = call i32 @_ZNKSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE8capacityEv(%"class.std::__2::vector"* %this1) #7
  %add.ptr6 = getelementptr inbounds %"struct.draco::MeshAreEquivalent::MeshInfo", %"struct.draco::MeshAreEquivalent::MeshInfo"* %call4, i32 %call5
  %2 = bitcast %"struct.draco::MeshAreEquivalent::MeshInfo"* %add.ptr6 to i8*
  %call7 = call %"struct.draco::MeshAreEquivalent::MeshInfo"* @_ZNKSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE4dataEv(%"class.std::__2::vector"* %this1) #7
  %3 = load i32, i32* %__current_size.addr, align 4
  %add.ptr8 = getelementptr inbounds %"struct.draco::MeshAreEquivalent::MeshInfo", %"struct.draco::MeshAreEquivalent::MeshInfo"* %call7, i32 %3
  %4 = bitcast %"struct.draco::MeshAreEquivalent::MeshInfo"* %add.ptr8 to i8*
  call void @_ZNKSt3__26vectorIN5draco17MeshAreEquivalent8MeshInfoENS_9allocatorIS3_EEE31__annotate_contiguous_containerEPKvS8_S8_S8_(%"class.std::__2::vector"* %this1, i8* %0, i8* %1, i8* %2, i8* %4) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.draco::MeshAreEquivalent::MeshInfo"** @_ZNSt3__24moveIRPN5draco17MeshAreEquivalent8MeshInfoEEEONS_16remove_referenceIT_E4typeEOS7_(%"struct.draco::MeshAreEquivalent::MeshInfo"** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"struct.draco::MeshAreEquivalent::MeshInfo"**, align 4
  store %"struct.draco::MeshAreEquivalent::MeshInfo"** %__t, %"struct.draco::MeshAreEquivalent::MeshInfo"*** %__t.addr, align 4
  %0 = load %"struct.draco::MeshAreEquivalent::MeshInfo"**, %"struct.draco::MeshAreEquivalent::MeshInfo"*** %__t.addr, align 4
  ret %"struct.draco::MeshAreEquivalent::MeshInfo"** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIN5draco17MeshAreEquivalent8MeshInfoERNS_9allocatorIS3_EEE5clearEv(%"struct.std::__2::__split_buffer.143"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.143"*, align 4
  store %"struct.std::__2::__split_buffer.143"* %this, %"struct.std::__2::__split_buffer.143"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.143"*, %"struct.std::__2::__split_buffer.143"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"struct.std::__2::__split_buffer.143", %"struct.std::__2::__split_buffer.143"* %this1, i32 0, i32 1
  %0 = load %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__begin_, align 4
  call void @_ZNSt3__214__split_bufferIN5draco17MeshAreEquivalent8MeshInfoERNS_9allocatorIS3_EEE17__destruct_at_endEPS3_(%"struct.std::__2::__split_buffer.143"* %this1, %"struct.draco::MeshAreEquivalent::MeshInfo"* %0) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco17MeshAreEquivalent8MeshInfoEEEE10deallocateERS5_PS4_m(%"class.std::__2::allocator.124"* nonnull align 1 dereferenceable(1) %__a, %"struct.draco::MeshAreEquivalent::MeshInfo"* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.124"*, align 4
  %__p.addr = alloca %"struct.draco::MeshAreEquivalent::MeshInfo"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.124"* %__a, %"class.std::__2::allocator.124"** %__a.addr, align 4
  store %"struct.draco::MeshAreEquivalent::MeshInfo"* %__p, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.124"*, %"class.std::__2::allocator.124"** %__a.addr, align 4
  %1 = load %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorIN5draco17MeshAreEquivalent8MeshInfoEE10deallocateEPS3_m(%"class.std::__2::allocator.124"* %0, %"struct.draco::MeshAreEquivalent::MeshInfo"* %1, i32 %2) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__214__split_bufferIN5draco17MeshAreEquivalent8MeshInfoERNS_9allocatorIS3_EEE8capacityEv(%"struct.std::__2::__split_buffer.143"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.143"*, align 4
  store %"struct.std::__2::__split_buffer.143"* %this, %"struct.std::__2::__split_buffer.143"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.143"*, %"struct.std::__2::__split_buffer.143"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"struct.draco::MeshAreEquivalent::MeshInfo"** @_ZNKSt3__214__split_bufferIN5draco17MeshAreEquivalent8MeshInfoERNS_9allocatorIS3_EEE9__end_capEv(%"struct.std::__2::__split_buffer.143"* %this1) #7
  %0 = load %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"** %call, align 4
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer.143", %"struct.std::__2::__split_buffer.143"* %this1, i32 0, i32 0
  %1 = load %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__first_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"struct.draco::MeshAreEquivalent::MeshInfo"* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint %"struct.draco::MeshAreEquivalent::MeshInfo"* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 28
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIN5draco17MeshAreEquivalent8MeshInfoERNS_9allocatorIS3_EEE17__destruct_at_endEPS3_(%"struct.std::__2::__split_buffer.143"* %this, %"struct.draco::MeshAreEquivalent::MeshInfo"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.143"*, align 4
  %__new_last.addr = alloca %"struct.draco::MeshAreEquivalent::MeshInfo"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant.134", align 1
  store %"struct.std::__2::__split_buffer.143"* %this, %"struct.std::__2::__split_buffer.143"** %this.addr, align 4
  store %"struct.draco::MeshAreEquivalent::MeshInfo"* %__new_last, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__new_last.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.143"*, %"struct.std::__2::__split_buffer.143"** %this.addr, align 4
  %0 = load %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__new_last.addr, align 4
  call void @_ZNSt3__214__split_bufferIN5draco17MeshAreEquivalent8MeshInfoERNS_9allocatorIS3_EEE17__destruct_at_endEPS3_NS_17integral_constantIbLb0EEE(%"struct.std::__2::__split_buffer.143"* %this1, %"struct.draco::MeshAreEquivalent::MeshInfo"* %0) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIN5draco17MeshAreEquivalent8MeshInfoERNS_9allocatorIS3_EEE17__destruct_at_endEPS3_NS_17integral_constantIbLb0EEE(%"struct.std::__2::__split_buffer.143"* %this, %"struct.draco::MeshAreEquivalent::MeshInfo"* %__new_last) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant.134", align 1
  %this.addr = alloca %"struct.std::__2::__split_buffer.143"*, align 4
  %__new_last.addr = alloca %"struct.draco::MeshAreEquivalent::MeshInfo"*, align 4
  store %"struct.std::__2::__split_buffer.143"* %this, %"struct.std::__2::__split_buffer.143"** %this.addr, align 4
  store %"struct.draco::MeshAreEquivalent::MeshInfo"* %__new_last, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__new_last.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.143"*, %"struct.std::__2::__split_buffer.143"** %this.addr, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__new_last.addr, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer.143", %"struct.std::__2::__split_buffer.143"* %this1, i32 0, i32 2
  %2 = load %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__end_, align 4
  %cmp = icmp ne %"struct.draco::MeshAreEquivalent::MeshInfo"* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.124"* @_ZNSt3__214__split_bufferIN5draco17MeshAreEquivalent8MeshInfoERNS_9allocatorIS3_EEE7__allocEv(%"struct.std::__2::__split_buffer.143"* %this1) #7
  %__end_2 = getelementptr inbounds %"struct.std::__2::__split_buffer.143", %"struct.std::__2::__split_buffer.143"* %this1, i32 0, i32 2
  %3 = load %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__end_2, align 4
  %incdec.ptr = getelementptr inbounds %"struct.draco::MeshAreEquivalent::MeshInfo", %"struct.draco::MeshAreEquivalent::MeshInfo"* %3, i32 -1
  store %"struct.draco::MeshAreEquivalent::MeshInfo"* %incdec.ptr, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__end_2, align 4
  %call3 = call %"struct.draco::MeshAreEquivalent::MeshInfo"* @_ZNSt3__212__to_addressIN5draco17MeshAreEquivalent8MeshInfoEEEPT_S5_(%"struct.draco::MeshAreEquivalent::MeshInfo"* %incdec.ptr) #7
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco17MeshAreEquivalent8MeshInfoEEEE7destroyIS4_EEvRS5_PT_(%"class.std::__2::allocator.124"* nonnull align 1 dereferenceable(1) %call, %"struct.draco::MeshAreEquivalent::MeshInfo"* %call3)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIN5draco17MeshAreEquivalent8MeshInfoEE10deallocateEPS3_m(%"class.std::__2::allocator.124"* %this, %"struct.draco::MeshAreEquivalent::MeshInfo"* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.124"*, align 4
  %__p.addr = alloca %"struct.draco::MeshAreEquivalent::MeshInfo"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.124"* %this, %"class.std::__2::allocator.124"** %this.addr, align 4
  store %"struct.draco::MeshAreEquivalent::MeshInfo"* %__p, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.124"*, %"class.std::__2::allocator.124"** %this.addr, align 4
  %0 = load %"struct.draco::MeshAreEquivalent::MeshInfo"*, %"struct.draco::MeshAreEquivalent::MeshInfo"** %__p.addr, align 4
  %1 = bitcast %"struct.draco::MeshAreEquivalent::MeshInfo"* %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 28
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.draco::MeshAreEquivalent::MeshInfo"** @_ZNKSt3__214__split_bufferIN5draco17MeshAreEquivalent8MeshInfoERNS_9allocatorIS3_EEE9__end_capEv(%"struct.std::__2::__split_buffer.143"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.143"*, align 4
  store %"struct.std::__2::__split_buffer.143"* %this, %"struct.std::__2::__split_buffer.143"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.143"*, %"struct.std::__2::__split_buffer.143"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer.143", %"struct.std::__2::__split_buffer.143"* %this1, i32 0, i32 3
  %call = call nonnull align 4 dereferenceable(4) %"struct.draco::MeshAreEquivalent::MeshInfo"** @_ZNKSt3__217__compressed_pairIPN5draco17MeshAreEquivalent8MeshInfoERNS_9allocatorIS3_EEE5firstEv(%"class.std::__2::__compressed_pair.144"* %__end_cap_) #7
  ret %"struct.draco::MeshAreEquivalent::MeshInfo"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.draco::MeshAreEquivalent::MeshInfo"** @_ZNKSt3__217__compressed_pairIPN5draco17MeshAreEquivalent8MeshInfoERNS_9allocatorIS3_EEE5firstEv(%"class.std::__2::__compressed_pair.144"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.144"*, align 4
  store %"class.std::__2::__compressed_pair.144"* %this, %"class.std::__2::__compressed_pair.144"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.144"*, %"class.std::__2::__compressed_pair.144"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.144"* %this1 to %"struct.std::__2::__compressed_pair_elem.122"*
  %call = call nonnull align 4 dereferenceable(4) %"struct.draco::MeshAreEquivalent::MeshInfo"** @_ZNKSt3__222__compressed_pair_elemIPN5draco17MeshAreEquivalent8MeshInfoELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.122"* %0) #7
  ret %"struct.draco::MeshAreEquivalent::MeshInfo"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.128"* @_ZNSt3__217__compressed_pairIPhNS_14default_deleteIA_hEEEC2IRS1_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.128"* returned %this, i8** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.128"*, align 4
  %__t1.addr = alloca i8**, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.128"* %this, %"class.std::__2::__compressed_pair.128"** %this.addr, align 4
  store i8** %__t1, i8*** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.128"*, %"class.std::__2::__compressed_pair.128"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.128"* %this1 to %"struct.std::__2::__compressed_pair_elem.61"*
  %1 = load i8**, i8*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIRPhEEOT_RNS_16remove_referenceIS3_E4typeE(i8** nonnull align 4 dereferenceable(4) %1) #7
  %call2 = call %"struct.std::__2::__compressed_pair_elem.61"* @_ZNSt3__222__compressed_pair_elemIPhLi0ELb0EEC2IRS1_vEEOT_(%"struct.std::__2::__compressed_pair_elem.61"* %0, i8** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.128"* %this1 to %"struct.std::__2::__compressed_pair_elem.129"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #7
  %call4 = call %"struct.std::__2::__compressed_pair_elem.129"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIA_hEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.129"* %2)
  ret %"class.std::__2::__compressed_pair.128"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIRPhEEOT_RNS_16remove_referenceIS3_E4typeE(i8** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca i8**, align 4
  store i8** %__t, i8*** %__t.addr, align 4
  %0 = load i8**, i8*** %__t.addr, align 4
  ret i8** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.61"* @_ZNSt3__222__compressed_pair_elemIPhLi0ELb0EEC2IRS1_vEEOT_(%"struct.std::__2::__compressed_pair_elem.61"* returned %this, i8** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.61"*, align 4
  %__u.addr = alloca i8**, align 4
  store %"struct.std::__2::__compressed_pair_elem.61"* %this, %"struct.std::__2::__compressed_pair_elem.61"** %this.addr, align 4
  store i8** %__u, i8*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.61"*, %"struct.std::__2::__compressed_pair_elem.61"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.61", %"struct.std::__2::__compressed_pair_elem.61"* %this1, i32 0, i32 0
  %0 = load i8**, i8*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIRPhEEOT_RNS_16remove_referenceIS3_E4typeE(i8** nonnull align 4 dereferenceable(4) %0) #7
  %1 = load i8*, i8** %call, align 4
  store i8* %1, i8** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.61"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.129"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIA_hEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.129"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.129"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.129"* %this, %"struct.std::__2::__compressed_pair_elem.129"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.129"*, %"struct.std::__2::__compressed_pair_elem.129"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.129"* %this1 to %"struct.std::__2::default_delete.130"*
  ret %"struct.std::__2::__compressed_pair_elem.129"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__210unique_ptrIA_hNS_14default_deleteIS1_EEE5resetEDn(%"class.std::__2::unique_ptr.127"* %this, i8* %0) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.127"*, align 4
  %.addr = alloca i8*, align 4
  %__tmp = alloca i8*, align 4
  store %"class.std::__2::unique_ptr.127"* %this, %"class.std::__2::unique_ptr.127"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.127"*, %"class.std::__2::unique_ptr.127"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.127", %"class.std::__2::unique_ptr.127"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__217__compressed_pairIPhNS_14default_deleteIA_hEEE5firstEv(%"class.std::__2::__compressed_pair.128"* %__ptr_) #7
  %1 = load i8*, i8** %call, align 4
  store i8* %1, i8** %__tmp, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr.127", %"class.std::__2::unique_ptr.127"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__217__compressed_pairIPhNS_14default_deleteIA_hEEE5firstEv(%"class.std::__2::__compressed_pair.128"* %__ptr_2) #7
  store i8* null, i8** %call3, align 4
  %2 = load i8*, i8** %__tmp, align 4
  %tobool = icmp ne i8* %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__ptr_4 = getelementptr inbounds %"class.std::__2::unique_ptr.127", %"class.std::__2::unique_ptr.127"* %this1, i32 0, i32 0
  %call5 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.130"* @_ZNSt3__217__compressed_pairIPhNS_14default_deleteIA_hEEE6secondEv(%"class.std::__2::__compressed_pair.128"* %__ptr_4) #7
  %3 = load i8*, i8** %__tmp, align 4
  call void @_ZNKSt3__214default_deleteIA_hEclIhEENS2_20_EnableIfConvertibleIT_E4typeEPS5_(%"struct.std::__2::default_delete.130"* %call5, i8* %3) #7
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNSt3__217__compressed_pairIPhNS_14default_deleteIA_hEEE5firstEv(%"class.std::__2::__compressed_pair.128"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.128"*, align 4
  store %"class.std::__2::__compressed_pair.128"* %this, %"class.std::__2::__compressed_pair.128"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.128"*, %"class.std::__2::__compressed_pair.128"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.128"* %this1 to %"struct.std::__2::__compressed_pair_elem.61"*
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__222__compressed_pair_elemIPhLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.61"* %0) #7
  ret i8** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.130"* @_ZNSt3__217__compressed_pairIPhNS_14default_deleteIA_hEEE6secondEv(%"class.std::__2::__compressed_pair.128"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.128"*, align 4
  store %"class.std::__2::__compressed_pair.128"* %this, %"class.std::__2::__compressed_pair.128"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.128"*, %"class.std::__2::__compressed_pair.128"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.128"* %this1 to %"struct.std::__2::__compressed_pair_elem.129"*
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.130"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIA_hEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.129"* %0) #7
  ret %"struct.std::__2::default_delete.130"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__214default_deleteIA_hEclIhEENS2_20_EnableIfConvertibleIT_E4typeEPS5_(%"struct.std::__2::default_delete.130"* %this, i8* %__ptr) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::default_delete.130"*, align 4
  %__ptr.addr = alloca i8*, align 4
  store %"struct.std::__2::default_delete.130"* %this, %"struct.std::__2::default_delete.130"** %this.addr, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  %this1 = load %"struct.std::__2::default_delete.130"*, %"struct.std::__2::default_delete.130"** %this.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  %isnull = icmp eq i8* %0, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %entry
  call void @_ZdaPv(i8* %0) #9
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNSt3__222__compressed_pair_elemIPhLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.61"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.61"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.61"* %this, %"struct.std::__2::__compressed_pair_elem.61"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.61"*, %"struct.std::__2::__compressed_pair_elem.61"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.61", %"struct.std::__2::__compressed_pair_elem.61"* %this1, i32 0, i32 0
  ret i8** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.130"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIA_hEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.129"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.129"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.129"* %this, %"struct.std::__2::__compressed_pair_elem.129"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.129"*, %"struct.std::__2::__compressed_pair_elem.129"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.129"* %this1 to %"struct.std::__2::default_delete.130"*
  ret %"struct.std::__2::default_delete.130"* %0
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdaPv(i8*) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIiNS_9allocatorIiEEEixEm(%"class.std::__2::vector.89"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.89"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector.89"* %this, %"class.std::__2::vector.89"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.89"*, %"class.std::__2::vector.89"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.89"* %this1 to %"class.std::__2::__vector_base.90"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.90", %"class.std::__2::__vector_base.90"* %0, i32 0, i32 0
  %1 = load i32*, i32** %__begin_, align 4
  %2 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds i32, i32* %1, i32 %2
  ret i32* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__217__compressed_pairIPhNS_14default_deleteIA_hEEE5firstEv(%"class.std::__2::__compressed_pair.128"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.128"*, align 4
  store %"class.std::__2::__compressed_pair.128"* %this, %"class.std::__2::__compressed_pair.128"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.128"*, %"class.std::__2::__compressed_pair.128"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.128"* %this1 to %"struct.std::__2::__compressed_pair_elem.61"*
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__222__compressed_pair_elemIPhLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.61"* %0) #7
  ret i8** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__222__compressed_pair_elemIPhLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.61"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.61"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.61"* %this, %"struct.std::__2::__compressed_pair_elem.61"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.61"*, %"struct.std::__2::__compressed_pair_elem.61"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.61", %"struct.std::__2::__compressed_pair_elem.61"* %this1, i32 0, i32 0
  ret i8** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEEixEm(%"class.std::__2::vector.89"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.89"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector.89"* %this, %"class.std::__2::vector.89"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.89"*, %"class.std::__2::vector.89"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.89"* %this1 to %"class.std::__2::__vector_base.90"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.90", %"class.std::__2::__vector_base.90"* %0, i32 0, i32 0
  %1 = load i32*, i32** %__begin_, align 4
  %2 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds i32, i32* %1, i32 %2
  ret i32* %arrayidx
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn }
attributes #3 = { nobuiltin allocsize(0) "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { noinline noreturn nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { noreturn "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { nounwind }
attributes #8 = { builtin allocsize(0) }
attributes #9 = { builtin nounwind }
attributes #10 = { noreturn }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
