; ModuleID = './draco/src/draco/core/bounding_box.cc'
source_filename = "./draco/src/draco/core/bounding_box.cc"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%"class.draco::BoundingBox" = type { %"class.draco::VectorD", %"class.draco::VectorD" }
%"class.draco::VectorD" = type { %"struct.std::__2::array" }
%"struct.std::__2::array" = type { [3 x float] }

$_ZN5draco7VectorDIfLi3EEC2ERKS1_ = comdat any

$_ZNK5draco7VectorDIfLi3EEixEi = comdat any

$_ZN5draco7VectorDIfLi3EEixEi = comdat any

$_ZNKSt3__25arrayIfLm3EEixEm = comdat any

$_ZNSt3__25arrayIfLm3EEixEm = comdat any

@_ZN5draco11BoundingBoxC1ERKNS_7VectorDIfLi3EEES4_ = hidden unnamed_addr alias %"class.draco::BoundingBox"* (%"class.draco::BoundingBox"*, %"class.draco::VectorD"*, %"class.draco::VectorD"*), %"class.draco::BoundingBox"* (%"class.draco::BoundingBox"*, %"class.draco::VectorD"*, %"class.draco::VectorD"*)* @_ZN5draco11BoundingBoxC2ERKNS_7VectorDIfLi3EEES4_

; Function Attrs: noinline nounwind optnone
define hidden %"class.draco::BoundingBox"* @_ZN5draco11BoundingBoxC2ERKNS_7VectorDIfLi3EEES4_(%"class.draco::BoundingBox"* returned %this, %"class.draco::VectorD"* nonnull align 4 dereferenceable(12) %min_point_in, %"class.draco::VectorD"* nonnull align 4 dereferenceable(12) %max_point_in) unnamed_addr #0 {
entry:
  %this.addr = alloca %"class.draco::BoundingBox"*, align 4
  %min_point_in.addr = alloca %"class.draco::VectorD"*, align 4
  %max_point_in.addr = alloca %"class.draco::VectorD"*, align 4
  store %"class.draco::BoundingBox"* %this, %"class.draco::BoundingBox"** %this.addr, align 4
  store %"class.draco::VectorD"* %min_point_in, %"class.draco::VectorD"** %min_point_in.addr, align 4
  store %"class.draco::VectorD"* %max_point_in, %"class.draco::VectorD"** %max_point_in.addr, align 4
  %this1 = load %"class.draco::BoundingBox"*, %"class.draco::BoundingBox"** %this.addr, align 4
  %min_point_ = getelementptr inbounds %"class.draco::BoundingBox", %"class.draco::BoundingBox"* %this1, i32 0, i32 0
  %0 = load %"class.draco::VectorD"*, %"class.draco::VectorD"** %min_point_in.addr, align 4
  %call = call %"class.draco::VectorD"* @_ZN5draco7VectorDIfLi3EEC2ERKS1_(%"class.draco::VectorD"* %min_point_, %"class.draco::VectorD"* nonnull align 4 dereferenceable(12) %0)
  %max_point_ = getelementptr inbounds %"class.draco::BoundingBox", %"class.draco::BoundingBox"* %this1, i32 0, i32 1
  %1 = load %"class.draco::VectorD"*, %"class.draco::VectorD"** %max_point_in.addr, align 4
  %call2 = call %"class.draco::VectorD"* @_ZN5draco7VectorDIfLi3EEC2ERKS1_(%"class.draco::VectorD"* %max_point_, %"class.draco::VectorD"* nonnull align 4 dereferenceable(12) %1)
  ret %"class.draco::BoundingBox"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::VectorD"* @_ZN5draco7VectorDIfLi3EEC2ERKS1_(%"class.draco::VectorD"* returned %this, %"class.draco::VectorD"* nonnull align 4 dereferenceable(12) %o) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"class.draco::VectorD"*, align 4
  %this.addr = alloca %"class.draco::VectorD"*, align 4
  %o.addr = alloca %"class.draco::VectorD"*, align 4
  %i = alloca i32, align 4
  store %"class.draco::VectorD"* %this, %"class.draco::VectorD"** %this.addr, align 4
  store %"class.draco::VectorD"* %o, %"class.draco::VectorD"** %o.addr, align 4
  %this1 = load %"class.draco::VectorD"*, %"class.draco::VectorD"** %this.addr, align 4
  store %"class.draco::VectorD"* %this1, %"class.draco::VectorD"** %retval, align 4
  %v_ = getelementptr inbounds %"class.draco::VectorD", %"class.draco::VectorD"* %this1, i32 0, i32 0
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %1 = load %"class.draco::VectorD"*, %"class.draco::VectorD"** %o.addr, align 4
  %2 = load i32, i32* %i, align 4
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK5draco7VectorDIfLi3EEixEi(%"class.draco::VectorD"* %1, i32 %2)
  %3 = load float, float* %call, align 4
  %4 = load i32, i32* %i, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZN5draco7VectorDIfLi3EEixEi(%"class.draco::VectorD"* %this1, i32 %4)
  store float %3, float* %call2, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %6 = load %"class.draco::VectorD"*, %"class.draco::VectorD"** %retval, align 4
  ret %"class.draco::VectorD"* %6
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK5draco7VectorDIfLi3EEixEi(%"class.draco::VectorD"* %this, i32 %i) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::VectorD"*, align 4
  %i.addr = alloca i32, align 4
  store %"class.draco::VectorD"* %this, %"class.draco::VectorD"** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %"class.draco::VectorD"*, %"class.draco::VectorD"** %this.addr, align 4
  %v_ = getelementptr inbounds %"class.draco::VectorD", %"class.draco::VectorD"* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNKSt3__25arrayIfLm3EEixEm(%"struct.std::__2::array"* %v_, i32 %0) #1
  ret float* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZN5draco7VectorDIfLi3EEixEi(%"class.draco::VectorD"* %this, i32 %i) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::VectorD"*, align 4
  %i.addr = alloca i32, align 4
  store %"class.draco::VectorD"* %this, %"class.draco::VectorD"** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %"class.draco::VectorD"*, %"class.draco::VectorD"** %this.addr, align 4
  %v_ = getelementptr inbounds %"class.draco::VectorD", %"class.draco::VectorD"* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNSt3__25arrayIfLm3EEixEm(%"struct.std::__2::array"* %v_, i32 %0) #1
  ret float* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNKSt3__25arrayIfLm3EEixEm(%"struct.std::__2::array"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::array"*, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::array"* %this, %"struct.std::__2::array"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %this.addr, align 4
  %__elems_ = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %this1, i32 0, i32 0
  %0 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds [3 x float], [3 x float]* %__elems_, i32 0, i32 %0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNSt3__25arrayIfLm3EEixEm(%"struct.std::__2::array"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::array"*, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::array"* %this, %"struct.std::__2::array"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %this.addr, align 4
  %__elems_ = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %this1, i32 0, i32 0
  %0 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds [3 x float], [3 x float]* %__elems_, i32 0, i32 %0
  ret float* %arrayidx
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
