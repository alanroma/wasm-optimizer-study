; ModuleID = './draco/src/draco/compression/decode.cc'
source_filename = "./draco/src/draco/compression/decode.cc"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%"class.draco::StatusOr" = type { %"class.draco::Status", %"class.std::__2::unique_ptr" }
%"class.draco::Status" = type { i32, %"class.std::__2::basic_string" }
%"class.std::__2::basic_string" = type { %"class.std::__2::__compressed_pair" }
%"class.std::__2::__compressed_pair" = type { %"struct.std::__2::__compressed_pair_elem" }
%"struct.std::__2::__compressed_pair_elem" = type { %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep" }
%"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep" = type { %union.anon }
%union.anon = type { %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long" }
%"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long" = type { i8*, i32, i32 }
%"class.std::__2::unique_ptr" = type { %"class.std::__2::__compressed_pair.1" }
%"class.std::__2::__compressed_pair.1" = type { %"struct.std::__2::__compressed_pair_elem.2" }
%"struct.std::__2::__compressed_pair_elem.2" = type { %"class.draco::MeshDecoder"* }
%"class.draco::MeshDecoder" = type { %"class.draco::PointCloudDecoder", %"class.draco::Mesh"* }
%"class.draco::PointCloudDecoder" = type { i32 (...)**, %"class.draco::PointCloud"*, %"class.std::__2::vector.102", %"class.std::__2::vector.95", %"class.draco::DecoderBuffer"*, i8, i8, %"class.draco::DracoOptions"* }
%"class.draco::PointCloud" = type { i32 (...)**, %"class.std::__2::unique_ptr.3", %"class.std::__2::vector.59", [5 x %"class.std::__2::vector.95"], i32 }
%"class.std::__2::unique_ptr.3" = type { %"class.std::__2::__compressed_pair.4" }
%"class.std::__2::__compressed_pair.4" = type { %"struct.std::__2::__compressed_pair_elem.5" }
%"struct.std::__2::__compressed_pair_elem.5" = type { %"class.draco::GeometryMetadata"* }
%"class.draco::GeometryMetadata" = type { %"class.draco::Metadata", %"class.std::__2::vector" }
%"class.draco::Metadata" = type { %"class.std::__2::unordered_map", %"class.std::__2::unordered_map.25" }
%"class.std::__2::unordered_map" = type { %"class.std::__2::__hash_table" }
%"class.std::__2::__hash_table" = type { %"class.std::__2::unique_ptr.6", %"class.std::__2::__compressed_pair.15", %"class.std::__2::__compressed_pair.20", %"class.std::__2::__compressed_pair.22" }
%"class.std::__2::unique_ptr.6" = type { %"class.std::__2::__compressed_pair.7" }
%"class.std::__2::__compressed_pair.7" = type { %"struct.std::__2::__compressed_pair_elem.8", %"struct.std::__2::__compressed_pair_elem.9" }
%"struct.std::__2::__compressed_pair_elem.8" = type { %"struct.std::__2::__hash_node_base"** }
%"struct.std::__2::__hash_node_base" = type { %"struct.std::__2::__hash_node_base"* }
%"struct.std::__2::__compressed_pair_elem.9" = type { %"class.std::__2::__bucket_list_deallocator" }
%"class.std::__2::__bucket_list_deallocator" = type { %"class.std::__2::__compressed_pair.10" }
%"class.std::__2::__compressed_pair.10" = type { %"struct.std::__2::__compressed_pair_elem.11" }
%"struct.std::__2::__compressed_pair_elem.11" = type { i32 }
%"class.std::__2::__compressed_pair.15" = type { %"struct.std::__2::__compressed_pair_elem.16" }
%"struct.std::__2::__compressed_pair_elem.16" = type { %"struct.std::__2::__hash_node_base" }
%"class.std::__2::__compressed_pair.20" = type { %"struct.std::__2::__compressed_pair_elem.11" }
%"class.std::__2::__compressed_pair.22" = type { %"struct.std::__2::__compressed_pair_elem.23" }
%"struct.std::__2::__compressed_pair_elem.23" = type { float }
%"class.std::__2::unordered_map.25" = type { %"class.std::__2::__hash_table.26" }
%"class.std::__2::__hash_table.26" = type { %"class.std::__2::unique_ptr.27", %"class.std::__2::__compressed_pair.37", %"class.std::__2::__compressed_pair.42", %"class.std::__2::__compressed_pair.45" }
%"class.std::__2::unique_ptr.27" = type { %"class.std::__2::__compressed_pair.28" }
%"class.std::__2::__compressed_pair.28" = type { %"struct.std::__2::__compressed_pair_elem.29", %"struct.std::__2::__compressed_pair_elem.31" }
%"struct.std::__2::__compressed_pair_elem.29" = type { %"struct.std::__2::__hash_node_base.30"** }
%"struct.std::__2::__hash_node_base.30" = type { %"struct.std::__2::__hash_node_base.30"* }
%"struct.std::__2::__compressed_pair_elem.31" = type { %"class.std::__2::__bucket_list_deallocator.32" }
%"class.std::__2::__bucket_list_deallocator.32" = type { %"class.std::__2::__compressed_pair.33" }
%"class.std::__2::__compressed_pair.33" = type { %"struct.std::__2::__compressed_pair_elem.11" }
%"class.std::__2::__compressed_pair.37" = type { %"struct.std::__2::__compressed_pair_elem.38" }
%"struct.std::__2::__compressed_pair_elem.38" = type { %"struct.std::__2::__hash_node_base.30" }
%"class.std::__2::__compressed_pair.42" = type { %"struct.std::__2::__compressed_pair_elem.11" }
%"class.std::__2::__compressed_pair.45" = type { %"struct.std::__2::__compressed_pair_elem.23" }
%"class.std::__2::vector" = type { %"class.std::__2::__vector_base" }
%"class.std::__2::__vector_base" = type { %"class.std::__2::unique_ptr.48"*, %"class.std::__2::unique_ptr.48"*, %"class.std::__2::__compressed_pair.52" }
%"class.std::__2::unique_ptr.48" = type { %"class.std::__2::__compressed_pair.49" }
%"class.std::__2::__compressed_pair.49" = type { %"struct.std::__2::__compressed_pair_elem.50" }
%"struct.std::__2::__compressed_pair_elem.50" = type { %"class.draco::AttributeMetadata"* }
%"class.draco::AttributeMetadata" = type { %"class.draco::Metadata", i32 }
%"class.std::__2::__compressed_pair.52" = type { %"struct.std::__2::__compressed_pair_elem.53" }
%"struct.std::__2::__compressed_pair_elem.53" = type { %"class.std::__2::unique_ptr.48"* }
%"class.std::__2::vector.59" = type { %"class.std::__2::__vector_base.60" }
%"class.std::__2::__vector_base.60" = type { %"class.std::__2::unique_ptr.61"*, %"class.std::__2::unique_ptr.61"*, %"class.std::__2::__compressed_pair.90" }
%"class.std::__2::unique_ptr.61" = type { %"class.std::__2::__compressed_pair.62" }
%"class.std::__2::__compressed_pair.62" = type { %"struct.std::__2::__compressed_pair_elem.63" }
%"struct.std::__2::__compressed_pair_elem.63" = type { %"class.draco::PointAttribute"* }
%"class.draco::PointAttribute" = type <{ %"class.draco::GeometryAttribute", %"class.std::__2::unique_ptr.71", %"class.draco::IndexTypeVector", i32, i8, [3 x i8], %"class.std::__2::unique_ptr.83", [4 x i8] }>
%"class.draco::GeometryAttribute" = type { %"class.draco::DataBuffer"*, %"struct.draco::DataBufferDescriptor", i8, i32, i8, i64, i64, i32, i32 }
%"class.draco::DataBuffer" = type { %"class.std::__2::vector.64", %"struct.draco::DataBufferDescriptor" }
%"class.std::__2::vector.64" = type { %"class.std::__2::__vector_base.65" }
%"class.std::__2::__vector_base.65" = type { i8*, i8*, %"class.std::__2::__compressed_pair.66" }
%"class.std::__2::__compressed_pair.66" = type { %"struct.std::__2::__compressed_pair_elem.67" }
%"struct.std::__2::__compressed_pair_elem.67" = type { i8* }
%"struct.draco::DataBufferDescriptor" = type { i64, i64 }
%"class.std::__2::unique_ptr.71" = type { %"class.std::__2::__compressed_pair.72" }
%"class.std::__2::__compressed_pair.72" = type { %"struct.std::__2::__compressed_pair_elem.73" }
%"struct.std::__2::__compressed_pair_elem.73" = type { %"class.draco::DataBuffer"* }
%"class.draco::IndexTypeVector" = type { %"class.std::__2::vector.76" }
%"class.std::__2::vector.76" = type { %"class.std::__2::__vector_base.77" }
%"class.std::__2::__vector_base.77" = type { %"class.draco::IndexType"*, %"class.draco::IndexType"*, %"class.std::__2::__compressed_pair.78" }
%"class.draco::IndexType" = type { i32 }
%"class.std::__2::__compressed_pair.78" = type { %"struct.std::__2::__compressed_pair_elem.79" }
%"struct.std::__2::__compressed_pair_elem.79" = type { %"class.draco::IndexType"* }
%"class.std::__2::unique_ptr.83" = type { %"class.std::__2::__compressed_pair.84" }
%"class.std::__2::__compressed_pair.84" = type { %"struct.std::__2::__compressed_pair_elem.85" }
%"struct.std::__2::__compressed_pair_elem.85" = type { %"class.draco::AttributeTransformData"* }
%"class.draco::AttributeTransformData" = type { i32, %"class.draco::DataBuffer" }
%"class.std::__2::__compressed_pair.90" = type { %"struct.std::__2::__compressed_pair_elem.91" }
%"struct.std::__2::__compressed_pair_elem.91" = type { %"class.std::__2::unique_ptr.61"* }
%"class.std::__2::vector.102" = type { %"class.std::__2::__vector_base.103" }
%"class.std::__2::__vector_base.103" = type { %"class.std::__2::unique_ptr.104"*, %"class.std::__2::unique_ptr.104"*, %"class.std::__2::__compressed_pair.109" }
%"class.std::__2::unique_ptr.104" = type { %"class.std::__2::__compressed_pair.105" }
%"class.std::__2::__compressed_pair.105" = type { %"struct.std::__2::__compressed_pair_elem.106" }
%"struct.std::__2::__compressed_pair_elem.106" = type { %"class.draco::AttributesDecoderInterface"* }
%"class.draco::AttributesDecoderInterface" = type { i32 (...)** }
%"class.std::__2::__compressed_pair.109" = type { %"struct.std::__2::__compressed_pair_elem.110" }
%"struct.std::__2::__compressed_pair_elem.110" = type { %"class.std::__2::unique_ptr.104"* }
%"class.std::__2::vector.95" = type { %"class.std::__2::__vector_base.96" }
%"class.std::__2::__vector_base.96" = type { i32*, i32*, %"class.std::__2::__compressed_pair.97" }
%"class.std::__2::__compressed_pair.97" = type { %"struct.std::__2::__compressed_pair_elem.98" }
%"struct.std::__2::__compressed_pair_elem.98" = type { i32* }
%"class.draco::DecoderBuffer" = type { i8*, i64, i64, %"class.draco::DecoderBuffer::BitDecoder", i8, i16 }
%"class.draco::DecoderBuffer::BitDecoder" = type { i8*, i8*, i32 }
%"class.draco::DracoOptions" = type { %"class.draco::Options", %"class.std::__2::map.121" }
%"class.draco::Options" = type { %"class.std::__2::map" }
%"class.std::__2::map" = type { %"class.std::__2::__tree" }
%"class.std::__2::__tree" = type { %"class.std::__2::__tree_end_node"*, %"class.std::__2::__compressed_pair.114", %"class.std::__2::__compressed_pair.119" }
%"class.std::__2::__tree_end_node" = type { %"class.std::__2::__tree_node_base"* }
%"class.std::__2::__tree_node_base" = type <{ %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_end_node"*, i8, [3 x i8] }>
%"class.std::__2::__compressed_pair.114" = type { %"struct.std::__2::__compressed_pair_elem.115" }
%"struct.std::__2::__compressed_pair_elem.115" = type { %"class.std::__2::__tree_end_node" }
%"class.std::__2::__compressed_pair.119" = type { %"struct.std::__2::__compressed_pair_elem.11" }
%"class.std::__2::map.121" = type { %"class.std::__2::__tree.122" }
%"class.std::__2::__tree.122" = type { %"class.std::__2::__tree_end_node"*, %"class.std::__2::__compressed_pair.123", %"class.std::__2::__compressed_pair.127" }
%"class.std::__2::__compressed_pair.123" = type { %"struct.std::__2::__compressed_pair_elem.115" }
%"class.std::__2::__compressed_pair.127" = type { %"struct.std::__2::__compressed_pair_elem.11" }
%"class.draco::Mesh" = type { %"class.draco::PointCloud", %"class.std::__2::vector.132", %"class.draco::IndexTypeVector.139" }
%"class.std::__2::vector.132" = type { %"class.std::__2::__vector_base.133" }
%"class.std::__2::__vector_base.133" = type { %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"*, %"class.std::__2::__compressed_pair.134" }
%"struct.draco::Mesh::AttributeData" = type { i32 }
%"class.std::__2::__compressed_pair.134" = type { %"struct.std::__2::__compressed_pair_elem.135" }
%"struct.std::__2::__compressed_pair_elem.135" = type { %"struct.draco::Mesh::AttributeData"* }
%"class.draco::IndexTypeVector.139" = type { %"class.std::__2::vector.140" }
%"class.std::__2::vector.140" = type { %"class.std::__2::__vector_base.141" }
%"class.std::__2::__vector_base.141" = type { %"struct.std::__2::array"*, %"struct.std::__2::array"*, %"class.std::__2::__compressed_pair.143" }
%"struct.std::__2::array" = type { [3 x %"class.draco::IndexType.142"] }
%"class.draco::IndexType.142" = type { i32 }
%"class.std::__2::__compressed_pair.143" = type { %"struct.std::__2::__compressed_pair_elem.144" }
%"struct.std::__2::__compressed_pair_elem.144" = type { %"struct.std::__2::array"* }
%"class.draco::MeshSequentialDecoder" = type { %"class.draco::MeshDecoder" }
%"class.draco::MeshEdgebreakerDecoder" = type { %"class.draco::MeshDecoder", %"class.std::__2::unique_ptr.150" }
%"class.std::__2::unique_ptr.150" = type { %"class.std::__2::__compressed_pair.151" }
%"class.std::__2::__compressed_pair.151" = type { %"struct.std::__2::__compressed_pair_elem.152" }
%"struct.std::__2::__compressed_pair_elem.152" = type { %"class.draco::MeshEdgebreakerDecoderImplInterface"* }
%"class.draco::MeshEdgebreakerDecoderImplInterface" = type { i32 (...)** }
%"struct.std::__2::__default_init_tag" = type { i8 }
%"class.std::__2::__basic_string_common" = type { i8 }
%"class.draco::StatusOr.155" = type { %"class.draco::Status", i32 }
%"struct.draco::DracoHeader" = type { [5 x i8], i8, i8, i8, i8, i16 }
%"class.draco::StatusOr.156" = type { %"class.draco::Status", %"class.std::__2::unique_ptr.157" }
%"class.std::__2::unique_ptr.157" = type { %"class.std::__2::__compressed_pair.158" }
%"class.std::__2::__compressed_pair.158" = type { %"struct.std::__2::__compressed_pair_elem.159" }
%"struct.std::__2::__compressed_pair_elem.159" = type { %"class.draco::PointCloud"* }
%"class.draco::Decoder" = type { %"class.draco::DracoOptions" }
%"class.std::__2::unique_ptr.162" = type { %"class.std::__2::__compressed_pair.163" }
%"class.std::__2::__compressed_pair.163" = type { %"struct.std::__2::__compressed_pair_elem.164" }
%"struct.std::__2::__compressed_pair_elem.164" = type { %"class.draco::Mesh"* }
%"struct.std::__2::default_delete.166" = type { i8 }
%"class.draco::StatusOr.167" = type { %"class.draco::Status", %"class.std::__2::unique_ptr.162" }
%"struct.std::__2::default_delete.149" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.0" = type { i8 }
%"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__raw" = type { [3 x i32] }
%"class.std::__2::allocator" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.148" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.160" = type { i8 }
%"struct.std::__2::default_delete.161" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.165" = type { i8 }
%"class.std::__2::__map_iterator" = type { %"class.std::__2::__tree_iterator" }
%"class.std::__2::__tree_iterator" = type { %"class.std::__2::__tree_end_node"* }
%"struct.std::__2::pair.168" = type <{ %"class.std::__2::__map_iterator", i8, [3 x i8] }>
%"struct.std::__2::pair.169" = type { i32, %"class.draco::Options" }
%"struct.std::__2::pair" = type { i32, %"class.draco::Options" }
%"struct.std::__2::__value_type" = type { %"struct.std::__2::pair" }
%"struct.std::__2::pair.170" = type <{ %"class.std::__2::__tree_iterator", i8, [3 x i8] }>
%"class.std::__2::__tree_node" = type { %"class.std::__2::__tree_node_base.base", %"struct.std::__2::__value_type" }
%"class.std::__2::__tree_node_base.base" = type <{ %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_end_node"*, i8 }>
%"class.std::__2::__map_value_compare.129" = type { i8 }
%"struct.std::__2::less.130" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.128" = type { i8 }
%"struct.std::__2::__extract_key_first_tag" = type { i8 }
%"struct.std::__2::__can_extract_key" = type { i8 }
%"class.std::__2::unique_ptr.172" = type { %"class.std::__2::__compressed_pair.173" }
%"class.std::__2::__compressed_pair.173" = type { %"struct.std::__2::__compressed_pair_elem.174", %"struct.std::__2::__compressed_pair_elem.175" }
%"struct.std::__2::__compressed_pair_elem.174" = type { %"class.std::__2::__tree_node"* }
%"struct.std::__2::__compressed_pair_elem.175" = type { %"class.std::__2::__tree_node_destructor" }
%"class.std::__2::__tree_node_destructor" = type <{ %"class.std::__2::allocator.125"*, i8, [3 x i8] }>
%"class.std::__2::allocator.125" = type { i8 }
%"struct.std::__2::integral_constant" = type { i8 }
%"struct.std::__2::__has_construct" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.124" = type { i8 }
%"struct.std::__2::integral_constant.176" = type { i8 }
%"struct.std::__2::__has_destroy" = type { i8 }
%"class.std::__2::__map_const_iterator" = type { %"class.std::__2::__tree_const_iterator" }
%"class.std::__2::__tree_const_iterator" = type { %"class.std::__2::__tree_end_node"* }
%"class.std::__2::allocator.117" = type { i8 }
%"class.std::__2::__map_value_compare" = type { i8 }
%"class.std::__2::__map_iterator.178" = type { %"class.std::__2::__tree_iterator.179" }
%"class.std::__2::__tree_iterator.179" = type { %"class.std::__2::__tree_end_node"* }
%"struct.std::__2::pair.177" = type { %"class.std::__2::basic_string", %"class.std::__2::basic_string" }
%"struct.std::__2::__has_select_on_container_copy_construction" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.116" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.120" = type { i8 }
%"struct.std::__2::__value_type.181" = type { %"struct.std::__2::pair.177" }
%"class.std::__2::__tree_node.180" = type { %"class.std::__2::__tree_node_base.base", %"struct.std::__2::__value_type.181" }
%"class.std::__2::unique_ptr.182" = type { %"class.std::__2::__compressed_pair.183" }
%"class.std::__2::__compressed_pair.183" = type { %"struct.std::__2::__compressed_pair_elem.184", %"struct.std::__2::__compressed_pair_elem.185" }
%"struct.std::__2::__compressed_pair_elem.184" = type { %"class.std::__2::__tree_node.180"* }
%"struct.std::__2::__compressed_pair_elem.185" = type { %"class.std::__2::__tree_node_destructor.186" }
%"class.std::__2::__tree_node_destructor.186" = type <{ %"class.std::__2::allocator.117"*, i8, [3 x i8] }>
%"struct.std::__2::less" = type { i8 }
%"class.std::__2::basic_string_view" = type { i8*, i32 }
%"struct.std::__2::__less" = type { i8 }
%"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short" = type { [11 x i8], %struct.anon }
%struct.anon = type { i8 }
%"struct.std::__2::bidirectional_iterator_tag" = type { i8 }
%"struct.std::__2::__has_construct.188" = type { i8 }
%"struct.std::__2::__has_destroy.189" = type { i8 }

$_ZNSt3__210unique_ptrIN5draco11MeshDecoderENS_14default_deleteIS2_EEEC2ILb1EvEEPS2_ = comdat any

$_ZN5draco8StatusOrINSt3__210unique_ptrINS_11MeshDecoderENS1_14default_deleteIS3_EEEEEC2EOS6_ = comdat any

$_ZNSt3__210unique_ptrIN5draco11MeshDecoderENS_14default_deleteIS2_EEED2Ev = comdat any

$_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2IDnEEPKc = comdat any

$_ZN5draco6StatusC2ENS0_4CodeERKNSt3__212basic_stringIcNS2_11char_traitsIcEENS2_9allocatorIcEEEE = comdat any

$_ZN5draco8StatusOrINSt3__210unique_ptrINS_11MeshDecoderENS1_14default_deleteIS3_EEEEEC2ERKNS_6StatusE = comdat any

$_ZN5draco6StatusD2Ev = comdat any

$_ZNK5draco6Status2okEv = comdat any

$_ZN5draco8StatusOrINS_19EncodedGeometryTypeEEC2ERKNS_6StatusE = comdat any

$_ZN5draco8StatusOrINS_19EncodedGeometryTypeEEC2EOS1_ = comdat any

$_ZN5draco13DecoderBufferD2Ev = comdat any

$_ZNK5draco8StatusOrINS_19EncodedGeometryTypeEE2okEv = comdat any

$_ZNSt3__24moveIRKN5draco6StatusEEEONS_16remove_referenceIT_E4typeEOS6_ = comdat any

$_ZNK5draco8StatusOrINS_19EncodedGeometryTypeEE6statusEv = comdat any

$_ZN5draco6StatusC2ERKS0_ = comdat any

$_ZN5draco8StatusOrINSt3__210unique_ptrINS_10PointCloudENS1_14default_deleteIS3_EEEEEC2ERKNS_6StatusE = comdat any

$_ZNSt3__24moveIRN5draco8StatusOrINS1_19EncodedGeometryTypeEEEEEONS_16remove_referenceIT_E4typeEOS7_ = comdat any

$_ZNO5draco8StatusOrINS_19EncodedGeometryTypeEE5valueEv = comdat any

$_ZNSt3__210unique_ptrIN5draco4MeshENS_14default_deleteIS2_EEEC2ILb1EvEEPS2_ = comdat any

$_ZNKSt3__210unique_ptrIN5draco4MeshENS_14default_deleteIS2_EEE3getEv = comdat any

$_ZNSt3__24moveIRNS_10unique_ptrIN5draco4MeshENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOS9_ = comdat any

$_ZNSt3__210unique_ptrIN5draco10PointCloudENS_14default_deleteIS2_EEEC2INS1_4MeshENS3_IS7_EEvvEEONS0_IT_T0_EE = comdat any

$_ZN5draco8StatusOrINSt3__210unique_ptrINS_10PointCloudENS1_14default_deleteIS3_EEEEEC2EOS6_ = comdat any

$_ZNSt3__210unique_ptrIN5draco10PointCloudENS_14default_deleteIS2_EEED2Ev = comdat any

$_ZNSt3__210unique_ptrIN5draco4MeshENS_14default_deleteIS2_EEED2Ev = comdat any

$_ZN5draco8StatusOrINS_19EncodedGeometryTypeEED2Ev = comdat any

$_ZN5draco8StatusOrINSt3__210unique_ptrINS_4MeshENS1_14default_deleteIS3_EEEEEC2ERKNS_6StatusE = comdat any

$_ZN5draco8StatusOrINSt3__210unique_ptrINS_4MeshENS1_14default_deleteIS3_EEEEEC2EOS6_ = comdat any

$_ZNK5draco8StatusOrINSt3__210unique_ptrINS_11MeshDecoderENS1_14default_deleteIS3_EEEEE2okEv = comdat any

$_ZNK5draco8StatusOrINSt3__210unique_ptrINS_11MeshDecoderENS1_14default_deleteIS3_EEEEE6statusEv = comdat any

$_ZNSt3__24moveIRN5draco8StatusOrINS_10unique_ptrINS1_11MeshDecoderENS_14default_deleteIS4_EEEEEEEEONS_16remove_referenceIT_E4typeEOSB_ = comdat any

$_ZNO5draco8StatusOrINSt3__210unique_ptrINS_11MeshDecoderENS1_14default_deleteIS3_EEEEE5valueEv = comdat any

$_ZNSt3__210unique_ptrIN5draco11MeshDecoderENS_14default_deleteIS2_EEEC2EOS5_ = comdat any

$_ZNKSt3__210unique_ptrIN5draco11MeshDecoderENS_14default_deleteIS2_EEEptEv = comdat any

$_ZN5draco8OkStatusEv = comdat any

$_ZN5draco8StatusOrINSt3__210unique_ptrINS_11MeshDecoderENS1_14default_deleteIS3_EEEEED2Ev = comdat any

$_ZN5draco12DracoOptionsINS_17GeometryAttribute4TypeEE16SetAttributeBoolERKS2_RKNSt3__212basic_stringIcNS6_11char_traitsIcEENS6_9allocatorIcEEEEb = comdat any

$_ZN5draco6StatusC2ENS0_4CodeE = comdat any

$_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2Ev = comdat any

$_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_EC2INS_18__default_init_tagESA_EEOT_OT0_ = comdat any

$_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__zeroEv = comdat any

$_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIcEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__29allocatorIcEC2Ev = comdat any

$_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco11MeshDecoderENS_14default_deleteIS2_EEEC2IRS3_NS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__27forwardIRPN5draco11MeshDecoderEEEOT_RNS_16remove_referenceIS5_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco11MeshDecoderELi0ELb0EEC2IRS3_vEEOT_ = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco11MeshDecoderEEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__210unique_ptrIN5draco11MeshDecoderENS_14default_deleteIS2_EEE5resetEPS2_ = comdat any

$_ZNSt3__217__compressed_pairIPN5draco11MeshDecoderENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco11MeshDecoderENS_14default_deleteIS2_EEE6secondEv = comdat any

$_ZNKSt3__214default_deleteIN5draco11MeshDecoderEEclEPS2_ = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco11MeshDecoderELi0ELb0EE5__getEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco11MeshDecoderEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__24moveIRNS_10unique_ptrIN5draco11MeshDecoderENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOS9_ = comdat any

$_ZNSt3__210unique_ptrIN5draco11MeshDecoderENS_14default_deleteIS2_EEE7releaseEv = comdat any

$_ZNSt3__27forwardINS_14default_deleteIN5draco11MeshDecoderEEEEEOT_RNS_16remove_referenceIS5_E4typeE = comdat any

$_ZNSt3__210unique_ptrIN5draco11MeshDecoderENS_14default_deleteIS2_EEE11get_deleterEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco11MeshDecoderENS_14default_deleteIS2_EEEC2IS3_S5_EEOT_OT0_ = comdat any

$_ZNSt3__27forwardIPN5draco11MeshDecoderEEEOT_RNS_16remove_referenceIS4_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco11MeshDecoderELi0ELb0EEC2IS3_vEEOT_ = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco11MeshDecoderEEELi1ELb1EEC2IS4_vEEOT_ = comdat any

$_ZNSt3__211char_traitsIcE6lengthEPKc = comdat any

$_ZNSt3__210unique_ptrIN5draco11MeshDecoderENS_14default_deleteIS2_EEEC2ILb1EvEEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco11MeshDecoderENS_14default_deleteIS2_EEEC2IS3_NS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__24moveIRN5draco19EncodedGeometryTypeEEEONS_16remove_referenceIT_E4typeEOS5_ = comdat any

$_ZNSt3__210unique_ptrIN5draco10PointCloudENS_14default_deleteIS2_EEEC2ILb1EvEEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco10PointCloudENS_14default_deleteIS2_EEEC2IS3_NS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__27forwardIPN5draco10PointCloudEEEOT_RNS_16remove_referenceIS4_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco10PointCloudELi0ELb0EEC2IS3_vEEOT_ = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco10PointCloudEEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__210unique_ptrIN5draco10PointCloudENS_14default_deleteIS2_EEE5resetEPS2_ = comdat any

$_ZNSt3__217__compressed_pairIPN5draco10PointCloudENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco10PointCloudENS_14default_deleteIS2_EEE6secondEv = comdat any

$_ZNKSt3__214default_deleteIN5draco10PointCloudEEclEPS2_ = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco10PointCloudELi0ELb0EE5__getEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco10PointCloudEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco4MeshENS_14default_deleteIS2_EEEC2IRS3_NS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__27forwardIRPN5draco4MeshEEEOT_RNS_16remove_referenceIS5_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco4MeshELi0ELb0EEC2IRS3_vEEOT_ = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco4MeshEEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__210unique_ptrIN5draco4MeshENS_14default_deleteIS2_EEE5resetEPS2_ = comdat any

$_ZNSt3__217__compressed_pairIPN5draco4MeshENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco4MeshENS_14default_deleteIS2_EEE6secondEv = comdat any

$_ZNKSt3__214default_deleteIN5draco4MeshEEclEPS2_ = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco4MeshELi0ELb0EE5__getEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco4MeshEEELi1ELb1EE5__getEv = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco4MeshENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPN5draco4MeshELi0ELb0EE5__getEv = comdat any

$_ZNSt3__210unique_ptrIN5draco4MeshENS_14default_deleteIS2_EEE7releaseEv = comdat any

$_ZNSt3__27forwardINS_14default_deleteIN5draco4MeshEEEEEOT_RNS_16remove_referenceIS5_E4typeE = comdat any

$_ZNSt3__210unique_ptrIN5draco4MeshENS_14default_deleteIS2_EEE11get_deleterEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco10PointCloudENS_14default_deleteIS2_EEEC2IPNS1_4MeshENS4_IS8_EEEEOT_OT0_ = comdat any

$_ZNSt3__27forwardIPN5draco4MeshEEEOT_RNS_16remove_referenceIS4_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco10PointCloudELi0ELb0EEC2IPNS1_4MeshEvEEOT_ = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco10PointCloudEEELi1ELb1EEC2INS1_INS2_4MeshEEEvEEOT_ = comdat any

$_ZNSt3__214default_deleteIN5draco10PointCloudEEC2INS1_4MeshEEERKNS0_IT_EEPNS_9enable_ifIXsr14is_convertibleIPS6_PS2_EE5valueEvE4typeE = comdat any

$_ZNSt3__24moveIRNS_10unique_ptrIN5draco10PointCloudENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOS9_ = comdat any

$_ZNSt3__210unique_ptrIN5draco10PointCloudENS_14default_deleteIS2_EEEC2EOS5_ = comdat any

$_ZNSt3__210unique_ptrIN5draco10PointCloudENS_14default_deleteIS2_EEE7releaseEv = comdat any

$_ZNSt3__27forwardINS_14default_deleteIN5draco10PointCloudEEEEEOT_RNS_16remove_referenceIS5_E4typeE = comdat any

$_ZNSt3__210unique_ptrIN5draco10PointCloudENS_14default_deleteIS2_EEE11get_deleterEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco10PointCloudENS_14default_deleteIS2_EEEC2IS3_S5_EEOT_OT0_ = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco10PointCloudEEELi1ELb1EEC2IS4_vEEOT_ = comdat any

$_ZNSt3__210unique_ptrIN5draco4MeshENS_14default_deleteIS2_EEEC2ILb1EvEEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco4MeshENS_14default_deleteIS2_EEEC2IS3_NS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco4MeshELi0ELb0EEC2IS3_vEEOT_ = comdat any

$_ZNSt3__210unique_ptrIN5draco4MeshENS_14default_deleteIS2_EEEC2EOS5_ = comdat any

$_ZNSt3__217__compressed_pairIPN5draco4MeshENS_14default_deleteIS2_EEEC2IS3_S5_EEOT_OT0_ = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco4MeshEEELi1ELb1EEC2IS4_vEEOT_ = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco11MeshDecoderENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPN5draco11MeshDecoderELi0ELb0EE5__getEv = comdat any

$_ZN5draco12DracoOptionsINS_17GeometryAttribute4TypeEE19GetAttributeOptionsERKS2_ = comdat any

$_ZNSt3__23mapIN5draco17GeometryAttribute4TypeENS1_7OptionsENS_4lessIS3_EENS_9allocatorINS_4pairIKS3_S4_EEEEE4findERS9_ = comdat any

$_ZNSt3__2neERKNS_14__map_iteratorINS_15__tree_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPNS_11__tree_nodeIS7_PvEElEEEESF_ = comdat any

$_ZNSt3__23mapIN5draco17GeometryAttribute4TypeENS1_7OptionsENS_4lessIS3_EENS_9allocatorINS_4pairIKS3_S4_EEEEE3endEv = comdat any

$_ZNKSt3__214__map_iteratorINS_15__tree_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPNS_11__tree_nodeIS7_PvEElEEEptEv = comdat any

$_ZNSt3__23mapIN5draco17GeometryAttribute4TypeENS1_7OptionsENS_4lessIS3_EENS_9allocatorINS_4pairIKS3_S4_EEEEE6insertINS8_IS3_S4_EEvEENS8_INS_14__map_iteratorINS_15__tree_iteratorINS_12__value_typeIS3_S4_EEPNS_11__tree_nodeISI_PvEElEEEEbEEOT_ = comdat any

$_ZNSt3__29make_pairIRKN5draco17GeometryAttribute4TypeERNS1_7OptionsEEENS_4pairINS_18__unwrap_ref_decayIT_E4typeENS9_IT0_E4typeEEEOSA_OSD_ = comdat any

$_ZNSt3__24pairIN5draco17GeometryAttribute4TypeENS1_7OptionsEED2Ev = comdat any

$_ZN5draco7OptionsD2Ev = comdat any

$_ZNSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE4findIS4_EENS_15__tree_iteratorIS6_PNS_11__tree_nodeIS6_PvEElEERKT_ = comdat any

$_ZNSt3__214__map_iteratorINS_15__tree_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPNS_11__tree_nodeIS7_PvEElEEEC2ESC_ = comdat any

$_ZNSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE13__lower_boundIS4_EENS_15__tree_iteratorIS6_PNS_11__tree_nodeIS6_PvEElEERKT_SJ_PNS_15__tree_end_nodeIPNS_16__tree_node_baseISH_EEEE = comdat any

$_ZNKSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE6__rootEv = comdat any

$_ZNSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE10__end_nodeEv = comdat any

$_ZNSt3__2neERKNS_15__tree_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEEPNS_11__tree_nodeIS6_PvEElEESD_ = comdat any

$_ZNSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE3endEv = comdat any

$_ZNSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE10value_compEv = comdat any

$_ZNKSt3__219__map_value_compareIN5draco17GeometryAttribute4TypeENS_12__value_typeIS3_NS1_7OptionsEEENS_4lessIS3_EELb1EEclERKS3_RKS6_ = comdat any

$_ZNKSt3__215__tree_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEEPNS_11__tree_nodeIS6_PvEElEdeEv = comdat any

$_ZNKSt3__219__map_value_compareIN5draco17GeometryAttribute4TypeENS_12__value_typeIS3_NS1_7OptionsEEENS_4lessIS3_EELb1EEclERKS6_RKS3_ = comdat any

$_ZNSt3__215__tree_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEEPNS_11__tree_nodeIS6_PvEElEC2EPNS_15__tree_end_nodeIPNS_16__tree_node_baseIS8_EEEE = comdat any

$_ZNKSt3__24lessIN5draco17GeometryAttribute4TypeEEclERKS3_S6_ = comdat any

$_ZNKSt3__212__value_typeIN5draco17GeometryAttribute4TypeENS1_7OptionsEE11__get_valueEv = comdat any

$_ZNKSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE10__end_nodeEv = comdat any

$_ZNSt3__214pointer_traitsIPNS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEEE10pointer_toERS6_ = comdat any

$_ZNKSt3__217__compressed_pairINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEENS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENSA_7OptionsEEES3_EEEEE5firstEv = comdat any

$_ZNSt3__29addressofINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEEEEPT_RS7_ = comdat any

$_ZNKSt3__222__compressed_pair_elemINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEENS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENSA_7OptionsEEES3_EEEEE5firstEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__2eqERKNS_15__tree_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEEPNS_11__tree_nodeIS6_PvEElEESD_ = comdat any

$_ZNSt3__217__compressed_pairImNS_19__map_value_compareIN5draco17GeometryAttribute4TypeENS_12__value_typeIS4_NS2_7OptionsEEENS_4lessIS4_EELb1EEEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_19__map_value_compareIN5draco17GeometryAttribute4TypeENS_12__value_typeIS4_NS2_7OptionsEEENS_4lessIS4_EELb1EEELi1ELb1EE5__getEv = comdat any

$_ZNKSt3__215__tree_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEEPNS_11__tree_nodeIS6_PvEElE8__get_npEv = comdat any

$_ZNSt3__214pointer_traitsIPNS_4pairIKN5draco17GeometryAttribute4TypeENS2_7OptionsEEEE10pointer_toERS7_ = comdat any

$_ZNKSt3__215__tree_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEEPNS_11__tree_nodeIS6_PvEElEptEv = comdat any

$_ZNSt3__212__value_typeIN5draco17GeometryAttribute4TypeENS1_7OptionsEE11__get_valueEv = comdat any

$_ZNSt3__29addressofINS_4pairIKN5draco17GeometryAttribute4TypeENS2_7OptionsEEEEEPT_RS8_ = comdat any

$_ZNSt3__214pointer_traitsIPNS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEEE10pointer_toERS6_ = comdat any

$_ZNSt3__29addressofINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEEEEPT_RS7_ = comdat any

$_ZNSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE15__insert_uniqueINS_4pairIS4_S5_EEvEENSF_INS_15__tree_iteratorIS6_PNS_11__tree_nodeIS6_PvEElEEbEEOT_ = comdat any

$_ZNSt3__27forwardINS_4pairIN5draco17GeometryAttribute4TypeENS2_7OptionsEEEEEOT_RNS_16remove_referenceIS7_E4typeE = comdat any

$_ZNSt3__24pairINS_14__map_iteratorINS_15__tree_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS4_7OptionsEEEPNS_11__tree_nodeIS8_PvEElEEEEbEC2ISD_bLb0EEEONS0_IT_T0_EE = comdat any

$_ZNSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE16__emplace_uniqueINS_4pairIS4_S5_EEEENSF_INS_15__tree_iteratorIS6_PNS_11__tree_nodeIS6_PvEElEEbEEOT_ = comdat any

$_ZNSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE28__emplace_unique_extract_keyINS_4pairIS4_S5_EEEENSF_INS_15__tree_iteratorIS6_PNS_11__tree_nodeIS6_PvEElEEbEEOT_NS_23__extract_key_first_tagE = comdat any

$_ZNSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE25__emplace_unique_key_argsIS4_JNS_4pairIS4_S5_EEEEENSF_INS_15__tree_iteratorIS6_PNS_11__tree_nodeIS6_PvEElEEbEERKT_DpOT0_ = comdat any

$_ZNSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE12__find_equalIS4_EERPNS_16__tree_node_baseIPvEERPNS_15__tree_end_nodeISI_EERKT_ = comdat any

$_ZNSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE16__construct_nodeIJNS_4pairIS4_S5_EEEEENS_10unique_ptrINS_11__tree_nodeIS6_PvEENS_22__tree_node_destructorINSB_ISK_EEEEEEDpOT_ = comdat any

$_ZNSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE16__insert_node_atEPNS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEERSI_SI_ = comdat any

$_ZNKSt3__210unique_ptrINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPvEENS_22__tree_node_destructorINS_9allocatorIS9_EEEEE3getEv = comdat any

$_ZNSt3__210unique_ptrINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPvEENS_22__tree_node_destructorINS_9allocatorIS9_EEEEE7releaseEv = comdat any

$_ZNSt3__210unique_ptrINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPvEENS_22__tree_node_destructorINS_9allocatorIS9_EEEEED2Ev = comdat any

$_ZNSt3__215__tree_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEEPNS_11__tree_nodeIS6_PvEElEC2ESA_ = comdat any

$_ZNSt3__24pairINS_15__tree_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPNS_11__tree_nodeIS7_PvEElEEbEC2ISC_RbLb0EEEOT_OT0_ = comdat any

$_ZNKSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE10__root_ptrEv = comdat any

$_ZNSt3__29addressofIPNS_16__tree_node_baseIPvEEEEPT_RS5_ = comdat any

$_ZNSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE12__node_allocEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS4_7OptionsEEEPvEEEEE8allocateERSB_m = comdat any

$_ZNSt3__222__tree_node_destructorINS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS4_7OptionsEEEPvEEEEEC2ERSB_b = comdat any

$_ZNSt3__210unique_ptrINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPvEENS_22__tree_node_destructorINS_9allocatorIS9_EEEEEC2ILb1EvEEPS9_NS_16__dependent_typeINS_27__unique_ptr_deleter_sfinaeISD_EEXT_EE20__good_rval_ref_typeE = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS4_7OptionsEEEPvEEEEE9constructINS_4pairIKS6_S7_EEJNSE_IS6_S7_EEEEEvRSB_PT_DpOT0_ = comdat any

$_ZNSt3__222__tree_key_value_typesINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEEE9__get_ptrERS6_ = comdat any

$_ZNKSt3__210unique_ptrINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPvEENS_22__tree_node_destructorINS_9allocatorIS9_EEEEEptEv = comdat any

$_ZNSt3__210unique_ptrINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPvEENS_22__tree_node_destructorINS_9allocatorIS9_EEEEE11get_deleterEv = comdat any

$_ZNSt3__217__compressed_pairINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEENS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENSA_7OptionsEEES3_EEEEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS4_7OptionsEEEPvEEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__29allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPvEEE8allocateEmPKv = comdat any

$_ZNKSt3__29allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPvEEE8max_sizeEv = comdat any

$_ZNSt3__220__throw_length_errorEPKc = comdat any

$_ZNSt3__217__libcpp_allocateEmm = comdat any

$_ZNSt3__24moveIRNS_22__tree_node_destructorINS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS5_7OptionsEEEPvEEEEEEEEONS_16remove_referenceIT_E4typeEOSG_ = comdat any

$_ZNSt3__217__compressed_pairIPNS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPvEENS_22__tree_node_destructorINS_9allocatorIS9_EEEEEC2IRSA_SE_EEOT_OT0_ = comdat any

$_ZNSt3__27forwardIRPNS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPvEEEEOT_RNS_16remove_referenceISC_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPNS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPvEELi0ELb0EEC2IRSA_vEEOT_ = comdat any

$_ZNSt3__27forwardINS_22__tree_node_destructorINS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS5_7OptionsEEEPvEEEEEEEEOT_RNS_16remove_referenceISE_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemINS_22__tree_node_destructorINS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS5_7OptionsEEEPvEEEEEELi1ELb0EEC2ISD_vEEOT_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS4_7OptionsEEEPvEEEEE11__constructINS_4pairIKS6_S7_EEJNSE_IS6_S7_EEEEEvNS_17integral_constantIbLb1EEERSB_PT_DpOT0_ = comdat any

$_ZNSt3__29allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPvEEE9constructINS_4pairIKS5_S6_EEJNSC_IS5_S6_EEEEEvPT_DpOT0_ = comdat any

$_ZNSt3__24pairIKN5draco17GeometryAttribute4TypeENS1_7OptionsEEC2IS3_S5_Lb0EEEONS0_IT_T0_EE = comdat any

$_ZNSt3__27forwardIN5draco17GeometryAttribute4TypeEEEOT_RNS_16remove_referenceIS4_E4typeE = comdat any

$_ZNSt3__27forwardIN5draco7OptionsEEEOT_RNS_16remove_referenceIS3_E4typeE = comdat any

$_ZN5draco7OptionsC2EOS0_ = comdat any

$_ZNSt3__23mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_NS_4lessIS6_EENS4_INS_4pairIKS6_S6_EEEEEC2EOSD_ = comdat any

$_ZNSt3__24moveIRNS_6__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EENS_19__map_value_compareIS8_S9_NS_4lessIS8_EELb1EEENS6_IS9_EEEEEEONS_16remove_referenceIT_E4typeEOSI_ = comdat any

$_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEEC2EOSE_ = comdat any

$_ZNSt3__24moveIRPNS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEEEEONS_16remove_referenceIT_E4typeEOSA_ = comdat any

$_ZNSt3__24moveIRNS_17__compressed_pairINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEENS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS8_IcEEEESF_EES4_EEEEEEEEONS_16remove_referenceIT_E4typeEOSM_ = comdat any

$_ZNSt3__24moveIRNS_17__compressed_pairImNS_19__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS8_S8_EENS_4lessIS8_EELb1EEEEEEEONS_16remove_referenceIT_E4typeEOSH_ = comdat any

$_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE4sizeEv = comdat any

$_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE10__end_nodeEv = comdat any

$_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE12__begin_nodeEv = comdat any

$_ZNSt3__217__compressed_pairImNS_19__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS7_S7_EENS_4lessIS7_EELb1EEEE5firstEv = comdat any

$_ZNSt3__222__compressed_pair_elemImLi0ELb0EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEENS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS7_IcEEEESE_EES3_EEEEE5firstEv = comdat any

$_ZNKSt3__217__compressed_pairIPNS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPvEENS_22__tree_node_destructorINS_9allocatorIS9_EEEEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPNS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPvEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPNS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPvEENS_22__tree_node_destructorINS_9allocatorIS9_EEEEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_22__tree_node_destructorINS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS5_7OptionsEEEPvEEEEEELi1ELb0EE5__getEv = comdat any

$_ZNSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE12__begin_nodeEv = comdat any

$_ZNSt3__227__tree_balance_after_insertIPNS_16__tree_node_baseIPvEEEEvT_S5_ = comdat any

$_ZNSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE4sizeEv = comdat any

$_ZNKSt3__216__tree_node_baseIPvE15__parent_unsafeEv = comdat any

$_ZNSt3__220__tree_is_left_childIPNS_16__tree_node_baseIPvEEEEbT_ = comdat any

$_ZNSt3__218__tree_left_rotateIPNS_16__tree_node_baseIPvEEEEvT_ = comdat any

$_ZNSt3__219__tree_right_rotateIPNS_16__tree_node_baseIPvEEEEvT_ = comdat any

$_ZNSt3__216__tree_node_baseIPvE12__set_parentEPS2_ = comdat any

$_ZNSt3__217__compressed_pairImNS_19__map_value_compareIN5draco17GeometryAttribute4TypeENS_12__value_typeIS4_NS2_7OptionsEEENS_4lessIS4_EELb1EEEE5firstEv = comdat any

$_ZNSt3__217__compressed_pairIPNS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPvEENS_22__tree_node_destructorINS_9allocatorIS9_EEEEE5firstEv = comdat any

$_ZNSt3__222__compressed_pair_elemIPNS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPvEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__210unique_ptrINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPvEENS_22__tree_node_destructorINS_9allocatorIS9_EEEEE5resetEPS9_ = comdat any

$_ZNSt3__222__tree_node_destructorINS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS4_7OptionsEEEPvEEEEEclEPSA_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS4_7OptionsEEEPvEEEEE7destroyINS_4pairIKS6_S7_EEEEvRSB_PT_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS4_7OptionsEEEPvEEEEE10deallocateERSB_PSA_m = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS4_7OptionsEEEPvEEEEE9__destroyINS_4pairIKS6_S7_EEEEvNS_17integral_constantIbLb0EEERSB_PT_ = comdat any

$_ZNSt3__24pairIKN5draco17GeometryAttribute4TypeENS1_7OptionsEED2Ev = comdat any

$_ZNSt3__29allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPvEEE10deallocateEPS9_m = comdat any

$_ZNSt3__219__libcpp_deallocateEPvmm = comdat any

$_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm = comdat any

$_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm = comdat any

$_ZNSt3__217_DeallocateCaller9__do_callEPv = comdat any

$_ZNSt3__27forwardINS_15__tree_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPNS_11__tree_nodeIS7_PvEElEEEEOT_RNS_16remove_referenceISD_E4typeE = comdat any

$_ZNSt3__27forwardIRbEEOT_RNS_16remove_referenceIS2_E4typeE = comdat any

$_ZNSt3__27forwardIbEEOT_RNS_16remove_referenceIS1_E4typeE = comdat any

$_ZNSt3__27forwardIRKN5draco17GeometryAttribute4TypeEEEOT_RNS_16remove_referenceIS6_E4typeE = comdat any

$_ZNSt3__27forwardIRN5draco7OptionsEEEOT_RNS_16remove_referenceIS4_E4typeE = comdat any

$_ZNSt3__24pairIN5draco17GeometryAttribute4TypeENS1_7OptionsEEC2IRKS3_RS4_Lb0EEEOT_OT0_ = comdat any

$_ZN5draco7OptionsC2ERKS0_ = comdat any

$_ZNSt3__23mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_NS_4lessIS6_EENS4_INS_4pairIKS6_S6_EEEEEC2ERKSD_ = comdat any

$_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEEC2ERKSE_ = comdat any

$_ZNSt3__23mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_NS_4lessIS6_EENS4_INS_4pairIKS6_S6_EEEEE6insertINS_20__map_const_iteratorINS_21__tree_const_iteratorINS_12__value_typeIS6_S6_EEPNS_11__tree_nodeISI_PvEElEEEEEEvT_SP_ = comdat any

$_ZNKSt3__23mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_NS_4lessIS6_EENS4_INS_4pairIKS6_S6_EEEEE5beginEv = comdat any

$_ZNKSt3__23mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_NS_4lessIS6_EENS4_INS_4pairIKS6_S6_EEEEE3endEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEEE37select_on_container_copy_constructionERKSC_ = comdat any

$_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE12__node_allocEv = comdat any

$_ZNSt3__217__compressed_pairINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEENS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS7_IcEEEESE_EES3_EEEEEC2INS_18__default_init_tagESH_EEOT_OT0_ = comdat any

$_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE10value_compEv = comdat any

$_ZNSt3__217__compressed_pairImNS_19__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS7_S7_EENS_4lessIS7_EELb1EEEEC2IiRKSC_EEOT_OT0_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEEE39__select_on_container_copy_constructionENS_17integral_constantIbLb0EEERKSC_ = comdat any

$_ZNKSt3__217__compressed_pairINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEENS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS7_IcEEEESE_EES3_EEEEE6secondEv = comdat any

$_ZNKSt3__222__compressed_pair_elemINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEELi0ELb0EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__27forwardINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEEEEOT_RNS_16remove_referenceISD_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEELi1ELb1EEC2ISC_vEEOT_ = comdat any

$_ZNSt3__215__tree_end_nodeIPNS_16__tree_node_baseIPvEEEC2Ev = comdat any

$_ZNKSt3__217__compressed_pairImNS_19__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS7_S7_EENS_4lessIS7_EELb1EEEE6secondEv = comdat any

$_ZNKSt3__222__compressed_pair_elemINS_19__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS7_S7_EENS_4lessIS7_EELb1EEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__27forwardIiEEOT_RNS_16remove_referenceIS1_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemImLi0ELb0EEC2IivEEOT_ = comdat any

$_ZNSt3__27forwardIRKNS_19__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS7_S7_EENS_4lessIS7_EELb1EEEEEOT_RNS_16remove_referenceISF_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemINS_19__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS7_S7_EENS_4lessIS7_EELb1EEELi1ELb1EEC2IRKSC_vEEOT_ = comdat any

$_ZNKSt3__23mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_NS_4lessIS6_EENS4_INS_4pairIKS6_S6_EEEEE4cendEv = comdat any

$_ZNSt3__2neERKNS_20__map_const_iteratorINS_21__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPNS_11__tree_nodeIS9_PvEElEEEESH_ = comdat any

$_ZNSt3__23mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_NS_4lessIS6_EENS4_INS_4pairIKS6_S6_EEEEE6insertENS_20__map_const_iteratorINS_21__tree_const_iteratorINS_12__value_typeIS6_S6_EEPNS_11__tree_nodeISH_PvEElEEEERKSB_ = comdat any

$_ZNSt3__220__map_const_iteratorINS_21__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPNS_11__tree_nodeIS9_PvEElEEEC2ESE_ = comdat any

$_ZNKSt3__220__map_const_iteratorINS_21__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPNS_11__tree_nodeIS9_PvEElEEEdeEv = comdat any

$_ZNSt3__220__map_const_iteratorINS_21__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPNS_11__tree_nodeIS9_PvEElEEEppEv = comdat any

$_ZNSt3__2neERKNS_21__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElEESF_ = comdat any

$_ZNSt3__2eqERKNS_21__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElEESF_ = comdat any

$_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE15__insert_uniqueENS_21__tree_const_iteratorIS8_PNS_11__tree_nodeIS8_PvEElEERKNS_4pairIKS7_S7_EE = comdat any

$_ZNSt3__214__map_iteratorINS_15__tree_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPNS_11__tree_nodeIS9_PvEElEEEC2ESE_ = comdat any

$_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE30__emplace_hint_unique_key_argsIS7_JRKNS_4pairIKS7_S7_EEEEENS_15__tree_iteratorIS8_PNS_11__tree_nodeIS8_PvEElEENS_21__tree_const_iteratorIS8_SP_lEERKT_DpOT0_ = comdat any

$_ZNSt3__222__tree_key_value_typesINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEE9__get_keyIKNS_4pairIKS7_S7_EEEENS_9enable_ifIXsr17__is_same_uncvrefIT_SD_EE5valueERSC_E4typeERSG_ = comdat any

$_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE12__find_equalIS7_EERPNS_16__tree_node_baseIPvEENS_21__tree_const_iteratorIS8_PNS_11__tree_nodeIS8_SH_EElEERPNS_15__tree_end_nodeISJ_EESK_RKT_ = comdat any

$_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE16__construct_nodeIJRKNS_4pairIKS7_S7_EEEEENS_10unique_ptrINS_11__tree_nodeIS8_PvEENS_22__tree_node_destructorINS5_ISO_EEEEEEDpOT_ = comdat any

$_ZNSt3__27forwardIRKNS_4pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEEEOT_RNS_16remove_referenceISC_E4typeE = comdat any

$_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE16__insert_node_atEPNS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEERSJ_SJ_ = comdat any

$_ZNKSt3__210unique_ptrINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEE3getEv = comdat any

$_ZNSt3__210unique_ptrINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEE7releaseEv = comdat any

$_ZNSt3__210unique_ptrINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEED2Ev = comdat any

$_ZNSt3__215__tree_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElEC2ESC_ = comdat any

$_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE3endEv = comdat any

$_ZNSt3__221__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElEC2ENS_15__tree_iteratorIS8_SC_lEE = comdat any

$_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE10value_compEv = comdat any

$_ZNKSt3__219__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS6_S6_EENS_4lessIS6_EELb1EEclERKS6_RKS8_ = comdat any

$_ZNKSt3__221__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElEdeEv = comdat any

$_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE5beginEv = comdat any

$_ZNKSt3__219__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS6_S6_EENS_4lessIS6_EELb1EEclERKS8_RKS6_ = comdat any

$_ZNSt3__221__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElEmmEv = comdat any

$_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE12__find_equalIS7_EERPNS_16__tree_node_baseIPvEERPNS_15__tree_end_nodeISJ_EERKT_ = comdat any

$_ZNSt3__24nextINS_21__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPNS_11__tree_nodeIS9_PvEElEEEENS_9enable_ifIXsr25__is_cpp17_input_iteratorIT_EE5valueESG_E4typeESG_NS_15iterator_traitsISG_E15difference_typeE = comdat any

$_ZNKSt3__221__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElE8__get_npEv = comdat any

$_ZNSt3__215__tree_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElEC2EPNS_15__tree_end_nodeIPNS_16__tree_node_baseISA_EEEE = comdat any

$_ZNSt3__217__compressed_pairImNS_19__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS7_S7_EENS_4lessIS7_EELb1EEEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_19__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS7_S7_EENS_4lessIS7_EELb1EEELi1ELb1EE5__getEv = comdat any

$_ZNKSt3__24lessINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEclERKS6_S9_ = comdat any

$_ZNKSt3__212__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_E11__get_valueEv = comdat any

$_ZNSt3__2ltIcNS_11char_traitsIcEENS_9allocatorIcEEEEbRKNS_12basic_stringIT_T0_T1_EESB_ = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE7compareERKS5_ = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE7compareINS_17basic_string_viewIcS2_EEEENS_9enable_ifIXsr33__can_be_converted_to_string_viewIcS2_T_EE5valueEiE4typeERKSA_ = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEcvNS_17basic_string_viewIcS2_EEEv = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4sizeEv = comdat any

$_ZNKSt3__217basic_string_viewIcNS_11char_traitsIcEEE4sizeEv = comdat any

$_ZNSt3__211char_traitsIcE7compareEPKcS3_m = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv = comdat any

$_ZNKSt3__217basic_string_viewIcNS_11char_traitsIcEEE4dataEv = comdat any

$_ZNSt3__23minImEERKT_S3_S3_ = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE9__is_longEv = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE15__get_long_sizeEv = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE16__get_short_sizeEv = comdat any

$_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EE5__getEv = comdat any

$_ZNSt3__212__to_addressIKcEEPT_S3_ = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE13__get_pointerEv = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE18__get_long_pointerEv = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE19__get_short_pointerEv = comdat any

$_ZNSt3__214pointer_traitsIPKcE10pointer_toERS1_ = comdat any

$_ZNSt3__29addressofIKcEEPT_RS2_ = comdat any

$_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_ = comdat any

$_ZNKSt3__26__lessImmEclERKmS3_ = comdat any

$_ZNSt3__217basic_string_viewIcNS_11char_traitsIcEEEC2EPKcm = comdat any

$_ZNSt3__216__tree_prev_iterIPNS_16__tree_node_baseIPvEEPNS_15__tree_end_nodeIS4_EEEET_T0_ = comdat any

$_ZNSt3__210__tree_maxIPNS_16__tree_node_baseIPvEEEET_S5_ = comdat any

$_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE6__rootEv = comdat any

$_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE10__root_ptrEv = comdat any

$_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE10__end_nodeEv = comdat any

$_ZNKSt3__217__compressed_pairINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEENS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS7_IcEEEESE_EES3_EEEEE5firstEv = comdat any

$_ZNSt3__27advanceINS_21__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPNS_11__tree_nodeIS9_PvEElEEEEvRT_NS_15iterator_traitsISF_E15difference_typeE = comdat any

$_ZNSt3__29__advanceINS_21__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPNS_11__tree_nodeIS9_PvEElEEEEvRT_NS_15iterator_traitsISF_E15difference_typeENS_26bidirectional_iterator_tagE = comdat any

$_ZNSt3__221__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElEppEv = comdat any

$_ZNSt3__216__tree_next_iterIPNS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEES5_EET_T0_ = comdat any

$_ZNSt3__210__tree_minIPNS_16__tree_node_baseIPvEEEET_S5_ = comdat any

$_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE12__node_allocEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEEE8allocateERSC_m = comdat any

$_ZNSt3__222__tree_node_destructorINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEEEC2ERSC_b = comdat any

$_ZNSt3__210unique_ptrINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEEC2ILb1EvEEPSB_NS_16__dependent_typeINS_27__unique_ptr_deleter_sfinaeISE_EEXT_EE20__good_rval_ref_typeE = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEEE9constructINS_4pairIKS8_S8_EEJRKSH_EEEvRSC_PT_DpOT0_ = comdat any

$_ZNSt3__222__tree_key_value_typesINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEE9__get_ptrERS8_ = comdat any

$_ZNKSt3__210unique_ptrINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEEptEv = comdat any

$_ZNSt3__210unique_ptrINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEE11get_deleterEv = comdat any

$_ZNSt3__217__compressed_pairINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEENS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS7_IcEEEESE_EES3_EEEEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__29allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEES7_EEPvEEE8allocateEmPKv = comdat any

$_ZNKSt3__29allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEES7_EEPvEEE8max_sizeEv = comdat any

$_ZNSt3__24moveIRNS_22__tree_node_destructorINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS2_IcEEEES9_EEPvEEEEEEEEONS_16remove_referenceIT_E4typeEOSH_ = comdat any

$_ZNSt3__217__compressed_pairIPNS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEEC2IRSC_SF_EEOT_OT0_ = comdat any

$_ZNSt3__27forwardIRPNS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEEEEOT_RNS_16remove_referenceISE_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPNS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEELi0ELb0EEC2IRSC_vEEOT_ = comdat any

$_ZNSt3__27forwardINS_22__tree_node_destructorINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS2_IcEEEES9_EEPvEEEEEEEEOT_RNS_16remove_referenceISF_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemINS_22__tree_node_destructorINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS2_IcEEEES9_EEPvEEEEEELi1ELb0EEC2ISE_vEEOT_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEEE11__constructINS_4pairIKS8_S8_EEJRKSH_EEEvNS_17integral_constantIbLb1EEERSC_PT_DpOT0_ = comdat any

$_ZNSt3__29allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEES7_EEPvEEE9constructINS_4pairIKS7_S7_EEJRKSF_EEEvPT_DpOT0_ = comdat any

$_ZNSt3__24pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_EC2ERKS8_ = comdat any

$_ZNSt3__29addressofINS_4pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEEEPT_RSA_ = comdat any

$_ZNSt3__212__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_E11__get_valueEv = comdat any

$_ZNKSt3__217__compressed_pairIPNS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPNS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPNS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_22__tree_node_destructorINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS2_IcEEEES9_EEPvEEEEEELi1ELb0EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPNS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEE5firstEv = comdat any

$_ZNSt3__222__compressed_pair_elemIPNS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__210unique_ptrINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEE5resetEPSB_ = comdat any

$_ZNSt3__222__tree_node_destructorINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEEEclEPSB_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEEE7destroyINS_4pairIKS8_S8_EEEEvRSC_PT_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEEE10deallocateERSC_PSB_m = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEEE9__destroyINS_4pairIKS8_S8_EEEEvNS_17integral_constantIbLb0EEERSC_PT_ = comdat any

$_ZNSt3__24pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_ED2Ev = comdat any

$_ZNSt3__29allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEES7_EEPvEEE10deallocateEPSA_m = comdat any

$_ZNKSt3__221__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElEptEv = comdat any

$_ZNSt3__214pointer_traitsIPKNS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEE10pointer_toERS9_ = comdat any

$_ZNSt3__29addressofIKNS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEEEPT_RSA_ = comdat any

$_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE5beginEv = comdat any

$_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE12__begin_nodeEv = comdat any

$_ZNSt3__221__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElEC2EPNS_15__tree_end_nodeIPNS_16__tree_node_baseISA_EEEE = comdat any

$_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE3endEv = comdat any

$_ZNSt3__23mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_NS_4lessIS6_EENS4_INS_4pairIKS6_S6_EEEEED2Ev = comdat any

$_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEED2Ev = comdat any

$_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE7destroyEPNS_11__tree_nodeIS8_PvEE = comdat any

@.str = private unnamed_addr constant [29 x i8] c"Unsupported encoding method.\00", align 1
@.str.1 = private unnamed_addr constant [27 x i8] c"Unsupported geometry type.\00", align 1
@.str.2 = private unnamed_addr constant [21 x i8] c"Input is not a mesh.\00", align 1
@.str.3 = private unnamed_addr constant [25 x i8] c"skip_attribute_transform\00", align 1
@.str.4 = private unnamed_addr constant [68 x i8] c"allocator<T>::allocate(size_t n) 'n' exceeds maximum supported size\00", align 1

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN5draco17CreateMeshDecoderEh(%"class.draco::StatusOr"* noalias sret align 4 %agg.result, i8 zeroext %method) #0 {
entry:
  %result.ptr = alloca i8*, align 4
  %method.addr = alloca i8, align 1
  %ref.tmp = alloca %"class.std::__2::unique_ptr", align 4
  %ref.tmp8 = alloca %"class.std::__2::unique_ptr", align 4
  %ref.tmp15 = alloca %"class.draco::Status", align 4
  %ref.tmp16 = alloca %"class.std::__2::basic_string", align 4
  %0 = bitcast %"class.draco::StatusOr"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store i8 %method, i8* %method.addr, align 1
  %1 = load i8, i8* %method.addr, align 1
  %conv = zext i8 %1 to i32
  %cmp = icmp eq i32 %conv, 0
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %call = call noalias nonnull i8* @_Znwm(i32 48) #8
  %2 = bitcast i8* %call to %"class.draco::MeshSequentialDecoder"*
  %call1 = call %"class.draco::MeshSequentialDecoder"* @_ZN5draco21MeshSequentialDecoderC1Ev(%"class.draco::MeshSequentialDecoder"* %2)
  %3 = bitcast %"class.draco::MeshSequentialDecoder"* %2 to %"class.draco::MeshDecoder"*
  %call2 = call %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrIN5draco11MeshDecoderENS_14default_deleteIS2_EEEC2ILb1EvEEPS2_(%"class.std::__2::unique_ptr"* %ref.tmp, %"class.draco::MeshDecoder"* %3) #9
  %call3 = call %"class.draco::StatusOr"* @_ZN5draco8StatusOrINSt3__210unique_ptrINS_11MeshDecoderENS1_14default_deleteIS3_EEEEEC2EOS6_(%"class.draco::StatusOr"* %agg.result, %"class.std::__2::unique_ptr"* nonnull align 4 dereferenceable(4) %ref.tmp)
  %call4 = call %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrIN5draco11MeshDecoderENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr"* %ref.tmp) #9
  br label %return

if.else:                                          ; preds = %entry
  %4 = load i8, i8* %method.addr, align 1
  %conv5 = zext i8 %4 to i32
  %cmp6 = icmp eq i32 %conv5, 1
  br i1 %cmp6, label %if.then7, label %if.end

if.then7:                                         ; preds = %if.else
  %call9 = call noalias nonnull i8* @_Znwm(i32 52) #8
  %5 = bitcast i8* %call9 to %"class.draco::MeshEdgebreakerDecoder"*
  %call10 = call %"class.draco::MeshEdgebreakerDecoder"* @_ZN5draco22MeshEdgebreakerDecoderC1Ev(%"class.draco::MeshEdgebreakerDecoder"* %5)
  %6 = bitcast %"class.draco::MeshEdgebreakerDecoder"* %5 to %"class.draco::MeshDecoder"*
  %call11 = call %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrIN5draco11MeshDecoderENS_14default_deleteIS2_EEEC2ILb1EvEEPS2_(%"class.std::__2::unique_ptr"* %ref.tmp8, %"class.draco::MeshDecoder"* %6) #9
  %call12 = call %"class.draco::StatusOr"* @_ZN5draco8StatusOrINSt3__210unique_ptrINS_11MeshDecoderENS1_14default_deleteIS3_EEEEEC2EOS6_(%"class.draco::StatusOr"* %agg.result, %"class.std::__2::unique_ptr"* nonnull align 4 dereferenceable(4) %ref.tmp8)
  %call13 = call %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrIN5draco11MeshDecoderENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr"* %ref.tmp8) #9
  br label %return

if.end:                                           ; preds = %if.else
  br label %if.end14

if.end14:                                         ; preds = %if.end
  %call17 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2IDnEEPKc(%"class.std::__2::basic_string"* %ref.tmp16, i8* getelementptr inbounds ([29 x i8], [29 x i8]* @.str, i32 0, i32 0))
  %call18 = call %"class.draco::Status"* @_ZN5draco6StatusC2ENS0_4CodeERKNSt3__212basic_stringIcNS2_11char_traitsIcEENS2_9allocatorIcEEEE(%"class.draco::Status"* %ref.tmp15, i32 -1, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %ref.tmp16)
  %call19 = call %"class.draco::StatusOr"* @_ZN5draco8StatusOrINSt3__210unique_ptrINS_11MeshDecoderENS1_14default_deleteIS3_EEEEEC2ERKNS_6StatusE(%"class.draco::StatusOr"* %agg.result, %"class.draco::Status"* nonnull align 4 dereferenceable(16) %ref.tmp15)
  %call20 = call %"class.draco::Status"* @_ZN5draco6StatusD2Ev(%"class.draco::Status"* %ref.tmp15) #9
  %call21 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_string"* %ref.tmp16) #9
  br label %return

return:                                           ; preds = %if.end14, %if.then7, %if.then
  ret void
}

; Function Attrs: nobuiltin allocsize(0)
declare nonnull i8* @_Znwm(i32) #1

declare %"class.draco::MeshSequentialDecoder"* @_ZN5draco21MeshSequentialDecoderC1Ev(%"class.draco::MeshSequentialDecoder"* returned) unnamed_addr #2

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrIN5draco11MeshDecoderENS_14default_deleteIS2_EEEC2ILb1EvEEPS2_(%"class.std::__2::unique_ptr"* returned %this, %"class.draco::MeshDecoder"* %__p) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  %__p.addr = alloca %"class.draco::MeshDecoder"*, align 4
  %ref.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  store %"class.draco::MeshDecoder"* %__p, %"class.draco::MeshDecoder"** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::__compressed_pair.1"* @_ZNSt3__217__compressed_pairIPN5draco11MeshDecoderENS_14default_deleteIS2_EEEC2IRS3_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.1"* %__ptr_, %"class.draco::MeshDecoder"** nonnull align 4 dereferenceable(4) %__p.addr, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp)
  ret %"class.std::__2::unique_ptr"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::StatusOr"* @_ZN5draco8StatusOrINSt3__210unique_ptrINS_11MeshDecoderENS1_14default_deleteIS3_EEEEEC2EOS6_(%"class.draco::StatusOr"* returned %this, %"class.std::__2::unique_ptr"* nonnull align 4 dereferenceable(4) %value) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::StatusOr"*, align 4
  %value.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  store %"class.draco::StatusOr"* %this, %"class.draco::StatusOr"** %this.addr, align 4
  store %"class.std::__2::unique_ptr"* %value, %"class.std::__2::unique_ptr"** %value.addr, align 4
  %this1 = load %"class.draco::StatusOr"*, %"class.draco::StatusOr"** %this.addr, align 4
  %status_ = getelementptr inbounds %"class.draco::StatusOr", %"class.draco::StatusOr"* %this1, i32 0, i32 0
  call void @_ZN5draco8OkStatusEv(%"class.draco::Status"* sret align 4 %status_)
  %value_ = getelementptr inbounds %"class.draco::StatusOr", %"class.draco::StatusOr"* %this1, i32 0, i32 1
  %0 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %value.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr"* @_ZNSt3__24moveIRNS_10unique_ptrIN5draco11MeshDecoderENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOS9_(%"class.std::__2::unique_ptr"* nonnull align 4 dereferenceable(4) %0) #9
  %call2 = call %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrIN5draco11MeshDecoderENS_14default_deleteIS2_EEEC2EOS5_(%"class.std::__2::unique_ptr"* %value_, %"class.std::__2::unique_ptr"* nonnull align 4 dereferenceable(4) %call) #9
  ret %"class.draco::StatusOr"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrIN5draco11MeshDecoderENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  call void @_ZNSt3__210unique_ptrIN5draco11MeshDecoderENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr"* %this1, %"class.draco::MeshDecoder"* null) #9
  ret %"class.std::__2::unique_ptr"* %this1
}

declare %"class.draco::MeshEdgebreakerDecoder"* @_ZN5draco22MeshEdgebreakerDecoderC1Ev(%"class.draco::MeshEdgebreakerDecoder"* returned) unnamed_addr #2

; Function Attrs: noinline nounwind optnone
define linkonce_odr %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2IDnEEPKc(%"class.std::__2::basic_string"* returned %this, i8* %__s) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  %__s.addr = alloca i8*, align 4
  %ref.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  %ref.tmp2 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  store i8* %__s, i8** %__s.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::basic_string"* %this1 to %"class.std::__2::__basic_string_common"*
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::__compressed_pair"* @_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_EC2INS_18__default_init_tagESA_EEOT_OT0_(%"class.std::__2::__compressed_pair"* %__r_, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  %1 = load i8*, i8** %__s.addr, align 4
  %2 = load i8*, i8** %__s.addr, align 4
  %call3 = call i32 @_ZNSt3__211char_traitsIcE6lengthEPKc(i8* %2) #9
  call void @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEPKcm(%"class.std::__2::basic_string"* %this1, i8* %1, i32 %call3)
  ret %"class.std::__2::basic_string"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::Status"* @_ZN5draco6StatusC2ENS0_4CodeERKNSt3__212basic_stringIcNS2_11char_traitsIcEENS2_9allocatorIcEEEE(%"class.draco::Status"* returned %this, i32 %code, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %error_msg) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::Status"*, align 4
  %code.addr = alloca i32, align 4
  %error_msg.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.draco::Status"* %this, %"class.draco::Status"** %this.addr, align 4
  store i32 %code, i32* %code.addr, align 4
  store %"class.std::__2::basic_string"* %error_msg, %"class.std::__2::basic_string"** %error_msg.addr, align 4
  %this1 = load %"class.draco::Status"*, %"class.draco::Status"** %this.addr, align 4
  %code_ = getelementptr inbounds %"class.draco::Status", %"class.draco::Status"* %this1, i32 0, i32 0
  %0 = load i32, i32* %code.addr, align 4
  store i32 %0, i32* %code_, align 4
  %error_msg_ = getelementptr inbounds %"class.draco::Status", %"class.draco::Status"* %this1, i32 0, i32 1
  %1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %error_msg.addr, align 4
  %call = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKS5_(%"class.std::__2::basic_string"* %error_msg_, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %1)
  ret %"class.draco::Status"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::StatusOr"* @_ZN5draco8StatusOrINSt3__210unique_ptrINS_11MeshDecoderENS1_14default_deleteIS3_EEEEEC2ERKNS_6StatusE(%"class.draco::StatusOr"* returned %this, %"class.draco::Status"* nonnull align 4 dereferenceable(16) %status) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::StatusOr"*, align 4
  %status.addr = alloca %"class.draco::Status"*, align 4
  store %"class.draco::StatusOr"* %this, %"class.draco::StatusOr"** %this.addr, align 4
  store %"class.draco::Status"* %status, %"class.draco::Status"** %status.addr, align 4
  %this1 = load %"class.draco::StatusOr"*, %"class.draco::StatusOr"** %this.addr, align 4
  %status_ = getelementptr inbounds %"class.draco::StatusOr", %"class.draco::StatusOr"* %this1, i32 0, i32 0
  %0 = load %"class.draco::Status"*, %"class.draco::Status"** %status.addr, align 4
  %call = call %"class.draco::Status"* @_ZN5draco6StatusC2ERKS0_(%"class.draco::Status"* %status_, %"class.draco::Status"* nonnull align 4 dereferenceable(16) %0)
  %value_ = getelementptr inbounds %"class.draco::StatusOr", %"class.draco::StatusOr"* %this1, i32 0, i32 1
  %call2 = call %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrIN5draco11MeshDecoderENS_14default_deleteIS2_EEEC2ILb1EvEEv(%"class.std::__2::unique_ptr"* %value_) #9
  ret %"class.draco::StatusOr"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::Status"* @_ZN5draco6StatusD2Ev(%"class.draco::Status"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::Status"*, align 4
  store %"class.draco::Status"* %this, %"class.draco::Status"** %this.addr, align 4
  %this1 = load %"class.draco::Status"*, %"class.draco::Status"** %this.addr, align 4
  %error_msg_ = getelementptr inbounds %"class.draco::Status", %"class.draco::Status"* %this1, i32 0, i32 1
  %call = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_string"* %error_msg_) #9
  ret %"class.draco::Status"* %this1
}

; Function Attrs: nounwind
declare %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_string"* returned) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN5draco7Decoder22GetEncodedGeometryTypeEPNS_13DecoderBufferE(%"class.draco::StatusOr.155"* noalias sret align 4 %agg.result, %"class.draco::DecoderBuffer"* %in_buffer) #0 {
entry:
  %result.ptr = alloca i8*, align 4
  %in_buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %temp_buffer = alloca %"class.draco::DecoderBuffer", align 8
  %header = alloca %"struct.draco::DracoHeader", align 2
  %_local_status = alloca %"class.draco::Status", align 4
  %cleanup.dest.slot = alloca i32, align 4
  %ref.tmp = alloca i32, align 4
  %0 = bitcast %"class.draco::StatusOr.155"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %"class.draco::DecoderBuffer"* %in_buffer, %"class.draco::DecoderBuffer"** %in_buffer.addr, align 4
  %1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %in_buffer.addr, align 4
  %2 = bitcast %"class.draco::DecoderBuffer"* %temp_buffer to i8*
  %3 = bitcast %"class.draco::DecoderBuffer"* %1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %2, i8* align 8 %3, i32 40, i1 false)
  call void @_ZN5draco17PointCloudDecoder12DecodeHeaderEPNS_13DecoderBufferEPNS_11DracoHeaderE(%"class.draco::Status"* sret align 4 %_local_status, %"class.draco::DecoderBuffer"* %temp_buffer, %"struct.draco::DracoHeader"* %header)
  %call = call zeroext i1 @_ZNK5draco6Status2okEv(%"class.draco::Status"* %_local_status)
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  %call1 = call %"class.draco::StatusOr.155"* @_ZN5draco8StatusOrINS_19EncodedGeometryTypeEEC2ERKNS_6StatusE(%"class.draco::StatusOr.155"* %agg.result, %"class.draco::Status"* nonnull align 4 dereferenceable(16) %_local_status)
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then
  %call2 = call %"class.draco::Status"* @_ZN5draco6StatusD2Ev(%"class.draco::Status"* %_local_status) #9
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup4 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  %encoder_type = getelementptr inbounds %"struct.draco::DracoHeader", %"struct.draco::DracoHeader"* %header, i32 0, i32 3
  %4 = load i8, i8* %encoder_type, align 1
  %conv = zext i8 %4 to i32
  store i32 %conv, i32* %ref.tmp, align 4
  %call3 = call %"class.draco::StatusOr.155"* @_ZN5draco8StatusOrINS_19EncodedGeometryTypeEEC2EOS1_(%"class.draco::StatusOr.155"* %agg.result, i32* nonnull align 4 dereferenceable(4) %ref.tmp)
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup4

cleanup4:                                         ; preds = %cleanup.cont, %cleanup
  %call5 = call %"class.draco::DecoderBuffer"* @_ZN5draco13DecoderBufferD2Ev(%"class.draco::DecoderBuffer"* %temp_buffer) #9
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #4

declare void @_ZN5draco17PointCloudDecoder12DecodeHeaderEPNS_13DecoderBufferEPNS_11DracoHeaderE(%"class.draco::Status"* sret align 4, %"class.draco::DecoderBuffer"*, %"struct.draco::DracoHeader"*) #2

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK5draco6Status2okEv(%"class.draco::Status"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::Status"*, align 4
  store %"class.draco::Status"* %this, %"class.draco::Status"** %this.addr, align 4
  %this1 = load %"class.draco::Status"*, %"class.draco::Status"** %this.addr, align 4
  %code_ = getelementptr inbounds %"class.draco::Status", %"class.draco::Status"* %this1, i32 0, i32 0
  %0 = load i32, i32* %code_, align 4
  %cmp = icmp eq i32 %0, 0
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::StatusOr.155"* @_ZN5draco8StatusOrINS_19EncodedGeometryTypeEEC2ERKNS_6StatusE(%"class.draco::StatusOr.155"* returned %this, %"class.draco::Status"* nonnull align 4 dereferenceable(16) %status) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::StatusOr.155"*, align 4
  %status.addr = alloca %"class.draco::Status"*, align 4
  store %"class.draco::StatusOr.155"* %this, %"class.draco::StatusOr.155"** %this.addr, align 4
  store %"class.draco::Status"* %status, %"class.draco::Status"** %status.addr, align 4
  %this1 = load %"class.draco::StatusOr.155"*, %"class.draco::StatusOr.155"** %this.addr, align 4
  %status_ = getelementptr inbounds %"class.draco::StatusOr.155", %"class.draco::StatusOr.155"* %this1, i32 0, i32 0
  %0 = load %"class.draco::Status"*, %"class.draco::Status"** %status.addr, align 4
  %call = call %"class.draco::Status"* @_ZN5draco6StatusC2ERKS0_(%"class.draco::Status"* %status_, %"class.draco::Status"* nonnull align 4 dereferenceable(16) %0)
  ret %"class.draco::StatusOr.155"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::StatusOr.155"* @_ZN5draco8StatusOrINS_19EncodedGeometryTypeEEC2EOS1_(%"class.draco::StatusOr.155"* returned %this, i32* nonnull align 4 dereferenceable(4) %value) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::StatusOr.155"*, align 4
  %value.addr = alloca i32*, align 4
  store %"class.draco::StatusOr.155"* %this, %"class.draco::StatusOr.155"** %this.addr, align 4
  store i32* %value, i32** %value.addr, align 4
  %this1 = load %"class.draco::StatusOr.155"*, %"class.draco::StatusOr.155"** %this.addr, align 4
  %status_ = getelementptr inbounds %"class.draco::StatusOr.155", %"class.draco::StatusOr.155"* %this1, i32 0, i32 0
  call void @_ZN5draco8OkStatusEv(%"class.draco::Status"* sret align 4 %status_)
  %value_ = getelementptr inbounds %"class.draco::StatusOr.155", %"class.draco::StatusOr.155"* %this1, i32 0, i32 1
  %0 = load i32*, i32** %value.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__24moveIRN5draco19EncodedGeometryTypeEEEONS_16remove_referenceIT_E4typeEOS5_(i32* nonnull align 4 dereferenceable(4) %0) #9
  %1 = load i32, i32* %call, align 4
  store i32 %1, i32* %value_, align 4
  ret %"class.draco::StatusOr.155"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::DecoderBuffer"* @_ZN5draco13DecoderBufferD2Ev(%"class.draco::DecoderBuffer"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  store %"class.draco::DecoderBuffer"* %this, %"class.draco::DecoderBuffer"** %this.addr, align 4
  %this1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %this.addr, align 4
  %bit_decoder_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 3
  %call = call %"class.draco::DecoderBuffer::BitDecoder"* @_ZN5draco13DecoderBuffer10BitDecoderD1Ev(%"class.draco::DecoderBuffer::BitDecoder"* %bit_decoder_) #9
  ret %"class.draco::DecoderBuffer"* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN5draco7Decoder26DecodePointCloudFromBufferEPNS_13DecoderBufferE(%"class.draco::StatusOr.156"* noalias sret align 4 %agg.result, %"class.draco::Decoder"* %this, %"class.draco::DecoderBuffer"* %in_buffer) #0 {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %"class.draco::Decoder"*, align 4
  %in_buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %_statusor66 = alloca %"class.draco::StatusOr.155", align 4
  %_status = alloca %"class.draco::Status", align 4
  %cleanup.dest.slot = alloca i32, align 4
  %type = alloca i32, align 4
  %mesh = alloca %"class.std::__2::unique_ptr.162", align 4
  %_local_status = alloca %"class.draco::Status", align 4
  %ref.tmp = alloca %"class.std::__2::unique_ptr.157", align 4
  %ref.tmp29 = alloca %"class.draco::Status", align 4
  %ref.tmp30 = alloca %"class.std::__2::basic_string", align 4
  %0 = bitcast %"class.draco::StatusOr.156"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %"class.draco::Decoder"* %this, %"class.draco::Decoder"** %this.addr, align 4
  store %"class.draco::DecoderBuffer"* %in_buffer, %"class.draco::DecoderBuffer"** %in_buffer.addr, align 4
  %this1 = load %"class.draco::Decoder"*, %"class.draco::Decoder"** %this.addr, align 4
  %1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %in_buffer.addr, align 4
  call void @_ZN5draco7Decoder22GetEncodedGeometryTypeEPNS_13DecoderBufferE(%"class.draco::StatusOr.155"* sret align 4 %_statusor66, %"class.draco::DecoderBuffer"* %1)
  %call = call zeroext i1 @_ZNK5draco8StatusOrINS_19EncodedGeometryTypeEE2okEv(%"class.draco::StatusOr.155"* %_statusor66)
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  %call2 = call nonnull align 4 dereferenceable(16) %"class.draco::Status"* @_ZNK5draco8StatusOrINS_19EncodedGeometryTypeEE6statusEv(%"class.draco::StatusOr.155"* %_statusor66)
  %call3 = call nonnull align 4 dereferenceable(16) %"class.draco::Status"* @_ZNSt3__24moveIRKN5draco6StatusEEEONS_16remove_referenceIT_E4typeEOS6_(%"class.draco::Status"* nonnull align 4 dereferenceable(16) %call2) #9
  %call4 = call %"class.draco::Status"* @_ZN5draco6StatusC2ERKS0_(%"class.draco::Status"* %_status, %"class.draco::Status"* nonnull align 4 dereferenceable(16) %call3)
  %call5 = call %"class.draco::StatusOr.156"* @_ZN5draco8StatusOrINSt3__210unique_ptrINS_10PointCloudENS1_14default_deleteIS3_EEEEEC2ERKNS_6StatusE(%"class.draco::StatusOr.156"* %agg.result, %"class.draco::Status"* nonnull align 4 dereferenceable(16) %_status)
  store i32 1, i32* %cleanup.dest.slot, align 4
  %call6 = call %"class.draco::Status"* @_ZN5draco6StatusD2Ev(%"class.draco::Status"* %_status) #9
  br label %cleanup36

if.end:                                           ; preds = %entry
  %call7 = call nonnull align 4 dereferenceable(20) %"class.draco::StatusOr.155"* @_ZNSt3__24moveIRN5draco8StatusOrINS1_19EncodedGeometryTypeEEEEEONS_16remove_referenceIT_E4typeEOS7_(%"class.draco::StatusOr.155"* nonnull align 4 dereferenceable(20) %_statusor66) #9
  %call8 = call nonnull align 4 dereferenceable(4) i32* @_ZNO5draco8StatusOrINS_19EncodedGeometryTypeEE5valueEv(%"class.draco::StatusOr.155"* %call7)
  %2 = load i32, i32* %call8, align 4
  store i32 %2, i32* %type, align 4
  %3 = load i32, i32* %type, align 4
  %cmp = icmp eq i32 %3, 0
  br i1 %cmp, label %if.then9, label %if.else

if.then9:                                         ; preds = %if.end
  br label %if.end28

if.else:                                          ; preds = %if.end
  %4 = load i32, i32* %type, align 4
  %cmp10 = icmp eq i32 %4, 1
  br i1 %cmp10, label %if.then11, label %if.end27

if.then11:                                        ; preds = %if.else
  %call12 = call noalias nonnull i8* @_Znwm(i32 108) #8
  %5 = bitcast i8* %call12 to %"class.draco::Mesh"*
  %call13 = call %"class.draco::Mesh"* @_ZN5draco4MeshC1Ev(%"class.draco::Mesh"* %5)
  %call14 = call %"class.std::__2::unique_ptr.162"* @_ZNSt3__210unique_ptrIN5draco4MeshENS_14default_deleteIS2_EEEC2ILb1EvEEPS2_(%"class.std::__2::unique_ptr.162"* %mesh, %"class.draco::Mesh"* %5) #9
  %6 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %in_buffer.addr, align 4
  %call15 = call %"class.draco::Mesh"* @_ZNKSt3__210unique_ptrIN5draco4MeshENS_14default_deleteIS2_EEE3getEv(%"class.std::__2::unique_ptr.162"* %mesh) #9
  call void @_ZN5draco7Decoder22DecodeBufferToGeometryEPNS_13DecoderBufferEPNS_4MeshE(%"class.draco::Status"* sret align 4 %_local_status, %"class.draco::Decoder"* %this1, %"class.draco::DecoderBuffer"* %6, %"class.draco::Mesh"* %call15)
  %call16 = call zeroext i1 @_ZNK5draco6Status2okEv(%"class.draco::Status"* %_local_status)
  br i1 %call16, label %if.end19, label %if.then17

if.then17:                                        ; preds = %if.then11
  %call18 = call %"class.draco::StatusOr.156"* @_ZN5draco8StatusOrINSt3__210unique_ptrINS_10PointCloudENS1_14default_deleteIS3_EEEEEC2ERKNS_6StatusE(%"class.draco::StatusOr.156"* %agg.result, %"class.draco::Status"* nonnull align 4 dereferenceable(16) %_local_status)
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end19:                                         ; preds = %if.then11
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end19, %if.then17
  %call20 = call %"class.draco::Status"* @_ZN5draco6StatusD2Ev(%"class.draco::Status"* %_local_status) #9
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup25 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  %call21 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.162"* @_ZNSt3__24moveIRNS_10unique_ptrIN5draco4MeshENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOS9_(%"class.std::__2::unique_ptr.162"* nonnull align 4 dereferenceable(4) %mesh) #9
  %call22 = call %"class.std::__2::unique_ptr.157"* @_ZNSt3__210unique_ptrIN5draco10PointCloudENS_14default_deleteIS2_EEEC2INS1_4MeshENS3_IS7_EEvvEEONS0_IT_T0_EE(%"class.std::__2::unique_ptr.157"* %ref.tmp, %"class.std::__2::unique_ptr.162"* nonnull align 4 dereferenceable(4) %call21) #9
  %call23 = call %"class.draco::StatusOr.156"* @_ZN5draco8StatusOrINSt3__210unique_ptrINS_10PointCloudENS1_14default_deleteIS3_EEEEEC2EOS6_(%"class.draco::StatusOr.156"* %agg.result, %"class.std::__2::unique_ptr.157"* nonnull align 4 dereferenceable(4) %ref.tmp)
  %call24 = call %"class.std::__2::unique_ptr.157"* @_ZNSt3__210unique_ptrIN5draco10PointCloudENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.157"* %ref.tmp) #9
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup25

cleanup25:                                        ; preds = %cleanup.cont, %cleanup
  %call26 = call %"class.std::__2::unique_ptr.162"* @_ZNSt3__210unique_ptrIN5draco4MeshENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.162"* %mesh) #9
  br label %cleanup36

if.end27:                                         ; preds = %if.else
  br label %if.end28

if.end28:                                         ; preds = %if.end27, %if.then9
  %call31 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2IDnEEPKc(%"class.std::__2::basic_string"* %ref.tmp30, i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str.1, i32 0, i32 0))
  %call32 = call %"class.draco::Status"* @_ZN5draco6StatusC2ENS0_4CodeERKNSt3__212basic_stringIcNS2_11char_traitsIcEENS2_9allocatorIcEEEE(%"class.draco::Status"* %ref.tmp29, i32 -1, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %ref.tmp30)
  %call33 = call %"class.draco::StatusOr.156"* @_ZN5draco8StatusOrINSt3__210unique_ptrINS_10PointCloudENS1_14default_deleteIS3_EEEEEC2ERKNS_6StatusE(%"class.draco::StatusOr.156"* %agg.result, %"class.draco::Status"* nonnull align 4 dereferenceable(16) %ref.tmp29)
  %call34 = call %"class.draco::Status"* @_ZN5draco6StatusD2Ev(%"class.draco::Status"* %ref.tmp29) #9
  %call35 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_string"* %ref.tmp30) #9
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup36

cleanup36:                                        ; preds = %if.end28, %cleanup25, %if.then
  %call37 = call %"class.draco::StatusOr.155"* @_ZN5draco8StatusOrINS_19EncodedGeometryTypeEED2Ev(%"class.draco::StatusOr.155"* %_statusor66) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK5draco8StatusOrINS_19EncodedGeometryTypeEE2okEv(%"class.draco::StatusOr.155"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::StatusOr.155"*, align 4
  store %"class.draco::StatusOr.155"* %this, %"class.draco::StatusOr.155"** %this.addr, align 4
  %this1 = load %"class.draco::StatusOr.155"*, %"class.draco::StatusOr.155"** %this.addr, align 4
  %status_ = getelementptr inbounds %"class.draco::StatusOr.155", %"class.draco::StatusOr.155"* %this1, i32 0, i32 0
  %call = call zeroext i1 @_ZNK5draco6Status2okEv(%"class.draco::Status"* %status_)
  ret i1 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %"class.draco::Status"* @_ZNSt3__24moveIRKN5draco6StatusEEEONS_16remove_referenceIT_E4typeEOS6_(%"class.draco::Status"* nonnull align 4 dereferenceable(16) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.draco::Status"*, align 4
  store %"class.draco::Status"* %__t, %"class.draco::Status"** %__t.addr, align 4
  %0 = load %"class.draco::Status"*, %"class.draco::Status"** %__t.addr, align 4
  ret %"class.draco::Status"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %"class.draco::Status"* @_ZNK5draco8StatusOrINS_19EncodedGeometryTypeEE6statusEv(%"class.draco::StatusOr.155"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::StatusOr.155"*, align 4
  store %"class.draco::StatusOr.155"* %this, %"class.draco::StatusOr.155"** %this.addr, align 4
  %this1 = load %"class.draco::StatusOr.155"*, %"class.draco::StatusOr.155"** %this.addr, align 4
  %status_ = getelementptr inbounds %"class.draco::StatusOr.155", %"class.draco::StatusOr.155"* %this1, i32 0, i32 0
  ret %"class.draco::Status"* %status_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::Status"* @_ZN5draco6StatusC2ERKS0_(%"class.draco::Status"* returned %this, %"class.draco::Status"* nonnull align 4 dereferenceable(16) %status) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::Status"*, align 4
  %status.addr = alloca %"class.draco::Status"*, align 4
  store %"class.draco::Status"* %this, %"class.draco::Status"** %this.addr, align 4
  store %"class.draco::Status"* %status, %"class.draco::Status"** %status.addr, align 4
  %this1 = load %"class.draco::Status"*, %"class.draco::Status"** %this.addr, align 4
  %code_ = getelementptr inbounds %"class.draco::Status", %"class.draco::Status"* %this1, i32 0, i32 0
  %0 = load %"class.draco::Status"*, %"class.draco::Status"** %status.addr, align 4
  %code_2 = getelementptr inbounds %"class.draco::Status", %"class.draco::Status"* %0, i32 0, i32 0
  %1 = load i32, i32* %code_2, align 4
  store i32 %1, i32* %code_, align 4
  %error_msg_ = getelementptr inbounds %"class.draco::Status", %"class.draco::Status"* %this1, i32 0, i32 1
  %2 = load %"class.draco::Status"*, %"class.draco::Status"** %status.addr, align 4
  %error_msg_3 = getelementptr inbounds %"class.draco::Status", %"class.draco::Status"* %2, i32 0, i32 1
  %call = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKS5_(%"class.std::__2::basic_string"* %error_msg_, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %error_msg_3)
  ret %"class.draco::Status"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::StatusOr.156"* @_ZN5draco8StatusOrINSt3__210unique_ptrINS_10PointCloudENS1_14default_deleteIS3_EEEEEC2ERKNS_6StatusE(%"class.draco::StatusOr.156"* returned %this, %"class.draco::Status"* nonnull align 4 dereferenceable(16) %status) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::StatusOr.156"*, align 4
  %status.addr = alloca %"class.draco::Status"*, align 4
  store %"class.draco::StatusOr.156"* %this, %"class.draco::StatusOr.156"** %this.addr, align 4
  store %"class.draco::Status"* %status, %"class.draco::Status"** %status.addr, align 4
  %this1 = load %"class.draco::StatusOr.156"*, %"class.draco::StatusOr.156"** %this.addr, align 4
  %status_ = getelementptr inbounds %"class.draco::StatusOr.156", %"class.draco::StatusOr.156"* %this1, i32 0, i32 0
  %0 = load %"class.draco::Status"*, %"class.draco::Status"** %status.addr, align 4
  %call = call %"class.draco::Status"* @_ZN5draco6StatusC2ERKS0_(%"class.draco::Status"* %status_, %"class.draco::Status"* nonnull align 4 dereferenceable(16) %0)
  %value_ = getelementptr inbounds %"class.draco::StatusOr.156", %"class.draco::StatusOr.156"* %this1, i32 0, i32 1
  %call2 = call %"class.std::__2::unique_ptr.157"* @_ZNSt3__210unique_ptrIN5draco10PointCloudENS_14default_deleteIS2_EEEC2ILb1EvEEv(%"class.std::__2::unique_ptr.157"* %value_) #9
  ret %"class.draco::StatusOr.156"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(20) %"class.draco::StatusOr.155"* @_ZNSt3__24moveIRN5draco8StatusOrINS1_19EncodedGeometryTypeEEEEEONS_16remove_referenceIT_E4typeEOS7_(%"class.draco::StatusOr.155"* nonnull align 4 dereferenceable(20) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.draco::StatusOr.155"*, align 4
  store %"class.draco::StatusOr.155"* %__t, %"class.draco::StatusOr.155"** %__t.addr, align 4
  %0 = load %"class.draco::StatusOr.155"*, %"class.draco::StatusOr.155"** %__t.addr, align 4
  ret %"class.draco::StatusOr.155"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNO5draco8StatusOrINS_19EncodedGeometryTypeEE5valueEv(%"class.draco::StatusOr.155"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::StatusOr.155"*, align 4
  store %"class.draco::StatusOr.155"* %this, %"class.draco::StatusOr.155"** %this.addr, align 4
  %this1 = load %"class.draco::StatusOr.155"*, %"class.draco::StatusOr.155"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::StatusOr.155", %"class.draco::StatusOr.155"* %this1, i32 0, i32 1
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__24moveIRN5draco19EncodedGeometryTypeEEEONS_16remove_referenceIT_E4typeEOS5_(i32* nonnull align 4 dereferenceable(4) %value_) #9
  ret i32* %call
}

declare %"class.draco::Mesh"* @_ZN5draco4MeshC1Ev(%"class.draco::Mesh"* returned) unnamed_addr #2

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.162"* @_ZNSt3__210unique_ptrIN5draco4MeshENS_14default_deleteIS2_EEEC2ILb1EvEEPS2_(%"class.std::__2::unique_ptr.162"* returned %this, %"class.draco::Mesh"* %__p) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.162"*, align 4
  %__p.addr = alloca %"class.draco::Mesh"*, align 4
  %ref.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::unique_ptr.162"* %this, %"class.std::__2::unique_ptr.162"** %this.addr, align 4
  store %"class.draco::Mesh"* %__p, %"class.draco::Mesh"** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.162"*, %"class.std::__2::unique_ptr.162"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.162", %"class.std::__2::unique_ptr.162"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::__compressed_pair.163"* @_ZNSt3__217__compressed_pairIPN5draco4MeshENS_14default_deleteIS2_EEEC2IRS3_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.163"* %__ptr_, %"class.draco::Mesh"** nonnull align 4 dereferenceable(4) %__p.addr, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp)
  ret %"class.std::__2::unique_ptr.162"* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN5draco7Decoder22DecodeBufferToGeometryEPNS_13DecoderBufferEPNS_4MeshE(%"class.draco::Status"* noalias sret align 4 %agg.result, %"class.draco::Decoder"* %this, %"class.draco::DecoderBuffer"* %in_buffer, %"class.draco::Mesh"* %out_geometry) #0 {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %"class.draco::Decoder"*, align 4
  %in_buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %out_geometry.addr = alloca %"class.draco::Mesh"*, align 4
  %temp_buffer = alloca %"class.draco::DecoderBuffer", align 8
  %header = alloca %"struct.draco::DracoHeader", align 2
  %nrvo = alloca i1, align 1
  %cleanup.dest.slot = alloca i32, align 4
  %ref.tmp = alloca %"class.std::__2::basic_string", align 4
  %_statusor119 = alloca %"class.draco::StatusOr", align 4
  %nrvo10 = alloca i1, align 1
  %decoder = alloca %"class.std::__2::unique_ptr", align 4
  %nrvo23 = alloca i1, align 1
  %0 = bitcast %"class.draco::Status"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %"class.draco::Decoder"* %this, %"class.draco::Decoder"** %this.addr, align 4
  store %"class.draco::DecoderBuffer"* %in_buffer, %"class.draco::DecoderBuffer"** %in_buffer.addr, align 4
  store %"class.draco::Mesh"* %out_geometry, %"class.draco::Mesh"** %out_geometry.addr, align 4
  %this1 = load %"class.draco::Decoder"*, %"class.draco::Decoder"** %this.addr, align 4
  %1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %in_buffer.addr, align 4
  %2 = bitcast %"class.draco::DecoderBuffer"* %temp_buffer to i8*
  %3 = bitcast %"class.draco::DecoderBuffer"* %1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %2, i8* align 8 %3, i32 40, i1 false)
  store i1 false, i1* %nrvo, align 1
  call void @_ZN5draco17PointCloudDecoder12DecodeHeaderEPNS_13DecoderBufferEPNS_11DracoHeaderE(%"class.draco::Status"* sret align 4 %agg.result, %"class.draco::DecoderBuffer"* %temp_buffer, %"struct.draco::DracoHeader"* %header)
  %call = call zeroext i1 @_ZNK5draco6Status2okEv(%"class.draco::Status"* %agg.result)
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i1 true, i1* %nrvo, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then
  %nrvo.val = load i1, i1* %nrvo, align 1
  br i1 %nrvo.val, label %nrvo.skipdtor, label %nrvo.unused

nrvo.unused:                                      ; preds = %cleanup
  %call2 = call %"class.draco::Status"* @_ZN5draco6StatusD2Ev(%"class.draco::Status"* %agg.result) #9
  br label %nrvo.skipdtor

nrvo.skipdtor:                                    ; preds = %nrvo.unused, %cleanup
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup39 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %nrvo.skipdtor
  %encoder_type = getelementptr inbounds %"struct.draco::DracoHeader", %"struct.draco::DracoHeader"* %header, i32 0, i32 3
  %4 = load i8, i8* %encoder_type, align 1
  %conv = zext i8 %4 to i32
  %cmp = icmp ne i32 %conv, 1
  br i1 %cmp, label %if.then3, label %if.end7

if.then3:                                         ; preds = %cleanup.cont
  %call4 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2IDnEEPKc(%"class.std::__2::basic_string"* %ref.tmp, i8* getelementptr inbounds ([21 x i8], [21 x i8]* @.str.2, i32 0, i32 0))
  %call5 = call %"class.draco::Status"* @_ZN5draco6StatusC2ENS0_4CodeERKNSt3__212basic_stringIcNS2_11char_traitsIcEENS2_9allocatorIcEEEE(%"class.draco::Status"* %agg.result, i32 -1, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %ref.tmp)
  %call6 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_string"* %ref.tmp) #9
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup39

if.end7:                                          ; preds = %cleanup.cont
  %encoder_method = getelementptr inbounds %"struct.draco::DracoHeader", %"struct.draco::DracoHeader"* %header, i32 0, i32 4
  %5 = load i8, i8* %encoder_method, align 2
  call void @_ZN5draco17CreateMeshDecoderEh(%"class.draco::StatusOr"* sret align 4 %_statusor119, i8 zeroext %5)
  %call8 = call zeroext i1 @_ZNK5draco8StatusOrINSt3__210unique_ptrINS_11MeshDecoderENS1_14default_deleteIS3_EEEEE2okEv(%"class.draco::StatusOr"* %_statusor119)
  br i1 %call8, label %if.end19, label %if.then9

if.then9:                                         ; preds = %if.end7
  store i1 false, i1* %nrvo10, align 1
  %call11 = call nonnull align 4 dereferenceable(16) %"class.draco::Status"* @_ZNK5draco8StatusOrINSt3__210unique_ptrINS_11MeshDecoderENS1_14default_deleteIS3_EEEEE6statusEv(%"class.draco::StatusOr"* %_statusor119)
  %call12 = call nonnull align 4 dereferenceable(16) %"class.draco::Status"* @_ZNSt3__24moveIRKN5draco6StatusEEEONS_16remove_referenceIT_E4typeEOS6_(%"class.draco::Status"* nonnull align 4 dereferenceable(16) %call11) #9
  %call13 = call %"class.draco::Status"* @_ZN5draco6StatusC2ERKS0_(%"class.draco::Status"* %agg.result, %"class.draco::Status"* nonnull align 4 dereferenceable(16) %call12)
  store i1 true, i1* %nrvo10, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  %nrvo.val15 = load i1, i1* %nrvo10, align 1
  br i1 %nrvo.val15, label %nrvo.skipdtor18, label %nrvo.unused16

nrvo.unused16:                                    ; preds = %if.then9
  %call17 = call %"class.draco::Status"* @_ZN5draco6StatusD2Ev(%"class.draco::Status"* %agg.result) #9
  br label %nrvo.skipdtor18

nrvo.skipdtor18:                                  ; preds = %nrvo.unused16, %if.then9
  br label %cleanup37

if.end19:                                         ; preds = %if.end7
  %call20 = call nonnull align 4 dereferenceable(20) %"class.draco::StatusOr"* @_ZNSt3__24moveIRN5draco8StatusOrINS_10unique_ptrINS1_11MeshDecoderENS_14default_deleteIS4_EEEEEEEEONS_16remove_referenceIT_E4typeEOSB_(%"class.draco::StatusOr"* nonnull align 4 dereferenceable(20) %_statusor119) #9
  %call21 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr"* @_ZNO5draco8StatusOrINSt3__210unique_ptrINS_11MeshDecoderENS1_14default_deleteIS3_EEEEE5valueEv(%"class.draco::StatusOr"* %call20)
  %call22 = call %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrIN5draco11MeshDecoderENS_14default_deleteIS2_EEEC2EOS5_(%"class.std::__2::unique_ptr"* %decoder, %"class.std::__2::unique_ptr"* nonnull align 4 dereferenceable(4) %call21) #9
  store i1 false, i1* %nrvo23, align 1
  %call24 = call %"class.draco::MeshDecoder"* @_ZNKSt3__210unique_ptrIN5draco11MeshDecoderENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr"* %decoder) #9
  %options_ = getelementptr inbounds %"class.draco::Decoder", %"class.draco::Decoder"* %this1, i32 0, i32 0
  %6 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %in_buffer.addr, align 4
  %7 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %out_geometry.addr, align 4
  call void @_ZN5draco11MeshDecoder6DecodeERKNS_12DracoOptionsINS_17GeometryAttribute4TypeEEEPNS_13DecoderBufferEPNS_4MeshE(%"class.draco::Status"* sret align 4 %agg.result, %"class.draco::MeshDecoder"* %call24, %"class.draco::DracoOptions"* nonnull align 4 dereferenceable(24) %options_, %"class.draco::DecoderBuffer"* %6, %"class.draco::Mesh"* %7)
  %call25 = call zeroext i1 @_ZNK5draco6Status2okEv(%"class.draco::Status"* %agg.result)
  br i1 %call25, label %if.end27, label %if.then26

if.then26:                                        ; preds = %if.end19
  store i1 true, i1* %nrvo23, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup28

if.end27:                                         ; preds = %if.end19
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup28

cleanup28:                                        ; preds = %if.end27, %if.then26
  %nrvo.val29 = load i1, i1* %nrvo23, align 1
  br i1 %nrvo.val29, label %nrvo.skipdtor32, label %nrvo.unused30

nrvo.unused30:                                    ; preds = %cleanup28
  %call31 = call %"class.draco::Status"* @_ZN5draco6StatusD2Ev(%"class.draco::Status"* %agg.result) #9
  br label %nrvo.skipdtor32

nrvo.skipdtor32:                                  ; preds = %nrvo.unused30, %cleanup28
  %cleanup.dest33 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest33, label %cleanup35 [
    i32 0, label %cleanup.cont34
  ]

cleanup.cont34:                                   ; preds = %nrvo.skipdtor32
  call void @_ZN5draco8OkStatusEv(%"class.draco::Status"* sret align 4 %agg.result)
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup35

cleanup35:                                        ; preds = %cleanup.cont34, %nrvo.skipdtor32
  %call36 = call %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrIN5draco11MeshDecoderENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr"* %decoder) #9
  br label %cleanup37

cleanup37:                                        ; preds = %cleanup35, %nrvo.skipdtor18
  %call38 = call %"class.draco::StatusOr"* @_ZN5draco8StatusOrINSt3__210unique_ptrINS_11MeshDecoderENS1_14default_deleteIS3_EEEEED2Ev(%"class.draco::StatusOr"* %_statusor119) #9
  br label %cleanup39

cleanup39:                                        ; preds = %cleanup37, %if.then3, %nrvo.skipdtor
  %call40 = call %"class.draco::DecoderBuffer"* @_ZN5draco13DecoderBufferD2Ev(%"class.draco::DecoderBuffer"* %temp_buffer) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::Mesh"* @_ZNKSt3__210unique_ptrIN5draco4MeshENS_14default_deleteIS2_EEE3getEv(%"class.std::__2::unique_ptr.162"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.162"*, align 4
  store %"class.std::__2::unique_ptr.162"* %this, %"class.std::__2::unique_ptr.162"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.162"*, %"class.std::__2::unique_ptr.162"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.162", %"class.std::__2::unique_ptr.162"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::Mesh"** @_ZNKSt3__217__compressed_pairIPN5draco4MeshENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.163"* %__ptr_) #9
  %0 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %call, align 4
  ret %"class.draco::Mesh"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.162"* @_ZNSt3__24moveIRNS_10unique_ptrIN5draco4MeshENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOS9_(%"class.std::__2::unique_ptr.162"* nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::unique_ptr.162"*, align 4
  store %"class.std::__2::unique_ptr.162"* %__t, %"class.std::__2::unique_ptr.162"** %__t.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.162"*, %"class.std::__2::unique_ptr.162"** %__t.addr, align 4
  ret %"class.std::__2::unique_ptr.162"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.157"* @_ZNSt3__210unique_ptrIN5draco10PointCloudENS_14default_deleteIS2_EEEC2INS1_4MeshENS3_IS7_EEvvEEONS0_IT_T0_EE(%"class.std::__2::unique_ptr.157"* returned %this, %"class.std::__2::unique_ptr.162"* nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.157"*, align 4
  %__u.addr = alloca %"class.std::__2::unique_ptr.162"*, align 4
  %ref.tmp = alloca %"class.draco::Mesh"*, align 4
  store %"class.std::__2::unique_ptr.157"* %this, %"class.std::__2::unique_ptr.157"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.162"* %__u, %"class.std::__2::unique_ptr.162"** %__u.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.157"*, %"class.std::__2::unique_ptr.157"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.157", %"class.std::__2::unique_ptr.157"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::unique_ptr.162"*, %"class.std::__2::unique_ptr.162"** %__u.addr, align 4
  %call = call %"class.draco::Mesh"* @_ZNSt3__210unique_ptrIN5draco4MeshENS_14default_deleteIS2_EEE7releaseEv(%"class.std::__2::unique_ptr.162"* %0) #9
  store %"class.draco::Mesh"* %call, %"class.draco::Mesh"** %ref.tmp, align 4
  %1 = load %"class.std::__2::unique_ptr.162"*, %"class.std::__2::unique_ptr.162"** %__u.addr, align 4
  %call2 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.166"* @_ZNSt3__210unique_ptrIN5draco4MeshENS_14default_deleteIS2_EEE11get_deleterEv(%"class.std::__2::unique_ptr.162"* %1) #9
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.166"* @_ZNSt3__27forwardINS_14default_deleteIN5draco4MeshEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.166"* nonnull align 1 dereferenceable(1) %call2) #9
  %call4 = call %"class.std::__2::__compressed_pair.158"* @_ZNSt3__217__compressed_pairIPN5draco10PointCloudENS_14default_deleteIS2_EEEC2IPNS1_4MeshENS4_IS8_EEEEOT_OT0_(%"class.std::__2::__compressed_pair.158"* %__ptr_, %"class.draco::Mesh"** nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::default_delete.166"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::unique_ptr.157"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::StatusOr.156"* @_ZN5draco8StatusOrINSt3__210unique_ptrINS_10PointCloudENS1_14default_deleteIS3_EEEEEC2EOS6_(%"class.draco::StatusOr.156"* returned %this, %"class.std::__2::unique_ptr.157"* nonnull align 4 dereferenceable(4) %value) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::StatusOr.156"*, align 4
  %value.addr = alloca %"class.std::__2::unique_ptr.157"*, align 4
  store %"class.draco::StatusOr.156"* %this, %"class.draco::StatusOr.156"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.157"* %value, %"class.std::__2::unique_ptr.157"** %value.addr, align 4
  %this1 = load %"class.draco::StatusOr.156"*, %"class.draco::StatusOr.156"** %this.addr, align 4
  %status_ = getelementptr inbounds %"class.draco::StatusOr.156", %"class.draco::StatusOr.156"* %this1, i32 0, i32 0
  call void @_ZN5draco8OkStatusEv(%"class.draco::Status"* sret align 4 %status_)
  %value_ = getelementptr inbounds %"class.draco::StatusOr.156", %"class.draco::StatusOr.156"* %this1, i32 0, i32 1
  %0 = load %"class.std::__2::unique_ptr.157"*, %"class.std::__2::unique_ptr.157"** %value.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.157"* @_ZNSt3__24moveIRNS_10unique_ptrIN5draco10PointCloudENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOS9_(%"class.std::__2::unique_ptr.157"* nonnull align 4 dereferenceable(4) %0) #9
  %call2 = call %"class.std::__2::unique_ptr.157"* @_ZNSt3__210unique_ptrIN5draco10PointCloudENS_14default_deleteIS2_EEEC2EOS5_(%"class.std::__2::unique_ptr.157"* %value_, %"class.std::__2::unique_ptr.157"* nonnull align 4 dereferenceable(4) %call) #9
  ret %"class.draco::StatusOr.156"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.157"* @_ZNSt3__210unique_ptrIN5draco10PointCloudENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.157"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.157"*, align 4
  store %"class.std::__2::unique_ptr.157"* %this, %"class.std::__2::unique_ptr.157"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.157"*, %"class.std::__2::unique_ptr.157"** %this.addr, align 4
  call void @_ZNSt3__210unique_ptrIN5draco10PointCloudENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.157"* %this1, %"class.draco::PointCloud"* null) #9
  ret %"class.std::__2::unique_ptr.157"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.162"* @_ZNSt3__210unique_ptrIN5draco4MeshENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.162"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.162"*, align 4
  store %"class.std::__2::unique_ptr.162"* %this, %"class.std::__2::unique_ptr.162"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.162"*, %"class.std::__2::unique_ptr.162"** %this.addr, align 4
  call void @_ZNSt3__210unique_ptrIN5draco4MeshENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.162"* %this1, %"class.draco::Mesh"* null) #9
  ret %"class.std::__2::unique_ptr.162"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::StatusOr.155"* @_ZN5draco8StatusOrINS_19EncodedGeometryTypeEED2Ev(%"class.draco::StatusOr.155"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::StatusOr.155"*, align 4
  store %"class.draco::StatusOr.155"* %this, %"class.draco::StatusOr.155"** %this.addr, align 4
  %this1 = load %"class.draco::StatusOr.155"*, %"class.draco::StatusOr.155"** %this.addr, align 4
  %status_ = getelementptr inbounds %"class.draco::StatusOr.155", %"class.draco::StatusOr.155"* %this1, i32 0, i32 0
  %call = call %"class.draco::Status"* @_ZN5draco6StatusD2Ev(%"class.draco::Status"* %status_) #9
  ret %"class.draco::StatusOr.155"* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN5draco7Decoder20DecodeMeshFromBufferEPNS_13DecoderBufferE(%"class.draco::StatusOr.167"* noalias sret align 4 %agg.result, %"class.draco::Decoder"* %this, %"class.draco::DecoderBuffer"* %in_buffer) #0 {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %"class.draco::Decoder"*, align 4
  %in_buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %mesh = alloca %"class.std::__2::unique_ptr.162", align 4
  %_local_status = alloca %"class.draco::Status", align 4
  %cleanup.dest.slot = alloca i32, align 4
  %0 = bitcast %"class.draco::StatusOr.167"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %"class.draco::Decoder"* %this, %"class.draco::Decoder"** %this.addr, align 4
  store %"class.draco::DecoderBuffer"* %in_buffer, %"class.draco::DecoderBuffer"** %in_buffer.addr, align 4
  %this1 = load %"class.draco::Decoder"*, %"class.draco::Decoder"** %this.addr, align 4
  %call = call noalias nonnull i8* @_Znwm(i32 108) #8
  %1 = bitcast i8* %call to %"class.draco::Mesh"*
  %call2 = call %"class.draco::Mesh"* @_ZN5draco4MeshC1Ev(%"class.draco::Mesh"* %1)
  %call3 = call %"class.std::__2::unique_ptr.162"* @_ZNSt3__210unique_ptrIN5draco4MeshENS_14default_deleteIS2_EEEC2ILb1EvEEPS2_(%"class.std::__2::unique_ptr.162"* %mesh, %"class.draco::Mesh"* %1) #9
  %2 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %in_buffer.addr, align 4
  %call4 = call %"class.draco::Mesh"* @_ZNKSt3__210unique_ptrIN5draco4MeshENS_14default_deleteIS2_EEE3getEv(%"class.std::__2::unique_ptr.162"* %mesh) #9
  call void @_ZN5draco7Decoder22DecodeBufferToGeometryEPNS_13DecoderBufferEPNS_4MeshE(%"class.draco::Status"* sret align 4 %_local_status, %"class.draco::Decoder"* %this1, %"class.draco::DecoderBuffer"* %2, %"class.draco::Mesh"* %call4)
  %call5 = call zeroext i1 @_ZNK5draco6Status2okEv(%"class.draco::Status"* %_local_status)
  br i1 %call5, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  %call6 = call %"class.draco::StatusOr.167"* @_ZN5draco8StatusOrINSt3__210unique_ptrINS_4MeshENS1_14default_deleteIS3_EEEEEC2ERKNS_6StatusE(%"class.draco::StatusOr.167"* %agg.result, %"class.draco::Status"* nonnull align 4 dereferenceable(16) %_local_status)
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then
  %call7 = call %"class.draco::Status"* @_ZN5draco6StatusD2Ev(%"class.draco::Status"* %_local_status) #9
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup10 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  %call8 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.162"* @_ZNSt3__24moveIRNS_10unique_ptrIN5draco4MeshENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOS9_(%"class.std::__2::unique_ptr.162"* nonnull align 4 dereferenceable(4) %mesh) #9
  %call9 = call %"class.draco::StatusOr.167"* @_ZN5draco8StatusOrINSt3__210unique_ptrINS_4MeshENS1_14default_deleteIS3_EEEEEC2EOS6_(%"class.draco::StatusOr.167"* %agg.result, %"class.std::__2::unique_ptr.162"* nonnull align 4 dereferenceable(4) %call8)
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup10

cleanup10:                                        ; preds = %cleanup.cont, %cleanup
  %call11 = call %"class.std::__2::unique_ptr.162"* @_ZNSt3__210unique_ptrIN5draco4MeshENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.162"* %mesh) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::StatusOr.167"* @_ZN5draco8StatusOrINSt3__210unique_ptrINS_4MeshENS1_14default_deleteIS3_EEEEEC2ERKNS_6StatusE(%"class.draco::StatusOr.167"* returned %this, %"class.draco::Status"* nonnull align 4 dereferenceable(16) %status) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::StatusOr.167"*, align 4
  %status.addr = alloca %"class.draco::Status"*, align 4
  store %"class.draco::StatusOr.167"* %this, %"class.draco::StatusOr.167"** %this.addr, align 4
  store %"class.draco::Status"* %status, %"class.draco::Status"** %status.addr, align 4
  %this1 = load %"class.draco::StatusOr.167"*, %"class.draco::StatusOr.167"** %this.addr, align 4
  %status_ = getelementptr inbounds %"class.draco::StatusOr.167", %"class.draco::StatusOr.167"* %this1, i32 0, i32 0
  %0 = load %"class.draco::Status"*, %"class.draco::Status"** %status.addr, align 4
  %call = call %"class.draco::Status"* @_ZN5draco6StatusC2ERKS0_(%"class.draco::Status"* %status_, %"class.draco::Status"* nonnull align 4 dereferenceable(16) %0)
  %value_ = getelementptr inbounds %"class.draco::StatusOr.167", %"class.draco::StatusOr.167"* %this1, i32 0, i32 1
  %call2 = call %"class.std::__2::unique_ptr.162"* @_ZNSt3__210unique_ptrIN5draco4MeshENS_14default_deleteIS2_EEEC2ILb1EvEEv(%"class.std::__2::unique_ptr.162"* %value_) #9
  ret %"class.draco::StatusOr.167"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::StatusOr.167"* @_ZN5draco8StatusOrINSt3__210unique_ptrINS_4MeshENS1_14default_deleteIS3_EEEEEC2EOS6_(%"class.draco::StatusOr.167"* returned %this, %"class.std::__2::unique_ptr.162"* nonnull align 4 dereferenceable(4) %value) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::StatusOr.167"*, align 4
  %value.addr = alloca %"class.std::__2::unique_ptr.162"*, align 4
  store %"class.draco::StatusOr.167"* %this, %"class.draco::StatusOr.167"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.162"* %value, %"class.std::__2::unique_ptr.162"** %value.addr, align 4
  %this1 = load %"class.draco::StatusOr.167"*, %"class.draco::StatusOr.167"** %this.addr, align 4
  %status_ = getelementptr inbounds %"class.draco::StatusOr.167", %"class.draco::StatusOr.167"* %this1, i32 0, i32 0
  call void @_ZN5draco8OkStatusEv(%"class.draco::Status"* sret align 4 %status_)
  %value_ = getelementptr inbounds %"class.draco::StatusOr.167", %"class.draco::StatusOr.167"* %this1, i32 0, i32 1
  %0 = load %"class.std::__2::unique_ptr.162"*, %"class.std::__2::unique_ptr.162"** %value.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.162"* @_ZNSt3__24moveIRNS_10unique_ptrIN5draco4MeshENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOS9_(%"class.std::__2::unique_ptr.162"* nonnull align 4 dereferenceable(4) %0) #9
  %call2 = call %"class.std::__2::unique_ptr.162"* @_ZNSt3__210unique_ptrIN5draco4MeshENS_14default_deleteIS2_EEEC2EOS5_(%"class.std::__2::unique_ptr.162"* %value_, %"class.std::__2::unique_ptr.162"* nonnull align 4 dereferenceable(4) %call) #9
  ret %"class.draco::StatusOr.167"* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN5draco7Decoder22DecodeBufferToGeometryEPNS_13DecoderBufferEPNS_10PointCloudE(%"class.draco::Status"* noalias sret align 4 %agg.result, %"class.draco::Decoder"* %this, %"class.draco::DecoderBuffer"* %in_buffer, %"class.draco::PointCloud"* %out_geometry) #0 {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %"class.draco::Decoder"*, align 4
  %in_buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %out_geometry.addr = alloca %"class.draco::PointCloud"*, align 4
  %ref.tmp = alloca %"class.std::__2::basic_string", align 4
  %0 = bitcast %"class.draco::Status"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %"class.draco::Decoder"* %this, %"class.draco::Decoder"** %this.addr, align 4
  store %"class.draco::DecoderBuffer"* %in_buffer, %"class.draco::DecoderBuffer"** %in_buffer.addr, align 4
  store %"class.draco::PointCloud"* %out_geometry, %"class.draco::PointCloud"** %out_geometry.addr, align 4
  %this1 = load %"class.draco::Decoder"*, %"class.draco::Decoder"** %this.addr, align 4
  %call = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2IDnEEPKc(%"class.std::__2::basic_string"* %ref.tmp, i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str.1, i32 0, i32 0))
  %call2 = call %"class.draco::Status"* @_ZN5draco6StatusC2ENS0_4CodeERKNSt3__212basic_stringIcNS2_11char_traitsIcEENS2_9allocatorIcEEEE(%"class.draco::Status"* %agg.result, i32 -1, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %ref.tmp)
  %call3 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_string"* %ref.tmp) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK5draco8StatusOrINSt3__210unique_ptrINS_11MeshDecoderENS1_14default_deleteIS3_EEEEE2okEv(%"class.draco::StatusOr"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::StatusOr"*, align 4
  store %"class.draco::StatusOr"* %this, %"class.draco::StatusOr"** %this.addr, align 4
  %this1 = load %"class.draco::StatusOr"*, %"class.draco::StatusOr"** %this.addr, align 4
  %status_ = getelementptr inbounds %"class.draco::StatusOr", %"class.draco::StatusOr"* %this1, i32 0, i32 0
  %call = call zeroext i1 @_ZNK5draco6Status2okEv(%"class.draco::Status"* %status_)
  ret i1 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %"class.draco::Status"* @_ZNK5draco8StatusOrINSt3__210unique_ptrINS_11MeshDecoderENS1_14default_deleteIS3_EEEEE6statusEv(%"class.draco::StatusOr"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::StatusOr"*, align 4
  store %"class.draco::StatusOr"* %this, %"class.draco::StatusOr"** %this.addr, align 4
  %this1 = load %"class.draco::StatusOr"*, %"class.draco::StatusOr"** %this.addr, align 4
  %status_ = getelementptr inbounds %"class.draco::StatusOr", %"class.draco::StatusOr"* %this1, i32 0, i32 0
  ret %"class.draco::Status"* %status_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(20) %"class.draco::StatusOr"* @_ZNSt3__24moveIRN5draco8StatusOrINS_10unique_ptrINS1_11MeshDecoderENS_14default_deleteIS4_EEEEEEEEONS_16remove_referenceIT_E4typeEOSB_(%"class.draco::StatusOr"* nonnull align 4 dereferenceable(20) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.draco::StatusOr"*, align 4
  store %"class.draco::StatusOr"* %__t, %"class.draco::StatusOr"** %__t.addr, align 4
  %0 = load %"class.draco::StatusOr"*, %"class.draco::StatusOr"** %__t.addr, align 4
  ret %"class.draco::StatusOr"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr"* @_ZNO5draco8StatusOrINSt3__210unique_ptrINS_11MeshDecoderENS1_14default_deleteIS3_EEEEE5valueEv(%"class.draco::StatusOr"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::StatusOr"*, align 4
  store %"class.draco::StatusOr"* %this, %"class.draco::StatusOr"** %this.addr, align 4
  %this1 = load %"class.draco::StatusOr"*, %"class.draco::StatusOr"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::StatusOr", %"class.draco::StatusOr"* %this1, i32 0, i32 1
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr"* @_ZNSt3__24moveIRNS_10unique_ptrIN5draco11MeshDecoderENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOS9_(%"class.std::__2::unique_ptr"* nonnull align 4 dereferenceable(4) %value_) #9
  ret %"class.std::__2::unique_ptr"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrIN5draco11MeshDecoderENS_14default_deleteIS2_EEEC2EOS5_(%"class.std::__2::unique_ptr"* returned %this, %"class.std::__2::unique_ptr"* nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  %__u.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  %ref.tmp = alloca %"class.draco::MeshDecoder"*, align 4
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  store %"class.std::__2::unique_ptr"* %__u, %"class.std::__2::unique_ptr"** %__u.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %__u.addr, align 4
  %call = call %"class.draco::MeshDecoder"* @_ZNSt3__210unique_ptrIN5draco11MeshDecoderENS_14default_deleteIS2_EEE7releaseEv(%"class.std::__2::unique_ptr"* %0) #9
  store %"class.draco::MeshDecoder"* %call, %"class.draco::MeshDecoder"** %ref.tmp, align 4
  %1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %__u.addr, align 4
  %call2 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.149"* @_ZNSt3__210unique_ptrIN5draco11MeshDecoderENS_14default_deleteIS2_EEE11get_deleterEv(%"class.std::__2::unique_ptr"* %1) #9
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.149"* @_ZNSt3__27forwardINS_14default_deleteIN5draco11MeshDecoderEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.149"* nonnull align 1 dereferenceable(1) %call2) #9
  %call4 = call %"class.std::__2::__compressed_pair.1"* @_ZNSt3__217__compressed_pairIPN5draco11MeshDecoderENS_14default_deleteIS2_EEEC2IS3_S5_EEOT_OT0_(%"class.std::__2::__compressed_pair.1"* %__ptr_, %"class.draco::MeshDecoder"** nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::default_delete.149"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::unique_ptr"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::MeshDecoder"* @_ZNKSt3__210unique_ptrIN5draco11MeshDecoderENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::MeshDecoder"** @_ZNKSt3__217__compressed_pairIPN5draco11MeshDecoderENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.1"* %__ptr_) #9
  %0 = load %"class.draco::MeshDecoder"*, %"class.draco::MeshDecoder"** %call, align 4
  ret %"class.draco::MeshDecoder"* %0
}

declare void @_ZN5draco11MeshDecoder6DecodeERKNS_12DracoOptionsINS_17GeometryAttribute4TypeEEEPNS_13DecoderBufferEPNS_4MeshE(%"class.draco::Status"* sret align 4, %"class.draco::MeshDecoder"*, %"class.draco::DracoOptions"* nonnull align 4 dereferenceable(24), %"class.draco::DecoderBuffer"*, %"class.draco::Mesh"*) #2

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco8OkStatusEv(%"class.draco::Status"* noalias sret align 4 %agg.result) #0 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %0 = bitcast %"class.draco::Status"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  %call = call %"class.draco::Status"* @_ZN5draco6StatusC2ENS0_4CodeE(%"class.draco::Status"* %agg.result, i32 0)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::StatusOr"* @_ZN5draco8StatusOrINSt3__210unique_ptrINS_11MeshDecoderENS1_14default_deleteIS3_EEEEED2Ev(%"class.draco::StatusOr"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::StatusOr"*, align 4
  store %"class.draco::StatusOr"* %this, %"class.draco::StatusOr"** %this.addr, align 4
  %this1 = load %"class.draco::StatusOr"*, %"class.draco::StatusOr"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::StatusOr", %"class.draco::StatusOr"* %this1, i32 0, i32 1
  %call = call %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrIN5draco11MeshDecoderENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr"* %value_) #9
  %status_ = getelementptr inbounds %"class.draco::StatusOr", %"class.draco::StatusOr"* %this1, i32 0, i32 0
  %call2 = call %"class.draco::Status"* @_ZN5draco6StatusD2Ev(%"class.draco::Status"* %status_) #9
  ret %"class.draco::StatusOr"* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN5draco7Decoder25SetSkipAttributeTransformENS_17GeometryAttribute4TypeE(%"class.draco::Decoder"* %this, i32 %att_type) #0 {
entry:
  %this.addr = alloca %"class.draco::Decoder"*, align 4
  %att_type.addr = alloca i32, align 4
  %ref.tmp = alloca %"class.std::__2::basic_string", align 4
  store %"class.draco::Decoder"* %this, %"class.draco::Decoder"** %this.addr, align 4
  store i32 %att_type, i32* %att_type.addr, align 4
  %this1 = load %"class.draco::Decoder"*, %"class.draco::Decoder"** %this.addr, align 4
  %options_ = getelementptr inbounds %"class.draco::Decoder", %"class.draco::Decoder"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2IDnEEPKc(%"class.std::__2::basic_string"* %ref.tmp, i8* getelementptr inbounds ([25 x i8], [25 x i8]* @.str.3, i32 0, i32 0))
  call void @_ZN5draco12DracoOptionsINS_17GeometryAttribute4TypeEE16SetAttributeBoolERKS2_RKNSt3__212basic_stringIcNS6_11char_traitsIcEENS6_9allocatorIcEEEEb(%"class.draco::DracoOptions"* %options_, i32* nonnull align 4 dereferenceable(4) %att_type.addr, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %ref.tmp, i1 zeroext true)
  %call2 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_string"* %ref.tmp) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco12DracoOptionsINS_17GeometryAttribute4TypeEE16SetAttributeBoolERKS2_RKNSt3__212basic_stringIcNS6_11char_traitsIcEENS6_9allocatorIcEEEEb(%"class.draco::DracoOptions"* %this, i32* nonnull align 4 dereferenceable(4) %att_key, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %name, i1 zeroext %val) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::DracoOptions"*, align 4
  %att_key.addr = alloca i32*, align 4
  %name.addr = alloca %"class.std::__2::basic_string"*, align 4
  %val.addr = alloca i8, align 1
  store %"class.draco::DracoOptions"* %this, %"class.draco::DracoOptions"** %this.addr, align 4
  store i32* %att_key, i32** %att_key.addr, align 4
  store %"class.std::__2::basic_string"* %name, %"class.std::__2::basic_string"** %name.addr, align 4
  %frombool = zext i1 %val to i8
  store i8 %frombool, i8* %val.addr, align 1
  %this1 = load %"class.draco::DracoOptions"*, %"class.draco::DracoOptions"** %this.addr, align 4
  %0 = load i32*, i32** %att_key.addr, align 4
  %call = call %"class.draco::Options"* @_ZN5draco12DracoOptionsINS_17GeometryAttribute4TypeEE19GetAttributeOptionsERKS2_(%"class.draco::DracoOptions"* %this1, i32* nonnull align 4 dereferenceable(4) %0)
  %1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %name.addr, align 4
  %2 = load i8, i8* %val.addr, align 1
  %tobool = trunc i8 %2 to i1
  call void @_ZN5draco7Options7SetBoolERKNSt3__212basic_stringIcNS1_11char_traitsIcEENS1_9allocatorIcEEEEb(%"class.draco::Options"* %call, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %1, i1 zeroext %tobool)
  ret void
}

declare %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKS5_(%"class.std::__2::basic_string"* returned, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12)) unnamed_addr #2

; Function Attrs: nounwind
declare %"class.draco::DecoderBuffer::BitDecoder"* @_ZN5draco13DecoderBuffer10BitDecoderD1Ev(%"class.draco::DecoderBuffer::BitDecoder"* returned) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::Status"* @_ZN5draco6StatusC2ENS0_4CodeE(%"class.draco::Status"* returned %this, i32 %code) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::Status"*, align 4
  %code.addr = alloca i32, align 4
  store %"class.draco::Status"* %this, %"class.draco::Status"** %this.addr, align 4
  store i32 %code, i32* %code.addr, align 4
  %this1 = load %"class.draco::Status"*, %"class.draco::Status"** %this.addr, align 4
  %code_ = getelementptr inbounds %"class.draco::Status", %"class.draco::Status"* %this1, i32 0, i32 0
  %0 = load i32, i32* %code.addr, align 4
  store i32 %0, i32* %code_, align 4
  %error_msg_ = getelementptr inbounds %"class.draco::Status", %"class.draco::Status"* %this1, i32 0, i32 1
  %call = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2Ev(%"class.std::__2::basic_string"* %error_msg_) #9
  ret %"class.draco::Status"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2Ev(%"class.std::__2::basic_string"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  %ref.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  %ref.tmp2 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::basic_string"* %this1 to %"class.std::__2::__basic_string_common"*
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::__compressed_pair"* @_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_EC2INS_18__default_init_tagESA_EEOT_OT0_(%"class.std::__2::__compressed_pair"* %__r_, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  call void @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__zeroEv(%"class.std::__2::basic_string"* %this1) #9
  ret %"class.std::__2::basic_string"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair"* @_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_EC2INS_18__default_init_tagESA_EEOT_OT0_(%"class.std::__2::__compressed_pair"* returned %this, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  %__t1.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  %agg.tmp3 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t1, %"struct.std::__2::__default_init_tag"** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %1 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t1.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %1) #9
  %call2 = call %"struct.std::__2::__compressed_pair_elem"* @_ZNSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem"* %0)
  %2 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem.0"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call4 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #9
  %call5 = call %"struct.std::__2::__compressed_pair_elem.0"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIcEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.0"* %2)
  ret %"class.std::__2::__compressed_pair"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__zeroEv(%"class.std::__2::basic_string"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  %__a = alloca [3 x i32]*, align 4
  %__i = alloca i32, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair"* %__r_) #9
  %0 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call, i32 0, i32 0
  %__r = bitcast %union.anon* %0 to %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__raw"*
  %__words = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__raw", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__raw"* %__r, i32 0, i32 0
  store [3 x i32]* %__words, [3 x i32]** %__a, align 4
  store i32 0, i32* %__i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %__i, align 4
  %cmp = icmp ult i32 %1, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load [3 x i32]*, [3 x i32]** %__a, align 4
  %3 = load i32, i32* %__i, align 4
  %arrayidx = getelementptr inbounds [3 x i32], [3 x i32]* %2, i32 0, i32 %3
  store i32 0, i32* %arrayidx, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %4 = load i32, i32* %__i, align 4
  %inc = add i32 %4, 1
  store i32 %inc, i32* %__i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  store %"struct.std::__2::__default_init_tag"* %__t, %"struct.std::__2::__default_init_tag"** %__t.addr, align 4
  %0 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t.addr, align 4
  ret %"struct.std::__2::__default_init_tag"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem"* @_ZNSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem"*, align 4
  store %"struct.std::__2::__compressed_pair_elem"* %this, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem"*, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem"* %this1, i32 0, i32 0
  ret %"struct.std::__2::__compressed_pair_elem"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.0"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIcEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.0"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.0"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.0"* %this, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.0"*, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.0"* %this1 to %"class.std::__2::allocator"*
  %call = call %"class.std::__2::allocator"* @_ZNSt3__29allocatorIcEC2Ev(%"class.std::__2::allocator"* %1) #9
  ret %"struct.std::__2::__compressed_pair_elem.0"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::allocator"* @_ZNSt3__29allocatorIcEC2Ev(%"class.std::__2::allocator"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  ret %"class.std::__2::allocator"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %0) #9
  ret %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem"*, align 4
  store %"struct.std::__2::__compressed_pair_elem"* %this, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem"*, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem"* %this1, i32 0, i32 0
  ret %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.1"* @_ZNSt3__217__compressed_pairIPN5draco11MeshDecoderENS_14default_deleteIS2_EEEC2IRS3_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.1"* returned %this, %"class.draco::MeshDecoder"** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.1"*, align 4
  %__t1.addr = alloca %"class.draco::MeshDecoder"**, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.1"* %this, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  store %"class.draco::MeshDecoder"** %__t1, %"class.draco::MeshDecoder"*** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.1"*, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.1"* %this1 to %"struct.std::__2::__compressed_pair_elem.2"*
  %1 = load %"class.draco::MeshDecoder"**, %"class.draco::MeshDecoder"*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::MeshDecoder"** @_ZNSt3__27forwardIRPN5draco11MeshDecoderEEEOT_RNS_16remove_referenceIS5_E4typeE(%"class.draco::MeshDecoder"** nonnull align 4 dereferenceable(4) %1) #9
  %call2 = call %"struct.std::__2::__compressed_pair_elem.2"* @_ZNSt3__222__compressed_pair_elemIPN5draco11MeshDecoderELi0ELb0EEC2IRS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.2"* %0, %"class.draco::MeshDecoder"** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.1"* %this1 to %"struct.std::__2::__compressed_pair_elem.148"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #9
  %call4 = call %"struct.std::__2::__compressed_pair_elem.148"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco11MeshDecoderEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.148"* %2)
  ret %"class.std::__2::__compressed_pair.1"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::MeshDecoder"** @_ZNSt3__27forwardIRPN5draco11MeshDecoderEEEOT_RNS_16remove_referenceIS5_E4typeE(%"class.draco::MeshDecoder"** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.draco::MeshDecoder"**, align 4
  store %"class.draco::MeshDecoder"** %__t, %"class.draco::MeshDecoder"*** %__t.addr, align 4
  %0 = load %"class.draco::MeshDecoder"**, %"class.draco::MeshDecoder"*** %__t.addr, align 4
  ret %"class.draco::MeshDecoder"** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.2"* @_ZNSt3__222__compressed_pair_elemIPN5draco11MeshDecoderELi0ELb0EEC2IRS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.2"* returned %this, %"class.draco::MeshDecoder"** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.2"*, align 4
  %__u.addr = alloca %"class.draco::MeshDecoder"**, align 4
  store %"struct.std::__2::__compressed_pair_elem.2"* %this, %"struct.std::__2::__compressed_pair_elem.2"** %this.addr, align 4
  store %"class.draco::MeshDecoder"** %__u, %"class.draco::MeshDecoder"*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.2"*, %"struct.std::__2::__compressed_pair_elem.2"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.2", %"struct.std::__2::__compressed_pair_elem.2"* %this1, i32 0, i32 0
  %0 = load %"class.draco::MeshDecoder"**, %"class.draco::MeshDecoder"*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::MeshDecoder"** @_ZNSt3__27forwardIRPN5draco11MeshDecoderEEEOT_RNS_16remove_referenceIS5_E4typeE(%"class.draco::MeshDecoder"** nonnull align 4 dereferenceable(4) %0) #9
  %1 = load %"class.draco::MeshDecoder"*, %"class.draco::MeshDecoder"** %call, align 4
  store %"class.draco::MeshDecoder"* %1, %"class.draco::MeshDecoder"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.2"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.148"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco11MeshDecoderEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.148"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.148"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.148"* %this, %"struct.std::__2::__compressed_pair_elem.148"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.148"*, %"struct.std::__2::__compressed_pair_elem.148"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.148"* %this1 to %"struct.std::__2::default_delete.149"*
  ret %"struct.std::__2::__compressed_pair_elem.148"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__210unique_ptrIN5draco11MeshDecoderENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr"* %this, %"class.draco::MeshDecoder"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  %__p.addr = alloca %"class.draco::MeshDecoder"*, align 4
  %__tmp = alloca %"class.draco::MeshDecoder"*, align 4
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  store %"class.draco::MeshDecoder"* %__p, %"class.draco::MeshDecoder"** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::MeshDecoder"** @_ZNSt3__217__compressed_pairIPN5draco11MeshDecoderENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.1"* %__ptr_) #9
  %0 = load %"class.draco::MeshDecoder"*, %"class.draco::MeshDecoder"** %call, align 4
  store %"class.draco::MeshDecoder"* %0, %"class.draco::MeshDecoder"** %__tmp, align 4
  %1 = load %"class.draco::MeshDecoder"*, %"class.draco::MeshDecoder"** %__p.addr, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::MeshDecoder"** @_ZNSt3__217__compressed_pairIPN5draco11MeshDecoderENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.1"* %__ptr_2) #9
  store %"class.draco::MeshDecoder"* %1, %"class.draco::MeshDecoder"** %call3, align 4
  %2 = load %"class.draco::MeshDecoder"*, %"class.draco::MeshDecoder"** %__tmp, align 4
  %tobool = icmp ne %"class.draco::MeshDecoder"* %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__ptr_4 = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call5 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.149"* @_ZNSt3__217__compressed_pairIPN5draco11MeshDecoderENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.1"* %__ptr_4) #9
  %3 = load %"class.draco::MeshDecoder"*, %"class.draco::MeshDecoder"** %__tmp, align 4
  call void @_ZNKSt3__214default_deleteIN5draco11MeshDecoderEEclEPS2_(%"struct.std::__2::default_delete.149"* %call5, %"class.draco::MeshDecoder"* %3) #9
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::MeshDecoder"** @_ZNSt3__217__compressed_pairIPN5draco11MeshDecoderENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.1"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.1"*, align 4
  store %"class.std::__2::__compressed_pair.1"* %this, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.1"*, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.1"* %this1 to %"struct.std::__2::__compressed_pair_elem.2"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::MeshDecoder"** @_ZNSt3__222__compressed_pair_elemIPN5draco11MeshDecoderELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.2"* %0) #9
  ret %"class.draco::MeshDecoder"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.149"* @_ZNSt3__217__compressed_pairIPN5draco11MeshDecoderENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.1"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.1"*, align 4
  store %"class.std::__2::__compressed_pair.1"* %this, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.1"*, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.1"* %this1 to %"struct.std::__2::__compressed_pair_elem.148"*
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.149"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco11MeshDecoderEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.148"* %0) #9
  ret %"struct.std::__2::default_delete.149"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__214default_deleteIN5draco11MeshDecoderEEclEPS2_(%"struct.std::__2::default_delete.149"* %this, %"class.draco::MeshDecoder"* %__ptr) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::default_delete.149"*, align 4
  %__ptr.addr = alloca %"class.draco::MeshDecoder"*, align 4
  store %"struct.std::__2::default_delete.149"* %this, %"struct.std::__2::default_delete.149"** %this.addr, align 4
  store %"class.draco::MeshDecoder"* %__ptr, %"class.draco::MeshDecoder"** %__ptr.addr, align 4
  %this1 = load %"struct.std::__2::default_delete.149"*, %"struct.std::__2::default_delete.149"** %this.addr, align 4
  %0 = load %"class.draco::MeshDecoder"*, %"class.draco::MeshDecoder"** %__ptr.addr, align 4
  %isnull = icmp eq %"class.draco::MeshDecoder"* %0, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %entry
  %1 = bitcast %"class.draco::MeshDecoder"* %0 to void (%"class.draco::MeshDecoder"*)***
  %vtable = load void (%"class.draco::MeshDecoder"*)**, void (%"class.draco::MeshDecoder"*)*** %1, align 4
  %vfn = getelementptr inbounds void (%"class.draco::MeshDecoder"*)*, void (%"class.draco::MeshDecoder"*)** %vtable, i64 1
  %2 = load void (%"class.draco::MeshDecoder"*)*, void (%"class.draco::MeshDecoder"*)** %vfn, align 4
  call void %2(%"class.draco::MeshDecoder"* %0) #9
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::MeshDecoder"** @_ZNSt3__222__compressed_pair_elemIPN5draco11MeshDecoderELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.2"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.2"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.2"* %this, %"struct.std::__2::__compressed_pair_elem.2"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.2"*, %"struct.std::__2::__compressed_pair_elem.2"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.2", %"struct.std::__2::__compressed_pair_elem.2"* %this1, i32 0, i32 0
  ret %"class.draco::MeshDecoder"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.149"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco11MeshDecoderEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.148"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.148"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.148"* %this, %"struct.std::__2::__compressed_pair_elem.148"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.148"*, %"struct.std::__2::__compressed_pair_elem.148"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.148"* %this1 to %"struct.std::__2::default_delete.149"*
  ret %"struct.std::__2::default_delete.149"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr"* @_ZNSt3__24moveIRNS_10unique_ptrIN5draco11MeshDecoderENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOS9_(%"class.std::__2::unique_ptr"* nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  store %"class.std::__2::unique_ptr"* %__t, %"class.std::__2::unique_ptr"** %__t.addr, align 4
  %0 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %__t.addr, align 4
  ret %"class.std::__2::unique_ptr"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::MeshDecoder"* @_ZNSt3__210unique_ptrIN5draco11MeshDecoderENS_14default_deleteIS2_EEE7releaseEv(%"class.std::__2::unique_ptr"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  %__t = alloca %"class.draco::MeshDecoder"*, align 4
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::MeshDecoder"** @_ZNSt3__217__compressed_pairIPN5draco11MeshDecoderENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.1"* %__ptr_) #9
  %0 = load %"class.draco::MeshDecoder"*, %"class.draco::MeshDecoder"** %call, align 4
  store %"class.draco::MeshDecoder"* %0, %"class.draco::MeshDecoder"** %__t, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::MeshDecoder"** @_ZNSt3__217__compressed_pairIPN5draco11MeshDecoderENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.1"* %__ptr_2) #9
  store %"class.draco::MeshDecoder"* null, %"class.draco::MeshDecoder"** %call3, align 4
  %1 = load %"class.draco::MeshDecoder"*, %"class.draco::MeshDecoder"** %__t, align 4
  ret %"class.draco::MeshDecoder"* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.149"* @_ZNSt3__27forwardINS_14default_deleteIN5draco11MeshDecoderEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.149"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"struct.std::__2::default_delete.149"*, align 4
  store %"struct.std::__2::default_delete.149"* %__t, %"struct.std::__2::default_delete.149"** %__t.addr, align 4
  %0 = load %"struct.std::__2::default_delete.149"*, %"struct.std::__2::default_delete.149"** %__t.addr, align 4
  ret %"struct.std::__2::default_delete.149"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.149"* @_ZNSt3__210unique_ptrIN5draco11MeshDecoderENS_14default_deleteIS2_EEE11get_deleterEv(%"class.std::__2::unique_ptr"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.149"* @_ZNSt3__217__compressed_pairIPN5draco11MeshDecoderENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.1"* %__ptr_) #9
  ret %"struct.std::__2::default_delete.149"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.1"* @_ZNSt3__217__compressed_pairIPN5draco11MeshDecoderENS_14default_deleteIS2_EEEC2IS3_S5_EEOT_OT0_(%"class.std::__2::__compressed_pair.1"* returned %this, %"class.draco::MeshDecoder"** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::default_delete.149"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.1"*, align 4
  %__t1.addr = alloca %"class.draco::MeshDecoder"**, align 4
  %__t2.addr = alloca %"struct.std::__2::default_delete.149"*, align 4
  store %"class.std::__2::__compressed_pair.1"* %this, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  store %"class.draco::MeshDecoder"** %__t1, %"class.draco::MeshDecoder"*** %__t1.addr, align 4
  store %"struct.std::__2::default_delete.149"* %__t2, %"struct.std::__2::default_delete.149"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.1"*, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.1"* %this1 to %"struct.std::__2::__compressed_pair_elem.2"*
  %1 = load %"class.draco::MeshDecoder"**, %"class.draco::MeshDecoder"*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::MeshDecoder"** @_ZNSt3__27forwardIPN5draco11MeshDecoderEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::MeshDecoder"** nonnull align 4 dereferenceable(4) %1) #9
  %call2 = call %"struct.std::__2::__compressed_pair_elem.2"* @_ZNSt3__222__compressed_pair_elemIPN5draco11MeshDecoderELi0ELb0EEC2IS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.2"* %0, %"class.draco::MeshDecoder"** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.1"* %this1 to %"struct.std::__2::__compressed_pair_elem.148"*
  %3 = load %"struct.std::__2::default_delete.149"*, %"struct.std::__2::default_delete.149"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.149"* @_ZNSt3__27forwardINS_14default_deleteIN5draco11MeshDecoderEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.149"* nonnull align 1 dereferenceable(1) %3) #9
  %call4 = call %"struct.std::__2::__compressed_pair_elem.148"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco11MeshDecoderEEELi1ELb1EEC2IS4_vEEOT_(%"struct.std::__2::__compressed_pair_elem.148"* %2, %"struct.std::__2::default_delete.149"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::__compressed_pair.1"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::MeshDecoder"** @_ZNSt3__27forwardIPN5draco11MeshDecoderEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::MeshDecoder"** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.draco::MeshDecoder"**, align 4
  store %"class.draco::MeshDecoder"** %__t, %"class.draco::MeshDecoder"*** %__t.addr, align 4
  %0 = load %"class.draco::MeshDecoder"**, %"class.draco::MeshDecoder"*** %__t.addr, align 4
  ret %"class.draco::MeshDecoder"** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.2"* @_ZNSt3__222__compressed_pair_elemIPN5draco11MeshDecoderELi0ELb0EEC2IS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.2"* returned %this, %"class.draco::MeshDecoder"** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.2"*, align 4
  %__u.addr = alloca %"class.draco::MeshDecoder"**, align 4
  store %"struct.std::__2::__compressed_pair_elem.2"* %this, %"struct.std::__2::__compressed_pair_elem.2"** %this.addr, align 4
  store %"class.draco::MeshDecoder"** %__u, %"class.draco::MeshDecoder"*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.2"*, %"struct.std::__2::__compressed_pair_elem.2"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.2", %"struct.std::__2::__compressed_pair_elem.2"* %this1, i32 0, i32 0
  %0 = load %"class.draco::MeshDecoder"**, %"class.draco::MeshDecoder"*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::MeshDecoder"** @_ZNSt3__27forwardIPN5draco11MeshDecoderEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::MeshDecoder"** nonnull align 4 dereferenceable(4) %0) #9
  %1 = load %"class.draco::MeshDecoder"*, %"class.draco::MeshDecoder"** %call, align 4
  store %"class.draco::MeshDecoder"* %1, %"class.draco::MeshDecoder"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.2"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.148"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco11MeshDecoderEEELi1ELb1EEC2IS4_vEEOT_(%"struct.std::__2::__compressed_pair_elem.148"* returned %this, %"struct.std::__2::default_delete.149"* nonnull align 1 dereferenceable(1) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.148"*, align 4
  %__u.addr = alloca %"struct.std::__2::default_delete.149"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.148"* %this, %"struct.std::__2::__compressed_pair_elem.148"** %this.addr, align 4
  store %"struct.std::__2::default_delete.149"* %__u, %"struct.std::__2::default_delete.149"** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.148"*, %"struct.std::__2::__compressed_pair_elem.148"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.148"* %this1 to %"struct.std::__2::default_delete.149"*
  %1 = load %"struct.std::__2::default_delete.149"*, %"struct.std::__2::default_delete.149"** %__u.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.149"* @_ZNSt3__27forwardINS_14default_deleteIN5draco11MeshDecoderEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.149"* nonnull align 1 dereferenceable(1) %1) #9
  ret %"struct.std::__2::__compressed_pair_elem.148"* %this1
}

declare void @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEPKcm(%"class.std::__2::basic_string"*, i8*, i32) #2

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__211char_traitsIcE6lengthEPKc(i8* %__s) #0 comdat {
entry:
  %__s.addr = alloca i8*, align 4
  store i8* %__s, i8** %__s.addr, align 4
  %0 = load i8*, i8** %__s.addr, align 4
  %call = call i32 @strlen(i8* %0) #9
  ret i32 %call
}

; Function Attrs: nounwind
declare i32 @strlen(i8*) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrIN5draco11MeshDecoderENS_14default_deleteIS2_EEEC2ILb1EvEEv(%"class.std::__2::unique_ptr"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  %ref.tmp = alloca %"class.draco::MeshDecoder"*, align 4
  %ref.tmp2 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  store %"class.draco::MeshDecoder"* null, %"class.draco::MeshDecoder"** %ref.tmp, align 4
  %call = call %"class.std::__2::__compressed_pair.1"* @_ZNSt3__217__compressed_pairIPN5draco11MeshDecoderENS_14default_deleteIS2_EEEC2IS3_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.1"* %__ptr_, %"class.draco::MeshDecoder"** nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  ret %"class.std::__2::unique_ptr"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.1"* @_ZNSt3__217__compressed_pairIPN5draco11MeshDecoderENS_14default_deleteIS2_EEEC2IS3_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.1"* returned %this, %"class.draco::MeshDecoder"** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.1"*, align 4
  %__t1.addr = alloca %"class.draco::MeshDecoder"**, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.1"* %this, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  store %"class.draco::MeshDecoder"** %__t1, %"class.draco::MeshDecoder"*** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.1"*, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.1"* %this1 to %"struct.std::__2::__compressed_pair_elem.2"*
  %1 = load %"class.draco::MeshDecoder"**, %"class.draco::MeshDecoder"*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::MeshDecoder"** @_ZNSt3__27forwardIPN5draco11MeshDecoderEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::MeshDecoder"** nonnull align 4 dereferenceable(4) %1) #9
  %call2 = call %"struct.std::__2::__compressed_pair_elem.2"* @_ZNSt3__222__compressed_pair_elemIPN5draco11MeshDecoderELi0ELb0EEC2IS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.2"* %0, %"class.draco::MeshDecoder"** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.1"* %this1 to %"struct.std::__2::__compressed_pair_elem.148"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #9
  %call4 = call %"struct.std::__2::__compressed_pair_elem.148"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco11MeshDecoderEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.148"* %2)
  ret %"class.std::__2::__compressed_pair.1"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__24moveIRN5draco19EncodedGeometryTypeEEEONS_16remove_referenceIT_E4typeEOS5_(i32* nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca i32*, align 4
  store i32* %__t, i32** %__t.addr, align 4
  %0 = load i32*, i32** %__t.addr, align 4
  ret i32* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.157"* @_ZNSt3__210unique_ptrIN5draco10PointCloudENS_14default_deleteIS2_EEEC2ILb1EvEEv(%"class.std::__2::unique_ptr.157"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.157"*, align 4
  %ref.tmp = alloca %"class.draco::PointCloud"*, align 4
  %ref.tmp2 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::unique_ptr.157"* %this, %"class.std::__2::unique_ptr.157"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.157"*, %"class.std::__2::unique_ptr.157"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.157", %"class.std::__2::unique_ptr.157"* %this1, i32 0, i32 0
  store %"class.draco::PointCloud"* null, %"class.draco::PointCloud"** %ref.tmp, align 4
  %call = call %"class.std::__2::__compressed_pair.158"* @_ZNSt3__217__compressed_pairIPN5draco10PointCloudENS_14default_deleteIS2_EEEC2IS3_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.158"* %__ptr_, %"class.draco::PointCloud"** nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  ret %"class.std::__2::unique_ptr.157"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.158"* @_ZNSt3__217__compressed_pairIPN5draco10PointCloudENS_14default_deleteIS2_EEEC2IS3_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.158"* returned %this, %"class.draco::PointCloud"** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.158"*, align 4
  %__t1.addr = alloca %"class.draco::PointCloud"**, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.158"* %this, %"class.std::__2::__compressed_pair.158"** %this.addr, align 4
  store %"class.draco::PointCloud"** %__t1, %"class.draco::PointCloud"*** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.158"*, %"class.std::__2::__compressed_pair.158"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.158"* %this1 to %"struct.std::__2::__compressed_pair_elem.159"*
  %1 = load %"class.draco::PointCloud"**, %"class.draco::PointCloud"*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointCloud"** @_ZNSt3__27forwardIPN5draco10PointCloudEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::PointCloud"** nonnull align 4 dereferenceable(4) %1) #9
  %call2 = call %"struct.std::__2::__compressed_pair_elem.159"* @_ZNSt3__222__compressed_pair_elemIPN5draco10PointCloudELi0ELb0EEC2IS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.159"* %0, %"class.draco::PointCloud"** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.158"* %this1 to %"struct.std::__2::__compressed_pair_elem.160"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #9
  %call4 = call %"struct.std::__2::__compressed_pair_elem.160"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco10PointCloudEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.160"* %2)
  ret %"class.std::__2::__compressed_pair.158"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::PointCloud"** @_ZNSt3__27forwardIPN5draco10PointCloudEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::PointCloud"** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.draco::PointCloud"**, align 4
  store %"class.draco::PointCloud"** %__t, %"class.draco::PointCloud"*** %__t.addr, align 4
  %0 = load %"class.draco::PointCloud"**, %"class.draco::PointCloud"*** %__t.addr, align 4
  ret %"class.draco::PointCloud"** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.159"* @_ZNSt3__222__compressed_pair_elemIPN5draco10PointCloudELi0ELb0EEC2IS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.159"* returned %this, %"class.draco::PointCloud"** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.159"*, align 4
  %__u.addr = alloca %"class.draco::PointCloud"**, align 4
  store %"struct.std::__2::__compressed_pair_elem.159"* %this, %"struct.std::__2::__compressed_pair_elem.159"** %this.addr, align 4
  store %"class.draco::PointCloud"** %__u, %"class.draco::PointCloud"*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.159"*, %"struct.std::__2::__compressed_pair_elem.159"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.159", %"struct.std::__2::__compressed_pair_elem.159"* %this1, i32 0, i32 0
  %0 = load %"class.draco::PointCloud"**, %"class.draco::PointCloud"*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointCloud"** @_ZNSt3__27forwardIPN5draco10PointCloudEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::PointCloud"** nonnull align 4 dereferenceable(4) %0) #9
  %1 = load %"class.draco::PointCloud"*, %"class.draco::PointCloud"** %call, align 4
  store %"class.draco::PointCloud"* %1, %"class.draco::PointCloud"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.159"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.160"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco10PointCloudEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.160"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.160"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.160"* %this, %"struct.std::__2::__compressed_pair_elem.160"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.160"*, %"struct.std::__2::__compressed_pair_elem.160"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.160"* %this1 to %"struct.std::__2::default_delete.161"*
  ret %"struct.std::__2::__compressed_pair_elem.160"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__210unique_ptrIN5draco10PointCloudENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.157"* %this, %"class.draco::PointCloud"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.157"*, align 4
  %__p.addr = alloca %"class.draco::PointCloud"*, align 4
  %__tmp = alloca %"class.draco::PointCloud"*, align 4
  store %"class.std::__2::unique_ptr.157"* %this, %"class.std::__2::unique_ptr.157"** %this.addr, align 4
  store %"class.draco::PointCloud"* %__p, %"class.draco::PointCloud"** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.157"*, %"class.std::__2::unique_ptr.157"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.157", %"class.std::__2::unique_ptr.157"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointCloud"** @_ZNSt3__217__compressed_pairIPN5draco10PointCloudENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.158"* %__ptr_) #9
  %0 = load %"class.draco::PointCloud"*, %"class.draco::PointCloud"** %call, align 4
  store %"class.draco::PointCloud"* %0, %"class.draco::PointCloud"** %__tmp, align 4
  %1 = load %"class.draco::PointCloud"*, %"class.draco::PointCloud"** %__p.addr, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr.157", %"class.std::__2::unique_ptr.157"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::PointCloud"** @_ZNSt3__217__compressed_pairIPN5draco10PointCloudENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.158"* %__ptr_2) #9
  store %"class.draco::PointCloud"* %1, %"class.draco::PointCloud"** %call3, align 4
  %2 = load %"class.draco::PointCloud"*, %"class.draco::PointCloud"** %__tmp, align 4
  %tobool = icmp ne %"class.draco::PointCloud"* %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__ptr_4 = getelementptr inbounds %"class.std::__2::unique_ptr.157", %"class.std::__2::unique_ptr.157"* %this1, i32 0, i32 0
  %call5 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.161"* @_ZNSt3__217__compressed_pairIPN5draco10PointCloudENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.158"* %__ptr_4) #9
  %3 = load %"class.draco::PointCloud"*, %"class.draco::PointCloud"** %__tmp, align 4
  call void @_ZNKSt3__214default_deleteIN5draco10PointCloudEEclEPS2_(%"struct.std::__2::default_delete.161"* %call5, %"class.draco::PointCloud"* %3) #9
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::PointCloud"** @_ZNSt3__217__compressed_pairIPN5draco10PointCloudENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.158"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.158"*, align 4
  store %"class.std::__2::__compressed_pair.158"* %this, %"class.std::__2::__compressed_pair.158"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.158"*, %"class.std::__2::__compressed_pair.158"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.158"* %this1 to %"struct.std::__2::__compressed_pair_elem.159"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointCloud"** @_ZNSt3__222__compressed_pair_elemIPN5draco10PointCloudELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.159"* %0) #9
  ret %"class.draco::PointCloud"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.161"* @_ZNSt3__217__compressed_pairIPN5draco10PointCloudENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.158"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.158"*, align 4
  store %"class.std::__2::__compressed_pair.158"* %this, %"class.std::__2::__compressed_pair.158"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.158"*, %"class.std::__2::__compressed_pair.158"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.158"* %this1 to %"struct.std::__2::__compressed_pair_elem.160"*
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.161"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco10PointCloudEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.160"* %0) #9
  ret %"struct.std::__2::default_delete.161"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__214default_deleteIN5draco10PointCloudEEclEPS2_(%"struct.std::__2::default_delete.161"* %this, %"class.draco::PointCloud"* %__ptr) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::default_delete.161"*, align 4
  %__ptr.addr = alloca %"class.draco::PointCloud"*, align 4
  store %"struct.std::__2::default_delete.161"* %this, %"struct.std::__2::default_delete.161"** %this.addr, align 4
  store %"class.draco::PointCloud"* %__ptr, %"class.draco::PointCloud"** %__ptr.addr, align 4
  %this1 = load %"struct.std::__2::default_delete.161"*, %"struct.std::__2::default_delete.161"** %this.addr, align 4
  %0 = load %"class.draco::PointCloud"*, %"class.draco::PointCloud"** %__ptr.addr, align 4
  %isnull = icmp eq %"class.draco::PointCloud"* %0, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %entry
  %1 = bitcast %"class.draco::PointCloud"* %0 to void (%"class.draco::PointCloud"*)***
  %vtable = load void (%"class.draco::PointCloud"*)**, void (%"class.draco::PointCloud"*)*** %1, align 4
  %vfn = getelementptr inbounds void (%"class.draco::PointCloud"*)*, void (%"class.draco::PointCloud"*)** %vtable, i64 1
  %2 = load void (%"class.draco::PointCloud"*)*, void (%"class.draco::PointCloud"*)** %vfn, align 4
  call void %2(%"class.draco::PointCloud"* %0) #9
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::PointCloud"** @_ZNSt3__222__compressed_pair_elemIPN5draco10PointCloudELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.159"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.159"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.159"* %this, %"struct.std::__2::__compressed_pair_elem.159"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.159"*, %"struct.std::__2::__compressed_pair_elem.159"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.159", %"struct.std::__2::__compressed_pair_elem.159"* %this1, i32 0, i32 0
  ret %"class.draco::PointCloud"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.161"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco10PointCloudEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.160"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.160"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.160"* %this, %"struct.std::__2::__compressed_pair_elem.160"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.160"*, %"struct.std::__2::__compressed_pair_elem.160"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.160"* %this1 to %"struct.std::__2::default_delete.161"*
  ret %"struct.std::__2::default_delete.161"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.163"* @_ZNSt3__217__compressed_pairIPN5draco4MeshENS_14default_deleteIS2_EEEC2IRS3_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.163"* returned %this, %"class.draco::Mesh"** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.163"*, align 4
  %__t1.addr = alloca %"class.draco::Mesh"**, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.163"* %this, %"class.std::__2::__compressed_pair.163"** %this.addr, align 4
  store %"class.draco::Mesh"** %__t1, %"class.draco::Mesh"*** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.163"*, %"class.std::__2::__compressed_pair.163"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.163"* %this1 to %"struct.std::__2::__compressed_pair_elem.164"*
  %1 = load %"class.draco::Mesh"**, %"class.draco::Mesh"*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::Mesh"** @_ZNSt3__27forwardIRPN5draco4MeshEEEOT_RNS_16remove_referenceIS5_E4typeE(%"class.draco::Mesh"** nonnull align 4 dereferenceable(4) %1) #9
  %call2 = call %"struct.std::__2::__compressed_pair_elem.164"* @_ZNSt3__222__compressed_pair_elemIPN5draco4MeshELi0ELb0EEC2IRS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.164"* %0, %"class.draco::Mesh"** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.163"* %this1 to %"struct.std::__2::__compressed_pair_elem.165"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #9
  %call4 = call %"struct.std::__2::__compressed_pair_elem.165"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco4MeshEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.165"* %2)
  ret %"class.std::__2::__compressed_pair.163"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::Mesh"** @_ZNSt3__27forwardIRPN5draco4MeshEEEOT_RNS_16remove_referenceIS5_E4typeE(%"class.draco::Mesh"** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.draco::Mesh"**, align 4
  store %"class.draco::Mesh"** %__t, %"class.draco::Mesh"*** %__t.addr, align 4
  %0 = load %"class.draco::Mesh"**, %"class.draco::Mesh"*** %__t.addr, align 4
  ret %"class.draco::Mesh"** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.164"* @_ZNSt3__222__compressed_pair_elemIPN5draco4MeshELi0ELb0EEC2IRS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.164"* returned %this, %"class.draco::Mesh"** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.164"*, align 4
  %__u.addr = alloca %"class.draco::Mesh"**, align 4
  store %"struct.std::__2::__compressed_pair_elem.164"* %this, %"struct.std::__2::__compressed_pair_elem.164"** %this.addr, align 4
  store %"class.draco::Mesh"** %__u, %"class.draco::Mesh"*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.164"*, %"struct.std::__2::__compressed_pair_elem.164"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.164", %"struct.std::__2::__compressed_pair_elem.164"* %this1, i32 0, i32 0
  %0 = load %"class.draco::Mesh"**, %"class.draco::Mesh"*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::Mesh"** @_ZNSt3__27forwardIRPN5draco4MeshEEEOT_RNS_16remove_referenceIS5_E4typeE(%"class.draco::Mesh"** nonnull align 4 dereferenceable(4) %0) #9
  %1 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %call, align 4
  store %"class.draco::Mesh"* %1, %"class.draco::Mesh"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.164"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.165"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco4MeshEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.165"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.165"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.165"* %this, %"struct.std::__2::__compressed_pair_elem.165"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.165"*, %"struct.std::__2::__compressed_pair_elem.165"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.165"* %this1 to %"struct.std::__2::default_delete.166"*
  ret %"struct.std::__2::__compressed_pair_elem.165"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__210unique_ptrIN5draco4MeshENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.162"* %this, %"class.draco::Mesh"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.162"*, align 4
  %__p.addr = alloca %"class.draco::Mesh"*, align 4
  %__tmp = alloca %"class.draco::Mesh"*, align 4
  store %"class.std::__2::unique_ptr.162"* %this, %"class.std::__2::unique_ptr.162"** %this.addr, align 4
  store %"class.draco::Mesh"* %__p, %"class.draco::Mesh"** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.162"*, %"class.std::__2::unique_ptr.162"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.162", %"class.std::__2::unique_ptr.162"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::Mesh"** @_ZNSt3__217__compressed_pairIPN5draco4MeshENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.163"* %__ptr_) #9
  %0 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %call, align 4
  store %"class.draco::Mesh"* %0, %"class.draco::Mesh"** %__tmp, align 4
  %1 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %__p.addr, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr.162", %"class.std::__2::unique_ptr.162"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::Mesh"** @_ZNSt3__217__compressed_pairIPN5draco4MeshENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.163"* %__ptr_2) #9
  store %"class.draco::Mesh"* %1, %"class.draco::Mesh"** %call3, align 4
  %2 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %__tmp, align 4
  %tobool = icmp ne %"class.draco::Mesh"* %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__ptr_4 = getelementptr inbounds %"class.std::__2::unique_ptr.162", %"class.std::__2::unique_ptr.162"* %this1, i32 0, i32 0
  %call5 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.166"* @_ZNSt3__217__compressed_pairIPN5draco4MeshENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.163"* %__ptr_4) #9
  %3 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %__tmp, align 4
  call void @_ZNKSt3__214default_deleteIN5draco4MeshEEclEPS2_(%"struct.std::__2::default_delete.166"* %call5, %"class.draco::Mesh"* %3) #9
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::Mesh"** @_ZNSt3__217__compressed_pairIPN5draco4MeshENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.163"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.163"*, align 4
  store %"class.std::__2::__compressed_pair.163"* %this, %"class.std::__2::__compressed_pair.163"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.163"*, %"class.std::__2::__compressed_pair.163"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.163"* %this1 to %"struct.std::__2::__compressed_pair_elem.164"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::Mesh"** @_ZNSt3__222__compressed_pair_elemIPN5draco4MeshELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.164"* %0) #9
  ret %"class.draco::Mesh"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.166"* @_ZNSt3__217__compressed_pairIPN5draco4MeshENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.163"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.163"*, align 4
  store %"class.std::__2::__compressed_pair.163"* %this, %"class.std::__2::__compressed_pair.163"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.163"*, %"class.std::__2::__compressed_pair.163"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.163"* %this1 to %"struct.std::__2::__compressed_pair_elem.165"*
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.166"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco4MeshEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.165"* %0) #9
  ret %"struct.std::__2::default_delete.166"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__214default_deleteIN5draco4MeshEEclEPS2_(%"struct.std::__2::default_delete.166"* %this, %"class.draco::Mesh"* %__ptr) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::default_delete.166"*, align 4
  %__ptr.addr = alloca %"class.draco::Mesh"*, align 4
  store %"struct.std::__2::default_delete.166"* %this, %"struct.std::__2::default_delete.166"** %this.addr, align 4
  store %"class.draco::Mesh"* %__ptr, %"class.draco::Mesh"** %__ptr.addr, align 4
  %this1 = load %"struct.std::__2::default_delete.166"*, %"struct.std::__2::default_delete.166"** %this.addr, align 4
  %0 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %__ptr.addr, align 4
  %isnull = icmp eq %"class.draco::Mesh"* %0, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %entry
  %1 = bitcast %"class.draco::Mesh"* %0 to void (%"class.draco::Mesh"*)***
  %vtable = load void (%"class.draco::Mesh"*)**, void (%"class.draco::Mesh"*)*** %1, align 4
  %vfn = getelementptr inbounds void (%"class.draco::Mesh"*)*, void (%"class.draco::Mesh"*)** %vtable, i64 1
  %2 = load void (%"class.draco::Mesh"*)*, void (%"class.draco::Mesh"*)** %vfn, align 4
  call void %2(%"class.draco::Mesh"* %0) #9
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::Mesh"** @_ZNSt3__222__compressed_pair_elemIPN5draco4MeshELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.164"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.164"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.164"* %this, %"struct.std::__2::__compressed_pair_elem.164"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.164"*, %"struct.std::__2::__compressed_pair_elem.164"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.164", %"struct.std::__2::__compressed_pair_elem.164"* %this1, i32 0, i32 0
  ret %"class.draco::Mesh"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.166"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco4MeshEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.165"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.165"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.165"* %this, %"struct.std::__2::__compressed_pair_elem.165"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.165"*, %"struct.std::__2::__compressed_pair_elem.165"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.165"* %this1 to %"struct.std::__2::default_delete.166"*
  ret %"struct.std::__2::default_delete.166"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::Mesh"** @_ZNKSt3__217__compressed_pairIPN5draco4MeshENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.163"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.163"*, align 4
  store %"class.std::__2::__compressed_pair.163"* %this, %"class.std::__2::__compressed_pair.163"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.163"*, %"class.std::__2::__compressed_pair.163"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.163"* %this1 to %"struct.std::__2::__compressed_pair_elem.164"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::Mesh"** @_ZNKSt3__222__compressed_pair_elemIPN5draco4MeshELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.164"* %0) #9
  ret %"class.draco::Mesh"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::Mesh"** @_ZNKSt3__222__compressed_pair_elemIPN5draco4MeshELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.164"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.164"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.164"* %this, %"struct.std::__2::__compressed_pair_elem.164"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.164"*, %"struct.std::__2::__compressed_pair_elem.164"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.164", %"struct.std::__2::__compressed_pair_elem.164"* %this1, i32 0, i32 0
  ret %"class.draco::Mesh"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::Mesh"* @_ZNSt3__210unique_ptrIN5draco4MeshENS_14default_deleteIS2_EEE7releaseEv(%"class.std::__2::unique_ptr.162"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.162"*, align 4
  %__t = alloca %"class.draco::Mesh"*, align 4
  store %"class.std::__2::unique_ptr.162"* %this, %"class.std::__2::unique_ptr.162"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.162"*, %"class.std::__2::unique_ptr.162"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.162", %"class.std::__2::unique_ptr.162"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::Mesh"** @_ZNSt3__217__compressed_pairIPN5draco4MeshENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.163"* %__ptr_) #9
  %0 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %call, align 4
  store %"class.draco::Mesh"* %0, %"class.draco::Mesh"** %__t, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr.162", %"class.std::__2::unique_ptr.162"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::Mesh"** @_ZNSt3__217__compressed_pairIPN5draco4MeshENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.163"* %__ptr_2) #9
  store %"class.draco::Mesh"* null, %"class.draco::Mesh"** %call3, align 4
  %1 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %__t, align 4
  ret %"class.draco::Mesh"* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.166"* @_ZNSt3__27forwardINS_14default_deleteIN5draco4MeshEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.166"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"struct.std::__2::default_delete.166"*, align 4
  store %"struct.std::__2::default_delete.166"* %__t, %"struct.std::__2::default_delete.166"** %__t.addr, align 4
  %0 = load %"struct.std::__2::default_delete.166"*, %"struct.std::__2::default_delete.166"** %__t.addr, align 4
  ret %"struct.std::__2::default_delete.166"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.166"* @_ZNSt3__210unique_ptrIN5draco4MeshENS_14default_deleteIS2_EEE11get_deleterEv(%"class.std::__2::unique_ptr.162"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.162"*, align 4
  store %"class.std::__2::unique_ptr.162"* %this, %"class.std::__2::unique_ptr.162"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.162"*, %"class.std::__2::unique_ptr.162"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.162", %"class.std::__2::unique_ptr.162"* %this1, i32 0, i32 0
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.166"* @_ZNSt3__217__compressed_pairIPN5draco4MeshENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.163"* %__ptr_) #9
  ret %"struct.std::__2::default_delete.166"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.158"* @_ZNSt3__217__compressed_pairIPN5draco10PointCloudENS_14default_deleteIS2_EEEC2IPNS1_4MeshENS4_IS8_EEEEOT_OT0_(%"class.std::__2::__compressed_pair.158"* returned %this, %"class.draco::Mesh"** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::default_delete.166"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.158"*, align 4
  %__t1.addr = alloca %"class.draco::Mesh"**, align 4
  %__t2.addr = alloca %"struct.std::__2::default_delete.166"*, align 4
  store %"class.std::__2::__compressed_pair.158"* %this, %"class.std::__2::__compressed_pair.158"** %this.addr, align 4
  store %"class.draco::Mesh"** %__t1, %"class.draco::Mesh"*** %__t1.addr, align 4
  store %"struct.std::__2::default_delete.166"* %__t2, %"struct.std::__2::default_delete.166"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.158"*, %"class.std::__2::__compressed_pair.158"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.158"* %this1 to %"struct.std::__2::__compressed_pair_elem.159"*
  %1 = load %"class.draco::Mesh"**, %"class.draco::Mesh"*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::Mesh"** @_ZNSt3__27forwardIPN5draco4MeshEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::Mesh"** nonnull align 4 dereferenceable(4) %1) #9
  %call2 = call %"struct.std::__2::__compressed_pair_elem.159"* @_ZNSt3__222__compressed_pair_elemIPN5draco10PointCloudELi0ELb0EEC2IPNS1_4MeshEvEEOT_(%"struct.std::__2::__compressed_pair_elem.159"* %0, %"class.draco::Mesh"** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.158"* %this1 to %"struct.std::__2::__compressed_pair_elem.160"*
  %3 = load %"struct.std::__2::default_delete.166"*, %"struct.std::__2::default_delete.166"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.166"* @_ZNSt3__27forwardINS_14default_deleteIN5draco4MeshEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.166"* nonnull align 1 dereferenceable(1) %3) #9
  %call4 = call %"struct.std::__2::__compressed_pair_elem.160"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco10PointCloudEEELi1ELb1EEC2INS1_INS2_4MeshEEEvEEOT_(%"struct.std::__2::__compressed_pair_elem.160"* %2, %"struct.std::__2::default_delete.166"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::__compressed_pair.158"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::Mesh"** @_ZNSt3__27forwardIPN5draco4MeshEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::Mesh"** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.draco::Mesh"**, align 4
  store %"class.draco::Mesh"** %__t, %"class.draco::Mesh"*** %__t.addr, align 4
  %0 = load %"class.draco::Mesh"**, %"class.draco::Mesh"*** %__t.addr, align 4
  ret %"class.draco::Mesh"** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.159"* @_ZNSt3__222__compressed_pair_elemIPN5draco10PointCloudELi0ELb0EEC2IPNS1_4MeshEvEEOT_(%"struct.std::__2::__compressed_pair_elem.159"* returned %this, %"class.draco::Mesh"** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.159"*, align 4
  %__u.addr = alloca %"class.draco::Mesh"**, align 4
  store %"struct.std::__2::__compressed_pair_elem.159"* %this, %"struct.std::__2::__compressed_pair_elem.159"** %this.addr, align 4
  store %"class.draco::Mesh"** %__u, %"class.draco::Mesh"*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.159"*, %"struct.std::__2::__compressed_pair_elem.159"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.159", %"struct.std::__2::__compressed_pair_elem.159"* %this1, i32 0, i32 0
  %0 = load %"class.draco::Mesh"**, %"class.draco::Mesh"*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::Mesh"** @_ZNSt3__27forwardIPN5draco4MeshEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::Mesh"** nonnull align 4 dereferenceable(4) %0) #9
  %1 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %call, align 4
  %2 = bitcast %"class.draco::Mesh"* %1 to %"class.draco::PointCloud"*
  store %"class.draco::PointCloud"* %2, %"class.draco::PointCloud"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.159"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.160"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco10PointCloudEEELi1ELb1EEC2INS1_INS2_4MeshEEEvEEOT_(%"struct.std::__2::__compressed_pair_elem.160"* returned %this, %"struct.std::__2::default_delete.166"* nonnull align 1 dereferenceable(1) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.160"*, align 4
  %__u.addr = alloca %"struct.std::__2::default_delete.166"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.160"* %this, %"struct.std::__2::__compressed_pair_elem.160"** %this.addr, align 4
  store %"struct.std::__2::default_delete.166"* %__u, %"struct.std::__2::default_delete.166"** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.160"*, %"struct.std::__2::__compressed_pair_elem.160"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.160"* %this1 to %"struct.std::__2::default_delete.161"*
  %1 = load %"struct.std::__2::default_delete.166"*, %"struct.std::__2::default_delete.166"** %__u.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.166"* @_ZNSt3__27forwardINS_14default_deleteIN5draco4MeshEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.166"* nonnull align 1 dereferenceable(1) %1) #9
  %call2 = call %"struct.std::__2::default_delete.161"* @_ZNSt3__214default_deleteIN5draco10PointCloudEEC2INS1_4MeshEEERKNS0_IT_EEPNS_9enable_ifIXsr14is_convertibleIPS6_PS2_EE5valueEvE4typeE(%"struct.std::__2::default_delete.161"* %0, %"struct.std::__2::default_delete.166"* nonnull align 1 dereferenceable(1) %call, i8* null) #9
  ret %"struct.std::__2::__compressed_pair_elem.160"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::default_delete.161"* @_ZNSt3__214default_deleteIN5draco10PointCloudEEC2INS1_4MeshEEERKNS0_IT_EEPNS_9enable_ifIXsr14is_convertibleIPS6_PS2_EE5valueEvE4typeE(%"struct.std::__2::default_delete.161"* returned %this, %"struct.std::__2::default_delete.166"* nonnull align 1 dereferenceable(1) %0, i8* %1) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::default_delete.161"*, align 4
  %.addr = alloca %"struct.std::__2::default_delete.166"*, align 4
  %.addr1 = alloca i8*, align 4
  store %"struct.std::__2::default_delete.161"* %this, %"struct.std::__2::default_delete.161"** %this.addr, align 4
  store %"struct.std::__2::default_delete.166"* %0, %"struct.std::__2::default_delete.166"** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  %this2 = load %"struct.std::__2::default_delete.161"*, %"struct.std::__2::default_delete.161"** %this.addr, align 4
  ret %"struct.std::__2::default_delete.161"* %this2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.157"* @_ZNSt3__24moveIRNS_10unique_ptrIN5draco10PointCloudENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOS9_(%"class.std::__2::unique_ptr.157"* nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::unique_ptr.157"*, align 4
  store %"class.std::__2::unique_ptr.157"* %__t, %"class.std::__2::unique_ptr.157"** %__t.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.157"*, %"class.std::__2::unique_ptr.157"** %__t.addr, align 4
  ret %"class.std::__2::unique_ptr.157"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.157"* @_ZNSt3__210unique_ptrIN5draco10PointCloudENS_14default_deleteIS2_EEEC2EOS5_(%"class.std::__2::unique_ptr.157"* returned %this, %"class.std::__2::unique_ptr.157"* nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.157"*, align 4
  %__u.addr = alloca %"class.std::__2::unique_ptr.157"*, align 4
  %ref.tmp = alloca %"class.draco::PointCloud"*, align 4
  store %"class.std::__2::unique_ptr.157"* %this, %"class.std::__2::unique_ptr.157"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.157"* %__u, %"class.std::__2::unique_ptr.157"** %__u.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.157"*, %"class.std::__2::unique_ptr.157"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.157", %"class.std::__2::unique_ptr.157"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::unique_ptr.157"*, %"class.std::__2::unique_ptr.157"** %__u.addr, align 4
  %call = call %"class.draco::PointCloud"* @_ZNSt3__210unique_ptrIN5draco10PointCloudENS_14default_deleteIS2_EEE7releaseEv(%"class.std::__2::unique_ptr.157"* %0) #9
  store %"class.draco::PointCloud"* %call, %"class.draco::PointCloud"** %ref.tmp, align 4
  %1 = load %"class.std::__2::unique_ptr.157"*, %"class.std::__2::unique_ptr.157"** %__u.addr, align 4
  %call2 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.161"* @_ZNSt3__210unique_ptrIN5draco10PointCloudENS_14default_deleteIS2_EEE11get_deleterEv(%"class.std::__2::unique_ptr.157"* %1) #9
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.161"* @_ZNSt3__27forwardINS_14default_deleteIN5draco10PointCloudEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.161"* nonnull align 1 dereferenceable(1) %call2) #9
  %call4 = call %"class.std::__2::__compressed_pair.158"* @_ZNSt3__217__compressed_pairIPN5draco10PointCloudENS_14default_deleteIS2_EEEC2IS3_S5_EEOT_OT0_(%"class.std::__2::__compressed_pair.158"* %__ptr_, %"class.draco::PointCloud"** nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::default_delete.161"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::unique_ptr.157"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::PointCloud"* @_ZNSt3__210unique_ptrIN5draco10PointCloudENS_14default_deleteIS2_EEE7releaseEv(%"class.std::__2::unique_ptr.157"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.157"*, align 4
  %__t = alloca %"class.draco::PointCloud"*, align 4
  store %"class.std::__2::unique_ptr.157"* %this, %"class.std::__2::unique_ptr.157"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.157"*, %"class.std::__2::unique_ptr.157"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.157", %"class.std::__2::unique_ptr.157"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointCloud"** @_ZNSt3__217__compressed_pairIPN5draco10PointCloudENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.158"* %__ptr_) #9
  %0 = load %"class.draco::PointCloud"*, %"class.draco::PointCloud"** %call, align 4
  store %"class.draco::PointCloud"* %0, %"class.draco::PointCloud"** %__t, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr.157", %"class.std::__2::unique_ptr.157"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::PointCloud"** @_ZNSt3__217__compressed_pairIPN5draco10PointCloudENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.158"* %__ptr_2) #9
  store %"class.draco::PointCloud"* null, %"class.draco::PointCloud"** %call3, align 4
  %1 = load %"class.draco::PointCloud"*, %"class.draco::PointCloud"** %__t, align 4
  ret %"class.draco::PointCloud"* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.161"* @_ZNSt3__27forwardINS_14default_deleteIN5draco10PointCloudEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.161"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"struct.std::__2::default_delete.161"*, align 4
  store %"struct.std::__2::default_delete.161"* %__t, %"struct.std::__2::default_delete.161"** %__t.addr, align 4
  %0 = load %"struct.std::__2::default_delete.161"*, %"struct.std::__2::default_delete.161"** %__t.addr, align 4
  ret %"struct.std::__2::default_delete.161"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.161"* @_ZNSt3__210unique_ptrIN5draco10PointCloudENS_14default_deleteIS2_EEE11get_deleterEv(%"class.std::__2::unique_ptr.157"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.157"*, align 4
  store %"class.std::__2::unique_ptr.157"* %this, %"class.std::__2::unique_ptr.157"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.157"*, %"class.std::__2::unique_ptr.157"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.157", %"class.std::__2::unique_ptr.157"* %this1, i32 0, i32 0
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.161"* @_ZNSt3__217__compressed_pairIPN5draco10PointCloudENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.158"* %__ptr_) #9
  ret %"struct.std::__2::default_delete.161"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.158"* @_ZNSt3__217__compressed_pairIPN5draco10PointCloudENS_14default_deleteIS2_EEEC2IS3_S5_EEOT_OT0_(%"class.std::__2::__compressed_pair.158"* returned %this, %"class.draco::PointCloud"** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::default_delete.161"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.158"*, align 4
  %__t1.addr = alloca %"class.draco::PointCloud"**, align 4
  %__t2.addr = alloca %"struct.std::__2::default_delete.161"*, align 4
  store %"class.std::__2::__compressed_pair.158"* %this, %"class.std::__2::__compressed_pair.158"** %this.addr, align 4
  store %"class.draco::PointCloud"** %__t1, %"class.draco::PointCloud"*** %__t1.addr, align 4
  store %"struct.std::__2::default_delete.161"* %__t2, %"struct.std::__2::default_delete.161"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.158"*, %"class.std::__2::__compressed_pair.158"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.158"* %this1 to %"struct.std::__2::__compressed_pair_elem.159"*
  %1 = load %"class.draco::PointCloud"**, %"class.draco::PointCloud"*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointCloud"** @_ZNSt3__27forwardIPN5draco10PointCloudEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::PointCloud"** nonnull align 4 dereferenceable(4) %1) #9
  %call2 = call %"struct.std::__2::__compressed_pair_elem.159"* @_ZNSt3__222__compressed_pair_elemIPN5draco10PointCloudELi0ELb0EEC2IS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.159"* %0, %"class.draco::PointCloud"** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.158"* %this1 to %"struct.std::__2::__compressed_pair_elem.160"*
  %3 = load %"struct.std::__2::default_delete.161"*, %"struct.std::__2::default_delete.161"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.161"* @_ZNSt3__27forwardINS_14default_deleteIN5draco10PointCloudEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.161"* nonnull align 1 dereferenceable(1) %3) #9
  %call4 = call %"struct.std::__2::__compressed_pair_elem.160"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco10PointCloudEEELi1ELb1EEC2IS4_vEEOT_(%"struct.std::__2::__compressed_pair_elem.160"* %2, %"struct.std::__2::default_delete.161"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::__compressed_pair.158"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.160"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco10PointCloudEEELi1ELb1EEC2IS4_vEEOT_(%"struct.std::__2::__compressed_pair_elem.160"* returned %this, %"struct.std::__2::default_delete.161"* nonnull align 1 dereferenceable(1) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.160"*, align 4
  %__u.addr = alloca %"struct.std::__2::default_delete.161"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.160"* %this, %"struct.std::__2::__compressed_pair_elem.160"** %this.addr, align 4
  store %"struct.std::__2::default_delete.161"* %__u, %"struct.std::__2::default_delete.161"** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.160"*, %"struct.std::__2::__compressed_pair_elem.160"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.160"* %this1 to %"struct.std::__2::default_delete.161"*
  %1 = load %"struct.std::__2::default_delete.161"*, %"struct.std::__2::default_delete.161"** %__u.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.161"* @_ZNSt3__27forwardINS_14default_deleteIN5draco10PointCloudEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.161"* nonnull align 1 dereferenceable(1) %1) #9
  ret %"struct.std::__2::__compressed_pair_elem.160"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.162"* @_ZNSt3__210unique_ptrIN5draco4MeshENS_14default_deleteIS2_EEEC2ILb1EvEEv(%"class.std::__2::unique_ptr.162"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.162"*, align 4
  %ref.tmp = alloca %"class.draco::Mesh"*, align 4
  %ref.tmp2 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::unique_ptr.162"* %this, %"class.std::__2::unique_ptr.162"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.162"*, %"class.std::__2::unique_ptr.162"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.162", %"class.std::__2::unique_ptr.162"* %this1, i32 0, i32 0
  store %"class.draco::Mesh"* null, %"class.draco::Mesh"** %ref.tmp, align 4
  %call = call %"class.std::__2::__compressed_pair.163"* @_ZNSt3__217__compressed_pairIPN5draco4MeshENS_14default_deleteIS2_EEEC2IS3_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.163"* %__ptr_, %"class.draco::Mesh"** nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  ret %"class.std::__2::unique_ptr.162"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.163"* @_ZNSt3__217__compressed_pairIPN5draco4MeshENS_14default_deleteIS2_EEEC2IS3_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.163"* returned %this, %"class.draco::Mesh"** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.163"*, align 4
  %__t1.addr = alloca %"class.draco::Mesh"**, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.163"* %this, %"class.std::__2::__compressed_pair.163"** %this.addr, align 4
  store %"class.draco::Mesh"** %__t1, %"class.draco::Mesh"*** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.163"*, %"class.std::__2::__compressed_pair.163"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.163"* %this1 to %"struct.std::__2::__compressed_pair_elem.164"*
  %1 = load %"class.draco::Mesh"**, %"class.draco::Mesh"*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::Mesh"** @_ZNSt3__27forwardIPN5draco4MeshEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::Mesh"** nonnull align 4 dereferenceable(4) %1) #9
  %call2 = call %"struct.std::__2::__compressed_pair_elem.164"* @_ZNSt3__222__compressed_pair_elemIPN5draco4MeshELi0ELb0EEC2IS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.164"* %0, %"class.draco::Mesh"** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.163"* %this1 to %"struct.std::__2::__compressed_pair_elem.165"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #9
  %call4 = call %"struct.std::__2::__compressed_pair_elem.165"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco4MeshEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.165"* %2)
  ret %"class.std::__2::__compressed_pair.163"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.164"* @_ZNSt3__222__compressed_pair_elemIPN5draco4MeshELi0ELb0EEC2IS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.164"* returned %this, %"class.draco::Mesh"** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.164"*, align 4
  %__u.addr = alloca %"class.draco::Mesh"**, align 4
  store %"struct.std::__2::__compressed_pair_elem.164"* %this, %"struct.std::__2::__compressed_pair_elem.164"** %this.addr, align 4
  store %"class.draco::Mesh"** %__u, %"class.draco::Mesh"*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.164"*, %"struct.std::__2::__compressed_pair_elem.164"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.164", %"struct.std::__2::__compressed_pair_elem.164"* %this1, i32 0, i32 0
  %0 = load %"class.draco::Mesh"**, %"class.draco::Mesh"*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::Mesh"** @_ZNSt3__27forwardIPN5draco4MeshEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::Mesh"** nonnull align 4 dereferenceable(4) %0) #9
  %1 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %call, align 4
  store %"class.draco::Mesh"* %1, %"class.draco::Mesh"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.164"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.162"* @_ZNSt3__210unique_ptrIN5draco4MeshENS_14default_deleteIS2_EEEC2EOS5_(%"class.std::__2::unique_ptr.162"* returned %this, %"class.std::__2::unique_ptr.162"* nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.162"*, align 4
  %__u.addr = alloca %"class.std::__2::unique_ptr.162"*, align 4
  %ref.tmp = alloca %"class.draco::Mesh"*, align 4
  store %"class.std::__2::unique_ptr.162"* %this, %"class.std::__2::unique_ptr.162"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.162"* %__u, %"class.std::__2::unique_ptr.162"** %__u.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.162"*, %"class.std::__2::unique_ptr.162"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.162", %"class.std::__2::unique_ptr.162"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::unique_ptr.162"*, %"class.std::__2::unique_ptr.162"** %__u.addr, align 4
  %call = call %"class.draco::Mesh"* @_ZNSt3__210unique_ptrIN5draco4MeshENS_14default_deleteIS2_EEE7releaseEv(%"class.std::__2::unique_ptr.162"* %0) #9
  store %"class.draco::Mesh"* %call, %"class.draco::Mesh"** %ref.tmp, align 4
  %1 = load %"class.std::__2::unique_ptr.162"*, %"class.std::__2::unique_ptr.162"** %__u.addr, align 4
  %call2 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.166"* @_ZNSt3__210unique_ptrIN5draco4MeshENS_14default_deleteIS2_EEE11get_deleterEv(%"class.std::__2::unique_ptr.162"* %1) #9
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.166"* @_ZNSt3__27forwardINS_14default_deleteIN5draco4MeshEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.166"* nonnull align 1 dereferenceable(1) %call2) #9
  %call4 = call %"class.std::__2::__compressed_pair.163"* @_ZNSt3__217__compressed_pairIPN5draco4MeshENS_14default_deleteIS2_EEEC2IS3_S5_EEOT_OT0_(%"class.std::__2::__compressed_pair.163"* %__ptr_, %"class.draco::Mesh"** nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::default_delete.166"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::unique_ptr.162"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.163"* @_ZNSt3__217__compressed_pairIPN5draco4MeshENS_14default_deleteIS2_EEEC2IS3_S5_EEOT_OT0_(%"class.std::__2::__compressed_pair.163"* returned %this, %"class.draco::Mesh"** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::default_delete.166"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.163"*, align 4
  %__t1.addr = alloca %"class.draco::Mesh"**, align 4
  %__t2.addr = alloca %"struct.std::__2::default_delete.166"*, align 4
  store %"class.std::__2::__compressed_pair.163"* %this, %"class.std::__2::__compressed_pair.163"** %this.addr, align 4
  store %"class.draco::Mesh"** %__t1, %"class.draco::Mesh"*** %__t1.addr, align 4
  store %"struct.std::__2::default_delete.166"* %__t2, %"struct.std::__2::default_delete.166"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.163"*, %"class.std::__2::__compressed_pair.163"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.163"* %this1 to %"struct.std::__2::__compressed_pair_elem.164"*
  %1 = load %"class.draco::Mesh"**, %"class.draco::Mesh"*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::Mesh"** @_ZNSt3__27forwardIPN5draco4MeshEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::Mesh"** nonnull align 4 dereferenceable(4) %1) #9
  %call2 = call %"struct.std::__2::__compressed_pair_elem.164"* @_ZNSt3__222__compressed_pair_elemIPN5draco4MeshELi0ELb0EEC2IS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.164"* %0, %"class.draco::Mesh"** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.163"* %this1 to %"struct.std::__2::__compressed_pair_elem.165"*
  %3 = load %"struct.std::__2::default_delete.166"*, %"struct.std::__2::default_delete.166"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.166"* @_ZNSt3__27forwardINS_14default_deleteIN5draco4MeshEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.166"* nonnull align 1 dereferenceable(1) %3) #9
  %call4 = call %"struct.std::__2::__compressed_pair_elem.165"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco4MeshEEELi1ELb1EEC2IS4_vEEOT_(%"struct.std::__2::__compressed_pair_elem.165"* %2, %"struct.std::__2::default_delete.166"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::__compressed_pair.163"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.165"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco4MeshEEELi1ELb1EEC2IS4_vEEOT_(%"struct.std::__2::__compressed_pair_elem.165"* returned %this, %"struct.std::__2::default_delete.166"* nonnull align 1 dereferenceable(1) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.165"*, align 4
  %__u.addr = alloca %"struct.std::__2::default_delete.166"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.165"* %this, %"struct.std::__2::__compressed_pair_elem.165"** %this.addr, align 4
  store %"struct.std::__2::default_delete.166"* %__u, %"struct.std::__2::default_delete.166"** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.165"*, %"struct.std::__2::__compressed_pair_elem.165"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.165"* %this1 to %"struct.std::__2::default_delete.166"*
  %1 = load %"struct.std::__2::default_delete.166"*, %"struct.std::__2::default_delete.166"** %__u.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.166"* @_ZNSt3__27forwardINS_14default_deleteIN5draco4MeshEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.166"* nonnull align 1 dereferenceable(1) %1) #9
  ret %"struct.std::__2::__compressed_pair_elem.165"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::MeshDecoder"** @_ZNKSt3__217__compressed_pairIPN5draco11MeshDecoderENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.1"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.1"*, align 4
  store %"class.std::__2::__compressed_pair.1"* %this, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.1"*, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.1"* %this1 to %"struct.std::__2::__compressed_pair_elem.2"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::MeshDecoder"** @_ZNKSt3__222__compressed_pair_elemIPN5draco11MeshDecoderELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.2"* %0) #9
  ret %"class.draco::MeshDecoder"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::MeshDecoder"** @_ZNKSt3__222__compressed_pair_elemIPN5draco11MeshDecoderELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.2"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.2"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.2"* %this, %"struct.std::__2::__compressed_pair_elem.2"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.2"*, %"struct.std::__2::__compressed_pair_elem.2"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.2", %"struct.std::__2::__compressed_pair_elem.2"* %this1, i32 0, i32 0
  ret %"class.draco::MeshDecoder"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::Options"* @_ZN5draco12DracoOptionsINS_17GeometryAttribute4TypeEE19GetAttributeOptionsERKS2_(%"class.draco::DracoOptions"* %this, i32* nonnull align 4 dereferenceable(4) %att_key) #0 comdat {
entry:
  %retval = alloca %"class.draco::Options"*, align 4
  %this.addr = alloca %"class.draco::DracoOptions"*, align 4
  %att_key.addr = alloca i32*, align 4
  %it = alloca %"class.std::__2::__map_iterator", align 4
  %ref.tmp = alloca %"class.std::__2::__map_iterator", align 4
  %new_options = alloca %"class.draco::Options", align 4
  %ref.tmp10 = alloca %"struct.std::__2::pair.168", align 4
  %ref.tmp12 = alloca %"struct.std::__2::pair.169", align 4
  store %"class.draco::DracoOptions"* %this, %"class.draco::DracoOptions"** %this.addr, align 4
  store i32* %att_key, i32** %att_key.addr, align 4
  %this1 = load %"class.draco::DracoOptions"*, %"class.draco::DracoOptions"** %this.addr, align 4
  %attribute_options_ = getelementptr inbounds %"class.draco::DracoOptions", %"class.draco::DracoOptions"* %this1, i32 0, i32 1
  %0 = load i32*, i32** %att_key.addr, align 4
  %call = call %"class.std::__2::__tree_end_node"* @_ZNSt3__23mapIN5draco17GeometryAttribute4TypeENS1_7OptionsENS_4lessIS3_EENS_9allocatorINS_4pairIKS3_S4_EEEEE4findERS9_(%"class.std::__2::map.121"* %attribute_options_, i32* nonnull align 4 dereferenceable(4) %0)
  %coerce.dive = getelementptr inbounds %"class.std::__2::__map_iterator", %"class.std::__2::__map_iterator"* %it, i32 0, i32 0
  %coerce.dive2 = getelementptr inbounds %"class.std::__2::__tree_iterator", %"class.std::__2::__tree_iterator"* %coerce.dive, i32 0, i32 0
  store %"class.std::__2::__tree_end_node"* %call, %"class.std::__2::__tree_end_node"** %coerce.dive2, align 4
  %attribute_options_3 = getelementptr inbounds %"class.draco::DracoOptions", %"class.draco::DracoOptions"* %this1, i32 0, i32 1
  %call4 = call %"class.std::__2::__tree_end_node"* @_ZNSt3__23mapIN5draco17GeometryAttribute4TypeENS1_7OptionsENS_4lessIS3_EENS_9allocatorINS_4pairIKS3_S4_EEEEE3endEv(%"class.std::__2::map.121"* %attribute_options_3) #9
  %coerce.dive5 = getelementptr inbounds %"class.std::__2::__map_iterator", %"class.std::__2::__map_iterator"* %ref.tmp, i32 0, i32 0
  %coerce.dive6 = getelementptr inbounds %"class.std::__2::__tree_iterator", %"class.std::__2::__tree_iterator"* %coerce.dive5, i32 0, i32 0
  store %"class.std::__2::__tree_end_node"* %call4, %"class.std::__2::__tree_end_node"** %coerce.dive6, align 4
  %call7 = call zeroext i1 @_ZNSt3__2neERKNS_14__map_iteratorINS_15__tree_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPNS_11__tree_nodeIS7_PvEElEEEESF_(%"class.std::__2::__map_iterator"* nonnull align 4 dereferenceable(4) %it, %"class.std::__2::__map_iterator"* nonnull align 4 dereferenceable(4) %ref.tmp)
  br i1 %call7, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call8 = call %"struct.std::__2::pair"* @_ZNKSt3__214__map_iteratorINS_15__tree_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPNS_11__tree_nodeIS7_PvEElEEEptEv(%"class.std::__2::__map_iterator"* %it)
  %second = getelementptr inbounds %"struct.std::__2::pair", %"struct.std::__2::pair"* %call8, i32 0, i32 1
  store %"class.draco::Options"* %second, %"class.draco::Options"** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %call9 = call %"class.draco::Options"* @_ZN5draco7OptionsC1Ev(%"class.draco::Options"* %new_options)
  %attribute_options_11 = getelementptr inbounds %"class.draco::DracoOptions", %"class.draco::DracoOptions"* %this1, i32 0, i32 1
  %1 = load i32*, i32** %att_key.addr, align 4
  call void @_ZNSt3__29make_pairIRKN5draco17GeometryAttribute4TypeERNS1_7OptionsEEENS_4pairINS_18__unwrap_ref_decayIT_E4typeENS9_IT0_E4typeEEEOSA_OSD_(%"struct.std::__2::pair.169"* sret align 4 %ref.tmp12, i32* nonnull align 4 dereferenceable(4) %1, %"class.draco::Options"* nonnull align 4 dereferenceable(12) %new_options)
  call void @_ZNSt3__23mapIN5draco17GeometryAttribute4TypeENS1_7OptionsENS_4lessIS3_EENS_9allocatorINS_4pairIKS3_S4_EEEEE6insertINS8_IS3_S4_EEvEENS8_INS_14__map_iteratorINS_15__tree_iteratorINS_12__value_typeIS3_S4_EEPNS_11__tree_nodeISI_PvEElEEEEbEEOT_(%"struct.std::__2::pair.168"* sret align 4 %ref.tmp10, %"class.std::__2::map.121"* %attribute_options_11, %"struct.std::__2::pair.169"* nonnull align 4 dereferenceable(16) %ref.tmp12)
  %first = getelementptr inbounds %"struct.std::__2::pair.168", %"struct.std::__2::pair.168"* %ref.tmp10, i32 0, i32 0
  %2 = bitcast %"class.std::__2::__map_iterator"* %it to i8*
  %3 = bitcast %"class.std::__2::__map_iterator"* %first to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 4, i1 false)
  %call13 = call %"struct.std::__2::pair.169"* @_ZNSt3__24pairIN5draco17GeometryAttribute4TypeENS1_7OptionsEED2Ev(%"struct.std::__2::pair.169"* %ref.tmp12) #9
  %call14 = call %"struct.std::__2::pair"* @_ZNKSt3__214__map_iteratorINS_15__tree_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPNS_11__tree_nodeIS7_PvEElEEEptEv(%"class.std::__2::__map_iterator"* %it)
  %second15 = getelementptr inbounds %"struct.std::__2::pair", %"struct.std::__2::pair"* %call14, i32 0, i32 1
  store %"class.draco::Options"* %second15, %"class.draco::Options"** %retval, align 4
  %call16 = call %"class.draco::Options"* @_ZN5draco7OptionsD2Ev(%"class.draco::Options"* %new_options) #9
  br label %return

return:                                           ; preds = %if.end, %if.then
  %4 = load %"class.draco::Options"*, %"class.draco::Options"** %retval, align 4
  ret %"class.draco::Options"* %4
}

declare void @_ZN5draco7Options7SetBoolERKNSt3__212basic_stringIcNS1_11char_traitsIcEENS1_9allocatorIcEEEEb(%"class.draco::Options"*, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12), i1 zeroext) #2

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_end_node"* @_ZNSt3__23mapIN5draco17GeometryAttribute4TypeENS1_7OptionsENS_4lessIS3_EENS_9allocatorINS_4pairIKS3_S4_EEEEE4findERS9_(%"class.std::__2::map.121"* %this, i32* nonnull align 4 dereferenceable(4) %__k) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__map_iterator", align 4
  %this.addr = alloca %"class.std::__2::map.121"*, align 4
  %__k.addr = alloca i32*, align 4
  %agg.tmp = alloca %"class.std::__2::__tree_iterator", align 4
  store %"class.std::__2::map.121"* %this, %"class.std::__2::map.121"** %this.addr, align 4
  store i32* %__k, i32** %__k.addr, align 4
  %this1 = load %"class.std::__2::map.121"*, %"class.std::__2::map.121"** %this.addr, align 4
  %__tree_ = getelementptr inbounds %"class.std::__2::map.121", %"class.std::__2::map.121"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__k.addr, align 4
  %call = call %"class.std::__2::__tree_end_node"* @_ZNSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE4findIS4_EENS_15__tree_iteratorIS6_PNS_11__tree_nodeIS6_PvEElEERKT_(%"class.std::__2::__tree.122"* %__tree_, i32* nonnull align 4 dereferenceable(4) %0)
  %coerce.dive = getelementptr inbounds %"class.std::__2::__tree_iterator", %"class.std::__2::__tree_iterator"* %agg.tmp, i32 0, i32 0
  store %"class.std::__2::__tree_end_node"* %call, %"class.std::__2::__tree_end_node"** %coerce.dive, align 4
  %coerce.dive2 = getelementptr inbounds %"class.std::__2::__tree_iterator", %"class.std::__2::__tree_iterator"* %agg.tmp, i32 0, i32 0
  %1 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %coerce.dive2, align 4
  %call3 = call %"class.std::__2::__map_iterator"* @_ZNSt3__214__map_iteratorINS_15__tree_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPNS_11__tree_nodeIS7_PvEElEEEC2ESC_(%"class.std::__2::__map_iterator"* %retval, %"class.std::__2::__tree_end_node"* %1) #9
  %coerce.dive4 = getelementptr inbounds %"class.std::__2::__map_iterator", %"class.std::__2::__map_iterator"* %retval, i32 0, i32 0
  %coerce.dive5 = getelementptr inbounds %"class.std::__2::__tree_iterator", %"class.std::__2::__tree_iterator"* %coerce.dive4, i32 0, i32 0
  %2 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %coerce.dive5, align 4
  ret %"class.std::__2::__tree_end_node"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNSt3__2neERKNS_14__map_iteratorINS_15__tree_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPNS_11__tree_nodeIS7_PvEElEEEESF_(%"class.std::__2::__map_iterator"* nonnull align 4 dereferenceable(4) %__x, %"class.std::__2::__map_iterator"* nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %__x.addr = alloca %"class.std::__2::__map_iterator"*, align 4
  %__y.addr = alloca %"class.std::__2::__map_iterator"*, align 4
  store %"class.std::__2::__map_iterator"* %__x, %"class.std::__2::__map_iterator"** %__x.addr, align 4
  store %"class.std::__2::__map_iterator"* %__y, %"class.std::__2::__map_iterator"** %__y.addr, align 4
  %0 = load %"class.std::__2::__map_iterator"*, %"class.std::__2::__map_iterator"** %__x.addr, align 4
  %__i_ = getelementptr inbounds %"class.std::__2::__map_iterator", %"class.std::__2::__map_iterator"* %0, i32 0, i32 0
  %1 = load %"class.std::__2::__map_iterator"*, %"class.std::__2::__map_iterator"** %__y.addr, align 4
  %__i_1 = getelementptr inbounds %"class.std::__2::__map_iterator", %"class.std::__2::__map_iterator"* %1, i32 0, i32 0
  %call = call zeroext i1 @_ZNSt3__2neERKNS_15__tree_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEEPNS_11__tree_nodeIS6_PvEElEESD_(%"class.std::__2::__tree_iterator"* nonnull align 4 dereferenceable(4) %__i_, %"class.std::__2::__tree_iterator"* nonnull align 4 dereferenceable(4) %__i_1)
  ret i1 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_end_node"* @_ZNSt3__23mapIN5draco17GeometryAttribute4TypeENS1_7OptionsENS_4lessIS3_EENS_9allocatorINS_4pairIKS3_S4_EEEEE3endEv(%"class.std::__2::map.121"* %this) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__map_iterator", align 4
  %this.addr = alloca %"class.std::__2::map.121"*, align 4
  %agg.tmp = alloca %"class.std::__2::__tree_iterator", align 4
  store %"class.std::__2::map.121"* %this, %"class.std::__2::map.121"** %this.addr, align 4
  %this1 = load %"class.std::__2::map.121"*, %"class.std::__2::map.121"** %this.addr, align 4
  %__tree_ = getelementptr inbounds %"class.std::__2::map.121", %"class.std::__2::map.121"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::__tree_end_node"* @_ZNSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE3endEv(%"class.std::__2::__tree.122"* %__tree_) #9
  %coerce.dive = getelementptr inbounds %"class.std::__2::__tree_iterator", %"class.std::__2::__tree_iterator"* %agg.tmp, i32 0, i32 0
  store %"class.std::__2::__tree_end_node"* %call, %"class.std::__2::__tree_end_node"** %coerce.dive, align 4
  %coerce.dive2 = getelementptr inbounds %"class.std::__2::__tree_iterator", %"class.std::__2::__tree_iterator"* %agg.tmp, i32 0, i32 0
  %0 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %coerce.dive2, align 4
  %call3 = call %"class.std::__2::__map_iterator"* @_ZNSt3__214__map_iteratorINS_15__tree_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPNS_11__tree_nodeIS7_PvEElEEEC2ESC_(%"class.std::__2::__map_iterator"* %retval, %"class.std::__2::__tree_end_node"* %0) #9
  %coerce.dive4 = getelementptr inbounds %"class.std::__2::__map_iterator", %"class.std::__2::__map_iterator"* %retval, i32 0, i32 0
  %coerce.dive5 = getelementptr inbounds %"class.std::__2::__tree_iterator", %"class.std::__2::__tree_iterator"* %coerce.dive4, i32 0, i32 0
  %1 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %coerce.dive5, align 4
  ret %"class.std::__2::__tree_end_node"* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::pair"* @_ZNKSt3__214__map_iteratorINS_15__tree_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPNS_11__tree_nodeIS7_PvEElEEEptEv(%"class.std::__2::__map_iterator"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__map_iterator"*, align 4
  store %"class.std::__2::__map_iterator"* %this, %"class.std::__2::__map_iterator"** %this.addr, align 4
  %this1 = load %"class.std::__2::__map_iterator"*, %"class.std::__2::__map_iterator"** %this.addr, align 4
  %__i_ = getelementptr inbounds %"class.std::__2::__map_iterator", %"class.std::__2::__map_iterator"* %this1, i32 0, i32 0
  %call = call %"struct.std::__2::__value_type"* @_ZNKSt3__215__tree_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEEPNS_11__tree_nodeIS6_PvEElEptEv(%"class.std::__2::__tree_iterator"* %__i_)
  %call2 = call nonnull align 4 dereferenceable(16) %"struct.std::__2::pair"* @_ZNSt3__212__value_typeIN5draco17GeometryAttribute4TypeENS1_7OptionsEE11__get_valueEv(%"struct.std::__2::__value_type"* %call)
  %call3 = call %"struct.std::__2::pair"* @_ZNSt3__214pointer_traitsIPNS_4pairIKN5draco17GeometryAttribute4TypeENS2_7OptionsEEEE10pointer_toERS7_(%"struct.std::__2::pair"* nonnull align 4 dereferenceable(16) %call2) #9
  ret %"struct.std::__2::pair"* %call3
}

declare %"class.draco::Options"* @_ZN5draco7OptionsC1Ev(%"class.draco::Options"* returned) unnamed_addr #2

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__23mapIN5draco17GeometryAttribute4TypeENS1_7OptionsENS_4lessIS3_EENS_9allocatorINS_4pairIKS3_S4_EEEEE6insertINS8_IS3_S4_EEvEENS8_INS_14__map_iteratorINS_15__tree_iteratorINS_12__value_typeIS3_S4_EEPNS_11__tree_nodeISI_PvEElEEEEbEEOT_(%"struct.std::__2::pair.168"* noalias sret align 4 %agg.result, %"class.std::__2::map.121"* %this, %"struct.std::__2::pair.169"* nonnull align 4 dereferenceable(16) %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::map.121"*, align 4
  %__p.addr = alloca %"struct.std::__2::pair.169"*, align 4
  %ref.tmp = alloca %"struct.std::__2::pair.170", align 4
  store %"class.std::__2::map.121"* %this, %"class.std::__2::map.121"** %this.addr, align 4
  store %"struct.std::__2::pair.169"* %__p, %"struct.std::__2::pair.169"** %__p.addr, align 4
  %this1 = load %"class.std::__2::map.121"*, %"class.std::__2::map.121"** %this.addr, align 4
  %__tree_ = getelementptr inbounds %"class.std::__2::map.121", %"class.std::__2::map.121"* %this1, i32 0, i32 0
  %0 = load %"struct.std::__2::pair.169"*, %"struct.std::__2::pair.169"** %__p.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %"struct.std::__2::pair.169"* @_ZNSt3__27forwardINS_4pairIN5draco17GeometryAttribute4TypeENS2_7OptionsEEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"struct.std::__2::pair.169"* nonnull align 4 dereferenceable(16) %0) #9
  call void @_ZNSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE15__insert_uniqueINS_4pairIS4_S5_EEvEENSF_INS_15__tree_iteratorIS6_PNS_11__tree_nodeIS6_PvEElEEbEEOT_(%"struct.std::__2::pair.170"* sret align 4 %ref.tmp, %"class.std::__2::__tree.122"* %__tree_, %"struct.std::__2::pair.169"* nonnull align 4 dereferenceable(16) %call)
  %call2 = call %"struct.std::__2::pair.168"* @_ZNSt3__24pairINS_14__map_iteratorINS_15__tree_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS4_7OptionsEEEPNS_11__tree_nodeIS8_PvEElEEEEbEC2ISD_bLb0EEEONS0_IT_T0_EE(%"struct.std::__2::pair.168"* %agg.result, %"struct.std::__2::pair.170"* nonnull align 4 dereferenceable(5) %ref.tmp) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29make_pairIRKN5draco17GeometryAttribute4TypeERNS1_7OptionsEEENS_4pairINS_18__unwrap_ref_decayIT_E4typeENS9_IT0_E4typeEEEOSA_OSD_(%"struct.std::__2::pair.169"* noalias sret align 4 %agg.result, i32* nonnull align 4 dereferenceable(4) %__t1, %"class.draco::Options"* nonnull align 4 dereferenceable(12) %__t2) #0 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %__t1.addr = alloca i32*, align 4
  %__t2.addr = alloca %"class.draco::Options"*, align 4
  %0 = bitcast %"struct.std::__2::pair.169"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store i32* %__t1, i32** %__t1.addr, align 4
  store %"class.draco::Options"* %__t2, %"class.draco::Options"** %__t2.addr, align 4
  %1 = load i32*, i32** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__27forwardIRKN5draco17GeometryAttribute4TypeEEEOT_RNS_16remove_referenceIS6_E4typeE(i32* nonnull align 4 dereferenceable(4) %1) #9
  %2 = load %"class.draco::Options"*, %"class.draco::Options"** %__t2.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(12) %"class.draco::Options"* @_ZNSt3__27forwardIRN5draco7OptionsEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::Options"* nonnull align 4 dereferenceable(12) %2) #9
  %call2 = call %"struct.std::__2::pair.169"* @_ZNSt3__24pairIN5draco17GeometryAttribute4TypeENS1_7OptionsEEC2IRKS3_RS4_Lb0EEEOT_OT0_(%"struct.std::__2::pair.169"* %agg.result, i32* nonnull align 4 dereferenceable(4) %call, %"class.draco::Options"* nonnull align 4 dereferenceable(12) %call1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::pair.169"* @_ZNSt3__24pairIN5draco17GeometryAttribute4TypeENS1_7OptionsEED2Ev(%"struct.std::__2::pair.169"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::pair.169"*, align 4
  store %"struct.std::__2::pair.169"* %this, %"struct.std::__2::pair.169"** %this.addr, align 4
  %this1 = load %"struct.std::__2::pair.169"*, %"struct.std::__2::pair.169"** %this.addr, align 4
  %second = getelementptr inbounds %"struct.std::__2::pair.169", %"struct.std::__2::pair.169"* %this1, i32 0, i32 1
  %call = call %"class.draco::Options"* @_ZN5draco7OptionsD2Ev(%"class.draco::Options"* %second) #9
  ret %"struct.std::__2::pair.169"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::Options"* @_ZN5draco7OptionsD2Ev(%"class.draco::Options"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::Options"*, align 4
  store %"class.draco::Options"* %this, %"class.draco::Options"** %this.addr, align 4
  %this1 = load %"class.draco::Options"*, %"class.draco::Options"** %this.addr, align 4
  %options_ = getelementptr inbounds %"class.draco::Options", %"class.draco::Options"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::map"* @_ZNSt3__23mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_NS_4lessIS6_EENS4_INS_4pairIKS6_S6_EEEEED2Ev(%"class.std::__2::map"* %options_) #9
  ret %"class.draco::Options"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_end_node"* @_ZNSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE4findIS4_EENS_15__tree_iteratorIS6_PNS_11__tree_nodeIS6_PvEElEERKT_(%"class.std::__2::__tree.122"* %this, i32* nonnull align 4 dereferenceable(4) %__v) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__tree_iterator", align 4
  %this.addr = alloca %"class.std::__2::__tree.122"*, align 4
  %__v.addr = alloca i32*, align 4
  %__p = alloca %"class.std::__2::__tree_iterator", align 4
  %ref.tmp = alloca %"class.std::__2::__tree_iterator", align 4
  store %"class.std::__2::__tree.122"* %this, %"class.std::__2::__tree.122"** %this.addr, align 4
  store i32* %__v, i32** %__v.addr, align 4
  %this1 = load %"class.std::__2::__tree.122"*, %"class.std::__2::__tree.122"** %this.addr, align 4
  %0 = load i32*, i32** %__v.addr, align 4
  %call = call %"class.std::__2::__tree_node"* @_ZNKSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE6__rootEv(%"class.std::__2::__tree.122"* %this1) #9
  %call2 = call %"class.std::__2::__tree_end_node"* @_ZNSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE10__end_nodeEv(%"class.std::__2::__tree.122"* %this1) #9
  %call3 = call %"class.std::__2::__tree_end_node"* @_ZNSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE13__lower_boundIS4_EENS_15__tree_iteratorIS6_PNS_11__tree_nodeIS6_PvEElEERKT_SJ_PNS_15__tree_end_nodeIPNS_16__tree_node_baseISH_EEEE(%"class.std::__2::__tree.122"* %this1, i32* nonnull align 4 dereferenceable(4) %0, %"class.std::__2::__tree_node"* %call, %"class.std::__2::__tree_end_node"* %call2)
  %coerce.dive = getelementptr inbounds %"class.std::__2::__tree_iterator", %"class.std::__2::__tree_iterator"* %__p, i32 0, i32 0
  store %"class.std::__2::__tree_end_node"* %call3, %"class.std::__2::__tree_end_node"** %coerce.dive, align 4
  %call4 = call %"class.std::__2::__tree_end_node"* @_ZNSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE3endEv(%"class.std::__2::__tree.122"* %this1) #9
  %coerce.dive5 = getelementptr inbounds %"class.std::__2::__tree_iterator", %"class.std::__2::__tree_iterator"* %ref.tmp, i32 0, i32 0
  store %"class.std::__2::__tree_end_node"* %call4, %"class.std::__2::__tree_end_node"** %coerce.dive5, align 4
  %call6 = call zeroext i1 @_ZNSt3__2neERKNS_15__tree_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEEPNS_11__tree_nodeIS6_PvEElEESD_(%"class.std::__2::__tree_iterator"* nonnull align 4 dereferenceable(4) %__p, %"class.std::__2::__tree_iterator"* nonnull align 4 dereferenceable(4) %ref.tmp)
  br i1 %call6, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %call7 = call nonnull align 1 dereferenceable(1) %"class.std::__2::__map_value_compare.129"* @_ZNSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE10value_compEv(%"class.std::__2::__tree.122"* %this1) #9
  %1 = load i32*, i32** %__v.addr, align 4
  %call8 = call nonnull align 4 dereferenceable(16) %"struct.std::__2::__value_type"* @_ZNKSt3__215__tree_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEEPNS_11__tree_nodeIS6_PvEElEdeEv(%"class.std::__2::__tree_iterator"* %__p)
  %call9 = call zeroext i1 @_ZNKSt3__219__map_value_compareIN5draco17GeometryAttribute4TypeENS_12__value_typeIS3_NS1_7OptionsEEENS_4lessIS3_EELb1EEclERKS3_RKS6_(%"class.std::__2::__map_value_compare.129"* %call7, i32* nonnull align 4 dereferenceable(4) %1, %"struct.std::__2::__value_type"* nonnull align 4 dereferenceable(16) %call8)
  %lnot = xor i1 %call9, true
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %2 = phi i1 [ false, %entry ], [ %lnot, %land.rhs ]
  br i1 %2, label %if.then, label %if.end

if.then:                                          ; preds = %land.end
  %3 = bitcast %"class.std::__2::__tree_iterator"* %retval to i8*
  %4 = bitcast %"class.std::__2::__tree_iterator"* %__p to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 4, i1 false)
  br label %return

if.end:                                           ; preds = %land.end
  %call10 = call %"class.std::__2::__tree_end_node"* @_ZNSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE3endEv(%"class.std::__2::__tree.122"* %this1) #9
  %coerce.dive11 = getelementptr inbounds %"class.std::__2::__tree_iterator", %"class.std::__2::__tree_iterator"* %retval, i32 0, i32 0
  store %"class.std::__2::__tree_end_node"* %call10, %"class.std::__2::__tree_end_node"** %coerce.dive11, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %coerce.dive12 = getelementptr inbounds %"class.std::__2::__tree_iterator", %"class.std::__2::__tree_iterator"* %retval, i32 0, i32 0
  %5 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %coerce.dive12, align 4
  ret %"class.std::__2::__tree_end_node"* %5
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__map_iterator"* @_ZNSt3__214__map_iteratorINS_15__tree_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPNS_11__tree_nodeIS7_PvEElEEEC2ESC_(%"class.std::__2::__map_iterator"* returned %this, %"class.std::__2::__tree_end_node"* %__i.coerce) unnamed_addr #0 comdat {
entry:
  %__i = alloca %"class.std::__2::__tree_iterator", align 4
  %this.addr = alloca %"class.std::__2::__map_iterator"*, align 4
  %coerce.dive = getelementptr inbounds %"class.std::__2::__tree_iterator", %"class.std::__2::__tree_iterator"* %__i, i32 0, i32 0
  store %"class.std::__2::__tree_end_node"* %__i.coerce, %"class.std::__2::__tree_end_node"** %coerce.dive, align 4
  store %"class.std::__2::__map_iterator"* %this, %"class.std::__2::__map_iterator"** %this.addr, align 4
  %this1 = load %"class.std::__2::__map_iterator"*, %"class.std::__2::__map_iterator"** %this.addr, align 4
  %__i_ = getelementptr inbounds %"class.std::__2::__map_iterator", %"class.std::__2::__map_iterator"* %this1, i32 0, i32 0
  %0 = bitcast %"class.std::__2::__tree_iterator"* %__i_ to i8*
  %1 = bitcast %"class.std::__2::__tree_iterator"* %__i to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %0, i8* align 4 %1, i32 4, i1 false)
  ret %"class.std::__2::__map_iterator"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_end_node"* @_ZNSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE13__lower_boundIS4_EENS_15__tree_iteratorIS6_PNS_11__tree_nodeIS6_PvEElEERKT_SJ_PNS_15__tree_end_nodeIPNS_16__tree_node_baseISH_EEEE(%"class.std::__2::__tree.122"* %this, i32* nonnull align 4 dereferenceable(4) %__v, %"class.std::__2::__tree_node"* %__root, %"class.std::__2::__tree_end_node"* %__result) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__tree_iterator", align 4
  %this.addr = alloca %"class.std::__2::__tree.122"*, align 4
  %__v.addr = alloca i32*, align 4
  %__root.addr = alloca %"class.std::__2::__tree_node"*, align 4
  %__result.addr = alloca %"class.std::__2::__tree_end_node"*, align 4
  store %"class.std::__2::__tree.122"* %this, %"class.std::__2::__tree.122"** %this.addr, align 4
  store i32* %__v, i32** %__v.addr, align 4
  store %"class.std::__2::__tree_node"* %__root, %"class.std::__2::__tree_node"** %__root.addr, align 4
  store %"class.std::__2::__tree_end_node"* %__result, %"class.std::__2::__tree_end_node"** %__result.addr, align 4
  %this1 = load %"class.std::__2::__tree.122"*, %"class.std::__2::__tree.122"** %this.addr, align 4
  br label %while.cond

while.cond:                                       ; preds = %if.end, %entry
  %0 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %__root.addr, align 4
  %cmp = icmp ne %"class.std::__2::__tree_node"* %0, null
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::__map_value_compare.129"* @_ZNSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE10value_compEv(%"class.std::__2::__tree.122"* %this1) #9
  %1 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %__root.addr, align 4
  %__value_ = getelementptr inbounds %"class.std::__2::__tree_node", %"class.std::__2::__tree_node"* %1, i32 0, i32 1
  %2 = load i32*, i32** %__v.addr, align 4
  %call2 = call zeroext i1 @_ZNKSt3__219__map_value_compareIN5draco17GeometryAttribute4TypeENS_12__value_typeIS3_NS1_7OptionsEEENS_4lessIS3_EELb1EEclERKS6_RKS3_(%"class.std::__2::__map_value_compare.129"* %call, %"struct.std::__2::__value_type"* nonnull align 4 dereferenceable(16) %__value_, i32* nonnull align 4 dereferenceable(4) %2)
  br i1 %call2, label %if.else, label %if.then

if.then:                                          ; preds = %while.body
  %3 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %__root.addr, align 4
  %4 = bitcast %"class.std::__2::__tree_node"* %3 to %"class.std::__2::__tree_end_node"*
  store %"class.std::__2::__tree_end_node"* %4, %"class.std::__2::__tree_end_node"** %__result.addr, align 4
  %5 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %__root.addr, align 4
  %6 = bitcast %"class.std::__2::__tree_node"* %5 to %"class.std::__2::__tree_end_node"*
  %__left_ = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %6, i32 0, i32 0
  %7 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__left_, align 4
  %8 = bitcast %"class.std::__2::__tree_node_base"* %7 to %"class.std::__2::__tree_node"*
  store %"class.std::__2::__tree_node"* %8, %"class.std::__2::__tree_node"** %__root.addr, align 4
  br label %if.end

if.else:                                          ; preds = %while.body
  %9 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %__root.addr, align 4
  %10 = bitcast %"class.std::__2::__tree_node"* %9 to %"class.std::__2::__tree_node_base"*
  %__right_ = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %10, i32 0, i32 1
  %11 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__right_, align 4
  %12 = bitcast %"class.std::__2::__tree_node_base"* %11 to %"class.std::__2::__tree_node"*
  store %"class.std::__2::__tree_node"* %12, %"class.std::__2::__tree_node"** %__root.addr, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %13 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %__result.addr, align 4
  %call3 = call %"class.std::__2::__tree_iterator"* @_ZNSt3__215__tree_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEEPNS_11__tree_nodeIS6_PvEElEC2EPNS_15__tree_end_nodeIPNS_16__tree_node_baseIS8_EEEE(%"class.std::__2::__tree_iterator"* %retval, %"class.std::__2::__tree_end_node"* %13) #9
  %coerce.dive = getelementptr inbounds %"class.std::__2::__tree_iterator", %"class.std::__2::__tree_iterator"* %retval, i32 0, i32 0
  %14 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %coerce.dive, align 4
  ret %"class.std::__2::__tree_end_node"* %14
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_node"* @_ZNKSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE6__rootEv(%"class.std::__2::__tree.122"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree.122"*, align 4
  store %"class.std::__2::__tree.122"* %this, %"class.std::__2::__tree.122"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree.122"*, %"class.std::__2::__tree.122"** %this.addr, align 4
  %call = call %"class.std::__2::__tree_end_node"* @_ZNKSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE10__end_nodeEv(%"class.std::__2::__tree.122"* %this1) #9
  %__left_ = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %call, i32 0, i32 0
  %0 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__left_, align 4
  %1 = bitcast %"class.std::__2::__tree_node_base"* %0 to %"class.std::__2::__tree_node"*
  ret %"class.std::__2::__tree_node"* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_end_node"* @_ZNSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE10__end_nodeEv(%"class.std::__2::__tree.122"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree.122"*, align 4
  store %"class.std::__2::__tree.122"* %this, %"class.std::__2::__tree.122"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree.122"*, %"class.std::__2::__tree.122"** %this.addr, align 4
  %__pair1_ = getelementptr inbounds %"class.std::__2::__tree.122", %"class.std::__2::__tree.122"* %this1, i32 0, i32 1
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_end_node"* @_ZNSt3__217__compressed_pairINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEENS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENSA_7OptionsEEES3_EEEEE5firstEv(%"class.std::__2::__compressed_pair.123"* %__pair1_) #9
  %call2 = call %"class.std::__2::__tree_end_node"* @_ZNSt3__214pointer_traitsIPNS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEEE10pointer_toERS6_(%"class.std::__2::__tree_end_node"* nonnull align 4 dereferenceable(4) %call) #9
  ret %"class.std::__2::__tree_end_node"* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNSt3__2neERKNS_15__tree_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEEPNS_11__tree_nodeIS6_PvEElEESD_(%"class.std::__2::__tree_iterator"* nonnull align 4 dereferenceable(4) %__x, %"class.std::__2::__tree_iterator"* nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %__x.addr = alloca %"class.std::__2::__tree_iterator"*, align 4
  %__y.addr = alloca %"class.std::__2::__tree_iterator"*, align 4
  store %"class.std::__2::__tree_iterator"* %__x, %"class.std::__2::__tree_iterator"** %__x.addr, align 4
  store %"class.std::__2::__tree_iterator"* %__y, %"class.std::__2::__tree_iterator"** %__y.addr, align 4
  %0 = load %"class.std::__2::__tree_iterator"*, %"class.std::__2::__tree_iterator"** %__x.addr, align 4
  %1 = load %"class.std::__2::__tree_iterator"*, %"class.std::__2::__tree_iterator"** %__y.addr, align 4
  %call = call zeroext i1 @_ZNSt3__2eqERKNS_15__tree_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEEPNS_11__tree_nodeIS6_PvEElEESD_(%"class.std::__2::__tree_iterator"* nonnull align 4 dereferenceable(4) %0, %"class.std::__2::__tree_iterator"* nonnull align 4 dereferenceable(4) %1)
  %lnot = xor i1 %call, true
  ret i1 %lnot
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_end_node"* @_ZNSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE3endEv(%"class.std::__2::__tree.122"* %this) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__tree_iterator", align 4
  %this.addr = alloca %"class.std::__2::__tree.122"*, align 4
  store %"class.std::__2::__tree.122"* %this, %"class.std::__2::__tree.122"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree.122"*, %"class.std::__2::__tree.122"** %this.addr, align 4
  %call = call %"class.std::__2::__tree_end_node"* @_ZNSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE10__end_nodeEv(%"class.std::__2::__tree.122"* %this1) #9
  %call2 = call %"class.std::__2::__tree_iterator"* @_ZNSt3__215__tree_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEEPNS_11__tree_nodeIS6_PvEElEC2EPNS_15__tree_end_nodeIPNS_16__tree_node_baseIS8_EEEE(%"class.std::__2::__tree_iterator"* %retval, %"class.std::__2::__tree_end_node"* %call) #9
  %coerce.dive = getelementptr inbounds %"class.std::__2::__tree_iterator", %"class.std::__2::__tree_iterator"* %retval, i32 0, i32 0
  %0 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %coerce.dive, align 4
  ret %"class.std::__2::__tree_end_node"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::__map_value_compare.129"* @_ZNSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE10value_compEv(%"class.std::__2::__tree.122"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree.122"*, align 4
  store %"class.std::__2::__tree.122"* %this, %"class.std::__2::__tree.122"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree.122"*, %"class.std::__2::__tree.122"** %this.addr, align 4
  %__pair3_ = getelementptr inbounds %"class.std::__2::__tree.122", %"class.std::__2::__tree.122"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::__map_value_compare.129"* @_ZNSt3__217__compressed_pairImNS_19__map_value_compareIN5draco17GeometryAttribute4TypeENS_12__value_typeIS4_NS2_7OptionsEEENS_4lessIS4_EELb1EEEE6secondEv(%"class.std::__2::__compressed_pair.127"* %__pair3_) #9
  ret %"class.std::__2::__map_value_compare.129"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__219__map_value_compareIN5draco17GeometryAttribute4TypeENS_12__value_typeIS3_NS1_7OptionsEEENS_4lessIS3_EELb1EEclERKS3_RKS6_(%"class.std::__2::__map_value_compare.129"* %this, i32* nonnull align 4 dereferenceable(4) %__x, %"struct.std::__2::__value_type"* nonnull align 4 dereferenceable(16) %__y) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__map_value_compare.129"*, align 4
  %__x.addr = alloca i32*, align 4
  %__y.addr = alloca %"struct.std::__2::__value_type"*, align 4
  store %"class.std::__2::__map_value_compare.129"* %this, %"class.std::__2::__map_value_compare.129"** %this.addr, align 4
  store i32* %__x, i32** %__x.addr, align 4
  store %"struct.std::__2::__value_type"* %__y, %"struct.std::__2::__value_type"** %__y.addr, align 4
  %this1 = load %"class.std::__2::__map_value_compare.129"*, %"class.std::__2::__map_value_compare.129"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__map_value_compare.129"* %this1 to %"struct.std::__2::less.130"*
  %1 = load i32*, i32** %__x.addr, align 4
  %2 = load %"struct.std::__2::__value_type"*, %"struct.std::__2::__value_type"** %__y.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %"struct.std::__2::pair"* @_ZNKSt3__212__value_typeIN5draco17GeometryAttribute4TypeENS1_7OptionsEE11__get_valueEv(%"struct.std::__2::__value_type"* %2)
  %first = getelementptr inbounds %"struct.std::__2::pair", %"struct.std::__2::pair"* %call, i32 0, i32 0
  %call2 = call zeroext i1 @_ZNKSt3__24lessIN5draco17GeometryAttribute4TypeEEclERKS3_S6_(%"struct.std::__2::less.130"* %0, i32* nonnull align 4 dereferenceable(4) %1, i32* nonnull align 4 dereferenceable(4) %first)
  ret i1 %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %"struct.std::__2::__value_type"* @_ZNKSt3__215__tree_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEEPNS_11__tree_nodeIS6_PvEElEdeEv(%"class.std::__2::__tree_iterator"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree_iterator"*, align 4
  store %"class.std::__2::__tree_iterator"* %this, %"class.std::__2::__tree_iterator"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree_iterator"*, %"class.std::__2::__tree_iterator"** %this.addr, align 4
  %call = call %"class.std::__2::__tree_node"* @_ZNKSt3__215__tree_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEEPNS_11__tree_nodeIS6_PvEElE8__get_npEv(%"class.std::__2::__tree_iterator"* %this1)
  %__value_ = getelementptr inbounds %"class.std::__2::__tree_node", %"class.std::__2::__tree_node"* %call, i32 0, i32 1
  ret %"struct.std::__2::__value_type"* %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__219__map_value_compareIN5draco17GeometryAttribute4TypeENS_12__value_typeIS3_NS1_7OptionsEEENS_4lessIS3_EELb1EEclERKS6_RKS3_(%"class.std::__2::__map_value_compare.129"* %this, %"struct.std::__2::__value_type"* nonnull align 4 dereferenceable(16) %__x, i32* nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__map_value_compare.129"*, align 4
  %__x.addr = alloca %"struct.std::__2::__value_type"*, align 4
  %__y.addr = alloca i32*, align 4
  store %"class.std::__2::__map_value_compare.129"* %this, %"class.std::__2::__map_value_compare.129"** %this.addr, align 4
  store %"struct.std::__2::__value_type"* %__x, %"struct.std::__2::__value_type"** %__x.addr, align 4
  store i32* %__y, i32** %__y.addr, align 4
  %this1 = load %"class.std::__2::__map_value_compare.129"*, %"class.std::__2::__map_value_compare.129"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__map_value_compare.129"* %this1 to %"struct.std::__2::less.130"*
  %1 = load %"struct.std::__2::__value_type"*, %"struct.std::__2::__value_type"** %__x.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %"struct.std::__2::pair"* @_ZNKSt3__212__value_typeIN5draco17GeometryAttribute4TypeENS1_7OptionsEE11__get_valueEv(%"struct.std::__2::__value_type"* %1)
  %first = getelementptr inbounds %"struct.std::__2::pair", %"struct.std::__2::pair"* %call, i32 0, i32 0
  %2 = load i32*, i32** %__y.addr, align 4
  %call2 = call zeroext i1 @_ZNKSt3__24lessIN5draco17GeometryAttribute4TypeEEclERKS3_S6_(%"struct.std::__2::less.130"* %0, i32* nonnull align 4 dereferenceable(4) %first, i32* nonnull align 4 dereferenceable(4) %2)
  ret i1 %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_iterator"* @_ZNSt3__215__tree_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEEPNS_11__tree_nodeIS6_PvEElEC2EPNS_15__tree_end_nodeIPNS_16__tree_node_baseIS8_EEEE(%"class.std::__2::__tree_iterator"* returned %this, %"class.std::__2::__tree_end_node"* %__p) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree_iterator"*, align 4
  %__p.addr = alloca %"class.std::__2::__tree_end_node"*, align 4
  store %"class.std::__2::__tree_iterator"* %this, %"class.std::__2::__tree_iterator"** %this.addr, align 4
  store %"class.std::__2::__tree_end_node"* %__p, %"class.std::__2::__tree_end_node"** %__p.addr, align 4
  %this1 = load %"class.std::__2::__tree_iterator"*, %"class.std::__2::__tree_iterator"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::__tree_iterator", %"class.std::__2::__tree_iterator"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %__p.addr, align 4
  store %"class.std::__2::__tree_end_node"* %0, %"class.std::__2::__tree_end_node"** %__ptr_, align 4
  ret %"class.std::__2::__tree_iterator"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__24lessIN5draco17GeometryAttribute4TypeEEclERKS3_S6_(%"struct.std::__2::less.130"* %this, i32* nonnull align 4 dereferenceable(4) %__x, i32* nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::less.130"*, align 4
  %__x.addr = alloca i32*, align 4
  %__y.addr = alloca i32*, align 4
  store %"struct.std::__2::less.130"* %this, %"struct.std::__2::less.130"** %this.addr, align 4
  store i32* %__x, i32** %__x.addr, align 4
  store i32* %__y, i32** %__y.addr, align 4
  %this1 = load %"struct.std::__2::less.130"*, %"struct.std::__2::less.130"** %this.addr, align 4
  %0 = load i32*, i32** %__x.addr, align 4
  %1 = load i32, i32* %0, align 4
  %2 = load i32*, i32** %__y.addr, align 4
  %3 = load i32, i32* %2, align 4
  %cmp = icmp slt i32 %1, %3
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %"struct.std::__2::pair"* @_ZNKSt3__212__value_typeIN5draco17GeometryAttribute4TypeENS1_7OptionsEE11__get_valueEv(%"struct.std::__2::__value_type"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__value_type"*, align 4
  store %"struct.std::__2::__value_type"* %this, %"struct.std::__2::__value_type"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__value_type"*, %"struct.std::__2::__value_type"** %this.addr, align 4
  %__cc = getelementptr inbounds %"struct.std::__2::__value_type", %"struct.std::__2::__value_type"* %this1, i32 0, i32 0
  ret %"struct.std::__2::pair"* %__cc
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_end_node"* @_ZNKSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE10__end_nodeEv(%"class.std::__2::__tree.122"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree.122"*, align 4
  store %"class.std::__2::__tree.122"* %this, %"class.std::__2::__tree.122"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree.122"*, %"class.std::__2::__tree.122"** %this.addr, align 4
  %__pair1_ = getelementptr inbounds %"class.std::__2::__tree.122", %"class.std::__2::__tree.122"* %this1, i32 0, i32 1
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_end_node"* @_ZNKSt3__217__compressed_pairINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEENS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENSA_7OptionsEEES3_EEEEE5firstEv(%"class.std::__2::__compressed_pair.123"* %__pair1_) #9
  %call2 = call %"class.std::__2::__tree_end_node"* @_ZNSt3__214pointer_traitsIPNS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEEE10pointer_toERS6_(%"class.std::__2::__tree_end_node"* nonnull align 4 dereferenceable(4) %call) #9
  ret %"class.std::__2::__tree_end_node"* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_end_node"* @_ZNSt3__214pointer_traitsIPNS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEEE10pointer_toERS6_(%"class.std::__2::__tree_end_node"* nonnull align 4 dereferenceable(4) %__r) #0 comdat {
entry:
  %__r.addr = alloca %"class.std::__2::__tree_end_node"*, align 4
  store %"class.std::__2::__tree_end_node"* %__r, %"class.std::__2::__tree_end_node"** %__r.addr, align 4
  %0 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %__r.addr, align 4
  %call = call %"class.std::__2::__tree_end_node"* @_ZNSt3__29addressofINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEEEEPT_RS7_(%"class.std::__2::__tree_end_node"* nonnull align 4 dereferenceable(4) %0) #9
  ret %"class.std::__2::__tree_end_node"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_end_node"* @_ZNKSt3__217__compressed_pairINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEENS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENSA_7OptionsEEES3_EEEEE5firstEv(%"class.std::__2::__compressed_pair.123"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.123"*, align 4
  store %"class.std::__2::__compressed_pair.123"* %this, %"class.std::__2::__compressed_pair.123"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.123"*, %"class.std::__2::__compressed_pair.123"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.123"* %this1 to %"struct.std::__2::__compressed_pair_elem.115"*
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_end_node"* @_ZNKSt3__222__compressed_pair_elemINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.115"* %0) #9
  ret %"class.std::__2::__tree_end_node"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_end_node"* @_ZNSt3__29addressofINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEEEEPT_RS7_(%"class.std::__2::__tree_end_node"* nonnull align 4 dereferenceable(4) %__x) #0 comdat {
entry:
  %__x.addr = alloca %"class.std::__2::__tree_end_node"*, align 4
  store %"class.std::__2::__tree_end_node"* %__x, %"class.std::__2::__tree_end_node"** %__x.addr, align 4
  %0 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %__x.addr, align 4
  ret %"class.std::__2::__tree_end_node"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_end_node"* @_ZNKSt3__222__compressed_pair_elemINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.115"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.115"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.115"* %this, %"struct.std::__2::__compressed_pair_elem.115"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.115"*, %"struct.std::__2::__compressed_pair_elem.115"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.115", %"struct.std::__2::__compressed_pair_elem.115"* %this1, i32 0, i32 0
  ret %"class.std::__2::__tree_end_node"* %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_end_node"* @_ZNSt3__217__compressed_pairINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEENS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENSA_7OptionsEEES3_EEEEE5firstEv(%"class.std::__2::__compressed_pair.123"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.123"*, align 4
  store %"class.std::__2::__compressed_pair.123"* %this, %"class.std::__2::__compressed_pair.123"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.123"*, %"class.std::__2::__compressed_pair.123"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.123"* %this1 to %"struct.std::__2::__compressed_pair_elem.115"*
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_end_node"* @_ZNSt3__222__compressed_pair_elemINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.115"* %0) #9
  ret %"class.std::__2::__tree_end_node"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_end_node"* @_ZNSt3__222__compressed_pair_elemINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.115"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.115"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.115"* %this, %"struct.std::__2::__compressed_pair_elem.115"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.115"*, %"struct.std::__2::__compressed_pair_elem.115"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.115", %"struct.std::__2::__compressed_pair_elem.115"* %this1, i32 0, i32 0
  ret %"class.std::__2::__tree_end_node"* %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNSt3__2eqERKNS_15__tree_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEEPNS_11__tree_nodeIS6_PvEElEESD_(%"class.std::__2::__tree_iterator"* nonnull align 4 dereferenceable(4) %__x, %"class.std::__2::__tree_iterator"* nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %__x.addr = alloca %"class.std::__2::__tree_iterator"*, align 4
  %__y.addr = alloca %"class.std::__2::__tree_iterator"*, align 4
  store %"class.std::__2::__tree_iterator"* %__x, %"class.std::__2::__tree_iterator"** %__x.addr, align 4
  store %"class.std::__2::__tree_iterator"* %__y, %"class.std::__2::__tree_iterator"** %__y.addr, align 4
  %0 = load %"class.std::__2::__tree_iterator"*, %"class.std::__2::__tree_iterator"** %__x.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::__tree_iterator", %"class.std::__2::__tree_iterator"* %0, i32 0, i32 0
  %1 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %__ptr_, align 4
  %2 = load %"class.std::__2::__tree_iterator"*, %"class.std::__2::__tree_iterator"** %__y.addr, align 4
  %__ptr_1 = getelementptr inbounds %"class.std::__2::__tree_iterator", %"class.std::__2::__tree_iterator"* %2, i32 0, i32 0
  %3 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %__ptr_1, align 4
  %cmp = icmp eq %"class.std::__2::__tree_end_node"* %1, %3
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::__map_value_compare.129"* @_ZNSt3__217__compressed_pairImNS_19__map_value_compareIN5draco17GeometryAttribute4TypeENS_12__value_typeIS4_NS2_7OptionsEEENS_4lessIS4_EELb1EEEE6secondEv(%"class.std::__2::__compressed_pair.127"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.127"*, align 4
  store %"class.std::__2::__compressed_pair.127"* %this, %"class.std::__2::__compressed_pair.127"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.127"*, %"class.std::__2::__compressed_pair.127"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.127"* %this1 to %"struct.std::__2::__compressed_pair_elem.128"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::__map_value_compare.129"* @_ZNSt3__222__compressed_pair_elemINS_19__map_value_compareIN5draco17GeometryAttribute4TypeENS_12__value_typeIS4_NS2_7OptionsEEENS_4lessIS4_EELb1EEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.128"* %0) #9
  ret %"class.std::__2::__map_value_compare.129"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::__map_value_compare.129"* @_ZNSt3__222__compressed_pair_elemINS_19__map_value_compareIN5draco17GeometryAttribute4TypeENS_12__value_typeIS4_NS2_7OptionsEEENS_4lessIS4_EELb1EEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.128"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.128"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.128"* %this, %"struct.std::__2::__compressed_pair_elem.128"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.128"*, %"struct.std::__2::__compressed_pair_elem.128"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.128"* %this1 to %"class.std::__2::__map_value_compare.129"*
  ret %"class.std::__2::__map_value_compare.129"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_node"* @_ZNKSt3__215__tree_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEEPNS_11__tree_nodeIS6_PvEElE8__get_npEv(%"class.std::__2::__tree_iterator"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree_iterator"*, align 4
  store %"class.std::__2::__tree_iterator"* %this, %"class.std::__2::__tree_iterator"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree_iterator"*, %"class.std::__2::__tree_iterator"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::__tree_iterator", %"class.std::__2::__tree_iterator"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %__ptr_, align 4
  %1 = bitcast %"class.std::__2::__tree_end_node"* %0 to %"class.std::__2::__tree_node"*
  ret %"class.std::__2::__tree_node"* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::pair"* @_ZNSt3__214pointer_traitsIPNS_4pairIKN5draco17GeometryAttribute4TypeENS2_7OptionsEEEE10pointer_toERS7_(%"struct.std::__2::pair"* nonnull align 4 dereferenceable(16) %__r) #0 comdat {
entry:
  %__r.addr = alloca %"struct.std::__2::pair"*, align 4
  store %"struct.std::__2::pair"* %__r, %"struct.std::__2::pair"** %__r.addr, align 4
  %0 = load %"struct.std::__2::pair"*, %"struct.std::__2::pair"** %__r.addr, align 4
  %call = call %"struct.std::__2::pair"* @_ZNSt3__29addressofINS_4pairIKN5draco17GeometryAttribute4TypeENS2_7OptionsEEEEEPT_RS8_(%"struct.std::__2::pair"* nonnull align 4 dereferenceable(16) %0) #9
  ret %"struct.std::__2::pair"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__value_type"* @_ZNKSt3__215__tree_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEEPNS_11__tree_nodeIS6_PvEElEptEv(%"class.std::__2::__tree_iterator"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree_iterator"*, align 4
  store %"class.std::__2::__tree_iterator"* %this, %"class.std::__2::__tree_iterator"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree_iterator"*, %"class.std::__2::__tree_iterator"** %this.addr, align 4
  %call = call %"class.std::__2::__tree_node"* @_ZNKSt3__215__tree_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEEPNS_11__tree_nodeIS6_PvEElE8__get_npEv(%"class.std::__2::__tree_iterator"* %this1)
  %__value_ = getelementptr inbounds %"class.std::__2::__tree_node", %"class.std::__2::__tree_node"* %call, i32 0, i32 1
  %call2 = call %"struct.std::__2::__value_type"* @_ZNSt3__214pointer_traitsIPNS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEEE10pointer_toERS6_(%"struct.std::__2::__value_type"* nonnull align 4 dereferenceable(16) %__value_) #9
  ret %"struct.std::__2::__value_type"* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %"struct.std::__2::pair"* @_ZNSt3__212__value_typeIN5draco17GeometryAttribute4TypeENS1_7OptionsEE11__get_valueEv(%"struct.std::__2::__value_type"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__value_type"*, align 4
  store %"struct.std::__2::__value_type"* %this, %"struct.std::__2::__value_type"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__value_type"*, %"struct.std::__2::__value_type"** %this.addr, align 4
  %__cc = getelementptr inbounds %"struct.std::__2::__value_type", %"struct.std::__2::__value_type"* %this1, i32 0, i32 0
  ret %"struct.std::__2::pair"* %__cc
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::pair"* @_ZNSt3__29addressofINS_4pairIKN5draco17GeometryAttribute4TypeENS2_7OptionsEEEEEPT_RS8_(%"struct.std::__2::pair"* nonnull align 4 dereferenceable(16) %__x) #0 comdat {
entry:
  %__x.addr = alloca %"struct.std::__2::pair"*, align 4
  store %"struct.std::__2::pair"* %__x, %"struct.std::__2::pair"** %__x.addr, align 4
  %0 = load %"struct.std::__2::pair"*, %"struct.std::__2::pair"** %__x.addr, align 4
  ret %"struct.std::__2::pair"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__value_type"* @_ZNSt3__214pointer_traitsIPNS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEEE10pointer_toERS6_(%"struct.std::__2::__value_type"* nonnull align 4 dereferenceable(16) %__r) #0 comdat {
entry:
  %__r.addr = alloca %"struct.std::__2::__value_type"*, align 4
  store %"struct.std::__2::__value_type"* %__r, %"struct.std::__2::__value_type"** %__r.addr, align 4
  %0 = load %"struct.std::__2::__value_type"*, %"struct.std::__2::__value_type"** %__r.addr, align 4
  %call = call %"struct.std::__2::__value_type"* @_ZNSt3__29addressofINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEEEEPT_RS7_(%"struct.std::__2::__value_type"* nonnull align 4 dereferenceable(16) %0) #9
  ret %"struct.std::__2::__value_type"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__value_type"* @_ZNSt3__29addressofINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEEEEPT_RS7_(%"struct.std::__2::__value_type"* nonnull align 4 dereferenceable(16) %__x) #0 comdat {
entry:
  %__x.addr = alloca %"struct.std::__2::__value_type"*, align 4
  store %"struct.std::__2::__value_type"* %__x, %"struct.std::__2::__value_type"** %__x.addr, align 4
  %0 = load %"struct.std::__2::__value_type"*, %"struct.std::__2::__value_type"** %__x.addr, align 4
  ret %"struct.std::__2::__value_type"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE15__insert_uniqueINS_4pairIS4_S5_EEvEENSF_INS_15__tree_iteratorIS6_PNS_11__tree_nodeIS6_PvEElEEbEEOT_(%"struct.std::__2::pair.170"* noalias sret align 4 %agg.result, %"class.std::__2::__tree.122"* %this, %"struct.std::__2::pair.169"* nonnull align 4 dereferenceable(16) %__v) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree.122"*, align 4
  %__v.addr = alloca %"struct.std::__2::pair.169"*, align 4
  store %"class.std::__2::__tree.122"* %this, %"class.std::__2::__tree.122"** %this.addr, align 4
  store %"struct.std::__2::pair.169"* %__v, %"struct.std::__2::pair.169"** %__v.addr, align 4
  %this1 = load %"class.std::__2::__tree.122"*, %"class.std::__2::__tree.122"** %this.addr, align 4
  %0 = load %"struct.std::__2::pair.169"*, %"struct.std::__2::pair.169"** %__v.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %"struct.std::__2::pair.169"* @_ZNSt3__27forwardINS_4pairIN5draco17GeometryAttribute4TypeENS2_7OptionsEEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"struct.std::__2::pair.169"* nonnull align 4 dereferenceable(16) %0) #9
  call void @_ZNSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE16__emplace_uniqueINS_4pairIS4_S5_EEEENSF_INS_15__tree_iteratorIS6_PNS_11__tree_nodeIS6_PvEElEEbEEOT_(%"struct.std::__2::pair.170"* sret align 4 %agg.result, %"class.std::__2::__tree.122"* %this1, %"struct.std::__2::pair.169"* nonnull align 4 dereferenceable(16) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %"struct.std::__2::pair.169"* @_ZNSt3__27forwardINS_4pairIN5draco17GeometryAttribute4TypeENS2_7OptionsEEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"struct.std::__2::pair.169"* nonnull align 4 dereferenceable(16) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"struct.std::__2::pair.169"*, align 4
  store %"struct.std::__2::pair.169"* %__t, %"struct.std::__2::pair.169"** %__t.addr, align 4
  %0 = load %"struct.std::__2::pair.169"*, %"struct.std::__2::pair.169"** %__t.addr, align 4
  ret %"struct.std::__2::pair.169"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::pair.168"* @_ZNSt3__24pairINS_14__map_iteratorINS_15__tree_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS4_7OptionsEEEPNS_11__tree_nodeIS8_PvEElEEEEbEC2ISD_bLb0EEEONS0_IT_T0_EE(%"struct.std::__2::pair.168"* returned %this, %"struct.std::__2::pair.170"* nonnull align 4 dereferenceable(5) %__p) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::pair.168"*, align 4
  %__p.addr = alloca %"struct.std::__2::pair.170"*, align 4
  %agg.tmp = alloca %"class.std::__2::__tree_iterator", align 4
  store %"struct.std::__2::pair.168"* %this, %"struct.std::__2::pair.168"** %this.addr, align 4
  store %"struct.std::__2::pair.170"* %__p, %"struct.std::__2::pair.170"** %__p.addr, align 4
  %this1 = load %"struct.std::__2::pair.168"*, %"struct.std::__2::pair.168"** %this.addr, align 4
  %first = getelementptr inbounds %"struct.std::__2::pair.168", %"struct.std::__2::pair.168"* %this1, i32 0, i32 0
  %0 = load %"struct.std::__2::pair.170"*, %"struct.std::__2::pair.170"** %__p.addr, align 4
  %first2 = getelementptr inbounds %"struct.std::__2::pair.170", %"struct.std::__2::pair.170"* %0, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_iterator"* @_ZNSt3__27forwardINS_15__tree_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPNS_11__tree_nodeIS7_PvEElEEEEOT_RNS_16remove_referenceISD_E4typeE(%"class.std::__2::__tree_iterator"* nonnull align 4 dereferenceable(4) %first2) #9
  %1 = bitcast %"class.std::__2::__tree_iterator"* %agg.tmp to i8*
  %2 = bitcast %"class.std::__2::__tree_iterator"* %call to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 4, i1 false)
  %coerce.dive = getelementptr inbounds %"class.std::__2::__tree_iterator", %"class.std::__2::__tree_iterator"* %agg.tmp, i32 0, i32 0
  %3 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %coerce.dive, align 4
  %call3 = call %"class.std::__2::__map_iterator"* @_ZNSt3__214__map_iteratorINS_15__tree_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPNS_11__tree_nodeIS7_PvEElEEEC2ESC_(%"class.std::__2::__map_iterator"* %first, %"class.std::__2::__tree_end_node"* %3) #9
  %second = getelementptr inbounds %"struct.std::__2::pair.168", %"struct.std::__2::pair.168"* %this1, i32 0, i32 1
  %4 = load %"struct.std::__2::pair.170"*, %"struct.std::__2::pair.170"** %__p.addr, align 4
  %second4 = getelementptr inbounds %"struct.std::__2::pair.170", %"struct.std::__2::pair.170"* %4, i32 0, i32 1
  %call5 = call nonnull align 1 dereferenceable(1) i8* @_ZNSt3__27forwardIbEEOT_RNS_16remove_referenceIS1_E4typeE(i8* nonnull align 1 dereferenceable(1) %second4) #9
  %5 = load i8, i8* %call5, align 1
  %tobool = trunc i8 %5 to i1
  %frombool = zext i1 %tobool to i8
  store i8 %frombool, i8* %second, align 4
  ret %"struct.std::__2::pair.168"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE16__emplace_uniqueINS_4pairIS4_S5_EEEENSF_INS_15__tree_iteratorIS6_PNS_11__tree_nodeIS6_PvEElEEbEEOT_(%"struct.std::__2::pair.170"* noalias sret align 4 %agg.result, %"class.std::__2::__tree.122"* %this, %"struct.std::__2::pair.169"* nonnull align 4 dereferenceable(16) %__x) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree.122"*, align 4
  %__x.addr = alloca %"struct.std::__2::pair.169"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__extract_key_first_tag", align 1
  %ref.tmp = alloca %"struct.std::__2::__can_extract_key", align 1
  store %"class.std::__2::__tree.122"* %this, %"class.std::__2::__tree.122"** %this.addr, align 4
  store %"struct.std::__2::pair.169"* %__x, %"struct.std::__2::pair.169"** %__x.addr, align 4
  %this1 = load %"class.std::__2::__tree.122"*, %"class.std::__2::__tree.122"** %this.addr, align 4
  %0 = load %"struct.std::__2::pair.169"*, %"struct.std::__2::pair.169"** %__x.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %"struct.std::__2::pair.169"* @_ZNSt3__27forwardINS_4pairIN5draco17GeometryAttribute4TypeENS2_7OptionsEEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"struct.std::__2::pair.169"* nonnull align 4 dereferenceable(16) %0) #9
  %1 = bitcast %"struct.std::__2::__can_extract_key"* %ref.tmp to %"struct.std::__2::__extract_key_first_tag"*
  call void @_ZNSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE28__emplace_unique_extract_keyINS_4pairIS4_S5_EEEENSF_INS_15__tree_iteratorIS6_PNS_11__tree_nodeIS6_PvEElEEbEEOT_NS_23__extract_key_first_tagE(%"struct.std::__2::pair.170"* sret align 4 %agg.result, %"class.std::__2::__tree.122"* %this1, %"struct.std::__2::pair.169"* nonnull align 4 dereferenceable(16) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE28__emplace_unique_extract_keyINS_4pairIS4_S5_EEEENSF_INS_15__tree_iteratorIS6_PNS_11__tree_nodeIS6_PvEElEEbEEOT_NS_23__extract_key_first_tagE(%"struct.std::__2::pair.170"* noalias sret align 4 %agg.result, %"class.std::__2::__tree.122"* %this, %"struct.std::__2::pair.169"* nonnull align 4 dereferenceable(16) %__x) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__extract_key_first_tag", align 1
  %this.addr = alloca %"class.std::__2::__tree.122"*, align 4
  %__x.addr = alloca %"struct.std::__2::pair.169"*, align 4
  store %"class.std::__2::__tree.122"* %this, %"class.std::__2::__tree.122"** %this.addr, align 4
  store %"struct.std::__2::pair.169"* %__x, %"struct.std::__2::pair.169"** %__x.addr, align 4
  %this1 = load %"class.std::__2::__tree.122"*, %"class.std::__2::__tree.122"** %this.addr, align 4
  %1 = load %"struct.std::__2::pair.169"*, %"struct.std::__2::pair.169"** %__x.addr, align 4
  %first = getelementptr inbounds %"struct.std::__2::pair.169", %"struct.std::__2::pair.169"* %1, i32 0, i32 0
  %2 = load %"struct.std::__2::pair.169"*, %"struct.std::__2::pair.169"** %__x.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %"struct.std::__2::pair.169"* @_ZNSt3__27forwardINS_4pairIN5draco17GeometryAttribute4TypeENS2_7OptionsEEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"struct.std::__2::pair.169"* nonnull align 4 dereferenceable(16) %2) #9
  call void @_ZNSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE25__emplace_unique_key_argsIS4_JNS_4pairIS4_S5_EEEEENSF_INS_15__tree_iteratorIS6_PNS_11__tree_nodeIS6_PvEElEEbEERKT_DpOT0_(%"struct.std::__2::pair.170"* sret align 4 %agg.result, %"class.std::__2::__tree.122"* %this1, i32* nonnull align 4 dereferenceable(4) %first, %"struct.std::__2::pair.169"* nonnull align 4 dereferenceable(16) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE25__emplace_unique_key_argsIS4_JNS_4pairIS4_S5_EEEEENSF_INS_15__tree_iteratorIS6_PNS_11__tree_nodeIS6_PvEElEEbEERKT_DpOT0_(%"struct.std::__2::pair.170"* noalias sret align 4 %agg.result, %"class.std::__2::__tree.122"* %this, i32* nonnull align 4 dereferenceable(4) %__k, %"struct.std::__2::pair.169"* nonnull align 4 dereferenceable(16) %__args) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree.122"*, align 4
  %__k.addr = alloca i32*, align 4
  %__args.addr = alloca %"struct.std::__2::pair.169"*, align 4
  %__parent = alloca %"class.std::__2::__tree_end_node"*, align 4
  %__child = alloca %"class.std::__2::__tree_node_base"**, align 4
  %__r = alloca %"class.std::__2::__tree_node"*, align 4
  %__inserted = alloca i8, align 1
  %__h = alloca %"class.std::__2::unique_ptr.172", align 4
  %ref.tmp = alloca %"class.std::__2::__tree_iterator", align 4
  store %"class.std::__2::__tree.122"* %this, %"class.std::__2::__tree.122"** %this.addr, align 4
  store i32* %__k, i32** %__k.addr, align 4
  store %"struct.std::__2::pair.169"* %__args, %"struct.std::__2::pair.169"** %__args.addr, align 4
  %this1 = load %"class.std::__2::__tree.122"*, %"class.std::__2::__tree.122"** %this.addr, align 4
  %0 = load i32*, i32** %__k.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_node_base"** @_ZNSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE12__find_equalIS4_EERPNS_16__tree_node_baseIPvEERPNS_15__tree_end_nodeISI_EERKT_(%"class.std::__2::__tree.122"* %this1, %"class.std::__2::__tree_end_node"** nonnull align 4 dereferenceable(4) %__parent, i32* nonnull align 4 dereferenceable(4) %0)
  store %"class.std::__2::__tree_node_base"** %call, %"class.std::__2::__tree_node_base"*** %__child, align 4
  %1 = load %"class.std::__2::__tree_node_base"**, %"class.std::__2::__tree_node_base"*** %__child, align 4
  %2 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %1, align 4
  %3 = bitcast %"class.std::__2::__tree_node_base"* %2 to %"class.std::__2::__tree_node"*
  store %"class.std::__2::__tree_node"* %3, %"class.std::__2::__tree_node"** %__r, align 4
  store i8 0, i8* %__inserted, align 1
  %4 = load %"class.std::__2::__tree_node_base"**, %"class.std::__2::__tree_node_base"*** %__child, align 4
  %5 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %4, align 4
  %cmp = icmp eq %"class.std::__2::__tree_node_base"* %5, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %6 = load %"struct.std::__2::pair.169"*, %"struct.std::__2::pair.169"** %__args.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(16) %"struct.std::__2::pair.169"* @_ZNSt3__27forwardINS_4pairIN5draco17GeometryAttribute4TypeENS2_7OptionsEEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"struct.std::__2::pair.169"* nonnull align 4 dereferenceable(16) %6) #9
  call void @_ZNSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE16__construct_nodeIJNS_4pairIS4_S5_EEEEENS_10unique_ptrINS_11__tree_nodeIS6_PvEENS_22__tree_node_destructorINSB_ISK_EEEEEEDpOT_(%"class.std::__2::unique_ptr.172"* sret align 4 %__h, %"class.std::__2::__tree.122"* %this1, %"struct.std::__2::pair.169"* nonnull align 4 dereferenceable(16) %call2)
  %7 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %__parent, align 4
  %8 = load %"class.std::__2::__tree_node_base"**, %"class.std::__2::__tree_node_base"*** %__child, align 4
  %call3 = call %"class.std::__2::__tree_node"* @_ZNKSt3__210unique_ptrINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPvEENS_22__tree_node_destructorINS_9allocatorIS9_EEEEE3getEv(%"class.std::__2::unique_ptr.172"* %__h) #9
  %9 = bitcast %"class.std::__2::__tree_node"* %call3 to %"class.std::__2::__tree_node_base"*
  call void @_ZNSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE16__insert_node_atEPNS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEERSI_SI_(%"class.std::__2::__tree.122"* %this1, %"class.std::__2::__tree_end_node"* %7, %"class.std::__2::__tree_node_base"** nonnull align 4 dereferenceable(4) %8, %"class.std::__2::__tree_node_base"* %9) #9
  %call4 = call %"class.std::__2::__tree_node"* @_ZNSt3__210unique_ptrINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPvEENS_22__tree_node_destructorINS_9allocatorIS9_EEEEE7releaseEv(%"class.std::__2::unique_ptr.172"* %__h) #9
  store %"class.std::__2::__tree_node"* %call4, %"class.std::__2::__tree_node"** %__r, align 4
  store i8 1, i8* %__inserted, align 1
  %call5 = call %"class.std::__2::unique_ptr.172"* @_ZNSt3__210unique_ptrINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPvEENS_22__tree_node_destructorINS_9allocatorIS9_EEEEED2Ev(%"class.std::__2::unique_ptr.172"* %__h) #9
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %10 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %__r, align 4
  %call6 = call %"class.std::__2::__tree_iterator"* @_ZNSt3__215__tree_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEEPNS_11__tree_nodeIS6_PvEElEC2ESA_(%"class.std::__2::__tree_iterator"* %ref.tmp, %"class.std::__2::__tree_node"* %10) #9
  %call7 = call %"struct.std::__2::pair.170"* @_ZNSt3__24pairINS_15__tree_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPNS_11__tree_nodeIS7_PvEElEEbEC2ISC_RbLb0EEEOT_OT0_(%"struct.std::__2::pair.170"* %agg.result, %"class.std::__2::__tree_iterator"* nonnull align 4 dereferenceable(4) %ref.tmp, i8* nonnull align 1 dereferenceable(1) %__inserted) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_node_base"** @_ZNSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE12__find_equalIS4_EERPNS_16__tree_node_baseIPvEERPNS_15__tree_end_nodeISI_EERKT_(%"class.std::__2::__tree.122"* %this, %"class.std::__2::__tree_end_node"** nonnull align 4 dereferenceable(4) %__parent, i32* nonnull align 4 dereferenceable(4) %__v) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__tree_node_base"**, align 4
  %this.addr = alloca %"class.std::__2::__tree.122"*, align 4
  %__parent.addr = alloca %"class.std::__2::__tree_end_node"**, align 4
  %__v.addr = alloca i32*, align 4
  %__nd = alloca %"class.std::__2::__tree_node"*, align 4
  %__nd_ptr = alloca %"class.std::__2::__tree_node_base"**, align 4
  store %"class.std::__2::__tree.122"* %this, %"class.std::__2::__tree.122"** %this.addr, align 4
  store %"class.std::__2::__tree_end_node"** %__parent, %"class.std::__2::__tree_end_node"*** %__parent.addr, align 4
  store i32* %__v, i32** %__v.addr, align 4
  %this1 = load %"class.std::__2::__tree.122"*, %"class.std::__2::__tree.122"** %this.addr, align 4
  %call = call %"class.std::__2::__tree_node"* @_ZNKSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE6__rootEv(%"class.std::__2::__tree.122"* %this1) #9
  store %"class.std::__2::__tree_node"* %call, %"class.std::__2::__tree_node"** %__nd, align 4
  %call2 = call %"class.std::__2::__tree_node_base"** @_ZNKSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE10__root_ptrEv(%"class.std::__2::__tree.122"* %this1) #9
  store %"class.std::__2::__tree_node_base"** %call2, %"class.std::__2::__tree_node_base"*** %__nd_ptr, align 4
  %0 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %__nd, align 4
  %cmp = icmp ne %"class.std::__2::__tree_node"* %0, null
  br i1 %cmp, label %if.then, label %if.end28

if.then:                                          ; preds = %entry
  br label %while.body

while.body:                                       ; preds = %if.then, %if.end27
  %call3 = call nonnull align 1 dereferenceable(1) %"class.std::__2::__map_value_compare.129"* @_ZNSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE10value_compEv(%"class.std::__2::__tree.122"* %this1) #9
  %1 = load i32*, i32** %__v.addr, align 4
  %2 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %__nd, align 4
  %__value_ = getelementptr inbounds %"class.std::__2::__tree_node", %"class.std::__2::__tree_node"* %2, i32 0, i32 1
  %call4 = call zeroext i1 @_ZNKSt3__219__map_value_compareIN5draco17GeometryAttribute4TypeENS_12__value_typeIS3_NS1_7OptionsEEENS_4lessIS3_EELb1EEclERKS3_RKS6_(%"class.std::__2::__map_value_compare.129"* %call3, i32* nonnull align 4 dereferenceable(4) %1, %"struct.std::__2::__value_type"* nonnull align 4 dereferenceable(16) %__value_)
  br i1 %call4, label %if.then5, label %if.else12

if.then5:                                         ; preds = %while.body
  %3 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %__nd, align 4
  %4 = bitcast %"class.std::__2::__tree_node"* %3 to %"class.std::__2::__tree_end_node"*
  %__left_ = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %4, i32 0, i32 0
  %5 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__left_, align 4
  %cmp6 = icmp ne %"class.std::__2::__tree_node_base"* %5, null
  br i1 %cmp6, label %if.then7, label %if.else

if.then7:                                         ; preds = %if.then5
  %6 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %__nd, align 4
  %7 = bitcast %"class.std::__2::__tree_node"* %6 to %"class.std::__2::__tree_end_node"*
  %__left_8 = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %7, i32 0, i32 0
  %call9 = call %"class.std::__2::__tree_node_base"** @_ZNSt3__29addressofIPNS_16__tree_node_baseIPvEEEEPT_RS5_(%"class.std::__2::__tree_node_base"** nonnull align 4 dereferenceable(4) %__left_8) #9
  store %"class.std::__2::__tree_node_base"** %call9, %"class.std::__2::__tree_node_base"*** %__nd_ptr, align 4
  %8 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %__nd, align 4
  %9 = bitcast %"class.std::__2::__tree_node"* %8 to %"class.std::__2::__tree_end_node"*
  %__left_10 = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %9, i32 0, i32 0
  %10 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__left_10, align 4
  %11 = bitcast %"class.std::__2::__tree_node_base"* %10 to %"class.std::__2::__tree_node"*
  store %"class.std::__2::__tree_node"* %11, %"class.std::__2::__tree_node"** %__nd, align 4
  br label %if.end

if.else:                                          ; preds = %if.then5
  %12 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %__nd, align 4
  %13 = bitcast %"class.std::__2::__tree_node"* %12 to %"class.std::__2::__tree_end_node"*
  %14 = load %"class.std::__2::__tree_end_node"**, %"class.std::__2::__tree_end_node"*** %__parent.addr, align 4
  store %"class.std::__2::__tree_end_node"* %13, %"class.std::__2::__tree_end_node"** %14, align 4
  %15 = load %"class.std::__2::__tree_end_node"**, %"class.std::__2::__tree_end_node"*** %__parent.addr, align 4
  %16 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %15, align 4
  %__left_11 = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %16, i32 0, i32 0
  store %"class.std::__2::__tree_node_base"** %__left_11, %"class.std::__2::__tree_node_base"*** %retval, align 4
  br label %return

if.end:                                           ; preds = %if.then7
  br label %if.end27

if.else12:                                        ; preds = %while.body
  %call13 = call nonnull align 1 dereferenceable(1) %"class.std::__2::__map_value_compare.129"* @_ZNSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE10value_compEv(%"class.std::__2::__tree.122"* %this1) #9
  %17 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %__nd, align 4
  %__value_14 = getelementptr inbounds %"class.std::__2::__tree_node", %"class.std::__2::__tree_node"* %17, i32 0, i32 1
  %18 = load i32*, i32** %__v.addr, align 4
  %call15 = call zeroext i1 @_ZNKSt3__219__map_value_compareIN5draco17GeometryAttribute4TypeENS_12__value_typeIS3_NS1_7OptionsEEENS_4lessIS3_EELb1EEclERKS6_RKS3_(%"class.std::__2::__map_value_compare.129"* %call13, %"struct.std::__2::__value_type"* nonnull align 4 dereferenceable(16) %__value_14, i32* nonnull align 4 dereferenceable(4) %18)
  br i1 %call15, label %if.then16, label %if.else25

if.then16:                                        ; preds = %if.else12
  %19 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %__nd, align 4
  %20 = bitcast %"class.std::__2::__tree_node"* %19 to %"class.std::__2::__tree_node_base"*
  %__right_ = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %20, i32 0, i32 1
  %21 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__right_, align 4
  %cmp17 = icmp ne %"class.std::__2::__tree_node_base"* %21, null
  br i1 %cmp17, label %if.then18, label %if.else22

if.then18:                                        ; preds = %if.then16
  %22 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %__nd, align 4
  %23 = bitcast %"class.std::__2::__tree_node"* %22 to %"class.std::__2::__tree_node_base"*
  %__right_19 = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %23, i32 0, i32 1
  %call20 = call %"class.std::__2::__tree_node_base"** @_ZNSt3__29addressofIPNS_16__tree_node_baseIPvEEEEPT_RS5_(%"class.std::__2::__tree_node_base"** nonnull align 4 dereferenceable(4) %__right_19) #9
  store %"class.std::__2::__tree_node_base"** %call20, %"class.std::__2::__tree_node_base"*** %__nd_ptr, align 4
  %24 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %__nd, align 4
  %25 = bitcast %"class.std::__2::__tree_node"* %24 to %"class.std::__2::__tree_node_base"*
  %__right_21 = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %25, i32 0, i32 1
  %26 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__right_21, align 4
  %27 = bitcast %"class.std::__2::__tree_node_base"* %26 to %"class.std::__2::__tree_node"*
  store %"class.std::__2::__tree_node"* %27, %"class.std::__2::__tree_node"** %__nd, align 4
  br label %if.end24

if.else22:                                        ; preds = %if.then16
  %28 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %__nd, align 4
  %29 = bitcast %"class.std::__2::__tree_node"* %28 to %"class.std::__2::__tree_end_node"*
  %30 = load %"class.std::__2::__tree_end_node"**, %"class.std::__2::__tree_end_node"*** %__parent.addr, align 4
  store %"class.std::__2::__tree_end_node"* %29, %"class.std::__2::__tree_end_node"** %30, align 4
  %31 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %__nd, align 4
  %32 = bitcast %"class.std::__2::__tree_node"* %31 to %"class.std::__2::__tree_node_base"*
  %__right_23 = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %32, i32 0, i32 1
  store %"class.std::__2::__tree_node_base"** %__right_23, %"class.std::__2::__tree_node_base"*** %retval, align 4
  br label %return

if.end24:                                         ; preds = %if.then18
  br label %if.end26

if.else25:                                        ; preds = %if.else12
  %33 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %__nd, align 4
  %34 = bitcast %"class.std::__2::__tree_node"* %33 to %"class.std::__2::__tree_end_node"*
  %35 = load %"class.std::__2::__tree_end_node"**, %"class.std::__2::__tree_end_node"*** %__parent.addr, align 4
  store %"class.std::__2::__tree_end_node"* %34, %"class.std::__2::__tree_end_node"** %35, align 4
  %36 = load %"class.std::__2::__tree_node_base"**, %"class.std::__2::__tree_node_base"*** %__nd_ptr, align 4
  store %"class.std::__2::__tree_node_base"** %36, %"class.std::__2::__tree_node_base"*** %retval, align 4
  br label %return

if.end26:                                         ; preds = %if.end24
  br label %if.end27

if.end27:                                         ; preds = %if.end26, %if.end
  br label %while.body

if.end28:                                         ; preds = %entry
  %call29 = call %"class.std::__2::__tree_end_node"* @_ZNSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE10__end_nodeEv(%"class.std::__2::__tree.122"* %this1) #9
  %37 = load %"class.std::__2::__tree_end_node"**, %"class.std::__2::__tree_end_node"*** %__parent.addr, align 4
  store %"class.std::__2::__tree_end_node"* %call29, %"class.std::__2::__tree_end_node"** %37, align 4
  %38 = load %"class.std::__2::__tree_end_node"**, %"class.std::__2::__tree_end_node"*** %__parent.addr, align 4
  %39 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %38, align 4
  %__left_30 = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %39, i32 0, i32 0
  store %"class.std::__2::__tree_node_base"** %__left_30, %"class.std::__2::__tree_node_base"*** %retval, align 4
  br label %return

return:                                           ; preds = %if.end28, %if.else25, %if.else22, %if.else
  %40 = load %"class.std::__2::__tree_node_base"**, %"class.std::__2::__tree_node_base"*** %retval, align 4
  ret %"class.std::__2::__tree_node_base"** %40
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE16__construct_nodeIJNS_4pairIS4_S5_EEEEENS_10unique_ptrINS_11__tree_nodeIS6_PvEENS_22__tree_node_destructorINSB_ISK_EEEEEEDpOT_(%"class.std::__2::unique_ptr.172"* noalias sret align 4 %agg.result, %"class.std::__2::__tree.122"* %this, %"struct.std::__2::pair.169"* nonnull align 4 dereferenceable(16) %__args) #0 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %"class.std::__2::__tree.122"*, align 4
  %__args.addr = alloca %"struct.std::__2::pair.169"*, align 4
  %__na = alloca %"class.std::__2::allocator.125"*, align 4
  %nrvo = alloca i1, align 1
  %ref.tmp = alloca %"class.std::__2::__tree_node_destructor", align 4
  %0 = bitcast %"class.std::__2::unique_ptr.172"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %"class.std::__2::__tree.122"* %this, %"class.std::__2::__tree.122"** %this.addr, align 4
  store %"struct.std::__2::pair.169"* %__args, %"struct.std::__2::pair.169"** %__args.addr, align 4
  %this1 = load %"class.std::__2::__tree.122"*, %"class.std::__2::__tree.122"** %this.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.125"* @_ZNSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE12__node_allocEv(%"class.std::__2::__tree.122"* %this1) #9
  store %"class.std::__2::allocator.125"* %call, %"class.std::__2::allocator.125"** %__na, align 4
  store i1 false, i1* %nrvo, align 1
  %1 = load %"class.std::__2::allocator.125"*, %"class.std::__2::allocator.125"** %__na, align 4
  %call2 = call %"class.std::__2::__tree_node"* @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS4_7OptionsEEEPvEEEEE8allocateERSB_m(%"class.std::__2::allocator.125"* nonnull align 1 dereferenceable(1) %1, i32 1)
  %2 = load %"class.std::__2::allocator.125"*, %"class.std::__2::allocator.125"** %__na, align 4
  %call3 = call %"class.std::__2::__tree_node_destructor"* @_ZNSt3__222__tree_node_destructorINS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS4_7OptionsEEEPvEEEEEC2ERSB_b(%"class.std::__2::__tree_node_destructor"* %ref.tmp, %"class.std::__2::allocator.125"* nonnull align 1 dereferenceable(1) %2, i1 zeroext false) #9
  %call4 = call %"class.std::__2::unique_ptr.172"* @_ZNSt3__210unique_ptrINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPvEENS_22__tree_node_destructorINS_9allocatorIS9_EEEEEC2ILb1EvEEPS9_NS_16__dependent_typeINS_27__unique_ptr_deleter_sfinaeISD_EEXT_EE20__good_rval_ref_typeE(%"class.std::__2::unique_ptr.172"* %agg.result, %"class.std::__2::__tree_node"* %call2, %"class.std::__2::__tree_node_destructor"* nonnull align 4 dereferenceable(5) %ref.tmp) #9
  %3 = load %"class.std::__2::allocator.125"*, %"class.std::__2::allocator.125"** %__na, align 4
  %call5 = call %"class.std::__2::__tree_node"* @_ZNKSt3__210unique_ptrINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPvEENS_22__tree_node_destructorINS_9allocatorIS9_EEEEEptEv(%"class.std::__2::unique_ptr.172"* %agg.result) #9
  %__value_ = getelementptr inbounds %"class.std::__2::__tree_node", %"class.std::__2::__tree_node"* %call5, i32 0, i32 1
  %call6 = call %"struct.std::__2::pair"* @_ZNSt3__222__tree_key_value_typesINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEEE9__get_ptrERS6_(%"struct.std::__2::__value_type"* nonnull align 4 dereferenceable(16) %__value_)
  %4 = load %"struct.std::__2::pair.169"*, %"struct.std::__2::pair.169"** %__args.addr, align 4
  %call7 = call nonnull align 4 dereferenceable(16) %"struct.std::__2::pair.169"* @_ZNSt3__27forwardINS_4pairIN5draco17GeometryAttribute4TypeENS2_7OptionsEEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"struct.std::__2::pair.169"* nonnull align 4 dereferenceable(16) %4) #9
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS4_7OptionsEEEPvEEEEE9constructINS_4pairIKS6_S7_EEJNSE_IS6_S7_EEEEEvRSB_PT_DpOT0_(%"class.std::__2::allocator.125"* nonnull align 1 dereferenceable(1) %3, %"struct.std::__2::pair"* %call6, %"struct.std::__2::pair.169"* nonnull align 4 dereferenceable(16) %call7)
  %call8 = call nonnull align 4 dereferenceable(5) %"class.std::__2::__tree_node_destructor"* @_ZNSt3__210unique_ptrINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPvEENS_22__tree_node_destructorINS_9allocatorIS9_EEEEE11get_deleterEv(%"class.std::__2::unique_ptr.172"* %agg.result) #9
  %__value_constructed = getelementptr inbounds %"class.std::__2::__tree_node_destructor", %"class.std::__2::__tree_node_destructor"* %call8, i32 0, i32 1
  store i8 1, i8* %__value_constructed, align 4
  store i1 true, i1* %nrvo, align 1
  %nrvo.val = load i1, i1* %nrvo, align 1
  br i1 %nrvo.val, label %nrvo.skipdtor, label %nrvo.unused

nrvo.unused:                                      ; preds = %entry
  %call9 = call %"class.std::__2::unique_ptr.172"* @_ZNSt3__210unique_ptrINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPvEENS_22__tree_node_destructorINS_9allocatorIS9_EEEEED2Ev(%"class.std::__2::unique_ptr.172"* %agg.result) #9
  br label %nrvo.skipdtor

nrvo.skipdtor:                                    ; preds = %nrvo.unused, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE16__insert_node_atEPNS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEERSI_SI_(%"class.std::__2::__tree.122"* %this, %"class.std::__2::__tree_end_node"* %__parent, %"class.std::__2::__tree_node_base"** nonnull align 4 dereferenceable(4) %__child, %"class.std::__2::__tree_node_base"* %__new_node) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree.122"*, align 4
  %__parent.addr = alloca %"class.std::__2::__tree_end_node"*, align 4
  %__child.addr = alloca %"class.std::__2::__tree_node_base"**, align 4
  %__new_node.addr = alloca %"class.std::__2::__tree_node_base"*, align 4
  store %"class.std::__2::__tree.122"* %this, %"class.std::__2::__tree.122"** %this.addr, align 4
  store %"class.std::__2::__tree_end_node"* %__parent, %"class.std::__2::__tree_end_node"** %__parent.addr, align 4
  store %"class.std::__2::__tree_node_base"** %__child, %"class.std::__2::__tree_node_base"*** %__child.addr, align 4
  store %"class.std::__2::__tree_node_base"* %__new_node, %"class.std::__2::__tree_node_base"** %__new_node.addr, align 4
  %this1 = load %"class.std::__2::__tree.122"*, %"class.std::__2::__tree.122"** %this.addr, align 4
  %0 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__new_node.addr, align 4
  %1 = bitcast %"class.std::__2::__tree_node_base"* %0 to %"class.std::__2::__tree_end_node"*
  %__left_ = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %1, i32 0, i32 0
  store %"class.std::__2::__tree_node_base"* null, %"class.std::__2::__tree_node_base"** %__left_, align 4
  %2 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__new_node.addr, align 4
  %__right_ = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %2, i32 0, i32 1
  store %"class.std::__2::__tree_node_base"* null, %"class.std::__2::__tree_node_base"** %__right_, align 4
  %3 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %__parent.addr, align 4
  %4 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__new_node.addr, align 4
  %__parent_ = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %4, i32 0, i32 2
  store %"class.std::__2::__tree_end_node"* %3, %"class.std::__2::__tree_end_node"** %__parent_, align 4
  %5 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__new_node.addr, align 4
  %6 = load %"class.std::__2::__tree_node_base"**, %"class.std::__2::__tree_node_base"*** %__child.addr, align 4
  store %"class.std::__2::__tree_node_base"* %5, %"class.std::__2::__tree_node_base"** %6, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_end_node"** @_ZNSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE12__begin_nodeEv(%"class.std::__2::__tree.122"* %this1) #9
  %7 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %call, align 4
  %__left_2 = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %7, i32 0, i32 0
  %8 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__left_2, align 4
  %cmp = icmp ne %"class.std::__2::__tree_node_base"* %8, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_end_node"** @_ZNSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE12__begin_nodeEv(%"class.std::__2::__tree.122"* %this1) #9
  %9 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %call3, align 4
  %__left_4 = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %9, i32 0, i32 0
  %10 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__left_4, align 4
  %11 = bitcast %"class.std::__2::__tree_node_base"* %10 to %"class.std::__2::__tree_end_node"*
  %call5 = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_end_node"** @_ZNSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE12__begin_nodeEv(%"class.std::__2::__tree.122"* %this1) #9
  store %"class.std::__2::__tree_end_node"* %11, %"class.std::__2::__tree_end_node"** %call5, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %call6 = call %"class.std::__2::__tree_end_node"* @_ZNSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE10__end_nodeEv(%"class.std::__2::__tree.122"* %this1) #9
  %__left_7 = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %call6, i32 0, i32 0
  %12 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__left_7, align 4
  %13 = load %"class.std::__2::__tree_node_base"**, %"class.std::__2::__tree_node_base"*** %__child.addr, align 4
  %14 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %13, align 4
  call void @_ZNSt3__227__tree_balance_after_insertIPNS_16__tree_node_baseIPvEEEEvT_S5_(%"class.std::__2::__tree_node_base"* %12, %"class.std::__2::__tree_node_base"* %14) #9
  %call8 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::__tree.122"* %this1) #9
  %15 = load i32, i32* %call8, align 4
  %inc = add i32 %15, 1
  store i32 %inc, i32* %call8, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_node"* @_ZNKSt3__210unique_ptrINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPvEENS_22__tree_node_destructorINS_9allocatorIS9_EEEEE3getEv(%"class.std::__2::unique_ptr.172"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.172"*, align 4
  store %"class.std::__2::unique_ptr.172"* %this, %"class.std::__2::unique_ptr.172"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.172"*, %"class.std::__2::unique_ptr.172"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.172", %"class.std::__2::unique_ptr.172"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_node"** @_ZNKSt3__217__compressed_pairIPNS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPvEENS_22__tree_node_destructorINS_9allocatorIS9_EEEEE5firstEv(%"class.std::__2::__compressed_pair.173"* %__ptr_) #9
  %0 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %call, align 4
  ret %"class.std::__2::__tree_node"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_node"* @_ZNSt3__210unique_ptrINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPvEENS_22__tree_node_destructorINS_9allocatorIS9_EEEEE7releaseEv(%"class.std::__2::unique_ptr.172"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.172"*, align 4
  %__t = alloca %"class.std::__2::__tree_node"*, align 4
  store %"class.std::__2::unique_ptr.172"* %this, %"class.std::__2::unique_ptr.172"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.172"*, %"class.std::__2::unique_ptr.172"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.172", %"class.std::__2::unique_ptr.172"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_node"** @_ZNSt3__217__compressed_pairIPNS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPvEENS_22__tree_node_destructorINS_9allocatorIS9_EEEEE5firstEv(%"class.std::__2::__compressed_pair.173"* %__ptr_) #9
  %0 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %call, align 4
  store %"class.std::__2::__tree_node"* %0, %"class.std::__2::__tree_node"** %__t, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr.172", %"class.std::__2::unique_ptr.172"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_node"** @_ZNSt3__217__compressed_pairIPNS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPvEENS_22__tree_node_destructorINS_9allocatorIS9_EEEEE5firstEv(%"class.std::__2::__compressed_pair.173"* %__ptr_2) #9
  store %"class.std::__2::__tree_node"* null, %"class.std::__2::__tree_node"** %call3, align 4
  %1 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %__t, align 4
  ret %"class.std::__2::__tree_node"* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.172"* @_ZNSt3__210unique_ptrINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPvEENS_22__tree_node_destructorINS_9allocatorIS9_EEEEED2Ev(%"class.std::__2::unique_ptr.172"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.172"*, align 4
  store %"class.std::__2::unique_ptr.172"* %this, %"class.std::__2::unique_ptr.172"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.172"*, %"class.std::__2::unique_ptr.172"** %this.addr, align 4
  call void @_ZNSt3__210unique_ptrINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPvEENS_22__tree_node_destructorINS_9allocatorIS9_EEEEE5resetEPS9_(%"class.std::__2::unique_ptr.172"* %this1, %"class.std::__2::__tree_node"* null) #9
  ret %"class.std::__2::unique_ptr.172"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_iterator"* @_ZNSt3__215__tree_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEEPNS_11__tree_nodeIS6_PvEElEC2ESA_(%"class.std::__2::__tree_iterator"* returned %this, %"class.std::__2::__tree_node"* %__p) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree_iterator"*, align 4
  %__p.addr = alloca %"class.std::__2::__tree_node"*, align 4
  store %"class.std::__2::__tree_iterator"* %this, %"class.std::__2::__tree_iterator"** %this.addr, align 4
  store %"class.std::__2::__tree_node"* %__p, %"class.std::__2::__tree_node"** %__p.addr, align 4
  %this1 = load %"class.std::__2::__tree_iterator"*, %"class.std::__2::__tree_iterator"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::__tree_iterator", %"class.std::__2::__tree_iterator"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %__p.addr, align 4
  %1 = bitcast %"class.std::__2::__tree_node"* %0 to %"class.std::__2::__tree_end_node"*
  store %"class.std::__2::__tree_end_node"* %1, %"class.std::__2::__tree_end_node"** %__ptr_, align 4
  ret %"class.std::__2::__tree_iterator"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::pair.170"* @_ZNSt3__24pairINS_15__tree_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPNS_11__tree_nodeIS7_PvEElEEbEC2ISC_RbLb0EEEOT_OT0_(%"struct.std::__2::pair.170"* returned %this, %"class.std::__2::__tree_iterator"* nonnull align 4 dereferenceable(4) %__u1, i8* nonnull align 1 dereferenceable(1) %__u2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::pair.170"*, align 4
  %__u1.addr = alloca %"class.std::__2::__tree_iterator"*, align 4
  %__u2.addr = alloca i8*, align 4
  store %"struct.std::__2::pair.170"* %this, %"struct.std::__2::pair.170"** %this.addr, align 4
  store %"class.std::__2::__tree_iterator"* %__u1, %"class.std::__2::__tree_iterator"** %__u1.addr, align 4
  store i8* %__u2, i8** %__u2.addr, align 4
  %this1 = load %"struct.std::__2::pair.170"*, %"struct.std::__2::pair.170"** %this.addr, align 4
  %first = getelementptr inbounds %"struct.std::__2::pair.170", %"struct.std::__2::pair.170"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::__tree_iterator"*, %"class.std::__2::__tree_iterator"** %__u1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_iterator"* @_ZNSt3__27forwardINS_15__tree_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPNS_11__tree_nodeIS7_PvEElEEEEOT_RNS_16remove_referenceISD_E4typeE(%"class.std::__2::__tree_iterator"* nonnull align 4 dereferenceable(4) %0) #9
  %1 = bitcast %"class.std::__2::__tree_iterator"* %first to i8*
  %2 = bitcast %"class.std::__2::__tree_iterator"* %call to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 4, i1 false)
  %second = getelementptr inbounds %"struct.std::__2::pair.170", %"struct.std::__2::pair.170"* %this1, i32 0, i32 1
  %3 = load i8*, i8** %__u2.addr, align 4
  %call2 = call nonnull align 1 dereferenceable(1) i8* @_ZNSt3__27forwardIRbEEOT_RNS_16remove_referenceIS2_E4typeE(i8* nonnull align 1 dereferenceable(1) %3) #9
  %4 = load i8, i8* %call2, align 1
  %tobool = trunc i8 %4 to i1
  %frombool = zext i1 %tobool to i8
  store i8 %frombool, i8* %second, align 4
  ret %"struct.std::__2::pair.170"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_node_base"** @_ZNKSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE10__root_ptrEv(%"class.std::__2::__tree.122"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree.122"*, align 4
  store %"class.std::__2::__tree.122"* %this, %"class.std::__2::__tree.122"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree.122"*, %"class.std::__2::__tree.122"** %this.addr, align 4
  %call = call %"class.std::__2::__tree_end_node"* @_ZNKSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE10__end_nodeEv(%"class.std::__2::__tree.122"* %this1) #9
  %__left_ = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %call, i32 0, i32 0
  %call2 = call %"class.std::__2::__tree_node_base"** @_ZNSt3__29addressofIPNS_16__tree_node_baseIPvEEEEPT_RS5_(%"class.std::__2::__tree_node_base"** nonnull align 4 dereferenceable(4) %__left_) #9
  ret %"class.std::__2::__tree_node_base"** %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_node_base"** @_ZNSt3__29addressofIPNS_16__tree_node_baseIPvEEEEPT_RS5_(%"class.std::__2::__tree_node_base"** nonnull align 4 dereferenceable(4) %__x) #0 comdat {
entry:
  %__x.addr = alloca %"class.std::__2::__tree_node_base"**, align 4
  store %"class.std::__2::__tree_node_base"** %__x, %"class.std::__2::__tree_node_base"*** %__x.addr, align 4
  %0 = load %"class.std::__2::__tree_node_base"**, %"class.std::__2::__tree_node_base"*** %__x.addr, align 4
  ret %"class.std::__2::__tree_node_base"** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.125"* @_ZNSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE12__node_allocEv(%"class.std::__2::__tree.122"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree.122"*, align 4
  store %"class.std::__2::__tree.122"* %this, %"class.std::__2::__tree.122"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree.122"*, %"class.std::__2::__tree.122"** %this.addr, align 4
  %__pair1_ = getelementptr inbounds %"class.std::__2::__tree.122", %"class.std::__2::__tree.122"* %this1, i32 0, i32 1
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.125"* @_ZNSt3__217__compressed_pairINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEENS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENSA_7OptionsEEES3_EEEEE6secondEv(%"class.std::__2::__compressed_pair.123"* %__pair1_) #9
  ret %"class.std::__2::allocator.125"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_node"* @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS4_7OptionsEEEPvEEEEE8allocateERSB_m(%"class.std::__2::allocator.125"* nonnull align 1 dereferenceable(1) %__a, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.125"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.125"* %__a, %"class.std::__2::allocator.125"** %__a.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.125"*, %"class.std::__2::allocator.125"** %__a.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call %"class.std::__2::__tree_node"* @_ZNSt3__29allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPvEEE8allocateEmPKv(%"class.std::__2::allocator.125"* %0, i32 %1, i8* null)
  ret %"class.std::__2::__tree_node"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_node_destructor"* @_ZNSt3__222__tree_node_destructorINS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS4_7OptionsEEEPvEEEEEC2ERSB_b(%"class.std::__2::__tree_node_destructor"* returned %this, %"class.std::__2::allocator.125"* nonnull align 1 dereferenceable(1) %__na, i1 zeroext %__val) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree_node_destructor"*, align 4
  %__na.addr = alloca %"class.std::__2::allocator.125"*, align 4
  %__val.addr = alloca i8, align 1
  store %"class.std::__2::__tree_node_destructor"* %this, %"class.std::__2::__tree_node_destructor"** %this.addr, align 4
  store %"class.std::__2::allocator.125"* %__na, %"class.std::__2::allocator.125"** %__na.addr, align 4
  %frombool = zext i1 %__val to i8
  store i8 %frombool, i8* %__val.addr, align 1
  %this1 = load %"class.std::__2::__tree_node_destructor"*, %"class.std::__2::__tree_node_destructor"** %this.addr, align 4
  %__na_ = getelementptr inbounds %"class.std::__2::__tree_node_destructor", %"class.std::__2::__tree_node_destructor"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::allocator.125"*, %"class.std::__2::allocator.125"** %__na.addr, align 4
  store %"class.std::__2::allocator.125"* %0, %"class.std::__2::allocator.125"** %__na_, align 4
  %__value_constructed = getelementptr inbounds %"class.std::__2::__tree_node_destructor", %"class.std::__2::__tree_node_destructor"* %this1, i32 0, i32 1
  %1 = load i8, i8* %__val.addr, align 1
  %tobool = trunc i8 %1 to i1
  %frombool2 = zext i1 %tobool to i8
  store i8 %frombool2, i8* %__value_constructed, align 4
  ret %"class.std::__2::__tree_node_destructor"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.172"* @_ZNSt3__210unique_ptrINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPvEENS_22__tree_node_destructorINS_9allocatorIS9_EEEEEC2ILb1EvEEPS9_NS_16__dependent_typeINS_27__unique_ptr_deleter_sfinaeISD_EEXT_EE20__good_rval_ref_typeE(%"class.std::__2::unique_ptr.172"* returned %this, %"class.std::__2::__tree_node"* %__p, %"class.std::__2::__tree_node_destructor"* nonnull align 4 dereferenceable(5) %__d) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.172"*, align 4
  %__p.addr = alloca %"class.std::__2::__tree_node"*, align 4
  %__d.addr = alloca %"class.std::__2::__tree_node_destructor"*, align 4
  store %"class.std::__2::unique_ptr.172"* %this, %"class.std::__2::unique_ptr.172"** %this.addr, align 4
  store %"class.std::__2::__tree_node"* %__p, %"class.std::__2::__tree_node"** %__p.addr, align 4
  store %"class.std::__2::__tree_node_destructor"* %__d, %"class.std::__2::__tree_node_destructor"** %__d.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.172"*, %"class.std::__2::unique_ptr.172"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.172", %"class.std::__2::unique_ptr.172"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::__tree_node_destructor"*, %"class.std::__2::__tree_node_destructor"** %__d.addr, align 4
  %call = call nonnull align 4 dereferenceable(5) %"class.std::__2::__tree_node_destructor"* @_ZNSt3__24moveIRNS_22__tree_node_destructorINS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS5_7OptionsEEEPvEEEEEEEEONS_16remove_referenceIT_E4typeEOSG_(%"class.std::__2::__tree_node_destructor"* nonnull align 4 dereferenceable(5) %0) #9
  %call2 = call %"class.std::__2::__compressed_pair.173"* @_ZNSt3__217__compressed_pairIPNS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPvEENS_22__tree_node_destructorINS_9allocatorIS9_EEEEEC2IRSA_SE_EEOT_OT0_(%"class.std::__2::__compressed_pair.173"* %__ptr_, %"class.std::__2::__tree_node"** nonnull align 4 dereferenceable(4) %__p.addr, %"class.std::__2::__tree_node_destructor"* nonnull align 4 dereferenceable(5) %call)
  ret %"class.std::__2::unique_ptr.172"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS4_7OptionsEEEPvEEEEE9constructINS_4pairIKS6_S7_EEJNSE_IS6_S7_EEEEEvRSB_PT_DpOT0_(%"class.std::__2::allocator.125"* nonnull align 1 dereferenceable(1) %__a, %"struct.std::__2::pair"* %__p, %"struct.std::__2::pair.169"* nonnull align 4 dereferenceable(16) %__args) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.125"*, align 4
  %__p.addr = alloca %"struct.std::__2::pair"*, align 4
  %__args.addr = alloca %"struct.std::__2::pair.169"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_construct", align 1
  store %"class.std::__2::allocator.125"* %__a, %"class.std::__2::allocator.125"** %__a.addr, align 4
  store %"struct.std::__2::pair"* %__p, %"struct.std::__2::pair"** %__p.addr, align 4
  store %"struct.std::__2::pair.169"* %__args, %"struct.std::__2::pair.169"** %__args.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_construct"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.125"*, %"class.std::__2::allocator.125"** %__a.addr, align 4
  %2 = load %"struct.std::__2::pair"*, %"struct.std::__2::pair"** %__p.addr, align 4
  %3 = load %"struct.std::__2::pair.169"*, %"struct.std::__2::pair.169"** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %"struct.std::__2::pair.169"* @_ZNSt3__27forwardINS_4pairIN5draco17GeometryAttribute4TypeENS2_7OptionsEEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"struct.std::__2::pair.169"* nonnull align 4 dereferenceable(16) %3) #9
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS4_7OptionsEEEPvEEEEE11__constructINS_4pairIKS6_S7_EEJNSE_IS6_S7_EEEEEvNS_17integral_constantIbLb1EEERSB_PT_DpOT0_(%"class.std::__2::allocator.125"* nonnull align 1 dereferenceable(1) %1, %"struct.std::__2::pair"* %2, %"struct.std::__2::pair.169"* nonnull align 4 dereferenceable(16) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::pair"* @_ZNSt3__222__tree_key_value_typesINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEEE9__get_ptrERS6_(%"struct.std::__2::__value_type"* nonnull align 4 dereferenceable(16) %__n) #0 comdat {
entry:
  %__n.addr = alloca %"struct.std::__2::__value_type"*, align 4
  store %"struct.std::__2::__value_type"* %__n, %"struct.std::__2::__value_type"** %__n.addr, align 4
  %0 = load %"struct.std::__2::__value_type"*, %"struct.std::__2::__value_type"** %__n.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %"struct.std::__2::pair"* @_ZNSt3__212__value_typeIN5draco17GeometryAttribute4TypeENS1_7OptionsEE11__get_valueEv(%"struct.std::__2::__value_type"* %0)
  %call1 = call %"struct.std::__2::pair"* @_ZNSt3__29addressofINS_4pairIKN5draco17GeometryAttribute4TypeENS2_7OptionsEEEEEPT_RS8_(%"struct.std::__2::pair"* nonnull align 4 dereferenceable(16) %call) #9
  ret %"struct.std::__2::pair"* %call1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_node"* @_ZNKSt3__210unique_ptrINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPvEENS_22__tree_node_destructorINS_9allocatorIS9_EEEEEptEv(%"class.std::__2::unique_ptr.172"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.172"*, align 4
  store %"class.std::__2::unique_ptr.172"* %this, %"class.std::__2::unique_ptr.172"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.172"*, %"class.std::__2::unique_ptr.172"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.172", %"class.std::__2::unique_ptr.172"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_node"** @_ZNKSt3__217__compressed_pairIPNS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPvEENS_22__tree_node_destructorINS_9allocatorIS9_EEEEE5firstEv(%"class.std::__2::__compressed_pair.173"* %__ptr_) #9
  %0 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %call, align 4
  ret %"class.std::__2::__tree_node"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(5) %"class.std::__2::__tree_node_destructor"* @_ZNSt3__210unique_ptrINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPvEENS_22__tree_node_destructorINS_9allocatorIS9_EEEEE11get_deleterEv(%"class.std::__2::unique_ptr.172"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.172"*, align 4
  store %"class.std::__2::unique_ptr.172"* %this, %"class.std::__2::unique_ptr.172"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.172"*, %"class.std::__2::unique_ptr.172"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.172", %"class.std::__2::unique_ptr.172"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(5) %"class.std::__2::__tree_node_destructor"* @_ZNSt3__217__compressed_pairIPNS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPvEENS_22__tree_node_destructorINS_9allocatorIS9_EEEEE6secondEv(%"class.std::__2::__compressed_pair.173"* %__ptr_) #9
  ret %"class.std::__2::__tree_node_destructor"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.125"* @_ZNSt3__217__compressed_pairINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEENS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENSA_7OptionsEEES3_EEEEE6secondEv(%"class.std::__2::__compressed_pair.123"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.123"*, align 4
  store %"class.std::__2::__compressed_pair.123"* %this, %"class.std::__2::__compressed_pair.123"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.123"*, %"class.std::__2::__compressed_pair.123"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.123"* %this1 to %"struct.std::__2::__compressed_pair_elem.124"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.125"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS4_7OptionsEEEPvEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.124"* %0) #9
  ret %"class.std::__2::allocator.125"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.125"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS4_7OptionsEEEPvEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.124"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.124"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.124"* %this, %"struct.std::__2::__compressed_pair_elem.124"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.124"*, %"struct.std::__2::__compressed_pair_elem.124"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.124"* %this1 to %"class.std::__2::allocator.125"*
  ret %"class.std::__2::allocator.125"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_node"* @_ZNSt3__29allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPvEEE8allocateEmPKv(%"class.std::__2::allocator.125"* %this, i32 %__n, i8* %0) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.125"*, align 4
  %__n.addr = alloca i32, align 4
  %.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.125"* %this, %"class.std::__2::allocator.125"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::allocator.125"*, %"class.std::__2::allocator.125"** %this.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPvEEE8max_sizeEv(%"class.std::__2::allocator.125"* %this1) #9
  %cmp = icmp ugt i32 %1, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__220__throw_length_errorEPKc(i8* getelementptr inbounds ([68 x i8], [68 x i8]* @.str.4, i32 0, i32 0)) #10
  unreachable

if.end:                                           ; preds = %entry
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 32
  %call2 = call i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %mul, i32 4)
  %3 = bitcast i8* %call2 to %"class.std::__2::__tree_node"*
  ret %"class.std::__2::__tree_node"* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__29allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPvEEE8max_sizeEv(%"class.std::__2::allocator.125"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.125"*, align 4
  store %"class.std::__2::allocator.125"* %this, %"class.std::__2::allocator.125"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.125"*, %"class.std::__2::allocator.125"** %this.addr, align 4
  ret i32 134217727
}

; Function Attrs: noinline noreturn nounwind optnone
define linkonce_odr hidden void @_ZNSt3__220__throw_length_errorEPKc(i8* %__msg) #5 comdat {
entry:
  %__msg.addr = alloca i8*, align 4
  store i8* %__msg, i8** %__msg.addr, align 4
  call void @abort() #10
  unreachable
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %__size, i32 %__align) #0 comdat {
entry:
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i32, i32* %__size.addr, align 4
  %call = call noalias nonnull i8* @_Znwm(i32 %0) #8
  ret i8* %call
}

; Function Attrs: noreturn
declare void @abort() #6

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(5) %"class.std::__2::__tree_node_destructor"* @_ZNSt3__24moveIRNS_22__tree_node_destructorINS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS5_7OptionsEEEPvEEEEEEEEONS_16remove_referenceIT_E4typeEOSG_(%"class.std::__2::__tree_node_destructor"* nonnull align 4 dereferenceable(5) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::__tree_node_destructor"*, align 4
  store %"class.std::__2::__tree_node_destructor"* %__t, %"class.std::__2::__tree_node_destructor"** %__t.addr, align 4
  %0 = load %"class.std::__2::__tree_node_destructor"*, %"class.std::__2::__tree_node_destructor"** %__t.addr, align 4
  ret %"class.std::__2::__tree_node_destructor"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.173"* @_ZNSt3__217__compressed_pairIPNS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPvEENS_22__tree_node_destructorINS_9allocatorIS9_EEEEEC2IRSA_SE_EEOT_OT0_(%"class.std::__2::__compressed_pair.173"* returned %this, %"class.std::__2::__tree_node"** nonnull align 4 dereferenceable(4) %__t1, %"class.std::__2::__tree_node_destructor"* nonnull align 4 dereferenceable(5) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.173"*, align 4
  %__t1.addr = alloca %"class.std::__2::__tree_node"**, align 4
  %__t2.addr = alloca %"class.std::__2::__tree_node_destructor"*, align 4
  store %"class.std::__2::__compressed_pair.173"* %this, %"class.std::__2::__compressed_pair.173"** %this.addr, align 4
  store %"class.std::__2::__tree_node"** %__t1, %"class.std::__2::__tree_node"*** %__t1.addr, align 4
  store %"class.std::__2::__tree_node_destructor"* %__t2, %"class.std::__2::__tree_node_destructor"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.173"*, %"class.std::__2::__compressed_pair.173"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.173"* %this1 to %"struct.std::__2::__compressed_pair_elem.174"*
  %1 = load %"class.std::__2::__tree_node"**, %"class.std::__2::__tree_node"*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_node"** @_ZNSt3__27forwardIRPNS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPvEEEEOT_RNS_16remove_referenceISC_E4typeE(%"class.std::__2::__tree_node"** nonnull align 4 dereferenceable(4) %1) #9
  %call2 = call %"struct.std::__2::__compressed_pair_elem.174"* @_ZNSt3__222__compressed_pair_elemIPNS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPvEELi0ELb0EEC2IRSA_vEEOT_(%"struct.std::__2::__compressed_pair_elem.174"* %0, %"class.std::__2::__tree_node"** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.173"* %this1 to i8*
  %3 = getelementptr inbounds i8, i8* %2, i32 4
  %4 = bitcast i8* %3 to %"struct.std::__2::__compressed_pair_elem.175"*
  %5 = load %"class.std::__2::__tree_node_destructor"*, %"class.std::__2::__tree_node_destructor"** %__t2.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(5) %"class.std::__2::__tree_node_destructor"* @_ZNSt3__27forwardINS_22__tree_node_destructorINS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS5_7OptionsEEEPvEEEEEEEEOT_RNS_16remove_referenceISE_E4typeE(%"class.std::__2::__tree_node_destructor"* nonnull align 4 dereferenceable(5) %5) #9
  %call4 = call %"struct.std::__2::__compressed_pair_elem.175"* @_ZNSt3__222__compressed_pair_elemINS_22__tree_node_destructorINS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS5_7OptionsEEEPvEEEEEELi1ELb0EEC2ISD_vEEOT_(%"struct.std::__2::__compressed_pair_elem.175"* %4, %"class.std::__2::__tree_node_destructor"* nonnull align 4 dereferenceable(5) %call3)
  ret %"class.std::__2::__compressed_pair.173"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_node"** @_ZNSt3__27forwardIRPNS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPvEEEEOT_RNS_16remove_referenceISC_E4typeE(%"class.std::__2::__tree_node"** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::__tree_node"**, align 4
  store %"class.std::__2::__tree_node"** %__t, %"class.std::__2::__tree_node"*** %__t.addr, align 4
  %0 = load %"class.std::__2::__tree_node"**, %"class.std::__2::__tree_node"*** %__t.addr, align 4
  ret %"class.std::__2::__tree_node"** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.174"* @_ZNSt3__222__compressed_pair_elemIPNS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPvEELi0ELb0EEC2IRSA_vEEOT_(%"struct.std::__2::__compressed_pair_elem.174"* returned %this, %"class.std::__2::__tree_node"** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.174"*, align 4
  %__u.addr = alloca %"class.std::__2::__tree_node"**, align 4
  store %"struct.std::__2::__compressed_pair_elem.174"* %this, %"struct.std::__2::__compressed_pair_elem.174"** %this.addr, align 4
  store %"class.std::__2::__tree_node"** %__u, %"class.std::__2::__tree_node"*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.174"*, %"struct.std::__2::__compressed_pair_elem.174"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.174", %"struct.std::__2::__compressed_pair_elem.174"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::__tree_node"**, %"class.std::__2::__tree_node"*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_node"** @_ZNSt3__27forwardIRPNS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPvEEEEOT_RNS_16remove_referenceISC_E4typeE(%"class.std::__2::__tree_node"** nonnull align 4 dereferenceable(4) %0) #9
  %1 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %call, align 4
  store %"class.std::__2::__tree_node"* %1, %"class.std::__2::__tree_node"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.174"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(5) %"class.std::__2::__tree_node_destructor"* @_ZNSt3__27forwardINS_22__tree_node_destructorINS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS5_7OptionsEEEPvEEEEEEEEOT_RNS_16remove_referenceISE_E4typeE(%"class.std::__2::__tree_node_destructor"* nonnull align 4 dereferenceable(5) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::__tree_node_destructor"*, align 4
  store %"class.std::__2::__tree_node_destructor"* %__t, %"class.std::__2::__tree_node_destructor"** %__t.addr, align 4
  %0 = load %"class.std::__2::__tree_node_destructor"*, %"class.std::__2::__tree_node_destructor"** %__t.addr, align 4
  ret %"class.std::__2::__tree_node_destructor"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.175"* @_ZNSt3__222__compressed_pair_elemINS_22__tree_node_destructorINS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS5_7OptionsEEEPvEEEEEELi1ELb0EEC2ISD_vEEOT_(%"struct.std::__2::__compressed_pair_elem.175"* returned %this, %"class.std::__2::__tree_node_destructor"* nonnull align 4 dereferenceable(5) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.175"*, align 4
  %__u.addr = alloca %"class.std::__2::__tree_node_destructor"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.175"* %this, %"struct.std::__2::__compressed_pair_elem.175"** %this.addr, align 4
  store %"class.std::__2::__tree_node_destructor"* %__u, %"class.std::__2::__tree_node_destructor"** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.175"*, %"struct.std::__2::__compressed_pair_elem.175"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.175", %"struct.std::__2::__compressed_pair_elem.175"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::__tree_node_destructor"*, %"class.std::__2::__tree_node_destructor"** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(5) %"class.std::__2::__tree_node_destructor"* @_ZNSt3__27forwardINS_22__tree_node_destructorINS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS5_7OptionsEEEPvEEEEEEEEOT_RNS_16remove_referenceISE_E4typeE(%"class.std::__2::__tree_node_destructor"* nonnull align 4 dereferenceable(5) %0) #9
  %1 = bitcast %"class.std::__2::__tree_node_destructor"* %__value_ to i8*
  %2 = bitcast %"class.std::__2::__tree_node_destructor"* %call to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 8, i1 false)
  ret %"struct.std::__2::__compressed_pair_elem.175"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS4_7OptionsEEEPvEEEEE11__constructINS_4pairIKS6_S7_EEJNSE_IS6_S7_EEEEEvNS_17integral_constantIbLb1EEERSB_PT_DpOT0_(%"class.std::__2::allocator.125"* nonnull align 1 dereferenceable(1) %__a, %"struct.std::__2::pair"* %__p, %"struct.std::__2::pair.169"* nonnull align 4 dereferenceable(16) %__args) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.125"*, align 4
  %__p.addr = alloca %"struct.std::__2::pair"*, align 4
  %__args.addr = alloca %"struct.std::__2::pair.169"*, align 4
  store %"class.std::__2::allocator.125"* %__a, %"class.std::__2::allocator.125"** %__a.addr, align 4
  store %"struct.std::__2::pair"* %__p, %"struct.std::__2::pair"** %__p.addr, align 4
  store %"struct.std::__2::pair.169"* %__args, %"struct.std::__2::pair.169"** %__args.addr, align 4
  %1 = load %"class.std::__2::allocator.125"*, %"class.std::__2::allocator.125"** %__a.addr, align 4
  %2 = load %"struct.std::__2::pair"*, %"struct.std::__2::pair"** %__p.addr, align 4
  %3 = load %"struct.std::__2::pair.169"*, %"struct.std::__2::pair.169"** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %"struct.std::__2::pair.169"* @_ZNSt3__27forwardINS_4pairIN5draco17GeometryAttribute4TypeENS2_7OptionsEEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"struct.std::__2::pair.169"* nonnull align 4 dereferenceable(16) %3) #9
  call void @_ZNSt3__29allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPvEEE9constructINS_4pairIKS5_S6_EEJNSC_IS5_S6_EEEEEvPT_DpOT0_(%"class.std::__2::allocator.125"* %1, %"struct.std::__2::pair"* %2, %"struct.std::__2::pair.169"* nonnull align 4 dereferenceable(16) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPvEEE9constructINS_4pairIKS5_S6_EEJNSC_IS5_S6_EEEEEvPT_DpOT0_(%"class.std::__2::allocator.125"* %this, %"struct.std::__2::pair"* %__p, %"struct.std::__2::pair.169"* nonnull align 4 dereferenceable(16) %__args) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.125"*, align 4
  %__p.addr = alloca %"struct.std::__2::pair"*, align 4
  %__args.addr = alloca %"struct.std::__2::pair.169"*, align 4
  store %"class.std::__2::allocator.125"* %this, %"class.std::__2::allocator.125"** %this.addr, align 4
  store %"struct.std::__2::pair"* %__p, %"struct.std::__2::pair"** %__p.addr, align 4
  store %"struct.std::__2::pair.169"* %__args, %"struct.std::__2::pair.169"** %__args.addr, align 4
  %this1 = load %"class.std::__2::allocator.125"*, %"class.std::__2::allocator.125"** %this.addr, align 4
  %0 = load %"struct.std::__2::pair"*, %"struct.std::__2::pair"** %__p.addr, align 4
  %1 = bitcast %"struct.std::__2::pair"* %0 to i8*
  %2 = bitcast i8* %1 to %"struct.std::__2::pair"*
  %3 = load %"struct.std::__2::pair.169"*, %"struct.std::__2::pair.169"** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %"struct.std::__2::pair.169"* @_ZNSt3__27forwardINS_4pairIN5draco17GeometryAttribute4TypeENS2_7OptionsEEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"struct.std::__2::pair.169"* nonnull align 4 dereferenceable(16) %3) #9
  %call2 = call %"struct.std::__2::pair"* @_ZNSt3__24pairIKN5draco17GeometryAttribute4TypeENS1_7OptionsEEC2IS3_S5_Lb0EEEONS0_IT_T0_EE(%"struct.std::__2::pair"* %2, %"struct.std::__2::pair.169"* nonnull align 4 dereferenceable(16) %call) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::pair"* @_ZNSt3__24pairIKN5draco17GeometryAttribute4TypeENS1_7OptionsEEC2IS3_S5_Lb0EEEONS0_IT_T0_EE(%"struct.std::__2::pair"* returned %this, %"struct.std::__2::pair.169"* nonnull align 4 dereferenceable(16) %__p) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::pair"*, align 4
  %__p.addr = alloca %"struct.std::__2::pair.169"*, align 4
  store %"struct.std::__2::pair"* %this, %"struct.std::__2::pair"** %this.addr, align 4
  store %"struct.std::__2::pair.169"* %__p, %"struct.std::__2::pair.169"** %__p.addr, align 4
  %this1 = load %"struct.std::__2::pair"*, %"struct.std::__2::pair"** %this.addr, align 4
  %first = getelementptr inbounds %"struct.std::__2::pair", %"struct.std::__2::pair"* %this1, i32 0, i32 0
  %0 = load %"struct.std::__2::pair.169"*, %"struct.std::__2::pair.169"** %__p.addr, align 4
  %first2 = getelementptr inbounds %"struct.std::__2::pair.169", %"struct.std::__2::pair.169"* %0, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__27forwardIN5draco17GeometryAttribute4TypeEEEOT_RNS_16remove_referenceIS4_E4typeE(i32* nonnull align 4 dereferenceable(4) %first2) #9
  %1 = load i32, i32* %call, align 4
  store i32 %1, i32* %first, align 4
  %second = getelementptr inbounds %"struct.std::__2::pair", %"struct.std::__2::pair"* %this1, i32 0, i32 1
  %2 = load %"struct.std::__2::pair.169"*, %"struct.std::__2::pair.169"** %__p.addr, align 4
  %second3 = getelementptr inbounds %"struct.std::__2::pair.169", %"struct.std::__2::pair.169"* %2, i32 0, i32 1
  %call4 = call nonnull align 4 dereferenceable(12) %"class.draco::Options"* @_ZNSt3__27forwardIN5draco7OptionsEEEOT_RNS_16remove_referenceIS3_E4typeE(%"class.draco::Options"* nonnull align 4 dereferenceable(12) %second3) #9
  %call5 = call %"class.draco::Options"* @_ZN5draco7OptionsC2EOS0_(%"class.draco::Options"* %second, %"class.draco::Options"* nonnull align 4 dereferenceable(12) %call4) #9
  ret %"struct.std::__2::pair"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__27forwardIN5draco17GeometryAttribute4TypeEEEOT_RNS_16remove_referenceIS4_E4typeE(i32* nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca i32*, align 4
  store i32* %__t, i32** %__t.addr, align 4
  %0 = load i32*, i32** %__t.addr, align 4
  ret i32* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"class.draco::Options"* @_ZNSt3__27forwardIN5draco7OptionsEEEOT_RNS_16remove_referenceIS3_E4typeE(%"class.draco::Options"* nonnull align 4 dereferenceable(12) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.draco::Options"*, align 4
  store %"class.draco::Options"* %__t, %"class.draco::Options"** %__t.addr, align 4
  %0 = load %"class.draco::Options"*, %"class.draco::Options"** %__t.addr, align 4
  ret %"class.draco::Options"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::Options"* @_ZN5draco7OptionsC2EOS0_(%"class.draco::Options"* returned %this, %"class.draco::Options"* nonnull align 4 dereferenceable(12) %0) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::Options"*, align 4
  %.addr = alloca %"class.draco::Options"*, align 4
  store %"class.draco::Options"* %this, %"class.draco::Options"** %this.addr, align 4
  store %"class.draco::Options"* %0, %"class.draco::Options"** %.addr, align 4
  %this1 = load %"class.draco::Options"*, %"class.draco::Options"** %this.addr, align 4
  %options_ = getelementptr inbounds %"class.draco::Options", %"class.draco::Options"* %this1, i32 0, i32 0
  %1 = load %"class.draco::Options"*, %"class.draco::Options"** %.addr, align 4
  %options_2 = getelementptr inbounds %"class.draco::Options", %"class.draco::Options"* %1, i32 0, i32 0
  %call = call %"class.std::__2::map"* @_ZNSt3__23mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_NS_4lessIS6_EENS4_INS_4pairIKS6_S6_EEEEEC2EOSD_(%"class.std::__2::map"* %options_, %"class.std::__2::map"* nonnull align 4 dereferenceable(12) %options_2) #9
  ret %"class.draco::Options"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::map"* @_ZNSt3__23mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_NS_4lessIS6_EENS4_INS_4pairIKS6_S6_EEEEEC2EOSD_(%"class.std::__2::map"* returned %this, %"class.std::__2::map"* nonnull align 4 dereferenceable(12) %__m) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::map"*, align 4
  %__m.addr = alloca %"class.std::__2::map"*, align 4
  store %"class.std::__2::map"* %this, %"class.std::__2::map"** %this.addr, align 4
  store %"class.std::__2::map"* %__m, %"class.std::__2::map"** %__m.addr, align 4
  %this1 = load %"class.std::__2::map"*, %"class.std::__2::map"** %this.addr, align 4
  %__tree_ = getelementptr inbounds %"class.std::__2::map", %"class.std::__2::map"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::map"*, %"class.std::__2::map"** %__m.addr, align 4
  %__tree_2 = getelementptr inbounds %"class.std::__2::map", %"class.std::__2::map"* %0, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(12) %"class.std::__2::__tree"* @_ZNSt3__24moveIRNS_6__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EENS_19__map_value_compareIS8_S9_NS_4lessIS8_EELb1EEENS6_IS9_EEEEEEONS_16remove_referenceIT_E4typeEOSI_(%"class.std::__2::__tree"* nonnull align 4 dereferenceable(12) %__tree_2) #9
  %call3 = call %"class.std::__2::__tree"* @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEEC2EOSE_(%"class.std::__2::__tree"* %__tree_, %"class.std::__2::__tree"* nonnull align 4 dereferenceable(12) %call) #9
  ret %"class.std::__2::map"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"class.std::__2::__tree"* @_ZNSt3__24moveIRNS_6__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EENS_19__map_value_compareIS8_S9_NS_4lessIS8_EELb1EEENS6_IS9_EEEEEEONS_16remove_referenceIT_E4typeEOSI_(%"class.std::__2::__tree"* nonnull align 4 dereferenceable(12) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::__tree"*, align 4
  store %"class.std::__2::__tree"* %__t, %"class.std::__2::__tree"** %__t.addr, align 4
  %0 = load %"class.std::__2::__tree"*, %"class.std::__2::__tree"** %__t.addr, align 4
  ret %"class.std::__2::__tree"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree"* @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEEC2EOSE_(%"class.std::__2::__tree"* returned %this, %"class.std::__2::__tree"* nonnull align 4 dereferenceable(12) %__t) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__tree"*, align 4
  %this.addr = alloca %"class.std::__2::__tree"*, align 4
  %__t.addr = alloca %"class.std::__2::__tree"*, align 4
  store %"class.std::__2::__tree"* %this, %"class.std::__2::__tree"** %this.addr, align 4
  store %"class.std::__2::__tree"* %__t, %"class.std::__2::__tree"** %__t.addr, align 4
  %this1 = load %"class.std::__2::__tree"*, %"class.std::__2::__tree"** %this.addr, align 4
  store %"class.std::__2::__tree"* %this1, %"class.std::__2::__tree"** %retval, align 4
  %__begin_node_ = getelementptr inbounds %"class.std::__2::__tree", %"class.std::__2::__tree"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::__tree"*, %"class.std::__2::__tree"** %__t.addr, align 4
  %__begin_node_2 = getelementptr inbounds %"class.std::__2::__tree", %"class.std::__2::__tree"* %0, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_end_node"** @_ZNSt3__24moveIRPNS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEEEEONS_16remove_referenceIT_E4typeEOSA_(%"class.std::__2::__tree_end_node"** nonnull align 4 dereferenceable(4) %__begin_node_2) #9
  %1 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %call, align 4
  store %"class.std::__2::__tree_end_node"* %1, %"class.std::__2::__tree_end_node"** %__begin_node_, align 4
  %__pair1_ = getelementptr inbounds %"class.std::__2::__tree", %"class.std::__2::__tree"* %this1, i32 0, i32 1
  %2 = load %"class.std::__2::__tree"*, %"class.std::__2::__tree"** %__t.addr, align 4
  %__pair1_3 = getelementptr inbounds %"class.std::__2::__tree", %"class.std::__2::__tree"* %2, i32 0, i32 1
  %call4 = call nonnull align 4 dereferenceable(4) %"class.std::__2::__compressed_pair.114"* @_ZNSt3__24moveIRNS_17__compressed_pairINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEENS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS8_IcEEEESF_EES4_EEEEEEEEONS_16remove_referenceIT_E4typeEOSM_(%"class.std::__2::__compressed_pair.114"* nonnull align 4 dereferenceable(4) %__pair1_3) #9
  %3 = bitcast %"class.std::__2::__compressed_pair.114"* %__pair1_ to i8*
  %4 = bitcast %"class.std::__2::__compressed_pair.114"* %call4 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 4, i1 false)
  %__pair3_ = getelementptr inbounds %"class.std::__2::__tree", %"class.std::__2::__tree"* %this1, i32 0, i32 2
  %5 = load %"class.std::__2::__tree"*, %"class.std::__2::__tree"** %__t.addr, align 4
  %__pair3_5 = getelementptr inbounds %"class.std::__2::__tree", %"class.std::__2::__tree"* %5, i32 0, i32 2
  %call6 = call nonnull align 4 dereferenceable(4) %"class.std::__2::__compressed_pair.119"* @_ZNSt3__24moveIRNS_17__compressed_pairImNS_19__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS8_S8_EENS_4lessIS8_EELb1EEEEEEEONS_16remove_referenceIT_E4typeEOSH_(%"class.std::__2::__compressed_pair.119"* nonnull align 4 dereferenceable(4) %__pair3_5) #9
  %6 = bitcast %"class.std::__2::__compressed_pair.119"* %__pair3_ to i8*
  %7 = bitcast %"class.std::__2::__compressed_pair.119"* %call6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 4, i1 false)
  %call7 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE4sizeEv(%"class.std::__2::__tree"* %this1) #9
  %8 = load i32, i32* %call7, align 4
  %cmp = icmp eq i32 %8, 0
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %call8 = call %"class.std::__2::__tree_end_node"* @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE10__end_nodeEv(%"class.std::__2::__tree"* %this1) #9
  %call9 = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_end_node"** @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE12__begin_nodeEv(%"class.std::__2::__tree"* %this1) #9
  store %"class.std::__2::__tree_end_node"* %call8, %"class.std::__2::__tree_end_node"** %call9, align 4
  br label %if.end

if.else:                                          ; preds = %entry
  %call10 = call %"class.std::__2::__tree_end_node"* @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE10__end_nodeEv(%"class.std::__2::__tree"* %this1) #9
  %call11 = call %"class.std::__2::__tree_end_node"* @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE10__end_nodeEv(%"class.std::__2::__tree"* %this1) #9
  %__left_ = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %call11, i32 0, i32 0
  %9 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__left_, align 4
  %__parent_ = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %9, i32 0, i32 2
  store %"class.std::__2::__tree_end_node"* %call10, %"class.std::__2::__tree_end_node"** %__parent_, align 4
  %10 = load %"class.std::__2::__tree"*, %"class.std::__2::__tree"** %__t.addr, align 4
  %call12 = call %"class.std::__2::__tree_end_node"* @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE10__end_nodeEv(%"class.std::__2::__tree"* %10) #9
  %11 = load %"class.std::__2::__tree"*, %"class.std::__2::__tree"** %__t.addr, align 4
  %call13 = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_end_node"** @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE12__begin_nodeEv(%"class.std::__2::__tree"* %11) #9
  store %"class.std::__2::__tree_end_node"* %call12, %"class.std::__2::__tree_end_node"** %call13, align 4
  %12 = load %"class.std::__2::__tree"*, %"class.std::__2::__tree"** %__t.addr, align 4
  %call14 = call %"class.std::__2::__tree_end_node"* @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE10__end_nodeEv(%"class.std::__2::__tree"* %12) #9
  %__left_15 = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %call14, i32 0, i32 0
  store %"class.std::__2::__tree_node_base"* null, %"class.std::__2::__tree_node_base"** %__left_15, align 4
  %13 = load %"class.std::__2::__tree"*, %"class.std::__2::__tree"** %__t.addr, align 4
  %call16 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE4sizeEv(%"class.std::__2::__tree"* %13) #9
  store i32 0, i32* %call16, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %14 = load %"class.std::__2::__tree"*, %"class.std::__2::__tree"** %retval, align 4
  ret %"class.std::__2::__tree"* %14
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_end_node"** @_ZNSt3__24moveIRPNS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEEEEONS_16remove_referenceIT_E4typeEOSA_(%"class.std::__2::__tree_end_node"** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::__tree_end_node"**, align 4
  store %"class.std::__2::__tree_end_node"** %__t, %"class.std::__2::__tree_end_node"*** %__t.addr, align 4
  %0 = load %"class.std::__2::__tree_end_node"**, %"class.std::__2::__tree_end_node"*** %__t.addr, align 4
  ret %"class.std::__2::__tree_end_node"** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__compressed_pair.114"* @_ZNSt3__24moveIRNS_17__compressed_pairINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEENS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS8_IcEEEESF_EES4_EEEEEEEEONS_16remove_referenceIT_E4typeEOSM_(%"class.std::__2::__compressed_pair.114"* nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::__compressed_pair.114"*, align 4
  store %"class.std::__2::__compressed_pair.114"* %__t, %"class.std::__2::__compressed_pair.114"** %__t.addr, align 4
  %0 = load %"class.std::__2::__compressed_pair.114"*, %"class.std::__2::__compressed_pair.114"** %__t.addr, align 4
  ret %"class.std::__2::__compressed_pair.114"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__compressed_pair.119"* @_ZNSt3__24moveIRNS_17__compressed_pairImNS_19__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS8_S8_EENS_4lessIS8_EELb1EEEEEEEONS_16remove_referenceIT_E4typeEOSH_(%"class.std::__2::__compressed_pair.119"* nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::__compressed_pair.119"*, align 4
  store %"class.std::__2::__compressed_pair.119"* %__t, %"class.std::__2::__compressed_pair.119"** %__t.addr, align 4
  %0 = load %"class.std::__2::__compressed_pair.119"*, %"class.std::__2::__compressed_pair.119"** %__t.addr, align 4
  ret %"class.std::__2::__compressed_pair.119"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE4sizeEv(%"class.std::__2::__tree"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree"*, align 4
  store %"class.std::__2::__tree"* %this, %"class.std::__2::__tree"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree"*, %"class.std::__2::__tree"** %this.addr, align 4
  %__pair3_ = getelementptr inbounds %"class.std::__2::__tree", %"class.std::__2::__tree"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__217__compressed_pairImNS_19__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS7_S7_EENS_4lessIS7_EELb1EEEE5firstEv(%"class.std::__2::__compressed_pair.119"* %__pair3_) #9
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_end_node"* @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE10__end_nodeEv(%"class.std::__2::__tree"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree"*, align 4
  store %"class.std::__2::__tree"* %this, %"class.std::__2::__tree"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree"*, %"class.std::__2::__tree"** %this.addr, align 4
  %__pair1_ = getelementptr inbounds %"class.std::__2::__tree", %"class.std::__2::__tree"* %this1, i32 0, i32 1
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_end_node"* @_ZNSt3__217__compressed_pairINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEENS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS7_IcEEEESE_EES3_EEEEE5firstEv(%"class.std::__2::__compressed_pair.114"* %__pair1_) #9
  %call2 = call %"class.std::__2::__tree_end_node"* @_ZNSt3__214pointer_traitsIPNS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEEE10pointer_toERS6_(%"class.std::__2::__tree_end_node"* nonnull align 4 dereferenceable(4) %call) #9
  ret %"class.std::__2::__tree_end_node"* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_end_node"** @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE12__begin_nodeEv(%"class.std::__2::__tree"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree"*, align 4
  store %"class.std::__2::__tree"* %this, %"class.std::__2::__tree"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree"*, %"class.std::__2::__tree"** %this.addr, align 4
  %__begin_node_ = getelementptr inbounds %"class.std::__2::__tree", %"class.std::__2::__tree"* %this1, i32 0, i32 0
  ret %"class.std::__2::__tree_end_node"** %__begin_node_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__217__compressed_pairImNS_19__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS7_S7_EENS_4lessIS7_EELb1EEEE5firstEv(%"class.std::__2::__compressed_pair.119"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.119"*, align 4
  store %"class.std::__2::__compressed_pair.119"* %this, %"class.std::__2::__compressed_pair.119"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.119"*, %"class.std::__2::__compressed_pair.119"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.119"* %this1 to %"struct.std::__2::__compressed_pair_elem.11"*
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__222__compressed_pair_elemImLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.11"* %0) #9
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__222__compressed_pair_elemImLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.11"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.11"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.11"* %this, %"struct.std::__2::__compressed_pair_elem.11"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.11"*, %"struct.std::__2::__compressed_pair_elem.11"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.11", %"struct.std::__2::__compressed_pair_elem.11"* %this1, i32 0, i32 0
  ret i32* %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_end_node"* @_ZNSt3__217__compressed_pairINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEENS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS7_IcEEEESE_EES3_EEEEE5firstEv(%"class.std::__2::__compressed_pair.114"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.114"*, align 4
  store %"class.std::__2::__compressed_pair.114"* %this, %"class.std::__2::__compressed_pair.114"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.114"*, %"class.std::__2::__compressed_pair.114"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.114"* %this1 to %"struct.std::__2::__compressed_pair_elem.115"*
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_end_node"* @_ZNSt3__222__compressed_pair_elemINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.115"* %0) #9
  ret %"class.std::__2::__tree_end_node"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_node"** @_ZNKSt3__217__compressed_pairIPNS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPvEENS_22__tree_node_destructorINS_9allocatorIS9_EEEEE5firstEv(%"class.std::__2::__compressed_pair.173"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.173"*, align 4
  store %"class.std::__2::__compressed_pair.173"* %this, %"class.std::__2::__compressed_pair.173"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.173"*, %"class.std::__2::__compressed_pair.173"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.173"* %this1 to %"struct.std::__2::__compressed_pair_elem.174"*
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_node"** @_ZNKSt3__222__compressed_pair_elemIPNS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPvEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.174"* %0) #9
  ret %"class.std::__2::__tree_node"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_node"** @_ZNKSt3__222__compressed_pair_elemIPNS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPvEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.174"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.174"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.174"* %this, %"struct.std::__2::__compressed_pair_elem.174"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.174"*, %"struct.std::__2::__compressed_pair_elem.174"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.174", %"struct.std::__2::__compressed_pair_elem.174"* %this1, i32 0, i32 0
  ret %"class.std::__2::__tree_node"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(5) %"class.std::__2::__tree_node_destructor"* @_ZNSt3__217__compressed_pairIPNS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPvEENS_22__tree_node_destructorINS_9allocatorIS9_EEEEE6secondEv(%"class.std::__2::__compressed_pair.173"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.173"*, align 4
  store %"class.std::__2::__compressed_pair.173"* %this, %"class.std::__2::__compressed_pair.173"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.173"*, %"class.std::__2::__compressed_pair.173"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.173"* %this1 to i8*
  %add.ptr = getelementptr inbounds i8, i8* %0, i32 4
  %1 = bitcast i8* %add.ptr to %"struct.std::__2::__compressed_pair_elem.175"*
  %call = call nonnull align 4 dereferenceable(5) %"class.std::__2::__tree_node_destructor"* @_ZNSt3__222__compressed_pair_elemINS_22__tree_node_destructorINS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS5_7OptionsEEEPvEEEEEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.175"* %1) #9
  ret %"class.std::__2::__tree_node_destructor"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(5) %"class.std::__2::__tree_node_destructor"* @_ZNSt3__222__compressed_pair_elemINS_22__tree_node_destructorINS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS5_7OptionsEEEPvEEEEEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.175"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.175"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.175"* %this, %"struct.std::__2::__compressed_pair_elem.175"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.175"*, %"struct.std::__2::__compressed_pair_elem.175"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.175", %"struct.std::__2::__compressed_pair_elem.175"* %this1, i32 0, i32 0
  ret %"class.std::__2::__tree_node_destructor"* %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_end_node"** @_ZNSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE12__begin_nodeEv(%"class.std::__2::__tree.122"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree.122"*, align 4
  store %"class.std::__2::__tree.122"* %this, %"class.std::__2::__tree.122"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree.122"*, %"class.std::__2::__tree.122"** %this.addr, align 4
  %__begin_node_ = getelementptr inbounds %"class.std::__2::__tree.122", %"class.std::__2::__tree.122"* %this1, i32 0, i32 0
  ret %"class.std::__2::__tree_end_node"** %__begin_node_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__227__tree_balance_after_insertIPNS_16__tree_node_baseIPvEEEEvT_S5_(%"class.std::__2::__tree_node_base"* %__root, %"class.std::__2::__tree_node_base"* %__x) #0 comdat {
entry:
  %__root.addr = alloca %"class.std::__2::__tree_node_base"*, align 4
  %__x.addr = alloca %"class.std::__2::__tree_node_base"*, align 4
  %__y = alloca %"class.std::__2::__tree_node_base"*, align 4
  %__y27 = alloca %"class.std::__2::__tree_node_base"*, align 4
  store %"class.std::__2::__tree_node_base"* %__root, %"class.std::__2::__tree_node_base"** %__root.addr, align 4
  store %"class.std::__2::__tree_node_base"* %__x, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %0 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %1 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__root.addr, align 4
  %cmp = icmp eq %"class.std::__2::__tree_node_base"* %0, %1
  %2 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %__is_black_ = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %2, i32 0, i32 3
  %frombool = zext i1 %cmp to i8
  store i8 %frombool, i8* %__is_black_, align 4
  br label %while.cond

while.cond:                                       ; preds = %if.end51, %entry
  %3 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %4 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__root.addr, align 4
  %cmp1 = icmp ne %"class.std::__2::__tree_node_base"* %3, %4
  br i1 %cmp1, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %while.cond
  %5 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %call = call %"class.std::__2::__tree_node_base"* @_ZNKSt3__216__tree_node_baseIPvE15__parent_unsafeEv(%"class.std::__2::__tree_node_base"* %5)
  %__is_black_2 = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %call, i32 0, i32 3
  %6 = load i8, i8* %__is_black_2, align 4
  %tobool = trunc i8 %6 to i1
  %lnot = xor i1 %tobool, true
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.cond
  %7 = phi i1 [ false, %while.cond ], [ %lnot, %land.rhs ]
  br i1 %7, label %while.body, label %while.end

while.body:                                       ; preds = %land.end
  %8 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %call3 = call %"class.std::__2::__tree_node_base"* @_ZNKSt3__216__tree_node_baseIPvE15__parent_unsafeEv(%"class.std::__2::__tree_node_base"* %8)
  %call4 = call zeroext i1 @_ZNSt3__220__tree_is_left_childIPNS_16__tree_node_baseIPvEEEEbT_(%"class.std::__2::__tree_node_base"* %call3) #9
  br i1 %call4, label %if.then, label %if.else26

if.then:                                          ; preds = %while.body
  %9 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %call5 = call %"class.std::__2::__tree_node_base"* @_ZNKSt3__216__tree_node_baseIPvE15__parent_unsafeEv(%"class.std::__2::__tree_node_base"* %9)
  %call6 = call %"class.std::__2::__tree_node_base"* @_ZNKSt3__216__tree_node_baseIPvE15__parent_unsafeEv(%"class.std::__2::__tree_node_base"* %call5)
  %__right_ = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %call6, i32 0, i32 1
  %10 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__right_, align 4
  store %"class.std::__2::__tree_node_base"* %10, %"class.std::__2::__tree_node_base"** %__y, align 4
  %11 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__y, align 4
  %cmp7 = icmp ne %"class.std::__2::__tree_node_base"* %11, null
  br i1 %cmp7, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %if.then
  %12 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__y, align 4
  %__is_black_8 = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %12, i32 0, i32 3
  %13 = load i8, i8* %__is_black_8, align 4
  %tobool9 = trunc i8 %13 to i1
  br i1 %tobool9, label %if.else, label %if.then10

if.then10:                                        ; preds = %land.lhs.true
  %14 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %call11 = call %"class.std::__2::__tree_node_base"* @_ZNKSt3__216__tree_node_baseIPvE15__parent_unsafeEv(%"class.std::__2::__tree_node_base"* %14)
  store %"class.std::__2::__tree_node_base"* %call11, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %15 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %__is_black_12 = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %15, i32 0, i32 3
  store i8 1, i8* %__is_black_12, align 4
  %16 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %call13 = call %"class.std::__2::__tree_node_base"* @_ZNKSt3__216__tree_node_baseIPvE15__parent_unsafeEv(%"class.std::__2::__tree_node_base"* %16)
  store %"class.std::__2::__tree_node_base"* %call13, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %17 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %18 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__root.addr, align 4
  %cmp14 = icmp eq %"class.std::__2::__tree_node_base"* %17, %18
  %19 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %__is_black_15 = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %19, i32 0, i32 3
  %frombool16 = zext i1 %cmp14 to i8
  store i8 %frombool16, i8* %__is_black_15, align 4
  %20 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__y, align 4
  %__is_black_17 = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %20, i32 0, i32 3
  store i8 1, i8* %__is_black_17, align 4
  br label %if.end25

if.else:                                          ; preds = %land.lhs.true, %if.then
  %21 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %call18 = call zeroext i1 @_ZNSt3__220__tree_is_left_childIPNS_16__tree_node_baseIPvEEEEbT_(%"class.std::__2::__tree_node_base"* %21) #9
  br i1 %call18, label %if.end, label %if.then19

if.then19:                                        ; preds = %if.else
  %22 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %call20 = call %"class.std::__2::__tree_node_base"* @_ZNKSt3__216__tree_node_baseIPvE15__parent_unsafeEv(%"class.std::__2::__tree_node_base"* %22)
  store %"class.std::__2::__tree_node_base"* %call20, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %23 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  call void @_ZNSt3__218__tree_left_rotateIPNS_16__tree_node_baseIPvEEEEvT_(%"class.std::__2::__tree_node_base"* %23) #9
  br label %if.end

if.end:                                           ; preds = %if.then19, %if.else
  %24 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %call21 = call %"class.std::__2::__tree_node_base"* @_ZNKSt3__216__tree_node_baseIPvE15__parent_unsafeEv(%"class.std::__2::__tree_node_base"* %24)
  store %"class.std::__2::__tree_node_base"* %call21, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %25 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %__is_black_22 = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %25, i32 0, i32 3
  store i8 1, i8* %__is_black_22, align 4
  %26 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %call23 = call %"class.std::__2::__tree_node_base"* @_ZNKSt3__216__tree_node_baseIPvE15__parent_unsafeEv(%"class.std::__2::__tree_node_base"* %26)
  store %"class.std::__2::__tree_node_base"* %call23, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %27 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %__is_black_24 = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %27, i32 0, i32 3
  store i8 0, i8* %__is_black_24, align 4
  %28 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  call void @_ZNSt3__219__tree_right_rotateIPNS_16__tree_node_baseIPvEEEEvT_(%"class.std::__2::__tree_node_base"* %28) #9
  br label %while.end

if.end25:                                         ; preds = %if.then10
  br label %if.end51

if.else26:                                        ; preds = %while.body
  %29 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %call28 = call %"class.std::__2::__tree_node_base"* @_ZNKSt3__216__tree_node_baseIPvE15__parent_unsafeEv(%"class.std::__2::__tree_node_base"* %29)
  %__parent_ = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %call28, i32 0, i32 2
  %30 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %__parent_, align 4
  %__left_ = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %30, i32 0, i32 0
  %31 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__left_, align 4
  store %"class.std::__2::__tree_node_base"* %31, %"class.std::__2::__tree_node_base"** %__y27, align 4
  %32 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__y27, align 4
  %cmp29 = icmp ne %"class.std::__2::__tree_node_base"* %32, null
  br i1 %cmp29, label %land.lhs.true30, label %if.else41

land.lhs.true30:                                  ; preds = %if.else26
  %33 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__y27, align 4
  %__is_black_31 = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %33, i32 0, i32 3
  %34 = load i8, i8* %__is_black_31, align 4
  %tobool32 = trunc i8 %34 to i1
  br i1 %tobool32, label %if.else41, label %if.then33

if.then33:                                        ; preds = %land.lhs.true30
  %35 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %call34 = call %"class.std::__2::__tree_node_base"* @_ZNKSt3__216__tree_node_baseIPvE15__parent_unsafeEv(%"class.std::__2::__tree_node_base"* %35)
  store %"class.std::__2::__tree_node_base"* %call34, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %36 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %__is_black_35 = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %36, i32 0, i32 3
  store i8 1, i8* %__is_black_35, align 4
  %37 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %call36 = call %"class.std::__2::__tree_node_base"* @_ZNKSt3__216__tree_node_baseIPvE15__parent_unsafeEv(%"class.std::__2::__tree_node_base"* %37)
  store %"class.std::__2::__tree_node_base"* %call36, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %38 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %39 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__root.addr, align 4
  %cmp37 = icmp eq %"class.std::__2::__tree_node_base"* %38, %39
  %40 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %__is_black_38 = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %40, i32 0, i32 3
  %frombool39 = zext i1 %cmp37 to i8
  store i8 %frombool39, i8* %__is_black_38, align 4
  %41 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__y27, align 4
  %__is_black_40 = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %41, i32 0, i32 3
  store i8 1, i8* %__is_black_40, align 4
  br label %if.end50

if.else41:                                        ; preds = %land.lhs.true30, %if.else26
  %42 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %call42 = call zeroext i1 @_ZNSt3__220__tree_is_left_childIPNS_16__tree_node_baseIPvEEEEbT_(%"class.std::__2::__tree_node_base"* %42) #9
  br i1 %call42, label %if.then43, label %if.end45

if.then43:                                        ; preds = %if.else41
  %43 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %call44 = call %"class.std::__2::__tree_node_base"* @_ZNKSt3__216__tree_node_baseIPvE15__parent_unsafeEv(%"class.std::__2::__tree_node_base"* %43)
  store %"class.std::__2::__tree_node_base"* %call44, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %44 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  call void @_ZNSt3__219__tree_right_rotateIPNS_16__tree_node_baseIPvEEEEvT_(%"class.std::__2::__tree_node_base"* %44) #9
  br label %if.end45

if.end45:                                         ; preds = %if.then43, %if.else41
  %45 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %call46 = call %"class.std::__2::__tree_node_base"* @_ZNKSt3__216__tree_node_baseIPvE15__parent_unsafeEv(%"class.std::__2::__tree_node_base"* %45)
  store %"class.std::__2::__tree_node_base"* %call46, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %46 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %__is_black_47 = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %46, i32 0, i32 3
  store i8 1, i8* %__is_black_47, align 4
  %47 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %call48 = call %"class.std::__2::__tree_node_base"* @_ZNKSt3__216__tree_node_baseIPvE15__parent_unsafeEv(%"class.std::__2::__tree_node_base"* %47)
  store %"class.std::__2::__tree_node_base"* %call48, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %48 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %__is_black_49 = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %48, i32 0, i32 3
  store i8 0, i8* %__is_black_49, align 4
  %49 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  call void @_ZNSt3__218__tree_left_rotateIPNS_16__tree_node_baseIPvEEEEvT_(%"class.std::__2::__tree_node_base"* %49) #9
  br label %while.end

if.end50:                                         ; preds = %if.then33
  br label %if.end51

if.end51:                                         ; preds = %if.end50, %if.end25
  br label %while.cond

while.end:                                        ; preds = %if.end45, %if.end, %land.end
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::__tree.122"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree.122"*, align 4
  store %"class.std::__2::__tree.122"* %this, %"class.std::__2::__tree.122"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree.122"*, %"class.std::__2::__tree.122"** %this.addr, align 4
  %__pair3_ = getelementptr inbounds %"class.std::__2::__tree.122", %"class.std::__2::__tree.122"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__217__compressed_pairImNS_19__map_value_compareIN5draco17GeometryAttribute4TypeENS_12__value_typeIS4_NS2_7OptionsEEENS_4lessIS4_EELb1EEEE5firstEv(%"class.std::__2::__compressed_pair.127"* %__pair3_) #9
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_node_base"* @_ZNKSt3__216__tree_node_baseIPvE15__parent_unsafeEv(%"class.std::__2::__tree_node_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree_node_base"*, align 4
  store %"class.std::__2::__tree_node_base"* %this, %"class.std::__2::__tree_node_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %this.addr, align 4
  %__parent_ = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %this1, i32 0, i32 2
  %0 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %__parent_, align 4
  %1 = bitcast %"class.std::__2::__tree_end_node"* %0 to %"class.std::__2::__tree_node_base"*
  ret %"class.std::__2::__tree_node_base"* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNSt3__220__tree_is_left_childIPNS_16__tree_node_baseIPvEEEEbT_(%"class.std::__2::__tree_node_base"* %__x) #0 comdat {
entry:
  %__x.addr = alloca %"class.std::__2::__tree_node_base"*, align 4
  store %"class.std::__2::__tree_node_base"* %__x, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %0 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %1 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %__parent_ = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %1, i32 0, i32 2
  %2 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %__parent_, align 4
  %__left_ = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %2, i32 0, i32 0
  %3 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__left_, align 4
  %cmp = icmp eq %"class.std::__2::__tree_node_base"* %0, %3
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__218__tree_left_rotateIPNS_16__tree_node_baseIPvEEEEvT_(%"class.std::__2::__tree_node_base"* %__x) #0 comdat {
entry:
  %__x.addr = alloca %"class.std::__2::__tree_node_base"*, align 4
  %__y = alloca %"class.std::__2::__tree_node_base"*, align 4
  store %"class.std::__2::__tree_node_base"* %__x, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %0 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %__right_ = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %0, i32 0, i32 1
  %1 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__right_, align 4
  store %"class.std::__2::__tree_node_base"* %1, %"class.std::__2::__tree_node_base"** %__y, align 4
  %2 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__y, align 4
  %3 = bitcast %"class.std::__2::__tree_node_base"* %2 to %"class.std::__2::__tree_end_node"*
  %__left_ = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %3, i32 0, i32 0
  %4 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__left_, align 4
  %5 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %__right_1 = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %5, i32 0, i32 1
  store %"class.std::__2::__tree_node_base"* %4, %"class.std::__2::__tree_node_base"** %__right_1, align 4
  %6 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %__right_2 = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %6, i32 0, i32 1
  %7 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__right_2, align 4
  %cmp = icmp ne %"class.std::__2::__tree_node_base"* %7, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %8 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %__right_3 = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %8, i32 0, i32 1
  %9 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__right_3, align 4
  %10 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  call void @_ZNSt3__216__tree_node_baseIPvE12__set_parentEPS2_(%"class.std::__2::__tree_node_base"* %9, %"class.std::__2::__tree_node_base"* %10)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %11 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %__parent_ = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %11, i32 0, i32 2
  %12 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %__parent_, align 4
  %13 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__y, align 4
  %__parent_4 = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %13, i32 0, i32 2
  store %"class.std::__2::__tree_end_node"* %12, %"class.std::__2::__tree_end_node"** %__parent_4, align 4
  %14 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %call = call zeroext i1 @_ZNSt3__220__tree_is_left_childIPNS_16__tree_node_baseIPvEEEEbT_(%"class.std::__2::__tree_node_base"* %14) #9
  br i1 %call, label %if.then5, label %if.else

if.then5:                                         ; preds = %if.end
  %15 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__y, align 4
  %16 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %__parent_6 = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %16, i32 0, i32 2
  %17 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %__parent_6, align 4
  %__left_7 = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %17, i32 0, i32 0
  store %"class.std::__2::__tree_node_base"* %15, %"class.std::__2::__tree_node_base"** %__left_7, align 4
  br label %if.end10

if.else:                                          ; preds = %if.end
  %18 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__y, align 4
  %19 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %call8 = call %"class.std::__2::__tree_node_base"* @_ZNKSt3__216__tree_node_baseIPvE15__parent_unsafeEv(%"class.std::__2::__tree_node_base"* %19)
  %__right_9 = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %call8, i32 0, i32 1
  store %"class.std::__2::__tree_node_base"* %18, %"class.std::__2::__tree_node_base"** %__right_9, align 4
  br label %if.end10

if.end10:                                         ; preds = %if.else, %if.then5
  %20 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %21 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__y, align 4
  %22 = bitcast %"class.std::__2::__tree_node_base"* %21 to %"class.std::__2::__tree_end_node"*
  %__left_11 = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %22, i32 0, i32 0
  store %"class.std::__2::__tree_node_base"* %20, %"class.std::__2::__tree_node_base"** %__left_11, align 4
  %23 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %24 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__y, align 4
  call void @_ZNSt3__216__tree_node_baseIPvE12__set_parentEPS2_(%"class.std::__2::__tree_node_base"* %23, %"class.std::__2::__tree_node_base"* %24)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__219__tree_right_rotateIPNS_16__tree_node_baseIPvEEEEvT_(%"class.std::__2::__tree_node_base"* %__x) #0 comdat {
entry:
  %__x.addr = alloca %"class.std::__2::__tree_node_base"*, align 4
  %__y = alloca %"class.std::__2::__tree_node_base"*, align 4
  store %"class.std::__2::__tree_node_base"* %__x, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %0 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %1 = bitcast %"class.std::__2::__tree_node_base"* %0 to %"class.std::__2::__tree_end_node"*
  %__left_ = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %1, i32 0, i32 0
  %2 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__left_, align 4
  store %"class.std::__2::__tree_node_base"* %2, %"class.std::__2::__tree_node_base"** %__y, align 4
  %3 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__y, align 4
  %__right_ = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %3, i32 0, i32 1
  %4 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__right_, align 4
  %5 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %6 = bitcast %"class.std::__2::__tree_node_base"* %5 to %"class.std::__2::__tree_end_node"*
  %__left_1 = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %6, i32 0, i32 0
  store %"class.std::__2::__tree_node_base"* %4, %"class.std::__2::__tree_node_base"** %__left_1, align 4
  %7 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %8 = bitcast %"class.std::__2::__tree_node_base"* %7 to %"class.std::__2::__tree_end_node"*
  %__left_2 = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %8, i32 0, i32 0
  %9 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__left_2, align 4
  %cmp = icmp ne %"class.std::__2::__tree_node_base"* %9, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %10 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %11 = bitcast %"class.std::__2::__tree_node_base"* %10 to %"class.std::__2::__tree_end_node"*
  %__left_3 = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %11, i32 0, i32 0
  %12 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__left_3, align 4
  %13 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  call void @_ZNSt3__216__tree_node_baseIPvE12__set_parentEPS2_(%"class.std::__2::__tree_node_base"* %12, %"class.std::__2::__tree_node_base"* %13)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %14 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %__parent_ = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %14, i32 0, i32 2
  %15 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %__parent_, align 4
  %16 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__y, align 4
  %__parent_4 = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %16, i32 0, i32 2
  store %"class.std::__2::__tree_end_node"* %15, %"class.std::__2::__tree_end_node"** %__parent_4, align 4
  %17 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %call = call zeroext i1 @_ZNSt3__220__tree_is_left_childIPNS_16__tree_node_baseIPvEEEEbT_(%"class.std::__2::__tree_node_base"* %17) #9
  br i1 %call, label %if.then5, label %if.else

if.then5:                                         ; preds = %if.end
  %18 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__y, align 4
  %19 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %__parent_6 = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %19, i32 0, i32 2
  %20 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %__parent_6, align 4
  %__left_7 = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %20, i32 0, i32 0
  store %"class.std::__2::__tree_node_base"* %18, %"class.std::__2::__tree_node_base"** %__left_7, align 4
  br label %if.end10

if.else:                                          ; preds = %if.end
  %21 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__y, align 4
  %22 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %call8 = call %"class.std::__2::__tree_node_base"* @_ZNKSt3__216__tree_node_baseIPvE15__parent_unsafeEv(%"class.std::__2::__tree_node_base"* %22)
  %__right_9 = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %call8, i32 0, i32 1
  store %"class.std::__2::__tree_node_base"* %21, %"class.std::__2::__tree_node_base"** %__right_9, align 4
  br label %if.end10

if.end10:                                         ; preds = %if.else, %if.then5
  %23 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %24 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__y, align 4
  %__right_11 = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %24, i32 0, i32 1
  store %"class.std::__2::__tree_node_base"* %23, %"class.std::__2::__tree_node_base"** %__right_11, align 4
  %25 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %26 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__y, align 4
  call void @_ZNSt3__216__tree_node_baseIPvE12__set_parentEPS2_(%"class.std::__2::__tree_node_base"* %25, %"class.std::__2::__tree_node_base"* %26)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216__tree_node_baseIPvE12__set_parentEPS2_(%"class.std::__2::__tree_node_base"* %this, %"class.std::__2::__tree_node_base"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree_node_base"*, align 4
  %__p.addr = alloca %"class.std::__2::__tree_node_base"*, align 4
  store %"class.std::__2::__tree_node_base"* %this, %"class.std::__2::__tree_node_base"** %this.addr, align 4
  store %"class.std::__2::__tree_node_base"* %__p, %"class.std::__2::__tree_node_base"** %__p.addr, align 4
  %this1 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %this.addr, align 4
  %0 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__p.addr, align 4
  %1 = bitcast %"class.std::__2::__tree_node_base"* %0 to %"class.std::__2::__tree_end_node"*
  %__parent_ = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %this1, i32 0, i32 2
  store %"class.std::__2::__tree_end_node"* %1, %"class.std::__2::__tree_end_node"** %__parent_, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__217__compressed_pairImNS_19__map_value_compareIN5draco17GeometryAttribute4TypeENS_12__value_typeIS4_NS2_7OptionsEEENS_4lessIS4_EELb1EEEE5firstEv(%"class.std::__2::__compressed_pair.127"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.127"*, align 4
  store %"class.std::__2::__compressed_pair.127"* %this, %"class.std::__2::__compressed_pair.127"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.127"*, %"class.std::__2::__compressed_pair.127"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.127"* %this1 to %"struct.std::__2::__compressed_pair_elem.11"*
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__222__compressed_pair_elemImLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.11"* %0) #9
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_node"** @_ZNSt3__217__compressed_pairIPNS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPvEENS_22__tree_node_destructorINS_9allocatorIS9_EEEEE5firstEv(%"class.std::__2::__compressed_pair.173"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.173"*, align 4
  store %"class.std::__2::__compressed_pair.173"* %this, %"class.std::__2::__compressed_pair.173"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.173"*, %"class.std::__2::__compressed_pair.173"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.173"* %this1 to %"struct.std::__2::__compressed_pair_elem.174"*
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_node"** @_ZNSt3__222__compressed_pair_elemIPNS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPvEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.174"* %0) #9
  ret %"class.std::__2::__tree_node"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_node"** @_ZNSt3__222__compressed_pair_elemIPNS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPvEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.174"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.174"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.174"* %this, %"struct.std::__2::__compressed_pair_elem.174"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.174"*, %"struct.std::__2::__compressed_pair_elem.174"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.174", %"struct.std::__2::__compressed_pair_elem.174"* %this1, i32 0, i32 0
  ret %"class.std::__2::__tree_node"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__210unique_ptrINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPvEENS_22__tree_node_destructorINS_9allocatorIS9_EEEEE5resetEPS9_(%"class.std::__2::unique_ptr.172"* %this, %"class.std::__2::__tree_node"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.172"*, align 4
  %__p.addr = alloca %"class.std::__2::__tree_node"*, align 4
  %__tmp = alloca %"class.std::__2::__tree_node"*, align 4
  store %"class.std::__2::unique_ptr.172"* %this, %"class.std::__2::unique_ptr.172"** %this.addr, align 4
  store %"class.std::__2::__tree_node"* %__p, %"class.std::__2::__tree_node"** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.172"*, %"class.std::__2::unique_ptr.172"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.172", %"class.std::__2::unique_ptr.172"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_node"** @_ZNSt3__217__compressed_pairIPNS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPvEENS_22__tree_node_destructorINS_9allocatorIS9_EEEEE5firstEv(%"class.std::__2::__compressed_pair.173"* %__ptr_) #9
  %0 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %call, align 4
  store %"class.std::__2::__tree_node"* %0, %"class.std::__2::__tree_node"** %__tmp, align 4
  %1 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %__p.addr, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr.172", %"class.std::__2::unique_ptr.172"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_node"** @_ZNSt3__217__compressed_pairIPNS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPvEENS_22__tree_node_destructorINS_9allocatorIS9_EEEEE5firstEv(%"class.std::__2::__compressed_pair.173"* %__ptr_2) #9
  store %"class.std::__2::__tree_node"* %1, %"class.std::__2::__tree_node"** %call3, align 4
  %2 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %__tmp, align 4
  %tobool = icmp ne %"class.std::__2::__tree_node"* %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__ptr_4 = getelementptr inbounds %"class.std::__2::unique_ptr.172", %"class.std::__2::unique_ptr.172"* %this1, i32 0, i32 0
  %call5 = call nonnull align 4 dereferenceable(5) %"class.std::__2::__tree_node_destructor"* @_ZNSt3__217__compressed_pairIPNS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPvEENS_22__tree_node_destructorINS_9allocatorIS9_EEEEE6secondEv(%"class.std::__2::__compressed_pair.173"* %__ptr_4) #9
  %3 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %__tmp, align 4
  call void @_ZNSt3__222__tree_node_destructorINS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS4_7OptionsEEEPvEEEEEclEPSA_(%"class.std::__2::__tree_node_destructor"* %call5, %"class.std::__2::__tree_node"* %3) #9
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__222__tree_node_destructorINS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS4_7OptionsEEEPvEEEEEclEPSA_(%"class.std::__2::__tree_node_destructor"* %this, %"class.std::__2::__tree_node"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree_node_destructor"*, align 4
  %__p.addr = alloca %"class.std::__2::__tree_node"*, align 4
  store %"class.std::__2::__tree_node_destructor"* %this, %"class.std::__2::__tree_node_destructor"** %this.addr, align 4
  store %"class.std::__2::__tree_node"* %__p, %"class.std::__2::__tree_node"** %__p.addr, align 4
  %this1 = load %"class.std::__2::__tree_node_destructor"*, %"class.std::__2::__tree_node_destructor"** %this.addr, align 4
  %__value_constructed = getelementptr inbounds %"class.std::__2::__tree_node_destructor", %"class.std::__2::__tree_node_destructor"* %this1, i32 0, i32 1
  %0 = load i8, i8* %__value_constructed, align 4
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__na_ = getelementptr inbounds %"class.std::__2::__tree_node_destructor", %"class.std::__2::__tree_node_destructor"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::allocator.125"*, %"class.std::__2::allocator.125"** %__na_, align 4
  %2 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %__p.addr, align 4
  %__value_ = getelementptr inbounds %"class.std::__2::__tree_node", %"class.std::__2::__tree_node"* %2, i32 0, i32 1
  %call = call %"struct.std::__2::pair"* @_ZNSt3__222__tree_key_value_typesINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEEE9__get_ptrERS6_(%"struct.std::__2::__value_type"* nonnull align 4 dereferenceable(16) %__value_)
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS4_7OptionsEEEPvEEEEE7destroyINS_4pairIKS6_S7_EEEEvRSB_PT_(%"class.std::__2::allocator.125"* nonnull align 1 dereferenceable(1) %1, %"struct.std::__2::pair"* %call)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %3 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %__p.addr, align 4
  %tobool2 = icmp ne %"class.std::__2::__tree_node"* %3, null
  br i1 %tobool2, label %if.then3, label %if.end5

if.then3:                                         ; preds = %if.end
  %__na_4 = getelementptr inbounds %"class.std::__2::__tree_node_destructor", %"class.std::__2::__tree_node_destructor"* %this1, i32 0, i32 0
  %4 = load %"class.std::__2::allocator.125"*, %"class.std::__2::allocator.125"** %__na_4, align 4
  %5 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS4_7OptionsEEEPvEEEEE10deallocateERSB_PSA_m(%"class.std::__2::allocator.125"* nonnull align 1 dereferenceable(1) %4, %"class.std::__2::__tree_node"* %5, i32 1) #9
  br label %if.end5

if.end5:                                          ; preds = %if.then3, %if.end
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS4_7OptionsEEEPvEEEEE7destroyINS_4pairIKS6_S7_EEEEvRSB_PT_(%"class.std::__2::allocator.125"* nonnull align 1 dereferenceable(1) %__a, %"struct.std::__2::pair"* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.125"*, align 4
  %__p.addr = alloca %"struct.std::__2::pair"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant.176", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy", align 1
  store %"class.std::__2::allocator.125"* %__a, %"class.std::__2::allocator.125"** %__a.addr, align 4
  store %"struct.std::__2::pair"* %__p, %"struct.std::__2::pair"** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy"* %ref.tmp to %"struct.std::__2::integral_constant.176"*
  %1 = load %"class.std::__2::allocator.125"*, %"class.std::__2::allocator.125"** %__a.addr, align 4
  %2 = load %"struct.std::__2::pair"*, %"struct.std::__2::pair"** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS4_7OptionsEEEPvEEEEE9__destroyINS_4pairIKS6_S7_EEEEvNS_17integral_constantIbLb0EEERSB_PT_(%"class.std::__2::allocator.125"* nonnull align 1 dereferenceable(1) %1, %"struct.std::__2::pair"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS4_7OptionsEEEPvEEEEE10deallocateERSB_PSA_m(%"class.std::__2::allocator.125"* nonnull align 1 dereferenceable(1) %__a, %"class.std::__2::__tree_node"* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.125"*, align 4
  %__p.addr = alloca %"class.std::__2::__tree_node"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.125"* %__a, %"class.std::__2::allocator.125"** %__a.addr, align 4
  store %"class.std::__2::__tree_node"* %__p, %"class.std::__2::__tree_node"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.125"*, %"class.std::__2::allocator.125"** %__a.addr, align 4
  %1 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPvEEE10deallocateEPS9_m(%"class.std::__2::allocator.125"* %0, %"class.std::__2::__tree_node"* %1, i32 %2) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS4_7OptionsEEEPvEEEEE9__destroyINS_4pairIKS6_S7_EEEEvNS_17integral_constantIbLb0EEERSB_PT_(%"class.std::__2::allocator.125"* nonnull align 1 dereferenceable(1) %0, %"struct.std::__2::pair"* %__p) #0 comdat {
entry:
  %1 = alloca %"struct.std::__2::integral_constant.176", align 1
  %.addr = alloca %"class.std::__2::allocator.125"*, align 4
  %__p.addr = alloca %"struct.std::__2::pair"*, align 4
  store %"class.std::__2::allocator.125"* %0, %"class.std::__2::allocator.125"** %.addr, align 4
  store %"struct.std::__2::pair"* %__p, %"struct.std::__2::pair"** %__p.addr, align 4
  %2 = load %"struct.std::__2::pair"*, %"struct.std::__2::pair"** %__p.addr, align 4
  %call = call %"struct.std::__2::pair"* @_ZNSt3__24pairIKN5draco17GeometryAttribute4TypeENS1_7OptionsEED2Ev(%"struct.std::__2::pair"* %2) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::pair"* @_ZNSt3__24pairIKN5draco17GeometryAttribute4TypeENS1_7OptionsEED2Ev(%"struct.std::__2::pair"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::pair"*, align 4
  store %"struct.std::__2::pair"* %this, %"struct.std::__2::pair"** %this.addr, align 4
  %this1 = load %"struct.std::__2::pair"*, %"struct.std::__2::pair"** %this.addr, align 4
  %second = getelementptr inbounds %"struct.std::__2::pair", %"struct.std::__2::pair"* %this1, i32 0, i32 1
  %call = call %"class.draco::Options"* @_ZN5draco7OptionsD2Ev(%"class.draco::Options"* %second) #9
  ret %"struct.std::__2::pair"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPvEEE10deallocateEPS9_m(%"class.std::__2::allocator.125"* %this, %"class.std::__2::__tree_node"* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.125"*, align 4
  %__p.addr = alloca %"class.std::__2::__tree_node"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.125"* %this, %"class.std::__2::allocator.125"** %this.addr, align 4
  store %"class.std::__2::__tree_node"* %__p, %"class.std::__2::__tree_node"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.125"*, %"class.std::__2::allocator.125"** %this.addr, align 4
  %0 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %__p.addr, align 4
  %1 = bitcast %"class.std::__2::__tree_node"* %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 32
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %__ptr, i32 %__size, i32 %__align) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  %1 = load i32, i32* %__size.addr, align 4
  %2 = load i32, i32* %__align.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm(i8* %0, i32 %1, i32 %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm(i8* %__ptr, i32 %__size, i32 %__align) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  %1 = load i32, i32* %__size.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm(i8* %0, i32 %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm(i8* %__ptr, i32 %__size) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller9__do_callEPv(i8* %0)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller9__do_callEPv(i8* %__ptr) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  call void @_ZdlPv(i8* %0) #11
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #7

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_iterator"* @_ZNSt3__27forwardINS_15__tree_iteratorINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPNS_11__tree_nodeIS7_PvEElEEEEOT_RNS_16remove_referenceISD_E4typeE(%"class.std::__2::__tree_iterator"* nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::__tree_iterator"*, align 4
  store %"class.std::__2::__tree_iterator"* %__t, %"class.std::__2::__tree_iterator"** %__t.addr, align 4
  %0 = load %"class.std::__2::__tree_iterator"*, %"class.std::__2::__tree_iterator"** %__t.addr, align 4
  ret %"class.std::__2::__tree_iterator"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) i8* @_ZNSt3__27forwardIRbEEOT_RNS_16remove_referenceIS2_E4typeE(i8* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca i8*, align 4
  store i8* %__t, i8** %__t.addr, align 4
  %0 = load i8*, i8** %__t.addr, align 4
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) i8* @_ZNSt3__27forwardIbEEOT_RNS_16remove_referenceIS1_E4typeE(i8* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca i8*, align 4
  store i8* %__t, i8** %__t.addr, align 4
  %0 = load i8*, i8** %__t.addr, align 4
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__27forwardIRKN5draco17GeometryAttribute4TypeEEEOT_RNS_16remove_referenceIS6_E4typeE(i32* nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca i32*, align 4
  store i32* %__t, i32** %__t.addr, align 4
  %0 = load i32*, i32** %__t.addr, align 4
  ret i32* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"class.draco::Options"* @_ZNSt3__27forwardIRN5draco7OptionsEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::Options"* nonnull align 4 dereferenceable(12) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.draco::Options"*, align 4
  store %"class.draco::Options"* %__t, %"class.draco::Options"** %__t.addr, align 4
  %0 = load %"class.draco::Options"*, %"class.draco::Options"** %__t.addr, align 4
  ret %"class.draco::Options"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::pair.169"* @_ZNSt3__24pairIN5draco17GeometryAttribute4TypeENS1_7OptionsEEC2IRKS3_RS4_Lb0EEEOT_OT0_(%"struct.std::__2::pair.169"* returned %this, i32* nonnull align 4 dereferenceable(4) %__u1, %"class.draco::Options"* nonnull align 4 dereferenceable(12) %__u2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::pair.169"*, align 4
  %__u1.addr = alloca i32*, align 4
  %__u2.addr = alloca %"class.draco::Options"*, align 4
  store %"struct.std::__2::pair.169"* %this, %"struct.std::__2::pair.169"** %this.addr, align 4
  store i32* %__u1, i32** %__u1.addr, align 4
  store %"class.draco::Options"* %__u2, %"class.draco::Options"** %__u2.addr, align 4
  %this1 = load %"struct.std::__2::pair.169"*, %"struct.std::__2::pair.169"** %this.addr, align 4
  %first = getelementptr inbounds %"struct.std::__2::pair.169", %"struct.std::__2::pair.169"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__u1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__27forwardIRKN5draco17GeometryAttribute4TypeEEEOT_RNS_16remove_referenceIS6_E4typeE(i32* nonnull align 4 dereferenceable(4) %0) #9
  %1 = load i32, i32* %call, align 4
  store i32 %1, i32* %first, align 4
  %second = getelementptr inbounds %"struct.std::__2::pair.169", %"struct.std::__2::pair.169"* %this1, i32 0, i32 1
  %2 = load %"class.draco::Options"*, %"class.draco::Options"** %__u2.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(12) %"class.draco::Options"* @_ZNSt3__27forwardIRN5draco7OptionsEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::Options"* nonnull align 4 dereferenceable(12) %2) #9
  %call3 = call %"class.draco::Options"* @_ZN5draco7OptionsC2ERKS0_(%"class.draco::Options"* %second, %"class.draco::Options"* nonnull align 4 dereferenceable(12) %call2)
  ret %"struct.std::__2::pair.169"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::Options"* @_ZN5draco7OptionsC2ERKS0_(%"class.draco::Options"* returned %this, %"class.draco::Options"* nonnull align 4 dereferenceable(12) %0) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::Options"*, align 4
  %.addr = alloca %"class.draco::Options"*, align 4
  store %"class.draco::Options"* %this, %"class.draco::Options"** %this.addr, align 4
  store %"class.draco::Options"* %0, %"class.draco::Options"** %.addr, align 4
  %this1 = load %"class.draco::Options"*, %"class.draco::Options"** %this.addr, align 4
  %options_ = getelementptr inbounds %"class.draco::Options", %"class.draco::Options"* %this1, i32 0, i32 0
  %1 = load %"class.draco::Options"*, %"class.draco::Options"** %.addr, align 4
  %options_2 = getelementptr inbounds %"class.draco::Options", %"class.draco::Options"* %1, i32 0, i32 0
  %call = call %"class.std::__2::map"* @_ZNSt3__23mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_NS_4lessIS6_EENS4_INS_4pairIKS6_S6_EEEEEC2ERKSD_(%"class.std::__2::map"* %options_, %"class.std::__2::map"* nonnull align 4 dereferenceable(12) %options_2)
  ret %"class.draco::Options"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::map"* @_ZNSt3__23mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_NS_4lessIS6_EENS4_INS_4pairIKS6_S6_EEEEEC2ERKSD_(%"class.std::__2::map"* returned %this, %"class.std::__2::map"* nonnull align 4 dereferenceable(12) %__m) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::map"*, align 4
  %__m.addr = alloca %"class.std::__2::map"*, align 4
  %agg.tmp = alloca %"class.std::__2::__map_const_iterator", align 4
  %agg.tmp5 = alloca %"class.std::__2::__map_const_iterator", align 4
  store %"class.std::__2::map"* %this, %"class.std::__2::map"** %this.addr, align 4
  store %"class.std::__2::map"* %__m, %"class.std::__2::map"** %__m.addr, align 4
  %this1 = load %"class.std::__2::map"*, %"class.std::__2::map"** %this.addr, align 4
  %__tree_ = getelementptr inbounds %"class.std::__2::map", %"class.std::__2::map"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::map"*, %"class.std::__2::map"** %__m.addr, align 4
  %__tree_2 = getelementptr inbounds %"class.std::__2::map", %"class.std::__2::map"* %0, i32 0, i32 0
  %call = call %"class.std::__2::__tree"* @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEEC2ERKSE_(%"class.std::__2::__tree"* %__tree_, %"class.std::__2::__tree"* nonnull align 4 dereferenceable(12) %__tree_2)
  %1 = load %"class.std::__2::map"*, %"class.std::__2::map"** %__m.addr, align 4
  %call3 = call %"class.std::__2::__tree_end_node"* @_ZNKSt3__23mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_NS_4lessIS6_EENS4_INS_4pairIKS6_S6_EEEEE5beginEv(%"class.std::__2::map"* %1) #9
  %coerce.dive = getelementptr inbounds %"class.std::__2::__map_const_iterator", %"class.std::__2::__map_const_iterator"* %agg.tmp, i32 0, i32 0
  %coerce.dive4 = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %coerce.dive, i32 0, i32 0
  store %"class.std::__2::__tree_end_node"* %call3, %"class.std::__2::__tree_end_node"** %coerce.dive4, align 4
  %2 = load %"class.std::__2::map"*, %"class.std::__2::map"** %__m.addr, align 4
  %call6 = call %"class.std::__2::__tree_end_node"* @_ZNKSt3__23mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_NS_4lessIS6_EENS4_INS_4pairIKS6_S6_EEEEE3endEv(%"class.std::__2::map"* %2) #9
  %coerce.dive7 = getelementptr inbounds %"class.std::__2::__map_const_iterator", %"class.std::__2::__map_const_iterator"* %agg.tmp5, i32 0, i32 0
  %coerce.dive8 = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %coerce.dive7, i32 0, i32 0
  store %"class.std::__2::__tree_end_node"* %call6, %"class.std::__2::__tree_end_node"** %coerce.dive8, align 4
  %coerce.dive9 = getelementptr inbounds %"class.std::__2::__map_const_iterator", %"class.std::__2::__map_const_iterator"* %agg.tmp, i32 0, i32 0
  %coerce.dive10 = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %coerce.dive9, i32 0, i32 0
  %3 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %coerce.dive10, align 4
  %coerce.dive11 = getelementptr inbounds %"class.std::__2::__map_const_iterator", %"class.std::__2::__map_const_iterator"* %agg.tmp5, i32 0, i32 0
  %coerce.dive12 = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %coerce.dive11, i32 0, i32 0
  %4 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %coerce.dive12, align 4
  call void @_ZNSt3__23mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_NS_4lessIS6_EENS4_INS_4pairIKS6_S6_EEEEE6insertINS_20__map_const_iteratorINS_21__tree_const_iteratorINS_12__value_typeIS6_S6_EEPNS_11__tree_nodeISI_PvEElEEEEEEvT_SP_(%"class.std::__2::map"* %this1, %"class.std::__2::__tree_end_node"* %3, %"class.std::__2::__tree_end_node"* %4)
  ret %"class.std::__2::map"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree"* @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEEC2ERKSE_(%"class.std::__2::__tree"* returned %this, %"class.std::__2::__tree"* nonnull align 4 dereferenceable(12) %__t) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree"*, align 4
  %__t.addr = alloca %"class.std::__2::__tree"*, align 4
  %ref.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  %ref.tmp2 = alloca %"class.std::__2::allocator.117", align 1
  %undef.agg.tmp = alloca %"class.std::__2::allocator.117", align 1
  %ref.tmp4 = alloca i32, align 4
  store %"class.std::__2::__tree"* %this, %"class.std::__2::__tree"** %this.addr, align 4
  store %"class.std::__2::__tree"* %__t, %"class.std::__2::__tree"** %__t.addr, align 4
  %this1 = load %"class.std::__2::__tree"*, %"class.std::__2::__tree"** %this.addr, align 4
  %__begin_node_ = getelementptr inbounds %"class.std::__2::__tree", %"class.std::__2::__tree"* %this1, i32 0, i32 0
  store %"class.std::__2::__tree_end_node"* null, %"class.std::__2::__tree_end_node"** %__begin_node_, align 4
  %__pair1_ = getelementptr inbounds %"class.std::__2::__tree", %"class.std::__2::__tree"* %this1, i32 0, i32 1
  %0 = load %"class.std::__2::__tree"*, %"class.std::__2::__tree"** %__t.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.117"* @_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE12__node_allocEv(%"class.std::__2::__tree"* %0) #9
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEEE37select_on_container_copy_constructionERKSC_(%"class.std::__2::allocator.117"* nonnull align 1 dereferenceable(1) %call)
  %call3 = call %"class.std::__2::__compressed_pair.114"* @_ZNSt3__217__compressed_pairINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEENS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS7_IcEEEESE_EES3_EEEEEC2INS_18__default_init_tagESH_EEOT_OT0_(%"class.std::__2::__compressed_pair.114"* %__pair1_, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp, %"class.std::__2::allocator.117"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  %__pair3_ = getelementptr inbounds %"class.std::__2::__tree", %"class.std::__2::__tree"* %this1, i32 0, i32 2
  store i32 0, i32* %ref.tmp4, align 4
  %1 = load %"class.std::__2::__tree"*, %"class.std::__2::__tree"** %__t.addr, align 4
  %call5 = call nonnull align 1 dereferenceable(1) %"class.std::__2::__map_value_compare"* @_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE10value_compEv(%"class.std::__2::__tree"* %1) #9
  %call6 = call %"class.std::__2::__compressed_pair.119"* @_ZNSt3__217__compressed_pairImNS_19__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS7_S7_EENS_4lessIS7_EELb1EEEEC2IiRKSC_EEOT_OT0_(%"class.std::__2::__compressed_pair.119"* %__pair3_, i32* nonnull align 4 dereferenceable(4) %ref.tmp4, %"class.std::__2::__map_value_compare"* nonnull align 1 dereferenceable(1) %call5)
  %call7 = call %"class.std::__2::__tree_end_node"* @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE10__end_nodeEv(%"class.std::__2::__tree"* %this1) #9
  %call8 = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_end_node"** @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE12__begin_nodeEv(%"class.std::__2::__tree"* %this1) #9
  store %"class.std::__2::__tree_end_node"* %call7, %"class.std::__2::__tree_end_node"** %call8, align 4
  ret %"class.std::__2::__tree"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__23mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_NS_4lessIS6_EENS4_INS_4pairIKS6_S6_EEEEE6insertINS_20__map_const_iteratorINS_21__tree_const_iteratorINS_12__value_typeIS6_S6_EEPNS_11__tree_nodeISI_PvEElEEEEEEvT_SP_(%"class.std::__2::map"* %this, %"class.std::__2::__tree_end_node"* %__f.coerce, %"class.std::__2::__tree_end_node"* %__l.coerce) #0 comdat {
entry:
  %__f = alloca %"class.std::__2::__map_const_iterator", align 4
  %__l = alloca %"class.std::__2::__map_const_iterator", align 4
  %this.addr = alloca %"class.std::__2::map"*, align 4
  %__e = alloca %"class.std::__2::__map_const_iterator", align 4
  %agg.tmp = alloca %"class.std::__2::__map_const_iterator", align 4
  %agg.tmp8 = alloca %"class.std::__2::__tree_const_iterator", align 4
  %coerce = alloca %"class.std::__2::__map_iterator.178", align 4
  %coerce.dive = getelementptr inbounds %"class.std::__2::__map_const_iterator", %"class.std::__2::__map_const_iterator"* %__f, i32 0, i32 0
  %coerce.dive1 = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %coerce.dive, i32 0, i32 0
  store %"class.std::__2::__tree_end_node"* %__f.coerce, %"class.std::__2::__tree_end_node"** %coerce.dive1, align 4
  %coerce.dive2 = getelementptr inbounds %"class.std::__2::__map_const_iterator", %"class.std::__2::__map_const_iterator"* %__l, i32 0, i32 0
  %coerce.dive3 = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %coerce.dive2, i32 0, i32 0
  store %"class.std::__2::__tree_end_node"* %__l.coerce, %"class.std::__2::__tree_end_node"** %coerce.dive3, align 4
  store %"class.std::__2::map"* %this, %"class.std::__2::map"** %this.addr, align 4
  %this4 = load %"class.std::__2::map"*, %"class.std::__2::map"** %this.addr, align 4
  %call = call %"class.std::__2::__tree_end_node"* @_ZNKSt3__23mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_NS_4lessIS6_EENS4_INS_4pairIKS6_S6_EEEEE4cendEv(%"class.std::__2::map"* %this4) #9
  %coerce.dive5 = getelementptr inbounds %"class.std::__2::__map_const_iterator", %"class.std::__2::__map_const_iterator"* %__e, i32 0, i32 0
  %coerce.dive6 = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %coerce.dive5, i32 0, i32 0
  store %"class.std::__2::__tree_end_node"* %call, %"class.std::__2::__tree_end_node"** %coerce.dive6, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %call7 = call zeroext i1 @_ZNSt3__2neERKNS_20__map_const_iteratorINS_21__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPNS_11__tree_nodeIS9_PvEElEEEESH_(%"class.std::__2::__map_const_iterator"* nonnull align 4 dereferenceable(4) %__f, %"class.std::__2::__map_const_iterator"* nonnull align 4 dereferenceable(4) %__l)
  br i1 %call7, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %__i_ = getelementptr inbounds %"class.std::__2::__map_const_iterator", %"class.std::__2::__map_const_iterator"* %__e, i32 0, i32 0
  %0 = bitcast %"class.std::__2::__tree_const_iterator"* %agg.tmp8 to i8*
  %1 = bitcast %"class.std::__2::__tree_const_iterator"* %__i_ to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %0, i8* align 4 %1, i32 4, i1 false)
  %coerce.dive9 = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %agg.tmp8, i32 0, i32 0
  %2 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %coerce.dive9, align 4
  %call10 = call %"class.std::__2::__map_const_iterator"* @_ZNSt3__220__map_const_iteratorINS_21__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPNS_11__tree_nodeIS9_PvEElEEEC2ESE_(%"class.std::__2::__map_const_iterator"* %agg.tmp, %"class.std::__2::__tree_end_node"* %2) #9
  %call11 = call nonnull align 4 dereferenceable(24) %"struct.std::__2::pair.177"* @_ZNKSt3__220__map_const_iteratorINS_21__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPNS_11__tree_nodeIS9_PvEElEEEdeEv(%"class.std::__2::__map_const_iterator"* %__f)
  %coerce.dive12 = getelementptr inbounds %"class.std::__2::__map_const_iterator", %"class.std::__2::__map_const_iterator"* %agg.tmp, i32 0, i32 0
  %coerce.dive13 = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %coerce.dive12, i32 0, i32 0
  %3 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %coerce.dive13, align 4
  %call14 = call %"class.std::__2::__tree_end_node"* @_ZNSt3__23mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_NS_4lessIS6_EENS4_INS_4pairIKS6_S6_EEEEE6insertENS_20__map_const_iteratorINS_21__tree_const_iteratorINS_12__value_typeIS6_S6_EEPNS_11__tree_nodeISH_PvEElEEEERKSB_(%"class.std::__2::map"* %this4, %"class.std::__2::__tree_end_node"* %3, %"struct.std::__2::pair.177"* nonnull align 4 dereferenceable(24) %call11)
  %coerce.dive15 = getelementptr inbounds %"class.std::__2::__map_iterator.178", %"class.std::__2::__map_iterator.178"* %coerce, i32 0, i32 0
  %coerce.dive16 = getelementptr inbounds %"class.std::__2::__tree_iterator.179", %"class.std::__2::__tree_iterator.179"* %coerce.dive15, i32 0, i32 0
  store %"class.std::__2::__tree_end_node"* %call14, %"class.std::__2::__tree_end_node"** %coerce.dive16, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %call17 = call nonnull align 4 dereferenceable(4) %"class.std::__2::__map_const_iterator"* @_ZNSt3__220__map_const_iteratorINS_21__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPNS_11__tree_nodeIS9_PvEElEEEppEv(%"class.std::__2::__map_const_iterator"* %__f)
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_end_node"* @_ZNKSt3__23mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_NS_4lessIS6_EENS4_INS_4pairIKS6_S6_EEEEE5beginEv(%"class.std::__2::map"* %this) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__map_const_iterator", align 4
  %this.addr = alloca %"class.std::__2::map"*, align 4
  %agg.tmp = alloca %"class.std::__2::__tree_const_iterator", align 4
  store %"class.std::__2::map"* %this, %"class.std::__2::map"** %this.addr, align 4
  %this1 = load %"class.std::__2::map"*, %"class.std::__2::map"** %this.addr, align 4
  %__tree_ = getelementptr inbounds %"class.std::__2::map", %"class.std::__2::map"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::__tree_end_node"* @_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE5beginEv(%"class.std::__2::__tree"* %__tree_) #9
  %coerce.dive = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %agg.tmp, i32 0, i32 0
  store %"class.std::__2::__tree_end_node"* %call, %"class.std::__2::__tree_end_node"** %coerce.dive, align 4
  %coerce.dive2 = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %agg.tmp, i32 0, i32 0
  %0 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %coerce.dive2, align 4
  %call3 = call %"class.std::__2::__map_const_iterator"* @_ZNSt3__220__map_const_iteratorINS_21__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPNS_11__tree_nodeIS9_PvEElEEEC2ESE_(%"class.std::__2::__map_const_iterator"* %retval, %"class.std::__2::__tree_end_node"* %0) #9
  %coerce.dive4 = getelementptr inbounds %"class.std::__2::__map_const_iterator", %"class.std::__2::__map_const_iterator"* %retval, i32 0, i32 0
  %coerce.dive5 = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %coerce.dive4, i32 0, i32 0
  %1 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %coerce.dive5, align 4
  ret %"class.std::__2::__tree_end_node"* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_end_node"* @_ZNKSt3__23mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_NS_4lessIS6_EENS4_INS_4pairIKS6_S6_EEEEE3endEv(%"class.std::__2::map"* %this) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__map_const_iterator", align 4
  %this.addr = alloca %"class.std::__2::map"*, align 4
  %agg.tmp = alloca %"class.std::__2::__tree_const_iterator", align 4
  store %"class.std::__2::map"* %this, %"class.std::__2::map"** %this.addr, align 4
  %this1 = load %"class.std::__2::map"*, %"class.std::__2::map"** %this.addr, align 4
  %__tree_ = getelementptr inbounds %"class.std::__2::map", %"class.std::__2::map"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::__tree_end_node"* @_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE3endEv(%"class.std::__2::__tree"* %__tree_) #9
  %coerce.dive = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %agg.tmp, i32 0, i32 0
  store %"class.std::__2::__tree_end_node"* %call, %"class.std::__2::__tree_end_node"** %coerce.dive, align 4
  %coerce.dive2 = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %agg.tmp, i32 0, i32 0
  %0 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %coerce.dive2, align 4
  %call3 = call %"class.std::__2::__map_const_iterator"* @_ZNSt3__220__map_const_iteratorINS_21__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPNS_11__tree_nodeIS9_PvEElEEEC2ESE_(%"class.std::__2::__map_const_iterator"* %retval, %"class.std::__2::__tree_end_node"* %0) #9
  %coerce.dive4 = getelementptr inbounds %"class.std::__2::__map_const_iterator", %"class.std::__2::__map_const_iterator"* %retval, i32 0, i32 0
  %coerce.dive5 = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %coerce.dive4, i32 0, i32 0
  %1 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %coerce.dive5, align 4
  ret %"class.std::__2::__tree_end_node"* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEEE37select_on_container_copy_constructionERKSC_(%"class.std::__2::allocator.117"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.117"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant.176", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_select_on_container_copy_construction", align 1
  %undef.agg.tmp = alloca %"class.std::__2::allocator.117", align 1
  store %"class.std::__2::allocator.117"* %__a, %"class.std::__2::allocator.117"** %__a.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_select_on_container_copy_construction"* %ref.tmp to %"struct.std::__2::integral_constant.176"*
  %1 = load %"class.std::__2::allocator.117"*, %"class.std::__2::allocator.117"** %__a.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEEE39__select_on_container_copy_constructionENS_17integral_constantIbLb0EEERKSC_(%"class.std::__2::allocator.117"* nonnull align 1 dereferenceable(1) %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.117"* @_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE12__node_allocEv(%"class.std::__2::__tree"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree"*, align 4
  store %"class.std::__2::__tree"* %this, %"class.std::__2::__tree"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree"*, %"class.std::__2::__tree"** %this.addr, align 4
  %__pair1_ = getelementptr inbounds %"class.std::__2::__tree", %"class.std::__2::__tree"* %this1, i32 0, i32 1
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.117"* @_ZNKSt3__217__compressed_pairINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEENS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS7_IcEEEESE_EES3_EEEEE6secondEv(%"class.std::__2::__compressed_pair.114"* %__pair1_) #9
  ret %"class.std::__2::allocator.117"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.114"* @_ZNSt3__217__compressed_pairINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEENS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS7_IcEEEESE_EES3_EEEEEC2INS_18__default_init_tagESH_EEOT_OT0_(%"class.std::__2::__compressed_pair.114"* returned %this, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t1, %"class.std::__2::allocator.117"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.114"*, align 4
  %__t1.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %__t2.addr = alloca %"class.std::__2::allocator.117"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.114"* %this, %"class.std::__2::__compressed_pair.114"** %this.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t1, %"struct.std::__2::__default_init_tag"** %__t1.addr, align 4
  store %"class.std::__2::allocator.117"* %__t2, %"class.std::__2::allocator.117"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.114"*, %"class.std::__2::__compressed_pair.114"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.114"* %this1 to %"struct.std::__2::__compressed_pair_elem.115"*
  %1 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t1.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %1) #9
  %call2 = call %"struct.std::__2::__compressed_pair_elem.115"* @_ZNSt3__222__compressed_pair_elemINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEELi0ELb0EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.115"* %0)
  %2 = bitcast %"class.std::__2::__compressed_pair.114"* %this1 to %"struct.std::__2::__compressed_pair_elem.116"*
  %3 = load %"class.std::__2::allocator.117"*, %"class.std::__2::allocator.117"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.117"* @_ZNSt3__27forwardINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEEEEOT_RNS_16remove_referenceISD_E4typeE(%"class.std::__2::allocator.117"* nonnull align 1 dereferenceable(1) %3) #9
  %call4 = call %"struct.std::__2::__compressed_pair_elem.116"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEELi1ELb1EEC2ISC_vEEOT_(%"struct.std::__2::__compressed_pair_elem.116"* %2, %"class.std::__2::allocator.117"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::__compressed_pair.114"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::__map_value_compare"* @_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE10value_compEv(%"class.std::__2::__tree"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree"*, align 4
  store %"class.std::__2::__tree"* %this, %"class.std::__2::__tree"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree"*, %"class.std::__2::__tree"** %this.addr, align 4
  %__pair3_ = getelementptr inbounds %"class.std::__2::__tree", %"class.std::__2::__tree"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::__map_value_compare"* @_ZNKSt3__217__compressed_pairImNS_19__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS7_S7_EENS_4lessIS7_EELb1EEEE6secondEv(%"class.std::__2::__compressed_pair.119"* %__pair3_) #9
  ret %"class.std::__2::__map_value_compare"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.119"* @_ZNSt3__217__compressed_pairImNS_19__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS7_S7_EENS_4lessIS7_EELb1EEEEC2IiRKSC_EEOT_OT0_(%"class.std::__2::__compressed_pair.119"* returned %this, i32* nonnull align 4 dereferenceable(4) %__t1, %"class.std::__2::__map_value_compare"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.119"*, align 4
  %__t1.addr = alloca i32*, align 4
  %__t2.addr = alloca %"class.std::__2::__map_value_compare"*, align 4
  store %"class.std::__2::__compressed_pair.119"* %this, %"class.std::__2::__compressed_pair.119"** %this.addr, align 4
  store i32* %__t1, i32** %__t1.addr, align 4
  store %"class.std::__2::__map_value_compare"* %__t2, %"class.std::__2::__map_value_compare"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.119"*, %"class.std::__2::__compressed_pair.119"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.119"* %this1 to %"struct.std::__2::__compressed_pair_elem.11"*
  %1 = load i32*, i32** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__27forwardIiEEOT_RNS_16remove_referenceIS1_E4typeE(i32* nonnull align 4 dereferenceable(4) %1) #9
  %call2 = call %"struct.std::__2::__compressed_pair_elem.11"* @_ZNSt3__222__compressed_pair_elemImLi0ELb0EEC2IivEEOT_(%"struct.std::__2::__compressed_pair_elem.11"* %0, i32* nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.119"* %this1 to %"struct.std::__2::__compressed_pair_elem.120"*
  %3 = load %"class.std::__2::__map_value_compare"*, %"class.std::__2::__map_value_compare"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"class.std::__2::__map_value_compare"* @_ZNSt3__27forwardIRKNS_19__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS7_S7_EENS_4lessIS7_EELb1EEEEEOT_RNS_16remove_referenceISF_E4typeE(%"class.std::__2::__map_value_compare"* nonnull align 1 dereferenceable(1) %3) #9
  %call4 = call %"struct.std::__2::__compressed_pair_elem.120"* @_ZNSt3__222__compressed_pair_elemINS_19__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS7_S7_EENS_4lessIS7_EELb1EEELi1ELb1EEC2IRKSC_vEEOT_(%"struct.std::__2::__compressed_pair_elem.120"* %2, %"class.std::__2::__map_value_compare"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::__compressed_pair.119"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEEE39__select_on_container_copy_constructionENS_17integral_constantIbLb0EEERKSC_(%"class.std::__2::allocator.117"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant.176", align 1
  %__a.addr = alloca %"class.std::__2::allocator.117"*, align 4
  store %"class.std::__2::allocator.117"* %__a, %"class.std::__2::allocator.117"** %__a.addr, align 4
  %1 = load %"class.std::__2::allocator.117"*, %"class.std::__2::allocator.117"** %__a.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.117"* @_ZNKSt3__217__compressed_pairINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEENS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS7_IcEEEESE_EES3_EEEEE6secondEv(%"class.std::__2::__compressed_pair.114"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.114"*, align 4
  store %"class.std::__2::__compressed_pair.114"* %this, %"class.std::__2::__compressed_pair.114"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.114"*, %"class.std::__2::__compressed_pair.114"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.114"* %this1 to %"struct.std::__2::__compressed_pair_elem.116"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.117"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.116"* %0) #9
  ret %"class.std::__2::allocator.117"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.117"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.116"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.116"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.116"* %this, %"struct.std::__2::__compressed_pair_elem.116"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.116"*, %"struct.std::__2::__compressed_pair_elem.116"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.116"* %this1 to %"class.std::__2::allocator.117"*
  ret %"class.std::__2::allocator.117"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.115"* @_ZNSt3__222__compressed_pair_elemINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEELi0ELb0EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.115"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.115"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.115"* %this, %"struct.std::__2::__compressed_pair_elem.115"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.115"*, %"struct.std::__2::__compressed_pair_elem.115"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.115", %"struct.std::__2::__compressed_pair_elem.115"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::__tree_end_node"* @_ZNSt3__215__tree_end_nodeIPNS_16__tree_node_baseIPvEEEC2Ev(%"class.std::__2::__tree_end_node"* %__value_) #9
  ret %"struct.std::__2::__compressed_pair_elem.115"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.117"* @_ZNSt3__27forwardINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEEEEOT_RNS_16remove_referenceISD_E4typeE(%"class.std::__2::allocator.117"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::allocator.117"*, align 4
  store %"class.std::__2::allocator.117"* %__t, %"class.std::__2::allocator.117"** %__t.addr, align 4
  %0 = load %"class.std::__2::allocator.117"*, %"class.std::__2::allocator.117"** %__t.addr, align 4
  ret %"class.std::__2::allocator.117"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.116"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEELi1ELb1EEC2ISC_vEEOT_(%"struct.std::__2::__compressed_pair_elem.116"* returned %this, %"class.std::__2::allocator.117"* nonnull align 1 dereferenceable(1) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.116"*, align 4
  %__u.addr = alloca %"class.std::__2::allocator.117"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.116"* %this, %"struct.std::__2::__compressed_pair_elem.116"** %this.addr, align 4
  store %"class.std::__2::allocator.117"* %__u, %"class.std::__2::allocator.117"** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.116"*, %"struct.std::__2::__compressed_pair_elem.116"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.116"* %this1 to %"class.std::__2::allocator.117"*
  %1 = load %"class.std::__2::allocator.117"*, %"class.std::__2::allocator.117"** %__u.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.117"* @_ZNSt3__27forwardINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEEEEOT_RNS_16remove_referenceISD_E4typeE(%"class.std::__2::allocator.117"* nonnull align 1 dereferenceable(1) %1) #9
  ret %"struct.std::__2::__compressed_pair_elem.116"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_end_node"* @_ZNSt3__215__tree_end_nodeIPNS_16__tree_node_baseIPvEEEC2Ev(%"class.std::__2::__tree_end_node"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree_end_node"*, align 4
  store %"class.std::__2::__tree_end_node"* %this, %"class.std::__2::__tree_end_node"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %this.addr, align 4
  %__left_ = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %this1, i32 0, i32 0
  store %"class.std::__2::__tree_node_base"* null, %"class.std::__2::__tree_node_base"** %__left_, align 4
  ret %"class.std::__2::__tree_end_node"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::__map_value_compare"* @_ZNKSt3__217__compressed_pairImNS_19__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS7_S7_EENS_4lessIS7_EELb1EEEE6secondEv(%"class.std::__2::__compressed_pair.119"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.119"*, align 4
  store %"class.std::__2::__compressed_pair.119"* %this, %"class.std::__2::__compressed_pair.119"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.119"*, %"class.std::__2::__compressed_pair.119"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.119"* %this1 to %"struct.std::__2::__compressed_pair_elem.120"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::__map_value_compare"* @_ZNKSt3__222__compressed_pair_elemINS_19__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS7_S7_EENS_4lessIS7_EELb1EEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.120"* %0) #9
  ret %"class.std::__2::__map_value_compare"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::__map_value_compare"* @_ZNKSt3__222__compressed_pair_elemINS_19__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS7_S7_EENS_4lessIS7_EELb1EEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.120"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.120"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.120"* %this, %"struct.std::__2::__compressed_pair_elem.120"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.120"*, %"struct.std::__2::__compressed_pair_elem.120"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.120"* %this1 to %"class.std::__2::__map_value_compare"*
  ret %"class.std::__2::__map_value_compare"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__27forwardIiEEOT_RNS_16remove_referenceIS1_E4typeE(i32* nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca i32*, align 4
  store i32* %__t, i32** %__t.addr, align 4
  %0 = load i32*, i32** %__t.addr, align 4
  ret i32* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.11"* @_ZNSt3__222__compressed_pair_elemImLi0ELb0EEC2IivEEOT_(%"struct.std::__2::__compressed_pair_elem.11"* returned %this, i32* nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.11"*, align 4
  %__u.addr = alloca i32*, align 4
  store %"struct.std::__2::__compressed_pair_elem.11"* %this, %"struct.std::__2::__compressed_pair_elem.11"** %this.addr, align 4
  store i32* %__u, i32** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.11"*, %"struct.std::__2::__compressed_pair_elem.11"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.11", %"struct.std::__2::__compressed_pair_elem.11"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__27forwardIiEEOT_RNS_16remove_referenceIS1_E4typeE(i32* nonnull align 4 dereferenceable(4) %0) #9
  %1 = load i32, i32* %call, align 4
  store i32 %1, i32* %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.11"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::__map_value_compare"* @_ZNSt3__27forwardIRKNS_19__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS7_S7_EENS_4lessIS7_EELb1EEEEEOT_RNS_16remove_referenceISF_E4typeE(%"class.std::__2::__map_value_compare"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::__map_value_compare"*, align 4
  store %"class.std::__2::__map_value_compare"* %__t, %"class.std::__2::__map_value_compare"** %__t.addr, align 4
  %0 = load %"class.std::__2::__map_value_compare"*, %"class.std::__2::__map_value_compare"** %__t.addr, align 4
  ret %"class.std::__2::__map_value_compare"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.120"* @_ZNSt3__222__compressed_pair_elemINS_19__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS7_S7_EENS_4lessIS7_EELb1EEELi1ELb1EEC2IRKSC_vEEOT_(%"struct.std::__2::__compressed_pair_elem.120"* returned %this, %"class.std::__2::__map_value_compare"* nonnull align 1 dereferenceable(1) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.120"*, align 4
  %__u.addr = alloca %"class.std::__2::__map_value_compare"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.120"* %this, %"struct.std::__2::__compressed_pair_elem.120"** %this.addr, align 4
  store %"class.std::__2::__map_value_compare"* %__u, %"class.std::__2::__map_value_compare"** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.120"*, %"struct.std::__2::__compressed_pair_elem.120"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.120"* %this1 to %"class.std::__2::__map_value_compare"*
  %1 = load %"class.std::__2::__map_value_compare"*, %"class.std::__2::__map_value_compare"** %__u.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::__map_value_compare"* @_ZNSt3__27forwardIRKNS_19__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS7_S7_EENS_4lessIS7_EELb1EEEEEOT_RNS_16remove_referenceISF_E4typeE(%"class.std::__2::__map_value_compare"* nonnull align 1 dereferenceable(1) %1) #9
  ret %"struct.std::__2::__compressed_pair_elem.120"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_end_node"* @_ZNKSt3__23mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_NS_4lessIS6_EENS4_INS_4pairIKS6_S6_EEEEE4cendEv(%"class.std::__2::map"* %this) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__map_const_iterator", align 4
  %this.addr = alloca %"class.std::__2::map"*, align 4
  store %"class.std::__2::map"* %this, %"class.std::__2::map"** %this.addr, align 4
  %this1 = load %"class.std::__2::map"*, %"class.std::__2::map"** %this.addr, align 4
  %call = call %"class.std::__2::__tree_end_node"* @_ZNKSt3__23mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_NS_4lessIS6_EENS4_INS_4pairIKS6_S6_EEEEE3endEv(%"class.std::__2::map"* %this1) #9
  %coerce.dive = getelementptr inbounds %"class.std::__2::__map_const_iterator", %"class.std::__2::__map_const_iterator"* %retval, i32 0, i32 0
  %coerce.dive2 = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %coerce.dive, i32 0, i32 0
  store %"class.std::__2::__tree_end_node"* %call, %"class.std::__2::__tree_end_node"** %coerce.dive2, align 4
  %coerce.dive3 = getelementptr inbounds %"class.std::__2::__map_const_iterator", %"class.std::__2::__map_const_iterator"* %retval, i32 0, i32 0
  %coerce.dive4 = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %coerce.dive3, i32 0, i32 0
  %0 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %coerce.dive4, align 4
  ret %"class.std::__2::__tree_end_node"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNSt3__2neERKNS_20__map_const_iteratorINS_21__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPNS_11__tree_nodeIS9_PvEElEEEESH_(%"class.std::__2::__map_const_iterator"* nonnull align 4 dereferenceable(4) %__x, %"class.std::__2::__map_const_iterator"* nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %__x.addr = alloca %"class.std::__2::__map_const_iterator"*, align 4
  %__y.addr = alloca %"class.std::__2::__map_const_iterator"*, align 4
  store %"class.std::__2::__map_const_iterator"* %__x, %"class.std::__2::__map_const_iterator"** %__x.addr, align 4
  store %"class.std::__2::__map_const_iterator"* %__y, %"class.std::__2::__map_const_iterator"** %__y.addr, align 4
  %0 = load %"class.std::__2::__map_const_iterator"*, %"class.std::__2::__map_const_iterator"** %__x.addr, align 4
  %__i_ = getelementptr inbounds %"class.std::__2::__map_const_iterator", %"class.std::__2::__map_const_iterator"* %0, i32 0, i32 0
  %1 = load %"class.std::__2::__map_const_iterator"*, %"class.std::__2::__map_const_iterator"** %__y.addr, align 4
  %__i_1 = getelementptr inbounds %"class.std::__2::__map_const_iterator", %"class.std::__2::__map_const_iterator"* %1, i32 0, i32 0
  %call = call zeroext i1 @_ZNSt3__2neERKNS_21__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElEESF_(%"class.std::__2::__tree_const_iterator"* nonnull align 4 dereferenceable(4) %__i_, %"class.std::__2::__tree_const_iterator"* nonnull align 4 dereferenceable(4) %__i_1)
  ret i1 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_end_node"* @_ZNSt3__23mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_NS_4lessIS6_EENS4_INS_4pairIKS6_S6_EEEEE6insertENS_20__map_const_iteratorINS_21__tree_const_iteratorINS_12__value_typeIS6_S6_EEPNS_11__tree_nodeISH_PvEElEEEERKSB_(%"class.std::__2::map"* %this, %"class.std::__2::__tree_end_node"* %__p.coerce, %"struct.std::__2::pair.177"* nonnull align 4 dereferenceable(24) %__v) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__map_iterator.178", align 4
  %__p = alloca %"class.std::__2::__map_const_iterator", align 4
  %this.addr = alloca %"class.std::__2::map"*, align 4
  %__v.addr = alloca %"struct.std::__2::pair.177"*, align 4
  %agg.tmp = alloca %"class.std::__2::__tree_iterator.179", align 4
  %agg.tmp3 = alloca %"class.std::__2::__tree_const_iterator", align 4
  %coerce.dive = getelementptr inbounds %"class.std::__2::__map_const_iterator", %"class.std::__2::__map_const_iterator"* %__p, i32 0, i32 0
  %coerce.dive1 = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %coerce.dive, i32 0, i32 0
  store %"class.std::__2::__tree_end_node"* %__p.coerce, %"class.std::__2::__tree_end_node"** %coerce.dive1, align 4
  store %"class.std::__2::map"* %this, %"class.std::__2::map"** %this.addr, align 4
  store %"struct.std::__2::pair.177"* %__v, %"struct.std::__2::pair.177"** %__v.addr, align 4
  %this2 = load %"class.std::__2::map"*, %"class.std::__2::map"** %this.addr, align 4
  %__tree_ = getelementptr inbounds %"class.std::__2::map", %"class.std::__2::map"* %this2, i32 0, i32 0
  %__i_ = getelementptr inbounds %"class.std::__2::__map_const_iterator", %"class.std::__2::__map_const_iterator"* %__p, i32 0, i32 0
  %0 = bitcast %"class.std::__2::__tree_const_iterator"* %agg.tmp3 to i8*
  %1 = bitcast %"class.std::__2::__tree_const_iterator"* %__i_ to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %0, i8* align 4 %1, i32 4, i1 false)
  %2 = load %"struct.std::__2::pair.177"*, %"struct.std::__2::pair.177"** %__v.addr, align 4
  %coerce.dive4 = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %agg.tmp3, i32 0, i32 0
  %3 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %coerce.dive4, align 4
  %call = call %"class.std::__2::__tree_end_node"* @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE15__insert_uniqueENS_21__tree_const_iteratorIS8_PNS_11__tree_nodeIS8_PvEElEERKNS_4pairIKS7_S7_EE(%"class.std::__2::__tree"* %__tree_, %"class.std::__2::__tree_end_node"* %3, %"struct.std::__2::pair.177"* nonnull align 4 dereferenceable(24) %2)
  %coerce.dive5 = getelementptr inbounds %"class.std::__2::__tree_iterator.179", %"class.std::__2::__tree_iterator.179"* %agg.tmp, i32 0, i32 0
  store %"class.std::__2::__tree_end_node"* %call, %"class.std::__2::__tree_end_node"** %coerce.dive5, align 4
  %coerce.dive6 = getelementptr inbounds %"class.std::__2::__tree_iterator.179", %"class.std::__2::__tree_iterator.179"* %agg.tmp, i32 0, i32 0
  %4 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %coerce.dive6, align 4
  %call7 = call %"class.std::__2::__map_iterator.178"* @_ZNSt3__214__map_iteratorINS_15__tree_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPNS_11__tree_nodeIS9_PvEElEEEC2ESE_(%"class.std::__2::__map_iterator.178"* %retval, %"class.std::__2::__tree_end_node"* %4) #9
  %coerce.dive8 = getelementptr inbounds %"class.std::__2::__map_iterator.178", %"class.std::__2::__map_iterator.178"* %retval, i32 0, i32 0
  %coerce.dive9 = getelementptr inbounds %"class.std::__2::__tree_iterator.179", %"class.std::__2::__tree_iterator.179"* %coerce.dive8, i32 0, i32 0
  %5 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %coerce.dive9, align 4
  ret %"class.std::__2::__tree_end_node"* %5
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__map_const_iterator"* @_ZNSt3__220__map_const_iteratorINS_21__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPNS_11__tree_nodeIS9_PvEElEEEC2ESE_(%"class.std::__2::__map_const_iterator"* returned %this, %"class.std::__2::__tree_end_node"* %__i.coerce) unnamed_addr #0 comdat {
entry:
  %__i = alloca %"class.std::__2::__tree_const_iterator", align 4
  %this.addr = alloca %"class.std::__2::__map_const_iterator"*, align 4
  %coerce.dive = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %__i, i32 0, i32 0
  store %"class.std::__2::__tree_end_node"* %__i.coerce, %"class.std::__2::__tree_end_node"** %coerce.dive, align 4
  store %"class.std::__2::__map_const_iterator"* %this, %"class.std::__2::__map_const_iterator"** %this.addr, align 4
  %this1 = load %"class.std::__2::__map_const_iterator"*, %"class.std::__2::__map_const_iterator"** %this.addr, align 4
  %__i_ = getelementptr inbounds %"class.std::__2::__map_const_iterator", %"class.std::__2::__map_const_iterator"* %this1, i32 0, i32 0
  %0 = bitcast %"class.std::__2::__tree_const_iterator"* %__i_ to i8*
  %1 = bitcast %"class.std::__2::__tree_const_iterator"* %__i to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %0, i8* align 4 %1, i32 4, i1 false)
  ret %"class.std::__2::__map_const_iterator"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(24) %"struct.std::__2::pair.177"* @_ZNKSt3__220__map_const_iteratorINS_21__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPNS_11__tree_nodeIS9_PvEElEEEdeEv(%"class.std::__2::__map_const_iterator"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__map_const_iterator"*, align 4
  store %"class.std::__2::__map_const_iterator"* %this, %"class.std::__2::__map_const_iterator"** %this.addr, align 4
  %this1 = load %"class.std::__2::__map_const_iterator"*, %"class.std::__2::__map_const_iterator"** %this.addr, align 4
  %__i_ = getelementptr inbounds %"class.std::__2::__map_const_iterator", %"class.std::__2::__map_const_iterator"* %this1, i32 0, i32 0
  %call = call %"struct.std::__2::__value_type.181"* @_ZNKSt3__221__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElEptEv(%"class.std::__2::__tree_const_iterator"* %__i_)
  %call2 = call nonnull align 4 dereferenceable(24) %"struct.std::__2::pair.177"* @_ZNKSt3__212__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_E11__get_valueEv(%"struct.std::__2::__value_type.181"* %call)
  ret %"struct.std::__2::pair.177"* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__map_const_iterator"* @_ZNSt3__220__map_const_iteratorINS_21__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPNS_11__tree_nodeIS9_PvEElEEEppEv(%"class.std::__2::__map_const_iterator"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__map_const_iterator"*, align 4
  store %"class.std::__2::__map_const_iterator"* %this, %"class.std::__2::__map_const_iterator"** %this.addr, align 4
  %this1 = load %"class.std::__2::__map_const_iterator"*, %"class.std::__2::__map_const_iterator"** %this.addr, align 4
  %__i_ = getelementptr inbounds %"class.std::__2::__map_const_iterator", %"class.std::__2::__map_const_iterator"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_const_iterator"* @_ZNSt3__221__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElEppEv(%"class.std::__2::__tree_const_iterator"* %__i_)
  ret %"class.std::__2::__map_const_iterator"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNSt3__2neERKNS_21__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElEESF_(%"class.std::__2::__tree_const_iterator"* nonnull align 4 dereferenceable(4) %__x, %"class.std::__2::__tree_const_iterator"* nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %__x.addr = alloca %"class.std::__2::__tree_const_iterator"*, align 4
  %__y.addr = alloca %"class.std::__2::__tree_const_iterator"*, align 4
  store %"class.std::__2::__tree_const_iterator"* %__x, %"class.std::__2::__tree_const_iterator"** %__x.addr, align 4
  store %"class.std::__2::__tree_const_iterator"* %__y, %"class.std::__2::__tree_const_iterator"** %__y.addr, align 4
  %0 = load %"class.std::__2::__tree_const_iterator"*, %"class.std::__2::__tree_const_iterator"** %__x.addr, align 4
  %1 = load %"class.std::__2::__tree_const_iterator"*, %"class.std::__2::__tree_const_iterator"** %__y.addr, align 4
  %call = call zeroext i1 @_ZNSt3__2eqERKNS_21__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElEESF_(%"class.std::__2::__tree_const_iterator"* nonnull align 4 dereferenceable(4) %0, %"class.std::__2::__tree_const_iterator"* nonnull align 4 dereferenceable(4) %1)
  %lnot = xor i1 %call, true
  ret i1 %lnot
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNSt3__2eqERKNS_21__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElEESF_(%"class.std::__2::__tree_const_iterator"* nonnull align 4 dereferenceable(4) %__x, %"class.std::__2::__tree_const_iterator"* nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %__x.addr = alloca %"class.std::__2::__tree_const_iterator"*, align 4
  %__y.addr = alloca %"class.std::__2::__tree_const_iterator"*, align 4
  store %"class.std::__2::__tree_const_iterator"* %__x, %"class.std::__2::__tree_const_iterator"** %__x.addr, align 4
  store %"class.std::__2::__tree_const_iterator"* %__y, %"class.std::__2::__tree_const_iterator"** %__y.addr, align 4
  %0 = load %"class.std::__2::__tree_const_iterator"*, %"class.std::__2::__tree_const_iterator"** %__x.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %0, i32 0, i32 0
  %1 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %__ptr_, align 4
  %2 = load %"class.std::__2::__tree_const_iterator"*, %"class.std::__2::__tree_const_iterator"** %__y.addr, align 4
  %__ptr_1 = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %2, i32 0, i32 0
  %3 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %__ptr_1, align 4
  %cmp = icmp eq %"class.std::__2::__tree_end_node"* %1, %3
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_end_node"* @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE15__insert_uniqueENS_21__tree_const_iteratorIS8_PNS_11__tree_nodeIS8_PvEElEERKNS_4pairIKS7_S7_EE(%"class.std::__2::__tree"* %this, %"class.std::__2::__tree_end_node"* %__p.coerce, %"struct.std::__2::pair.177"* nonnull align 4 dereferenceable(24) %__v) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__tree_iterator.179", align 4
  %__p = alloca %"class.std::__2::__tree_const_iterator", align 4
  %this.addr = alloca %"class.std::__2::__tree"*, align 4
  %__v.addr = alloca %"struct.std::__2::pair.177"*, align 4
  %agg.tmp = alloca %"class.std::__2::__tree_const_iterator", align 4
  %coerce.dive = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %__p, i32 0, i32 0
  store %"class.std::__2::__tree_end_node"* %__p.coerce, %"class.std::__2::__tree_end_node"** %coerce.dive, align 4
  store %"class.std::__2::__tree"* %this, %"class.std::__2::__tree"** %this.addr, align 4
  store %"struct.std::__2::pair.177"* %__v, %"struct.std::__2::pair.177"** %__v.addr, align 4
  %this1 = load %"class.std::__2::__tree"*, %"class.std::__2::__tree"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__tree_const_iterator"* %agg.tmp to i8*
  %1 = bitcast %"class.std::__2::__tree_const_iterator"* %__p to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %0, i8* align 4 %1, i32 4, i1 false)
  %2 = load %"struct.std::__2::pair.177"*, %"struct.std::__2::pair.177"** %__v.addr, align 4
  %call = call nonnull align 4 dereferenceable(12) %"class.std::__2::basic_string"* @_ZNSt3__222__tree_key_value_typesINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEE9__get_keyIKNS_4pairIKS7_S7_EEEENS_9enable_ifIXsr17__is_same_uncvrefIT_SD_EE5valueERSC_E4typeERSG_(%"struct.std::__2::pair.177"* nonnull align 4 dereferenceable(24) %2)
  %3 = load %"struct.std::__2::pair.177"*, %"struct.std::__2::pair.177"** %__v.addr, align 4
  %coerce.dive2 = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %agg.tmp, i32 0, i32 0
  %4 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %coerce.dive2, align 4
  %call3 = call %"class.std::__2::__tree_end_node"* @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE30__emplace_hint_unique_key_argsIS7_JRKNS_4pairIKS7_S7_EEEEENS_15__tree_iteratorIS8_PNS_11__tree_nodeIS8_PvEElEENS_21__tree_const_iteratorIS8_SP_lEERKT_DpOT0_(%"class.std::__2::__tree"* %this1, %"class.std::__2::__tree_end_node"* %4, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %call, %"struct.std::__2::pair.177"* nonnull align 4 dereferenceable(24) %3)
  %coerce.dive4 = getelementptr inbounds %"class.std::__2::__tree_iterator.179", %"class.std::__2::__tree_iterator.179"* %retval, i32 0, i32 0
  store %"class.std::__2::__tree_end_node"* %call3, %"class.std::__2::__tree_end_node"** %coerce.dive4, align 4
  %coerce.dive5 = getelementptr inbounds %"class.std::__2::__tree_iterator.179", %"class.std::__2::__tree_iterator.179"* %retval, i32 0, i32 0
  %5 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %coerce.dive5, align 4
  ret %"class.std::__2::__tree_end_node"* %5
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__map_iterator.178"* @_ZNSt3__214__map_iteratorINS_15__tree_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPNS_11__tree_nodeIS9_PvEElEEEC2ESE_(%"class.std::__2::__map_iterator.178"* returned %this, %"class.std::__2::__tree_end_node"* %__i.coerce) unnamed_addr #0 comdat {
entry:
  %__i = alloca %"class.std::__2::__tree_iterator.179", align 4
  %this.addr = alloca %"class.std::__2::__map_iterator.178"*, align 4
  %coerce.dive = getelementptr inbounds %"class.std::__2::__tree_iterator.179", %"class.std::__2::__tree_iterator.179"* %__i, i32 0, i32 0
  store %"class.std::__2::__tree_end_node"* %__i.coerce, %"class.std::__2::__tree_end_node"** %coerce.dive, align 4
  store %"class.std::__2::__map_iterator.178"* %this, %"class.std::__2::__map_iterator.178"** %this.addr, align 4
  %this1 = load %"class.std::__2::__map_iterator.178"*, %"class.std::__2::__map_iterator.178"** %this.addr, align 4
  %__i_ = getelementptr inbounds %"class.std::__2::__map_iterator.178", %"class.std::__2::__map_iterator.178"* %this1, i32 0, i32 0
  %0 = bitcast %"class.std::__2::__tree_iterator.179"* %__i_ to i8*
  %1 = bitcast %"class.std::__2::__tree_iterator.179"* %__i to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %0, i8* align 4 %1, i32 4, i1 false)
  ret %"class.std::__2::__map_iterator.178"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_end_node"* @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE30__emplace_hint_unique_key_argsIS7_JRKNS_4pairIKS7_S7_EEEEENS_15__tree_iteratorIS8_PNS_11__tree_nodeIS8_PvEElEENS_21__tree_const_iteratorIS8_SP_lEERKT_DpOT0_(%"class.std::__2::__tree"* %this, %"class.std::__2::__tree_end_node"* %__p.coerce, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %__k, %"struct.std::__2::pair.177"* nonnull align 4 dereferenceable(24) %__args) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__tree_iterator.179", align 4
  %__p = alloca %"class.std::__2::__tree_const_iterator", align 4
  %this.addr = alloca %"class.std::__2::__tree"*, align 4
  %__k.addr = alloca %"class.std::__2::basic_string"*, align 4
  %__args.addr = alloca %"struct.std::__2::pair.177"*, align 4
  %__parent = alloca %"class.std::__2::__tree_end_node"*, align 4
  %__dummy = alloca %"class.std::__2::__tree_node_base"*, align 4
  %__child = alloca %"class.std::__2::__tree_node_base"**, align 4
  %agg.tmp = alloca %"class.std::__2::__tree_const_iterator", align 4
  %__r = alloca %"class.std::__2::__tree_node.180"*, align 4
  %__h = alloca %"class.std::__2::unique_ptr.182", align 4
  %coerce.dive = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %__p, i32 0, i32 0
  store %"class.std::__2::__tree_end_node"* %__p.coerce, %"class.std::__2::__tree_end_node"** %coerce.dive, align 4
  store %"class.std::__2::__tree"* %this, %"class.std::__2::__tree"** %this.addr, align 4
  store %"class.std::__2::basic_string"* %__k, %"class.std::__2::basic_string"** %__k.addr, align 4
  store %"struct.std::__2::pair.177"* %__args, %"struct.std::__2::pair.177"** %__args.addr, align 4
  %this1 = load %"class.std::__2::__tree"*, %"class.std::__2::__tree"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__tree_const_iterator"* %agg.tmp to i8*
  %1 = bitcast %"class.std::__2::__tree_const_iterator"* %__p to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %0, i8* align 4 %1, i32 4, i1 false)
  %2 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__k.addr, align 4
  %coerce.dive2 = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %agg.tmp, i32 0, i32 0
  %3 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %coerce.dive2, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_node_base"** @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE12__find_equalIS7_EERPNS_16__tree_node_baseIPvEENS_21__tree_const_iteratorIS8_PNS_11__tree_nodeIS8_SH_EElEERPNS_15__tree_end_nodeISJ_EESK_RKT_(%"class.std::__2::__tree"* %this1, %"class.std::__2::__tree_end_node"* %3, %"class.std::__2::__tree_end_node"** nonnull align 4 dereferenceable(4) %__parent, %"class.std::__2::__tree_node_base"** nonnull align 4 dereferenceable(4) %__dummy, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %2)
  store %"class.std::__2::__tree_node_base"** %call, %"class.std::__2::__tree_node_base"*** %__child, align 4
  %4 = load %"class.std::__2::__tree_node_base"**, %"class.std::__2::__tree_node_base"*** %__child, align 4
  %5 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %4, align 4
  %6 = bitcast %"class.std::__2::__tree_node_base"* %5 to %"class.std::__2::__tree_node.180"*
  store %"class.std::__2::__tree_node.180"* %6, %"class.std::__2::__tree_node.180"** %__r, align 4
  %7 = load %"class.std::__2::__tree_node_base"**, %"class.std::__2::__tree_node_base"*** %__child, align 4
  %8 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %7, align 4
  %cmp = icmp eq %"class.std::__2::__tree_node_base"* %8, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %9 = load %"struct.std::__2::pair.177"*, %"struct.std::__2::pair.177"** %__args.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(24) %"struct.std::__2::pair.177"* @_ZNSt3__27forwardIRKNS_4pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEEEOT_RNS_16remove_referenceISC_E4typeE(%"struct.std::__2::pair.177"* nonnull align 4 dereferenceable(24) %9) #9
  call void @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE16__construct_nodeIJRKNS_4pairIKS7_S7_EEEEENS_10unique_ptrINS_11__tree_nodeIS8_PvEENS_22__tree_node_destructorINS5_ISO_EEEEEEDpOT_(%"class.std::__2::unique_ptr.182"* sret align 4 %__h, %"class.std::__2::__tree"* %this1, %"struct.std::__2::pair.177"* nonnull align 4 dereferenceable(24) %call3)
  %10 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %__parent, align 4
  %11 = load %"class.std::__2::__tree_node_base"**, %"class.std::__2::__tree_node_base"*** %__child, align 4
  %call4 = call %"class.std::__2::__tree_node.180"* @_ZNKSt3__210unique_ptrINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEE3getEv(%"class.std::__2::unique_ptr.182"* %__h) #9
  %12 = bitcast %"class.std::__2::__tree_node.180"* %call4 to %"class.std::__2::__tree_node_base"*
  call void @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE16__insert_node_atEPNS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEERSJ_SJ_(%"class.std::__2::__tree"* %this1, %"class.std::__2::__tree_end_node"* %10, %"class.std::__2::__tree_node_base"** nonnull align 4 dereferenceable(4) %11, %"class.std::__2::__tree_node_base"* %12) #9
  %call5 = call %"class.std::__2::__tree_node.180"* @_ZNSt3__210unique_ptrINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEE7releaseEv(%"class.std::__2::unique_ptr.182"* %__h) #9
  store %"class.std::__2::__tree_node.180"* %call5, %"class.std::__2::__tree_node.180"** %__r, align 4
  %call6 = call %"class.std::__2::unique_ptr.182"* @_ZNSt3__210unique_ptrINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEED2Ev(%"class.std::__2::unique_ptr.182"* %__h) #9
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %13 = load %"class.std::__2::__tree_node.180"*, %"class.std::__2::__tree_node.180"** %__r, align 4
  %call7 = call %"class.std::__2::__tree_iterator.179"* @_ZNSt3__215__tree_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElEC2ESC_(%"class.std::__2::__tree_iterator.179"* %retval, %"class.std::__2::__tree_node.180"* %13) #9
  %coerce.dive8 = getelementptr inbounds %"class.std::__2::__tree_iterator.179", %"class.std::__2::__tree_iterator.179"* %retval, i32 0, i32 0
  %14 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %coerce.dive8, align 4
  ret %"class.std::__2::__tree_end_node"* %14
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"class.std::__2::basic_string"* @_ZNSt3__222__tree_key_value_typesINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEE9__get_keyIKNS_4pairIKS7_S7_EEEENS_9enable_ifIXsr17__is_same_uncvrefIT_SD_EE5valueERSC_E4typeERSG_(%"struct.std::__2::pair.177"* nonnull align 4 dereferenceable(24) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"struct.std::__2::pair.177"*, align 4
  store %"struct.std::__2::pair.177"* %__t, %"struct.std::__2::pair.177"** %__t.addr, align 4
  %0 = load %"struct.std::__2::pair.177"*, %"struct.std::__2::pair.177"** %__t.addr, align 4
  %first = getelementptr inbounds %"struct.std::__2::pair.177", %"struct.std::__2::pair.177"* %0, i32 0, i32 0
  ret %"class.std::__2::basic_string"* %first
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_node_base"** @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE12__find_equalIS7_EERPNS_16__tree_node_baseIPvEENS_21__tree_const_iteratorIS8_PNS_11__tree_nodeIS8_SH_EElEERPNS_15__tree_end_nodeISJ_EESK_RKT_(%"class.std::__2::__tree"* %this, %"class.std::__2::__tree_end_node"* %__hint.coerce, %"class.std::__2::__tree_end_node"** nonnull align 4 dereferenceable(4) %__parent, %"class.std::__2::__tree_node_base"** nonnull align 4 dereferenceable(4) %__dummy, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %__v) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__tree_node_base"**, align 4
  %__hint = alloca %"class.std::__2::__tree_const_iterator", align 4
  %this.addr = alloca %"class.std::__2::__tree"*, align 4
  %__parent.addr = alloca %"class.std::__2::__tree_end_node"**, align 4
  %__dummy.addr = alloca %"class.std::__2::__tree_node_base"**, align 4
  %__v.addr = alloca %"class.std::__2::basic_string"*, align 4
  %ref.tmp = alloca %"class.std::__2::__tree_const_iterator", align 4
  %agg.tmp = alloca %"class.std::__2::__tree_iterator.179", align 4
  %__prior = alloca %"class.std::__2::__tree_const_iterator", align 4
  %ref.tmp9 = alloca %"class.std::__2::__tree_const_iterator", align 4
  %agg.tmp10 = alloca %"class.std::__2::__tree_iterator.179", align 4
  %__next = alloca %"class.std::__2::__tree_const_iterator", align 4
  %agg.tmp34 = alloca %"class.std::__2::__tree_const_iterator", align 4
  %ref.tmp38 = alloca %"class.std::__2::__tree_const_iterator", align 4
  %agg.tmp39 = alloca %"class.std::__2::__tree_iterator.179", align 4
  %coerce.dive = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %__hint, i32 0, i32 0
  store %"class.std::__2::__tree_end_node"* %__hint.coerce, %"class.std::__2::__tree_end_node"** %coerce.dive, align 4
  store %"class.std::__2::__tree"* %this, %"class.std::__2::__tree"** %this.addr, align 4
  store %"class.std::__2::__tree_end_node"** %__parent, %"class.std::__2::__tree_end_node"*** %__parent.addr, align 4
  store %"class.std::__2::__tree_node_base"** %__dummy, %"class.std::__2::__tree_node_base"*** %__dummy.addr, align 4
  store %"class.std::__2::basic_string"* %__v, %"class.std::__2::basic_string"** %__v.addr, align 4
  %this1 = load %"class.std::__2::__tree"*, %"class.std::__2::__tree"** %this.addr, align 4
  %call = call %"class.std::__2::__tree_end_node"* @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE3endEv(%"class.std::__2::__tree"* %this1) #9
  %coerce.dive2 = getelementptr inbounds %"class.std::__2::__tree_iterator.179", %"class.std::__2::__tree_iterator.179"* %agg.tmp, i32 0, i32 0
  store %"class.std::__2::__tree_end_node"* %call, %"class.std::__2::__tree_end_node"** %coerce.dive2, align 4
  %coerce.dive3 = getelementptr inbounds %"class.std::__2::__tree_iterator.179", %"class.std::__2::__tree_iterator.179"* %agg.tmp, i32 0, i32 0
  %0 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %coerce.dive3, align 4
  %call4 = call %"class.std::__2::__tree_const_iterator"* @_ZNSt3__221__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElEC2ENS_15__tree_iteratorIS8_SC_lEE(%"class.std::__2::__tree_const_iterator"* %ref.tmp, %"class.std::__2::__tree_end_node"* %0) #9
  %call5 = call zeroext i1 @_ZNSt3__2eqERKNS_21__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElEESF_(%"class.std::__2::__tree_const_iterator"* nonnull align 4 dereferenceable(4) %__hint, %"class.std::__2::__tree_const_iterator"* nonnull align 4 dereferenceable(4) %ref.tmp)
  br i1 %call5, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %entry
  %call6 = call nonnull align 1 dereferenceable(1) %"class.std::__2::__map_value_compare"* @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE10value_compEv(%"class.std::__2::__tree"* %this1) #9
  %1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__v.addr, align 4
  %call7 = call nonnull align 4 dereferenceable(24) %"struct.std::__2::__value_type.181"* @_ZNKSt3__221__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElEdeEv(%"class.std::__2::__tree_const_iterator"* %__hint)
  %call8 = call zeroext i1 @_ZNKSt3__219__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS6_S6_EENS_4lessIS6_EELb1EEclERKS6_RKS8_(%"class.std::__2::__map_value_compare"* %call6, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %1, %"struct.std::__2::__value_type.181"* nonnull align 4 dereferenceable(24) %call7)
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %entry
  %2 = phi i1 [ true, %entry ], [ %call8, %lor.rhs ]
  br i1 %2, label %if.then, label %if.else29

if.then:                                          ; preds = %lor.end
  %3 = bitcast %"class.std::__2::__tree_const_iterator"* %__prior to i8*
  %4 = bitcast %"class.std::__2::__tree_const_iterator"* %__hint to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 4, i1 false)
  %call11 = call %"class.std::__2::__tree_end_node"* @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE5beginEv(%"class.std::__2::__tree"* %this1) #9
  %coerce.dive12 = getelementptr inbounds %"class.std::__2::__tree_iterator.179", %"class.std::__2::__tree_iterator.179"* %agg.tmp10, i32 0, i32 0
  store %"class.std::__2::__tree_end_node"* %call11, %"class.std::__2::__tree_end_node"** %coerce.dive12, align 4
  %coerce.dive13 = getelementptr inbounds %"class.std::__2::__tree_iterator.179", %"class.std::__2::__tree_iterator.179"* %agg.tmp10, i32 0, i32 0
  %5 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %coerce.dive13, align 4
  %call14 = call %"class.std::__2::__tree_const_iterator"* @_ZNSt3__221__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElEC2ENS_15__tree_iteratorIS8_SC_lEE(%"class.std::__2::__tree_const_iterator"* %ref.tmp9, %"class.std::__2::__tree_end_node"* %5) #9
  %call15 = call zeroext i1 @_ZNSt3__2eqERKNS_21__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElEESF_(%"class.std::__2::__tree_const_iterator"* nonnull align 4 dereferenceable(4) %__prior, %"class.std::__2::__tree_const_iterator"* nonnull align 4 dereferenceable(4) %ref.tmp9)
  br i1 %call15, label %lor.end21, label %lor.rhs16

lor.rhs16:                                        ; preds = %if.then
  %call17 = call nonnull align 1 dereferenceable(1) %"class.std::__2::__map_value_compare"* @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE10value_compEv(%"class.std::__2::__tree"* %this1) #9
  %call18 = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_const_iterator"* @_ZNSt3__221__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElEmmEv(%"class.std::__2::__tree_const_iterator"* %__prior)
  %call19 = call nonnull align 4 dereferenceable(24) %"struct.std::__2::__value_type.181"* @_ZNKSt3__221__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElEdeEv(%"class.std::__2::__tree_const_iterator"* %call18)
  %6 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__v.addr, align 4
  %call20 = call zeroext i1 @_ZNKSt3__219__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS6_S6_EENS_4lessIS6_EELb1EEclERKS8_RKS6_(%"class.std::__2::__map_value_compare"* %call17, %"struct.std::__2::__value_type.181"* nonnull align 4 dereferenceable(24) %call19, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %6)
  br label %lor.end21

lor.end21:                                        ; preds = %lor.rhs16, %if.then
  %7 = phi i1 [ true, %if.then ], [ %call20, %lor.rhs16 ]
  br i1 %7, label %if.then22, label %if.end

if.then22:                                        ; preds = %lor.end21
  %__ptr_ = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %__hint, i32 0, i32 0
  %8 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %__ptr_, align 4
  %__left_ = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %8, i32 0, i32 0
  %9 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__left_, align 4
  %cmp = icmp eq %"class.std::__2::__tree_node_base"* %9, null
  br i1 %cmp, label %if.then23, label %if.else

if.then23:                                        ; preds = %if.then22
  %__ptr_24 = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %__hint, i32 0, i32 0
  %10 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %__ptr_24, align 4
  %11 = load %"class.std::__2::__tree_end_node"**, %"class.std::__2::__tree_end_node"*** %__parent.addr, align 4
  store %"class.std::__2::__tree_end_node"* %10, %"class.std::__2::__tree_end_node"** %11, align 4
  %12 = load %"class.std::__2::__tree_end_node"**, %"class.std::__2::__tree_end_node"*** %__parent.addr, align 4
  %13 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %12, align 4
  %__left_25 = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %13, i32 0, i32 0
  store %"class.std::__2::__tree_node_base"** %__left_25, %"class.std::__2::__tree_node_base"*** %retval, align 4
  br label %return

if.else:                                          ; preds = %if.then22
  %__ptr_26 = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %__prior, i32 0, i32 0
  %14 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %__ptr_26, align 4
  %15 = load %"class.std::__2::__tree_end_node"**, %"class.std::__2::__tree_end_node"*** %__parent.addr, align 4
  store %"class.std::__2::__tree_end_node"* %14, %"class.std::__2::__tree_end_node"** %15, align 4
  %__ptr_27 = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %__prior, i32 0, i32 0
  %16 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %__ptr_27, align 4
  %17 = bitcast %"class.std::__2::__tree_end_node"* %16 to %"class.std::__2::__tree_node_base"*
  %__right_ = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %17, i32 0, i32 1
  store %"class.std::__2::__tree_node_base"** %__right_, %"class.std::__2::__tree_node_base"*** %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.end21
  %18 = load %"class.std::__2::__tree_end_node"**, %"class.std::__2::__tree_end_node"*** %__parent.addr, align 4
  %19 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__v.addr, align 4
  %call28 = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_node_base"** @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE12__find_equalIS7_EERPNS_16__tree_node_baseIPvEERPNS_15__tree_end_nodeISJ_EERKT_(%"class.std::__2::__tree"* %this1, %"class.std::__2::__tree_end_node"** nonnull align 4 dereferenceable(4) %18, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %19)
  store %"class.std::__2::__tree_node_base"** %call28, %"class.std::__2::__tree_node_base"*** %retval, align 4
  br label %return

if.else29:                                        ; preds = %lor.end
  %call30 = call nonnull align 1 dereferenceable(1) %"class.std::__2::__map_value_compare"* @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE10value_compEv(%"class.std::__2::__tree"* %this1) #9
  %call31 = call nonnull align 4 dereferenceable(24) %"struct.std::__2::__value_type.181"* @_ZNKSt3__221__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElEdeEv(%"class.std::__2::__tree_const_iterator"* %__hint)
  %20 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__v.addr, align 4
  %call32 = call zeroext i1 @_ZNKSt3__219__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS6_S6_EENS_4lessIS6_EELb1EEclERKS8_RKS6_(%"class.std::__2::__map_value_compare"* %call30, %"struct.std::__2::__value_type.181"* nonnull align 4 dereferenceable(24) %call31, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %20)
  br i1 %call32, label %if.then33, label %if.end63

if.then33:                                        ; preds = %if.else29
  %21 = bitcast %"class.std::__2::__tree_const_iterator"* %agg.tmp34 to i8*
  %22 = bitcast %"class.std::__2::__tree_const_iterator"* %__hint to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %21, i8* align 4 %22, i32 4, i1 false)
  %coerce.dive35 = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %agg.tmp34, i32 0, i32 0
  %23 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %coerce.dive35, align 4
  %call36 = call %"class.std::__2::__tree_end_node"* @_ZNSt3__24nextINS_21__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPNS_11__tree_nodeIS9_PvEElEEEENS_9enable_ifIXsr25__is_cpp17_input_iteratorIT_EE5valueESG_E4typeESG_NS_15iterator_traitsISG_E15difference_typeE(%"class.std::__2::__tree_end_node"* %23, i32 1)
  %coerce.dive37 = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %__next, i32 0, i32 0
  store %"class.std::__2::__tree_end_node"* %call36, %"class.std::__2::__tree_end_node"** %coerce.dive37, align 4
  %call40 = call %"class.std::__2::__tree_end_node"* @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE3endEv(%"class.std::__2::__tree"* %this1) #9
  %coerce.dive41 = getelementptr inbounds %"class.std::__2::__tree_iterator.179", %"class.std::__2::__tree_iterator.179"* %agg.tmp39, i32 0, i32 0
  store %"class.std::__2::__tree_end_node"* %call40, %"class.std::__2::__tree_end_node"** %coerce.dive41, align 4
  %coerce.dive42 = getelementptr inbounds %"class.std::__2::__tree_iterator.179", %"class.std::__2::__tree_iterator.179"* %agg.tmp39, i32 0, i32 0
  %24 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %coerce.dive42, align 4
  %call43 = call %"class.std::__2::__tree_const_iterator"* @_ZNSt3__221__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElEC2ENS_15__tree_iteratorIS8_SC_lEE(%"class.std::__2::__tree_const_iterator"* %ref.tmp38, %"class.std::__2::__tree_end_node"* %24) #9
  %call44 = call zeroext i1 @_ZNSt3__2eqERKNS_21__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElEESF_(%"class.std::__2::__tree_const_iterator"* nonnull align 4 dereferenceable(4) %__next, %"class.std::__2::__tree_const_iterator"* nonnull align 4 dereferenceable(4) %ref.tmp38)
  br i1 %call44, label %lor.end49, label %lor.rhs45

lor.rhs45:                                        ; preds = %if.then33
  %call46 = call nonnull align 1 dereferenceable(1) %"class.std::__2::__map_value_compare"* @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE10value_compEv(%"class.std::__2::__tree"* %this1) #9
  %25 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__v.addr, align 4
  %call47 = call nonnull align 4 dereferenceable(24) %"struct.std::__2::__value_type.181"* @_ZNKSt3__221__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElEdeEv(%"class.std::__2::__tree_const_iterator"* %__next)
  %call48 = call zeroext i1 @_ZNKSt3__219__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS6_S6_EENS_4lessIS6_EELb1EEclERKS6_RKS8_(%"class.std::__2::__map_value_compare"* %call46, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %25, %"struct.std::__2::__value_type.181"* nonnull align 4 dereferenceable(24) %call47)
  br label %lor.end49

lor.end49:                                        ; preds = %lor.rhs45, %if.then33
  %26 = phi i1 [ true, %if.then33 ], [ %call48, %lor.rhs45 ]
  br i1 %26, label %if.then50, label %if.end61

if.then50:                                        ; preds = %lor.end49
  %call51 = call %"class.std::__2::__tree_node.180"* @_ZNKSt3__221__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElE8__get_npEv(%"class.std::__2::__tree_const_iterator"* %__hint)
  %27 = bitcast %"class.std::__2::__tree_node.180"* %call51 to %"class.std::__2::__tree_node_base"*
  %__right_52 = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %27, i32 0, i32 1
  %28 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__right_52, align 4
  %cmp53 = icmp eq %"class.std::__2::__tree_node_base"* %28, null
  br i1 %cmp53, label %if.then54, label %if.else58

if.then54:                                        ; preds = %if.then50
  %__ptr_55 = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %__hint, i32 0, i32 0
  %29 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %__ptr_55, align 4
  %30 = load %"class.std::__2::__tree_end_node"**, %"class.std::__2::__tree_end_node"*** %__parent.addr, align 4
  store %"class.std::__2::__tree_end_node"* %29, %"class.std::__2::__tree_end_node"** %30, align 4
  %__ptr_56 = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %__hint, i32 0, i32 0
  %31 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %__ptr_56, align 4
  %32 = bitcast %"class.std::__2::__tree_end_node"* %31 to %"class.std::__2::__tree_node_base"*
  %__right_57 = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %32, i32 0, i32 1
  store %"class.std::__2::__tree_node_base"** %__right_57, %"class.std::__2::__tree_node_base"*** %retval, align 4
  br label %return

if.else58:                                        ; preds = %if.then50
  %__ptr_59 = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %__next, i32 0, i32 0
  %33 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %__ptr_59, align 4
  %34 = load %"class.std::__2::__tree_end_node"**, %"class.std::__2::__tree_end_node"*** %__parent.addr, align 4
  store %"class.std::__2::__tree_end_node"* %33, %"class.std::__2::__tree_end_node"** %34, align 4
  %35 = load %"class.std::__2::__tree_end_node"**, %"class.std::__2::__tree_end_node"*** %__parent.addr, align 4
  %36 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %35, align 4
  %__left_60 = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %36, i32 0, i32 0
  store %"class.std::__2::__tree_node_base"** %__left_60, %"class.std::__2::__tree_node_base"*** %retval, align 4
  br label %return

if.end61:                                         ; preds = %lor.end49
  %37 = load %"class.std::__2::__tree_end_node"**, %"class.std::__2::__tree_end_node"*** %__parent.addr, align 4
  %38 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__v.addr, align 4
  %call62 = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_node_base"** @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE12__find_equalIS7_EERPNS_16__tree_node_baseIPvEERPNS_15__tree_end_nodeISJ_EERKT_(%"class.std::__2::__tree"* %this1, %"class.std::__2::__tree_end_node"** nonnull align 4 dereferenceable(4) %37, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %38)
  store %"class.std::__2::__tree_node_base"** %call62, %"class.std::__2::__tree_node_base"*** %retval, align 4
  br label %return

if.end63:                                         ; preds = %if.else29
  br label %if.end64

if.end64:                                         ; preds = %if.end63
  %__ptr_65 = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %__hint, i32 0, i32 0
  %39 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %__ptr_65, align 4
  %40 = load %"class.std::__2::__tree_end_node"**, %"class.std::__2::__tree_end_node"*** %__parent.addr, align 4
  store %"class.std::__2::__tree_end_node"* %39, %"class.std::__2::__tree_end_node"** %40, align 4
  %__ptr_66 = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %__hint, i32 0, i32 0
  %41 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %__ptr_66, align 4
  %42 = bitcast %"class.std::__2::__tree_end_node"* %41 to %"class.std::__2::__tree_node_base"*
  %43 = load %"class.std::__2::__tree_node_base"**, %"class.std::__2::__tree_node_base"*** %__dummy.addr, align 4
  store %"class.std::__2::__tree_node_base"* %42, %"class.std::__2::__tree_node_base"** %43, align 4
  %44 = load %"class.std::__2::__tree_node_base"**, %"class.std::__2::__tree_node_base"*** %__dummy.addr, align 4
  store %"class.std::__2::__tree_node_base"** %44, %"class.std::__2::__tree_node_base"*** %retval, align 4
  br label %return

return:                                           ; preds = %if.end64, %if.end61, %if.else58, %if.then54, %if.end, %if.else, %if.then23
  %45 = load %"class.std::__2::__tree_node_base"**, %"class.std::__2::__tree_node_base"*** %retval, align 4
  ret %"class.std::__2::__tree_node_base"** %45
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE16__construct_nodeIJRKNS_4pairIKS7_S7_EEEEENS_10unique_ptrINS_11__tree_nodeIS8_PvEENS_22__tree_node_destructorINS5_ISO_EEEEEEDpOT_(%"class.std::__2::unique_ptr.182"* noalias sret align 4 %agg.result, %"class.std::__2::__tree"* %this, %"struct.std::__2::pair.177"* nonnull align 4 dereferenceable(24) %__args) #0 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %"class.std::__2::__tree"*, align 4
  %__args.addr = alloca %"struct.std::__2::pair.177"*, align 4
  %__na = alloca %"class.std::__2::allocator.117"*, align 4
  %nrvo = alloca i1, align 1
  %ref.tmp = alloca %"class.std::__2::__tree_node_destructor.186", align 4
  %0 = bitcast %"class.std::__2::unique_ptr.182"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %"class.std::__2::__tree"* %this, %"class.std::__2::__tree"** %this.addr, align 4
  store %"struct.std::__2::pair.177"* %__args, %"struct.std::__2::pair.177"** %__args.addr, align 4
  %this1 = load %"class.std::__2::__tree"*, %"class.std::__2::__tree"** %this.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.117"* @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE12__node_allocEv(%"class.std::__2::__tree"* %this1) #9
  store %"class.std::__2::allocator.117"* %call, %"class.std::__2::allocator.117"** %__na, align 4
  store i1 false, i1* %nrvo, align 1
  %1 = load %"class.std::__2::allocator.117"*, %"class.std::__2::allocator.117"** %__na, align 4
  %call2 = call %"class.std::__2::__tree_node.180"* @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEEE8allocateERSC_m(%"class.std::__2::allocator.117"* nonnull align 1 dereferenceable(1) %1, i32 1)
  %2 = load %"class.std::__2::allocator.117"*, %"class.std::__2::allocator.117"** %__na, align 4
  %call3 = call %"class.std::__2::__tree_node_destructor.186"* @_ZNSt3__222__tree_node_destructorINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEEEC2ERSC_b(%"class.std::__2::__tree_node_destructor.186"* %ref.tmp, %"class.std::__2::allocator.117"* nonnull align 1 dereferenceable(1) %2, i1 zeroext false) #9
  %call4 = call %"class.std::__2::unique_ptr.182"* @_ZNSt3__210unique_ptrINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEEC2ILb1EvEEPSB_NS_16__dependent_typeINS_27__unique_ptr_deleter_sfinaeISE_EEXT_EE20__good_rval_ref_typeE(%"class.std::__2::unique_ptr.182"* %agg.result, %"class.std::__2::__tree_node.180"* %call2, %"class.std::__2::__tree_node_destructor.186"* nonnull align 4 dereferenceable(5) %ref.tmp) #9
  %3 = load %"class.std::__2::allocator.117"*, %"class.std::__2::allocator.117"** %__na, align 4
  %call5 = call %"class.std::__2::__tree_node.180"* @_ZNKSt3__210unique_ptrINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEEptEv(%"class.std::__2::unique_ptr.182"* %agg.result) #9
  %__value_ = getelementptr inbounds %"class.std::__2::__tree_node.180", %"class.std::__2::__tree_node.180"* %call5, i32 0, i32 1
  %call6 = call %"struct.std::__2::pair.177"* @_ZNSt3__222__tree_key_value_typesINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEE9__get_ptrERS8_(%"struct.std::__2::__value_type.181"* nonnull align 4 dereferenceable(24) %__value_)
  %4 = load %"struct.std::__2::pair.177"*, %"struct.std::__2::pair.177"** %__args.addr, align 4
  %call7 = call nonnull align 4 dereferenceable(24) %"struct.std::__2::pair.177"* @_ZNSt3__27forwardIRKNS_4pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEEEOT_RNS_16remove_referenceISC_E4typeE(%"struct.std::__2::pair.177"* nonnull align 4 dereferenceable(24) %4) #9
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEEE9constructINS_4pairIKS8_S8_EEJRKSH_EEEvRSC_PT_DpOT0_(%"class.std::__2::allocator.117"* nonnull align 1 dereferenceable(1) %3, %"struct.std::__2::pair.177"* %call6, %"struct.std::__2::pair.177"* nonnull align 4 dereferenceable(24) %call7)
  %call8 = call nonnull align 4 dereferenceable(5) %"class.std::__2::__tree_node_destructor.186"* @_ZNSt3__210unique_ptrINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEE11get_deleterEv(%"class.std::__2::unique_ptr.182"* %agg.result) #9
  %__value_constructed = getelementptr inbounds %"class.std::__2::__tree_node_destructor.186", %"class.std::__2::__tree_node_destructor.186"* %call8, i32 0, i32 1
  store i8 1, i8* %__value_constructed, align 4
  store i1 true, i1* %nrvo, align 1
  %nrvo.val = load i1, i1* %nrvo, align 1
  br i1 %nrvo.val, label %nrvo.skipdtor, label %nrvo.unused

nrvo.unused:                                      ; preds = %entry
  %call9 = call %"class.std::__2::unique_ptr.182"* @_ZNSt3__210unique_ptrINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEED2Ev(%"class.std::__2::unique_ptr.182"* %agg.result) #9
  br label %nrvo.skipdtor

nrvo.skipdtor:                                    ; preds = %nrvo.unused, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(24) %"struct.std::__2::pair.177"* @_ZNSt3__27forwardIRKNS_4pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEEEOT_RNS_16remove_referenceISC_E4typeE(%"struct.std::__2::pair.177"* nonnull align 4 dereferenceable(24) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"struct.std::__2::pair.177"*, align 4
  store %"struct.std::__2::pair.177"* %__t, %"struct.std::__2::pair.177"** %__t.addr, align 4
  %0 = load %"struct.std::__2::pair.177"*, %"struct.std::__2::pair.177"** %__t.addr, align 4
  ret %"struct.std::__2::pair.177"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE16__insert_node_atEPNS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEERSJ_SJ_(%"class.std::__2::__tree"* %this, %"class.std::__2::__tree_end_node"* %__parent, %"class.std::__2::__tree_node_base"** nonnull align 4 dereferenceable(4) %__child, %"class.std::__2::__tree_node_base"* %__new_node) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree"*, align 4
  %__parent.addr = alloca %"class.std::__2::__tree_end_node"*, align 4
  %__child.addr = alloca %"class.std::__2::__tree_node_base"**, align 4
  %__new_node.addr = alloca %"class.std::__2::__tree_node_base"*, align 4
  store %"class.std::__2::__tree"* %this, %"class.std::__2::__tree"** %this.addr, align 4
  store %"class.std::__2::__tree_end_node"* %__parent, %"class.std::__2::__tree_end_node"** %__parent.addr, align 4
  store %"class.std::__2::__tree_node_base"** %__child, %"class.std::__2::__tree_node_base"*** %__child.addr, align 4
  store %"class.std::__2::__tree_node_base"* %__new_node, %"class.std::__2::__tree_node_base"** %__new_node.addr, align 4
  %this1 = load %"class.std::__2::__tree"*, %"class.std::__2::__tree"** %this.addr, align 4
  %0 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__new_node.addr, align 4
  %1 = bitcast %"class.std::__2::__tree_node_base"* %0 to %"class.std::__2::__tree_end_node"*
  %__left_ = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %1, i32 0, i32 0
  store %"class.std::__2::__tree_node_base"* null, %"class.std::__2::__tree_node_base"** %__left_, align 4
  %2 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__new_node.addr, align 4
  %__right_ = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %2, i32 0, i32 1
  store %"class.std::__2::__tree_node_base"* null, %"class.std::__2::__tree_node_base"** %__right_, align 4
  %3 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %__parent.addr, align 4
  %4 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__new_node.addr, align 4
  %__parent_ = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %4, i32 0, i32 2
  store %"class.std::__2::__tree_end_node"* %3, %"class.std::__2::__tree_end_node"** %__parent_, align 4
  %5 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__new_node.addr, align 4
  %6 = load %"class.std::__2::__tree_node_base"**, %"class.std::__2::__tree_node_base"*** %__child.addr, align 4
  store %"class.std::__2::__tree_node_base"* %5, %"class.std::__2::__tree_node_base"** %6, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_end_node"** @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE12__begin_nodeEv(%"class.std::__2::__tree"* %this1) #9
  %7 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %call, align 4
  %__left_2 = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %7, i32 0, i32 0
  %8 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__left_2, align 4
  %cmp = icmp ne %"class.std::__2::__tree_node_base"* %8, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_end_node"** @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE12__begin_nodeEv(%"class.std::__2::__tree"* %this1) #9
  %9 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %call3, align 4
  %__left_4 = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %9, i32 0, i32 0
  %10 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__left_4, align 4
  %11 = bitcast %"class.std::__2::__tree_node_base"* %10 to %"class.std::__2::__tree_end_node"*
  %call5 = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_end_node"** @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE12__begin_nodeEv(%"class.std::__2::__tree"* %this1) #9
  store %"class.std::__2::__tree_end_node"* %11, %"class.std::__2::__tree_end_node"** %call5, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %call6 = call %"class.std::__2::__tree_end_node"* @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE10__end_nodeEv(%"class.std::__2::__tree"* %this1) #9
  %__left_7 = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %call6, i32 0, i32 0
  %12 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__left_7, align 4
  %13 = load %"class.std::__2::__tree_node_base"**, %"class.std::__2::__tree_node_base"*** %__child.addr, align 4
  %14 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %13, align 4
  call void @_ZNSt3__227__tree_balance_after_insertIPNS_16__tree_node_baseIPvEEEEvT_S5_(%"class.std::__2::__tree_node_base"* %12, %"class.std::__2::__tree_node_base"* %14) #9
  %call8 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE4sizeEv(%"class.std::__2::__tree"* %this1) #9
  %15 = load i32, i32* %call8, align 4
  %inc = add i32 %15, 1
  store i32 %inc, i32* %call8, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_node.180"* @_ZNKSt3__210unique_ptrINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEE3getEv(%"class.std::__2::unique_ptr.182"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.182"*, align 4
  store %"class.std::__2::unique_ptr.182"* %this, %"class.std::__2::unique_ptr.182"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.182"*, %"class.std::__2::unique_ptr.182"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.182", %"class.std::__2::unique_ptr.182"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_node.180"** @_ZNKSt3__217__compressed_pairIPNS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEE5firstEv(%"class.std::__2::__compressed_pair.183"* %__ptr_) #9
  %0 = load %"class.std::__2::__tree_node.180"*, %"class.std::__2::__tree_node.180"** %call, align 4
  ret %"class.std::__2::__tree_node.180"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_node.180"* @_ZNSt3__210unique_ptrINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEE7releaseEv(%"class.std::__2::unique_ptr.182"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.182"*, align 4
  %__t = alloca %"class.std::__2::__tree_node.180"*, align 4
  store %"class.std::__2::unique_ptr.182"* %this, %"class.std::__2::unique_ptr.182"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.182"*, %"class.std::__2::unique_ptr.182"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.182", %"class.std::__2::unique_ptr.182"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_node.180"** @_ZNSt3__217__compressed_pairIPNS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEE5firstEv(%"class.std::__2::__compressed_pair.183"* %__ptr_) #9
  %0 = load %"class.std::__2::__tree_node.180"*, %"class.std::__2::__tree_node.180"** %call, align 4
  store %"class.std::__2::__tree_node.180"* %0, %"class.std::__2::__tree_node.180"** %__t, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr.182", %"class.std::__2::unique_ptr.182"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_node.180"** @_ZNSt3__217__compressed_pairIPNS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEE5firstEv(%"class.std::__2::__compressed_pair.183"* %__ptr_2) #9
  store %"class.std::__2::__tree_node.180"* null, %"class.std::__2::__tree_node.180"** %call3, align 4
  %1 = load %"class.std::__2::__tree_node.180"*, %"class.std::__2::__tree_node.180"** %__t, align 4
  ret %"class.std::__2::__tree_node.180"* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.182"* @_ZNSt3__210unique_ptrINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEED2Ev(%"class.std::__2::unique_ptr.182"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.182"*, align 4
  store %"class.std::__2::unique_ptr.182"* %this, %"class.std::__2::unique_ptr.182"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.182"*, %"class.std::__2::unique_ptr.182"** %this.addr, align 4
  call void @_ZNSt3__210unique_ptrINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEE5resetEPSB_(%"class.std::__2::unique_ptr.182"* %this1, %"class.std::__2::__tree_node.180"* null) #9
  ret %"class.std::__2::unique_ptr.182"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_iterator.179"* @_ZNSt3__215__tree_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElEC2ESC_(%"class.std::__2::__tree_iterator.179"* returned %this, %"class.std::__2::__tree_node.180"* %__p) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree_iterator.179"*, align 4
  %__p.addr = alloca %"class.std::__2::__tree_node.180"*, align 4
  store %"class.std::__2::__tree_iterator.179"* %this, %"class.std::__2::__tree_iterator.179"** %this.addr, align 4
  store %"class.std::__2::__tree_node.180"* %__p, %"class.std::__2::__tree_node.180"** %__p.addr, align 4
  %this1 = load %"class.std::__2::__tree_iterator.179"*, %"class.std::__2::__tree_iterator.179"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::__tree_iterator.179", %"class.std::__2::__tree_iterator.179"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::__tree_node.180"*, %"class.std::__2::__tree_node.180"** %__p.addr, align 4
  %1 = bitcast %"class.std::__2::__tree_node.180"* %0 to %"class.std::__2::__tree_end_node"*
  store %"class.std::__2::__tree_end_node"* %1, %"class.std::__2::__tree_end_node"** %__ptr_, align 4
  ret %"class.std::__2::__tree_iterator.179"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_end_node"* @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE3endEv(%"class.std::__2::__tree"* %this) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__tree_iterator.179", align 4
  %this.addr = alloca %"class.std::__2::__tree"*, align 4
  store %"class.std::__2::__tree"* %this, %"class.std::__2::__tree"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree"*, %"class.std::__2::__tree"** %this.addr, align 4
  %call = call %"class.std::__2::__tree_end_node"* @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE10__end_nodeEv(%"class.std::__2::__tree"* %this1) #9
  %call2 = call %"class.std::__2::__tree_iterator.179"* @_ZNSt3__215__tree_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElEC2EPNS_15__tree_end_nodeIPNS_16__tree_node_baseISA_EEEE(%"class.std::__2::__tree_iterator.179"* %retval, %"class.std::__2::__tree_end_node"* %call) #9
  %coerce.dive = getelementptr inbounds %"class.std::__2::__tree_iterator.179", %"class.std::__2::__tree_iterator.179"* %retval, i32 0, i32 0
  %0 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %coerce.dive, align 4
  ret %"class.std::__2::__tree_end_node"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_const_iterator"* @_ZNSt3__221__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElEC2ENS_15__tree_iteratorIS8_SC_lEE(%"class.std::__2::__tree_const_iterator"* returned %this, %"class.std::__2::__tree_end_node"* %__p.coerce) unnamed_addr #0 comdat {
entry:
  %__p = alloca %"class.std::__2::__tree_iterator.179", align 4
  %this.addr = alloca %"class.std::__2::__tree_const_iterator"*, align 4
  %coerce.dive = getelementptr inbounds %"class.std::__2::__tree_iterator.179", %"class.std::__2::__tree_iterator.179"* %__p, i32 0, i32 0
  store %"class.std::__2::__tree_end_node"* %__p.coerce, %"class.std::__2::__tree_end_node"** %coerce.dive, align 4
  store %"class.std::__2::__tree_const_iterator"* %this, %"class.std::__2::__tree_const_iterator"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree_const_iterator"*, %"class.std::__2::__tree_const_iterator"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %this1, i32 0, i32 0
  %__ptr_2 = getelementptr inbounds %"class.std::__2::__tree_iterator.179", %"class.std::__2::__tree_iterator.179"* %__p, i32 0, i32 0
  %0 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %__ptr_2, align 4
  store %"class.std::__2::__tree_end_node"* %0, %"class.std::__2::__tree_end_node"** %__ptr_, align 4
  ret %"class.std::__2::__tree_const_iterator"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::__map_value_compare"* @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE10value_compEv(%"class.std::__2::__tree"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree"*, align 4
  store %"class.std::__2::__tree"* %this, %"class.std::__2::__tree"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree"*, %"class.std::__2::__tree"** %this.addr, align 4
  %__pair3_ = getelementptr inbounds %"class.std::__2::__tree", %"class.std::__2::__tree"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::__map_value_compare"* @_ZNSt3__217__compressed_pairImNS_19__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS7_S7_EENS_4lessIS7_EELb1EEEE6secondEv(%"class.std::__2::__compressed_pair.119"* %__pair3_) #9
  ret %"class.std::__2::__map_value_compare"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__219__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS6_S6_EENS_4lessIS6_EELb1EEclERKS6_RKS8_(%"class.std::__2::__map_value_compare"* %this, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %__x, %"struct.std::__2::__value_type.181"* nonnull align 4 dereferenceable(24) %__y) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__map_value_compare"*, align 4
  %__x.addr = alloca %"class.std::__2::basic_string"*, align 4
  %__y.addr = alloca %"struct.std::__2::__value_type.181"*, align 4
  store %"class.std::__2::__map_value_compare"* %this, %"class.std::__2::__map_value_compare"** %this.addr, align 4
  store %"class.std::__2::basic_string"* %__x, %"class.std::__2::basic_string"** %__x.addr, align 4
  store %"struct.std::__2::__value_type.181"* %__y, %"struct.std::__2::__value_type.181"** %__y.addr, align 4
  %this1 = load %"class.std::__2::__map_value_compare"*, %"class.std::__2::__map_value_compare"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__map_value_compare"* %this1 to %"struct.std::__2::less"*
  %1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__x.addr, align 4
  %2 = load %"struct.std::__2::__value_type.181"*, %"struct.std::__2::__value_type.181"** %__y.addr, align 4
  %call = call nonnull align 4 dereferenceable(24) %"struct.std::__2::pair.177"* @_ZNKSt3__212__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_E11__get_valueEv(%"struct.std::__2::__value_type.181"* %2)
  %first = getelementptr inbounds %"struct.std::__2::pair.177", %"struct.std::__2::pair.177"* %call, i32 0, i32 0
  %call2 = call zeroext i1 @_ZNKSt3__24lessINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEclERKS6_S9_(%"struct.std::__2::less"* %0, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %1, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %first)
  ret i1 %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(24) %"struct.std::__2::__value_type.181"* @_ZNKSt3__221__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElEdeEv(%"class.std::__2::__tree_const_iterator"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree_const_iterator"*, align 4
  store %"class.std::__2::__tree_const_iterator"* %this, %"class.std::__2::__tree_const_iterator"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree_const_iterator"*, %"class.std::__2::__tree_const_iterator"** %this.addr, align 4
  %call = call %"class.std::__2::__tree_node.180"* @_ZNKSt3__221__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElE8__get_npEv(%"class.std::__2::__tree_const_iterator"* %this1)
  %__value_ = getelementptr inbounds %"class.std::__2::__tree_node.180", %"class.std::__2::__tree_node.180"* %call, i32 0, i32 1
  ret %"struct.std::__2::__value_type.181"* %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_end_node"* @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE5beginEv(%"class.std::__2::__tree"* %this) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__tree_iterator.179", align 4
  %this.addr = alloca %"class.std::__2::__tree"*, align 4
  store %"class.std::__2::__tree"* %this, %"class.std::__2::__tree"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree"*, %"class.std::__2::__tree"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_end_node"** @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE12__begin_nodeEv(%"class.std::__2::__tree"* %this1) #9
  %0 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %call, align 4
  %call2 = call %"class.std::__2::__tree_iterator.179"* @_ZNSt3__215__tree_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElEC2EPNS_15__tree_end_nodeIPNS_16__tree_node_baseISA_EEEE(%"class.std::__2::__tree_iterator.179"* %retval, %"class.std::__2::__tree_end_node"* %0) #9
  %coerce.dive = getelementptr inbounds %"class.std::__2::__tree_iterator.179", %"class.std::__2::__tree_iterator.179"* %retval, i32 0, i32 0
  %1 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %coerce.dive, align 4
  ret %"class.std::__2::__tree_end_node"* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__219__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS6_S6_EENS_4lessIS6_EELb1EEclERKS8_RKS6_(%"class.std::__2::__map_value_compare"* %this, %"struct.std::__2::__value_type.181"* nonnull align 4 dereferenceable(24) %__x, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %__y) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__map_value_compare"*, align 4
  %__x.addr = alloca %"struct.std::__2::__value_type.181"*, align 4
  %__y.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::__map_value_compare"* %this, %"class.std::__2::__map_value_compare"** %this.addr, align 4
  store %"struct.std::__2::__value_type.181"* %__x, %"struct.std::__2::__value_type.181"** %__x.addr, align 4
  store %"class.std::__2::basic_string"* %__y, %"class.std::__2::basic_string"** %__y.addr, align 4
  %this1 = load %"class.std::__2::__map_value_compare"*, %"class.std::__2::__map_value_compare"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__map_value_compare"* %this1 to %"struct.std::__2::less"*
  %1 = load %"struct.std::__2::__value_type.181"*, %"struct.std::__2::__value_type.181"** %__x.addr, align 4
  %call = call nonnull align 4 dereferenceable(24) %"struct.std::__2::pair.177"* @_ZNKSt3__212__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_E11__get_valueEv(%"struct.std::__2::__value_type.181"* %1)
  %first = getelementptr inbounds %"struct.std::__2::pair.177", %"struct.std::__2::pair.177"* %call, i32 0, i32 0
  %2 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__y.addr, align 4
  %call2 = call zeroext i1 @_ZNKSt3__24lessINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEclERKS6_S9_(%"struct.std::__2::less"* %0, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %first, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %2)
  ret i1 %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_const_iterator"* @_ZNSt3__221__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElEmmEv(%"class.std::__2::__tree_const_iterator"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree_const_iterator"*, align 4
  store %"class.std::__2::__tree_const_iterator"* %this, %"class.std::__2::__tree_const_iterator"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree_const_iterator"*, %"class.std::__2::__tree_const_iterator"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %__ptr_, align 4
  %call = call %"class.std::__2::__tree_node_base"* @_ZNSt3__216__tree_prev_iterIPNS_16__tree_node_baseIPvEEPNS_15__tree_end_nodeIS4_EEEET_T0_(%"class.std::__2::__tree_end_node"* %0) #9
  %1 = bitcast %"class.std::__2::__tree_node_base"* %call to %"class.std::__2::__tree_end_node"*
  %__ptr_2 = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %this1, i32 0, i32 0
  store %"class.std::__2::__tree_end_node"* %1, %"class.std::__2::__tree_end_node"** %__ptr_2, align 4
  ret %"class.std::__2::__tree_const_iterator"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_node_base"** @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE12__find_equalIS7_EERPNS_16__tree_node_baseIPvEERPNS_15__tree_end_nodeISJ_EERKT_(%"class.std::__2::__tree"* %this, %"class.std::__2::__tree_end_node"** nonnull align 4 dereferenceable(4) %__parent, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %__v) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__tree_node_base"**, align 4
  %this.addr = alloca %"class.std::__2::__tree"*, align 4
  %__parent.addr = alloca %"class.std::__2::__tree_end_node"**, align 4
  %__v.addr = alloca %"class.std::__2::basic_string"*, align 4
  %__nd = alloca %"class.std::__2::__tree_node.180"*, align 4
  %__nd_ptr = alloca %"class.std::__2::__tree_node_base"**, align 4
  store %"class.std::__2::__tree"* %this, %"class.std::__2::__tree"** %this.addr, align 4
  store %"class.std::__2::__tree_end_node"** %__parent, %"class.std::__2::__tree_end_node"*** %__parent.addr, align 4
  store %"class.std::__2::basic_string"* %__v, %"class.std::__2::basic_string"** %__v.addr, align 4
  %this1 = load %"class.std::__2::__tree"*, %"class.std::__2::__tree"** %this.addr, align 4
  %call = call %"class.std::__2::__tree_node.180"* @_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE6__rootEv(%"class.std::__2::__tree"* %this1) #9
  store %"class.std::__2::__tree_node.180"* %call, %"class.std::__2::__tree_node.180"** %__nd, align 4
  %call2 = call %"class.std::__2::__tree_node_base"** @_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE10__root_ptrEv(%"class.std::__2::__tree"* %this1) #9
  store %"class.std::__2::__tree_node_base"** %call2, %"class.std::__2::__tree_node_base"*** %__nd_ptr, align 4
  %0 = load %"class.std::__2::__tree_node.180"*, %"class.std::__2::__tree_node.180"** %__nd, align 4
  %cmp = icmp ne %"class.std::__2::__tree_node.180"* %0, null
  br i1 %cmp, label %if.then, label %if.end28

if.then:                                          ; preds = %entry
  br label %while.body

while.body:                                       ; preds = %if.then, %if.end27
  %call3 = call nonnull align 1 dereferenceable(1) %"class.std::__2::__map_value_compare"* @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE10value_compEv(%"class.std::__2::__tree"* %this1) #9
  %1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__v.addr, align 4
  %2 = load %"class.std::__2::__tree_node.180"*, %"class.std::__2::__tree_node.180"** %__nd, align 4
  %__value_ = getelementptr inbounds %"class.std::__2::__tree_node.180", %"class.std::__2::__tree_node.180"* %2, i32 0, i32 1
  %call4 = call zeroext i1 @_ZNKSt3__219__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS6_S6_EENS_4lessIS6_EELb1EEclERKS6_RKS8_(%"class.std::__2::__map_value_compare"* %call3, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %1, %"struct.std::__2::__value_type.181"* nonnull align 4 dereferenceable(24) %__value_)
  br i1 %call4, label %if.then5, label %if.else12

if.then5:                                         ; preds = %while.body
  %3 = load %"class.std::__2::__tree_node.180"*, %"class.std::__2::__tree_node.180"** %__nd, align 4
  %4 = bitcast %"class.std::__2::__tree_node.180"* %3 to %"class.std::__2::__tree_end_node"*
  %__left_ = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %4, i32 0, i32 0
  %5 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__left_, align 4
  %cmp6 = icmp ne %"class.std::__2::__tree_node_base"* %5, null
  br i1 %cmp6, label %if.then7, label %if.else

if.then7:                                         ; preds = %if.then5
  %6 = load %"class.std::__2::__tree_node.180"*, %"class.std::__2::__tree_node.180"** %__nd, align 4
  %7 = bitcast %"class.std::__2::__tree_node.180"* %6 to %"class.std::__2::__tree_end_node"*
  %__left_8 = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %7, i32 0, i32 0
  %call9 = call %"class.std::__2::__tree_node_base"** @_ZNSt3__29addressofIPNS_16__tree_node_baseIPvEEEEPT_RS5_(%"class.std::__2::__tree_node_base"** nonnull align 4 dereferenceable(4) %__left_8) #9
  store %"class.std::__2::__tree_node_base"** %call9, %"class.std::__2::__tree_node_base"*** %__nd_ptr, align 4
  %8 = load %"class.std::__2::__tree_node.180"*, %"class.std::__2::__tree_node.180"** %__nd, align 4
  %9 = bitcast %"class.std::__2::__tree_node.180"* %8 to %"class.std::__2::__tree_end_node"*
  %__left_10 = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %9, i32 0, i32 0
  %10 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__left_10, align 4
  %11 = bitcast %"class.std::__2::__tree_node_base"* %10 to %"class.std::__2::__tree_node.180"*
  store %"class.std::__2::__tree_node.180"* %11, %"class.std::__2::__tree_node.180"** %__nd, align 4
  br label %if.end

if.else:                                          ; preds = %if.then5
  %12 = load %"class.std::__2::__tree_node.180"*, %"class.std::__2::__tree_node.180"** %__nd, align 4
  %13 = bitcast %"class.std::__2::__tree_node.180"* %12 to %"class.std::__2::__tree_end_node"*
  %14 = load %"class.std::__2::__tree_end_node"**, %"class.std::__2::__tree_end_node"*** %__parent.addr, align 4
  store %"class.std::__2::__tree_end_node"* %13, %"class.std::__2::__tree_end_node"** %14, align 4
  %15 = load %"class.std::__2::__tree_end_node"**, %"class.std::__2::__tree_end_node"*** %__parent.addr, align 4
  %16 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %15, align 4
  %__left_11 = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %16, i32 0, i32 0
  store %"class.std::__2::__tree_node_base"** %__left_11, %"class.std::__2::__tree_node_base"*** %retval, align 4
  br label %return

if.end:                                           ; preds = %if.then7
  br label %if.end27

if.else12:                                        ; preds = %while.body
  %call13 = call nonnull align 1 dereferenceable(1) %"class.std::__2::__map_value_compare"* @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE10value_compEv(%"class.std::__2::__tree"* %this1) #9
  %17 = load %"class.std::__2::__tree_node.180"*, %"class.std::__2::__tree_node.180"** %__nd, align 4
  %__value_14 = getelementptr inbounds %"class.std::__2::__tree_node.180", %"class.std::__2::__tree_node.180"* %17, i32 0, i32 1
  %18 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__v.addr, align 4
  %call15 = call zeroext i1 @_ZNKSt3__219__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS6_S6_EENS_4lessIS6_EELb1EEclERKS8_RKS6_(%"class.std::__2::__map_value_compare"* %call13, %"struct.std::__2::__value_type.181"* nonnull align 4 dereferenceable(24) %__value_14, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %18)
  br i1 %call15, label %if.then16, label %if.else25

if.then16:                                        ; preds = %if.else12
  %19 = load %"class.std::__2::__tree_node.180"*, %"class.std::__2::__tree_node.180"** %__nd, align 4
  %20 = bitcast %"class.std::__2::__tree_node.180"* %19 to %"class.std::__2::__tree_node_base"*
  %__right_ = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %20, i32 0, i32 1
  %21 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__right_, align 4
  %cmp17 = icmp ne %"class.std::__2::__tree_node_base"* %21, null
  br i1 %cmp17, label %if.then18, label %if.else22

if.then18:                                        ; preds = %if.then16
  %22 = load %"class.std::__2::__tree_node.180"*, %"class.std::__2::__tree_node.180"** %__nd, align 4
  %23 = bitcast %"class.std::__2::__tree_node.180"* %22 to %"class.std::__2::__tree_node_base"*
  %__right_19 = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %23, i32 0, i32 1
  %call20 = call %"class.std::__2::__tree_node_base"** @_ZNSt3__29addressofIPNS_16__tree_node_baseIPvEEEEPT_RS5_(%"class.std::__2::__tree_node_base"** nonnull align 4 dereferenceable(4) %__right_19) #9
  store %"class.std::__2::__tree_node_base"** %call20, %"class.std::__2::__tree_node_base"*** %__nd_ptr, align 4
  %24 = load %"class.std::__2::__tree_node.180"*, %"class.std::__2::__tree_node.180"** %__nd, align 4
  %25 = bitcast %"class.std::__2::__tree_node.180"* %24 to %"class.std::__2::__tree_node_base"*
  %__right_21 = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %25, i32 0, i32 1
  %26 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__right_21, align 4
  %27 = bitcast %"class.std::__2::__tree_node_base"* %26 to %"class.std::__2::__tree_node.180"*
  store %"class.std::__2::__tree_node.180"* %27, %"class.std::__2::__tree_node.180"** %__nd, align 4
  br label %if.end24

if.else22:                                        ; preds = %if.then16
  %28 = load %"class.std::__2::__tree_node.180"*, %"class.std::__2::__tree_node.180"** %__nd, align 4
  %29 = bitcast %"class.std::__2::__tree_node.180"* %28 to %"class.std::__2::__tree_end_node"*
  %30 = load %"class.std::__2::__tree_end_node"**, %"class.std::__2::__tree_end_node"*** %__parent.addr, align 4
  store %"class.std::__2::__tree_end_node"* %29, %"class.std::__2::__tree_end_node"** %30, align 4
  %31 = load %"class.std::__2::__tree_node.180"*, %"class.std::__2::__tree_node.180"** %__nd, align 4
  %32 = bitcast %"class.std::__2::__tree_node.180"* %31 to %"class.std::__2::__tree_node_base"*
  %__right_23 = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %32, i32 0, i32 1
  store %"class.std::__2::__tree_node_base"** %__right_23, %"class.std::__2::__tree_node_base"*** %retval, align 4
  br label %return

if.end24:                                         ; preds = %if.then18
  br label %if.end26

if.else25:                                        ; preds = %if.else12
  %33 = load %"class.std::__2::__tree_node.180"*, %"class.std::__2::__tree_node.180"** %__nd, align 4
  %34 = bitcast %"class.std::__2::__tree_node.180"* %33 to %"class.std::__2::__tree_end_node"*
  %35 = load %"class.std::__2::__tree_end_node"**, %"class.std::__2::__tree_end_node"*** %__parent.addr, align 4
  store %"class.std::__2::__tree_end_node"* %34, %"class.std::__2::__tree_end_node"** %35, align 4
  %36 = load %"class.std::__2::__tree_node_base"**, %"class.std::__2::__tree_node_base"*** %__nd_ptr, align 4
  store %"class.std::__2::__tree_node_base"** %36, %"class.std::__2::__tree_node_base"*** %retval, align 4
  br label %return

if.end26:                                         ; preds = %if.end24
  br label %if.end27

if.end27:                                         ; preds = %if.end26, %if.end
  br label %while.body

if.end28:                                         ; preds = %entry
  %call29 = call %"class.std::__2::__tree_end_node"* @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE10__end_nodeEv(%"class.std::__2::__tree"* %this1) #9
  %37 = load %"class.std::__2::__tree_end_node"**, %"class.std::__2::__tree_end_node"*** %__parent.addr, align 4
  store %"class.std::__2::__tree_end_node"* %call29, %"class.std::__2::__tree_end_node"** %37, align 4
  %38 = load %"class.std::__2::__tree_end_node"**, %"class.std::__2::__tree_end_node"*** %__parent.addr, align 4
  %39 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %38, align 4
  %__left_30 = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %39, i32 0, i32 0
  store %"class.std::__2::__tree_node_base"** %__left_30, %"class.std::__2::__tree_node_base"*** %retval, align 4
  br label %return

return:                                           ; preds = %if.end28, %if.else25, %if.else22, %if.else
  %40 = load %"class.std::__2::__tree_node_base"**, %"class.std::__2::__tree_node_base"*** %retval, align 4
  ret %"class.std::__2::__tree_node_base"** %40
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_end_node"* @_ZNSt3__24nextINS_21__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPNS_11__tree_nodeIS9_PvEElEEEENS_9enable_ifIXsr25__is_cpp17_input_iteratorIT_EE5valueESG_E4typeESG_NS_15iterator_traitsISG_E15difference_typeE(%"class.std::__2::__tree_end_node"* %__x.coerce, i32 %__n) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__tree_const_iterator", align 4
  %__x = alloca %"class.std::__2::__tree_const_iterator", align 4
  %__n.addr = alloca i32, align 4
  %coerce.dive = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %__x, i32 0, i32 0
  store %"class.std::__2::__tree_end_node"* %__x.coerce, %"class.std::__2::__tree_end_node"** %coerce.dive, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__27advanceINS_21__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPNS_11__tree_nodeIS9_PvEElEEEEvRT_NS_15iterator_traitsISF_E15difference_typeE(%"class.std::__2::__tree_const_iterator"* nonnull align 4 dereferenceable(4) %__x, i32 %0)
  %1 = bitcast %"class.std::__2::__tree_const_iterator"* %retval to i8*
  %2 = bitcast %"class.std::__2::__tree_const_iterator"* %__x to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 4, i1 false)
  %coerce.dive1 = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %retval, i32 0, i32 0
  %3 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %coerce.dive1, align 4
  ret %"class.std::__2::__tree_end_node"* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_node.180"* @_ZNKSt3__221__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElE8__get_npEv(%"class.std::__2::__tree_const_iterator"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree_const_iterator"*, align 4
  store %"class.std::__2::__tree_const_iterator"* %this, %"class.std::__2::__tree_const_iterator"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree_const_iterator"*, %"class.std::__2::__tree_const_iterator"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %__ptr_, align 4
  %1 = bitcast %"class.std::__2::__tree_end_node"* %0 to %"class.std::__2::__tree_node.180"*
  ret %"class.std::__2::__tree_node.180"* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_iterator.179"* @_ZNSt3__215__tree_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElEC2EPNS_15__tree_end_nodeIPNS_16__tree_node_baseISA_EEEE(%"class.std::__2::__tree_iterator.179"* returned %this, %"class.std::__2::__tree_end_node"* %__p) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree_iterator.179"*, align 4
  %__p.addr = alloca %"class.std::__2::__tree_end_node"*, align 4
  store %"class.std::__2::__tree_iterator.179"* %this, %"class.std::__2::__tree_iterator.179"** %this.addr, align 4
  store %"class.std::__2::__tree_end_node"* %__p, %"class.std::__2::__tree_end_node"** %__p.addr, align 4
  %this1 = load %"class.std::__2::__tree_iterator.179"*, %"class.std::__2::__tree_iterator.179"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::__tree_iterator.179", %"class.std::__2::__tree_iterator.179"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %__p.addr, align 4
  store %"class.std::__2::__tree_end_node"* %0, %"class.std::__2::__tree_end_node"** %__ptr_, align 4
  ret %"class.std::__2::__tree_iterator.179"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::__map_value_compare"* @_ZNSt3__217__compressed_pairImNS_19__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS7_S7_EENS_4lessIS7_EELb1EEEE6secondEv(%"class.std::__2::__compressed_pair.119"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.119"*, align 4
  store %"class.std::__2::__compressed_pair.119"* %this, %"class.std::__2::__compressed_pair.119"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.119"*, %"class.std::__2::__compressed_pair.119"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.119"* %this1 to %"struct.std::__2::__compressed_pair_elem.120"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::__map_value_compare"* @_ZNSt3__222__compressed_pair_elemINS_19__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS7_S7_EENS_4lessIS7_EELb1EEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.120"* %0) #9
  ret %"class.std::__2::__map_value_compare"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::__map_value_compare"* @_ZNSt3__222__compressed_pair_elemINS_19__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS7_S7_EENS_4lessIS7_EELb1EEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.120"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.120"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.120"* %this, %"struct.std::__2::__compressed_pair_elem.120"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.120"*, %"struct.std::__2::__compressed_pair_elem.120"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.120"* %this1 to %"class.std::__2::__map_value_compare"*
  ret %"class.std::__2::__map_value_compare"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__24lessINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEclERKS6_S9_(%"struct.std::__2::less"* %this, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %__x, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %__y) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::less"*, align 4
  %__x.addr = alloca %"class.std::__2::basic_string"*, align 4
  %__y.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"struct.std::__2::less"* %this, %"struct.std::__2::less"** %this.addr, align 4
  store %"class.std::__2::basic_string"* %__x, %"class.std::__2::basic_string"** %__x.addr, align 4
  store %"class.std::__2::basic_string"* %__y, %"class.std::__2::basic_string"** %__y.addr, align 4
  %this1 = load %"struct.std::__2::less"*, %"struct.std::__2::less"** %this.addr, align 4
  %0 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__x.addr, align 4
  %1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__y.addr, align 4
  %call = call zeroext i1 @_ZNSt3__2ltIcNS_11char_traitsIcEENS_9allocatorIcEEEEbRKNS_12basic_stringIT_T0_T1_EESB_(%"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %0, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %1) #9
  ret i1 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(24) %"struct.std::__2::pair.177"* @_ZNKSt3__212__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_E11__get_valueEv(%"struct.std::__2::__value_type.181"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__value_type.181"*, align 4
  store %"struct.std::__2::__value_type.181"* %this, %"struct.std::__2::__value_type.181"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__value_type.181"*, %"struct.std::__2::__value_type.181"** %this.addr, align 4
  %__cc = getelementptr inbounds %"struct.std::__2::__value_type.181", %"struct.std::__2::__value_type.181"* %this1, i32 0, i32 0
  ret %"struct.std::__2::pair.177"* %__cc
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNSt3__2ltIcNS_11char_traitsIcEENS_9allocatorIcEEEEbRKNS_12basic_stringIT_T0_T1_EESB_(%"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %__lhs, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %__rhs) #0 comdat {
entry:
  %__lhs.addr = alloca %"class.std::__2::basic_string"*, align 4
  %__rhs.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %__lhs, %"class.std::__2::basic_string"** %__lhs.addr, align 4
  store %"class.std::__2::basic_string"* %__rhs, %"class.std::__2::basic_string"** %__rhs.addr, align 4
  %0 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__lhs.addr, align 4
  %1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__rhs.addr, align 4
  %call = call i32 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE7compareERKS5_(%"class.std::__2::basic_string"* %0, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %1) #9
  %cmp = icmp slt i32 %call, 0
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE7compareERKS5_(%"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %__str) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  %__str.addr = alloca %"class.std::__2::basic_string"*, align 4
  %ref.tmp = alloca %"class.std::__2::basic_string_view", align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  store %"class.std::__2::basic_string"* %__str, %"class.std::__2::basic_string"** %__str.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %0 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__str.addr, align 4
  call void @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEcvNS_17basic_string_viewIcS2_EEEv(%"class.std::__2::basic_string_view"* sret align 4 %ref.tmp, %"class.std::__2::basic_string"* %0) #9
  %call = call i32 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE7compareINS_17basic_string_viewIcS2_EEEENS_9enable_ifIXsr33__can_be_converted_to_string_viewIcS2_T_EE5valueEiE4typeERKSA_(%"class.std::__2::basic_string"* %this1, %"class.std::__2::basic_string_view"* nonnull align 4 dereferenceable(8) %ref.tmp)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr i32 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE7compareINS_17basic_string_viewIcS2_EEEENS_9enable_ifIXsr33__can_be_converted_to_string_viewIcS2_T_EE5valueEiE4typeERKSA_(%"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string_view"* nonnull align 4 dereferenceable(8) %__t) #0 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  %__t.addr = alloca %"class.std::__2::basic_string_view"*, align 4
  %__sv = alloca %"class.std::__2::basic_string_view", align 4
  %__lhs_sz = alloca i32, align 4
  %__rhs_sz = alloca i32, align 4
  %__result = alloca i32, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  store %"class.std::__2::basic_string_view"* %__t, %"class.std::__2::basic_string_view"** %__t.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %0 = load %"class.std::__2::basic_string_view"*, %"class.std::__2::basic_string_view"** %__t.addr, align 4
  %1 = bitcast %"class.std::__2::basic_string_view"* %__sv to i8*
  %2 = bitcast %"class.std::__2::basic_string_view"* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 8, i1 false)
  %call = call i32 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4sizeEv(%"class.std::__2::basic_string"* %this1) #9
  store i32 %call, i32* %__lhs_sz, align 4
  %call2 = call i32 @_ZNKSt3__217basic_string_viewIcNS_11char_traitsIcEEE4sizeEv(%"class.std::__2::basic_string_view"* %__sv) #9
  store i32 %call2, i32* %__rhs_sz, align 4
  %call3 = call i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv(%"class.std::__2::basic_string"* %this1) #9
  %call4 = call i8* @_ZNKSt3__217basic_string_viewIcNS_11char_traitsIcEEE4dataEv(%"class.std::__2::basic_string_view"* %__sv) #9
  %call5 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %__lhs_sz, i32* nonnull align 4 dereferenceable(4) %__rhs_sz)
  %3 = load i32, i32* %call5, align 4
  %call6 = call i32 @_ZNSt3__211char_traitsIcE7compareEPKcS3_m(i8* %call3, i8* %call4, i32 %3) #9
  store i32 %call6, i32* %__result, align 4
  %4 = load i32, i32* %__result, align 4
  %cmp = icmp ne i32 %4, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %5 = load i32, i32* %__result, align 4
  store i32 %5, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %6 = load i32, i32* %__lhs_sz, align 4
  %7 = load i32, i32* %__rhs_sz, align 4
  %cmp7 = icmp ult i32 %6, %7
  br i1 %cmp7, label %if.then8, label %if.end9

if.then8:                                         ; preds = %if.end
  store i32 -1, i32* %retval, align 4
  br label %return

if.end9:                                          ; preds = %if.end
  %8 = load i32, i32* %__lhs_sz, align 4
  %9 = load i32, i32* %__rhs_sz, align 4
  %cmp10 = icmp ugt i32 %8, %9
  br i1 %cmp10, label %if.then11, label %if.end12

if.then11:                                        ; preds = %if.end9
  store i32 1, i32* %retval, align 4
  br label %return

if.end12:                                         ; preds = %if.end9
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end12, %if.then11, %if.then8, %if.then
  %10 = load i32, i32* %retval, align 4
  ret i32 %10
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEcvNS_17basic_string_viewIcS2_EEEv(%"class.std::__2::basic_string_view"* noalias sret align 4 %agg.result, %"class.std::__2::basic_string"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %call = call i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv(%"class.std::__2::basic_string"* %this1) #9
  %call2 = call i32 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4sizeEv(%"class.std::__2::basic_string"* %this1) #9
  %call3 = call %"class.std::__2::basic_string_view"* @_ZNSt3__217basic_string_viewIcNS_11char_traitsIcEEEC2EPKcm(%"class.std::__2::basic_string_view"* %agg.result, i8* %call, i32 %call2) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4sizeEv(%"class.std::__2::basic_string"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %call = call zeroext i1 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE9__is_longEv(%"class.std::__2::basic_string"* %this1) #9
  br i1 %call, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %call2 = call i32 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE15__get_long_sizeEv(%"class.std::__2::basic_string"* %this1) #9
  br label %cond.end

cond.false:                                       ; preds = %entry
  %call3 = call i32 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE16__get_short_sizeEv(%"class.std::__2::basic_string"* %this1) #9
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %call2, %cond.true ], [ %call3, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__217basic_string_viewIcNS_11char_traitsIcEEE4sizeEv(%"class.std::__2::basic_string_view"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string_view"*, align 4
  store %"class.std::__2::basic_string_view"* %this, %"class.std::__2::basic_string_view"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string_view"*, %"class.std::__2::basic_string_view"** %this.addr, align 4
  %__size = getelementptr inbounds %"class.std::__2::basic_string_view", %"class.std::__2::basic_string_view"* %this1, i32 0, i32 1
  %0 = load i32, i32* %__size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__211char_traitsIcE7compareEPKcS3_m(i8* %__s1, i8* %__s2, i32 %__n) #0 comdat {
entry:
  %retval = alloca i32, align 4
  %__s1.addr = alloca i8*, align 4
  %__s2.addr = alloca i8*, align 4
  %__n.addr = alloca i32, align 4
  store i8* %__s1, i8** %__s1.addr, align 4
  store i8* %__s2, i8** %__s2.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %cmp = icmp eq i32 %0, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load i8*, i8** %__s1.addr, align 4
  %2 = load i8*, i8** %__s2.addr, align 4
  %3 = load i32, i32* %__n.addr, align 4
  %call = call i32 @memcmp(i8* %1, i8* %2, i32 %3) #9
  store i32 %call, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %4 = load i32, i32* %retval, align 4
  ret i32 %4
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv(%"class.std::__2::basic_string"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %call = call i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE13__get_pointerEv(%"class.std::__2::basic_string"* %this1) #9
  %call2 = call i8* @_ZNSt3__212__to_addressIKcEEPT_S3_(i8* %call) #9
  ret i8* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__217basic_string_viewIcNS_11char_traitsIcEEE4dataEv(%"class.std::__2::basic_string_view"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string_view"*, align 4
  store %"class.std::__2::basic_string_view"* %this, %"class.std::__2::basic_string_view"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string_view"*, %"class.std::__2::basic_string_view"** %this.addr, align 4
  %__data = getelementptr inbounds %"class.std::__2::basic_string_view", %"class.std::__2::basic_string_view"* %this1, i32 0, i32 0
  %0 = load i8*, i8** %__data, align 4
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::__less", align 1
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__a.addr, align 4
  %1 = load i32*, i32** %__b.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE9__is_longEv(%"class.std::__2::basic_string"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair"* %__r_) #9
  %0 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call, i32 0, i32 0
  %__s = bitcast %union.anon* %0 to %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short"*
  %1 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short"* %__s, i32 0, i32 1
  %__size_ = getelementptr inbounds %struct.anon, %struct.anon* %1, i32 0, i32 0
  %2 = load i8, i8* %__size_, align 1
  %conv = zext i8 %2 to i32
  %and = and i32 %conv, 128
  %tobool = icmp ne i32 %and, 0
  ret i1 %tobool
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE15__get_long_sizeEv(%"class.std::__2::basic_string"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair"* %__r_) #9
  %0 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call, i32 0, i32 0
  %__l = bitcast %union.anon* %0 to %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long"*
  %__size_ = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long"* %__l, i32 0, i32 1
  %1 = load i32, i32* %__size_, align 4
  ret i32 %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE16__get_short_sizeEv(%"class.std::__2::basic_string"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair"* %__r_) #9
  %0 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call, i32 0, i32 0
  %__s = bitcast %union.anon* %0 to %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short"*
  %1 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short"* %__s, i32 0, i32 1
  %__size_ = getelementptr inbounds %struct.anon, %struct.anon* %1, i32 0, i32 0
  %2 = load i8, i8* %__size_, align 1
  %conv = zext i8 %2 to i32
  ret i32 %conv
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %0) #9
  ret %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem"*, align 4
  store %"struct.std::__2::__compressed_pair_elem"* %this, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem"*, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem"* %this1, i32 0, i32 0
  ret %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %__value_
}

; Function Attrs: nounwind
declare i32 @memcmp(i8*, i8*, i32) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__212__to_addressIKcEEPT_S3_(i8* %__p) #0 comdat {
entry:
  %__p.addr = alloca i8*, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %0 = load i8*, i8** %__p.addr, align 4
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE13__get_pointerEv(%"class.std::__2::basic_string"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %call = call zeroext i1 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE9__is_longEv(%"class.std::__2::basic_string"* %this1) #9
  br i1 %call, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %call2 = call i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE18__get_long_pointerEv(%"class.std::__2::basic_string"* %this1) #9
  br label %cond.end

cond.false:                                       ; preds = %entry
  %call3 = call i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE19__get_short_pointerEv(%"class.std::__2::basic_string"* %this1) #9
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i8* [ %call2, %cond.true ], [ %call3, %cond.false ]
  ret i8* %cond
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE18__get_long_pointerEv(%"class.std::__2::basic_string"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair"* %__r_) #9
  %0 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call, i32 0, i32 0
  %__l = bitcast %union.anon* %0 to %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long"*
  %__data_ = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long"* %__l, i32 0, i32 0
  %1 = load i8*, i8** %__data_, align 4
  ret i8* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE19__get_short_pointerEv(%"class.std::__2::basic_string"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair"* %__r_) #9
  %0 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call, i32 0, i32 0
  %__s = bitcast %union.anon* %0 to %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short"*
  %__data_ = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short"* %__s, i32 0, i32 0
  %arrayidx = getelementptr inbounds [11 x i8], [11 x i8]* %__data_, i32 0, i32 0
  %call2 = call i8* @_ZNSt3__214pointer_traitsIPKcE10pointer_toERS1_(i8* nonnull align 1 dereferenceable(1) %arrayidx) #9
  ret i8* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__214pointer_traitsIPKcE10pointer_toERS1_(i8* nonnull align 1 dereferenceable(1) %__r) #0 comdat {
entry:
  %__r.addr = alloca i8*, align 4
  store i8* %__r, i8** %__r.addr, align 4
  %0 = load i8*, i8** %__r.addr, align 4
  %call = call i8* @_ZNSt3__29addressofIKcEEPT_RS2_(i8* nonnull align 1 dereferenceable(1) %0) #9
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__29addressofIKcEEPT_RS2_(i8* nonnull align 1 dereferenceable(1) %__x) #0 comdat {
entry:
  %__x.addr = alloca i8*, align 4
  store i8* %__x, i8** %__x.addr, align 4
  %0 = load i8*, i8** %__x.addr, align 4
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__comp = alloca %"struct.std::__2::__less", align 1
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__b.addr, align 4
  %1 = load i32*, i32** %__a.addr, align 4
  %call = call zeroext i1 @_ZNKSt3__26__lessImmEclERKmS3_(%"struct.std::__2::__less"* %__comp, i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  br i1 %call, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32*, i32** %__b.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %3 = load i32*, i32** %__a.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi i32* [ %2, %cond.true ], [ %3, %cond.false ]
  ret i32* %cond-lvalue
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__26__lessImmEclERKmS3_(%"struct.std::__2::__less"* %this, i32* nonnull align 4 dereferenceable(4) %__x, i32* nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__less"*, align 4
  %__x.addr = alloca i32*, align 4
  %__y.addr = alloca i32*, align 4
  store %"struct.std::__2::__less"* %this, %"struct.std::__2::__less"** %this.addr, align 4
  store i32* %__x, i32** %__x.addr, align 4
  store i32* %__y, i32** %__y.addr, align 4
  %this1 = load %"struct.std::__2::__less"*, %"struct.std::__2::__less"** %this.addr, align 4
  %0 = load i32*, i32** %__x.addr, align 4
  %1 = load i32, i32* %0, align 4
  %2 = load i32*, i32** %__y.addr, align 4
  %3 = load i32, i32* %2, align 4
  %cmp = icmp ult i32 %1, %3
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::basic_string_view"* @_ZNSt3__217basic_string_viewIcNS_11char_traitsIcEEEC2EPKcm(%"class.std::__2::basic_string_view"* returned %this, i8* %__s, i32 %__len) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string_view"*, align 4
  %__s.addr = alloca i8*, align 4
  %__len.addr = alloca i32, align 4
  store %"class.std::__2::basic_string_view"* %this, %"class.std::__2::basic_string_view"** %this.addr, align 4
  store i8* %__s, i8** %__s.addr, align 4
  store i32 %__len, i32* %__len.addr, align 4
  %this1 = load %"class.std::__2::basic_string_view"*, %"class.std::__2::basic_string_view"** %this.addr, align 4
  %__data = getelementptr inbounds %"class.std::__2::basic_string_view", %"class.std::__2::basic_string_view"* %this1, i32 0, i32 0
  %0 = load i8*, i8** %__s.addr, align 4
  store i8* %0, i8** %__data, align 4
  %__size = getelementptr inbounds %"class.std::__2::basic_string_view", %"class.std::__2::basic_string_view"* %this1, i32 0, i32 1
  %1 = load i32, i32* %__len.addr, align 4
  store i32 %1, i32* %__size, align 4
  ret %"class.std::__2::basic_string_view"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_node_base"* @_ZNSt3__216__tree_prev_iterIPNS_16__tree_node_baseIPvEEPNS_15__tree_end_nodeIS4_EEEET_T0_(%"class.std::__2::__tree_end_node"* %__x) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__tree_node_base"*, align 4
  %__x.addr = alloca %"class.std::__2::__tree_end_node"*, align 4
  %__xx = alloca %"class.std::__2::__tree_node_base"*, align 4
  store %"class.std::__2::__tree_end_node"* %__x, %"class.std::__2::__tree_end_node"** %__x.addr, align 4
  %0 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %__x.addr, align 4
  %__left_ = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %0, i32 0, i32 0
  %1 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__left_, align 4
  %cmp = icmp ne %"class.std::__2::__tree_node_base"* %1, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %__x.addr, align 4
  %__left_1 = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %2, i32 0, i32 0
  %3 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__left_1, align 4
  %call = call %"class.std::__2::__tree_node_base"* @_ZNSt3__210__tree_maxIPNS_16__tree_node_baseIPvEEEET_S5_(%"class.std::__2::__tree_node_base"* %3) #9
  store %"class.std::__2::__tree_node_base"* %call, %"class.std::__2::__tree_node_base"** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %4 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %__x.addr, align 4
  %5 = bitcast %"class.std::__2::__tree_end_node"* %4 to %"class.std::__2::__tree_node_base"*
  store %"class.std::__2::__tree_node_base"* %5, %"class.std::__2::__tree_node_base"** %__xx, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.end
  %6 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__xx, align 4
  %call2 = call zeroext i1 @_ZNSt3__220__tree_is_left_childIPNS_16__tree_node_baseIPvEEEEbT_(%"class.std::__2::__tree_node_base"* %6) #9
  br i1 %call2, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %7 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__xx, align 4
  %call3 = call %"class.std::__2::__tree_node_base"* @_ZNKSt3__216__tree_node_baseIPvE15__parent_unsafeEv(%"class.std::__2::__tree_node_base"* %7)
  store %"class.std::__2::__tree_node_base"* %call3, %"class.std::__2::__tree_node_base"** %__xx, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %8 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__xx, align 4
  %call4 = call %"class.std::__2::__tree_node_base"* @_ZNKSt3__216__tree_node_baseIPvE15__parent_unsafeEv(%"class.std::__2::__tree_node_base"* %8)
  store %"class.std::__2::__tree_node_base"* %call4, %"class.std::__2::__tree_node_base"** %retval, align 4
  br label %return

return:                                           ; preds = %while.end, %if.then
  %9 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %retval, align 4
  ret %"class.std::__2::__tree_node_base"* %9
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_node_base"* @_ZNSt3__210__tree_maxIPNS_16__tree_node_baseIPvEEEET_S5_(%"class.std::__2::__tree_node_base"* %__x) #0 comdat {
entry:
  %__x.addr = alloca %"class.std::__2::__tree_node_base"*, align 4
  store %"class.std::__2::__tree_node_base"* %__x, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %0 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %__right_ = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %0, i32 0, i32 1
  %1 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__right_, align 4
  %cmp = icmp ne %"class.std::__2::__tree_node_base"* %1, null
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %2 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %__right_1 = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %2, i32 0, i32 1
  %3 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__right_1, align 4
  store %"class.std::__2::__tree_node_base"* %3, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %4 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  ret %"class.std::__2::__tree_node_base"* %4
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_node.180"* @_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE6__rootEv(%"class.std::__2::__tree"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree"*, align 4
  store %"class.std::__2::__tree"* %this, %"class.std::__2::__tree"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree"*, %"class.std::__2::__tree"** %this.addr, align 4
  %call = call %"class.std::__2::__tree_end_node"* @_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE10__end_nodeEv(%"class.std::__2::__tree"* %this1) #9
  %__left_ = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %call, i32 0, i32 0
  %0 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__left_, align 4
  %1 = bitcast %"class.std::__2::__tree_node_base"* %0 to %"class.std::__2::__tree_node.180"*
  ret %"class.std::__2::__tree_node.180"* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_node_base"** @_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE10__root_ptrEv(%"class.std::__2::__tree"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree"*, align 4
  store %"class.std::__2::__tree"* %this, %"class.std::__2::__tree"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree"*, %"class.std::__2::__tree"** %this.addr, align 4
  %call = call %"class.std::__2::__tree_end_node"* @_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE10__end_nodeEv(%"class.std::__2::__tree"* %this1) #9
  %__left_ = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %call, i32 0, i32 0
  %call2 = call %"class.std::__2::__tree_node_base"** @_ZNSt3__29addressofIPNS_16__tree_node_baseIPvEEEEPT_RS5_(%"class.std::__2::__tree_node_base"** nonnull align 4 dereferenceable(4) %__left_) #9
  ret %"class.std::__2::__tree_node_base"** %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_end_node"* @_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE10__end_nodeEv(%"class.std::__2::__tree"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree"*, align 4
  store %"class.std::__2::__tree"* %this, %"class.std::__2::__tree"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree"*, %"class.std::__2::__tree"** %this.addr, align 4
  %__pair1_ = getelementptr inbounds %"class.std::__2::__tree", %"class.std::__2::__tree"* %this1, i32 0, i32 1
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_end_node"* @_ZNKSt3__217__compressed_pairINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEENS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS7_IcEEEESE_EES3_EEEEE5firstEv(%"class.std::__2::__compressed_pair.114"* %__pair1_) #9
  %call2 = call %"class.std::__2::__tree_end_node"* @_ZNSt3__214pointer_traitsIPNS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEEE10pointer_toERS6_(%"class.std::__2::__tree_end_node"* nonnull align 4 dereferenceable(4) %call) #9
  ret %"class.std::__2::__tree_end_node"* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_end_node"* @_ZNKSt3__217__compressed_pairINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEENS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS7_IcEEEESE_EES3_EEEEE5firstEv(%"class.std::__2::__compressed_pair.114"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.114"*, align 4
  store %"class.std::__2::__compressed_pair.114"* %this, %"class.std::__2::__compressed_pair.114"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.114"*, %"class.std::__2::__compressed_pair.114"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.114"* %this1 to %"struct.std::__2::__compressed_pair_elem.115"*
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_end_node"* @_ZNKSt3__222__compressed_pair_elemINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.115"* %0) #9
  ret %"class.std::__2::__tree_end_node"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__27advanceINS_21__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPNS_11__tree_nodeIS9_PvEElEEEEvRT_NS_15iterator_traitsISF_E15difference_typeE(%"class.std::__2::__tree_const_iterator"* nonnull align 4 dereferenceable(4) %__i, i32 %__n) #0 comdat {
entry:
  %__i.addr = alloca %"class.std::__2::__tree_const_iterator"*, align 4
  %__n.addr = alloca i32, align 4
  %agg.tmp = alloca %"struct.std::__2::bidirectional_iterator_tag", align 1
  store %"class.std::__2::__tree_const_iterator"* %__i, %"class.std::__2::__tree_const_iterator"** %__i.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::__tree_const_iterator"*, %"class.std::__2::__tree_const_iterator"** %__i.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29__advanceINS_21__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPNS_11__tree_nodeIS9_PvEElEEEEvRT_NS_15iterator_traitsISF_E15difference_typeENS_26bidirectional_iterator_tagE(%"class.std::__2::__tree_const_iterator"* nonnull align 4 dereferenceable(4) %0, i32 %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29__advanceINS_21__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPNS_11__tree_nodeIS9_PvEElEEEEvRT_NS_15iterator_traitsISF_E15difference_typeENS_26bidirectional_iterator_tagE(%"class.std::__2::__tree_const_iterator"* nonnull align 4 dereferenceable(4) %__i, i32 %__n) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::bidirectional_iterator_tag", align 1
  %__i.addr = alloca %"class.std::__2::__tree_const_iterator"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::__tree_const_iterator"* %__i, %"class.std::__2::__tree_const_iterator"** %__i.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %cmp = icmp sge i32 %1, 0
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %2 = load i32, i32* %__n.addr, align 4
  %cmp1 = icmp sgt i32 %2, 0
  br i1 %cmp1, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %"class.std::__2::__tree_const_iterator"*, %"class.std::__2::__tree_const_iterator"** %__i.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_const_iterator"* @_ZNSt3__221__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElEppEv(%"class.std::__2::__tree_const_iterator"* %3)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %4 = load i32, i32* %__n.addr, align 4
  %dec = add nsw i32 %4, -1
  store i32 %dec, i32* %__n.addr, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end

if.else:                                          ; preds = %entry
  br label %for.cond2

for.cond2:                                        ; preds = %for.inc6, %if.else
  %5 = load i32, i32* %__n.addr, align 4
  %cmp3 = icmp slt i32 %5, 0
  br i1 %cmp3, label %for.body4, label %for.end7

for.body4:                                        ; preds = %for.cond2
  %6 = load %"class.std::__2::__tree_const_iterator"*, %"class.std::__2::__tree_const_iterator"** %__i.addr, align 4
  %call5 = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_const_iterator"* @_ZNSt3__221__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElEmmEv(%"class.std::__2::__tree_const_iterator"* %6)
  br label %for.inc6

for.inc6:                                         ; preds = %for.body4
  %7 = load i32, i32* %__n.addr, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %__n.addr, align 4
  br label %for.cond2

for.end7:                                         ; preds = %for.cond2
  br label %if.end

if.end:                                           ; preds = %for.end7, %for.end
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_const_iterator"* @_ZNSt3__221__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElEppEv(%"class.std::__2::__tree_const_iterator"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree_const_iterator"*, align 4
  store %"class.std::__2::__tree_const_iterator"* %this, %"class.std::__2::__tree_const_iterator"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree_const_iterator"*, %"class.std::__2::__tree_const_iterator"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %__ptr_, align 4
  %1 = bitcast %"class.std::__2::__tree_end_node"* %0 to %"class.std::__2::__tree_node_base"*
  %call = call %"class.std::__2::__tree_end_node"* @_ZNSt3__216__tree_next_iterIPNS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEES5_EET_T0_(%"class.std::__2::__tree_node_base"* %1) #9
  %__ptr_2 = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %this1, i32 0, i32 0
  store %"class.std::__2::__tree_end_node"* %call, %"class.std::__2::__tree_end_node"** %__ptr_2, align 4
  ret %"class.std::__2::__tree_const_iterator"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_end_node"* @_ZNSt3__216__tree_next_iterIPNS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEES5_EET_T0_(%"class.std::__2::__tree_node_base"* %__x) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__tree_end_node"*, align 4
  %__x.addr = alloca %"class.std::__2::__tree_node_base"*, align 4
  store %"class.std::__2::__tree_node_base"* %__x, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %0 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %__right_ = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %0, i32 0, i32 1
  %1 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__right_, align 4
  %cmp = icmp ne %"class.std::__2::__tree_node_base"* %1, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %__right_1 = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %2, i32 0, i32 1
  %3 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__right_1, align 4
  %call = call %"class.std::__2::__tree_node_base"* @_ZNSt3__210__tree_minIPNS_16__tree_node_baseIPvEEEET_S5_(%"class.std::__2::__tree_node_base"* %3) #9
  %4 = bitcast %"class.std::__2::__tree_node_base"* %call to %"class.std::__2::__tree_end_node"*
  store %"class.std::__2::__tree_end_node"* %4, %"class.std::__2::__tree_end_node"** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.end
  %5 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %call2 = call zeroext i1 @_ZNSt3__220__tree_is_left_childIPNS_16__tree_node_baseIPvEEEEbT_(%"class.std::__2::__tree_node_base"* %5) #9
  %lnot = xor i1 %call2, true
  br i1 %lnot, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %6 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %call3 = call %"class.std::__2::__tree_node_base"* @_ZNKSt3__216__tree_node_baseIPvE15__parent_unsafeEv(%"class.std::__2::__tree_node_base"* %6)
  store %"class.std::__2::__tree_node_base"* %call3, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %7 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %__parent_ = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %7, i32 0, i32 2
  %8 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %__parent_, align 4
  store %"class.std::__2::__tree_end_node"* %8, %"class.std::__2::__tree_end_node"** %retval, align 4
  br label %return

return:                                           ; preds = %while.end, %if.then
  %9 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %retval, align 4
  ret %"class.std::__2::__tree_end_node"* %9
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_node_base"* @_ZNSt3__210__tree_minIPNS_16__tree_node_baseIPvEEEET_S5_(%"class.std::__2::__tree_node_base"* %__x) #0 comdat {
entry:
  %__x.addr = alloca %"class.std::__2::__tree_node_base"*, align 4
  store %"class.std::__2::__tree_node_base"* %__x, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %0 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %1 = bitcast %"class.std::__2::__tree_node_base"* %0 to %"class.std::__2::__tree_end_node"*
  %__left_ = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %1, i32 0, i32 0
  %2 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__left_, align 4
  %cmp = icmp ne %"class.std::__2::__tree_node_base"* %2, null
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %3 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %4 = bitcast %"class.std::__2::__tree_node_base"* %3 to %"class.std::__2::__tree_end_node"*
  %__left_1 = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %4, i32 0, i32 0
  %5 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__left_1, align 4
  store %"class.std::__2::__tree_node_base"* %5, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %6 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  ret %"class.std::__2::__tree_node_base"* %6
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.117"* @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE12__node_allocEv(%"class.std::__2::__tree"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree"*, align 4
  store %"class.std::__2::__tree"* %this, %"class.std::__2::__tree"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree"*, %"class.std::__2::__tree"** %this.addr, align 4
  %__pair1_ = getelementptr inbounds %"class.std::__2::__tree", %"class.std::__2::__tree"* %this1, i32 0, i32 1
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.117"* @_ZNSt3__217__compressed_pairINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEENS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS7_IcEEEESE_EES3_EEEEE6secondEv(%"class.std::__2::__compressed_pair.114"* %__pair1_) #9
  ret %"class.std::__2::allocator.117"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_node.180"* @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEEE8allocateERSC_m(%"class.std::__2::allocator.117"* nonnull align 1 dereferenceable(1) %__a, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.117"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.117"* %__a, %"class.std::__2::allocator.117"** %__a.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.117"*, %"class.std::__2::allocator.117"** %__a.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call %"class.std::__2::__tree_node.180"* @_ZNSt3__29allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEES7_EEPvEEE8allocateEmPKv(%"class.std::__2::allocator.117"* %0, i32 %1, i8* null)
  ret %"class.std::__2::__tree_node.180"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_node_destructor.186"* @_ZNSt3__222__tree_node_destructorINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEEEC2ERSC_b(%"class.std::__2::__tree_node_destructor.186"* returned %this, %"class.std::__2::allocator.117"* nonnull align 1 dereferenceable(1) %__na, i1 zeroext %__val) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree_node_destructor.186"*, align 4
  %__na.addr = alloca %"class.std::__2::allocator.117"*, align 4
  %__val.addr = alloca i8, align 1
  store %"class.std::__2::__tree_node_destructor.186"* %this, %"class.std::__2::__tree_node_destructor.186"** %this.addr, align 4
  store %"class.std::__2::allocator.117"* %__na, %"class.std::__2::allocator.117"** %__na.addr, align 4
  %frombool = zext i1 %__val to i8
  store i8 %frombool, i8* %__val.addr, align 1
  %this1 = load %"class.std::__2::__tree_node_destructor.186"*, %"class.std::__2::__tree_node_destructor.186"** %this.addr, align 4
  %__na_ = getelementptr inbounds %"class.std::__2::__tree_node_destructor.186", %"class.std::__2::__tree_node_destructor.186"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::allocator.117"*, %"class.std::__2::allocator.117"** %__na.addr, align 4
  store %"class.std::__2::allocator.117"* %0, %"class.std::__2::allocator.117"** %__na_, align 4
  %__value_constructed = getelementptr inbounds %"class.std::__2::__tree_node_destructor.186", %"class.std::__2::__tree_node_destructor.186"* %this1, i32 0, i32 1
  %1 = load i8, i8* %__val.addr, align 1
  %tobool = trunc i8 %1 to i1
  %frombool2 = zext i1 %tobool to i8
  store i8 %frombool2, i8* %__value_constructed, align 4
  ret %"class.std::__2::__tree_node_destructor.186"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.182"* @_ZNSt3__210unique_ptrINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEEC2ILb1EvEEPSB_NS_16__dependent_typeINS_27__unique_ptr_deleter_sfinaeISE_EEXT_EE20__good_rval_ref_typeE(%"class.std::__2::unique_ptr.182"* returned %this, %"class.std::__2::__tree_node.180"* %__p, %"class.std::__2::__tree_node_destructor.186"* nonnull align 4 dereferenceable(5) %__d) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.182"*, align 4
  %__p.addr = alloca %"class.std::__2::__tree_node.180"*, align 4
  %__d.addr = alloca %"class.std::__2::__tree_node_destructor.186"*, align 4
  store %"class.std::__2::unique_ptr.182"* %this, %"class.std::__2::unique_ptr.182"** %this.addr, align 4
  store %"class.std::__2::__tree_node.180"* %__p, %"class.std::__2::__tree_node.180"** %__p.addr, align 4
  store %"class.std::__2::__tree_node_destructor.186"* %__d, %"class.std::__2::__tree_node_destructor.186"** %__d.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.182"*, %"class.std::__2::unique_ptr.182"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.182", %"class.std::__2::unique_ptr.182"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::__tree_node_destructor.186"*, %"class.std::__2::__tree_node_destructor.186"** %__d.addr, align 4
  %call = call nonnull align 4 dereferenceable(5) %"class.std::__2::__tree_node_destructor.186"* @_ZNSt3__24moveIRNS_22__tree_node_destructorINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS2_IcEEEES9_EEPvEEEEEEEEONS_16remove_referenceIT_E4typeEOSH_(%"class.std::__2::__tree_node_destructor.186"* nonnull align 4 dereferenceable(5) %0) #9
  %call2 = call %"class.std::__2::__compressed_pair.183"* @_ZNSt3__217__compressed_pairIPNS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEEC2IRSC_SF_EEOT_OT0_(%"class.std::__2::__compressed_pair.183"* %__ptr_, %"class.std::__2::__tree_node.180"** nonnull align 4 dereferenceable(4) %__p.addr, %"class.std::__2::__tree_node_destructor.186"* nonnull align 4 dereferenceable(5) %call)
  ret %"class.std::__2::unique_ptr.182"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEEE9constructINS_4pairIKS8_S8_EEJRKSH_EEEvRSC_PT_DpOT0_(%"class.std::__2::allocator.117"* nonnull align 1 dereferenceable(1) %__a, %"struct.std::__2::pair.177"* %__p, %"struct.std::__2::pair.177"* nonnull align 4 dereferenceable(24) %__args) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.117"*, align 4
  %__p.addr = alloca %"struct.std::__2::pair.177"*, align 4
  %__args.addr = alloca %"struct.std::__2::pair.177"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_construct.188", align 1
  store %"class.std::__2::allocator.117"* %__a, %"class.std::__2::allocator.117"** %__a.addr, align 4
  store %"struct.std::__2::pair.177"* %__p, %"struct.std::__2::pair.177"** %__p.addr, align 4
  store %"struct.std::__2::pair.177"* %__args, %"struct.std::__2::pair.177"** %__args.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_construct.188"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.117"*, %"class.std::__2::allocator.117"** %__a.addr, align 4
  %2 = load %"struct.std::__2::pair.177"*, %"struct.std::__2::pair.177"** %__p.addr, align 4
  %3 = load %"struct.std::__2::pair.177"*, %"struct.std::__2::pair.177"** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(24) %"struct.std::__2::pair.177"* @_ZNSt3__27forwardIRKNS_4pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEEEOT_RNS_16remove_referenceISC_E4typeE(%"struct.std::__2::pair.177"* nonnull align 4 dereferenceable(24) %3) #9
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEEE11__constructINS_4pairIKS8_S8_EEJRKSH_EEEvNS_17integral_constantIbLb1EEERSC_PT_DpOT0_(%"class.std::__2::allocator.117"* nonnull align 1 dereferenceable(1) %1, %"struct.std::__2::pair.177"* %2, %"struct.std::__2::pair.177"* nonnull align 4 dereferenceable(24) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::pair.177"* @_ZNSt3__222__tree_key_value_typesINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEE9__get_ptrERS8_(%"struct.std::__2::__value_type.181"* nonnull align 4 dereferenceable(24) %__n) #0 comdat {
entry:
  %__n.addr = alloca %"struct.std::__2::__value_type.181"*, align 4
  store %"struct.std::__2::__value_type.181"* %__n, %"struct.std::__2::__value_type.181"** %__n.addr, align 4
  %0 = load %"struct.std::__2::__value_type.181"*, %"struct.std::__2::__value_type.181"** %__n.addr, align 4
  %call = call nonnull align 4 dereferenceable(24) %"struct.std::__2::pair.177"* @_ZNSt3__212__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_E11__get_valueEv(%"struct.std::__2::__value_type.181"* %0)
  %call1 = call %"struct.std::__2::pair.177"* @_ZNSt3__29addressofINS_4pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEEEPT_RSA_(%"struct.std::__2::pair.177"* nonnull align 4 dereferenceable(24) %call) #9
  ret %"struct.std::__2::pair.177"* %call1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_node.180"* @_ZNKSt3__210unique_ptrINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEEptEv(%"class.std::__2::unique_ptr.182"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.182"*, align 4
  store %"class.std::__2::unique_ptr.182"* %this, %"class.std::__2::unique_ptr.182"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.182"*, %"class.std::__2::unique_ptr.182"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.182", %"class.std::__2::unique_ptr.182"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_node.180"** @_ZNKSt3__217__compressed_pairIPNS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEE5firstEv(%"class.std::__2::__compressed_pair.183"* %__ptr_) #9
  %0 = load %"class.std::__2::__tree_node.180"*, %"class.std::__2::__tree_node.180"** %call, align 4
  ret %"class.std::__2::__tree_node.180"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(5) %"class.std::__2::__tree_node_destructor.186"* @_ZNSt3__210unique_ptrINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEE11get_deleterEv(%"class.std::__2::unique_ptr.182"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.182"*, align 4
  store %"class.std::__2::unique_ptr.182"* %this, %"class.std::__2::unique_ptr.182"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.182"*, %"class.std::__2::unique_ptr.182"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.182", %"class.std::__2::unique_ptr.182"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(5) %"class.std::__2::__tree_node_destructor.186"* @_ZNSt3__217__compressed_pairIPNS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEE6secondEv(%"class.std::__2::__compressed_pair.183"* %__ptr_) #9
  ret %"class.std::__2::__tree_node_destructor.186"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.117"* @_ZNSt3__217__compressed_pairINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEENS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS7_IcEEEESE_EES3_EEEEE6secondEv(%"class.std::__2::__compressed_pair.114"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.114"*, align 4
  store %"class.std::__2::__compressed_pair.114"* %this, %"class.std::__2::__compressed_pair.114"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.114"*, %"class.std::__2::__compressed_pair.114"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.114"* %this1 to %"struct.std::__2::__compressed_pair_elem.116"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.117"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.116"* %0) #9
  ret %"class.std::__2::allocator.117"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.117"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.116"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.116"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.116"* %this, %"struct.std::__2::__compressed_pair_elem.116"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.116"*, %"struct.std::__2::__compressed_pair_elem.116"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.116"* %this1 to %"class.std::__2::allocator.117"*
  ret %"class.std::__2::allocator.117"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_node.180"* @_ZNSt3__29allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEES7_EEPvEEE8allocateEmPKv(%"class.std::__2::allocator.117"* %this, i32 %__n, i8* %0) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.117"*, align 4
  %__n.addr = alloca i32, align 4
  %.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.117"* %this, %"class.std::__2::allocator.117"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::allocator.117"*, %"class.std::__2::allocator.117"** %this.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEES7_EEPvEEE8max_sizeEv(%"class.std::__2::allocator.117"* %this1) #9
  %cmp = icmp ugt i32 %1, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__220__throw_length_errorEPKc(i8* getelementptr inbounds ([68 x i8], [68 x i8]* @.str.4, i32 0, i32 0)) #10
  unreachable

if.end:                                           ; preds = %entry
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 40
  %call2 = call i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %mul, i32 4)
  %3 = bitcast i8* %call2 to %"class.std::__2::__tree_node.180"*
  ret %"class.std::__2::__tree_node.180"* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__29allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEES7_EEPvEEE8max_sizeEv(%"class.std::__2::allocator.117"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.117"*, align 4
  store %"class.std::__2::allocator.117"* %this, %"class.std::__2::allocator.117"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.117"*, %"class.std::__2::allocator.117"** %this.addr, align 4
  ret i32 107374182
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(5) %"class.std::__2::__tree_node_destructor.186"* @_ZNSt3__24moveIRNS_22__tree_node_destructorINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS2_IcEEEES9_EEPvEEEEEEEEONS_16remove_referenceIT_E4typeEOSH_(%"class.std::__2::__tree_node_destructor.186"* nonnull align 4 dereferenceable(5) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::__tree_node_destructor.186"*, align 4
  store %"class.std::__2::__tree_node_destructor.186"* %__t, %"class.std::__2::__tree_node_destructor.186"** %__t.addr, align 4
  %0 = load %"class.std::__2::__tree_node_destructor.186"*, %"class.std::__2::__tree_node_destructor.186"** %__t.addr, align 4
  ret %"class.std::__2::__tree_node_destructor.186"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.183"* @_ZNSt3__217__compressed_pairIPNS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEEC2IRSC_SF_EEOT_OT0_(%"class.std::__2::__compressed_pair.183"* returned %this, %"class.std::__2::__tree_node.180"** nonnull align 4 dereferenceable(4) %__t1, %"class.std::__2::__tree_node_destructor.186"* nonnull align 4 dereferenceable(5) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.183"*, align 4
  %__t1.addr = alloca %"class.std::__2::__tree_node.180"**, align 4
  %__t2.addr = alloca %"class.std::__2::__tree_node_destructor.186"*, align 4
  store %"class.std::__2::__compressed_pair.183"* %this, %"class.std::__2::__compressed_pair.183"** %this.addr, align 4
  store %"class.std::__2::__tree_node.180"** %__t1, %"class.std::__2::__tree_node.180"*** %__t1.addr, align 4
  store %"class.std::__2::__tree_node_destructor.186"* %__t2, %"class.std::__2::__tree_node_destructor.186"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.183"*, %"class.std::__2::__compressed_pair.183"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.183"* %this1 to %"struct.std::__2::__compressed_pair_elem.184"*
  %1 = load %"class.std::__2::__tree_node.180"**, %"class.std::__2::__tree_node.180"*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_node.180"** @_ZNSt3__27forwardIRPNS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEEEEOT_RNS_16remove_referenceISE_E4typeE(%"class.std::__2::__tree_node.180"** nonnull align 4 dereferenceable(4) %1) #9
  %call2 = call %"struct.std::__2::__compressed_pair_elem.184"* @_ZNSt3__222__compressed_pair_elemIPNS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEELi0ELb0EEC2IRSC_vEEOT_(%"struct.std::__2::__compressed_pair_elem.184"* %0, %"class.std::__2::__tree_node.180"** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.183"* %this1 to i8*
  %3 = getelementptr inbounds i8, i8* %2, i32 4
  %4 = bitcast i8* %3 to %"struct.std::__2::__compressed_pair_elem.185"*
  %5 = load %"class.std::__2::__tree_node_destructor.186"*, %"class.std::__2::__tree_node_destructor.186"** %__t2.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(5) %"class.std::__2::__tree_node_destructor.186"* @_ZNSt3__27forwardINS_22__tree_node_destructorINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS2_IcEEEES9_EEPvEEEEEEEEOT_RNS_16remove_referenceISF_E4typeE(%"class.std::__2::__tree_node_destructor.186"* nonnull align 4 dereferenceable(5) %5) #9
  %call4 = call %"struct.std::__2::__compressed_pair_elem.185"* @_ZNSt3__222__compressed_pair_elemINS_22__tree_node_destructorINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS2_IcEEEES9_EEPvEEEEEELi1ELb0EEC2ISE_vEEOT_(%"struct.std::__2::__compressed_pair_elem.185"* %4, %"class.std::__2::__tree_node_destructor.186"* nonnull align 4 dereferenceable(5) %call3)
  ret %"class.std::__2::__compressed_pair.183"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_node.180"** @_ZNSt3__27forwardIRPNS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEEEEOT_RNS_16remove_referenceISE_E4typeE(%"class.std::__2::__tree_node.180"** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::__tree_node.180"**, align 4
  store %"class.std::__2::__tree_node.180"** %__t, %"class.std::__2::__tree_node.180"*** %__t.addr, align 4
  %0 = load %"class.std::__2::__tree_node.180"**, %"class.std::__2::__tree_node.180"*** %__t.addr, align 4
  ret %"class.std::__2::__tree_node.180"** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.184"* @_ZNSt3__222__compressed_pair_elemIPNS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEELi0ELb0EEC2IRSC_vEEOT_(%"struct.std::__2::__compressed_pair_elem.184"* returned %this, %"class.std::__2::__tree_node.180"** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.184"*, align 4
  %__u.addr = alloca %"class.std::__2::__tree_node.180"**, align 4
  store %"struct.std::__2::__compressed_pair_elem.184"* %this, %"struct.std::__2::__compressed_pair_elem.184"** %this.addr, align 4
  store %"class.std::__2::__tree_node.180"** %__u, %"class.std::__2::__tree_node.180"*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.184"*, %"struct.std::__2::__compressed_pair_elem.184"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.184", %"struct.std::__2::__compressed_pair_elem.184"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::__tree_node.180"**, %"class.std::__2::__tree_node.180"*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_node.180"** @_ZNSt3__27forwardIRPNS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEEEEOT_RNS_16remove_referenceISE_E4typeE(%"class.std::__2::__tree_node.180"** nonnull align 4 dereferenceable(4) %0) #9
  %1 = load %"class.std::__2::__tree_node.180"*, %"class.std::__2::__tree_node.180"** %call, align 4
  store %"class.std::__2::__tree_node.180"* %1, %"class.std::__2::__tree_node.180"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.184"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(5) %"class.std::__2::__tree_node_destructor.186"* @_ZNSt3__27forwardINS_22__tree_node_destructorINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS2_IcEEEES9_EEPvEEEEEEEEOT_RNS_16remove_referenceISF_E4typeE(%"class.std::__2::__tree_node_destructor.186"* nonnull align 4 dereferenceable(5) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::__tree_node_destructor.186"*, align 4
  store %"class.std::__2::__tree_node_destructor.186"* %__t, %"class.std::__2::__tree_node_destructor.186"** %__t.addr, align 4
  %0 = load %"class.std::__2::__tree_node_destructor.186"*, %"class.std::__2::__tree_node_destructor.186"** %__t.addr, align 4
  ret %"class.std::__2::__tree_node_destructor.186"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.185"* @_ZNSt3__222__compressed_pair_elemINS_22__tree_node_destructorINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS2_IcEEEES9_EEPvEEEEEELi1ELb0EEC2ISE_vEEOT_(%"struct.std::__2::__compressed_pair_elem.185"* returned %this, %"class.std::__2::__tree_node_destructor.186"* nonnull align 4 dereferenceable(5) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.185"*, align 4
  %__u.addr = alloca %"class.std::__2::__tree_node_destructor.186"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.185"* %this, %"struct.std::__2::__compressed_pair_elem.185"** %this.addr, align 4
  store %"class.std::__2::__tree_node_destructor.186"* %__u, %"class.std::__2::__tree_node_destructor.186"** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.185"*, %"struct.std::__2::__compressed_pair_elem.185"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.185", %"struct.std::__2::__compressed_pair_elem.185"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::__tree_node_destructor.186"*, %"class.std::__2::__tree_node_destructor.186"** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(5) %"class.std::__2::__tree_node_destructor.186"* @_ZNSt3__27forwardINS_22__tree_node_destructorINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS2_IcEEEES9_EEPvEEEEEEEEOT_RNS_16remove_referenceISF_E4typeE(%"class.std::__2::__tree_node_destructor.186"* nonnull align 4 dereferenceable(5) %0) #9
  %1 = bitcast %"class.std::__2::__tree_node_destructor.186"* %__value_ to i8*
  %2 = bitcast %"class.std::__2::__tree_node_destructor.186"* %call to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 8, i1 false)
  ret %"struct.std::__2::__compressed_pair_elem.185"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEEE11__constructINS_4pairIKS8_S8_EEJRKSH_EEEvNS_17integral_constantIbLb1EEERSC_PT_DpOT0_(%"class.std::__2::allocator.117"* nonnull align 1 dereferenceable(1) %__a, %"struct.std::__2::pair.177"* %__p, %"struct.std::__2::pair.177"* nonnull align 4 dereferenceable(24) %__args) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.117"*, align 4
  %__p.addr = alloca %"struct.std::__2::pair.177"*, align 4
  %__args.addr = alloca %"struct.std::__2::pair.177"*, align 4
  store %"class.std::__2::allocator.117"* %__a, %"class.std::__2::allocator.117"** %__a.addr, align 4
  store %"struct.std::__2::pair.177"* %__p, %"struct.std::__2::pair.177"** %__p.addr, align 4
  store %"struct.std::__2::pair.177"* %__args, %"struct.std::__2::pair.177"** %__args.addr, align 4
  %1 = load %"class.std::__2::allocator.117"*, %"class.std::__2::allocator.117"** %__a.addr, align 4
  %2 = load %"struct.std::__2::pair.177"*, %"struct.std::__2::pair.177"** %__p.addr, align 4
  %3 = load %"struct.std::__2::pair.177"*, %"struct.std::__2::pair.177"** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(24) %"struct.std::__2::pair.177"* @_ZNSt3__27forwardIRKNS_4pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEEEOT_RNS_16remove_referenceISC_E4typeE(%"struct.std::__2::pair.177"* nonnull align 4 dereferenceable(24) %3) #9
  call void @_ZNSt3__29allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEES7_EEPvEEE9constructINS_4pairIKS7_S7_EEJRKSF_EEEvPT_DpOT0_(%"class.std::__2::allocator.117"* %1, %"struct.std::__2::pair.177"* %2, %"struct.std::__2::pair.177"* nonnull align 4 dereferenceable(24) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEES7_EEPvEEE9constructINS_4pairIKS7_S7_EEJRKSF_EEEvPT_DpOT0_(%"class.std::__2::allocator.117"* %this, %"struct.std::__2::pair.177"* %__p, %"struct.std::__2::pair.177"* nonnull align 4 dereferenceable(24) %__args) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.117"*, align 4
  %__p.addr = alloca %"struct.std::__2::pair.177"*, align 4
  %__args.addr = alloca %"struct.std::__2::pair.177"*, align 4
  store %"class.std::__2::allocator.117"* %this, %"class.std::__2::allocator.117"** %this.addr, align 4
  store %"struct.std::__2::pair.177"* %__p, %"struct.std::__2::pair.177"** %__p.addr, align 4
  store %"struct.std::__2::pair.177"* %__args, %"struct.std::__2::pair.177"** %__args.addr, align 4
  %this1 = load %"class.std::__2::allocator.117"*, %"class.std::__2::allocator.117"** %this.addr, align 4
  %0 = load %"struct.std::__2::pair.177"*, %"struct.std::__2::pair.177"** %__p.addr, align 4
  %1 = bitcast %"struct.std::__2::pair.177"* %0 to i8*
  %2 = bitcast i8* %1 to %"struct.std::__2::pair.177"*
  %3 = load %"struct.std::__2::pair.177"*, %"struct.std::__2::pair.177"** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(24) %"struct.std::__2::pair.177"* @_ZNSt3__27forwardIRKNS_4pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEEEOT_RNS_16remove_referenceISC_E4typeE(%"struct.std::__2::pair.177"* nonnull align 4 dereferenceable(24) %3) #9
  %call2 = call %"struct.std::__2::pair.177"* @_ZNSt3__24pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_EC2ERKS8_(%"struct.std::__2::pair.177"* %2, %"struct.std::__2::pair.177"* nonnull align 4 dereferenceable(24) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::pair.177"* @_ZNSt3__24pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_EC2ERKS8_(%"struct.std::__2::pair.177"* returned %this, %"struct.std::__2::pair.177"* nonnull align 4 dereferenceable(24) %0) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::pair.177"*, align 4
  %.addr = alloca %"struct.std::__2::pair.177"*, align 4
  store %"struct.std::__2::pair.177"* %this, %"struct.std::__2::pair.177"** %this.addr, align 4
  store %"struct.std::__2::pair.177"* %0, %"struct.std::__2::pair.177"** %.addr, align 4
  %this1 = load %"struct.std::__2::pair.177"*, %"struct.std::__2::pair.177"** %this.addr, align 4
  %first = getelementptr inbounds %"struct.std::__2::pair.177", %"struct.std::__2::pair.177"* %this1, i32 0, i32 0
  %1 = load %"struct.std::__2::pair.177"*, %"struct.std::__2::pair.177"** %.addr, align 4
  %first2 = getelementptr inbounds %"struct.std::__2::pair.177", %"struct.std::__2::pair.177"* %1, i32 0, i32 0
  %call = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKS5_(%"class.std::__2::basic_string"* %first, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %first2)
  %second = getelementptr inbounds %"struct.std::__2::pair.177", %"struct.std::__2::pair.177"* %this1, i32 0, i32 1
  %2 = load %"struct.std::__2::pair.177"*, %"struct.std::__2::pair.177"** %.addr, align 4
  %second3 = getelementptr inbounds %"struct.std::__2::pair.177", %"struct.std::__2::pair.177"* %2, i32 0, i32 1
  %call4 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKS5_(%"class.std::__2::basic_string"* %second, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %second3)
  ret %"struct.std::__2::pair.177"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::pair.177"* @_ZNSt3__29addressofINS_4pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEEEPT_RSA_(%"struct.std::__2::pair.177"* nonnull align 4 dereferenceable(24) %__x) #0 comdat {
entry:
  %__x.addr = alloca %"struct.std::__2::pair.177"*, align 4
  store %"struct.std::__2::pair.177"* %__x, %"struct.std::__2::pair.177"** %__x.addr, align 4
  %0 = load %"struct.std::__2::pair.177"*, %"struct.std::__2::pair.177"** %__x.addr, align 4
  ret %"struct.std::__2::pair.177"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(24) %"struct.std::__2::pair.177"* @_ZNSt3__212__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_E11__get_valueEv(%"struct.std::__2::__value_type.181"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__value_type.181"*, align 4
  store %"struct.std::__2::__value_type.181"* %this, %"struct.std::__2::__value_type.181"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__value_type.181"*, %"struct.std::__2::__value_type.181"** %this.addr, align 4
  %__cc = getelementptr inbounds %"struct.std::__2::__value_type.181", %"struct.std::__2::__value_type.181"* %this1, i32 0, i32 0
  ret %"struct.std::__2::pair.177"* %__cc
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_node.180"** @_ZNKSt3__217__compressed_pairIPNS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEE5firstEv(%"class.std::__2::__compressed_pair.183"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.183"*, align 4
  store %"class.std::__2::__compressed_pair.183"* %this, %"class.std::__2::__compressed_pair.183"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.183"*, %"class.std::__2::__compressed_pair.183"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.183"* %this1 to %"struct.std::__2::__compressed_pair_elem.184"*
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_node.180"** @_ZNKSt3__222__compressed_pair_elemIPNS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.184"* %0) #9
  ret %"class.std::__2::__tree_node.180"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_node.180"** @_ZNKSt3__222__compressed_pair_elemIPNS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.184"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.184"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.184"* %this, %"struct.std::__2::__compressed_pair_elem.184"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.184"*, %"struct.std::__2::__compressed_pair_elem.184"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.184", %"struct.std::__2::__compressed_pair_elem.184"* %this1, i32 0, i32 0
  ret %"class.std::__2::__tree_node.180"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(5) %"class.std::__2::__tree_node_destructor.186"* @_ZNSt3__217__compressed_pairIPNS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEE6secondEv(%"class.std::__2::__compressed_pair.183"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.183"*, align 4
  store %"class.std::__2::__compressed_pair.183"* %this, %"class.std::__2::__compressed_pair.183"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.183"*, %"class.std::__2::__compressed_pair.183"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.183"* %this1 to i8*
  %add.ptr = getelementptr inbounds i8, i8* %0, i32 4
  %1 = bitcast i8* %add.ptr to %"struct.std::__2::__compressed_pair_elem.185"*
  %call = call nonnull align 4 dereferenceable(5) %"class.std::__2::__tree_node_destructor.186"* @_ZNSt3__222__compressed_pair_elemINS_22__tree_node_destructorINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS2_IcEEEES9_EEPvEEEEEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.185"* %1) #9
  ret %"class.std::__2::__tree_node_destructor.186"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(5) %"class.std::__2::__tree_node_destructor.186"* @_ZNSt3__222__compressed_pair_elemINS_22__tree_node_destructorINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS2_IcEEEES9_EEPvEEEEEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.185"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.185"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.185"* %this, %"struct.std::__2::__compressed_pair_elem.185"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.185"*, %"struct.std::__2::__compressed_pair_elem.185"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.185", %"struct.std::__2::__compressed_pair_elem.185"* %this1, i32 0, i32 0
  ret %"class.std::__2::__tree_node_destructor.186"* %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_node.180"** @_ZNSt3__217__compressed_pairIPNS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEE5firstEv(%"class.std::__2::__compressed_pair.183"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.183"*, align 4
  store %"class.std::__2::__compressed_pair.183"* %this, %"class.std::__2::__compressed_pair.183"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.183"*, %"class.std::__2::__compressed_pair.183"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.183"* %this1 to %"struct.std::__2::__compressed_pair_elem.184"*
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_node.180"** @_ZNSt3__222__compressed_pair_elemIPNS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.184"* %0) #9
  ret %"class.std::__2::__tree_node.180"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_node.180"** @_ZNSt3__222__compressed_pair_elemIPNS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.184"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.184"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.184"* %this, %"struct.std::__2::__compressed_pair_elem.184"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.184"*, %"struct.std::__2::__compressed_pair_elem.184"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.184", %"struct.std::__2::__compressed_pair_elem.184"* %this1, i32 0, i32 0
  ret %"class.std::__2::__tree_node.180"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__210unique_ptrINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEE5resetEPSB_(%"class.std::__2::unique_ptr.182"* %this, %"class.std::__2::__tree_node.180"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.182"*, align 4
  %__p.addr = alloca %"class.std::__2::__tree_node.180"*, align 4
  %__tmp = alloca %"class.std::__2::__tree_node.180"*, align 4
  store %"class.std::__2::unique_ptr.182"* %this, %"class.std::__2::unique_ptr.182"** %this.addr, align 4
  store %"class.std::__2::__tree_node.180"* %__p, %"class.std::__2::__tree_node.180"** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.182"*, %"class.std::__2::unique_ptr.182"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.182", %"class.std::__2::unique_ptr.182"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_node.180"** @_ZNSt3__217__compressed_pairIPNS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEE5firstEv(%"class.std::__2::__compressed_pair.183"* %__ptr_) #9
  %0 = load %"class.std::__2::__tree_node.180"*, %"class.std::__2::__tree_node.180"** %call, align 4
  store %"class.std::__2::__tree_node.180"* %0, %"class.std::__2::__tree_node.180"** %__tmp, align 4
  %1 = load %"class.std::__2::__tree_node.180"*, %"class.std::__2::__tree_node.180"** %__p.addr, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr.182", %"class.std::__2::unique_ptr.182"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_node.180"** @_ZNSt3__217__compressed_pairIPNS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEE5firstEv(%"class.std::__2::__compressed_pair.183"* %__ptr_2) #9
  store %"class.std::__2::__tree_node.180"* %1, %"class.std::__2::__tree_node.180"** %call3, align 4
  %2 = load %"class.std::__2::__tree_node.180"*, %"class.std::__2::__tree_node.180"** %__tmp, align 4
  %tobool = icmp ne %"class.std::__2::__tree_node.180"* %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__ptr_4 = getelementptr inbounds %"class.std::__2::unique_ptr.182", %"class.std::__2::unique_ptr.182"* %this1, i32 0, i32 0
  %call5 = call nonnull align 4 dereferenceable(5) %"class.std::__2::__tree_node_destructor.186"* @_ZNSt3__217__compressed_pairIPNS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEE6secondEv(%"class.std::__2::__compressed_pair.183"* %__ptr_4) #9
  %3 = load %"class.std::__2::__tree_node.180"*, %"class.std::__2::__tree_node.180"** %__tmp, align 4
  call void @_ZNSt3__222__tree_node_destructorINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEEEclEPSB_(%"class.std::__2::__tree_node_destructor.186"* %call5, %"class.std::__2::__tree_node.180"* %3) #9
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__222__tree_node_destructorINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEEEclEPSB_(%"class.std::__2::__tree_node_destructor.186"* %this, %"class.std::__2::__tree_node.180"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree_node_destructor.186"*, align 4
  %__p.addr = alloca %"class.std::__2::__tree_node.180"*, align 4
  store %"class.std::__2::__tree_node_destructor.186"* %this, %"class.std::__2::__tree_node_destructor.186"** %this.addr, align 4
  store %"class.std::__2::__tree_node.180"* %__p, %"class.std::__2::__tree_node.180"** %__p.addr, align 4
  %this1 = load %"class.std::__2::__tree_node_destructor.186"*, %"class.std::__2::__tree_node_destructor.186"** %this.addr, align 4
  %__value_constructed = getelementptr inbounds %"class.std::__2::__tree_node_destructor.186", %"class.std::__2::__tree_node_destructor.186"* %this1, i32 0, i32 1
  %0 = load i8, i8* %__value_constructed, align 4
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__na_ = getelementptr inbounds %"class.std::__2::__tree_node_destructor.186", %"class.std::__2::__tree_node_destructor.186"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::allocator.117"*, %"class.std::__2::allocator.117"** %__na_, align 4
  %2 = load %"class.std::__2::__tree_node.180"*, %"class.std::__2::__tree_node.180"** %__p.addr, align 4
  %__value_ = getelementptr inbounds %"class.std::__2::__tree_node.180", %"class.std::__2::__tree_node.180"* %2, i32 0, i32 1
  %call = call %"struct.std::__2::pair.177"* @_ZNSt3__222__tree_key_value_typesINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEE9__get_ptrERS8_(%"struct.std::__2::__value_type.181"* nonnull align 4 dereferenceable(24) %__value_)
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEEE7destroyINS_4pairIKS8_S8_EEEEvRSC_PT_(%"class.std::__2::allocator.117"* nonnull align 1 dereferenceable(1) %1, %"struct.std::__2::pair.177"* %call)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %3 = load %"class.std::__2::__tree_node.180"*, %"class.std::__2::__tree_node.180"** %__p.addr, align 4
  %tobool2 = icmp ne %"class.std::__2::__tree_node.180"* %3, null
  br i1 %tobool2, label %if.then3, label %if.end5

if.then3:                                         ; preds = %if.end
  %__na_4 = getelementptr inbounds %"class.std::__2::__tree_node_destructor.186", %"class.std::__2::__tree_node_destructor.186"* %this1, i32 0, i32 0
  %4 = load %"class.std::__2::allocator.117"*, %"class.std::__2::allocator.117"** %__na_4, align 4
  %5 = load %"class.std::__2::__tree_node.180"*, %"class.std::__2::__tree_node.180"** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEEE10deallocateERSC_PSB_m(%"class.std::__2::allocator.117"* nonnull align 1 dereferenceable(1) %4, %"class.std::__2::__tree_node.180"* %5, i32 1) #9
  br label %if.end5

if.end5:                                          ; preds = %if.then3, %if.end
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEEE7destroyINS_4pairIKS8_S8_EEEEvRSC_PT_(%"class.std::__2::allocator.117"* nonnull align 1 dereferenceable(1) %__a, %"struct.std::__2::pair.177"* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.117"*, align 4
  %__p.addr = alloca %"struct.std::__2::pair.177"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant.176", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy.189", align 1
  store %"class.std::__2::allocator.117"* %__a, %"class.std::__2::allocator.117"** %__a.addr, align 4
  store %"struct.std::__2::pair.177"* %__p, %"struct.std::__2::pair.177"** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy.189"* %ref.tmp to %"struct.std::__2::integral_constant.176"*
  %1 = load %"class.std::__2::allocator.117"*, %"class.std::__2::allocator.117"** %__a.addr, align 4
  %2 = load %"struct.std::__2::pair.177"*, %"struct.std::__2::pair.177"** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEEE9__destroyINS_4pairIKS8_S8_EEEEvNS_17integral_constantIbLb0EEERSC_PT_(%"class.std::__2::allocator.117"* nonnull align 1 dereferenceable(1) %1, %"struct.std::__2::pair.177"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEEE10deallocateERSC_PSB_m(%"class.std::__2::allocator.117"* nonnull align 1 dereferenceable(1) %__a, %"class.std::__2::__tree_node.180"* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.117"*, align 4
  %__p.addr = alloca %"class.std::__2::__tree_node.180"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.117"* %__a, %"class.std::__2::allocator.117"** %__a.addr, align 4
  store %"class.std::__2::__tree_node.180"* %__p, %"class.std::__2::__tree_node.180"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.117"*, %"class.std::__2::allocator.117"** %__a.addr, align 4
  %1 = load %"class.std::__2::__tree_node.180"*, %"class.std::__2::__tree_node.180"** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEES7_EEPvEEE10deallocateEPSA_m(%"class.std::__2::allocator.117"* %0, %"class.std::__2::__tree_node.180"* %1, i32 %2) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEEE9__destroyINS_4pairIKS8_S8_EEEEvNS_17integral_constantIbLb0EEERSC_PT_(%"class.std::__2::allocator.117"* nonnull align 1 dereferenceable(1) %0, %"struct.std::__2::pair.177"* %__p) #0 comdat {
entry:
  %1 = alloca %"struct.std::__2::integral_constant.176", align 1
  %.addr = alloca %"class.std::__2::allocator.117"*, align 4
  %__p.addr = alloca %"struct.std::__2::pair.177"*, align 4
  store %"class.std::__2::allocator.117"* %0, %"class.std::__2::allocator.117"** %.addr, align 4
  store %"struct.std::__2::pair.177"* %__p, %"struct.std::__2::pair.177"** %__p.addr, align 4
  %2 = load %"struct.std::__2::pair.177"*, %"struct.std::__2::pair.177"** %__p.addr, align 4
  %call = call %"struct.std::__2::pair.177"* @_ZNSt3__24pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_ED2Ev(%"struct.std::__2::pair.177"* %2) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::pair.177"* @_ZNSt3__24pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_ED2Ev(%"struct.std::__2::pair.177"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::pair.177"*, align 4
  store %"struct.std::__2::pair.177"* %this, %"struct.std::__2::pair.177"** %this.addr, align 4
  %this1 = load %"struct.std::__2::pair.177"*, %"struct.std::__2::pair.177"** %this.addr, align 4
  %second = getelementptr inbounds %"struct.std::__2::pair.177", %"struct.std::__2::pair.177"* %this1, i32 0, i32 1
  %call = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_string"* %second) #9
  %first = getelementptr inbounds %"struct.std::__2::pair.177", %"struct.std::__2::pair.177"* %this1, i32 0, i32 0
  %call2 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_string"* %first) #9
  ret %"struct.std::__2::pair.177"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEES7_EEPvEEE10deallocateEPSA_m(%"class.std::__2::allocator.117"* %this, %"class.std::__2::__tree_node.180"* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.117"*, align 4
  %__p.addr = alloca %"class.std::__2::__tree_node.180"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.117"* %this, %"class.std::__2::allocator.117"** %this.addr, align 4
  store %"class.std::__2::__tree_node.180"* %__p, %"class.std::__2::__tree_node.180"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.117"*, %"class.std::__2::allocator.117"** %this.addr, align 4
  %0 = load %"class.std::__2::__tree_node.180"*, %"class.std::__2::__tree_node.180"** %__p.addr, align 4
  %1 = bitcast %"class.std::__2::__tree_node.180"* %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 40
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__value_type.181"* @_ZNKSt3__221__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElEptEv(%"class.std::__2::__tree_const_iterator"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree_const_iterator"*, align 4
  store %"class.std::__2::__tree_const_iterator"* %this, %"class.std::__2::__tree_const_iterator"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree_const_iterator"*, %"class.std::__2::__tree_const_iterator"** %this.addr, align 4
  %call = call %"class.std::__2::__tree_node.180"* @_ZNKSt3__221__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElE8__get_npEv(%"class.std::__2::__tree_const_iterator"* %this1)
  %__value_ = getelementptr inbounds %"class.std::__2::__tree_node.180", %"class.std::__2::__tree_node.180"* %call, i32 0, i32 1
  %call2 = call %"struct.std::__2::__value_type.181"* @_ZNSt3__214pointer_traitsIPKNS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEE10pointer_toERS9_(%"struct.std::__2::__value_type.181"* nonnull align 4 dereferenceable(24) %__value_) #9
  ret %"struct.std::__2::__value_type.181"* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__value_type.181"* @_ZNSt3__214pointer_traitsIPKNS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEE10pointer_toERS9_(%"struct.std::__2::__value_type.181"* nonnull align 4 dereferenceable(24) %__r) #0 comdat {
entry:
  %__r.addr = alloca %"struct.std::__2::__value_type.181"*, align 4
  store %"struct.std::__2::__value_type.181"* %__r, %"struct.std::__2::__value_type.181"** %__r.addr, align 4
  %0 = load %"struct.std::__2::__value_type.181"*, %"struct.std::__2::__value_type.181"** %__r.addr, align 4
  %call = call %"struct.std::__2::__value_type.181"* @_ZNSt3__29addressofIKNS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEEEPT_RSA_(%"struct.std::__2::__value_type.181"* nonnull align 4 dereferenceable(24) %0) #9
  ret %"struct.std::__2::__value_type.181"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__value_type.181"* @_ZNSt3__29addressofIKNS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEEEPT_RSA_(%"struct.std::__2::__value_type.181"* nonnull align 4 dereferenceable(24) %__x) #0 comdat {
entry:
  %__x.addr = alloca %"struct.std::__2::__value_type.181"*, align 4
  store %"struct.std::__2::__value_type.181"* %__x, %"struct.std::__2::__value_type.181"** %__x.addr, align 4
  %0 = load %"struct.std::__2::__value_type.181"*, %"struct.std::__2::__value_type.181"** %__x.addr, align 4
  ret %"struct.std::__2::__value_type.181"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_end_node"* @_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE5beginEv(%"class.std::__2::__tree"* %this) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__tree_const_iterator", align 4
  %this.addr = alloca %"class.std::__2::__tree"*, align 4
  store %"class.std::__2::__tree"* %this, %"class.std::__2::__tree"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree"*, %"class.std::__2::__tree"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_end_node"** @_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE12__begin_nodeEv(%"class.std::__2::__tree"* %this1) #9
  %0 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %call, align 4
  %call2 = call %"class.std::__2::__tree_const_iterator"* @_ZNSt3__221__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElEC2EPNS_15__tree_end_nodeIPNS_16__tree_node_baseISA_EEEE(%"class.std::__2::__tree_const_iterator"* %retval, %"class.std::__2::__tree_end_node"* %0) #9
  %coerce.dive = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %retval, i32 0, i32 0
  %1 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %coerce.dive, align 4
  ret %"class.std::__2::__tree_end_node"* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_end_node"** @_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE12__begin_nodeEv(%"class.std::__2::__tree"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree"*, align 4
  store %"class.std::__2::__tree"* %this, %"class.std::__2::__tree"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree"*, %"class.std::__2::__tree"** %this.addr, align 4
  %__begin_node_ = getelementptr inbounds %"class.std::__2::__tree", %"class.std::__2::__tree"* %this1, i32 0, i32 0
  ret %"class.std::__2::__tree_end_node"** %__begin_node_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_const_iterator"* @_ZNSt3__221__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElEC2EPNS_15__tree_end_nodeIPNS_16__tree_node_baseISA_EEEE(%"class.std::__2::__tree_const_iterator"* returned %this, %"class.std::__2::__tree_end_node"* %__p) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree_const_iterator"*, align 4
  %__p.addr = alloca %"class.std::__2::__tree_end_node"*, align 4
  store %"class.std::__2::__tree_const_iterator"* %this, %"class.std::__2::__tree_const_iterator"** %this.addr, align 4
  store %"class.std::__2::__tree_end_node"* %__p, %"class.std::__2::__tree_end_node"** %__p.addr, align 4
  %this1 = load %"class.std::__2::__tree_const_iterator"*, %"class.std::__2::__tree_const_iterator"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %__p.addr, align 4
  store %"class.std::__2::__tree_end_node"* %0, %"class.std::__2::__tree_end_node"** %__ptr_, align 4
  ret %"class.std::__2::__tree_const_iterator"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_end_node"* @_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE3endEv(%"class.std::__2::__tree"* %this) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__tree_const_iterator", align 4
  %this.addr = alloca %"class.std::__2::__tree"*, align 4
  store %"class.std::__2::__tree"* %this, %"class.std::__2::__tree"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree"*, %"class.std::__2::__tree"** %this.addr, align 4
  %call = call %"class.std::__2::__tree_end_node"* @_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE10__end_nodeEv(%"class.std::__2::__tree"* %this1) #9
  %call2 = call %"class.std::__2::__tree_const_iterator"* @_ZNSt3__221__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElEC2EPNS_15__tree_end_nodeIPNS_16__tree_node_baseISA_EEEE(%"class.std::__2::__tree_const_iterator"* %retval, %"class.std::__2::__tree_end_node"* %call) #9
  %coerce.dive = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %retval, i32 0, i32 0
  %0 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %coerce.dive, align 4
  ret %"class.std::__2::__tree_end_node"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::map"* @_ZNSt3__23mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_NS_4lessIS6_EENS4_INS_4pairIKS6_S6_EEEEED2Ev(%"class.std::__2::map"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::map"*, align 4
  store %"class.std::__2::map"* %this, %"class.std::__2::map"** %this.addr, align 4
  %this1 = load %"class.std::__2::map"*, %"class.std::__2::map"** %this.addr, align 4
  %__tree_ = getelementptr inbounds %"class.std::__2::map", %"class.std::__2::map"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::__tree"* @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEED2Ev(%"class.std::__2::__tree"* %__tree_) #9
  ret %"class.std::__2::map"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree"* @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEED2Ev(%"class.std::__2::__tree"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree"*, align 4
  store %"class.std::__2::__tree"* %this, %"class.std::__2::__tree"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree"*, %"class.std::__2::__tree"** %this.addr, align 4
  %call = call %"class.std::__2::__tree_node.180"* @_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE6__rootEv(%"class.std::__2::__tree"* %this1) #9
  call void @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE7destroyEPNS_11__tree_nodeIS8_PvEE(%"class.std::__2::__tree"* %this1, %"class.std::__2::__tree_node.180"* %call) #9
  ret %"class.std::__2::__tree"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE7destroyEPNS_11__tree_nodeIS8_PvEE(%"class.std::__2::__tree"* %this, %"class.std::__2::__tree_node.180"* %__nd) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree"*, align 4
  %__nd.addr = alloca %"class.std::__2::__tree_node.180"*, align 4
  %__na = alloca %"class.std::__2::allocator.117"*, align 4
  store %"class.std::__2::__tree"* %this, %"class.std::__2::__tree"** %this.addr, align 4
  store %"class.std::__2::__tree_node.180"* %__nd, %"class.std::__2::__tree_node.180"** %__nd.addr, align 4
  %this1 = load %"class.std::__2::__tree"*, %"class.std::__2::__tree"** %this.addr, align 4
  %0 = load %"class.std::__2::__tree_node.180"*, %"class.std::__2::__tree_node.180"** %__nd.addr, align 4
  %cmp = icmp ne %"class.std::__2::__tree_node.180"* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %"class.std::__2::__tree_node.180"*, %"class.std::__2::__tree_node.180"** %__nd.addr, align 4
  %2 = bitcast %"class.std::__2::__tree_node.180"* %1 to %"class.std::__2::__tree_end_node"*
  %__left_ = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %2, i32 0, i32 0
  %3 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__left_, align 4
  %4 = bitcast %"class.std::__2::__tree_node_base"* %3 to %"class.std::__2::__tree_node.180"*
  call void @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE7destroyEPNS_11__tree_nodeIS8_PvEE(%"class.std::__2::__tree"* %this1, %"class.std::__2::__tree_node.180"* %4) #9
  %5 = load %"class.std::__2::__tree_node.180"*, %"class.std::__2::__tree_node.180"** %__nd.addr, align 4
  %6 = bitcast %"class.std::__2::__tree_node.180"* %5 to %"class.std::__2::__tree_node_base"*
  %__right_ = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %6, i32 0, i32 1
  %7 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__right_, align 4
  %8 = bitcast %"class.std::__2::__tree_node_base"* %7 to %"class.std::__2::__tree_node.180"*
  call void @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE7destroyEPNS_11__tree_nodeIS8_PvEE(%"class.std::__2::__tree"* %this1, %"class.std::__2::__tree_node.180"* %8) #9
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.117"* @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE12__node_allocEv(%"class.std::__2::__tree"* %this1) #9
  store %"class.std::__2::allocator.117"* %call, %"class.std::__2::allocator.117"** %__na, align 4
  %9 = load %"class.std::__2::allocator.117"*, %"class.std::__2::allocator.117"** %__na, align 4
  %10 = load %"class.std::__2::__tree_node.180"*, %"class.std::__2::__tree_node.180"** %__nd.addr, align 4
  %__value_ = getelementptr inbounds %"class.std::__2::__tree_node.180", %"class.std::__2::__tree_node.180"* %10, i32 0, i32 1
  %call2 = call %"struct.std::__2::pair.177"* @_ZNSt3__222__tree_key_value_typesINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEE9__get_ptrERS8_(%"struct.std::__2::__value_type.181"* nonnull align 4 dereferenceable(24) %__value_)
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEEE7destroyINS_4pairIKS8_S8_EEEEvRSC_PT_(%"class.std::__2::allocator.117"* nonnull align 1 dereferenceable(1) %9, %"struct.std::__2::pair.177"* %call2)
  %11 = load %"class.std::__2::allocator.117"*, %"class.std::__2::allocator.117"** %__na, align 4
  %12 = load %"class.std::__2::__tree_node.180"*, %"class.std::__2::__tree_node.180"** %__nd.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEEE10deallocateERSC_PSB_m(%"class.std::__2::allocator.117"* nonnull align 1 dereferenceable(1) %11, %"class.std::__2::__tree_node.180"* %12, i32 1) #9
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nobuiltin allocsize(0) "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { argmemonly nounwind willreturn }
attributes #5 = { noinline noreturn nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { noreturn "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #8 = { builtin allocsize(0) }
attributes #9 = { nounwind }
attributes #10 = { noreturn }
attributes #11 = { builtin nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
