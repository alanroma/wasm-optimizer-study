; ModuleID = './draco/src/draco/compression/mesh/mesh_edgebreaker_decoder.cc'
source_filename = "./draco/src/draco/compression/mesh/mesh_edgebreaker_decoder.cc"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%"class.draco::MeshEdgebreakerDecoder" = type { %"class.draco::MeshDecoder", %"class.std::__2::unique_ptr.122" }
%"class.draco::MeshDecoder" = type { %"class.draco::PointCloudDecoder", %"class.draco::Mesh"* }
%"class.draco::PointCloudDecoder" = type { i32 (...)**, %"class.draco::PointCloud"*, %"class.std::__2::vector.94", %"class.std::__2::vector.87", %"class.draco::DecoderBuffer"*, i8, i8, %"class.draco::DracoOptions"* }
%"class.draco::PointCloud" = type { i32 (...)**, %"class.std::__2::unique_ptr", %"class.std::__2::vector.51", [5 x %"class.std::__2::vector.87"], i32 }
%"class.std::__2::unique_ptr" = type { %"class.std::__2::__compressed_pair" }
%"class.std::__2::__compressed_pair" = type { %"struct.std::__2::__compressed_pair_elem" }
%"struct.std::__2::__compressed_pair_elem" = type { %"class.draco::GeometryMetadata"* }
%"class.draco::GeometryMetadata" = type { %"class.draco::Metadata", %"class.std::__2::vector" }
%"class.draco::Metadata" = type { %"class.std::__2::unordered_map", %"class.std::__2::unordered_map.17" }
%"class.std::__2::unordered_map" = type { %"class.std::__2::__hash_table" }
%"class.std::__2::__hash_table" = type { %"class.std::__2::unique_ptr.0", %"class.std::__2::__compressed_pair.7", %"class.std::__2::__compressed_pair.12", %"class.std::__2::__compressed_pair.14" }
%"class.std::__2::unique_ptr.0" = type { %"class.std::__2::__compressed_pair.1" }
%"class.std::__2::__compressed_pair.1" = type { %"struct.std::__2::__compressed_pair_elem.2", %"struct.std::__2::__compressed_pair_elem.3" }
%"struct.std::__2::__compressed_pair_elem.2" = type { %"struct.std::__2::__hash_node_base"** }
%"struct.std::__2::__hash_node_base" = type { %"struct.std::__2::__hash_node_base"* }
%"struct.std::__2::__compressed_pair_elem.3" = type { %"class.std::__2::__bucket_list_deallocator" }
%"class.std::__2::__bucket_list_deallocator" = type { %"class.std::__2::__compressed_pair.4" }
%"class.std::__2::__compressed_pair.4" = type { %"struct.std::__2::__compressed_pair_elem.5" }
%"struct.std::__2::__compressed_pair_elem.5" = type { i32 }
%"class.std::__2::__compressed_pair.7" = type { %"struct.std::__2::__compressed_pair_elem.8" }
%"struct.std::__2::__compressed_pair_elem.8" = type { %"struct.std::__2::__hash_node_base" }
%"class.std::__2::__compressed_pair.12" = type { %"struct.std::__2::__compressed_pair_elem.5" }
%"class.std::__2::__compressed_pair.14" = type { %"struct.std::__2::__compressed_pair_elem.15" }
%"struct.std::__2::__compressed_pair_elem.15" = type { float }
%"class.std::__2::unordered_map.17" = type { %"class.std::__2::__hash_table.18" }
%"class.std::__2::__hash_table.18" = type { %"class.std::__2::unique_ptr.19", %"class.std::__2::__compressed_pair.29", %"class.std::__2::__compressed_pair.34", %"class.std::__2::__compressed_pair.37" }
%"class.std::__2::unique_ptr.19" = type { %"class.std::__2::__compressed_pair.20" }
%"class.std::__2::__compressed_pair.20" = type { %"struct.std::__2::__compressed_pair_elem.21", %"struct.std::__2::__compressed_pair_elem.23" }
%"struct.std::__2::__compressed_pair_elem.21" = type { %"struct.std::__2::__hash_node_base.22"** }
%"struct.std::__2::__hash_node_base.22" = type { %"struct.std::__2::__hash_node_base.22"* }
%"struct.std::__2::__compressed_pair_elem.23" = type { %"class.std::__2::__bucket_list_deallocator.24" }
%"class.std::__2::__bucket_list_deallocator.24" = type { %"class.std::__2::__compressed_pair.25" }
%"class.std::__2::__compressed_pair.25" = type { %"struct.std::__2::__compressed_pair_elem.5" }
%"class.std::__2::__compressed_pair.29" = type { %"struct.std::__2::__compressed_pair_elem.30" }
%"struct.std::__2::__compressed_pair_elem.30" = type { %"struct.std::__2::__hash_node_base.22" }
%"class.std::__2::__compressed_pair.34" = type { %"struct.std::__2::__compressed_pair_elem.5" }
%"class.std::__2::__compressed_pair.37" = type { %"struct.std::__2::__compressed_pair_elem.15" }
%"class.std::__2::vector" = type { %"class.std::__2::__vector_base" }
%"class.std::__2::__vector_base" = type { %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"*, %"class.std::__2::__compressed_pair.44" }
%"class.std::__2::unique_ptr.40" = type { %"class.std::__2::__compressed_pair.41" }
%"class.std::__2::__compressed_pair.41" = type { %"struct.std::__2::__compressed_pair_elem.42" }
%"struct.std::__2::__compressed_pair_elem.42" = type { %"class.draco::AttributeMetadata"* }
%"class.draco::AttributeMetadata" = type { %"class.draco::Metadata", i32 }
%"class.std::__2::__compressed_pair.44" = type { %"struct.std::__2::__compressed_pair_elem.45" }
%"struct.std::__2::__compressed_pair_elem.45" = type { %"class.std::__2::unique_ptr.40"* }
%"class.std::__2::vector.51" = type { %"class.std::__2::__vector_base.52" }
%"class.std::__2::__vector_base.52" = type { %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"*, %"class.std::__2::__compressed_pair.82" }
%"class.std::__2::unique_ptr.53" = type { %"class.std::__2::__compressed_pair.54" }
%"class.std::__2::__compressed_pair.54" = type { %"struct.std::__2::__compressed_pair_elem.55" }
%"struct.std::__2::__compressed_pair_elem.55" = type { %"class.draco::PointAttribute"* }
%"class.draco::PointAttribute" = type <{ %"class.draco::GeometryAttribute", %"class.std::__2::unique_ptr.63", %"class.draco::IndexTypeVector", i32, i8, [3 x i8], %"class.std::__2::unique_ptr.75", [4 x i8] }>
%"class.draco::GeometryAttribute" = type { %"class.draco::DataBuffer"*, %"struct.draco::DataBufferDescriptor", i8, i32, i8, i64, i64, i32, i32 }
%"class.draco::DataBuffer" = type { %"class.std::__2::vector.56", %"struct.draco::DataBufferDescriptor" }
%"class.std::__2::vector.56" = type { %"class.std::__2::__vector_base.57" }
%"class.std::__2::__vector_base.57" = type { i8*, i8*, %"class.std::__2::__compressed_pair.58" }
%"class.std::__2::__compressed_pair.58" = type { %"struct.std::__2::__compressed_pair_elem.59" }
%"struct.std::__2::__compressed_pair_elem.59" = type { i8* }
%"struct.draco::DataBufferDescriptor" = type { i64, i64 }
%"class.std::__2::unique_ptr.63" = type { %"class.std::__2::__compressed_pair.64" }
%"class.std::__2::__compressed_pair.64" = type { %"struct.std::__2::__compressed_pair_elem.65" }
%"struct.std::__2::__compressed_pair_elem.65" = type { %"class.draco::DataBuffer"* }
%"class.draco::IndexTypeVector" = type { %"class.std::__2::vector.68" }
%"class.std::__2::vector.68" = type { %"class.std::__2::__vector_base.69" }
%"class.std::__2::__vector_base.69" = type { %"class.draco::IndexType"*, %"class.draco::IndexType"*, %"class.std::__2::__compressed_pair.70" }
%"class.draco::IndexType" = type { i32 }
%"class.std::__2::__compressed_pair.70" = type { %"struct.std::__2::__compressed_pair_elem.71" }
%"struct.std::__2::__compressed_pair_elem.71" = type { %"class.draco::IndexType"* }
%"class.std::__2::unique_ptr.75" = type { %"class.std::__2::__compressed_pair.76" }
%"class.std::__2::__compressed_pair.76" = type { %"struct.std::__2::__compressed_pair_elem.77" }
%"struct.std::__2::__compressed_pair_elem.77" = type { %"class.draco::AttributeTransformData"* }
%"class.draco::AttributeTransformData" = type { i32, %"class.draco::DataBuffer" }
%"class.std::__2::__compressed_pair.82" = type { %"struct.std::__2::__compressed_pair_elem.83" }
%"struct.std::__2::__compressed_pair_elem.83" = type { %"class.std::__2::unique_ptr.53"* }
%"class.std::__2::vector.94" = type { %"class.std::__2::__vector_base.95" }
%"class.std::__2::__vector_base.95" = type { %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"*, %"class.std::__2::__compressed_pair.101" }
%"class.std::__2::unique_ptr.96" = type { %"class.std::__2::__compressed_pair.97" }
%"class.std::__2::__compressed_pair.97" = type { %"struct.std::__2::__compressed_pair_elem.98" }
%"struct.std::__2::__compressed_pair_elem.98" = type { %"class.draco::AttributesDecoderInterface"* }
%"class.draco::AttributesDecoderInterface" = type { i32 (...)** }
%"class.std::__2::__compressed_pair.101" = type { %"struct.std::__2::__compressed_pair_elem.102" }
%"struct.std::__2::__compressed_pair_elem.102" = type { %"class.std::__2::unique_ptr.96"* }
%"class.std::__2::vector.87" = type { %"class.std::__2::__vector_base.88" }
%"class.std::__2::__vector_base.88" = type { i32*, i32*, %"class.std::__2::__compressed_pair.89" }
%"class.std::__2::__compressed_pair.89" = type { %"struct.std::__2::__compressed_pair_elem.90" }
%"struct.std::__2::__compressed_pair_elem.90" = type { i32* }
%"class.draco::DecoderBuffer" = type { i8*, i64, i64, %"class.draco::DecoderBuffer::BitDecoder", i8, i16 }
%"class.draco::DecoderBuffer::BitDecoder" = type { i8*, i8*, i32 }
%"class.draco::DracoOptions" = type opaque
%"class.draco::Mesh" = type { %"class.draco::PointCloud", %"class.std::__2::vector.106", %"class.draco::IndexTypeVector.113" }
%"class.std::__2::vector.106" = type { %"class.std::__2::__vector_base.107" }
%"class.std::__2::__vector_base.107" = type { %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"*, %"class.std::__2::__compressed_pair.108" }
%"struct.draco::Mesh::AttributeData" = type { i32 }
%"class.std::__2::__compressed_pair.108" = type { %"struct.std::__2::__compressed_pair_elem.109" }
%"struct.std::__2::__compressed_pair_elem.109" = type { %"struct.draco::Mesh::AttributeData"* }
%"class.draco::IndexTypeVector.113" = type { %"class.std::__2::vector.114" }
%"class.std::__2::vector.114" = type { %"class.std::__2::__vector_base.115" }
%"class.std::__2::__vector_base.115" = type { %"struct.std::__2::array"*, %"struct.std::__2::array"*, %"class.std::__2::__compressed_pair.117" }
%"struct.std::__2::array" = type { [3 x %"class.draco::IndexType.116"] }
%"class.draco::IndexType.116" = type { i32 }
%"class.std::__2::__compressed_pair.117" = type { %"struct.std::__2::__compressed_pair_elem.118" }
%"struct.std::__2::__compressed_pair_elem.118" = type { %"struct.std::__2::array"* }
%"class.std::__2::unique_ptr.122" = type { %"class.std::__2::__compressed_pair.123" }
%"class.std::__2::__compressed_pair.123" = type { %"struct.std::__2::__compressed_pair_elem.124" }
%"struct.std::__2::__compressed_pair_elem.124" = type { %"class.draco::MeshEdgebreakerDecoderImplInterface"* }
%"class.draco::MeshEdgebreakerDecoderImplInterface" = type { i32 (...)** }
%"struct.std::__2::__default_init_tag" = type { i8 }
%"class.draco::MeshEdgebreakerDecoderImpl" = type { %"class.draco::MeshEdgebreakerDecoderImplInterface", %"class.draco::MeshEdgebreakerDecoder"*, %"class.std::__2::unique_ptr.127", %"class.std::__2::vector.140", %"class.std::__2::vector.87", %"class.std::__2::vector.161", %"class.std::__2::vector.168", %"class.std::__2::vector.175", %"class.std::__2::vector.140", i32, i32, i32, %"class.std::__2::vector.175", %"class.std::__2::vector.175", %"class.std::__2::vector.175", i32, %"class.std::__2::unordered_map.180", i32, %"class.std::__2::vector.87", %"class.std::__2::vector.87", %"struct.draco::MeshAttributeIndicesEncodingData", i32, %"class.std::__2::vector.207", [4 x i8], %"class.draco::MeshEdgebreakerTraversalDecoder" }
%"class.std::__2::unique_ptr.127" = type { %"class.std::__2::__compressed_pair.128" }
%"class.std::__2::__compressed_pair.128" = type { %"struct.std::__2::__compressed_pair_elem.129" }
%"struct.std::__2::__compressed_pair_elem.129" = type { %"class.draco::CornerTable"* }
%"class.draco::CornerTable" = type { %"class.draco::IndexTypeVector.130", %"class.draco::IndexTypeVector.139", %"class.draco::IndexTypeVector.148", i32, i32, i32, %"class.draco::IndexTypeVector.149", %"class.draco::ValenceCache" }
%"class.draco::IndexTypeVector.130" = type { %"class.std::__2::vector.131" }
%"class.std::__2::vector.131" = type { %"class.std::__2::__vector_base.132" }
%"class.std::__2::__vector_base.132" = type { %"class.draco::IndexType.133"*, %"class.draco::IndexType.133"*, %"class.std::__2::__compressed_pair.134" }
%"class.draco::IndexType.133" = type { i32 }
%"class.std::__2::__compressed_pair.134" = type { %"struct.std::__2::__compressed_pair_elem.135" }
%"struct.std::__2::__compressed_pair_elem.135" = type { %"class.draco::IndexType.133"* }
%"class.draco::IndexTypeVector.139" = type { %"class.std::__2::vector.140" }
%"class.draco::IndexTypeVector.148" = type { %"class.std::__2::vector.140" }
%"class.draco::IndexTypeVector.149" = type { %"class.std::__2::vector.131" }
%"class.draco::ValenceCache" = type { %"class.draco::CornerTable"*, %"class.draco::IndexTypeVector.150", %"class.draco::IndexTypeVector.158" }
%"class.draco::IndexTypeVector.150" = type { %"class.std::__2::vector.151" }
%"class.std::__2::vector.151" = type { %"class.std::__2::__vector_base.152" }
%"class.std::__2::__vector_base.152" = type { i8*, i8*, %"class.std::__2::__compressed_pair.153" }
%"class.std::__2::__compressed_pair.153" = type { %"struct.std::__2::__compressed_pair_elem.154" }
%"struct.std::__2::__compressed_pair_elem.154" = type { i8* }
%"class.draco::IndexTypeVector.158" = type { %"class.std::__2::vector.87" }
%"class.std::__2::vector.161" = type { %"class.std::__2::__vector_base.162" }
%"class.std::__2::__vector_base.162" = type { %"struct.draco::TopologySplitEventData"*, %"struct.draco::TopologySplitEventData"*, %"class.std::__2::__compressed_pair.163" }
%"struct.draco::TopologySplitEventData" = type { i32, i32, i8 }
%"class.std::__2::__compressed_pair.163" = type { %"struct.std::__2::__compressed_pair_elem.164" }
%"struct.std::__2::__compressed_pair_elem.164" = type { %"struct.draco::TopologySplitEventData"* }
%"class.std::__2::vector.168" = type { %"class.std::__2::__vector_base.169" }
%"class.std::__2::__vector_base.169" = type { %"struct.draco::HoleEventData"*, %"struct.draco::HoleEventData"*, %"class.std::__2::__compressed_pair.170" }
%"struct.draco::HoleEventData" = type { i32 }
%"class.std::__2::__compressed_pair.170" = type { %"struct.std::__2::__compressed_pair_elem.171" }
%"struct.std::__2::__compressed_pair_elem.171" = type { %"struct.draco::HoleEventData"* }
%"class.std::__2::vector.140" = type { %"class.std::__2::__vector_base.141" }
%"class.std::__2::__vector_base.141" = type { %"class.draco::IndexType.142"*, %"class.draco::IndexType.142"*, %"class.std::__2::__compressed_pair.143" }
%"class.draco::IndexType.142" = type { i32 }
%"class.std::__2::__compressed_pair.143" = type { %"struct.std::__2::__compressed_pair_elem.144" }
%"struct.std::__2::__compressed_pair_elem.144" = type { %"class.draco::IndexType.142"* }
%"class.std::__2::vector.175" = type { i32*, i32, %"class.std::__2::__compressed_pair.176" }
%"class.std::__2::__compressed_pair.176" = type { %"struct.std::__2::__compressed_pair_elem.5" }
%"class.std::__2::unordered_map.180" = type { %"class.std::__2::__hash_table.181" }
%"class.std::__2::__hash_table.181" = type { %"class.std::__2::unique_ptr.182", %"class.std::__2::__compressed_pair.192", %"class.std::__2::__compressed_pair.197", %"class.std::__2::__compressed_pair.202" }
%"class.std::__2::unique_ptr.182" = type { %"class.std::__2::__compressed_pair.183" }
%"class.std::__2::__compressed_pair.183" = type { %"struct.std::__2::__compressed_pair_elem.184", %"struct.std::__2::__compressed_pair_elem.186" }
%"struct.std::__2::__compressed_pair_elem.184" = type { %"struct.std::__2::__hash_node_base.185"** }
%"struct.std::__2::__hash_node_base.185" = type { %"struct.std::__2::__hash_node_base.185"* }
%"struct.std::__2::__compressed_pair_elem.186" = type { %"class.std::__2::__bucket_list_deallocator.187" }
%"class.std::__2::__bucket_list_deallocator.187" = type { %"class.std::__2::__compressed_pair.188" }
%"class.std::__2::__compressed_pair.188" = type { %"struct.std::__2::__compressed_pair_elem.5" }
%"class.std::__2::__compressed_pair.192" = type { %"struct.std::__2::__compressed_pair_elem.193" }
%"struct.std::__2::__compressed_pair_elem.193" = type { %"struct.std::__2::__hash_node_base.185" }
%"class.std::__2::__compressed_pair.197" = type { %"struct.std::__2::__compressed_pair_elem.5" }
%"class.std::__2::__compressed_pair.202" = type { %"struct.std::__2::__compressed_pair_elem.15" }
%"struct.draco::MeshAttributeIndicesEncodingData" = type { %"class.std::__2::vector.140", %"class.std::__2::vector.87", i32 }
%"class.std::__2::vector.207" = type { %"class.std::__2::__vector_base.208" }
%"class.std::__2::__vector_base.208" = type { %"struct.draco::MeshEdgebreakerDecoderImpl<draco::MeshEdgebreakerTraversalDecoder>::AttributeData"*, %"struct.draco::MeshEdgebreakerDecoderImpl<draco::MeshEdgebreakerTraversalDecoder>::AttributeData"*, %"class.std::__2::__compressed_pair.209" }
%"struct.draco::MeshEdgebreakerDecoderImpl<draco::MeshEdgebreakerTraversalDecoder>::AttributeData" = type opaque
%"class.std::__2::__compressed_pair.209" = type { %"struct.std::__2::__compressed_pair_elem.210" }
%"struct.std::__2::__compressed_pair_elem.210" = type { %"struct.draco::MeshEdgebreakerDecoderImpl<draco::MeshEdgebreakerTraversalDecoder>::AttributeData"* }
%"class.draco::MeshEdgebreakerTraversalDecoder" = type <{ %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer", %"class.draco::RAnsBitDecoder", %"class.draco::DecoderBuffer", %"class.std::__2::unique_ptr.214", i32, %"class.draco::MeshEdgebreakerDecoderImplInterface"*, [4 x i8] }>
%"class.draco::RAnsBitDecoder" = type <{ %"struct.draco::AnsDecoder", i8, [3 x i8] }>
%"struct.draco::AnsDecoder" = type { i8*, i32, i32 }
%"class.std::__2::unique_ptr.214" = type { %"class.std::__2::__compressed_pair.215" }
%"class.std::__2::__compressed_pair.215" = type { %"struct.std::__2::__compressed_pair_elem.216" }
%"struct.std::__2::__compressed_pair_elem.216" = type { %"class.draco::RAnsBitDecoder"* }
%"class.draco::MeshEdgebreakerDecoderImpl.219" = type { %"class.draco::MeshEdgebreakerDecoderImplInterface", %"class.draco::MeshEdgebreakerDecoder"*, %"class.std::__2::unique_ptr.127", %"class.std::__2::vector.140", %"class.std::__2::vector.87", %"class.std::__2::vector.161", %"class.std::__2::vector.168", %"class.std::__2::vector.175", %"class.std::__2::vector.140", i32, i32, i32, %"class.std::__2::vector.175", %"class.std::__2::vector.175", %"class.std::__2::vector.175", i32, %"class.std::__2::unordered_map.180", i32, %"class.std::__2::vector.87", %"class.std::__2::vector.87", %"struct.draco::MeshAttributeIndicesEncodingData", i32, %"class.std::__2::vector.220", [4 x i8], %"class.draco::MeshEdgebreakerTraversalPredictiveDecoder" }
%"class.std::__2::vector.220" = type { %"class.std::__2::__vector_base.221" }
%"class.std::__2::__vector_base.221" = type { %"struct.draco::MeshEdgebreakerDecoderImpl<draco::MeshEdgebreakerTraversalPredictiveDecoder>::AttributeData"*, %"struct.draco::MeshEdgebreakerDecoderImpl<draco::MeshEdgebreakerTraversalPredictiveDecoder>::AttributeData"*, %"class.std::__2::__compressed_pair.222" }
%"struct.draco::MeshEdgebreakerDecoderImpl<draco::MeshEdgebreakerTraversalPredictiveDecoder>::AttributeData" = type opaque
%"class.std::__2::__compressed_pair.222" = type { %"struct.std::__2::__compressed_pair_elem.223" }
%"struct.std::__2::__compressed_pair_elem.223" = type { %"struct.draco::MeshEdgebreakerDecoderImpl<draco::MeshEdgebreakerTraversalPredictiveDecoder>::AttributeData"* }
%"class.draco::MeshEdgebreakerTraversalPredictiveDecoder" = type { %"class.draco::MeshEdgebreakerTraversalDecoder.base", %"class.draco::CornerTable"*, i32, %"class.std::__2::vector.87", %"class.draco::RAnsBitDecoder", i32, i32 }
%"class.draco::MeshEdgebreakerTraversalDecoder.base" = type <{ %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer", %"class.draco::RAnsBitDecoder", %"class.draco::DecoderBuffer", %"class.std::__2::unique_ptr.214", i32, %"class.draco::MeshEdgebreakerDecoderImplInterface"* }>
%"class.draco::MeshEdgebreakerDecoderImpl.227" = type { %"class.draco::MeshEdgebreakerDecoderImplInterface", %"class.draco::MeshEdgebreakerDecoder"*, %"class.std::__2::unique_ptr.127", %"class.std::__2::vector.140", %"class.std::__2::vector.87", %"class.std::__2::vector.161", %"class.std::__2::vector.168", %"class.std::__2::vector.175", %"class.std::__2::vector.140", i32, i32, i32, %"class.std::__2::vector.175", %"class.std::__2::vector.175", %"class.std::__2::vector.175", i32, %"class.std::__2::unordered_map.180", i32, %"class.std::__2::vector.87", %"class.std::__2::vector.87", %"struct.draco::MeshAttributeIndicesEncodingData", i32, %"class.std::__2::vector.228", [4 x i8], %"class.draco::MeshEdgebreakerTraversalValenceDecoder" }
%"class.std::__2::vector.228" = type { %"class.std::__2::__vector_base.229" }
%"class.std::__2::__vector_base.229" = type { %"struct.draco::MeshEdgebreakerDecoderImpl<draco::MeshEdgebreakerTraversalValenceDecoder>::AttributeData"*, %"struct.draco::MeshEdgebreakerDecoderImpl<draco::MeshEdgebreakerTraversalValenceDecoder>::AttributeData"*, %"class.std::__2::__compressed_pair.230" }
%"struct.draco::MeshEdgebreakerDecoderImpl<draco::MeshEdgebreakerTraversalValenceDecoder>::AttributeData" = type opaque
%"class.std::__2::__compressed_pair.230" = type { %"struct.std::__2::__compressed_pair_elem.231" }
%"struct.std::__2::__compressed_pair_elem.231" = type { %"struct.draco::MeshEdgebreakerDecoderImpl<draco::MeshEdgebreakerTraversalValenceDecoder>::AttributeData"* }
%"class.draco::MeshEdgebreakerTraversalValenceDecoder" = type { %"class.draco::MeshEdgebreakerTraversalDecoder.base", %"class.draco::CornerTable"*, i32, %"class.draco::IndexTypeVector.158", i32, i32, i32, i32, %"class.std::__2::vector.235", %"class.std::__2::vector.87" }
%"class.std::__2::vector.235" = type { %"class.std::__2::__vector_base.236" }
%"class.std::__2::__vector_base.236" = type { %"class.std::__2::vector.237"*, %"class.std::__2::vector.237"*, %"class.std::__2::__compressed_pair.244" }
%"class.std::__2::vector.237" = type { %"class.std::__2::__vector_base.238" }
%"class.std::__2::__vector_base.238" = type { i32*, i32*, %"class.std::__2::__compressed_pair.239" }
%"class.std::__2::__compressed_pair.239" = type { %"struct.std::__2::__compressed_pair_elem.240" }
%"struct.std::__2::__compressed_pair_elem.240" = type { i32* }
%"class.std::__2::__compressed_pair.244" = type { %"struct.std::__2::__compressed_pair_elem.245" }
%"struct.std::__2::__compressed_pair_elem.245" = type { %"class.std::__2::vector.237"* }
%"struct.std::__2::default_delete.126" = type { i8 }
%"class.draco::MeshAttributeCornerTable" = type { %"class.std::__2::vector.175", %"class.std::__2::vector.175", i8, %"class.std::__2::vector.131", %"class.std::__2::vector.140", %"class.std::__2::vector.68", %"class.draco::CornerTable"*, %"class.draco::ValenceCache.249" }
%"class.draco::ValenceCache.249" = type { %"class.draco::MeshAttributeCornerTable"*, %"class.draco::IndexTypeVector.150", %"class.draco::IndexTypeVector.158" }
%"struct.std::__2::__compressed_pair_elem.125" = type { i8 }
%"class.std::__2::allocator.92" = type { i8 }
%"struct.std::__2::integral_constant" = type { i8 }
%"struct.std::__2::__has_destroy" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.91" = type { i8 }
%"class.std::__2::allocator.104" = type { i8 }
%"struct.std::__2::__has_destroy.250" = type { i8 }
%"struct.std::__2::default_delete.100" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.99" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.103" = type { i8 }

$_ZNSt3__210unique_ptrIN5draco35MeshEdgebreakerDecoderImplInterfaceENS_14default_deleteIS2_EEEC2ILb1EvEEv = comdat any

$_ZNKSt3__210unique_ptrIN5draco35MeshEdgebreakerDecoderImplInterfaceENS_14default_deleteIS2_EEEptEv = comdat any

$_ZN5draco17PointCloudDecoder6bufferEv = comdat any

$_ZN5draco13DecoderBuffer6DecodeIhEEbPT_ = comdat any

$_ZNSt3__210unique_ptrIN5draco35MeshEdgebreakerDecoderImplInterfaceENS_14default_deleteIS2_EEEaSEDn = comdat any

$_ZNSt3__210unique_ptrIN5draco35MeshEdgebreakerDecoderImplInterfaceENS_14default_deleteIS2_EEEC2ILb1EvEEPS2_ = comdat any

$_ZNSt3__210unique_ptrIN5draco35MeshEdgebreakerDecoderImplInterfaceENS_14default_deleteIS2_EEEaSEOS5_ = comdat any

$_ZNSt3__210unique_ptrIN5draco35MeshEdgebreakerDecoderImplInterfaceENS_14default_deleteIS2_EEED2Ev = comdat any

$_ZNKSt3__210unique_ptrIN5draco35MeshEdgebreakerDecoderImplInterfaceENS_14default_deleteIS2_EEEcvbEv = comdat any

$_ZN5draco22MeshEdgebreakerDecoderD2Ev = comdat any

$_ZN5draco22MeshEdgebreakerDecoderD0Ev = comdat any

$_ZNK5draco11MeshDecoder15GetGeometryTypeEv = comdat any

$_ZNK5draco22MeshEdgebreakerDecoder14GetCornerTableEv = comdat any

$_ZNK5draco22MeshEdgebreakerDecoder23GetAttributeCornerTableEi = comdat any

$_ZNK5draco22MeshEdgebreakerDecoder24GetAttributeEncodingDataEi = comdat any

$_ZNSt3__217__compressed_pairIPN5draco35MeshEdgebreakerDecoderImplInterfaceENS_14default_deleteIS2_EEEC2IS3_NS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__27forwardIPN5draco35MeshEdgebreakerDecoderImplInterfaceEEEOT_RNS_16remove_referenceIS4_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco35MeshEdgebreakerDecoderImplInterfaceELi0ELb0EEC2IS3_vEEOT_ = comdat any

$_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco35MeshEdgebreakerDecoderImplInterfaceEEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZN5draco11MeshDecoderD2Ev = comdat any

$_ZN5draco17PointCloudDecoderD2Ev = comdat any

$_ZNSt3__26vectorIiNS_9allocatorIiEEED2Ev = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEED2Ev = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEE17__annotate_deleteEv = comdat any

$_ZNSt3__213__vector_baseIiNS_9allocatorIiEEED2Ev = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEE31__annotate_contiguous_containerEPKvS5_S5_S5_ = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEE8capacityEv = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEE4sizeEv = comdat any

$_ZNSt3__212__to_addressIiEEPT_S2_ = comdat any

$_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPiNS_9allocatorIiEEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPiLi0ELb0EE5__getEv = comdat any

$_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE5clearEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIiEEE10deallocateERS2_Pim = comdat any

$_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE7__allocEv = comdat any

$_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE17__destruct_at_endEPi = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIiEEE7destroyIiEEvRS2_PT_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIiEEE9__destroyIiEEvNS_17integral_constantIbLb1EEERS2_PT_ = comdat any

$_ZNSt3__29allocatorIiE7destroyEPi = comdat any

$_ZNSt3__29allocatorIiE10deallocateEPim = comdat any

$_ZNSt3__219__libcpp_deallocateEPvmm = comdat any

$_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm = comdat any

$_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm = comdat any

$_ZNSt3__217_DeallocateCaller9__do_callEPv = comdat any

$_ZNSt3__217__compressed_pairIPiNS_9allocatorIiEEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIiEELi1ELb1EE5__getEv = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__annotate_deleteEv = comdat any

$_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEED2Ev = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE31__annotate_contiguous_containerEPKvSB_SB_SB_ = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv = comdat any

$_ZNSt3__212__to_addressINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEEPT_S8_ = comdat any

$_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5clearEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE10deallocateERS8_PS7_m = comdat any

$_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv = comdat any

$_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__destruct_at_endEPS6_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE7destroyIS7_EEvRS8_PT_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE9__destroyIS7_EEvNS_17integral_constantIbLb1EEERS8_PT_ = comdat any

$_ZNSt3__29allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEE7destroyEPS6_ = comdat any

$_ZNSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEED2Ev = comdat any

$_ZNSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE5resetEPS2_ = comdat any

$_ZNSt3__217__compressed_pairIPN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE6secondEv = comdat any

$_ZNKSt3__214default_deleteIN5draco26AttributesDecoderInterfaceEEclEPS2_ = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco26AttributesDecoderInterfaceELi0ELb0EE5__getEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco26AttributesDecoderInterfaceEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__29allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEE10deallocateEPS6_m = comdat any

$_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEELi1ELb1EE5__getEv = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco35MeshEdgebreakerDecoderImplInterfaceENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPN5draco35MeshEdgebreakerDecoderImplInterfaceELi0ELb0EE5__getEv = comdat any

$_ZNSt3__210unique_ptrIN5draco35MeshEdgebreakerDecoderImplInterfaceENS_14default_deleteIS2_EEE5resetEPS2_ = comdat any

$_ZNSt3__217__compressed_pairIPN5draco35MeshEdgebreakerDecoderImplInterfaceENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco35MeshEdgebreakerDecoderImplInterfaceENS_14default_deleteIS2_EEE6secondEv = comdat any

$_ZNKSt3__214default_deleteIN5draco35MeshEdgebreakerDecoderImplInterfaceEEclEPS2_ = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco35MeshEdgebreakerDecoderImplInterfaceELi0ELb0EE5__getEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco35MeshEdgebreakerDecoderImplInterfaceEEELi1ELb1EE5__getEv = comdat any

$_ZN5draco13DecoderBuffer4PeekIhEEbPT_ = comdat any

$_ZNSt3__217__compressed_pairIPN5draco35MeshEdgebreakerDecoderImplInterfaceENS_14default_deleteIS2_EEEC2IRS3_NS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__27forwardIRPN5draco35MeshEdgebreakerDecoderImplInterfaceEEEOT_RNS_16remove_referenceIS5_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco35MeshEdgebreakerDecoderImplInterfaceELi0ELb0EEC2IRS3_vEEOT_ = comdat any

$_ZNSt3__210unique_ptrIN5draco35MeshEdgebreakerDecoderImplInterfaceENS_14default_deleteIS2_EEE7releaseEv = comdat any

$_ZNSt3__27forwardINS_14default_deleteIN5draco35MeshEdgebreakerDecoderImplInterfaceEEEEEOT_RNS_16remove_referenceIS5_E4typeE = comdat any

$_ZNSt3__210unique_ptrIN5draco35MeshEdgebreakerDecoderImplInterfaceENS_14default_deleteIS2_EEE11get_deleterEv = comdat any

@_ZTVN5draco22MeshEdgebreakerDecoderE = hidden unnamed_addr constant { [15 x i8*] } { [15 x i8*] [i8* null, i8* null, i8* bitcast (%"class.draco::MeshEdgebreakerDecoder"* (%"class.draco::MeshEdgebreakerDecoder"*)* @_ZN5draco22MeshEdgebreakerDecoderD2Ev to i8*), i8* bitcast (void (%"class.draco::MeshEdgebreakerDecoder"*)* @_ZN5draco22MeshEdgebreakerDecoderD0Ev to i8*), i8* bitcast (i32 (%"class.draco::MeshDecoder"*)* @_ZNK5draco11MeshDecoder15GetGeometryTypeEv to i8*), i8* bitcast (i1 (%"class.draco::MeshEdgebreakerDecoder"*)* @_ZN5draco22MeshEdgebreakerDecoder17InitializeDecoderEv to i8*), i8* bitcast (i1 (%"class.draco::MeshEdgebreakerDecoder"*, i32)* @_ZN5draco22MeshEdgebreakerDecoder23CreateAttributesDecoderEi to i8*), i8* bitcast (i1 (%"class.draco::MeshDecoder"*)* @_ZN5draco11MeshDecoder18DecodeGeometryDataEv to i8*), i8* bitcast (i1 (%"class.draco::PointCloudDecoder"*)* @_ZN5draco17PointCloudDecoder21DecodePointAttributesEv to i8*), i8* bitcast (i1 (%"class.draco::PointCloudDecoder"*)* @_ZN5draco17PointCloudDecoder19DecodeAllAttributesEv to i8*), i8* bitcast (i1 (%"class.draco::MeshEdgebreakerDecoder"*)* @_ZN5draco22MeshEdgebreakerDecoder19OnAttributesDecodedEv to i8*), i8* bitcast (%"class.draco::CornerTable"* (%"class.draco::MeshEdgebreakerDecoder"*)* @_ZNK5draco22MeshEdgebreakerDecoder14GetCornerTableEv to i8*), i8* bitcast (%"class.draco::MeshAttributeCornerTable"* (%"class.draco::MeshEdgebreakerDecoder"*, i32)* @_ZNK5draco22MeshEdgebreakerDecoder23GetAttributeCornerTableEi to i8*), i8* bitcast (%"struct.draco::MeshAttributeIndicesEncodingData"* (%"class.draco::MeshEdgebreakerDecoder"*, i32)* @_ZNK5draco22MeshEdgebreakerDecoder24GetAttributeEncodingDataEi to i8*), i8* bitcast (i1 (%"class.draco::MeshEdgebreakerDecoder"*)* @_ZN5draco22MeshEdgebreakerDecoder18DecodeConnectivityEv to i8*)] }, align 4
@_ZTVN5draco17PointCloudDecoderE = external unnamed_addr constant { [11 x i8*] }, align 4

@_ZN5draco22MeshEdgebreakerDecoderC1Ev = hidden unnamed_addr alias %"class.draco::MeshEdgebreakerDecoder"* (%"class.draco::MeshEdgebreakerDecoder"*), %"class.draco::MeshEdgebreakerDecoder"* (%"class.draco::MeshEdgebreakerDecoder"*)* @_ZN5draco22MeshEdgebreakerDecoderC2Ev

; Function Attrs: noinline nounwind optnone
define hidden %"class.draco::MeshEdgebreakerDecoder"* @_ZN5draco22MeshEdgebreakerDecoderC2Ev(%"class.draco::MeshEdgebreakerDecoder"* returned %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %"class.draco::MeshEdgebreakerDecoder"*, align 4
  store %"class.draco::MeshEdgebreakerDecoder"* %this, %"class.draco::MeshEdgebreakerDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::MeshEdgebreakerDecoder"*, %"class.draco::MeshEdgebreakerDecoder"** %this.addr, align 4
  %0 = bitcast %"class.draco::MeshEdgebreakerDecoder"* %this1 to %"class.draco::MeshDecoder"*
  %call = call %"class.draco::MeshDecoder"* @_ZN5draco11MeshDecoderC2Ev(%"class.draco::MeshDecoder"* %0)
  %1 = bitcast %"class.draco::MeshEdgebreakerDecoder"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [15 x i8*] }, { [15 x i8*] }* @_ZTVN5draco22MeshEdgebreakerDecoderE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %impl_ = getelementptr inbounds %"class.draco::MeshEdgebreakerDecoder", %"class.draco::MeshEdgebreakerDecoder"* %this1, i32 0, i32 1
  %call2 = call %"class.std::__2::unique_ptr.122"* @_ZNSt3__210unique_ptrIN5draco35MeshEdgebreakerDecoderImplInterfaceENS_14default_deleteIS2_EEEC2ILb1EvEEv(%"class.std::__2::unique_ptr.122"* %impl_) #5
  ret %"class.draco::MeshEdgebreakerDecoder"* %this1
}

declare %"class.draco::MeshDecoder"* @_ZN5draco11MeshDecoderC2Ev(%"class.draco::MeshDecoder"* returned) unnamed_addr #1

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.122"* @_ZNSt3__210unique_ptrIN5draco35MeshEdgebreakerDecoderImplInterfaceENS_14default_deleteIS2_EEEC2ILb1EvEEv(%"class.std::__2::unique_ptr.122"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.122"*, align 4
  %ref.tmp = alloca %"class.draco::MeshEdgebreakerDecoderImplInterface"*, align 4
  %ref.tmp2 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::unique_ptr.122"* %this, %"class.std::__2::unique_ptr.122"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.122"*, %"class.std::__2::unique_ptr.122"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.122", %"class.std::__2::unique_ptr.122"* %this1, i32 0, i32 0
  store %"class.draco::MeshEdgebreakerDecoderImplInterface"* null, %"class.draco::MeshEdgebreakerDecoderImplInterface"** %ref.tmp, align 4
  %call = call %"class.std::__2::__compressed_pair.123"* @_ZNSt3__217__compressed_pairIPN5draco35MeshEdgebreakerDecoderImplInterfaceENS_14default_deleteIS2_EEEC2IS3_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.123"* %__ptr_, %"class.draco::MeshEdgebreakerDecoderImplInterface"** nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  ret %"class.std::__2::unique_ptr.122"* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden zeroext i1 @_ZN5draco22MeshEdgebreakerDecoder23CreateAttributesDecoderEi(%"class.draco::MeshEdgebreakerDecoder"* %this, i32 %att_decoder_id) unnamed_addr #0 {
entry:
  %this.addr = alloca %"class.draco::MeshEdgebreakerDecoder"*, align 4
  %att_decoder_id.addr = alloca i32, align 4
  store %"class.draco::MeshEdgebreakerDecoder"* %this, %"class.draco::MeshEdgebreakerDecoder"** %this.addr, align 4
  store i32 %att_decoder_id, i32* %att_decoder_id.addr, align 4
  %this1 = load %"class.draco::MeshEdgebreakerDecoder"*, %"class.draco::MeshEdgebreakerDecoder"** %this.addr, align 4
  %impl_ = getelementptr inbounds %"class.draco::MeshEdgebreakerDecoder", %"class.draco::MeshEdgebreakerDecoder"* %this1, i32 0, i32 1
  %call = call %"class.draco::MeshEdgebreakerDecoderImplInterface"* @_ZNKSt3__210unique_ptrIN5draco35MeshEdgebreakerDecoderImplInterfaceENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr.122"* %impl_) #5
  %0 = load i32, i32* %att_decoder_id.addr, align 4
  %1 = bitcast %"class.draco::MeshEdgebreakerDecoderImplInterface"* %call to i1 (%"class.draco::MeshEdgebreakerDecoderImplInterface"*, i32)***
  %vtable = load i1 (%"class.draco::MeshEdgebreakerDecoderImplInterface"*, i32)**, i1 (%"class.draco::MeshEdgebreakerDecoderImplInterface"*, i32)*** %1, align 4
  %vfn = getelementptr inbounds i1 (%"class.draco::MeshEdgebreakerDecoderImplInterface"*, i32)*, i1 (%"class.draco::MeshEdgebreakerDecoderImplInterface"*, i32)** %vtable, i64 5
  %2 = load i1 (%"class.draco::MeshEdgebreakerDecoderImplInterface"*, i32)*, i1 (%"class.draco::MeshEdgebreakerDecoderImplInterface"*, i32)** %vfn, align 4
  %call2 = call zeroext i1 %2(%"class.draco::MeshEdgebreakerDecoderImplInterface"* %call, i32 %0)
  ret i1 %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::MeshEdgebreakerDecoderImplInterface"* @_ZNKSt3__210unique_ptrIN5draco35MeshEdgebreakerDecoderImplInterfaceENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr.122"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.122"*, align 4
  store %"class.std::__2::unique_ptr.122"* %this, %"class.std::__2::unique_ptr.122"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.122"*, %"class.std::__2::unique_ptr.122"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.122", %"class.std::__2::unique_ptr.122"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::MeshEdgebreakerDecoderImplInterface"** @_ZNKSt3__217__compressed_pairIPN5draco35MeshEdgebreakerDecoderImplInterfaceENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.123"* %__ptr_) #5
  %0 = load %"class.draco::MeshEdgebreakerDecoderImplInterface"*, %"class.draco::MeshEdgebreakerDecoderImplInterface"** %call, align 4
  ret %"class.draco::MeshEdgebreakerDecoderImplInterface"* %0
}

; Function Attrs: noinline nounwind optnone
define hidden zeroext i1 @_ZN5draco22MeshEdgebreakerDecoder17InitializeDecoderEv(%"class.draco::MeshEdgebreakerDecoder"* %this) unnamed_addr #0 {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::MeshEdgebreakerDecoder"*, align 4
  %traversal_decoder_type = alloca i8, align 1
  %ref.tmp = alloca %"class.std::__2::unique_ptr.122", align 4
  %ref.tmp14 = alloca %"class.std::__2::unique_ptr.122", align 4
  %ref.tmp25 = alloca %"class.std::__2::unique_ptr.122", align 4
  store %"class.draco::MeshEdgebreakerDecoder"* %this, %"class.draco::MeshEdgebreakerDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::MeshEdgebreakerDecoder"*, %"class.draco::MeshEdgebreakerDecoder"** %this.addr, align 4
  %0 = bitcast %"class.draco::MeshEdgebreakerDecoder"* %this1 to %"class.draco::PointCloudDecoder"*
  %call = call %"class.draco::DecoderBuffer"* @_ZN5draco17PointCloudDecoder6bufferEv(%"class.draco::PointCloudDecoder"* %0)
  %call2 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIhEEbPT_(%"class.draco::DecoderBuffer"* %call, i8* %traversal_decoder_type)
  br i1 %call2, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %impl_ = getelementptr inbounds %"class.draco::MeshEdgebreakerDecoder", %"class.draco::MeshEdgebreakerDecoder"* %this1, i32 0, i32 1
  %call3 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.122"* @_ZNSt3__210unique_ptrIN5draco35MeshEdgebreakerDecoderImplInterfaceENS_14default_deleteIS2_EEEaSEDn(%"class.std::__2::unique_ptr.122"* %impl_, i8* null) #5
  %1 = load i8, i8* %traversal_decoder_type, align 1
  %conv = zext i8 %1 to i32
  %cmp = icmp eq i32 %conv, 0
  br i1 %cmp, label %if.then4, label %if.else

if.then4:                                         ; preds = %if.end
  %call5 = call noalias nonnull i8* @_Znwm(i32 384) #6
  %2 = bitcast i8* %call5 to %"class.draco::MeshEdgebreakerDecoderImpl"*
  %call6 = call %"class.draco::MeshEdgebreakerDecoderImpl"* @_ZN5draco26MeshEdgebreakerDecoderImplINS_31MeshEdgebreakerTraversalDecoderEEC1Ev(%"class.draco::MeshEdgebreakerDecoderImpl"* %2)
  %3 = bitcast %"class.draco::MeshEdgebreakerDecoderImpl"* %2 to %"class.draco::MeshEdgebreakerDecoderImplInterface"*
  %call7 = call %"class.std::__2::unique_ptr.122"* @_ZNSt3__210unique_ptrIN5draco35MeshEdgebreakerDecoderImplInterfaceENS_14default_deleteIS2_EEEC2ILb1EvEEPS2_(%"class.std::__2::unique_ptr.122"* %ref.tmp, %"class.draco::MeshEdgebreakerDecoderImplInterface"* %3) #5
  %impl_8 = getelementptr inbounds %"class.draco::MeshEdgebreakerDecoder", %"class.draco::MeshEdgebreakerDecoder"* %this1, i32 0, i32 1
  %call9 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.122"* @_ZNSt3__210unique_ptrIN5draco35MeshEdgebreakerDecoderImplInterfaceENS_14default_deleteIS2_EEEaSEOS5_(%"class.std::__2::unique_ptr.122"* %impl_8, %"class.std::__2::unique_ptr.122"* nonnull align 4 dereferenceable(4) %ref.tmp) #5
  %call10 = call %"class.std::__2::unique_ptr.122"* @_ZNSt3__210unique_ptrIN5draco35MeshEdgebreakerDecoderImplInterfaceENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.122"* %ref.tmp) #5
  br label %if.end34

if.else:                                          ; preds = %if.end
  %4 = load i8, i8* %traversal_decoder_type, align 1
  %conv11 = zext i8 %4 to i32
  %cmp12 = icmp eq i32 %conv11, 1
  br i1 %cmp12, label %if.then13, label %if.else21

if.then13:                                        ; preds = %if.else
  %call15 = call noalias nonnull i8* @_Znwm(i32 424) #6
  %5 = bitcast i8* %call15 to %"class.draco::MeshEdgebreakerDecoderImpl.219"*
  %call16 = call %"class.draco::MeshEdgebreakerDecoderImpl.219"* @_ZN5draco26MeshEdgebreakerDecoderImplINS_41MeshEdgebreakerTraversalPredictiveDecoderEEC1Ev(%"class.draco::MeshEdgebreakerDecoderImpl.219"* %5)
  %6 = bitcast %"class.draco::MeshEdgebreakerDecoderImpl.219"* %5 to %"class.draco::MeshEdgebreakerDecoderImplInterface"*
  %call17 = call %"class.std::__2::unique_ptr.122"* @_ZNSt3__210unique_ptrIN5draco35MeshEdgebreakerDecoderImplInterfaceENS_14default_deleteIS2_EEEC2ILb1EvEEPS2_(%"class.std::__2::unique_ptr.122"* %ref.tmp14, %"class.draco::MeshEdgebreakerDecoderImplInterface"* %6) #5
  %impl_18 = getelementptr inbounds %"class.draco::MeshEdgebreakerDecoder", %"class.draco::MeshEdgebreakerDecoder"* %this1, i32 0, i32 1
  %call19 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.122"* @_ZNSt3__210unique_ptrIN5draco35MeshEdgebreakerDecoderImplInterfaceENS_14default_deleteIS2_EEEaSEOS5_(%"class.std::__2::unique_ptr.122"* %impl_18, %"class.std::__2::unique_ptr.122"* nonnull align 4 dereferenceable(4) %ref.tmp14) #5
  %call20 = call %"class.std::__2::unique_ptr.122"* @_ZNSt3__210unique_ptrIN5draco35MeshEdgebreakerDecoderImplInterfaceENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.122"* %ref.tmp14) #5
  br label %if.end33

if.else21:                                        ; preds = %if.else
  %7 = load i8, i8* %traversal_decoder_type, align 1
  %conv22 = zext i8 %7 to i32
  %cmp23 = icmp eq i32 %conv22, 2
  br i1 %cmp23, label %if.then24, label %if.end32

if.then24:                                        ; preds = %if.else21
  %call26 = call noalias nonnull i8* @_Znwm(i32 440) #6
  %8 = bitcast i8* %call26 to %"class.draco::MeshEdgebreakerDecoderImpl.227"*
  %call27 = call %"class.draco::MeshEdgebreakerDecoderImpl.227"* @_ZN5draco26MeshEdgebreakerDecoderImplINS_38MeshEdgebreakerTraversalValenceDecoderEEC1Ev(%"class.draco::MeshEdgebreakerDecoderImpl.227"* %8)
  %9 = bitcast %"class.draco::MeshEdgebreakerDecoderImpl.227"* %8 to %"class.draco::MeshEdgebreakerDecoderImplInterface"*
  %call28 = call %"class.std::__2::unique_ptr.122"* @_ZNSt3__210unique_ptrIN5draco35MeshEdgebreakerDecoderImplInterfaceENS_14default_deleteIS2_EEEC2ILb1EvEEPS2_(%"class.std::__2::unique_ptr.122"* %ref.tmp25, %"class.draco::MeshEdgebreakerDecoderImplInterface"* %9) #5
  %impl_29 = getelementptr inbounds %"class.draco::MeshEdgebreakerDecoder", %"class.draco::MeshEdgebreakerDecoder"* %this1, i32 0, i32 1
  %call30 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.122"* @_ZNSt3__210unique_ptrIN5draco35MeshEdgebreakerDecoderImplInterfaceENS_14default_deleteIS2_EEEaSEOS5_(%"class.std::__2::unique_ptr.122"* %impl_29, %"class.std::__2::unique_ptr.122"* nonnull align 4 dereferenceable(4) %ref.tmp25) #5
  %call31 = call %"class.std::__2::unique_ptr.122"* @_ZNSt3__210unique_ptrIN5draco35MeshEdgebreakerDecoderImplInterfaceENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.122"* %ref.tmp25) #5
  br label %if.end32

if.end32:                                         ; preds = %if.then24, %if.else21
  br label %if.end33

if.end33:                                         ; preds = %if.end32, %if.then13
  br label %if.end34

if.end34:                                         ; preds = %if.end33, %if.then4
  %impl_35 = getelementptr inbounds %"class.draco::MeshEdgebreakerDecoder", %"class.draco::MeshEdgebreakerDecoder"* %this1, i32 0, i32 1
  %call36 = call zeroext i1 @_ZNKSt3__210unique_ptrIN5draco35MeshEdgebreakerDecoderImplInterfaceENS_14default_deleteIS2_EEEcvbEv(%"class.std::__2::unique_ptr.122"* %impl_35) #5
  br i1 %call36, label %if.end38, label %if.then37

if.then37:                                        ; preds = %if.end34
  store i1 false, i1* %retval, align 1
  br label %return

if.end38:                                         ; preds = %if.end34
  %impl_39 = getelementptr inbounds %"class.draco::MeshEdgebreakerDecoder", %"class.draco::MeshEdgebreakerDecoder"* %this1, i32 0, i32 1
  %call40 = call %"class.draco::MeshEdgebreakerDecoderImplInterface"* @_ZNKSt3__210unique_ptrIN5draco35MeshEdgebreakerDecoderImplInterfaceENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr.122"* %impl_39) #5
  %10 = bitcast %"class.draco::MeshEdgebreakerDecoderImplInterface"* %call40 to i1 (%"class.draco::MeshEdgebreakerDecoderImplInterface"*, %"class.draco::MeshEdgebreakerDecoder"*)***
  %vtable = load i1 (%"class.draco::MeshEdgebreakerDecoderImplInterface"*, %"class.draco::MeshEdgebreakerDecoder"*)**, i1 (%"class.draco::MeshEdgebreakerDecoderImplInterface"*, %"class.draco::MeshEdgebreakerDecoder"*)*** %10, align 4
  %vfn = getelementptr inbounds i1 (%"class.draco::MeshEdgebreakerDecoderImplInterface"*, %"class.draco::MeshEdgebreakerDecoder"*)*, i1 (%"class.draco::MeshEdgebreakerDecoderImplInterface"*, %"class.draco::MeshEdgebreakerDecoder"*)** %vtable, i64 2
  %11 = load i1 (%"class.draco::MeshEdgebreakerDecoderImplInterface"*, %"class.draco::MeshEdgebreakerDecoder"*)*, i1 (%"class.draco::MeshEdgebreakerDecoderImplInterface"*, %"class.draco::MeshEdgebreakerDecoder"*)** %vfn, align 4
  %call41 = call zeroext i1 %11(%"class.draco::MeshEdgebreakerDecoderImplInterface"* %call40, %"class.draco::MeshEdgebreakerDecoder"* %this1)
  br i1 %call41, label %if.end43, label %if.then42

if.then42:                                        ; preds = %if.end38
  store i1 false, i1* %retval, align 1
  br label %return

if.end43:                                         ; preds = %if.end38
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end43, %if.then42, %if.then37, %if.then
  %12 = load i1, i1* %retval, align 1
  ret i1 %12
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::DecoderBuffer"* @_ZN5draco17PointCloudDecoder6bufferEv(%"class.draco::PointCloudDecoder"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointCloudDecoder"*, align 4
  store %"class.draco::PointCloudDecoder"* %this, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::PointCloudDecoder"*, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  %buffer_ = getelementptr inbounds %"class.draco::PointCloudDecoder", %"class.draco::PointCloudDecoder"* %this1, i32 0, i32 4
  %0 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer_, align 4
  ret %"class.draco::DecoderBuffer"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIhEEbPT_(%"class.draco::DecoderBuffer"* %this, i8* %out_val) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %out_val.addr = alloca i8*, align 4
  store %"class.draco::DecoderBuffer"* %this, %"class.draco::DecoderBuffer"** %this.addr, align 4
  store i8* %out_val, i8** %out_val.addr, align 4
  %this1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %this.addr, align 4
  %0 = load i8*, i8** %out_val.addr, align 4
  %call = call zeroext i1 @_ZN5draco13DecoderBuffer4PeekIhEEbPT_(%"class.draco::DecoderBuffer"* %this1, i8* %0)
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %pos_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %1 = load i64, i64* %pos_, align 8
  %add = add nsw i64 %1, 1
  store i64 %add, i64* %pos_, align 8
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %2 = load i1, i1* %retval, align 1
  ret i1 %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.122"* @_ZNSt3__210unique_ptrIN5draco35MeshEdgebreakerDecoderImplInterfaceENS_14default_deleteIS2_EEEaSEDn(%"class.std::__2::unique_ptr.122"* %this, i8* %0) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.122"*, align 4
  %.addr = alloca i8*, align 4
  store %"class.std::__2::unique_ptr.122"* %this, %"class.std::__2::unique_ptr.122"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.122"*, %"class.std::__2::unique_ptr.122"** %this.addr, align 4
  call void @_ZNSt3__210unique_ptrIN5draco35MeshEdgebreakerDecoderImplInterfaceENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.122"* %this1, %"class.draco::MeshEdgebreakerDecoderImplInterface"* null) #5
  ret %"class.std::__2::unique_ptr.122"* %this1
}

; Function Attrs: nobuiltin allocsize(0)
declare nonnull i8* @_Znwm(i32) #2

declare %"class.draco::MeshEdgebreakerDecoderImpl"* @_ZN5draco26MeshEdgebreakerDecoderImplINS_31MeshEdgebreakerTraversalDecoderEEC1Ev(%"class.draco::MeshEdgebreakerDecoderImpl"* returned) unnamed_addr #1

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.122"* @_ZNSt3__210unique_ptrIN5draco35MeshEdgebreakerDecoderImplInterfaceENS_14default_deleteIS2_EEEC2ILb1EvEEPS2_(%"class.std::__2::unique_ptr.122"* returned %this, %"class.draco::MeshEdgebreakerDecoderImplInterface"* %__p) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.122"*, align 4
  %__p.addr = alloca %"class.draco::MeshEdgebreakerDecoderImplInterface"*, align 4
  %ref.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::unique_ptr.122"* %this, %"class.std::__2::unique_ptr.122"** %this.addr, align 4
  store %"class.draco::MeshEdgebreakerDecoderImplInterface"* %__p, %"class.draco::MeshEdgebreakerDecoderImplInterface"** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.122"*, %"class.std::__2::unique_ptr.122"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.122", %"class.std::__2::unique_ptr.122"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::__compressed_pair.123"* @_ZNSt3__217__compressed_pairIPN5draco35MeshEdgebreakerDecoderImplInterfaceENS_14default_deleteIS2_EEEC2IRS3_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.123"* %__ptr_, %"class.draco::MeshEdgebreakerDecoderImplInterface"** nonnull align 4 dereferenceable(4) %__p.addr, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp)
  ret %"class.std::__2::unique_ptr.122"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.122"* @_ZNSt3__210unique_ptrIN5draco35MeshEdgebreakerDecoderImplInterfaceENS_14default_deleteIS2_EEEaSEOS5_(%"class.std::__2::unique_ptr.122"* %this, %"class.std::__2::unique_ptr.122"* nonnull align 4 dereferenceable(4) %__u) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.122"*, align 4
  %__u.addr = alloca %"class.std::__2::unique_ptr.122"*, align 4
  store %"class.std::__2::unique_ptr.122"* %this, %"class.std::__2::unique_ptr.122"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.122"* %__u, %"class.std::__2::unique_ptr.122"** %__u.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.122"*, %"class.std::__2::unique_ptr.122"** %this.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.122"*, %"class.std::__2::unique_ptr.122"** %__u.addr, align 4
  %call = call %"class.draco::MeshEdgebreakerDecoderImplInterface"* @_ZNSt3__210unique_ptrIN5draco35MeshEdgebreakerDecoderImplInterfaceENS_14default_deleteIS2_EEE7releaseEv(%"class.std::__2::unique_ptr.122"* %0) #5
  call void @_ZNSt3__210unique_ptrIN5draco35MeshEdgebreakerDecoderImplInterfaceENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.122"* %this1, %"class.draco::MeshEdgebreakerDecoderImplInterface"* %call) #5
  %1 = load %"class.std::__2::unique_ptr.122"*, %"class.std::__2::unique_ptr.122"** %__u.addr, align 4
  %call2 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.126"* @_ZNSt3__210unique_ptrIN5draco35MeshEdgebreakerDecoderImplInterfaceENS_14default_deleteIS2_EEE11get_deleterEv(%"class.std::__2::unique_ptr.122"* %1) #5
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.126"* @_ZNSt3__27forwardINS_14default_deleteIN5draco35MeshEdgebreakerDecoderImplInterfaceEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.126"* nonnull align 1 dereferenceable(1) %call2) #5
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.122", %"class.std::__2::unique_ptr.122"* %this1, i32 0, i32 0
  %call4 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.126"* @_ZNSt3__217__compressed_pairIPN5draco35MeshEdgebreakerDecoderImplInterfaceENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.123"* %__ptr_) #5
  ret %"class.std::__2::unique_ptr.122"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.122"* @_ZNSt3__210unique_ptrIN5draco35MeshEdgebreakerDecoderImplInterfaceENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.122"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.122"*, align 4
  store %"class.std::__2::unique_ptr.122"* %this, %"class.std::__2::unique_ptr.122"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.122"*, %"class.std::__2::unique_ptr.122"** %this.addr, align 4
  call void @_ZNSt3__210unique_ptrIN5draco35MeshEdgebreakerDecoderImplInterfaceENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.122"* %this1, %"class.draco::MeshEdgebreakerDecoderImplInterface"* null) #5
  ret %"class.std::__2::unique_ptr.122"* %this1
}

declare %"class.draco::MeshEdgebreakerDecoderImpl.219"* @_ZN5draco26MeshEdgebreakerDecoderImplINS_41MeshEdgebreakerTraversalPredictiveDecoderEEC1Ev(%"class.draco::MeshEdgebreakerDecoderImpl.219"* returned) unnamed_addr #1

declare %"class.draco::MeshEdgebreakerDecoderImpl.227"* @_ZN5draco26MeshEdgebreakerDecoderImplINS_38MeshEdgebreakerTraversalValenceDecoderEEC1Ev(%"class.draco::MeshEdgebreakerDecoderImpl.227"* returned) unnamed_addr #1

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__210unique_ptrIN5draco35MeshEdgebreakerDecoderImplInterfaceENS_14default_deleteIS2_EEEcvbEv(%"class.std::__2::unique_ptr.122"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.122"*, align 4
  store %"class.std::__2::unique_ptr.122"* %this, %"class.std::__2::unique_ptr.122"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.122"*, %"class.std::__2::unique_ptr.122"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.122", %"class.std::__2::unique_ptr.122"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::MeshEdgebreakerDecoderImplInterface"** @_ZNKSt3__217__compressed_pairIPN5draco35MeshEdgebreakerDecoderImplInterfaceENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.123"* %__ptr_) #5
  %0 = load %"class.draco::MeshEdgebreakerDecoderImplInterface"*, %"class.draco::MeshEdgebreakerDecoderImplInterface"** %call, align 4
  %cmp = icmp ne %"class.draco::MeshEdgebreakerDecoderImplInterface"* %0, null
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define hidden zeroext i1 @_ZN5draco22MeshEdgebreakerDecoder18DecodeConnectivityEv(%"class.draco::MeshEdgebreakerDecoder"* %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %"class.draco::MeshEdgebreakerDecoder"*, align 4
  store %"class.draco::MeshEdgebreakerDecoder"* %this, %"class.draco::MeshEdgebreakerDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::MeshEdgebreakerDecoder"*, %"class.draco::MeshEdgebreakerDecoder"** %this.addr, align 4
  %impl_ = getelementptr inbounds %"class.draco::MeshEdgebreakerDecoder", %"class.draco::MeshEdgebreakerDecoder"* %this1, i32 0, i32 1
  %call = call %"class.draco::MeshEdgebreakerDecoderImplInterface"* @_ZNKSt3__210unique_ptrIN5draco35MeshEdgebreakerDecoderImplInterfaceENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr.122"* %impl_) #5
  %0 = bitcast %"class.draco::MeshEdgebreakerDecoderImplInterface"* %call to i1 (%"class.draco::MeshEdgebreakerDecoderImplInterface"*)***
  %vtable = load i1 (%"class.draco::MeshEdgebreakerDecoderImplInterface"*)**, i1 (%"class.draco::MeshEdgebreakerDecoderImplInterface"*)*** %0, align 4
  %vfn = getelementptr inbounds i1 (%"class.draco::MeshEdgebreakerDecoderImplInterface"*)*, i1 (%"class.draco::MeshEdgebreakerDecoderImplInterface"*)** %vtable, i64 6
  %1 = load i1 (%"class.draco::MeshEdgebreakerDecoderImplInterface"*)*, i1 (%"class.draco::MeshEdgebreakerDecoderImplInterface"*)** %vfn, align 4
  %call2 = call zeroext i1 %1(%"class.draco::MeshEdgebreakerDecoderImplInterface"* %call)
  ret i1 %call2
}

; Function Attrs: noinline nounwind optnone
define hidden zeroext i1 @_ZN5draco22MeshEdgebreakerDecoder19OnAttributesDecodedEv(%"class.draco::MeshEdgebreakerDecoder"* %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %"class.draco::MeshEdgebreakerDecoder"*, align 4
  store %"class.draco::MeshEdgebreakerDecoder"* %this, %"class.draco::MeshEdgebreakerDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::MeshEdgebreakerDecoder"*, %"class.draco::MeshEdgebreakerDecoder"** %this.addr, align 4
  %impl_ = getelementptr inbounds %"class.draco::MeshEdgebreakerDecoder", %"class.draco::MeshEdgebreakerDecoder"* %this1, i32 0, i32 1
  %call = call %"class.draco::MeshEdgebreakerDecoderImplInterface"* @_ZNKSt3__210unique_ptrIN5draco35MeshEdgebreakerDecoderImplInterfaceENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr.122"* %impl_) #5
  %0 = bitcast %"class.draco::MeshEdgebreakerDecoderImplInterface"* %call to i1 (%"class.draco::MeshEdgebreakerDecoderImplInterface"*)***
  %vtable = load i1 (%"class.draco::MeshEdgebreakerDecoderImplInterface"*)**, i1 (%"class.draco::MeshEdgebreakerDecoderImplInterface"*)*** %0, align 4
  %vfn = getelementptr inbounds i1 (%"class.draco::MeshEdgebreakerDecoderImplInterface"*)*, i1 (%"class.draco::MeshEdgebreakerDecoderImplInterface"*)** %vtable, i64 7
  %1 = load i1 (%"class.draco::MeshEdgebreakerDecoderImplInterface"*)*, i1 (%"class.draco::MeshEdgebreakerDecoderImplInterface"*)** %vfn, align 4
  %call2 = call zeroext i1 %1(%"class.draco::MeshEdgebreakerDecoderImplInterface"* %call)
  ret i1 %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::MeshEdgebreakerDecoder"* @_ZN5draco22MeshEdgebreakerDecoderD2Ev(%"class.draco::MeshEdgebreakerDecoder"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::MeshEdgebreakerDecoder"*, align 4
  store %"class.draco::MeshEdgebreakerDecoder"* %this, %"class.draco::MeshEdgebreakerDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::MeshEdgebreakerDecoder"*, %"class.draco::MeshEdgebreakerDecoder"** %this.addr, align 4
  %0 = bitcast %"class.draco::MeshEdgebreakerDecoder"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [15 x i8*] }, { [15 x i8*] }* @_ZTVN5draco22MeshEdgebreakerDecoderE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %impl_ = getelementptr inbounds %"class.draco::MeshEdgebreakerDecoder", %"class.draco::MeshEdgebreakerDecoder"* %this1, i32 0, i32 1
  %call = call %"class.std::__2::unique_ptr.122"* @_ZNSt3__210unique_ptrIN5draco35MeshEdgebreakerDecoderImplInterfaceENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.122"* %impl_) #5
  %1 = bitcast %"class.draco::MeshEdgebreakerDecoder"* %this1 to %"class.draco::MeshDecoder"*
  %call2 = call %"class.draco::MeshDecoder"* @_ZN5draco11MeshDecoderD2Ev(%"class.draco::MeshDecoder"* %1) #5
  ret %"class.draco::MeshEdgebreakerDecoder"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco22MeshEdgebreakerDecoderD0Ev(%"class.draco::MeshEdgebreakerDecoder"* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::MeshEdgebreakerDecoder"*, align 4
  store %"class.draco::MeshEdgebreakerDecoder"* %this, %"class.draco::MeshEdgebreakerDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::MeshEdgebreakerDecoder"*, %"class.draco::MeshEdgebreakerDecoder"** %this.addr, align 4
  %call = call %"class.draco::MeshEdgebreakerDecoder"* @_ZN5draco22MeshEdgebreakerDecoderD2Ev(%"class.draco::MeshEdgebreakerDecoder"* %this1) #5
  %0 = bitcast %"class.draco::MeshEdgebreakerDecoder"* %this1 to i8*
  call void @_ZdlPv(i8* %0) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco11MeshDecoder15GetGeometryTypeEv(%"class.draco::MeshDecoder"* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::MeshDecoder"*, align 4
  store %"class.draco::MeshDecoder"* %this, %"class.draco::MeshDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::MeshDecoder"*, %"class.draco::MeshDecoder"** %this.addr, align 4
  ret i32 1
}

declare zeroext i1 @_ZN5draco11MeshDecoder18DecodeGeometryDataEv(%"class.draco::MeshDecoder"*) unnamed_addr #1

declare zeroext i1 @_ZN5draco17PointCloudDecoder21DecodePointAttributesEv(%"class.draco::PointCloudDecoder"*) unnamed_addr #1

declare zeroext i1 @_ZN5draco17PointCloudDecoder19DecodeAllAttributesEv(%"class.draco::PointCloudDecoder"*) unnamed_addr #1

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::CornerTable"* @_ZNK5draco22MeshEdgebreakerDecoder14GetCornerTableEv(%"class.draco::MeshEdgebreakerDecoder"* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::MeshEdgebreakerDecoder"*, align 4
  store %"class.draco::MeshEdgebreakerDecoder"* %this, %"class.draco::MeshEdgebreakerDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::MeshEdgebreakerDecoder"*, %"class.draco::MeshEdgebreakerDecoder"** %this.addr, align 4
  %impl_ = getelementptr inbounds %"class.draco::MeshEdgebreakerDecoder", %"class.draco::MeshEdgebreakerDecoder"* %this1, i32 0, i32 1
  %call = call %"class.draco::MeshEdgebreakerDecoderImplInterface"* @_ZNKSt3__210unique_ptrIN5draco35MeshEdgebreakerDecoderImplInterfaceENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr.122"* %impl_) #5
  %0 = bitcast %"class.draco::MeshEdgebreakerDecoderImplInterface"* %call to %"class.draco::CornerTable"* (%"class.draco::MeshEdgebreakerDecoderImplInterface"*)***
  %vtable = load %"class.draco::CornerTable"* (%"class.draco::MeshEdgebreakerDecoderImplInterface"*)**, %"class.draco::CornerTable"* (%"class.draco::MeshEdgebreakerDecoderImplInterface"*)*** %0, align 4
  %vfn = getelementptr inbounds %"class.draco::CornerTable"* (%"class.draco::MeshEdgebreakerDecoderImplInterface"*)*, %"class.draco::CornerTable"* (%"class.draco::MeshEdgebreakerDecoderImplInterface"*)** %vtable, i64 9
  %1 = load %"class.draco::CornerTable"* (%"class.draco::MeshEdgebreakerDecoderImplInterface"*)*, %"class.draco::CornerTable"* (%"class.draco::MeshEdgebreakerDecoderImplInterface"*)** %vfn, align 4
  %call2 = call %"class.draco::CornerTable"* %1(%"class.draco::MeshEdgebreakerDecoderImplInterface"* %call)
  ret %"class.draco::CornerTable"* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::MeshAttributeCornerTable"* @_ZNK5draco22MeshEdgebreakerDecoder23GetAttributeCornerTableEi(%"class.draco::MeshEdgebreakerDecoder"* %this, i32 %att_id) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::MeshEdgebreakerDecoder"*, align 4
  %att_id.addr = alloca i32, align 4
  store %"class.draco::MeshEdgebreakerDecoder"* %this, %"class.draco::MeshEdgebreakerDecoder"** %this.addr, align 4
  store i32 %att_id, i32* %att_id.addr, align 4
  %this1 = load %"class.draco::MeshEdgebreakerDecoder"*, %"class.draco::MeshEdgebreakerDecoder"** %this.addr, align 4
  %impl_ = getelementptr inbounds %"class.draco::MeshEdgebreakerDecoder", %"class.draco::MeshEdgebreakerDecoder"* %this1, i32 0, i32 1
  %call = call %"class.draco::MeshEdgebreakerDecoderImplInterface"* @_ZNKSt3__210unique_ptrIN5draco35MeshEdgebreakerDecoderImplInterfaceENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr.122"* %impl_) #5
  %0 = load i32, i32* %att_id.addr, align 4
  %1 = bitcast %"class.draco::MeshEdgebreakerDecoderImplInterface"* %call to %"class.draco::MeshAttributeCornerTable"* (%"class.draco::MeshEdgebreakerDecoderImplInterface"*, i32)***
  %vtable = load %"class.draco::MeshAttributeCornerTable"* (%"class.draco::MeshEdgebreakerDecoderImplInterface"*, i32)**, %"class.draco::MeshAttributeCornerTable"* (%"class.draco::MeshEdgebreakerDecoderImplInterface"*, i32)*** %1, align 4
  %vfn = getelementptr inbounds %"class.draco::MeshAttributeCornerTable"* (%"class.draco::MeshEdgebreakerDecoderImplInterface"*, i32)*, %"class.draco::MeshAttributeCornerTable"* (%"class.draco::MeshEdgebreakerDecoderImplInterface"*, i32)** %vtable, i64 3
  %2 = load %"class.draco::MeshAttributeCornerTable"* (%"class.draco::MeshEdgebreakerDecoderImplInterface"*, i32)*, %"class.draco::MeshAttributeCornerTable"* (%"class.draco::MeshEdgebreakerDecoderImplInterface"*, i32)** %vfn, align 4
  %call2 = call %"class.draco::MeshAttributeCornerTable"* %2(%"class.draco::MeshEdgebreakerDecoderImplInterface"* %call, i32 %0)
  ret %"class.draco::MeshAttributeCornerTable"* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.draco::MeshAttributeIndicesEncodingData"* @_ZNK5draco22MeshEdgebreakerDecoder24GetAttributeEncodingDataEi(%"class.draco::MeshEdgebreakerDecoder"* %this, i32 %att_id) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::MeshEdgebreakerDecoder"*, align 4
  %att_id.addr = alloca i32, align 4
  store %"class.draco::MeshEdgebreakerDecoder"* %this, %"class.draco::MeshEdgebreakerDecoder"** %this.addr, align 4
  store i32 %att_id, i32* %att_id.addr, align 4
  %this1 = load %"class.draco::MeshEdgebreakerDecoder"*, %"class.draco::MeshEdgebreakerDecoder"** %this.addr, align 4
  %impl_ = getelementptr inbounds %"class.draco::MeshEdgebreakerDecoder", %"class.draco::MeshEdgebreakerDecoder"* %this1, i32 0, i32 1
  %call = call %"class.draco::MeshEdgebreakerDecoderImplInterface"* @_ZNKSt3__210unique_ptrIN5draco35MeshEdgebreakerDecoderImplInterfaceENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr.122"* %impl_) #5
  %0 = load i32, i32* %att_id.addr, align 4
  %1 = bitcast %"class.draco::MeshEdgebreakerDecoderImplInterface"* %call to %"struct.draco::MeshAttributeIndicesEncodingData"* (%"class.draco::MeshEdgebreakerDecoderImplInterface"*, i32)***
  %vtable = load %"struct.draco::MeshAttributeIndicesEncodingData"* (%"class.draco::MeshEdgebreakerDecoderImplInterface"*, i32)**, %"struct.draco::MeshAttributeIndicesEncodingData"* (%"class.draco::MeshEdgebreakerDecoderImplInterface"*, i32)*** %1, align 4
  %vfn = getelementptr inbounds %"struct.draco::MeshAttributeIndicesEncodingData"* (%"class.draco::MeshEdgebreakerDecoderImplInterface"*, i32)*, %"struct.draco::MeshAttributeIndicesEncodingData"* (%"class.draco::MeshEdgebreakerDecoderImplInterface"*, i32)** %vtable, i64 4
  %2 = load %"struct.draco::MeshAttributeIndicesEncodingData"* (%"class.draco::MeshEdgebreakerDecoderImplInterface"*, i32)*, %"struct.draco::MeshAttributeIndicesEncodingData"* (%"class.draco::MeshEdgebreakerDecoderImplInterface"*, i32)** %vfn, align 4
  %call2 = call %"struct.draco::MeshAttributeIndicesEncodingData"* %2(%"class.draco::MeshEdgebreakerDecoderImplInterface"* %call, i32 %0)
  ret %"struct.draco::MeshAttributeIndicesEncodingData"* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.123"* @_ZNSt3__217__compressed_pairIPN5draco35MeshEdgebreakerDecoderImplInterfaceENS_14default_deleteIS2_EEEC2IS3_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.123"* returned %this, %"class.draco::MeshEdgebreakerDecoderImplInterface"** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.123"*, align 4
  %__t1.addr = alloca %"class.draco::MeshEdgebreakerDecoderImplInterface"**, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.123"* %this, %"class.std::__2::__compressed_pair.123"** %this.addr, align 4
  store %"class.draco::MeshEdgebreakerDecoderImplInterface"** %__t1, %"class.draco::MeshEdgebreakerDecoderImplInterface"*** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.123"*, %"class.std::__2::__compressed_pair.123"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.123"* %this1 to %"struct.std::__2::__compressed_pair_elem.124"*
  %1 = load %"class.draco::MeshEdgebreakerDecoderImplInterface"**, %"class.draco::MeshEdgebreakerDecoderImplInterface"*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::MeshEdgebreakerDecoderImplInterface"** @_ZNSt3__27forwardIPN5draco35MeshEdgebreakerDecoderImplInterfaceEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::MeshEdgebreakerDecoderImplInterface"** nonnull align 4 dereferenceable(4) %1) #5
  %call2 = call %"struct.std::__2::__compressed_pair_elem.124"* @_ZNSt3__222__compressed_pair_elemIPN5draco35MeshEdgebreakerDecoderImplInterfaceELi0ELb0EEC2IS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.124"* %0, %"class.draco::MeshEdgebreakerDecoderImplInterface"** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.123"* %this1 to %"struct.std::__2::__compressed_pair_elem.125"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #5
  %call4 = call %"struct.std::__2::__compressed_pair_elem.125"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco35MeshEdgebreakerDecoderImplInterfaceEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.125"* %2)
  ret %"class.std::__2::__compressed_pair.123"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::MeshEdgebreakerDecoderImplInterface"** @_ZNSt3__27forwardIPN5draco35MeshEdgebreakerDecoderImplInterfaceEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::MeshEdgebreakerDecoderImplInterface"** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.draco::MeshEdgebreakerDecoderImplInterface"**, align 4
  store %"class.draco::MeshEdgebreakerDecoderImplInterface"** %__t, %"class.draco::MeshEdgebreakerDecoderImplInterface"*** %__t.addr, align 4
  %0 = load %"class.draco::MeshEdgebreakerDecoderImplInterface"**, %"class.draco::MeshEdgebreakerDecoderImplInterface"*** %__t.addr, align 4
  ret %"class.draco::MeshEdgebreakerDecoderImplInterface"** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.124"* @_ZNSt3__222__compressed_pair_elemIPN5draco35MeshEdgebreakerDecoderImplInterfaceELi0ELb0EEC2IS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.124"* returned %this, %"class.draco::MeshEdgebreakerDecoderImplInterface"** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.124"*, align 4
  %__u.addr = alloca %"class.draco::MeshEdgebreakerDecoderImplInterface"**, align 4
  store %"struct.std::__2::__compressed_pair_elem.124"* %this, %"struct.std::__2::__compressed_pair_elem.124"** %this.addr, align 4
  store %"class.draco::MeshEdgebreakerDecoderImplInterface"** %__u, %"class.draco::MeshEdgebreakerDecoderImplInterface"*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.124"*, %"struct.std::__2::__compressed_pair_elem.124"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.124", %"struct.std::__2::__compressed_pair_elem.124"* %this1, i32 0, i32 0
  %0 = load %"class.draco::MeshEdgebreakerDecoderImplInterface"**, %"class.draco::MeshEdgebreakerDecoderImplInterface"*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::MeshEdgebreakerDecoderImplInterface"** @_ZNSt3__27forwardIPN5draco35MeshEdgebreakerDecoderImplInterfaceEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::MeshEdgebreakerDecoderImplInterface"** nonnull align 4 dereferenceable(4) %0) #5
  %1 = load %"class.draco::MeshEdgebreakerDecoderImplInterface"*, %"class.draco::MeshEdgebreakerDecoderImplInterface"** %call, align 4
  store %"class.draco::MeshEdgebreakerDecoderImplInterface"* %1, %"class.draco::MeshEdgebreakerDecoderImplInterface"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.124"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  store %"struct.std::__2::__default_init_tag"* %__t, %"struct.std::__2::__default_init_tag"** %__t.addr, align 4
  %0 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t.addr, align 4
  ret %"struct.std::__2::__default_init_tag"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.125"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco35MeshEdgebreakerDecoderImplInterfaceEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.125"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.125"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.125"* %this, %"struct.std::__2::__compressed_pair_elem.125"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.125"*, %"struct.std::__2::__compressed_pair_elem.125"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.125"* %this1 to %"struct.std::__2::default_delete.126"*
  ret %"struct.std::__2::__compressed_pair_elem.125"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::MeshDecoder"* @_ZN5draco11MeshDecoderD2Ev(%"class.draco::MeshDecoder"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::MeshDecoder"*, align 4
  store %"class.draco::MeshDecoder"* %this, %"class.draco::MeshDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::MeshDecoder"*, %"class.draco::MeshDecoder"** %this.addr, align 4
  %0 = bitcast %"class.draco::MeshDecoder"* %this1 to %"class.draco::PointCloudDecoder"*
  %call = call %"class.draco::PointCloudDecoder"* @_ZN5draco17PointCloudDecoderD2Ev(%"class.draco::PointCloudDecoder"* %0) #5
  ret %"class.draco::MeshDecoder"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::PointCloudDecoder"* @_ZN5draco17PointCloudDecoderD2Ev(%"class.draco::PointCloudDecoder"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointCloudDecoder"*, align 4
  store %"class.draco::PointCloudDecoder"* %this, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::PointCloudDecoder"*, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  %0 = bitcast %"class.draco::PointCloudDecoder"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [11 x i8*] }, { [11 x i8*] }* @_ZTVN5draco17PointCloudDecoderE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %attribute_to_decoder_map_ = getelementptr inbounds %"class.draco::PointCloudDecoder", %"class.draco::PointCloudDecoder"* %this1, i32 0, i32 3
  %call = call %"class.std::__2::vector.87"* @_ZNSt3__26vectorIiNS_9allocatorIiEEED2Ev(%"class.std::__2::vector.87"* %attribute_to_decoder_map_) #5
  %attributes_decoders_ = getelementptr inbounds %"class.draco::PointCloudDecoder", %"class.draco::PointCloudDecoder"* %this1, i32 0, i32 2
  %call2 = call %"class.std::__2::vector.94"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEED2Ev(%"class.std::__2::vector.94"* %attributes_decoders_) #5
  ret %"class.draco::PointCloudDecoder"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector.87"* @_ZNSt3__26vectorIiNS_9allocatorIiEEED2Ev(%"class.std::__2::vector.87"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  call void @_ZNKSt3__26vectorIiNS_9allocatorIiEEE17__annotate_deleteEv(%"class.std::__2::vector.87"* %this1) #5
  %0 = bitcast %"class.std::__2::vector.87"* %this1 to %"class.std::__2::__vector_base.88"*
  %call = call %"class.std::__2::__vector_base.88"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEED2Ev(%"class.std::__2::__vector_base.88"* %0) #5
  ret %"class.std::__2::vector.87"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector.94"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEED2Ev(%"class.std::__2::vector.94"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  call void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__annotate_deleteEv(%"class.std::__2::vector.94"* %this1) #5
  %0 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %call = call %"class.std::__2::__vector_base.95"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEED2Ev(%"class.std::__2::__vector_base.95"* %0) #5
  ret %"class.std::__2::vector.94"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIiNS_9allocatorIiEEE17__annotate_deleteEv(%"class.std::__2::vector.87"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  %call = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector.87"* %this1) #5
  %0 = bitcast i32* %call to i8*
  %call2 = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector.87"* %this1) #5
  %call3 = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::vector.87"* %this1) #5
  %add.ptr = getelementptr inbounds i32, i32* %call2, i32 %call3
  %1 = bitcast i32* %add.ptr to i8*
  %call4 = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector.87"* %this1) #5
  %call5 = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4sizeEv(%"class.std::__2::vector.87"* %this1) #5
  %add.ptr6 = getelementptr inbounds i32, i32* %call4, i32 %call5
  %2 = bitcast i32* %add.ptr6 to i8*
  %call7 = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector.87"* %this1) #5
  %call8 = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::vector.87"* %this1) #5
  %add.ptr9 = getelementptr inbounds i32, i32* %call7, i32 %call8
  %3 = bitcast i32* %add.ptr9 to i8*
  call void @_ZNKSt3__26vectorIiNS_9allocatorIiEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector.87"* %this1, i8* %0, i8* %1, i8* %2, i8* %3) #5
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base.88"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEED2Ev(%"class.std::__2::__vector_base.88"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__vector_base.88"*, align 4
  %this.addr = alloca %"class.std::__2::__vector_base.88"*, align 4
  store %"class.std::__2::__vector_base.88"* %this, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.88"*, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  store %"class.std::__2::__vector_base.88"* %this1, %"class.std::__2::__vector_base.88"** %retval, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__begin_, align 4
  %cmp = icmp ne i32* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE5clearEv(%"class.std::__2::__vector_base.88"* %this1) #5
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE7__allocEv(%"class.std::__2::__vector_base.88"* %this1) #5
  %__begin_2 = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %this1, i32 0, i32 0
  %1 = load i32*, i32** %__begin_2, align 4
  %call3 = call i32 @_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::__vector_base.88"* %this1) #5
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE10deallocateERS2_Pim(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %call, i32* %1, i32 %call3) #5
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"class.std::__2::__vector_base.88"*, %"class.std::__2::__vector_base.88"** %retval, align 4
  ret %"class.std::__2::__vector_base.88"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIiNS_9allocatorIiEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector.87"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector.87"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.87"* %this1 to %"class.std::__2::__vector_base.88"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %0, i32 0, i32 0
  %1 = load i32*, i32** %__begin_, align 4
  %call = call i32* @_ZNSt3__212__to_addressIiEEPT_S2_(i32* %1) #5
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::vector.87"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.87"* %this1 to %"class.std::__2::__vector_base.88"*
  %call = call i32 @_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::__vector_base.88"* %0) #5
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4sizeEv(%"class.std::__2::vector.87"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.87"* %this1 to %"class.std::__2::__vector_base.88"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %0, i32 0, i32 1
  %1 = load i32*, i32** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector.87"* %this1 to %"class.std::__2::__vector_base.88"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %2, i32 0, i32 0
  %3 = load i32*, i32** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint i32* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint i32* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNSt3__212__to_addressIiEEPT_S2_(i32* %__p) #0 comdat {
entry:
  %__p.addr = alloca i32*, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %0 = load i32*, i32** %__p.addr, align 4
  ret i32* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::__vector_base.88"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.88"*, align 4
  store %"class.std::__2::__vector_base.88"* %this, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.88"*, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE9__end_capEv(%"class.std::__2::__vector_base.88"* %this1) #5
  %0 = load i32*, i32** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %this1, i32 0, i32 0
  %1 = load i32*, i32** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint i32* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint i32* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE9__end_capEv(%"class.std::__2::__vector_base.88"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.88"*, align 4
  store %"class.std::__2::__vector_base.88"* %this, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.88"*, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__217__compressed_pairIPiNS_9allocatorIiEEE5firstEv(%"class.std::__2::__compressed_pair.89"* %__end_cap_) #5
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__217__compressed_pairIPiNS_9allocatorIiEEE5firstEv(%"class.std::__2::__compressed_pair.89"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.89"*, align 4
  store %"class.std::__2::__compressed_pair.89"* %this, %"class.std::__2::__compressed_pair.89"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.89"*, %"class.std::__2::__compressed_pair.89"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.89"* %this1 to %"struct.std::__2::__compressed_pair_elem.90"*
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__222__compressed_pair_elemIPiLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.90"* %0) #5
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__222__compressed_pair_elemIPiLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.90"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.90"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.90"* %this, %"struct.std::__2::__compressed_pair_elem.90"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.90"*, %"struct.std::__2::__compressed_pair_elem.90"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.90", %"struct.std::__2::__compressed_pair_elem.90"* %this1, i32 0, i32 0
  ret i32** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE5clearEv(%"class.std::__2::__vector_base.88"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.88"*, align 4
  store %"class.std::__2::__vector_base.88"* %this, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.88"*, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__begin_, align 4
  call void @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE17__destruct_at_endEPi(%"class.std::__2::__vector_base.88"* %this1, i32* %0) #5
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE10deallocateERS2_Pim(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %__a, i32* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.92"*, align 4
  %__p.addr = alloca i32*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.92"* %__a, %"class.std::__2::allocator.92"** %__a.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.92"*, %"class.std::__2::allocator.92"** %__a.addr, align 4
  %1 = load i32*, i32** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorIiE10deallocateEPim(%"class.std::__2::allocator.92"* %0, i32* %1, i32 %2) #5
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE7__allocEv(%"class.std::__2::__vector_base.88"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.88"*, align 4
  store %"class.std::__2::__vector_base.88"* %this, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.88"*, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__217__compressed_pairIPiNS_9allocatorIiEEE6secondEv(%"class.std::__2::__compressed_pair.89"* %__end_cap_) #5
  ret %"class.std::__2::allocator.92"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE17__destruct_at_endEPi(%"class.std::__2::__vector_base.88"* %this, i32* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.88"*, align 4
  %__new_last.addr = alloca i32*, align 4
  %__soon_to_be_end = alloca i32*, align 4
  store %"class.std::__2::__vector_base.88"* %this, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  store i32* %__new_last, i32** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.88"*, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %this1, i32 0, i32 1
  %0 = load i32*, i32** %__end_, align 4
  store i32* %0, i32** %__soon_to_be_end, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load i32*, i32** %__new_last.addr, align 4
  %2 = load i32*, i32** %__soon_to_be_end, align 4
  %cmp = icmp ne i32* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE7__allocEv(%"class.std::__2::__vector_base.88"* %this1) #5
  %3 = load i32*, i32** %__soon_to_be_end, align 4
  %incdec.ptr = getelementptr inbounds i32, i32* %3, i32 -1
  store i32* %incdec.ptr, i32** %__soon_to_be_end, align 4
  %call2 = call i32* @_ZNSt3__212__to_addressIiEEPT_S2_(i32* %incdec.ptr) #5
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE7destroyIiEEvRS2_PT_(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %call, i32* %call2)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %4 = load i32*, i32** %__new_last.addr, align 4
  %__end_3 = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %this1, i32 0, i32 1
  store i32* %4, i32** %__end_3, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE7destroyIiEEvRS2_PT_(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %__a, i32* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.92"*, align 4
  %__p.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy", align 1
  store %"class.std::__2::allocator.92"* %__a, %"class.std::__2::allocator.92"** %__a.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.92"*, %"class.std::__2::allocator.92"** %__a.addr, align 4
  %2 = load i32*, i32** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE9__destroyIiEEvNS_17integral_constantIbLb1EEERS2_PT_(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %1, i32* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE9__destroyIiEEvNS_17integral_constantIbLb1EEERS2_PT_(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %__a, i32* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.92"*, align 4
  %__p.addr = alloca i32*, align 4
  store %"class.std::__2::allocator.92"* %__a, %"class.std::__2::allocator.92"** %__a.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.92"*, %"class.std::__2::allocator.92"** %__a.addr, align 4
  %2 = load i32*, i32** %__p.addr, align 4
  call void @_ZNSt3__29allocatorIiE7destroyEPi(%"class.std::__2::allocator.92"* %1, i32* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIiE7destroyEPi(%"class.std::__2::allocator.92"* %this, i32* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.92"*, align 4
  %__p.addr = alloca i32*, align 4
  store %"class.std::__2::allocator.92"* %this, %"class.std::__2::allocator.92"** %this.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.92"*, %"class.std::__2::allocator.92"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIiE10deallocateEPim(%"class.std::__2::allocator.92"* %this, i32* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.92"*, align 4
  %__p.addr = alloca i32*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.92"* %this, %"class.std::__2::allocator.92"** %this.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.92"*, %"class.std::__2::allocator.92"** %this.addr, align 4
  %0 = load i32*, i32** %__p.addr, align 4
  %1 = bitcast i32* %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %__ptr, i32 %__size, i32 %__align) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  %1 = load i32, i32* %__size.addr, align 4
  %2 = load i32, i32* %__align.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm(i8* %0, i32 %1, i32 %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm(i8* %__ptr, i32 %__size, i32 %__align) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  %1 = load i32, i32* %__size.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm(i8* %0, i32 %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm(i8* %__ptr, i32 %__size) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller9__do_callEPv(i8* %0)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller9__do_callEPv(i8* %__ptr) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  call void @_ZdlPv(i8* %0) #7
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__217__compressed_pairIPiNS_9allocatorIiEEE6secondEv(%"class.std::__2::__compressed_pair.89"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.89"*, align 4
  store %"class.std::__2::__compressed_pair.89"* %this, %"class.std::__2::__compressed_pair.89"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.89"*, %"class.std::__2::__compressed_pair.89"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.89"* %this1 to %"struct.std::__2::__compressed_pair_elem.91"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIiEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.91"* %0) #5
  ret %"class.std::__2::allocator.92"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIiEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.91"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.91"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.91"* %this, %"struct.std::__2::__compressed_pair_elem.91"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.91"*, %"struct.std::__2::__compressed_pair_elem.91"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.91"* %this1 to %"class.std::__2::allocator.92"*
  ret %"class.std::__2::allocator.92"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__annotate_deleteEv(%"class.std::__2::vector.94"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  %call = call %"class.std::__2::unique_ptr.96"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.94"* %this1) #5
  %0 = bitcast %"class.std::__2::unique_ptr.96"* %call to i8*
  %call2 = call %"class.std::__2::unique_ptr.96"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.94"* %this1) #5
  %call3 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector.94"* %this1) #5
  %add.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %call2, i32 %call3
  %1 = bitcast %"class.std::__2::unique_ptr.96"* %add.ptr to i8*
  %call4 = call %"class.std::__2::unique_ptr.96"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.94"* %this1) #5
  %call5 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.94"* %this1) #5
  %add.ptr6 = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %call4, i32 %call5
  %2 = bitcast %"class.std::__2::unique_ptr.96"* %add.ptr6 to i8*
  %call7 = call %"class.std::__2::unique_ptr.96"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.94"* %this1) #5
  %call8 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector.94"* %this1) #5
  %add.ptr9 = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %call7, i32 %call8
  %3 = bitcast %"class.std::__2::unique_ptr.96"* %add.ptr9 to i8*
  call void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE31__annotate_contiguous_containerEPKvSB_SB_SB_(%"class.std::__2::vector.94"* %this1, i8* %0, i8* %1, i8* %2, i8* %3) #5
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base.95"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEED2Ev(%"class.std::__2::__vector_base.95"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__vector_base.95"*, align 4
  %this.addr = alloca %"class.std::__2::__vector_base.95"*, align 4
  store %"class.std::__2::__vector_base.95"* %this, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.95"*, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  store %"class.std::__2::__vector_base.95"* %this1, %"class.std::__2::__vector_base.95"** %retval, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__begin_, align 4
  %cmp = icmp ne %"class.std::__2::unique_ptr.96"* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5clearEv(%"class.std::__2::__vector_base.95"* %this1) #5
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base.95"* %this1) #5
  %__begin_2 = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__begin_2, align 4
  %call3 = call i32 @_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::__vector_base.95"* %this1) #5
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE10deallocateERS8_PS7_m(%"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %call, %"class.std::__2::unique_ptr.96"* %1, i32 %call3) #5
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"class.std::__2::__vector_base.95"*, %"class.std::__2::__vector_base.95"** %retval, align 4
  ret %"class.std::__2::__vector_base.95"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE31__annotate_contiguous_containerEPKvSB_SB_SB_(%"class.std::__2::vector.94"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.96"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.94"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %0, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__begin_, align 4
  %call = call %"class.std::__2::unique_ptr.96"* @_ZNSt3__212__to_addressINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEEPT_S8_(%"class.std::__2::unique_ptr.96"* %1) #5
  ret %"class.std::__2::unique_ptr.96"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector.94"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %call = call i32 @_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::__vector_base.95"* %0) #5
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.94"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %0, i32 0, i32 1
  %1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %2, i32 0, i32 0
  %3 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.std::__2::unique_ptr.96"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.std::__2::unique_ptr.96"* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.96"* @_ZNSt3__212__to_addressINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEEPT_S8_(%"class.std::__2::unique_ptr.96"* %__p) #0 comdat {
entry:
  %__p.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  store %"class.std::__2::unique_ptr.96"* %__p, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  ret %"class.std::__2::unique_ptr.96"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::__vector_base.95"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.95"*, align 4
  store %"class.std::__2::__vector_base.95"* %this, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.95"*, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"** @_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE9__end_capEv(%"class.std::__2::__vector_base.95"* %this1) #5
  %0 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.std::__2::unique_ptr.96"* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.std::__2::unique_ptr.96"* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"** @_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE9__end_capEv(%"class.std::__2::__vector_base.95"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.95"*, align 4
  store %"class.std::__2::__vector_base.95"* %this, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.95"*, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"** @_ZNKSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.101"* %__end_cap_) #5
  ret %"class.std::__2::unique_ptr.96"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"** @_ZNKSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.101"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.101"*, align 4
  store %"class.std::__2::__compressed_pair.101"* %this, %"class.std::__2::__compressed_pair.101"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.101"*, %"class.std::__2::__compressed_pair.101"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.101"* %this1 to %"struct.std::__2::__compressed_pair_elem.102"*
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"** @_ZNKSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.102"* %0) #5
  ret %"class.std::__2::unique_ptr.96"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"** @_ZNKSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.102"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.102"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.102"* %this, %"struct.std::__2::__compressed_pair_elem.102"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.102"*, %"struct.std::__2::__compressed_pair_elem.102"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.102", %"struct.std::__2::__compressed_pair_elem.102"* %this1, i32 0, i32 0
  ret %"class.std::__2::unique_ptr.96"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5clearEv(%"class.std::__2::__vector_base.95"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.95"*, align 4
  store %"class.std::__2::__vector_base.95"* %this, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.95"*, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__begin_, align 4
  call void @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__destruct_at_endEPS6_(%"class.std::__2::__vector_base.95"* %this1, %"class.std::__2::unique_ptr.96"* %0) #5
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE10deallocateERS8_PS7_m(%"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %__a, %"class.std::__2::unique_ptr.96"* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.104"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.104"* %__a, %"class.std::__2::allocator.104"** %__a.addr, align 4
  store %"class.std::__2::unique_ptr.96"* %__p, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.104"*, %"class.std::__2::allocator.104"** %__a.addr, align 4
  %1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEE10deallocateEPS6_m(%"class.std::__2::allocator.104"* %0, %"class.std::__2::unique_ptr.96"* %1, i32 %2) #5
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base.95"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.95"*, align 4
  store %"class.std::__2::__vector_base.95"* %this, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.95"*, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE6secondEv(%"class.std::__2::__compressed_pair.101"* %__end_cap_) #5
  ret %"class.std::__2::allocator.104"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__destruct_at_endEPS6_(%"class.std::__2::__vector_base.95"* %this, %"class.std::__2::unique_ptr.96"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.95"*, align 4
  %__new_last.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  %__soon_to_be_end = alloca %"class.std::__2::unique_ptr.96"*, align 4
  store %"class.std::__2::__vector_base.95"* %this, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.96"* %__new_last, %"class.std::__2::unique_ptr.96"** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.95"*, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %this1, i32 0, i32 1
  %0 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__end_, align 4
  store %"class.std::__2::unique_ptr.96"* %0, %"class.std::__2::unique_ptr.96"** %__soon_to_be_end, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__new_last.addr, align 4
  %2 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__soon_to_be_end, align 4
  %cmp = icmp ne %"class.std::__2::unique_ptr.96"* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base.95"* %this1) #5
  %3 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__soon_to_be_end, align 4
  %incdec.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %3, i32 -1
  store %"class.std::__2::unique_ptr.96"* %incdec.ptr, %"class.std::__2::unique_ptr.96"** %__soon_to_be_end, align 4
  %call2 = call %"class.std::__2::unique_ptr.96"* @_ZNSt3__212__to_addressINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEEPT_S8_(%"class.std::__2::unique_ptr.96"* %incdec.ptr) #5
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE7destroyIS7_EEvRS8_PT_(%"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %call, %"class.std::__2::unique_ptr.96"* %call2)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %4 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__new_last.addr, align 4
  %__end_3 = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %this1, i32 0, i32 1
  store %"class.std::__2::unique_ptr.96"* %4, %"class.std::__2::unique_ptr.96"** %__end_3, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE7destroyIS7_EEvRS8_PT_(%"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %__a, %"class.std::__2::unique_ptr.96"* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.104"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy.250", align 1
  store %"class.std::__2::allocator.104"* %__a, %"class.std::__2::allocator.104"** %__a.addr, align 4
  store %"class.std::__2::unique_ptr.96"* %__p, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy.250"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.104"*, %"class.std::__2::allocator.104"** %__a.addr, align 4
  %2 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE9__destroyIS7_EEvNS_17integral_constantIbLb1EEERS8_PT_(%"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %1, %"class.std::__2::unique_ptr.96"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE9__destroyIS7_EEvNS_17integral_constantIbLb1EEERS8_PT_(%"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %__a, %"class.std::__2::unique_ptr.96"* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.104"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  store %"class.std::__2::allocator.104"* %__a, %"class.std::__2::allocator.104"** %__a.addr, align 4
  store %"class.std::__2::unique_ptr.96"* %__p, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.104"*, %"class.std::__2::allocator.104"** %__a.addr, align 4
  %2 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  call void @_ZNSt3__29allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEE7destroyEPS6_(%"class.std::__2::allocator.104"* %1, %"class.std::__2::unique_ptr.96"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEE7destroyEPS6_(%"class.std::__2::allocator.104"* %this, %"class.std::__2::unique_ptr.96"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.104"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  store %"class.std::__2::allocator.104"* %this, %"class.std::__2::allocator.104"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.96"* %__p, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.104"*, %"class.std::__2::allocator.104"** %this.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  %call = call %"class.std::__2::unique_ptr.96"* @_ZNSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.96"* %0) #5
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.96"* @_ZNSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.96"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  store %"class.std::__2::unique_ptr.96"* %this, %"class.std::__2::unique_ptr.96"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %this.addr, align 4
  call void @_ZNSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.96"* %this1, %"class.draco::AttributesDecoderInterface"* null) #5
  ret %"class.std::__2::unique_ptr.96"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.96"* %this, %"class.draco::AttributesDecoderInterface"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  %__p.addr = alloca %"class.draco::AttributesDecoderInterface"*, align 4
  %__tmp = alloca %"class.draco::AttributesDecoderInterface"*, align 4
  store %"class.std::__2::unique_ptr.96"* %this, %"class.std::__2::unique_ptr.96"** %this.addr, align 4
  store %"class.draco::AttributesDecoderInterface"* %__p, %"class.draco::AttributesDecoderInterface"** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributesDecoderInterface"** @_ZNSt3__217__compressed_pairIPN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.97"* %__ptr_) #5
  %0 = load %"class.draco::AttributesDecoderInterface"*, %"class.draco::AttributesDecoderInterface"** %call, align 4
  store %"class.draco::AttributesDecoderInterface"* %0, %"class.draco::AttributesDecoderInterface"** %__tmp, align 4
  %1 = load %"class.draco::AttributesDecoderInterface"*, %"class.draco::AttributesDecoderInterface"** %__p.addr, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::AttributesDecoderInterface"** @_ZNSt3__217__compressed_pairIPN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.97"* %__ptr_2) #5
  store %"class.draco::AttributesDecoderInterface"* %1, %"class.draco::AttributesDecoderInterface"** %call3, align 4
  %2 = load %"class.draco::AttributesDecoderInterface"*, %"class.draco::AttributesDecoderInterface"** %__tmp, align 4
  %tobool = icmp ne %"class.draco::AttributesDecoderInterface"* %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__ptr_4 = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %this1, i32 0, i32 0
  %call5 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.100"* @_ZNSt3__217__compressed_pairIPN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.97"* %__ptr_4) #5
  %3 = load %"class.draco::AttributesDecoderInterface"*, %"class.draco::AttributesDecoderInterface"** %__tmp, align 4
  call void @_ZNKSt3__214default_deleteIN5draco26AttributesDecoderInterfaceEEclEPS2_(%"struct.std::__2::default_delete.100"* %call5, %"class.draco::AttributesDecoderInterface"* %3) #5
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::AttributesDecoderInterface"** @_ZNSt3__217__compressed_pairIPN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.97"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.97"*, align 4
  store %"class.std::__2::__compressed_pair.97"* %this, %"class.std::__2::__compressed_pair.97"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.97"*, %"class.std::__2::__compressed_pair.97"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.97"* %this1 to %"struct.std::__2::__compressed_pair_elem.98"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributesDecoderInterface"** @_ZNSt3__222__compressed_pair_elemIPN5draco26AttributesDecoderInterfaceELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.98"* %0) #5
  ret %"class.draco::AttributesDecoderInterface"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.100"* @_ZNSt3__217__compressed_pairIPN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.97"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.97"*, align 4
  store %"class.std::__2::__compressed_pair.97"* %this, %"class.std::__2::__compressed_pair.97"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.97"*, %"class.std::__2::__compressed_pair.97"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.97"* %this1 to %"struct.std::__2::__compressed_pair_elem.99"*
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.100"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco26AttributesDecoderInterfaceEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.99"* %0) #5
  ret %"struct.std::__2::default_delete.100"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__214default_deleteIN5draco26AttributesDecoderInterfaceEEclEPS2_(%"struct.std::__2::default_delete.100"* %this, %"class.draco::AttributesDecoderInterface"* %__ptr) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::default_delete.100"*, align 4
  %__ptr.addr = alloca %"class.draco::AttributesDecoderInterface"*, align 4
  store %"struct.std::__2::default_delete.100"* %this, %"struct.std::__2::default_delete.100"** %this.addr, align 4
  store %"class.draco::AttributesDecoderInterface"* %__ptr, %"class.draco::AttributesDecoderInterface"** %__ptr.addr, align 4
  %this1 = load %"struct.std::__2::default_delete.100"*, %"struct.std::__2::default_delete.100"** %this.addr, align 4
  %0 = load %"class.draco::AttributesDecoderInterface"*, %"class.draco::AttributesDecoderInterface"** %__ptr.addr, align 4
  %isnull = icmp eq %"class.draco::AttributesDecoderInterface"* %0, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %entry
  %1 = bitcast %"class.draco::AttributesDecoderInterface"* %0 to void (%"class.draco::AttributesDecoderInterface"*)***
  %vtable = load void (%"class.draco::AttributesDecoderInterface"*)**, void (%"class.draco::AttributesDecoderInterface"*)*** %1, align 4
  %vfn = getelementptr inbounds void (%"class.draco::AttributesDecoderInterface"*)*, void (%"class.draco::AttributesDecoderInterface"*)** %vtable, i64 1
  %2 = load void (%"class.draco::AttributesDecoderInterface"*)*, void (%"class.draco::AttributesDecoderInterface"*)** %vfn, align 4
  call void %2(%"class.draco::AttributesDecoderInterface"* %0) #5
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::AttributesDecoderInterface"** @_ZNSt3__222__compressed_pair_elemIPN5draco26AttributesDecoderInterfaceELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.98"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.98"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.98"* %this, %"struct.std::__2::__compressed_pair_elem.98"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.98"*, %"struct.std::__2::__compressed_pair_elem.98"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.98", %"struct.std::__2::__compressed_pair_elem.98"* %this1, i32 0, i32 0
  ret %"class.draco::AttributesDecoderInterface"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.100"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco26AttributesDecoderInterfaceEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.99"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.99"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.99"* %this, %"struct.std::__2::__compressed_pair_elem.99"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.99"*, %"struct.std::__2::__compressed_pair_elem.99"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.99"* %this1 to %"struct.std::__2::default_delete.100"*
  ret %"struct.std::__2::default_delete.100"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEE10deallocateEPS6_m(%"class.std::__2::allocator.104"* %this, %"class.std::__2::unique_ptr.96"* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.104"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.104"* %this, %"class.std::__2::allocator.104"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.96"* %__p, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.104"*, %"class.std::__2::allocator.104"** %this.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  %1 = bitcast %"class.std::__2::unique_ptr.96"* %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE6secondEv(%"class.std::__2::__compressed_pair.101"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.101"*, align 4
  store %"class.std::__2::__compressed_pair.101"* %this, %"class.std::__2::__compressed_pair.101"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.101"*, %"class.std::__2::__compressed_pair.101"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.101"* %this1 to %"struct.std::__2::__compressed_pair_elem.103"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.103"* %0) #5
  ret %"class.std::__2::allocator.104"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.103"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.103"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.103"* %this, %"struct.std::__2::__compressed_pair_elem.103"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.103"*, %"struct.std::__2::__compressed_pair_elem.103"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.103"* %this1 to %"class.std::__2::allocator.104"*
  ret %"class.std::__2::allocator.104"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::MeshEdgebreakerDecoderImplInterface"** @_ZNKSt3__217__compressed_pairIPN5draco35MeshEdgebreakerDecoderImplInterfaceENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.123"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.123"*, align 4
  store %"class.std::__2::__compressed_pair.123"* %this, %"class.std::__2::__compressed_pair.123"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.123"*, %"class.std::__2::__compressed_pair.123"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.123"* %this1 to %"struct.std::__2::__compressed_pair_elem.124"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::MeshEdgebreakerDecoderImplInterface"** @_ZNKSt3__222__compressed_pair_elemIPN5draco35MeshEdgebreakerDecoderImplInterfaceELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.124"* %0) #5
  ret %"class.draco::MeshEdgebreakerDecoderImplInterface"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::MeshEdgebreakerDecoderImplInterface"** @_ZNKSt3__222__compressed_pair_elemIPN5draco35MeshEdgebreakerDecoderImplInterfaceELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.124"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.124"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.124"* %this, %"struct.std::__2::__compressed_pair_elem.124"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.124"*, %"struct.std::__2::__compressed_pair_elem.124"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.124", %"struct.std::__2::__compressed_pair_elem.124"* %this1, i32 0, i32 0
  ret %"class.draco::MeshEdgebreakerDecoderImplInterface"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__210unique_ptrIN5draco35MeshEdgebreakerDecoderImplInterfaceENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.122"* %this, %"class.draco::MeshEdgebreakerDecoderImplInterface"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.122"*, align 4
  %__p.addr = alloca %"class.draco::MeshEdgebreakerDecoderImplInterface"*, align 4
  %__tmp = alloca %"class.draco::MeshEdgebreakerDecoderImplInterface"*, align 4
  store %"class.std::__2::unique_ptr.122"* %this, %"class.std::__2::unique_ptr.122"** %this.addr, align 4
  store %"class.draco::MeshEdgebreakerDecoderImplInterface"* %__p, %"class.draco::MeshEdgebreakerDecoderImplInterface"** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.122"*, %"class.std::__2::unique_ptr.122"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.122", %"class.std::__2::unique_ptr.122"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::MeshEdgebreakerDecoderImplInterface"** @_ZNSt3__217__compressed_pairIPN5draco35MeshEdgebreakerDecoderImplInterfaceENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.123"* %__ptr_) #5
  %0 = load %"class.draco::MeshEdgebreakerDecoderImplInterface"*, %"class.draco::MeshEdgebreakerDecoderImplInterface"** %call, align 4
  store %"class.draco::MeshEdgebreakerDecoderImplInterface"* %0, %"class.draco::MeshEdgebreakerDecoderImplInterface"** %__tmp, align 4
  %1 = load %"class.draco::MeshEdgebreakerDecoderImplInterface"*, %"class.draco::MeshEdgebreakerDecoderImplInterface"** %__p.addr, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr.122", %"class.std::__2::unique_ptr.122"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::MeshEdgebreakerDecoderImplInterface"** @_ZNSt3__217__compressed_pairIPN5draco35MeshEdgebreakerDecoderImplInterfaceENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.123"* %__ptr_2) #5
  store %"class.draco::MeshEdgebreakerDecoderImplInterface"* %1, %"class.draco::MeshEdgebreakerDecoderImplInterface"** %call3, align 4
  %2 = load %"class.draco::MeshEdgebreakerDecoderImplInterface"*, %"class.draco::MeshEdgebreakerDecoderImplInterface"** %__tmp, align 4
  %tobool = icmp ne %"class.draco::MeshEdgebreakerDecoderImplInterface"* %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__ptr_4 = getelementptr inbounds %"class.std::__2::unique_ptr.122", %"class.std::__2::unique_ptr.122"* %this1, i32 0, i32 0
  %call5 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.126"* @_ZNSt3__217__compressed_pairIPN5draco35MeshEdgebreakerDecoderImplInterfaceENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.123"* %__ptr_4) #5
  %3 = load %"class.draco::MeshEdgebreakerDecoderImplInterface"*, %"class.draco::MeshEdgebreakerDecoderImplInterface"** %__tmp, align 4
  call void @_ZNKSt3__214default_deleteIN5draco35MeshEdgebreakerDecoderImplInterfaceEEclEPS2_(%"struct.std::__2::default_delete.126"* %call5, %"class.draco::MeshEdgebreakerDecoderImplInterface"* %3) #5
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::MeshEdgebreakerDecoderImplInterface"** @_ZNSt3__217__compressed_pairIPN5draco35MeshEdgebreakerDecoderImplInterfaceENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.123"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.123"*, align 4
  store %"class.std::__2::__compressed_pair.123"* %this, %"class.std::__2::__compressed_pair.123"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.123"*, %"class.std::__2::__compressed_pair.123"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.123"* %this1 to %"struct.std::__2::__compressed_pair_elem.124"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::MeshEdgebreakerDecoderImplInterface"** @_ZNSt3__222__compressed_pair_elemIPN5draco35MeshEdgebreakerDecoderImplInterfaceELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.124"* %0) #5
  ret %"class.draco::MeshEdgebreakerDecoderImplInterface"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.126"* @_ZNSt3__217__compressed_pairIPN5draco35MeshEdgebreakerDecoderImplInterfaceENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.123"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.123"*, align 4
  store %"class.std::__2::__compressed_pair.123"* %this, %"class.std::__2::__compressed_pair.123"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.123"*, %"class.std::__2::__compressed_pair.123"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.123"* %this1 to %"struct.std::__2::__compressed_pair_elem.125"*
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.126"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco35MeshEdgebreakerDecoderImplInterfaceEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.125"* %0) #5
  ret %"struct.std::__2::default_delete.126"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__214default_deleteIN5draco35MeshEdgebreakerDecoderImplInterfaceEEclEPS2_(%"struct.std::__2::default_delete.126"* %this, %"class.draco::MeshEdgebreakerDecoderImplInterface"* %__ptr) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::default_delete.126"*, align 4
  %__ptr.addr = alloca %"class.draco::MeshEdgebreakerDecoderImplInterface"*, align 4
  store %"struct.std::__2::default_delete.126"* %this, %"struct.std::__2::default_delete.126"** %this.addr, align 4
  store %"class.draco::MeshEdgebreakerDecoderImplInterface"* %__ptr, %"class.draco::MeshEdgebreakerDecoderImplInterface"** %__ptr.addr, align 4
  %this1 = load %"struct.std::__2::default_delete.126"*, %"struct.std::__2::default_delete.126"** %this.addr, align 4
  %0 = load %"class.draco::MeshEdgebreakerDecoderImplInterface"*, %"class.draco::MeshEdgebreakerDecoderImplInterface"** %__ptr.addr, align 4
  %isnull = icmp eq %"class.draco::MeshEdgebreakerDecoderImplInterface"* %0, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %entry
  %1 = bitcast %"class.draco::MeshEdgebreakerDecoderImplInterface"* %0 to void (%"class.draco::MeshEdgebreakerDecoderImplInterface"*)***
  %vtable = load void (%"class.draco::MeshEdgebreakerDecoderImplInterface"*)**, void (%"class.draco::MeshEdgebreakerDecoderImplInterface"*)*** %1, align 4
  %vfn = getelementptr inbounds void (%"class.draco::MeshEdgebreakerDecoderImplInterface"*)*, void (%"class.draco::MeshEdgebreakerDecoderImplInterface"*)** %vtable, i64 1
  %2 = load void (%"class.draco::MeshEdgebreakerDecoderImplInterface"*)*, void (%"class.draco::MeshEdgebreakerDecoderImplInterface"*)** %vfn, align 4
  call void %2(%"class.draco::MeshEdgebreakerDecoderImplInterface"* %0) #5
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::MeshEdgebreakerDecoderImplInterface"** @_ZNSt3__222__compressed_pair_elemIPN5draco35MeshEdgebreakerDecoderImplInterfaceELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.124"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.124"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.124"* %this, %"struct.std::__2::__compressed_pair_elem.124"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.124"*, %"struct.std::__2::__compressed_pair_elem.124"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.124", %"struct.std::__2::__compressed_pair_elem.124"* %this1, i32 0, i32 0
  ret %"class.draco::MeshEdgebreakerDecoderImplInterface"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.126"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco35MeshEdgebreakerDecoderImplInterfaceEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.125"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.125"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.125"* %this, %"struct.std::__2::__compressed_pair_elem.125"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.125"*, %"struct.std::__2::__compressed_pair_elem.125"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.125"* %this1 to %"struct.std::__2::default_delete.126"*
  ret %"struct.std::__2::default_delete.126"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco13DecoderBuffer4PeekIhEEbPT_(%"class.draco::DecoderBuffer"* %this, i8* %out_val) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %out_val.addr = alloca i8*, align 4
  %size_to_decode = alloca i32, align 4
  store %"class.draco::DecoderBuffer"* %this, %"class.draco::DecoderBuffer"** %this.addr, align 4
  store i8* %out_val, i8** %out_val.addr, align 4
  %this1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %this.addr, align 4
  store i32 1, i32* %size_to_decode, align 4
  %data_size_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 1
  %0 = load i64, i64* %data_size_, align 8
  %pos_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %1 = load i64, i64* %pos_, align 8
  %add = add nsw i64 %1, 1
  %cmp = icmp slt i64 %0, %add
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %2 = load i8*, i8** %out_val.addr, align 4
  %data_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 0
  %3 = load i8*, i8** %data_, align 8
  %pos_2 = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %4 = load i64, i64* %pos_2, align 8
  %idx.ext = trunc i64 %4 to i32
  %add.ptr = getelementptr inbounds i8, i8* %3, i32 %idx.ext
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %2, i8* align 1 %add.ptr, i32 1, i1 false)
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %5 = load i1, i1* %retval, align 1
  ret i1 %5
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.123"* @_ZNSt3__217__compressed_pairIPN5draco35MeshEdgebreakerDecoderImplInterfaceENS_14default_deleteIS2_EEEC2IRS3_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.123"* returned %this, %"class.draco::MeshEdgebreakerDecoderImplInterface"** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.123"*, align 4
  %__t1.addr = alloca %"class.draco::MeshEdgebreakerDecoderImplInterface"**, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.123"* %this, %"class.std::__2::__compressed_pair.123"** %this.addr, align 4
  store %"class.draco::MeshEdgebreakerDecoderImplInterface"** %__t1, %"class.draco::MeshEdgebreakerDecoderImplInterface"*** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.123"*, %"class.std::__2::__compressed_pair.123"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.123"* %this1 to %"struct.std::__2::__compressed_pair_elem.124"*
  %1 = load %"class.draco::MeshEdgebreakerDecoderImplInterface"**, %"class.draco::MeshEdgebreakerDecoderImplInterface"*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::MeshEdgebreakerDecoderImplInterface"** @_ZNSt3__27forwardIRPN5draco35MeshEdgebreakerDecoderImplInterfaceEEEOT_RNS_16remove_referenceIS5_E4typeE(%"class.draco::MeshEdgebreakerDecoderImplInterface"** nonnull align 4 dereferenceable(4) %1) #5
  %call2 = call %"struct.std::__2::__compressed_pair_elem.124"* @_ZNSt3__222__compressed_pair_elemIPN5draco35MeshEdgebreakerDecoderImplInterfaceELi0ELb0EEC2IRS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.124"* %0, %"class.draco::MeshEdgebreakerDecoderImplInterface"** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.123"* %this1 to %"struct.std::__2::__compressed_pair_elem.125"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #5
  %call4 = call %"struct.std::__2::__compressed_pair_elem.125"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco35MeshEdgebreakerDecoderImplInterfaceEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.125"* %2)
  ret %"class.std::__2::__compressed_pair.123"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::MeshEdgebreakerDecoderImplInterface"** @_ZNSt3__27forwardIRPN5draco35MeshEdgebreakerDecoderImplInterfaceEEEOT_RNS_16remove_referenceIS5_E4typeE(%"class.draco::MeshEdgebreakerDecoderImplInterface"** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.draco::MeshEdgebreakerDecoderImplInterface"**, align 4
  store %"class.draco::MeshEdgebreakerDecoderImplInterface"** %__t, %"class.draco::MeshEdgebreakerDecoderImplInterface"*** %__t.addr, align 4
  %0 = load %"class.draco::MeshEdgebreakerDecoderImplInterface"**, %"class.draco::MeshEdgebreakerDecoderImplInterface"*** %__t.addr, align 4
  ret %"class.draco::MeshEdgebreakerDecoderImplInterface"** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.124"* @_ZNSt3__222__compressed_pair_elemIPN5draco35MeshEdgebreakerDecoderImplInterfaceELi0ELb0EEC2IRS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.124"* returned %this, %"class.draco::MeshEdgebreakerDecoderImplInterface"** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.124"*, align 4
  %__u.addr = alloca %"class.draco::MeshEdgebreakerDecoderImplInterface"**, align 4
  store %"struct.std::__2::__compressed_pair_elem.124"* %this, %"struct.std::__2::__compressed_pair_elem.124"** %this.addr, align 4
  store %"class.draco::MeshEdgebreakerDecoderImplInterface"** %__u, %"class.draco::MeshEdgebreakerDecoderImplInterface"*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.124"*, %"struct.std::__2::__compressed_pair_elem.124"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.124", %"struct.std::__2::__compressed_pair_elem.124"* %this1, i32 0, i32 0
  %0 = load %"class.draco::MeshEdgebreakerDecoderImplInterface"**, %"class.draco::MeshEdgebreakerDecoderImplInterface"*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::MeshEdgebreakerDecoderImplInterface"** @_ZNSt3__27forwardIRPN5draco35MeshEdgebreakerDecoderImplInterfaceEEEOT_RNS_16remove_referenceIS5_E4typeE(%"class.draco::MeshEdgebreakerDecoderImplInterface"** nonnull align 4 dereferenceable(4) %0) #5
  %1 = load %"class.draco::MeshEdgebreakerDecoderImplInterface"*, %"class.draco::MeshEdgebreakerDecoderImplInterface"** %call, align 4
  store %"class.draco::MeshEdgebreakerDecoderImplInterface"* %1, %"class.draco::MeshEdgebreakerDecoderImplInterface"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.124"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::MeshEdgebreakerDecoderImplInterface"* @_ZNSt3__210unique_ptrIN5draco35MeshEdgebreakerDecoderImplInterfaceENS_14default_deleteIS2_EEE7releaseEv(%"class.std::__2::unique_ptr.122"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.122"*, align 4
  %__t = alloca %"class.draco::MeshEdgebreakerDecoderImplInterface"*, align 4
  store %"class.std::__2::unique_ptr.122"* %this, %"class.std::__2::unique_ptr.122"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.122"*, %"class.std::__2::unique_ptr.122"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.122", %"class.std::__2::unique_ptr.122"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::MeshEdgebreakerDecoderImplInterface"** @_ZNSt3__217__compressed_pairIPN5draco35MeshEdgebreakerDecoderImplInterfaceENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.123"* %__ptr_) #5
  %0 = load %"class.draco::MeshEdgebreakerDecoderImplInterface"*, %"class.draco::MeshEdgebreakerDecoderImplInterface"** %call, align 4
  store %"class.draco::MeshEdgebreakerDecoderImplInterface"* %0, %"class.draco::MeshEdgebreakerDecoderImplInterface"** %__t, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr.122", %"class.std::__2::unique_ptr.122"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::MeshEdgebreakerDecoderImplInterface"** @_ZNSt3__217__compressed_pairIPN5draco35MeshEdgebreakerDecoderImplInterfaceENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.123"* %__ptr_2) #5
  store %"class.draco::MeshEdgebreakerDecoderImplInterface"* null, %"class.draco::MeshEdgebreakerDecoderImplInterface"** %call3, align 4
  %1 = load %"class.draco::MeshEdgebreakerDecoderImplInterface"*, %"class.draco::MeshEdgebreakerDecoderImplInterface"** %__t, align 4
  ret %"class.draco::MeshEdgebreakerDecoderImplInterface"* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.126"* @_ZNSt3__27forwardINS_14default_deleteIN5draco35MeshEdgebreakerDecoderImplInterfaceEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.126"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"struct.std::__2::default_delete.126"*, align 4
  store %"struct.std::__2::default_delete.126"* %__t, %"struct.std::__2::default_delete.126"** %__t.addr, align 4
  %0 = load %"struct.std::__2::default_delete.126"*, %"struct.std::__2::default_delete.126"** %__t.addr, align 4
  ret %"struct.std::__2::default_delete.126"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.126"* @_ZNSt3__210unique_ptrIN5draco35MeshEdgebreakerDecoderImplInterfaceENS_14default_deleteIS2_EEE11get_deleterEv(%"class.std::__2::unique_ptr.122"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.122"*, align 4
  store %"class.std::__2::unique_ptr.122"* %this, %"class.std::__2::unique_ptr.122"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.122"*, %"class.std::__2::unique_ptr.122"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.122", %"class.std::__2::unique_ptr.122"* %this1, i32 0, i32 0
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.126"* @_ZNSt3__217__compressed_pairIPN5draco35MeshEdgebreakerDecoderImplInterfaceENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.123"* %__ptr_) #5
  ret %"struct.std::__2::default_delete.126"* %call
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { nobuiltin allocsize(0) "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { argmemonly nounwind willreturn }
attributes #5 = { nounwind }
attributes #6 = { builtin allocsize(0) }
attributes #7 = { builtin nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
