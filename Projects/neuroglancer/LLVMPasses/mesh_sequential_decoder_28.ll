; ModuleID = './draco/src/draco/compression/mesh/mesh_sequential_decoder.cc'
source_filename = "./draco/src/draco/compression/mesh/mesh_sequential_decoder.cc"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%"class.draco::MeshSequentialDecoder" = type { %"class.draco::MeshDecoder" }
%"class.draco::MeshDecoder" = type { %"class.draco::PointCloudDecoder", %"class.draco::Mesh"* }
%"class.draco::PointCloudDecoder" = type { i32 (...)**, %"class.draco::PointCloud"*, %"class.std::__2::vector.94", %"class.std::__2::vector.87", %"class.draco::DecoderBuffer"*, i8, i8, %"class.draco::DracoOptions"* }
%"class.draco::PointCloud" = type { i32 (...)**, %"class.std::__2::unique_ptr", %"class.std::__2::vector.51", [5 x %"class.std::__2::vector.87"], i32 }
%"class.std::__2::unique_ptr" = type { %"class.std::__2::__compressed_pair" }
%"class.std::__2::__compressed_pair" = type { %"struct.std::__2::__compressed_pair_elem" }
%"struct.std::__2::__compressed_pair_elem" = type { %"class.draco::GeometryMetadata"* }
%"class.draco::GeometryMetadata" = type { %"class.draco::Metadata", %"class.std::__2::vector" }
%"class.draco::Metadata" = type { %"class.std::__2::unordered_map", %"class.std::__2::unordered_map.17" }
%"class.std::__2::unordered_map" = type { %"class.std::__2::__hash_table" }
%"class.std::__2::__hash_table" = type { %"class.std::__2::unique_ptr.0", %"class.std::__2::__compressed_pair.7", %"class.std::__2::__compressed_pair.12", %"class.std::__2::__compressed_pair.14" }
%"class.std::__2::unique_ptr.0" = type { %"class.std::__2::__compressed_pair.1" }
%"class.std::__2::__compressed_pair.1" = type { %"struct.std::__2::__compressed_pair_elem.2", %"struct.std::__2::__compressed_pair_elem.3" }
%"struct.std::__2::__compressed_pair_elem.2" = type { %"struct.std::__2::__hash_node_base"** }
%"struct.std::__2::__hash_node_base" = type { %"struct.std::__2::__hash_node_base"* }
%"struct.std::__2::__compressed_pair_elem.3" = type { %"class.std::__2::__bucket_list_deallocator" }
%"class.std::__2::__bucket_list_deallocator" = type { %"class.std::__2::__compressed_pair.4" }
%"class.std::__2::__compressed_pair.4" = type { %"struct.std::__2::__compressed_pair_elem.5" }
%"struct.std::__2::__compressed_pair_elem.5" = type { i32 }
%"class.std::__2::__compressed_pair.7" = type { %"struct.std::__2::__compressed_pair_elem.8" }
%"struct.std::__2::__compressed_pair_elem.8" = type { %"struct.std::__2::__hash_node_base" }
%"class.std::__2::__compressed_pair.12" = type { %"struct.std::__2::__compressed_pair_elem.5" }
%"class.std::__2::__compressed_pair.14" = type { %"struct.std::__2::__compressed_pair_elem.15" }
%"struct.std::__2::__compressed_pair_elem.15" = type { float }
%"class.std::__2::unordered_map.17" = type { %"class.std::__2::__hash_table.18" }
%"class.std::__2::__hash_table.18" = type { %"class.std::__2::unique_ptr.19", %"class.std::__2::__compressed_pair.29", %"class.std::__2::__compressed_pair.34", %"class.std::__2::__compressed_pair.37" }
%"class.std::__2::unique_ptr.19" = type { %"class.std::__2::__compressed_pair.20" }
%"class.std::__2::__compressed_pair.20" = type { %"struct.std::__2::__compressed_pair_elem.21", %"struct.std::__2::__compressed_pair_elem.23" }
%"struct.std::__2::__compressed_pair_elem.21" = type { %"struct.std::__2::__hash_node_base.22"** }
%"struct.std::__2::__hash_node_base.22" = type { %"struct.std::__2::__hash_node_base.22"* }
%"struct.std::__2::__compressed_pair_elem.23" = type { %"class.std::__2::__bucket_list_deallocator.24" }
%"class.std::__2::__bucket_list_deallocator.24" = type { %"class.std::__2::__compressed_pair.25" }
%"class.std::__2::__compressed_pair.25" = type { %"struct.std::__2::__compressed_pair_elem.5" }
%"class.std::__2::__compressed_pair.29" = type { %"struct.std::__2::__compressed_pair_elem.30" }
%"struct.std::__2::__compressed_pair_elem.30" = type { %"struct.std::__2::__hash_node_base.22" }
%"class.std::__2::__compressed_pair.34" = type { %"struct.std::__2::__compressed_pair_elem.5" }
%"class.std::__2::__compressed_pair.37" = type { %"struct.std::__2::__compressed_pair_elem.15" }
%"class.std::__2::vector" = type { %"class.std::__2::__vector_base" }
%"class.std::__2::__vector_base" = type { %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"*, %"class.std::__2::__compressed_pair.44" }
%"class.std::__2::unique_ptr.40" = type { %"class.std::__2::__compressed_pair.41" }
%"class.std::__2::__compressed_pair.41" = type { %"struct.std::__2::__compressed_pair_elem.42" }
%"struct.std::__2::__compressed_pair_elem.42" = type { %"class.draco::AttributeMetadata"* }
%"class.draco::AttributeMetadata" = type { %"class.draco::Metadata", i32 }
%"class.std::__2::__compressed_pair.44" = type { %"struct.std::__2::__compressed_pair_elem.45" }
%"struct.std::__2::__compressed_pair_elem.45" = type { %"class.std::__2::unique_ptr.40"* }
%"class.std::__2::vector.51" = type { %"class.std::__2::__vector_base.52" }
%"class.std::__2::__vector_base.52" = type { %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"*, %"class.std::__2::__compressed_pair.82" }
%"class.std::__2::unique_ptr.53" = type { %"class.std::__2::__compressed_pair.54" }
%"class.std::__2::__compressed_pair.54" = type { %"struct.std::__2::__compressed_pair_elem.55" }
%"struct.std::__2::__compressed_pair_elem.55" = type { %"class.draco::PointAttribute"* }
%"class.draco::PointAttribute" = type <{ %"class.draco::GeometryAttribute", %"class.std::__2::unique_ptr.63", %"class.draco::IndexTypeVector", i32, i8, [3 x i8], %"class.std::__2::unique_ptr.75", [4 x i8] }>
%"class.draco::GeometryAttribute" = type { %"class.draco::DataBuffer"*, %"struct.draco::DataBufferDescriptor", i8, i32, i8, i64, i64, i32, i32 }
%"class.draco::DataBuffer" = type { %"class.std::__2::vector.56", %"struct.draco::DataBufferDescriptor" }
%"class.std::__2::vector.56" = type { %"class.std::__2::__vector_base.57" }
%"class.std::__2::__vector_base.57" = type { i8*, i8*, %"class.std::__2::__compressed_pair.58" }
%"class.std::__2::__compressed_pair.58" = type { %"struct.std::__2::__compressed_pair_elem.59" }
%"struct.std::__2::__compressed_pair_elem.59" = type { i8* }
%"struct.draco::DataBufferDescriptor" = type { i64, i64 }
%"class.std::__2::unique_ptr.63" = type { %"class.std::__2::__compressed_pair.64" }
%"class.std::__2::__compressed_pair.64" = type { %"struct.std::__2::__compressed_pair_elem.65" }
%"struct.std::__2::__compressed_pair_elem.65" = type { %"class.draco::DataBuffer"* }
%"class.draco::IndexTypeVector" = type { %"class.std::__2::vector.68" }
%"class.std::__2::vector.68" = type { %"class.std::__2::__vector_base.69" }
%"class.std::__2::__vector_base.69" = type { %"class.draco::IndexType"*, %"class.draco::IndexType"*, %"class.std::__2::__compressed_pair.70" }
%"class.draco::IndexType" = type { i32 }
%"class.std::__2::__compressed_pair.70" = type { %"struct.std::__2::__compressed_pair_elem.71" }
%"struct.std::__2::__compressed_pair_elem.71" = type { %"class.draco::IndexType"* }
%"class.std::__2::unique_ptr.75" = type { %"class.std::__2::__compressed_pair.76" }
%"class.std::__2::__compressed_pair.76" = type { %"struct.std::__2::__compressed_pair_elem.77" }
%"struct.std::__2::__compressed_pair_elem.77" = type { %"class.draco::AttributeTransformData"* }
%"class.draco::AttributeTransformData" = type { i32, %"class.draco::DataBuffer" }
%"class.std::__2::__compressed_pair.82" = type { %"struct.std::__2::__compressed_pair_elem.83" }
%"struct.std::__2::__compressed_pair_elem.83" = type { %"class.std::__2::unique_ptr.53"* }
%"class.std::__2::vector.94" = type { %"class.std::__2::__vector_base.95" }
%"class.std::__2::__vector_base.95" = type { %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"*, %"class.std::__2::__compressed_pair.101" }
%"class.std::__2::unique_ptr.96" = type { %"class.std::__2::__compressed_pair.97" }
%"class.std::__2::__compressed_pair.97" = type { %"struct.std::__2::__compressed_pair_elem.98" }
%"struct.std::__2::__compressed_pair_elem.98" = type { %"class.draco::AttributesDecoderInterface"* }
%"class.draco::AttributesDecoderInterface" = type { i32 (...)** }
%"class.std::__2::__compressed_pair.101" = type { %"struct.std::__2::__compressed_pair_elem.102" }
%"struct.std::__2::__compressed_pair_elem.102" = type { %"class.std::__2::unique_ptr.96"* }
%"class.std::__2::vector.87" = type { %"class.std::__2::__vector_base.88" }
%"class.std::__2::__vector_base.88" = type { i32*, i32*, %"class.std::__2::__compressed_pair.89" }
%"class.std::__2::__compressed_pair.89" = type { %"struct.std::__2::__compressed_pair_elem.90" }
%"struct.std::__2::__compressed_pair_elem.90" = type { i32* }
%"class.draco::DecoderBuffer" = type { i8*, i64, i64, %"class.draco::DecoderBuffer::BitDecoder", i8, i16 }
%"class.draco::DecoderBuffer::BitDecoder" = type { i8*, i8*, i32 }
%"class.draco::DracoOptions" = type opaque
%"class.draco::Mesh" = type { %"class.draco::PointCloud", %"class.std::__2::vector.106", %"class.draco::IndexTypeVector.113" }
%"class.std::__2::vector.106" = type { %"class.std::__2::__vector_base.107" }
%"class.std::__2::__vector_base.107" = type { %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"*, %"class.std::__2::__compressed_pair.108" }
%"struct.draco::Mesh::AttributeData" = type { i32 }
%"class.std::__2::__compressed_pair.108" = type { %"struct.std::__2::__compressed_pair_elem.109" }
%"struct.std::__2::__compressed_pair_elem.109" = type { %"struct.draco::Mesh::AttributeData"* }
%"class.draco::IndexTypeVector.113" = type { %"class.std::__2::vector.114" }
%"class.std::__2::vector.114" = type { %"class.std::__2::__vector_base.115" }
%"class.std::__2::__vector_base.115" = type { %"struct.std::__2::array"*, %"struct.std::__2::array"*, %"class.std::__2::__compressed_pair.117" }
%"struct.std::__2::array" = type { [3 x %"class.draco::IndexType.116"] }
%"class.draco::IndexType.116" = type { i32 }
%"class.std::__2::__compressed_pair.117" = type { %"struct.std::__2::__compressed_pair_elem.118" }
%"struct.std::__2::__compressed_pair_elem.118" = type { %"struct.std::__2::array"* }
%"class.std::__2::vector.151" = type { %"class.std::__2::__vector_base.152" }
%"class.std::__2::__vector_base.152" = type { i32*, i32*, %"class.std::__2::__compressed_pair.153" }
%"class.std::__2::__compressed_pair.153" = type { %"struct.std::__2::__compressed_pair_elem.154" }
%"struct.std::__2::__compressed_pair_elem.154" = type { i32* }
%"class.std::__2::unique_ptr.122" = type { %"class.std::__2::__compressed_pair.123" }
%"class.std::__2::__compressed_pair.123" = type { %"struct.std::__2::__compressed_pair_elem.124" }
%"struct.std::__2::__compressed_pair_elem.124" = type { %"class.draco::AttributesDecoder"* }
%"class.draco::AttributesDecoder" = type { %"class.draco::AttributesDecoderInterface", %"class.std::__2::vector.87", %"class.std::__2::vector.87", %"class.draco::PointCloudDecoder"*, %"class.draco::PointCloud"* }
%"class.std::__2::unique_ptr.146" = type { %"class.std::__2::__compressed_pair.147" }
%"class.std::__2::__compressed_pair.147" = type { %"struct.std::__2::__compressed_pair_elem.148" }
%"struct.std::__2::__compressed_pair_elem.148" = type { %"class.draco::PointsSequencer"* }
%"class.draco::PointsSequencer" = type { i32 (...)**, %"class.std::__2::vector.139"* }
%"class.std::__2::vector.139" = type { %"class.std::__2::__vector_base.140" }
%"class.std::__2::__vector_base.140" = type { %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"*, %"class.std::__2::__compressed_pair.141" }
%"class.std::__2::__compressed_pair.141" = type { %"struct.std::__2::__compressed_pair_elem.142" }
%"struct.std::__2::__compressed_pair_elem.142" = type { %"class.draco::IndexType.116"* }
%"class.draco::SequentialAttributeDecodersController" = type { %"class.draco::AttributesDecoder", %"class.std::__2::vector.127", %"class.std::__2::vector.139", %"class.std::__2::unique_ptr.146" }
%"class.std::__2::vector.127" = type { %"class.std::__2::__vector_base.128" }
%"class.std::__2::__vector_base.128" = type { %"class.std::__2::unique_ptr.129"*, %"class.std::__2::unique_ptr.129"*, %"class.std::__2::__compressed_pair.134" }
%"class.std::__2::unique_ptr.129" = type { %"class.std::__2::__compressed_pair.130" }
%"class.std::__2::__compressed_pair.130" = type { %"struct.std::__2::__compressed_pair_elem.131" }
%"struct.std::__2::__compressed_pair_elem.131" = type { %"class.draco::SequentialAttributeDecoder"* }
%"class.draco::SequentialAttributeDecoder" = type { i32 (...)**, %"class.draco::PointCloudDecoder"*, %"class.draco::PointAttribute"*, i32, %"class.std::__2::unique_ptr.53" }
%"class.std::__2::__compressed_pair.134" = type { %"struct.std::__2::__compressed_pair_elem.135" }
%"struct.std::__2::__compressed_pair_elem.135" = type { %"class.std::__2::unique_ptr.129"* }
%"class.draco::LinearSequencer" = type { %"class.draco::PointsSequencer", i32 }
%"struct.std::__2::__default_init_tag" = type { i8 }
%"struct.std::__2::default_delete.126" = type { i8 }
%"class.draco::CornerTable" = type { %"class.draco::IndexTypeVector.158", %"class.draco::IndexTypeVector.167", %"class.draco::IndexTypeVector.176", i32, i32, i32, %"class.draco::IndexTypeVector.177", %"class.draco::ValenceCache" }
%"class.draco::IndexTypeVector.158" = type { %"class.std::__2::vector.159" }
%"class.std::__2::vector.159" = type { %"class.std::__2::__vector_base.160" }
%"class.std::__2::__vector_base.160" = type { %"class.draco::IndexType.161"*, %"class.draco::IndexType.161"*, %"class.std::__2::__compressed_pair.162" }
%"class.draco::IndexType.161" = type { i32 }
%"class.std::__2::__compressed_pair.162" = type { %"struct.std::__2::__compressed_pair_elem.163" }
%"struct.std::__2::__compressed_pair_elem.163" = type { %"class.draco::IndexType.161"* }
%"class.draco::IndexTypeVector.167" = type { %"class.std::__2::vector.168" }
%"class.std::__2::vector.168" = type { %"class.std::__2::__vector_base.169" }
%"class.std::__2::__vector_base.169" = type { %"class.draco::IndexType.170"*, %"class.draco::IndexType.170"*, %"class.std::__2::__compressed_pair.171" }
%"class.draco::IndexType.170" = type { i32 }
%"class.std::__2::__compressed_pair.171" = type { %"struct.std::__2::__compressed_pair_elem.172" }
%"struct.std::__2::__compressed_pair_elem.172" = type { %"class.draco::IndexType.170"* }
%"class.draco::IndexTypeVector.176" = type { %"class.std::__2::vector.168" }
%"class.draco::IndexTypeVector.177" = type { %"class.std::__2::vector.159" }
%"class.draco::ValenceCache" = type { %"class.draco::CornerTable"*, %"class.draco::IndexTypeVector.178", %"class.draco::IndexTypeVector.186" }
%"class.draco::IndexTypeVector.178" = type { %"class.std::__2::vector.179" }
%"class.std::__2::vector.179" = type { %"class.std::__2::__vector_base.180" }
%"class.std::__2::__vector_base.180" = type { i8*, i8*, %"class.std::__2::__compressed_pair.181" }
%"class.std::__2::__compressed_pair.181" = type { %"struct.std::__2::__compressed_pair_elem.182" }
%"struct.std::__2::__compressed_pair_elem.182" = type { i8* }
%"class.draco::IndexTypeVector.186" = type { %"class.std::__2::vector.87" }
%"class.draco::MeshAttributeCornerTable" = type { %"class.std::__2::vector.187", %"class.std::__2::vector.187", i8, %"class.std::__2::vector.159", %"class.std::__2::vector.168", %"class.std::__2::vector.68", %"class.draco::CornerTable"*, %"class.draco::ValenceCache.192" }
%"class.std::__2::vector.187" = type { i32*, i32, %"class.std::__2::__compressed_pair.188" }
%"class.std::__2::__compressed_pair.188" = type { %"struct.std::__2::__compressed_pair_elem.5" }
%"class.draco::ValenceCache.192" = type { %"class.draco::MeshAttributeCornerTable"*, %"class.draco::IndexTypeVector.178", %"class.draco::IndexTypeVector.186" }
%"struct.draco::MeshAttributeIndicesEncodingData" = type { %"class.std::__2::vector.168", %"class.std::__2::vector.87", i32 }
%"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction" = type { %"class.std::__2::vector.114"*, %"struct.std::__2::array"*, %"struct.std::__2::array"* }
%"class.std::__2::allocator.120" = type { i8 }
%"struct.std::__2::__split_buffer" = type { %"struct.std::__2::array"*, %"struct.std::__2::array"*, %"struct.std::__2::array"*, %"class.std::__2::__compressed_pair.193" }
%"class.std::__2::__compressed_pair.193" = type { %"struct.std::__2::__compressed_pair_elem.118", %"struct.std::__2::__compressed_pair_elem.194" }
%"struct.std::__2::__compressed_pair_elem.194" = type { %"class.std::__2::allocator.120"* }
%"struct.std::__2::integral_constant" = type { i8 }
%"struct.std::__2::__has_construct" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.119" = type { i8 }
%"class.std::__2::__vector_base_common" = type { i8 }
%"class.std::__2::__split_buffer_common" = type { i8 }
%"struct.std::__2::__less" = type { i8 }
%"struct.std::__2::__has_max_size" = type { i8 }
%"struct.std::__2::integral_constant.195" = type { i8 }
%"struct.std::__2::__has_destroy" = type { i8 }
%"struct.std::__2::default_delete.100" = type { i8 }
%"class.std::__2::allocator.104" = type { i8 }
%"struct.std::__2::__split_buffer.196" = type { %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"*, %"class.std::__2::__compressed_pair.197" }
%"class.std::__2::__compressed_pair.197" = type { %"struct.std::__2::__compressed_pair_elem.102", %"struct.std::__2::__compressed_pair_elem.198" }
%"struct.std::__2::__compressed_pair_elem.198" = type { %"class.std::__2::allocator.104"* }
%"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction" = type { %"class.std::__2::vector.94"*, %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"* }
%"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction" = type { %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** }
%"struct.std::__2::__has_construct.199" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.99" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.103" = type { i8 }
%"struct.std::__2::__has_max_size.200" = type { i8 }
%"struct.std::__2::__has_construct.201" = type { i8 }
%"struct.std::__2::__has_destroy.202" = type { i8 }
%"class.std::__2::allocator.73" = type { i8 }
%"struct.std::__2::__has_destroy.203" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.72" = type { i8 }
%"class.std::__2::allocator.144" = type { i8 }
%"struct.std::__2::__split_buffer.204" = type { %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"*, %"class.std::__2::__compressed_pair.205" }
%"class.std::__2::__compressed_pair.205" = type { %"struct.std::__2::__compressed_pair_elem.142", %"struct.std::__2::__compressed_pair_elem.206" }
%"struct.std::__2::__compressed_pair_elem.206" = type { %"class.std::__2::allocator.144"* }
%"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction" = type { %"class.std::__2::vector.139"*, %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"* }
%"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction" = type { %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** }
%"struct.std::__2::__has_construct.207" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.143" = type { i8 }
%"struct.std::__2::__has_max_size.208" = type { i8 }
%"struct.std::__2::__has_destroy.209" = type { i8 }
%"class.std::__2::allocator.92" = type { i8 }
%"struct.std::__2::__has_destroy.210" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.91" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.149" = type { i8 }
%"struct.std::__2::default_delete.150" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.125" = type { i8 }
%"class.std::__2::allocator.156" = type { i8 }
%"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction" = type { %"class.std::__2::vector.151"*, i32*, i32* }
%"struct.std::__2::__compressed_pair_elem.155" = type { i8 }
%"struct.std::__2::__has_max_size.211" = type { i8 }
%"struct.std::__2::__has_construct.212" = type { i8 }
%"struct.std::__2::__has_destroy.213" = type { i8 }

$_ZNK5draco17PointCloudDecoder17bitstream_versionEv = comdat any

$_ZN5draco17PointCloudDecoder6bufferEv = comdat any

$_ZN5draco13DecoderBuffer6DecodeIjEEbPT_ = comdat any

$_ZN5draco12DecodeVarintIjEEbPT_PNS_13DecoderBufferE = comdat any

$_ZN5draco13DecoderBuffer6DecodeIhEEbPT_ = comdat any

$_ZNSt3__25arrayIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELm3EEC2Ev = comdat any

$_ZNSt3__25arrayIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELm3EEixEm = comdat any

$_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEaSERKj = comdat any

$_ZNK5draco11MeshDecoder4meshEv = comdat any

$_ZN5draco4Mesh7AddFaceERKNSt3__25arrayINS_9IndexTypeIjNS_20PointIndex_tag_type_EEELm3EEE = comdat any

$_ZN5draco13DecoderBuffer6DecodeItEEbPT_ = comdat any

$_ZNK5draco10PointCloud10num_pointsEv = comdat any

$_ZN5draco17PointCloudDecoder11point_cloudEv = comdat any

$_ZN5draco10PointCloud14set_num_pointsEj = comdat any

$_ZN5draco17PointCloudDecoder20SetAttributesDecoderEiNSt3__210unique_ptrINS_26AttributesDecoderInterfaceENS1_14default_deleteIS3_EEEE = comdat any

$_ZN5draco15LinearSequencerC2Ei = comdat any

$_ZNSt3__210unique_ptrIN5draco15PointsSequencerENS_14default_deleteIS2_EEEC2ILb1EvEEPS2_ = comdat any

$_ZNSt3__210unique_ptrIN5draco17AttributesDecoderENS_14default_deleteIS2_EEEC2ILb1EvEEPS2_ = comdat any

$_ZNSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEEC2INS1_17AttributesDecoderENS3_IS7_EEvvEEONS0_IT_T0_EE = comdat any

$_ZNSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEED2Ev = comdat any

$_ZNSt3__210unique_ptrIN5draco17AttributesDecoderENS_14default_deleteIS2_EEED2Ev = comdat any

$_ZNSt3__210unique_ptrIN5draco15PointsSequencerENS_14default_deleteIS2_EEED2Ev = comdat any

$_ZNSt3__26vectorIjNS_9allocatorIjEEEC2Em = comdat any

$_ZNSt3__26vectorIjNS_9allocatorIjEEE4dataEv = comdat any

$_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm = comdat any

$_ZNSt3__26vectorIjNS_9allocatorIjEEED2Ev = comdat any

$_ZN5draco21MeshSequentialDecoderD2Ev = comdat any

$_ZN5draco21MeshSequentialDecoderD0Ev = comdat any

$_ZNK5draco11MeshDecoder15GetGeometryTypeEv = comdat any

$_ZN5draco17PointCloudDecoder17InitializeDecoderEv = comdat any

$_ZN5draco17PointCloudDecoder19OnAttributesDecodedEv = comdat any

$_ZNK5draco11MeshDecoder14GetCornerTableEv = comdat any

$_ZNK5draco11MeshDecoder23GetAttributeCornerTableEi = comdat any

$_ZNK5draco11MeshDecoder24GetAttributeEncodingDataEi = comdat any

$_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEC2Ev = comdat any

$_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEENSt3__25arrayINS1_IjNS_20PointIndex_tag_type_EEELm3EEEE9push_backERKS8_ = comdat any

$_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE9push_backERKS6_ = comdat any

$_ZNSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE9__end_capEv = comdat any

$_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE22__construct_one_at_endIJRKS6_EEEvDpOT_ = comdat any

$_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE21__push_back_slow_pathIRKS6_EEvOT_ = comdat any

$_ZNSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE5firstEv = comdat any

$_ZNSt3__222__compressed_pair_elemIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE21_ConstructTransactionC2ERS9_m = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE9constructIS7_JRKS7_EEEvRS8_PT_DpOT0_ = comdat any

$_ZNSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE7__allocEv = comdat any

$_ZNSt3__212__to_addressINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEEEPT_S8_ = comdat any

$_ZNSt3__27forwardIRKNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEEEOT_RNS_16remove_referenceIS9_E4typeE = comdat any

$_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE21_ConstructTransactionD2Ev = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE11__constructIS7_JRKS7_EEEvNS_17integral_constantIbLb1EEERS8_PT_DpOT0_ = comdat any

$_ZNSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEE9constructIS6_JRKS6_EEEvPT_DpOT0_ = comdat any

$_ZNSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEELi1ELb1EE5__getEv = comdat any

$_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE11__recommendEm = comdat any

$_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4sizeEv = comdat any

$_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEEC2EmmS9_ = comdat any

$_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS6_RS8_EE = comdat any

$_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEED2Ev = comdat any

$_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE8max_sizeEv = comdat any

$_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE8capacityEv = comdat any

$_ZNSt3__23maxImEERKT_S3_S3_ = comdat any

$_ZNSt3__23minImEERKT_S3_S3_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE8max_sizeERKS8_ = comdat any

$_ZNKSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE7__allocEv = comdat any

$_ZNSt3__214numeric_limitsIlE3maxEv = comdat any

$_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_ = comdat any

$_ZNKSt3__26__lessImmEclERKmS3_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS8_ = comdat any

$_ZNKSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEE8max_sizeEv = comdat any

$_ZNKSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE6secondEv = comdat any

$_ZNKSt3__222__compressed_pair_elemINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv = comdat any

$_ZNKSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__23maxImNS_6__lessImmEEEERKT_S5_S5_T0_ = comdat any

$_ZNSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEEC2IDnSA_EEOT_OT0_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE8allocateERS8_m = comdat any

$_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE7__allocEv = comdat any

$_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE9__end_capEv = comdat any

$_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEELi0ELb0EEC2IDnvEEOT_ = comdat any

$_ZNSt3__27forwardIRNS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEEEOT_RNS_16remove_referenceISA_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIRNS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEELi1ELb0EEC2IS9_vEEOT_ = comdat any

$_ZNSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEE8allocateEmPKv = comdat any

$_ZNSt3__220__throw_length_errorEPKc = comdat any

$_ZNSt3__217__libcpp_allocateEmm = comdat any

$_ZNSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemIRNS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEELi1ELb0EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE5firstEv = comdat any

$_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE17__annotate_deleteEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE46__construct_backward_with_exception_guaranteesIS7_EENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS8_PT_SE_EE5valuesr31is_trivially_move_constructibleISE_EE5valueEvE4typeERS8_SF_SF_RSF_ = comdat any

$_ZNSt3__24swapIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS9_EE5valueEvE4typeERS9_SC_ = comdat any

$_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE14__annotate_newEm = comdat any

$_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE26__invalidate_all_iteratorsEv = comdat any

$_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE31__annotate_contiguous_containerEPKvSB_SB_SB_ = comdat any

$_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4dataEv = comdat any

$_ZNSt3__24moveIRPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEEEONS_16remove_referenceIT_E4typeEOSA_ = comdat any

$_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE5clearEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE10deallocateERS8_PS7_m = comdat any

$_ZNKSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE8capacityEv = comdat any

$_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE17__destruct_at_endEPS6_ = comdat any

$_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE17__destruct_at_endEPS6_NS_17integral_constantIbLb0EEE = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE7destroyIS7_EEvRS8_PT_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE9__destroyIS7_EEvNS_17integral_constantIbLb1EEERS8_PT_ = comdat any

$_ZNSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEE7destroyEPS6_ = comdat any

$_ZNSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEE10deallocateEPS6_m = comdat any

$_ZNSt3__219__libcpp_deallocateEPvmm = comdat any

$_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm = comdat any

$_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm = comdat any

$_ZNSt3__217_DeallocateCaller9__do_callEPv = comdat any

$_ZNKSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE5firstEv = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE6resizeEm = comdat any

$_ZNSt3__24moveIRNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOS9_ = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEixEm = comdat any

$_ZNSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEEaSEOS5_ = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8__appendEm = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__destruct_at_endEPS6_ = comdat any

$_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE9__end_capEv = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE18__construct_at_endEm = comdat any

$_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE11__recommendEm = comdat any

$_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEEC2EmmS9_ = comdat any

$_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE18__construct_at_endEm = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS6_RS8_EE = comdat any

$_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEED2Ev = comdat any

$_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5firstEv = comdat any

$_ZNSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE21_ConstructTransactionC2ERS9_m = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE9constructIS7_JEEEvRS8_PT_DpOT0_ = comdat any

$_ZNSt3__212__to_addressINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEEPT_S8_ = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE21_ConstructTransactionD2Ev = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE11__constructIS7_JEEEvNS_17integral_constantIbLb1EEERS8_PT_DpOT0_ = comdat any

$_ZNSt3__29allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEE9constructIS6_JEEEvPT_DpOT0_ = comdat any

$_ZNSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEEC2ILb1EvEEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEEC2IS3_NS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__27forwardIPN5draco26AttributesDecoderInterfaceEEEOT_RNS_16remove_referenceIS4_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco26AttributesDecoderInterfaceELi0ELb0EEC2IS3_vEEOT_ = comdat any

$_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco26AttributesDecoderInterfaceEEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEELi1ELb1EE5__getEv = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8max_sizeEv = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE8max_sizeERKS8_ = comdat any

$_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS8_ = comdat any

$_ZNKSt3__29allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEE8max_sizeEv = comdat any

$_ZNKSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE6secondEv = comdat any

$_ZNKSt3__222__compressed_pair_elemINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEELi1ELb1EE5__getEv = comdat any

$_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEEC2IDnSA_EEOT_OT0_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE8allocateERS8_m = comdat any

$_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE7__allocEv = comdat any

$_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE9__end_capEv = comdat any

$_ZNSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEELi0ELb0EEC2IDnvEEOT_ = comdat any

$_ZNSt3__27forwardIRNS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEEEOT_RNS_16remove_referenceISA_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIRNS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEELi1ELb0EEC2IS9_vEEOT_ = comdat any

$_ZNSt3__29allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEE8allocateEmPKv = comdat any

$_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemIRNS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEELi1ELb0EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE5firstEv = comdat any

$_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE21_ConstructTransactionC2EPPS6_m = comdat any

$_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE21_ConstructTransactionD2Ev = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__annotate_deleteEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE46__construct_backward_with_exception_guaranteesIPS7_EEvRS8_T_SD_RSD_ = comdat any

$_ZNSt3__24swapIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS9_EE5valueEvE4typeERS9_SC_ = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE14__annotate_newEm = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE26__invalidate_all_iteratorsEv = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE31__annotate_contiguous_containerEPKvSB_SB_SB_ = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE9constructIS7_JS7_EEEvRS8_PT_DpOT0_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE11__constructIS7_JS7_EEEvNS_17integral_constantIbLb1EEERS8_PT_DpOT0_ = comdat any

$_ZNSt3__27forwardINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEEOT_RNS_16remove_referenceIS7_E4typeE = comdat any

$_ZNSt3__29allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEE9constructIS6_JS6_EEEvPT_DpOT0_ = comdat any

$_ZNSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEEC2EOS5_ = comdat any

$_ZNSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE7releaseEv = comdat any

$_ZNSt3__27forwardINS_14default_deleteIN5draco26AttributesDecoderInterfaceEEEEEOT_RNS_16remove_referenceIS5_E4typeE = comdat any

$_ZNSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE11get_deleterEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEEC2IS3_S5_EEOT_OT0_ = comdat any

$_ZNSt3__217__compressed_pairIPN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco26AttributesDecoderInterfaceELi0ELb0EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco26AttributesDecoderInterfaceEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco26AttributesDecoderInterfaceEEELi1ELb1EEC2IS4_vEEOT_ = comdat any

$_ZNSt3__24moveIRPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOSA_ = comdat any

$_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE5clearEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE10deallocateERS8_PS7_m = comdat any

$_ZNKSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE8capacityEv = comdat any

$_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE17__destruct_at_endEPS6_ = comdat any

$_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE17__destruct_at_endEPS6_NS_17integral_constantIbLb0EEE = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE7destroyIS7_EEvRS8_PT_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE9__destroyIS7_EEvNS_17integral_constantIbLb1EEERS8_PT_ = comdat any

$_ZNSt3__29allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEE7destroyEPS6_ = comdat any

$_ZNSt3__29allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEE10deallocateEPS6_m = comdat any

$_ZNKSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE5firstEv = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE27__invalidate_iterators_pastEPS6_ = comdat any

$_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__destruct_at_endEPS6_ = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__annotate_shrinkEm = comdat any

$_ZNSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE5resetEPS2_ = comdat any

$_ZNKSt3__214default_deleteIN5draco26AttributesDecoderInterfaceEEclEPS2_ = comdat any

$_ZN5draco15PointsSequencerC2Ev = comdat any

$_ZN5draco15LinearSequencerD2Ev = comdat any

$_ZN5draco15LinearSequencerD0Ev = comdat any

$_ZN5draco15LinearSequencer34UpdatePointToAttributeIndexMappingEPNS_14PointAttributeE = comdat any

$_ZN5draco15LinearSequencer24GenerateSequenceInternalEv = comdat any

$_ZN5draco15PointsSequencerD2Ev = comdat any

$_ZN5draco15PointsSequencerD0Ev = comdat any

$_ZN5draco15PointsSequencer34UpdatePointToAttributeIndexMappingEPNS_14PointAttributeE = comdat any

$_ZN5draco14PointAttribute18SetIdentityMappingEv = comdat any

$_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEEE5clearEv = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5clearEv = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5clearEv = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_shrinkEm = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE26__invalidate_all_iteratorsEv = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE7destroyIS5_EEvRS6_PT_ = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv = comdat any

$_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEPT_S6_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE9__destroyIS5_EEvNS_17integral_constantIbLb1EEERS6_PT_ = comdat any

$_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE7destroyEPS4_ = comdat any

$_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb1EE5__getEv = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_ = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELi0ELb0EE5__getEv = comdat any

$_ZNK5draco15PointsSequencer13out_point_idsEv = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE6resizeEm = comdat any

$_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEC2Ej = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE2atEm = comdat any

$_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEaSERKS2_ = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE8__appendEm = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_ = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE18__construct_at_endEm = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE11__recommendEm = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEEC2EmmS7_ = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE18__construct_at_endEm = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS4_RS6_EE = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEED2Ev = comdat any

$_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionC2ERS7_m = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE9constructIS5_JEEEvRS6_PT_DpOT0_ = comdat any

$_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEEEPT_S6_ = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionD2Ev = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE11__constructIS5_JEEEvNS_17integral_constantIbLb1EEERS6_PT_DpOT0_ = comdat any

$_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEE9constructIS4_JEEEvPT_DpOT0_ = comdat any

$_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEELi1ELb1EE5__getEv = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE8max_sizeEv = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE8max_sizeERKS6_ = comdat any

$_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS6_ = comdat any

$_ZNKSt3__29allocatorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEE8max_sizeEv = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv = comdat any

$_ZNKSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEELi1ELb1EE5__getEv = comdat any

$_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEEC2IDnS8_EEOT_OT0_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE8allocateERS6_m = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE7__allocEv = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE9__end_capEv = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELi0ELb0EEC2IDnvEEOT_ = comdat any

$_ZNSt3__27forwardIRNS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEEEOT_RNS_16remove_referenceIS8_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEELi1ELb0EEC2IS7_vEEOT_ = comdat any

$_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEE8allocateEmPKv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEELi1ELb0EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE5firstEv = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE21_ConstructTransactionC2EPPS4_m = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE21_ConstructTransactionD2Ev = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_deleteEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE46__construct_backward_with_exception_guaranteesIS5_EENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS6_PT_SC_EE5valuesr31is_trivially_move_constructibleISC_EE5valueEvE4typeERS6_SD_SD_RSD_ = comdat any

$_ZNSt3__24swapIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS7_EE5valueEvE4typeERS7_SA_ = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE14__annotate_newEm = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE26__invalidate_all_iteratorsEv = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_ = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv = comdat any

$_ZNSt3__24moveIRPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEEEONS_16remove_referenceIT_E4typeEOS8_ = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE5clearEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE10deallocateERS6_PS5_m = comdat any

$_ZNKSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE8capacityEv = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE17__destruct_at_endEPS4_ = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE17__destruct_at_endEPS4_NS_17integral_constantIbLb0EEE = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE7destroyIS5_EEvRS6_PT_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE9__destroyIS5_EEvNS_17integral_constantIbLb1EEERS6_PT_ = comdat any

$_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEE7destroyEPS4_ = comdat any

$_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEE10deallocateEPS4_m = comdat any

$_ZNKSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE5firstEv = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE27__invalidate_iterators_pastEPS4_ = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_ = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_shrinkEm = comdat any

$_ZN5draco11MeshDecoderD2Ev = comdat any

$_ZN5draco17PointCloudDecoderD2Ev = comdat any

$_ZNSt3__26vectorIiNS_9allocatorIiEEED2Ev = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEED2Ev = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEE17__annotate_deleteEv = comdat any

$_ZNSt3__213__vector_baseIiNS_9allocatorIiEEED2Ev = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEE31__annotate_contiguous_containerEPKvS5_S5_S5_ = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEE8capacityEv = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEE4sizeEv = comdat any

$_ZNSt3__212__to_addressIiEEPT_S2_ = comdat any

$_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPiNS_9allocatorIiEEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPiLi0ELb0EE5__getEv = comdat any

$_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE5clearEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIiEEE10deallocateERS2_Pim = comdat any

$_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE7__allocEv = comdat any

$_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE17__destruct_at_endEPi = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIiEEE7destroyIiEEvRS2_PT_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIiEEE9__destroyIiEEvNS_17integral_constantIbLb1EEERS2_PT_ = comdat any

$_ZNSt3__29allocatorIiE7destroyEPi = comdat any

$_ZNSt3__29allocatorIiE10deallocateEPim = comdat any

$_ZNSt3__217__compressed_pairIPiNS_9allocatorIiEEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIiEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEED2Ev = comdat any

$_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5clearEv = comdat any

$_ZN5draco13DecoderBuffer4PeekIjEEbPT_ = comdat any

$_ZN5draco13DecoderBuffer4PeekIhEEbPT_ = comdat any

$_ZN5draco13DecoderBuffer4PeekItEEbPT_ = comdat any

$_ZNSt3__217__compressed_pairIPN5draco15PointsSequencerENS_14default_deleteIS2_EEEC2IRS3_NS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__27forwardIRPN5draco15PointsSequencerEEEOT_RNS_16remove_referenceIS5_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco15PointsSequencerELi0ELb0EEC2IRS3_vEEOT_ = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco15PointsSequencerEEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__210unique_ptrIN5draco15PointsSequencerENS_14default_deleteIS2_EEE5resetEPS2_ = comdat any

$_ZNSt3__217__compressed_pairIPN5draco15PointsSequencerENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco15PointsSequencerENS_14default_deleteIS2_EEE6secondEv = comdat any

$_ZNKSt3__214default_deleteIN5draco15PointsSequencerEEclEPS2_ = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco15PointsSequencerELi0ELb0EE5__getEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco15PointsSequencerEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco17AttributesDecoderENS_14default_deleteIS2_EEEC2IRS3_NS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__27forwardIRPN5draco17AttributesDecoderEEEOT_RNS_16remove_referenceIS5_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco17AttributesDecoderELi0ELb0EEC2IRS3_vEEOT_ = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco17AttributesDecoderEEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__210unique_ptrIN5draco17AttributesDecoderENS_14default_deleteIS2_EEE5resetEPS2_ = comdat any

$_ZNSt3__217__compressed_pairIPN5draco17AttributesDecoderENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco17AttributesDecoderENS_14default_deleteIS2_EEE6secondEv = comdat any

$_ZNKSt3__214default_deleteIN5draco17AttributesDecoderEEclEPS2_ = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco17AttributesDecoderELi0ELb0EE5__getEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco17AttributesDecoderEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__210unique_ptrIN5draco17AttributesDecoderENS_14default_deleteIS2_EEE7releaseEv = comdat any

$_ZNSt3__27forwardINS_14default_deleteIN5draco17AttributesDecoderEEEEEOT_RNS_16remove_referenceIS5_E4typeE = comdat any

$_ZNSt3__210unique_ptrIN5draco17AttributesDecoderENS_14default_deleteIS2_EEE11get_deleterEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEEC2IPNS1_17AttributesDecoderENS4_IS8_EEEEOT_OT0_ = comdat any

$_ZNSt3__27forwardIPN5draco17AttributesDecoderEEEOT_RNS_16remove_referenceIS4_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco26AttributesDecoderInterfaceELi0ELb0EEC2IPNS1_17AttributesDecoderEvEEOT_ = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco26AttributesDecoderInterfaceEEELi1ELb1EEC2INS1_INS2_17AttributesDecoderEEEvEEOT_ = comdat any

$_ZNSt3__214default_deleteIN5draco26AttributesDecoderInterfaceEEC2INS1_17AttributesDecoderEEERKNS0_IT_EEPNS_9enable_ifIXsr14is_convertibleIPS6_PS2_EE5valueEvE4typeE = comdat any

$_ZNSt3__213__vector_baseIjNS_9allocatorIjEEEC2Ev = comdat any

$_ZNSt3__26vectorIjNS_9allocatorIjEEE11__vallocateEm = comdat any

$_ZNSt3__26vectorIjNS_9allocatorIjEEE18__construct_at_endEm = comdat any

$_ZNSt3__220__vector_base_commonILb1EEC2Ev = comdat any

$_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEEC2IDnNS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__222__compressed_pair_elemIPjLi0ELb0EEC2IDnvEEOT_ = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__29allocatorIjEC2Ev = comdat any

$_ZNKSt3__26vectorIjNS_9allocatorIjEEE8max_sizeEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIjEEE8allocateERS2_m = comdat any

$_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv = comdat any

$_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE9__end_capEv = comdat any

$_ZNKSt3__26vectorIjNS_9allocatorIjEEE14__annotate_newEm = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIjEEE8max_sizeERKS2_ = comdat any

$_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIjEEE10__max_sizeENS_17integral_constantIbLb1EEERKS2_ = comdat any

$_ZNKSt3__29allocatorIjE8max_sizeEv = comdat any

$_ZNKSt3__217__compressed_pairIPjNS_9allocatorIjEEE6secondEv = comdat any

$_ZNKSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__29allocatorIjE8allocateEmPKv = comdat any

$_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEE5firstEv = comdat any

$_ZNSt3__222__compressed_pair_elemIPjLi0ELb0EE5__getEv = comdat any

$_ZNKSt3__26vectorIjNS_9allocatorIjEEE31__annotate_contiguous_containerEPKvS5_S5_S5_ = comdat any

$_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv = comdat any

$_ZNKSt3__26vectorIjNS_9allocatorIjEEE8capacityEv = comdat any

$_ZNSt3__212__to_addressIjEEPT_S2_ = comdat any

$_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPjNS_9allocatorIjEEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPjLi0ELb0EE5__getEv = comdat any

$_ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionC2ERS3_m = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIjEEE9constructIjJEEEvRS2_PT_DpOT0_ = comdat any

$_ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionD2Ev = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIjEEE11__constructIjJEEEvNS_17integral_constantIbLb1EEERS2_PT_DpOT0_ = comdat any

$_ZNSt3__29allocatorIjE9constructIjJEEEvPT_DpOT0_ = comdat any

$_ZNKSt3__26vectorIjNS_9allocatorIjEEE17__annotate_deleteEv = comdat any

$_ZNSt3__213__vector_baseIjNS_9allocatorIjEEED2Ev = comdat any

$_ZNKSt3__26vectorIjNS_9allocatorIjEEE4sizeEv = comdat any

$_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE5clearEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIjEEE10deallocateERS2_Pjm = comdat any

$_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE17__destruct_at_endEPj = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIjEEE7destroyIjEEvRS2_PT_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIjEEE9__destroyIjEEvNS_17integral_constantIbLb1EEERS2_PT_ = comdat any

$_ZNSt3__29allocatorIjE7destroyEPj = comdat any

$_ZNSt3__29allocatorIjE10deallocateEPjm = comdat any

$_ZTVN5draco15LinearSequencerE = comdat any

$_ZTVN5draco15PointsSequencerE = comdat any

@_ZTVN5draco21MeshSequentialDecoderE = hidden unnamed_addr constant { [15 x i8*] } { [15 x i8*] [i8* null, i8* null, i8* bitcast (%"class.draco::MeshSequentialDecoder"* (%"class.draco::MeshSequentialDecoder"*)* @_ZN5draco21MeshSequentialDecoderD2Ev to i8*), i8* bitcast (void (%"class.draco::MeshSequentialDecoder"*)* @_ZN5draco21MeshSequentialDecoderD0Ev to i8*), i8* bitcast (i32 (%"class.draco::MeshDecoder"*)* @_ZNK5draco11MeshDecoder15GetGeometryTypeEv to i8*), i8* bitcast (i1 (%"class.draco::PointCloudDecoder"*)* @_ZN5draco17PointCloudDecoder17InitializeDecoderEv to i8*), i8* bitcast (i1 (%"class.draco::MeshSequentialDecoder"*, i32)* @_ZN5draco21MeshSequentialDecoder23CreateAttributesDecoderEi to i8*), i8* bitcast (i1 (%"class.draco::MeshDecoder"*)* @_ZN5draco11MeshDecoder18DecodeGeometryDataEv to i8*), i8* bitcast (i1 (%"class.draco::PointCloudDecoder"*)* @_ZN5draco17PointCloudDecoder21DecodePointAttributesEv to i8*), i8* bitcast (i1 (%"class.draco::PointCloudDecoder"*)* @_ZN5draco17PointCloudDecoder19DecodeAllAttributesEv to i8*), i8* bitcast (i1 (%"class.draco::PointCloudDecoder"*)* @_ZN5draco17PointCloudDecoder19OnAttributesDecodedEv to i8*), i8* bitcast (%"class.draco::CornerTable"* (%"class.draco::MeshDecoder"*)* @_ZNK5draco11MeshDecoder14GetCornerTableEv to i8*), i8* bitcast (%"class.draco::MeshAttributeCornerTable"* (%"class.draco::MeshDecoder"*, i32)* @_ZNK5draco11MeshDecoder23GetAttributeCornerTableEi to i8*), i8* bitcast (%"struct.draco::MeshAttributeIndicesEncodingData"* (%"class.draco::MeshDecoder"*, i32)* @_ZNK5draco11MeshDecoder24GetAttributeEncodingDataEi to i8*), i8* bitcast (i1 (%"class.draco::MeshSequentialDecoder"*)* @_ZN5draco21MeshSequentialDecoder18DecodeConnectivityEv to i8*)] }, align 4
@.str = private unnamed_addr constant [68 x i8] c"allocator<T>::allocate(size_t n) 'n' exceeds maximum supported size\00", align 1
@_ZTVN5draco15LinearSequencerE = linkonce_odr hidden unnamed_addr constant { [6 x i8*] } { [6 x i8*] [i8* null, i8* null, i8* bitcast (%"class.draco::LinearSequencer"* (%"class.draco::LinearSequencer"*)* @_ZN5draco15LinearSequencerD2Ev to i8*), i8* bitcast (void (%"class.draco::LinearSequencer"*)* @_ZN5draco15LinearSequencerD0Ev to i8*), i8* bitcast (i1 (%"class.draco::LinearSequencer"*, %"class.draco::PointAttribute"*)* @_ZN5draco15LinearSequencer34UpdatePointToAttributeIndexMappingEPNS_14PointAttributeE to i8*), i8* bitcast (i1 (%"class.draco::LinearSequencer"*)* @_ZN5draco15LinearSequencer24GenerateSequenceInternalEv to i8*)] }, comdat, align 4
@_ZTVN5draco15PointsSequencerE = linkonce_odr hidden unnamed_addr constant { [6 x i8*] } { [6 x i8*] [i8* null, i8* null, i8* bitcast (%"class.draco::PointsSequencer"* (%"class.draco::PointsSequencer"*)* @_ZN5draco15PointsSequencerD2Ev to i8*), i8* bitcast (void (%"class.draco::PointsSequencer"*)* @_ZN5draco15PointsSequencerD0Ev to i8*), i8* bitcast (i1 (%"class.draco::PointsSequencer"*, %"class.draco::PointAttribute"*)* @_ZN5draco15PointsSequencer34UpdatePointToAttributeIndexMappingEPNS_14PointAttributeE to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, comdat, align 4
@_ZTVN5draco17PointCloudDecoderE = external unnamed_addr constant { [11 x i8*] }, align 4

@_ZN5draco21MeshSequentialDecoderC1Ev = hidden unnamed_addr alias %"class.draco::MeshSequentialDecoder"* (%"class.draco::MeshSequentialDecoder"*), %"class.draco::MeshSequentialDecoder"* (%"class.draco::MeshSequentialDecoder"*)* @_ZN5draco21MeshSequentialDecoderC2Ev

; Function Attrs: noinline nounwind optnone
define hidden %"class.draco::MeshSequentialDecoder"* @_ZN5draco21MeshSequentialDecoderC2Ev(%"class.draco::MeshSequentialDecoder"* returned %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %"class.draco::MeshSequentialDecoder"*, align 4
  store %"class.draco::MeshSequentialDecoder"* %this, %"class.draco::MeshSequentialDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::MeshSequentialDecoder"*, %"class.draco::MeshSequentialDecoder"** %this.addr, align 4
  %0 = bitcast %"class.draco::MeshSequentialDecoder"* %this1 to %"class.draco::MeshDecoder"*
  %call = call %"class.draco::MeshDecoder"* @_ZN5draco11MeshDecoderC2Ev(%"class.draco::MeshDecoder"* %0)
  %1 = bitcast %"class.draco::MeshSequentialDecoder"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [15 x i8*] }, { [15 x i8*] }* @_ZTVN5draco21MeshSequentialDecoderE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  ret %"class.draco::MeshSequentialDecoder"* %this1
}

declare %"class.draco::MeshDecoder"* @_ZN5draco11MeshDecoderC2Ev(%"class.draco::MeshDecoder"* returned) unnamed_addr #1

; Function Attrs: noinline nounwind optnone
define hidden zeroext i1 @_ZN5draco21MeshSequentialDecoder18DecodeConnectivityEv(%"class.draco::MeshSequentialDecoder"* %this) unnamed_addr #0 {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::MeshSequentialDecoder"*, align 4
  %num_faces = alloca i32, align 4
  %num_points = alloca i32, align 4
  %faces_64 = alloca i64, align 8
  %points_64 = alloca i64, align 8
  %connectivity_method = alloca i8, align 1
  %i = alloca i32, align 4
  %face = alloca %"struct.std::__2::array", align 4
  %j = alloca i32, align 4
  %val = alloca i8, align 1
  %ref.tmp = alloca i32, align 4
  %i58 = alloca i32, align 4
  %face62 = alloca %"struct.std::__2::array", align 4
  %j64 = alloca i32, align 4
  %val68 = alloca i16, align 2
  %ref.tmp73 = alloca i32, align 4
  %i92 = alloca i32, align 4
  %face96 = alloca %"struct.std::__2::array", align 4
  %j98 = alloca i32, align 4
  %val102 = alloca i32, align 4
  %i117 = alloca i32, align 4
  %face121 = alloca %"struct.std::__2::array", align 4
  %j123 = alloca i32, align 4
  %val127 = alloca i32, align 4
  store %"class.draco::MeshSequentialDecoder"* %this, %"class.draco::MeshSequentialDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::MeshSequentialDecoder"*, %"class.draco::MeshSequentialDecoder"** %this.addr, align 4
  %0 = bitcast %"class.draco::MeshSequentialDecoder"* %this1 to %"class.draco::PointCloudDecoder"*
  %call = call zeroext i16 @_ZNK5draco17PointCloudDecoder17bitstream_versionEv(%"class.draco::PointCloudDecoder"* %0)
  %conv = zext i16 %call to i32
  %cmp = icmp slt i32 %conv, 514
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = bitcast %"class.draco::MeshSequentialDecoder"* %this1 to %"class.draco::PointCloudDecoder"*
  %call2 = call %"class.draco::DecoderBuffer"* @_ZN5draco17PointCloudDecoder6bufferEv(%"class.draco::PointCloudDecoder"* %1)
  %call3 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIjEEbPT_(%"class.draco::DecoderBuffer"* %call2, i32* %num_faces)
  br i1 %call3, label %if.end, label %if.then4

if.then4:                                         ; preds = %if.then
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %if.then
  %2 = bitcast %"class.draco::MeshSequentialDecoder"* %this1 to %"class.draco::PointCloudDecoder"*
  %call5 = call %"class.draco::DecoderBuffer"* @_ZN5draco17PointCloudDecoder6bufferEv(%"class.draco::PointCloudDecoder"* %2)
  %call6 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIjEEbPT_(%"class.draco::DecoderBuffer"* %call5, i32* %num_points)
  br i1 %call6, label %if.end8, label %if.then7

if.then7:                                         ; preds = %if.end
  store i1 false, i1* %retval, align 1
  br label %return

if.end8:                                          ; preds = %if.end
  br label %if.end17

if.else:                                          ; preds = %entry
  %3 = bitcast %"class.draco::MeshSequentialDecoder"* %this1 to %"class.draco::PointCloudDecoder"*
  %call9 = call %"class.draco::DecoderBuffer"* @_ZN5draco17PointCloudDecoder6bufferEv(%"class.draco::PointCloudDecoder"* %3)
  %call10 = call zeroext i1 @_ZN5draco12DecodeVarintIjEEbPT_PNS_13DecoderBufferE(i32* %num_faces, %"class.draco::DecoderBuffer"* %call9)
  br i1 %call10, label %if.end12, label %if.then11

if.then11:                                        ; preds = %if.else
  store i1 false, i1* %retval, align 1
  br label %return

if.end12:                                         ; preds = %if.else
  %4 = bitcast %"class.draco::MeshSequentialDecoder"* %this1 to %"class.draco::PointCloudDecoder"*
  %call13 = call %"class.draco::DecoderBuffer"* @_ZN5draco17PointCloudDecoder6bufferEv(%"class.draco::PointCloudDecoder"* %4)
  %call14 = call zeroext i1 @_ZN5draco12DecodeVarintIjEEbPT_PNS_13DecoderBufferE(i32* %num_points, %"class.draco::DecoderBuffer"* %call13)
  br i1 %call14, label %if.end16, label %if.then15

if.then15:                                        ; preds = %if.end12
  store i1 false, i1* %retval, align 1
  br label %return

if.end16:                                         ; preds = %if.end12
  br label %if.end17

if.end17:                                         ; preds = %if.end16, %if.end8
  %5 = load i32, i32* %num_faces, align 4
  %conv18 = zext i32 %5 to i64
  store i64 %conv18, i64* %faces_64, align 8
  %6 = load i32, i32* %num_points, align 4
  %conv19 = zext i32 %6 to i64
  store i64 %conv19, i64* %points_64, align 8
  %7 = load i64, i64* %faces_64, align 8
  %cmp20 = icmp ugt i64 %7, 1431655765
  br i1 %cmp20, label %if.then21, label %if.end22

if.then21:                                        ; preds = %if.end17
  store i1 false, i1* %retval, align 1
  br label %return

if.end22:                                         ; preds = %if.end17
  %8 = load i64, i64* %points_64, align 8
  %9 = load i64, i64* %faces_64, align 8
  %mul = mul i64 %9, 3
  %cmp23 = icmp ugt i64 %8, %mul
  br i1 %cmp23, label %if.then24, label %if.end25

if.then24:                                        ; preds = %if.end22
  store i1 false, i1* %retval, align 1
  br label %return

if.end25:                                         ; preds = %if.end22
  %10 = bitcast %"class.draco::MeshSequentialDecoder"* %this1 to %"class.draco::PointCloudDecoder"*
  %call26 = call %"class.draco::DecoderBuffer"* @_ZN5draco17PointCloudDecoder6bufferEv(%"class.draco::PointCloudDecoder"* %10)
  %call27 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIhEEbPT_(%"class.draco::DecoderBuffer"* %call26, i8* %connectivity_method)
  br i1 %call27, label %if.end29, label %if.then28

if.then28:                                        ; preds = %if.end25
  store i1 false, i1* %retval, align 1
  br label %return

if.end29:                                         ; preds = %if.end25
  %11 = load i8, i8* %connectivity_method, align 1
  %conv30 = zext i8 %11 to i32
  %cmp31 = icmp eq i32 %conv30, 0
  br i1 %cmp31, label %if.then32, label %if.else36

if.then32:                                        ; preds = %if.end29
  %12 = load i32, i32* %num_faces, align 4
  %call33 = call zeroext i1 @_ZN5draco21MeshSequentialDecoder26DecodeAndDecompressIndicesEj(%"class.draco::MeshSequentialDecoder"* %this1, i32 %12)
  br i1 %call33, label %if.end35, label %if.then34

if.then34:                                        ; preds = %if.then32
  store i1 false, i1* %retval, align 1
  br label %return

if.end35:                                         ; preds = %if.then32
  br label %if.end144

if.else36:                                        ; preds = %if.end29
  %13 = load i32, i32* %num_points, align 4
  %cmp37 = icmp ult i32 %13, 256
  br i1 %cmp37, label %if.then38, label %if.else55

if.then38:                                        ; preds = %if.else36
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc52, %if.then38
  %14 = load i32, i32* %i, align 4
  %15 = load i32, i32* %num_faces, align 4
  %cmp39 = icmp ult i32 %14, %15
  br i1 %cmp39, label %for.body, label %for.end54

for.body:                                         ; preds = %for.cond
  %call40 = call %"struct.std::__2::array"* @_ZNSt3__25arrayIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELm3EEC2Ev(%"struct.std::__2::array"* %face)
  store i32 0, i32* %j, align 4
  br label %for.cond41

for.cond41:                                       ; preds = %for.inc, %for.body
  %16 = load i32, i32* %j, align 4
  %cmp42 = icmp slt i32 %16, 3
  br i1 %cmp42, label %for.body43, label %for.end

for.body43:                                       ; preds = %for.cond41
  %17 = bitcast %"class.draco::MeshSequentialDecoder"* %this1 to %"class.draco::PointCloudDecoder"*
  %call44 = call %"class.draco::DecoderBuffer"* @_ZN5draco17PointCloudDecoder6bufferEv(%"class.draco::PointCloudDecoder"* %17)
  %call45 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIhEEbPT_(%"class.draco::DecoderBuffer"* %call44, i8* %val)
  br i1 %call45, label %if.end47, label %if.then46

if.then46:                                        ; preds = %for.body43
  store i1 false, i1* %retval, align 1
  br label %return

if.end47:                                         ; preds = %for.body43
  %18 = load i8, i8* %val, align 1
  %conv48 = zext i8 %18 to i32
  store i32 %conv48, i32* %ref.tmp, align 4
  %19 = load i32, i32* %j, align 4
  %call49 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.116"* @_ZNSt3__25arrayIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELm3EEixEm(%"struct.std::__2::array"* %face, i32 %19) #8
  %call50 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.116"* @_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEaSERKj(%"class.draco::IndexType.116"* %call49, i32* nonnull align 4 dereferenceable(4) %ref.tmp)
  br label %for.inc

for.inc:                                          ; preds = %if.end47
  %20 = load i32, i32* %j, align 4
  %inc = add nsw i32 %20, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond41

for.end:                                          ; preds = %for.cond41
  %21 = bitcast %"class.draco::MeshSequentialDecoder"* %this1 to %"class.draco::MeshDecoder"*
  %call51 = call %"class.draco::Mesh"* @_ZNK5draco11MeshDecoder4meshEv(%"class.draco::MeshDecoder"* %21)
  call void @_ZN5draco4Mesh7AddFaceERKNSt3__25arrayINS_9IndexTypeIjNS_20PointIndex_tag_type_EEELm3EEE(%"class.draco::Mesh"* %call51, %"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %face)
  br label %for.inc52

for.inc52:                                        ; preds = %for.end
  %22 = load i32, i32* %i, align 4
  %inc53 = add i32 %22, 1
  store i32 %inc53, i32* %i, align 4
  br label %for.cond

for.end54:                                        ; preds = %for.cond
  br label %if.end143

if.else55:                                        ; preds = %if.else36
  %23 = load i32, i32* %num_points, align 4
  %cmp56 = icmp ult i32 %23, 65536
  br i1 %cmp56, label %if.then57, label %if.else84

if.then57:                                        ; preds = %if.else55
  store i32 0, i32* %i58, align 4
  br label %for.cond59

for.cond59:                                       ; preds = %for.inc81, %if.then57
  %24 = load i32, i32* %i58, align 4
  %25 = load i32, i32* %num_faces, align 4
  %cmp60 = icmp ult i32 %24, %25
  br i1 %cmp60, label %for.body61, label %for.end83

for.body61:                                       ; preds = %for.cond59
  %call63 = call %"struct.std::__2::array"* @_ZNSt3__25arrayIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELm3EEC2Ev(%"struct.std::__2::array"* %face62)
  store i32 0, i32* %j64, align 4
  br label %for.cond65

for.cond65:                                       ; preds = %for.inc77, %for.body61
  %26 = load i32, i32* %j64, align 4
  %cmp66 = icmp slt i32 %26, 3
  br i1 %cmp66, label %for.body67, label %for.end79

for.body67:                                       ; preds = %for.cond65
  %27 = bitcast %"class.draco::MeshSequentialDecoder"* %this1 to %"class.draco::PointCloudDecoder"*
  %call69 = call %"class.draco::DecoderBuffer"* @_ZN5draco17PointCloudDecoder6bufferEv(%"class.draco::PointCloudDecoder"* %27)
  %call70 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeItEEbPT_(%"class.draco::DecoderBuffer"* %call69, i16* %val68)
  br i1 %call70, label %if.end72, label %if.then71

if.then71:                                        ; preds = %for.body67
  store i1 false, i1* %retval, align 1
  br label %return

if.end72:                                         ; preds = %for.body67
  %28 = load i16, i16* %val68, align 2
  %conv74 = zext i16 %28 to i32
  store i32 %conv74, i32* %ref.tmp73, align 4
  %29 = load i32, i32* %j64, align 4
  %call75 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.116"* @_ZNSt3__25arrayIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELm3EEixEm(%"struct.std::__2::array"* %face62, i32 %29) #8
  %call76 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.116"* @_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEaSERKj(%"class.draco::IndexType.116"* %call75, i32* nonnull align 4 dereferenceable(4) %ref.tmp73)
  br label %for.inc77

for.inc77:                                        ; preds = %if.end72
  %30 = load i32, i32* %j64, align 4
  %inc78 = add nsw i32 %30, 1
  store i32 %inc78, i32* %j64, align 4
  br label %for.cond65

for.end79:                                        ; preds = %for.cond65
  %31 = bitcast %"class.draco::MeshSequentialDecoder"* %this1 to %"class.draco::MeshDecoder"*
  %call80 = call %"class.draco::Mesh"* @_ZNK5draco11MeshDecoder4meshEv(%"class.draco::MeshDecoder"* %31)
  call void @_ZN5draco4Mesh7AddFaceERKNSt3__25arrayINS_9IndexTypeIjNS_20PointIndex_tag_type_EEELm3EEE(%"class.draco::Mesh"* %call80, %"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %face62)
  br label %for.inc81

for.inc81:                                        ; preds = %for.end79
  %32 = load i32, i32* %i58, align 4
  %inc82 = add i32 %32, 1
  store i32 %inc82, i32* %i58, align 4
  br label %for.cond59

for.end83:                                        ; preds = %for.cond59
  br label %if.end142

if.else84:                                        ; preds = %if.else55
  %33 = bitcast %"class.draco::MeshSequentialDecoder"* %this1 to %"class.draco::MeshDecoder"*
  %call85 = call %"class.draco::Mesh"* @_ZNK5draco11MeshDecoder4meshEv(%"class.draco::MeshDecoder"* %33)
  %34 = bitcast %"class.draco::Mesh"* %call85 to %"class.draco::PointCloud"*
  %call86 = call i32 @_ZNK5draco10PointCloud10num_pointsEv(%"class.draco::PointCloud"* %34)
  %cmp87 = icmp ult i32 %call86, 2097152
  br i1 %cmp87, label %land.lhs.true, label %if.else116

land.lhs.true:                                    ; preds = %if.else84
  %35 = bitcast %"class.draco::MeshSequentialDecoder"* %this1 to %"class.draco::PointCloudDecoder"*
  %call88 = call zeroext i16 @_ZNK5draco17PointCloudDecoder17bitstream_versionEv(%"class.draco::PointCloudDecoder"* %35)
  %conv89 = zext i16 %call88 to i32
  %cmp90 = icmp sge i32 %conv89, 514
  br i1 %cmp90, label %if.then91, label %if.else116

if.then91:                                        ; preds = %land.lhs.true
  store i32 0, i32* %i92, align 4
  br label %for.cond93

for.cond93:                                       ; preds = %for.inc113, %if.then91
  %36 = load i32, i32* %i92, align 4
  %37 = load i32, i32* %num_faces, align 4
  %cmp94 = icmp ult i32 %36, %37
  br i1 %cmp94, label %for.body95, label %for.end115

for.body95:                                       ; preds = %for.cond93
  %call97 = call %"struct.std::__2::array"* @_ZNSt3__25arrayIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELm3EEC2Ev(%"struct.std::__2::array"* %face96)
  store i32 0, i32* %j98, align 4
  br label %for.cond99

for.cond99:                                       ; preds = %for.inc109, %for.body95
  %38 = load i32, i32* %j98, align 4
  %cmp100 = icmp slt i32 %38, 3
  br i1 %cmp100, label %for.body101, label %for.end111

for.body101:                                      ; preds = %for.cond99
  %39 = bitcast %"class.draco::MeshSequentialDecoder"* %this1 to %"class.draco::PointCloudDecoder"*
  %call103 = call %"class.draco::DecoderBuffer"* @_ZN5draco17PointCloudDecoder6bufferEv(%"class.draco::PointCloudDecoder"* %39)
  %call104 = call zeroext i1 @_ZN5draco12DecodeVarintIjEEbPT_PNS_13DecoderBufferE(i32* %val102, %"class.draco::DecoderBuffer"* %call103)
  br i1 %call104, label %if.end106, label %if.then105

if.then105:                                       ; preds = %for.body101
  store i1 false, i1* %retval, align 1
  br label %return

if.end106:                                        ; preds = %for.body101
  %40 = load i32, i32* %j98, align 4
  %call107 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.116"* @_ZNSt3__25arrayIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELm3EEixEm(%"struct.std::__2::array"* %face96, i32 %40) #8
  %call108 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.116"* @_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEaSERKj(%"class.draco::IndexType.116"* %call107, i32* nonnull align 4 dereferenceable(4) %val102)
  br label %for.inc109

for.inc109:                                       ; preds = %if.end106
  %41 = load i32, i32* %j98, align 4
  %inc110 = add nsw i32 %41, 1
  store i32 %inc110, i32* %j98, align 4
  br label %for.cond99

for.end111:                                       ; preds = %for.cond99
  %42 = bitcast %"class.draco::MeshSequentialDecoder"* %this1 to %"class.draco::MeshDecoder"*
  %call112 = call %"class.draco::Mesh"* @_ZNK5draco11MeshDecoder4meshEv(%"class.draco::MeshDecoder"* %42)
  call void @_ZN5draco4Mesh7AddFaceERKNSt3__25arrayINS_9IndexTypeIjNS_20PointIndex_tag_type_EEELm3EEE(%"class.draco::Mesh"* %call112, %"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %face96)
  br label %for.inc113

for.inc113:                                       ; preds = %for.end111
  %43 = load i32, i32* %i92, align 4
  %inc114 = add i32 %43, 1
  store i32 %inc114, i32* %i92, align 4
  br label %for.cond93

for.end115:                                       ; preds = %for.cond93
  br label %if.end141

if.else116:                                       ; preds = %land.lhs.true, %if.else84
  store i32 0, i32* %i117, align 4
  br label %for.cond118

for.cond118:                                      ; preds = %for.inc138, %if.else116
  %44 = load i32, i32* %i117, align 4
  %45 = load i32, i32* %num_faces, align 4
  %cmp119 = icmp ult i32 %44, %45
  br i1 %cmp119, label %for.body120, label %for.end140

for.body120:                                      ; preds = %for.cond118
  %call122 = call %"struct.std::__2::array"* @_ZNSt3__25arrayIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELm3EEC2Ev(%"struct.std::__2::array"* %face121)
  store i32 0, i32* %j123, align 4
  br label %for.cond124

for.cond124:                                      ; preds = %for.inc134, %for.body120
  %46 = load i32, i32* %j123, align 4
  %cmp125 = icmp slt i32 %46, 3
  br i1 %cmp125, label %for.body126, label %for.end136

for.body126:                                      ; preds = %for.cond124
  %47 = bitcast %"class.draco::MeshSequentialDecoder"* %this1 to %"class.draco::PointCloudDecoder"*
  %call128 = call %"class.draco::DecoderBuffer"* @_ZN5draco17PointCloudDecoder6bufferEv(%"class.draco::PointCloudDecoder"* %47)
  %call129 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIjEEbPT_(%"class.draco::DecoderBuffer"* %call128, i32* %val127)
  br i1 %call129, label %if.end131, label %if.then130

if.then130:                                       ; preds = %for.body126
  store i1 false, i1* %retval, align 1
  br label %return

if.end131:                                        ; preds = %for.body126
  %48 = load i32, i32* %j123, align 4
  %call132 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.116"* @_ZNSt3__25arrayIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELm3EEixEm(%"struct.std::__2::array"* %face121, i32 %48) #8
  %call133 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.116"* @_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEaSERKj(%"class.draco::IndexType.116"* %call132, i32* nonnull align 4 dereferenceable(4) %val127)
  br label %for.inc134

for.inc134:                                       ; preds = %if.end131
  %49 = load i32, i32* %j123, align 4
  %inc135 = add nsw i32 %49, 1
  store i32 %inc135, i32* %j123, align 4
  br label %for.cond124

for.end136:                                       ; preds = %for.cond124
  %50 = bitcast %"class.draco::MeshSequentialDecoder"* %this1 to %"class.draco::MeshDecoder"*
  %call137 = call %"class.draco::Mesh"* @_ZNK5draco11MeshDecoder4meshEv(%"class.draco::MeshDecoder"* %50)
  call void @_ZN5draco4Mesh7AddFaceERKNSt3__25arrayINS_9IndexTypeIjNS_20PointIndex_tag_type_EEELm3EEE(%"class.draco::Mesh"* %call137, %"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %face121)
  br label %for.inc138

for.inc138:                                       ; preds = %for.end136
  %51 = load i32, i32* %i117, align 4
  %inc139 = add i32 %51, 1
  store i32 %inc139, i32* %i117, align 4
  br label %for.cond118

for.end140:                                       ; preds = %for.cond118
  br label %if.end141

if.end141:                                        ; preds = %for.end140, %for.end115
  br label %if.end142

if.end142:                                        ; preds = %if.end141, %for.end83
  br label %if.end143

if.end143:                                        ; preds = %if.end142, %for.end54
  br label %if.end144

if.end144:                                        ; preds = %if.end143, %if.end35
  %52 = bitcast %"class.draco::MeshSequentialDecoder"* %this1 to %"class.draco::PointCloudDecoder"*
  %call145 = call %"class.draco::PointCloud"* @_ZN5draco17PointCloudDecoder11point_cloudEv(%"class.draco::PointCloudDecoder"* %52)
  %53 = load i32, i32* %num_points, align 4
  call void @_ZN5draco10PointCloud14set_num_pointsEj(%"class.draco::PointCloud"* %call145, i32 %53)
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end144, %if.then130, %if.then105, %if.then71, %if.then46, %if.then34, %if.then28, %if.then24, %if.then21, %if.then15, %if.then11, %if.then7, %if.then4
  %54 = load i1, i1* %retval, align 1
  ret i1 %54
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i16 @_ZNK5draco17PointCloudDecoder17bitstream_versionEv(%"class.draco::PointCloudDecoder"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointCloudDecoder"*, align 4
  store %"class.draco::PointCloudDecoder"* %this, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::PointCloudDecoder"*, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  %version_major_ = getelementptr inbounds %"class.draco::PointCloudDecoder", %"class.draco::PointCloudDecoder"* %this1, i32 0, i32 5
  %0 = load i8, i8* %version_major_, align 4
  %conv = zext i8 %0 to i16
  %conv2 = zext i16 %conv to i32
  %shl = shl i32 %conv2, 8
  %version_minor_ = getelementptr inbounds %"class.draco::PointCloudDecoder", %"class.draco::PointCloudDecoder"* %this1, i32 0, i32 6
  %1 = load i8, i8* %version_minor_, align 1
  %conv3 = zext i8 %1 to i32
  %or = or i32 %shl, %conv3
  %conv4 = trunc i32 %or to i16
  ret i16 %conv4
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::DecoderBuffer"* @_ZN5draco17PointCloudDecoder6bufferEv(%"class.draco::PointCloudDecoder"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointCloudDecoder"*, align 4
  store %"class.draco::PointCloudDecoder"* %this, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::PointCloudDecoder"*, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  %buffer_ = getelementptr inbounds %"class.draco::PointCloudDecoder", %"class.draco::PointCloudDecoder"* %this1, i32 0, i32 4
  %0 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer_, align 4
  ret %"class.draco::DecoderBuffer"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIjEEbPT_(%"class.draco::DecoderBuffer"* %this, i32* %out_val) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %out_val.addr = alloca i32*, align 4
  store %"class.draco::DecoderBuffer"* %this, %"class.draco::DecoderBuffer"** %this.addr, align 4
  store i32* %out_val, i32** %out_val.addr, align 4
  %this1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %this.addr, align 4
  %0 = load i32*, i32** %out_val.addr, align 4
  %call = call zeroext i1 @_ZN5draco13DecoderBuffer4PeekIjEEbPT_(%"class.draco::DecoderBuffer"* %this1, i32* %0)
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %pos_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %1 = load i64, i64* %pos_, align 8
  %add = add nsw i64 %1, 4
  store i64 %add, i64* %pos_, align 8
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %2 = load i1, i1* %retval, align 1
  ret i1 %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco12DecodeVarintIjEEbPT_PNS_13DecoderBufferE(i32* %out_val, %"class.draco::DecoderBuffer"* %buffer) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %out_val.addr = alloca i32*, align 4
  %buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %in = alloca i8, align 1
  store i32* %out_val, i32** %out_val.addr, align 4
  store %"class.draco::DecoderBuffer"* %buffer, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %0 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIhEEbPT_(%"class.draco::DecoderBuffer"* %0, i8* %in)
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %1 = load i8, i8* %in, align 1
  %conv = zext i8 %1 to i32
  %and = and i32 %conv, 128
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then1, label %if.else

if.then1:                                         ; preds = %if.end
  %2 = load i32*, i32** %out_val.addr, align 4
  %3 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call2 = call zeroext i1 @_ZN5draco12DecodeVarintIjEEbPT_PNS_13DecoderBufferE(i32* %2, %"class.draco::DecoderBuffer"* %3)
  br i1 %call2, label %if.end4, label %if.then3

if.then3:                                         ; preds = %if.then1
  store i1 false, i1* %retval, align 1
  br label %return

if.end4:                                          ; preds = %if.then1
  %4 = load i32*, i32** %out_val.addr, align 4
  %5 = load i32, i32* %4, align 4
  %shl = shl i32 %5, 7
  store i32 %shl, i32* %4, align 4
  %6 = load i8, i8* %in, align 1
  %conv5 = zext i8 %6 to i32
  %and6 = and i32 %conv5, 127
  %7 = load i32*, i32** %out_val.addr, align 4
  %8 = load i32, i32* %7, align 4
  %or = or i32 %8, %and6
  store i32 %or, i32* %7, align 4
  br label %if.end8

if.else:                                          ; preds = %if.end
  %9 = load i8, i8* %in, align 1
  %conv7 = zext i8 %9 to i32
  %10 = load i32*, i32** %out_val.addr, align 4
  store i32 %conv7, i32* %10, align 4
  br label %if.end8

if.end8:                                          ; preds = %if.else, %if.end4
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end8, %if.then3, %if.then
  %11 = load i1, i1* %retval, align 1
  ret i1 %11
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIhEEbPT_(%"class.draco::DecoderBuffer"* %this, i8* %out_val) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %out_val.addr = alloca i8*, align 4
  store %"class.draco::DecoderBuffer"* %this, %"class.draco::DecoderBuffer"** %this.addr, align 4
  store i8* %out_val, i8** %out_val.addr, align 4
  %this1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %this.addr, align 4
  %0 = load i8*, i8** %out_val.addr, align 4
  %call = call zeroext i1 @_ZN5draco13DecoderBuffer4PeekIhEEbPT_(%"class.draco::DecoderBuffer"* %this1, i8* %0)
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %pos_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %1 = load i64, i64* %pos_, align 8
  %add = add nsw i64 %1, 1
  store i64 %add, i64* %pos_, align 8
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %2 = load i1, i1* %retval, align 1
  ret i1 %2
}

; Function Attrs: noinline nounwind optnone
define hidden zeroext i1 @_ZN5draco21MeshSequentialDecoder26DecodeAndDecompressIndicesEj(%"class.draco::MeshSequentialDecoder"* %this, i32 %num_faces) #0 {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::MeshSequentialDecoder"*, align 4
  %num_faces.addr = alloca i32, align 4
  %indices_buffer = alloca %"class.std::__2::vector.151", align 4
  %cleanup.dest.slot = alloca i32, align 4
  %last_index_value = alloca i32, align 4
  %vertex_index = alloca i32, align 4
  %i = alloca i32, align 4
  %face = alloca %"struct.std::__2::array", align 4
  %j = alloca i32, align 4
  %encoded_val = alloca i32, align 4
  %index_diff = alloca i32, align 4
  %index_value = alloca i32, align 4
  %ref.tmp = alloca i32, align 4
  store %"class.draco::MeshSequentialDecoder"* %this, %"class.draco::MeshSequentialDecoder"** %this.addr, align 4
  store i32 %num_faces, i32* %num_faces.addr, align 4
  %this1 = load %"class.draco::MeshSequentialDecoder"*, %"class.draco::MeshSequentialDecoder"** %this.addr, align 4
  %0 = load i32, i32* %num_faces.addr, align 4
  %mul = mul i32 %0, 3
  %call = call %"class.std::__2::vector.151"* @_ZNSt3__26vectorIjNS_9allocatorIjEEEC2Em(%"class.std::__2::vector.151"* %indices_buffer, i32 %mul)
  %1 = load i32, i32* %num_faces.addr, align 4
  %mul2 = mul i32 %1, 3
  %2 = bitcast %"class.draco::MeshSequentialDecoder"* %this1 to %"class.draco::PointCloudDecoder"*
  %call3 = call %"class.draco::DecoderBuffer"* @_ZN5draco17PointCloudDecoder6bufferEv(%"class.draco::PointCloudDecoder"* %2)
  %call4 = call i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEE4dataEv(%"class.std::__2::vector.151"* %indices_buffer) #8
  %call5 = call zeroext i1 @_ZN5draco13DecodeSymbolsEjiPNS_13DecoderBufferEPj(i32 %mul2, i32 1, %"class.draco::DecoderBuffer"* %call3, i32* %call4)
  br i1 %call5, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  store i32 0, i32* %last_index_value, align 4
  store i32 0, i32* %vertex_index, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc17, %if.end
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %num_faces.addr, align 4
  %cmp = icmp ult i32 %3, %4
  br i1 %cmp, label %for.body, label %for.end19

for.body:                                         ; preds = %for.cond
  %call6 = call %"struct.std::__2::array"* @_ZNSt3__25arrayIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELm3EEC2Ev(%"struct.std::__2::array"* %face)
  store i32 0, i32* %j, align 4
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc, %for.body
  %5 = load i32, i32* %j, align 4
  %cmp8 = icmp slt i32 %5, 3
  br i1 %cmp8, label %for.body9, label %for.end

for.body9:                                        ; preds = %for.cond7
  %6 = load i32, i32* %vertex_index, align 4
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %vertex_index, align 4
  %call10 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector.151"* %indices_buffer, i32 %6) #8
  %7 = load i32, i32* %call10, align 4
  store i32 %7, i32* %encoded_val, align 4
  %8 = load i32, i32* %encoded_val, align 4
  %shr = lshr i32 %8, 1
  store i32 %shr, i32* %index_diff, align 4
  %9 = load i32, i32* %encoded_val, align 4
  %and = and i32 %9, 1
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then11, label %if.end12

if.then11:                                        ; preds = %for.body9
  %10 = load i32, i32* %index_diff, align 4
  %sub = sub nsw i32 0, %10
  store i32 %sub, i32* %index_diff, align 4
  br label %if.end12

if.end12:                                         ; preds = %if.then11, %for.body9
  %11 = load i32, i32* %index_diff, align 4
  %12 = load i32, i32* %last_index_value, align 4
  %add = add nsw i32 %11, %12
  store i32 %add, i32* %index_value, align 4
  %13 = load i32, i32* %index_value, align 4
  store i32 %13, i32* %ref.tmp, align 4
  %14 = load i32, i32* %j, align 4
  %call13 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.116"* @_ZNSt3__25arrayIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELm3EEixEm(%"struct.std::__2::array"* %face, i32 %14) #8
  %call14 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.116"* @_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEaSERKj(%"class.draco::IndexType.116"* %call13, i32* nonnull align 4 dereferenceable(4) %ref.tmp)
  %15 = load i32, i32* %index_value, align 4
  store i32 %15, i32* %last_index_value, align 4
  br label %for.inc

for.inc:                                          ; preds = %if.end12
  %16 = load i32, i32* %j, align 4
  %inc15 = add nsw i32 %16, 1
  store i32 %inc15, i32* %j, align 4
  br label %for.cond7

for.end:                                          ; preds = %for.cond7
  %17 = bitcast %"class.draco::MeshSequentialDecoder"* %this1 to %"class.draco::MeshDecoder"*
  %call16 = call %"class.draco::Mesh"* @_ZNK5draco11MeshDecoder4meshEv(%"class.draco::MeshDecoder"* %17)
  call void @_ZN5draco4Mesh7AddFaceERKNSt3__25arrayINS_9IndexTypeIjNS_20PointIndex_tag_type_EEELm3EEE(%"class.draco::Mesh"* %call16, %"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %face)
  br label %for.inc17

for.inc17:                                        ; preds = %for.end
  %18 = load i32, i32* %i, align 4
  %inc18 = add i32 %18, 1
  store i32 %inc18, i32* %i, align 4
  br label %for.cond

for.end19:                                        ; preds = %for.cond
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end19, %if.then
  %call20 = call %"class.std::__2::vector.151"* @_ZNSt3__26vectorIjNS_9allocatorIjEEED2Ev(%"class.std::__2::vector.151"* %indices_buffer) #8
  %19 = load i1, i1* %retval, align 1
  ret i1 %19
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::array"* @_ZNSt3__25arrayIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELm3EEC2Ev(%"struct.std::__2::array"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"struct.std::__2::array"*, align 4
  %this.addr = alloca %"struct.std::__2::array"*, align 4
  store %"struct.std::__2::array"* %this, %"struct.std::__2::array"** %this.addr, align 4
  %this1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %this.addr, align 4
  store %"struct.std::__2::array"* %this1, %"struct.std::__2::array"** %retval, align 4
  %__elems_ = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %"class.draco::IndexType.116"], [3 x %"class.draco::IndexType.116"]* %__elems_, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %"class.draco::IndexType.116", %"class.draco::IndexType.116"* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %"class.draco::IndexType.116"* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %"class.draco::IndexType.116"* @_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEC2Ev(%"class.draco::IndexType.116"* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %"class.draco::IndexType.116", %"class.draco::IndexType.116"* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %"class.draco::IndexType.116"* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %retval, align 4
  ret %"struct.std::__2::array"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.116"* @_ZNSt3__25arrayIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELm3EEixEm(%"struct.std::__2::array"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::array"*, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::array"* %this, %"struct.std::__2::array"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %this.addr, align 4
  %__elems_ = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %this1, i32 0, i32 0
  %0 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %"class.draco::IndexType.116"], [3 x %"class.draco::IndexType.116"]* %__elems_, i32 0, i32 %0
  ret %"class.draco::IndexType.116"* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.116"* @_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEaSERKj(%"class.draco::IndexType.116"* %this, i32* nonnull align 4 dereferenceable(4) %val) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.116"*, align 4
  %val.addr = alloca i32*, align 4
  store %"class.draco::IndexType.116"* %this, %"class.draco::IndexType.116"** %this.addr, align 4
  store i32* %val, i32** %val.addr, align 4
  %this1 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %this.addr, align 4
  %0 = load i32*, i32** %val.addr, align 4
  %1 = load i32, i32* %0, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.116", %"class.draco::IndexType.116"* %this1, i32 0, i32 0
  store i32 %1, i32* %value_, align 4
  ret %"class.draco::IndexType.116"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::Mesh"* @_ZNK5draco11MeshDecoder4meshEv(%"class.draco::MeshDecoder"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::MeshDecoder"*, align 4
  store %"class.draco::MeshDecoder"* %this, %"class.draco::MeshDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::MeshDecoder"*, %"class.draco::MeshDecoder"** %this.addr, align 4
  %mesh_ = getelementptr inbounds %"class.draco::MeshDecoder", %"class.draco::MeshDecoder"* %this1, i32 0, i32 1
  %0 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %mesh_, align 4
  ret %"class.draco::Mesh"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco4Mesh7AddFaceERKNSt3__25arrayINS_9IndexTypeIjNS_20PointIndex_tag_type_EEELm3EEE(%"class.draco::Mesh"* %this, %"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %face) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::Mesh"*, align 4
  %face.addr = alloca %"struct.std::__2::array"*, align 4
  store %"class.draco::Mesh"* %this, %"class.draco::Mesh"** %this.addr, align 4
  store %"struct.std::__2::array"* %face, %"struct.std::__2::array"** %face.addr, align 4
  %this1 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %this.addr, align 4
  %faces_ = getelementptr inbounds %"class.draco::Mesh", %"class.draco::Mesh"* %this1, i32 0, i32 2
  %0 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %face.addr, align 4
  call void @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEENSt3__25arrayINS1_IjNS_20PointIndex_tag_type_EEELm3EEEE9push_backERKS8_(%"class.draco::IndexTypeVector.113"* %faces_, %"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %0)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco13DecoderBuffer6DecodeItEEbPT_(%"class.draco::DecoderBuffer"* %this, i16* %out_val) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %out_val.addr = alloca i16*, align 4
  store %"class.draco::DecoderBuffer"* %this, %"class.draco::DecoderBuffer"** %this.addr, align 4
  store i16* %out_val, i16** %out_val.addr, align 4
  %this1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %this.addr, align 4
  %0 = load i16*, i16** %out_val.addr, align 4
  %call = call zeroext i1 @_ZN5draco13DecoderBuffer4PeekItEEbPT_(%"class.draco::DecoderBuffer"* %this1, i16* %0)
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %pos_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %1 = load i64, i64* %pos_, align 8
  %add = add nsw i64 %1, 2
  store i64 %add, i64* %pos_, align 8
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %2 = load i1, i1* %retval, align 1
  ret i1 %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco10PointCloud10num_pointsEv(%"class.draco::PointCloud"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointCloud"*, align 4
  store %"class.draco::PointCloud"* %this, %"class.draco::PointCloud"** %this.addr, align 4
  %this1 = load %"class.draco::PointCloud"*, %"class.draco::PointCloud"** %this.addr, align 4
  %num_points_ = getelementptr inbounds %"class.draco::PointCloud", %"class.draco::PointCloud"* %this1, i32 0, i32 4
  %0 = load i32, i32* %num_points_, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::PointCloud"* @_ZN5draco17PointCloudDecoder11point_cloudEv(%"class.draco::PointCloudDecoder"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointCloudDecoder"*, align 4
  store %"class.draco::PointCloudDecoder"* %this, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::PointCloudDecoder"*, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  %point_cloud_ = getelementptr inbounds %"class.draco::PointCloudDecoder", %"class.draco::PointCloudDecoder"* %this1, i32 0, i32 1
  %0 = load %"class.draco::PointCloud"*, %"class.draco::PointCloud"** %point_cloud_, align 4
  ret %"class.draco::PointCloud"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco10PointCloud14set_num_pointsEj(%"class.draco::PointCloud"* %this, i32 %num) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointCloud"*, align 4
  %num.addr = alloca i32, align 4
  store %"class.draco::PointCloud"* %this, %"class.draco::PointCloud"** %this.addr, align 4
  store i32 %num, i32* %num.addr, align 4
  %this1 = load %"class.draco::PointCloud"*, %"class.draco::PointCloud"** %this.addr, align 4
  %0 = load i32, i32* %num.addr, align 4
  %num_points_ = getelementptr inbounds %"class.draco::PointCloud", %"class.draco::PointCloud"* %this1, i32 0, i32 4
  store i32 %0, i32* %num_points_, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden zeroext i1 @_ZN5draco21MeshSequentialDecoder23CreateAttributesDecoderEi(%"class.draco::MeshSequentialDecoder"* %this, i32 %att_decoder_id) unnamed_addr #0 {
entry:
  %this.addr = alloca %"class.draco::MeshSequentialDecoder"*, align 4
  %att_decoder_id.addr = alloca i32, align 4
  %agg.tmp = alloca %"class.std::__2::unique_ptr.96", align 4
  %ref.tmp = alloca %"class.std::__2::unique_ptr.122", align 4
  %agg.tmp2 = alloca %"class.std::__2::unique_ptr.146", align 4
  store %"class.draco::MeshSequentialDecoder"* %this, %"class.draco::MeshSequentialDecoder"** %this.addr, align 4
  store i32 %att_decoder_id, i32* %att_decoder_id.addr, align 4
  %this1 = load %"class.draco::MeshSequentialDecoder"*, %"class.draco::MeshSequentialDecoder"** %this.addr, align 4
  %0 = bitcast %"class.draco::MeshSequentialDecoder"* %this1 to %"class.draco::PointCloudDecoder"*
  %1 = load i32, i32* %att_decoder_id.addr, align 4
  %call = call noalias nonnull i8* @_Znwm(i32 64) #9
  %2 = bitcast i8* %call to %"class.draco::SequentialAttributeDecodersController"*
  %call3 = call noalias nonnull i8* @_Znwm(i32 12) #9
  %3 = bitcast i8* %call3 to %"class.draco::LinearSequencer"*
  %4 = bitcast %"class.draco::MeshSequentialDecoder"* %this1 to %"class.draco::PointCloudDecoder"*
  %call4 = call %"class.draco::PointCloud"* @_ZN5draco17PointCloudDecoder11point_cloudEv(%"class.draco::PointCloudDecoder"* %4)
  %call5 = call i32 @_ZNK5draco10PointCloud10num_pointsEv(%"class.draco::PointCloud"* %call4)
  %call6 = call %"class.draco::LinearSequencer"* @_ZN5draco15LinearSequencerC2Ei(%"class.draco::LinearSequencer"* %3, i32 %call5)
  %5 = bitcast %"class.draco::LinearSequencer"* %3 to %"class.draco::PointsSequencer"*
  %call7 = call %"class.std::__2::unique_ptr.146"* @_ZNSt3__210unique_ptrIN5draco15PointsSequencerENS_14default_deleteIS2_EEEC2ILb1EvEEPS2_(%"class.std::__2::unique_ptr.146"* %agg.tmp2, %"class.draco::PointsSequencer"* %5) #8
  %call8 = call %"class.draco::SequentialAttributeDecodersController"* @_ZN5draco37SequentialAttributeDecodersControllerC1ENSt3__210unique_ptrINS_15PointsSequencerENS1_14default_deleteIS3_EEEE(%"class.draco::SequentialAttributeDecodersController"* %2, %"class.std::__2::unique_ptr.146"* %agg.tmp2)
  %6 = bitcast %"class.draco::SequentialAttributeDecodersController"* %2 to %"class.draco::AttributesDecoder"*
  %call9 = call %"class.std::__2::unique_ptr.122"* @_ZNSt3__210unique_ptrIN5draco17AttributesDecoderENS_14default_deleteIS2_EEEC2ILb1EvEEPS2_(%"class.std::__2::unique_ptr.122"* %ref.tmp, %"class.draco::AttributesDecoder"* %6) #8
  %call10 = call %"class.std::__2::unique_ptr.96"* @_ZNSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEEC2INS1_17AttributesDecoderENS3_IS7_EEvvEEONS0_IT_T0_EE(%"class.std::__2::unique_ptr.96"* %agg.tmp, %"class.std::__2::unique_ptr.122"* nonnull align 4 dereferenceable(4) %ref.tmp) #8
  %call11 = call zeroext i1 @_ZN5draco17PointCloudDecoder20SetAttributesDecoderEiNSt3__210unique_ptrINS_26AttributesDecoderInterfaceENS1_14default_deleteIS3_EEEE(%"class.draco::PointCloudDecoder"* %0, i32 %1, %"class.std::__2::unique_ptr.96"* %agg.tmp)
  %call12 = call %"class.std::__2::unique_ptr.96"* @_ZNSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.96"* %agg.tmp) #8
  %call13 = call %"class.std::__2::unique_ptr.122"* @_ZNSt3__210unique_ptrIN5draco17AttributesDecoderENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.122"* %ref.tmp) #8
  %call14 = call %"class.std::__2::unique_ptr.146"* @_ZNSt3__210unique_ptrIN5draco15PointsSequencerENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.146"* %agg.tmp2) #8
  ret i1 %call11
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco17PointCloudDecoder20SetAttributesDecoderEiNSt3__210unique_ptrINS_26AttributesDecoderInterfaceENS1_14default_deleteIS3_EEEE(%"class.draco::PointCloudDecoder"* %this, i32 %att_decoder_id, %"class.std::__2::unique_ptr.96"* %decoder) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::PointCloudDecoder"*, align 4
  %att_decoder_id.addr = alloca i32, align 4
  store %"class.draco::PointCloudDecoder"* %this, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  store i32 %att_decoder_id, i32* %att_decoder_id.addr, align 4
  %this1 = load %"class.draco::PointCloudDecoder"*, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  %0 = load i32, i32* %att_decoder_id.addr, align 4
  %cmp = icmp slt i32 %0, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %1 = load i32, i32* %att_decoder_id.addr, align 4
  %attributes_decoders_ = getelementptr inbounds %"class.draco::PointCloudDecoder", %"class.draco::PointCloudDecoder"* %this1, i32 0, i32 2
  %call = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.94"* %attributes_decoders_) #8
  %cmp2 = icmp sge i32 %1, %call
  br i1 %cmp2, label %if.then3, label %if.end5

if.then3:                                         ; preds = %if.end
  %attributes_decoders_4 = getelementptr inbounds %"class.draco::PointCloudDecoder", %"class.draco::PointCloudDecoder"* %this1, i32 0, i32 2
  %2 = load i32, i32* %att_decoder_id.addr, align 4
  %add = add nsw i32 %2, 1
  call void @_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE6resizeEm(%"class.std::__2::vector.94"* %attributes_decoders_4, i32 %add)
  br label %if.end5

if.end5:                                          ; preds = %if.then3, %if.end
  %call6 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"* @_ZNSt3__24moveIRNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOS9_(%"class.std::__2::unique_ptr.96"* nonnull align 4 dereferenceable(4) %decoder) #8
  %attributes_decoders_7 = getelementptr inbounds %"class.draco::PointCloudDecoder", %"class.draco::PointCloudDecoder"* %this1, i32 0, i32 2
  %3 = load i32, i32* %att_decoder_id.addr, align 4
  %call8 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEixEm(%"class.std::__2::vector.94"* %attributes_decoders_7, i32 %3) #8
  %call9 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"* @_ZNSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEEaSEOS5_(%"class.std::__2::unique_ptr.96"* %call8, %"class.std::__2::unique_ptr.96"* nonnull align 4 dereferenceable(4) %call6) #8
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end5, %if.then
  %4 = load i1, i1* %retval, align 1
  ret i1 %4
}

; Function Attrs: nobuiltin allocsize(0)
declare nonnull i8* @_Znwm(i32) #2

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::LinearSequencer"* @_ZN5draco15LinearSequencerC2Ei(%"class.draco::LinearSequencer"* returned %this, i32 %num_points) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::LinearSequencer"*, align 4
  %num_points.addr = alloca i32, align 4
  store %"class.draco::LinearSequencer"* %this, %"class.draco::LinearSequencer"** %this.addr, align 4
  store i32 %num_points, i32* %num_points.addr, align 4
  %this1 = load %"class.draco::LinearSequencer"*, %"class.draco::LinearSequencer"** %this.addr, align 4
  %0 = bitcast %"class.draco::LinearSequencer"* %this1 to %"class.draco::PointsSequencer"*
  %call = call %"class.draco::PointsSequencer"* @_ZN5draco15PointsSequencerC2Ev(%"class.draco::PointsSequencer"* %0)
  %1 = bitcast %"class.draco::LinearSequencer"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [6 x i8*] }, { [6 x i8*] }* @_ZTVN5draco15LinearSequencerE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %num_points_ = getelementptr inbounds %"class.draco::LinearSequencer", %"class.draco::LinearSequencer"* %this1, i32 0, i32 1
  %2 = load i32, i32* %num_points.addr, align 4
  store i32 %2, i32* %num_points_, align 4
  ret %"class.draco::LinearSequencer"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.146"* @_ZNSt3__210unique_ptrIN5draco15PointsSequencerENS_14default_deleteIS2_EEEC2ILb1EvEEPS2_(%"class.std::__2::unique_ptr.146"* returned %this, %"class.draco::PointsSequencer"* %__p) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.146"*, align 4
  %__p.addr = alloca %"class.draco::PointsSequencer"*, align 4
  %ref.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::unique_ptr.146"* %this, %"class.std::__2::unique_ptr.146"** %this.addr, align 4
  store %"class.draco::PointsSequencer"* %__p, %"class.draco::PointsSequencer"** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.146"*, %"class.std::__2::unique_ptr.146"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.146", %"class.std::__2::unique_ptr.146"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::__compressed_pair.147"* @_ZNSt3__217__compressed_pairIPN5draco15PointsSequencerENS_14default_deleteIS2_EEEC2IRS3_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.147"* %__ptr_, %"class.draco::PointsSequencer"** nonnull align 4 dereferenceable(4) %__p.addr, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp)
  ret %"class.std::__2::unique_ptr.146"* %this1
}

declare %"class.draco::SequentialAttributeDecodersController"* @_ZN5draco37SequentialAttributeDecodersControllerC1ENSt3__210unique_ptrINS_15PointsSequencerENS1_14default_deleteIS3_EEEE(%"class.draco::SequentialAttributeDecodersController"* returned, %"class.std::__2::unique_ptr.146"*) unnamed_addr #1

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.122"* @_ZNSt3__210unique_ptrIN5draco17AttributesDecoderENS_14default_deleteIS2_EEEC2ILb1EvEEPS2_(%"class.std::__2::unique_ptr.122"* returned %this, %"class.draco::AttributesDecoder"* %__p) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.122"*, align 4
  %__p.addr = alloca %"class.draco::AttributesDecoder"*, align 4
  %ref.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::unique_ptr.122"* %this, %"class.std::__2::unique_ptr.122"** %this.addr, align 4
  store %"class.draco::AttributesDecoder"* %__p, %"class.draco::AttributesDecoder"** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.122"*, %"class.std::__2::unique_ptr.122"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.122", %"class.std::__2::unique_ptr.122"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::__compressed_pair.123"* @_ZNSt3__217__compressed_pairIPN5draco17AttributesDecoderENS_14default_deleteIS2_EEEC2IRS3_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.123"* %__ptr_, %"class.draco::AttributesDecoder"** nonnull align 4 dereferenceable(4) %__p.addr, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp)
  ret %"class.std::__2::unique_ptr.122"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.96"* @_ZNSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEEC2INS1_17AttributesDecoderENS3_IS7_EEvvEEONS0_IT_T0_EE(%"class.std::__2::unique_ptr.96"* returned %this, %"class.std::__2::unique_ptr.122"* nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  %__u.addr = alloca %"class.std::__2::unique_ptr.122"*, align 4
  %ref.tmp = alloca %"class.draco::AttributesDecoder"*, align 4
  store %"class.std::__2::unique_ptr.96"* %this, %"class.std::__2::unique_ptr.96"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.122"* %__u, %"class.std::__2::unique_ptr.122"** %__u.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::unique_ptr.122"*, %"class.std::__2::unique_ptr.122"** %__u.addr, align 4
  %call = call %"class.draco::AttributesDecoder"* @_ZNSt3__210unique_ptrIN5draco17AttributesDecoderENS_14default_deleteIS2_EEE7releaseEv(%"class.std::__2::unique_ptr.122"* %0) #8
  store %"class.draco::AttributesDecoder"* %call, %"class.draco::AttributesDecoder"** %ref.tmp, align 4
  %1 = load %"class.std::__2::unique_ptr.122"*, %"class.std::__2::unique_ptr.122"** %__u.addr, align 4
  %call2 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.126"* @_ZNSt3__210unique_ptrIN5draco17AttributesDecoderENS_14default_deleteIS2_EEE11get_deleterEv(%"class.std::__2::unique_ptr.122"* %1) #8
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.126"* @_ZNSt3__27forwardINS_14default_deleteIN5draco17AttributesDecoderEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.126"* nonnull align 1 dereferenceable(1) %call2) #8
  %call4 = call %"class.std::__2::__compressed_pair.97"* @_ZNSt3__217__compressed_pairIPN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEEC2IPNS1_17AttributesDecoderENS4_IS8_EEEEOT_OT0_(%"class.std::__2::__compressed_pair.97"* %__ptr_, %"class.draco::AttributesDecoder"** nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::default_delete.126"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::unique_ptr.96"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.96"* @_ZNSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.96"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  store %"class.std::__2::unique_ptr.96"* %this, %"class.std::__2::unique_ptr.96"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %this.addr, align 4
  call void @_ZNSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.96"* %this1, %"class.draco::AttributesDecoderInterface"* null) #8
  ret %"class.std::__2::unique_ptr.96"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.122"* @_ZNSt3__210unique_ptrIN5draco17AttributesDecoderENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.122"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.122"*, align 4
  store %"class.std::__2::unique_ptr.122"* %this, %"class.std::__2::unique_ptr.122"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.122"*, %"class.std::__2::unique_ptr.122"** %this.addr, align 4
  call void @_ZNSt3__210unique_ptrIN5draco17AttributesDecoderENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.122"* %this1, %"class.draco::AttributesDecoder"* null) #8
  ret %"class.std::__2::unique_ptr.122"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.146"* @_ZNSt3__210unique_ptrIN5draco15PointsSequencerENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.146"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.146"*, align 4
  store %"class.std::__2::unique_ptr.146"* %this, %"class.std::__2::unique_ptr.146"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.146"*, %"class.std::__2::unique_ptr.146"** %this.addr, align 4
  call void @_ZNSt3__210unique_ptrIN5draco15PointsSequencerENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.146"* %this1, %"class.draco::PointsSequencer"* null) #8
  ret %"class.std::__2::unique_ptr.146"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector.151"* @_ZNSt3__26vectorIjNS_9allocatorIjEEEC2Em(%"class.std::__2::vector.151"* returned %this, i32 %__n) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"class.std::__2::vector.151"*, align 4
  %this.addr = alloca %"class.std::__2::vector.151"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector.151"* %this, %"class.std::__2::vector.151"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.151"*, %"class.std::__2::vector.151"** %this.addr, align 4
  store %"class.std::__2::vector.151"* %this1, %"class.std::__2::vector.151"** %retval, align 4
  %0 = bitcast %"class.std::__2::vector.151"* %this1 to %"class.std::__2::__vector_base.152"*
  %call = call %"class.std::__2::__vector_base.152"* @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEEC2Ev(%"class.std::__2::__vector_base.152"* %0) #8
  %1 = load i32, i32* %__n.addr, align 4
  %cmp = icmp ugt i32 %1, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__26vectorIjNS_9allocatorIjEEE11__vallocateEm(%"class.std::__2::vector.151"* %this1, i32 %2)
  %3 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__26vectorIjNS_9allocatorIjEEE18__construct_at_endEm(%"class.std::__2::vector.151"* %this1, i32 %3)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %4 = load %"class.std::__2::vector.151"*, %"class.std::__2::vector.151"** %retval, align 4
  ret %"class.std::__2::vector.151"* %4
}

declare zeroext i1 @_ZN5draco13DecodeSymbolsEjiPNS_13DecoderBufferEPj(i32, i32, %"class.draco::DecoderBuffer"*, i32*) #1

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEE4dataEv(%"class.std::__2::vector.151"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.151"*, align 4
  store %"class.std::__2::vector.151"* %this, %"class.std::__2::vector.151"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.151"*, %"class.std::__2::vector.151"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.151"* %this1 to %"class.std::__2::__vector_base.152"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.152", %"class.std::__2::__vector_base.152"* %0, i32 0, i32 0
  %1 = load i32*, i32** %__begin_, align 4
  %call = call i32* @_ZNSt3__212__to_addressIjEEPT_S2_(i32* %1) #8
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEEixEm(%"class.std::__2::vector.151"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.151"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector.151"* %this, %"class.std::__2::vector.151"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.151"*, %"class.std::__2::vector.151"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.151"* %this1 to %"class.std::__2::__vector_base.152"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.152", %"class.std::__2::__vector_base.152"* %0, i32 0, i32 0
  %1 = load i32*, i32** %__begin_, align 4
  %2 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds i32, i32* %1, i32 %2
  ret i32* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector.151"* @_ZNSt3__26vectorIjNS_9allocatorIjEEED2Ev(%"class.std::__2::vector.151"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.151"*, align 4
  store %"class.std::__2::vector.151"* %this, %"class.std::__2::vector.151"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.151"*, %"class.std::__2::vector.151"** %this.addr, align 4
  call void @_ZNKSt3__26vectorIjNS_9allocatorIjEEE17__annotate_deleteEv(%"class.std::__2::vector.151"* %this1) #8
  %0 = bitcast %"class.std::__2::vector.151"* %this1 to %"class.std::__2::__vector_base.152"*
  %call = call %"class.std::__2::__vector_base.152"* @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEED2Ev(%"class.std::__2::__vector_base.152"* %0) #8
  ret %"class.std::__2::vector.151"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::MeshSequentialDecoder"* @_ZN5draco21MeshSequentialDecoderD2Ev(%"class.draco::MeshSequentialDecoder"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::MeshSequentialDecoder"*, align 4
  store %"class.draco::MeshSequentialDecoder"* %this, %"class.draco::MeshSequentialDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::MeshSequentialDecoder"*, %"class.draco::MeshSequentialDecoder"** %this.addr, align 4
  %0 = bitcast %"class.draco::MeshSequentialDecoder"* %this1 to %"class.draco::MeshDecoder"*
  %call = call %"class.draco::MeshDecoder"* @_ZN5draco11MeshDecoderD2Ev(%"class.draco::MeshDecoder"* %0) #8
  ret %"class.draco::MeshSequentialDecoder"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco21MeshSequentialDecoderD0Ev(%"class.draco::MeshSequentialDecoder"* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::MeshSequentialDecoder"*, align 4
  store %"class.draco::MeshSequentialDecoder"* %this, %"class.draco::MeshSequentialDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::MeshSequentialDecoder"*, %"class.draco::MeshSequentialDecoder"** %this.addr, align 4
  %call = call %"class.draco::MeshSequentialDecoder"* @_ZN5draco21MeshSequentialDecoderD2Ev(%"class.draco::MeshSequentialDecoder"* %this1) #8
  %0 = bitcast %"class.draco::MeshSequentialDecoder"* %this1 to i8*
  call void @_ZdlPv(i8* %0) #10
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco11MeshDecoder15GetGeometryTypeEv(%"class.draco::MeshDecoder"* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::MeshDecoder"*, align 4
  store %"class.draco::MeshDecoder"* %this, %"class.draco::MeshDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::MeshDecoder"*, %"class.draco::MeshDecoder"** %this.addr, align 4
  ret i32 1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco17PointCloudDecoder17InitializeDecoderEv(%"class.draco::PointCloudDecoder"* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointCloudDecoder"*, align 4
  store %"class.draco::PointCloudDecoder"* %this, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::PointCloudDecoder"*, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  ret i1 true
}

declare zeroext i1 @_ZN5draco11MeshDecoder18DecodeGeometryDataEv(%"class.draco::MeshDecoder"*) unnamed_addr #1

declare zeroext i1 @_ZN5draco17PointCloudDecoder21DecodePointAttributesEv(%"class.draco::PointCloudDecoder"*) unnamed_addr #1

declare zeroext i1 @_ZN5draco17PointCloudDecoder19DecodeAllAttributesEv(%"class.draco::PointCloudDecoder"*) unnamed_addr #1

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco17PointCloudDecoder19OnAttributesDecodedEv(%"class.draco::PointCloudDecoder"* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointCloudDecoder"*, align 4
  store %"class.draco::PointCloudDecoder"* %this, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::PointCloudDecoder"*, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  ret i1 true
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::CornerTable"* @_ZNK5draco11MeshDecoder14GetCornerTableEv(%"class.draco::MeshDecoder"* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::MeshDecoder"*, align 4
  store %"class.draco::MeshDecoder"* %this, %"class.draco::MeshDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::MeshDecoder"*, %"class.draco::MeshDecoder"** %this.addr, align 4
  ret %"class.draco::CornerTable"* null
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::MeshAttributeCornerTable"* @_ZNK5draco11MeshDecoder23GetAttributeCornerTableEi(%"class.draco::MeshDecoder"* %this, i32 %0) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::MeshDecoder"*, align 4
  %.addr = alloca i32, align 4
  store %"class.draco::MeshDecoder"* %this, %"class.draco::MeshDecoder"** %this.addr, align 4
  store i32 %0, i32* %.addr, align 4
  %this1 = load %"class.draco::MeshDecoder"*, %"class.draco::MeshDecoder"** %this.addr, align 4
  ret %"class.draco::MeshAttributeCornerTable"* null
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.draco::MeshAttributeIndicesEncodingData"* @_ZNK5draco11MeshDecoder24GetAttributeEncodingDataEi(%"class.draco::MeshDecoder"* %this, i32 %0) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::MeshDecoder"*, align 4
  %.addr = alloca i32, align 4
  store %"class.draco::MeshDecoder"* %this, %"class.draco::MeshDecoder"** %this.addr, align 4
  store i32 %0, i32* %.addr, align 4
  %this1 = load %"class.draco::MeshDecoder"*, %"class.draco::MeshDecoder"** %this.addr, align 4
  ret %"struct.draco::MeshAttributeIndicesEncodingData"* null
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType.116"* @_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEC2Ev(%"class.draco::IndexType.116"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.116"*, align 4
  store %"class.draco::IndexType.116"* %this, %"class.draco::IndexType.116"** %this.addr, align 4
  %this1 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.116", %"class.draco::IndexType.116"* %this1, i32 0, i32 0
  store i32 0, i32* %value_, align 4
  ret %"class.draco::IndexType.116"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEENSt3__25arrayINS1_IjNS_20PointIndex_tag_type_EEELm3EEEE9push_backERKS8_(%"class.draco::IndexTypeVector.113"* %this, %"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %val) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector.113"*, align 4
  %val.addr = alloca %"struct.std::__2::array"*, align 4
  store %"class.draco::IndexTypeVector.113"* %this, %"class.draco::IndexTypeVector.113"** %this.addr, align 4
  store %"struct.std::__2::array"* %val, %"struct.std::__2::array"** %val.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector.113"*, %"class.draco::IndexTypeVector.113"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector.113", %"class.draco::IndexTypeVector.113"* %this1, i32 0, i32 0
  %0 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %val.addr, align 4
  call void @_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE9push_backERKS6_(%"class.std::__2::vector.114"* %vector_, %"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %0)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE9push_backERKS6_(%"class.std::__2::vector.114"* %this, %"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %__x) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.114"*, align 4
  %__x.addr = alloca %"struct.std::__2::array"*, align 4
  store %"class.std::__2::vector.114"* %this, %"class.std::__2::vector.114"** %this.addr, align 4
  store %"struct.std::__2::array"* %__x, %"struct.std::__2::array"** %__x.addr, align 4
  %this1 = load %"class.std::__2::vector.114"*, %"class.std::__2::vector.114"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.114"* %this1 to %"class.std::__2::__vector_base.115"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.115", %"class.std::__2::__vector_base.115"* %0, i32 0, i32 1
  %1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector.114"* %this1 to %"class.std::__2::__vector_base.115"*
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE9__end_capEv(%"class.std::__2::__vector_base.115"* %2) #8
  %3 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %call, align 4
  %cmp = icmp ne %"struct.std::__2::array"* %1, %3
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %4 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__x.addr, align 4
  call void @_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE22__construct_one_at_endIJRKS6_EEEvDpOT_(%"class.std::__2::vector.114"* %this1, %"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %4)
  br label %if.end

if.else:                                          ; preds = %entry
  %5 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__x.addr, align 4
  call void @_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE21__push_back_slow_pathIRKS6_EEvOT_(%"class.std::__2::vector.114"* %this1, %"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %5)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE9__end_capEv(%"class.std::__2::__vector_base.115"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.115"*, align 4
  store %"class.std::__2::__vector_base.115"* %this, %"class.std::__2::__vector_base.115"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.115"*, %"class.std::__2::__vector_base.115"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.115", %"class.std::__2::__vector_base.115"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.117"* %__end_cap_) #8
  ret %"struct.std::__2::array"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE22__construct_one_at_endIJRKS6_EEEvDpOT_(%"class.std::__2::vector.114"* %this, %"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %__args) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.114"*, align 4
  %__args.addr = alloca %"struct.std::__2::array"*, align 4
  %__tx = alloca %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction", align 4
  store %"class.std::__2::vector.114"* %this, %"class.std::__2::vector.114"** %this.addr, align 4
  store %"struct.std::__2::array"* %__args, %"struct.std::__2::array"** %__args.addr, align 4
  %this1 = load %"class.std::__2::vector.114"*, %"class.std::__2::vector.114"** %this.addr, align 4
  %call = call %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"* @_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE21_ConstructTransactionC2ERS9_m(%"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"* %__tx, %"class.std::__2::vector.114"* nonnull align 4 dereferenceable(12) %this1, i32 1)
  %0 = bitcast %"class.std::__2::vector.114"* %this1 to %"class.std::__2::__vector_base.115"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.120"* @_ZNSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base.115"* %0) #8
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__pos_, align 4
  %call3 = call %"struct.std::__2::array"* @_ZNSt3__212__to_addressINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEEEPT_S8_(%"struct.std::__2::array"* %1) #8
  %2 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__args.addr, align 4
  %call4 = call nonnull align 4 dereferenceable(12) %"struct.std::__2::array"* @_ZNSt3__27forwardIRKNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEEEOT_RNS_16remove_referenceIS9_E4typeE(%"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %2) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE9constructIS7_JRKS7_EEEvRS8_PT_DpOT0_(%"class.std::__2::allocator.120"* nonnull align 1 dereferenceable(1) %call2, %"struct.std::__2::array"* %call3, %"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %call4)
  %__pos_5 = getelementptr inbounds %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %3 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__pos_5, align 4
  %incdec.ptr = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %3, i32 1
  store %"struct.std::__2::array"* %incdec.ptr, %"struct.std::__2::array"** %__pos_5, align 4
  %call6 = call %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"* @_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"* %__tx) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE21__push_back_slow_pathIRKS6_EEvOT_(%"class.std::__2::vector.114"* %this, %"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %__x) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.114"*, align 4
  %__x.addr = alloca %"struct.std::__2::array"*, align 4
  %__a = alloca %"class.std::__2::allocator.120"*, align 4
  %__v = alloca %"struct.std::__2::__split_buffer", align 4
  store %"class.std::__2::vector.114"* %this, %"class.std::__2::vector.114"** %this.addr, align 4
  store %"struct.std::__2::array"* %__x, %"struct.std::__2::array"** %__x.addr, align 4
  %this1 = load %"class.std::__2::vector.114"*, %"class.std::__2::vector.114"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.114"* %this1 to %"class.std::__2::__vector_base.115"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.120"* @_ZNSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base.115"* %0) #8
  store %"class.std::__2::allocator.120"* %call, %"class.std::__2::allocator.120"** %__a, align 4
  %call2 = call i32 @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.114"* %this1) #8
  %add = add i32 %call2, 1
  %call3 = call i32 @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE11__recommendEm(%"class.std::__2::vector.114"* %this1, i32 %add)
  %call4 = call i32 @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.114"* %this1) #8
  %1 = load %"class.std::__2::allocator.120"*, %"class.std::__2::allocator.120"** %__a, align 4
  %call5 = call %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEEC2EmmS9_(%"struct.std::__2::__split_buffer"* %__v, i32 %call3, i32 %call4, %"class.std::__2::allocator.120"* nonnull align 1 dereferenceable(1) %1)
  %2 = load %"class.std::__2::allocator.120"*, %"class.std::__2::allocator.120"** %__a, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %__v, i32 0, i32 2
  %3 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__end_, align 4
  %call6 = call %"struct.std::__2::array"* @_ZNSt3__212__to_addressINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEEEPT_S8_(%"struct.std::__2::array"* %3) #8
  %4 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__x.addr, align 4
  %call7 = call nonnull align 4 dereferenceable(12) %"struct.std::__2::array"* @_ZNSt3__27forwardIRKNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEEEOT_RNS_16remove_referenceIS9_E4typeE(%"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %4) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE9constructIS7_JRKS7_EEEvRS8_PT_DpOT0_(%"class.std::__2::allocator.120"* nonnull align 1 dereferenceable(1) %2, %"struct.std::__2::array"* %call6, %"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %call7)
  %__end_8 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %__v, i32 0, i32 2
  %5 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__end_8, align 4
  %incdec.ptr = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %5, i32 1
  store %"struct.std::__2::array"* %incdec.ptr, %"struct.std::__2::array"** %__end_8, align 4
  call void @_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS6_RS8_EE(%"class.std::__2::vector.114"* %this1, %"struct.std::__2::__split_buffer"* nonnull align 4 dereferenceable(20) %__v)
  %call9 = call %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEED2Ev(%"struct.std::__2::__split_buffer"* %__v) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.117"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.117"*, align 4
  store %"class.std::__2::__compressed_pair.117"* %this, %"class.std::__2::__compressed_pair.117"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.117"*, %"class.std::__2::__compressed_pair.117"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.117"* %this1 to %"struct.std::__2::__compressed_pair_elem.118"*
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNSt3__222__compressed_pair_elemIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.118"* %0) #8
  ret %"struct.std::__2::array"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNSt3__222__compressed_pair_elemIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.118"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.118"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.118"* %this, %"struct.std::__2::__compressed_pair_elem.118"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.118"*, %"struct.std::__2::__compressed_pair_elem.118"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.118", %"struct.std::__2::__compressed_pair_elem.118"* %this1, i32 0, i32 0
  ret %"struct.std::__2::array"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"* @_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE21_ConstructTransactionC2ERS9_m(%"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"* returned %this, %"class.std::__2::vector.114"* nonnull align 4 dereferenceable(12) %__v, i32 %__n) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"*, align 4
  %__v.addr = alloca %"class.std::__2::vector.114"*, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"** %this.addr, align 4
  store %"class.std::__2::vector.114"* %__v, %"class.std::__2::vector.114"** %__v.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"*, %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"** %this.addr, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::vector.114"*, %"class.std::__2::vector.114"** %__v.addr, align 4
  store %"class.std::__2::vector.114"* %0, %"class.std::__2::vector.114"** %__v_, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %1 = load %"class.std::__2::vector.114"*, %"class.std::__2::vector.114"** %__v.addr, align 4
  %2 = bitcast %"class.std::__2::vector.114"* %1 to %"class.std::__2::__vector_base.115"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.115", %"class.std::__2::__vector_base.115"* %2, i32 0, i32 1
  %3 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__end_, align 4
  store %"struct.std::__2::array"* %3, %"struct.std::__2::array"** %__pos_, align 4
  %__new_end_ = getelementptr inbounds %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"* %this1, i32 0, i32 2
  %4 = load %"class.std::__2::vector.114"*, %"class.std::__2::vector.114"** %__v.addr, align 4
  %5 = bitcast %"class.std::__2::vector.114"* %4 to %"class.std::__2::__vector_base.115"*
  %__end_2 = getelementptr inbounds %"class.std::__2::__vector_base.115", %"class.std::__2::__vector_base.115"* %5, i32 0, i32 1
  %6 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__end_2, align 4
  %7 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %6, i32 %7
  store %"struct.std::__2::array"* %add.ptr, %"struct.std::__2::array"** %__new_end_, align 4
  ret %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE9constructIS7_JRKS7_EEEvRS8_PT_DpOT0_(%"class.std::__2::allocator.120"* nonnull align 1 dereferenceable(1) %__a, %"struct.std::__2::array"* %__p, %"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %__args) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.120"*, align 4
  %__p.addr = alloca %"struct.std::__2::array"*, align 4
  %__args.addr = alloca %"struct.std::__2::array"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_construct", align 1
  store %"class.std::__2::allocator.120"* %__a, %"class.std::__2::allocator.120"** %__a.addr, align 4
  store %"struct.std::__2::array"* %__p, %"struct.std::__2::array"** %__p.addr, align 4
  store %"struct.std::__2::array"* %__args, %"struct.std::__2::array"** %__args.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_construct"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.120"*, %"class.std::__2::allocator.120"** %__a.addr, align 4
  %2 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__p.addr, align 4
  %3 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::array"* @_ZNSt3__27forwardIRKNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEEEOT_RNS_16remove_referenceIS9_E4typeE(%"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %3) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE11__constructIS7_JRKS7_EEEvNS_17integral_constantIbLb1EEERS8_PT_DpOT0_(%"class.std::__2::allocator.120"* nonnull align 1 dereferenceable(1) %1, %"struct.std::__2::array"* %2, %"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.120"* @_ZNSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base.115"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.115"*, align 4
  store %"class.std::__2::__vector_base.115"* %this, %"class.std::__2::__vector_base.115"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.115"*, %"class.std::__2::__vector_base.115"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.115", %"class.std::__2::__vector_base.115"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.120"* @_ZNSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE6secondEv(%"class.std::__2::__compressed_pair.117"* %__end_cap_) #8
  ret %"class.std::__2::allocator.120"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::array"* @_ZNSt3__212__to_addressINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEEEPT_S8_(%"struct.std::__2::array"* %__p) #0 comdat {
entry:
  %__p.addr = alloca %"struct.std::__2::array"*, align 4
  store %"struct.std::__2::array"* %__p, %"struct.std::__2::array"** %__p.addr, align 4
  %0 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__p.addr, align 4
  ret %"struct.std::__2::array"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"struct.std::__2::array"* @_ZNSt3__27forwardIRKNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEEEOT_RNS_16remove_referenceIS9_E4typeE(%"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"struct.std::__2::array"*, align 4
  store %"struct.std::__2::array"* %__t, %"struct.std::__2::array"** %__t.addr, align 4
  %0 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__t.addr, align 4
  ret %"struct.std::__2::array"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"* @_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"*, align 4
  store %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"** %this.addr, align 4
  %this1 = load %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"*, %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %0 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__pos_, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::vector.114"*, %"class.std::__2::vector.114"** %__v_, align 4
  %2 = bitcast %"class.std::__2::vector.114"* %1 to %"class.std::__2::__vector_base.115"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.115", %"class.std::__2::__vector_base.115"* %2, i32 0, i32 1
  store %"struct.std::__2::array"* %0, %"struct.std::__2::array"** %__end_, align 4
  ret %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE11__constructIS7_JRKS7_EEEvNS_17integral_constantIbLb1EEERS8_PT_DpOT0_(%"class.std::__2::allocator.120"* nonnull align 1 dereferenceable(1) %__a, %"struct.std::__2::array"* %__p, %"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %__args) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.120"*, align 4
  %__p.addr = alloca %"struct.std::__2::array"*, align 4
  %__args.addr = alloca %"struct.std::__2::array"*, align 4
  store %"class.std::__2::allocator.120"* %__a, %"class.std::__2::allocator.120"** %__a.addr, align 4
  store %"struct.std::__2::array"* %__p, %"struct.std::__2::array"** %__p.addr, align 4
  store %"struct.std::__2::array"* %__args, %"struct.std::__2::array"** %__args.addr, align 4
  %1 = load %"class.std::__2::allocator.120"*, %"class.std::__2::allocator.120"** %__a.addr, align 4
  %2 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__p.addr, align 4
  %3 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::array"* @_ZNSt3__27forwardIRKNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEEEOT_RNS_16remove_referenceIS9_E4typeE(%"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %3) #8
  call void @_ZNSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEE9constructIS6_JRKS6_EEEvPT_DpOT0_(%"class.std::__2::allocator.120"* %1, %"struct.std::__2::array"* %2, %"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEE9constructIS6_JRKS6_EEEvPT_DpOT0_(%"class.std::__2::allocator.120"* %this, %"struct.std::__2::array"* %__p, %"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %__args) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.120"*, align 4
  %__p.addr = alloca %"struct.std::__2::array"*, align 4
  %__args.addr = alloca %"struct.std::__2::array"*, align 4
  store %"class.std::__2::allocator.120"* %this, %"class.std::__2::allocator.120"** %this.addr, align 4
  store %"struct.std::__2::array"* %__p, %"struct.std::__2::array"** %__p.addr, align 4
  store %"struct.std::__2::array"* %__args, %"struct.std::__2::array"** %__args.addr, align 4
  %this1 = load %"class.std::__2::allocator.120"*, %"class.std::__2::allocator.120"** %this.addr, align 4
  %0 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__p.addr, align 4
  %1 = bitcast %"struct.std::__2::array"* %0 to i8*
  %2 = bitcast i8* %1 to %"struct.std::__2::array"*
  %3 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::array"* @_ZNSt3__27forwardIRKNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEEEOT_RNS_16remove_referenceIS9_E4typeE(%"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %3) #8
  %4 = bitcast %"struct.std::__2::array"* %2 to i8*
  %5 = bitcast %"struct.std::__2::array"* %call to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 12, i1 false)
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.120"* @_ZNSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE6secondEv(%"class.std::__2::__compressed_pair.117"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.117"*, align 4
  store %"class.std::__2::__compressed_pair.117"* %this, %"class.std::__2::__compressed_pair.117"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.117"*, %"class.std::__2::__compressed_pair.117"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.117"* %this1 to %"struct.std::__2::__compressed_pair_elem.119"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.120"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.119"* %0) #8
  ret %"class.std::__2::allocator.120"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.120"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.119"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.119"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.119"* %this, %"struct.std::__2::__compressed_pair_elem.119"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.119"*, %"struct.std::__2::__compressed_pair_elem.119"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.119"* %this1 to %"class.std::__2::allocator.120"*
  ret %"class.std::__2::allocator.120"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE11__recommendEm(%"class.std::__2::vector.114"* %this, i32 %__new_size) #0 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %"class.std::__2::vector.114"*, align 4
  %__new_size.addr = alloca i32, align 4
  %__ms = alloca i32, align 4
  %__cap = alloca i32, align 4
  %ref.tmp = alloca i32, align 4
  store %"class.std::__2::vector.114"* %this, %"class.std::__2::vector.114"** %this.addr, align 4
  store i32 %__new_size, i32* %__new_size.addr, align 4
  %this1 = load %"class.std::__2::vector.114"*, %"class.std::__2::vector.114"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE8max_sizeEv(%"class.std::__2::vector.114"* %this1) #8
  store i32 %call, i32* %__ms, align 4
  %0 = load i32, i32* %__new_size.addr, align 4
  %1 = load i32, i32* %__ms, align 4
  %cmp = icmp ugt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = bitcast %"class.std::__2::vector.114"* %this1 to %"class.std::__2::__vector_base_common"*
  call void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"* %2) #11
  unreachable

if.end:                                           ; preds = %entry
  %call2 = call i32 @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector.114"* %this1) #8
  store i32 %call2, i32* %__cap, align 4
  %3 = load i32, i32* %__cap, align 4
  %4 = load i32, i32* %__ms, align 4
  %div = udiv i32 %4, 2
  %cmp3 = icmp uge i32 %3, %div
  br i1 %cmp3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %if.end
  %5 = load i32, i32* %__ms, align 4
  store i32 %5, i32* %retval, align 4
  br label %return

if.end5:                                          ; preds = %if.end
  %6 = load i32, i32* %__cap, align 4
  %mul = mul i32 2, %6
  store i32 %mul, i32* %ref.tmp, align 4
  %call6 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %__new_size.addr)
  %7 = load i32, i32* %call6, align 4
  store i32 %7, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end5, %if.then4
  %8 = load i32, i32* %retval, align 4
  ret i32 %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.114"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.114"*, align 4
  store %"class.std::__2::vector.114"* %this, %"class.std::__2::vector.114"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.114"*, %"class.std::__2::vector.114"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.114"* %this1 to %"class.std::__2::__vector_base.115"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.115", %"class.std::__2::__vector_base.115"* %0, i32 0, i32 1
  %1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector.114"* %this1 to %"class.std::__2::__vector_base.115"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.115", %"class.std::__2::__vector_base.115"* %2, i32 0, i32 0
  %3 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"struct.std::__2::array"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"struct.std::__2::array"* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 12
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEEC2EmmS9_(%"struct.std::__2::__split_buffer"* returned %this, i32 %__cap, i32 %__start, %"class.std::__2::allocator.120"* nonnull align 1 dereferenceable(1) %__a) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"struct.std::__2::__split_buffer"*, align 4
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__cap.addr = alloca i32, align 4
  %__start.addr = alloca i32, align 4
  %__a.addr = alloca %"class.std::__2::allocator.120"*, align 4
  %ref.tmp = alloca i8*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store i32 %__cap, i32* %__cap.addr, align 4
  store i32 %__start, i32* %__start.addr, align 4
  store %"class.std::__2::allocator.120"* %__a, %"class.std::__2::allocator.120"** %__a.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer"* %this1, %"struct.std::__2::__split_buffer"** %retval, align 4
  %0 = bitcast %"struct.std::__2::__split_buffer"* %this1 to %"class.std::__2::__split_buffer_common"*
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  store i8* null, i8** %ref.tmp, align 4
  %1 = load %"class.std::__2::allocator.120"*, %"class.std::__2::allocator.120"** %__a.addr, align 4
  %call = call %"class.std::__2::__compressed_pair.193"* @_ZNSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEEC2IDnSA_EEOT_OT0_(%"class.std::__2::__compressed_pair.193"* %__end_cap_, i8** nonnull align 4 dereferenceable(4) %ref.tmp, %"class.std::__2::allocator.120"* nonnull align 1 dereferenceable(1) %1)
  %2 = load i32, i32* %__cap.addr, align 4
  %cmp = icmp ne i32 %2, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.120"* @_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE7__allocEv(%"struct.std::__2::__split_buffer"* %this1) #8
  %3 = load i32, i32* %__cap.addr, align 4
  %call3 = call %"struct.std::__2::array"* @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE8allocateERS8_m(%"class.std::__2::allocator.120"* nonnull align 1 dereferenceable(1) %call2, i32 %3)
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %"struct.std::__2::array"* [ %call3, %cond.true ], [ null, %cond.false ]
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  store %"struct.std::__2::array"* %cond, %"struct.std::__2::array"** %__first_, align 4
  %__first_4 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %4 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__first_4, align 4
  %5 = load i32, i32* %__start.addr, align 4
  %add.ptr = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %4, i32 %5
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 2
  store %"struct.std::__2::array"* %add.ptr, %"struct.std::__2::array"** %__end_, align 4
  %__begin_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 1
  store %"struct.std::__2::array"* %add.ptr, %"struct.std::__2::array"** %__begin_, align 4
  %__first_5 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %6 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__first_5, align 4
  %7 = load i32, i32* %__cap.addr, align 4
  %add.ptr6 = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %6, i32 %7
  %call7 = call nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this1) #8
  store %"struct.std::__2::array"* %add.ptr6, %"struct.std::__2::array"** %call7, align 4
  %8 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %retval, align 4
  ret %"struct.std::__2::__split_buffer"* %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS6_RS8_EE(%"class.std::__2::vector.114"* %this, %"struct.std::__2::__split_buffer"* nonnull align 4 dereferenceable(20) %__v) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.114"*, align 4
  %__v.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"class.std::__2::vector.114"* %this, %"class.std::__2::vector.114"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer"* %__v, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %this1 = load %"class.std::__2::vector.114"*, %"class.std::__2::vector.114"** %this.addr, align 4
  call void @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE17__annotate_deleteEv(%"class.std::__2::vector.114"* %this1) #8
  %0 = bitcast %"class.std::__2::vector.114"* %this1 to %"class.std::__2::__vector_base.115"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.120"* @_ZNSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base.115"* %0) #8
  %1 = bitcast %"class.std::__2::vector.114"* %this1 to %"class.std::__2::__vector_base.115"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.115", %"class.std::__2::__vector_base.115"* %1, i32 0, i32 0
  %2 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__begin_, align 4
  %3 = bitcast %"class.std::__2::vector.114"* %this1 to %"class.std::__2::__vector_base.115"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.115", %"class.std::__2::__vector_base.115"* %3, i32 0, i32 1
  %4 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__end_, align 4
  %5 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__begin_2 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %5, i32 0, i32 1
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE46__construct_backward_with_exception_guaranteesIS7_EENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS8_PT_SE_EE5valuesr31is_trivially_move_constructibleISE_EE5valueEvE4typeERS8_SF_SF_RSF_(%"class.std::__2::allocator.120"* nonnull align 1 dereferenceable(1) %call, %"struct.std::__2::array"* %2, %"struct.std::__2::array"* %4, %"struct.std::__2::array"** nonnull align 4 dereferenceable(4) %__begin_2)
  %6 = bitcast %"class.std::__2::vector.114"* %this1 to %"class.std::__2::__vector_base.115"*
  %__begin_3 = getelementptr inbounds %"class.std::__2::__vector_base.115", %"class.std::__2::__vector_base.115"* %6, i32 0, i32 0
  %7 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__begin_4 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %7, i32 0, i32 1
  call void @_ZNSt3__24swapIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS9_EE5valueEvE4typeERS9_SC_(%"struct.std::__2::array"** nonnull align 4 dereferenceable(4) %__begin_3, %"struct.std::__2::array"** nonnull align 4 dereferenceable(4) %__begin_4) #8
  %8 = bitcast %"class.std::__2::vector.114"* %this1 to %"class.std::__2::__vector_base.115"*
  %__end_5 = getelementptr inbounds %"class.std::__2::__vector_base.115", %"class.std::__2::__vector_base.115"* %8, i32 0, i32 1
  %9 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__end_6 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %9, i32 0, i32 2
  call void @_ZNSt3__24swapIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS9_EE5valueEvE4typeERS9_SC_(%"struct.std::__2::array"** nonnull align 4 dereferenceable(4) %__end_5, %"struct.std::__2::array"** nonnull align 4 dereferenceable(4) %__end_6) #8
  %10 = bitcast %"class.std::__2::vector.114"* %this1 to %"class.std::__2::__vector_base.115"*
  %call7 = call nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE9__end_capEv(%"class.std::__2::__vector_base.115"* %10) #8
  %11 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %call8 = call nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE9__end_capEv(%"struct.std::__2::__split_buffer"* %11) #8
  call void @_ZNSt3__24swapIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS9_EE5valueEvE4typeERS9_SC_(%"struct.std::__2::array"** nonnull align 4 dereferenceable(4) %call7, %"struct.std::__2::array"** nonnull align 4 dereferenceable(4) %call8) #8
  %12 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__begin_9 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %12, i32 0, i32 1
  %13 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__begin_9, align 4
  %14 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %14, i32 0, i32 0
  store %"struct.std::__2::array"* %13, %"struct.std::__2::array"** %__first_, align 4
  %call10 = call i32 @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.114"* %this1) #8
  call void @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE14__annotate_newEm(%"class.std::__2::vector.114"* %this1, i32 %call10) #8
  call void @_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector.114"* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEED2Ev(%"struct.std::__2::__split_buffer"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"struct.std::__2::__split_buffer"*, align 4
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer"* %this1, %"struct.std::__2::__split_buffer"** %retval, align 4
  call void @_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE5clearEv(%"struct.std::__2::__split_buffer"* %this1) #8
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %0 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__first_, align 4
  %tobool = icmp ne %"struct.std::__2::array"* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.120"* @_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE7__allocEv(%"struct.std::__2::__split_buffer"* %this1) #8
  %__first_2 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__first_2, align 4
  %call3 = call i32 @_ZNKSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE8capacityEv(%"struct.std::__2::__split_buffer"* %this1)
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE10deallocateERS8_PS7_m(%"class.std::__2::allocator.120"* nonnull align 1 dereferenceable(1) %call, %"struct.std::__2::array"* %1, i32 %call3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %retval, align 4
  ret %"struct.std::__2::__split_buffer"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE8max_sizeEv(%"class.std::__2::vector.114"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.114"*, align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp3 = alloca i32, align 4
  store %"class.std::__2::vector.114"* %this, %"class.std::__2::vector.114"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.114"*, %"class.std::__2::vector.114"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.114"* %this1 to %"class.std::__2::__vector_base.115"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.120"* @_ZNKSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base.115"* %0) #8
  %call2 = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE8max_sizeERKS8_(%"class.std::__2::allocator.120"* nonnull align 1 dereferenceable(1) %call) #8
  store i32 %call2, i32* %ref.tmp, align 4
  %call4 = call i32 @_ZNSt3__214numeric_limitsIlE3maxEv() #8
  store i32 %call4, i32* %ref.tmp3, align 4
  %call5 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %1 = load i32, i32* %call5, align 4
  ret i32 %1
}

; Function Attrs: noreturn
declare void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"*) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector.114"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.114"*, align 4
  store %"class.std::__2::vector.114"* %this, %"class.std::__2::vector.114"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.114"*, %"class.std::__2::vector.114"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.114"* %this1 to %"class.std::__2::__vector_base.115"*
  %call = call i32 @_ZNKSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::__vector_base.115"* %0) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::__less", align 1
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__a.addr, align 4
  %1 = load i32*, i32** %__b.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::__less", align 1
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__a.addr, align 4
  %1 = load i32*, i32** %__b.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE8max_sizeERKS8_(%"class.std::__2::allocator.120"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.120"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_max_size", align 1
  store %"class.std::__2::allocator.120"* %__a, %"class.std::__2::allocator.120"** %__a.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_max_size"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.120"*, %"class.std::__2::allocator.120"** %__a.addr, align 4
  %call = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS8_(%"class.std::__2::allocator.120"* nonnull align 1 dereferenceable(1) %1) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.120"* @_ZNKSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base.115"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.115"*, align 4
  store %"class.std::__2::__vector_base.115"* %this, %"class.std::__2::__vector_base.115"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.115"*, %"class.std::__2::__vector_base.115"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.115", %"class.std::__2::__vector_base.115"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.120"* @_ZNKSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE6secondEv(%"class.std::__2::__compressed_pair.117"* %__end_cap_) #8
  ret %"class.std::__2::allocator.120"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__214numeric_limitsIlE3maxEv() #0 comdat {
entry:
  %call = call i32 @_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv() #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__comp = alloca %"struct.std::__2::__less", align 1
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__b.addr, align 4
  %1 = load i32*, i32** %__a.addr, align 4
  %call = call zeroext i1 @_ZNKSt3__26__lessImmEclERKmS3_(%"struct.std::__2::__less"* %__comp, i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  br i1 %call, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32*, i32** %__b.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %3 = load i32*, i32** %__a.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi i32* [ %2, %cond.true ], [ %3, %cond.false ]
  ret i32* %cond-lvalue
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__26__lessImmEclERKmS3_(%"struct.std::__2::__less"* %this, i32* nonnull align 4 dereferenceable(4) %__x, i32* nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__less"*, align 4
  %__x.addr = alloca i32*, align 4
  %__y.addr = alloca i32*, align 4
  store %"struct.std::__2::__less"* %this, %"struct.std::__2::__less"** %this.addr, align 4
  store i32* %__x, i32** %__x.addr, align 4
  store i32* %__y, i32** %__y.addr, align 4
  %this1 = load %"struct.std::__2::__less"*, %"struct.std::__2::__less"** %this.addr, align 4
  %0 = load i32*, i32** %__x.addr, align 4
  %1 = load i32, i32* %0, align 4
  %2 = load i32*, i32** %__y.addr, align 4
  %3 = load i32, i32* %2, align 4
  %cmp = icmp ult i32 %1, %3
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS8_(%"class.std::__2::allocator.120"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.120"*, align 4
  store %"class.std::__2::allocator.120"* %__a, %"class.std::__2::allocator.120"** %__a.addr, align 4
  %1 = load %"class.std::__2::allocator.120"*, %"class.std::__2::allocator.120"** %__a.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEE8max_sizeEv(%"class.std::__2::allocator.120"* %1) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEE8max_sizeEv(%"class.std::__2::allocator.120"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.120"*, align 4
  store %"class.std::__2::allocator.120"* %this, %"class.std::__2::allocator.120"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.120"*, %"class.std::__2::allocator.120"** %this.addr, align 4
  ret i32 357913941
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.120"* @_ZNKSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE6secondEv(%"class.std::__2::__compressed_pair.117"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.117"*, align 4
  store %"class.std::__2::__compressed_pair.117"* %this, %"class.std::__2::__compressed_pair.117"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.117"*, %"class.std::__2::__compressed_pair.117"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.117"* %this1 to %"struct.std::__2::__compressed_pair_elem.119"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.120"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.119"* %0) #8
  ret %"class.std::__2::allocator.120"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.120"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.119"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.119"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.119"* %this, %"struct.std::__2::__compressed_pair_elem.119"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.119"*, %"struct.std::__2::__compressed_pair_elem.119"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.119"* %this1 to %"class.std::__2::allocator.120"*
  ret %"class.std::__2::allocator.120"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv() #0 comdat {
entry:
  ret i32 2147483647
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::__vector_base.115"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.115"*, align 4
  store %"class.std::__2::__vector_base.115"* %this, %"class.std::__2::__vector_base.115"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.115"*, %"class.std::__2::__vector_base.115"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNKSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE9__end_capEv(%"class.std::__2::__vector_base.115"* %this1) #8
  %0 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.115", %"class.std::__2::__vector_base.115"* %this1, i32 0, i32 0
  %1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"struct.std::__2::array"* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint %"struct.std::__2::array"* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 12
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNKSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE9__end_capEv(%"class.std::__2::__vector_base.115"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.115"*, align 4
  store %"class.std::__2::__vector_base.115"* %this, %"class.std::__2::__vector_base.115"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.115"*, %"class.std::__2::__vector_base.115"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.115", %"class.std::__2::__vector_base.115"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNKSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.117"* %__end_cap_) #8
  ret %"struct.std::__2::array"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNKSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.117"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.117"*, align 4
  store %"class.std::__2::__compressed_pair.117"* %this, %"class.std::__2::__compressed_pair.117"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.117"*, %"class.std::__2::__compressed_pair.117"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.117"* %this1 to %"struct.std::__2::__compressed_pair_elem.118"*
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNKSt3__222__compressed_pair_elemIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.118"* %0) #8
  ret %"struct.std::__2::array"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNKSt3__222__compressed_pair_elemIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.118"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.118"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.118"* %this, %"struct.std::__2::__compressed_pair_elem.118"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.118"*, %"struct.std::__2::__compressed_pair_elem.118"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.118", %"struct.std::__2::__compressed_pair_elem.118"* %this1, i32 0, i32 0
  ret %"struct.std::__2::array"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__comp = alloca %"struct.std::__2::__less", align 1
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__a.addr, align 4
  %1 = load i32*, i32** %__b.addr, align 4
  %call = call zeroext i1 @_ZNKSt3__26__lessImmEclERKmS3_(%"struct.std::__2::__less"* %__comp, i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  br i1 %call, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32*, i32** %__b.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %3 = load i32*, i32** %__a.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi i32* [ %2, %cond.true ], [ %3, %cond.false ]
  ret i32* %cond-lvalue
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.193"* @_ZNSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEEC2IDnSA_EEOT_OT0_(%"class.std::__2::__compressed_pair.193"* returned %this, i8** nonnull align 4 dereferenceable(4) %__t1, %"class.std::__2::allocator.120"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.193"*, align 4
  %__t1.addr = alloca i8**, align 4
  %__t2.addr = alloca %"class.std::__2::allocator.120"*, align 4
  store %"class.std::__2::__compressed_pair.193"* %this, %"class.std::__2::__compressed_pair.193"** %this.addr, align 4
  store i8** %__t1, i8*** %__t1.addr, align 4
  store %"class.std::__2::allocator.120"* %__t2, %"class.std::__2::allocator.120"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.193"*, %"class.std::__2::__compressed_pair.193"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.193"* %this1 to %"struct.std::__2::__compressed_pair_elem.118"*
  %1 = load i8**, i8*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %1) #8
  %call2 = call %"struct.std::__2::__compressed_pair_elem.118"* @_ZNSt3__222__compressed_pair_elemIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEELi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.118"* %0, i8** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.193"* %this1 to i8*
  %3 = getelementptr inbounds i8, i8* %2, i32 4
  %4 = bitcast i8* %3 to %"struct.std::__2::__compressed_pair_elem.194"*
  %5 = load %"class.std::__2::allocator.120"*, %"class.std::__2::allocator.120"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.120"* @_ZNSt3__27forwardIRNS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEEEOT_RNS_16remove_referenceISA_E4typeE(%"class.std::__2::allocator.120"* nonnull align 1 dereferenceable(1) %5) #8
  %call4 = call %"struct.std::__2::__compressed_pair_elem.194"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEELi1ELb0EEC2IS9_vEEOT_(%"struct.std::__2::__compressed_pair_elem.194"* %4, %"class.std::__2::allocator.120"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::__compressed_pair.193"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::array"* @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE8allocateERS8_m(%"class.std::__2::allocator.120"* nonnull align 1 dereferenceable(1) %__a, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.120"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.120"* %__a, %"class.std::__2::allocator.120"** %__a.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.120"*, %"class.std::__2::allocator.120"** %__a.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call %"struct.std::__2::array"* @_ZNSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEE8allocateEmPKv(%"class.std::__2::allocator.120"* %0, i32 %1, i8* null)
  ret %"struct.std::__2::array"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.120"* @_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE7__allocEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.120"* @_ZNSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE6secondEv(%"class.std::__2::__compressed_pair.193"* %__end_cap_) #8
  ret %"class.std::__2::allocator.120"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.193"* %__end_cap_) #8
  ret %"struct.std::__2::array"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca i8**, align 4
  store i8** %__t, i8*** %__t.addr, align 4
  %0 = load i8**, i8*** %__t.addr, align 4
  ret i8** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.118"* @_ZNSt3__222__compressed_pair_elemIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEELi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.118"* returned %this, i8** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.118"*, align 4
  %__u.addr = alloca i8**, align 4
  store %"struct.std::__2::__compressed_pair_elem.118"* %this, %"struct.std::__2::__compressed_pair_elem.118"** %this.addr, align 4
  store i8** %__u, i8*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.118"*, %"struct.std::__2::__compressed_pair_elem.118"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.118", %"struct.std::__2::__compressed_pair_elem.118"* %this1, i32 0, i32 0
  %0 = load i8**, i8*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %0) #8
  store %"struct.std::__2::array"* null, %"struct.std::__2::array"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.118"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.120"* @_ZNSt3__27forwardIRNS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEEEOT_RNS_16remove_referenceISA_E4typeE(%"class.std::__2::allocator.120"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::allocator.120"*, align 4
  store %"class.std::__2::allocator.120"* %__t, %"class.std::__2::allocator.120"** %__t.addr, align 4
  %0 = load %"class.std::__2::allocator.120"*, %"class.std::__2::allocator.120"** %__t.addr, align 4
  ret %"class.std::__2::allocator.120"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.194"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEELi1ELb0EEC2IS9_vEEOT_(%"struct.std::__2::__compressed_pair_elem.194"* returned %this, %"class.std::__2::allocator.120"* nonnull align 1 dereferenceable(1) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.194"*, align 4
  %__u.addr = alloca %"class.std::__2::allocator.120"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.194"* %this, %"struct.std::__2::__compressed_pair_elem.194"** %this.addr, align 4
  store %"class.std::__2::allocator.120"* %__u, %"class.std::__2::allocator.120"** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.194"*, %"struct.std::__2::__compressed_pair_elem.194"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.194", %"struct.std::__2::__compressed_pair_elem.194"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::allocator.120"*, %"class.std::__2::allocator.120"** %__u.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.120"* @_ZNSt3__27forwardIRNS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEEEOT_RNS_16remove_referenceISA_E4typeE(%"class.std::__2::allocator.120"* nonnull align 1 dereferenceable(1) %0) #8
  store %"class.std::__2::allocator.120"* %call, %"class.std::__2::allocator.120"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.194"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::array"* @_ZNSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEE8allocateEmPKv(%"class.std::__2::allocator.120"* %this, i32 %__n, i8* %0) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.120"*, align 4
  %__n.addr = alloca i32, align 4
  %.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.120"* %this, %"class.std::__2::allocator.120"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::allocator.120"*, %"class.std::__2::allocator.120"** %this.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEE8max_sizeEv(%"class.std::__2::allocator.120"* %this1) #8
  %cmp = icmp ugt i32 %1, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__220__throw_length_errorEPKc(i8* getelementptr inbounds ([68 x i8], [68 x i8]* @.str, i32 0, i32 0)) #11
  unreachable

if.end:                                           ; preds = %entry
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 12
  %call2 = call i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %mul, i32 4)
  %3 = bitcast i8* %call2 to %"struct.std::__2::array"*
  ret %"struct.std::__2::array"* %3
}

; Function Attrs: noinline noreturn nounwind optnone
define linkonce_odr hidden void @_ZNSt3__220__throw_length_errorEPKc(i8* %__msg) #5 comdat {
entry:
  %__msg.addr = alloca i8*, align 4
  store i8* %__msg, i8** %__msg.addr, align 4
  call void @abort() #11
  unreachable
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %__size, i32 %__align) #0 comdat {
entry:
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i32, i32* %__size.addr, align 4
  %call = call noalias nonnull i8* @_Znwm(i32 %0) #9
  ret i8* %call
}

; Function Attrs: noreturn
declare void @abort() #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.120"* @_ZNSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE6secondEv(%"class.std::__2::__compressed_pair.193"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.193"*, align 4
  store %"class.std::__2::__compressed_pair.193"* %this, %"class.std::__2::__compressed_pair.193"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.193"*, %"class.std::__2::__compressed_pair.193"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.193"* %this1 to i8*
  %add.ptr = getelementptr inbounds i8, i8* %0, i32 4
  %1 = bitcast i8* %add.ptr to %"struct.std::__2::__compressed_pair_elem.194"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.120"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.194"* %1) #8
  ret %"class.std::__2::allocator.120"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.120"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.194"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.194"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.194"* %this, %"struct.std::__2::__compressed_pair_elem.194"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.194"*, %"struct.std::__2::__compressed_pair_elem.194"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.194", %"struct.std::__2::__compressed_pair_elem.194"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::allocator.120"*, %"class.std::__2::allocator.120"** %__value_, align 4
  ret %"class.std::__2::allocator.120"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.193"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.193"*, align 4
  store %"class.std::__2::__compressed_pair.193"* %this, %"class.std::__2::__compressed_pair.193"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.193"*, %"class.std::__2::__compressed_pair.193"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.193"* %this1 to %"struct.std::__2::__compressed_pair_elem.118"*
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNSt3__222__compressed_pair_elemIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.118"* %0) #8
  ret %"struct.std::__2::array"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE17__annotate_deleteEv(%"class.std::__2::vector.114"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.114"*, align 4
  store %"class.std::__2::vector.114"* %this, %"class.std::__2::vector.114"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.114"*, %"class.std::__2::vector.114"** %this.addr, align 4
  %call = call %"struct.std::__2::array"* @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.114"* %this1) #8
  %0 = bitcast %"struct.std::__2::array"* %call to i8*
  %call2 = call %"struct.std::__2::array"* @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.114"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector.114"* %this1) #8
  %add.ptr = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %call2, i32 %call3
  %1 = bitcast %"struct.std::__2::array"* %add.ptr to i8*
  %call4 = call %"struct.std::__2::array"* @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.114"* %this1) #8
  %call5 = call i32 @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.114"* %this1) #8
  %add.ptr6 = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %call4, i32 %call5
  %2 = bitcast %"struct.std::__2::array"* %add.ptr6 to i8*
  %call7 = call %"struct.std::__2::array"* @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.114"* %this1) #8
  %call8 = call i32 @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector.114"* %this1) #8
  %add.ptr9 = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %call7, i32 %call8
  %3 = bitcast %"struct.std::__2::array"* %add.ptr9 to i8*
  call void @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE31__annotate_contiguous_containerEPKvSB_SB_SB_(%"class.std::__2::vector.114"* %this1, i8* %0, i8* %1, i8* %2, i8* %3) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE46__construct_backward_with_exception_guaranteesIS7_EENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS8_PT_SE_EE5valuesr31is_trivially_move_constructibleISE_EE5valueEvE4typeERS8_SF_SF_RSF_(%"class.std::__2::allocator.120"* nonnull align 1 dereferenceable(1) %0, %"struct.std::__2::array"* %__begin1, %"struct.std::__2::array"* %__end1, %"struct.std::__2::array"** nonnull align 4 dereferenceable(4) %__end2) #0 comdat {
entry:
  %.addr = alloca %"class.std::__2::allocator.120"*, align 4
  %__begin1.addr = alloca %"struct.std::__2::array"*, align 4
  %__end1.addr = alloca %"struct.std::__2::array"*, align 4
  %__end2.addr = alloca %"struct.std::__2::array"**, align 4
  %_Np = alloca i32, align 4
  store %"class.std::__2::allocator.120"* %0, %"class.std::__2::allocator.120"** %.addr, align 4
  store %"struct.std::__2::array"* %__begin1, %"struct.std::__2::array"** %__begin1.addr, align 4
  store %"struct.std::__2::array"* %__end1, %"struct.std::__2::array"** %__end1.addr, align 4
  store %"struct.std::__2::array"** %__end2, %"struct.std::__2::array"*** %__end2.addr, align 4
  %1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__end1.addr, align 4
  %2 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__begin1.addr, align 4
  %sub.ptr.lhs.cast = ptrtoint %"struct.std::__2::array"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"struct.std::__2::array"* %2 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 12
  store i32 %sub.ptr.div, i32* %_Np, align 4
  %3 = load i32, i32* %_Np, align 4
  %4 = load %"struct.std::__2::array"**, %"struct.std::__2::array"*** %__end2.addr, align 4
  %5 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %4, align 4
  %idx.neg = sub i32 0, %3
  %add.ptr = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %5, i32 %idx.neg
  store %"struct.std::__2::array"* %add.ptr, %"struct.std::__2::array"** %4, align 4
  %6 = load i32, i32* %_Np, align 4
  %cmp = icmp sgt i32 %6, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %7 = load %"struct.std::__2::array"**, %"struct.std::__2::array"*** %__end2.addr, align 4
  %8 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %7, align 4
  %9 = bitcast %"struct.std::__2::array"* %8 to i8*
  %10 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__begin1.addr, align 4
  %11 = bitcast %"struct.std::__2::array"* %10 to i8*
  %12 = load i32, i32* %_Np, align 4
  %mul = mul i32 %12, 12
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %11, i32 %mul, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__24swapIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS9_EE5valueEvE4typeERS9_SC_(%"struct.std::__2::array"** nonnull align 4 dereferenceable(4) %__x, %"struct.std::__2::array"** nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %__x.addr = alloca %"struct.std::__2::array"**, align 4
  %__y.addr = alloca %"struct.std::__2::array"**, align 4
  %__t = alloca %"struct.std::__2::array"*, align 4
  store %"struct.std::__2::array"** %__x, %"struct.std::__2::array"*** %__x.addr, align 4
  store %"struct.std::__2::array"** %__y, %"struct.std::__2::array"*** %__y.addr, align 4
  %0 = load %"struct.std::__2::array"**, %"struct.std::__2::array"*** %__x.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNSt3__24moveIRPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEEEONS_16remove_referenceIT_E4typeEOSA_(%"struct.std::__2::array"** nonnull align 4 dereferenceable(4) %0) #8
  %1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %call, align 4
  store %"struct.std::__2::array"* %1, %"struct.std::__2::array"** %__t, align 4
  %2 = load %"struct.std::__2::array"**, %"struct.std::__2::array"*** %__y.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNSt3__24moveIRPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEEEONS_16remove_referenceIT_E4typeEOSA_(%"struct.std::__2::array"** nonnull align 4 dereferenceable(4) %2) #8
  %3 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %call1, align 4
  %4 = load %"struct.std::__2::array"**, %"struct.std::__2::array"*** %__x.addr, align 4
  store %"struct.std::__2::array"* %3, %"struct.std::__2::array"** %4, align 4
  %call2 = call nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNSt3__24moveIRPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEEEONS_16remove_referenceIT_E4typeEOSA_(%"struct.std::__2::array"** nonnull align 4 dereferenceable(4) %__t) #8
  %5 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %call2, align 4
  %6 = load %"struct.std::__2::array"**, %"struct.std::__2::array"*** %__y.addr, align 4
  store %"struct.std::__2::array"* %5, %"struct.std::__2::array"** %6, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE14__annotate_newEm(%"class.std::__2::vector.114"* %this, i32 %__current_size) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.114"*, align 4
  %__current_size.addr = alloca i32, align 4
  store %"class.std::__2::vector.114"* %this, %"class.std::__2::vector.114"** %this.addr, align 4
  store i32 %__current_size, i32* %__current_size.addr, align 4
  %this1 = load %"class.std::__2::vector.114"*, %"class.std::__2::vector.114"** %this.addr, align 4
  %call = call %"struct.std::__2::array"* @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.114"* %this1) #8
  %0 = bitcast %"struct.std::__2::array"* %call to i8*
  %call2 = call %"struct.std::__2::array"* @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.114"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector.114"* %this1) #8
  %add.ptr = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %call2, i32 %call3
  %1 = bitcast %"struct.std::__2::array"* %add.ptr to i8*
  %call4 = call %"struct.std::__2::array"* @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.114"* %this1) #8
  %call5 = call i32 @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector.114"* %this1) #8
  %add.ptr6 = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %call4, i32 %call5
  %2 = bitcast %"struct.std::__2::array"* %add.ptr6 to i8*
  %call7 = call %"struct.std::__2::array"* @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.114"* %this1) #8
  %3 = load i32, i32* %__current_size.addr, align 4
  %add.ptr8 = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %call7, i32 %3
  %4 = bitcast %"struct.std::__2::array"* %add.ptr8 to i8*
  call void @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE31__annotate_contiguous_containerEPKvSB_SB_SB_(%"class.std::__2::vector.114"* %this1, i8* %0, i8* %1, i8* %2, i8* %4) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector.114"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.114"*, align 4
  store %"class.std::__2::vector.114"* %this, %"class.std::__2::vector.114"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.114"*, %"class.std::__2::vector.114"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE31__annotate_contiguous_containerEPKvSB_SB_SB_(%"class.std::__2::vector.114"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.114"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector.114"* %this, %"class.std::__2::vector.114"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector.114"*, %"class.std::__2::vector.114"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::array"* @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.114"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.114"*, align 4
  store %"class.std::__2::vector.114"* %this, %"class.std::__2::vector.114"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.114"*, %"class.std::__2::vector.114"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.114"* %this1 to %"class.std::__2::__vector_base.115"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.115", %"class.std::__2::__vector_base.115"* %0, i32 0, i32 0
  %1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__begin_, align 4
  %call = call %"struct.std::__2::array"* @_ZNSt3__212__to_addressINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEEEPT_S8_(%"struct.std::__2::array"* %1) #8
  ret %"struct.std::__2::array"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNSt3__24moveIRPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEEEONS_16remove_referenceIT_E4typeEOSA_(%"struct.std::__2::array"** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"struct.std::__2::array"**, align 4
  store %"struct.std::__2::array"** %__t, %"struct.std::__2::array"*** %__t.addr, align 4
  %0 = load %"struct.std::__2::array"**, %"struct.std::__2::array"*** %__t.addr, align 4
  ret %"struct.std::__2::array"** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE5clearEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 1
  %0 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__begin_, align 4
  call void @_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE17__destruct_at_endEPS6_(%"struct.std::__2::__split_buffer"* %this1, %"struct.std::__2::array"* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE10deallocateERS8_PS7_m(%"class.std::__2::allocator.120"* nonnull align 1 dereferenceable(1) %__a, %"struct.std::__2::array"* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.120"*, align 4
  %__p.addr = alloca %"struct.std::__2::array"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.120"* %__a, %"class.std::__2::allocator.120"** %__a.addr, align 4
  store %"struct.std::__2::array"* %__p, %"struct.std::__2::array"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.120"*, %"class.std::__2::allocator.120"** %__a.addr, align 4
  %1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEE10deallocateEPS6_m(%"class.std::__2::allocator.120"* %0, %"struct.std::__2::array"* %1, i32 %2) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE8capacityEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNKSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this1) #8
  %0 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %call, align 4
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__first_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"struct.std::__2::array"* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint %"struct.std::__2::array"* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 12
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE17__destruct_at_endEPS6_(%"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::array"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__new_last.addr = alloca %"struct.std::__2::array"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant.195", align 1
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store %"struct.std::__2::array"* %__new_last, %"struct.std::__2::array"** %__new_last.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %0 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__new_last.addr, align 4
  call void @_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE17__destruct_at_endEPS6_NS_17integral_constantIbLb0EEE(%"struct.std::__2::__split_buffer"* %this1, %"struct.std::__2::array"* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE17__destruct_at_endEPS6_NS_17integral_constantIbLb0EEE(%"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::array"* %__new_last) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant.195", align 1
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__new_last.addr = alloca %"struct.std::__2::array"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store %"struct.std::__2::array"* %__new_last, %"struct.std::__2::array"** %__new_last.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__new_last.addr, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 2
  %2 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__end_, align 4
  %cmp = icmp ne %"struct.std::__2::array"* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.120"* @_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE7__allocEv(%"struct.std::__2::__split_buffer"* %this1) #8
  %__end_2 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 2
  %3 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__end_2, align 4
  %incdec.ptr = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %3, i32 -1
  store %"struct.std::__2::array"* %incdec.ptr, %"struct.std::__2::array"** %__end_2, align 4
  %call3 = call %"struct.std::__2::array"* @_ZNSt3__212__to_addressINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEEEPT_S8_(%"struct.std::__2::array"* %incdec.ptr) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE7destroyIS7_EEvRS8_PT_(%"class.std::__2::allocator.120"* nonnull align 1 dereferenceable(1) %call, %"struct.std::__2::array"* %call3)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE7destroyIS7_EEvRS8_PT_(%"class.std::__2::allocator.120"* nonnull align 1 dereferenceable(1) %__a, %"struct.std::__2::array"* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.120"*, align 4
  %__p.addr = alloca %"struct.std::__2::array"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy", align 1
  store %"class.std::__2::allocator.120"* %__a, %"class.std::__2::allocator.120"** %__a.addr, align 4
  store %"struct.std::__2::array"* %__p, %"struct.std::__2::array"** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.120"*, %"class.std::__2::allocator.120"** %__a.addr, align 4
  %2 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE9__destroyIS7_EEvNS_17integral_constantIbLb1EEERS8_PT_(%"class.std::__2::allocator.120"* nonnull align 1 dereferenceable(1) %1, %"struct.std::__2::array"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE9__destroyIS7_EEvNS_17integral_constantIbLb1EEERS8_PT_(%"class.std::__2::allocator.120"* nonnull align 1 dereferenceable(1) %__a, %"struct.std::__2::array"* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.120"*, align 4
  %__p.addr = alloca %"struct.std::__2::array"*, align 4
  store %"class.std::__2::allocator.120"* %__a, %"class.std::__2::allocator.120"** %__a.addr, align 4
  store %"struct.std::__2::array"* %__p, %"struct.std::__2::array"** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.120"*, %"class.std::__2::allocator.120"** %__a.addr, align 4
  %2 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__p.addr, align 4
  call void @_ZNSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEE7destroyEPS6_(%"class.std::__2::allocator.120"* %1, %"struct.std::__2::array"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEE7destroyEPS6_(%"class.std::__2::allocator.120"* %this, %"struct.std::__2::array"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.120"*, align 4
  %__p.addr = alloca %"struct.std::__2::array"*, align 4
  store %"class.std::__2::allocator.120"* %this, %"class.std::__2::allocator.120"** %this.addr, align 4
  store %"struct.std::__2::array"* %__p, %"struct.std::__2::array"** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.120"*, %"class.std::__2::allocator.120"** %this.addr, align 4
  %0 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__p.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEE10deallocateEPS6_m(%"class.std::__2::allocator.120"* %this, %"struct.std::__2::array"* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.120"*, align 4
  %__p.addr = alloca %"struct.std::__2::array"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.120"* %this, %"class.std::__2::allocator.120"** %this.addr, align 4
  store %"struct.std::__2::array"* %__p, %"struct.std::__2::array"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.120"*, %"class.std::__2::allocator.120"** %this.addr, align 4
  %0 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__p.addr, align 4
  %1 = bitcast %"struct.std::__2::array"* %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 12
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %__ptr, i32 %__size, i32 %__align) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  %1 = load i32, i32* %__size.addr, align 4
  %2 = load i32, i32* %__align.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm(i8* %0, i32 %1, i32 %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm(i8* %__ptr, i32 %__size, i32 %__align) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  %1 = load i32, i32* %__size.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm(i8* %0, i32 %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm(i8* %__ptr, i32 %__size) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller9__do_callEPv(i8* %0)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller9__do_callEPv(i8* %__ptr) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  call void @_ZdlPv(i8* %0) #10
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #6

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNKSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNKSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.193"* %__end_cap_) #8
  ret %"struct.std::__2::array"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNKSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.193"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.193"*, align 4
  store %"class.std::__2::__compressed_pair.193"* %this, %"class.std::__2::__compressed_pair.193"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.193"*, %"class.std::__2::__compressed_pair.193"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.193"* %this1 to %"struct.std::__2::__compressed_pair_elem.118"*
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNKSt3__222__compressed_pair_elemIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.118"* %0) #8
  ret %"struct.std::__2::array"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.94"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %0, i32 0, i32 1
  %1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %2, i32 0, i32 0
  %3 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.std::__2::unique_ptr.96"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.std::__2::unique_ptr.96"* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE6resizeEm(%"class.std::__2::vector.94"* %this, i32 %__sz) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  %__sz.addr = alloca i32, align 4
  %__cs = alloca i32, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  store i32 %__sz, i32* %__sz.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.94"* %this1) #8
  store i32 %call, i32* %__cs, align 4
  %0 = load i32, i32* %__cs, align 4
  %1 = load i32, i32* %__sz.addr, align 4
  %cmp = icmp ult i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %__sz.addr, align 4
  %3 = load i32, i32* %__cs, align 4
  %sub = sub i32 %2, %3
  call void @_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8__appendEm(%"class.std::__2::vector.94"* %this1, i32 %sub)
  br label %if.end4

if.else:                                          ; preds = %entry
  %4 = load i32, i32* %__cs, align 4
  %5 = load i32, i32* %__sz.addr, align 4
  %cmp2 = icmp ugt i32 %4, %5
  br i1 %cmp2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.else
  %6 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %6, i32 0, i32 0
  %7 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__begin_, align 4
  %8 = load i32, i32* %__sz.addr, align 4
  %add.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %7, i32 %8
  call void @_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__destruct_at_endEPS6_(%"class.std::__2::vector.94"* %this1, %"class.std::__2::unique_ptr.96"* %add.ptr) #8
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.else
  br label %if.end4

if.end4:                                          ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"* @_ZNSt3__24moveIRNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOS9_(%"class.std::__2::unique_ptr.96"* nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  store %"class.std::__2::unique_ptr.96"* %__t, %"class.std::__2::unique_ptr.96"** %__t.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__t.addr, align 4
  ret %"class.std::__2::unique_ptr.96"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEixEm(%"class.std::__2::vector.94"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %0, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__begin_, align 4
  %2 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %1, i32 %2
  ret %"class.std::__2::unique_ptr.96"* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"* @_ZNSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEEaSEOS5_(%"class.std::__2::unique_ptr.96"* %this, %"class.std::__2::unique_ptr.96"* nonnull align 4 dereferenceable(4) %__u) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  %__u.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  store %"class.std::__2::unique_ptr.96"* %this, %"class.std::__2::unique_ptr.96"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.96"* %__u, %"class.std::__2::unique_ptr.96"** %__u.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %this.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__u.addr, align 4
  %call = call %"class.draco::AttributesDecoderInterface"* @_ZNSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE7releaseEv(%"class.std::__2::unique_ptr.96"* %0) #8
  call void @_ZNSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.96"* %this1, %"class.draco::AttributesDecoderInterface"* %call) #8
  %1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__u.addr, align 4
  %call2 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.100"* @_ZNSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE11get_deleterEv(%"class.std::__2::unique_ptr.96"* %1) #8
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.100"* @_ZNSt3__27forwardINS_14default_deleteIN5draco26AttributesDecoderInterfaceEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.100"* nonnull align 1 dereferenceable(1) %call2) #8
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %this1, i32 0, i32 0
  %call4 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.100"* @_ZNSt3__217__compressed_pairIPN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.97"* %__ptr_) #8
  ret %"class.std::__2::unique_ptr.96"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8__appendEm(%"class.std::__2::vector.94"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  %__n.addr = alloca i32, align 4
  %__a = alloca %"class.std::__2::allocator.104"*, align 4
  %__v = alloca %"struct.std::__2::__split_buffer.196", align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"** @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE9__end_capEv(%"class.std::__2::__vector_base.95"* %0) #8
  %1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %call, align 4
  %2 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %2, i32 0, i32 1
  %3 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__end_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.std::__2::unique_ptr.96"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.std::__2::unique_ptr.96"* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  %4 = load i32, i32* %__n.addr, align 4
  %cmp = icmp uge i32 %sub.ptr.div, %4
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %5 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE18__construct_at_endEm(%"class.std::__2::vector.94"* %this1, i32 %5)
  br label %if.end

if.else:                                          ; preds = %entry
  %6 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base.95"* %6) #8
  store %"class.std::__2::allocator.104"* %call2, %"class.std::__2::allocator.104"** %__a, align 4
  %call3 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.94"* %this1) #8
  %7 = load i32, i32* %__n.addr, align 4
  %add = add i32 %call3, %7
  %call4 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE11__recommendEm(%"class.std::__2::vector.94"* %this1, i32 %add)
  %call5 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.94"* %this1) #8
  %8 = load %"class.std::__2::allocator.104"*, %"class.std::__2::allocator.104"** %__a, align 4
  %call6 = call %"struct.std::__2::__split_buffer.196"* @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEEC2EmmS9_(%"struct.std::__2::__split_buffer.196"* %__v, i32 %call4, i32 %call5, %"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %8)
  %9 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE18__construct_at_endEm(%"struct.std::__2::__split_buffer.196"* %__v, i32 %9)
  call void @_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS6_RS8_EE(%"class.std::__2::vector.94"* %this1, %"struct.std::__2::__split_buffer.196"* nonnull align 4 dereferenceable(20) %__v)
  %call7 = call %"struct.std::__2::__split_buffer.196"* @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEED2Ev(%"struct.std::__2::__split_buffer.196"* %__v) #8
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__destruct_at_endEPS6_(%"class.std::__2::vector.94"* %this, %"class.std::__2::unique_ptr.96"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  %__new_last.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  %__old_size = alloca i32, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.96"* %__new_last, %"class.std::__2::unique_ptr.96"** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__new_last.addr, align 4
  call void @_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE27__invalidate_iterators_pastEPS6_(%"class.std::__2::vector.94"* %this1, %"class.std::__2::unique_ptr.96"* %0)
  %call = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.94"* %this1) #8
  store i32 %call, i32* %__old_size, align 4
  %1 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %2 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__new_last.addr, align 4
  call void @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__destruct_at_endEPS6_(%"class.std::__2::__vector_base.95"* %1, %"class.std::__2::unique_ptr.96"* %2) #8
  %3 = load i32, i32* %__old_size, align 4
  call void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__annotate_shrinkEm(%"class.std::__2::vector.94"* %this1, i32 %3) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"** @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE9__end_capEv(%"class.std::__2::__vector_base.95"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.95"*, align 4
  store %"class.std::__2::__vector_base.95"* %this, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.95"*, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"** @_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.101"* %__end_cap_) #8
  ret %"class.std::__2::unique_ptr.96"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE18__construct_at_endEm(%"class.std::__2::vector.94"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  %__n.addr = alloca i32, align 4
  %__tx = alloca %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction", align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %call = call %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE21_ConstructTransactionC2ERS9_m(%"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction"* %__tx, %"class.std::__2::vector.94"* nonnull align 4 dereferenceable(12) %this1, i32 %0)
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__pos_, align 4
  %__new_end_ = getelementptr inbounds %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction"* %__tx, i32 0, i32 2
  %2 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__new_end_, align 4
  %cmp = icmp ne %"class.std::__2::unique_ptr.96"* %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base.95"* %3) #8
  %__pos_3 = getelementptr inbounds %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %4 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__pos_3, align 4
  %call4 = call %"class.std::__2::unique_ptr.96"* @_ZNSt3__212__to_addressINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEEPT_S8_(%"class.std::__2::unique_ptr.96"* %4) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE9constructIS7_JEEEvRS8_PT_DpOT0_(%"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %call2, %"class.std::__2::unique_ptr.96"* %call4)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %__pos_5 = getelementptr inbounds %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %5 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__pos_5, align 4
  %incdec.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %5, i32 1
  store %"class.std::__2::unique_ptr.96"* %incdec.ptr, %"class.std::__2::unique_ptr.96"** %__pos_5, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call6 = call %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction"* %__tx) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base.95"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.95"*, align 4
  store %"class.std::__2::__vector_base.95"* %this, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.95"*, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE6secondEv(%"class.std::__2::__compressed_pair.101"* %__end_cap_) #8
  ret %"class.std::__2::allocator.104"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE11__recommendEm(%"class.std::__2::vector.94"* %this, i32 %__new_size) #0 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  %__new_size.addr = alloca i32, align 4
  %__ms = alloca i32, align 4
  %__cap = alloca i32, align 4
  %ref.tmp = alloca i32, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  store i32 %__new_size, i32* %__new_size.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8max_sizeEv(%"class.std::__2::vector.94"* %this1) #8
  store i32 %call, i32* %__ms, align 4
  %0 = load i32, i32* %__new_size.addr, align 4
  %1 = load i32, i32* %__ms, align 4
  %cmp = icmp ugt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base_common"*
  call void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"* %2) #11
  unreachable

if.end:                                           ; preds = %entry
  %call2 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector.94"* %this1) #8
  store i32 %call2, i32* %__cap, align 4
  %3 = load i32, i32* %__cap, align 4
  %4 = load i32, i32* %__ms, align 4
  %div = udiv i32 %4, 2
  %cmp3 = icmp uge i32 %3, %div
  br i1 %cmp3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %if.end
  %5 = load i32, i32* %__ms, align 4
  store i32 %5, i32* %retval, align 4
  br label %return

if.end5:                                          ; preds = %if.end
  %6 = load i32, i32* %__cap, align 4
  %mul = mul i32 2, %6
  store i32 %mul, i32* %ref.tmp, align 4
  %call6 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %__new_size.addr)
  %7 = load i32, i32* %call6, align 4
  store i32 %7, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end5, %if.then4
  %8 = load i32, i32* %retval, align 4
  ret i32 %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer.196"* @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEEC2EmmS9_(%"struct.std::__2::__split_buffer.196"* returned %this, i32 %__cap, i32 %__start, %"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %__a) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"struct.std::__2::__split_buffer.196"*, align 4
  %this.addr = alloca %"struct.std::__2::__split_buffer.196"*, align 4
  %__cap.addr = alloca i32, align 4
  %__start.addr = alloca i32, align 4
  %__a.addr = alloca %"class.std::__2::allocator.104"*, align 4
  %ref.tmp = alloca i8*, align 4
  store %"struct.std::__2::__split_buffer.196"* %this, %"struct.std::__2::__split_buffer.196"** %this.addr, align 4
  store i32 %__cap, i32* %__cap.addr, align 4
  store i32 %__start, i32* %__start.addr, align 4
  store %"class.std::__2::allocator.104"* %__a, %"class.std::__2::allocator.104"** %__a.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.196"*, %"struct.std::__2::__split_buffer.196"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer.196"* %this1, %"struct.std::__2::__split_buffer.196"** %retval, align 4
  %0 = bitcast %"struct.std::__2::__split_buffer.196"* %this1 to %"class.std::__2::__split_buffer_common"*
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer.196", %"struct.std::__2::__split_buffer.196"* %this1, i32 0, i32 3
  store i8* null, i8** %ref.tmp, align 4
  %1 = load %"class.std::__2::allocator.104"*, %"class.std::__2::allocator.104"** %__a.addr, align 4
  %call = call %"class.std::__2::__compressed_pair.197"* @_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEEC2IDnSA_EEOT_OT0_(%"class.std::__2::__compressed_pair.197"* %__end_cap_, i8** nonnull align 4 dereferenceable(4) %ref.tmp, %"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %1)
  %2 = load i32, i32* %__cap.addr, align 4
  %cmp = icmp ne i32 %2, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE7__allocEv(%"struct.std::__2::__split_buffer.196"* %this1) #8
  %3 = load i32, i32* %__cap.addr, align 4
  %call3 = call %"class.std::__2::unique_ptr.96"* @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE8allocateERS8_m(%"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %call2, i32 %3)
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %"class.std::__2::unique_ptr.96"* [ %call3, %cond.true ], [ null, %cond.false ]
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer.196", %"struct.std::__2::__split_buffer.196"* %this1, i32 0, i32 0
  store %"class.std::__2::unique_ptr.96"* %cond, %"class.std::__2::unique_ptr.96"** %__first_, align 4
  %__first_4 = getelementptr inbounds %"struct.std::__2::__split_buffer.196", %"struct.std::__2::__split_buffer.196"* %this1, i32 0, i32 0
  %4 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__first_4, align 4
  %5 = load i32, i32* %__start.addr, align 4
  %add.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %4, i32 %5
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer.196", %"struct.std::__2::__split_buffer.196"* %this1, i32 0, i32 2
  store %"class.std::__2::unique_ptr.96"* %add.ptr, %"class.std::__2::unique_ptr.96"** %__end_, align 4
  %__begin_ = getelementptr inbounds %"struct.std::__2::__split_buffer.196", %"struct.std::__2::__split_buffer.196"* %this1, i32 0, i32 1
  store %"class.std::__2::unique_ptr.96"* %add.ptr, %"class.std::__2::unique_ptr.96"** %__begin_, align 4
  %__first_5 = getelementptr inbounds %"struct.std::__2::__split_buffer.196", %"struct.std::__2::__split_buffer.196"* %this1, i32 0, i32 0
  %6 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__first_5, align 4
  %7 = load i32, i32* %__cap.addr, align 4
  %add.ptr6 = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %6, i32 %7
  %call7 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"** @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE9__end_capEv(%"struct.std::__2::__split_buffer.196"* %this1) #8
  store %"class.std::__2::unique_ptr.96"* %add.ptr6, %"class.std::__2::unique_ptr.96"** %call7, align 4
  %8 = load %"struct.std::__2::__split_buffer.196"*, %"struct.std::__2::__split_buffer.196"** %retval, align 4
  ret %"struct.std::__2::__split_buffer.196"* %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE18__construct_at_endEm(%"struct.std::__2::__split_buffer.196"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.196"*, align 4
  %__n.addr = alloca i32, align 4
  %__tx = alloca %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction", align 4
  store %"struct.std::__2::__split_buffer.196"* %this, %"struct.std::__2::__split_buffer.196"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.196"*, %"struct.std::__2::__split_buffer.196"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer.196", %"struct.std::__2::__split_buffer.196"* %this1, i32 0, i32 2
  %0 = load i32, i32* %__n.addr, align 4
  %call = call %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE21_ConstructTransactionC2EPPS6_m(%"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction"* %__tx, %"class.std::__2::unique_ptr.96"** %__end_, i32 %0) #8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %__pos_ = getelementptr inbounds %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction"* %__tx, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__pos_, align 4
  %__end_2 = getelementptr inbounds %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %2 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__end_2, align 4
  %cmp = icmp ne %"class.std::__2::unique_ptr.96"* %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %call3 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE7__allocEv(%"struct.std::__2::__split_buffer.196"* %this1) #8
  %__pos_4 = getelementptr inbounds %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction"* %__tx, i32 0, i32 0
  %3 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__pos_4, align 4
  %call5 = call %"class.std::__2::unique_ptr.96"* @_ZNSt3__212__to_addressINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEEPT_S8_(%"class.std::__2::unique_ptr.96"* %3) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE9constructIS7_JEEEvRS8_PT_DpOT0_(%"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %call3, %"class.std::__2::unique_ptr.96"* %call5)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %__pos_6 = getelementptr inbounds %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction"* %__tx, i32 0, i32 0
  %4 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__pos_6, align 4
  %incdec.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %4, i32 1
  store %"class.std::__2::unique_ptr.96"* %incdec.ptr, %"class.std::__2::unique_ptr.96"** %__pos_6, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call7 = call %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction"* %__tx) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS6_RS8_EE(%"class.std::__2::vector.94"* %this, %"struct.std::__2::__split_buffer.196"* nonnull align 4 dereferenceable(20) %__v) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  %__v.addr = alloca %"struct.std::__2::__split_buffer.196"*, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer.196"* %__v, %"struct.std::__2::__split_buffer.196"** %__v.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  call void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__annotate_deleteEv(%"class.std::__2::vector.94"* %this1) #8
  %0 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base.95"* %0) #8
  %1 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %1, i32 0, i32 0
  %2 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__begin_, align 4
  %3 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %3, i32 0, i32 1
  %4 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__end_, align 4
  %5 = load %"struct.std::__2::__split_buffer.196"*, %"struct.std::__2::__split_buffer.196"** %__v.addr, align 4
  %__begin_2 = getelementptr inbounds %"struct.std::__2::__split_buffer.196", %"struct.std::__2::__split_buffer.196"* %5, i32 0, i32 1
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE46__construct_backward_with_exception_guaranteesIPS7_EEvRS8_T_SD_RSD_(%"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %call, %"class.std::__2::unique_ptr.96"* %2, %"class.std::__2::unique_ptr.96"* %4, %"class.std::__2::unique_ptr.96"** nonnull align 4 dereferenceable(4) %__begin_2)
  %6 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %__begin_3 = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %6, i32 0, i32 0
  %7 = load %"struct.std::__2::__split_buffer.196"*, %"struct.std::__2::__split_buffer.196"** %__v.addr, align 4
  %__begin_4 = getelementptr inbounds %"struct.std::__2::__split_buffer.196", %"struct.std::__2::__split_buffer.196"* %7, i32 0, i32 1
  call void @_ZNSt3__24swapIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS9_EE5valueEvE4typeERS9_SC_(%"class.std::__2::unique_ptr.96"** nonnull align 4 dereferenceable(4) %__begin_3, %"class.std::__2::unique_ptr.96"** nonnull align 4 dereferenceable(4) %__begin_4) #8
  %8 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %__end_5 = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %8, i32 0, i32 1
  %9 = load %"struct.std::__2::__split_buffer.196"*, %"struct.std::__2::__split_buffer.196"** %__v.addr, align 4
  %__end_6 = getelementptr inbounds %"struct.std::__2::__split_buffer.196", %"struct.std::__2::__split_buffer.196"* %9, i32 0, i32 2
  call void @_ZNSt3__24swapIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS9_EE5valueEvE4typeERS9_SC_(%"class.std::__2::unique_ptr.96"** nonnull align 4 dereferenceable(4) %__end_5, %"class.std::__2::unique_ptr.96"** nonnull align 4 dereferenceable(4) %__end_6) #8
  %10 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %call7 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"** @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE9__end_capEv(%"class.std::__2::__vector_base.95"* %10) #8
  %11 = load %"struct.std::__2::__split_buffer.196"*, %"struct.std::__2::__split_buffer.196"** %__v.addr, align 4
  %call8 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"** @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE9__end_capEv(%"struct.std::__2::__split_buffer.196"* %11) #8
  call void @_ZNSt3__24swapIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS9_EE5valueEvE4typeERS9_SC_(%"class.std::__2::unique_ptr.96"** nonnull align 4 dereferenceable(4) %call7, %"class.std::__2::unique_ptr.96"** nonnull align 4 dereferenceable(4) %call8) #8
  %12 = load %"struct.std::__2::__split_buffer.196"*, %"struct.std::__2::__split_buffer.196"** %__v.addr, align 4
  %__begin_9 = getelementptr inbounds %"struct.std::__2::__split_buffer.196", %"struct.std::__2::__split_buffer.196"* %12, i32 0, i32 1
  %13 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__begin_9, align 4
  %14 = load %"struct.std::__2::__split_buffer.196"*, %"struct.std::__2::__split_buffer.196"** %__v.addr, align 4
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer.196", %"struct.std::__2::__split_buffer.196"* %14, i32 0, i32 0
  store %"class.std::__2::unique_ptr.96"* %13, %"class.std::__2::unique_ptr.96"** %__first_, align 4
  %call10 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.94"* %this1) #8
  call void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE14__annotate_newEm(%"class.std::__2::vector.94"* %this1, i32 %call10) #8
  call void @_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector.94"* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer.196"* @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEED2Ev(%"struct.std::__2::__split_buffer.196"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"struct.std::__2::__split_buffer.196"*, align 4
  %this.addr = alloca %"struct.std::__2::__split_buffer.196"*, align 4
  store %"struct.std::__2::__split_buffer.196"* %this, %"struct.std::__2::__split_buffer.196"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.196"*, %"struct.std::__2::__split_buffer.196"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer.196"* %this1, %"struct.std::__2::__split_buffer.196"** %retval, align 4
  call void @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE5clearEv(%"struct.std::__2::__split_buffer.196"* %this1) #8
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer.196", %"struct.std::__2::__split_buffer.196"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__first_, align 4
  %tobool = icmp ne %"class.std::__2::unique_ptr.96"* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE7__allocEv(%"struct.std::__2::__split_buffer.196"* %this1) #8
  %__first_2 = getelementptr inbounds %"struct.std::__2::__split_buffer.196", %"struct.std::__2::__split_buffer.196"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__first_2, align 4
  %call3 = call i32 @_ZNKSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE8capacityEv(%"struct.std::__2::__split_buffer.196"* %this1)
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE10deallocateERS8_PS7_m(%"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %call, %"class.std::__2::unique_ptr.96"* %1, i32 %call3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"struct.std::__2::__split_buffer.196"*, %"struct.std::__2::__split_buffer.196"** %retval, align 4
  ret %"struct.std::__2::__split_buffer.196"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"** @_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.101"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.101"*, align 4
  store %"class.std::__2::__compressed_pair.101"* %this, %"class.std::__2::__compressed_pair.101"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.101"*, %"class.std::__2::__compressed_pair.101"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.101"* %this1 to %"struct.std::__2::__compressed_pair_elem.102"*
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"** @_ZNSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.102"* %0) #8
  ret %"class.std::__2::unique_ptr.96"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"** @_ZNSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.102"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.102"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.102"* %this, %"struct.std::__2::__compressed_pair_elem.102"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.102"*, %"struct.std::__2::__compressed_pair_elem.102"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.102", %"struct.std::__2::__compressed_pair_elem.102"* %this1, i32 0, i32 0
  ret %"class.std::__2::unique_ptr.96"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE21_ConstructTransactionC2ERS9_m(%"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction"* returned %this, %"class.std::__2::vector.94"* nonnull align 4 dereferenceable(12) %__v, i32 %__n) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction"*, align 4
  %__v.addr = alloca %"class.std::__2::vector.94"*, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction"** %this.addr, align 4
  store %"class.std::__2::vector.94"* %__v, %"class.std::__2::vector.94"** %__v.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction"*, %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction"** %this.addr, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %__v.addr, align 4
  store %"class.std::__2::vector.94"* %0, %"class.std::__2::vector.94"** %__v_, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %__v.addr, align 4
  %2 = bitcast %"class.std::__2::vector.94"* %1 to %"class.std::__2::__vector_base.95"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %2, i32 0, i32 1
  %3 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__end_, align 4
  store %"class.std::__2::unique_ptr.96"* %3, %"class.std::__2::unique_ptr.96"** %__pos_, align 4
  %__new_end_ = getelementptr inbounds %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction"* %this1, i32 0, i32 2
  %4 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %__v.addr, align 4
  %5 = bitcast %"class.std::__2::vector.94"* %4 to %"class.std::__2::__vector_base.95"*
  %__end_2 = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %5, i32 0, i32 1
  %6 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__end_2, align 4
  %7 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %6, i32 %7
  store %"class.std::__2::unique_ptr.96"* %add.ptr, %"class.std::__2::unique_ptr.96"** %__new_end_, align 4
  ret %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE9constructIS7_JEEEvRS8_PT_DpOT0_(%"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %__a, %"class.std::__2::unique_ptr.96"* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.104"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_construct.199", align 1
  store %"class.std::__2::allocator.104"* %__a, %"class.std::__2::allocator.104"** %__a.addr, align 4
  store %"class.std::__2::unique_ptr.96"* %__p, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_construct.199"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.104"*, %"class.std::__2::allocator.104"** %__a.addr, align 4
  %2 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE11__constructIS7_JEEEvNS_17integral_constantIbLb1EEERS8_PT_DpOT0_(%"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %1, %"class.std::__2::unique_ptr.96"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.96"* @_ZNSt3__212__to_addressINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEEPT_S8_(%"class.std::__2::unique_ptr.96"* %__p) #0 comdat {
entry:
  %__p.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  store %"class.std::__2::unique_ptr.96"* %__p, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  ret %"class.std::__2::unique_ptr.96"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction"*, align 4
  store %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction"** %this.addr, align 4
  %this1 = load %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction"*, %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %0 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__pos_, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %__v_, align 4
  %2 = bitcast %"class.std::__2::vector.94"* %1 to %"class.std::__2::__vector_base.95"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %2, i32 0, i32 1
  store %"class.std::__2::unique_ptr.96"* %0, %"class.std::__2::unique_ptr.96"** %__end_, align 4
  ret %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE11__constructIS7_JEEEvNS_17integral_constantIbLb1EEERS8_PT_DpOT0_(%"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %__a, %"class.std::__2::unique_ptr.96"* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.104"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  store %"class.std::__2::allocator.104"* %__a, %"class.std::__2::allocator.104"** %__a.addr, align 4
  store %"class.std::__2::unique_ptr.96"* %__p, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.104"*, %"class.std::__2::allocator.104"** %__a.addr, align 4
  %2 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  call void @_ZNSt3__29allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEE9constructIS6_JEEEvPT_DpOT0_(%"class.std::__2::allocator.104"* %1, %"class.std::__2::unique_ptr.96"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEE9constructIS6_JEEEvPT_DpOT0_(%"class.std::__2::allocator.104"* %this, %"class.std::__2::unique_ptr.96"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.104"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  store %"class.std::__2::allocator.104"* %this, %"class.std::__2::allocator.104"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.96"* %__p, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.104"*, %"class.std::__2::allocator.104"** %this.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  %1 = bitcast %"class.std::__2::unique_ptr.96"* %0 to i8*
  %2 = bitcast i8* %1 to %"class.std::__2::unique_ptr.96"*
  %call = call %"class.std::__2::unique_ptr.96"* @_ZNSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEEC2ILb1EvEEv(%"class.std::__2::unique_ptr.96"* %2) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.96"* @_ZNSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEEC2ILb1EvEEv(%"class.std::__2::unique_ptr.96"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  %ref.tmp = alloca %"class.draco::AttributesDecoderInterface"*, align 4
  %ref.tmp2 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::unique_ptr.96"* %this, %"class.std::__2::unique_ptr.96"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %this1, i32 0, i32 0
  store %"class.draco::AttributesDecoderInterface"* null, %"class.draco::AttributesDecoderInterface"** %ref.tmp, align 4
  %call = call %"class.std::__2::__compressed_pair.97"* @_ZNSt3__217__compressed_pairIPN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEEC2IS3_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.97"* %__ptr_, %"class.draco::AttributesDecoderInterface"** nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  ret %"class.std::__2::unique_ptr.96"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.97"* @_ZNSt3__217__compressed_pairIPN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEEC2IS3_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.97"* returned %this, %"class.draco::AttributesDecoderInterface"** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.97"*, align 4
  %__t1.addr = alloca %"class.draco::AttributesDecoderInterface"**, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.97"* %this, %"class.std::__2::__compressed_pair.97"** %this.addr, align 4
  store %"class.draco::AttributesDecoderInterface"** %__t1, %"class.draco::AttributesDecoderInterface"*** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.97"*, %"class.std::__2::__compressed_pair.97"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.97"* %this1 to %"struct.std::__2::__compressed_pair_elem.98"*
  %1 = load %"class.draco::AttributesDecoderInterface"**, %"class.draco::AttributesDecoderInterface"*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributesDecoderInterface"** @_ZNSt3__27forwardIPN5draco26AttributesDecoderInterfaceEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::AttributesDecoderInterface"** nonnull align 4 dereferenceable(4) %1) #8
  %call2 = call %"struct.std::__2::__compressed_pair_elem.98"* @_ZNSt3__222__compressed_pair_elemIPN5draco26AttributesDecoderInterfaceELi0ELb0EEC2IS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.98"* %0, %"class.draco::AttributesDecoderInterface"** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.97"* %this1 to %"struct.std::__2::__compressed_pair_elem.99"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #8
  %call4 = call %"struct.std::__2::__compressed_pair_elem.99"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco26AttributesDecoderInterfaceEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.99"* %2)
  ret %"class.std::__2::__compressed_pair.97"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::AttributesDecoderInterface"** @_ZNSt3__27forwardIPN5draco26AttributesDecoderInterfaceEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::AttributesDecoderInterface"** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.draco::AttributesDecoderInterface"**, align 4
  store %"class.draco::AttributesDecoderInterface"** %__t, %"class.draco::AttributesDecoderInterface"*** %__t.addr, align 4
  %0 = load %"class.draco::AttributesDecoderInterface"**, %"class.draco::AttributesDecoderInterface"*** %__t.addr, align 4
  ret %"class.draco::AttributesDecoderInterface"** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.98"* @_ZNSt3__222__compressed_pair_elemIPN5draco26AttributesDecoderInterfaceELi0ELb0EEC2IS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.98"* returned %this, %"class.draco::AttributesDecoderInterface"** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.98"*, align 4
  %__u.addr = alloca %"class.draco::AttributesDecoderInterface"**, align 4
  store %"struct.std::__2::__compressed_pair_elem.98"* %this, %"struct.std::__2::__compressed_pair_elem.98"** %this.addr, align 4
  store %"class.draco::AttributesDecoderInterface"** %__u, %"class.draco::AttributesDecoderInterface"*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.98"*, %"struct.std::__2::__compressed_pair_elem.98"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.98", %"struct.std::__2::__compressed_pair_elem.98"* %this1, i32 0, i32 0
  %0 = load %"class.draco::AttributesDecoderInterface"**, %"class.draco::AttributesDecoderInterface"*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributesDecoderInterface"** @_ZNSt3__27forwardIPN5draco26AttributesDecoderInterfaceEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::AttributesDecoderInterface"** nonnull align 4 dereferenceable(4) %0) #8
  %1 = load %"class.draco::AttributesDecoderInterface"*, %"class.draco::AttributesDecoderInterface"** %call, align 4
  store %"class.draco::AttributesDecoderInterface"* %1, %"class.draco::AttributesDecoderInterface"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.98"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  store %"struct.std::__2::__default_init_tag"* %__t, %"struct.std::__2::__default_init_tag"** %__t.addr, align 4
  %0 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t.addr, align 4
  ret %"struct.std::__2::__default_init_tag"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.99"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco26AttributesDecoderInterfaceEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.99"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.99"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.99"* %this, %"struct.std::__2::__compressed_pair_elem.99"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.99"*, %"struct.std::__2::__compressed_pair_elem.99"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.99"* %this1 to %"struct.std::__2::default_delete.100"*
  ret %"struct.std::__2::__compressed_pair_elem.99"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE6secondEv(%"class.std::__2::__compressed_pair.101"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.101"*, align 4
  store %"class.std::__2::__compressed_pair.101"* %this, %"class.std::__2::__compressed_pair.101"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.101"*, %"class.std::__2::__compressed_pair.101"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.101"* %this1 to %"struct.std::__2::__compressed_pair_elem.103"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.103"* %0) #8
  ret %"class.std::__2::allocator.104"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.103"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.103"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.103"* %this, %"struct.std::__2::__compressed_pair_elem.103"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.103"*, %"struct.std::__2::__compressed_pair_elem.103"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.103"* %this1 to %"class.std::__2::allocator.104"*
  ret %"class.std::__2::allocator.104"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8max_sizeEv(%"class.std::__2::vector.94"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp3 = alloca i32, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base.95"* %0) #8
  %call2 = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE8max_sizeERKS8_(%"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %call) #8
  store i32 %call2, i32* %ref.tmp, align 4
  %call4 = call i32 @_ZNSt3__214numeric_limitsIlE3maxEv() #8
  store i32 %call4, i32* %ref.tmp3, align 4
  %call5 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %1 = load i32, i32* %call5, align 4
  ret i32 %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector.94"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %call = call i32 @_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::__vector_base.95"* %0) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE8max_sizeERKS8_(%"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.104"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_max_size.200", align 1
  store %"class.std::__2::allocator.104"* %__a, %"class.std::__2::allocator.104"** %__a.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_max_size.200"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.104"*, %"class.std::__2::allocator.104"** %__a.addr, align 4
  %call = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS8_(%"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %1) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base.95"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.95"*, align 4
  store %"class.std::__2::__vector_base.95"* %this, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.95"*, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNKSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE6secondEv(%"class.std::__2::__compressed_pair.101"* %__end_cap_) #8
  ret %"class.std::__2::allocator.104"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS8_(%"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.104"*, align 4
  store %"class.std::__2::allocator.104"* %__a, %"class.std::__2::allocator.104"** %__a.addr, align 4
  %1 = load %"class.std::__2::allocator.104"*, %"class.std::__2::allocator.104"** %__a.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEE8max_sizeEv(%"class.std::__2::allocator.104"* %1) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__29allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEE8max_sizeEv(%"class.std::__2::allocator.104"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.104"*, align 4
  store %"class.std::__2::allocator.104"* %this, %"class.std::__2::allocator.104"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.104"*, %"class.std::__2::allocator.104"** %this.addr, align 4
  ret i32 1073741823
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNKSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE6secondEv(%"class.std::__2::__compressed_pair.101"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.101"*, align 4
  store %"class.std::__2::__compressed_pair.101"* %this, %"class.std::__2::__compressed_pair.101"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.101"*, %"class.std::__2::__compressed_pair.101"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.101"* %this1 to %"struct.std::__2::__compressed_pair_elem.103"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.103"* %0) #8
  ret %"class.std::__2::allocator.104"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.103"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.103"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.103"* %this, %"struct.std::__2::__compressed_pair_elem.103"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.103"*, %"struct.std::__2::__compressed_pair_elem.103"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.103"* %this1 to %"class.std::__2::allocator.104"*
  ret %"class.std::__2::allocator.104"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::__vector_base.95"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.95"*, align 4
  store %"class.std::__2::__vector_base.95"* %this, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.95"*, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"** @_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE9__end_capEv(%"class.std::__2::__vector_base.95"* %this1) #8
  %0 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.std::__2::unique_ptr.96"* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.std::__2::unique_ptr.96"* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"** @_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE9__end_capEv(%"class.std::__2::__vector_base.95"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.95"*, align 4
  store %"class.std::__2::__vector_base.95"* %this, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.95"*, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"** @_ZNKSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.101"* %__end_cap_) #8
  ret %"class.std::__2::unique_ptr.96"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"** @_ZNKSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.101"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.101"*, align 4
  store %"class.std::__2::__compressed_pair.101"* %this, %"class.std::__2::__compressed_pair.101"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.101"*, %"class.std::__2::__compressed_pair.101"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.101"* %this1 to %"struct.std::__2::__compressed_pair_elem.102"*
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"** @_ZNKSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.102"* %0) #8
  ret %"class.std::__2::unique_ptr.96"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"** @_ZNKSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.102"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.102"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.102"* %this, %"struct.std::__2::__compressed_pair_elem.102"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.102"*, %"struct.std::__2::__compressed_pair_elem.102"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.102", %"struct.std::__2::__compressed_pair_elem.102"* %this1, i32 0, i32 0
  ret %"class.std::__2::unique_ptr.96"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.197"* @_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEEC2IDnSA_EEOT_OT0_(%"class.std::__2::__compressed_pair.197"* returned %this, i8** nonnull align 4 dereferenceable(4) %__t1, %"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.197"*, align 4
  %__t1.addr = alloca i8**, align 4
  %__t2.addr = alloca %"class.std::__2::allocator.104"*, align 4
  store %"class.std::__2::__compressed_pair.197"* %this, %"class.std::__2::__compressed_pair.197"** %this.addr, align 4
  store i8** %__t1, i8*** %__t1.addr, align 4
  store %"class.std::__2::allocator.104"* %__t2, %"class.std::__2::allocator.104"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.197"*, %"class.std::__2::__compressed_pair.197"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.197"* %this1 to %"struct.std::__2::__compressed_pair_elem.102"*
  %1 = load i8**, i8*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %1) #8
  %call2 = call %"struct.std::__2::__compressed_pair_elem.102"* @_ZNSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEELi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.102"* %0, i8** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.197"* %this1 to i8*
  %3 = getelementptr inbounds i8, i8* %2, i32 4
  %4 = bitcast i8* %3 to %"struct.std::__2::__compressed_pair_elem.198"*
  %5 = load %"class.std::__2::allocator.104"*, %"class.std::__2::allocator.104"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNSt3__27forwardIRNS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEEEOT_RNS_16remove_referenceISA_E4typeE(%"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %5) #8
  %call4 = call %"struct.std::__2::__compressed_pair_elem.198"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEELi1ELb0EEC2IS9_vEEOT_(%"struct.std::__2::__compressed_pair_elem.198"* %4, %"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::__compressed_pair.197"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.96"* @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE8allocateERS8_m(%"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %__a, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.104"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.104"* %__a, %"class.std::__2::allocator.104"** %__a.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.104"*, %"class.std::__2::allocator.104"** %__a.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call %"class.std::__2::unique_ptr.96"* @_ZNSt3__29allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEE8allocateEmPKv(%"class.std::__2::allocator.104"* %0, i32 %1, i8* null)
  ret %"class.std::__2::unique_ptr.96"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE7__allocEv(%"struct.std::__2::__split_buffer.196"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.196"*, align 4
  store %"struct.std::__2::__split_buffer.196"* %this, %"struct.std::__2::__split_buffer.196"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.196"*, %"struct.std::__2::__split_buffer.196"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer.196", %"struct.std::__2::__split_buffer.196"* %this1, i32 0, i32 3
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE6secondEv(%"class.std::__2::__compressed_pair.197"* %__end_cap_) #8
  ret %"class.std::__2::allocator.104"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"** @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE9__end_capEv(%"struct.std::__2::__split_buffer.196"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.196"*, align 4
  store %"struct.std::__2::__split_buffer.196"* %this, %"struct.std::__2::__split_buffer.196"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.196"*, %"struct.std::__2::__split_buffer.196"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer.196", %"struct.std::__2::__split_buffer.196"* %this1, i32 0, i32 3
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"** @_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.197"* %__end_cap_) #8
  ret %"class.std::__2::unique_ptr.96"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.102"* @_ZNSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEELi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.102"* returned %this, i8** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.102"*, align 4
  %__u.addr = alloca i8**, align 4
  store %"struct.std::__2::__compressed_pair_elem.102"* %this, %"struct.std::__2::__compressed_pair_elem.102"** %this.addr, align 4
  store i8** %__u, i8*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.102"*, %"struct.std::__2::__compressed_pair_elem.102"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.102", %"struct.std::__2::__compressed_pair_elem.102"* %this1, i32 0, i32 0
  %0 = load i8**, i8*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %0) #8
  store %"class.std::__2::unique_ptr.96"* null, %"class.std::__2::unique_ptr.96"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.102"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNSt3__27forwardIRNS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEEEOT_RNS_16remove_referenceISA_E4typeE(%"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::allocator.104"*, align 4
  store %"class.std::__2::allocator.104"* %__t, %"class.std::__2::allocator.104"** %__t.addr, align 4
  %0 = load %"class.std::__2::allocator.104"*, %"class.std::__2::allocator.104"** %__t.addr, align 4
  ret %"class.std::__2::allocator.104"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.198"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEELi1ELb0EEC2IS9_vEEOT_(%"struct.std::__2::__compressed_pair_elem.198"* returned %this, %"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.198"*, align 4
  %__u.addr = alloca %"class.std::__2::allocator.104"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.198"* %this, %"struct.std::__2::__compressed_pair_elem.198"** %this.addr, align 4
  store %"class.std::__2::allocator.104"* %__u, %"class.std::__2::allocator.104"** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.198"*, %"struct.std::__2::__compressed_pair_elem.198"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.198", %"struct.std::__2::__compressed_pair_elem.198"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::allocator.104"*, %"class.std::__2::allocator.104"** %__u.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNSt3__27forwardIRNS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEEEOT_RNS_16remove_referenceISA_E4typeE(%"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %0) #8
  store %"class.std::__2::allocator.104"* %call, %"class.std::__2::allocator.104"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.198"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.96"* @_ZNSt3__29allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEE8allocateEmPKv(%"class.std::__2::allocator.104"* %this, i32 %__n, i8* %0) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.104"*, align 4
  %__n.addr = alloca i32, align 4
  %.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.104"* %this, %"class.std::__2::allocator.104"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::allocator.104"*, %"class.std::__2::allocator.104"** %this.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEE8max_sizeEv(%"class.std::__2::allocator.104"* %this1) #8
  %cmp = icmp ugt i32 %1, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__220__throw_length_errorEPKc(i8* getelementptr inbounds ([68 x i8], [68 x i8]* @.str, i32 0, i32 0)) #11
  unreachable

if.end:                                           ; preds = %entry
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  %call2 = call i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %mul, i32 4)
  %3 = bitcast i8* %call2 to %"class.std::__2::unique_ptr.96"*
  ret %"class.std::__2::unique_ptr.96"* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE6secondEv(%"class.std::__2::__compressed_pair.197"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.197"*, align 4
  store %"class.std::__2::__compressed_pair.197"* %this, %"class.std::__2::__compressed_pair.197"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.197"*, %"class.std::__2::__compressed_pair.197"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.197"* %this1 to i8*
  %add.ptr = getelementptr inbounds i8, i8* %0, i32 4
  %1 = bitcast i8* %add.ptr to %"struct.std::__2::__compressed_pair_elem.198"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.198"* %1) #8
  ret %"class.std::__2::allocator.104"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.198"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.198"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.198"* %this, %"struct.std::__2::__compressed_pair_elem.198"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.198"*, %"struct.std::__2::__compressed_pair_elem.198"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.198", %"struct.std::__2::__compressed_pair_elem.198"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::allocator.104"*, %"class.std::__2::allocator.104"** %__value_, align 4
  ret %"class.std::__2::allocator.104"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"** @_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.197"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.197"*, align 4
  store %"class.std::__2::__compressed_pair.197"* %this, %"class.std::__2::__compressed_pair.197"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.197"*, %"class.std::__2::__compressed_pair.197"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.197"* %this1 to %"struct.std::__2::__compressed_pair_elem.102"*
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"** @_ZNSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.102"* %0) #8
  ret %"class.std::__2::unique_ptr.96"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE21_ConstructTransactionC2EPPS6_m(%"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction"* returned %this, %"class.std::__2::unique_ptr.96"** %__p, i32 %__n) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.96"**, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction"* %this, %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.96"** %__p, %"class.std::__2::unique_ptr.96"*** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction"*, %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::unique_ptr.96"**, %"class.std::__2::unique_ptr.96"*** %__p.addr, align 4
  %1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %0, align 4
  store %"class.std::__2::unique_ptr.96"* %1, %"class.std::__2::unique_ptr.96"** %__pos_, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction"* %this1, i32 0, i32 1
  %2 = load %"class.std::__2::unique_ptr.96"**, %"class.std::__2::unique_ptr.96"*** %__p.addr, align 4
  %3 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %2, align 4
  %4 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %3, i32 %4
  store %"class.std::__2::unique_ptr.96"* %add.ptr, %"class.std::__2::unique_ptr.96"** %__end_, align 4
  %__dest_ = getelementptr inbounds %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction"* %this1, i32 0, i32 2
  %5 = load %"class.std::__2::unique_ptr.96"**, %"class.std::__2::unique_ptr.96"*** %__p.addr, align 4
  store %"class.std::__2::unique_ptr.96"** %5, %"class.std::__2::unique_ptr.96"*** %__dest_, align 4
  ret %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction"*, align 4
  store %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction"* %this, %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction"*, %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__pos_, align 4
  %__dest_ = getelementptr inbounds %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction"* %this1, i32 0, i32 2
  %1 = load %"class.std::__2::unique_ptr.96"**, %"class.std::__2::unique_ptr.96"*** %__dest_, align 4
  store %"class.std::__2::unique_ptr.96"* %0, %"class.std::__2::unique_ptr.96"** %1, align 4
  ret %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__annotate_deleteEv(%"class.std::__2::vector.94"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  %call = call %"class.std::__2::unique_ptr.96"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.94"* %this1) #8
  %0 = bitcast %"class.std::__2::unique_ptr.96"* %call to i8*
  %call2 = call %"class.std::__2::unique_ptr.96"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.94"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector.94"* %this1) #8
  %add.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %call2, i32 %call3
  %1 = bitcast %"class.std::__2::unique_ptr.96"* %add.ptr to i8*
  %call4 = call %"class.std::__2::unique_ptr.96"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.94"* %this1) #8
  %call5 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.94"* %this1) #8
  %add.ptr6 = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %call4, i32 %call5
  %2 = bitcast %"class.std::__2::unique_ptr.96"* %add.ptr6 to i8*
  %call7 = call %"class.std::__2::unique_ptr.96"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.94"* %this1) #8
  %call8 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector.94"* %this1) #8
  %add.ptr9 = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %call7, i32 %call8
  %3 = bitcast %"class.std::__2::unique_ptr.96"* %add.ptr9 to i8*
  call void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE31__annotate_contiguous_containerEPKvSB_SB_SB_(%"class.std::__2::vector.94"* %this1, i8* %0, i8* %1, i8* %2, i8* %3) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE46__construct_backward_with_exception_guaranteesIPS7_EEvRS8_T_SD_RSD_(%"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %__a, %"class.std::__2::unique_ptr.96"* %__begin1, %"class.std::__2::unique_ptr.96"* %__end1, %"class.std::__2::unique_ptr.96"** nonnull align 4 dereferenceable(4) %__end2) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.104"*, align 4
  %__begin1.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  %__end1.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  %__end2.addr = alloca %"class.std::__2::unique_ptr.96"**, align 4
  store %"class.std::__2::allocator.104"* %__a, %"class.std::__2::allocator.104"** %__a.addr, align 4
  store %"class.std::__2::unique_ptr.96"* %__begin1, %"class.std::__2::unique_ptr.96"** %__begin1.addr, align 4
  store %"class.std::__2::unique_ptr.96"* %__end1, %"class.std::__2::unique_ptr.96"** %__end1.addr, align 4
  store %"class.std::__2::unique_ptr.96"** %__end2, %"class.std::__2::unique_ptr.96"*** %__end2.addr, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %0 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__end1.addr, align 4
  %1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__begin1.addr, align 4
  %cmp = icmp ne %"class.std::__2::unique_ptr.96"* %0, %1
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %2 = load %"class.std::__2::allocator.104"*, %"class.std::__2::allocator.104"** %__a.addr, align 4
  %3 = load %"class.std::__2::unique_ptr.96"**, %"class.std::__2::unique_ptr.96"*** %__end2.addr, align 4
  %4 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %3, align 4
  %add.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %4, i32 -1
  %call = call %"class.std::__2::unique_ptr.96"* @_ZNSt3__212__to_addressINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEEPT_S8_(%"class.std::__2::unique_ptr.96"* %add.ptr) #8
  %5 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__end1.addr, align 4
  %incdec.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %5, i32 -1
  store %"class.std::__2::unique_ptr.96"* %incdec.ptr, %"class.std::__2::unique_ptr.96"** %__end1.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"* @_ZNSt3__24moveIRNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOS9_(%"class.std::__2::unique_ptr.96"* nonnull align 4 dereferenceable(4) %incdec.ptr) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE9constructIS7_JS7_EEEvRS8_PT_DpOT0_(%"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %2, %"class.std::__2::unique_ptr.96"* %call, %"class.std::__2::unique_ptr.96"* nonnull align 4 dereferenceable(4) %call1)
  %6 = load %"class.std::__2::unique_ptr.96"**, %"class.std::__2::unique_ptr.96"*** %__end2.addr, align 4
  %7 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %6, align 4
  %incdec.ptr2 = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %7, i32 -1
  store %"class.std::__2::unique_ptr.96"* %incdec.ptr2, %"class.std::__2::unique_ptr.96"** %6, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__24swapIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS9_EE5valueEvE4typeERS9_SC_(%"class.std::__2::unique_ptr.96"** nonnull align 4 dereferenceable(4) %__x, %"class.std::__2::unique_ptr.96"** nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %__x.addr = alloca %"class.std::__2::unique_ptr.96"**, align 4
  %__y.addr = alloca %"class.std::__2::unique_ptr.96"**, align 4
  %__t = alloca %"class.std::__2::unique_ptr.96"*, align 4
  store %"class.std::__2::unique_ptr.96"** %__x, %"class.std::__2::unique_ptr.96"*** %__x.addr, align 4
  store %"class.std::__2::unique_ptr.96"** %__y, %"class.std::__2::unique_ptr.96"*** %__y.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.96"**, %"class.std::__2::unique_ptr.96"*** %__x.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"** @_ZNSt3__24moveIRPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOSA_(%"class.std::__2::unique_ptr.96"** nonnull align 4 dereferenceable(4) %0) #8
  %1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %call, align 4
  store %"class.std::__2::unique_ptr.96"* %1, %"class.std::__2::unique_ptr.96"** %__t, align 4
  %2 = load %"class.std::__2::unique_ptr.96"**, %"class.std::__2::unique_ptr.96"*** %__y.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"** @_ZNSt3__24moveIRPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOSA_(%"class.std::__2::unique_ptr.96"** nonnull align 4 dereferenceable(4) %2) #8
  %3 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %call1, align 4
  %4 = load %"class.std::__2::unique_ptr.96"**, %"class.std::__2::unique_ptr.96"*** %__x.addr, align 4
  store %"class.std::__2::unique_ptr.96"* %3, %"class.std::__2::unique_ptr.96"** %4, align 4
  %call2 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"** @_ZNSt3__24moveIRPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOSA_(%"class.std::__2::unique_ptr.96"** nonnull align 4 dereferenceable(4) %__t) #8
  %5 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %call2, align 4
  %6 = load %"class.std::__2::unique_ptr.96"**, %"class.std::__2::unique_ptr.96"*** %__y.addr, align 4
  store %"class.std::__2::unique_ptr.96"* %5, %"class.std::__2::unique_ptr.96"** %6, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE14__annotate_newEm(%"class.std::__2::vector.94"* %this, i32 %__current_size) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  %__current_size.addr = alloca i32, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  store i32 %__current_size, i32* %__current_size.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  %call = call %"class.std::__2::unique_ptr.96"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.94"* %this1) #8
  %0 = bitcast %"class.std::__2::unique_ptr.96"* %call to i8*
  %call2 = call %"class.std::__2::unique_ptr.96"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.94"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector.94"* %this1) #8
  %add.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %call2, i32 %call3
  %1 = bitcast %"class.std::__2::unique_ptr.96"* %add.ptr to i8*
  %call4 = call %"class.std::__2::unique_ptr.96"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.94"* %this1) #8
  %call5 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector.94"* %this1) #8
  %add.ptr6 = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %call4, i32 %call5
  %2 = bitcast %"class.std::__2::unique_ptr.96"* %add.ptr6 to i8*
  %call7 = call %"class.std::__2::unique_ptr.96"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.94"* %this1) #8
  %3 = load i32, i32* %__current_size.addr, align 4
  %add.ptr8 = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %call7, i32 %3
  %4 = bitcast %"class.std::__2::unique_ptr.96"* %add.ptr8 to i8*
  call void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE31__annotate_contiguous_containerEPKvSB_SB_SB_(%"class.std::__2::vector.94"* %this1, i8* %0, i8* %1, i8* %2, i8* %4) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector.94"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE31__annotate_contiguous_containerEPKvSB_SB_SB_(%"class.std::__2::vector.94"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.96"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.94"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %0, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__begin_, align 4
  %call = call %"class.std::__2::unique_ptr.96"* @_ZNSt3__212__to_addressINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEEPT_S8_(%"class.std::__2::unique_ptr.96"* %1) #8
  ret %"class.std::__2::unique_ptr.96"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE9constructIS7_JS7_EEEvRS8_PT_DpOT0_(%"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %__a, %"class.std::__2::unique_ptr.96"* %__p, %"class.std::__2::unique_ptr.96"* nonnull align 4 dereferenceable(4) %__args) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.104"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  %__args.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_construct.201", align 1
  store %"class.std::__2::allocator.104"* %__a, %"class.std::__2::allocator.104"** %__a.addr, align 4
  store %"class.std::__2::unique_ptr.96"* %__p, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  store %"class.std::__2::unique_ptr.96"* %__args, %"class.std::__2::unique_ptr.96"** %__args.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_construct.201"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.104"*, %"class.std::__2::allocator.104"** %__a.addr, align 4
  %2 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  %3 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"* @_ZNSt3__27forwardINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"class.std::__2::unique_ptr.96"* nonnull align 4 dereferenceable(4) %3) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE11__constructIS7_JS7_EEEvNS_17integral_constantIbLb1EEERS8_PT_DpOT0_(%"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %1, %"class.std::__2::unique_ptr.96"* %2, %"class.std::__2::unique_ptr.96"* nonnull align 4 dereferenceable(4) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE11__constructIS7_JS7_EEEvNS_17integral_constantIbLb1EEERS8_PT_DpOT0_(%"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %__a, %"class.std::__2::unique_ptr.96"* %__p, %"class.std::__2::unique_ptr.96"* nonnull align 4 dereferenceable(4) %__args) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.104"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  %__args.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  store %"class.std::__2::allocator.104"* %__a, %"class.std::__2::allocator.104"** %__a.addr, align 4
  store %"class.std::__2::unique_ptr.96"* %__p, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  store %"class.std::__2::unique_ptr.96"* %__args, %"class.std::__2::unique_ptr.96"** %__args.addr, align 4
  %1 = load %"class.std::__2::allocator.104"*, %"class.std::__2::allocator.104"** %__a.addr, align 4
  %2 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  %3 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"* @_ZNSt3__27forwardINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"class.std::__2::unique_ptr.96"* nonnull align 4 dereferenceable(4) %3) #8
  call void @_ZNSt3__29allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEE9constructIS6_JS6_EEEvPT_DpOT0_(%"class.std::__2::allocator.104"* %1, %"class.std::__2::unique_ptr.96"* %2, %"class.std::__2::unique_ptr.96"* nonnull align 4 dereferenceable(4) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"* @_ZNSt3__27forwardINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"class.std::__2::unique_ptr.96"* nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  store %"class.std::__2::unique_ptr.96"* %__t, %"class.std::__2::unique_ptr.96"** %__t.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__t.addr, align 4
  ret %"class.std::__2::unique_ptr.96"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEE9constructIS6_JS6_EEEvPT_DpOT0_(%"class.std::__2::allocator.104"* %this, %"class.std::__2::unique_ptr.96"* %__p, %"class.std::__2::unique_ptr.96"* nonnull align 4 dereferenceable(4) %__args) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.104"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  %__args.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  store %"class.std::__2::allocator.104"* %this, %"class.std::__2::allocator.104"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.96"* %__p, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  store %"class.std::__2::unique_ptr.96"* %__args, %"class.std::__2::unique_ptr.96"** %__args.addr, align 4
  %this1 = load %"class.std::__2::allocator.104"*, %"class.std::__2::allocator.104"** %this.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  %1 = bitcast %"class.std::__2::unique_ptr.96"* %0 to i8*
  %2 = bitcast i8* %1 to %"class.std::__2::unique_ptr.96"*
  %3 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"* @_ZNSt3__27forwardINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"class.std::__2::unique_ptr.96"* nonnull align 4 dereferenceable(4) %3) #8
  %call2 = call %"class.std::__2::unique_ptr.96"* @_ZNSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEEC2EOS5_(%"class.std::__2::unique_ptr.96"* %2, %"class.std::__2::unique_ptr.96"* nonnull align 4 dereferenceable(4) %call) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.96"* @_ZNSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEEC2EOS5_(%"class.std::__2::unique_ptr.96"* returned %this, %"class.std::__2::unique_ptr.96"* nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  %__u.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  %ref.tmp = alloca %"class.draco::AttributesDecoderInterface"*, align 4
  store %"class.std::__2::unique_ptr.96"* %this, %"class.std::__2::unique_ptr.96"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.96"* %__u, %"class.std::__2::unique_ptr.96"** %__u.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__u.addr, align 4
  %call = call %"class.draco::AttributesDecoderInterface"* @_ZNSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE7releaseEv(%"class.std::__2::unique_ptr.96"* %0) #8
  store %"class.draco::AttributesDecoderInterface"* %call, %"class.draco::AttributesDecoderInterface"** %ref.tmp, align 4
  %1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__u.addr, align 4
  %call2 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.100"* @_ZNSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE11get_deleterEv(%"class.std::__2::unique_ptr.96"* %1) #8
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.100"* @_ZNSt3__27forwardINS_14default_deleteIN5draco26AttributesDecoderInterfaceEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.100"* nonnull align 1 dereferenceable(1) %call2) #8
  %call4 = call %"class.std::__2::__compressed_pair.97"* @_ZNSt3__217__compressed_pairIPN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEEC2IS3_S5_EEOT_OT0_(%"class.std::__2::__compressed_pair.97"* %__ptr_, %"class.draco::AttributesDecoderInterface"** nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::default_delete.100"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::unique_ptr.96"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::AttributesDecoderInterface"* @_ZNSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE7releaseEv(%"class.std::__2::unique_ptr.96"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  %__t = alloca %"class.draco::AttributesDecoderInterface"*, align 4
  store %"class.std::__2::unique_ptr.96"* %this, %"class.std::__2::unique_ptr.96"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributesDecoderInterface"** @_ZNSt3__217__compressed_pairIPN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.97"* %__ptr_) #8
  %0 = load %"class.draco::AttributesDecoderInterface"*, %"class.draco::AttributesDecoderInterface"** %call, align 4
  store %"class.draco::AttributesDecoderInterface"* %0, %"class.draco::AttributesDecoderInterface"** %__t, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::AttributesDecoderInterface"** @_ZNSt3__217__compressed_pairIPN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.97"* %__ptr_2) #8
  store %"class.draco::AttributesDecoderInterface"* null, %"class.draco::AttributesDecoderInterface"** %call3, align 4
  %1 = load %"class.draco::AttributesDecoderInterface"*, %"class.draco::AttributesDecoderInterface"** %__t, align 4
  ret %"class.draco::AttributesDecoderInterface"* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.100"* @_ZNSt3__27forwardINS_14default_deleteIN5draco26AttributesDecoderInterfaceEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.100"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"struct.std::__2::default_delete.100"*, align 4
  store %"struct.std::__2::default_delete.100"* %__t, %"struct.std::__2::default_delete.100"** %__t.addr, align 4
  %0 = load %"struct.std::__2::default_delete.100"*, %"struct.std::__2::default_delete.100"** %__t.addr, align 4
  ret %"struct.std::__2::default_delete.100"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.100"* @_ZNSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE11get_deleterEv(%"class.std::__2::unique_ptr.96"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  store %"class.std::__2::unique_ptr.96"* %this, %"class.std::__2::unique_ptr.96"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %this1, i32 0, i32 0
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.100"* @_ZNSt3__217__compressed_pairIPN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.97"* %__ptr_) #8
  ret %"struct.std::__2::default_delete.100"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.97"* @_ZNSt3__217__compressed_pairIPN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEEC2IS3_S5_EEOT_OT0_(%"class.std::__2::__compressed_pair.97"* returned %this, %"class.draco::AttributesDecoderInterface"** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::default_delete.100"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.97"*, align 4
  %__t1.addr = alloca %"class.draco::AttributesDecoderInterface"**, align 4
  %__t2.addr = alloca %"struct.std::__2::default_delete.100"*, align 4
  store %"class.std::__2::__compressed_pair.97"* %this, %"class.std::__2::__compressed_pair.97"** %this.addr, align 4
  store %"class.draco::AttributesDecoderInterface"** %__t1, %"class.draco::AttributesDecoderInterface"*** %__t1.addr, align 4
  store %"struct.std::__2::default_delete.100"* %__t2, %"struct.std::__2::default_delete.100"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.97"*, %"class.std::__2::__compressed_pair.97"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.97"* %this1 to %"struct.std::__2::__compressed_pair_elem.98"*
  %1 = load %"class.draco::AttributesDecoderInterface"**, %"class.draco::AttributesDecoderInterface"*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributesDecoderInterface"** @_ZNSt3__27forwardIPN5draco26AttributesDecoderInterfaceEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::AttributesDecoderInterface"** nonnull align 4 dereferenceable(4) %1) #8
  %call2 = call %"struct.std::__2::__compressed_pair_elem.98"* @_ZNSt3__222__compressed_pair_elemIPN5draco26AttributesDecoderInterfaceELi0ELb0EEC2IS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.98"* %0, %"class.draco::AttributesDecoderInterface"** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.97"* %this1 to %"struct.std::__2::__compressed_pair_elem.99"*
  %3 = load %"struct.std::__2::default_delete.100"*, %"struct.std::__2::default_delete.100"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.100"* @_ZNSt3__27forwardINS_14default_deleteIN5draco26AttributesDecoderInterfaceEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.100"* nonnull align 1 dereferenceable(1) %3) #8
  %call4 = call %"struct.std::__2::__compressed_pair_elem.99"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco26AttributesDecoderInterfaceEEELi1ELb1EEC2IS4_vEEOT_(%"struct.std::__2::__compressed_pair_elem.99"* %2, %"struct.std::__2::default_delete.100"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::__compressed_pair.97"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::AttributesDecoderInterface"** @_ZNSt3__217__compressed_pairIPN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.97"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.97"*, align 4
  store %"class.std::__2::__compressed_pair.97"* %this, %"class.std::__2::__compressed_pair.97"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.97"*, %"class.std::__2::__compressed_pair.97"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.97"* %this1 to %"struct.std::__2::__compressed_pair_elem.98"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributesDecoderInterface"** @_ZNSt3__222__compressed_pair_elemIPN5draco26AttributesDecoderInterfaceELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.98"* %0) #8
  ret %"class.draco::AttributesDecoderInterface"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::AttributesDecoderInterface"** @_ZNSt3__222__compressed_pair_elemIPN5draco26AttributesDecoderInterfaceELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.98"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.98"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.98"* %this, %"struct.std::__2::__compressed_pair_elem.98"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.98"*, %"struct.std::__2::__compressed_pair_elem.98"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.98", %"struct.std::__2::__compressed_pair_elem.98"* %this1, i32 0, i32 0
  ret %"class.draco::AttributesDecoderInterface"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.100"* @_ZNSt3__217__compressed_pairIPN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.97"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.97"*, align 4
  store %"class.std::__2::__compressed_pair.97"* %this, %"class.std::__2::__compressed_pair.97"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.97"*, %"class.std::__2::__compressed_pair.97"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.97"* %this1 to %"struct.std::__2::__compressed_pair_elem.99"*
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.100"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco26AttributesDecoderInterfaceEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.99"* %0) #8
  ret %"struct.std::__2::default_delete.100"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.100"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco26AttributesDecoderInterfaceEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.99"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.99"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.99"* %this, %"struct.std::__2::__compressed_pair_elem.99"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.99"*, %"struct.std::__2::__compressed_pair_elem.99"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.99"* %this1 to %"struct.std::__2::default_delete.100"*
  ret %"struct.std::__2::default_delete.100"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.99"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco26AttributesDecoderInterfaceEEELi1ELb1EEC2IS4_vEEOT_(%"struct.std::__2::__compressed_pair_elem.99"* returned %this, %"struct.std::__2::default_delete.100"* nonnull align 1 dereferenceable(1) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.99"*, align 4
  %__u.addr = alloca %"struct.std::__2::default_delete.100"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.99"* %this, %"struct.std::__2::__compressed_pair_elem.99"** %this.addr, align 4
  store %"struct.std::__2::default_delete.100"* %__u, %"struct.std::__2::default_delete.100"** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.99"*, %"struct.std::__2::__compressed_pair_elem.99"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.99"* %this1 to %"struct.std::__2::default_delete.100"*
  %1 = load %"struct.std::__2::default_delete.100"*, %"struct.std::__2::default_delete.100"** %__u.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.100"* @_ZNSt3__27forwardINS_14default_deleteIN5draco26AttributesDecoderInterfaceEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.100"* nonnull align 1 dereferenceable(1) %1) #8
  ret %"struct.std::__2::__compressed_pair_elem.99"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"** @_ZNSt3__24moveIRPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOSA_(%"class.std::__2::unique_ptr.96"** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::unique_ptr.96"**, align 4
  store %"class.std::__2::unique_ptr.96"** %__t, %"class.std::__2::unique_ptr.96"*** %__t.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.96"**, %"class.std::__2::unique_ptr.96"*** %__t.addr, align 4
  ret %"class.std::__2::unique_ptr.96"** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE5clearEv(%"struct.std::__2::__split_buffer.196"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.196"*, align 4
  store %"struct.std::__2::__split_buffer.196"* %this, %"struct.std::__2::__split_buffer.196"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.196"*, %"struct.std::__2::__split_buffer.196"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"struct.std::__2::__split_buffer.196", %"struct.std::__2::__split_buffer.196"* %this1, i32 0, i32 1
  %0 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__begin_, align 4
  call void @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE17__destruct_at_endEPS6_(%"struct.std::__2::__split_buffer.196"* %this1, %"class.std::__2::unique_ptr.96"* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE10deallocateERS8_PS7_m(%"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %__a, %"class.std::__2::unique_ptr.96"* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.104"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.104"* %__a, %"class.std::__2::allocator.104"** %__a.addr, align 4
  store %"class.std::__2::unique_ptr.96"* %__p, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.104"*, %"class.std::__2::allocator.104"** %__a.addr, align 4
  %1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEE10deallocateEPS6_m(%"class.std::__2::allocator.104"* %0, %"class.std::__2::unique_ptr.96"* %1, i32 %2) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE8capacityEv(%"struct.std::__2::__split_buffer.196"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.196"*, align 4
  store %"struct.std::__2::__split_buffer.196"* %this, %"struct.std::__2::__split_buffer.196"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.196"*, %"struct.std::__2::__split_buffer.196"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"** @_ZNKSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE9__end_capEv(%"struct.std::__2::__split_buffer.196"* %this1) #8
  %0 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %call, align 4
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer.196", %"struct.std::__2::__split_buffer.196"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__first_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.std::__2::unique_ptr.96"* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.std::__2::unique_ptr.96"* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE17__destruct_at_endEPS6_(%"struct.std::__2::__split_buffer.196"* %this, %"class.std::__2::unique_ptr.96"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.196"*, align 4
  %__new_last.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant.195", align 1
  store %"struct.std::__2::__split_buffer.196"* %this, %"struct.std::__2::__split_buffer.196"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.96"* %__new_last, %"class.std::__2::unique_ptr.96"** %__new_last.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.196"*, %"struct.std::__2::__split_buffer.196"** %this.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__new_last.addr, align 4
  call void @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE17__destruct_at_endEPS6_NS_17integral_constantIbLb0EEE(%"struct.std::__2::__split_buffer.196"* %this1, %"class.std::__2::unique_ptr.96"* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE17__destruct_at_endEPS6_NS_17integral_constantIbLb0EEE(%"struct.std::__2::__split_buffer.196"* %this, %"class.std::__2::unique_ptr.96"* %__new_last) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant.195", align 1
  %this.addr = alloca %"struct.std::__2::__split_buffer.196"*, align 4
  %__new_last.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  store %"struct.std::__2::__split_buffer.196"* %this, %"struct.std::__2::__split_buffer.196"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.96"* %__new_last, %"class.std::__2::unique_ptr.96"** %__new_last.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.196"*, %"struct.std::__2::__split_buffer.196"** %this.addr, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__new_last.addr, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer.196", %"struct.std::__2::__split_buffer.196"* %this1, i32 0, i32 2
  %2 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__end_, align 4
  %cmp = icmp ne %"class.std::__2::unique_ptr.96"* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE7__allocEv(%"struct.std::__2::__split_buffer.196"* %this1) #8
  %__end_2 = getelementptr inbounds %"struct.std::__2::__split_buffer.196", %"struct.std::__2::__split_buffer.196"* %this1, i32 0, i32 2
  %3 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__end_2, align 4
  %incdec.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %3, i32 -1
  store %"class.std::__2::unique_ptr.96"* %incdec.ptr, %"class.std::__2::unique_ptr.96"** %__end_2, align 4
  %call3 = call %"class.std::__2::unique_ptr.96"* @_ZNSt3__212__to_addressINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEEPT_S8_(%"class.std::__2::unique_ptr.96"* %incdec.ptr) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE7destroyIS7_EEvRS8_PT_(%"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %call, %"class.std::__2::unique_ptr.96"* %call3)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE7destroyIS7_EEvRS8_PT_(%"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %__a, %"class.std::__2::unique_ptr.96"* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.104"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy.202", align 1
  store %"class.std::__2::allocator.104"* %__a, %"class.std::__2::allocator.104"** %__a.addr, align 4
  store %"class.std::__2::unique_ptr.96"* %__p, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy.202"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.104"*, %"class.std::__2::allocator.104"** %__a.addr, align 4
  %2 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE9__destroyIS7_EEvNS_17integral_constantIbLb1EEERS8_PT_(%"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %1, %"class.std::__2::unique_ptr.96"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE9__destroyIS7_EEvNS_17integral_constantIbLb1EEERS8_PT_(%"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %__a, %"class.std::__2::unique_ptr.96"* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.104"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  store %"class.std::__2::allocator.104"* %__a, %"class.std::__2::allocator.104"** %__a.addr, align 4
  store %"class.std::__2::unique_ptr.96"* %__p, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.104"*, %"class.std::__2::allocator.104"** %__a.addr, align 4
  %2 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  call void @_ZNSt3__29allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEE7destroyEPS6_(%"class.std::__2::allocator.104"* %1, %"class.std::__2::unique_ptr.96"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEE7destroyEPS6_(%"class.std::__2::allocator.104"* %this, %"class.std::__2::unique_ptr.96"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.104"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  store %"class.std::__2::allocator.104"* %this, %"class.std::__2::allocator.104"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.96"* %__p, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.104"*, %"class.std::__2::allocator.104"** %this.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  %call = call %"class.std::__2::unique_ptr.96"* @_ZNSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.96"* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEE10deallocateEPS6_m(%"class.std::__2::allocator.104"* %this, %"class.std::__2::unique_ptr.96"* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.104"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.104"* %this, %"class.std::__2::allocator.104"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.96"* %__p, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.104"*, %"class.std::__2::allocator.104"** %this.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  %1 = bitcast %"class.std::__2::unique_ptr.96"* %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"** @_ZNKSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE9__end_capEv(%"struct.std::__2::__split_buffer.196"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.196"*, align 4
  store %"struct.std::__2::__split_buffer.196"* %this, %"struct.std::__2::__split_buffer.196"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.196"*, %"struct.std::__2::__split_buffer.196"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer.196", %"struct.std::__2::__split_buffer.196"* %this1, i32 0, i32 3
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"** @_ZNKSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.197"* %__end_cap_) #8
  ret %"class.std::__2::unique_ptr.96"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"** @_ZNKSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.197"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.197"*, align 4
  store %"class.std::__2::__compressed_pair.197"* %this, %"class.std::__2::__compressed_pair.197"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.197"*, %"class.std::__2::__compressed_pair.197"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.197"* %this1 to %"struct.std::__2::__compressed_pair_elem.102"*
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"** @_ZNKSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.102"* %0) #8
  ret %"class.std::__2::unique_ptr.96"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE27__invalidate_iterators_pastEPS6_(%"class.std::__2::vector.94"* %this, %"class.std::__2::unique_ptr.96"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  %__new_last.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.96"* %__new_last, %"class.std::__2::unique_ptr.96"** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__destruct_at_endEPS6_(%"class.std::__2::__vector_base.95"* %this, %"class.std::__2::unique_ptr.96"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.95"*, align 4
  %__new_last.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  %__soon_to_be_end = alloca %"class.std::__2::unique_ptr.96"*, align 4
  store %"class.std::__2::__vector_base.95"* %this, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.96"* %__new_last, %"class.std::__2::unique_ptr.96"** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.95"*, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %this1, i32 0, i32 1
  %0 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__end_, align 4
  store %"class.std::__2::unique_ptr.96"* %0, %"class.std::__2::unique_ptr.96"** %__soon_to_be_end, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__new_last.addr, align 4
  %2 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__soon_to_be_end, align 4
  %cmp = icmp ne %"class.std::__2::unique_ptr.96"* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base.95"* %this1) #8
  %3 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__soon_to_be_end, align 4
  %incdec.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %3, i32 -1
  store %"class.std::__2::unique_ptr.96"* %incdec.ptr, %"class.std::__2::unique_ptr.96"** %__soon_to_be_end, align 4
  %call2 = call %"class.std::__2::unique_ptr.96"* @_ZNSt3__212__to_addressINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEEPT_S8_(%"class.std::__2::unique_ptr.96"* %incdec.ptr) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE7destroyIS7_EEvRS8_PT_(%"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %call, %"class.std::__2::unique_ptr.96"* %call2)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %4 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__new_last.addr, align 4
  %__end_3 = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %this1, i32 0, i32 1
  store %"class.std::__2::unique_ptr.96"* %4, %"class.std::__2::unique_ptr.96"** %__end_3, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__annotate_shrinkEm(%"class.std::__2::vector.94"* %this, i32 %__old_size) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  %__old_size.addr = alloca i32, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  store i32 %__old_size, i32* %__old_size.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  %call = call %"class.std::__2::unique_ptr.96"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.94"* %this1) #8
  %0 = bitcast %"class.std::__2::unique_ptr.96"* %call to i8*
  %call2 = call %"class.std::__2::unique_ptr.96"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.94"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector.94"* %this1) #8
  %add.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %call2, i32 %call3
  %1 = bitcast %"class.std::__2::unique_ptr.96"* %add.ptr to i8*
  %call4 = call %"class.std::__2::unique_ptr.96"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.94"* %this1) #8
  %2 = load i32, i32* %__old_size.addr, align 4
  %add.ptr5 = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %call4, i32 %2
  %3 = bitcast %"class.std::__2::unique_ptr.96"* %add.ptr5 to i8*
  %call6 = call %"class.std::__2::unique_ptr.96"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.94"* %this1) #8
  %call7 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.94"* %this1) #8
  %add.ptr8 = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %call6, i32 %call7
  %4 = bitcast %"class.std::__2::unique_ptr.96"* %add.ptr8 to i8*
  call void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE31__annotate_contiguous_containerEPKvSB_SB_SB_(%"class.std::__2::vector.94"* %this1, i8* %0, i8* %1, i8* %3, i8* %4) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.96"* %this, %"class.draco::AttributesDecoderInterface"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  %__p.addr = alloca %"class.draco::AttributesDecoderInterface"*, align 4
  %__tmp = alloca %"class.draco::AttributesDecoderInterface"*, align 4
  store %"class.std::__2::unique_ptr.96"* %this, %"class.std::__2::unique_ptr.96"** %this.addr, align 4
  store %"class.draco::AttributesDecoderInterface"* %__p, %"class.draco::AttributesDecoderInterface"** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributesDecoderInterface"** @_ZNSt3__217__compressed_pairIPN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.97"* %__ptr_) #8
  %0 = load %"class.draco::AttributesDecoderInterface"*, %"class.draco::AttributesDecoderInterface"** %call, align 4
  store %"class.draco::AttributesDecoderInterface"* %0, %"class.draco::AttributesDecoderInterface"** %__tmp, align 4
  %1 = load %"class.draco::AttributesDecoderInterface"*, %"class.draco::AttributesDecoderInterface"** %__p.addr, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::AttributesDecoderInterface"** @_ZNSt3__217__compressed_pairIPN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.97"* %__ptr_2) #8
  store %"class.draco::AttributesDecoderInterface"* %1, %"class.draco::AttributesDecoderInterface"** %call3, align 4
  %2 = load %"class.draco::AttributesDecoderInterface"*, %"class.draco::AttributesDecoderInterface"** %__tmp, align 4
  %tobool = icmp ne %"class.draco::AttributesDecoderInterface"* %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__ptr_4 = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %this1, i32 0, i32 0
  %call5 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.100"* @_ZNSt3__217__compressed_pairIPN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.97"* %__ptr_4) #8
  %3 = load %"class.draco::AttributesDecoderInterface"*, %"class.draco::AttributesDecoderInterface"** %__tmp, align 4
  call void @_ZNKSt3__214default_deleteIN5draco26AttributesDecoderInterfaceEEclEPS2_(%"struct.std::__2::default_delete.100"* %call5, %"class.draco::AttributesDecoderInterface"* %3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__214default_deleteIN5draco26AttributesDecoderInterfaceEEclEPS2_(%"struct.std::__2::default_delete.100"* %this, %"class.draco::AttributesDecoderInterface"* %__ptr) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::default_delete.100"*, align 4
  %__ptr.addr = alloca %"class.draco::AttributesDecoderInterface"*, align 4
  store %"struct.std::__2::default_delete.100"* %this, %"struct.std::__2::default_delete.100"** %this.addr, align 4
  store %"class.draco::AttributesDecoderInterface"* %__ptr, %"class.draco::AttributesDecoderInterface"** %__ptr.addr, align 4
  %this1 = load %"struct.std::__2::default_delete.100"*, %"struct.std::__2::default_delete.100"** %this.addr, align 4
  %0 = load %"class.draco::AttributesDecoderInterface"*, %"class.draco::AttributesDecoderInterface"** %__ptr.addr, align 4
  %isnull = icmp eq %"class.draco::AttributesDecoderInterface"* %0, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %entry
  %1 = bitcast %"class.draco::AttributesDecoderInterface"* %0 to void (%"class.draco::AttributesDecoderInterface"*)***
  %vtable = load void (%"class.draco::AttributesDecoderInterface"*)**, void (%"class.draco::AttributesDecoderInterface"*)*** %1, align 4
  %vfn = getelementptr inbounds void (%"class.draco::AttributesDecoderInterface"*)*, void (%"class.draco::AttributesDecoderInterface"*)** %vtable, i64 1
  %2 = load void (%"class.draco::AttributesDecoderInterface"*)*, void (%"class.draco::AttributesDecoderInterface"*)** %vfn, align 4
  call void %2(%"class.draco::AttributesDecoderInterface"* %0) #8
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::PointsSequencer"* @_ZN5draco15PointsSequencerC2Ev(%"class.draco::PointsSequencer"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointsSequencer"*, align 4
  store %"class.draco::PointsSequencer"* %this, %"class.draco::PointsSequencer"** %this.addr, align 4
  %this1 = load %"class.draco::PointsSequencer"*, %"class.draco::PointsSequencer"** %this.addr, align 4
  %0 = bitcast %"class.draco::PointsSequencer"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [6 x i8*] }, { [6 x i8*] }* @_ZTVN5draco15PointsSequencerE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %out_point_ids_ = getelementptr inbounds %"class.draco::PointsSequencer", %"class.draco::PointsSequencer"* %this1, i32 0, i32 1
  store %"class.std::__2::vector.139"* null, %"class.std::__2::vector.139"** %out_point_ids_, align 4
  ret %"class.draco::PointsSequencer"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::LinearSequencer"* @_ZN5draco15LinearSequencerD2Ev(%"class.draco::LinearSequencer"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::LinearSequencer"*, align 4
  store %"class.draco::LinearSequencer"* %this, %"class.draco::LinearSequencer"** %this.addr, align 4
  %this1 = load %"class.draco::LinearSequencer"*, %"class.draco::LinearSequencer"** %this.addr, align 4
  %0 = bitcast %"class.draco::LinearSequencer"* %this1 to %"class.draco::PointsSequencer"*
  %call = call %"class.draco::PointsSequencer"* @_ZN5draco15PointsSequencerD2Ev(%"class.draco::PointsSequencer"* %0) #8
  ret %"class.draco::LinearSequencer"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco15LinearSequencerD0Ev(%"class.draco::LinearSequencer"* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::LinearSequencer"*, align 4
  store %"class.draco::LinearSequencer"* %this, %"class.draco::LinearSequencer"** %this.addr, align 4
  %this1 = load %"class.draco::LinearSequencer"*, %"class.draco::LinearSequencer"** %this.addr, align 4
  %call = call %"class.draco::LinearSequencer"* @_ZN5draco15LinearSequencerD2Ev(%"class.draco::LinearSequencer"* %this1) #8
  %0 = bitcast %"class.draco::LinearSequencer"* %this1 to i8*
  call void @_ZdlPv(i8* %0) #10
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco15LinearSequencer34UpdatePointToAttributeIndexMappingEPNS_14PointAttributeE(%"class.draco::LinearSequencer"* %this, %"class.draco::PointAttribute"* %attribute) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::LinearSequencer"*, align 4
  %attribute.addr = alloca %"class.draco::PointAttribute"*, align 4
  store %"class.draco::LinearSequencer"* %this, %"class.draco::LinearSequencer"** %this.addr, align 4
  store %"class.draco::PointAttribute"* %attribute, %"class.draco::PointAttribute"** %attribute.addr, align 4
  %this1 = load %"class.draco::LinearSequencer"*, %"class.draco::LinearSequencer"** %this.addr, align 4
  %0 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %attribute.addr, align 4
  call void @_ZN5draco14PointAttribute18SetIdentityMappingEv(%"class.draco::PointAttribute"* %0)
  ret i1 true
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco15LinearSequencer24GenerateSequenceInternalEv(%"class.draco::LinearSequencer"* %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::LinearSequencer"*, align 4
  %i = alloca i32, align 4
  %ref.tmp = alloca %"class.draco::IndexType.116", align 4
  store %"class.draco::LinearSequencer"* %this, %"class.draco::LinearSequencer"** %this.addr, align 4
  %this1 = load %"class.draco::LinearSequencer"*, %"class.draco::LinearSequencer"** %this.addr, align 4
  %num_points_ = getelementptr inbounds %"class.draco::LinearSequencer", %"class.draco::LinearSequencer"* %this1, i32 0, i32 1
  %0 = load i32, i32* %num_points_, align 4
  %cmp = icmp slt i32 %0, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %1 = bitcast %"class.draco::LinearSequencer"* %this1 to %"class.draco::PointsSequencer"*
  %call = call %"class.std::__2::vector.139"* @_ZNK5draco15PointsSequencer13out_point_idsEv(%"class.draco::PointsSequencer"* %1)
  %num_points_2 = getelementptr inbounds %"class.draco::LinearSequencer", %"class.draco::LinearSequencer"* %this1, i32 0, i32 1
  %2 = load i32, i32* %num_points_2, align 4
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE6resizeEm(%"class.std::__2::vector.139"* %call, i32 %2)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %3 = load i32, i32* %i, align 4
  %num_points_3 = getelementptr inbounds %"class.draco::LinearSequencer", %"class.draco::LinearSequencer"* %this1, i32 0, i32 1
  %4 = load i32, i32* %num_points_3, align 4
  %cmp4 = icmp slt i32 %3, %4
  br i1 %cmp4, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %5 = load i32, i32* %i, align 4
  %call5 = call %"class.draco::IndexType.116"* @_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEC2Ej(%"class.draco::IndexType.116"* %ref.tmp, i32 %5)
  %6 = bitcast %"class.draco::LinearSequencer"* %this1 to %"class.draco::PointsSequencer"*
  %call6 = call %"class.std::__2::vector.139"* @_ZNK5draco15PointsSequencer13out_point_idsEv(%"class.draco::PointsSequencer"* %6)
  %7 = load i32, i32* %i, align 4
  %call7 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.116"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE2atEm(%"class.std::__2::vector.139"* %call6, i32 %7)
  %call8 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.116"* @_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEaSERKS2_(%"class.draco::IndexType.116"* %call7, %"class.draco::IndexType.116"* nonnull align 4 dereferenceable(4) %ref.tmp)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %8 = load i32, i32* %i, align 4
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %for.end, %if.then
  %9 = load i1, i1* %retval, align 1
  ret i1 %9
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::PointsSequencer"* @_ZN5draco15PointsSequencerD2Ev(%"class.draco::PointsSequencer"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointsSequencer"*, align 4
  store %"class.draco::PointsSequencer"* %this, %"class.draco::PointsSequencer"** %this.addr, align 4
  %this1 = load %"class.draco::PointsSequencer"*, %"class.draco::PointsSequencer"** %this.addr, align 4
  ret %"class.draco::PointsSequencer"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco15PointsSequencerD0Ev(%"class.draco::PointsSequencer"* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointsSequencer"*, align 4
  store %"class.draco::PointsSequencer"* %this, %"class.draco::PointsSequencer"** %this.addr, align 4
  %this1 = load %"class.draco::PointsSequencer"*, %"class.draco::PointsSequencer"** %this.addr, align 4
  call void @llvm.trap() #12
  unreachable
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco15PointsSequencer34UpdatePointToAttributeIndexMappingEPNS_14PointAttributeE(%"class.draco::PointsSequencer"* %this, %"class.draco::PointAttribute"* %0) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointsSequencer"*, align 4
  %.addr = alloca %"class.draco::PointAttribute"*, align 4
  store %"class.draco::PointsSequencer"* %this, %"class.draco::PointsSequencer"** %this.addr, align 4
  store %"class.draco::PointAttribute"* %0, %"class.draco::PointAttribute"** %.addr, align 4
  %this1 = load %"class.draco::PointsSequencer"*, %"class.draco::PointsSequencer"** %this.addr, align 4
  ret i1 false
}

declare void @__cxa_pure_virtual() unnamed_addr

; Function Attrs: cold noreturn nounwind
declare void @llvm.trap() #7

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco14PointAttribute18SetIdentityMappingEv(%"class.draco::PointAttribute"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointAttribute"*, align 4
  store %"class.draco::PointAttribute"* %this, %"class.draco::PointAttribute"** %this.addr, align 4
  %this1 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %this.addr, align 4
  %identity_mapping_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 4
  store i8 1, i8* %identity_mapping_, align 4
  %indices_map_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 2
  call void @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEEE5clearEv(%"class.draco::IndexTypeVector"* %indices_map_)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEEE5clearEv(%"class.draco::IndexTypeVector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector"*, align 4
  store %"class.draco::IndexTypeVector"* %this, %"class.draco::IndexTypeVector"** %this.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector"*, %"class.draco::IndexTypeVector"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector", %"class.draco::IndexTypeVector"* %this1, i32 0, i32 0
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5clearEv(%"class.std::__2::vector.68"* %vector_) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5clearEv(%"class.std::__2::vector.68"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.68"*, align 4
  %__old_size = alloca i32, align 4
  store %"class.std::__2::vector.68"* %this, %"class.std::__2::vector.68"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.68"* %this1) #8
  store i32 %call, i32* %__old_size, align 4
  %0 = bitcast %"class.std::__2::vector.68"* %this1 to %"class.std::__2::__vector_base.69"*
  call void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5clearEv(%"class.std::__2::__vector_base.69"* %0) #8
  %1 = load i32, i32* %__old_size, align 4
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_shrinkEm(%"class.std::__2::vector.68"* %this1, i32 %1) #8
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector.68"* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.68"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.68"*, align 4
  store %"class.std::__2::vector.68"* %this, %"class.std::__2::vector.68"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.68"* %this1 to %"class.std::__2::__vector_base.69"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %0, i32 0, i32 1
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector.68"* %this1 to %"class.std::__2::__vector_base.69"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %2, i32 0, i32 0
  %3 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.draco::IndexType"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.draco::IndexType"* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5clearEv(%"class.std::__2::__vector_base.69"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.69"*, align 4
  store %"class.std::__2::__vector_base.69"* %this, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.69"*, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  call void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_(%"class.std::__2::__vector_base.69"* %this1, %"class.draco::IndexType"* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_shrinkEm(%"class.std::__2::vector.68"* %this, i32 %__old_size) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.68"*, align 4
  %__old_size.addr = alloca i32, align 4
  store %"class.std::__2::vector.68"* %this, %"class.std::__2::vector.68"** %this.addr, align 4
  store i32 %__old_size, i32* %__old_size.addr, align 4
  %this1 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %this.addr, align 4
  %call = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.68"* %this1) #8
  %0 = bitcast %"class.draco::IndexType"* %call to i8*
  %call2 = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.68"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.68"* %this1) #8
  %add.ptr = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %call2, i32 %call3
  %1 = bitcast %"class.draco::IndexType"* %add.ptr to i8*
  %call4 = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.68"* %this1) #8
  %2 = load i32, i32* %__old_size.addr, align 4
  %add.ptr5 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %call4, i32 %2
  %3 = bitcast %"class.draco::IndexType"* %add.ptr5 to i8*
  %call6 = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.68"* %this1) #8
  %call7 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.68"* %this1) #8
  %add.ptr8 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %call6, i32 %call7
  %4 = bitcast %"class.draco::IndexType"* %add.ptr8 to i8*
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_(%"class.std::__2::vector.68"* %this1, i8* %0, i8* %1, i8* %3, i8* %4) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector.68"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.68"*, align 4
  store %"class.std::__2::vector.68"* %this, %"class.std::__2::vector.68"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_(%"class.std::__2::__vector_base.69"* %this, %"class.draco::IndexType"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.69"*, align 4
  %__new_last.addr = alloca %"class.draco::IndexType"*, align 4
  %__soon_to_be_end = alloca %"class.draco::IndexType"*, align 4
  store %"class.std::__2::__vector_base.69"* %this, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  store %"class.draco::IndexType"* %__new_last, %"class.draco::IndexType"** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.69"*, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %this1, i32 0, i32 1
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__end_, align 4
  store %"class.draco::IndexType"* %0, %"class.draco::IndexType"** %__soon_to_be_end, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__new_last.addr, align 4
  %2 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__soon_to_be_end, align 4
  %cmp = icmp ne %"class.draco::IndexType"* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.69"* %this1) #8
  %3 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__soon_to_be_end, align 4
  %incdec.ptr = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %3, i32 -1
  store %"class.draco::IndexType"* %incdec.ptr, %"class.draco::IndexType"** %__soon_to_be_end, align 4
  %call2 = call %"class.draco::IndexType"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType"* %incdec.ptr) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE7destroyIS5_EEvRS6_PT_(%"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %call, %"class.draco::IndexType"* %call2)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %4 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__new_last.addr, align 4
  %__end_3 = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %this1, i32 0, i32 1
  store %"class.draco::IndexType"* %4, %"class.draco::IndexType"** %__end_3, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE7destroyIS5_EEvRS6_PT_(%"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType"* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.73"*, align 4
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy.203", align 1
  store %"class.std::__2::allocator.73"* %__a, %"class.std::__2::allocator.73"** %__a.addr, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy.203"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.73"*, %"class.std::__2::allocator.73"** %__a.addr, align 4
  %2 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE9__destroyIS5_EEvNS_17integral_constantIbLb1EEERS6_PT_(%"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %1, %"class.draco::IndexType"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.69"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.69"*, align 4
  store %"class.std::__2::__vector_base.69"* %this, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.69"*, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.70"* %__end_cap_) #8
  ret %"class.std::__2::allocator.73"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType"* %__p) #0 comdat {
entry:
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  ret %"class.draco::IndexType"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE9__destroyIS5_EEvNS_17integral_constantIbLb1EEERS6_PT_(%"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType"* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.73"*, align 4
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.std::__2::allocator.73"* %__a, %"class.std::__2::allocator.73"** %__a.addr, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.73"*, %"class.std::__2::allocator.73"** %__a.addr, align 4
  %2 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  call void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE7destroyEPS4_(%"class.std::__2::allocator.73"* %1, %"class.draco::IndexType"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE7destroyEPS4_(%"class.std::__2::allocator.73"* %this, %"class.draco::IndexType"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.73"*, align 4
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.std::__2::allocator.73"* %this, %"class.std::__2::allocator.73"** %this.addr, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.73"*, %"class.std::__2::allocator.73"** %this.addr, align 4
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.70"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.70"*, align 4
  store %"class.std::__2::__compressed_pair.70"* %this, %"class.std::__2::__compressed_pair.70"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.70"*, %"class.std::__2::__compressed_pair.70"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.70"* %this1 to %"struct.std::__2::__compressed_pair_elem.72"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.72"* %0) #8
  ret %"class.std::__2::allocator.73"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.72"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.72"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.72"* %this, %"struct.std::__2::__compressed_pair_elem.72"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.72"*, %"struct.std::__2::__compressed_pair_elem.72"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.72"* %this1 to %"class.std::__2::allocator.73"*
  ret %"class.std::__2::allocator.73"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_(%"class.std::__2::vector.68"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.68"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector.68"* %this, %"class.std::__2::vector.68"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.68"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.68"*, align 4
  store %"class.std::__2::vector.68"* %this, %"class.std::__2::vector.68"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.68"* %this1 to %"class.std::__2::__vector_base.69"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %0, i32 0, i32 0
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  %call = call %"class.draco::IndexType"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType"* %1) #8
  ret %"class.draco::IndexType"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.68"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.68"*, align 4
  store %"class.std::__2::vector.68"* %this, %"class.std::__2::vector.68"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.68"* %this1 to %"class.std::__2::__vector_base.69"*
  %call = call i32 @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::__vector_base.69"* %0) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::__vector_base.69"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.69"*, align 4
  store %"class.std::__2::__vector_base.69"* %this, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.69"*, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base.69"* %this1) #8
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %this1, i32 0, i32 0
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.draco::IndexType"* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.draco::IndexType"* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base.69"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.69"*, align 4
  store %"class.std::__2::__vector_base.69"* %this, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.69"*, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.70"* %__end_cap_) #8
  ret %"class.draco::IndexType"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.70"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.70"*, align 4
  store %"class.std::__2::__compressed_pair.70"* %this, %"class.std::__2::__compressed_pair.70"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.70"*, %"class.std::__2::__compressed_pair.70"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.70"* %this1 to %"struct.std::__2::__compressed_pair_elem.71"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.71"* %0) #8
  ret %"class.draco::IndexType"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.71"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.71"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.71"* %this, %"struct.std::__2::__compressed_pair_elem.71"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.71"*, %"struct.std::__2::__compressed_pair_elem.71"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.71", %"struct.std::__2::__compressed_pair_elem.71"* %this1, i32 0, i32 0
  ret %"class.draco::IndexType"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector.139"* @_ZNK5draco15PointsSequencer13out_point_idsEv(%"class.draco::PointsSequencer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointsSequencer"*, align 4
  store %"class.draco::PointsSequencer"* %this, %"class.draco::PointsSequencer"** %this.addr, align 4
  %this1 = load %"class.draco::PointsSequencer"*, %"class.draco::PointsSequencer"** %this.addr, align 4
  %out_point_ids_ = getelementptr inbounds %"class.draco::PointsSequencer", %"class.draco::PointsSequencer"* %this1, i32 0, i32 1
  %0 = load %"class.std::__2::vector.139"*, %"class.std::__2::vector.139"** %out_point_ids_, align 4
  ret %"class.std::__2::vector.139"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE6resizeEm(%"class.std::__2::vector.139"* %this, i32 %__sz) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.139"*, align 4
  %__sz.addr = alloca i32, align 4
  %__cs = alloca i32, align 4
  store %"class.std::__2::vector.139"* %this, %"class.std::__2::vector.139"** %this.addr, align 4
  store i32 %__sz, i32* %__sz.addr, align 4
  %this1 = load %"class.std::__2::vector.139"*, %"class.std::__2::vector.139"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.139"* %this1) #8
  store i32 %call, i32* %__cs, align 4
  %0 = load i32, i32* %__cs, align 4
  %1 = load i32, i32* %__sz.addr, align 4
  %cmp = icmp ult i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %__sz.addr, align 4
  %3 = load i32, i32* %__cs, align 4
  %sub = sub i32 %2, %3
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE8__appendEm(%"class.std::__2::vector.139"* %this1, i32 %sub)
  br label %if.end4

if.else:                                          ; preds = %entry
  %4 = load i32, i32* %__cs, align 4
  %5 = load i32, i32* %__sz.addr, align 4
  %cmp2 = icmp ugt i32 %4, %5
  br i1 %cmp2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.else
  %6 = bitcast %"class.std::__2::vector.139"* %this1 to %"class.std::__2::__vector_base.140"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.140", %"class.std::__2::__vector_base.140"* %6, i32 0, i32 0
  %7 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %__begin_, align 4
  %8 = load i32, i32* %__sz.addr, align 4
  %add.ptr = getelementptr inbounds %"class.draco::IndexType.116", %"class.draco::IndexType.116"* %7, i32 %8
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_(%"class.std::__2::vector.139"* %this1, %"class.draco::IndexType.116"* %add.ptr) #8
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.else
  br label %if.end4

if.end4:                                          ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType.116"* @_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEC2Ej(%"class.draco::IndexType.116"* returned %this, i32 %value) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.116"*, align 4
  %value.addr = alloca i32, align 4
  store %"class.draco::IndexType.116"* %this, %"class.draco::IndexType.116"** %this.addr, align 4
  store i32 %value, i32* %value.addr, align 4
  %this1 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.116", %"class.draco::IndexType.116"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value.addr, align 4
  store i32 %0, i32* %value_, align 4
  ret %"class.draco::IndexType.116"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.116"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE2atEm(%"class.std::__2::vector.139"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.139"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector.139"* %this, %"class.std::__2::vector.139"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.139"*, %"class.std::__2::vector.139"** %this.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.139"* %this1) #8
  %cmp = icmp uge i32 %0, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %"class.std::__2::vector.139"* %this1 to %"class.std::__2::__vector_base_common"*
  call void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_out_of_rangeEv(%"class.std::__2::__vector_base_common"* %1) #11
  unreachable

if.end:                                           ; preds = %entry
  %2 = bitcast %"class.std::__2::vector.139"* %this1 to %"class.std::__2::__vector_base.140"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.140", %"class.std::__2::__vector_base.140"* %2, i32 0, i32 0
  %3 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %__begin_, align 4
  %4 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds %"class.draco::IndexType.116", %"class.draco::IndexType.116"* %3, i32 %4
  ret %"class.draco::IndexType.116"* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.116"* @_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEaSERKS2_(%"class.draco::IndexType.116"* %this, %"class.draco::IndexType.116"* nonnull align 4 dereferenceable(4) %i) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.116"*, align 4
  %i.addr = alloca %"class.draco::IndexType.116"*, align 4
  store %"class.draco::IndexType.116"* %this, %"class.draco::IndexType.116"** %this.addr, align 4
  store %"class.draco::IndexType.116"* %i, %"class.draco::IndexType.116"** %i.addr, align 4
  %this1 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %this.addr, align 4
  %0 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %i.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.116", %"class.draco::IndexType.116"* %0, i32 0, i32 0
  %1 = load i32, i32* %value_, align 4
  %value_2 = getelementptr inbounds %"class.draco::IndexType.116", %"class.draco::IndexType.116"* %this1, i32 0, i32 0
  store i32 %1, i32* %value_2, align 4
  ret %"class.draco::IndexType.116"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.139"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.139"*, align 4
  store %"class.std::__2::vector.139"* %this, %"class.std::__2::vector.139"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.139"*, %"class.std::__2::vector.139"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.139"* %this1 to %"class.std::__2::__vector_base.140"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.140", %"class.std::__2::__vector_base.140"* %0, i32 0, i32 1
  %1 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector.139"* %this1 to %"class.std::__2::__vector_base.140"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.140", %"class.std::__2::__vector_base.140"* %2, i32 0, i32 0
  %3 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.draco::IndexType.116"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.draco::IndexType.116"* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE8__appendEm(%"class.std::__2::vector.139"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.139"*, align 4
  %__n.addr = alloca i32, align 4
  %__a = alloca %"class.std::__2::allocator.144"*, align 4
  %__v = alloca %"struct.std::__2::__split_buffer.204", align 4
  store %"class.std::__2::vector.139"* %this, %"class.std::__2::vector.139"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.139"*, %"class.std::__2::vector.139"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.139"* %this1 to %"class.std::__2::__vector_base.140"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.116"** @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base.140"* %0) #8
  %1 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %call, align 4
  %2 = bitcast %"class.std::__2::vector.139"* %this1 to %"class.std::__2::__vector_base.140"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.140", %"class.std::__2::__vector_base.140"* %2, i32 0, i32 1
  %3 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %__end_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.draco::IndexType.116"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.draco::IndexType.116"* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  %4 = load i32, i32* %__n.addr, align 4
  %cmp = icmp uge i32 %sub.ptr.div, %4
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %5 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE18__construct_at_endEm(%"class.std::__2::vector.139"* %this1, i32 %5)
  br label %if.end

if.else:                                          ; preds = %entry
  %6 = bitcast %"class.std::__2::vector.139"* %this1 to %"class.std::__2::__vector_base.140"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.144"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.140"* %6) #8
  store %"class.std::__2::allocator.144"* %call2, %"class.std::__2::allocator.144"** %__a, align 4
  %call3 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.139"* %this1) #8
  %7 = load i32, i32* %__n.addr, align 4
  %add = add i32 %call3, %7
  %call4 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE11__recommendEm(%"class.std::__2::vector.139"* %this1, i32 %add)
  %call5 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.139"* %this1) #8
  %8 = load %"class.std::__2::allocator.144"*, %"class.std::__2::allocator.144"** %__a, align 4
  %call6 = call %"struct.std::__2::__split_buffer.204"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEEC2EmmS7_(%"struct.std::__2::__split_buffer.204"* %__v, i32 %call4, i32 %call5, %"class.std::__2::allocator.144"* nonnull align 1 dereferenceable(1) %8)
  %9 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE18__construct_at_endEm(%"struct.std::__2::__split_buffer.204"* %__v, i32 %9)
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS4_RS6_EE(%"class.std::__2::vector.139"* %this1, %"struct.std::__2::__split_buffer.204"* nonnull align 4 dereferenceable(20) %__v)
  %call7 = call %"struct.std::__2::__split_buffer.204"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEED2Ev(%"struct.std::__2::__split_buffer.204"* %__v) #8
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_(%"class.std::__2::vector.139"* %this, %"class.draco::IndexType.116"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.139"*, align 4
  %__new_last.addr = alloca %"class.draco::IndexType.116"*, align 4
  %__old_size = alloca i32, align 4
  store %"class.std::__2::vector.139"* %this, %"class.std::__2::vector.139"** %this.addr, align 4
  store %"class.draco::IndexType.116"* %__new_last, %"class.draco::IndexType.116"** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::vector.139"*, %"class.std::__2::vector.139"** %this.addr, align 4
  %0 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %__new_last.addr, align 4
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE27__invalidate_iterators_pastEPS4_(%"class.std::__2::vector.139"* %this1, %"class.draco::IndexType.116"* %0)
  %call = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.139"* %this1) #8
  store i32 %call, i32* %__old_size, align 4
  %1 = bitcast %"class.std::__2::vector.139"* %this1 to %"class.std::__2::__vector_base.140"*
  %2 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %__new_last.addr, align 4
  call void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_(%"class.std::__2::__vector_base.140"* %1, %"class.draco::IndexType.116"* %2) #8
  %3 = load i32, i32* %__old_size, align 4
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_shrinkEm(%"class.std::__2::vector.139"* %this1, i32 %3) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.116"** @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base.140"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.140"*, align 4
  store %"class.std::__2::__vector_base.140"* %this, %"class.std::__2::__vector_base.140"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.140"*, %"class.std::__2::__vector_base.140"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.140", %"class.std::__2::__vector_base.140"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.116"** @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.141"* %__end_cap_) #8
  ret %"class.draco::IndexType.116"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE18__construct_at_endEm(%"class.std::__2::vector.139"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.139"*, align 4
  %__n.addr = alloca i32, align 4
  %__tx = alloca %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction", align 4
  store %"class.std::__2::vector.139"* %this, %"class.std::__2::vector.139"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.139"*, %"class.std::__2::vector.139"** %this.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %call = call %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionC2ERS7_m(%"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"* %__tx, %"class.std::__2::vector.139"* nonnull align 4 dereferenceable(12) %this1, i32 %0)
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %1 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %__pos_, align 4
  %__new_end_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"* %__tx, i32 0, i32 2
  %2 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %__new_end_, align 4
  %cmp = icmp ne %"class.draco::IndexType.116"* %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = bitcast %"class.std::__2::vector.139"* %this1 to %"class.std::__2::__vector_base.140"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.144"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.140"* %3) #8
  %__pos_3 = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %4 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %__pos_3, align 4
  %call4 = call %"class.draco::IndexType.116"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType.116"* %4) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE9constructIS5_JEEEvRS6_PT_DpOT0_(%"class.std::__2::allocator.144"* nonnull align 1 dereferenceable(1) %call2, %"class.draco::IndexType.116"* %call4)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %__pos_5 = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %5 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %__pos_5, align 4
  %incdec.ptr = getelementptr inbounds %"class.draco::IndexType.116", %"class.draco::IndexType.116"* %5, i32 1
  store %"class.draco::IndexType.116"* %incdec.ptr, %"class.draco::IndexType.116"** %__pos_5, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call6 = call %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"* %__tx) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.144"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.140"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.140"*, align 4
  store %"class.std::__2::__vector_base.140"* %this, %"class.std::__2::__vector_base.140"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.140"*, %"class.std::__2::__vector_base.140"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.140", %"class.std::__2::__vector_base.140"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.144"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.141"* %__end_cap_) #8
  ret %"class.std::__2::allocator.144"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE11__recommendEm(%"class.std::__2::vector.139"* %this, i32 %__new_size) #0 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %"class.std::__2::vector.139"*, align 4
  %__new_size.addr = alloca i32, align 4
  %__ms = alloca i32, align 4
  %__cap = alloca i32, align 4
  %ref.tmp = alloca i32, align 4
  store %"class.std::__2::vector.139"* %this, %"class.std::__2::vector.139"** %this.addr, align 4
  store i32 %__new_size, i32* %__new_size.addr, align 4
  %this1 = load %"class.std::__2::vector.139"*, %"class.std::__2::vector.139"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE8max_sizeEv(%"class.std::__2::vector.139"* %this1) #8
  store i32 %call, i32* %__ms, align 4
  %0 = load i32, i32* %__new_size.addr, align 4
  %1 = load i32, i32* %__ms, align 4
  %cmp = icmp ugt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = bitcast %"class.std::__2::vector.139"* %this1 to %"class.std::__2::__vector_base_common"*
  call void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"* %2) #11
  unreachable

if.end:                                           ; preds = %entry
  %call2 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.139"* %this1) #8
  store i32 %call2, i32* %__cap, align 4
  %3 = load i32, i32* %__cap, align 4
  %4 = load i32, i32* %__ms, align 4
  %div = udiv i32 %4, 2
  %cmp3 = icmp uge i32 %3, %div
  br i1 %cmp3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %if.end
  %5 = load i32, i32* %__ms, align 4
  store i32 %5, i32* %retval, align 4
  br label %return

if.end5:                                          ; preds = %if.end
  %6 = load i32, i32* %__cap, align 4
  %mul = mul i32 2, %6
  store i32 %mul, i32* %ref.tmp, align 4
  %call6 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %__new_size.addr)
  %7 = load i32, i32* %call6, align 4
  store i32 %7, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end5, %if.then4
  %8 = load i32, i32* %retval, align 4
  ret i32 %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer.204"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEEC2EmmS7_(%"struct.std::__2::__split_buffer.204"* returned %this, i32 %__cap, i32 %__start, %"class.std::__2::allocator.144"* nonnull align 1 dereferenceable(1) %__a) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"struct.std::__2::__split_buffer.204"*, align 4
  %this.addr = alloca %"struct.std::__2::__split_buffer.204"*, align 4
  %__cap.addr = alloca i32, align 4
  %__start.addr = alloca i32, align 4
  %__a.addr = alloca %"class.std::__2::allocator.144"*, align 4
  %ref.tmp = alloca i8*, align 4
  store %"struct.std::__2::__split_buffer.204"* %this, %"struct.std::__2::__split_buffer.204"** %this.addr, align 4
  store i32 %__cap, i32* %__cap.addr, align 4
  store i32 %__start, i32* %__start.addr, align 4
  store %"class.std::__2::allocator.144"* %__a, %"class.std::__2::allocator.144"** %__a.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.204"*, %"struct.std::__2::__split_buffer.204"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer.204"* %this1, %"struct.std::__2::__split_buffer.204"** %retval, align 4
  %0 = bitcast %"struct.std::__2::__split_buffer.204"* %this1 to %"class.std::__2::__split_buffer_common"*
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer.204", %"struct.std::__2::__split_buffer.204"* %this1, i32 0, i32 3
  store i8* null, i8** %ref.tmp, align 4
  %1 = load %"class.std::__2::allocator.144"*, %"class.std::__2::allocator.144"** %__a.addr, align 4
  %call = call %"class.std::__2::__compressed_pair.205"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEEC2IDnS8_EEOT_OT0_(%"class.std::__2::__compressed_pair.205"* %__end_cap_, i8** nonnull align 4 dereferenceable(4) %ref.tmp, %"class.std::__2::allocator.144"* nonnull align 1 dereferenceable(1) %1)
  %2 = load i32, i32* %__cap.addr, align 4
  %cmp = icmp ne i32 %2, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.144"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE7__allocEv(%"struct.std::__2::__split_buffer.204"* %this1) #8
  %3 = load i32, i32* %__cap.addr, align 4
  %call3 = call %"class.draco::IndexType.116"* @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE8allocateERS6_m(%"class.std::__2::allocator.144"* nonnull align 1 dereferenceable(1) %call2, i32 %3)
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %"class.draco::IndexType.116"* [ %call3, %cond.true ], [ null, %cond.false ]
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer.204", %"struct.std::__2::__split_buffer.204"* %this1, i32 0, i32 0
  store %"class.draco::IndexType.116"* %cond, %"class.draco::IndexType.116"** %__first_, align 4
  %__first_4 = getelementptr inbounds %"struct.std::__2::__split_buffer.204", %"struct.std::__2::__split_buffer.204"* %this1, i32 0, i32 0
  %4 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %__first_4, align 4
  %5 = load i32, i32* %__start.addr, align 4
  %add.ptr = getelementptr inbounds %"class.draco::IndexType.116", %"class.draco::IndexType.116"* %4, i32 %5
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer.204", %"struct.std::__2::__split_buffer.204"* %this1, i32 0, i32 2
  store %"class.draco::IndexType.116"* %add.ptr, %"class.draco::IndexType.116"** %__end_, align 4
  %__begin_ = getelementptr inbounds %"struct.std::__2::__split_buffer.204", %"struct.std::__2::__split_buffer.204"* %this1, i32 0, i32 1
  store %"class.draco::IndexType.116"* %add.ptr, %"class.draco::IndexType.116"** %__begin_, align 4
  %__first_5 = getelementptr inbounds %"struct.std::__2::__split_buffer.204", %"struct.std::__2::__split_buffer.204"* %this1, i32 0, i32 0
  %6 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %__first_5, align 4
  %7 = load i32, i32* %__cap.addr, align 4
  %add.ptr6 = getelementptr inbounds %"class.draco::IndexType.116", %"class.draco::IndexType.116"* %6, i32 %7
  %call7 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.116"** @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE9__end_capEv(%"struct.std::__2::__split_buffer.204"* %this1) #8
  store %"class.draco::IndexType.116"* %add.ptr6, %"class.draco::IndexType.116"** %call7, align 4
  %8 = load %"struct.std::__2::__split_buffer.204"*, %"struct.std::__2::__split_buffer.204"** %retval, align 4
  ret %"struct.std::__2::__split_buffer.204"* %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE18__construct_at_endEm(%"struct.std::__2::__split_buffer.204"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.204"*, align 4
  %__n.addr = alloca i32, align 4
  %__tx = alloca %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction", align 4
  store %"struct.std::__2::__split_buffer.204"* %this, %"struct.std::__2::__split_buffer.204"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.204"*, %"struct.std::__2::__split_buffer.204"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer.204", %"struct.std::__2::__split_buffer.204"* %this1, i32 0, i32 2
  %0 = load i32, i32* %__n.addr, align 4
  %call = call %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE21_ConstructTransactionC2EPPS4_m(%"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction"* %__tx, %"class.draco::IndexType.116"** %__end_, i32 %0) #8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %__pos_ = getelementptr inbounds %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction"* %__tx, i32 0, i32 0
  %1 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %__pos_, align 4
  %__end_2 = getelementptr inbounds %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %2 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %__end_2, align 4
  %cmp = icmp ne %"class.draco::IndexType.116"* %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %call3 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.144"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE7__allocEv(%"struct.std::__2::__split_buffer.204"* %this1) #8
  %__pos_4 = getelementptr inbounds %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction"* %__tx, i32 0, i32 0
  %3 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %__pos_4, align 4
  %call5 = call %"class.draco::IndexType.116"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType.116"* %3) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE9constructIS5_JEEEvRS6_PT_DpOT0_(%"class.std::__2::allocator.144"* nonnull align 1 dereferenceable(1) %call3, %"class.draco::IndexType.116"* %call5)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %__pos_6 = getelementptr inbounds %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction"* %__tx, i32 0, i32 0
  %4 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %__pos_6, align 4
  %incdec.ptr = getelementptr inbounds %"class.draco::IndexType.116", %"class.draco::IndexType.116"* %4, i32 1
  store %"class.draco::IndexType.116"* %incdec.ptr, %"class.draco::IndexType.116"** %__pos_6, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call7 = call %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction"* %__tx) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS4_RS6_EE(%"class.std::__2::vector.139"* %this, %"struct.std::__2::__split_buffer.204"* nonnull align 4 dereferenceable(20) %__v) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.139"*, align 4
  %__v.addr = alloca %"struct.std::__2::__split_buffer.204"*, align 4
  store %"class.std::__2::vector.139"* %this, %"class.std::__2::vector.139"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer.204"* %__v, %"struct.std::__2::__split_buffer.204"** %__v.addr, align 4
  %this1 = load %"class.std::__2::vector.139"*, %"class.std::__2::vector.139"** %this.addr, align 4
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_deleteEv(%"class.std::__2::vector.139"* %this1) #8
  %0 = bitcast %"class.std::__2::vector.139"* %this1 to %"class.std::__2::__vector_base.140"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.144"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.140"* %0) #8
  %1 = bitcast %"class.std::__2::vector.139"* %this1 to %"class.std::__2::__vector_base.140"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.140", %"class.std::__2::__vector_base.140"* %1, i32 0, i32 0
  %2 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %__begin_, align 4
  %3 = bitcast %"class.std::__2::vector.139"* %this1 to %"class.std::__2::__vector_base.140"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.140", %"class.std::__2::__vector_base.140"* %3, i32 0, i32 1
  %4 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %__end_, align 4
  %5 = load %"struct.std::__2::__split_buffer.204"*, %"struct.std::__2::__split_buffer.204"** %__v.addr, align 4
  %__begin_2 = getelementptr inbounds %"struct.std::__2::__split_buffer.204", %"struct.std::__2::__split_buffer.204"* %5, i32 0, i32 1
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE46__construct_backward_with_exception_guaranteesIS5_EENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS6_PT_SC_EE5valuesr31is_trivially_move_constructibleISC_EE5valueEvE4typeERS6_SD_SD_RSD_(%"class.std::__2::allocator.144"* nonnull align 1 dereferenceable(1) %call, %"class.draco::IndexType.116"* %2, %"class.draco::IndexType.116"* %4, %"class.draco::IndexType.116"** nonnull align 4 dereferenceable(4) %__begin_2)
  %6 = bitcast %"class.std::__2::vector.139"* %this1 to %"class.std::__2::__vector_base.140"*
  %__begin_3 = getelementptr inbounds %"class.std::__2::__vector_base.140", %"class.std::__2::__vector_base.140"* %6, i32 0, i32 0
  %7 = load %"struct.std::__2::__split_buffer.204"*, %"struct.std::__2::__split_buffer.204"** %__v.addr, align 4
  %__begin_4 = getelementptr inbounds %"struct.std::__2::__split_buffer.204", %"struct.std::__2::__split_buffer.204"* %7, i32 0, i32 1
  call void @_ZNSt3__24swapIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS7_EE5valueEvE4typeERS7_SA_(%"class.draco::IndexType.116"** nonnull align 4 dereferenceable(4) %__begin_3, %"class.draco::IndexType.116"** nonnull align 4 dereferenceable(4) %__begin_4) #8
  %8 = bitcast %"class.std::__2::vector.139"* %this1 to %"class.std::__2::__vector_base.140"*
  %__end_5 = getelementptr inbounds %"class.std::__2::__vector_base.140", %"class.std::__2::__vector_base.140"* %8, i32 0, i32 1
  %9 = load %"struct.std::__2::__split_buffer.204"*, %"struct.std::__2::__split_buffer.204"** %__v.addr, align 4
  %__end_6 = getelementptr inbounds %"struct.std::__2::__split_buffer.204", %"struct.std::__2::__split_buffer.204"* %9, i32 0, i32 2
  call void @_ZNSt3__24swapIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS7_EE5valueEvE4typeERS7_SA_(%"class.draco::IndexType.116"** nonnull align 4 dereferenceable(4) %__end_5, %"class.draco::IndexType.116"** nonnull align 4 dereferenceable(4) %__end_6) #8
  %10 = bitcast %"class.std::__2::vector.139"* %this1 to %"class.std::__2::__vector_base.140"*
  %call7 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.116"** @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base.140"* %10) #8
  %11 = load %"struct.std::__2::__split_buffer.204"*, %"struct.std::__2::__split_buffer.204"** %__v.addr, align 4
  %call8 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.116"** @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE9__end_capEv(%"struct.std::__2::__split_buffer.204"* %11) #8
  call void @_ZNSt3__24swapIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS7_EE5valueEvE4typeERS7_SA_(%"class.draco::IndexType.116"** nonnull align 4 dereferenceable(4) %call7, %"class.draco::IndexType.116"** nonnull align 4 dereferenceable(4) %call8) #8
  %12 = load %"struct.std::__2::__split_buffer.204"*, %"struct.std::__2::__split_buffer.204"** %__v.addr, align 4
  %__begin_9 = getelementptr inbounds %"struct.std::__2::__split_buffer.204", %"struct.std::__2::__split_buffer.204"* %12, i32 0, i32 1
  %13 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %__begin_9, align 4
  %14 = load %"struct.std::__2::__split_buffer.204"*, %"struct.std::__2::__split_buffer.204"** %__v.addr, align 4
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer.204", %"struct.std::__2::__split_buffer.204"* %14, i32 0, i32 0
  store %"class.draco::IndexType.116"* %13, %"class.draco::IndexType.116"** %__first_, align 4
  %call10 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.139"* %this1) #8
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE14__annotate_newEm(%"class.std::__2::vector.139"* %this1, i32 %call10) #8
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector.139"* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer.204"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEED2Ev(%"struct.std::__2::__split_buffer.204"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"struct.std::__2::__split_buffer.204"*, align 4
  %this.addr = alloca %"struct.std::__2::__split_buffer.204"*, align 4
  store %"struct.std::__2::__split_buffer.204"* %this, %"struct.std::__2::__split_buffer.204"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.204"*, %"struct.std::__2::__split_buffer.204"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer.204"* %this1, %"struct.std::__2::__split_buffer.204"** %retval, align 4
  call void @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE5clearEv(%"struct.std::__2::__split_buffer.204"* %this1) #8
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer.204", %"struct.std::__2::__split_buffer.204"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %__first_, align 4
  %tobool = icmp ne %"class.draco::IndexType.116"* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.144"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE7__allocEv(%"struct.std::__2::__split_buffer.204"* %this1) #8
  %__first_2 = getelementptr inbounds %"struct.std::__2::__split_buffer.204", %"struct.std::__2::__split_buffer.204"* %this1, i32 0, i32 0
  %1 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %__first_2, align 4
  %call3 = call i32 @_ZNKSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE8capacityEv(%"struct.std::__2::__split_buffer.204"* %this1)
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE10deallocateERS6_PS5_m(%"class.std::__2::allocator.144"* nonnull align 1 dereferenceable(1) %call, %"class.draco::IndexType.116"* %1, i32 %call3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"struct.std::__2::__split_buffer.204"*, %"struct.std::__2::__split_buffer.204"** %retval, align 4
  ret %"struct.std::__2::__split_buffer.204"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.116"** @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.141"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.141"*, align 4
  store %"class.std::__2::__compressed_pair.141"* %this, %"class.std::__2::__compressed_pair.141"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.141"*, %"class.std::__2::__compressed_pair.141"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.141"* %this1 to %"struct.std::__2::__compressed_pair_elem.142"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.116"** @_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.142"* %0) #8
  ret %"class.draco::IndexType.116"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.116"** @_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.142"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.142"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.142"* %this, %"struct.std::__2::__compressed_pair_elem.142"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.142"*, %"struct.std::__2::__compressed_pair_elem.142"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.142", %"struct.std::__2::__compressed_pair_elem.142"* %this1, i32 0, i32 0
  ret %"class.draco::IndexType.116"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionC2ERS7_m(%"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"* returned %this, %"class.std::__2::vector.139"* nonnull align 4 dereferenceable(12) %__v, i32 %__n) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"*, align 4
  %__v.addr = alloca %"class.std::__2::vector.139"*, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"** %this.addr, align 4
  store %"class.std::__2::vector.139"* %__v, %"class.std::__2::vector.139"** %__v.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"*, %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"** %this.addr, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::vector.139"*, %"class.std::__2::vector.139"** %__v.addr, align 4
  store %"class.std::__2::vector.139"* %0, %"class.std::__2::vector.139"** %__v_, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %1 = load %"class.std::__2::vector.139"*, %"class.std::__2::vector.139"** %__v.addr, align 4
  %2 = bitcast %"class.std::__2::vector.139"* %1 to %"class.std::__2::__vector_base.140"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.140", %"class.std::__2::__vector_base.140"* %2, i32 0, i32 1
  %3 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %__end_, align 4
  store %"class.draco::IndexType.116"* %3, %"class.draco::IndexType.116"** %__pos_, align 4
  %__new_end_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"* %this1, i32 0, i32 2
  %4 = load %"class.std::__2::vector.139"*, %"class.std::__2::vector.139"** %__v.addr, align 4
  %5 = bitcast %"class.std::__2::vector.139"* %4 to %"class.std::__2::__vector_base.140"*
  %__end_2 = getelementptr inbounds %"class.std::__2::__vector_base.140", %"class.std::__2::__vector_base.140"* %5, i32 0, i32 1
  %6 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %__end_2, align 4
  %7 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds %"class.draco::IndexType.116", %"class.draco::IndexType.116"* %6, i32 %7
  store %"class.draco::IndexType.116"* %add.ptr, %"class.draco::IndexType.116"** %__new_end_, align 4
  ret %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE9constructIS5_JEEEvRS6_PT_DpOT0_(%"class.std::__2::allocator.144"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType.116"* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.144"*, align 4
  %__p.addr = alloca %"class.draco::IndexType.116"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_construct.207", align 1
  store %"class.std::__2::allocator.144"* %__a, %"class.std::__2::allocator.144"** %__a.addr, align 4
  store %"class.draco::IndexType.116"* %__p, %"class.draco::IndexType.116"** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_construct.207"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.144"*, %"class.std::__2::allocator.144"** %__a.addr, align 4
  %2 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE11__constructIS5_JEEEvNS_17integral_constantIbLb1EEERS6_PT_DpOT0_(%"class.std::__2::allocator.144"* nonnull align 1 dereferenceable(1) %1, %"class.draco::IndexType.116"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType.116"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType.116"* %__p) #0 comdat {
entry:
  %__p.addr = alloca %"class.draco::IndexType.116"*, align 4
  store %"class.draco::IndexType.116"* %__p, %"class.draco::IndexType.116"** %__p.addr, align 4
  %0 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %__p.addr, align 4
  ret %"class.draco::IndexType.116"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"*, align 4
  store %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"** %this.addr, align 4
  %this1 = load %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"*, %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %0 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %__pos_, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::vector.139"*, %"class.std::__2::vector.139"** %__v_, align 4
  %2 = bitcast %"class.std::__2::vector.139"* %1 to %"class.std::__2::__vector_base.140"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.140", %"class.std::__2::__vector_base.140"* %2, i32 0, i32 1
  store %"class.draco::IndexType.116"* %0, %"class.draco::IndexType.116"** %__end_, align 4
  ret %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE11__constructIS5_JEEEvNS_17integral_constantIbLb1EEERS6_PT_DpOT0_(%"class.std::__2::allocator.144"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType.116"* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.144"*, align 4
  %__p.addr = alloca %"class.draco::IndexType.116"*, align 4
  store %"class.std::__2::allocator.144"* %__a, %"class.std::__2::allocator.144"** %__a.addr, align 4
  store %"class.draco::IndexType.116"* %__p, %"class.draco::IndexType.116"** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.144"*, %"class.std::__2::allocator.144"** %__a.addr, align 4
  %2 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %__p.addr, align 4
  call void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEE9constructIS4_JEEEvPT_DpOT0_(%"class.std::__2::allocator.144"* %1, %"class.draco::IndexType.116"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEE9constructIS4_JEEEvPT_DpOT0_(%"class.std::__2::allocator.144"* %this, %"class.draco::IndexType.116"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.144"*, align 4
  %__p.addr = alloca %"class.draco::IndexType.116"*, align 4
  store %"class.std::__2::allocator.144"* %this, %"class.std::__2::allocator.144"** %this.addr, align 4
  store %"class.draco::IndexType.116"* %__p, %"class.draco::IndexType.116"** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.144"*, %"class.std::__2::allocator.144"** %this.addr, align 4
  %0 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %__p.addr, align 4
  %1 = bitcast %"class.draco::IndexType.116"* %0 to i8*
  %2 = bitcast i8* %1 to %"class.draco::IndexType.116"*
  %call = call %"class.draco::IndexType.116"* @_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEC2Ev(%"class.draco::IndexType.116"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.144"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.141"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.141"*, align 4
  store %"class.std::__2::__compressed_pair.141"* %this, %"class.std::__2::__compressed_pair.141"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.141"*, %"class.std::__2::__compressed_pair.141"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.141"* %this1 to %"struct.std::__2::__compressed_pair_elem.143"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.144"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.143"* %0) #8
  ret %"class.std::__2::allocator.144"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.144"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.143"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.143"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.143"* %this, %"struct.std::__2::__compressed_pair_elem.143"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.143"*, %"struct.std::__2::__compressed_pair_elem.143"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.143"* %this1 to %"class.std::__2::allocator.144"*
  ret %"class.std::__2::allocator.144"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE8max_sizeEv(%"class.std::__2::vector.139"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.139"*, align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp3 = alloca i32, align 4
  store %"class.std::__2::vector.139"* %this, %"class.std::__2::vector.139"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.139"*, %"class.std::__2::vector.139"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.139"* %this1 to %"class.std::__2::__vector_base.140"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.144"* @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.140"* %0) #8
  %call2 = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE8max_sizeERKS6_(%"class.std::__2::allocator.144"* nonnull align 1 dereferenceable(1) %call) #8
  store i32 %call2, i32* %ref.tmp, align 4
  %call4 = call i32 @_ZNSt3__214numeric_limitsIlE3maxEv() #8
  store i32 %call4, i32* %ref.tmp3, align 4
  %call5 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %1 = load i32, i32* %call5, align 4
  ret i32 %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.139"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.139"*, align 4
  store %"class.std::__2::vector.139"* %this, %"class.std::__2::vector.139"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.139"*, %"class.std::__2::vector.139"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.139"* %this1 to %"class.std::__2::__vector_base.140"*
  %call = call i32 @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::__vector_base.140"* %0) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE8max_sizeERKS6_(%"class.std::__2::allocator.144"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.144"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_max_size.208", align 1
  store %"class.std::__2::allocator.144"* %__a, %"class.std::__2::allocator.144"** %__a.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_max_size.208"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.144"*, %"class.std::__2::allocator.144"** %__a.addr, align 4
  %call = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS6_(%"class.std::__2::allocator.144"* nonnull align 1 dereferenceable(1) %1) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.144"* @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.140"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.140"*, align 4
  store %"class.std::__2::__vector_base.140"* %this, %"class.std::__2::__vector_base.140"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.140"*, %"class.std::__2::__vector_base.140"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.140", %"class.std::__2::__vector_base.140"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.144"* @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.141"* %__end_cap_) #8
  ret %"class.std::__2::allocator.144"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS6_(%"class.std::__2::allocator.144"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.144"*, align 4
  store %"class.std::__2::allocator.144"* %__a, %"class.std::__2::allocator.144"** %__a.addr, align 4
  %1 = load %"class.std::__2::allocator.144"*, %"class.std::__2::allocator.144"** %__a.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEE8max_sizeEv(%"class.std::__2::allocator.144"* %1) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__29allocatorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEE8max_sizeEv(%"class.std::__2::allocator.144"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.144"*, align 4
  store %"class.std::__2::allocator.144"* %this, %"class.std::__2::allocator.144"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.144"*, %"class.std::__2::allocator.144"** %this.addr, align 4
  ret i32 1073741823
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.144"* @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.141"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.141"*, align 4
  store %"class.std::__2::__compressed_pair.141"* %this, %"class.std::__2::__compressed_pair.141"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.141"*, %"class.std::__2::__compressed_pair.141"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.141"* %this1 to %"struct.std::__2::__compressed_pair_elem.143"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.144"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.143"* %0) #8
  ret %"class.std::__2::allocator.144"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.144"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.143"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.143"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.143"* %this, %"struct.std::__2::__compressed_pair_elem.143"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.143"*, %"struct.std::__2::__compressed_pair_elem.143"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.143"* %this1 to %"class.std::__2::allocator.144"*
  ret %"class.std::__2::allocator.144"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::__vector_base.140"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.140"*, align 4
  store %"class.std::__2::__vector_base.140"* %this, %"class.std::__2::__vector_base.140"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.140"*, %"class.std::__2::__vector_base.140"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.116"** @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base.140"* %this1) #8
  %0 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.140", %"class.std::__2::__vector_base.140"* %this1, i32 0, i32 0
  %1 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.draco::IndexType.116"* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.draco::IndexType.116"* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.116"** @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base.140"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.140"*, align 4
  store %"class.std::__2::__vector_base.140"* %this, %"class.std::__2::__vector_base.140"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.140"*, %"class.std::__2::__vector_base.140"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.140", %"class.std::__2::__vector_base.140"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.116"** @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.141"* %__end_cap_) #8
  ret %"class.draco::IndexType.116"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.116"** @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.141"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.141"*, align 4
  store %"class.std::__2::__compressed_pair.141"* %this, %"class.std::__2::__compressed_pair.141"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.141"*, %"class.std::__2::__compressed_pair.141"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.141"* %this1 to %"struct.std::__2::__compressed_pair_elem.142"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.116"** @_ZNKSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.142"* %0) #8
  ret %"class.draco::IndexType.116"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.116"** @_ZNKSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.142"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.142"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.142"* %this, %"struct.std::__2::__compressed_pair_elem.142"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.142"*, %"struct.std::__2::__compressed_pair_elem.142"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.142", %"struct.std::__2::__compressed_pair_elem.142"* %this1, i32 0, i32 0
  ret %"class.draco::IndexType.116"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.205"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEEC2IDnS8_EEOT_OT0_(%"class.std::__2::__compressed_pair.205"* returned %this, i8** nonnull align 4 dereferenceable(4) %__t1, %"class.std::__2::allocator.144"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.205"*, align 4
  %__t1.addr = alloca i8**, align 4
  %__t2.addr = alloca %"class.std::__2::allocator.144"*, align 4
  store %"class.std::__2::__compressed_pair.205"* %this, %"class.std::__2::__compressed_pair.205"** %this.addr, align 4
  store i8** %__t1, i8*** %__t1.addr, align 4
  store %"class.std::__2::allocator.144"* %__t2, %"class.std::__2::allocator.144"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.205"*, %"class.std::__2::__compressed_pair.205"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.205"* %this1 to %"struct.std::__2::__compressed_pair_elem.142"*
  %1 = load i8**, i8*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %1) #8
  %call2 = call %"struct.std::__2::__compressed_pair_elem.142"* @_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.142"* %0, i8** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.205"* %this1 to i8*
  %3 = getelementptr inbounds i8, i8* %2, i32 4
  %4 = bitcast i8* %3 to %"struct.std::__2::__compressed_pair_elem.206"*
  %5 = load %"class.std::__2::allocator.144"*, %"class.std::__2::allocator.144"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.144"* @_ZNSt3__27forwardIRNS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEEEOT_RNS_16remove_referenceIS8_E4typeE(%"class.std::__2::allocator.144"* nonnull align 1 dereferenceable(1) %5) #8
  %call4 = call %"struct.std::__2::__compressed_pair_elem.206"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEELi1ELb0EEC2IS7_vEEOT_(%"struct.std::__2::__compressed_pair_elem.206"* %4, %"class.std::__2::allocator.144"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::__compressed_pair.205"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType.116"* @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE8allocateERS6_m(%"class.std::__2::allocator.144"* nonnull align 1 dereferenceable(1) %__a, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.144"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.144"* %__a, %"class.std::__2::allocator.144"** %__a.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.144"*, %"class.std::__2::allocator.144"** %__a.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call %"class.draco::IndexType.116"* @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEE8allocateEmPKv(%"class.std::__2::allocator.144"* %0, i32 %1, i8* null)
  ret %"class.draco::IndexType.116"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.144"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE7__allocEv(%"struct.std::__2::__split_buffer.204"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.204"*, align 4
  store %"struct.std::__2::__split_buffer.204"* %this, %"struct.std::__2::__split_buffer.204"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.204"*, %"struct.std::__2::__split_buffer.204"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer.204", %"struct.std::__2::__split_buffer.204"* %this1, i32 0, i32 3
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.144"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.205"* %__end_cap_) #8
  ret %"class.std::__2::allocator.144"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.116"** @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE9__end_capEv(%"struct.std::__2::__split_buffer.204"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.204"*, align 4
  store %"struct.std::__2::__split_buffer.204"* %this, %"struct.std::__2::__split_buffer.204"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.204"*, %"struct.std::__2::__split_buffer.204"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer.204", %"struct.std::__2::__split_buffer.204"* %this1, i32 0, i32 3
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.116"** @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.205"* %__end_cap_) #8
  ret %"class.draco::IndexType.116"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.142"* @_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.142"* returned %this, i8** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.142"*, align 4
  %__u.addr = alloca i8**, align 4
  store %"struct.std::__2::__compressed_pair_elem.142"* %this, %"struct.std::__2::__compressed_pair_elem.142"** %this.addr, align 4
  store i8** %__u, i8*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.142"*, %"struct.std::__2::__compressed_pair_elem.142"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.142", %"struct.std::__2::__compressed_pair_elem.142"* %this1, i32 0, i32 0
  %0 = load i8**, i8*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %0) #8
  store %"class.draco::IndexType.116"* null, %"class.draco::IndexType.116"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.142"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.144"* @_ZNSt3__27forwardIRNS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEEEOT_RNS_16remove_referenceIS8_E4typeE(%"class.std::__2::allocator.144"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::allocator.144"*, align 4
  store %"class.std::__2::allocator.144"* %__t, %"class.std::__2::allocator.144"** %__t.addr, align 4
  %0 = load %"class.std::__2::allocator.144"*, %"class.std::__2::allocator.144"** %__t.addr, align 4
  ret %"class.std::__2::allocator.144"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.206"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEELi1ELb0EEC2IS7_vEEOT_(%"struct.std::__2::__compressed_pair_elem.206"* returned %this, %"class.std::__2::allocator.144"* nonnull align 1 dereferenceable(1) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.206"*, align 4
  %__u.addr = alloca %"class.std::__2::allocator.144"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.206"* %this, %"struct.std::__2::__compressed_pair_elem.206"** %this.addr, align 4
  store %"class.std::__2::allocator.144"* %__u, %"class.std::__2::allocator.144"** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.206"*, %"struct.std::__2::__compressed_pair_elem.206"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.206", %"struct.std::__2::__compressed_pair_elem.206"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::allocator.144"*, %"class.std::__2::allocator.144"** %__u.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.144"* @_ZNSt3__27forwardIRNS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEEEOT_RNS_16remove_referenceIS8_E4typeE(%"class.std::__2::allocator.144"* nonnull align 1 dereferenceable(1) %0) #8
  store %"class.std::__2::allocator.144"* %call, %"class.std::__2::allocator.144"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.206"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType.116"* @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEE8allocateEmPKv(%"class.std::__2::allocator.144"* %this, i32 %__n, i8* %0) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.144"*, align 4
  %__n.addr = alloca i32, align 4
  %.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.144"* %this, %"class.std::__2::allocator.144"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::allocator.144"*, %"class.std::__2::allocator.144"** %this.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEE8max_sizeEv(%"class.std::__2::allocator.144"* %this1) #8
  %cmp = icmp ugt i32 %1, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__220__throw_length_errorEPKc(i8* getelementptr inbounds ([68 x i8], [68 x i8]* @.str, i32 0, i32 0)) #11
  unreachable

if.end:                                           ; preds = %entry
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  %call2 = call i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %mul, i32 4)
  %3 = bitcast i8* %call2 to %"class.draco::IndexType.116"*
  ret %"class.draco::IndexType.116"* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.144"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.205"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.205"*, align 4
  store %"class.std::__2::__compressed_pair.205"* %this, %"class.std::__2::__compressed_pair.205"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.205"*, %"class.std::__2::__compressed_pair.205"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.205"* %this1 to i8*
  %add.ptr = getelementptr inbounds i8, i8* %0, i32 4
  %1 = bitcast i8* %add.ptr to %"struct.std::__2::__compressed_pair_elem.206"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.144"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.206"* %1) #8
  ret %"class.std::__2::allocator.144"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.144"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.206"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.206"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.206"* %this, %"struct.std::__2::__compressed_pair_elem.206"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.206"*, %"struct.std::__2::__compressed_pair_elem.206"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.206", %"struct.std::__2::__compressed_pair_elem.206"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::allocator.144"*, %"class.std::__2::allocator.144"** %__value_, align 4
  ret %"class.std::__2::allocator.144"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.116"** @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.205"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.205"*, align 4
  store %"class.std::__2::__compressed_pair.205"* %this, %"class.std::__2::__compressed_pair.205"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.205"*, %"class.std::__2::__compressed_pair.205"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.205"* %this1 to %"struct.std::__2::__compressed_pair_elem.142"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.116"** @_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.142"* %0) #8
  ret %"class.draco::IndexType.116"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE21_ConstructTransactionC2EPPS4_m(%"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction"* returned %this, %"class.draco::IndexType.116"** %__p, i32 %__n) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction"*, align 4
  %__p.addr = alloca %"class.draco::IndexType.116"**, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction"* %this, %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction"** %this.addr, align 4
  store %"class.draco::IndexType.116"** %__p, %"class.draco::IndexType.116"*** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction"*, %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexType.116"**, %"class.draco::IndexType.116"*** %__p.addr, align 4
  %1 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %0, align 4
  store %"class.draco::IndexType.116"* %1, %"class.draco::IndexType.116"** %__pos_, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction"* %this1, i32 0, i32 1
  %2 = load %"class.draco::IndexType.116"**, %"class.draco::IndexType.116"*** %__p.addr, align 4
  %3 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %2, align 4
  %4 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds %"class.draco::IndexType.116", %"class.draco::IndexType.116"* %3, i32 %4
  store %"class.draco::IndexType.116"* %add.ptr, %"class.draco::IndexType.116"** %__end_, align 4
  %__dest_ = getelementptr inbounds %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction"* %this1, i32 0, i32 2
  %5 = load %"class.draco::IndexType.116"**, %"class.draco::IndexType.116"*** %__p.addr, align 4
  store %"class.draco::IndexType.116"** %5, %"class.draco::IndexType.116"*** %__dest_, align 4
  ret %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction"*, align 4
  store %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction"* %this, %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction"*, %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %__pos_, align 4
  %__dest_ = getelementptr inbounds %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction"* %this1, i32 0, i32 2
  %1 = load %"class.draco::IndexType.116"**, %"class.draco::IndexType.116"*** %__dest_, align 4
  store %"class.draco::IndexType.116"* %0, %"class.draco::IndexType.116"** %1, align 4
  ret %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_deleteEv(%"class.std::__2::vector.139"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.139"*, align 4
  store %"class.std::__2::vector.139"* %this, %"class.std::__2::vector.139"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.139"*, %"class.std::__2::vector.139"** %this.addr, align 4
  %call = call %"class.draco::IndexType.116"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.139"* %this1) #8
  %0 = bitcast %"class.draco::IndexType.116"* %call to i8*
  %call2 = call %"class.draco::IndexType.116"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.139"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.139"* %this1) #8
  %add.ptr = getelementptr inbounds %"class.draco::IndexType.116", %"class.draco::IndexType.116"* %call2, i32 %call3
  %1 = bitcast %"class.draco::IndexType.116"* %add.ptr to i8*
  %call4 = call %"class.draco::IndexType.116"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.139"* %this1) #8
  %call5 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.139"* %this1) #8
  %add.ptr6 = getelementptr inbounds %"class.draco::IndexType.116", %"class.draco::IndexType.116"* %call4, i32 %call5
  %2 = bitcast %"class.draco::IndexType.116"* %add.ptr6 to i8*
  %call7 = call %"class.draco::IndexType.116"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.139"* %this1) #8
  %call8 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.139"* %this1) #8
  %add.ptr9 = getelementptr inbounds %"class.draco::IndexType.116", %"class.draco::IndexType.116"* %call7, i32 %call8
  %3 = bitcast %"class.draco::IndexType.116"* %add.ptr9 to i8*
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_(%"class.std::__2::vector.139"* %this1, i8* %0, i8* %1, i8* %2, i8* %3) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE46__construct_backward_with_exception_guaranteesIS5_EENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS6_PT_SC_EE5valuesr31is_trivially_move_constructibleISC_EE5valueEvE4typeERS6_SD_SD_RSD_(%"class.std::__2::allocator.144"* nonnull align 1 dereferenceable(1) %0, %"class.draco::IndexType.116"* %__begin1, %"class.draco::IndexType.116"* %__end1, %"class.draco::IndexType.116"** nonnull align 4 dereferenceable(4) %__end2) #0 comdat {
entry:
  %.addr = alloca %"class.std::__2::allocator.144"*, align 4
  %__begin1.addr = alloca %"class.draco::IndexType.116"*, align 4
  %__end1.addr = alloca %"class.draco::IndexType.116"*, align 4
  %__end2.addr = alloca %"class.draco::IndexType.116"**, align 4
  %_Np = alloca i32, align 4
  store %"class.std::__2::allocator.144"* %0, %"class.std::__2::allocator.144"** %.addr, align 4
  store %"class.draco::IndexType.116"* %__begin1, %"class.draco::IndexType.116"** %__begin1.addr, align 4
  store %"class.draco::IndexType.116"* %__end1, %"class.draco::IndexType.116"** %__end1.addr, align 4
  store %"class.draco::IndexType.116"** %__end2, %"class.draco::IndexType.116"*** %__end2.addr, align 4
  %1 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %__end1.addr, align 4
  %2 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %__begin1.addr, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.draco::IndexType.116"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.draco::IndexType.116"* %2 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  store i32 %sub.ptr.div, i32* %_Np, align 4
  %3 = load i32, i32* %_Np, align 4
  %4 = load %"class.draco::IndexType.116"**, %"class.draco::IndexType.116"*** %__end2.addr, align 4
  %5 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %4, align 4
  %idx.neg = sub i32 0, %3
  %add.ptr = getelementptr inbounds %"class.draco::IndexType.116", %"class.draco::IndexType.116"* %5, i32 %idx.neg
  store %"class.draco::IndexType.116"* %add.ptr, %"class.draco::IndexType.116"** %4, align 4
  %6 = load i32, i32* %_Np, align 4
  %cmp = icmp sgt i32 %6, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %7 = load %"class.draco::IndexType.116"**, %"class.draco::IndexType.116"*** %__end2.addr, align 4
  %8 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %7, align 4
  %9 = bitcast %"class.draco::IndexType.116"* %8 to i8*
  %10 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %__begin1.addr, align 4
  %11 = bitcast %"class.draco::IndexType.116"* %10 to i8*
  %12 = load i32, i32* %_Np, align 4
  %mul = mul i32 %12, 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %11, i32 %mul, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__24swapIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS7_EE5valueEvE4typeERS7_SA_(%"class.draco::IndexType.116"** nonnull align 4 dereferenceable(4) %__x, %"class.draco::IndexType.116"** nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %__x.addr = alloca %"class.draco::IndexType.116"**, align 4
  %__y.addr = alloca %"class.draco::IndexType.116"**, align 4
  %__t = alloca %"class.draco::IndexType.116"*, align 4
  store %"class.draco::IndexType.116"** %__x, %"class.draco::IndexType.116"*** %__x.addr, align 4
  store %"class.draco::IndexType.116"** %__y, %"class.draco::IndexType.116"*** %__y.addr, align 4
  %0 = load %"class.draco::IndexType.116"**, %"class.draco::IndexType.116"*** %__x.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.116"** @_ZNSt3__24moveIRPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEEEONS_16remove_referenceIT_E4typeEOS8_(%"class.draco::IndexType.116"** nonnull align 4 dereferenceable(4) %0) #8
  %1 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %call, align 4
  store %"class.draco::IndexType.116"* %1, %"class.draco::IndexType.116"** %__t, align 4
  %2 = load %"class.draco::IndexType.116"**, %"class.draco::IndexType.116"*** %__y.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.116"** @_ZNSt3__24moveIRPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEEEONS_16remove_referenceIT_E4typeEOS8_(%"class.draco::IndexType.116"** nonnull align 4 dereferenceable(4) %2) #8
  %3 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %call1, align 4
  %4 = load %"class.draco::IndexType.116"**, %"class.draco::IndexType.116"*** %__x.addr, align 4
  store %"class.draco::IndexType.116"* %3, %"class.draco::IndexType.116"** %4, align 4
  %call2 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.116"** @_ZNSt3__24moveIRPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEEEONS_16remove_referenceIT_E4typeEOS8_(%"class.draco::IndexType.116"** nonnull align 4 dereferenceable(4) %__t) #8
  %5 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %call2, align 4
  %6 = load %"class.draco::IndexType.116"**, %"class.draco::IndexType.116"*** %__y.addr, align 4
  store %"class.draco::IndexType.116"* %5, %"class.draco::IndexType.116"** %6, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE14__annotate_newEm(%"class.std::__2::vector.139"* %this, i32 %__current_size) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.139"*, align 4
  %__current_size.addr = alloca i32, align 4
  store %"class.std::__2::vector.139"* %this, %"class.std::__2::vector.139"** %this.addr, align 4
  store i32 %__current_size, i32* %__current_size.addr, align 4
  %this1 = load %"class.std::__2::vector.139"*, %"class.std::__2::vector.139"** %this.addr, align 4
  %call = call %"class.draco::IndexType.116"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.139"* %this1) #8
  %0 = bitcast %"class.draco::IndexType.116"* %call to i8*
  %call2 = call %"class.draco::IndexType.116"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.139"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.139"* %this1) #8
  %add.ptr = getelementptr inbounds %"class.draco::IndexType.116", %"class.draco::IndexType.116"* %call2, i32 %call3
  %1 = bitcast %"class.draco::IndexType.116"* %add.ptr to i8*
  %call4 = call %"class.draco::IndexType.116"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.139"* %this1) #8
  %call5 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.139"* %this1) #8
  %add.ptr6 = getelementptr inbounds %"class.draco::IndexType.116", %"class.draco::IndexType.116"* %call4, i32 %call5
  %2 = bitcast %"class.draco::IndexType.116"* %add.ptr6 to i8*
  %call7 = call %"class.draco::IndexType.116"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.139"* %this1) #8
  %3 = load i32, i32* %__current_size.addr, align 4
  %add.ptr8 = getelementptr inbounds %"class.draco::IndexType.116", %"class.draco::IndexType.116"* %call7, i32 %3
  %4 = bitcast %"class.draco::IndexType.116"* %add.ptr8 to i8*
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_(%"class.std::__2::vector.139"* %this1, i8* %0, i8* %1, i8* %2, i8* %4) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector.139"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.139"*, align 4
  store %"class.std::__2::vector.139"* %this, %"class.std::__2::vector.139"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.139"*, %"class.std::__2::vector.139"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_(%"class.std::__2::vector.139"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.139"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector.139"* %this, %"class.std::__2::vector.139"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector.139"*, %"class.std::__2::vector.139"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType.116"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.139"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.139"*, align 4
  store %"class.std::__2::vector.139"* %this, %"class.std::__2::vector.139"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.139"*, %"class.std::__2::vector.139"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.139"* %this1 to %"class.std::__2::__vector_base.140"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.140", %"class.std::__2::__vector_base.140"* %0, i32 0, i32 0
  %1 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %__begin_, align 4
  %call = call %"class.draco::IndexType.116"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType.116"* %1) #8
  ret %"class.draco::IndexType.116"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.116"** @_ZNSt3__24moveIRPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEEEONS_16remove_referenceIT_E4typeEOS8_(%"class.draco::IndexType.116"** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.draco::IndexType.116"**, align 4
  store %"class.draco::IndexType.116"** %__t, %"class.draco::IndexType.116"*** %__t.addr, align 4
  %0 = load %"class.draco::IndexType.116"**, %"class.draco::IndexType.116"*** %__t.addr, align 4
  ret %"class.draco::IndexType.116"** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE5clearEv(%"struct.std::__2::__split_buffer.204"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.204"*, align 4
  store %"struct.std::__2::__split_buffer.204"* %this, %"struct.std::__2::__split_buffer.204"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.204"*, %"struct.std::__2::__split_buffer.204"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"struct.std::__2::__split_buffer.204", %"struct.std::__2::__split_buffer.204"* %this1, i32 0, i32 1
  %0 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %__begin_, align 4
  call void @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE17__destruct_at_endEPS4_(%"struct.std::__2::__split_buffer.204"* %this1, %"class.draco::IndexType.116"* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE10deallocateERS6_PS5_m(%"class.std::__2::allocator.144"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType.116"* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.144"*, align 4
  %__p.addr = alloca %"class.draco::IndexType.116"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.144"* %__a, %"class.std::__2::allocator.144"** %__a.addr, align 4
  store %"class.draco::IndexType.116"* %__p, %"class.draco::IndexType.116"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.144"*, %"class.std::__2::allocator.144"** %__a.addr, align 4
  %1 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEE10deallocateEPS4_m(%"class.std::__2::allocator.144"* %0, %"class.draco::IndexType.116"* %1, i32 %2) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE8capacityEv(%"struct.std::__2::__split_buffer.204"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.204"*, align 4
  store %"struct.std::__2::__split_buffer.204"* %this, %"struct.std::__2::__split_buffer.204"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.204"*, %"struct.std::__2::__split_buffer.204"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.116"** @_ZNKSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE9__end_capEv(%"struct.std::__2::__split_buffer.204"* %this1) #8
  %0 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %call, align 4
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer.204", %"struct.std::__2::__split_buffer.204"* %this1, i32 0, i32 0
  %1 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %__first_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.draco::IndexType.116"* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.draco::IndexType.116"* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE17__destruct_at_endEPS4_(%"struct.std::__2::__split_buffer.204"* %this, %"class.draco::IndexType.116"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.204"*, align 4
  %__new_last.addr = alloca %"class.draco::IndexType.116"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant.195", align 1
  store %"struct.std::__2::__split_buffer.204"* %this, %"struct.std::__2::__split_buffer.204"** %this.addr, align 4
  store %"class.draco::IndexType.116"* %__new_last, %"class.draco::IndexType.116"** %__new_last.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.204"*, %"struct.std::__2::__split_buffer.204"** %this.addr, align 4
  %0 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %__new_last.addr, align 4
  call void @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE17__destruct_at_endEPS4_NS_17integral_constantIbLb0EEE(%"struct.std::__2::__split_buffer.204"* %this1, %"class.draco::IndexType.116"* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE17__destruct_at_endEPS4_NS_17integral_constantIbLb0EEE(%"struct.std::__2::__split_buffer.204"* %this, %"class.draco::IndexType.116"* %__new_last) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant.195", align 1
  %this.addr = alloca %"struct.std::__2::__split_buffer.204"*, align 4
  %__new_last.addr = alloca %"class.draco::IndexType.116"*, align 4
  store %"struct.std::__2::__split_buffer.204"* %this, %"struct.std::__2::__split_buffer.204"** %this.addr, align 4
  store %"class.draco::IndexType.116"* %__new_last, %"class.draco::IndexType.116"** %__new_last.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.204"*, %"struct.std::__2::__split_buffer.204"** %this.addr, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %__new_last.addr, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer.204", %"struct.std::__2::__split_buffer.204"* %this1, i32 0, i32 2
  %2 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %__end_, align 4
  %cmp = icmp ne %"class.draco::IndexType.116"* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.144"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE7__allocEv(%"struct.std::__2::__split_buffer.204"* %this1) #8
  %__end_2 = getelementptr inbounds %"struct.std::__2::__split_buffer.204", %"struct.std::__2::__split_buffer.204"* %this1, i32 0, i32 2
  %3 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %__end_2, align 4
  %incdec.ptr = getelementptr inbounds %"class.draco::IndexType.116", %"class.draco::IndexType.116"* %3, i32 -1
  store %"class.draco::IndexType.116"* %incdec.ptr, %"class.draco::IndexType.116"** %__end_2, align 4
  %call3 = call %"class.draco::IndexType.116"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType.116"* %incdec.ptr) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE7destroyIS5_EEvRS6_PT_(%"class.std::__2::allocator.144"* nonnull align 1 dereferenceable(1) %call, %"class.draco::IndexType.116"* %call3)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE7destroyIS5_EEvRS6_PT_(%"class.std::__2::allocator.144"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType.116"* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.144"*, align 4
  %__p.addr = alloca %"class.draco::IndexType.116"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy.209", align 1
  store %"class.std::__2::allocator.144"* %__a, %"class.std::__2::allocator.144"** %__a.addr, align 4
  store %"class.draco::IndexType.116"* %__p, %"class.draco::IndexType.116"** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy.209"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.144"*, %"class.std::__2::allocator.144"** %__a.addr, align 4
  %2 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE9__destroyIS5_EEvNS_17integral_constantIbLb1EEERS6_PT_(%"class.std::__2::allocator.144"* nonnull align 1 dereferenceable(1) %1, %"class.draco::IndexType.116"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE9__destroyIS5_EEvNS_17integral_constantIbLb1EEERS6_PT_(%"class.std::__2::allocator.144"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType.116"* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.144"*, align 4
  %__p.addr = alloca %"class.draco::IndexType.116"*, align 4
  store %"class.std::__2::allocator.144"* %__a, %"class.std::__2::allocator.144"** %__a.addr, align 4
  store %"class.draco::IndexType.116"* %__p, %"class.draco::IndexType.116"** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.144"*, %"class.std::__2::allocator.144"** %__a.addr, align 4
  %2 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %__p.addr, align 4
  call void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEE7destroyEPS4_(%"class.std::__2::allocator.144"* %1, %"class.draco::IndexType.116"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEE7destroyEPS4_(%"class.std::__2::allocator.144"* %this, %"class.draco::IndexType.116"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.144"*, align 4
  %__p.addr = alloca %"class.draco::IndexType.116"*, align 4
  store %"class.std::__2::allocator.144"* %this, %"class.std::__2::allocator.144"** %this.addr, align 4
  store %"class.draco::IndexType.116"* %__p, %"class.draco::IndexType.116"** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.144"*, %"class.std::__2::allocator.144"** %this.addr, align 4
  %0 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %__p.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEE10deallocateEPS4_m(%"class.std::__2::allocator.144"* %this, %"class.draco::IndexType.116"* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.144"*, align 4
  %__p.addr = alloca %"class.draco::IndexType.116"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.144"* %this, %"class.std::__2::allocator.144"** %this.addr, align 4
  store %"class.draco::IndexType.116"* %__p, %"class.draco::IndexType.116"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.144"*, %"class.std::__2::allocator.144"** %this.addr, align 4
  %0 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %__p.addr, align 4
  %1 = bitcast %"class.draco::IndexType.116"* %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.116"** @_ZNKSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE9__end_capEv(%"struct.std::__2::__split_buffer.204"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.204"*, align 4
  store %"struct.std::__2::__split_buffer.204"* %this, %"struct.std::__2::__split_buffer.204"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.204"*, %"struct.std::__2::__split_buffer.204"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer.204", %"struct.std::__2::__split_buffer.204"* %this1, i32 0, i32 3
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.116"** @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.205"* %__end_cap_) #8
  ret %"class.draco::IndexType.116"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.116"** @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.205"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.205"*, align 4
  store %"class.std::__2::__compressed_pair.205"* %this, %"class.std::__2::__compressed_pair.205"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.205"*, %"class.std::__2::__compressed_pair.205"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.205"* %this1 to %"struct.std::__2::__compressed_pair_elem.142"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.116"** @_ZNKSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.142"* %0) #8
  ret %"class.draco::IndexType.116"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE27__invalidate_iterators_pastEPS4_(%"class.std::__2::vector.139"* %this, %"class.draco::IndexType.116"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.139"*, align 4
  %__new_last.addr = alloca %"class.draco::IndexType.116"*, align 4
  store %"class.std::__2::vector.139"* %this, %"class.std::__2::vector.139"** %this.addr, align 4
  store %"class.draco::IndexType.116"* %__new_last, %"class.draco::IndexType.116"** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::vector.139"*, %"class.std::__2::vector.139"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_(%"class.std::__2::__vector_base.140"* %this, %"class.draco::IndexType.116"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.140"*, align 4
  %__new_last.addr = alloca %"class.draco::IndexType.116"*, align 4
  %__soon_to_be_end = alloca %"class.draco::IndexType.116"*, align 4
  store %"class.std::__2::__vector_base.140"* %this, %"class.std::__2::__vector_base.140"** %this.addr, align 4
  store %"class.draco::IndexType.116"* %__new_last, %"class.draco::IndexType.116"** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.140"*, %"class.std::__2::__vector_base.140"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.140", %"class.std::__2::__vector_base.140"* %this1, i32 0, i32 1
  %0 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %__end_, align 4
  store %"class.draco::IndexType.116"* %0, %"class.draco::IndexType.116"** %__soon_to_be_end, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %__new_last.addr, align 4
  %2 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %__soon_to_be_end, align 4
  %cmp = icmp ne %"class.draco::IndexType.116"* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.144"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.140"* %this1) #8
  %3 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %__soon_to_be_end, align 4
  %incdec.ptr = getelementptr inbounds %"class.draco::IndexType.116", %"class.draco::IndexType.116"* %3, i32 -1
  store %"class.draco::IndexType.116"* %incdec.ptr, %"class.draco::IndexType.116"** %__soon_to_be_end, align 4
  %call2 = call %"class.draco::IndexType.116"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType.116"* %incdec.ptr) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE7destroyIS5_EEvRS6_PT_(%"class.std::__2::allocator.144"* nonnull align 1 dereferenceable(1) %call, %"class.draco::IndexType.116"* %call2)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %4 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %__new_last.addr, align 4
  %__end_3 = getelementptr inbounds %"class.std::__2::__vector_base.140", %"class.std::__2::__vector_base.140"* %this1, i32 0, i32 1
  store %"class.draco::IndexType.116"* %4, %"class.draco::IndexType.116"** %__end_3, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_shrinkEm(%"class.std::__2::vector.139"* %this, i32 %__old_size) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.139"*, align 4
  %__old_size.addr = alloca i32, align 4
  store %"class.std::__2::vector.139"* %this, %"class.std::__2::vector.139"** %this.addr, align 4
  store i32 %__old_size, i32* %__old_size.addr, align 4
  %this1 = load %"class.std::__2::vector.139"*, %"class.std::__2::vector.139"** %this.addr, align 4
  %call = call %"class.draco::IndexType.116"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.139"* %this1) #8
  %0 = bitcast %"class.draco::IndexType.116"* %call to i8*
  %call2 = call %"class.draco::IndexType.116"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.139"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.139"* %this1) #8
  %add.ptr = getelementptr inbounds %"class.draco::IndexType.116", %"class.draco::IndexType.116"* %call2, i32 %call3
  %1 = bitcast %"class.draco::IndexType.116"* %add.ptr to i8*
  %call4 = call %"class.draco::IndexType.116"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.139"* %this1) #8
  %2 = load i32, i32* %__old_size.addr, align 4
  %add.ptr5 = getelementptr inbounds %"class.draco::IndexType.116", %"class.draco::IndexType.116"* %call4, i32 %2
  %3 = bitcast %"class.draco::IndexType.116"* %add.ptr5 to i8*
  %call6 = call %"class.draco::IndexType.116"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.139"* %this1) #8
  %call7 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.139"* %this1) #8
  %add.ptr8 = getelementptr inbounds %"class.draco::IndexType.116", %"class.draco::IndexType.116"* %call6, i32 %call7
  %4 = bitcast %"class.draco::IndexType.116"* %add.ptr8 to i8*
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_(%"class.std::__2::vector.139"* %this1, i8* %0, i8* %1, i8* %3, i8* %4) #8
  ret void
}

; Function Attrs: noreturn
declare void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_out_of_rangeEv(%"class.std::__2::__vector_base_common"*) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::MeshDecoder"* @_ZN5draco11MeshDecoderD2Ev(%"class.draco::MeshDecoder"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::MeshDecoder"*, align 4
  store %"class.draco::MeshDecoder"* %this, %"class.draco::MeshDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::MeshDecoder"*, %"class.draco::MeshDecoder"** %this.addr, align 4
  %0 = bitcast %"class.draco::MeshDecoder"* %this1 to %"class.draco::PointCloudDecoder"*
  %call = call %"class.draco::PointCloudDecoder"* @_ZN5draco17PointCloudDecoderD2Ev(%"class.draco::PointCloudDecoder"* %0) #8
  ret %"class.draco::MeshDecoder"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::PointCloudDecoder"* @_ZN5draco17PointCloudDecoderD2Ev(%"class.draco::PointCloudDecoder"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointCloudDecoder"*, align 4
  store %"class.draco::PointCloudDecoder"* %this, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::PointCloudDecoder"*, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  %0 = bitcast %"class.draco::PointCloudDecoder"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [11 x i8*] }, { [11 x i8*] }* @_ZTVN5draco17PointCloudDecoderE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %attribute_to_decoder_map_ = getelementptr inbounds %"class.draco::PointCloudDecoder", %"class.draco::PointCloudDecoder"* %this1, i32 0, i32 3
  %call = call %"class.std::__2::vector.87"* @_ZNSt3__26vectorIiNS_9allocatorIiEEED2Ev(%"class.std::__2::vector.87"* %attribute_to_decoder_map_) #8
  %attributes_decoders_ = getelementptr inbounds %"class.draco::PointCloudDecoder", %"class.draco::PointCloudDecoder"* %this1, i32 0, i32 2
  %call2 = call %"class.std::__2::vector.94"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEED2Ev(%"class.std::__2::vector.94"* %attributes_decoders_) #8
  ret %"class.draco::PointCloudDecoder"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector.87"* @_ZNSt3__26vectorIiNS_9allocatorIiEEED2Ev(%"class.std::__2::vector.87"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  call void @_ZNKSt3__26vectorIiNS_9allocatorIiEEE17__annotate_deleteEv(%"class.std::__2::vector.87"* %this1) #8
  %0 = bitcast %"class.std::__2::vector.87"* %this1 to %"class.std::__2::__vector_base.88"*
  %call = call %"class.std::__2::__vector_base.88"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEED2Ev(%"class.std::__2::__vector_base.88"* %0) #8
  ret %"class.std::__2::vector.87"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector.94"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEED2Ev(%"class.std::__2::vector.94"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  call void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__annotate_deleteEv(%"class.std::__2::vector.94"* %this1) #8
  %0 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %call = call %"class.std::__2::__vector_base.95"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEED2Ev(%"class.std::__2::__vector_base.95"* %0) #8
  ret %"class.std::__2::vector.94"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIiNS_9allocatorIiEEE17__annotate_deleteEv(%"class.std::__2::vector.87"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  %call = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector.87"* %this1) #8
  %0 = bitcast i32* %call to i8*
  %call2 = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector.87"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::vector.87"* %this1) #8
  %add.ptr = getelementptr inbounds i32, i32* %call2, i32 %call3
  %1 = bitcast i32* %add.ptr to i8*
  %call4 = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector.87"* %this1) #8
  %call5 = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4sizeEv(%"class.std::__2::vector.87"* %this1) #8
  %add.ptr6 = getelementptr inbounds i32, i32* %call4, i32 %call5
  %2 = bitcast i32* %add.ptr6 to i8*
  %call7 = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector.87"* %this1) #8
  %call8 = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::vector.87"* %this1) #8
  %add.ptr9 = getelementptr inbounds i32, i32* %call7, i32 %call8
  %3 = bitcast i32* %add.ptr9 to i8*
  call void @_ZNKSt3__26vectorIiNS_9allocatorIiEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector.87"* %this1, i8* %0, i8* %1, i8* %2, i8* %3) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base.88"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEED2Ev(%"class.std::__2::__vector_base.88"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__vector_base.88"*, align 4
  %this.addr = alloca %"class.std::__2::__vector_base.88"*, align 4
  store %"class.std::__2::__vector_base.88"* %this, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.88"*, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  store %"class.std::__2::__vector_base.88"* %this1, %"class.std::__2::__vector_base.88"** %retval, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__begin_, align 4
  %cmp = icmp ne i32* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE5clearEv(%"class.std::__2::__vector_base.88"* %this1) #8
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE7__allocEv(%"class.std::__2::__vector_base.88"* %this1) #8
  %__begin_2 = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %this1, i32 0, i32 0
  %1 = load i32*, i32** %__begin_2, align 4
  %call3 = call i32 @_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::__vector_base.88"* %this1) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE10deallocateERS2_Pim(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %call, i32* %1, i32 %call3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"class.std::__2::__vector_base.88"*, %"class.std::__2::__vector_base.88"** %retval, align 4
  ret %"class.std::__2::__vector_base.88"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIiNS_9allocatorIiEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector.87"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector.87"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.87"* %this1 to %"class.std::__2::__vector_base.88"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %0, i32 0, i32 0
  %1 = load i32*, i32** %__begin_, align 4
  %call = call i32* @_ZNSt3__212__to_addressIiEEPT_S2_(i32* %1) #8
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::vector.87"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.87"* %this1 to %"class.std::__2::__vector_base.88"*
  %call = call i32 @_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::__vector_base.88"* %0) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4sizeEv(%"class.std::__2::vector.87"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.87"* %this1 to %"class.std::__2::__vector_base.88"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %0, i32 0, i32 1
  %1 = load i32*, i32** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector.87"* %this1 to %"class.std::__2::__vector_base.88"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %2, i32 0, i32 0
  %3 = load i32*, i32** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint i32* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint i32* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNSt3__212__to_addressIiEEPT_S2_(i32* %__p) #0 comdat {
entry:
  %__p.addr = alloca i32*, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %0 = load i32*, i32** %__p.addr, align 4
  ret i32* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::__vector_base.88"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.88"*, align 4
  store %"class.std::__2::__vector_base.88"* %this, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.88"*, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE9__end_capEv(%"class.std::__2::__vector_base.88"* %this1) #8
  %0 = load i32*, i32** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %this1, i32 0, i32 0
  %1 = load i32*, i32** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint i32* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint i32* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE9__end_capEv(%"class.std::__2::__vector_base.88"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.88"*, align 4
  store %"class.std::__2::__vector_base.88"* %this, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.88"*, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__217__compressed_pairIPiNS_9allocatorIiEEE5firstEv(%"class.std::__2::__compressed_pair.89"* %__end_cap_) #8
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__217__compressed_pairIPiNS_9allocatorIiEEE5firstEv(%"class.std::__2::__compressed_pair.89"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.89"*, align 4
  store %"class.std::__2::__compressed_pair.89"* %this, %"class.std::__2::__compressed_pair.89"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.89"*, %"class.std::__2::__compressed_pair.89"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.89"* %this1 to %"struct.std::__2::__compressed_pair_elem.90"*
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__222__compressed_pair_elemIPiLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.90"* %0) #8
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__222__compressed_pair_elemIPiLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.90"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.90"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.90"* %this, %"struct.std::__2::__compressed_pair_elem.90"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.90"*, %"struct.std::__2::__compressed_pair_elem.90"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.90", %"struct.std::__2::__compressed_pair_elem.90"* %this1, i32 0, i32 0
  ret i32** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE5clearEv(%"class.std::__2::__vector_base.88"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.88"*, align 4
  store %"class.std::__2::__vector_base.88"* %this, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.88"*, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__begin_, align 4
  call void @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE17__destruct_at_endEPi(%"class.std::__2::__vector_base.88"* %this1, i32* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE10deallocateERS2_Pim(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %__a, i32* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.92"*, align 4
  %__p.addr = alloca i32*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.92"* %__a, %"class.std::__2::allocator.92"** %__a.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.92"*, %"class.std::__2::allocator.92"** %__a.addr, align 4
  %1 = load i32*, i32** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorIiE10deallocateEPim(%"class.std::__2::allocator.92"* %0, i32* %1, i32 %2) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE7__allocEv(%"class.std::__2::__vector_base.88"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.88"*, align 4
  store %"class.std::__2::__vector_base.88"* %this, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.88"*, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__217__compressed_pairIPiNS_9allocatorIiEEE6secondEv(%"class.std::__2::__compressed_pair.89"* %__end_cap_) #8
  ret %"class.std::__2::allocator.92"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE17__destruct_at_endEPi(%"class.std::__2::__vector_base.88"* %this, i32* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.88"*, align 4
  %__new_last.addr = alloca i32*, align 4
  %__soon_to_be_end = alloca i32*, align 4
  store %"class.std::__2::__vector_base.88"* %this, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  store i32* %__new_last, i32** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.88"*, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %this1, i32 0, i32 1
  %0 = load i32*, i32** %__end_, align 4
  store i32* %0, i32** %__soon_to_be_end, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load i32*, i32** %__new_last.addr, align 4
  %2 = load i32*, i32** %__soon_to_be_end, align 4
  %cmp = icmp ne i32* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE7__allocEv(%"class.std::__2::__vector_base.88"* %this1) #8
  %3 = load i32*, i32** %__soon_to_be_end, align 4
  %incdec.ptr = getelementptr inbounds i32, i32* %3, i32 -1
  store i32* %incdec.ptr, i32** %__soon_to_be_end, align 4
  %call2 = call i32* @_ZNSt3__212__to_addressIiEEPT_S2_(i32* %incdec.ptr) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE7destroyIiEEvRS2_PT_(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %call, i32* %call2)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %4 = load i32*, i32** %__new_last.addr, align 4
  %__end_3 = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %this1, i32 0, i32 1
  store i32* %4, i32** %__end_3, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE7destroyIiEEvRS2_PT_(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %__a, i32* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.92"*, align 4
  %__p.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy.210", align 1
  store %"class.std::__2::allocator.92"* %__a, %"class.std::__2::allocator.92"** %__a.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy.210"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.92"*, %"class.std::__2::allocator.92"** %__a.addr, align 4
  %2 = load i32*, i32** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE9__destroyIiEEvNS_17integral_constantIbLb1EEERS2_PT_(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %1, i32* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE9__destroyIiEEvNS_17integral_constantIbLb1EEERS2_PT_(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %__a, i32* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.92"*, align 4
  %__p.addr = alloca i32*, align 4
  store %"class.std::__2::allocator.92"* %__a, %"class.std::__2::allocator.92"** %__a.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.92"*, %"class.std::__2::allocator.92"** %__a.addr, align 4
  %2 = load i32*, i32** %__p.addr, align 4
  call void @_ZNSt3__29allocatorIiE7destroyEPi(%"class.std::__2::allocator.92"* %1, i32* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIiE7destroyEPi(%"class.std::__2::allocator.92"* %this, i32* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.92"*, align 4
  %__p.addr = alloca i32*, align 4
  store %"class.std::__2::allocator.92"* %this, %"class.std::__2::allocator.92"** %this.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.92"*, %"class.std::__2::allocator.92"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIiE10deallocateEPim(%"class.std::__2::allocator.92"* %this, i32* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.92"*, align 4
  %__p.addr = alloca i32*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.92"* %this, %"class.std::__2::allocator.92"** %this.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.92"*, %"class.std::__2::allocator.92"** %this.addr, align 4
  %0 = load i32*, i32** %__p.addr, align 4
  %1 = bitcast i32* %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__217__compressed_pairIPiNS_9allocatorIiEEE6secondEv(%"class.std::__2::__compressed_pair.89"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.89"*, align 4
  store %"class.std::__2::__compressed_pair.89"* %this, %"class.std::__2::__compressed_pair.89"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.89"*, %"class.std::__2::__compressed_pair.89"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.89"* %this1 to %"struct.std::__2::__compressed_pair_elem.91"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIiEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.91"* %0) #8
  ret %"class.std::__2::allocator.92"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIiEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.91"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.91"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.91"* %this, %"struct.std::__2::__compressed_pair_elem.91"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.91"*, %"struct.std::__2::__compressed_pair_elem.91"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.91"* %this1 to %"class.std::__2::allocator.92"*
  ret %"class.std::__2::allocator.92"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base.95"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEED2Ev(%"class.std::__2::__vector_base.95"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__vector_base.95"*, align 4
  %this.addr = alloca %"class.std::__2::__vector_base.95"*, align 4
  store %"class.std::__2::__vector_base.95"* %this, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.95"*, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  store %"class.std::__2::__vector_base.95"* %this1, %"class.std::__2::__vector_base.95"** %retval, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__begin_, align 4
  %cmp = icmp ne %"class.std::__2::unique_ptr.96"* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5clearEv(%"class.std::__2::__vector_base.95"* %this1) #8
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base.95"* %this1) #8
  %__begin_2 = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__begin_2, align 4
  %call3 = call i32 @_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::__vector_base.95"* %this1) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE10deallocateERS8_PS7_m(%"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %call, %"class.std::__2::unique_ptr.96"* %1, i32 %call3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"class.std::__2::__vector_base.95"*, %"class.std::__2::__vector_base.95"** %retval, align 4
  ret %"class.std::__2::__vector_base.95"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5clearEv(%"class.std::__2::__vector_base.95"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.95"*, align 4
  store %"class.std::__2::__vector_base.95"* %this, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.95"*, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__begin_, align 4
  call void @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__destruct_at_endEPS6_(%"class.std::__2::__vector_base.95"* %this1, %"class.std::__2::unique_ptr.96"* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco13DecoderBuffer4PeekIjEEbPT_(%"class.draco::DecoderBuffer"* %this, i32* %out_val) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %out_val.addr = alloca i32*, align 4
  %size_to_decode = alloca i32, align 4
  store %"class.draco::DecoderBuffer"* %this, %"class.draco::DecoderBuffer"** %this.addr, align 4
  store i32* %out_val, i32** %out_val.addr, align 4
  %this1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %this.addr, align 4
  store i32 4, i32* %size_to_decode, align 4
  %data_size_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 1
  %0 = load i64, i64* %data_size_, align 8
  %pos_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %1 = load i64, i64* %pos_, align 8
  %add = add nsw i64 %1, 4
  %cmp = icmp slt i64 %0, %add
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %2 = load i32*, i32** %out_val.addr, align 4
  %3 = bitcast i32* %2 to i8*
  %data_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 0
  %4 = load i8*, i8** %data_, align 8
  %pos_2 = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %5 = load i64, i64* %pos_2, align 8
  %idx.ext = trunc i64 %5 to i32
  %add.ptr = getelementptr inbounds i8, i8* %4, i32 %idx.ext
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 1 %add.ptr, i32 4, i1 false)
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %6 = load i1, i1* %retval, align 1
  ret i1 %6
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco13DecoderBuffer4PeekIhEEbPT_(%"class.draco::DecoderBuffer"* %this, i8* %out_val) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %out_val.addr = alloca i8*, align 4
  %size_to_decode = alloca i32, align 4
  store %"class.draco::DecoderBuffer"* %this, %"class.draco::DecoderBuffer"** %this.addr, align 4
  store i8* %out_val, i8** %out_val.addr, align 4
  %this1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %this.addr, align 4
  store i32 1, i32* %size_to_decode, align 4
  %data_size_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 1
  %0 = load i64, i64* %data_size_, align 8
  %pos_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %1 = load i64, i64* %pos_, align 8
  %add = add nsw i64 %1, 1
  %cmp = icmp slt i64 %0, %add
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %2 = load i8*, i8** %out_val.addr, align 4
  %data_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 0
  %3 = load i8*, i8** %data_, align 8
  %pos_2 = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %4 = load i64, i64* %pos_2, align 8
  %idx.ext = trunc i64 %4 to i32
  %add.ptr = getelementptr inbounds i8, i8* %3, i32 %idx.ext
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %2, i8* align 1 %add.ptr, i32 1, i1 false)
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %5 = load i1, i1* %retval, align 1
  ret i1 %5
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco13DecoderBuffer4PeekItEEbPT_(%"class.draco::DecoderBuffer"* %this, i16* %out_val) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %out_val.addr = alloca i16*, align 4
  %size_to_decode = alloca i32, align 4
  store %"class.draco::DecoderBuffer"* %this, %"class.draco::DecoderBuffer"** %this.addr, align 4
  store i16* %out_val, i16** %out_val.addr, align 4
  %this1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %this.addr, align 4
  store i32 2, i32* %size_to_decode, align 4
  %data_size_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 1
  %0 = load i64, i64* %data_size_, align 8
  %pos_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %1 = load i64, i64* %pos_, align 8
  %add = add nsw i64 %1, 2
  %cmp = icmp slt i64 %0, %add
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %2 = load i16*, i16** %out_val.addr, align 4
  %3 = bitcast i16* %2 to i8*
  %data_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 0
  %4 = load i8*, i8** %data_, align 8
  %pos_2 = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %5 = load i64, i64* %pos_2, align 8
  %idx.ext = trunc i64 %5 to i32
  %add.ptr = getelementptr inbounds i8, i8* %4, i32 %idx.ext
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %3, i8* align 1 %add.ptr, i32 2, i1 false)
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %6 = load i1, i1* %retval, align 1
  ret i1 %6
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.147"* @_ZNSt3__217__compressed_pairIPN5draco15PointsSequencerENS_14default_deleteIS2_EEEC2IRS3_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.147"* returned %this, %"class.draco::PointsSequencer"** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.147"*, align 4
  %__t1.addr = alloca %"class.draco::PointsSequencer"**, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.147"* %this, %"class.std::__2::__compressed_pair.147"** %this.addr, align 4
  store %"class.draco::PointsSequencer"** %__t1, %"class.draco::PointsSequencer"*** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.147"*, %"class.std::__2::__compressed_pair.147"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.147"* %this1 to %"struct.std::__2::__compressed_pair_elem.148"*
  %1 = load %"class.draco::PointsSequencer"**, %"class.draco::PointsSequencer"*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointsSequencer"** @_ZNSt3__27forwardIRPN5draco15PointsSequencerEEEOT_RNS_16remove_referenceIS5_E4typeE(%"class.draco::PointsSequencer"** nonnull align 4 dereferenceable(4) %1) #8
  %call2 = call %"struct.std::__2::__compressed_pair_elem.148"* @_ZNSt3__222__compressed_pair_elemIPN5draco15PointsSequencerELi0ELb0EEC2IRS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.148"* %0, %"class.draco::PointsSequencer"** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.147"* %this1 to %"struct.std::__2::__compressed_pair_elem.149"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #8
  %call4 = call %"struct.std::__2::__compressed_pair_elem.149"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco15PointsSequencerEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.149"* %2)
  ret %"class.std::__2::__compressed_pair.147"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::PointsSequencer"** @_ZNSt3__27forwardIRPN5draco15PointsSequencerEEEOT_RNS_16remove_referenceIS5_E4typeE(%"class.draco::PointsSequencer"** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.draco::PointsSequencer"**, align 4
  store %"class.draco::PointsSequencer"** %__t, %"class.draco::PointsSequencer"*** %__t.addr, align 4
  %0 = load %"class.draco::PointsSequencer"**, %"class.draco::PointsSequencer"*** %__t.addr, align 4
  ret %"class.draco::PointsSequencer"** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.148"* @_ZNSt3__222__compressed_pair_elemIPN5draco15PointsSequencerELi0ELb0EEC2IRS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.148"* returned %this, %"class.draco::PointsSequencer"** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.148"*, align 4
  %__u.addr = alloca %"class.draco::PointsSequencer"**, align 4
  store %"struct.std::__2::__compressed_pair_elem.148"* %this, %"struct.std::__2::__compressed_pair_elem.148"** %this.addr, align 4
  store %"class.draco::PointsSequencer"** %__u, %"class.draco::PointsSequencer"*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.148"*, %"struct.std::__2::__compressed_pair_elem.148"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.148", %"struct.std::__2::__compressed_pair_elem.148"* %this1, i32 0, i32 0
  %0 = load %"class.draco::PointsSequencer"**, %"class.draco::PointsSequencer"*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointsSequencer"** @_ZNSt3__27forwardIRPN5draco15PointsSequencerEEEOT_RNS_16remove_referenceIS5_E4typeE(%"class.draco::PointsSequencer"** nonnull align 4 dereferenceable(4) %0) #8
  %1 = load %"class.draco::PointsSequencer"*, %"class.draco::PointsSequencer"** %call, align 4
  store %"class.draco::PointsSequencer"* %1, %"class.draco::PointsSequencer"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.148"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.149"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco15PointsSequencerEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.149"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.149"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.149"* %this, %"struct.std::__2::__compressed_pair_elem.149"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.149"*, %"struct.std::__2::__compressed_pair_elem.149"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.149"* %this1 to %"struct.std::__2::default_delete.150"*
  ret %"struct.std::__2::__compressed_pair_elem.149"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__210unique_ptrIN5draco15PointsSequencerENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.146"* %this, %"class.draco::PointsSequencer"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.146"*, align 4
  %__p.addr = alloca %"class.draco::PointsSequencer"*, align 4
  %__tmp = alloca %"class.draco::PointsSequencer"*, align 4
  store %"class.std::__2::unique_ptr.146"* %this, %"class.std::__2::unique_ptr.146"** %this.addr, align 4
  store %"class.draco::PointsSequencer"* %__p, %"class.draco::PointsSequencer"** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.146"*, %"class.std::__2::unique_ptr.146"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.146", %"class.std::__2::unique_ptr.146"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointsSequencer"** @_ZNSt3__217__compressed_pairIPN5draco15PointsSequencerENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.147"* %__ptr_) #8
  %0 = load %"class.draco::PointsSequencer"*, %"class.draco::PointsSequencer"** %call, align 4
  store %"class.draco::PointsSequencer"* %0, %"class.draco::PointsSequencer"** %__tmp, align 4
  %1 = load %"class.draco::PointsSequencer"*, %"class.draco::PointsSequencer"** %__p.addr, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr.146", %"class.std::__2::unique_ptr.146"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::PointsSequencer"** @_ZNSt3__217__compressed_pairIPN5draco15PointsSequencerENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.147"* %__ptr_2) #8
  store %"class.draco::PointsSequencer"* %1, %"class.draco::PointsSequencer"** %call3, align 4
  %2 = load %"class.draco::PointsSequencer"*, %"class.draco::PointsSequencer"** %__tmp, align 4
  %tobool = icmp ne %"class.draco::PointsSequencer"* %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__ptr_4 = getelementptr inbounds %"class.std::__2::unique_ptr.146", %"class.std::__2::unique_ptr.146"* %this1, i32 0, i32 0
  %call5 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.150"* @_ZNSt3__217__compressed_pairIPN5draco15PointsSequencerENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.147"* %__ptr_4) #8
  %3 = load %"class.draco::PointsSequencer"*, %"class.draco::PointsSequencer"** %__tmp, align 4
  call void @_ZNKSt3__214default_deleteIN5draco15PointsSequencerEEclEPS2_(%"struct.std::__2::default_delete.150"* %call5, %"class.draco::PointsSequencer"* %3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::PointsSequencer"** @_ZNSt3__217__compressed_pairIPN5draco15PointsSequencerENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.147"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.147"*, align 4
  store %"class.std::__2::__compressed_pair.147"* %this, %"class.std::__2::__compressed_pair.147"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.147"*, %"class.std::__2::__compressed_pair.147"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.147"* %this1 to %"struct.std::__2::__compressed_pair_elem.148"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointsSequencer"** @_ZNSt3__222__compressed_pair_elemIPN5draco15PointsSequencerELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.148"* %0) #8
  ret %"class.draco::PointsSequencer"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.150"* @_ZNSt3__217__compressed_pairIPN5draco15PointsSequencerENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.147"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.147"*, align 4
  store %"class.std::__2::__compressed_pair.147"* %this, %"class.std::__2::__compressed_pair.147"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.147"*, %"class.std::__2::__compressed_pair.147"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.147"* %this1 to %"struct.std::__2::__compressed_pair_elem.149"*
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.150"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco15PointsSequencerEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.149"* %0) #8
  ret %"struct.std::__2::default_delete.150"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__214default_deleteIN5draco15PointsSequencerEEclEPS2_(%"struct.std::__2::default_delete.150"* %this, %"class.draco::PointsSequencer"* %__ptr) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::default_delete.150"*, align 4
  %__ptr.addr = alloca %"class.draco::PointsSequencer"*, align 4
  store %"struct.std::__2::default_delete.150"* %this, %"struct.std::__2::default_delete.150"** %this.addr, align 4
  store %"class.draco::PointsSequencer"* %__ptr, %"class.draco::PointsSequencer"** %__ptr.addr, align 4
  %this1 = load %"struct.std::__2::default_delete.150"*, %"struct.std::__2::default_delete.150"** %this.addr, align 4
  %0 = load %"class.draco::PointsSequencer"*, %"class.draco::PointsSequencer"** %__ptr.addr, align 4
  %isnull = icmp eq %"class.draco::PointsSequencer"* %0, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %entry
  %1 = bitcast %"class.draco::PointsSequencer"* %0 to void (%"class.draco::PointsSequencer"*)***
  %vtable = load void (%"class.draco::PointsSequencer"*)**, void (%"class.draco::PointsSequencer"*)*** %1, align 4
  %vfn = getelementptr inbounds void (%"class.draco::PointsSequencer"*)*, void (%"class.draco::PointsSequencer"*)** %vtable, i64 1
  %2 = load void (%"class.draco::PointsSequencer"*)*, void (%"class.draco::PointsSequencer"*)** %vfn, align 4
  call void %2(%"class.draco::PointsSequencer"* %0) #8
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::PointsSequencer"** @_ZNSt3__222__compressed_pair_elemIPN5draco15PointsSequencerELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.148"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.148"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.148"* %this, %"struct.std::__2::__compressed_pair_elem.148"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.148"*, %"struct.std::__2::__compressed_pair_elem.148"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.148", %"struct.std::__2::__compressed_pair_elem.148"* %this1, i32 0, i32 0
  ret %"class.draco::PointsSequencer"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.150"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco15PointsSequencerEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.149"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.149"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.149"* %this, %"struct.std::__2::__compressed_pair_elem.149"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.149"*, %"struct.std::__2::__compressed_pair_elem.149"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.149"* %this1 to %"struct.std::__2::default_delete.150"*
  ret %"struct.std::__2::default_delete.150"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.123"* @_ZNSt3__217__compressed_pairIPN5draco17AttributesDecoderENS_14default_deleteIS2_EEEC2IRS3_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.123"* returned %this, %"class.draco::AttributesDecoder"** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.123"*, align 4
  %__t1.addr = alloca %"class.draco::AttributesDecoder"**, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.123"* %this, %"class.std::__2::__compressed_pair.123"** %this.addr, align 4
  store %"class.draco::AttributesDecoder"** %__t1, %"class.draco::AttributesDecoder"*** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.123"*, %"class.std::__2::__compressed_pair.123"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.123"* %this1 to %"struct.std::__2::__compressed_pair_elem.124"*
  %1 = load %"class.draco::AttributesDecoder"**, %"class.draco::AttributesDecoder"*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributesDecoder"** @_ZNSt3__27forwardIRPN5draco17AttributesDecoderEEEOT_RNS_16remove_referenceIS5_E4typeE(%"class.draco::AttributesDecoder"** nonnull align 4 dereferenceable(4) %1) #8
  %call2 = call %"struct.std::__2::__compressed_pair_elem.124"* @_ZNSt3__222__compressed_pair_elemIPN5draco17AttributesDecoderELi0ELb0EEC2IRS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.124"* %0, %"class.draco::AttributesDecoder"** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.123"* %this1 to %"struct.std::__2::__compressed_pair_elem.125"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #8
  %call4 = call %"struct.std::__2::__compressed_pair_elem.125"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco17AttributesDecoderEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.125"* %2)
  ret %"class.std::__2::__compressed_pair.123"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::AttributesDecoder"** @_ZNSt3__27forwardIRPN5draco17AttributesDecoderEEEOT_RNS_16remove_referenceIS5_E4typeE(%"class.draco::AttributesDecoder"** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.draco::AttributesDecoder"**, align 4
  store %"class.draco::AttributesDecoder"** %__t, %"class.draco::AttributesDecoder"*** %__t.addr, align 4
  %0 = load %"class.draco::AttributesDecoder"**, %"class.draco::AttributesDecoder"*** %__t.addr, align 4
  ret %"class.draco::AttributesDecoder"** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.124"* @_ZNSt3__222__compressed_pair_elemIPN5draco17AttributesDecoderELi0ELb0EEC2IRS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.124"* returned %this, %"class.draco::AttributesDecoder"** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.124"*, align 4
  %__u.addr = alloca %"class.draco::AttributesDecoder"**, align 4
  store %"struct.std::__2::__compressed_pair_elem.124"* %this, %"struct.std::__2::__compressed_pair_elem.124"** %this.addr, align 4
  store %"class.draco::AttributesDecoder"** %__u, %"class.draco::AttributesDecoder"*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.124"*, %"struct.std::__2::__compressed_pair_elem.124"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.124", %"struct.std::__2::__compressed_pair_elem.124"* %this1, i32 0, i32 0
  %0 = load %"class.draco::AttributesDecoder"**, %"class.draco::AttributesDecoder"*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributesDecoder"** @_ZNSt3__27forwardIRPN5draco17AttributesDecoderEEEOT_RNS_16remove_referenceIS5_E4typeE(%"class.draco::AttributesDecoder"** nonnull align 4 dereferenceable(4) %0) #8
  %1 = load %"class.draco::AttributesDecoder"*, %"class.draco::AttributesDecoder"** %call, align 4
  store %"class.draco::AttributesDecoder"* %1, %"class.draco::AttributesDecoder"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.124"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.125"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco17AttributesDecoderEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.125"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.125"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.125"* %this, %"struct.std::__2::__compressed_pair_elem.125"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.125"*, %"struct.std::__2::__compressed_pair_elem.125"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.125"* %this1 to %"struct.std::__2::default_delete.126"*
  ret %"struct.std::__2::__compressed_pair_elem.125"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__210unique_ptrIN5draco17AttributesDecoderENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.122"* %this, %"class.draco::AttributesDecoder"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.122"*, align 4
  %__p.addr = alloca %"class.draco::AttributesDecoder"*, align 4
  %__tmp = alloca %"class.draco::AttributesDecoder"*, align 4
  store %"class.std::__2::unique_ptr.122"* %this, %"class.std::__2::unique_ptr.122"** %this.addr, align 4
  store %"class.draco::AttributesDecoder"* %__p, %"class.draco::AttributesDecoder"** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.122"*, %"class.std::__2::unique_ptr.122"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.122", %"class.std::__2::unique_ptr.122"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributesDecoder"** @_ZNSt3__217__compressed_pairIPN5draco17AttributesDecoderENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.123"* %__ptr_) #8
  %0 = load %"class.draco::AttributesDecoder"*, %"class.draco::AttributesDecoder"** %call, align 4
  store %"class.draco::AttributesDecoder"* %0, %"class.draco::AttributesDecoder"** %__tmp, align 4
  %1 = load %"class.draco::AttributesDecoder"*, %"class.draco::AttributesDecoder"** %__p.addr, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr.122", %"class.std::__2::unique_ptr.122"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::AttributesDecoder"** @_ZNSt3__217__compressed_pairIPN5draco17AttributesDecoderENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.123"* %__ptr_2) #8
  store %"class.draco::AttributesDecoder"* %1, %"class.draco::AttributesDecoder"** %call3, align 4
  %2 = load %"class.draco::AttributesDecoder"*, %"class.draco::AttributesDecoder"** %__tmp, align 4
  %tobool = icmp ne %"class.draco::AttributesDecoder"* %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__ptr_4 = getelementptr inbounds %"class.std::__2::unique_ptr.122", %"class.std::__2::unique_ptr.122"* %this1, i32 0, i32 0
  %call5 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.126"* @_ZNSt3__217__compressed_pairIPN5draco17AttributesDecoderENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.123"* %__ptr_4) #8
  %3 = load %"class.draco::AttributesDecoder"*, %"class.draco::AttributesDecoder"** %__tmp, align 4
  call void @_ZNKSt3__214default_deleteIN5draco17AttributesDecoderEEclEPS2_(%"struct.std::__2::default_delete.126"* %call5, %"class.draco::AttributesDecoder"* %3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::AttributesDecoder"** @_ZNSt3__217__compressed_pairIPN5draco17AttributesDecoderENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.123"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.123"*, align 4
  store %"class.std::__2::__compressed_pair.123"* %this, %"class.std::__2::__compressed_pair.123"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.123"*, %"class.std::__2::__compressed_pair.123"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.123"* %this1 to %"struct.std::__2::__compressed_pair_elem.124"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributesDecoder"** @_ZNSt3__222__compressed_pair_elemIPN5draco17AttributesDecoderELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.124"* %0) #8
  ret %"class.draco::AttributesDecoder"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.126"* @_ZNSt3__217__compressed_pairIPN5draco17AttributesDecoderENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.123"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.123"*, align 4
  store %"class.std::__2::__compressed_pair.123"* %this, %"class.std::__2::__compressed_pair.123"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.123"*, %"class.std::__2::__compressed_pair.123"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.123"* %this1 to %"struct.std::__2::__compressed_pair_elem.125"*
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.126"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco17AttributesDecoderEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.125"* %0) #8
  ret %"struct.std::__2::default_delete.126"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__214default_deleteIN5draco17AttributesDecoderEEclEPS2_(%"struct.std::__2::default_delete.126"* %this, %"class.draco::AttributesDecoder"* %__ptr) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::default_delete.126"*, align 4
  %__ptr.addr = alloca %"class.draco::AttributesDecoder"*, align 4
  store %"struct.std::__2::default_delete.126"* %this, %"struct.std::__2::default_delete.126"** %this.addr, align 4
  store %"class.draco::AttributesDecoder"* %__ptr, %"class.draco::AttributesDecoder"** %__ptr.addr, align 4
  %this1 = load %"struct.std::__2::default_delete.126"*, %"struct.std::__2::default_delete.126"** %this.addr, align 4
  %0 = load %"class.draco::AttributesDecoder"*, %"class.draco::AttributesDecoder"** %__ptr.addr, align 4
  %isnull = icmp eq %"class.draco::AttributesDecoder"* %0, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %entry
  %1 = bitcast %"class.draco::AttributesDecoder"* %0 to void (%"class.draco::AttributesDecoder"*)***
  %vtable = load void (%"class.draco::AttributesDecoder"*)**, void (%"class.draco::AttributesDecoder"*)*** %1, align 4
  %vfn = getelementptr inbounds void (%"class.draco::AttributesDecoder"*)*, void (%"class.draco::AttributesDecoder"*)** %vtable, i64 1
  %2 = load void (%"class.draco::AttributesDecoder"*)*, void (%"class.draco::AttributesDecoder"*)** %vfn, align 4
  call void %2(%"class.draco::AttributesDecoder"* %0) #8
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::AttributesDecoder"** @_ZNSt3__222__compressed_pair_elemIPN5draco17AttributesDecoderELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.124"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.124"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.124"* %this, %"struct.std::__2::__compressed_pair_elem.124"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.124"*, %"struct.std::__2::__compressed_pair_elem.124"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.124", %"struct.std::__2::__compressed_pair_elem.124"* %this1, i32 0, i32 0
  ret %"class.draco::AttributesDecoder"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.126"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco17AttributesDecoderEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.125"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.125"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.125"* %this, %"struct.std::__2::__compressed_pair_elem.125"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.125"*, %"struct.std::__2::__compressed_pair_elem.125"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.125"* %this1 to %"struct.std::__2::default_delete.126"*
  ret %"struct.std::__2::default_delete.126"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::AttributesDecoder"* @_ZNSt3__210unique_ptrIN5draco17AttributesDecoderENS_14default_deleteIS2_EEE7releaseEv(%"class.std::__2::unique_ptr.122"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.122"*, align 4
  %__t = alloca %"class.draco::AttributesDecoder"*, align 4
  store %"class.std::__2::unique_ptr.122"* %this, %"class.std::__2::unique_ptr.122"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.122"*, %"class.std::__2::unique_ptr.122"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.122", %"class.std::__2::unique_ptr.122"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributesDecoder"** @_ZNSt3__217__compressed_pairIPN5draco17AttributesDecoderENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.123"* %__ptr_) #8
  %0 = load %"class.draco::AttributesDecoder"*, %"class.draco::AttributesDecoder"** %call, align 4
  store %"class.draco::AttributesDecoder"* %0, %"class.draco::AttributesDecoder"** %__t, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr.122", %"class.std::__2::unique_ptr.122"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::AttributesDecoder"** @_ZNSt3__217__compressed_pairIPN5draco17AttributesDecoderENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.123"* %__ptr_2) #8
  store %"class.draco::AttributesDecoder"* null, %"class.draco::AttributesDecoder"** %call3, align 4
  %1 = load %"class.draco::AttributesDecoder"*, %"class.draco::AttributesDecoder"** %__t, align 4
  ret %"class.draco::AttributesDecoder"* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.126"* @_ZNSt3__27forwardINS_14default_deleteIN5draco17AttributesDecoderEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.126"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"struct.std::__2::default_delete.126"*, align 4
  store %"struct.std::__2::default_delete.126"* %__t, %"struct.std::__2::default_delete.126"** %__t.addr, align 4
  %0 = load %"struct.std::__2::default_delete.126"*, %"struct.std::__2::default_delete.126"** %__t.addr, align 4
  ret %"struct.std::__2::default_delete.126"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.126"* @_ZNSt3__210unique_ptrIN5draco17AttributesDecoderENS_14default_deleteIS2_EEE11get_deleterEv(%"class.std::__2::unique_ptr.122"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.122"*, align 4
  store %"class.std::__2::unique_ptr.122"* %this, %"class.std::__2::unique_ptr.122"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.122"*, %"class.std::__2::unique_ptr.122"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.122", %"class.std::__2::unique_ptr.122"* %this1, i32 0, i32 0
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.126"* @_ZNSt3__217__compressed_pairIPN5draco17AttributesDecoderENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.123"* %__ptr_) #8
  ret %"struct.std::__2::default_delete.126"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.97"* @_ZNSt3__217__compressed_pairIPN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEEC2IPNS1_17AttributesDecoderENS4_IS8_EEEEOT_OT0_(%"class.std::__2::__compressed_pair.97"* returned %this, %"class.draco::AttributesDecoder"** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::default_delete.126"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.97"*, align 4
  %__t1.addr = alloca %"class.draco::AttributesDecoder"**, align 4
  %__t2.addr = alloca %"struct.std::__2::default_delete.126"*, align 4
  store %"class.std::__2::__compressed_pair.97"* %this, %"class.std::__2::__compressed_pair.97"** %this.addr, align 4
  store %"class.draco::AttributesDecoder"** %__t1, %"class.draco::AttributesDecoder"*** %__t1.addr, align 4
  store %"struct.std::__2::default_delete.126"* %__t2, %"struct.std::__2::default_delete.126"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.97"*, %"class.std::__2::__compressed_pair.97"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.97"* %this1 to %"struct.std::__2::__compressed_pair_elem.98"*
  %1 = load %"class.draco::AttributesDecoder"**, %"class.draco::AttributesDecoder"*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributesDecoder"** @_ZNSt3__27forwardIPN5draco17AttributesDecoderEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::AttributesDecoder"** nonnull align 4 dereferenceable(4) %1) #8
  %call2 = call %"struct.std::__2::__compressed_pair_elem.98"* @_ZNSt3__222__compressed_pair_elemIPN5draco26AttributesDecoderInterfaceELi0ELb0EEC2IPNS1_17AttributesDecoderEvEEOT_(%"struct.std::__2::__compressed_pair_elem.98"* %0, %"class.draco::AttributesDecoder"** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.97"* %this1 to %"struct.std::__2::__compressed_pair_elem.99"*
  %3 = load %"struct.std::__2::default_delete.126"*, %"struct.std::__2::default_delete.126"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.126"* @_ZNSt3__27forwardINS_14default_deleteIN5draco17AttributesDecoderEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.126"* nonnull align 1 dereferenceable(1) %3) #8
  %call4 = call %"struct.std::__2::__compressed_pair_elem.99"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco26AttributesDecoderInterfaceEEELi1ELb1EEC2INS1_INS2_17AttributesDecoderEEEvEEOT_(%"struct.std::__2::__compressed_pair_elem.99"* %2, %"struct.std::__2::default_delete.126"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::__compressed_pair.97"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::AttributesDecoder"** @_ZNSt3__27forwardIPN5draco17AttributesDecoderEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::AttributesDecoder"** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.draco::AttributesDecoder"**, align 4
  store %"class.draco::AttributesDecoder"** %__t, %"class.draco::AttributesDecoder"*** %__t.addr, align 4
  %0 = load %"class.draco::AttributesDecoder"**, %"class.draco::AttributesDecoder"*** %__t.addr, align 4
  ret %"class.draco::AttributesDecoder"** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.98"* @_ZNSt3__222__compressed_pair_elemIPN5draco26AttributesDecoderInterfaceELi0ELb0EEC2IPNS1_17AttributesDecoderEvEEOT_(%"struct.std::__2::__compressed_pair_elem.98"* returned %this, %"class.draco::AttributesDecoder"** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.98"*, align 4
  %__u.addr = alloca %"class.draco::AttributesDecoder"**, align 4
  store %"struct.std::__2::__compressed_pair_elem.98"* %this, %"struct.std::__2::__compressed_pair_elem.98"** %this.addr, align 4
  store %"class.draco::AttributesDecoder"** %__u, %"class.draco::AttributesDecoder"*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.98"*, %"struct.std::__2::__compressed_pair_elem.98"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.98", %"struct.std::__2::__compressed_pair_elem.98"* %this1, i32 0, i32 0
  %0 = load %"class.draco::AttributesDecoder"**, %"class.draco::AttributesDecoder"*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributesDecoder"** @_ZNSt3__27forwardIPN5draco17AttributesDecoderEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::AttributesDecoder"** nonnull align 4 dereferenceable(4) %0) #8
  %1 = load %"class.draco::AttributesDecoder"*, %"class.draco::AttributesDecoder"** %call, align 4
  %2 = bitcast %"class.draco::AttributesDecoder"* %1 to %"class.draco::AttributesDecoderInterface"*
  store %"class.draco::AttributesDecoderInterface"* %2, %"class.draco::AttributesDecoderInterface"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.98"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.99"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco26AttributesDecoderInterfaceEEELi1ELb1EEC2INS1_INS2_17AttributesDecoderEEEvEEOT_(%"struct.std::__2::__compressed_pair_elem.99"* returned %this, %"struct.std::__2::default_delete.126"* nonnull align 1 dereferenceable(1) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.99"*, align 4
  %__u.addr = alloca %"struct.std::__2::default_delete.126"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.99"* %this, %"struct.std::__2::__compressed_pair_elem.99"** %this.addr, align 4
  store %"struct.std::__2::default_delete.126"* %__u, %"struct.std::__2::default_delete.126"** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.99"*, %"struct.std::__2::__compressed_pair_elem.99"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.99"* %this1 to %"struct.std::__2::default_delete.100"*
  %1 = load %"struct.std::__2::default_delete.126"*, %"struct.std::__2::default_delete.126"** %__u.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.126"* @_ZNSt3__27forwardINS_14default_deleteIN5draco17AttributesDecoderEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.126"* nonnull align 1 dereferenceable(1) %1) #8
  %call2 = call %"struct.std::__2::default_delete.100"* @_ZNSt3__214default_deleteIN5draco26AttributesDecoderInterfaceEEC2INS1_17AttributesDecoderEEERKNS0_IT_EEPNS_9enable_ifIXsr14is_convertibleIPS6_PS2_EE5valueEvE4typeE(%"struct.std::__2::default_delete.100"* %0, %"struct.std::__2::default_delete.126"* nonnull align 1 dereferenceable(1) %call, i8* null) #8
  ret %"struct.std::__2::__compressed_pair_elem.99"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::default_delete.100"* @_ZNSt3__214default_deleteIN5draco26AttributesDecoderInterfaceEEC2INS1_17AttributesDecoderEEERKNS0_IT_EEPNS_9enable_ifIXsr14is_convertibleIPS6_PS2_EE5valueEvE4typeE(%"struct.std::__2::default_delete.100"* returned %this, %"struct.std::__2::default_delete.126"* nonnull align 1 dereferenceable(1) %0, i8* %1) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::default_delete.100"*, align 4
  %.addr = alloca %"struct.std::__2::default_delete.126"*, align 4
  %.addr1 = alloca i8*, align 4
  store %"struct.std::__2::default_delete.100"* %this, %"struct.std::__2::default_delete.100"** %this.addr, align 4
  store %"struct.std::__2::default_delete.126"* %0, %"struct.std::__2::default_delete.126"** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  %this2 = load %"struct.std::__2::default_delete.100"*, %"struct.std::__2::default_delete.100"** %this.addr, align 4
  ret %"struct.std::__2::default_delete.100"* %this2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base.152"* @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEEC2Ev(%"class.std::__2::__vector_base.152"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.152"*, align 4
  %ref.tmp = alloca i8*, align 4
  %ref.tmp2 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__vector_base.152"* %this, %"class.std::__2::__vector_base.152"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.152"*, %"class.std::__2::__vector_base.152"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__vector_base.152"* %this1 to %"class.std::__2::__vector_base_common"*
  %call = call %"class.std::__2::__vector_base_common"* @_ZNSt3__220__vector_base_commonILb1EEC2Ev(%"class.std::__2::__vector_base_common"* %0)
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.152", %"class.std::__2::__vector_base.152"* %this1, i32 0, i32 0
  store i32* null, i32** %__begin_, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.152", %"class.std::__2::__vector_base.152"* %this1, i32 0, i32 1
  store i32* null, i32** %__end_, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.152", %"class.std::__2::__vector_base.152"* %this1, i32 0, i32 2
  store i8* null, i8** %ref.tmp, align 4
  %call3 = call %"class.std::__2::__compressed_pair.153"* @_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEEC2IDnNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.153"* %__end_cap_, i8** nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  ret %"class.std::__2::__vector_base.152"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIjNS_9allocatorIjEEE11__vallocateEm(%"class.std::__2::vector.151"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.151"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector.151"* %this, %"class.std::__2::vector.151"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.151"*, %"class.std::__2::vector.151"** %this.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE8max_sizeEv(%"class.std::__2::vector.151"* %this1) #8
  %cmp = icmp ugt i32 %0, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %"class.std::__2::vector.151"* %this1 to %"class.std::__2::__vector_base_common"*
  call void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"* %1) #11
  unreachable

if.end:                                           ; preds = %entry
  %2 = bitcast %"class.std::__2::vector.151"* %this1 to %"class.std::__2::__vector_base.152"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.156"* @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv(%"class.std::__2::__vector_base.152"* %2) #8
  %3 = load i32, i32* %__n.addr, align 4
  %call3 = call i32* @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE8allocateERS2_m(%"class.std::__2::allocator.156"* nonnull align 1 dereferenceable(1) %call2, i32 %3)
  %4 = bitcast %"class.std::__2::vector.151"* %this1 to %"class.std::__2::__vector_base.152"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.152", %"class.std::__2::__vector_base.152"* %4, i32 0, i32 1
  store i32* %call3, i32** %__end_, align 4
  %5 = bitcast %"class.std::__2::vector.151"* %this1 to %"class.std::__2::__vector_base.152"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.152", %"class.std::__2::__vector_base.152"* %5, i32 0, i32 0
  store i32* %call3, i32** %__begin_, align 4
  %6 = bitcast %"class.std::__2::vector.151"* %this1 to %"class.std::__2::__vector_base.152"*
  %__begin_4 = getelementptr inbounds %"class.std::__2::__vector_base.152", %"class.std::__2::__vector_base.152"* %6, i32 0, i32 0
  %7 = load i32*, i32** %__begin_4, align 4
  %8 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds i32, i32* %7, i32 %8
  %9 = bitcast %"class.std::__2::vector.151"* %this1 to %"class.std::__2::__vector_base.152"*
  %call5 = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE9__end_capEv(%"class.std::__2::__vector_base.152"* %9) #8
  store i32* %add.ptr, i32** %call5, align 4
  call void @_ZNKSt3__26vectorIjNS_9allocatorIjEEE14__annotate_newEm(%"class.std::__2::vector.151"* %this1, i32 0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIjNS_9allocatorIjEEE18__construct_at_endEm(%"class.std::__2::vector.151"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.151"*, align 4
  %__n.addr = alloca i32, align 4
  %__tx = alloca %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction", align 4
  store %"class.std::__2::vector.151"* %this, %"class.std::__2::vector.151"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.151"*, %"class.std::__2::vector.151"** %this.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %call = call %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* @_ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionC2ERS3_m(%"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %__tx, %"class.std::__2::vector.151"* nonnull align 4 dereferenceable(12) %this1, i32 %0)
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction", %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %1 = load i32*, i32** %__pos_, align 4
  %__new_end_ = getelementptr inbounds %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction", %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %__tx, i32 0, i32 2
  %2 = load i32*, i32** %__new_end_, align 4
  %cmp = icmp ne i32* %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = bitcast %"class.std::__2::vector.151"* %this1 to %"class.std::__2::__vector_base.152"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.156"* @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv(%"class.std::__2::__vector_base.152"* %3) #8
  %__pos_3 = getelementptr inbounds %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction", %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %4 = load i32*, i32** %__pos_3, align 4
  %call4 = call i32* @_ZNSt3__212__to_addressIjEEPT_S2_(i32* %4) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE9constructIjJEEEvRS2_PT_DpOT0_(%"class.std::__2::allocator.156"* nonnull align 1 dereferenceable(1) %call2, i32* %call4)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %__pos_5 = getelementptr inbounds %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction", %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %5 = load i32*, i32** %__pos_5, align 4
  %incdec.ptr = getelementptr inbounds i32, i32* %5, i32 1
  store i32* %incdec.ptr, i32** %__pos_5, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call6 = call %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* @_ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %__tx) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base_common"* @_ZNSt3__220__vector_base_commonILb1EEC2Ev(%"class.std::__2::__vector_base_common"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base_common"*, align 4
  store %"class.std::__2::__vector_base_common"* %this, %"class.std::__2::__vector_base_common"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base_common"*, %"class.std::__2::__vector_base_common"** %this.addr, align 4
  ret %"class.std::__2::__vector_base_common"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.153"* @_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEEC2IDnNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.153"* returned %this, i8** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.153"*, align 4
  %__t1.addr = alloca i8**, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.153"* %this, %"class.std::__2::__compressed_pair.153"** %this.addr, align 4
  store i8** %__t1, i8*** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.153"*, %"class.std::__2::__compressed_pair.153"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.153"* %this1 to %"struct.std::__2::__compressed_pair_elem.154"*
  %1 = load i8**, i8*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %1) #8
  %call2 = call %"struct.std::__2::__compressed_pair_elem.154"* @_ZNSt3__222__compressed_pair_elemIPjLi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.154"* %0, i8** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.153"* %this1 to %"struct.std::__2::__compressed_pair_elem.155"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #8
  %call4 = call %"struct.std::__2::__compressed_pair_elem.155"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.155"* %2)
  ret %"class.std::__2::__compressed_pair.153"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.154"* @_ZNSt3__222__compressed_pair_elemIPjLi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.154"* returned %this, i8** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.154"*, align 4
  %__u.addr = alloca i8**, align 4
  store %"struct.std::__2::__compressed_pair_elem.154"* %this, %"struct.std::__2::__compressed_pair_elem.154"** %this.addr, align 4
  store i8** %__u, i8*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.154"*, %"struct.std::__2::__compressed_pair_elem.154"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.154", %"struct.std::__2::__compressed_pair_elem.154"* %this1, i32 0, i32 0
  %0 = load i8**, i8*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %0) #8
  store i32* null, i32** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.154"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.155"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.155"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.155"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.155"* %this, %"struct.std::__2::__compressed_pair_elem.155"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.155"*, %"struct.std::__2::__compressed_pair_elem.155"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.155"* %this1 to %"class.std::__2::allocator.156"*
  %call = call %"class.std::__2::allocator.156"* @_ZNSt3__29allocatorIjEC2Ev(%"class.std::__2::allocator.156"* %1) #8
  ret %"struct.std::__2::__compressed_pair_elem.155"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::allocator.156"* @_ZNSt3__29allocatorIjEC2Ev(%"class.std::__2::allocator.156"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.156"*, align 4
  store %"class.std::__2::allocator.156"* %this, %"class.std::__2::allocator.156"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.156"*, %"class.std::__2::allocator.156"** %this.addr, align 4
  ret %"class.std::__2::allocator.156"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE8max_sizeEv(%"class.std::__2::vector.151"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.151"*, align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp3 = alloca i32, align 4
  store %"class.std::__2::vector.151"* %this, %"class.std::__2::vector.151"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.151"*, %"class.std::__2::vector.151"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.151"* %this1 to %"class.std::__2::__vector_base.152"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.156"* @_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv(%"class.std::__2::__vector_base.152"* %0) #8
  %call2 = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE8max_sizeERKS2_(%"class.std::__2::allocator.156"* nonnull align 1 dereferenceable(1) %call) #8
  store i32 %call2, i32* %ref.tmp, align 4
  %call4 = call i32 @_ZNSt3__214numeric_limitsIlE3maxEv() #8
  store i32 %call4, i32* %ref.tmp3, align 4
  %call5 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %1 = load i32, i32* %call5, align 4
  ret i32 %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE8allocateERS2_m(%"class.std::__2::allocator.156"* nonnull align 1 dereferenceable(1) %__a, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.156"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.156"* %__a, %"class.std::__2::allocator.156"** %__a.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.156"*, %"class.std::__2::allocator.156"** %__a.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call i32* @_ZNSt3__29allocatorIjE8allocateEmPKv(%"class.std::__2::allocator.156"* %0, i32 %1, i8* null)
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.156"* @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv(%"class.std::__2::__vector_base.152"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.152"*, align 4
  store %"class.std::__2::__vector_base.152"* %this, %"class.std::__2::__vector_base.152"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.152"*, %"class.std::__2::__vector_base.152"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.152", %"class.std::__2::__vector_base.152"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.156"* @_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEE6secondEv(%"class.std::__2::__compressed_pair.153"* %__end_cap_) #8
  ret %"class.std::__2::allocator.156"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE9__end_capEv(%"class.std::__2::__vector_base.152"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.152"*, align 4
  store %"class.std::__2::__vector_base.152"* %this, %"class.std::__2::__vector_base.152"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.152"*, %"class.std::__2::__vector_base.152"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.152", %"class.std::__2::__vector_base.152"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEE5firstEv(%"class.std::__2::__compressed_pair.153"* %__end_cap_) #8
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIjNS_9allocatorIjEEE14__annotate_newEm(%"class.std::__2::vector.151"* %this, i32 %__current_size) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.151"*, align 4
  %__current_size.addr = alloca i32, align 4
  store %"class.std::__2::vector.151"* %this, %"class.std::__2::vector.151"** %this.addr, align 4
  store i32 %__current_size, i32* %__current_size.addr, align 4
  %this1 = load %"class.std::__2::vector.151"*, %"class.std::__2::vector.151"** %this.addr, align 4
  %call = call i32* @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv(%"class.std::__2::vector.151"* %this1) #8
  %0 = bitcast i32* %call to i8*
  %call2 = call i32* @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv(%"class.std::__2::vector.151"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE8capacityEv(%"class.std::__2::vector.151"* %this1) #8
  %add.ptr = getelementptr inbounds i32, i32* %call2, i32 %call3
  %1 = bitcast i32* %add.ptr to i8*
  %call4 = call i32* @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv(%"class.std::__2::vector.151"* %this1) #8
  %call5 = call i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE8capacityEv(%"class.std::__2::vector.151"* %this1) #8
  %add.ptr6 = getelementptr inbounds i32, i32* %call4, i32 %call5
  %2 = bitcast i32* %add.ptr6 to i8*
  %call7 = call i32* @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv(%"class.std::__2::vector.151"* %this1) #8
  %3 = load i32, i32* %__current_size.addr, align 4
  %add.ptr8 = getelementptr inbounds i32, i32* %call7, i32 %3
  %4 = bitcast i32* %add.ptr8 to i8*
  call void @_ZNKSt3__26vectorIjNS_9allocatorIjEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector.151"* %this1, i8* %0, i8* %1, i8* %2, i8* %4) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE8max_sizeERKS2_(%"class.std::__2::allocator.156"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.156"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_max_size.211", align 1
  store %"class.std::__2::allocator.156"* %__a, %"class.std::__2::allocator.156"** %__a.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_max_size.211"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.156"*, %"class.std::__2::allocator.156"** %__a.addr, align 4
  %call = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE10__max_sizeENS_17integral_constantIbLb1EEERKS2_(%"class.std::__2::allocator.156"* nonnull align 1 dereferenceable(1) %1) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.156"* @_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv(%"class.std::__2::__vector_base.152"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.152"*, align 4
  store %"class.std::__2::__vector_base.152"* %this, %"class.std::__2::__vector_base.152"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.152"*, %"class.std::__2::__vector_base.152"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.152", %"class.std::__2::__vector_base.152"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.156"* @_ZNKSt3__217__compressed_pairIPjNS_9allocatorIjEEE6secondEv(%"class.std::__2::__compressed_pair.153"* %__end_cap_) #8
  ret %"class.std::__2::allocator.156"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE10__max_sizeENS_17integral_constantIbLb1EEERKS2_(%"class.std::__2::allocator.156"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.156"*, align 4
  store %"class.std::__2::allocator.156"* %__a, %"class.std::__2::allocator.156"** %__a.addr, align 4
  %1 = load %"class.std::__2::allocator.156"*, %"class.std::__2::allocator.156"** %__a.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorIjE8max_sizeEv(%"class.std::__2::allocator.156"* %1) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__29allocatorIjE8max_sizeEv(%"class.std::__2::allocator.156"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.156"*, align 4
  store %"class.std::__2::allocator.156"* %this, %"class.std::__2::allocator.156"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.156"*, %"class.std::__2::allocator.156"** %this.addr, align 4
  ret i32 1073741823
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.156"* @_ZNKSt3__217__compressed_pairIPjNS_9allocatorIjEEE6secondEv(%"class.std::__2::__compressed_pair.153"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.153"*, align 4
  store %"class.std::__2::__compressed_pair.153"* %this, %"class.std::__2::__compressed_pair.153"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.153"*, %"class.std::__2::__compressed_pair.153"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.153"* %this1 to %"struct.std::__2::__compressed_pair_elem.155"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.156"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.155"* %0) #8
  ret %"class.std::__2::allocator.156"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.156"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.155"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.155"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.155"* %this, %"struct.std::__2::__compressed_pair_elem.155"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.155"*, %"struct.std::__2::__compressed_pair_elem.155"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.155"* %this1 to %"class.std::__2::allocator.156"*
  ret %"class.std::__2::allocator.156"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNSt3__29allocatorIjE8allocateEmPKv(%"class.std::__2::allocator.156"* %this, i32 %__n, i8* %0) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.156"*, align 4
  %__n.addr = alloca i32, align 4
  %.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.156"* %this, %"class.std::__2::allocator.156"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::allocator.156"*, %"class.std::__2::allocator.156"** %this.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorIjE8max_sizeEv(%"class.std::__2::allocator.156"* %this1) #8
  %cmp = icmp ugt i32 %1, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__220__throw_length_errorEPKc(i8* getelementptr inbounds ([68 x i8], [68 x i8]* @.str, i32 0, i32 0)) #11
  unreachable

if.end:                                           ; preds = %entry
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  %call2 = call i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %mul, i32 4)
  %3 = bitcast i8* %call2 to i32*
  ret i32* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.156"* @_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEE6secondEv(%"class.std::__2::__compressed_pair.153"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.153"*, align 4
  store %"class.std::__2::__compressed_pair.153"* %this, %"class.std::__2::__compressed_pair.153"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.153"*, %"class.std::__2::__compressed_pair.153"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.153"* %this1 to %"struct.std::__2::__compressed_pair_elem.155"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.156"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.155"* %0) #8
  ret %"class.std::__2::allocator.156"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.156"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.155"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.155"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.155"* %this, %"struct.std::__2::__compressed_pair_elem.155"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.155"*, %"struct.std::__2::__compressed_pair_elem.155"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.155"* %this1 to %"class.std::__2::allocator.156"*
  ret %"class.std::__2::allocator.156"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEE5firstEv(%"class.std::__2::__compressed_pair.153"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.153"*, align 4
  store %"class.std::__2::__compressed_pair.153"* %this, %"class.std::__2::__compressed_pair.153"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.153"*, %"class.std::__2::__compressed_pair.153"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.153"* %this1 to %"struct.std::__2::__compressed_pair_elem.154"*
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__222__compressed_pair_elemIPjLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.154"* %0) #8
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNSt3__222__compressed_pair_elemIPjLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.154"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.154"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.154"* %this, %"struct.std::__2::__compressed_pair_elem.154"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.154"*, %"struct.std::__2::__compressed_pair_elem.154"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.154", %"struct.std::__2::__compressed_pair_elem.154"* %this1, i32 0, i32 0
  ret i32** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIjNS_9allocatorIjEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector.151"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.151"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector.151"* %this, %"class.std::__2::vector.151"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector.151"*, %"class.std::__2::vector.151"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv(%"class.std::__2::vector.151"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.151"*, align 4
  store %"class.std::__2::vector.151"* %this, %"class.std::__2::vector.151"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.151"*, %"class.std::__2::vector.151"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.151"* %this1 to %"class.std::__2::__vector_base.152"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.152", %"class.std::__2::__vector_base.152"* %0, i32 0, i32 0
  %1 = load i32*, i32** %__begin_, align 4
  %call = call i32* @_ZNSt3__212__to_addressIjEEPT_S2_(i32* %1) #8
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE8capacityEv(%"class.std::__2::vector.151"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.151"*, align 4
  store %"class.std::__2::vector.151"* %this, %"class.std::__2::vector.151"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.151"*, %"class.std::__2::vector.151"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.151"* %this1 to %"class.std::__2::__vector_base.152"*
  %call = call i32 @_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE8capacityEv(%"class.std::__2::__vector_base.152"* %0) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNSt3__212__to_addressIjEEPT_S2_(i32* %__p) #0 comdat {
entry:
  %__p.addr = alloca i32*, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %0 = load i32*, i32** %__p.addr, align 4
  ret i32* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE8capacityEv(%"class.std::__2::__vector_base.152"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.152"*, align 4
  store %"class.std::__2::__vector_base.152"* %this, %"class.std::__2::__vector_base.152"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.152"*, %"class.std::__2::__vector_base.152"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE9__end_capEv(%"class.std::__2::__vector_base.152"* %this1) #8
  %0 = load i32*, i32** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.152", %"class.std::__2::__vector_base.152"* %this1, i32 0, i32 0
  %1 = load i32*, i32** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint i32* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint i32* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE9__end_capEv(%"class.std::__2::__vector_base.152"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.152"*, align 4
  store %"class.std::__2::__vector_base.152"* %this, %"class.std::__2::__vector_base.152"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.152"*, %"class.std::__2::__vector_base.152"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.152", %"class.std::__2::__vector_base.152"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__217__compressed_pairIPjNS_9allocatorIjEEE5firstEv(%"class.std::__2::__compressed_pair.153"* %__end_cap_) #8
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__217__compressed_pairIPjNS_9allocatorIjEEE5firstEv(%"class.std::__2::__compressed_pair.153"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.153"*, align 4
  store %"class.std::__2::__compressed_pair.153"* %this, %"class.std::__2::__compressed_pair.153"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.153"*, %"class.std::__2::__compressed_pair.153"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.153"* %this1 to %"struct.std::__2::__compressed_pair_elem.154"*
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__222__compressed_pair_elemIPjLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.154"* %0) #8
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__222__compressed_pair_elemIPjLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.154"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.154"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.154"* %this, %"struct.std::__2::__compressed_pair_elem.154"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.154"*, %"struct.std::__2::__compressed_pair_elem.154"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.154", %"struct.std::__2::__compressed_pair_elem.154"* %this1, i32 0, i32 0
  ret i32** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* @_ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionC2ERS3_m(%"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* returned %this, %"class.std::__2::vector.151"* nonnull align 4 dereferenceable(12) %__v, i32 %__n) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"*, align 4
  %__v.addr = alloca %"class.std::__2::vector.151"*, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"** %this.addr, align 4
  store %"class.std::__2::vector.151"* %__v, %"class.std::__2::vector.151"** %__v.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"*, %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"** %this.addr, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction", %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::vector.151"*, %"class.std::__2::vector.151"** %__v.addr, align 4
  store %"class.std::__2::vector.151"* %0, %"class.std::__2::vector.151"** %__v_, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction", %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %1 = load %"class.std::__2::vector.151"*, %"class.std::__2::vector.151"** %__v.addr, align 4
  %2 = bitcast %"class.std::__2::vector.151"* %1 to %"class.std::__2::__vector_base.152"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.152", %"class.std::__2::__vector_base.152"* %2, i32 0, i32 1
  %3 = load i32*, i32** %__end_, align 4
  store i32* %3, i32** %__pos_, align 4
  %__new_end_ = getelementptr inbounds %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction", %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %this1, i32 0, i32 2
  %4 = load %"class.std::__2::vector.151"*, %"class.std::__2::vector.151"** %__v.addr, align 4
  %5 = bitcast %"class.std::__2::vector.151"* %4 to %"class.std::__2::__vector_base.152"*
  %__end_2 = getelementptr inbounds %"class.std::__2::__vector_base.152", %"class.std::__2::__vector_base.152"* %5, i32 0, i32 1
  %6 = load i32*, i32** %__end_2, align 4
  %7 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds i32, i32* %6, i32 %7
  store i32* %add.ptr, i32** %__new_end_, align 4
  ret %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE9constructIjJEEEvRS2_PT_DpOT0_(%"class.std::__2::allocator.156"* nonnull align 1 dereferenceable(1) %__a, i32* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.156"*, align 4
  %__p.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_construct.212", align 1
  store %"class.std::__2::allocator.156"* %__a, %"class.std::__2::allocator.156"** %__a.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_construct.212"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.156"*, %"class.std::__2::allocator.156"** %__a.addr, align 4
  %2 = load i32*, i32** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE11__constructIjJEEEvNS_17integral_constantIbLb1EEERS2_PT_DpOT0_(%"class.std::__2::allocator.156"* nonnull align 1 dereferenceable(1) %1, i32* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* @_ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"*, align 4
  store %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"** %this.addr, align 4
  %this1 = load %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"*, %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction", %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %0 = load i32*, i32** %__pos_, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction", %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::vector.151"*, %"class.std::__2::vector.151"** %__v_, align 4
  %2 = bitcast %"class.std::__2::vector.151"* %1 to %"class.std::__2::__vector_base.152"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.152", %"class.std::__2::__vector_base.152"* %2, i32 0, i32 1
  store i32* %0, i32** %__end_, align 4
  ret %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE11__constructIjJEEEvNS_17integral_constantIbLb1EEERS2_PT_DpOT0_(%"class.std::__2::allocator.156"* nonnull align 1 dereferenceable(1) %__a, i32* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.156"*, align 4
  %__p.addr = alloca i32*, align 4
  store %"class.std::__2::allocator.156"* %__a, %"class.std::__2::allocator.156"** %__a.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.156"*, %"class.std::__2::allocator.156"** %__a.addr, align 4
  %2 = load i32*, i32** %__p.addr, align 4
  call void @_ZNSt3__29allocatorIjE9constructIjJEEEvPT_DpOT0_(%"class.std::__2::allocator.156"* %1, i32* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIjE9constructIjJEEEvPT_DpOT0_(%"class.std::__2::allocator.156"* %this, i32* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.156"*, align 4
  %__p.addr = alloca i32*, align 4
  store %"class.std::__2::allocator.156"* %this, %"class.std::__2::allocator.156"** %this.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.156"*, %"class.std::__2::allocator.156"** %this.addr, align 4
  %0 = load i32*, i32** %__p.addr, align 4
  %1 = bitcast i32* %0 to i8*
  %2 = bitcast i8* %1 to i32*
  store i32 0, i32* %2, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIjNS_9allocatorIjEEE17__annotate_deleteEv(%"class.std::__2::vector.151"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.151"*, align 4
  store %"class.std::__2::vector.151"* %this, %"class.std::__2::vector.151"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.151"*, %"class.std::__2::vector.151"** %this.addr, align 4
  %call = call i32* @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv(%"class.std::__2::vector.151"* %this1) #8
  %0 = bitcast i32* %call to i8*
  %call2 = call i32* @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv(%"class.std::__2::vector.151"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE8capacityEv(%"class.std::__2::vector.151"* %this1) #8
  %add.ptr = getelementptr inbounds i32, i32* %call2, i32 %call3
  %1 = bitcast i32* %add.ptr to i8*
  %call4 = call i32* @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv(%"class.std::__2::vector.151"* %this1) #8
  %call5 = call i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4sizeEv(%"class.std::__2::vector.151"* %this1) #8
  %add.ptr6 = getelementptr inbounds i32, i32* %call4, i32 %call5
  %2 = bitcast i32* %add.ptr6 to i8*
  %call7 = call i32* @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv(%"class.std::__2::vector.151"* %this1) #8
  %call8 = call i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE8capacityEv(%"class.std::__2::vector.151"* %this1) #8
  %add.ptr9 = getelementptr inbounds i32, i32* %call7, i32 %call8
  %3 = bitcast i32* %add.ptr9 to i8*
  call void @_ZNKSt3__26vectorIjNS_9allocatorIjEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector.151"* %this1, i8* %0, i8* %1, i8* %2, i8* %3) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base.152"* @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEED2Ev(%"class.std::__2::__vector_base.152"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__vector_base.152"*, align 4
  %this.addr = alloca %"class.std::__2::__vector_base.152"*, align 4
  store %"class.std::__2::__vector_base.152"* %this, %"class.std::__2::__vector_base.152"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.152"*, %"class.std::__2::__vector_base.152"** %this.addr, align 4
  store %"class.std::__2::__vector_base.152"* %this1, %"class.std::__2::__vector_base.152"** %retval, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.152", %"class.std::__2::__vector_base.152"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__begin_, align 4
  %cmp = icmp ne i32* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE5clearEv(%"class.std::__2::__vector_base.152"* %this1) #8
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.156"* @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv(%"class.std::__2::__vector_base.152"* %this1) #8
  %__begin_2 = getelementptr inbounds %"class.std::__2::__vector_base.152", %"class.std::__2::__vector_base.152"* %this1, i32 0, i32 0
  %1 = load i32*, i32** %__begin_2, align 4
  %call3 = call i32 @_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE8capacityEv(%"class.std::__2::__vector_base.152"* %this1) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE10deallocateERS2_Pjm(%"class.std::__2::allocator.156"* nonnull align 1 dereferenceable(1) %call, i32* %1, i32 %call3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"class.std::__2::__vector_base.152"*, %"class.std::__2::__vector_base.152"** %retval, align 4
  ret %"class.std::__2::__vector_base.152"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4sizeEv(%"class.std::__2::vector.151"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.151"*, align 4
  store %"class.std::__2::vector.151"* %this, %"class.std::__2::vector.151"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.151"*, %"class.std::__2::vector.151"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.151"* %this1 to %"class.std::__2::__vector_base.152"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.152", %"class.std::__2::__vector_base.152"* %0, i32 0, i32 1
  %1 = load i32*, i32** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector.151"* %this1 to %"class.std::__2::__vector_base.152"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.152", %"class.std::__2::__vector_base.152"* %2, i32 0, i32 0
  %3 = load i32*, i32** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint i32* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint i32* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE5clearEv(%"class.std::__2::__vector_base.152"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.152"*, align 4
  store %"class.std::__2::__vector_base.152"* %this, %"class.std::__2::__vector_base.152"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.152"*, %"class.std::__2::__vector_base.152"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.152", %"class.std::__2::__vector_base.152"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__begin_, align 4
  call void @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE17__destruct_at_endEPj(%"class.std::__2::__vector_base.152"* %this1, i32* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE10deallocateERS2_Pjm(%"class.std::__2::allocator.156"* nonnull align 1 dereferenceable(1) %__a, i32* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.156"*, align 4
  %__p.addr = alloca i32*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.156"* %__a, %"class.std::__2::allocator.156"** %__a.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.156"*, %"class.std::__2::allocator.156"** %__a.addr, align 4
  %1 = load i32*, i32** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorIjE10deallocateEPjm(%"class.std::__2::allocator.156"* %0, i32* %1, i32 %2) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE17__destruct_at_endEPj(%"class.std::__2::__vector_base.152"* %this, i32* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.152"*, align 4
  %__new_last.addr = alloca i32*, align 4
  %__soon_to_be_end = alloca i32*, align 4
  store %"class.std::__2::__vector_base.152"* %this, %"class.std::__2::__vector_base.152"** %this.addr, align 4
  store i32* %__new_last, i32** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.152"*, %"class.std::__2::__vector_base.152"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.152", %"class.std::__2::__vector_base.152"* %this1, i32 0, i32 1
  %0 = load i32*, i32** %__end_, align 4
  store i32* %0, i32** %__soon_to_be_end, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load i32*, i32** %__new_last.addr, align 4
  %2 = load i32*, i32** %__soon_to_be_end, align 4
  %cmp = icmp ne i32* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.156"* @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv(%"class.std::__2::__vector_base.152"* %this1) #8
  %3 = load i32*, i32** %__soon_to_be_end, align 4
  %incdec.ptr = getelementptr inbounds i32, i32* %3, i32 -1
  store i32* %incdec.ptr, i32** %__soon_to_be_end, align 4
  %call2 = call i32* @_ZNSt3__212__to_addressIjEEPT_S2_(i32* %incdec.ptr) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE7destroyIjEEvRS2_PT_(%"class.std::__2::allocator.156"* nonnull align 1 dereferenceable(1) %call, i32* %call2)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %4 = load i32*, i32** %__new_last.addr, align 4
  %__end_3 = getelementptr inbounds %"class.std::__2::__vector_base.152", %"class.std::__2::__vector_base.152"* %this1, i32 0, i32 1
  store i32* %4, i32** %__end_3, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE7destroyIjEEvRS2_PT_(%"class.std::__2::allocator.156"* nonnull align 1 dereferenceable(1) %__a, i32* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.156"*, align 4
  %__p.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy.213", align 1
  store %"class.std::__2::allocator.156"* %__a, %"class.std::__2::allocator.156"** %__a.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy.213"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.156"*, %"class.std::__2::allocator.156"** %__a.addr, align 4
  %2 = load i32*, i32** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE9__destroyIjEEvNS_17integral_constantIbLb1EEERS2_PT_(%"class.std::__2::allocator.156"* nonnull align 1 dereferenceable(1) %1, i32* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE9__destroyIjEEvNS_17integral_constantIbLb1EEERS2_PT_(%"class.std::__2::allocator.156"* nonnull align 1 dereferenceable(1) %__a, i32* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.156"*, align 4
  %__p.addr = alloca i32*, align 4
  store %"class.std::__2::allocator.156"* %__a, %"class.std::__2::allocator.156"** %__a.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.156"*, %"class.std::__2::allocator.156"** %__a.addr, align 4
  %2 = load i32*, i32** %__p.addr, align 4
  call void @_ZNSt3__29allocatorIjE7destroyEPj(%"class.std::__2::allocator.156"* %1, i32* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIjE7destroyEPj(%"class.std::__2::allocator.156"* %this, i32* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.156"*, align 4
  %__p.addr = alloca i32*, align 4
  store %"class.std::__2::allocator.156"* %this, %"class.std::__2::allocator.156"** %this.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.156"*, %"class.std::__2::allocator.156"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIjE10deallocateEPjm(%"class.std::__2::allocator.156"* %this, i32* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.156"*, align 4
  %__p.addr = alloca i32*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.156"* %this, %"class.std::__2::allocator.156"** %this.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.156"*, %"class.std::__2::allocator.156"** %this.addr, align 4
  %0 = load i32*, i32** %__p.addr, align 4
  %1 = bitcast i32* %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { nobuiltin allocsize(0) "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind willreturn }
attributes #4 = { noreturn "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { noinline noreturn nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { cold noreturn nounwind }
attributes #8 = { nounwind }
attributes #9 = { builtin allocsize(0) }
attributes #10 = { builtin nounwind }
attributes #11 = { noreturn }
attributes #12 = { noreturn nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
