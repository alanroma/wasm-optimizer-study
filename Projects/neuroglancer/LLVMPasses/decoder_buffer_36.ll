; ModuleID = './draco/src/draco/core/decoder_buffer.cc'
source_filename = "./draco/src/draco/core/decoder_buffer.cc"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%"class.draco::DecoderBuffer" = type { i8*, i64, i64, %"class.draco::DecoderBuffer::BitDecoder", i8, i16 }
%"class.draco::DecoderBuffer::BitDecoder" = type { i8*, i8*, i32 }

$_ZN5draco13DecoderBuffer6DecodeIyEEbPT_ = comdat any

$_ZN5draco12DecodeVarintIyEEbPT_PNS_13DecoderBufferE = comdat any

$_ZN5draco13DecoderBuffer10BitDecoder5resetEPKvm = comdat any

$_ZNK5draco13DecoderBuffer9data_headEv = comdat any

$_ZNK5draco13DecoderBuffer14remaining_sizeEv = comdat any

$_ZNK5draco13DecoderBuffer10BitDecoder11BitsDecodedEv = comdat any

$_ZN5draco13DecoderBuffer4PeekIyEEbPT_ = comdat any

$_ZN5draco13DecoderBuffer6DecodeIhEEbPT_ = comdat any

$_ZN5draco13DecoderBuffer4PeekIhEEbPT_ = comdat any

@_ZN5draco13DecoderBufferC1Ev = hidden unnamed_addr alias %"class.draco::DecoderBuffer"* (%"class.draco::DecoderBuffer"*), %"class.draco::DecoderBuffer"* (%"class.draco::DecoderBuffer"*)* @_ZN5draco13DecoderBufferC2Ev
@_ZN5draco13DecoderBuffer10BitDecoderC1Ev = hidden unnamed_addr alias %"class.draco::DecoderBuffer::BitDecoder"* (%"class.draco::DecoderBuffer::BitDecoder"*), %"class.draco::DecoderBuffer::BitDecoder"* (%"class.draco::DecoderBuffer::BitDecoder"*)* @_ZN5draco13DecoderBuffer10BitDecoderC2Ev
@_ZN5draco13DecoderBuffer10BitDecoderD1Ev = hidden unnamed_addr alias %"class.draco::DecoderBuffer::BitDecoder"* (%"class.draco::DecoderBuffer::BitDecoder"*), %"class.draco::DecoderBuffer::BitDecoder"* (%"class.draco::DecoderBuffer::BitDecoder"*)* @_ZN5draco13DecoderBuffer10BitDecoderD2Ev

; Function Attrs: noinline nounwind optnone
define hidden %"class.draco::DecoderBuffer"* @_ZN5draco13DecoderBufferC2Ev(%"class.draco::DecoderBuffer"* returned %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  store %"class.draco::DecoderBuffer"* %this, %"class.draco::DecoderBuffer"** %this.addr, align 4
  %this1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %this.addr, align 4
  %data_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 0
  store i8* null, i8** %data_, align 8
  %data_size_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 1
  store i64 0, i64* %data_size_, align 8
  %pos_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  store i64 0, i64* %pos_, align 8
  %bit_decoder_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 3
  %call = call %"class.draco::DecoderBuffer::BitDecoder"* @_ZN5draco13DecoderBuffer10BitDecoderC1Ev(%"class.draco::DecoderBuffer::BitDecoder"* %bit_decoder_)
  %bit_mode_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 4
  store i8 0, i8* %bit_mode_, align 4
  %bitstream_version_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 5
  store i16 0, i16* %bitstream_version_, align 2
  ret %"class.draco::DecoderBuffer"* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN5draco13DecoderBuffer4InitEPKcm(%"class.draco::DecoderBuffer"* %this, i8* %data, i32 %data_size) #0 {
entry:
  %this.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %data.addr = alloca i8*, align 4
  %data_size.addr = alloca i32, align 4
  store %"class.draco::DecoderBuffer"* %this, %"class.draco::DecoderBuffer"** %this.addr, align 4
  store i8* %data, i8** %data.addr, align 4
  store i32 %data_size, i32* %data_size.addr, align 4
  %this1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %this.addr, align 4
  %0 = load i8*, i8** %data.addr, align 4
  %1 = load i32, i32* %data_size.addr, align 4
  %bitstream_version_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 5
  %2 = load i16, i16* %bitstream_version_, align 2
  call void @_ZN5draco13DecoderBuffer4InitEPKcmt(%"class.draco::DecoderBuffer"* %this1, i8* %0, i32 %1, i16 zeroext %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN5draco13DecoderBuffer4InitEPKcmt(%"class.draco::DecoderBuffer"* %this, i8* %data, i32 %data_size, i16 zeroext %version) #0 {
entry:
  %this.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %data.addr = alloca i8*, align 4
  %data_size.addr = alloca i32, align 4
  %version.addr = alloca i16, align 2
  store %"class.draco::DecoderBuffer"* %this, %"class.draco::DecoderBuffer"** %this.addr, align 4
  store i8* %data, i8** %data.addr, align 4
  store i32 %data_size, i32* %data_size.addr, align 4
  store i16 %version, i16* %version.addr, align 2
  %this1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %this.addr, align 4
  %0 = load i8*, i8** %data.addr, align 4
  %data_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 0
  store i8* %0, i8** %data_, align 8
  %1 = load i32, i32* %data_size.addr, align 4
  %conv = zext i32 %1 to i64
  %data_size_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 1
  store i64 %conv, i64* %data_size_, align 8
  %2 = load i16, i16* %version.addr, align 2
  %bitstream_version_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 5
  store i16 %2, i16* %bitstream_version_, align 2
  %pos_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  store i64 0, i64* %pos_, align 8
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden zeroext i1 @_ZN5draco13DecoderBuffer16StartBitDecodingEbPy(%"class.draco::DecoderBuffer"* %this, i1 zeroext %decode_size, i64* %out_size) #0 {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %decode_size.addr = alloca i8, align 1
  %out_size.addr = alloca i64*, align 4
  store %"class.draco::DecoderBuffer"* %this, %"class.draco::DecoderBuffer"** %this.addr, align 4
  %frombool = zext i1 %decode_size to i8
  store i8 %frombool, i8* %decode_size.addr, align 1
  store i64* %out_size, i64** %out_size.addr, align 4
  %this1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %this.addr, align 4
  %0 = load i8, i8* %decode_size.addr, align 1
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.end8

if.then:                                          ; preds = %entry
  %bitstream_version_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 5
  %1 = load i16, i16* %bitstream_version_, align 2
  %conv = zext i16 %1 to i32
  %cmp = icmp slt i32 %conv, 514
  br i1 %cmp, label %if.then2, label %if.else

if.then2:                                         ; preds = %if.then
  %2 = load i64*, i64** %out_size.addr, align 4
  %call = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIyEEbPT_(%"class.draco::DecoderBuffer"* %this1, i64* %2)
  br i1 %call, label %if.end, label %if.then3

if.then3:                                         ; preds = %if.then2
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %if.then2
  br label %if.end7

if.else:                                          ; preds = %if.then
  %3 = load i64*, i64** %out_size.addr, align 4
  %call4 = call zeroext i1 @_ZN5draco12DecodeVarintIyEEbPT_PNS_13DecoderBufferE(i64* %3, %"class.draco::DecoderBuffer"* %this1)
  br i1 %call4, label %if.end6, label %if.then5

if.then5:                                         ; preds = %if.else
  store i1 false, i1* %retval, align 1
  br label %return

if.end6:                                          ; preds = %if.else
  br label %if.end7

if.end7:                                          ; preds = %if.end6, %if.end
  br label %if.end8

if.end8:                                          ; preds = %if.end7, %entry
  %bit_mode_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 4
  store i8 1, i8* %bit_mode_, align 4
  %bit_decoder_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 3
  %call9 = call i8* @_ZNK5draco13DecoderBuffer9data_headEv(%"class.draco::DecoderBuffer"* %this1)
  %call10 = call i64 @_ZNK5draco13DecoderBuffer14remaining_sizeEv(%"class.draco::DecoderBuffer"* %this1)
  %conv11 = trunc i64 %call10 to i32
  call void @_ZN5draco13DecoderBuffer10BitDecoder5resetEPKvm(%"class.draco::DecoderBuffer::BitDecoder"* %bit_decoder_, i8* %call9, i32 %conv11)
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end8, %if.then5, %if.then3
  %4 = load i1, i1* %retval, align 1
  ret i1 %4
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIyEEbPT_(%"class.draco::DecoderBuffer"* %this, i64* %out_val) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %out_val.addr = alloca i64*, align 4
  store %"class.draco::DecoderBuffer"* %this, %"class.draco::DecoderBuffer"** %this.addr, align 4
  store i64* %out_val, i64** %out_val.addr, align 4
  %this1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %this.addr, align 4
  %0 = load i64*, i64** %out_val.addr, align 4
  %call = call zeroext i1 @_ZN5draco13DecoderBuffer4PeekIyEEbPT_(%"class.draco::DecoderBuffer"* %this1, i64* %0)
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %pos_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %1 = load i64, i64* %pos_, align 8
  %add = add nsw i64 %1, 8
  store i64 %add, i64* %pos_, align 8
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %2 = load i1, i1* %retval, align 1
  ret i1 %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco12DecodeVarintIyEEbPT_PNS_13DecoderBufferE(i64* %out_val, %"class.draco::DecoderBuffer"* %buffer) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %out_val.addr = alloca i64*, align 4
  %buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %in = alloca i8, align 1
  store i64* %out_val, i64** %out_val.addr, align 4
  store %"class.draco::DecoderBuffer"* %buffer, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %0 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIhEEbPT_(%"class.draco::DecoderBuffer"* %0, i8* %in)
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %1 = load i8, i8* %in, align 1
  %conv = zext i8 %1 to i32
  %and = and i32 %conv, 128
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then1, label %if.else

if.then1:                                         ; preds = %if.end
  %2 = load i64*, i64** %out_val.addr, align 4
  %3 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call2 = call zeroext i1 @_ZN5draco12DecodeVarintIyEEbPT_PNS_13DecoderBufferE(i64* %2, %"class.draco::DecoderBuffer"* %3)
  br i1 %call2, label %if.end4, label %if.then3

if.then3:                                         ; preds = %if.then1
  store i1 false, i1* %retval, align 1
  br label %return

if.end4:                                          ; preds = %if.then1
  %4 = load i64*, i64** %out_val.addr, align 4
  %5 = load i64, i64* %4, align 8
  %shl = shl i64 %5, 7
  store i64 %shl, i64* %4, align 8
  %6 = load i8, i8* %in, align 1
  %conv5 = zext i8 %6 to i32
  %and6 = and i32 %conv5, 127
  %conv7 = sext i32 %and6 to i64
  %7 = load i64*, i64** %out_val.addr, align 4
  %8 = load i64, i64* %7, align 8
  %or = or i64 %8, %conv7
  store i64 %or, i64* %7, align 8
  br label %if.end9

if.else:                                          ; preds = %if.end
  %9 = load i8, i8* %in, align 1
  %conv8 = zext i8 %9 to i64
  %10 = load i64*, i64** %out_val.addr, align 4
  store i64 %conv8, i64* %10, align 8
  br label %if.end9

if.end9:                                          ; preds = %if.else, %if.end4
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end9, %if.then3, %if.then
  %11 = load i1, i1* %retval, align 1
  ret i1 %11
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco13DecoderBuffer10BitDecoder5resetEPKvm(%"class.draco::DecoderBuffer::BitDecoder"* %this, i8* %b, i32 %s) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::DecoderBuffer::BitDecoder"*, align 4
  %b.addr = alloca i8*, align 4
  %s.addr = alloca i32, align 4
  store %"class.draco::DecoderBuffer::BitDecoder"* %this, %"class.draco::DecoderBuffer::BitDecoder"** %this.addr, align 4
  store i8* %b, i8** %b.addr, align 4
  store i32 %s, i32* %s.addr, align 4
  %this1 = load %"class.draco::DecoderBuffer::BitDecoder"*, %"class.draco::DecoderBuffer::BitDecoder"** %this.addr, align 4
  %bit_offset_ = getelementptr inbounds %"class.draco::DecoderBuffer::BitDecoder", %"class.draco::DecoderBuffer::BitDecoder"* %this1, i32 0, i32 2
  store i32 0, i32* %bit_offset_, align 4
  %0 = load i8*, i8** %b.addr, align 4
  %bit_buffer_ = getelementptr inbounds %"class.draco::DecoderBuffer::BitDecoder", %"class.draco::DecoderBuffer::BitDecoder"* %this1, i32 0, i32 0
  store i8* %0, i8** %bit_buffer_, align 4
  %bit_buffer_2 = getelementptr inbounds %"class.draco::DecoderBuffer::BitDecoder", %"class.draco::DecoderBuffer::BitDecoder"* %this1, i32 0, i32 0
  %1 = load i8*, i8** %bit_buffer_2, align 4
  %2 = load i32, i32* %s.addr, align 4
  %add.ptr = getelementptr inbounds i8, i8* %1, i32 %2
  %bit_buffer_end_ = getelementptr inbounds %"class.draco::DecoderBuffer::BitDecoder", %"class.draco::DecoderBuffer::BitDecoder"* %this1, i32 0, i32 1
  store i8* %add.ptr, i8** %bit_buffer_end_, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNK5draco13DecoderBuffer9data_headEv(%"class.draco::DecoderBuffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  store %"class.draco::DecoderBuffer"* %this, %"class.draco::DecoderBuffer"** %this.addr, align 4
  %this1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %this.addr, align 4
  %data_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 0
  %0 = load i8*, i8** %data_, align 8
  %pos_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %1 = load i64, i64* %pos_, align 8
  %idx.ext = trunc i64 %1 to i32
  %add.ptr = getelementptr inbounds i8, i8* %0, i32 %idx.ext
  ret i8* %add.ptr
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i64 @_ZNK5draco13DecoderBuffer14remaining_sizeEv(%"class.draco::DecoderBuffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  store %"class.draco::DecoderBuffer"* %this, %"class.draco::DecoderBuffer"** %this.addr, align 4
  %this1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %this.addr, align 4
  %data_size_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 1
  %0 = load i64, i64* %data_size_, align 8
  %pos_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %1 = load i64, i64* %pos_, align 8
  %sub = sub nsw i64 %0, %1
  ret i64 %sub
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN5draco13DecoderBuffer14EndBitDecodingEv(%"class.draco::DecoderBuffer"* %this) #0 {
entry:
  %this.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %bits_decoded = alloca i64, align 8
  %bytes_decoded = alloca i64, align 8
  store %"class.draco::DecoderBuffer"* %this, %"class.draco::DecoderBuffer"** %this.addr, align 4
  %this1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %this.addr, align 4
  %bit_mode_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 4
  store i8 0, i8* %bit_mode_, align 4
  %bit_decoder_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 3
  %call = call i64 @_ZNK5draco13DecoderBuffer10BitDecoder11BitsDecodedEv(%"class.draco::DecoderBuffer::BitDecoder"* %bit_decoder_)
  store i64 %call, i64* %bits_decoded, align 8
  %0 = load i64, i64* %bits_decoded, align 8
  %add = add i64 %0, 7
  %div = udiv i64 %add, 8
  store i64 %div, i64* %bytes_decoded, align 8
  %1 = load i64, i64* %bytes_decoded, align 8
  %pos_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %2 = load i64, i64* %pos_, align 8
  %add2 = add i64 %2, %1
  store i64 %add2, i64* %pos_, align 8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i64 @_ZNK5draco13DecoderBuffer10BitDecoder11BitsDecodedEv(%"class.draco::DecoderBuffer::BitDecoder"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::DecoderBuffer::BitDecoder"*, align 4
  store %"class.draco::DecoderBuffer::BitDecoder"* %this, %"class.draco::DecoderBuffer::BitDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::DecoderBuffer::BitDecoder"*, %"class.draco::DecoderBuffer::BitDecoder"** %this.addr, align 4
  %bit_offset_ = getelementptr inbounds %"class.draco::DecoderBuffer::BitDecoder", %"class.draco::DecoderBuffer::BitDecoder"* %this1, i32 0, i32 2
  %0 = load i32, i32* %bit_offset_, align 4
  %conv = zext i32 %0 to i64
  ret i64 %conv
}

; Function Attrs: noinline nounwind optnone
define hidden %"class.draco::DecoderBuffer::BitDecoder"* @_ZN5draco13DecoderBuffer10BitDecoderC2Ev(%"class.draco::DecoderBuffer::BitDecoder"* returned %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %"class.draco::DecoderBuffer::BitDecoder"*, align 4
  store %"class.draco::DecoderBuffer::BitDecoder"* %this, %"class.draco::DecoderBuffer::BitDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::DecoderBuffer::BitDecoder"*, %"class.draco::DecoderBuffer::BitDecoder"** %this.addr, align 4
  %bit_buffer_ = getelementptr inbounds %"class.draco::DecoderBuffer::BitDecoder", %"class.draco::DecoderBuffer::BitDecoder"* %this1, i32 0, i32 0
  store i8* null, i8** %bit_buffer_, align 4
  %bit_buffer_end_ = getelementptr inbounds %"class.draco::DecoderBuffer::BitDecoder", %"class.draco::DecoderBuffer::BitDecoder"* %this1, i32 0, i32 1
  store i8* null, i8** %bit_buffer_end_, align 4
  %bit_offset_ = getelementptr inbounds %"class.draco::DecoderBuffer::BitDecoder", %"class.draco::DecoderBuffer::BitDecoder"* %this1, i32 0, i32 2
  store i32 0, i32* %bit_offset_, align 4
  ret %"class.draco::DecoderBuffer::BitDecoder"* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden %"class.draco::DecoderBuffer::BitDecoder"* @_ZN5draco13DecoderBuffer10BitDecoderD2Ev(%"class.draco::DecoderBuffer::BitDecoder"* returned %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %"class.draco::DecoderBuffer::BitDecoder"*, align 4
  store %"class.draco::DecoderBuffer::BitDecoder"* %this, %"class.draco::DecoderBuffer::BitDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::DecoderBuffer::BitDecoder"*, %"class.draco::DecoderBuffer::BitDecoder"** %this.addr, align 4
  ret %"class.draco::DecoderBuffer::BitDecoder"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco13DecoderBuffer4PeekIyEEbPT_(%"class.draco::DecoderBuffer"* %this, i64* %out_val) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %out_val.addr = alloca i64*, align 4
  %size_to_decode = alloca i32, align 4
  store %"class.draco::DecoderBuffer"* %this, %"class.draco::DecoderBuffer"** %this.addr, align 4
  store i64* %out_val, i64** %out_val.addr, align 4
  %this1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %this.addr, align 4
  store i32 8, i32* %size_to_decode, align 4
  %data_size_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 1
  %0 = load i64, i64* %data_size_, align 8
  %pos_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %1 = load i64, i64* %pos_, align 8
  %add = add nsw i64 %1, 8
  %cmp = icmp slt i64 %0, %add
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %2 = load i64*, i64** %out_val.addr, align 4
  %3 = bitcast i64* %2 to i8*
  %data_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 0
  %4 = load i8*, i8** %data_, align 8
  %pos_2 = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %5 = load i64, i64* %pos_2, align 8
  %idx.ext = trunc i64 %5 to i32
  %add.ptr = getelementptr inbounds i8, i8* %4, i32 %idx.ext
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %3, i8* align 1 %add.ptr, i32 8, i1 false)
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %6 = load i1, i1* %retval, align 1
  ret i1 %6
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIhEEbPT_(%"class.draco::DecoderBuffer"* %this, i8* %out_val) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %out_val.addr = alloca i8*, align 4
  store %"class.draco::DecoderBuffer"* %this, %"class.draco::DecoderBuffer"** %this.addr, align 4
  store i8* %out_val, i8** %out_val.addr, align 4
  %this1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %this.addr, align 4
  %0 = load i8*, i8** %out_val.addr, align 4
  %call = call zeroext i1 @_ZN5draco13DecoderBuffer4PeekIhEEbPT_(%"class.draco::DecoderBuffer"* %this1, i8* %0)
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %pos_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %1 = load i64, i64* %pos_, align 8
  %add = add nsw i64 %1, 1
  store i64 %add, i64* %pos_, align 8
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %2 = load i1, i1* %retval, align 1
  ret i1 %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco13DecoderBuffer4PeekIhEEbPT_(%"class.draco::DecoderBuffer"* %this, i8* %out_val) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %out_val.addr = alloca i8*, align 4
  %size_to_decode = alloca i32, align 4
  store %"class.draco::DecoderBuffer"* %this, %"class.draco::DecoderBuffer"** %this.addr, align 4
  store i8* %out_val, i8** %out_val.addr, align 4
  %this1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %this.addr, align 4
  store i32 1, i32* %size_to_decode, align 4
  %data_size_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 1
  %0 = load i64, i64* %data_size_, align 8
  %pos_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %1 = load i64, i64* %pos_, align 8
  %add = add nsw i64 %1, 1
  %cmp = icmp slt i64 %0, %add
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %2 = load i8*, i8** %out_val.addr, align 4
  %data_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 0
  %3 = load i8*, i8** %data_, align 8
  %pos_2 = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %4 = load i64, i64* %pos_2, align 8
  %idx.ext = trunc i64 %4 to i32
  %add.ptr = getelementptr inbounds i8, i8* %3, i32 %idx.ext
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %2, i8* align 1 %add.ptr, i32 1, i1 false)
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %5 = load i1, i1* %retval, align 1
  ret i1 %5
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
