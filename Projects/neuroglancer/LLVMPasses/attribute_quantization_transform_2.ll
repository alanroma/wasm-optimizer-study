; ModuleID = './draco/src/draco/attributes/attribute_quantization_transform.cc'
source_filename = "./draco/src/draco/attributes/attribute_quantization_transform.cc"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%"class.draco::AttributeQuantizationTransform" = type { %"class.draco::AttributeTransform", i32, %"class.std::__2::vector", float }
%"class.draco::AttributeTransform" = type { i32 (...)** }
%"class.std::__2::vector" = type { %"class.std::__2::__vector_base" }
%"class.std::__2::__vector_base" = type { float*, float*, %"class.std::__2::__compressed_pair" }
%"class.std::__2::__compressed_pair" = type { %"struct.std::__2::__compressed_pair_elem" }
%"struct.std::__2::__compressed_pair_elem" = type { float* }
%"class.draco::PointAttribute" = type <{ %"class.draco::GeometryAttribute", %"class.std::__2::unique_ptr", %"class.draco::IndexTypeVector", i32, i8, [3 x i8], %"class.std::__2::unique_ptr.18", [4 x i8] }>
%"class.draco::GeometryAttribute" = type { %"class.draco::DataBuffer"*, %"struct.draco::DataBufferDescriptor", i8, i32, i8, i64, i64, i32, i32 }
%"class.draco::DataBuffer" = type { %"class.std::__2::vector.1", %"struct.draco::DataBufferDescriptor" }
%"class.std::__2::vector.1" = type { %"class.std::__2::__vector_base.2" }
%"class.std::__2::__vector_base.2" = type { i8*, i8*, %"class.std::__2::__compressed_pair.3" }
%"class.std::__2::__compressed_pair.3" = type { %"struct.std::__2::__compressed_pair_elem.4" }
%"struct.std::__2::__compressed_pair_elem.4" = type { i8* }
%"struct.draco::DataBufferDescriptor" = type { i64, i64 }
%"class.std::__2::unique_ptr" = type { %"class.std::__2::__compressed_pair.8" }
%"class.std::__2::__compressed_pair.8" = type { %"struct.std::__2::__compressed_pair_elem.9" }
%"struct.std::__2::__compressed_pair_elem.9" = type { %"class.draco::DataBuffer"* }
%"class.draco::IndexTypeVector" = type { %"class.std::__2::vector.11" }
%"class.std::__2::vector.11" = type { %"class.std::__2::__vector_base.12" }
%"class.std::__2::__vector_base.12" = type { %"class.draco::IndexType"*, %"class.draco::IndexType"*, %"class.std::__2::__compressed_pair.13" }
%"class.draco::IndexType" = type { i32 }
%"class.std::__2::__compressed_pair.13" = type { %"struct.std::__2::__compressed_pair_elem.14" }
%"struct.std::__2::__compressed_pair_elem.14" = type { %"class.draco::IndexType"* }
%"class.std::__2::unique_ptr.18" = type { %"class.std::__2::__compressed_pair.19" }
%"class.std::__2::__compressed_pair.19" = type { %"struct.std::__2::__compressed_pair_elem.20" }
%"struct.std::__2::__compressed_pair_elem.20" = type { %"class.draco::AttributeTransformData"* }
%"class.draco::AttributeTransformData" = type { i32, %"class.draco::DataBuffer" }
%"class.std::__2::unique_ptr.23" = type { %"class.std::__2::__compressed_pair.24" }
%"class.std::__2::__compressed_pair.24" = type { %"struct.std::__2::__compressed_pair_elem" }
%"struct.std::__2::integral_constant" = type { i8 }
%"struct.std::__2::__default_init_tag" = type { i8 }
%"class.draco::EncoderBuffer" = type <{ %"class.std::__2::vector.27", %"class.std::__2::unique_ptr.34", i64, i8, [7 x i8] }>
%"class.std::__2::vector.27" = type { %"class.std::__2::__vector_base.28" }
%"class.std::__2::__vector_base.28" = type { i8*, i8*, %"class.std::__2::__compressed_pair.29" }
%"class.std::__2::__compressed_pair.29" = type { %"struct.std::__2::__compressed_pair_elem.30" }
%"struct.std::__2::__compressed_pair_elem.30" = type { i8* }
%"class.std::__2::unique_ptr.34" = type { %"class.std::__2::__compressed_pair.35" }
%"class.std::__2::__compressed_pair.35" = type { %"struct.std::__2::__compressed_pair_elem.36" }
%"struct.std::__2::__compressed_pair_elem.36" = type { %"class.draco::EncoderBuffer::BitEncoder"* }
%"class.draco::EncoderBuffer::BitEncoder" = type { i8*, i32 }
%"class.std::__2::__wrap_iter" = type { i8* }
%"class.std::__2::__wrap_iter.52" = type { i8* }
%"class.std::__2::unique_ptr.39" = type { %"class.std::__2::__compressed_pair.40" }
%"class.std::__2::__compressed_pair.40" = type { %"struct.std::__2::__compressed_pair_elem.41" }
%"struct.std::__2::__compressed_pair_elem.41" = type { %"class.draco::PointAttribute"* }
%"class.draco::Quantizer" = type { float }
%"class.draco::IndexType.44" = type { i32 }
%"class.std::__2::vector.45" = type { %"class.std::__2::__vector_base.46" }
%"class.std::__2::__vector_base.46" = type { %"class.draco::IndexType.44"*, %"class.draco::IndexType.44"*, %"class.std::__2::__compressed_pair.47" }
%"class.std::__2::__compressed_pair.47" = type { %"struct.std::__2::__compressed_pair_elem.48" }
%"struct.std::__2::__compressed_pair_elem.48" = type { %"class.draco::IndexType.44"* }
%"class.std::__2::allocator.32" = type { i8 }
%"struct.std::__2::__split_buffer" = type { i8*, i8*, i8*, %"class.std::__2::__compressed_pair.53" }
%"class.std::__2::__compressed_pair.53" = type { %"struct.std::__2::__compressed_pair_elem.30", %"struct.std::__2::__compressed_pair_elem.54" }
%"struct.std::__2::__compressed_pair_elem.54" = type { %"class.std::__2::allocator.32"* }
%"struct.std::__2::random_access_iterator_tag" = type { i8 }
%"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction" = type { %"class.std::__2::vector.27"*, i8*, i8* }
%"class.std::__2::__vector_base_common" = type { i8 }
%"class.std::__2::__split_buffer_common" = type { i8 }
%"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction" = type { i8*, i8*, i8** }
%"struct.std::__2::__has_construct" = type { i8 }
%"struct.std::__2::__has_construct.55" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.31" = type { i8 }
%"struct.std::__2::__less" = type { i8 }
%"struct.std::__2::__has_max_size" = type { i8 }
%"struct.std::__2::integral_constant.56" = type { i8 }
%"struct.std::__2::__has_destroy" = type { i8 }
%"class.std::__2::allocator" = type { i8 }
%"struct.std::__2::__has_destroy.57" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.0" = type { i8 }
%"struct.std::__2::__split_buffer.58" = type { float*, float*, float*, %"class.std::__2::__compressed_pair.59" }
%"class.std::__2::__compressed_pair.59" = type { %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem.60" }
%"struct.std::__2::__compressed_pair_elem.60" = type { %"class.std::__2::allocator"* }
%"struct.std::__2::vector<float, std::__2::allocator<float>>::_ConstructTransaction" = type { %"class.std::__2::vector"*, float*, float* }
%"struct.std::__2::__split_buffer<float, std::__2::allocator<float> &>::_ConstructTransaction" = type { float*, float*, float** }
%"struct.std::__2::__has_construct.61" = type { i8 }
%"struct.std::__2::__has_max_size.62" = type { i8 }
%"struct.std::__2::__has_construct.63" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.25" = type { i8 }
%"struct.std::__2::default_delete.26" = type { i8 }
%"struct.std::__2::default_delete.43" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.42" = type { i8 }
%"struct.std::__2::default_delete.22" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.21" = type { i8 }
%"class.std::__2::allocator.6" = type { i8 }
%"struct.std::__2::__has_destroy.64" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.5" = type { i8 }
%"class.std::__2::allocator.16" = type { i8 }
%"struct.std::__2::__has_destroy.65" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.15" = type { i8 }
%"struct.std::__2::default_delete" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.10" = type { i8 }

$_ZNK5draco14PointAttribute25GetAttributeTransformDataEv = comdat any

$_ZNK5draco22AttributeTransformData14transform_typeEv = comdat any

$_ZNK5draco22AttributeTransformData17GetParameterValueIiEET_i = comdat any

$_ZNSt3__26vectorIfNS_9allocatorIfEEE6resizeEm = comdat any

$_ZNK5draco17GeometryAttribute14num_componentsEv = comdat any

$_ZNK5draco22AttributeTransformData17GetParameterValueIfEET_i = comdat any

$_ZNSt3__26vectorIfNS_9allocatorIfEEEixEm = comdat any

$_ZN5draco22AttributeTransformData18set_transform_typeENS_22AttributeTransformTypeE = comdat any

$_ZN5draco22AttributeTransformData20AppendParameterValueIiEEvRKT_ = comdat any

$_ZNKSt3__26vectorIfNS_9allocatorIfEEE4sizeEv = comdat any

$_ZN5draco22AttributeTransformData20AppendParameterValueIfEEvRKT_ = comdat any

$_ZNKSt3__26vectorIfNS_9allocatorIfEEEixEm = comdat any

$_ZNSt3__26vectorIfNS_9allocatorIfEEE6assignIPKfEENS_9enable_ifIXaasr27__is_cpp17_forward_iteratorIT_EE5valuesr16is_constructibleIfNS_15iterator_traitsIS8_E9referenceEEE5valueEvE4typeES8_S8_ = comdat any

$_ZNSt3__26vectorIfNS_9allocatorIfEEEC2EmRKf = comdat any

$_ZNSt3__26vectorIfNS_9allocatorIfEEEaSEOS3_ = comdat any

$_ZNSt3__26vectorIfNS_9allocatorIfEEED2Ev = comdat any

$_ZNSt3__210unique_ptrIA_fNS_14default_deleteIS1_EEEC2IPfLb1EvvEET_ = comdat any

$_ZNK5draco17GeometryAttribute8GetValueENS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEEPv = comdat any

$_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEC2Ej = comdat any

$_ZNKSt3__210unique_ptrIA_fNS_14default_deleteIS1_EEE3getEv = comdat any

$_ZNSt3__26vectorIfNS_9allocatorIfEEE4dataEv = comdat any

$_ZNK5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEltERKj = comdat any

$_ZNK5draco14PointAttribute4sizeEv = comdat any

$_ZNKSt3__210unique_ptrIA_fNS_14default_deleteIS1_EEEixEm = comdat any

$_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEppEv = comdat any

$_ZNSt3__210unique_ptrIA_fNS_14default_deleteIS1_EEED2Ev = comdat any

$_ZNK5draco30AttributeQuantizationTransform14is_initializedEv = comdat any

$_ZN5draco13EncoderBuffer6EncodeEPKvm = comdat any

$_ZNKSt3__26vectorIfNS_9allocatorIfEEE4dataEv = comdat any

$_ZN5draco13EncoderBuffer6EncodeIfEEbRKT_ = comdat any

$_ZN5draco13EncoderBuffer6EncodeIhEEbRKT_ = comdat any

$_ZNKSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEEptEv = comdat any

$_ZN5draco17GeometryAttribute10GetAddressENS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEE = comdat any

$_ZNK5draco30AttributeQuantizationTransform5rangeEv = comdat any

$_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEC2Ej = comdat any

$_ZNK5draco9IndexTypeIjNS_20PointIndex_tag_type_EEltERKj = comdat any

$_ZNK5draco14PointAttribute12mapped_indexENS_9IndexTypeIjNS_20PointIndex_tag_type_EEE = comdat any

$_ZNK5draco30AttributeQuantizationTransform10min_valuesEv = comdat any

$_ZNK5draco9Quantizer13QuantizeFloatEf = comdat any

$_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEppEv = comdat any

$_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEED2Ev = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEEixEm = comdat any

$_ZN5draco30AttributeQuantizationTransformD2Ev = comdat any

$_ZN5draco30AttributeQuantizationTransformD0Ev = comdat any

$_ZNK5draco30AttributeQuantizationTransform4TypeEv = comdat any

$_ZNKSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE3getEv = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPN5draco22AttributeTransformDataELi0ELb0EE5__getEv = comdat any

$_ZNK5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EE5valueEv = comdat any

$_ZNK5draco10DataBuffer4ReadExPvm = comdat any

$_ZNK5draco10DataBuffer4dataEv = comdat any

$_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv = comdat any

$_ZNSt3__212__to_addressIhEEPT_S2_ = comdat any

$_ZNK5draco13EncoderBuffer18bit_encoder_activeEv = comdat any

$_ZNSt3__26vectorIcNS_9allocatorIcEEE6insertIPKhEENS_9enable_ifIXaasr27__is_cpp17_forward_iteratorIT_EE5valuesr16is_constructibleIcNS_15iterator_traitsIS8_E9referenceEEE5valueENS_11__wrap_iterIPcEEE4typeENSC_IPKcEES8_S8_ = comdat any

$_ZNSt3__26vectorIcNS_9allocatorIcEEE3endEv = comdat any

$_ZNSt3__211__wrap_iterIPKcEC2IPcEERKNS0_IT_EEPNS_9enable_ifIXsr14is_convertibleIS6_S2_EE5valueEvE4typeE = comdat any

$_ZNSt3__2miIPKcPcEEDTmicldtfp_4baseEcldtfp0_4baseEERKNS_11__wrap_iterIT_EERKNS5_IT0_EE = comdat any

$_ZNSt3__26vectorIcNS_9allocatorIcEEE5beginEv = comdat any

$_ZNSt3__28distanceIPKhEENS_15iterator_traitsIT_E15difference_typeES4_S4_ = comdat any

$_ZNSt3__213__vector_baseIcNS_9allocatorIcEEE9__end_capEv = comdat any

$_ZNSt3__27advanceIPKhEEvRT_NS_15iterator_traitsIS3_E15difference_typeE = comdat any

$_ZNSt3__26vectorIcNS_9allocatorIcEEE18__construct_at_endIPKhEENS_9enable_ifIXsr27__is_cpp17_forward_iteratorIT_EE5valueEvE4typeES8_S8_m = comdat any

$_ZNSt3__26vectorIcNS_9allocatorIcEEE12__move_rangeEPcS4_S4_ = comdat any

$_ZNSt3__24copyIPKhPcEET0_T_S5_S4_ = comdat any

$_ZNSt3__213__vector_baseIcNS_9allocatorIcEEE7__allocEv = comdat any

$_ZNKSt3__26vectorIcNS_9allocatorIcEEE11__recommendEm = comdat any

$_ZNKSt3__26vectorIcNS_9allocatorIcEEE4sizeEv = comdat any

$_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEEC2EmmS3_ = comdat any

$_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE18__construct_at_endIPKhEENS_9enable_ifIXsr27__is_cpp17_forward_iteratorIT_EE5valueEvE4typeES9_S9_ = comdat any

$_ZNSt3__26vectorIcNS_9allocatorIcEEE26__swap_out_circular_bufferERNS_14__split_bufferIcRS2_EEPc = comdat any

$_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEED2Ev = comdat any

$_ZNSt3__26vectorIcNS_9allocatorIcEEE11__make_iterEPc = comdat any

$_ZNKSt3__211__wrap_iterIPKcE4baseEv = comdat any

$_ZNKSt3__211__wrap_iterIPcE4baseEv = comdat any

$_ZNSt3__210__distanceIPKhEENS_15iterator_traitsIT_E15difference_typeES4_S4_NS_26random_access_iterator_tagE = comdat any

$_ZNSt3__217__compressed_pairIPcNS_9allocatorIcEEE5firstEv = comdat any

$_ZNSt3__222__compressed_pair_elemIPcLi0ELb0EE5__getEv = comdat any

$_ZNSt3__29__advanceIPKhEEvRT_NS_15iterator_traitsIS3_E15difference_typeENS_26random_access_iterator_tagE = comdat any

$_ZNSt3__26vectorIcNS_9allocatorIcEEE21_ConstructTransactionC2ERS3_m = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIcEEE25__construct_range_forwardIPKhPcEEvRS2_T_S9_RT0_ = comdat any

$_ZNSt3__26vectorIcNS_9allocatorIcEEE21_ConstructTransactionD2Ev = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIcEEE9constructIcJRKhEEEvRS2_PT_DpOT0_ = comdat any

$_ZNSt3__212__to_addressIcEEPT_S2_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIcEEE11__constructIcJRKhEEEvNS_17integral_constantIbLb1EEERS2_PT_DpOT0_ = comdat any

$_ZNSt3__27forwardIRKhEEOT_RNS_16remove_referenceIS3_E4typeE = comdat any

$_ZNSt3__29allocatorIcE9constructIcJRKhEEEvPT_DpOT0_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIcEEE9constructIcJcEEEvRS2_PT_DpOT0_ = comdat any

$_ZNSt3__24moveIRcEEONS_16remove_referenceIT_E4typeEOS3_ = comdat any

$_ZNSt3__213move_backwardIPcS1_EET0_T_S3_S2_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIcEEE11__constructIcJcEEEvNS_17integral_constantIbLb1EEERS2_PT_DpOT0_ = comdat any

$_ZNSt3__27forwardIcEEOT_RNS_16remove_referenceIS1_E4typeE = comdat any

$_ZNSt3__29allocatorIcE9constructIcJcEEEvPT_DpOT0_ = comdat any

$_ZNSt3__215__move_backwardIccEENS_9enable_ifIXaasr7is_sameINS_12remove_constIT_E4typeET0_EE5valuesr28is_trivially_copy_assignableIS6_EE5valueEPS6_E4typeEPS3_SA_S7_ = comdat any

$_ZNSt3__213__unwrap_iterIPcEET_S2_ = comdat any

$_ZNSt3__26__copyIPKhPcEET0_T_S5_S4_ = comdat any

$_ZNSt3__213__unwrap_iterIPKhEET_S3_ = comdat any

$_ZNSt3__216__copy_constexprIPKhPcEET0_T_S5_S4_ = comdat any

$_ZNSt3__217__compressed_pairIPcNS_9allocatorIcEEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIcEELi1ELb1EE5__getEv = comdat any

$_ZNKSt3__26vectorIcNS_9allocatorIcEEE8max_sizeEv = comdat any

$_ZNKSt3__26vectorIcNS_9allocatorIcEEE8capacityEv = comdat any

$_ZNSt3__23maxImEERKT_S3_S3_ = comdat any

$_ZNSt3__23minImEERKT_S3_S3_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIcEEE8max_sizeERKS2_ = comdat any

$_ZNKSt3__213__vector_baseIcNS_9allocatorIcEEE7__allocEv = comdat any

$_ZNSt3__214numeric_limitsIlE3maxEv = comdat any

$_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_ = comdat any

$_ZNKSt3__26__lessImmEclERKmS3_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIcEEE10__max_sizeENS_17integral_constantIbLb1EEERKS2_ = comdat any

$_ZNKSt3__29allocatorIcE8max_sizeEv = comdat any

$_ZNKSt3__217__compressed_pairIPcNS_9allocatorIcEEE6secondEv = comdat any

$_ZNKSt3__222__compressed_pair_elemINS_9allocatorIcEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv = comdat any

$_ZNKSt3__213__vector_baseIcNS_9allocatorIcEEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseIcNS_9allocatorIcEEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPcNS_9allocatorIcEEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPcLi0ELb0EE5__getEv = comdat any

$_ZNSt3__23maxImNS_6__lessImmEEEERKT_S5_S5_T0_ = comdat any

$_ZNSt3__217__compressed_pairIPcRNS_9allocatorIcEEEC2IDnS4_EEOT_OT0_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIcEEE8allocateERS2_m = comdat any

$_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE7__allocEv = comdat any

$_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE9__end_capEv = comdat any

$_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPcLi0ELb0EEC2IDnvEEOT_ = comdat any

$_ZNSt3__27forwardIRNS_9allocatorIcEEEEOT_RNS_16remove_referenceIS4_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIcEELi1ELb0EEC2IS3_vEEOT_ = comdat any

$_ZNSt3__29allocatorIcE8allocateEmPKv = comdat any

$_ZNSt3__220__throw_length_errorEPKc = comdat any

$_ZNSt3__217__libcpp_allocateEmm = comdat any

$_ZNSt3__217__compressed_pairIPcRNS_9allocatorIcEEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIcEELi1ELb0EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPcRNS_9allocatorIcEEE5firstEv = comdat any

$_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE21_ConstructTransactionC2EPPcm = comdat any

$_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE21_ConstructTransactionD2Ev = comdat any

$_ZNKSt3__26vectorIcNS_9allocatorIcEEE17__annotate_deleteEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIcEEE46__construct_backward_with_exception_guaranteesIcEENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS2_PT_S8_EE5valuesr31is_trivially_move_constructibleIS8_EE5valueEvE4typeERS2_S9_S9_RS9_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIcEEE45__construct_forward_with_exception_guaranteesIcEENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS2_PT_S8_EE5valuesr31is_trivially_move_constructibleIS8_EE5valueEvE4typeERS2_S9_S9_RS9_ = comdat any

$_ZNSt3__24swapIPcEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_ = comdat any

$_ZNKSt3__26vectorIcNS_9allocatorIcEEE14__annotate_newEm = comdat any

$_ZNSt3__26vectorIcNS_9allocatorIcEEE26__invalidate_all_iteratorsEv = comdat any

$_ZNKSt3__26vectorIcNS_9allocatorIcEEE31__annotate_contiguous_containerEPKvS5_S5_S5_ = comdat any

$_ZNKSt3__26vectorIcNS_9allocatorIcEEE4dataEv = comdat any

$_ZNSt3__24moveIRPcEEONS_16remove_referenceIT_E4typeEOS4_ = comdat any

$_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE5clearEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIcEEE10deallocateERS2_Pcm = comdat any

$_ZNKSt3__214__split_bufferIcRNS_9allocatorIcEEE8capacityEv = comdat any

$_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE17__destruct_at_endEPc = comdat any

$_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE17__destruct_at_endEPcNS_17integral_constantIbLb0EEE = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIcEEE7destroyIcEEvRS2_PT_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIcEEE9__destroyIcEEvNS_17integral_constantIbLb1EEERS2_PT_ = comdat any

$_ZNSt3__29allocatorIcE7destroyEPc = comdat any

$_ZNSt3__29allocatorIcE10deallocateEPcm = comdat any

$_ZNSt3__219__libcpp_deallocateEPvmm = comdat any

$_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm = comdat any

$_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm = comdat any

$_ZNSt3__217_DeallocateCaller9__do_callEPv = comdat any

$_ZNKSt3__214__split_bufferIcRNS_9allocatorIcEEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPcRNS_9allocatorIcEEE5firstEv = comdat any

$_ZNSt3__211__wrap_iterIPcEC2ES1_ = comdat any

$_ZNK5draco17GeometryAttribute10GetBytePosENS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEE = comdat any

$_ZN5draco10DataBuffer4dataEv = comdat any

$_ZNSt3__26vectorIhNS_9allocatorIhEEEixEm = comdat any

$_ZNK5draco9IndexTypeIjNS_20PointIndex_tag_type_EE5valueEv = comdat any

$_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEEEixERKS3_ = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEEixEm = comdat any

$_Z5floorf = comdat any

$_ZN5draco18AttributeTransformD2Ev = comdat any

$_ZNKSt3__26vectorIfNS_9allocatorIfEEE17__annotate_deleteEv = comdat any

$_ZNSt3__213__vector_baseIfNS_9allocatorIfEEED2Ev = comdat any

$_ZNKSt3__26vectorIfNS_9allocatorIfEEE31__annotate_contiguous_containerEPKvS5_S5_S5_ = comdat any

$_ZNKSt3__26vectorIfNS_9allocatorIfEEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseIfNS_9allocatorIfEEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseIfNS_9allocatorIfEEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPfNS_9allocatorIfEEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPfLi0ELb0EE5__getEv = comdat any

$_ZNSt3__213__vector_baseIfNS_9allocatorIfEEE5clearEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIfEEE10deallocateERS2_Pfm = comdat any

$_ZNSt3__213__vector_baseIfNS_9allocatorIfEEE7__allocEv = comdat any

$_ZNSt3__213__vector_baseIfNS_9allocatorIfEEE17__destruct_at_endEPf = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIfEEE7destroyIfEEvRS2_PT_ = comdat any

$_ZNSt3__212__to_addressIfEEPT_S2_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIfEEE9__destroyIfEEvNS_17integral_constantIbLb1EEERS2_PT_ = comdat any

$_ZNSt3__29allocatorIfE7destroyEPf = comdat any

$_ZNSt3__29allocatorIfE10deallocateEPfm = comdat any

$_ZNSt3__217__compressed_pairIPfNS_9allocatorIfEEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIfEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__26vectorIfNS_9allocatorIfEEE8__appendEm = comdat any

$_ZNSt3__26vectorIfNS_9allocatorIfEEE17__destruct_at_endEPf = comdat any

$_ZNSt3__213__vector_baseIfNS_9allocatorIfEEE9__end_capEv = comdat any

$_ZNSt3__26vectorIfNS_9allocatorIfEEE18__construct_at_endEm = comdat any

$_ZNKSt3__26vectorIfNS_9allocatorIfEEE11__recommendEm = comdat any

$_ZNSt3__214__split_bufferIfRNS_9allocatorIfEEEC2EmmS3_ = comdat any

$_ZNSt3__214__split_bufferIfRNS_9allocatorIfEEE18__construct_at_endEm = comdat any

$_ZNSt3__26vectorIfNS_9allocatorIfEEE26__swap_out_circular_bufferERNS_14__split_bufferIfRS2_EE = comdat any

$_ZNSt3__214__split_bufferIfRNS_9allocatorIfEEED2Ev = comdat any

$_ZNSt3__217__compressed_pairIPfNS_9allocatorIfEEE5firstEv = comdat any

$_ZNSt3__222__compressed_pair_elemIPfLi0ELb0EE5__getEv = comdat any

$_ZNSt3__26vectorIfNS_9allocatorIfEEE21_ConstructTransactionC2ERS3_m = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIfEEE9constructIfJEEEvRS2_PT_DpOT0_ = comdat any

$_ZNSt3__26vectorIfNS_9allocatorIfEEE21_ConstructTransactionD2Ev = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIfEEE11__constructIfJEEEvNS_17integral_constantIbLb1EEERS2_PT_DpOT0_ = comdat any

$_ZNSt3__29allocatorIfE9constructIfJEEEvPT_DpOT0_ = comdat any

$_ZNKSt3__26vectorIfNS_9allocatorIfEEE8max_sizeEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIfEEE8max_sizeERKS2_ = comdat any

$_ZNKSt3__213__vector_baseIfNS_9allocatorIfEEE7__allocEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIfEEE10__max_sizeENS_17integral_constantIbLb1EEERKS2_ = comdat any

$_ZNKSt3__29allocatorIfE8max_sizeEv = comdat any

$_ZNKSt3__217__compressed_pairIPfNS_9allocatorIfEEE6secondEv = comdat any

$_ZNKSt3__222__compressed_pair_elemINS_9allocatorIfEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPfRNS_9allocatorIfEEEC2IDnS4_EEOT_OT0_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIfEEE8allocateERS2_m = comdat any

$_ZNSt3__214__split_bufferIfRNS_9allocatorIfEEE7__allocEv = comdat any

$_ZNSt3__214__split_bufferIfRNS_9allocatorIfEEE9__end_capEv = comdat any

$_ZNSt3__222__compressed_pair_elemIPfLi0ELb0EEC2IDnvEEOT_ = comdat any

$_ZNSt3__27forwardIRNS_9allocatorIfEEEEOT_RNS_16remove_referenceIS4_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIfEELi1ELb0EEC2IS3_vEEOT_ = comdat any

$_ZNSt3__29allocatorIfE8allocateEmPKv = comdat any

$_ZNSt3__217__compressed_pairIPfRNS_9allocatorIfEEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIfEELi1ELb0EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPfRNS_9allocatorIfEEE5firstEv = comdat any

$_ZNSt3__214__split_bufferIfRNS_9allocatorIfEEE21_ConstructTransactionC2EPPfm = comdat any

$_ZNSt3__214__split_bufferIfRNS_9allocatorIfEEE21_ConstructTransactionD2Ev = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIfEEE46__construct_backward_with_exception_guaranteesIfEENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS2_PT_S8_EE5valuesr31is_trivially_move_constructibleIS8_EE5valueEvE4typeERS2_S9_S9_RS9_ = comdat any

$_ZNSt3__24swapIPfEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_ = comdat any

$_ZNKSt3__26vectorIfNS_9allocatorIfEEE14__annotate_newEm = comdat any

$_ZNSt3__26vectorIfNS_9allocatorIfEEE26__invalidate_all_iteratorsEv = comdat any

$_ZNSt3__24moveIRPfEEONS_16remove_referenceIT_E4typeEOS4_ = comdat any

$_ZNSt3__214__split_bufferIfRNS_9allocatorIfEEE5clearEv = comdat any

$_ZNKSt3__214__split_bufferIfRNS_9allocatorIfEEE8capacityEv = comdat any

$_ZNSt3__214__split_bufferIfRNS_9allocatorIfEEE17__destruct_at_endEPf = comdat any

$_ZNSt3__214__split_bufferIfRNS_9allocatorIfEEE17__destruct_at_endEPfNS_17integral_constantIbLb0EEE = comdat any

$_ZNKSt3__214__split_bufferIfRNS_9allocatorIfEEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPfRNS_9allocatorIfEEE5firstEv = comdat any

$_ZNSt3__26vectorIfNS_9allocatorIfEEE27__invalidate_iterators_pastEPf = comdat any

$_ZNKSt3__26vectorIfNS_9allocatorIfEEE17__annotate_shrinkEm = comdat any

$_ZN5draco22AttributeTransformData17SetParameterValueIiEEviRKT_ = comdat any

$_ZNK5draco10DataBuffer9data_sizeEv = comdat any

$_ZN5draco10DataBuffer5WriteExPKvm = comdat any

$_ZNKSt3__26vectorIhNS_9allocatorIhEEE4sizeEv = comdat any

$_ZN5draco22AttributeTransformData17SetParameterValueIfEEviRKT_ = comdat any

$_ZNSt3__28distanceIPKfEENS_15iterator_traitsIT_E15difference_typeES4_S4_ = comdat any

$_ZNSt3__27advanceIPKfEEvRT_NS_15iterator_traitsIS3_E15difference_typeE = comdat any

$_ZNSt3__24copyIPKfPfEET0_T_S5_S4_ = comdat any

$_ZNSt3__26vectorIfNS_9allocatorIfEEE18__construct_at_endIPKfEENS_9enable_ifIXsr27__is_cpp17_forward_iteratorIT_EE5valueEvE4typeES8_S8_m = comdat any

$_ZNSt3__26vectorIfNS_9allocatorIfEEE13__vdeallocateEv = comdat any

$_ZNSt3__26vectorIfNS_9allocatorIfEEE11__vallocateEm = comdat any

$_ZNSt3__210__distanceIPKfEENS_15iterator_traitsIT_E15difference_typeES4_S4_NS_26random_access_iterator_tagE = comdat any

$_ZNSt3__29__advanceIPKfEEvRT_NS_15iterator_traitsIS3_E15difference_typeENS_26random_access_iterator_tagE = comdat any

$_ZNSt3__26__copyIKffEENS_9enable_ifIXaasr7is_sameINS_12remove_constIT_E4typeET0_EE5valuesr28is_trivially_copy_assignableIS7_EE5valueEPS7_E4typeEPS4_SB_S8_ = comdat any

$_ZNSt3__213__unwrap_iterIPKfEET_S3_ = comdat any

$_ZNSt3__213__unwrap_iterIPfEET_S2_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIfEEE25__construct_range_forwardIKffffEENS_9enable_ifIXaaaasr31is_trivially_move_constructibleIT0_EE5valuesr7is_sameIT1_T2_EE5valueooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS2_PS7_RT_EE5valueEvE4typeERS2_PSD_SI_RSC_ = comdat any

$_ZNSt3__26vectorIfNS_9allocatorIfEEE5clearEv = comdat any

$_ZNSt3__213__vector_baseIfNS_9allocatorIfEEEC2Ev = comdat any

$_ZNSt3__26vectorIfNS_9allocatorIfEEE18__construct_at_endEmRKf = comdat any

$_ZNSt3__220__vector_base_commonILb1EEC2Ev = comdat any

$_ZNSt3__217__compressed_pairIPfNS_9allocatorIfEEEC2IDnNS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIfEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__29allocatorIfEC2Ev = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIfEEE9constructIfJRKfEEEvRS2_PT_DpOT0_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIfEEE11__constructIfJRKfEEEvNS_17integral_constantIbLb1EEERS2_PT_DpOT0_ = comdat any

$_ZNSt3__27forwardIRKfEEOT_RNS_16remove_referenceIS3_E4typeE = comdat any

$_ZNSt3__29allocatorIfE9constructIfJRKfEEEvPT_DpOT0_ = comdat any

$_ZNSt3__26vectorIfNS_9allocatorIfEEE13__move_assignERS3_NS_17integral_constantIbLb1EEE = comdat any

$_ZNSt3__213__vector_baseIfNS_9allocatorIfEEE19__move_assign_allocERS3_ = comdat any

$_ZNSt3__213__vector_baseIfNS_9allocatorIfEEE19__move_assign_allocERS3_NS_17integral_constantIbLb1EEE = comdat any

$_ZNSt3__24moveIRNS_9allocatorIfEEEEONS_16remove_referenceIT_E4typeEOS5_ = comdat any

$_ZNSt3__217__compressed_pairIPfNS_14default_deleteIA_fEEEC2IRS1_NS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__27forwardIRPfEEOT_RNS_16remove_referenceIS3_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPfLi0ELb0EEC2IRS1_vEEOT_ = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIA_fEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__210unique_ptrIA_fNS_14default_deleteIS1_EEE5resetEDn = comdat any

$_ZNSt3__217__compressed_pairIPfNS_14default_deleteIA_fEEE5firstEv = comdat any

$_ZNSt3__217__compressed_pairIPfNS_14default_deleteIA_fEEE6secondEv = comdat any

$_ZNKSt3__214default_deleteIA_fEclIfEENS2_20_EnableIfConvertibleIT_E4typeEPS5_ = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIA_fEELi1ELb1EE5__getEv = comdat any

$_ZNKSt3__217__compressed_pairIPfNS_14default_deleteIA_fEEE5firstEv = comdat any

$_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEE5resetEPS2_ = comdat any

$_ZNSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE6secondEv = comdat any

$_ZNKSt3__214default_deleteIN5draco14PointAttributeEEclEPS2_ = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco14PointAttributeELi0ELb0EE5__getEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco14PointAttributeEEELi1ELb1EE5__getEv = comdat any

$_ZN5draco14PointAttributeD2Ev = comdat any

$_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEED2Ev = comdat any

$_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEEED2Ev = comdat any

$_ZNSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEED2Ev = comdat any

$_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE5resetEPS2_ = comdat any

$_ZNSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE6secondEv = comdat any

$_ZNKSt3__214default_deleteIN5draco22AttributeTransformDataEEclEPS2_ = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco22AttributeTransformDataELi0ELb0EE5__getEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco22AttributeTransformDataEEELi1ELb1EE5__getEv = comdat any

$_ZN5draco22AttributeTransformDataD2Ev = comdat any

$_ZN5draco10DataBufferD2Ev = comdat any

$_ZNSt3__26vectorIhNS_9allocatorIhEEED2Ev = comdat any

$_ZNKSt3__26vectorIhNS_9allocatorIhEEE17__annotate_deleteEv = comdat any

$_ZNSt3__213__vector_baseIhNS_9allocatorIhEEED2Ev = comdat any

$_ZNKSt3__26vectorIhNS_9allocatorIhEEE31__annotate_contiguous_containerEPKvS5_S5_S5_ = comdat any

$_ZNKSt3__26vectorIhNS_9allocatorIhEEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPhNS_9allocatorIhEEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPhLi0ELb0EE5__getEv = comdat any

$_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE5clearEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIhEEE10deallocateERS2_Phm = comdat any

$_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE7__allocEv = comdat any

$_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE17__destruct_at_endEPh = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIhEEE7destroyIhEEvRS2_PT_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIhEEE9__destroyIhEEvNS_17integral_constantIbLb1EEERS2_PT_ = comdat any

$_ZNSt3__29allocatorIhE7destroyEPh = comdat any

$_ZNSt3__29allocatorIhE10deallocateEPhm = comdat any

$_ZNSt3__217__compressed_pairIPhNS_9allocatorIhEEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIhEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEED2Ev = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_deleteEv = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEED2Ev = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_ = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv = comdat any

$_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEPT_S6_ = comdat any

$_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5clearEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE10deallocateERS6_PS5_m = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE7destroyIS5_EEvRS6_PT_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE9__destroyIS5_EEvNS_17integral_constantIbLb1EEERS6_PT_ = comdat any

$_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE7destroyEPS4_ = comdat any

$_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE10deallocateEPS4_m = comdat any

$_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEE5resetEPS2_ = comdat any

$_ZNSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEE6secondEv = comdat any

$_ZNKSt3__214default_deleteIN5draco10DataBufferEEclEPS2_ = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco10DataBufferELi0ELb0EE5__getEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco10DataBufferEEELi1ELb1EE5__getEv = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPN5draco14PointAttributeELi0ELb0EE5__getEv = comdat any

@_ZTVN5draco30AttributeQuantizationTransformE = hidden unnamed_addr constant { [7 x i8*] } { [7 x i8*] [i8* null, i8* null, i8* bitcast (%"class.draco::AttributeQuantizationTransform"* (%"class.draco::AttributeQuantizationTransform"*)* @_ZN5draco30AttributeQuantizationTransformD2Ev to i8*), i8* bitcast (void (%"class.draco::AttributeQuantizationTransform"*)* @_ZN5draco30AttributeQuantizationTransformD0Ev to i8*), i8* bitcast (i32 (%"class.draco::AttributeQuantizationTransform"*)* @_ZNK5draco30AttributeQuantizationTransform4TypeEv to i8*), i8* bitcast (i1 (%"class.draco::AttributeQuantizationTransform"*, %"class.draco::PointAttribute"*)* @_ZN5draco30AttributeQuantizationTransform17InitFromAttributeERKNS_14PointAttributeE to i8*), i8* bitcast (void (%"class.draco::AttributeQuantizationTransform"*, %"class.draco::AttributeTransformData"*)* @_ZNK5draco30AttributeQuantizationTransform28CopyToAttributeTransformDataEPNS_22AttributeTransformDataE to i8*)] }, align 4
@.str = private unnamed_addr constant [68 x i8] c"allocator<T>::allocate(size_t n) 'n' exceeds maximum supported size\00", align 1

; Function Attrs: noinline nounwind optnone
define hidden zeroext i1 @_ZN5draco30AttributeQuantizationTransform17InitFromAttributeERKNS_14PointAttributeE(%"class.draco::AttributeQuantizationTransform"* %this, %"class.draco::PointAttribute"* nonnull align 8 dereferenceable(92) %attribute) unnamed_addr #0 {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::AttributeQuantizationTransform"*, align 4
  %attribute.addr = alloca %"class.draco::PointAttribute"*, align 4
  %transform_data = alloca %"class.draco::AttributeTransformData"*, align 4
  %byte_offset = alloca i32, align 4
  %i = alloca i32, align 4
  store %"class.draco::AttributeQuantizationTransform"* %this, %"class.draco::AttributeQuantizationTransform"** %this.addr, align 4
  store %"class.draco::PointAttribute"* %attribute, %"class.draco::PointAttribute"** %attribute.addr, align 4
  %this1 = load %"class.draco::AttributeQuantizationTransform"*, %"class.draco::AttributeQuantizationTransform"** %this.addr, align 4
  %0 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %attribute.addr, align 4
  %call = call %"class.draco::AttributeTransformData"* @_ZNK5draco14PointAttribute25GetAttributeTransformDataEv(%"class.draco::PointAttribute"* %0)
  store %"class.draco::AttributeTransformData"* %call, %"class.draco::AttributeTransformData"** %transform_data, align 4
  %1 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %transform_data, align 4
  %tobool = icmp ne %"class.draco::AttributeTransformData"* %1, null
  br i1 %tobool, label %lor.lhs.false, label %if.then

lor.lhs.false:                                    ; preds = %entry
  %2 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %transform_data, align 4
  %call2 = call i32 @_ZNK5draco22AttributeTransformData14transform_typeEv(%"class.draco::AttributeTransformData"* %2)
  %cmp = icmp ne i32 %call2, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  store i32 0, i32* %byte_offset, align 4
  %3 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %transform_data, align 4
  %4 = load i32, i32* %byte_offset, align 4
  %call3 = call i32 @_ZNK5draco22AttributeTransformData17GetParameterValueIiEET_i(%"class.draco::AttributeTransformData"* %3, i32 %4)
  %quantization_bits_ = getelementptr inbounds %"class.draco::AttributeQuantizationTransform", %"class.draco::AttributeQuantizationTransform"* %this1, i32 0, i32 1
  store i32 %call3, i32* %quantization_bits_, align 4
  %5 = load i32, i32* %byte_offset, align 4
  %add = add nsw i32 %5, 4
  store i32 %add, i32* %byte_offset, align 4
  %min_values_ = getelementptr inbounds %"class.draco::AttributeQuantizationTransform", %"class.draco::AttributeQuantizationTransform"* %this1, i32 0, i32 2
  %6 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %attribute.addr, align 4
  %7 = bitcast %"class.draco::PointAttribute"* %6 to %"class.draco::GeometryAttribute"*
  %call4 = call signext i8 @_ZNK5draco17GeometryAttribute14num_componentsEv(%"class.draco::GeometryAttribute"* %7)
  %conv = sext i8 %call4 to i32
  call void @_ZNSt3__26vectorIfNS_9allocatorIfEEE6resizeEm(%"class.std::__2::vector"* %min_values_, i32 %conv)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %8 = load i32, i32* %i, align 4
  %9 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %attribute.addr, align 4
  %10 = bitcast %"class.draco::PointAttribute"* %9 to %"class.draco::GeometryAttribute"*
  %call5 = call signext i8 @_ZNK5draco17GeometryAttribute14num_componentsEv(%"class.draco::GeometryAttribute"* %10)
  %conv6 = sext i8 %call5 to i32
  %cmp7 = icmp slt i32 %8, %conv6
  br i1 %cmp7, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %11 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %transform_data, align 4
  %12 = load i32, i32* %byte_offset, align 4
  %call8 = call float @_ZNK5draco22AttributeTransformData17GetParameterValueIfEET_i(%"class.draco::AttributeTransformData"* %11, i32 %12)
  %min_values_9 = getelementptr inbounds %"class.draco::AttributeQuantizationTransform", %"class.draco::AttributeQuantizationTransform"* %this1, i32 0, i32 2
  %13 = load i32, i32* %i, align 4
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNSt3__26vectorIfNS_9allocatorIfEEEixEm(%"class.std::__2::vector"* %min_values_9, i32 %13) #8
  store float %call8, float* %call10, align 4
  %14 = load i32, i32* %byte_offset, align 4
  %add11 = add nsw i32 %14, 4
  store i32 %add11, i32* %byte_offset, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %15 = load i32, i32* %i, align 4
  %inc = add nsw i32 %15, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %16 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %transform_data, align 4
  %17 = load i32, i32* %byte_offset, align 4
  %call12 = call float @_ZNK5draco22AttributeTransformData17GetParameterValueIfEET_i(%"class.draco::AttributeTransformData"* %16, i32 %17)
  %range_ = getelementptr inbounds %"class.draco::AttributeQuantizationTransform", %"class.draco::AttributeQuantizationTransform"* %this1, i32 0, i32 3
  store float %call12, float* %range_, align 4
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %for.end, %if.then
  %18 = load i1, i1* %retval, align 1
  ret i1 %18
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::AttributeTransformData"* @_ZNK5draco14PointAttribute25GetAttributeTransformDataEv(%"class.draco::PointAttribute"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointAttribute"*, align 4
  store %"class.draco::PointAttribute"* %this, %"class.draco::PointAttribute"** %this.addr, align 4
  %this1 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %this.addr, align 4
  %attribute_transform_data_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 6
  %call = call %"class.draco::AttributeTransformData"* @_ZNKSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE3getEv(%"class.std::__2::unique_ptr.18"* %attribute_transform_data_) #8
  ret %"class.draco::AttributeTransformData"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco22AttributeTransformData14transform_typeEv(%"class.draco::AttributeTransformData"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::AttributeTransformData"*, align 4
  store %"class.draco::AttributeTransformData"* %this, %"class.draco::AttributeTransformData"** %this.addr, align 4
  %this1 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %this.addr, align 4
  %transform_type_ = getelementptr inbounds %"class.draco::AttributeTransformData", %"class.draco::AttributeTransformData"* %this1, i32 0, i32 0
  %0 = load i32, i32* %transform_type_, align 8
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco22AttributeTransformData17GetParameterValueIiEET_i(%"class.draco::AttributeTransformData"* %this, i32 %byte_offset) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::AttributeTransformData"*, align 4
  %byte_offset.addr = alloca i32, align 4
  %out_data = alloca i32, align 4
  store %"class.draco::AttributeTransformData"* %this, %"class.draco::AttributeTransformData"** %this.addr, align 4
  store i32 %byte_offset, i32* %byte_offset.addr, align 4
  %this1 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %this.addr, align 4
  %buffer_ = getelementptr inbounds %"class.draco::AttributeTransformData", %"class.draco::AttributeTransformData"* %this1, i32 0, i32 1
  %0 = load i32, i32* %byte_offset.addr, align 4
  %conv = sext i32 %0 to i64
  %1 = bitcast i32* %out_data to i8*
  call void @_ZNK5draco10DataBuffer4ReadExPvm(%"class.draco::DataBuffer"* %buffer_, i64 %conv, i8* %1, i32 4)
  %2 = load i32, i32* %out_data, align 4
  ret i32 %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIfNS_9allocatorIfEEE6resizeEm(%"class.std::__2::vector"* %this, i32 %__sz) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__sz.addr = alloca i32, align 4
  %__cs = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__sz, i32* %__sz.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIfNS_9allocatorIfEEE4sizeEv(%"class.std::__2::vector"* %this1) #8
  store i32 %call, i32* %__cs, align 4
  %0 = load i32, i32* %__cs, align 4
  %1 = load i32, i32* %__sz.addr, align 4
  %cmp = icmp ult i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %__sz.addr, align 4
  %3 = load i32, i32* %__cs, align 4
  %sub = sub i32 %2, %3
  call void @_ZNSt3__26vectorIfNS_9allocatorIfEEE8__appendEm(%"class.std::__2::vector"* %this1, i32 %sub)
  br label %if.end4

if.else:                                          ; preds = %entry
  %4 = load i32, i32* %__cs, align 4
  %5 = load i32, i32* %__sz.addr, align 4
  %cmp2 = icmp ugt i32 %4, %5
  br i1 %cmp2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.else
  %6 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %6, i32 0, i32 0
  %7 = load float*, float** %__begin_, align 4
  %8 = load i32, i32* %__sz.addr, align 4
  %add.ptr = getelementptr inbounds float, float* %7, i32 %8
  call void @_ZNSt3__26vectorIfNS_9allocatorIfEEE17__destruct_at_endEPf(%"class.std::__2::vector"* %this1, float* %add.ptr) #8
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.else
  br label %if.end4

if.end4:                                          ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden signext i8 @_ZNK5draco17GeometryAttribute14num_componentsEv(%"class.draco::GeometryAttribute"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::GeometryAttribute"*, align 4
  store %"class.draco::GeometryAttribute"* %this, %"class.draco::GeometryAttribute"** %this.addr, align 4
  %this1 = load %"class.draco::GeometryAttribute"*, %"class.draco::GeometryAttribute"** %this.addr, align 4
  %num_components_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 2
  %0 = load i8, i8* %num_components_, align 8
  ret i8 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK5draco22AttributeTransformData17GetParameterValueIfEET_i(%"class.draco::AttributeTransformData"* %this, i32 %byte_offset) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::AttributeTransformData"*, align 4
  %byte_offset.addr = alloca i32, align 4
  %out_data = alloca float, align 4
  store %"class.draco::AttributeTransformData"* %this, %"class.draco::AttributeTransformData"** %this.addr, align 4
  store i32 %byte_offset, i32* %byte_offset.addr, align 4
  %this1 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %this.addr, align 4
  %buffer_ = getelementptr inbounds %"class.draco::AttributeTransformData", %"class.draco::AttributeTransformData"* %this1, i32 0, i32 1
  %0 = load i32, i32* %byte_offset.addr, align 4
  %conv = sext i32 %0 to i64
  %1 = bitcast float* %out_data to i8*
  call void @_ZNK5draco10DataBuffer4ReadExPvm(%"class.draco::DataBuffer"* %buffer_, i64 %conv, i8* %1, i32 4)
  %2 = load float, float* %out_data, align 4
  ret float %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNSt3__26vectorIfNS_9allocatorIfEEEixEm(%"class.std::__2::vector"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 0
  %1 = load float*, float** %__begin_, align 4
  %2 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds float, float* %1, i32 %2
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZNK5draco30AttributeQuantizationTransform28CopyToAttributeTransformDataEPNS_22AttributeTransformDataE(%"class.draco::AttributeQuantizationTransform"* %this, %"class.draco::AttributeTransformData"* %out_data) unnamed_addr #0 {
entry:
  %this.addr = alloca %"class.draco::AttributeQuantizationTransform"*, align 4
  %out_data.addr = alloca %"class.draco::AttributeTransformData"*, align 4
  %i = alloca i32, align 4
  store %"class.draco::AttributeQuantizationTransform"* %this, %"class.draco::AttributeQuantizationTransform"** %this.addr, align 4
  store %"class.draco::AttributeTransformData"* %out_data, %"class.draco::AttributeTransformData"** %out_data.addr, align 4
  %this1 = load %"class.draco::AttributeQuantizationTransform"*, %"class.draco::AttributeQuantizationTransform"** %this.addr, align 4
  %0 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %out_data.addr, align 4
  call void @_ZN5draco22AttributeTransformData18set_transform_typeENS_22AttributeTransformTypeE(%"class.draco::AttributeTransformData"* %0, i32 1)
  %1 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %out_data.addr, align 4
  %quantization_bits_ = getelementptr inbounds %"class.draco::AttributeQuantizationTransform", %"class.draco::AttributeQuantizationTransform"* %this1, i32 0, i32 1
  call void @_ZN5draco22AttributeTransformData20AppendParameterValueIiEEvRKT_(%"class.draco::AttributeTransformData"* %1, i32* nonnull align 4 dereferenceable(4) %quantization_bits_)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4
  %min_values_ = getelementptr inbounds %"class.draco::AttributeQuantizationTransform", %"class.draco::AttributeQuantizationTransform"* %this1, i32 0, i32 2
  %call = call i32 @_ZNKSt3__26vectorIfNS_9allocatorIfEEE4sizeEv(%"class.std::__2::vector"* %min_values_) #8
  %cmp = icmp ult i32 %2, %call
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %out_data.addr, align 4
  %min_values_2 = getelementptr inbounds %"class.draco::AttributeQuantizationTransform", %"class.draco::AttributeQuantizationTransform"* %this1, i32 0, i32 2
  %4 = load i32, i32* %i, align 4
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNKSt3__26vectorIfNS_9allocatorIfEEEixEm(%"class.std::__2::vector"* %min_values_2, i32 %4) #8
  call void @_ZN5draco22AttributeTransformData20AppendParameterValueIfEEvRKT_(%"class.draco::AttributeTransformData"* %3, float* nonnull align 4 dereferenceable(4) %call3)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %6 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %out_data.addr, align 4
  %range_ = getelementptr inbounds %"class.draco::AttributeQuantizationTransform", %"class.draco::AttributeQuantizationTransform"* %this1, i32 0, i32 3
  call void @_ZN5draco22AttributeTransformData20AppendParameterValueIfEEvRKT_(%"class.draco::AttributeTransformData"* %6, float* nonnull align 4 dereferenceable(4) %range_)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco22AttributeTransformData18set_transform_typeENS_22AttributeTransformTypeE(%"class.draco::AttributeTransformData"* %this, i32 %type) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::AttributeTransformData"*, align 4
  %type.addr = alloca i32, align 4
  store %"class.draco::AttributeTransformData"* %this, %"class.draco::AttributeTransformData"** %this.addr, align 4
  store i32 %type, i32* %type.addr, align 4
  %this1 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %this.addr, align 4
  %0 = load i32, i32* %type.addr, align 4
  %transform_type_ = getelementptr inbounds %"class.draco::AttributeTransformData", %"class.draco::AttributeTransformData"* %this1, i32 0, i32 0
  store i32 %0, i32* %transform_type_, align 8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco22AttributeTransformData20AppendParameterValueIiEEvRKT_(%"class.draco::AttributeTransformData"* %this, i32* nonnull align 4 dereferenceable(4) %in_data) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::AttributeTransformData"*, align 4
  %in_data.addr = alloca i32*, align 4
  store %"class.draco::AttributeTransformData"* %this, %"class.draco::AttributeTransformData"** %this.addr, align 4
  store i32* %in_data, i32** %in_data.addr, align 4
  %this1 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %this.addr, align 4
  %buffer_ = getelementptr inbounds %"class.draco::AttributeTransformData", %"class.draco::AttributeTransformData"* %this1, i32 0, i32 1
  %call = call i32 @_ZNK5draco10DataBuffer9data_sizeEv(%"class.draco::DataBuffer"* %buffer_)
  %0 = load i32*, i32** %in_data.addr, align 4
  call void @_ZN5draco22AttributeTransformData17SetParameterValueIiEEviRKT_(%"class.draco::AttributeTransformData"* %this1, i32 %call, i32* nonnull align 4 dereferenceable(4) %0)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIfNS_9allocatorIfEEE4sizeEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 1
  %1 = load float*, float** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %2, i32 0, i32 0
  %3 = load float*, float** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint float* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint float* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco22AttributeTransformData20AppendParameterValueIfEEvRKT_(%"class.draco::AttributeTransformData"* %this, float* nonnull align 4 dereferenceable(4) %in_data) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::AttributeTransformData"*, align 4
  %in_data.addr = alloca float*, align 4
  store %"class.draco::AttributeTransformData"* %this, %"class.draco::AttributeTransformData"** %this.addr, align 4
  store float* %in_data, float** %in_data.addr, align 4
  %this1 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %this.addr, align 4
  %buffer_ = getelementptr inbounds %"class.draco::AttributeTransformData", %"class.draco::AttributeTransformData"* %this1, i32 0, i32 1
  %call = call i32 @_ZNK5draco10DataBuffer9data_sizeEv(%"class.draco::DataBuffer"* %buffer_)
  %0 = load float*, float** %in_data.addr, align 4
  call void @_ZN5draco22AttributeTransformData17SetParameterValueIfEEviRKT_(%"class.draco::AttributeTransformData"* %this1, i32 %call, float* nonnull align 4 dereferenceable(4) %0)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNKSt3__26vectorIfNS_9allocatorIfEEEixEm(%"class.std::__2::vector"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 0
  %1 = load float*, float** %__begin_, align 4
  %2 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds float, float* %1, i32 %2
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN5draco30AttributeQuantizationTransform13SetParametersEiPKfif(%"class.draco::AttributeQuantizationTransform"* %this, i32 %quantization_bits, float* %min_values, i32 %num_components, float %range) #0 {
entry:
  %this.addr = alloca %"class.draco::AttributeQuantizationTransform"*, align 4
  %quantization_bits.addr = alloca i32, align 4
  %min_values.addr = alloca float*, align 4
  %num_components.addr = alloca i32, align 4
  %range.addr = alloca float, align 4
  store %"class.draco::AttributeQuantizationTransform"* %this, %"class.draco::AttributeQuantizationTransform"** %this.addr, align 4
  store i32 %quantization_bits, i32* %quantization_bits.addr, align 4
  store float* %min_values, float** %min_values.addr, align 4
  store i32 %num_components, i32* %num_components.addr, align 4
  store float %range, float* %range.addr, align 4
  %this1 = load %"class.draco::AttributeQuantizationTransform"*, %"class.draco::AttributeQuantizationTransform"** %this.addr, align 4
  %0 = load i32, i32* %quantization_bits.addr, align 4
  %quantization_bits_ = getelementptr inbounds %"class.draco::AttributeQuantizationTransform", %"class.draco::AttributeQuantizationTransform"* %this1, i32 0, i32 1
  store i32 %0, i32* %quantization_bits_, align 4
  %min_values_ = getelementptr inbounds %"class.draco::AttributeQuantizationTransform", %"class.draco::AttributeQuantizationTransform"* %this1, i32 0, i32 2
  %1 = load float*, float** %min_values.addr, align 4
  %2 = load float*, float** %min_values.addr, align 4
  %3 = load i32, i32* %num_components.addr, align 4
  %add.ptr = getelementptr inbounds float, float* %2, i32 %3
  call void @_ZNSt3__26vectorIfNS_9allocatorIfEEE6assignIPKfEENS_9enable_ifIXaasr27__is_cpp17_forward_iteratorIT_EE5valuesr16is_constructibleIfNS_15iterator_traitsIS8_E9referenceEEE5valueEvE4typeES8_S8_(%"class.std::__2::vector"* %min_values_, float* %1, float* %add.ptr)
  %4 = load float, float* %range.addr, align 4
  %range_ = getelementptr inbounds %"class.draco::AttributeQuantizationTransform", %"class.draco::AttributeQuantizationTransform"* %this1, i32 0, i32 3
  store float %4, float* %range_, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIfNS_9allocatorIfEEE6assignIPKfEENS_9enable_ifIXaasr27__is_cpp17_forward_iteratorIT_EE5valuesr16is_constructibleIfNS_15iterator_traitsIS8_E9referenceEEE5valueEvE4typeES8_S8_(%"class.std::__2::vector"* %this, float* %__first, float* %__last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__first.addr = alloca float*, align 4
  %__last.addr = alloca float*, align 4
  %__new_size = alloca i32, align 4
  %__mid = alloca float*, align 4
  %__growing = alloca i8, align 1
  %__m = alloca float*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store float* %__first, float** %__first.addr, align 4
  store float* %__last, float** %__last.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = load float*, float** %__first.addr, align 4
  %1 = load float*, float** %__last.addr, align 4
  %call = call i32 @_ZNSt3__28distanceIPKfEENS_15iterator_traitsIT_E15difference_typeES4_S4_(float* %0, float* %1)
  store i32 %call, i32* %__new_size, align 4
  %2 = load i32, i32* %__new_size, align 4
  %call2 = call i32 @_ZNKSt3__26vectorIfNS_9allocatorIfEEE8capacityEv(%"class.std::__2::vector"* %this1) #8
  %cmp = icmp ule i32 %2, %call2
  br i1 %cmp, label %if.then, label %if.else11

if.then:                                          ; preds = %entry
  %3 = load float*, float** %__last.addr, align 4
  store float* %3, float** %__mid, align 4
  store i8 0, i8* %__growing, align 1
  %4 = load i32, i32* %__new_size, align 4
  %call3 = call i32 @_ZNKSt3__26vectorIfNS_9allocatorIfEEE4sizeEv(%"class.std::__2::vector"* %this1) #8
  %cmp4 = icmp ugt i32 %4, %call3
  br i1 %cmp4, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.then
  store i8 1, i8* %__growing, align 1
  %5 = load float*, float** %__first.addr, align 4
  store float* %5, float** %__mid, align 4
  %call6 = call i32 @_ZNKSt3__26vectorIfNS_9allocatorIfEEE4sizeEv(%"class.std::__2::vector"* %this1) #8
  call void @_ZNSt3__27advanceIPKfEEvRT_NS_15iterator_traitsIS3_E15difference_typeE(float** nonnull align 4 dereferenceable(4) %__mid, i32 %call6)
  br label %if.end

if.end:                                           ; preds = %if.then5, %if.then
  %6 = load float*, float** %__first.addr, align 4
  %7 = load float*, float** %__mid, align 4
  %8 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %8, i32 0, i32 0
  %9 = load float*, float** %__begin_, align 4
  %call7 = call float* @_ZNSt3__24copyIPKfPfEET0_T_S5_S4_(float* %6, float* %7, float* %9)
  store float* %call7, float** %__m, align 4
  %10 = load i8, i8* %__growing, align 1
  %tobool = trunc i8 %10 to i1
  br i1 %tobool, label %if.then8, label %if.else

if.then8:                                         ; preds = %if.end
  %11 = load float*, float** %__mid, align 4
  %12 = load float*, float** %__last.addr, align 4
  %13 = load i32, i32* %__new_size, align 4
  %call9 = call i32 @_ZNKSt3__26vectorIfNS_9allocatorIfEEE4sizeEv(%"class.std::__2::vector"* %this1) #8
  %sub = sub i32 %13, %call9
  call void @_ZNSt3__26vectorIfNS_9allocatorIfEEE18__construct_at_endIPKfEENS_9enable_ifIXsr27__is_cpp17_forward_iteratorIT_EE5valueEvE4typeES8_S8_m(%"class.std::__2::vector"* %this1, float* %11, float* %12, i32 %sub)
  br label %if.end10

if.else:                                          ; preds = %if.end
  %14 = load float*, float** %__m, align 4
  call void @_ZNSt3__26vectorIfNS_9allocatorIfEEE17__destruct_at_endEPf(%"class.std::__2::vector"* %this1, float* %14) #8
  br label %if.end10

if.end10:                                         ; preds = %if.else, %if.then8
  br label %if.end13

if.else11:                                        ; preds = %entry
  call void @_ZNSt3__26vectorIfNS_9allocatorIfEEE13__vdeallocateEv(%"class.std::__2::vector"* %this1) #8
  %15 = load i32, i32* %__new_size, align 4
  %call12 = call i32 @_ZNKSt3__26vectorIfNS_9allocatorIfEEE11__recommendEm(%"class.std::__2::vector"* %this1, i32 %15)
  call void @_ZNSt3__26vectorIfNS_9allocatorIfEEE11__vallocateEm(%"class.std::__2::vector"* %this1, i32 %call12)
  %16 = load float*, float** %__first.addr, align 4
  %17 = load float*, float** %__last.addr, align 4
  %18 = load i32, i32* %__new_size, align 4
  call void @_ZNSt3__26vectorIfNS_9allocatorIfEEE18__construct_at_endIPKfEENS_9enable_ifIXsr27__is_cpp17_forward_iteratorIT_EE5valueEvE4typeES8_S8_m(%"class.std::__2::vector"* %this1, float* %16, float* %17, i32 %18)
  br label %if.end13

if.end13:                                         ; preds = %if.else11, %if.end10
  call void @_ZNSt3__26vectorIfNS_9allocatorIfEEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector"* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden zeroext i1 @_ZN5draco30AttributeQuantizationTransform17ComputeParametersERKNS_14PointAttributeEi(%"class.draco::AttributeQuantizationTransform"* %this, %"class.draco::PointAttribute"* nonnull align 8 dereferenceable(92) %attribute, i32 %quantization_bits) #0 {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::AttributeQuantizationTransform"*, align 4
  %attribute.addr = alloca %"class.draco::PointAttribute"*, align 4
  %quantization_bits.addr = alloca i32, align 4
  %num_components = alloca i32, align 4
  %ref.tmp = alloca %"class.std::__2::vector", align 4
  %ref.tmp3 = alloca float, align 4
  %max_values = alloca %"class.std::__2::unique_ptr.23", align 4
  %att_val = alloca %"class.std::__2::unique_ptr.23", align 4
  %agg.tmp = alloca %"class.draco::IndexType", align 4
  %agg.tmp13 = alloca %"class.draco::IndexType", align 4
  %agg.tmp18 = alloca %"class.draco::IndexType", align 4
  %i = alloca %"class.draco::IndexType", align 4
  %ref.tmp23 = alloca i32, align 4
  %agg.tmp26 = alloca %"class.draco::IndexType", align 4
  %c = alloca i32, align 4
  %c51 = alloca i32, align 4
  %dif = alloca float, align 4
  store %"class.draco::AttributeQuantizationTransform"* %this, %"class.draco::AttributeQuantizationTransform"** %this.addr, align 4
  store %"class.draco::PointAttribute"* %attribute, %"class.draco::PointAttribute"** %attribute.addr, align 4
  store i32 %quantization_bits, i32* %quantization_bits.addr, align 4
  %this1 = load %"class.draco::AttributeQuantizationTransform"*, %"class.draco::AttributeQuantizationTransform"** %this.addr, align 4
  %quantization_bits_ = getelementptr inbounds %"class.draco::AttributeQuantizationTransform", %"class.draco::AttributeQuantizationTransform"* %this1, i32 0, i32 1
  %0 = load i32, i32* %quantization_bits_, align 4
  %cmp = icmp ne i32 %0, -1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %1 = load i32, i32* %quantization_bits.addr, align 4
  %quantization_bits_2 = getelementptr inbounds %"class.draco::AttributeQuantizationTransform", %"class.draco::AttributeQuantizationTransform"* %this1, i32 0, i32 1
  store i32 %1, i32* %quantization_bits_2, align 4
  %2 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %attribute.addr, align 4
  %3 = bitcast %"class.draco::PointAttribute"* %2 to %"class.draco::GeometryAttribute"*
  %call = call signext i8 @_ZNK5draco17GeometryAttribute14num_componentsEv(%"class.draco::GeometryAttribute"* %3)
  %conv = sext i8 %call to i32
  store i32 %conv, i32* %num_components, align 4
  %range_ = getelementptr inbounds %"class.draco::AttributeQuantizationTransform", %"class.draco::AttributeQuantizationTransform"* %this1, i32 0, i32 3
  store float 0.000000e+00, float* %range_, align 4
  %4 = load i32, i32* %num_components, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  %call4 = call %"class.std::__2::vector"* @_ZNSt3__26vectorIfNS_9allocatorIfEEEC2EmRKf(%"class.std::__2::vector"* %ref.tmp, i32 %4, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %min_values_ = getelementptr inbounds %"class.draco::AttributeQuantizationTransform", %"class.draco::AttributeQuantizationTransform"* %this1, i32 0, i32 2
  %call5 = call nonnull align 4 dereferenceable(12) %"class.std::__2::vector"* @_ZNSt3__26vectorIfNS_9allocatorIfEEEaSEOS3_(%"class.std::__2::vector"* %min_values_, %"class.std::__2::vector"* nonnull align 4 dereferenceable(12) %ref.tmp) #8
  %call6 = call %"class.std::__2::vector"* @_ZNSt3__26vectorIfNS_9allocatorIfEEED2Ev(%"class.std::__2::vector"* %ref.tmp) #8
  %5 = load i32, i32* %num_components, align 4
  %6 = call { i32, i1 } @llvm.umul.with.overflow.i32(i32 %5, i32 4)
  %7 = extractvalue { i32, i1 } %6, 1
  %8 = extractvalue { i32, i1 } %6, 0
  %9 = select i1 %7, i32 -1, i32 %8
  %call7 = call noalias nonnull i8* @_Znam(i32 %9) #9
  %10 = bitcast i8* %call7 to float*
  %call8 = call %"class.std::__2::unique_ptr.23"* @_ZNSt3__210unique_ptrIA_fNS_14default_deleteIS1_EEEC2IPfLb1EvvEET_(%"class.std::__2::unique_ptr.23"* %max_values, float* %10) #8
  %11 = load i32, i32* %num_components, align 4
  %12 = call { i32, i1 } @llvm.umul.with.overflow.i32(i32 %11, i32 4)
  %13 = extractvalue { i32, i1 } %12, 1
  %14 = extractvalue { i32, i1 } %12, 0
  %15 = select i1 %13, i32 -1, i32 %14
  %call9 = call noalias nonnull i8* @_Znam(i32 %15) #9
  %16 = bitcast i8* %call9 to float*
  %call10 = call %"class.std::__2::unique_ptr.23"* @_ZNSt3__210unique_ptrIA_fNS_14default_deleteIS1_EEEC2IPfLb1EvvEET_(%"class.std::__2::unique_ptr.23"* %att_val, float* %16) #8
  %17 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %attribute.addr, align 4
  %18 = bitcast %"class.draco::PointAttribute"* %17 to %"class.draco::GeometryAttribute"*
  %call11 = call %"class.draco::IndexType"* @_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEC2Ej(%"class.draco::IndexType"* %agg.tmp, i32 0)
  %call12 = call float* @_ZNKSt3__210unique_ptrIA_fNS_14default_deleteIS1_EEE3getEv(%"class.std::__2::unique_ptr.23"* %att_val) #8
  %19 = bitcast float* %call12 to i8*
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %agg.tmp, i32 0, i32 0
  %20 = load i32, i32* %coerce.dive, align 4
  call void @_ZNK5draco17GeometryAttribute8GetValueENS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEEPv(%"class.draco::GeometryAttribute"* %18, i32 %20, i8* %19)
  %21 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %attribute.addr, align 4
  %22 = bitcast %"class.draco::PointAttribute"* %21 to %"class.draco::GeometryAttribute"*
  %call14 = call %"class.draco::IndexType"* @_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEC2Ej(%"class.draco::IndexType"* %agg.tmp13, i32 0)
  %min_values_15 = getelementptr inbounds %"class.draco::AttributeQuantizationTransform", %"class.draco::AttributeQuantizationTransform"* %this1, i32 0, i32 2
  %call16 = call float* @_ZNSt3__26vectorIfNS_9allocatorIfEEE4dataEv(%"class.std::__2::vector"* %min_values_15) #8
  %23 = bitcast float* %call16 to i8*
  %coerce.dive17 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %agg.tmp13, i32 0, i32 0
  %24 = load i32, i32* %coerce.dive17, align 4
  call void @_ZNK5draco17GeometryAttribute8GetValueENS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEEPv(%"class.draco::GeometryAttribute"* %22, i32 %24, i8* %23)
  %25 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %attribute.addr, align 4
  %26 = bitcast %"class.draco::PointAttribute"* %25 to %"class.draco::GeometryAttribute"*
  %call19 = call %"class.draco::IndexType"* @_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEC2Ej(%"class.draco::IndexType"* %agg.tmp18, i32 0)
  %call20 = call float* @_ZNKSt3__210unique_ptrIA_fNS_14default_deleteIS1_EEE3getEv(%"class.std::__2::unique_ptr.23"* %max_values) #8
  %27 = bitcast float* %call20 to i8*
  %coerce.dive21 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %agg.tmp18, i32 0, i32 0
  %28 = load i32, i32* %coerce.dive21, align 4
  call void @_ZNK5draco17GeometryAttribute8GetValueENS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEEPv(%"class.draco::GeometryAttribute"* %26, i32 %28, i8* %27)
  %call22 = call %"class.draco::IndexType"* @_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEC2Ej(%"class.draco::IndexType"* %i, i32 1)
  br label %for.cond

for.cond:                                         ; preds = %for.inc48, %if.end
  %29 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %attribute.addr, align 4
  %call24 = call i32 @_ZNK5draco14PointAttribute4sizeEv(%"class.draco::PointAttribute"* %29)
  store i32 %call24, i32* %ref.tmp23, align 4
  %call25 = call zeroext i1 @_ZNK5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEltERKj(%"class.draco::IndexType"* %i, i32* nonnull align 4 dereferenceable(4) %ref.tmp23)
  br i1 %call25, label %for.body, label %for.end50

for.body:                                         ; preds = %for.cond
  %30 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %attribute.addr, align 4
  %31 = bitcast %"class.draco::PointAttribute"* %30 to %"class.draco::GeometryAttribute"*
  %32 = bitcast %"class.draco::IndexType"* %agg.tmp26 to i8*
  %33 = bitcast %"class.draco::IndexType"* %i to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %32, i8* align 4 %33, i32 4, i1 false)
  %call27 = call float* @_ZNKSt3__210unique_ptrIA_fNS_14default_deleteIS1_EEE3getEv(%"class.std::__2::unique_ptr.23"* %att_val) #8
  %34 = bitcast float* %call27 to i8*
  %coerce.dive28 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %agg.tmp26, i32 0, i32 0
  %35 = load i32, i32* %coerce.dive28, align 4
  call void @_ZNK5draco17GeometryAttribute8GetValueENS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEEPv(%"class.draco::GeometryAttribute"* %31, i32 %35, i8* %34)
  store i32 0, i32* %c, align 4
  br label %for.cond29

for.cond29:                                       ; preds = %for.inc, %for.body
  %36 = load i32, i32* %c, align 4
  %37 = load i32, i32* %num_components, align 4
  %cmp30 = icmp slt i32 %36, %37
  br i1 %cmp30, label %for.body31, label %for.end

for.body31:                                       ; preds = %for.cond29
  %min_values_32 = getelementptr inbounds %"class.draco::AttributeQuantizationTransform", %"class.draco::AttributeQuantizationTransform"* %this1, i32 0, i32 2
  %38 = load i32, i32* %c, align 4
  %call33 = call nonnull align 4 dereferenceable(4) float* @_ZNSt3__26vectorIfNS_9allocatorIfEEEixEm(%"class.std::__2::vector"* %min_values_32, i32 %38) #8
  %39 = load float, float* %call33, align 4
  %40 = load i32, i32* %c, align 4
  %call34 = call nonnull align 4 dereferenceable(4) float* @_ZNKSt3__210unique_ptrIA_fNS_14default_deleteIS1_EEEixEm(%"class.std::__2::unique_ptr.23"* %att_val, i32 %40)
  %41 = load float, float* %call34, align 4
  %cmp35 = fcmp ogt float %39, %41
  br i1 %cmp35, label %if.then36, label %if.end40

if.then36:                                        ; preds = %for.body31
  %42 = load i32, i32* %c, align 4
  %call37 = call nonnull align 4 dereferenceable(4) float* @_ZNKSt3__210unique_ptrIA_fNS_14default_deleteIS1_EEEixEm(%"class.std::__2::unique_ptr.23"* %att_val, i32 %42)
  %43 = load float, float* %call37, align 4
  %min_values_38 = getelementptr inbounds %"class.draco::AttributeQuantizationTransform", %"class.draco::AttributeQuantizationTransform"* %this1, i32 0, i32 2
  %44 = load i32, i32* %c, align 4
  %call39 = call nonnull align 4 dereferenceable(4) float* @_ZNSt3__26vectorIfNS_9allocatorIfEEEixEm(%"class.std::__2::vector"* %min_values_38, i32 %44) #8
  store float %43, float* %call39, align 4
  br label %if.end40

if.end40:                                         ; preds = %if.then36, %for.body31
  %45 = load i32, i32* %c, align 4
  %call41 = call nonnull align 4 dereferenceable(4) float* @_ZNKSt3__210unique_ptrIA_fNS_14default_deleteIS1_EEEixEm(%"class.std::__2::unique_ptr.23"* %max_values, i32 %45)
  %46 = load float, float* %call41, align 4
  %47 = load i32, i32* %c, align 4
  %call42 = call nonnull align 4 dereferenceable(4) float* @_ZNKSt3__210unique_ptrIA_fNS_14default_deleteIS1_EEEixEm(%"class.std::__2::unique_ptr.23"* %att_val, i32 %47)
  %48 = load float, float* %call42, align 4
  %cmp43 = fcmp olt float %46, %48
  br i1 %cmp43, label %if.then44, label %if.end47

if.then44:                                        ; preds = %if.end40
  %49 = load i32, i32* %c, align 4
  %call45 = call nonnull align 4 dereferenceable(4) float* @_ZNKSt3__210unique_ptrIA_fNS_14default_deleteIS1_EEEixEm(%"class.std::__2::unique_ptr.23"* %att_val, i32 %49)
  %50 = load float, float* %call45, align 4
  %51 = load i32, i32* %c, align 4
  %call46 = call nonnull align 4 dereferenceable(4) float* @_ZNKSt3__210unique_ptrIA_fNS_14default_deleteIS1_EEEixEm(%"class.std::__2::unique_ptr.23"* %max_values, i32 %51)
  store float %50, float* %call46, align 4
  br label %if.end47

if.end47:                                         ; preds = %if.then44, %if.end40
  br label %for.inc

for.inc:                                          ; preds = %if.end47
  %52 = load i32, i32* %c, align 4
  %inc = add nsw i32 %52, 1
  store i32 %inc, i32* %c, align 4
  br label %for.cond29

for.end:                                          ; preds = %for.cond29
  br label %for.inc48

for.inc48:                                        ; preds = %for.end
  %call49 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEppEv(%"class.draco::IndexType"* %i)
  br label %for.cond

for.end50:                                        ; preds = %for.cond
  store i32 0, i32* %c51, align 4
  br label %for.cond52

for.cond52:                                       ; preds = %for.inc63, %for.end50
  %53 = load i32, i32* %c51, align 4
  %54 = load i32, i32* %num_components, align 4
  %cmp53 = icmp slt i32 %53, %54
  br i1 %cmp53, label %for.body54, label %for.end65

for.body54:                                       ; preds = %for.cond52
  %55 = load i32, i32* %c51, align 4
  %call55 = call nonnull align 4 dereferenceable(4) float* @_ZNKSt3__210unique_ptrIA_fNS_14default_deleteIS1_EEEixEm(%"class.std::__2::unique_ptr.23"* %max_values, i32 %55)
  %56 = load float, float* %call55, align 4
  %min_values_56 = getelementptr inbounds %"class.draco::AttributeQuantizationTransform", %"class.draco::AttributeQuantizationTransform"* %this1, i32 0, i32 2
  %57 = load i32, i32* %c51, align 4
  %call57 = call nonnull align 4 dereferenceable(4) float* @_ZNSt3__26vectorIfNS_9allocatorIfEEEixEm(%"class.std::__2::vector"* %min_values_56, i32 %57) #8
  %58 = load float, float* %call57, align 4
  %sub = fsub float %56, %58
  store float %sub, float* %dif, align 4
  %59 = load float, float* %dif, align 4
  %range_58 = getelementptr inbounds %"class.draco::AttributeQuantizationTransform", %"class.draco::AttributeQuantizationTransform"* %this1, i32 0, i32 3
  %60 = load float, float* %range_58, align 4
  %cmp59 = fcmp ogt float %59, %60
  br i1 %cmp59, label %if.then60, label %if.end62

if.then60:                                        ; preds = %for.body54
  %61 = load float, float* %dif, align 4
  %range_61 = getelementptr inbounds %"class.draco::AttributeQuantizationTransform", %"class.draco::AttributeQuantizationTransform"* %this1, i32 0, i32 3
  store float %61, float* %range_61, align 4
  br label %if.end62

if.end62:                                         ; preds = %if.then60, %for.body54
  br label %for.inc63

for.inc63:                                        ; preds = %if.end62
  %62 = load i32, i32* %c51, align 4
  %inc64 = add nsw i32 %62, 1
  store i32 %inc64, i32* %c51, align 4
  br label %for.cond52

for.end65:                                        ; preds = %for.cond52
  %range_66 = getelementptr inbounds %"class.draco::AttributeQuantizationTransform", %"class.draco::AttributeQuantizationTransform"* %this1, i32 0, i32 3
  %63 = load float, float* %range_66, align 4
  %cmp67 = fcmp oeq float %63, 0.000000e+00
  br i1 %cmp67, label %if.then68, label %if.end70

if.then68:                                        ; preds = %for.end65
  %range_69 = getelementptr inbounds %"class.draco::AttributeQuantizationTransform", %"class.draco::AttributeQuantizationTransform"* %this1, i32 0, i32 3
  store float 1.000000e+00, float* %range_69, align 4
  br label %if.end70

if.end70:                                         ; preds = %if.then68, %for.end65
  store i1 true, i1* %retval, align 1
  %call71 = call %"class.std::__2::unique_ptr.23"* @_ZNSt3__210unique_ptrIA_fNS_14default_deleteIS1_EEED2Ev(%"class.std::__2::unique_ptr.23"* %att_val) #8
  %call72 = call %"class.std::__2::unique_ptr.23"* @_ZNSt3__210unique_ptrIA_fNS_14default_deleteIS1_EEED2Ev(%"class.std::__2::unique_ptr.23"* %max_values) #8
  br label %return

return:                                           ; preds = %if.end70, %if.then
  %64 = load i1, i1* %retval, align 1
  ret i1 %64
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector"* @_ZNSt3__26vectorIfNS_9allocatorIfEEEC2EmRKf(%"class.std::__2::vector"* returned %this, i32 %__n, float* nonnull align 4 dereferenceable(4) %__x) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"class.std::__2::vector"*, align 4
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__n.addr = alloca i32, align 4
  %__x.addr = alloca float*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store float* %__x, float** %__x.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  store %"class.std::__2::vector"* %this1, %"class.std::__2::vector"** %retval, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call %"class.std::__2::__vector_base"* @_ZNSt3__213__vector_baseIfNS_9allocatorIfEEEC2Ev(%"class.std::__2::__vector_base"* %0) #8
  %1 = load i32, i32* %__n.addr, align 4
  %cmp = icmp ugt i32 %1, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__26vectorIfNS_9allocatorIfEEE11__vallocateEm(%"class.std::__2::vector"* %this1, i32 %2)
  %3 = load i32, i32* %__n.addr, align 4
  %4 = load float*, float** %__x.addr, align 4
  call void @_ZNSt3__26vectorIfNS_9allocatorIfEEE18__construct_at_endEmRKf(%"class.std::__2::vector"* %this1, i32 %3, float* nonnull align 4 dereferenceable(4) %4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %5 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %retval, align 4
  ret %"class.std::__2::vector"* %5
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"class.std::__2::vector"* @_ZNSt3__26vectorIfNS_9allocatorIfEEEaSEOS3_(%"class.std::__2::vector"* %this, %"class.std::__2::vector"* nonnull align 4 dereferenceable(12) %__x) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__x.addr = alloca %"class.std::__2::vector"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store %"class.std::__2::vector"* %__x, %"class.std::__2::vector"** %__x.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__x.addr, align 4
  call void @_ZNSt3__26vectorIfNS_9allocatorIfEEE13__move_assignERS3_NS_17integral_constantIbLb1EEE(%"class.std::__2::vector"* %this1, %"class.std::__2::vector"* nonnull align 4 dereferenceable(12) %0) #8
  ret %"class.std::__2::vector"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector"* @_ZNSt3__26vectorIfNS_9allocatorIfEEED2Ev(%"class.std::__2::vector"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  call void @_ZNKSt3__26vectorIfNS_9allocatorIfEEE17__annotate_deleteEv(%"class.std::__2::vector"* %this1) #8
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call %"class.std::__2::__vector_base"* @_ZNSt3__213__vector_baseIfNS_9allocatorIfEEED2Ev(%"class.std::__2::__vector_base"* %0) #8
  ret %"class.std::__2::vector"* %this1
}

; Function Attrs: nounwind readnone speculatable willreturn
declare { i32, i1 } @llvm.umul.with.overflow.i32(i32, i32) #1

; Function Attrs: nobuiltin allocsize(0)
declare nonnull i8* @_Znam(i32) #2

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.23"* @_ZNSt3__210unique_ptrIA_fNS_14default_deleteIS1_EEEC2IPfLb1EvvEET_(%"class.std::__2::unique_ptr.23"* returned %this, float* %__p) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.23"*, align 4
  %__p.addr = alloca float*, align 4
  %ref.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::unique_ptr.23"* %this, %"class.std::__2::unique_ptr.23"** %this.addr, align 4
  store float* %__p, float** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.23"*, %"class.std::__2::unique_ptr.23"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.23", %"class.std::__2::unique_ptr.23"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::__compressed_pair.24"* @_ZNSt3__217__compressed_pairIPfNS_14default_deleteIA_fEEEC2IRS1_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.24"* %__ptr_, float** nonnull align 4 dereferenceable(4) %__p.addr, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp)
  ret %"class.std::__2::unique_ptr.23"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK5draco17GeometryAttribute8GetValueENS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEEPv(%"class.draco::GeometryAttribute"* %this, i32 %att_index.coerce, i8* %out_data) #0 comdat {
entry:
  %att_index = alloca %"class.draco::IndexType", align 4
  %this.addr = alloca %"class.draco::GeometryAttribute"*, align 4
  %out_data.addr = alloca i8*, align 4
  %byte_pos = alloca i64, align 8
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %att_index, i32 0, i32 0
  store i32 %att_index.coerce, i32* %coerce.dive, align 4
  store %"class.draco::GeometryAttribute"* %this, %"class.draco::GeometryAttribute"** %this.addr, align 4
  store i8* %out_data, i8** %out_data.addr, align 4
  %this1 = load %"class.draco::GeometryAttribute"*, %"class.draco::GeometryAttribute"** %this.addr, align 4
  %byte_offset_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 6
  %0 = load i64, i64* %byte_offset_, align 8
  %byte_stride_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 5
  %1 = load i64, i64* %byte_stride_, align 8
  %call = call i32 @_ZNK5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EE5valueEv(%"class.draco::IndexType"* %att_index)
  %conv = zext i32 %call to i64
  %mul = mul nsw i64 %1, %conv
  %add = add nsw i64 %0, %mul
  store i64 %add, i64* %byte_pos, align 8
  %buffer_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 0
  %2 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %buffer_, align 8
  %3 = load i64, i64* %byte_pos, align 8
  %4 = load i8*, i8** %out_data.addr, align 4
  %byte_stride_2 = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 5
  %5 = load i64, i64* %byte_stride_2, align 8
  %conv3 = trunc i64 %5 to i32
  call void @_ZNK5draco10DataBuffer4ReadExPvm(%"class.draco::DataBuffer"* %2, i64 %3, i8* %4, i32 %conv3)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType"* @_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEC2Ej(%"class.draco::IndexType"* returned %this, i32 %value) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType"*, align 4
  %value.addr = alloca i32, align 4
  store %"class.draco::IndexType"* %this, %"class.draco::IndexType"** %this.addr, align 4
  store i32 %value, i32* %value.addr, align 4
  %this1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value.addr, align 4
  store i32 %0, i32* %value_, align 4
  ret %"class.draco::IndexType"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZNKSt3__210unique_ptrIA_fNS_14default_deleteIS1_EEE3getEv(%"class.std::__2::unique_ptr.23"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.23"*, align 4
  store %"class.std::__2::unique_ptr.23"* %this, %"class.std::__2::unique_ptr.23"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.23"*, %"class.std::__2::unique_ptr.23"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.23", %"class.std::__2::unique_ptr.23"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float** @_ZNKSt3__217__compressed_pairIPfNS_14default_deleteIA_fEEE5firstEv(%"class.std::__2::__compressed_pair.24"* %__ptr_) #8
  %0 = load float*, float** %call, align 4
  ret float* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZNSt3__26vectorIfNS_9allocatorIfEEE4dataEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 0
  %1 = load float*, float** %__begin_, align 4
  %call = call float* @_ZNSt3__212__to_addressIfEEPT_S2_(float* %1) #8
  ret float* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEltERKj(%"class.draco::IndexType"* %this, i32* nonnull align 4 dereferenceable(4) %val) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType"*, align 4
  %val.addr = alloca i32*, align 4
  store %"class.draco::IndexType"* %this, %"class.draco::IndexType"** %this.addr, align 4
  store i32* %val, i32** %val.addr, align 4
  %this1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value_, align 4
  %1 = load i32*, i32** %val.addr, align 4
  %2 = load i32, i32* %1, align 4
  %cmp = icmp ult i32 %0, %2
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco14PointAttribute4sizeEv(%"class.draco::PointAttribute"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointAttribute"*, align 4
  store %"class.draco::PointAttribute"* %this, %"class.draco::PointAttribute"** %this.addr, align 4
  %this1 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %this.addr, align 4
  %num_unique_entries_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 3
  %0 = load i32, i32* %num_unique_entries_, align 8
  ret i32 %0
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNKSt3__210unique_ptrIA_fNS_14default_deleteIS1_EEEixEm(%"class.std::__2::unique_ptr.23"* %this, i32 %__i) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.23"*, align 4
  %__i.addr = alloca i32, align 4
  store %"class.std::__2::unique_ptr.23"* %this, %"class.std::__2::unique_ptr.23"** %this.addr, align 4
  store i32 %__i, i32* %__i.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.23"*, %"class.std::__2::unique_ptr.23"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.23", %"class.std::__2::unique_ptr.23"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float** @_ZNKSt3__217__compressed_pairIPfNS_14default_deleteIA_fEEE5firstEv(%"class.std::__2::__compressed_pair.24"* %__ptr_) #8
  %0 = load float*, float** %call, align 4
  %1 = load i32, i32* %__i.addr, align 4
  %arrayidx = getelementptr inbounds float, float* %0, i32 %1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEppEv(%"class.draco::IndexType"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.draco::IndexType"* %this, %"class.draco::IndexType"** %this.addr, align 4
  %this1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value_, align 4
  %inc = add i32 %0, 1
  store i32 %inc, i32* %value_, align 4
  ret %"class.draco::IndexType"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.23"* @_ZNSt3__210unique_ptrIA_fNS_14default_deleteIS1_EEED2Ev(%"class.std::__2::unique_ptr.23"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.23"*, align 4
  store %"class.std::__2::unique_ptr.23"* %this, %"class.std::__2::unique_ptr.23"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.23"*, %"class.std::__2::unique_ptr.23"** %this.addr, align 4
  call void @_ZNSt3__210unique_ptrIA_fNS_14default_deleteIS1_EEE5resetEDn(%"class.std::__2::unique_ptr.23"* %this1, i8* null) #8
  ret %"class.std::__2::unique_ptr.23"* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden zeroext i1 @_ZNK5draco30AttributeQuantizationTransform16EncodeParametersEPNS_13EncoderBufferE(%"class.draco::AttributeQuantizationTransform"* %this, %"class.draco::EncoderBuffer"* %encoder_buffer) #0 {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::AttributeQuantizationTransform"*, align 4
  %encoder_buffer.addr = alloca %"class.draco::EncoderBuffer"*, align 4
  %ref.tmp = alloca i8, align 1
  store %"class.draco::AttributeQuantizationTransform"* %this, %"class.draco::AttributeQuantizationTransform"** %this.addr, align 4
  store %"class.draco::EncoderBuffer"* %encoder_buffer, %"class.draco::EncoderBuffer"** %encoder_buffer.addr, align 4
  %this1 = load %"class.draco::AttributeQuantizationTransform"*, %"class.draco::AttributeQuantizationTransform"** %this.addr, align 4
  %call = call zeroext i1 @_ZNK5draco30AttributeQuantizationTransform14is_initializedEv(%"class.draco::AttributeQuantizationTransform"* %this1)
  br i1 %call, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %0 = load %"class.draco::EncoderBuffer"*, %"class.draco::EncoderBuffer"** %encoder_buffer.addr, align 4
  %min_values_ = getelementptr inbounds %"class.draco::AttributeQuantizationTransform", %"class.draco::AttributeQuantizationTransform"* %this1, i32 0, i32 2
  %call2 = call float* @_ZNKSt3__26vectorIfNS_9allocatorIfEEE4dataEv(%"class.std::__2::vector"* %min_values_) #8
  %1 = bitcast float* %call2 to i8*
  %min_values_3 = getelementptr inbounds %"class.draco::AttributeQuantizationTransform", %"class.draco::AttributeQuantizationTransform"* %this1, i32 0, i32 2
  %call4 = call i32 @_ZNKSt3__26vectorIfNS_9allocatorIfEEE4sizeEv(%"class.std::__2::vector"* %min_values_3) #8
  %mul = mul i32 4, %call4
  %call5 = call zeroext i1 @_ZN5draco13EncoderBuffer6EncodeEPKvm(%"class.draco::EncoderBuffer"* %0, i8* %1, i32 %mul)
  %2 = load %"class.draco::EncoderBuffer"*, %"class.draco::EncoderBuffer"** %encoder_buffer.addr, align 4
  %range_ = getelementptr inbounds %"class.draco::AttributeQuantizationTransform", %"class.draco::AttributeQuantizationTransform"* %this1, i32 0, i32 3
  %call6 = call zeroext i1 @_ZN5draco13EncoderBuffer6EncodeIfEEbRKT_(%"class.draco::EncoderBuffer"* %2, float* nonnull align 4 dereferenceable(4) %range_)
  %3 = load %"class.draco::EncoderBuffer"*, %"class.draco::EncoderBuffer"** %encoder_buffer.addr, align 4
  %quantization_bits_ = getelementptr inbounds %"class.draco::AttributeQuantizationTransform", %"class.draco::AttributeQuantizationTransform"* %this1, i32 0, i32 1
  %4 = load i32, i32* %quantization_bits_, align 4
  %conv = trunc i32 %4 to i8
  store i8 %conv, i8* %ref.tmp, align 1
  %call7 = call zeroext i1 @_ZN5draco13EncoderBuffer6EncodeIhEEbRKT_(%"class.draco::EncoderBuffer"* %3, i8* nonnull align 1 dereferenceable(1) %ref.tmp)
  store i1 true, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %5 = load i1, i1* %retval, align 1
  ret i1 %5
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK5draco30AttributeQuantizationTransform14is_initializedEv(%"class.draco::AttributeQuantizationTransform"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::AttributeQuantizationTransform"*, align 4
  store %"class.draco::AttributeQuantizationTransform"* %this, %"class.draco::AttributeQuantizationTransform"** %this.addr, align 4
  %this1 = load %"class.draco::AttributeQuantizationTransform"*, %"class.draco::AttributeQuantizationTransform"** %this.addr, align 4
  %quantization_bits_ = getelementptr inbounds %"class.draco::AttributeQuantizationTransform", %"class.draco::AttributeQuantizationTransform"* %this1, i32 0, i32 1
  %0 = load i32, i32* %quantization_bits_, align 4
  %cmp = icmp ne i32 %0, -1
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco13EncoderBuffer6EncodeEPKvm(%"class.draco::EncoderBuffer"* %this, i8* %data, i32 %data_size) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::EncoderBuffer"*, align 4
  %data.addr = alloca i8*, align 4
  %data_size.addr = alloca i32, align 4
  %src_data = alloca i8*, align 4
  %agg.tmp = alloca %"class.std::__2::__wrap_iter", align 4
  %ref.tmp = alloca %"class.std::__2::__wrap_iter.52", align 4
  %coerce = alloca %"class.std::__2::__wrap_iter.52", align 4
  store %"class.draco::EncoderBuffer"* %this, %"class.draco::EncoderBuffer"** %this.addr, align 4
  store i8* %data, i8** %data.addr, align 4
  store i32 %data_size, i32* %data_size.addr, align 4
  %this1 = load %"class.draco::EncoderBuffer"*, %"class.draco::EncoderBuffer"** %this.addr, align 4
  %call = call zeroext i1 @_ZNK5draco13EncoderBuffer18bit_encoder_activeEv(%"class.draco::EncoderBuffer"* %this1)
  br i1 %call, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %0 = load i8*, i8** %data.addr, align 4
  store i8* %0, i8** %src_data, align 4
  %buffer_ = getelementptr inbounds %"class.draco::EncoderBuffer", %"class.draco::EncoderBuffer"* %this1, i32 0, i32 0
  %buffer_2 = getelementptr inbounds %"class.draco::EncoderBuffer", %"class.draco::EncoderBuffer"* %this1, i32 0, i32 0
  %call3 = call i8* @_ZNSt3__26vectorIcNS_9allocatorIcEEE3endEv(%"class.std::__2::vector.27"* %buffer_2) #8
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter.52", %"class.std::__2::__wrap_iter.52"* %ref.tmp, i32 0, i32 0
  store i8* %call3, i8** %coerce.dive, align 4
  %call4 = call %"class.std::__2::__wrap_iter"* @_ZNSt3__211__wrap_iterIPKcEC2IPcEERKNS0_IT_EEPNS_9enable_ifIXsr14is_convertibleIS6_S2_EE5valueEvE4typeE(%"class.std::__2::__wrap_iter"* %agg.tmp, %"class.std::__2::__wrap_iter.52"* nonnull align 4 dereferenceable(4) %ref.tmp, i8* null) #8
  %1 = load i8*, i8** %src_data, align 4
  %2 = load i8*, i8** %src_data, align 4
  %3 = load i32, i32* %data_size.addr, align 4
  %add.ptr = getelementptr inbounds i8, i8* %2, i32 %3
  %coerce.dive5 = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %agg.tmp, i32 0, i32 0
  %4 = load i8*, i8** %coerce.dive5, align 4
  %call6 = call i8* @_ZNSt3__26vectorIcNS_9allocatorIcEEE6insertIPKhEENS_9enable_ifIXaasr27__is_cpp17_forward_iteratorIT_EE5valuesr16is_constructibleIcNS_15iterator_traitsIS8_E9referenceEEE5valueENS_11__wrap_iterIPcEEE4typeENSC_IPKcEES8_S8_(%"class.std::__2::vector.27"* %buffer_, i8* %4, i8* %1, i8* %add.ptr)
  %coerce.dive7 = getelementptr inbounds %"class.std::__2::__wrap_iter.52", %"class.std::__2::__wrap_iter.52"* %coerce, i32 0, i32 0
  store i8* %call6, i8** %coerce.dive7, align 4
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %5 = load i1, i1* %retval, align 1
  ret i1 %5
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZNKSt3__26vectorIfNS_9allocatorIfEEE4dataEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 0
  %1 = load float*, float** %__begin_, align 4
  %call = call float* @_ZNSt3__212__to_addressIfEEPT_S2_(float* %1) #8
  ret float* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco13EncoderBuffer6EncodeIfEEbRKT_(%"class.draco::EncoderBuffer"* %this, float* nonnull align 4 dereferenceable(4) %data) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::EncoderBuffer"*, align 4
  %data.addr = alloca float*, align 4
  %src_data = alloca i8*, align 4
  %agg.tmp = alloca %"class.std::__2::__wrap_iter", align 4
  %ref.tmp = alloca %"class.std::__2::__wrap_iter.52", align 4
  %coerce = alloca %"class.std::__2::__wrap_iter.52", align 4
  store %"class.draco::EncoderBuffer"* %this, %"class.draco::EncoderBuffer"** %this.addr, align 4
  store float* %data, float** %data.addr, align 4
  %this1 = load %"class.draco::EncoderBuffer"*, %"class.draco::EncoderBuffer"** %this.addr, align 4
  %call = call zeroext i1 @_ZNK5draco13EncoderBuffer18bit_encoder_activeEv(%"class.draco::EncoderBuffer"* %this1)
  br i1 %call, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %0 = load float*, float** %data.addr, align 4
  %1 = bitcast float* %0 to i8*
  store i8* %1, i8** %src_data, align 4
  %buffer_ = getelementptr inbounds %"class.draco::EncoderBuffer", %"class.draco::EncoderBuffer"* %this1, i32 0, i32 0
  %buffer_2 = getelementptr inbounds %"class.draco::EncoderBuffer", %"class.draco::EncoderBuffer"* %this1, i32 0, i32 0
  %call3 = call i8* @_ZNSt3__26vectorIcNS_9allocatorIcEEE3endEv(%"class.std::__2::vector.27"* %buffer_2) #8
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter.52", %"class.std::__2::__wrap_iter.52"* %ref.tmp, i32 0, i32 0
  store i8* %call3, i8** %coerce.dive, align 4
  %call4 = call %"class.std::__2::__wrap_iter"* @_ZNSt3__211__wrap_iterIPKcEC2IPcEERKNS0_IT_EEPNS_9enable_ifIXsr14is_convertibleIS6_S2_EE5valueEvE4typeE(%"class.std::__2::__wrap_iter"* %agg.tmp, %"class.std::__2::__wrap_iter.52"* nonnull align 4 dereferenceable(4) %ref.tmp, i8* null) #8
  %2 = load i8*, i8** %src_data, align 4
  %3 = load i8*, i8** %src_data, align 4
  %add.ptr = getelementptr inbounds i8, i8* %3, i32 4
  %coerce.dive5 = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %agg.tmp, i32 0, i32 0
  %4 = load i8*, i8** %coerce.dive5, align 4
  %call6 = call i8* @_ZNSt3__26vectorIcNS_9allocatorIcEEE6insertIPKhEENS_9enable_ifIXaasr27__is_cpp17_forward_iteratorIT_EE5valuesr16is_constructibleIcNS_15iterator_traitsIS8_E9referenceEEE5valueENS_11__wrap_iterIPcEEE4typeENSC_IPKcEES8_S8_(%"class.std::__2::vector.27"* %buffer_, i8* %4, i8* %2, i8* %add.ptr)
  %coerce.dive7 = getelementptr inbounds %"class.std::__2::__wrap_iter.52", %"class.std::__2::__wrap_iter.52"* %coerce, i32 0, i32 0
  store i8* %call6, i8** %coerce.dive7, align 4
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %5 = load i1, i1* %retval, align 1
  ret i1 %5
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco13EncoderBuffer6EncodeIhEEbRKT_(%"class.draco::EncoderBuffer"* %this, i8* nonnull align 1 dereferenceable(1) %data) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::EncoderBuffer"*, align 4
  %data.addr = alloca i8*, align 4
  %src_data = alloca i8*, align 4
  %agg.tmp = alloca %"class.std::__2::__wrap_iter", align 4
  %ref.tmp = alloca %"class.std::__2::__wrap_iter.52", align 4
  %coerce = alloca %"class.std::__2::__wrap_iter.52", align 4
  store %"class.draco::EncoderBuffer"* %this, %"class.draco::EncoderBuffer"** %this.addr, align 4
  store i8* %data, i8** %data.addr, align 4
  %this1 = load %"class.draco::EncoderBuffer"*, %"class.draco::EncoderBuffer"** %this.addr, align 4
  %call = call zeroext i1 @_ZNK5draco13EncoderBuffer18bit_encoder_activeEv(%"class.draco::EncoderBuffer"* %this1)
  br i1 %call, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %0 = load i8*, i8** %data.addr, align 4
  store i8* %0, i8** %src_data, align 4
  %buffer_ = getelementptr inbounds %"class.draco::EncoderBuffer", %"class.draco::EncoderBuffer"* %this1, i32 0, i32 0
  %buffer_2 = getelementptr inbounds %"class.draco::EncoderBuffer", %"class.draco::EncoderBuffer"* %this1, i32 0, i32 0
  %call3 = call i8* @_ZNSt3__26vectorIcNS_9allocatorIcEEE3endEv(%"class.std::__2::vector.27"* %buffer_2) #8
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter.52", %"class.std::__2::__wrap_iter.52"* %ref.tmp, i32 0, i32 0
  store i8* %call3, i8** %coerce.dive, align 4
  %call4 = call %"class.std::__2::__wrap_iter"* @_ZNSt3__211__wrap_iterIPKcEC2IPcEERKNS0_IT_EEPNS_9enable_ifIXsr14is_convertibleIS6_S2_EE5valueEvE4typeE(%"class.std::__2::__wrap_iter"* %agg.tmp, %"class.std::__2::__wrap_iter.52"* nonnull align 4 dereferenceable(4) %ref.tmp, i8* null) #8
  %1 = load i8*, i8** %src_data, align 4
  %2 = load i8*, i8** %src_data, align 4
  %add.ptr = getelementptr inbounds i8, i8* %2, i32 1
  %coerce.dive5 = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %agg.tmp, i32 0, i32 0
  %3 = load i8*, i8** %coerce.dive5, align 4
  %call6 = call i8* @_ZNSt3__26vectorIcNS_9allocatorIcEEE6insertIPKhEENS_9enable_ifIXaasr27__is_cpp17_forward_iteratorIT_EE5valuesr16is_constructibleIcNS_15iterator_traitsIS8_E9referenceEEE5valueENS_11__wrap_iterIPcEEE4typeENSC_IPKcEES8_S8_(%"class.std::__2::vector.27"* %buffer_, i8* %3, i8* %1, i8* %add.ptr)
  %coerce.dive7 = getelementptr inbounds %"class.std::__2::__wrap_iter.52", %"class.std::__2::__wrap_iter.52"* %coerce, i32 0, i32 0
  store i8* %call6, i8** %coerce.dive7, align 4
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %4 = load i1, i1* %retval, align 1
  ret i1 %4
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZNK5draco30AttributeQuantizationTransform25GeneratePortableAttributeERKNS_14PointAttributeEi(%"class.std::__2::unique_ptr.39"* noalias sret align 4 %agg.result, %"class.draco::AttributeQuantizationTransform"* %this, %"class.draco::PointAttribute"* nonnull align 8 dereferenceable(92) %attribute, i32 %num_points) #0 {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %"class.draco::AttributeQuantizationTransform"*, align 4
  %attribute.addr = alloca %"class.draco::PointAttribute"*, align 4
  %num_points.addr = alloca i32, align 4
  %num_entries = alloca i32, align 4
  %num_components = alloca i32, align 4
  %nrvo = alloca i1, align 1
  %portable_attribute_data = alloca i32*, align 4
  %agg.tmp = alloca %"class.draco::IndexType", align 4
  %max_quantized_value = alloca i32, align 4
  %quantizer = alloca %"class.draco::Quantizer", align 4
  %dst_index = alloca i32, align 4
  %att_val = alloca %"class.std::__2::unique_ptr.23", align 4
  %i = alloca %"class.draco::IndexType.44", align 4
  %ref.tmp = alloca i32, align 4
  %att_val_id = alloca %"class.draco::IndexType", align 4
  %agg.tmp11 = alloca %"class.draco::IndexType.44", align 4
  %agg.tmp15 = alloca %"class.draco::IndexType", align 4
  %c = alloca i32, align 4
  %value = alloca float, align 4
  %q_val = alloca i32, align 4
  %0 = bitcast %"class.std::__2::unique_ptr.39"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %"class.draco::AttributeQuantizationTransform"* %this, %"class.draco::AttributeQuantizationTransform"** %this.addr, align 4
  store %"class.draco::PointAttribute"* %attribute, %"class.draco::PointAttribute"** %attribute.addr, align 4
  store i32 %num_points, i32* %num_points.addr, align 4
  %this1 = load %"class.draco::AttributeQuantizationTransform"*, %"class.draco::AttributeQuantizationTransform"** %this.addr, align 4
  %1 = load i32, i32* %num_points.addr, align 4
  store i32 %1, i32* %num_entries, align 4
  %2 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %attribute.addr, align 4
  %3 = bitcast %"class.draco::PointAttribute"* %2 to %"class.draco::GeometryAttribute"*
  %call = call signext i8 @_ZNK5draco17GeometryAttribute14num_componentsEv(%"class.draco::GeometryAttribute"* %3)
  %conv = sext i8 %call to i32
  store i32 %conv, i32* %num_components, align 4
  store i1 false, i1* %nrvo, align 1
  %4 = bitcast %"class.draco::AttributeQuantizationTransform"* %this1 to %"class.draco::AttributeTransform"*
  %5 = load i32, i32* %num_entries, align 4
  %6 = load i32, i32* %num_components, align 4
  %7 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %attribute.addr, align 4
  call void @_ZNK5draco18AttributeTransform21InitPortableAttributeEiiiRKNS_14PointAttributeEb(%"class.std::__2::unique_ptr.39"* sret align 4 %agg.result, %"class.draco::AttributeTransform"* %4, i32 %5, i32 %6, i32 0, %"class.draco::PointAttribute"* nonnull align 8 dereferenceable(92) %7, i1 zeroext true)
  %call2 = call %"class.draco::PointAttribute"* @_ZNKSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr.39"* %agg.result) #8
  %8 = bitcast %"class.draco::PointAttribute"* %call2 to %"class.draco::GeometryAttribute"*
  %call3 = call %"class.draco::IndexType"* @_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEC2Ej(%"class.draco::IndexType"* %agg.tmp, i32 0)
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %agg.tmp, i32 0, i32 0
  %9 = load i32, i32* %coerce.dive, align 4
  %call4 = call i8* @_ZN5draco17GeometryAttribute10GetAddressENS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEE(%"class.draco::GeometryAttribute"* %8, i32 %9)
  %10 = bitcast i8* %call4 to i32*
  store i32* %10, i32** %portable_attribute_data, align 4
  %quantization_bits_ = getelementptr inbounds %"class.draco::AttributeQuantizationTransform", %"class.draco::AttributeQuantizationTransform"* %this1, i32 0, i32 1
  %11 = load i32, i32* %quantization_bits_, align 4
  %shl = shl i32 1, %11
  %sub = sub nsw i32 %shl, 1
  store i32 %sub, i32* %max_quantized_value, align 4
  %call5 = call %"class.draco::Quantizer"* @_ZN5draco9QuantizerC1Ev(%"class.draco::Quantizer"* %quantizer)
  %call6 = call float @_ZNK5draco30AttributeQuantizationTransform5rangeEv(%"class.draco::AttributeQuantizationTransform"* %this1)
  %12 = load i32, i32* %max_quantized_value, align 4
  call void @_ZN5draco9Quantizer4InitEfi(%"class.draco::Quantizer"* %quantizer, float %call6, i32 %12)
  store i32 0, i32* %dst_index, align 4
  %13 = load i32, i32* %num_components, align 4
  %14 = call { i32, i1 } @llvm.umul.with.overflow.i32(i32 %13, i32 4)
  %15 = extractvalue { i32, i1 } %14, 1
  %16 = extractvalue { i32, i1 } %14, 0
  %17 = select i1 %15, i32 -1, i32 %16
  %call7 = call noalias nonnull i8* @_Znam(i32 %17) #9
  %18 = bitcast i8* %call7 to float*
  %call8 = call %"class.std::__2::unique_ptr.23"* @_ZNSt3__210unique_ptrIA_fNS_14default_deleteIS1_EEEC2IPfLb1EvvEET_(%"class.std::__2::unique_ptr.23"* %att_val, float* %18) #8
  %call9 = call %"class.draco::IndexType.44"* @_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEC2Ej(%"class.draco::IndexType.44"* %i, i32 0)
  br label %for.cond

for.cond:                                         ; preds = %for.inc26, %entry
  %19 = load i32, i32* %num_points.addr, align 4
  store i32 %19, i32* %ref.tmp, align 4
  %call10 = call zeroext i1 @_ZNK5draco9IndexTypeIjNS_20PointIndex_tag_type_EEltERKj(%"class.draco::IndexType.44"* %i, i32* nonnull align 4 dereferenceable(4) %ref.tmp)
  br i1 %call10, label %for.body, label %for.end28

for.body:                                         ; preds = %for.cond
  %20 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %attribute.addr, align 4
  %21 = bitcast %"class.draco::IndexType.44"* %agg.tmp11 to i8*
  %22 = bitcast %"class.draco::IndexType.44"* %i to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %21, i8* align 4 %22, i32 4, i1 false)
  %coerce.dive12 = getelementptr inbounds %"class.draco::IndexType.44", %"class.draco::IndexType.44"* %agg.tmp11, i32 0, i32 0
  %23 = load i32, i32* %coerce.dive12, align 4
  %call13 = call i32 @_ZNK5draco14PointAttribute12mapped_indexENS_9IndexTypeIjNS_20PointIndex_tag_type_EEE(%"class.draco::PointAttribute"* %20, i32 %23)
  %coerce.dive14 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %att_val_id, i32 0, i32 0
  store i32 %call13, i32* %coerce.dive14, align 4
  %24 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %attribute.addr, align 4
  %25 = bitcast %"class.draco::PointAttribute"* %24 to %"class.draco::GeometryAttribute"*
  %26 = bitcast %"class.draco::IndexType"* %agg.tmp15 to i8*
  %27 = bitcast %"class.draco::IndexType"* %att_val_id to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %26, i8* align 4 %27, i32 4, i1 false)
  %call16 = call float* @_ZNKSt3__210unique_ptrIA_fNS_14default_deleteIS1_EEE3getEv(%"class.std::__2::unique_ptr.23"* %att_val) #8
  %28 = bitcast float* %call16 to i8*
  %coerce.dive17 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %agg.tmp15, i32 0, i32 0
  %29 = load i32, i32* %coerce.dive17, align 4
  call void @_ZNK5draco17GeometryAttribute8GetValueENS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEEPv(%"class.draco::GeometryAttribute"* %25, i32 %29, i8* %28)
  store i32 0, i32* %c, align 4
  br label %for.cond18

for.cond18:                                       ; preds = %for.inc, %for.body
  %30 = load i32, i32* %c, align 4
  %31 = load i32, i32* %num_components, align 4
  %cmp = icmp slt i32 %30, %31
  br i1 %cmp, label %for.body19, label %for.end

for.body19:                                       ; preds = %for.cond18
  %32 = load i32, i32* %c, align 4
  %call20 = call nonnull align 4 dereferenceable(4) float* @_ZNKSt3__210unique_ptrIA_fNS_14default_deleteIS1_EEEixEm(%"class.std::__2::unique_ptr.23"* %att_val, i32 %32)
  %33 = load float, float* %call20, align 4
  %call21 = call nonnull align 4 dereferenceable(12) %"class.std::__2::vector"* @_ZNK5draco30AttributeQuantizationTransform10min_valuesEv(%"class.draco::AttributeQuantizationTransform"* %this1)
  %34 = load i32, i32* %c, align 4
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNKSt3__26vectorIfNS_9allocatorIfEEEixEm(%"class.std::__2::vector"* %call21, i32 %34) #8
  %35 = load float, float* %call22, align 4
  %sub23 = fsub float %33, %35
  store float %sub23, float* %value, align 4
  %36 = load float, float* %value, align 4
  %call24 = call i32 @_ZNK5draco9Quantizer13QuantizeFloatEf(%"class.draco::Quantizer"* %quantizer, float %36)
  store i32 %call24, i32* %q_val, align 4
  %37 = load i32, i32* %q_val, align 4
  %38 = load i32*, i32** %portable_attribute_data, align 4
  %39 = load i32, i32* %dst_index, align 4
  %inc = add nsw i32 %39, 1
  store i32 %inc, i32* %dst_index, align 4
  %arrayidx = getelementptr inbounds i32, i32* %38, i32 %39
  store i32 %37, i32* %arrayidx, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body19
  %40 = load i32, i32* %c, align 4
  %inc25 = add nsw i32 %40, 1
  store i32 %inc25, i32* %c, align 4
  br label %for.cond18

for.end:                                          ; preds = %for.cond18
  br label %for.inc26

for.inc26:                                        ; preds = %for.end
  %call27 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.44"* @_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEppEv(%"class.draco::IndexType.44"* %i)
  br label %for.cond

for.end28:                                        ; preds = %for.cond
  store i1 true, i1* %nrvo, align 1
  %call29 = call %"class.std::__2::unique_ptr.23"* @_ZNSt3__210unique_ptrIA_fNS_14default_deleteIS1_EEED2Ev(%"class.std::__2::unique_ptr.23"* %att_val) #8
  %nrvo.val = load i1, i1* %nrvo, align 1
  br i1 %nrvo.val, label %nrvo.skipdtor, label %nrvo.unused

nrvo.unused:                                      ; preds = %for.end28
  %call30 = call %"class.std::__2::unique_ptr.39"* @_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.39"* %agg.result) #8
  br label %nrvo.skipdtor

nrvo.skipdtor:                                    ; preds = %nrvo.unused, %for.end28
  ret void
}

declare void @_ZNK5draco18AttributeTransform21InitPortableAttributeEiiiRKNS_14PointAttributeEb(%"class.std::__2::unique_ptr.39"* sret align 4, %"class.draco::AttributeTransform"*, i32, i32, i32, %"class.draco::PointAttribute"* nonnull align 8 dereferenceable(92), i1 zeroext) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::PointAttribute"* @_ZNKSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr.39"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.39"*, align 4
  store %"class.std::__2::unique_ptr.39"* %this, %"class.std::__2::unique_ptr.39"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.39"*, %"class.std::__2::unique_ptr.39"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.39", %"class.std::__2::unique_ptr.39"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNKSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.40"* %__ptr_) #8
  %0 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %call, align 4
  ret %"class.draco::PointAttribute"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN5draco17GeometryAttribute10GetAddressENS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEE(%"class.draco::GeometryAttribute"* %this, i32 %att_index.coerce) #0 comdat {
entry:
  %att_index = alloca %"class.draco::IndexType", align 4
  %this.addr = alloca %"class.draco::GeometryAttribute"*, align 4
  %byte_pos = alloca i64, align 8
  %agg.tmp = alloca %"class.draco::IndexType", align 4
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %att_index, i32 0, i32 0
  store i32 %att_index.coerce, i32* %coerce.dive, align 4
  store %"class.draco::GeometryAttribute"* %this, %"class.draco::GeometryAttribute"** %this.addr, align 4
  %this1 = load %"class.draco::GeometryAttribute"*, %"class.draco::GeometryAttribute"** %this.addr, align 4
  %0 = bitcast %"class.draco::IndexType"* %agg.tmp to i8*
  %1 = bitcast %"class.draco::IndexType"* %att_index to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %0, i8* align 4 %1, i32 4, i1 false)
  %coerce.dive2 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %agg.tmp, i32 0, i32 0
  %2 = load i32, i32* %coerce.dive2, align 4
  %call = call i64 @_ZNK5draco17GeometryAttribute10GetBytePosENS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEE(%"class.draco::GeometryAttribute"* %this1, i32 %2)
  store i64 %call, i64* %byte_pos, align 8
  %buffer_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 0
  %3 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %buffer_, align 8
  %call3 = call i8* @_ZN5draco10DataBuffer4dataEv(%"class.draco::DataBuffer"* %3)
  %4 = load i64, i64* %byte_pos, align 8
  %idx.ext = trunc i64 %4 to i32
  %add.ptr = getelementptr inbounds i8, i8* %call3, i32 %idx.ext
  ret i8* %add.ptr
}

declare %"class.draco::Quantizer"* @_ZN5draco9QuantizerC1Ev(%"class.draco::Quantizer"* returned) unnamed_addr #4

declare void @_ZN5draco9Quantizer4InitEfi(%"class.draco::Quantizer"*, float, i32) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK5draco30AttributeQuantizationTransform5rangeEv(%"class.draco::AttributeQuantizationTransform"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::AttributeQuantizationTransform"*, align 4
  store %"class.draco::AttributeQuantizationTransform"* %this, %"class.draco::AttributeQuantizationTransform"** %this.addr, align 4
  %this1 = load %"class.draco::AttributeQuantizationTransform"*, %"class.draco::AttributeQuantizationTransform"** %this.addr, align 4
  %range_ = getelementptr inbounds %"class.draco::AttributeQuantizationTransform", %"class.draco::AttributeQuantizationTransform"* %this1, i32 0, i32 3
  %0 = load float, float* %range_, align 4
  ret float %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType.44"* @_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEC2Ej(%"class.draco::IndexType.44"* returned %this, i32 %value) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.44"*, align 4
  %value.addr = alloca i32, align 4
  store %"class.draco::IndexType.44"* %this, %"class.draco::IndexType.44"** %this.addr, align 4
  store i32 %value, i32* %value.addr, align 4
  %this1 = load %"class.draco::IndexType.44"*, %"class.draco::IndexType.44"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.44", %"class.draco::IndexType.44"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value.addr, align 4
  store i32 %0, i32* %value_, align 4
  ret %"class.draco::IndexType.44"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK5draco9IndexTypeIjNS_20PointIndex_tag_type_EEltERKj(%"class.draco::IndexType.44"* %this, i32* nonnull align 4 dereferenceable(4) %val) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.44"*, align 4
  %val.addr = alloca i32*, align 4
  store %"class.draco::IndexType.44"* %this, %"class.draco::IndexType.44"** %this.addr, align 4
  store i32* %val, i32** %val.addr, align 4
  %this1 = load %"class.draco::IndexType.44"*, %"class.draco::IndexType.44"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.44", %"class.draco::IndexType.44"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value_, align 4
  %1 = load i32*, i32** %val.addr, align 4
  %2 = load i32, i32* %1, align 4
  %cmp = icmp ult i32 %0, %2
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco14PointAttribute12mapped_indexENS_9IndexTypeIjNS_20PointIndex_tag_type_EEE(%"class.draco::PointAttribute"* %this, i32 %point_index.coerce) #0 comdat {
entry:
  %retval = alloca %"class.draco::IndexType", align 4
  %point_index = alloca %"class.draco::IndexType.44", align 4
  %this.addr = alloca %"class.draco::PointAttribute"*, align 4
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType.44", %"class.draco::IndexType.44"* %point_index, i32 0, i32 0
  store i32 %point_index.coerce, i32* %coerce.dive, align 4
  store %"class.draco::PointAttribute"* %this, %"class.draco::PointAttribute"** %this.addr, align 4
  %this1 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %this.addr, align 4
  %identity_mapping_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 4
  %0 = load i8, i8* %identity_mapping_, align 4
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call = call i32 @_ZNK5draco9IndexTypeIjNS_20PointIndex_tag_type_EE5valueEv(%"class.draco::IndexType.44"* %point_index)
  %call2 = call %"class.draco::IndexType"* @_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEC2Ej(%"class.draco::IndexType"* %retval, i32 %call)
  br label %return

if.end:                                           ; preds = %entry
  %indices_map_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 2
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEEEixERKS3_(%"class.draco::IndexTypeVector"* %indices_map_, %"class.draco::IndexType.44"* nonnull align 4 dereferenceable(4) %point_index)
  %1 = bitcast %"class.draco::IndexType"* %retval to i8*
  %2 = bitcast %"class.draco::IndexType"* %call3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 4, i1 false)
  br label %return

return:                                           ; preds = %if.end, %if.then
  %coerce.dive4 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %retval, i32 0, i32 0
  %3 = load i32, i32* %coerce.dive4, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"class.std::__2::vector"* @_ZNK5draco30AttributeQuantizationTransform10min_valuesEv(%"class.draco::AttributeQuantizationTransform"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::AttributeQuantizationTransform"*, align 4
  store %"class.draco::AttributeQuantizationTransform"* %this, %"class.draco::AttributeQuantizationTransform"** %this.addr, align 4
  %this1 = load %"class.draco::AttributeQuantizationTransform"*, %"class.draco::AttributeQuantizationTransform"** %this.addr, align 4
  %min_values_ = getelementptr inbounds %"class.draco::AttributeQuantizationTransform", %"class.draco::AttributeQuantizationTransform"* %this1, i32 0, i32 2
  ret %"class.std::__2::vector"* %min_values_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco9Quantizer13QuantizeFloatEf(%"class.draco::Quantizer"* %this, float %val) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::Quantizer"*, align 4
  %val.addr = alloca float, align 4
  store %"class.draco::Quantizer"* %this, %"class.draco::Quantizer"** %this.addr, align 4
  store float %val, float* %val.addr, align 4
  %this1 = load %"class.draco::Quantizer"*, %"class.draco::Quantizer"** %this.addr, align 4
  %inverse_delta_ = getelementptr inbounds %"class.draco::Quantizer", %"class.draco::Quantizer"* %this1, i32 0, i32 0
  %0 = load float, float* %inverse_delta_, align 4
  %1 = load float, float* %val.addr, align 4
  %mul = fmul float %1, %0
  store float %mul, float* %val.addr, align 4
  %2 = load float, float* %val.addr, align 4
  %add = fadd float %2, 5.000000e-01
  %call = call float @_Z5floorf(float %add) #8
  %conv = fptosi float %call to i32
  ret i32 %conv
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.44"* @_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEppEv(%"class.draco::IndexType.44"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.44"*, align 4
  store %"class.draco::IndexType.44"* %this, %"class.draco::IndexType.44"** %this.addr, align 4
  %this1 = load %"class.draco::IndexType.44"*, %"class.draco::IndexType.44"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.44", %"class.draco::IndexType.44"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value_, align 4
  %inc = add i32 %0, 1
  store i32 %inc, i32* %value_, align 4
  ret %"class.draco::IndexType.44"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.39"* @_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.39"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.39"*, align 4
  store %"class.std::__2::unique_ptr.39"* %this, %"class.std::__2::unique_ptr.39"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.39"*, %"class.std::__2::unique_ptr.39"** %this.addr, align 4
  call void @_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.39"* %this1, %"class.draco::PointAttribute"* null) #8
  ret %"class.std::__2::unique_ptr.39"* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZNK5draco30AttributeQuantizationTransform25GeneratePortableAttributeERKNS_14PointAttributeERKNSt3__26vectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS4_9allocatorIS8_EEEEi(%"class.std::__2::unique_ptr.39"* noalias sret align 4 %agg.result, %"class.draco::AttributeQuantizationTransform"* %this, %"class.draco::PointAttribute"* nonnull align 8 dereferenceable(92) %attribute, %"class.std::__2::vector.45"* nonnull align 4 dereferenceable(12) %point_ids, i32 %num_points) #0 {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %"class.draco::AttributeQuantizationTransform"*, align 4
  %attribute.addr = alloca %"class.draco::PointAttribute"*, align 4
  %point_ids.addr = alloca %"class.std::__2::vector.45"*, align 4
  %num_points.addr = alloca i32, align 4
  %num_entries = alloca i32, align 4
  %num_components = alloca i32, align 4
  %nrvo = alloca i1, align 1
  %portable_attribute_data = alloca i32*, align 4
  %agg.tmp = alloca %"class.draco::IndexType", align 4
  %max_quantized_value = alloca i32, align 4
  %quantizer = alloca %"class.draco::Quantizer", align 4
  %dst_index = alloca i32, align 4
  %att_val = alloca %"class.std::__2::unique_ptr.23", align 4
  %i = alloca i32, align 4
  %att_val_id = alloca %"class.draco::IndexType", align 4
  %agg.tmp11 = alloca %"class.draco::IndexType.44", align 4
  %agg.tmp16 = alloca %"class.draco::IndexType", align 4
  %c = alloca i32, align 4
  %value = alloca float, align 4
  %q_val = alloca i32, align 4
  %0 = bitcast %"class.std::__2::unique_ptr.39"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %"class.draco::AttributeQuantizationTransform"* %this, %"class.draco::AttributeQuantizationTransform"** %this.addr, align 4
  store %"class.draco::PointAttribute"* %attribute, %"class.draco::PointAttribute"** %attribute.addr, align 4
  store %"class.std::__2::vector.45"* %point_ids, %"class.std::__2::vector.45"** %point_ids.addr, align 4
  store i32 %num_points, i32* %num_points.addr, align 4
  %this1 = load %"class.draco::AttributeQuantizationTransform"*, %"class.draco::AttributeQuantizationTransform"** %this.addr, align 4
  %1 = load %"class.std::__2::vector.45"*, %"class.std::__2::vector.45"** %point_ids.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.45"* %1) #8
  store i32 %call, i32* %num_entries, align 4
  %2 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %attribute.addr, align 4
  %3 = bitcast %"class.draco::PointAttribute"* %2 to %"class.draco::GeometryAttribute"*
  %call2 = call signext i8 @_ZNK5draco17GeometryAttribute14num_componentsEv(%"class.draco::GeometryAttribute"* %3)
  %conv = sext i8 %call2 to i32
  store i32 %conv, i32* %num_components, align 4
  store i1 false, i1* %nrvo, align 1
  %4 = bitcast %"class.draco::AttributeQuantizationTransform"* %this1 to %"class.draco::AttributeTransform"*
  %5 = load i32, i32* %num_entries, align 4
  %6 = load i32, i32* %num_components, align 4
  %7 = load i32, i32* %num_points.addr, align 4
  %8 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %attribute.addr, align 4
  call void @_ZNK5draco18AttributeTransform21InitPortableAttributeEiiiRKNS_14PointAttributeEb(%"class.std::__2::unique_ptr.39"* sret align 4 %agg.result, %"class.draco::AttributeTransform"* %4, i32 %5, i32 %6, i32 %7, %"class.draco::PointAttribute"* nonnull align 8 dereferenceable(92) %8, i1 zeroext true)
  %call3 = call %"class.draco::PointAttribute"* @_ZNKSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr.39"* %agg.result) #8
  %9 = bitcast %"class.draco::PointAttribute"* %call3 to %"class.draco::GeometryAttribute"*
  %call4 = call %"class.draco::IndexType"* @_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEC2Ej(%"class.draco::IndexType"* %agg.tmp, i32 0)
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %agg.tmp, i32 0, i32 0
  %10 = load i32, i32* %coerce.dive, align 4
  %call5 = call i8* @_ZN5draco17GeometryAttribute10GetAddressENS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEE(%"class.draco::GeometryAttribute"* %9, i32 %10)
  %11 = bitcast i8* %call5 to i32*
  store i32* %11, i32** %portable_attribute_data, align 4
  %quantization_bits_ = getelementptr inbounds %"class.draco::AttributeQuantizationTransform", %"class.draco::AttributeQuantizationTransform"* %this1, i32 0, i32 1
  %12 = load i32, i32* %quantization_bits_, align 4
  %shl = shl i32 1, %12
  %sub = sub nsw i32 %shl, 1
  store i32 %sub, i32* %max_quantized_value, align 4
  %call6 = call %"class.draco::Quantizer"* @_ZN5draco9QuantizerC1Ev(%"class.draco::Quantizer"* %quantizer)
  %call7 = call float @_ZNK5draco30AttributeQuantizationTransform5rangeEv(%"class.draco::AttributeQuantizationTransform"* %this1)
  %13 = load i32, i32* %max_quantized_value, align 4
  call void @_ZN5draco9Quantizer4InitEfi(%"class.draco::Quantizer"* %quantizer, float %call7, i32 %13)
  store i32 0, i32* %dst_index, align 4
  %14 = load i32, i32* %num_components, align 4
  %15 = call { i32, i1 } @llvm.umul.with.overflow.i32(i32 %14, i32 4)
  %16 = extractvalue { i32, i1 } %15, 1
  %17 = extractvalue { i32, i1 } %15, 0
  %18 = select i1 %16, i32 -1, i32 %17
  %call8 = call noalias nonnull i8* @_Znam(i32 %18) #9
  %19 = bitcast i8* %call8 to float*
  %call9 = call %"class.std::__2::unique_ptr.23"* @_ZNSt3__210unique_ptrIA_fNS_14default_deleteIS1_EEEC2IPfLb1EvvEET_(%"class.std::__2::unique_ptr.23"* %att_val, float* %19) #8
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc28, %entry
  %20 = load i32, i32* %i, align 4
  %21 = load %"class.std::__2::vector.45"*, %"class.std::__2::vector.45"** %point_ids.addr, align 4
  %call10 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.45"* %21) #8
  %cmp = icmp ult i32 %20, %call10
  br i1 %cmp, label %for.body, label %for.end30

for.body:                                         ; preds = %for.cond
  %22 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %attribute.addr, align 4
  %23 = load %"class.std::__2::vector.45"*, %"class.std::__2::vector.45"** %point_ids.addr, align 4
  %24 = load i32, i32* %i, align 4
  %call12 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.44"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEEixEm(%"class.std::__2::vector.45"* %23, i32 %24) #8
  %25 = bitcast %"class.draco::IndexType.44"* %agg.tmp11 to i8*
  %26 = bitcast %"class.draco::IndexType.44"* %call12 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %25, i8* align 4 %26, i32 4, i1 false)
  %coerce.dive13 = getelementptr inbounds %"class.draco::IndexType.44", %"class.draco::IndexType.44"* %agg.tmp11, i32 0, i32 0
  %27 = load i32, i32* %coerce.dive13, align 4
  %call14 = call i32 @_ZNK5draco14PointAttribute12mapped_indexENS_9IndexTypeIjNS_20PointIndex_tag_type_EEE(%"class.draco::PointAttribute"* %22, i32 %27)
  %coerce.dive15 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %att_val_id, i32 0, i32 0
  store i32 %call14, i32* %coerce.dive15, align 4
  %28 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %attribute.addr, align 4
  %29 = bitcast %"class.draco::PointAttribute"* %28 to %"class.draco::GeometryAttribute"*
  %30 = bitcast %"class.draco::IndexType"* %agg.tmp16 to i8*
  %31 = bitcast %"class.draco::IndexType"* %att_val_id to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %30, i8* align 4 %31, i32 4, i1 false)
  %call17 = call float* @_ZNKSt3__210unique_ptrIA_fNS_14default_deleteIS1_EEE3getEv(%"class.std::__2::unique_ptr.23"* %att_val) #8
  %32 = bitcast float* %call17 to i8*
  %coerce.dive18 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %agg.tmp16, i32 0, i32 0
  %33 = load i32, i32* %coerce.dive18, align 4
  call void @_ZNK5draco17GeometryAttribute8GetValueENS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEEPv(%"class.draco::GeometryAttribute"* %29, i32 %33, i8* %32)
  store i32 0, i32* %c, align 4
  br label %for.cond19

for.cond19:                                       ; preds = %for.inc, %for.body
  %34 = load i32, i32* %c, align 4
  %35 = load i32, i32* %num_components, align 4
  %cmp20 = icmp slt i32 %34, %35
  br i1 %cmp20, label %for.body21, label %for.end

for.body21:                                       ; preds = %for.cond19
  %36 = load i32, i32* %c, align 4
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNKSt3__210unique_ptrIA_fNS_14default_deleteIS1_EEEixEm(%"class.std::__2::unique_ptr.23"* %att_val, i32 %36)
  %37 = load float, float* %call22, align 4
  %call23 = call nonnull align 4 dereferenceable(12) %"class.std::__2::vector"* @_ZNK5draco30AttributeQuantizationTransform10min_valuesEv(%"class.draco::AttributeQuantizationTransform"* %this1)
  %38 = load i32, i32* %c, align 4
  %call24 = call nonnull align 4 dereferenceable(4) float* @_ZNKSt3__26vectorIfNS_9allocatorIfEEEixEm(%"class.std::__2::vector"* %call23, i32 %38) #8
  %39 = load float, float* %call24, align 4
  %sub25 = fsub float %37, %39
  store float %sub25, float* %value, align 4
  %40 = load float, float* %value, align 4
  %call26 = call i32 @_ZNK5draco9Quantizer13QuantizeFloatEf(%"class.draco::Quantizer"* %quantizer, float %40)
  store i32 %call26, i32* %q_val, align 4
  %41 = load i32, i32* %q_val, align 4
  %42 = load i32*, i32** %portable_attribute_data, align 4
  %43 = load i32, i32* %dst_index, align 4
  %inc = add nsw i32 %43, 1
  store i32 %inc, i32* %dst_index, align 4
  %arrayidx = getelementptr inbounds i32, i32* %42, i32 %43
  store i32 %41, i32* %arrayidx, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body21
  %44 = load i32, i32* %c, align 4
  %inc27 = add nsw i32 %44, 1
  store i32 %inc27, i32* %c, align 4
  br label %for.cond19

for.end:                                          ; preds = %for.cond19
  br label %for.inc28

for.inc28:                                        ; preds = %for.end
  %45 = load i32, i32* %i, align 4
  %inc29 = add i32 %45, 1
  store i32 %inc29, i32* %i, align 4
  br label %for.cond

for.end30:                                        ; preds = %for.cond
  store i1 true, i1* %nrvo, align 1
  %call31 = call %"class.std::__2::unique_ptr.23"* @_ZNSt3__210unique_ptrIA_fNS_14default_deleteIS1_EEED2Ev(%"class.std::__2::unique_ptr.23"* %att_val) #8
  %nrvo.val = load i1, i1* %nrvo, align 1
  br i1 %nrvo.val, label %nrvo.skipdtor, label %nrvo.unused

nrvo.unused:                                      ; preds = %for.end30
  %call32 = call %"class.std::__2::unique_ptr.39"* @_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.39"* %agg.result) #8
  br label %nrvo.skipdtor

nrvo.skipdtor:                                    ; preds = %nrvo.unused, %for.end30
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.45"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.45"*, align 4
  store %"class.std::__2::vector.45"* %this, %"class.std::__2::vector.45"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.45"*, %"class.std::__2::vector.45"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.45"* %this1 to %"class.std::__2::__vector_base.46"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.46", %"class.std::__2::__vector_base.46"* %0, i32 0, i32 1
  %1 = load %"class.draco::IndexType.44"*, %"class.draco::IndexType.44"** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector.45"* %this1 to %"class.std::__2::__vector_base.46"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.46", %"class.std::__2::__vector_base.46"* %2, i32 0, i32 0
  %3 = load %"class.draco::IndexType.44"*, %"class.draco::IndexType.44"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.draco::IndexType.44"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.draco::IndexType.44"* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.44"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEEixEm(%"class.std::__2::vector.45"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.45"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector.45"* %this, %"class.std::__2::vector.45"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.45"*, %"class.std::__2::vector.45"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.45"* %this1 to %"class.std::__2::__vector_base.46"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.46", %"class.std::__2::__vector_base.46"* %0, i32 0, i32 0
  %1 = load %"class.draco::IndexType.44"*, %"class.draco::IndexType.44"** %__begin_, align 4
  %2 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds %"class.draco::IndexType.44", %"class.draco::IndexType.44"* %1, i32 %2
  ret %"class.draco::IndexType.44"* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::AttributeQuantizationTransform"* @_ZN5draco30AttributeQuantizationTransformD2Ev(%"class.draco::AttributeQuantizationTransform"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::AttributeQuantizationTransform"*, align 4
  store %"class.draco::AttributeQuantizationTransform"* %this, %"class.draco::AttributeQuantizationTransform"** %this.addr, align 4
  %this1 = load %"class.draco::AttributeQuantizationTransform"*, %"class.draco::AttributeQuantizationTransform"** %this.addr, align 4
  %0 = bitcast %"class.draco::AttributeQuantizationTransform"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [7 x i8*] }, { [7 x i8*] }* @_ZTVN5draco30AttributeQuantizationTransformE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %min_values_ = getelementptr inbounds %"class.draco::AttributeQuantizationTransform", %"class.draco::AttributeQuantizationTransform"* %this1, i32 0, i32 2
  %call = call %"class.std::__2::vector"* @_ZNSt3__26vectorIfNS_9allocatorIfEEED2Ev(%"class.std::__2::vector"* %min_values_) #8
  %1 = bitcast %"class.draco::AttributeQuantizationTransform"* %this1 to %"class.draco::AttributeTransform"*
  %call2 = call %"class.draco::AttributeTransform"* @_ZN5draco18AttributeTransformD2Ev(%"class.draco::AttributeTransform"* %1) #8
  ret %"class.draco::AttributeQuantizationTransform"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco30AttributeQuantizationTransformD0Ev(%"class.draco::AttributeQuantizationTransform"* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::AttributeQuantizationTransform"*, align 4
  store %"class.draco::AttributeQuantizationTransform"* %this, %"class.draco::AttributeQuantizationTransform"** %this.addr, align 4
  %this1 = load %"class.draco::AttributeQuantizationTransform"*, %"class.draco::AttributeQuantizationTransform"** %this.addr, align 4
  %call = call %"class.draco::AttributeQuantizationTransform"* @_ZN5draco30AttributeQuantizationTransformD2Ev(%"class.draco::AttributeQuantizationTransform"* %this1) #8
  %0 = bitcast %"class.draco::AttributeQuantizationTransform"* %this1 to i8*
  call void @_ZdlPv(i8* %0) #10
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco30AttributeQuantizationTransform4TypeEv(%"class.draco::AttributeQuantizationTransform"* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::AttributeQuantizationTransform"*, align 4
  store %"class.draco::AttributeQuantizationTransform"* %this, %"class.draco::AttributeQuantizationTransform"** %this.addr, align 4
  %this1 = load %"class.draco::AttributeQuantizationTransform"*, %"class.draco::AttributeQuantizationTransform"** %this.addr, align 4
  ret i32 1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::AttributeTransformData"* @_ZNKSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE3getEv(%"class.std::__2::unique_ptr.18"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.18"*, align 4
  store %"class.std::__2::unique_ptr.18"* %this, %"class.std::__2::unique_ptr.18"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.18"*, %"class.std::__2::unique_ptr.18"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.18", %"class.std::__2::unique_ptr.18"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributeTransformData"** @_ZNKSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.19"* %__ptr_) #8
  %0 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %call, align 4
  ret %"class.draco::AttributeTransformData"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::AttributeTransformData"** @_ZNKSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.19"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.19"*, align 4
  store %"class.std::__2::__compressed_pair.19"* %this, %"class.std::__2::__compressed_pair.19"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.19"*, %"class.std::__2::__compressed_pair.19"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.19"* %this1 to %"struct.std::__2::__compressed_pair_elem.20"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributeTransformData"** @_ZNKSt3__222__compressed_pair_elemIPN5draco22AttributeTransformDataELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.20"* %0) #8
  ret %"class.draco::AttributeTransformData"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::AttributeTransformData"** @_ZNKSt3__222__compressed_pair_elemIPN5draco22AttributeTransformDataELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.20"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.20"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.20"* %this, %"struct.std::__2::__compressed_pair_elem.20"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.20"*, %"struct.std::__2::__compressed_pair_elem.20"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.20", %"struct.std::__2::__compressed_pair_elem.20"* %this1, i32 0, i32 0
  ret %"class.draco::AttributeTransformData"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EE5valueEv(%"class.draco::IndexType"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.draco::IndexType"* %this, %"class.draco::IndexType"** %this.addr, align 4
  %this1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value_, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK5draco10DataBuffer4ReadExPvm(%"class.draco::DataBuffer"* %this, i64 %byte_pos, i8* %out_data, i32 %data_size) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::DataBuffer"*, align 4
  %byte_pos.addr = alloca i64, align 8
  %out_data.addr = alloca i8*, align 4
  %data_size.addr = alloca i32, align 4
  store %"class.draco::DataBuffer"* %this, %"class.draco::DataBuffer"** %this.addr, align 4
  store i64 %byte_pos, i64* %byte_pos.addr, align 8
  store i8* %out_data, i8** %out_data.addr, align 4
  store i32 %data_size, i32* %data_size.addr, align 4
  %this1 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %this.addr, align 4
  %0 = load i8*, i8** %out_data.addr, align 4
  %call = call i8* @_ZNK5draco10DataBuffer4dataEv(%"class.draco::DataBuffer"* %this1)
  %1 = load i64, i64* %byte_pos.addr, align 8
  %idx.ext = trunc i64 %1 to i32
  %add.ptr = getelementptr inbounds i8, i8* %call, i32 %idx.ext
  %2 = load i32, i32* %data_size.addr, align 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %0, i8* align 1 %add.ptr, i32 %2, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNK5draco10DataBuffer4dataEv(%"class.draco::DataBuffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::DataBuffer"*, align 4
  store %"class.draco::DataBuffer"* %this, %"class.draco::DataBuffer"** %this.addr, align 4
  %this1 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %this.addr, align 4
  %data_ = getelementptr inbounds %"class.draco::DataBuffer", %"class.draco::DataBuffer"* %this1, i32 0, i32 0
  %call = call i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector.1"* %data_) #8
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector.1"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.1"*, align 4
  store %"class.std::__2::vector.1"* %this, %"class.std::__2::vector.1"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base.2"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %0, i32 0, i32 0
  %1 = load i8*, i8** %__begin_, align 4
  %call = call i8* @_ZNSt3__212__to_addressIhEEPT_S2_(i8* %1) #8
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__212__to_addressIhEEPT_S2_(i8* %__p) #0 comdat {
entry:
  %__p.addr = alloca i8*, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %0 = load i8*, i8** %__p.addr, align 4
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK5draco13EncoderBuffer18bit_encoder_activeEv(%"class.draco::EncoderBuffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::EncoderBuffer"*, align 4
  store %"class.draco::EncoderBuffer"* %this, %"class.draco::EncoderBuffer"** %this.addr, align 4
  %this1 = load %"class.draco::EncoderBuffer"*, %"class.draco::EncoderBuffer"** %this.addr, align 4
  %bit_encoder_reserved_bytes_ = getelementptr inbounds %"class.draco::EncoderBuffer", %"class.draco::EncoderBuffer"* %this1, i32 0, i32 2
  %0 = load i64, i64* %bit_encoder_reserved_bytes_, align 8
  %cmp = icmp sgt i64 %0, 0
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__26vectorIcNS_9allocatorIcEEE6insertIPKhEENS_9enable_ifIXaasr27__is_cpp17_forward_iteratorIT_EE5valuesr16is_constructibleIcNS_15iterator_traitsIS8_E9referenceEEE5valueENS_11__wrap_iterIPcEEE4typeENSC_IPKcEES8_S8_(%"class.std::__2::vector.27"* %this, i8* %__position.coerce, i8* %__first, i8* %__last) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__wrap_iter.52", align 4
  %__position = alloca %"class.std::__2::__wrap_iter", align 4
  %this.addr = alloca %"class.std::__2::vector.27"*, align 4
  %__first.addr = alloca i8*, align 4
  %__last.addr = alloca i8*, align 4
  %__p = alloca i8*, align 4
  %ref.tmp = alloca %"class.std::__2::__wrap_iter.52", align 4
  %__n = alloca i32, align 4
  %__old_n = alloca i32, align 4
  %__old_last = alloca i8*, align 4
  %__m = alloca i8*, align 4
  %__dx = alloca i32, align 4
  %__diff = alloca i32, align 4
  %__a = alloca %"class.std::__2::allocator.32"*, align 4
  %__v = alloca %"struct.std::__2::__split_buffer", align 4
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %__position, i32 0, i32 0
  store i8* %__position.coerce, i8** %coerce.dive, align 4
  store %"class.std::__2::vector.27"* %this, %"class.std::__2::vector.27"** %this.addr, align 4
  store i8* %__first, i8** %__first.addr, align 4
  store i8* %__last, i8** %__last.addr, align 4
  %this1 = load %"class.std::__2::vector.27"*, %"class.std::__2::vector.27"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.27"* %this1 to %"class.std::__2::__vector_base.28"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.28", %"class.std::__2::__vector_base.28"* %0, i32 0, i32 0
  %1 = load i8*, i8** %__begin_, align 4
  %call = call i8* @_ZNSt3__26vectorIcNS_9allocatorIcEEE5beginEv(%"class.std::__2::vector.27"* %this1) #8
  %coerce.dive2 = getelementptr inbounds %"class.std::__2::__wrap_iter.52", %"class.std::__2::__wrap_iter.52"* %ref.tmp, i32 0, i32 0
  store i8* %call, i8** %coerce.dive2, align 4
  %call3 = call i32 @_ZNSt3__2miIPKcPcEEDTmicldtfp_4baseEcldtfp0_4baseEERKNS_11__wrap_iterIT_EERKNS5_IT0_EE(%"class.std::__2::__wrap_iter"* nonnull align 4 dereferenceable(4) %__position, %"class.std::__2::__wrap_iter.52"* nonnull align 4 dereferenceable(4) %ref.tmp) #8
  %add.ptr = getelementptr inbounds i8, i8* %1, i32 %call3
  store i8* %add.ptr, i8** %__p, align 4
  %2 = load i8*, i8** %__first.addr, align 4
  %3 = load i8*, i8** %__last.addr, align 4
  %call4 = call i32 @_ZNSt3__28distanceIPKhEENS_15iterator_traitsIT_E15difference_typeES4_S4_(i8* %2, i8* %3)
  store i32 %call4, i32* %__n, align 4
  %4 = load i32, i32* %__n, align 4
  %cmp = icmp sgt i32 %4, 0
  br i1 %cmp, label %if.then, label %if.end35

if.then:                                          ; preds = %entry
  %5 = load i32, i32* %__n, align 4
  %6 = bitcast %"class.std::__2::vector.27"* %this1 to %"class.std::__2::__vector_base.28"*
  %call5 = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__213__vector_baseIcNS_9allocatorIcEEE9__end_capEv(%"class.std::__2::__vector_base.28"* %6) #8
  %7 = load i8*, i8** %call5, align 4
  %8 = bitcast %"class.std::__2::vector.27"* %this1 to %"class.std::__2::__vector_base.28"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.28", %"class.std::__2::__vector_base.28"* %8, i32 0, i32 1
  %9 = load i8*, i8** %__end_, align 4
  %sub.ptr.lhs.cast = ptrtoint i8* %7 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %9 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %cmp6 = icmp sle i32 %5, %sub.ptr.sub
  br i1 %cmp6, label %if.then7, label %if.else

if.then7:                                         ; preds = %if.then
  %10 = load i32, i32* %__n, align 4
  store i32 %10, i32* %__old_n, align 4
  %11 = bitcast %"class.std::__2::vector.27"* %this1 to %"class.std::__2::__vector_base.28"*
  %__end_8 = getelementptr inbounds %"class.std::__2::__vector_base.28", %"class.std::__2::__vector_base.28"* %11, i32 0, i32 1
  %12 = load i8*, i8** %__end_8, align 4
  store i8* %12, i8** %__old_last, align 4
  %13 = load i8*, i8** %__last.addr, align 4
  store i8* %13, i8** %__m, align 4
  %14 = bitcast %"class.std::__2::vector.27"* %this1 to %"class.std::__2::__vector_base.28"*
  %__end_9 = getelementptr inbounds %"class.std::__2::__vector_base.28", %"class.std::__2::__vector_base.28"* %14, i32 0, i32 1
  %15 = load i8*, i8** %__end_9, align 4
  %16 = load i8*, i8** %__p, align 4
  %sub.ptr.lhs.cast10 = ptrtoint i8* %15 to i32
  %sub.ptr.rhs.cast11 = ptrtoint i8* %16 to i32
  %sub.ptr.sub12 = sub i32 %sub.ptr.lhs.cast10, %sub.ptr.rhs.cast11
  store i32 %sub.ptr.sub12, i32* %__dx, align 4
  %17 = load i32, i32* %__n, align 4
  %18 = load i32, i32* %__dx, align 4
  %cmp13 = icmp sgt i32 %17, %18
  br i1 %cmp13, label %if.then14, label %if.end

if.then14:                                        ; preds = %if.then7
  %19 = load i8*, i8** %__first.addr, align 4
  store i8* %19, i8** %__m, align 4
  %20 = bitcast %"class.std::__2::vector.27"* %this1 to %"class.std::__2::__vector_base.28"*
  %__end_15 = getelementptr inbounds %"class.std::__2::__vector_base.28", %"class.std::__2::__vector_base.28"* %20, i32 0, i32 1
  %21 = load i8*, i8** %__end_15, align 4
  %22 = load i8*, i8** %__p, align 4
  %sub.ptr.lhs.cast16 = ptrtoint i8* %21 to i32
  %sub.ptr.rhs.cast17 = ptrtoint i8* %22 to i32
  %sub.ptr.sub18 = sub i32 %sub.ptr.lhs.cast16, %sub.ptr.rhs.cast17
  store i32 %sub.ptr.sub18, i32* %__diff, align 4
  %23 = load i32, i32* %__diff, align 4
  call void @_ZNSt3__27advanceIPKhEEvRT_NS_15iterator_traitsIS3_E15difference_typeE(i8** nonnull align 4 dereferenceable(4) %__m, i32 %23)
  %24 = load i8*, i8** %__m, align 4
  %25 = load i8*, i8** %__last.addr, align 4
  %26 = load i32, i32* %__n, align 4
  %27 = load i32, i32* %__diff, align 4
  %sub = sub nsw i32 %26, %27
  call void @_ZNSt3__26vectorIcNS_9allocatorIcEEE18__construct_at_endIPKhEENS_9enable_ifIXsr27__is_cpp17_forward_iteratorIT_EE5valueEvE4typeES8_S8_m(%"class.std::__2::vector.27"* %this1, i8* %24, i8* %25, i32 %sub)
  %28 = load i32, i32* %__dx, align 4
  store i32 %28, i32* %__n, align 4
  br label %if.end

if.end:                                           ; preds = %if.then14, %if.then7
  %29 = load i32, i32* %__n, align 4
  %cmp19 = icmp sgt i32 %29, 0
  br i1 %cmp19, label %if.then20, label %if.end23

if.then20:                                        ; preds = %if.end
  %30 = load i8*, i8** %__p, align 4
  %31 = load i8*, i8** %__old_last, align 4
  %32 = load i8*, i8** %__p, align 4
  %33 = load i32, i32* %__old_n, align 4
  %add.ptr21 = getelementptr inbounds i8, i8* %32, i32 %33
  call void @_ZNSt3__26vectorIcNS_9allocatorIcEEE12__move_rangeEPcS4_S4_(%"class.std::__2::vector.27"* %this1, i8* %30, i8* %31, i8* %add.ptr21)
  %34 = load i8*, i8** %__first.addr, align 4
  %35 = load i8*, i8** %__m, align 4
  %36 = load i8*, i8** %__p, align 4
  %call22 = call i8* @_ZNSt3__24copyIPKhPcEET0_T_S5_S4_(i8* %34, i8* %35, i8* %36)
  br label %if.end23

if.end23:                                         ; preds = %if.then20, %if.end
  br label %if.end34

if.else:                                          ; preds = %if.then
  %37 = bitcast %"class.std::__2::vector.27"* %this1 to %"class.std::__2::__vector_base.28"*
  %call24 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.32"* @_ZNSt3__213__vector_baseIcNS_9allocatorIcEEE7__allocEv(%"class.std::__2::__vector_base.28"* %37) #8
  store %"class.std::__2::allocator.32"* %call24, %"class.std::__2::allocator.32"** %__a, align 4
  %call25 = call i32 @_ZNKSt3__26vectorIcNS_9allocatorIcEEE4sizeEv(%"class.std::__2::vector.27"* %this1) #8
  %38 = load i32, i32* %__n, align 4
  %add = add i32 %call25, %38
  %call26 = call i32 @_ZNKSt3__26vectorIcNS_9allocatorIcEEE11__recommendEm(%"class.std::__2::vector.27"* %this1, i32 %add)
  %39 = load i8*, i8** %__p, align 4
  %40 = bitcast %"class.std::__2::vector.27"* %this1 to %"class.std::__2::__vector_base.28"*
  %__begin_27 = getelementptr inbounds %"class.std::__2::__vector_base.28", %"class.std::__2::__vector_base.28"* %40, i32 0, i32 0
  %41 = load i8*, i8** %__begin_27, align 4
  %sub.ptr.lhs.cast28 = ptrtoint i8* %39 to i32
  %sub.ptr.rhs.cast29 = ptrtoint i8* %41 to i32
  %sub.ptr.sub30 = sub i32 %sub.ptr.lhs.cast28, %sub.ptr.rhs.cast29
  %42 = load %"class.std::__2::allocator.32"*, %"class.std::__2::allocator.32"** %__a, align 4
  %call31 = call %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEEC2EmmS3_(%"struct.std::__2::__split_buffer"* %__v, i32 %call26, i32 %sub.ptr.sub30, %"class.std::__2::allocator.32"* nonnull align 1 dereferenceable(1) %42)
  %43 = load i8*, i8** %__first.addr, align 4
  %44 = load i8*, i8** %__last.addr, align 4
  call void @_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE18__construct_at_endIPKhEENS_9enable_ifIXsr27__is_cpp17_forward_iteratorIT_EE5valueEvE4typeES9_S9_(%"struct.std::__2::__split_buffer"* %__v, i8* %43, i8* %44)
  %45 = load i8*, i8** %__p, align 4
  %call32 = call i8* @_ZNSt3__26vectorIcNS_9allocatorIcEEE26__swap_out_circular_bufferERNS_14__split_bufferIcRS2_EEPc(%"class.std::__2::vector.27"* %this1, %"struct.std::__2::__split_buffer"* nonnull align 4 dereferenceable(20) %__v, i8* %45)
  store i8* %call32, i8** %__p, align 4
  %call33 = call %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEED2Ev(%"struct.std::__2::__split_buffer"* %__v) #8
  br label %if.end34

if.end34:                                         ; preds = %if.else, %if.end23
  br label %if.end35

if.end35:                                         ; preds = %if.end34, %entry
  %46 = load i8*, i8** %__p, align 4
  %call36 = call i8* @_ZNSt3__26vectorIcNS_9allocatorIcEEE11__make_iterEPc(%"class.std::__2::vector.27"* %this1, i8* %46) #8
  %coerce.dive37 = getelementptr inbounds %"class.std::__2::__wrap_iter.52", %"class.std::__2::__wrap_iter.52"* %retval, i32 0, i32 0
  store i8* %call36, i8** %coerce.dive37, align 4
  %coerce.dive38 = getelementptr inbounds %"class.std::__2::__wrap_iter.52", %"class.std::__2::__wrap_iter.52"* %retval, i32 0, i32 0
  %47 = load i8*, i8** %coerce.dive38, align 4
  ret i8* %47
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__26vectorIcNS_9allocatorIcEEE3endEv(%"class.std::__2::vector.27"* %this) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__wrap_iter.52", align 4
  %this.addr = alloca %"class.std::__2::vector.27"*, align 4
  store %"class.std::__2::vector.27"* %this, %"class.std::__2::vector.27"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.27"*, %"class.std::__2::vector.27"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.27"* %this1 to %"class.std::__2::__vector_base.28"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.28", %"class.std::__2::__vector_base.28"* %0, i32 0, i32 1
  %1 = load i8*, i8** %__end_, align 4
  %call = call i8* @_ZNSt3__26vectorIcNS_9allocatorIcEEE11__make_iterEPc(%"class.std::__2::vector.27"* %this1, i8* %1) #8
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter.52", %"class.std::__2::__wrap_iter.52"* %retval, i32 0, i32 0
  store i8* %call, i8** %coerce.dive, align 4
  %coerce.dive2 = getelementptr inbounds %"class.std::__2::__wrap_iter.52", %"class.std::__2::__wrap_iter.52"* %retval, i32 0, i32 0
  %2 = load i8*, i8** %coerce.dive2, align 4
  ret i8* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__wrap_iter"* @_ZNSt3__211__wrap_iterIPKcEC2IPcEERKNS0_IT_EEPNS_9enable_ifIXsr14is_convertibleIS6_S2_EE5valueEvE4typeE(%"class.std::__2::__wrap_iter"* returned %this, %"class.std::__2::__wrap_iter.52"* nonnull align 4 dereferenceable(4) %__u, i8* %0) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__wrap_iter"*, align 4
  %__u.addr = alloca %"class.std::__2::__wrap_iter.52"*, align 4
  %.addr = alloca i8*, align 4
  store %"class.std::__2::__wrap_iter"* %this, %"class.std::__2::__wrap_iter"** %this.addr, align 4
  store %"class.std::__2::__wrap_iter.52"* %__u, %"class.std::__2::__wrap_iter.52"** %__u.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::__wrap_iter"*, %"class.std::__2::__wrap_iter"** %this.addr, align 4
  %__i = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::__wrap_iter.52"*, %"class.std::__2::__wrap_iter.52"** %__u.addr, align 4
  %call = call i8* @_ZNKSt3__211__wrap_iterIPcE4baseEv(%"class.std::__2::__wrap_iter.52"* %1) #8
  store i8* %call, i8** %__i, align 4
  ret %"class.std::__2::__wrap_iter"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__2miIPKcPcEEDTmicldtfp_4baseEcldtfp0_4baseEERKNS_11__wrap_iterIT_EERKNS5_IT0_EE(%"class.std::__2::__wrap_iter"* nonnull align 4 dereferenceable(4) %__x, %"class.std::__2::__wrap_iter.52"* nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %__x.addr = alloca %"class.std::__2::__wrap_iter"*, align 4
  %__y.addr = alloca %"class.std::__2::__wrap_iter.52"*, align 4
  store %"class.std::__2::__wrap_iter"* %__x, %"class.std::__2::__wrap_iter"** %__x.addr, align 4
  store %"class.std::__2::__wrap_iter.52"* %__y, %"class.std::__2::__wrap_iter.52"** %__y.addr, align 4
  %0 = load %"class.std::__2::__wrap_iter"*, %"class.std::__2::__wrap_iter"** %__x.addr, align 4
  %call = call i8* @_ZNKSt3__211__wrap_iterIPKcE4baseEv(%"class.std::__2::__wrap_iter"* %0) #8
  %1 = load %"class.std::__2::__wrap_iter.52"*, %"class.std::__2::__wrap_iter.52"** %__y.addr, align 4
  %call1 = call i8* @_ZNKSt3__211__wrap_iterIPcE4baseEv(%"class.std::__2::__wrap_iter.52"* %1) #8
  %sub.ptr.lhs.cast = ptrtoint i8* %call to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %call1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  ret i32 %sub.ptr.sub
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__26vectorIcNS_9allocatorIcEEE5beginEv(%"class.std::__2::vector.27"* %this) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__wrap_iter.52", align 4
  %this.addr = alloca %"class.std::__2::vector.27"*, align 4
  store %"class.std::__2::vector.27"* %this, %"class.std::__2::vector.27"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.27"*, %"class.std::__2::vector.27"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.27"* %this1 to %"class.std::__2::__vector_base.28"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.28", %"class.std::__2::__vector_base.28"* %0, i32 0, i32 0
  %1 = load i8*, i8** %__begin_, align 4
  %call = call i8* @_ZNSt3__26vectorIcNS_9allocatorIcEEE11__make_iterEPc(%"class.std::__2::vector.27"* %this1, i8* %1) #8
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter.52", %"class.std::__2::__wrap_iter.52"* %retval, i32 0, i32 0
  store i8* %call, i8** %coerce.dive, align 4
  %coerce.dive2 = getelementptr inbounds %"class.std::__2::__wrap_iter.52", %"class.std::__2::__wrap_iter.52"* %retval, i32 0, i32 0
  %2 = load i8*, i8** %coerce.dive2, align 4
  ret i8* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__28distanceIPKhEENS_15iterator_traitsIT_E15difference_typeES4_S4_(i8* %__first, i8* %__last) #0 comdat {
entry:
  %__first.addr = alloca i8*, align 4
  %__last.addr = alloca i8*, align 4
  %agg.tmp = alloca %"struct.std::__2::random_access_iterator_tag", align 1
  store i8* %__first, i8** %__first.addr, align 4
  store i8* %__last, i8** %__last.addr, align 4
  %0 = load i8*, i8** %__first.addr, align 4
  %1 = load i8*, i8** %__last.addr, align 4
  %call = call i32 @_ZNSt3__210__distanceIPKhEENS_15iterator_traitsIT_E15difference_typeES4_S4_NS_26random_access_iterator_tagE(i8* %0, i8* %1)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNSt3__213__vector_baseIcNS_9allocatorIcEEE9__end_capEv(%"class.std::__2::__vector_base.28"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.28"*, align 4
  store %"class.std::__2::__vector_base.28"* %this, %"class.std::__2::__vector_base.28"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.28"*, %"class.std::__2::__vector_base.28"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.28", %"class.std::__2::__vector_base.28"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__217__compressed_pairIPcNS_9allocatorIcEEE5firstEv(%"class.std::__2::__compressed_pair.29"* %__end_cap_) #8
  ret i8** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__27advanceIPKhEEvRT_NS_15iterator_traitsIS3_E15difference_typeE(i8** nonnull align 4 dereferenceable(4) %__i, i32 %__n) #0 comdat {
entry:
  %__i.addr = alloca i8**, align 4
  %__n.addr = alloca i32, align 4
  %agg.tmp = alloca %"struct.std::__2::random_access_iterator_tag", align 1
  store i8** %__i, i8*** %__i.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load i8**, i8*** %__i.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29__advanceIPKhEEvRT_NS_15iterator_traitsIS3_E15difference_typeENS_26random_access_iterator_tagE(i8** nonnull align 4 dereferenceable(4) %0, i32 %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIcNS_9allocatorIcEEE18__construct_at_endIPKhEENS_9enable_ifIXsr27__is_cpp17_forward_iteratorIT_EE5valueEvE4typeES8_S8_m(%"class.std::__2::vector.27"* %this, i8* %__first, i8* %__last, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.27"*, align 4
  %__first.addr = alloca i8*, align 4
  %__last.addr = alloca i8*, align 4
  %__n.addr = alloca i32, align 4
  %__tx = alloca %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction", align 4
  store %"class.std::__2::vector.27"* %this, %"class.std::__2::vector.27"** %this.addr, align 4
  store i8* %__first, i8** %__first.addr, align 4
  store i8* %__last, i8** %__last.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.27"*, %"class.std::__2::vector.27"** %this.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %call = call %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"* @_ZNSt3__26vectorIcNS_9allocatorIcEEE21_ConstructTransactionC2ERS3_m(%"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"* %__tx, %"class.std::__2::vector.27"* nonnull align 4 dereferenceable(12) %this1, i32 %0)
  %1 = bitcast %"class.std::__2::vector.27"* %this1 to %"class.std::__2::__vector_base.28"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.32"* @_ZNSt3__213__vector_baseIcNS_9allocatorIcEEE7__allocEv(%"class.std::__2::__vector_base.28"* %1) #8
  %2 = load i8*, i8** %__first.addr, align 4
  %3 = load i8*, i8** %__last.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction", %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE25__construct_range_forwardIPKhPcEEvRS2_T_S9_RT0_(%"class.std::__2::allocator.32"* nonnull align 1 dereferenceable(1) %call2, i8* %2, i8* %3, i8** nonnull align 4 dereferenceable(4) %__pos_)
  %call3 = call %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"* @_ZNSt3__26vectorIcNS_9allocatorIcEEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"* %__tx) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIcNS_9allocatorIcEEE12__move_rangeEPcS4_S4_(%"class.std::__2::vector.27"* %this, i8* %__from_s, i8* %__from_e, i8* %__to) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.27"*, align 4
  %__from_s.addr = alloca i8*, align 4
  %__from_e.addr = alloca i8*, align 4
  %__to.addr = alloca i8*, align 4
  %__old_last = alloca i8*, align 4
  %__n = alloca i32, align 4
  %__i = alloca i8*, align 4
  %__tx = alloca %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction", align 4
  store %"class.std::__2::vector.27"* %this, %"class.std::__2::vector.27"** %this.addr, align 4
  store i8* %__from_s, i8** %__from_s.addr, align 4
  store i8* %__from_e, i8** %__from_e.addr, align 4
  store i8* %__to, i8** %__to.addr, align 4
  %this1 = load %"class.std::__2::vector.27"*, %"class.std::__2::vector.27"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.27"* %this1 to %"class.std::__2::__vector_base.28"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.28", %"class.std::__2::__vector_base.28"* %0, i32 0, i32 1
  %1 = load i8*, i8** %__end_, align 4
  store i8* %1, i8** %__old_last, align 4
  %2 = load i8*, i8** %__old_last, align 4
  %3 = load i8*, i8** %__to.addr, align 4
  %sub.ptr.lhs.cast = ptrtoint i8* %2 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  store i32 %sub.ptr.sub, i32* %__n, align 4
  %4 = load i8*, i8** %__from_s.addr, align 4
  %5 = load i32, i32* %__n, align 4
  %add.ptr = getelementptr inbounds i8, i8* %4, i32 %5
  store i8* %add.ptr, i8** %__i, align 4
  %6 = load i8*, i8** %__from_e.addr, align 4
  %7 = load i8*, i8** %__i, align 4
  %sub.ptr.lhs.cast2 = ptrtoint i8* %6 to i32
  %sub.ptr.rhs.cast3 = ptrtoint i8* %7 to i32
  %sub.ptr.sub4 = sub i32 %sub.ptr.lhs.cast2, %sub.ptr.rhs.cast3
  %call = call %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"* @_ZNSt3__26vectorIcNS_9allocatorIcEEE21_ConstructTransactionC2ERS3_m(%"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"* %__tx, %"class.std::__2::vector.27"* nonnull align 4 dereferenceable(12) %this1, i32 %sub.ptr.sub4)
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %8 = load i8*, i8** %__i, align 4
  %9 = load i8*, i8** %__from_e.addr, align 4
  %cmp = icmp ult i8* %8, %9
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %10 = bitcast %"class.std::__2::vector.27"* %this1 to %"class.std::__2::__vector_base.28"*
  %call5 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.32"* @_ZNSt3__213__vector_baseIcNS_9allocatorIcEEE7__allocEv(%"class.std::__2::__vector_base.28"* %10) #8
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction", %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %11 = load i8*, i8** %__pos_, align 4
  %call6 = call i8* @_ZNSt3__212__to_addressIcEEPT_S2_(i8* %11) #8
  %12 = load i8*, i8** %__i, align 4
  %call7 = call nonnull align 1 dereferenceable(1) i8* @_ZNSt3__24moveIRcEEONS_16remove_referenceIT_E4typeEOS3_(i8* nonnull align 1 dereferenceable(1) %12) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE9constructIcJcEEEvRS2_PT_DpOT0_(%"class.std::__2::allocator.32"* nonnull align 1 dereferenceable(1) %call5, i8* %call6, i8* nonnull align 1 dereferenceable(1) %call7)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %13 = load i8*, i8** %__i, align 4
  %incdec.ptr = getelementptr inbounds i8, i8* %13, i32 1
  store i8* %incdec.ptr, i8** %__i, align 4
  %__pos_8 = getelementptr inbounds %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction", %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %14 = load i8*, i8** %__pos_8, align 4
  %incdec.ptr9 = getelementptr inbounds i8, i8* %14, i32 1
  store i8* %incdec.ptr9, i8** %__pos_8, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call10 = call %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"* @_ZNSt3__26vectorIcNS_9allocatorIcEEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"* %__tx) #8
  %15 = load i8*, i8** %__from_s.addr, align 4
  %16 = load i8*, i8** %__from_s.addr, align 4
  %17 = load i32, i32* %__n, align 4
  %add.ptr11 = getelementptr inbounds i8, i8* %16, i32 %17
  %18 = load i8*, i8** %__old_last, align 4
  %call12 = call i8* @_ZNSt3__213move_backwardIPcS1_EET0_T_S3_S2_(i8* %15, i8* %add.ptr11, i8* %18)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__24copyIPKhPcEET0_T_S5_S4_(i8* %__first, i8* %__last, i8* %__result) #0 comdat {
entry:
  %__first.addr = alloca i8*, align 4
  %__last.addr = alloca i8*, align 4
  %__result.addr = alloca i8*, align 4
  store i8* %__first, i8** %__first.addr, align 4
  store i8* %__last, i8** %__last.addr, align 4
  store i8* %__result, i8** %__result.addr, align 4
  %0 = load i8*, i8** %__first.addr, align 4
  %call = call i8* @_ZNSt3__213__unwrap_iterIPKhEET_S3_(i8* %0)
  %1 = load i8*, i8** %__last.addr, align 4
  %call1 = call i8* @_ZNSt3__213__unwrap_iterIPKhEET_S3_(i8* %1)
  %2 = load i8*, i8** %__result.addr, align 4
  %call2 = call i8* @_ZNSt3__213__unwrap_iterIPcEET_S2_(i8* %2)
  %call3 = call i8* @_ZNSt3__26__copyIPKhPcEET0_T_S5_S4_(i8* %call, i8* %call1, i8* %call2)
  ret i8* %call3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.32"* @_ZNSt3__213__vector_baseIcNS_9allocatorIcEEE7__allocEv(%"class.std::__2::__vector_base.28"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.28"*, align 4
  store %"class.std::__2::__vector_base.28"* %this, %"class.std::__2::__vector_base.28"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.28"*, %"class.std::__2::__vector_base.28"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.28", %"class.std::__2::__vector_base.28"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.32"* @_ZNSt3__217__compressed_pairIPcNS_9allocatorIcEEE6secondEv(%"class.std::__2::__compressed_pair.29"* %__end_cap_) #8
  ret %"class.std::__2::allocator.32"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIcNS_9allocatorIcEEE11__recommendEm(%"class.std::__2::vector.27"* %this, i32 %__new_size) #0 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %"class.std::__2::vector.27"*, align 4
  %__new_size.addr = alloca i32, align 4
  %__ms = alloca i32, align 4
  %__cap = alloca i32, align 4
  %ref.tmp = alloca i32, align 4
  store %"class.std::__2::vector.27"* %this, %"class.std::__2::vector.27"** %this.addr, align 4
  store i32 %__new_size, i32* %__new_size.addr, align 4
  %this1 = load %"class.std::__2::vector.27"*, %"class.std::__2::vector.27"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIcNS_9allocatorIcEEE8max_sizeEv(%"class.std::__2::vector.27"* %this1) #8
  store i32 %call, i32* %__ms, align 4
  %0 = load i32, i32* %__new_size.addr, align 4
  %1 = load i32, i32* %__ms, align 4
  %cmp = icmp ugt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = bitcast %"class.std::__2::vector.27"* %this1 to %"class.std::__2::__vector_base_common"*
  call void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"* %2) #11
  unreachable

if.end:                                           ; preds = %entry
  %call2 = call i32 @_ZNKSt3__26vectorIcNS_9allocatorIcEEE8capacityEv(%"class.std::__2::vector.27"* %this1) #8
  store i32 %call2, i32* %__cap, align 4
  %3 = load i32, i32* %__cap, align 4
  %4 = load i32, i32* %__ms, align 4
  %div = udiv i32 %4, 2
  %cmp3 = icmp uge i32 %3, %div
  br i1 %cmp3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %if.end
  %5 = load i32, i32* %__ms, align 4
  store i32 %5, i32* %retval, align 4
  br label %return

if.end5:                                          ; preds = %if.end
  %6 = load i32, i32* %__cap, align 4
  %mul = mul i32 2, %6
  store i32 %mul, i32* %ref.tmp, align 4
  %call6 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %__new_size.addr)
  %7 = load i32, i32* %call6, align 4
  store i32 %7, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end5, %if.then4
  %8 = load i32, i32* %retval, align 4
  ret i32 %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIcNS_9allocatorIcEEE4sizeEv(%"class.std::__2::vector.27"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.27"*, align 4
  store %"class.std::__2::vector.27"* %this, %"class.std::__2::vector.27"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.27"*, %"class.std::__2::vector.27"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.27"* %this1 to %"class.std::__2::__vector_base.28"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.28", %"class.std::__2::__vector_base.28"* %0, i32 0, i32 1
  %1 = load i8*, i8** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector.27"* %this1 to %"class.std::__2::__vector_base.28"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.28", %"class.std::__2::__vector_base.28"* %2, i32 0, i32 0
  %3 = load i8*, i8** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint i8* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  ret i32 %sub.ptr.sub
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEEC2EmmS3_(%"struct.std::__2::__split_buffer"* returned %this, i32 %__cap, i32 %__start, %"class.std::__2::allocator.32"* nonnull align 1 dereferenceable(1) %__a) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"struct.std::__2::__split_buffer"*, align 4
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__cap.addr = alloca i32, align 4
  %__start.addr = alloca i32, align 4
  %__a.addr = alloca %"class.std::__2::allocator.32"*, align 4
  %ref.tmp = alloca i8*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store i32 %__cap, i32* %__cap.addr, align 4
  store i32 %__start, i32* %__start.addr, align 4
  store %"class.std::__2::allocator.32"* %__a, %"class.std::__2::allocator.32"** %__a.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer"* %this1, %"struct.std::__2::__split_buffer"** %retval, align 4
  %0 = bitcast %"struct.std::__2::__split_buffer"* %this1 to %"class.std::__2::__split_buffer_common"*
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  store i8* null, i8** %ref.tmp, align 4
  %1 = load %"class.std::__2::allocator.32"*, %"class.std::__2::allocator.32"** %__a.addr, align 4
  %call = call %"class.std::__2::__compressed_pair.53"* @_ZNSt3__217__compressed_pairIPcRNS_9allocatorIcEEEC2IDnS4_EEOT_OT0_(%"class.std::__2::__compressed_pair.53"* %__end_cap_, i8** nonnull align 4 dereferenceable(4) %ref.tmp, %"class.std::__2::allocator.32"* nonnull align 1 dereferenceable(1) %1)
  %2 = load i32, i32* %__cap.addr, align 4
  %cmp = icmp ne i32 %2, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.32"* @_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE7__allocEv(%"struct.std::__2::__split_buffer"* %this1) #8
  %3 = load i32, i32* %__cap.addr, align 4
  %call3 = call i8* @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE8allocateERS2_m(%"class.std::__2::allocator.32"* nonnull align 1 dereferenceable(1) %call2, i32 %3)
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i8* [ %call3, %cond.true ], [ null, %cond.false ]
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  store i8* %cond, i8** %__first_, align 4
  %__first_4 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %4 = load i8*, i8** %__first_4, align 4
  %5 = load i32, i32* %__start.addr, align 4
  %add.ptr = getelementptr inbounds i8, i8* %4, i32 %5
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 2
  store i8* %add.ptr, i8** %__end_, align 4
  %__begin_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 1
  store i8* %add.ptr, i8** %__begin_, align 4
  %__first_5 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %6 = load i8*, i8** %__first_5, align 4
  %7 = load i32, i32* %__cap.addr, align 4
  %add.ptr6 = getelementptr inbounds i8, i8* %6, i32 %7
  %call7 = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this1) #8
  store i8* %add.ptr6, i8** %call7, align 4
  %8 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %retval, align 4
  ret %"struct.std::__2::__split_buffer"* %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE18__construct_at_endIPKhEENS_9enable_ifIXsr27__is_cpp17_forward_iteratorIT_EE5valueEvE4typeES9_S9_(%"struct.std::__2::__split_buffer"* %this, i8* %__first, i8* %__last) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__first.addr = alloca i8*, align 4
  %__last.addr = alloca i8*, align 4
  %__tx = alloca %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction", align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store i8* %__first, i8** %__first.addr, align 4
  store i8* %__last, i8** %__last.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 2
  %0 = load i8*, i8** %__first.addr, align 4
  %1 = load i8*, i8** %__last.addr, align 4
  %call = call i32 @_ZNSt3__28distanceIPKhEENS_15iterator_traitsIT_E15difference_typeES4_S4_(i8* %0, i8* %1)
  %call2 = call %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE21_ConstructTransactionC2EPPcm(%"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"* %__tx, i8** %__end_, i32 %call) #8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %__pos_ = getelementptr inbounds %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"* %__tx, i32 0, i32 0
  %2 = load i8*, i8** %__pos_, align 4
  %__end_3 = getelementptr inbounds %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %3 = load i8*, i8** %__end_3, align 4
  %cmp = icmp ne i8* %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %call4 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.32"* @_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE7__allocEv(%"struct.std::__2::__split_buffer"* %this1) #8
  %__pos_5 = getelementptr inbounds %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"* %__tx, i32 0, i32 0
  %4 = load i8*, i8** %__pos_5, align 4
  %call6 = call i8* @_ZNSt3__212__to_addressIcEEPT_S2_(i8* %4) #8
  %5 = load i8*, i8** %__first.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE9constructIcJRKhEEEvRS2_PT_DpOT0_(%"class.std::__2::allocator.32"* nonnull align 1 dereferenceable(1) %call4, i8* %call6, i8* nonnull align 1 dereferenceable(1) %5)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %__pos_7 = getelementptr inbounds %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"* %__tx, i32 0, i32 0
  %6 = load i8*, i8** %__pos_7, align 4
  %incdec.ptr = getelementptr inbounds i8, i8* %6, i32 1
  store i8* %incdec.ptr, i8** %__pos_7, align 4
  %7 = load i8*, i8** %__first.addr, align 4
  %incdec.ptr8 = getelementptr inbounds i8, i8* %7, i32 1
  store i8* %incdec.ptr8, i8** %__first.addr, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call9 = call %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE21_ConstructTransactionD2Ev(%"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"* %__tx) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__26vectorIcNS_9allocatorIcEEE26__swap_out_circular_bufferERNS_14__split_bufferIcRS2_EEPc(%"class.std::__2::vector.27"* %this, %"struct.std::__2::__split_buffer"* nonnull align 4 dereferenceable(20) %__v, i8* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.27"*, align 4
  %__v.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__p.addr = alloca i8*, align 4
  %__r = alloca i8*, align 4
  store %"class.std::__2::vector.27"* %this, %"class.std::__2::vector.27"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer"* %__v, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %this1 = load %"class.std::__2::vector.27"*, %"class.std::__2::vector.27"** %this.addr, align 4
  call void @_ZNKSt3__26vectorIcNS_9allocatorIcEEE17__annotate_deleteEv(%"class.std::__2::vector.27"* %this1) #8
  %0 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__begin_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %0, i32 0, i32 1
  %1 = load i8*, i8** %__begin_, align 4
  store i8* %1, i8** %__r, align 4
  %2 = bitcast %"class.std::__2::vector.27"* %this1 to %"class.std::__2::__vector_base.28"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.32"* @_ZNSt3__213__vector_baseIcNS_9allocatorIcEEE7__allocEv(%"class.std::__2::__vector_base.28"* %2) #8
  %3 = bitcast %"class.std::__2::vector.27"* %this1 to %"class.std::__2::__vector_base.28"*
  %__begin_2 = getelementptr inbounds %"class.std::__2::__vector_base.28", %"class.std::__2::__vector_base.28"* %3, i32 0, i32 0
  %4 = load i8*, i8** %__begin_2, align 4
  %5 = load i8*, i8** %__p.addr, align 4
  %6 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__begin_3 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %6, i32 0, i32 1
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE46__construct_backward_with_exception_guaranteesIcEENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS2_PT_S8_EE5valuesr31is_trivially_move_constructibleIS8_EE5valueEvE4typeERS2_S9_S9_RS9_(%"class.std::__2::allocator.32"* nonnull align 1 dereferenceable(1) %call, i8* %4, i8* %5, i8** nonnull align 4 dereferenceable(4) %__begin_3)
  %7 = bitcast %"class.std::__2::vector.27"* %this1 to %"class.std::__2::__vector_base.28"*
  %call4 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.32"* @_ZNSt3__213__vector_baseIcNS_9allocatorIcEEE7__allocEv(%"class.std::__2::__vector_base.28"* %7) #8
  %8 = load i8*, i8** %__p.addr, align 4
  %9 = bitcast %"class.std::__2::vector.27"* %this1 to %"class.std::__2::__vector_base.28"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.28", %"class.std::__2::__vector_base.28"* %9, i32 0, i32 1
  %10 = load i8*, i8** %__end_, align 4
  %11 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__end_5 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %11, i32 0, i32 2
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE45__construct_forward_with_exception_guaranteesIcEENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS2_PT_S8_EE5valuesr31is_trivially_move_constructibleIS8_EE5valueEvE4typeERS2_S9_S9_RS9_(%"class.std::__2::allocator.32"* nonnull align 1 dereferenceable(1) %call4, i8* %8, i8* %10, i8** nonnull align 4 dereferenceable(4) %__end_5)
  %12 = bitcast %"class.std::__2::vector.27"* %this1 to %"class.std::__2::__vector_base.28"*
  %__begin_6 = getelementptr inbounds %"class.std::__2::__vector_base.28", %"class.std::__2::__vector_base.28"* %12, i32 0, i32 0
  %13 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__begin_7 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %13, i32 0, i32 1
  call void @_ZNSt3__24swapIPcEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_(i8** nonnull align 4 dereferenceable(4) %__begin_6, i8** nonnull align 4 dereferenceable(4) %__begin_7) #8
  %14 = bitcast %"class.std::__2::vector.27"* %this1 to %"class.std::__2::__vector_base.28"*
  %__end_8 = getelementptr inbounds %"class.std::__2::__vector_base.28", %"class.std::__2::__vector_base.28"* %14, i32 0, i32 1
  %15 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__end_9 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %15, i32 0, i32 2
  call void @_ZNSt3__24swapIPcEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_(i8** nonnull align 4 dereferenceable(4) %__end_8, i8** nonnull align 4 dereferenceable(4) %__end_9) #8
  %16 = bitcast %"class.std::__2::vector.27"* %this1 to %"class.std::__2::__vector_base.28"*
  %call10 = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__213__vector_baseIcNS_9allocatorIcEEE9__end_capEv(%"class.std::__2::__vector_base.28"* %16) #8
  %17 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE9__end_capEv(%"struct.std::__2::__split_buffer"* %17) #8
  call void @_ZNSt3__24swapIPcEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_(i8** nonnull align 4 dereferenceable(4) %call10, i8** nonnull align 4 dereferenceable(4) %call11) #8
  %18 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__begin_12 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %18, i32 0, i32 1
  %19 = load i8*, i8** %__begin_12, align 4
  %20 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %20, i32 0, i32 0
  store i8* %19, i8** %__first_, align 4
  %call13 = call i32 @_ZNKSt3__26vectorIcNS_9allocatorIcEEE4sizeEv(%"class.std::__2::vector.27"* %this1) #8
  call void @_ZNKSt3__26vectorIcNS_9allocatorIcEEE14__annotate_newEm(%"class.std::__2::vector.27"* %this1, i32 %call13) #8
  call void @_ZNSt3__26vectorIcNS_9allocatorIcEEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector.27"* %this1)
  %21 = load i8*, i8** %__r, align 4
  ret i8* %21
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEED2Ev(%"struct.std::__2::__split_buffer"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"struct.std::__2::__split_buffer"*, align 4
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer"* %this1, %"struct.std::__2::__split_buffer"** %retval, align 4
  call void @_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE5clearEv(%"struct.std::__2::__split_buffer"* %this1) #8
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %0 = load i8*, i8** %__first_, align 4
  %tobool = icmp ne i8* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.32"* @_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE7__allocEv(%"struct.std::__2::__split_buffer"* %this1) #8
  %__first_2 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %1 = load i8*, i8** %__first_2, align 4
  %call3 = call i32 @_ZNKSt3__214__split_bufferIcRNS_9allocatorIcEEE8capacityEv(%"struct.std::__2::__split_buffer"* %this1)
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE10deallocateERS2_Pcm(%"class.std::__2::allocator.32"* nonnull align 1 dereferenceable(1) %call, i8* %1, i32 %call3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %retval, align 4
  ret %"struct.std::__2::__split_buffer"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__26vectorIcNS_9allocatorIcEEE11__make_iterEPc(%"class.std::__2::vector.27"* %this, i8* %__p) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__wrap_iter.52", align 4
  %this.addr = alloca %"class.std::__2::vector.27"*, align 4
  %__p.addr = alloca i8*, align 4
  store %"class.std::__2::vector.27"* %this, %"class.std::__2::vector.27"** %this.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %this1 = load %"class.std::__2::vector.27"*, %"class.std::__2::vector.27"** %this.addr, align 4
  %0 = load i8*, i8** %__p.addr, align 4
  %call = call %"class.std::__2::__wrap_iter.52"* @_ZNSt3__211__wrap_iterIPcEC2ES1_(%"class.std::__2::__wrap_iter.52"* %retval, i8* %0) #8
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter.52", %"class.std::__2::__wrap_iter.52"* %retval, i32 0, i32 0
  %1 = load i8*, i8** %coerce.dive, align 4
  ret i8* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__211__wrap_iterIPKcE4baseEv(%"class.std::__2::__wrap_iter"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__wrap_iter"*, align 4
  store %"class.std::__2::__wrap_iter"* %this, %"class.std::__2::__wrap_iter"** %this.addr, align 4
  %this1 = load %"class.std::__2::__wrap_iter"*, %"class.std::__2::__wrap_iter"** %this.addr, align 4
  %__i = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %this1, i32 0, i32 0
  %0 = load i8*, i8** %__i, align 4
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__211__wrap_iterIPcE4baseEv(%"class.std::__2::__wrap_iter.52"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__wrap_iter.52"*, align 4
  store %"class.std::__2::__wrap_iter.52"* %this, %"class.std::__2::__wrap_iter.52"** %this.addr, align 4
  %this1 = load %"class.std::__2::__wrap_iter.52"*, %"class.std::__2::__wrap_iter.52"** %this.addr, align 4
  %__i = getelementptr inbounds %"class.std::__2::__wrap_iter.52", %"class.std::__2::__wrap_iter.52"* %this1, i32 0, i32 0
  %0 = load i8*, i8** %__i, align 4
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__210__distanceIPKhEENS_15iterator_traitsIT_E15difference_typeES4_S4_NS_26random_access_iterator_tagE(i8* %__first, i8* %__last) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::random_access_iterator_tag", align 1
  %__first.addr = alloca i8*, align 4
  %__last.addr = alloca i8*, align 4
  store i8* %__first, i8** %__first.addr, align 4
  store i8* %__last, i8** %__last.addr, align 4
  %1 = load i8*, i8** %__last.addr, align 4
  %2 = load i8*, i8** %__first.addr, align 4
  %sub.ptr.lhs.cast = ptrtoint i8* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %2 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  ret i32 %sub.ptr.sub
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNSt3__217__compressed_pairIPcNS_9allocatorIcEEE5firstEv(%"class.std::__2::__compressed_pair.29"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.29"*, align 4
  store %"class.std::__2::__compressed_pair.29"* %this, %"class.std::__2::__compressed_pair.29"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.29"*, %"class.std::__2::__compressed_pair.29"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.29"* %this1 to %"struct.std::__2::__compressed_pair_elem.30"*
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__222__compressed_pair_elemIPcLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.30"* %0) #8
  ret i8** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNSt3__222__compressed_pair_elemIPcLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.30"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.30"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.30"* %this, %"struct.std::__2::__compressed_pair_elem.30"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.30"*, %"struct.std::__2::__compressed_pair_elem.30"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.30", %"struct.std::__2::__compressed_pair_elem.30"* %this1, i32 0, i32 0
  ret i8** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29__advanceIPKhEEvRT_NS_15iterator_traitsIS3_E15difference_typeENS_26random_access_iterator_tagE(i8** nonnull align 4 dereferenceable(4) %__i, i32 %__n) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::random_access_iterator_tag", align 1
  %__i.addr = alloca i8**, align 4
  %__n.addr = alloca i32, align 4
  store i8** %__i, i8*** %__i.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %2 = load i8**, i8*** %__i.addr, align 4
  %3 = load i8*, i8** %2, align 4
  %add.ptr = getelementptr inbounds i8, i8* %3, i32 %1
  store i8* %add.ptr, i8** %2, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"* @_ZNSt3__26vectorIcNS_9allocatorIcEEE21_ConstructTransactionC2ERS3_m(%"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"* returned %this, %"class.std::__2::vector.27"* nonnull align 4 dereferenceable(12) %__v, i32 %__n) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"*, align 4
  %__v.addr = alloca %"class.std::__2::vector.27"*, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"** %this.addr, align 4
  store %"class.std::__2::vector.27"* %__v, %"class.std::__2::vector.27"** %__v.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"*, %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"** %this.addr, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction", %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::vector.27"*, %"class.std::__2::vector.27"** %__v.addr, align 4
  store %"class.std::__2::vector.27"* %0, %"class.std::__2::vector.27"** %__v_, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction", %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %1 = load %"class.std::__2::vector.27"*, %"class.std::__2::vector.27"** %__v.addr, align 4
  %2 = bitcast %"class.std::__2::vector.27"* %1 to %"class.std::__2::__vector_base.28"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.28", %"class.std::__2::__vector_base.28"* %2, i32 0, i32 1
  %3 = load i8*, i8** %__end_, align 4
  store i8* %3, i8** %__pos_, align 4
  %__new_end_ = getelementptr inbounds %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction", %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"* %this1, i32 0, i32 2
  %4 = load %"class.std::__2::vector.27"*, %"class.std::__2::vector.27"** %__v.addr, align 4
  %5 = bitcast %"class.std::__2::vector.27"* %4 to %"class.std::__2::__vector_base.28"*
  %__end_2 = getelementptr inbounds %"class.std::__2::__vector_base.28", %"class.std::__2::__vector_base.28"* %5, i32 0, i32 1
  %6 = load i8*, i8** %__end_2, align 4
  %7 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds i8, i8* %6, i32 %7
  store i8* %add.ptr, i8** %__new_end_, align 4
  ret %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE25__construct_range_forwardIPKhPcEEvRS2_T_S9_RT0_(%"class.std::__2::allocator.32"* nonnull align 1 dereferenceable(1) %__a, i8* %__begin1, i8* %__end1, i8** nonnull align 4 dereferenceable(4) %__begin2) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.32"*, align 4
  %__begin1.addr = alloca i8*, align 4
  %__end1.addr = alloca i8*, align 4
  %__begin2.addr = alloca i8**, align 4
  store %"class.std::__2::allocator.32"* %__a, %"class.std::__2::allocator.32"** %__a.addr, align 4
  store i8* %__begin1, i8** %__begin1.addr, align 4
  store i8* %__end1, i8** %__end1.addr, align 4
  store i8** %__begin2, i8*** %__begin2.addr, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i8*, i8** %__begin1.addr, align 4
  %1 = load i8*, i8** %__end1.addr, align 4
  %cmp = icmp ne i8* %0, %1
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load %"class.std::__2::allocator.32"*, %"class.std::__2::allocator.32"** %__a.addr, align 4
  %3 = load i8**, i8*** %__begin2.addr, align 4
  %4 = load i8*, i8** %3, align 4
  %call = call i8* @_ZNSt3__212__to_addressIcEEPT_S2_(i8* %4) #8
  %5 = load i8*, i8** %__begin1.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE9constructIcJRKhEEEvRS2_PT_DpOT0_(%"class.std::__2::allocator.32"* nonnull align 1 dereferenceable(1) %2, i8* %call, i8* nonnull align 1 dereferenceable(1) %5)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i8*, i8** %__begin1.addr, align 4
  %incdec.ptr = getelementptr inbounds i8, i8* %6, i32 1
  store i8* %incdec.ptr, i8** %__begin1.addr, align 4
  %7 = load i8**, i8*** %__begin2.addr, align 4
  %8 = load i8*, i8** %7, align 4
  %incdec.ptr1 = getelementptr inbounds i8, i8* %8, i32 1
  store i8* %incdec.ptr1, i8** %7, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"* @_ZNSt3__26vectorIcNS_9allocatorIcEEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"*, align 4
  store %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"** %this.addr, align 4
  %this1 = load %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"*, %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction", %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %0 = load i8*, i8** %__pos_, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction", %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::vector.27"*, %"class.std::__2::vector.27"** %__v_, align 4
  %2 = bitcast %"class.std::__2::vector.27"* %1 to %"class.std::__2::__vector_base.28"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.28", %"class.std::__2::__vector_base.28"* %2, i32 0, i32 1
  store i8* %0, i8** %__end_, align 4
  ret %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE9constructIcJRKhEEEvRS2_PT_DpOT0_(%"class.std::__2::allocator.32"* nonnull align 1 dereferenceable(1) %__a, i8* %__p, i8* nonnull align 1 dereferenceable(1) %__args) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.32"*, align 4
  %__p.addr = alloca i8*, align 4
  %__args.addr = alloca i8*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_construct", align 1
  store %"class.std::__2::allocator.32"* %__a, %"class.std::__2::allocator.32"** %__a.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  store i8* %__args, i8** %__args.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_construct"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.32"*, %"class.std::__2::allocator.32"** %__a.addr, align 4
  %2 = load i8*, i8** %__p.addr, align 4
  %3 = load i8*, i8** %__args.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) i8* @_ZNSt3__27forwardIRKhEEOT_RNS_16remove_referenceIS3_E4typeE(i8* nonnull align 1 dereferenceable(1) %3) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE11__constructIcJRKhEEEvNS_17integral_constantIbLb1EEERS2_PT_DpOT0_(%"class.std::__2::allocator.32"* nonnull align 1 dereferenceable(1) %1, i8* %2, i8* nonnull align 1 dereferenceable(1) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__212__to_addressIcEEPT_S2_(i8* %__p) #0 comdat {
entry:
  %__p.addr = alloca i8*, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %0 = load i8*, i8** %__p.addr, align 4
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE11__constructIcJRKhEEEvNS_17integral_constantIbLb1EEERS2_PT_DpOT0_(%"class.std::__2::allocator.32"* nonnull align 1 dereferenceable(1) %__a, i8* %__p, i8* nonnull align 1 dereferenceable(1) %__args) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.32"*, align 4
  %__p.addr = alloca i8*, align 4
  %__args.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.32"* %__a, %"class.std::__2::allocator.32"** %__a.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  store i8* %__args, i8** %__args.addr, align 4
  %1 = load %"class.std::__2::allocator.32"*, %"class.std::__2::allocator.32"** %__a.addr, align 4
  %2 = load i8*, i8** %__p.addr, align 4
  %3 = load i8*, i8** %__args.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) i8* @_ZNSt3__27forwardIRKhEEOT_RNS_16remove_referenceIS3_E4typeE(i8* nonnull align 1 dereferenceable(1) %3) #8
  call void @_ZNSt3__29allocatorIcE9constructIcJRKhEEEvPT_DpOT0_(%"class.std::__2::allocator.32"* %1, i8* %2, i8* nonnull align 1 dereferenceable(1) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) i8* @_ZNSt3__27forwardIRKhEEOT_RNS_16remove_referenceIS3_E4typeE(i8* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca i8*, align 4
  store i8* %__t, i8** %__t.addr, align 4
  %0 = load i8*, i8** %__t.addr, align 4
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIcE9constructIcJRKhEEEvPT_DpOT0_(%"class.std::__2::allocator.32"* %this, i8* %__p, i8* nonnull align 1 dereferenceable(1) %__args) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.32"*, align 4
  %__p.addr = alloca i8*, align 4
  %__args.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.32"* %this, %"class.std::__2::allocator.32"** %this.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  store i8* %__args, i8** %__args.addr, align 4
  %this1 = load %"class.std::__2::allocator.32"*, %"class.std::__2::allocator.32"** %this.addr, align 4
  %0 = load i8*, i8** %__p.addr, align 4
  %1 = load i8*, i8** %__args.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) i8* @_ZNSt3__27forwardIRKhEEOT_RNS_16remove_referenceIS3_E4typeE(i8* nonnull align 1 dereferenceable(1) %1) #8
  %2 = load i8, i8* %call, align 1
  store i8 %2, i8* %0, align 1
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE9constructIcJcEEEvRS2_PT_DpOT0_(%"class.std::__2::allocator.32"* nonnull align 1 dereferenceable(1) %__a, i8* %__p, i8* nonnull align 1 dereferenceable(1) %__args) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.32"*, align 4
  %__p.addr = alloca i8*, align 4
  %__args.addr = alloca i8*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_construct.55", align 1
  store %"class.std::__2::allocator.32"* %__a, %"class.std::__2::allocator.32"** %__a.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  store i8* %__args, i8** %__args.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_construct.55"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.32"*, %"class.std::__2::allocator.32"** %__a.addr, align 4
  %2 = load i8*, i8** %__p.addr, align 4
  %3 = load i8*, i8** %__args.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) i8* @_ZNSt3__27forwardIcEEOT_RNS_16remove_referenceIS1_E4typeE(i8* nonnull align 1 dereferenceable(1) %3) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE11__constructIcJcEEEvNS_17integral_constantIbLb1EEERS2_PT_DpOT0_(%"class.std::__2::allocator.32"* nonnull align 1 dereferenceable(1) %1, i8* %2, i8* nonnull align 1 dereferenceable(1) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) i8* @_ZNSt3__24moveIRcEEONS_16remove_referenceIT_E4typeEOS3_(i8* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca i8*, align 4
  store i8* %__t, i8** %__t.addr, align 4
  %0 = load i8*, i8** %__t.addr, align 4
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__213move_backwardIPcS1_EET0_T_S3_S2_(i8* %__first, i8* %__last, i8* %__result) #0 comdat {
entry:
  %__first.addr = alloca i8*, align 4
  %__last.addr = alloca i8*, align 4
  %__result.addr = alloca i8*, align 4
  store i8* %__first, i8** %__first.addr, align 4
  store i8* %__last, i8** %__last.addr, align 4
  store i8* %__result, i8** %__result.addr, align 4
  %0 = load i8*, i8** %__first.addr, align 4
  %call = call i8* @_ZNSt3__213__unwrap_iterIPcEET_S2_(i8* %0)
  %1 = load i8*, i8** %__last.addr, align 4
  %call1 = call i8* @_ZNSt3__213__unwrap_iterIPcEET_S2_(i8* %1)
  %2 = load i8*, i8** %__result.addr, align 4
  %call2 = call i8* @_ZNSt3__213__unwrap_iterIPcEET_S2_(i8* %2)
  %call3 = call i8* @_ZNSt3__215__move_backwardIccEENS_9enable_ifIXaasr7is_sameINS_12remove_constIT_E4typeET0_EE5valuesr28is_trivially_copy_assignableIS6_EE5valueEPS6_E4typeEPS3_SA_S7_(i8* %call, i8* %call1, i8* %call2)
  ret i8* %call3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE11__constructIcJcEEEvNS_17integral_constantIbLb1EEERS2_PT_DpOT0_(%"class.std::__2::allocator.32"* nonnull align 1 dereferenceable(1) %__a, i8* %__p, i8* nonnull align 1 dereferenceable(1) %__args) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.32"*, align 4
  %__p.addr = alloca i8*, align 4
  %__args.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.32"* %__a, %"class.std::__2::allocator.32"** %__a.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  store i8* %__args, i8** %__args.addr, align 4
  %1 = load %"class.std::__2::allocator.32"*, %"class.std::__2::allocator.32"** %__a.addr, align 4
  %2 = load i8*, i8** %__p.addr, align 4
  %3 = load i8*, i8** %__args.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) i8* @_ZNSt3__27forwardIcEEOT_RNS_16remove_referenceIS1_E4typeE(i8* nonnull align 1 dereferenceable(1) %3) #8
  call void @_ZNSt3__29allocatorIcE9constructIcJcEEEvPT_DpOT0_(%"class.std::__2::allocator.32"* %1, i8* %2, i8* nonnull align 1 dereferenceable(1) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) i8* @_ZNSt3__27forwardIcEEOT_RNS_16remove_referenceIS1_E4typeE(i8* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca i8*, align 4
  store i8* %__t, i8** %__t.addr, align 4
  %0 = load i8*, i8** %__t.addr, align 4
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIcE9constructIcJcEEEvPT_DpOT0_(%"class.std::__2::allocator.32"* %this, i8* %__p, i8* nonnull align 1 dereferenceable(1) %__args) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.32"*, align 4
  %__p.addr = alloca i8*, align 4
  %__args.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.32"* %this, %"class.std::__2::allocator.32"** %this.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  store i8* %__args, i8** %__args.addr, align 4
  %this1 = load %"class.std::__2::allocator.32"*, %"class.std::__2::allocator.32"** %this.addr, align 4
  %0 = load i8*, i8** %__p.addr, align 4
  %1 = load i8*, i8** %__args.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) i8* @_ZNSt3__27forwardIcEEOT_RNS_16remove_referenceIS1_E4typeE(i8* nonnull align 1 dereferenceable(1) %1) #8
  %2 = load i8, i8* %call, align 1
  store i8 %2, i8* %0, align 1
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__215__move_backwardIccEENS_9enable_ifIXaasr7is_sameINS_12remove_constIT_E4typeET0_EE5valuesr28is_trivially_copy_assignableIS6_EE5valueEPS6_E4typeEPS3_SA_S7_(i8* %__first, i8* %__last, i8* %__result) #0 comdat {
entry:
  %__first.addr = alloca i8*, align 4
  %__last.addr = alloca i8*, align 4
  %__result.addr = alloca i8*, align 4
  %__n = alloca i32, align 4
  store i8* %__first, i8** %__first.addr, align 4
  store i8* %__last, i8** %__last.addr, align 4
  store i8* %__result, i8** %__result.addr, align 4
  %0 = load i8*, i8** %__last.addr, align 4
  %1 = load i8*, i8** %__first.addr, align 4
  %sub.ptr.lhs.cast = ptrtoint i8* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  store i32 %sub.ptr.sub, i32* %__n, align 4
  %2 = load i32, i32* %__n, align 4
  %cmp = icmp ugt i32 %2, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %3 = load i32, i32* %__n, align 4
  %4 = load i8*, i8** %__result.addr, align 4
  %idx.neg = sub i32 0, %3
  %add.ptr = getelementptr inbounds i8, i8* %4, i32 %idx.neg
  store i8* %add.ptr, i8** %__result.addr, align 4
  %5 = load i8*, i8** %__result.addr, align 4
  %6 = load i8*, i8** %__first.addr, align 4
  %7 = load i32, i32* %__n, align 4
  %mul = mul i32 %7, 1
  call void @llvm.memmove.p0i8.p0i8.i32(i8* align 1 %5, i8* align 1 %6, i32 %mul, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %8 = load i8*, i8** %__result.addr, align 4
  ret i8* %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__213__unwrap_iterIPcEET_S2_(i8* %__i) #0 comdat {
entry:
  %__i.addr = alloca i8*, align 4
  store i8* %__i, i8** %__i.addr, align 4
  %0 = load i8*, i8** %__i.addr, align 4
  ret i8* %0
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memmove.p0i8.p0i8.i32(i8* nocapture, i8* nocapture readonly, i32, i1 immarg) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__26__copyIPKhPcEET0_T_S5_S4_(i8* %__first, i8* %__last, i8* %__result) #0 comdat {
entry:
  %__first.addr = alloca i8*, align 4
  %__last.addr = alloca i8*, align 4
  %__result.addr = alloca i8*, align 4
  store i8* %__first, i8** %__first.addr, align 4
  store i8* %__last, i8** %__last.addr, align 4
  store i8* %__result, i8** %__result.addr, align 4
  %0 = load i8*, i8** %__first.addr, align 4
  %1 = load i8*, i8** %__last.addr, align 4
  %2 = load i8*, i8** %__result.addr, align 4
  %call = call i8* @_ZNSt3__216__copy_constexprIPKhPcEET0_T_S5_S4_(i8* %0, i8* %1, i8* %2)
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__213__unwrap_iterIPKhEET_S3_(i8* %__i) #0 comdat {
entry:
  %__i.addr = alloca i8*, align 4
  store i8* %__i, i8** %__i.addr, align 4
  %0 = load i8*, i8** %__i.addr, align 4
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__216__copy_constexprIPKhPcEET0_T_S5_S4_(i8* %__first, i8* %__last, i8* %__result) #0 comdat {
entry:
  %__first.addr = alloca i8*, align 4
  %__last.addr = alloca i8*, align 4
  %__result.addr = alloca i8*, align 4
  store i8* %__first, i8** %__first.addr, align 4
  store i8* %__last, i8** %__last.addr, align 4
  store i8* %__result, i8** %__result.addr, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i8*, i8** %__first.addr, align 4
  %1 = load i8*, i8** %__last.addr, align 4
  %cmp = icmp ne i8* %0, %1
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load i8*, i8** %__first.addr, align 4
  %3 = load i8, i8* %2, align 1
  %4 = load i8*, i8** %__result.addr, align 4
  store i8 %3, i8* %4, align 1
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i8*, i8** %__first.addr, align 4
  %incdec.ptr = getelementptr inbounds i8, i8* %5, i32 1
  store i8* %incdec.ptr, i8** %__first.addr, align 4
  %6 = load i8*, i8** %__result.addr, align 4
  %incdec.ptr1 = getelementptr inbounds i8, i8* %6, i32 1
  store i8* %incdec.ptr1, i8** %__result.addr, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = load i8*, i8** %__result.addr, align 4
  ret i8* %7
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.32"* @_ZNSt3__217__compressed_pairIPcNS_9allocatorIcEEE6secondEv(%"class.std::__2::__compressed_pair.29"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.29"*, align 4
  store %"class.std::__2::__compressed_pair.29"* %this, %"class.std::__2::__compressed_pair.29"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.29"*, %"class.std::__2::__compressed_pair.29"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.29"* %this1 to %"struct.std::__2::__compressed_pair_elem.31"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.32"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIcEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.31"* %0) #8
  ret %"class.std::__2::allocator.32"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.32"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIcEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.31"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.31"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.31"* %this, %"struct.std::__2::__compressed_pair_elem.31"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.31"*, %"struct.std::__2::__compressed_pair_elem.31"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.31"* %this1 to %"class.std::__2::allocator.32"*
  ret %"class.std::__2::allocator.32"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIcNS_9allocatorIcEEE8max_sizeEv(%"class.std::__2::vector.27"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.27"*, align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp3 = alloca i32, align 4
  store %"class.std::__2::vector.27"* %this, %"class.std::__2::vector.27"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.27"*, %"class.std::__2::vector.27"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.27"* %this1 to %"class.std::__2::__vector_base.28"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.32"* @_ZNKSt3__213__vector_baseIcNS_9allocatorIcEEE7__allocEv(%"class.std::__2::__vector_base.28"* %0) #8
  %call2 = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE8max_sizeERKS2_(%"class.std::__2::allocator.32"* nonnull align 1 dereferenceable(1) %call) #8
  store i32 %call2, i32* %ref.tmp, align 4
  %call4 = call i32 @_ZNSt3__214numeric_limitsIlE3maxEv() #8
  store i32 %call4, i32* %ref.tmp3, align 4
  %call5 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %1 = load i32, i32* %call5, align 4
  ret i32 %1
}

; Function Attrs: noreturn
declare void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"*) #5

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIcNS_9allocatorIcEEE8capacityEv(%"class.std::__2::vector.27"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.27"*, align 4
  store %"class.std::__2::vector.27"* %this, %"class.std::__2::vector.27"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.27"*, %"class.std::__2::vector.27"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.27"* %this1 to %"class.std::__2::__vector_base.28"*
  %call = call i32 @_ZNKSt3__213__vector_baseIcNS_9allocatorIcEEE8capacityEv(%"class.std::__2::__vector_base.28"* %0) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::__less", align 1
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__a.addr, align 4
  %1 = load i32*, i32** %__b.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::__less", align 1
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__a.addr, align 4
  %1 = load i32*, i32** %__b.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE8max_sizeERKS2_(%"class.std::__2::allocator.32"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.32"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_max_size", align 1
  store %"class.std::__2::allocator.32"* %__a, %"class.std::__2::allocator.32"** %__a.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_max_size"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.32"*, %"class.std::__2::allocator.32"** %__a.addr, align 4
  %call = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE10__max_sizeENS_17integral_constantIbLb1EEERKS2_(%"class.std::__2::allocator.32"* nonnull align 1 dereferenceable(1) %1) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.32"* @_ZNKSt3__213__vector_baseIcNS_9allocatorIcEEE7__allocEv(%"class.std::__2::__vector_base.28"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.28"*, align 4
  store %"class.std::__2::__vector_base.28"* %this, %"class.std::__2::__vector_base.28"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.28"*, %"class.std::__2::__vector_base.28"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.28", %"class.std::__2::__vector_base.28"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.32"* @_ZNKSt3__217__compressed_pairIPcNS_9allocatorIcEEE6secondEv(%"class.std::__2::__compressed_pair.29"* %__end_cap_) #8
  ret %"class.std::__2::allocator.32"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__214numeric_limitsIlE3maxEv() #0 comdat {
entry:
  %call = call i32 @_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv() #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__comp = alloca %"struct.std::__2::__less", align 1
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__b.addr, align 4
  %1 = load i32*, i32** %__a.addr, align 4
  %call = call zeroext i1 @_ZNKSt3__26__lessImmEclERKmS3_(%"struct.std::__2::__less"* %__comp, i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  br i1 %call, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32*, i32** %__b.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %3 = load i32*, i32** %__a.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi i32* [ %2, %cond.true ], [ %3, %cond.false ]
  ret i32* %cond-lvalue
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__26__lessImmEclERKmS3_(%"struct.std::__2::__less"* %this, i32* nonnull align 4 dereferenceable(4) %__x, i32* nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__less"*, align 4
  %__x.addr = alloca i32*, align 4
  %__y.addr = alloca i32*, align 4
  store %"struct.std::__2::__less"* %this, %"struct.std::__2::__less"** %this.addr, align 4
  store i32* %__x, i32** %__x.addr, align 4
  store i32* %__y, i32** %__y.addr, align 4
  %this1 = load %"struct.std::__2::__less"*, %"struct.std::__2::__less"** %this.addr, align 4
  %0 = load i32*, i32** %__x.addr, align 4
  %1 = load i32, i32* %0, align 4
  %2 = load i32*, i32** %__y.addr, align 4
  %3 = load i32, i32* %2, align 4
  %cmp = icmp ult i32 %1, %3
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE10__max_sizeENS_17integral_constantIbLb1EEERKS2_(%"class.std::__2::allocator.32"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.32"*, align 4
  store %"class.std::__2::allocator.32"* %__a, %"class.std::__2::allocator.32"** %__a.addr, align 4
  %1 = load %"class.std::__2::allocator.32"*, %"class.std::__2::allocator.32"** %__a.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorIcE8max_sizeEv(%"class.std::__2::allocator.32"* %1) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__29allocatorIcE8max_sizeEv(%"class.std::__2::allocator.32"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.32"*, align 4
  store %"class.std::__2::allocator.32"* %this, %"class.std::__2::allocator.32"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.32"*, %"class.std::__2::allocator.32"** %this.addr, align 4
  ret i32 -1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.32"* @_ZNKSt3__217__compressed_pairIPcNS_9allocatorIcEEE6secondEv(%"class.std::__2::__compressed_pair.29"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.29"*, align 4
  store %"class.std::__2::__compressed_pair.29"* %this, %"class.std::__2::__compressed_pair.29"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.29"*, %"class.std::__2::__compressed_pair.29"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.29"* %this1 to %"struct.std::__2::__compressed_pair_elem.31"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.32"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorIcEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.31"* %0) #8
  ret %"class.std::__2::allocator.32"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.32"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorIcEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.31"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.31"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.31"* %this, %"struct.std::__2::__compressed_pair_elem.31"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.31"*, %"struct.std::__2::__compressed_pair_elem.31"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.31"* %this1 to %"class.std::__2::allocator.32"*
  ret %"class.std::__2::allocator.32"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv() #0 comdat {
entry:
  ret i32 2147483647
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseIcNS_9allocatorIcEEE8capacityEv(%"class.std::__2::__vector_base.28"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.28"*, align 4
  store %"class.std::__2::__vector_base.28"* %this, %"class.std::__2::__vector_base.28"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.28"*, %"class.std::__2::__vector_base.28"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__213__vector_baseIcNS_9allocatorIcEEE9__end_capEv(%"class.std::__2::__vector_base.28"* %this1) #8
  %0 = load i8*, i8** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.28", %"class.std::__2::__vector_base.28"* %this1, i32 0, i32 0
  %1 = load i8*, i8** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint i8* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  ret i32 %sub.ptr.sub
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__213__vector_baseIcNS_9allocatorIcEEE9__end_capEv(%"class.std::__2::__vector_base.28"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.28"*, align 4
  store %"class.std::__2::__vector_base.28"* %this, %"class.std::__2::__vector_base.28"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.28"*, %"class.std::__2::__vector_base.28"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.28", %"class.std::__2::__vector_base.28"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__217__compressed_pairIPcNS_9allocatorIcEEE5firstEv(%"class.std::__2::__compressed_pair.29"* %__end_cap_) #8
  ret i8** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__217__compressed_pairIPcNS_9allocatorIcEEE5firstEv(%"class.std::__2::__compressed_pair.29"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.29"*, align 4
  store %"class.std::__2::__compressed_pair.29"* %this, %"class.std::__2::__compressed_pair.29"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.29"*, %"class.std::__2::__compressed_pair.29"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.29"* %this1 to %"struct.std::__2::__compressed_pair_elem.30"*
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__222__compressed_pair_elemIPcLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.30"* %0) #8
  ret i8** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__222__compressed_pair_elemIPcLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.30"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.30"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.30"* %this, %"struct.std::__2::__compressed_pair_elem.30"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.30"*, %"struct.std::__2::__compressed_pair_elem.30"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.30", %"struct.std::__2::__compressed_pair_elem.30"* %this1, i32 0, i32 0
  ret i8** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__comp = alloca %"struct.std::__2::__less", align 1
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__a.addr, align 4
  %1 = load i32*, i32** %__b.addr, align 4
  %call = call zeroext i1 @_ZNKSt3__26__lessImmEclERKmS3_(%"struct.std::__2::__less"* %__comp, i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  br i1 %call, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32*, i32** %__b.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %3 = load i32*, i32** %__a.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi i32* [ %2, %cond.true ], [ %3, %cond.false ]
  ret i32* %cond-lvalue
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.53"* @_ZNSt3__217__compressed_pairIPcRNS_9allocatorIcEEEC2IDnS4_EEOT_OT0_(%"class.std::__2::__compressed_pair.53"* returned %this, i8** nonnull align 4 dereferenceable(4) %__t1, %"class.std::__2::allocator.32"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.53"*, align 4
  %__t1.addr = alloca i8**, align 4
  %__t2.addr = alloca %"class.std::__2::allocator.32"*, align 4
  store %"class.std::__2::__compressed_pair.53"* %this, %"class.std::__2::__compressed_pair.53"** %this.addr, align 4
  store i8** %__t1, i8*** %__t1.addr, align 4
  store %"class.std::__2::allocator.32"* %__t2, %"class.std::__2::allocator.32"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.53"*, %"class.std::__2::__compressed_pair.53"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.53"* %this1 to %"struct.std::__2::__compressed_pair_elem.30"*
  %1 = load i8**, i8*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %1) #8
  %call2 = call %"struct.std::__2::__compressed_pair_elem.30"* @_ZNSt3__222__compressed_pair_elemIPcLi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.30"* %0, i8** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.53"* %this1 to i8*
  %3 = getelementptr inbounds i8, i8* %2, i32 4
  %4 = bitcast i8* %3 to %"struct.std::__2::__compressed_pair_elem.54"*
  %5 = load %"class.std::__2::allocator.32"*, %"class.std::__2::allocator.32"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.32"* @_ZNSt3__27forwardIRNS_9allocatorIcEEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.std::__2::allocator.32"* nonnull align 1 dereferenceable(1) %5) #8
  %call4 = call %"struct.std::__2::__compressed_pair_elem.54"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIcEELi1ELb0EEC2IS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.54"* %4, %"class.std::__2::allocator.32"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::__compressed_pair.53"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE8allocateERS2_m(%"class.std::__2::allocator.32"* nonnull align 1 dereferenceable(1) %__a, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.32"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.32"* %__a, %"class.std::__2::allocator.32"** %__a.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.32"*, %"class.std::__2::allocator.32"** %__a.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call i8* @_ZNSt3__29allocatorIcE8allocateEmPKv(%"class.std::__2::allocator.32"* %0, i32 %1, i8* null)
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.32"* @_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE7__allocEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.32"* @_ZNSt3__217__compressed_pairIPcRNS_9allocatorIcEEE6secondEv(%"class.std::__2::__compressed_pair.53"* %__end_cap_) #8
  ret %"class.std::__2::allocator.32"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__217__compressed_pairIPcRNS_9allocatorIcEEE5firstEv(%"class.std::__2::__compressed_pair.53"* %__end_cap_) #8
  ret i8** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca i8**, align 4
  store i8** %__t, i8*** %__t.addr, align 4
  %0 = load i8**, i8*** %__t.addr, align 4
  ret i8** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.30"* @_ZNSt3__222__compressed_pair_elemIPcLi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.30"* returned %this, i8** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.30"*, align 4
  %__u.addr = alloca i8**, align 4
  store %"struct.std::__2::__compressed_pair_elem.30"* %this, %"struct.std::__2::__compressed_pair_elem.30"** %this.addr, align 4
  store i8** %__u, i8*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.30"*, %"struct.std::__2::__compressed_pair_elem.30"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.30", %"struct.std::__2::__compressed_pair_elem.30"* %this1, i32 0, i32 0
  %0 = load i8**, i8*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %0) #8
  store i8* null, i8** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.30"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.32"* @_ZNSt3__27forwardIRNS_9allocatorIcEEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.std::__2::allocator.32"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::allocator.32"*, align 4
  store %"class.std::__2::allocator.32"* %__t, %"class.std::__2::allocator.32"** %__t.addr, align 4
  %0 = load %"class.std::__2::allocator.32"*, %"class.std::__2::allocator.32"** %__t.addr, align 4
  ret %"class.std::__2::allocator.32"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.54"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIcEELi1ELb0EEC2IS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.54"* returned %this, %"class.std::__2::allocator.32"* nonnull align 1 dereferenceable(1) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.54"*, align 4
  %__u.addr = alloca %"class.std::__2::allocator.32"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.54"* %this, %"struct.std::__2::__compressed_pair_elem.54"** %this.addr, align 4
  store %"class.std::__2::allocator.32"* %__u, %"class.std::__2::allocator.32"** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.54"*, %"struct.std::__2::__compressed_pair_elem.54"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.54", %"struct.std::__2::__compressed_pair_elem.54"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::allocator.32"*, %"class.std::__2::allocator.32"** %__u.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.32"* @_ZNSt3__27forwardIRNS_9allocatorIcEEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.std::__2::allocator.32"* nonnull align 1 dereferenceable(1) %0) #8
  store %"class.std::__2::allocator.32"* %call, %"class.std::__2::allocator.32"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.54"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__29allocatorIcE8allocateEmPKv(%"class.std::__2::allocator.32"* %this, i32 %__n, i8* %0) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.32"*, align 4
  %__n.addr = alloca i32, align 4
  %.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.32"* %this, %"class.std::__2::allocator.32"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::allocator.32"*, %"class.std::__2::allocator.32"** %this.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorIcE8max_sizeEv(%"class.std::__2::allocator.32"* %this1) #8
  %cmp = icmp ugt i32 %1, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__220__throw_length_errorEPKc(i8* getelementptr inbounds ([68 x i8], [68 x i8]* @.str, i32 0, i32 0)) #11
  unreachable

if.end:                                           ; preds = %entry
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 1
  %call2 = call i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %mul, i32 1)
  ret i8* %call2
}

; Function Attrs: noinline noreturn nounwind optnone
define linkonce_odr hidden void @_ZNSt3__220__throw_length_errorEPKc(i8* %__msg) #6 comdat {
entry:
  %__msg.addr = alloca i8*, align 4
  store i8* %__msg, i8** %__msg.addr, align 4
  call void @abort() #11
  unreachable
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %__size, i32 %__align) #0 comdat {
entry:
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i32, i32* %__size.addr, align 4
  %call = call noalias nonnull i8* @_Znwm(i32 %0) #9
  ret i8* %call
}

; Function Attrs: noreturn
declare void @abort() #5

; Function Attrs: nobuiltin allocsize(0)
declare nonnull i8* @_Znwm(i32) #2

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.32"* @_ZNSt3__217__compressed_pairIPcRNS_9allocatorIcEEE6secondEv(%"class.std::__2::__compressed_pair.53"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.53"*, align 4
  store %"class.std::__2::__compressed_pair.53"* %this, %"class.std::__2::__compressed_pair.53"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.53"*, %"class.std::__2::__compressed_pair.53"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.53"* %this1 to i8*
  %add.ptr = getelementptr inbounds i8, i8* %0, i32 4
  %1 = bitcast i8* %add.ptr to %"struct.std::__2::__compressed_pair_elem.54"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.32"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIcEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.54"* %1) #8
  ret %"class.std::__2::allocator.32"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.32"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIcEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.54"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.54"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.54"* %this, %"struct.std::__2::__compressed_pair_elem.54"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.54"*, %"struct.std::__2::__compressed_pair_elem.54"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.54", %"struct.std::__2::__compressed_pair_elem.54"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::allocator.32"*, %"class.std::__2::allocator.32"** %__value_, align 4
  ret %"class.std::__2::allocator.32"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNSt3__217__compressed_pairIPcRNS_9allocatorIcEEE5firstEv(%"class.std::__2::__compressed_pair.53"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.53"*, align 4
  store %"class.std::__2::__compressed_pair.53"* %this, %"class.std::__2::__compressed_pair.53"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.53"*, %"class.std::__2::__compressed_pair.53"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.53"* %this1 to %"struct.std::__2::__compressed_pair_elem.30"*
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__222__compressed_pair_elemIPcLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.30"* %0) #8
  ret i8** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE21_ConstructTransactionC2EPPcm(%"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"* returned %this, i8** %__p, i32 %__n) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"*, align 4
  %__p.addr = alloca i8**, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"* %this, %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"** %this.addr, align 4
  store i8** %__p, i8*** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"*, %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load i8**, i8*** %__p.addr, align 4
  %1 = load i8*, i8** %0, align 4
  store i8* %1, i8** %__pos_, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"* %this1, i32 0, i32 1
  %2 = load i8**, i8*** %__p.addr, align 4
  %3 = load i8*, i8** %2, align 4
  %4 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds i8, i8* %3, i32 %4
  store i8* %add.ptr, i8** %__end_, align 4
  %__dest_ = getelementptr inbounds %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"* %this1, i32 0, i32 2
  %5 = load i8**, i8*** %__p.addr, align 4
  store i8** %5, i8*** %__dest_, align 4
  ret %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE21_ConstructTransactionD2Ev(%"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"*, align 4
  store %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"* %this, %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"*, %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load i8*, i8** %__pos_, align 4
  %__dest_ = getelementptr inbounds %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"* %this1, i32 0, i32 2
  %1 = load i8**, i8*** %__dest_, align 4
  store i8* %0, i8** %1, align 4
  ret %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIcNS_9allocatorIcEEE17__annotate_deleteEv(%"class.std::__2::vector.27"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.27"*, align 4
  store %"class.std::__2::vector.27"* %this, %"class.std::__2::vector.27"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.27"*, %"class.std::__2::vector.27"** %this.addr, align 4
  %call = call i8* @_ZNKSt3__26vectorIcNS_9allocatorIcEEE4dataEv(%"class.std::__2::vector.27"* %this1) #8
  %call2 = call i8* @_ZNKSt3__26vectorIcNS_9allocatorIcEEE4dataEv(%"class.std::__2::vector.27"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorIcNS_9allocatorIcEEE8capacityEv(%"class.std::__2::vector.27"* %this1) #8
  %add.ptr = getelementptr inbounds i8, i8* %call2, i32 %call3
  %call4 = call i8* @_ZNKSt3__26vectorIcNS_9allocatorIcEEE4dataEv(%"class.std::__2::vector.27"* %this1) #8
  %call5 = call i32 @_ZNKSt3__26vectorIcNS_9allocatorIcEEE4sizeEv(%"class.std::__2::vector.27"* %this1) #8
  %add.ptr6 = getelementptr inbounds i8, i8* %call4, i32 %call5
  %call7 = call i8* @_ZNKSt3__26vectorIcNS_9allocatorIcEEE4dataEv(%"class.std::__2::vector.27"* %this1) #8
  %call8 = call i32 @_ZNKSt3__26vectorIcNS_9allocatorIcEEE8capacityEv(%"class.std::__2::vector.27"* %this1) #8
  %add.ptr9 = getelementptr inbounds i8, i8* %call7, i32 %call8
  call void @_ZNKSt3__26vectorIcNS_9allocatorIcEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector.27"* %this1, i8* %call, i8* %add.ptr, i8* %add.ptr6, i8* %add.ptr9) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE46__construct_backward_with_exception_guaranteesIcEENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS2_PT_S8_EE5valuesr31is_trivially_move_constructibleIS8_EE5valueEvE4typeERS2_S9_S9_RS9_(%"class.std::__2::allocator.32"* nonnull align 1 dereferenceable(1) %0, i8* %__begin1, i8* %__end1, i8** nonnull align 4 dereferenceable(4) %__end2) #0 comdat {
entry:
  %.addr = alloca %"class.std::__2::allocator.32"*, align 4
  %__begin1.addr = alloca i8*, align 4
  %__end1.addr = alloca i8*, align 4
  %__end2.addr = alloca i8**, align 4
  %_Np = alloca i32, align 4
  store %"class.std::__2::allocator.32"* %0, %"class.std::__2::allocator.32"** %.addr, align 4
  store i8* %__begin1, i8** %__begin1.addr, align 4
  store i8* %__end1, i8** %__end1.addr, align 4
  store i8** %__end2, i8*** %__end2.addr, align 4
  %1 = load i8*, i8** %__end1.addr, align 4
  %2 = load i8*, i8** %__begin1.addr, align 4
  %sub.ptr.lhs.cast = ptrtoint i8* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %2 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  store i32 %sub.ptr.sub, i32* %_Np, align 4
  %3 = load i32, i32* %_Np, align 4
  %4 = load i8**, i8*** %__end2.addr, align 4
  %5 = load i8*, i8** %4, align 4
  %idx.neg = sub i32 0, %3
  %add.ptr = getelementptr inbounds i8, i8* %5, i32 %idx.neg
  store i8* %add.ptr, i8** %4, align 4
  %6 = load i32, i32* %_Np, align 4
  %cmp = icmp sgt i32 %6, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %7 = load i8**, i8*** %__end2.addr, align 4
  %8 = load i8*, i8** %7, align 4
  %9 = load i8*, i8** %__begin1.addr, align 4
  %10 = load i32, i32* %_Np, align 4
  %mul = mul i32 %10, 1
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %8, i8* align 1 %9, i32 %mul, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE45__construct_forward_with_exception_guaranteesIcEENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS2_PT_S8_EE5valuesr31is_trivially_move_constructibleIS8_EE5valueEvE4typeERS2_S9_S9_RS9_(%"class.std::__2::allocator.32"* nonnull align 1 dereferenceable(1) %0, i8* %__begin1, i8* %__end1, i8** nonnull align 4 dereferenceable(4) %__begin2) #0 comdat {
entry:
  %.addr = alloca %"class.std::__2::allocator.32"*, align 4
  %__begin1.addr = alloca i8*, align 4
  %__end1.addr = alloca i8*, align 4
  %__begin2.addr = alloca i8**, align 4
  %_Np = alloca i32, align 4
  store %"class.std::__2::allocator.32"* %0, %"class.std::__2::allocator.32"** %.addr, align 4
  store i8* %__begin1, i8** %__begin1.addr, align 4
  store i8* %__end1, i8** %__end1.addr, align 4
  store i8** %__begin2, i8*** %__begin2.addr, align 4
  %1 = load i8*, i8** %__end1.addr, align 4
  %2 = load i8*, i8** %__begin1.addr, align 4
  %sub.ptr.lhs.cast = ptrtoint i8* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %2 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  store i32 %sub.ptr.sub, i32* %_Np, align 4
  %3 = load i32, i32* %_Np, align 4
  %cmp = icmp sgt i32 %3, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = load i8**, i8*** %__begin2.addr, align 4
  %5 = load i8*, i8** %4, align 4
  %6 = load i8*, i8** %__begin1.addr, align 4
  %7 = load i32, i32* %_Np, align 4
  %mul = mul i32 %7, 1
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %5, i8* align 1 %6, i32 %mul, i1 false)
  %8 = load i32, i32* %_Np, align 4
  %9 = load i8**, i8*** %__begin2.addr, align 4
  %10 = load i8*, i8** %9, align 4
  %add.ptr = getelementptr inbounds i8, i8* %10, i32 %8
  store i8* %add.ptr, i8** %9, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__24swapIPcEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_(i8** nonnull align 4 dereferenceable(4) %__x, i8** nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %__x.addr = alloca i8**, align 4
  %__y.addr = alloca i8**, align 4
  %__t = alloca i8*, align 4
  store i8** %__x, i8*** %__x.addr, align 4
  store i8** %__y, i8*** %__y.addr, align 4
  %0 = load i8**, i8*** %__x.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__24moveIRPcEEONS_16remove_referenceIT_E4typeEOS4_(i8** nonnull align 4 dereferenceable(4) %0) #8
  %1 = load i8*, i8** %call, align 4
  store i8* %1, i8** %__t, align 4
  %2 = load i8**, i8*** %__y.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__24moveIRPcEEONS_16remove_referenceIT_E4typeEOS4_(i8** nonnull align 4 dereferenceable(4) %2) #8
  %3 = load i8*, i8** %call1, align 4
  %4 = load i8**, i8*** %__x.addr, align 4
  store i8* %3, i8** %4, align 4
  %call2 = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__24moveIRPcEEONS_16remove_referenceIT_E4typeEOS4_(i8** nonnull align 4 dereferenceable(4) %__t) #8
  %5 = load i8*, i8** %call2, align 4
  %6 = load i8**, i8*** %__y.addr, align 4
  store i8* %5, i8** %6, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIcNS_9allocatorIcEEE14__annotate_newEm(%"class.std::__2::vector.27"* %this, i32 %__current_size) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.27"*, align 4
  %__current_size.addr = alloca i32, align 4
  store %"class.std::__2::vector.27"* %this, %"class.std::__2::vector.27"** %this.addr, align 4
  store i32 %__current_size, i32* %__current_size.addr, align 4
  %this1 = load %"class.std::__2::vector.27"*, %"class.std::__2::vector.27"** %this.addr, align 4
  %call = call i8* @_ZNKSt3__26vectorIcNS_9allocatorIcEEE4dataEv(%"class.std::__2::vector.27"* %this1) #8
  %call2 = call i8* @_ZNKSt3__26vectorIcNS_9allocatorIcEEE4dataEv(%"class.std::__2::vector.27"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorIcNS_9allocatorIcEEE8capacityEv(%"class.std::__2::vector.27"* %this1) #8
  %add.ptr = getelementptr inbounds i8, i8* %call2, i32 %call3
  %call4 = call i8* @_ZNKSt3__26vectorIcNS_9allocatorIcEEE4dataEv(%"class.std::__2::vector.27"* %this1) #8
  %call5 = call i32 @_ZNKSt3__26vectorIcNS_9allocatorIcEEE8capacityEv(%"class.std::__2::vector.27"* %this1) #8
  %add.ptr6 = getelementptr inbounds i8, i8* %call4, i32 %call5
  %call7 = call i8* @_ZNKSt3__26vectorIcNS_9allocatorIcEEE4dataEv(%"class.std::__2::vector.27"* %this1) #8
  %0 = load i32, i32* %__current_size.addr, align 4
  %add.ptr8 = getelementptr inbounds i8, i8* %call7, i32 %0
  call void @_ZNKSt3__26vectorIcNS_9allocatorIcEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector.27"* %this1, i8* %call, i8* %add.ptr, i8* %add.ptr6, i8* %add.ptr8) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIcNS_9allocatorIcEEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector.27"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.27"*, align 4
  store %"class.std::__2::vector.27"* %this, %"class.std::__2::vector.27"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.27"*, %"class.std::__2::vector.27"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIcNS_9allocatorIcEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector.27"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.27"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector.27"* %this, %"class.std::__2::vector.27"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector.27"*, %"class.std::__2::vector.27"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__26vectorIcNS_9allocatorIcEEE4dataEv(%"class.std::__2::vector.27"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.27"*, align 4
  store %"class.std::__2::vector.27"* %this, %"class.std::__2::vector.27"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.27"*, %"class.std::__2::vector.27"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.27"* %this1 to %"class.std::__2::__vector_base.28"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.28", %"class.std::__2::__vector_base.28"* %0, i32 0, i32 0
  %1 = load i8*, i8** %__begin_, align 4
  %call = call i8* @_ZNSt3__212__to_addressIcEEPT_S2_(i8* %1) #8
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNSt3__24moveIRPcEEONS_16remove_referenceIT_E4typeEOS4_(i8** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca i8**, align 4
  store i8** %__t, i8*** %__t.addr, align 4
  %0 = load i8**, i8*** %__t.addr, align 4
  ret i8** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE5clearEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 1
  %0 = load i8*, i8** %__begin_, align 4
  call void @_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE17__destruct_at_endEPc(%"struct.std::__2::__split_buffer"* %this1, i8* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE10deallocateERS2_Pcm(%"class.std::__2::allocator.32"* nonnull align 1 dereferenceable(1) %__a, i8* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.32"*, align 4
  %__p.addr = alloca i8*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.32"* %__a, %"class.std::__2::allocator.32"** %__a.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.32"*, %"class.std::__2::allocator.32"** %__a.addr, align 4
  %1 = load i8*, i8** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorIcE10deallocateEPcm(%"class.std::__2::allocator.32"* %0, i8* %1, i32 %2) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__214__split_bufferIcRNS_9allocatorIcEEE8capacityEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__214__split_bufferIcRNS_9allocatorIcEEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this1) #8
  %0 = load i8*, i8** %call, align 4
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %1 = load i8*, i8** %__first_, align 4
  %sub.ptr.lhs.cast = ptrtoint i8* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  ret i32 %sub.ptr.sub
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE17__destruct_at_endEPc(%"struct.std::__2::__split_buffer"* %this, i8* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__new_last.addr = alloca i8*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant.56", align 1
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store i8* %__new_last, i8** %__new_last.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %0 = load i8*, i8** %__new_last.addr, align 4
  call void @_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE17__destruct_at_endEPcNS_17integral_constantIbLb0EEE(%"struct.std::__2::__split_buffer"* %this1, i8* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE17__destruct_at_endEPcNS_17integral_constantIbLb0EEE(%"struct.std::__2::__split_buffer"* %this, i8* %__new_last) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant.56", align 1
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__new_last.addr = alloca i8*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store i8* %__new_last, i8** %__new_last.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load i8*, i8** %__new_last.addr, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 2
  %2 = load i8*, i8** %__end_, align 4
  %cmp = icmp ne i8* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.32"* @_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE7__allocEv(%"struct.std::__2::__split_buffer"* %this1) #8
  %__end_2 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 2
  %3 = load i8*, i8** %__end_2, align 4
  %incdec.ptr = getelementptr inbounds i8, i8* %3, i32 -1
  store i8* %incdec.ptr, i8** %__end_2, align 4
  %call3 = call i8* @_ZNSt3__212__to_addressIcEEPT_S2_(i8* %incdec.ptr) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE7destroyIcEEvRS2_PT_(%"class.std::__2::allocator.32"* nonnull align 1 dereferenceable(1) %call, i8* %call3)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE7destroyIcEEvRS2_PT_(%"class.std::__2::allocator.32"* nonnull align 1 dereferenceable(1) %__a, i8* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.32"*, align 4
  %__p.addr = alloca i8*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy", align 1
  store %"class.std::__2::allocator.32"* %__a, %"class.std::__2::allocator.32"** %__a.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.32"*, %"class.std::__2::allocator.32"** %__a.addr, align 4
  %2 = load i8*, i8** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE9__destroyIcEEvNS_17integral_constantIbLb1EEERS2_PT_(%"class.std::__2::allocator.32"* nonnull align 1 dereferenceable(1) %1, i8* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE9__destroyIcEEvNS_17integral_constantIbLb1EEERS2_PT_(%"class.std::__2::allocator.32"* nonnull align 1 dereferenceable(1) %__a, i8* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.32"*, align 4
  %__p.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.32"* %__a, %"class.std::__2::allocator.32"** %__a.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.32"*, %"class.std::__2::allocator.32"** %__a.addr, align 4
  %2 = load i8*, i8** %__p.addr, align 4
  call void @_ZNSt3__29allocatorIcE7destroyEPc(%"class.std::__2::allocator.32"* %1, i8* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIcE7destroyEPc(%"class.std::__2::allocator.32"* %this, i8* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.32"*, align 4
  %__p.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.32"* %this, %"class.std::__2::allocator.32"** %this.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.32"*, %"class.std::__2::allocator.32"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIcE10deallocateEPcm(%"class.std::__2::allocator.32"* %this, i8* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.32"*, align 4
  %__p.addr = alloca i8*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.32"* %this, %"class.std::__2::allocator.32"** %this.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.32"*, %"class.std::__2::allocator.32"** %this.addr, align 4
  %0 = load i8*, i8** %__p.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %1, 1
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %0, i32 %mul, i32 1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %__ptr, i32 %__size, i32 %__align) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  %1 = load i32, i32* %__size.addr, align 4
  %2 = load i32, i32* %__align.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm(i8* %0, i32 %1, i32 %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm(i8* %__ptr, i32 %__size, i32 %__align) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  %1 = load i32, i32* %__size.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm(i8* %0, i32 %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm(i8* %__ptr, i32 %__size) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller9__do_callEPv(i8* %0)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller9__do_callEPv(i8* %__ptr) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  call void @_ZdlPv(i8* %0) #10
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #7

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__214__split_bufferIcRNS_9allocatorIcEEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__217__compressed_pairIPcRNS_9allocatorIcEEE5firstEv(%"class.std::__2::__compressed_pair.53"* %__end_cap_) #8
  ret i8** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__217__compressed_pairIPcRNS_9allocatorIcEEE5firstEv(%"class.std::__2::__compressed_pair.53"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.53"*, align 4
  store %"class.std::__2::__compressed_pair.53"* %this, %"class.std::__2::__compressed_pair.53"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.53"*, %"class.std::__2::__compressed_pair.53"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.53"* %this1 to %"struct.std::__2::__compressed_pair_elem.30"*
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__222__compressed_pair_elemIPcLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.30"* %0) #8
  ret i8** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__wrap_iter.52"* @_ZNSt3__211__wrap_iterIPcEC2ES1_(%"class.std::__2::__wrap_iter.52"* returned %this, i8* %__x) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__wrap_iter.52"*, align 4
  %__x.addr = alloca i8*, align 4
  store %"class.std::__2::__wrap_iter.52"* %this, %"class.std::__2::__wrap_iter.52"** %this.addr, align 4
  store i8* %__x, i8** %__x.addr, align 4
  %this1 = load %"class.std::__2::__wrap_iter.52"*, %"class.std::__2::__wrap_iter.52"** %this.addr, align 4
  %__i = getelementptr inbounds %"class.std::__2::__wrap_iter.52", %"class.std::__2::__wrap_iter.52"* %this1, i32 0, i32 0
  %0 = load i8*, i8** %__x.addr, align 4
  store i8* %0, i8** %__i, align 4
  ret %"class.std::__2::__wrap_iter.52"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i64 @_ZNK5draco17GeometryAttribute10GetBytePosENS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEE(%"class.draco::GeometryAttribute"* %this, i32 %att_index.coerce) #0 comdat {
entry:
  %att_index = alloca %"class.draco::IndexType", align 4
  %this.addr = alloca %"class.draco::GeometryAttribute"*, align 4
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %att_index, i32 0, i32 0
  store i32 %att_index.coerce, i32* %coerce.dive, align 4
  store %"class.draco::GeometryAttribute"* %this, %"class.draco::GeometryAttribute"** %this.addr, align 4
  %this1 = load %"class.draco::GeometryAttribute"*, %"class.draco::GeometryAttribute"** %this.addr, align 4
  %byte_offset_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 6
  %0 = load i64, i64* %byte_offset_, align 8
  %byte_stride_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 5
  %1 = load i64, i64* %byte_stride_, align 8
  %call = call i32 @_ZNK5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EE5valueEv(%"class.draco::IndexType"* %att_index)
  %conv = zext i32 %call to i64
  %mul = mul nsw i64 %1, %conv
  %add = add nsw i64 %0, %mul
  ret i64 %add
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN5draco10DataBuffer4dataEv(%"class.draco::DataBuffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::DataBuffer"*, align 4
  store %"class.draco::DataBuffer"* %this, %"class.draco::DataBuffer"** %this.addr, align 4
  %this1 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %this.addr, align 4
  %data_ = getelementptr inbounds %"class.draco::DataBuffer", %"class.draco::DataBuffer"* %this1, i32 0, i32 0
  %call = call nonnull align 1 dereferenceable(1) i8* @_ZNSt3__26vectorIhNS_9allocatorIhEEEixEm(%"class.std::__2::vector.1"* %data_, i32 0) #8
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) i8* @_ZNSt3__26vectorIhNS_9allocatorIhEEEixEm(%"class.std::__2::vector.1"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.1"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector.1"* %this, %"class.std::__2::vector.1"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base.2"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %0, i32 0, i32 0
  %1 = load i8*, i8** %__begin_, align 4
  %2 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds i8, i8* %1, i32 %2
  ret i8* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco9IndexTypeIjNS_20PointIndex_tag_type_EE5valueEv(%"class.draco::IndexType.44"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.44"*, align 4
  store %"class.draco::IndexType.44"* %this, %"class.draco::IndexType.44"** %this.addr, align 4
  %this1 = load %"class.draco::IndexType.44"*, %"class.draco::IndexType.44"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.44", %"class.draco::IndexType.44"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value_, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEEEixERKS3_(%"class.draco::IndexTypeVector"* %this, %"class.draco::IndexType.44"* nonnull align 4 dereferenceable(4) %index) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector"*, align 4
  %index.addr = alloca %"class.draco::IndexType.44"*, align 4
  store %"class.draco::IndexTypeVector"* %this, %"class.draco::IndexTypeVector"** %this.addr, align 4
  store %"class.draco::IndexType.44"* %index, %"class.draco::IndexType.44"** %index.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector"*, %"class.draco::IndexTypeVector"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector", %"class.draco::IndexTypeVector"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexType.44"*, %"class.draco::IndexType.44"** %index.addr, align 4
  %call = call i32 @_ZNK5draco9IndexTypeIjNS_20PointIndex_tag_type_EE5valueEv(%"class.draco::IndexType.44"* %0)
  %call2 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEEixEm(%"class.std::__2::vector.11"* %vector_, i32 %call) #8
  ret %"class.draco::IndexType"* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEEixEm(%"class.std::__2::vector.11"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.11"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector.11"* %this, %"class.std::__2::vector.11"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.11"*, %"class.std::__2::vector.11"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.11"* %this1 to %"class.std::__2::__vector_base.12"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.12", %"class.std::__2::__vector_base.12"* %0, i32 0, i32 0
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  %2 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %1, i32 %2
  ret %"class.draco::IndexType"* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z5floorf(float %__lcpp_x) #0 comdat {
entry:
  %__lcpp_x.addr = alloca float, align 4
  store float %__lcpp_x, float* %__lcpp_x.addr, align 4
  %0 = load float, float* %__lcpp_x.addr, align 4
  %1 = call float @llvm.floor.f32(float %0)
  ret float %1
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.floor.f32(float) #1

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::AttributeTransform"* @_ZN5draco18AttributeTransformD2Ev(%"class.draco::AttributeTransform"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::AttributeTransform"*, align 4
  store %"class.draco::AttributeTransform"* %this, %"class.draco::AttributeTransform"** %this.addr, align 4
  %this1 = load %"class.draco::AttributeTransform"*, %"class.draco::AttributeTransform"** %this.addr, align 4
  ret %"class.draco::AttributeTransform"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIfNS_9allocatorIfEEE17__annotate_deleteEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %call = call float* @_ZNKSt3__26vectorIfNS_9allocatorIfEEE4dataEv(%"class.std::__2::vector"* %this1) #8
  %0 = bitcast float* %call to i8*
  %call2 = call float* @_ZNKSt3__26vectorIfNS_9allocatorIfEEE4dataEv(%"class.std::__2::vector"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorIfNS_9allocatorIfEEE8capacityEv(%"class.std::__2::vector"* %this1) #8
  %add.ptr = getelementptr inbounds float, float* %call2, i32 %call3
  %1 = bitcast float* %add.ptr to i8*
  %call4 = call float* @_ZNKSt3__26vectorIfNS_9allocatorIfEEE4dataEv(%"class.std::__2::vector"* %this1) #8
  %call5 = call i32 @_ZNKSt3__26vectorIfNS_9allocatorIfEEE4sizeEv(%"class.std::__2::vector"* %this1) #8
  %add.ptr6 = getelementptr inbounds float, float* %call4, i32 %call5
  %2 = bitcast float* %add.ptr6 to i8*
  %call7 = call float* @_ZNKSt3__26vectorIfNS_9allocatorIfEEE4dataEv(%"class.std::__2::vector"* %this1) #8
  %call8 = call i32 @_ZNKSt3__26vectorIfNS_9allocatorIfEEE8capacityEv(%"class.std::__2::vector"* %this1) #8
  %add.ptr9 = getelementptr inbounds float, float* %call7, i32 %call8
  %3 = bitcast float* %add.ptr9 to i8*
  call void @_ZNKSt3__26vectorIfNS_9allocatorIfEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector"* %this1, i8* %0, i8* %1, i8* %2, i8* %3) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base"* @_ZNSt3__213__vector_baseIfNS_9allocatorIfEEED2Ev(%"class.std::__2::__vector_base"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__vector_base"*, align 4
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  store %"class.std::__2::__vector_base"* %this1, %"class.std::__2::__vector_base"** %retval, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  %0 = load float*, float** %__begin_, align 4
  %cmp = icmp ne float* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__213__vector_baseIfNS_9allocatorIfEEE5clearEv(%"class.std::__2::__vector_base"* %this1) #8
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__213__vector_baseIfNS_9allocatorIfEEE7__allocEv(%"class.std::__2::__vector_base"* %this1) #8
  %__begin_2 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  %1 = load float*, float** %__begin_2, align 4
  %call3 = call i32 @_ZNKSt3__213__vector_baseIfNS_9allocatorIfEEE8capacityEv(%"class.std::__2::__vector_base"* %this1) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIfEEE10deallocateERS2_Pfm(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call, float* %1, i32 %call3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %retval, align 4
  ret %"class.std::__2::__vector_base"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIfNS_9allocatorIfEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIfNS_9allocatorIfEEE8capacityEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call i32 @_ZNKSt3__213__vector_baseIfNS_9allocatorIfEEE8capacityEv(%"class.std::__2::__vector_base"* %0) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseIfNS_9allocatorIfEEE8capacityEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) float** @_ZNKSt3__213__vector_baseIfNS_9allocatorIfEEE9__end_capEv(%"class.std::__2::__vector_base"* %this1) #8
  %0 = load float*, float** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  %1 = load float*, float** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint float* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint float* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float** @_ZNKSt3__213__vector_baseIfNS_9allocatorIfEEE9__end_capEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) float** @_ZNKSt3__217__compressed_pairIPfNS_9allocatorIfEEE5firstEv(%"class.std::__2::__compressed_pair"* %__end_cap_) #8
  ret float** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float** @_ZNKSt3__217__compressed_pairIPfNS_9allocatorIfEEE5firstEv(%"class.std::__2::__compressed_pair"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %call = call nonnull align 4 dereferenceable(4) float** @_ZNKSt3__222__compressed_pair_elemIPfLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %0) #8
  ret float** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float** @_ZNKSt3__222__compressed_pair_elemIPfLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem"*, align 4
  store %"struct.std::__2::__compressed_pair_elem"* %this, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem"*, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem"* %this1, i32 0, i32 0
  ret float** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIfNS_9allocatorIfEEE5clearEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  %0 = load float*, float** %__begin_, align 4
  call void @_ZNSt3__213__vector_baseIfNS_9allocatorIfEEE17__destruct_at_endEPf(%"class.std::__2::__vector_base"* %this1, float* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIfEEE10deallocateERS2_Pfm(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, float* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca float*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store float* %__p, float** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %1 = load float*, float** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorIfE10deallocateEPfm(%"class.std::__2::allocator"* %0, float* %1, i32 %2) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__213__vector_baseIfNS_9allocatorIfEEE7__allocEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__217__compressed_pairIPfNS_9allocatorIfEEE6secondEv(%"class.std::__2::__compressed_pair"* %__end_cap_) #8
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIfNS_9allocatorIfEEE17__destruct_at_endEPf(%"class.std::__2::__vector_base"* %this, float* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  %__new_last.addr = alloca float*, align 4
  %__soon_to_be_end = alloca float*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  store float* %__new_last, float** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 1
  %0 = load float*, float** %__end_, align 4
  store float* %0, float** %__soon_to_be_end, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load float*, float** %__new_last.addr, align 4
  %2 = load float*, float** %__soon_to_be_end, align 4
  %cmp = icmp ne float* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__213__vector_baseIfNS_9allocatorIfEEE7__allocEv(%"class.std::__2::__vector_base"* %this1) #8
  %3 = load float*, float** %__soon_to_be_end, align 4
  %incdec.ptr = getelementptr inbounds float, float* %3, i32 -1
  store float* %incdec.ptr, float** %__soon_to_be_end, align 4
  %call2 = call float* @_ZNSt3__212__to_addressIfEEPT_S2_(float* %incdec.ptr) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIfEEE7destroyIfEEvRS2_PT_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call, float* %call2)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %4 = load float*, float** %__new_last.addr, align 4
  %__end_3 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 1
  store float* %4, float** %__end_3, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIfEEE7destroyIfEEvRS2_PT_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, float* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca float*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy.57", align 1
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store float* %__p, float** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy.57"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %2 = load float*, float** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIfEEE9__destroyIfEEvNS_17integral_constantIbLb1EEERS2_PT_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %1, float* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZNSt3__212__to_addressIfEEPT_S2_(float* %__p) #0 comdat {
entry:
  %__p.addr = alloca float*, align 4
  store float* %__p, float** %__p.addr, align 4
  %0 = load float*, float** %__p.addr, align 4
  ret float* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIfEEE9__destroyIfEEvNS_17integral_constantIbLb1EEERS2_PT_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, float* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca float*, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store float* %__p, float** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %2 = load float*, float** %__p.addr, align 4
  call void @_ZNSt3__29allocatorIfE7destroyEPf(%"class.std::__2::allocator"* %1, float* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIfE7destroyEPf(%"class.std::__2::allocator"* %this, float* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca float*, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  store float* %__p, float** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIfE10deallocateEPfm(%"class.std::__2::allocator"* %this, float* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca float*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  store float* %__p, float** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  %0 = load float*, float** %__p.addr, align 4
  %1 = bitcast float* %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__217__compressed_pairIPfNS_9allocatorIfEEE6secondEv(%"class.std::__2::__compressed_pair"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem.0"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIfEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.0"* %0) #8
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIfEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.0"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.0"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.0"* %this, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.0"*, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.0"* %this1 to %"class.std::__2::allocator"*
  ret %"class.std::__2::allocator"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIfNS_9allocatorIfEEE8__appendEm(%"class.std::__2::vector"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__n.addr = alloca i32, align 4
  %__a = alloca %"class.std::__2::allocator"*, align 4
  %__v = alloca %"struct.std::__2::__split_buffer.58", align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call nonnull align 4 dereferenceable(4) float** @_ZNSt3__213__vector_baseIfNS_9allocatorIfEEE9__end_capEv(%"class.std::__2::__vector_base"* %0) #8
  %1 = load float*, float** %call, align 4
  %2 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %2, i32 0, i32 1
  %3 = load float*, float** %__end_, align 4
  %sub.ptr.lhs.cast = ptrtoint float* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint float* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  %4 = load i32, i32* %__n.addr, align 4
  %cmp = icmp uge i32 %sub.ptr.div, %4
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %5 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__26vectorIfNS_9allocatorIfEEE18__construct_at_endEm(%"class.std::__2::vector"* %this1, i32 %5)
  br label %if.end

if.else:                                          ; preds = %entry
  %6 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__213__vector_baseIfNS_9allocatorIfEEE7__allocEv(%"class.std::__2::__vector_base"* %6) #8
  store %"class.std::__2::allocator"* %call2, %"class.std::__2::allocator"** %__a, align 4
  %call3 = call i32 @_ZNKSt3__26vectorIfNS_9allocatorIfEEE4sizeEv(%"class.std::__2::vector"* %this1) #8
  %7 = load i32, i32* %__n.addr, align 4
  %add = add i32 %call3, %7
  %call4 = call i32 @_ZNKSt3__26vectorIfNS_9allocatorIfEEE11__recommendEm(%"class.std::__2::vector"* %this1, i32 %add)
  %call5 = call i32 @_ZNKSt3__26vectorIfNS_9allocatorIfEEE4sizeEv(%"class.std::__2::vector"* %this1) #8
  %8 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a, align 4
  %call6 = call %"struct.std::__2::__split_buffer.58"* @_ZNSt3__214__split_bufferIfRNS_9allocatorIfEEEC2EmmS3_(%"struct.std::__2::__split_buffer.58"* %__v, i32 %call4, i32 %call5, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %8)
  %9 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__214__split_bufferIfRNS_9allocatorIfEEE18__construct_at_endEm(%"struct.std::__2::__split_buffer.58"* %__v, i32 %9)
  call void @_ZNSt3__26vectorIfNS_9allocatorIfEEE26__swap_out_circular_bufferERNS_14__split_bufferIfRS2_EE(%"class.std::__2::vector"* %this1, %"struct.std::__2::__split_buffer.58"* nonnull align 4 dereferenceable(20) %__v)
  %call7 = call %"struct.std::__2::__split_buffer.58"* @_ZNSt3__214__split_bufferIfRNS_9allocatorIfEEED2Ev(%"struct.std::__2::__split_buffer.58"* %__v) #8
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIfNS_9allocatorIfEEE17__destruct_at_endEPf(%"class.std::__2::vector"* %this, float* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__new_last.addr = alloca float*, align 4
  %__old_size = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store float* %__new_last, float** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = load float*, float** %__new_last.addr, align 4
  call void @_ZNSt3__26vectorIfNS_9allocatorIfEEE27__invalidate_iterators_pastEPf(%"class.std::__2::vector"* %this1, float* %0)
  %call = call i32 @_ZNKSt3__26vectorIfNS_9allocatorIfEEE4sizeEv(%"class.std::__2::vector"* %this1) #8
  store i32 %call, i32* %__old_size, align 4
  %1 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %2 = load float*, float** %__new_last.addr, align 4
  call void @_ZNSt3__213__vector_baseIfNS_9allocatorIfEEE17__destruct_at_endEPf(%"class.std::__2::__vector_base"* %1, float* %2) #8
  %3 = load i32, i32* %__old_size, align 4
  call void @_ZNKSt3__26vectorIfNS_9allocatorIfEEE17__annotate_shrinkEm(%"class.std::__2::vector"* %this1, i32 %3) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float** @_ZNSt3__213__vector_baseIfNS_9allocatorIfEEE9__end_capEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) float** @_ZNSt3__217__compressed_pairIPfNS_9allocatorIfEEE5firstEv(%"class.std::__2::__compressed_pair"* %__end_cap_) #8
  ret float** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIfNS_9allocatorIfEEE18__construct_at_endEm(%"class.std::__2::vector"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__n.addr = alloca i32, align 4
  %__tx = alloca %"struct.std::__2::vector<float, std::__2::allocator<float>>::_ConstructTransaction", align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %call = call %"struct.std::__2::vector<float, std::__2::allocator<float>>::_ConstructTransaction"* @_ZNSt3__26vectorIfNS_9allocatorIfEEE21_ConstructTransactionC2ERS3_m(%"struct.std::__2::vector<float, std::__2::allocator<float>>::_ConstructTransaction"* %__tx, %"class.std::__2::vector"* nonnull align 4 dereferenceable(12) %this1, i32 %0)
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<float, std::__2::allocator<float>>::_ConstructTransaction", %"struct.std::__2::vector<float, std::__2::allocator<float>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %1 = load float*, float** %__pos_, align 4
  %__new_end_ = getelementptr inbounds %"struct.std::__2::vector<float, std::__2::allocator<float>>::_ConstructTransaction", %"struct.std::__2::vector<float, std::__2::allocator<float>>::_ConstructTransaction"* %__tx, i32 0, i32 2
  %2 = load float*, float** %__new_end_, align 4
  %cmp = icmp ne float* %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__213__vector_baseIfNS_9allocatorIfEEE7__allocEv(%"class.std::__2::__vector_base"* %3) #8
  %__pos_3 = getelementptr inbounds %"struct.std::__2::vector<float, std::__2::allocator<float>>::_ConstructTransaction", %"struct.std::__2::vector<float, std::__2::allocator<float>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %4 = load float*, float** %__pos_3, align 4
  %call4 = call float* @_ZNSt3__212__to_addressIfEEPT_S2_(float* %4) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIfEEE9constructIfJEEEvRS2_PT_DpOT0_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call2, float* %call4)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %__pos_5 = getelementptr inbounds %"struct.std::__2::vector<float, std::__2::allocator<float>>::_ConstructTransaction", %"struct.std::__2::vector<float, std::__2::allocator<float>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %5 = load float*, float** %__pos_5, align 4
  %incdec.ptr = getelementptr inbounds float, float* %5, i32 1
  store float* %incdec.ptr, float** %__pos_5, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call6 = call %"struct.std::__2::vector<float, std::__2::allocator<float>>::_ConstructTransaction"* @_ZNSt3__26vectorIfNS_9allocatorIfEEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<float, std::__2::allocator<float>>::_ConstructTransaction"* %__tx) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIfNS_9allocatorIfEEE11__recommendEm(%"class.std::__2::vector"* %this, i32 %__new_size) #0 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__new_size.addr = alloca i32, align 4
  %__ms = alloca i32, align 4
  %__cap = alloca i32, align 4
  %ref.tmp = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__new_size, i32* %__new_size.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIfNS_9allocatorIfEEE8max_sizeEv(%"class.std::__2::vector"* %this1) #8
  store i32 %call, i32* %__ms, align 4
  %0 = load i32, i32* %__new_size.addr, align 4
  %1 = load i32, i32* %__ms, align 4
  %cmp = icmp ugt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base_common"*
  call void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"* %2) #11
  unreachable

if.end:                                           ; preds = %entry
  %call2 = call i32 @_ZNKSt3__26vectorIfNS_9allocatorIfEEE8capacityEv(%"class.std::__2::vector"* %this1) #8
  store i32 %call2, i32* %__cap, align 4
  %3 = load i32, i32* %__cap, align 4
  %4 = load i32, i32* %__ms, align 4
  %div = udiv i32 %4, 2
  %cmp3 = icmp uge i32 %3, %div
  br i1 %cmp3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %if.end
  %5 = load i32, i32* %__ms, align 4
  store i32 %5, i32* %retval, align 4
  br label %return

if.end5:                                          ; preds = %if.end
  %6 = load i32, i32* %__cap, align 4
  %mul = mul i32 2, %6
  store i32 %mul, i32* %ref.tmp, align 4
  %call6 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %__new_size.addr)
  %7 = load i32, i32* %call6, align 4
  store i32 %7, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end5, %if.then4
  %8 = load i32, i32* %retval, align 4
  ret i32 %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer.58"* @_ZNSt3__214__split_bufferIfRNS_9allocatorIfEEEC2EmmS3_(%"struct.std::__2::__split_buffer.58"* returned %this, i32 %__cap, i32 %__start, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"struct.std::__2::__split_buffer.58"*, align 4
  %this.addr = alloca %"struct.std::__2::__split_buffer.58"*, align 4
  %__cap.addr = alloca i32, align 4
  %__start.addr = alloca i32, align 4
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %ref.tmp = alloca i8*, align 4
  store %"struct.std::__2::__split_buffer.58"* %this, %"struct.std::__2::__split_buffer.58"** %this.addr, align 4
  store i32 %__cap, i32* %__cap.addr, align 4
  store i32 %__start, i32* %__start.addr, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.58"*, %"struct.std::__2::__split_buffer.58"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer.58"* %this1, %"struct.std::__2::__split_buffer.58"** %retval, align 4
  %0 = bitcast %"struct.std::__2::__split_buffer.58"* %this1 to %"class.std::__2::__split_buffer_common"*
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer.58", %"struct.std::__2::__split_buffer.58"* %this1, i32 0, i32 3
  store i8* null, i8** %ref.tmp, align 4
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %call = call %"class.std::__2::__compressed_pair.59"* @_ZNSt3__217__compressed_pairIPfRNS_9allocatorIfEEEC2IDnS4_EEOT_OT0_(%"class.std::__2::__compressed_pair.59"* %__end_cap_, i8** nonnull align 4 dereferenceable(4) %ref.tmp, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %1)
  %2 = load i32, i32* %__cap.addr, align 4
  %cmp = icmp ne i32 %2, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__214__split_bufferIfRNS_9allocatorIfEEE7__allocEv(%"struct.std::__2::__split_buffer.58"* %this1) #8
  %3 = load i32, i32* %__cap.addr, align 4
  %call3 = call float* @_ZNSt3__216allocator_traitsINS_9allocatorIfEEE8allocateERS2_m(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call2, i32 %3)
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float* [ %call3, %cond.true ], [ null, %cond.false ]
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer.58", %"struct.std::__2::__split_buffer.58"* %this1, i32 0, i32 0
  store float* %cond, float** %__first_, align 4
  %__first_4 = getelementptr inbounds %"struct.std::__2::__split_buffer.58", %"struct.std::__2::__split_buffer.58"* %this1, i32 0, i32 0
  %4 = load float*, float** %__first_4, align 4
  %5 = load i32, i32* %__start.addr, align 4
  %add.ptr = getelementptr inbounds float, float* %4, i32 %5
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer.58", %"struct.std::__2::__split_buffer.58"* %this1, i32 0, i32 2
  store float* %add.ptr, float** %__end_, align 4
  %__begin_ = getelementptr inbounds %"struct.std::__2::__split_buffer.58", %"struct.std::__2::__split_buffer.58"* %this1, i32 0, i32 1
  store float* %add.ptr, float** %__begin_, align 4
  %__first_5 = getelementptr inbounds %"struct.std::__2::__split_buffer.58", %"struct.std::__2::__split_buffer.58"* %this1, i32 0, i32 0
  %6 = load float*, float** %__first_5, align 4
  %7 = load i32, i32* %__cap.addr, align 4
  %add.ptr6 = getelementptr inbounds float, float* %6, i32 %7
  %call7 = call nonnull align 4 dereferenceable(4) float** @_ZNSt3__214__split_bufferIfRNS_9allocatorIfEEE9__end_capEv(%"struct.std::__2::__split_buffer.58"* %this1) #8
  store float* %add.ptr6, float** %call7, align 4
  %8 = load %"struct.std::__2::__split_buffer.58"*, %"struct.std::__2::__split_buffer.58"** %retval, align 4
  ret %"struct.std::__2::__split_buffer.58"* %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIfRNS_9allocatorIfEEE18__construct_at_endEm(%"struct.std::__2::__split_buffer.58"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.58"*, align 4
  %__n.addr = alloca i32, align 4
  %__tx = alloca %"struct.std::__2::__split_buffer<float, std::__2::allocator<float> &>::_ConstructTransaction", align 4
  store %"struct.std::__2::__split_buffer.58"* %this, %"struct.std::__2::__split_buffer.58"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.58"*, %"struct.std::__2::__split_buffer.58"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer.58", %"struct.std::__2::__split_buffer.58"* %this1, i32 0, i32 2
  %0 = load i32, i32* %__n.addr, align 4
  %call = call %"struct.std::__2::__split_buffer<float, std::__2::allocator<float> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferIfRNS_9allocatorIfEEE21_ConstructTransactionC2EPPfm(%"struct.std::__2::__split_buffer<float, std::__2::allocator<float> &>::_ConstructTransaction"* %__tx, float** %__end_, i32 %0) #8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %__pos_ = getelementptr inbounds %"struct.std::__2::__split_buffer<float, std::__2::allocator<float> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<float, std::__2::allocator<float> &>::_ConstructTransaction"* %__tx, i32 0, i32 0
  %1 = load float*, float** %__pos_, align 4
  %__end_2 = getelementptr inbounds %"struct.std::__2::__split_buffer<float, std::__2::allocator<float> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<float, std::__2::allocator<float> &>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %2 = load float*, float** %__end_2, align 4
  %cmp = icmp ne float* %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %call3 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__214__split_bufferIfRNS_9allocatorIfEEE7__allocEv(%"struct.std::__2::__split_buffer.58"* %this1) #8
  %__pos_4 = getelementptr inbounds %"struct.std::__2::__split_buffer<float, std::__2::allocator<float> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<float, std::__2::allocator<float> &>::_ConstructTransaction"* %__tx, i32 0, i32 0
  %3 = load float*, float** %__pos_4, align 4
  %call5 = call float* @_ZNSt3__212__to_addressIfEEPT_S2_(float* %3) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIfEEE9constructIfJEEEvRS2_PT_DpOT0_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call3, float* %call5)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %__pos_6 = getelementptr inbounds %"struct.std::__2::__split_buffer<float, std::__2::allocator<float> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<float, std::__2::allocator<float> &>::_ConstructTransaction"* %__tx, i32 0, i32 0
  %4 = load float*, float** %__pos_6, align 4
  %incdec.ptr = getelementptr inbounds float, float* %4, i32 1
  store float* %incdec.ptr, float** %__pos_6, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call7 = call %"struct.std::__2::__split_buffer<float, std::__2::allocator<float> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferIfRNS_9allocatorIfEEE21_ConstructTransactionD2Ev(%"struct.std::__2::__split_buffer<float, std::__2::allocator<float> &>::_ConstructTransaction"* %__tx) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIfNS_9allocatorIfEEE26__swap_out_circular_bufferERNS_14__split_bufferIfRS2_EE(%"class.std::__2::vector"* %this, %"struct.std::__2::__split_buffer.58"* nonnull align 4 dereferenceable(20) %__v) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__v.addr = alloca %"struct.std::__2::__split_buffer.58"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer.58"* %__v, %"struct.std::__2::__split_buffer.58"** %__v.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  call void @_ZNKSt3__26vectorIfNS_9allocatorIfEEE17__annotate_deleteEv(%"class.std::__2::vector"* %this1) #8
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__213__vector_baseIfNS_9allocatorIfEEE7__allocEv(%"class.std::__2::__vector_base"* %0) #8
  %1 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %1, i32 0, i32 0
  %2 = load float*, float** %__begin_, align 4
  %3 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %3, i32 0, i32 1
  %4 = load float*, float** %__end_, align 4
  %5 = load %"struct.std::__2::__split_buffer.58"*, %"struct.std::__2::__split_buffer.58"** %__v.addr, align 4
  %__begin_2 = getelementptr inbounds %"struct.std::__2::__split_buffer.58", %"struct.std::__2::__split_buffer.58"* %5, i32 0, i32 1
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIfEEE46__construct_backward_with_exception_guaranteesIfEENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS2_PT_S8_EE5valuesr31is_trivially_move_constructibleIS8_EE5valueEvE4typeERS2_S9_S9_RS9_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call, float* %2, float* %4, float** nonnull align 4 dereferenceable(4) %__begin_2)
  %6 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_3 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %6, i32 0, i32 0
  %7 = load %"struct.std::__2::__split_buffer.58"*, %"struct.std::__2::__split_buffer.58"** %__v.addr, align 4
  %__begin_4 = getelementptr inbounds %"struct.std::__2::__split_buffer.58", %"struct.std::__2::__split_buffer.58"* %7, i32 0, i32 1
  call void @_ZNSt3__24swapIPfEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_(float** nonnull align 4 dereferenceable(4) %__begin_3, float** nonnull align 4 dereferenceable(4) %__begin_4) #8
  %8 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__end_5 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %8, i32 0, i32 1
  %9 = load %"struct.std::__2::__split_buffer.58"*, %"struct.std::__2::__split_buffer.58"** %__v.addr, align 4
  %__end_6 = getelementptr inbounds %"struct.std::__2::__split_buffer.58", %"struct.std::__2::__split_buffer.58"* %9, i32 0, i32 2
  call void @_ZNSt3__24swapIPfEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_(float** nonnull align 4 dereferenceable(4) %__end_5, float** nonnull align 4 dereferenceable(4) %__end_6) #8
  %10 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call7 = call nonnull align 4 dereferenceable(4) float** @_ZNSt3__213__vector_baseIfNS_9allocatorIfEEE9__end_capEv(%"class.std::__2::__vector_base"* %10) #8
  %11 = load %"struct.std::__2::__split_buffer.58"*, %"struct.std::__2::__split_buffer.58"** %__v.addr, align 4
  %call8 = call nonnull align 4 dereferenceable(4) float** @_ZNSt3__214__split_bufferIfRNS_9allocatorIfEEE9__end_capEv(%"struct.std::__2::__split_buffer.58"* %11) #8
  call void @_ZNSt3__24swapIPfEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_(float** nonnull align 4 dereferenceable(4) %call7, float** nonnull align 4 dereferenceable(4) %call8) #8
  %12 = load %"struct.std::__2::__split_buffer.58"*, %"struct.std::__2::__split_buffer.58"** %__v.addr, align 4
  %__begin_9 = getelementptr inbounds %"struct.std::__2::__split_buffer.58", %"struct.std::__2::__split_buffer.58"* %12, i32 0, i32 1
  %13 = load float*, float** %__begin_9, align 4
  %14 = load %"struct.std::__2::__split_buffer.58"*, %"struct.std::__2::__split_buffer.58"** %__v.addr, align 4
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer.58", %"struct.std::__2::__split_buffer.58"* %14, i32 0, i32 0
  store float* %13, float** %__first_, align 4
  %call10 = call i32 @_ZNKSt3__26vectorIfNS_9allocatorIfEEE4sizeEv(%"class.std::__2::vector"* %this1) #8
  call void @_ZNKSt3__26vectorIfNS_9allocatorIfEEE14__annotate_newEm(%"class.std::__2::vector"* %this1, i32 %call10) #8
  call void @_ZNSt3__26vectorIfNS_9allocatorIfEEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector"* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer.58"* @_ZNSt3__214__split_bufferIfRNS_9allocatorIfEEED2Ev(%"struct.std::__2::__split_buffer.58"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"struct.std::__2::__split_buffer.58"*, align 4
  %this.addr = alloca %"struct.std::__2::__split_buffer.58"*, align 4
  store %"struct.std::__2::__split_buffer.58"* %this, %"struct.std::__2::__split_buffer.58"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.58"*, %"struct.std::__2::__split_buffer.58"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer.58"* %this1, %"struct.std::__2::__split_buffer.58"** %retval, align 4
  call void @_ZNSt3__214__split_bufferIfRNS_9allocatorIfEEE5clearEv(%"struct.std::__2::__split_buffer.58"* %this1) #8
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer.58", %"struct.std::__2::__split_buffer.58"* %this1, i32 0, i32 0
  %0 = load float*, float** %__first_, align 4
  %tobool = icmp ne float* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__214__split_bufferIfRNS_9allocatorIfEEE7__allocEv(%"struct.std::__2::__split_buffer.58"* %this1) #8
  %__first_2 = getelementptr inbounds %"struct.std::__2::__split_buffer.58", %"struct.std::__2::__split_buffer.58"* %this1, i32 0, i32 0
  %1 = load float*, float** %__first_2, align 4
  %call3 = call i32 @_ZNKSt3__214__split_bufferIfRNS_9allocatorIfEEE8capacityEv(%"struct.std::__2::__split_buffer.58"* %this1)
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIfEEE10deallocateERS2_Pfm(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call, float* %1, i32 %call3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"struct.std::__2::__split_buffer.58"*, %"struct.std::__2::__split_buffer.58"** %retval, align 4
  ret %"struct.std::__2::__split_buffer.58"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float** @_ZNSt3__217__compressed_pairIPfNS_9allocatorIfEEE5firstEv(%"class.std::__2::__compressed_pair"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %call = call nonnull align 4 dereferenceable(4) float** @_ZNSt3__222__compressed_pair_elemIPfLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %0) #8
  ret float** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float** @_ZNSt3__222__compressed_pair_elemIPfLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem"*, align 4
  store %"struct.std::__2::__compressed_pair_elem"* %this, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem"*, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem"* %this1, i32 0, i32 0
  ret float** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<float, std::__2::allocator<float>>::_ConstructTransaction"* @_ZNSt3__26vectorIfNS_9allocatorIfEEE21_ConstructTransactionC2ERS3_m(%"struct.std::__2::vector<float, std::__2::allocator<float>>::_ConstructTransaction"* returned %this, %"class.std::__2::vector"* nonnull align 4 dereferenceable(12) %__v, i32 %__n) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<float, std::__2::allocator<float>>::_ConstructTransaction"*, align 4
  %__v.addr = alloca %"class.std::__2::vector"*, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::vector<float, std::__2::allocator<float>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<float, std::__2::allocator<float>>::_ConstructTransaction"** %this.addr, align 4
  store %"class.std::__2::vector"* %__v, %"class.std::__2::vector"** %__v.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::vector<float, std::__2::allocator<float>>::_ConstructTransaction"*, %"struct.std::__2::vector<float, std::__2::allocator<float>>::_ConstructTransaction"** %this.addr, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<float, std::__2::allocator<float>>::_ConstructTransaction", %"struct.std::__2::vector<float, std::__2::allocator<float>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__v.addr, align 4
  store %"class.std::__2::vector"* %0, %"class.std::__2::vector"** %__v_, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<float, std::__2::allocator<float>>::_ConstructTransaction", %"struct.std::__2::vector<float, std::__2::allocator<float>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__v.addr, align 4
  %2 = bitcast %"class.std::__2::vector"* %1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %2, i32 0, i32 1
  %3 = load float*, float** %__end_, align 4
  store float* %3, float** %__pos_, align 4
  %__new_end_ = getelementptr inbounds %"struct.std::__2::vector<float, std::__2::allocator<float>>::_ConstructTransaction", %"struct.std::__2::vector<float, std::__2::allocator<float>>::_ConstructTransaction"* %this1, i32 0, i32 2
  %4 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__v.addr, align 4
  %5 = bitcast %"class.std::__2::vector"* %4 to %"class.std::__2::__vector_base"*
  %__end_2 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %5, i32 0, i32 1
  %6 = load float*, float** %__end_2, align 4
  %7 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds float, float* %6, i32 %7
  store float* %add.ptr, float** %__new_end_, align 4
  ret %"struct.std::__2::vector<float, std::__2::allocator<float>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIfEEE9constructIfJEEEvRS2_PT_DpOT0_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, float* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca float*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_construct.61", align 1
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store float* %__p, float** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_construct.61"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %2 = load float*, float** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIfEEE11__constructIfJEEEvNS_17integral_constantIbLb1EEERS2_PT_DpOT0_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %1, float* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<float, std::__2::allocator<float>>::_ConstructTransaction"* @_ZNSt3__26vectorIfNS_9allocatorIfEEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<float, std::__2::allocator<float>>::_ConstructTransaction"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<float, std::__2::allocator<float>>::_ConstructTransaction"*, align 4
  store %"struct.std::__2::vector<float, std::__2::allocator<float>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<float, std::__2::allocator<float>>::_ConstructTransaction"** %this.addr, align 4
  %this1 = load %"struct.std::__2::vector<float, std::__2::allocator<float>>::_ConstructTransaction"*, %"struct.std::__2::vector<float, std::__2::allocator<float>>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<float, std::__2::allocator<float>>::_ConstructTransaction", %"struct.std::__2::vector<float, std::__2::allocator<float>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %0 = load float*, float** %__pos_, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<float, std::__2::allocator<float>>::_ConstructTransaction", %"struct.std::__2::vector<float, std::__2::allocator<float>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__v_, align 4
  %2 = bitcast %"class.std::__2::vector"* %1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %2, i32 0, i32 1
  store float* %0, float** %__end_, align 4
  ret %"struct.std::__2::vector<float, std::__2::allocator<float>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIfEEE11__constructIfJEEEvNS_17integral_constantIbLb1EEERS2_PT_DpOT0_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, float* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca float*, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store float* %__p, float** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %2 = load float*, float** %__p.addr, align 4
  call void @_ZNSt3__29allocatorIfE9constructIfJEEEvPT_DpOT0_(%"class.std::__2::allocator"* %1, float* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIfE9constructIfJEEEvPT_DpOT0_(%"class.std::__2::allocator"* %this, float* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca float*, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  store float* %__p, float** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  %0 = load float*, float** %__p.addr, align 4
  %1 = bitcast float* %0 to i8*
  %2 = bitcast i8* %1 to float*
  store float 0.000000e+00, float* %2, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIfNS_9allocatorIfEEE8max_sizeEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp3 = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNKSt3__213__vector_baseIfNS_9allocatorIfEEE7__allocEv(%"class.std::__2::__vector_base"* %0) #8
  %call2 = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorIfEEE8max_sizeERKS2_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call) #8
  store i32 %call2, i32* %ref.tmp, align 4
  %call4 = call i32 @_ZNSt3__214numeric_limitsIlE3maxEv() #8
  store i32 %call4, i32* %ref.tmp3, align 4
  %call5 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %1 = load i32, i32* %call5, align 4
  ret i32 %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorIfEEE8max_sizeERKS2_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_max_size.62", align 1
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_max_size.62"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %call = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorIfEEE10__max_sizeENS_17integral_constantIbLb1EEERKS2_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %1) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNKSt3__213__vector_baseIfNS_9allocatorIfEEE7__allocEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNKSt3__217__compressed_pairIPfNS_9allocatorIfEEE6secondEv(%"class.std::__2::__compressed_pair"* %__end_cap_) #8
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorIfEEE10__max_sizeENS_17integral_constantIbLb1EEERKS2_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorIfE8max_sizeEv(%"class.std::__2::allocator"* %1) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__29allocatorIfE8max_sizeEv(%"class.std::__2::allocator"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  ret i32 1073741823
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNKSt3__217__compressed_pairIPfNS_9allocatorIfEEE6secondEv(%"class.std::__2::__compressed_pair"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem.0"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorIfEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.0"* %0) #8
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorIfEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.0"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.0"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.0"* %this, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.0"*, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.0"* %this1 to %"class.std::__2::allocator"*
  ret %"class.std::__2::allocator"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.59"* @_ZNSt3__217__compressed_pairIPfRNS_9allocatorIfEEEC2IDnS4_EEOT_OT0_(%"class.std::__2::__compressed_pair.59"* returned %this, i8** nonnull align 4 dereferenceable(4) %__t1, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.59"*, align 4
  %__t1.addr = alloca i8**, align 4
  %__t2.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"class.std::__2::__compressed_pair.59"* %this, %"class.std::__2::__compressed_pair.59"** %this.addr, align 4
  store i8** %__t1, i8*** %__t1.addr, align 4
  store %"class.std::__2::allocator"* %__t2, %"class.std::__2::allocator"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.59"*, %"class.std::__2::__compressed_pair.59"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.59"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %1 = load i8**, i8*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %1) #8
  %call2 = call %"struct.std::__2::__compressed_pair_elem"* @_ZNSt3__222__compressed_pair_elemIPfLi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem"* %0, i8** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.59"* %this1 to i8*
  %3 = getelementptr inbounds i8, i8* %2, i32 4
  %4 = bitcast i8* %3 to %"struct.std::__2::__compressed_pair_elem.60"*
  %5 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__27forwardIRNS_9allocatorIfEEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %5) #8
  %call4 = call %"struct.std::__2::__compressed_pair_elem.60"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIfEELi1ELb0EEC2IS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.60"* %4, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::__compressed_pair.59"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZNSt3__216allocator_traitsINS_9allocatorIfEEE8allocateERS2_m(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call float* @_ZNSt3__29allocatorIfE8allocateEmPKv(%"class.std::__2::allocator"* %0, i32 %1, i8* null)
  ret float* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__214__split_bufferIfRNS_9allocatorIfEEE7__allocEv(%"struct.std::__2::__split_buffer.58"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.58"*, align 4
  store %"struct.std::__2::__split_buffer.58"* %this, %"struct.std::__2::__split_buffer.58"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.58"*, %"struct.std::__2::__split_buffer.58"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer.58", %"struct.std::__2::__split_buffer.58"* %this1, i32 0, i32 3
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__217__compressed_pairIPfRNS_9allocatorIfEEE6secondEv(%"class.std::__2::__compressed_pair.59"* %__end_cap_) #8
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float** @_ZNSt3__214__split_bufferIfRNS_9allocatorIfEEE9__end_capEv(%"struct.std::__2::__split_buffer.58"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.58"*, align 4
  store %"struct.std::__2::__split_buffer.58"* %this, %"struct.std::__2::__split_buffer.58"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.58"*, %"struct.std::__2::__split_buffer.58"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer.58", %"struct.std::__2::__split_buffer.58"* %this1, i32 0, i32 3
  %call = call nonnull align 4 dereferenceable(4) float** @_ZNSt3__217__compressed_pairIPfRNS_9allocatorIfEEE5firstEv(%"class.std::__2::__compressed_pair.59"* %__end_cap_) #8
  ret float** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem"* @_ZNSt3__222__compressed_pair_elemIPfLi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem"* returned %this, i8** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem"*, align 4
  %__u.addr = alloca i8**, align 4
  store %"struct.std::__2::__compressed_pair_elem"* %this, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  store i8** %__u, i8*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem"*, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem"* %this1, i32 0, i32 0
  %0 = load i8**, i8*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %0) #8
  store float* null, float** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__27forwardIRNS_9allocatorIfEEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"class.std::__2::allocator"* %__t, %"class.std::__2::allocator"** %__t.addr, align 4
  %0 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__t.addr, align 4
  ret %"class.std::__2::allocator"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.60"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIfEELi1ELb0EEC2IS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.60"* returned %this, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.60"*, align 4
  %__u.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.60"* %this, %"struct.std::__2::__compressed_pair_elem.60"** %this.addr, align 4
  store %"class.std::__2::allocator"* %__u, %"class.std::__2::allocator"** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.60"*, %"struct.std::__2::__compressed_pair_elem.60"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.60", %"struct.std::__2::__compressed_pair_elem.60"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__u.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__27forwardIRNS_9allocatorIfEEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %0) #8
  store %"class.std::__2::allocator"* %call, %"class.std::__2::allocator"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.60"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZNSt3__29allocatorIfE8allocateEmPKv(%"class.std::__2::allocator"* %this, i32 %__n, i8* %0) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  %__n.addr = alloca i32, align 4
  %.addr = alloca i8*, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorIfE8max_sizeEv(%"class.std::__2::allocator"* %this1) #8
  %cmp = icmp ugt i32 %1, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__220__throw_length_errorEPKc(i8* getelementptr inbounds ([68 x i8], [68 x i8]* @.str, i32 0, i32 0)) #11
  unreachable

if.end:                                           ; preds = %entry
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  %call2 = call i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %mul, i32 4)
  %3 = bitcast i8* %call2 to float*
  ret float* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__217__compressed_pairIPfRNS_9allocatorIfEEE6secondEv(%"class.std::__2::__compressed_pair.59"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.59"*, align 4
  store %"class.std::__2::__compressed_pair.59"* %this, %"class.std::__2::__compressed_pair.59"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.59"*, %"class.std::__2::__compressed_pair.59"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.59"* %this1 to i8*
  %add.ptr = getelementptr inbounds i8, i8* %0, i32 4
  %1 = bitcast i8* %add.ptr to %"struct.std::__2::__compressed_pair_elem.60"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIfEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.60"* %1) #8
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIfEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.60"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.60"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.60"* %this, %"struct.std::__2::__compressed_pair_elem.60"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.60"*, %"struct.std::__2::__compressed_pair_elem.60"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.60", %"struct.std::__2::__compressed_pair_elem.60"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__value_, align 4
  ret %"class.std::__2::allocator"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float** @_ZNSt3__217__compressed_pairIPfRNS_9allocatorIfEEE5firstEv(%"class.std::__2::__compressed_pair.59"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.59"*, align 4
  store %"class.std::__2::__compressed_pair.59"* %this, %"class.std::__2::__compressed_pair.59"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.59"*, %"class.std::__2::__compressed_pair.59"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.59"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %call = call nonnull align 4 dereferenceable(4) float** @_ZNSt3__222__compressed_pair_elemIPfLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %0) #8
  ret float** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer<float, std::__2::allocator<float> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferIfRNS_9allocatorIfEEE21_ConstructTransactionC2EPPfm(%"struct.std::__2::__split_buffer<float, std::__2::allocator<float> &>::_ConstructTransaction"* returned %this, float** %__p, i32 %__n) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer<float, std::__2::allocator<float> &>::_ConstructTransaction"*, align 4
  %__p.addr = alloca float**, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::__split_buffer<float, std::__2::allocator<float> &>::_ConstructTransaction"* %this, %"struct.std::__2::__split_buffer<float, std::__2::allocator<float> &>::_ConstructTransaction"** %this.addr, align 4
  store float** %__p, float*** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer<float, std::__2::allocator<float> &>::_ConstructTransaction"*, %"struct.std::__2::__split_buffer<float, std::__2::allocator<float> &>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::__split_buffer<float, std::__2::allocator<float> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<float, std::__2::allocator<float> &>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load float**, float*** %__p.addr, align 4
  %1 = load float*, float** %0, align 4
  store float* %1, float** %__pos_, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer<float, std::__2::allocator<float> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<float, std::__2::allocator<float> &>::_ConstructTransaction"* %this1, i32 0, i32 1
  %2 = load float**, float*** %__p.addr, align 4
  %3 = load float*, float** %2, align 4
  %4 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds float, float* %3, i32 %4
  store float* %add.ptr, float** %__end_, align 4
  %__dest_ = getelementptr inbounds %"struct.std::__2::__split_buffer<float, std::__2::allocator<float> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<float, std::__2::allocator<float> &>::_ConstructTransaction"* %this1, i32 0, i32 2
  %5 = load float**, float*** %__p.addr, align 4
  store float** %5, float*** %__dest_, align 4
  ret %"struct.std::__2::__split_buffer<float, std::__2::allocator<float> &>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer<float, std::__2::allocator<float> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferIfRNS_9allocatorIfEEE21_ConstructTransactionD2Ev(%"struct.std::__2::__split_buffer<float, std::__2::allocator<float> &>::_ConstructTransaction"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer<float, std::__2::allocator<float> &>::_ConstructTransaction"*, align 4
  store %"struct.std::__2::__split_buffer<float, std::__2::allocator<float> &>::_ConstructTransaction"* %this, %"struct.std::__2::__split_buffer<float, std::__2::allocator<float> &>::_ConstructTransaction"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer<float, std::__2::allocator<float> &>::_ConstructTransaction"*, %"struct.std::__2::__split_buffer<float, std::__2::allocator<float> &>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::__split_buffer<float, std::__2::allocator<float> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<float, std::__2::allocator<float> &>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load float*, float** %__pos_, align 4
  %__dest_ = getelementptr inbounds %"struct.std::__2::__split_buffer<float, std::__2::allocator<float> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<float, std::__2::allocator<float> &>::_ConstructTransaction"* %this1, i32 0, i32 2
  %1 = load float**, float*** %__dest_, align 4
  store float* %0, float** %1, align 4
  ret %"struct.std::__2::__split_buffer<float, std::__2::allocator<float> &>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIfEEE46__construct_backward_with_exception_guaranteesIfEENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS2_PT_S8_EE5valuesr31is_trivially_move_constructibleIS8_EE5valueEvE4typeERS2_S9_S9_RS9_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %0, float* %__begin1, float* %__end1, float** nonnull align 4 dereferenceable(4) %__end2) #0 comdat {
entry:
  %.addr = alloca %"class.std::__2::allocator"*, align 4
  %__begin1.addr = alloca float*, align 4
  %__end1.addr = alloca float*, align 4
  %__end2.addr = alloca float**, align 4
  %_Np = alloca i32, align 4
  store %"class.std::__2::allocator"* %0, %"class.std::__2::allocator"** %.addr, align 4
  store float* %__begin1, float** %__begin1.addr, align 4
  store float* %__end1, float** %__end1.addr, align 4
  store float** %__end2, float*** %__end2.addr, align 4
  %1 = load float*, float** %__end1.addr, align 4
  %2 = load float*, float** %__begin1.addr, align 4
  %sub.ptr.lhs.cast = ptrtoint float* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint float* %2 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  store i32 %sub.ptr.div, i32* %_Np, align 4
  %3 = load i32, i32* %_Np, align 4
  %4 = load float**, float*** %__end2.addr, align 4
  %5 = load float*, float** %4, align 4
  %idx.neg = sub i32 0, %3
  %add.ptr = getelementptr inbounds float, float* %5, i32 %idx.neg
  store float* %add.ptr, float** %4, align 4
  %6 = load i32, i32* %_Np, align 4
  %cmp = icmp sgt i32 %6, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %7 = load float**, float*** %__end2.addr, align 4
  %8 = load float*, float** %7, align 4
  %9 = bitcast float* %8 to i8*
  %10 = load float*, float** %__begin1.addr, align 4
  %11 = bitcast float* %10 to i8*
  %12 = load i32, i32* %_Np, align 4
  %mul = mul i32 %12, 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %11, i32 %mul, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__24swapIPfEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_(float** nonnull align 4 dereferenceable(4) %__x, float** nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %__x.addr = alloca float**, align 4
  %__y.addr = alloca float**, align 4
  %__t = alloca float*, align 4
  store float** %__x, float*** %__x.addr, align 4
  store float** %__y, float*** %__y.addr, align 4
  %0 = load float**, float*** %__x.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) float** @_ZNSt3__24moveIRPfEEONS_16remove_referenceIT_E4typeEOS4_(float** nonnull align 4 dereferenceable(4) %0) #8
  %1 = load float*, float** %call, align 4
  store float* %1, float** %__t, align 4
  %2 = load float**, float*** %__y.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(4) float** @_ZNSt3__24moveIRPfEEONS_16remove_referenceIT_E4typeEOS4_(float** nonnull align 4 dereferenceable(4) %2) #8
  %3 = load float*, float** %call1, align 4
  %4 = load float**, float*** %__x.addr, align 4
  store float* %3, float** %4, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float** @_ZNSt3__24moveIRPfEEONS_16remove_referenceIT_E4typeEOS4_(float** nonnull align 4 dereferenceable(4) %__t) #8
  %5 = load float*, float** %call2, align 4
  %6 = load float**, float*** %__y.addr, align 4
  store float* %5, float** %6, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIfNS_9allocatorIfEEE14__annotate_newEm(%"class.std::__2::vector"* %this, i32 %__current_size) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__current_size.addr = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__current_size, i32* %__current_size.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %call = call float* @_ZNKSt3__26vectorIfNS_9allocatorIfEEE4dataEv(%"class.std::__2::vector"* %this1) #8
  %0 = bitcast float* %call to i8*
  %call2 = call float* @_ZNKSt3__26vectorIfNS_9allocatorIfEEE4dataEv(%"class.std::__2::vector"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorIfNS_9allocatorIfEEE8capacityEv(%"class.std::__2::vector"* %this1) #8
  %add.ptr = getelementptr inbounds float, float* %call2, i32 %call3
  %1 = bitcast float* %add.ptr to i8*
  %call4 = call float* @_ZNKSt3__26vectorIfNS_9allocatorIfEEE4dataEv(%"class.std::__2::vector"* %this1) #8
  %call5 = call i32 @_ZNKSt3__26vectorIfNS_9allocatorIfEEE8capacityEv(%"class.std::__2::vector"* %this1) #8
  %add.ptr6 = getelementptr inbounds float, float* %call4, i32 %call5
  %2 = bitcast float* %add.ptr6 to i8*
  %call7 = call float* @_ZNKSt3__26vectorIfNS_9allocatorIfEEE4dataEv(%"class.std::__2::vector"* %this1) #8
  %3 = load i32, i32* %__current_size.addr, align 4
  %add.ptr8 = getelementptr inbounds float, float* %call7, i32 %3
  %4 = bitcast float* %add.ptr8 to i8*
  call void @_ZNKSt3__26vectorIfNS_9allocatorIfEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector"* %this1, i8* %0, i8* %1, i8* %2, i8* %4) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIfNS_9allocatorIfEEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float** @_ZNSt3__24moveIRPfEEONS_16remove_referenceIT_E4typeEOS4_(float** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca float**, align 4
  store float** %__t, float*** %__t.addr, align 4
  %0 = load float**, float*** %__t.addr, align 4
  ret float** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIfRNS_9allocatorIfEEE5clearEv(%"struct.std::__2::__split_buffer.58"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.58"*, align 4
  store %"struct.std::__2::__split_buffer.58"* %this, %"struct.std::__2::__split_buffer.58"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.58"*, %"struct.std::__2::__split_buffer.58"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"struct.std::__2::__split_buffer.58", %"struct.std::__2::__split_buffer.58"* %this1, i32 0, i32 1
  %0 = load float*, float** %__begin_, align 4
  call void @_ZNSt3__214__split_bufferIfRNS_9allocatorIfEEE17__destruct_at_endEPf(%"struct.std::__2::__split_buffer.58"* %this1, float* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__214__split_bufferIfRNS_9allocatorIfEEE8capacityEv(%"struct.std::__2::__split_buffer.58"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.58"*, align 4
  store %"struct.std::__2::__split_buffer.58"* %this, %"struct.std::__2::__split_buffer.58"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.58"*, %"struct.std::__2::__split_buffer.58"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) float** @_ZNKSt3__214__split_bufferIfRNS_9allocatorIfEEE9__end_capEv(%"struct.std::__2::__split_buffer.58"* %this1) #8
  %0 = load float*, float** %call, align 4
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer.58", %"struct.std::__2::__split_buffer.58"* %this1, i32 0, i32 0
  %1 = load float*, float** %__first_, align 4
  %sub.ptr.lhs.cast = ptrtoint float* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint float* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIfRNS_9allocatorIfEEE17__destruct_at_endEPf(%"struct.std::__2::__split_buffer.58"* %this, float* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.58"*, align 4
  %__new_last.addr = alloca float*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant.56", align 1
  store %"struct.std::__2::__split_buffer.58"* %this, %"struct.std::__2::__split_buffer.58"** %this.addr, align 4
  store float* %__new_last, float** %__new_last.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.58"*, %"struct.std::__2::__split_buffer.58"** %this.addr, align 4
  %0 = load float*, float** %__new_last.addr, align 4
  call void @_ZNSt3__214__split_bufferIfRNS_9allocatorIfEEE17__destruct_at_endEPfNS_17integral_constantIbLb0EEE(%"struct.std::__2::__split_buffer.58"* %this1, float* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIfRNS_9allocatorIfEEE17__destruct_at_endEPfNS_17integral_constantIbLb0EEE(%"struct.std::__2::__split_buffer.58"* %this, float* %__new_last) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant.56", align 1
  %this.addr = alloca %"struct.std::__2::__split_buffer.58"*, align 4
  %__new_last.addr = alloca float*, align 4
  store %"struct.std::__2::__split_buffer.58"* %this, %"struct.std::__2::__split_buffer.58"** %this.addr, align 4
  store float* %__new_last, float** %__new_last.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.58"*, %"struct.std::__2::__split_buffer.58"** %this.addr, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load float*, float** %__new_last.addr, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer.58", %"struct.std::__2::__split_buffer.58"* %this1, i32 0, i32 2
  %2 = load float*, float** %__end_, align 4
  %cmp = icmp ne float* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__214__split_bufferIfRNS_9allocatorIfEEE7__allocEv(%"struct.std::__2::__split_buffer.58"* %this1) #8
  %__end_2 = getelementptr inbounds %"struct.std::__2::__split_buffer.58", %"struct.std::__2::__split_buffer.58"* %this1, i32 0, i32 2
  %3 = load float*, float** %__end_2, align 4
  %incdec.ptr = getelementptr inbounds float, float* %3, i32 -1
  store float* %incdec.ptr, float** %__end_2, align 4
  %call3 = call float* @_ZNSt3__212__to_addressIfEEPT_S2_(float* %incdec.ptr) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIfEEE7destroyIfEEvRS2_PT_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call, float* %call3)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float** @_ZNKSt3__214__split_bufferIfRNS_9allocatorIfEEE9__end_capEv(%"struct.std::__2::__split_buffer.58"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.58"*, align 4
  store %"struct.std::__2::__split_buffer.58"* %this, %"struct.std::__2::__split_buffer.58"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.58"*, %"struct.std::__2::__split_buffer.58"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer.58", %"struct.std::__2::__split_buffer.58"* %this1, i32 0, i32 3
  %call = call nonnull align 4 dereferenceable(4) float** @_ZNKSt3__217__compressed_pairIPfRNS_9allocatorIfEEE5firstEv(%"class.std::__2::__compressed_pair.59"* %__end_cap_) #8
  ret float** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float** @_ZNKSt3__217__compressed_pairIPfRNS_9allocatorIfEEE5firstEv(%"class.std::__2::__compressed_pair.59"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.59"*, align 4
  store %"class.std::__2::__compressed_pair.59"* %this, %"class.std::__2::__compressed_pair.59"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.59"*, %"class.std::__2::__compressed_pair.59"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.59"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %call = call nonnull align 4 dereferenceable(4) float** @_ZNKSt3__222__compressed_pair_elemIPfLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %0) #8
  ret float** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIfNS_9allocatorIfEEE27__invalidate_iterators_pastEPf(%"class.std::__2::vector"* %this, float* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__new_last.addr = alloca float*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store float* %__new_last, float** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIfNS_9allocatorIfEEE17__annotate_shrinkEm(%"class.std::__2::vector"* %this, i32 %__old_size) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__old_size.addr = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__old_size, i32* %__old_size.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %call = call float* @_ZNKSt3__26vectorIfNS_9allocatorIfEEE4dataEv(%"class.std::__2::vector"* %this1) #8
  %0 = bitcast float* %call to i8*
  %call2 = call float* @_ZNKSt3__26vectorIfNS_9allocatorIfEEE4dataEv(%"class.std::__2::vector"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorIfNS_9allocatorIfEEE8capacityEv(%"class.std::__2::vector"* %this1) #8
  %add.ptr = getelementptr inbounds float, float* %call2, i32 %call3
  %1 = bitcast float* %add.ptr to i8*
  %call4 = call float* @_ZNKSt3__26vectorIfNS_9allocatorIfEEE4dataEv(%"class.std::__2::vector"* %this1) #8
  %2 = load i32, i32* %__old_size.addr, align 4
  %add.ptr5 = getelementptr inbounds float, float* %call4, i32 %2
  %3 = bitcast float* %add.ptr5 to i8*
  %call6 = call float* @_ZNKSt3__26vectorIfNS_9allocatorIfEEE4dataEv(%"class.std::__2::vector"* %this1) #8
  %call7 = call i32 @_ZNKSt3__26vectorIfNS_9allocatorIfEEE4sizeEv(%"class.std::__2::vector"* %this1) #8
  %add.ptr8 = getelementptr inbounds float, float* %call6, i32 %call7
  %4 = bitcast float* %add.ptr8 to i8*
  call void @_ZNKSt3__26vectorIfNS_9allocatorIfEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector"* %this1, i8* %0, i8* %1, i8* %3, i8* %4) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco22AttributeTransformData17SetParameterValueIiEEviRKT_(%"class.draco::AttributeTransformData"* %this, i32 %byte_offset, i32* nonnull align 4 dereferenceable(4) %in_data) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::AttributeTransformData"*, align 4
  %byte_offset.addr = alloca i32, align 4
  %in_data.addr = alloca i32*, align 4
  store %"class.draco::AttributeTransformData"* %this, %"class.draco::AttributeTransformData"** %this.addr, align 4
  store i32 %byte_offset, i32* %byte_offset.addr, align 4
  store i32* %in_data, i32** %in_data.addr, align 4
  %this1 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %this.addr, align 4
  %0 = load i32, i32* %byte_offset.addr, align 4
  %add = add i32 %0, 4
  %buffer_ = getelementptr inbounds %"class.draco::AttributeTransformData", %"class.draco::AttributeTransformData"* %this1, i32 0, i32 1
  %call = call i32 @_ZNK5draco10DataBuffer9data_sizeEv(%"class.draco::DataBuffer"* %buffer_)
  %cmp = icmp ugt i32 %add, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %buffer_2 = getelementptr inbounds %"class.draco::AttributeTransformData", %"class.draco::AttributeTransformData"* %this1, i32 0, i32 1
  %1 = load i32, i32* %byte_offset.addr, align 4
  %add3 = add i32 %1, 4
  %conv = zext i32 %add3 to i64
  call void @_ZN5draco10DataBuffer6ResizeEx(%"class.draco::DataBuffer"* %buffer_2, i64 %conv)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %buffer_4 = getelementptr inbounds %"class.draco::AttributeTransformData", %"class.draco::AttributeTransformData"* %this1, i32 0, i32 1
  %2 = load i32, i32* %byte_offset.addr, align 4
  %conv5 = sext i32 %2 to i64
  %3 = load i32*, i32** %in_data.addr, align 4
  %4 = bitcast i32* %3 to i8*
  call void @_ZN5draco10DataBuffer5WriteExPKvm(%"class.draco::DataBuffer"* %buffer_4, i64 %conv5, i8* %4, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco10DataBuffer9data_sizeEv(%"class.draco::DataBuffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::DataBuffer"*, align 4
  store %"class.draco::DataBuffer"* %this, %"class.draco::DataBuffer"** %this.addr, align 4
  %this1 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %this.addr, align 4
  %data_ = getelementptr inbounds %"class.draco::DataBuffer", %"class.draco::DataBuffer"* %this1, i32 0, i32 0
  %call = call i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4sizeEv(%"class.std::__2::vector.1"* %data_) #8
  ret i32 %call
}

declare void @_ZN5draco10DataBuffer6ResizeEx(%"class.draco::DataBuffer"*, i64) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco10DataBuffer5WriteExPKvm(%"class.draco::DataBuffer"* %this, i64 %byte_pos, i8* %in_data, i32 %data_size) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::DataBuffer"*, align 4
  %byte_pos.addr = alloca i64, align 8
  %in_data.addr = alloca i8*, align 4
  %data_size.addr = alloca i32, align 4
  store %"class.draco::DataBuffer"* %this, %"class.draco::DataBuffer"** %this.addr, align 4
  store i64 %byte_pos, i64* %byte_pos.addr, align 8
  store i8* %in_data, i8** %in_data.addr, align 4
  store i32 %data_size, i32* %data_size.addr, align 4
  %this1 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %this.addr, align 4
  %call = call i8* @_ZN5draco10DataBuffer4dataEv(%"class.draco::DataBuffer"* %this1)
  %0 = load i64, i64* %byte_pos.addr, align 8
  %idx.ext = trunc i64 %0 to i32
  %add.ptr = getelementptr inbounds i8, i8* %call, i32 %idx.ext
  %1 = load i8*, i8** %in_data.addr, align 4
  %2 = load i32, i32* %data_size.addr, align 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %add.ptr, i8* align 1 %1, i32 %2, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4sizeEv(%"class.std::__2::vector.1"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.1"*, align 4
  store %"class.std::__2::vector.1"* %this, %"class.std::__2::vector.1"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base.2"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %0, i32 0, i32 1
  %1 = load i8*, i8** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base.2"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %2, i32 0, i32 0
  %3 = load i8*, i8** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint i8* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  ret i32 %sub.ptr.sub
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco22AttributeTransformData17SetParameterValueIfEEviRKT_(%"class.draco::AttributeTransformData"* %this, i32 %byte_offset, float* nonnull align 4 dereferenceable(4) %in_data) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::AttributeTransformData"*, align 4
  %byte_offset.addr = alloca i32, align 4
  %in_data.addr = alloca float*, align 4
  store %"class.draco::AttributeTransformData"* %this, %"class.draco::AttributeTransformData"** %this.addr, align 4
  store i32 %byte_offset, i32* %byte_offset.addr, align 4
  store float* %in_data, float** %in_data.addr, align 4
  %this1 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %this.addr, align 4
  %0 = load i32, i32* %byte_offset.addr, align 4
  %add = add i32 %0, 4
  %buffer_ = getelementptr inbounds %"class.draco::AttributeTransformData", %"class.draco::AttributeTransformData"* %this1, i32 0, i32 1
  %call = call i32 @_ZNK5draco10DataBuffer9data_sizeEv(%"class.draco::DataBuffer"* %buffer_)
  %cmp = icmp ugt i32 %add, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %buffer_2 = getelementptr inbounds %"class.draco::AttributeTransformData", %"class.draco::AttributeTransformData"* %this1, i32 0, i32 1
  %1 = load i32, i32* %byte_offset.addr, align 4
  %add3 = add i32 %1, 4
  %conv = zext i32 %add3 to i64
  call void @_ZN5draco10DataBuffer6ResizeEx(%"class.draco::DataBuffer"* %buffer_2, i64 %conv)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %buffer_4 = getelementptr inbounds %"class.draco::AttributeTransformData", %"class.draco::AttributeTransformData"* %this1, i32 0, i32 1
  %2 = load i32, i32* %byte_offset.addr, align 4
  %conv5 = sext i32 %2 to i64
  %3 = load float*, float** %in_data.addr, align 4
  %4 = bitcast float* %3 to i8*
  call void @_ZN5draco10DataBuffer5WriteExPKvm(%"class.draco::DataBuffer"* %buffer_4, i64 %conv5, i8* %4, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__28distanceIPKfEENS_15iterator_traitsIT_E15difference_typeES4_S4_(float* %__first, float* %__last) #0 comdat {
entry:
  %__first.addr = alloca float*, align 4
  %__last.addr = alloca float*, align 4
  %agg.tmp = alloca %"struct.std::__2::random_access_iterator_tag", align 1
  store float* %__first, float** %__first.addr, align 4
  store float* %__last, float** %__last.addr, align 4
  %0 = load float*, float** %__first.addr, align 4
  %1 = load float*, float** %__last.addr, align 4
  %call = call i32 @_ZNSt3__210__distanceIPKfEENS_15iterator_traitsIT_E15difference_typeES4_S4_NS_26random_access_iterator_tagE(float* %0, float* %1)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__27advanceIPKfEEvRT_NS_15iterator_traitsIS3_E15difference_typeE(float** nonnull align 4 dereferenceable(4) %__i, i32 %__n) #0 comdat {
entry:
  %__i.addr = alloca float**, align 4
  %__n.addr = alloca i32, align 4
  %agg.tmp = alloca %"struct.std::__2::random_access_iterator_tag", align 1
  store float** %__i, float*** %__i.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load float**, float*** %__i.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29__advanceIPKfEEvRT_NS_15iterator_traitsIS3_E15difference_typeENS_26random_access_iterator_tagE(float** nonnull align 4 dereferenceable(4) %0, i32 %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZNSt3__24copyIPKfPfEET0_T_S5_S4_(float* %__first, float* %__last, float* %__result) #0 comdat {
entry:
  %__first.addr = alloca float*, align 4
  %__last.addr = alloca float*, align 4
  %__result.addr = alloca float*, align 4
  store float* %__first, float** %__first.addr, align 4
  store float* %__last, float** %__last.addr, align 4
  store float* %__result, float** %__result.addr, align 4
  %0 = load float*, float** %__first.addr, align 4
  %call = call float* @_ZNSt3__213__unwrap_iterIPKfEET_S3_(float* %0)
  %1 = load float*, float** %__last.addr, align 4
  %call1 = call float* @_ZNSt3__213__unwrap_iterIPKfEET_S3_(float* %1)
  %2 = load float*, float** %__result.addr, align 4
  %call2 = call float* @_ZNSt3__213__unwrap_iterIPfEET_S2_(float* %2)
  %call3 = call float* @_ZNSt3__26__copyIKffEENS_9enable_ifIXaasr7is_sameINS_12remove_constIT_E4typeET0_EE5valuesr28is_trivially_copy_assignableIS7_EE5valueEPS7_E4typeEPS4_SB_S8_(float* %call, float* %call1, float* %call2)
  ret float* %call3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIfNS_9allocatorIfEEE18__construct_at_endIPKfEENS_9enable_ifIXsr27__is_cpp17_forward_iteratorIT_EE5valueEvE4typeES8_S8_m(%"class.std::__2::vector"* %this, float* %__first, float* %__last, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__first.addr = alloca float*, align 4
  %__last.addr = alloca float*, align 4
  %__n.addr = alloca i32, align 4
  %__tx = alloca %"struct.std::__2::vector<float, std::__2::allocator<float>>::_ConstructTransaction", align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store float* %__first, float** %__first.addr, align 4
  store float* %__last, float** %__last.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %call = call %"struct.std::__2::vector<float, std::__2::allocator<float>>::_ConstructTransaction"* @_ZNSt3__26vectorIfNS_9allocatorIfEEE21_ConstructTransactionC2ERS3_m(%"struct.std::__2::vector<float, std::__2::allocator<float>>::_ConstructTransaction"* %__tx, %"class.std::__2::vector"* nonnull align 4 dereferenceable(12) %this1, i32 %0)
  %1 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__213__vector_baseIfNS_9allocatorIfEEE7__allocEv(%"class.std::__2::__vector_base"* %1) #8
  %2 = load float*, float** %__first.addr, align 4
  %3 = load float*, float** %__last.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<float, std::__2::allocator<float>>::_ConstructTransaction", %"struct.std::__2::vector<float, std::__2::allocator<float>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIfEEE25__construct_range_forwardIKffffEENS_9enable_ifIXaaaasr31is_trivially_move_constructibleIT0_EE5valuesr7is_sameIT1_T2_EE5valueooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS2_PS7_RT_EE5valueEvE4typeERS2_PSD_SI_RSC_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call2, float* %2, float* %3, float** nonnull align 4 dereferenceable(4) %__pos_)
  %call3 = call %"struct.std::__2::vector<float, std::__2::allocator<float>>::_ConstructTransaction"* @_ZNSt3__26vectorIfNS_9allocatorIfEEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<float, std::__2::allocator<float>>::_ConstructTransaction"* %__tx) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIfNS_9allocatorIfEEE13__vdeallocateEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 0
  %1 = load float*, float** %__begin_, align 4
  %cmp = icmp ne float* %1, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__26vectorIfNS_9allocatorIfEEE5clearEv(%"class.std::__2::vector"* %this1) #8
  %2 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__213__vector_baseIfNS_9allocatorIfEEE7__allocEv(%"class.std::__2::__vector_base"* %2) #8
  %3 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_2 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %3, i32 0, i32 0
  %4 = load float*, float** %__begin_2, align 4
  %call3 = call i32 @_ZNKSt3__26vectorIfNS_9allocatorIfEEE8capacityEv(%"class.std::__2::vector"* %this1) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIfEEE10deallocateERS2_Pfm(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call, float* %4, i32 %call3) #8
  %5 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call4 = call nonnull align 4 dereferenceable(4) float** @_ZNSt3__213__vector_baseIfNS_9allocatorIfEEE9__end_capEv(%"class.std::__2::__vector_base"* %5) #8
  store float* null, float** %call4, align 4
  %6 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %6, i32 0, i32 1
  store float* null, float** %__end_, align 4
  %7 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_5 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %7, i32 0, i32 0
  store float* null, float** %__begin_5, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIfNS_9allocatorIfEEE11__vallocateEm(%"class.std::__2::vector"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIfNS_9allocatorIfEEE8max_sizeEv(%"class.std::__2::vector"* %this1) #8
  %cmp = icmp ugt i32 %0, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base_common"*
  call void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"* %1) #11
  unreachable

if.end:                                           ; preds = %entry
  %2 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__213__vector_baseIfNS_9allocatorIfEEE7__allocEv(%"class.std::__2::__vector_base"* %2) #8
  %3 = load i32, i32* %__n.addr, align 4
  %call3 = call float* @_ZNSt3__216allocator_traitsINS_9allocatorIfEEE8allocateERS2_m(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call2, i32 %3)
  %4 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %4, i32 0, i32 1
  store float* %call3, float** %__end_, align 4
  %5 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %5, i32 0, i32 0
  store float* %call3, float** %__begin_, align 4
  %6 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_4 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %6, i32 0, i32 0
  %7 = load float*, float** %__begin_4, align 4
  %8 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds float, float* %7, i32 %8
  %9 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call5 = call nonnull align 4 dereferenceable(4) float** @_ZNSt3__213__vector_baseIfNS_9allocatorIfEEE9__end_capEv(%"class.std::__2::__vector_base"* %9) #8
  store float* %add.ptr, float** %call5, align 4
  call void @_ZNKSt3__26vectorIfNS_9allocatorIfEEE14__annotate_newEm(%"class.std::__2::vector"* %this1, i32 0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__210__distanceIPKfEENS_15iterator_traitsIT_E15difference_typeES4_S4_NS_26random_access_iterator_tagE(float* %__first, float* %__last) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::random_access_iterator_tag", align 1
  %__first.addr = alloca float*, align 4
  %__last.addr = alloca float*, align 4
  store float* %__first, float** %__first.addr, align 4
  store float* %__last, float** %__last.addr, align 4
  %1 = load float*, float** %__last.addr, align 4
  %2 = load float*, float** %__first.addr, align 4
  %sub.ptr.lhs.cast = ptrtoint float* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint float* %2 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29__advanceIPKfEEvRT_NS_15iterator_traitsIS3_E15difference_typeENS_26random_access_iterator_tagE(float** nonnull align 4 dereferenceable(4) %__i, i32 %__n) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::random_access_iterator_tag", align 1
  %__i.addr = alloca float**, align 4
  %__n.addr = alloca i32, align 4
  store float** %__i, float*** %__i.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %2 = load float**, float*** %__i.addr, align 4
  %3 = load float*, float** %2, align 4
  %add.ptr = getelementptr inbounds float, float* %3, i32 %1
  store float* %add.ptr, float** %2, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZNSt3__26__copyIKffEENS_9enable_ifIXaasr7is_sameINS_12remove_constIT_E4typeET0_EE5valuesr28is_trivially_copy_assignableIS7_EE5valueEPS7_E4typeEPS4_SB_S8_(float* %__first, float* %__last, float* %__result) #0 comdat {
entry:
  %__first.addr = alloca float*, align 4
  %__last.addr = alloca float*, align 4
  %__result.addr = alloca float*, align 4
  %__n = alloca i32, align 4
  store float* %__first, float** %__first.addr, align 4
  store float* %__last, float** %__last.addr, align 4
  store float* %__result, float** %__result.addr, align 4
  %0 = load float*, float** %__last.addr, align 4
  %1 = load float*, float** %__first.addr, align 4
  %sub.ptr.lhs.cast = ptrtoint float* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint float* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  store i32 %sub.ptr.div, i32* %__n, align 4
  %2 = load i32, i32* %__n, align 4
  %cmp = icmp ugt i32 %2, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %3 = load float*, float** %__result.addr, align 4
  %4 = bitcast float* %3 to i8*
  %5 = load float*, float** %__first.addr, align 4
  %6 = bitcast float* %5 to i8*
  %7 = load i32, i32* %__n, align 4
  %mul = mul i32 %7, 4
  call void @llvm.memmove.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %6, i32 %mul, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %8 = load float*, float** %__result.addr, align 4
  %9 = load i32, i32* %__n, align 4
  %add.ptr = getelementptr inbounds float, float* %8, i32 %9
  ret float* %add.ptr
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZNSt3__213__unwrap_iterIPKfEET_S3_(float* %__i) #0 comdat {
entry:
  %__i.addr = alloca float*, align 4
  store float* %__i, float** %__i.addr, align 4
  %0 = load float*, float** %__i.addr, align 4
  ret float* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZNSt3__213__unwrap_iterIPfEET_S2_(float* %__i) #0 comdat {
entry:
  %__i.addr = alloca float*, align 4
  store float* %__i, float** %__i.addr, align 4
  %0 = load float*, float** %__i.addr, align 4
  ret float* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIfEEE25__construct_range_forwardIKffffEENS_9enable_ifIXaaaasr31is_trivially_move_constructibleIT0_EE5valuesr7is_sameIT1_T2_EE5valueooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS2_PS7_RT_EE5valueEvE4typeERS2_PSD_SI_RSC_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %0, float* %__begin1, float* %__end1, float** nonnull align 4 dereferenceable(4) %__begin2) #0 comdat {
entry:
  %.addr = alloca %"class.std::__2::allocator"*, align 4
  %__begin1.addr = alloca float*, align 4
  %__end1.addr = alloca float*, align 4
  %__begin2.addr = alloca float**, align 4
  %_Np = alloca i32, align 4
  store %"class.std::__2::allocator"* %0, %"class.std::__2::allocator"** %.addr, align 4
  store float* %__begin1, float** %__begin1.addr, align 4
  store float* %__end1, float** %__end1.addr, align 4
  store float** %__begin2, float*** %__begin2.addr, align 4
  %1 = load float*, float** %__end1.addr, align 4
  %2 = load float*, float** %__begin1.addr, align 4
  %sub.ptr.lhs.cast = ptrtoint float* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint float* %2 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  store i32 %sub.ptr.div, i32* %_Np, align 4
  %3 = load i32, i32* %_Np, align 4
  %cmp = icmp sgt i32 %3, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = load float**, float*** %__begin2.addr, align 4
  %5 = load float*, float** %4, align 4
  %6 = bitcast float* %5 to i8*
  %7 = load float*, float** %__begin1.addr, align 4
  %8 = bitcast float* %7 to i8*
  %9 = load i32, i32* %_Np, align 4
  %mul = mul i32 %9, 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %8, i32 %mul, i1 false)
  %10 = load i32, i32* %_Np, align 4
  %11 = load float**, float*** %__begin2.addr, align 4
  %12 = load float*, float** %11, align 4
  %add.ptr = getelementptr inbounds float, float* %12, i32 %10
  store float* %add.ptr, float** %11, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIfNS_9allocatorIfEEE5clearEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__old_size = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIfNS_9allocatorIfEEE4sizeEv(%"class.std::__2::vector"* %this1) #8
  store i32 %call, i32* %__old_size, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  call void @_ZNSt3__213__vector_baseIfNS_9allocatorIfEEE5clearEv(%"class.std::__2::__vector_base"* %0) #8
  %1 = load i32, i32* %__old_size, align 4
  call void @_ZNKSt3__26vectorIfNS_9allocatorIfEEE17__annotate_shrinkEm(%"class.std::__2::vector"* %this1, i32 %1) #8
  call void @_ZNSt3__26vectorIfNS_9allocatorIfEEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector"* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base"* @_ZNSt3__213__vector_baseIfNS_9allocatorIfEEEC2Ev(%"class.std::__2::__vector_base"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  %ref.tmp = alloca i8*, align 4
  %ref.tmp2 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__vector_base"* %this1 to %"class.std::__2::__vector_base_common"*
  %call = call %"class.std::__2::__vector_base_common"* @_ZNSt3__220__vector_base_commonILb1EEC2Ev(%"class.std::__2::__vector_base_common"* %0)
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  store float* null, float** %__begin_, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 1
  store float* null, float** %__end_, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  store i8* null, i8** %ref.tmp, align 4
  %call3 = call %"class.std::__2::__compressed_pair"* @_ZNSt3__217__compressed_pairIPfNS_9allocatorIfEEEC2IDnNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair"* %__end_cap_, i8** nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  ret %"class.std::__2::__vector_base"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIfNS_9allocatorIfEEE18__construct_at_endEmRKf(%"class.std::__2::vector"* %this, i32 %__n, float* nonnull align 4 dereferenceable(4) %__x) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__n.addr = alloca i32, align 4
  %__x.addr = alloca float*, align 4
  %__tx = alloca %"struct.std::__2::vector<float, std::__2::allocator<float>>::_ConstructTransaction", align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store float* %__x, float** %__x.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %call = call %"struct.std::__2::vector<float, std::__2::allocator<float>>::_ConstructTransaction"* @_ZNSt3__26vectorIfNS_9allocatorIfEEE21_ConstructTransactionC2ERS3_m(%"struct.std::__2::vector<float, std::__2::allocator<float>>::_ConstructTransaction"* %__tx, %"class.std::__2::vector"* nonnull align 4 dereferenceable(12) %this1, i32 %0)
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<float, std::__2::allocator<float>>::_ConstructTransaction", %"struct.std::__2::vector<float, std::__2::allocator<float>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %1 = load float*, float** %__pos_, align 4
  %__new_end_ = getelementptr inbounds %"struct.std::__2::vector<float, std::__2::allocator<float>>::_ConstructTransaction", %"struct.std::__2::vector<float, std::__2::allocator<float>>::_ConstructTransaction"* %__tx, i32 0, i32 2
  %2 = load float*, float** %__new_end_, align 4
  %cmp = icmp ne float* %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__213__vector_baseIfNS_9allocatorIfEEE7__allocEv(%"class.std::__2::__vector_base"* %3) #8
  %__pos_3 = getelementptr inbounds %"struct.std::__2::vector<float, std::__2::allocator<float>>::_ConstructTransaction", %"struct.std::__2::vector<float, std::__2::allocator<float>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %4 = load float*, float** %__pos_3, align 4
  %call4 = call float* @_ZNSt3__212__to_addressIfEEPT_S2_(float* %4) #8
  %5 = load float*, float** %__x.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIfEEE9constructIfJRKfEEEvRS2_PT_DpOT0_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call2, float* %call4, float* nonnull align 4 dereferenceable(4) %5)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %__pos_5 = getelementptr inbounds %"struct.std::__2::vector<float, std::__2::allocator<float>>::_ConstructTransaction", %"struct.std::__2::vector<float, std::__2::allocator<float>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %6 = load float*, float** %__pos_5, align 4
  %incdec.ptr = getelementptr inbounds float, float* %6, i32 1
  store float* %incdec.ptr, float** %__pos_5, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call6 = call %"struct.std::__2::vector<float, std::__2::allocator<float>>::_ConstructTransaction"* @_ZNSt3__26vectorIfNS_9allocatorIfEEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<float, std::__2::allocator<float>>::_ConstructTransaction"* %__tx) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base_common"* @_ZNSt3__220__vector_base_commonILb1EEC2Ev(%"class.std::__2::__vector_base_common"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base_common"*, align 4
  store %"class.std::__2::__vector_base_common"* %this, %"class.std::__2::__vector_base_common"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base_common"*, %"class.std::__2::__vector_base_common"** %this.addr, align 4
  ret %"class.std::__2::__vector_base_common"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair"* @_ZNSt3__217__compressed_pairIPfNS_9allocatorIfEEEC2IDnNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair"* returned %this, i8** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  %__t1.addr = alloca i8**, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  store i8** %__t1, i8*** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %1 = load i8**, i8*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %1) #8
  %call2 = call %"struct.std::__2::__compressed_pair_elem"* @_ZNSt3__222__compressed_pair_elemIPfLi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem"* %0, i8** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem.0"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #8
  %call4 = call %"struct.std::__2::__compressed_pair_elem.0"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIfEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.0"* %2)
  ret %"class.std::__2::__compressed_pair"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  store %"struct.std::__2::__default_init_tag"* %__t, %"struct.std::__2::__default_init_tag"** %__t.addr, align 4
  %0 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t.addr, align 4
  ret %"struct.std::__2::__default_init_tag"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.0"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIfEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.0"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.0"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.0"* %this, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.0"*, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.0"* %this1 to %"class.std::__2::allocator"*
  %call = call %"class.std::__2::allocator"* @_ZNSt3__29allocatorIfEC2Ev(%"class.std::__2::allocator"* %1) #8
  ret %"struct.std::__2::__compressed_pair_elem.0"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::allocator"* @_ZNSt3__29allocatorIfEC2Ev(%"class.std::__2::allocator"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  ret %"class.std::__2::allocator"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIfEEE9constructIfJRKfEEEvRS2_PT_DpOT0_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, float* %__p, float* nonnull align 4 dereferenceable(4) %__args) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca float*, align 4
  %__args.addr = alloca float*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_construct.63", align 1
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store float* %__p, float** %__p.addr, align 4
  store float* %__args, float** %__args.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_construct.63"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %2 = load float*, float** %__p.addr, align 4
  %3 = load float*, float** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNSt3__27forwardIRKfEEOT_RNS_16remove_referenceIS3_E4typeE(float* nonnull align 4 dereferenceable(4) %3) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIfEEE11__constructIfJRKfEEEvNS_17integral_constantIbLb1EEERS2_PT_DpOT0_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %1, float* %2, float* nonnull align 4 dereferenceable(4) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIfEEE11__constructIfJRKfEEEvNS_17integral_constantIbLb1EEERS2_PT_DpOT0_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, float* %__p, float* nonnull align 4 dereferenceable(4) %__args) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca float*, align 4
  %__args.addr = alloca float*, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store float* %__p, float** %__p.addr, align 4
  store float* %__args, float** %__args.addr, align 4
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %2 = load float*, float** %__p.addr, align 4
  %3 = load float*, float** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNSt3__27forwardIRKfEEOT_RNS_16remove_referenceIS3_E4typeE(float* nonnull align 4 dereferenceable(4) %3) #8
  call void @_ZNSt3__29allocatorIfE9constructIfJRKfEEEvPT_DpOT0_(%"class.std::__2::allocator"* %1, float* %2, float* nonnull align 4 dereferenceable(4) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNSt3__27forwardIRKfEEOT_RNS_16remove_referenceIS3_E4typeE(float* nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca float*, align 4
  store float* %__t, float** %__t.addr, align 4
  %0 = load float*, float** %__t.addr, align 4
  ret float* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIfE9constructIfJRKfEEEvPT_DpOT0_(%"class.std::__2::allocator"* %this, float* %__p, float* nonnull align 4 dereferenceable(4) %__args) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca float*, align 4
  %__args.addr = alloca float*, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  store float* %__p, float** %__p.addr, align 4
  store float* %__args, float** %__args.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  %0 = load float*, float** %__p.addr, align 4
  %1 = bitcast float* %0 to i8*
  %2 = bitcast i8* %1 to float*
  %3 = load float*, float** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNSt3__27forwardIRKfEEOT_RNS_16remove_referenceIS3_E4typeE(float* nonnull align 4 dereferenceable(4) %3) #8
  %4 = load float, float* %call, align 4
  store float %4, float* %2, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIfNS_9allocatorIfEEE13__move_assignERS3_NS_17integral_constantIbLb1EEE(%"class.std::__2::vector"* %this, %"class.std::__2::vector"* nonnull align 4 dereferenceable(12) %__c) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__c.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store %"class.std::__2::vector"* %__c, %"class.std::__2::vector"** %__c.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  call void @_ZNSt3__26vectorIfNS_9allocatorIfEEE13__vdeallocateEv(%"class.std::__2::vector"* %this1) #8
  %1 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %2 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__c.addr, align 4
  %3 = bitcast %"class.std::__2::vector"* %2 to %"class.std::__2::__vector_base"*
  call void @_ZNSt3__213__vector_baseIfNS_9allocatorIfEEE19__move_assign_allocERS3_(%"class.std::__2::__vector_base"* %1, %"class.std::__2::__vector_base"* nonnull align 4 dereferenceable(12) %3) #8
  %4 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__c.addr, align 4
  %5 = bitcast %"class.std::__2::vector"* %4 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %5, i32 0, i32 0
  %6 = load float*, float** %__begin_, align 4
  %7 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_2 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %7, i32 0, i32 0
  store float* %6, float** %__begin_2, align 4
  %8 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__c.addr, align 4
  %9 = bitcast %"class.std::__2::vector"* %8 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %9, i32 0, i32 1
  %10 = load float*, float** %__end_, align 4
  %11 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__end_3 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %11, i32 0, i32 1
  store float* %10, float** %__end_3, align 4
  %12 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__c.addr, align 4
  %13 = bitcast %"class.std::__2::vector"* %12 to %"class.std::__2::__vector_base"*
  %call = call nonnull align 4 dereferenceable(4) float** @_ZNSt3__213__vector_baseIfNS_9allocatorIfEEE9__end_capEv(%"class.std::__2::__vector_base"* %13) #8
  %14 = load float*, float** %call, align 4
  %15 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call4 = call nonnull align 4 dereferenceable(4) float** @_ZNSt3__213__vector_baseIfNS_9allocatorIfEEE9__end_capEv(%"class.std::__2::__vector_base"* %15) #8
  store float* %14, float** %call4, align 4
  %16 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__c.addr, align 4
  %17 = bitcast %"class.std::__2::vector"* %16 to %"class.std::__2::__vector_base"*
  %call5 = call nonnull align 4 dereferenceable(4) float** @_ZNSt3__213__vector_baseIfNS_9allocatorIfEEE9__end_capEv(%"class.std::__2::__vector_base"* %17) #8
  store float* null, float** %call5, align 4
  %18 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__c.addr, align 4
  %19 = bitcast %"class.std::__2::vector"* %18 to %"class.std::__2::__vector_base"*
  %__end_6 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %19, i32 0, i32 1
  store float* null, float** %__end_6, align 4
  %20 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__c.addr, align 4
  %21 = bitcast %"class.std::__2::vector"* %20 to %"class.std::__2::__vector_base"*
  %__begin_7 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %21, i32 0, i32 0
  store float* null, float** %__begin_7, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIfNS_9allocatorIfEEE19__move_assign_allocERS3_(%"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"* nonnull align 4 dereferenceable(12) %__c) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  %__c.addr = alloca %"class.std::__2::__vector_base"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  store %"class.std::__2::__vector_base"* %__c, %"class.std::__2::__vector_base"** %__c.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %0 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %__c.addr, align 4
  call void @_ZNSt3__213__vector_baseIfNS_9allocatorIfEEE19__move_assign_allocERS3_NS_17integral_constantIbLb1EEE(%"class.std::__2::__vector_base"* %this1, %"class.std::__2::__vector_base"* nonnull align 4 dereferenceable(12) %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIfNS_9allocatorIfEEE19__move_assign_allocERS3_NS_17integral_constantIbLb1EEE(%"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"* nonnull align 4 dereferenceable(12) %__c) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  %__c.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  store %"class.std::__2::__vector_base"* %__c, %"class.std::__2::__vector_base"** %__c.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %__c.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__213__vector_baseIfNS_9allocatorIfEEE7__allocEv(%"class.std::__2::__vector_base"* %1) #8
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__24moveIRNS_9allocatorIfEEEEONS_16remove_referenceIT_E4typeEOS5_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call) #8
  %call3 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__213__vector_baseIfNS_9allocatorIfEEE7__allocEv(%"class.std::__2::__vector_base"* %this1) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__24moveIRNS_9allocatorIfEEEEONS_16remove_referenceIT_E4typeEOS5_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"class.std::__2::allocator"* %__t, %"class.std::__2::allocator"** %__t.addr, align 4
  %0 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__t.addr, align 4
  ret %"class.std::__2::allocator"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.24"* @_ZNSt3__217__compressed_pairIPfNS_14default_deleteIA_fEEEC2IRS1_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.24"* returned %this, float** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.24"*, align 4
  %__t1.addr = alloca float**, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.24"* %this, %"class.std::__2::__compressed_pair.24"** %this.addr, align 4
  store float** %__t1, float*** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.24"*, %"class.std::__2::__compressed_pair.24"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.24"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %1 = load float**, float*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) float** @_ZNSt3__27forwardIRPfEEOT_RNS_16remove_referenceIS3_E4typeE(float** nonnull align 4 dereferenceable(4) %1) #8
  %call2 = call %"struct.std::__2::__compressed_pair_elem"* @_ZNSt3__222__compressed_pair_elemIPfLi0ELb0EEC2IRS1_vEEOT_(%"struct.std::__2::__compressed_pair_elem"* %0, float** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.24"* %this1 to %"struct.std::__2::__compressed_pair_elem.25"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #8
  %call4 = call %"struct.std::__2::__compressed_pair_elem.25"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIA_fEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.25"* %2)
  ret %"class.std::__2::__compressed_pair.24"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float** @_ZNSt3__27forwardIRPfEEOT_RNS_16remove_referenceIS3_E4typeE(float** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca float**, align 4
  store float** %__t, float*** %__t.addr, align 4
  %0 = load float**, float*** %__t.addr, align 4
  ret float** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem"* @_ZNSt3__222__compressed_pair_elemIPfLi0ELb0EEC2IRS1_vEEOT_(%"struct.std::__2::__compressed_pair_elem"* returned %this, float** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem"*, align 4
  %__u.addr = alloca float**, align 4
  store %"struct.std::__2::__compressed_pair_elem"* %this, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  store float** %__u, float*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem"*, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem"* %this1, i32 0, i32 0
  %0 = load float**, float*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) float** @_ZNSt3__27forwardIRPfEEOT_RNS_16remove_referenceIS3_E4typeE(float** nonnull align 4 dereferenceable(4) %0) #8
  %1 = load float*, float** %call, align 4
  store float* %1, float** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.25"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIA_fEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.25"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.25"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.25"* %this, %"struct.std::__2::__compressed_pair_elem.25"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.25"*, %"struct.std::__2::__compressed_pair_elem.25"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.25"* %this1 to %"struct.std::__2::default_delete.26"*
  ret %"struct.std::__2::__compressed_pair_elem.25"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__210unique_ptrIA_fNS_14default_deleteIS1_EEE5resetEDn(%"class.std::__2::unique_ptr.23"* %this, i8* %0) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.23"*, align 4
  %.addr = alloca i8*, align 4
  %__tmp = alloca float*, align 4
  store %"class.std::__2::unique_ptr.23"* %this, %"class.std::__2::unique_ptr.23"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.23"*, %"class.std::__2::unique_ptr.23"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.23", %"class.std::__2::unique_ptr.23"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float** @_ZNSt3__217__compressed_pairIPfNS_14default_deleteIA_fEEE5firstEv(%"class.std::__2::__compressed_pair.24"* %__ptr_) #8
  %1 = load float*, float** %call, align 4
  store float* %1, float** %__tmp, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr.23", %"class.std::__2::unique_ptr.23"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) float** @_ZNSt3__217__compressed_pairIPfNS_14default_deleteIA_fEEE5firstEv(%"class.std::__2::__compressed_pair.24"* %__ptr_2) #8
  store float* null, float** %call3, align 4
  %2 = load float*, float** %__tmp, align 4
  %tobool = icmp ne float* %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__ptr_4 = getelementptr inbounds %"class.std::__2::unique_ptr.23", %"class.std::__2::unique_ptr.23"* %this1, i32 0, i32 0
  %call5 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.26"* @_ZNSt3__217__compressed_pairIPfNS_14default_deleteIA_fEEE6secondEv(%"class.std::__2::__compressed_pair.24"* %__ptr_4) #8
  %3 = load float*, float** %__tmp, align 4
  call void @_ZNKSt3__214default_deleteIA_fEclIfEENS2_20_EnableIfConvertibleIT_E4typeEPS5_(%"struct.std::__2::default_delete.26"* %call5, float* %3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float** @_ZNSt3__217__compressed_pairIPfNS_14default_deleteIA_fEEE5firstEv(%"class.std::__2::__compressed_pair.24"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.24"*, align 4
  store %"class.std::__2::__compressed_pair.24"* %this, %"class.std::__2::__compressed_pair.24"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.24"*, %"class.std::__2::__compressed_pair.24"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.24"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %call = call nonnull align 4 dereferenceable(4) float** @_ZNSt3__222__compressed_pair_elemIPfLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %0) #8
  ret float** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.26"* @_ZNSt3__217__compressed_pairIPfNS_14default_deleteIA_fEEE6secondEv(%"class.std::__2::__compressed_pair.24"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.24"*, align 4
  store %"class.std::__2::__compressed_pair.24"* %this, %"class.std::__2::__compressed_pair.24"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.24"*, %"class.std::__2::__compressed_pair.24"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.24"* %this1 to %"struct.std::__2::__compressed_pair_elem.25"*
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.26"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIA_fEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.25"* %0) #8
  ret %"struct.std::__2::default_delete.26"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__214default_deleteIA_fEclIfEENS2_20_EnableIfConvertibleIT_E4typeEPS5_(%"struct.std::__2::default_delete.26"* %this, float* %__ptr) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::default_delete.26"*, align 4
  %__ptr.addr = alloca float*, align 4
  store %"struct.std::__2::default_delete.26"* %this, %"struct.std::__2::default_delete.26"** %this.addr, align 4
  store float* %__ptr, float** %__ptr.addr, align 4
  %this1 = load %"struct.std::__2::default_delete.26"*, %"struct.std::__2::default_delete.26"** %this.addr, align 4
  %0 = load float*, float** %__ptr.addr, align 4
  %isnull = icmp eq float* %0, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %entry
  %1 = bitcast float* %0 to i8*
  call void @_ZdaPv(i8* %1) #10
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.26"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIA_fEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.25"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.25"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.25"* %this, %"struct.std::__2::__compressed_pair_elem.25"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.25"*, %"struct.std::__2::__compressed_pair_elem.25"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.25"* %this1 to %"struct.std::__2::default_delete.26"*
  ret %"struct.std::__2::default_delete.26"* %0
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdaPv(i8*) #7

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float** @_ZNKSt3__217__compressed_pairIPfNS_14default_deleteIA_fEEE5firstEv(%"class.std::__2::__compressed_pair.24"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.24"*, align 4
  store %"class.std::__2::__compressed_pair.24"* %this, %"class.std::__2::__compressed_pair.24"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.24"*, %"class.std::__2::__compressed_pair.24"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.24"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %call = call nonnull align 4 dereferenceable(4) float** @_ZNKSt3__222__compressed_pair_elemIPfLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %0) #8
  ret float** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.39"* %this, %"class.draco::PointAttribute"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.39"*, align 4
  %__p.addr = alloca %"class.draco::PointAttribute"*, align 4
  %__tmp = alloca %"class.draco::PointAttribute"*, align 4
  store %"class.std::__2::unique_ptr.39"* %this, %"class.std::__2::unique_ptr.39"** %this.addr, align 4
  store %"class.draco::PointAttribute"* %__p, %"class.draco::PointAttribute"** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.39"*, %"class.std::__2::unique_ptr.39"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.39", %"class.std::__2::unique_ptr.39"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.40"* %__ptr_) #8
  %0 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %call, align 4
  store %"class.draco::PointAttribute"* %0, %"class.draco::PointAttribute"** %__tmp, align 4
  %1 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %__p.addr, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr.39", %"class.std::__2::unique_ptr.39"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.40"* %__ptr_2) #8
  store %"class.draco::PointAttribute"* %1, %"class.draco::PointAttribute"** %call3, align 4
  %2 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %__tmp, align 4
  %tobool = icmp ne %"class.draco::PointAttribute"* %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__ptr_4 = getelementptr inbounds %"class.std::__2::unique_ptr.39", %"class.std::__2::unique_ptr.39"* %this1, i32 0, i32 0
  %call5 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.43"* @_ZNSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.40"* %__ptr_4) #8
  %3 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %__tmp, align 4
  call void @_ZNKSt3__214default_deleteIN5draco14PointAttributeEEclEPS2_(%"struct.std::__2::default_delete.43"* %call5, %"class.draco::PointAttribute"* %3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.40"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.40"*, align 4
  store %"class.std::__2::__compressed_pair.40"* %this, %"class.std::__2::__compressed_pair.40"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.40"*, %"class.std::__2::__compressed_pair.40"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.40"* %this1 to %"struct.std::__2::__compressed_pair_elem.41"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNSt3__222__compressed_pair_elemIPN5draco14PointAttributeELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.41"* %0) #8
  ret %"class.draco::PointAttribute"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.43"* @_ZNSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.40"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.40"*, align 4
  store %"class.std::__2::__compressed_pair.40"* %this, %"class.std::__2::__compressed_pair.40"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.40"*, %"class.std::__2::__compressed_pair.40"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.40"* %this1 to %"struct.std::__2::__compressed_pair_elem.42"*
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.43"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco14PointAttributeEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.42"* %0) #8
  ret %"struct.std::__2::default_delete.43"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__214default_deleteIN5draco14PointAttributeEEclEPS2_(%"struct.std::__2::default_delete.43"* %this, %"class.draco::PointAttribute"* %__ptr) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::default_delete.43"*, align 4
  %__ptr.addr = alloca %"class.draco::PointAttribute"*, align 4
  store %"struct.std::__2::default_delete.43"* %this, %"struct.std::__2::default_delete.43"** %this.addr, align 4
  store %"class.draco::PointAttribute"* %__ptr, %"class.draco::PointAttribute"** %__ptr.addr, align 4
  %this1 = load %"struct.std::__2::default_delete.43"*, %"struct.std::__2::default_delete.43"** %this.addr, align 4
  %0 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %__ptr.addr, align 4
  %isnull = icmp eq %"class.draco::PointAttribute"* %0, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %entry
  %call = call %"class.draco::PointAttribute"* @_ZN5draco14PointAttributeD2Ev(%"class.draco::PointAttribute"* %0) #8
  %1 = bitcast %"class.draco::PointAttribute"* %0 to i8*
  call void @_ZdlPv(i8* %1) #10
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNSt3__222__compressed_pair_elemIPN5draco14PointAttributeELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.41"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.41"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.41"* %this, %"struct.std::__2::__compressed_pair_elem.41"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.41"*, %"struct.std::__2::__compressed_pair_elem.41"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.41", %"struct.std::__2::__compressed_pair_elem.41"* %this1, i32 0, i32 0
  ret %"class.draco::PointAttribute"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.43"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco14PointAttributeEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.42"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.42"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.42"* %this, %"struct.std::__2::__compressed_pair_elem.42"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.42"*, %"struct.std::__2::__compressed_pair_elem.42"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.42"* %this1 to %"struct.std::__2::default_delete.43"*
  ret %"struct.std::__2::default_delete.43"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::PointAttribute"* @_ZN5draco14PointAttributeD2Ev(%"class.draco::PointAttribute"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointAttribute"*, align 4
  store %"class.draco::PointAttribute"* %this, %"class.draco::PointAttribute"** %this.addr, align 4
  %this1 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %this.addr, align 4
  %attribute_transform_data_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 6
  %call = call %"class.std::__2::unique_ptr.18"* @_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.18"* %attribute_transform_data_) #8
  %indices_map_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 2
  %call2 = call %"class.draco::IndexTypeVector"* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEEED2Ev(%"class.draco::IndexTypeVector"* %indices_map_) #8
  %attribute_buffer_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 1
  %call3 = call %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr"* %attribute_buffer_) #8
  ret %"class.draco::PointAttribute"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.18"* @_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.18"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.18"*, align 4
  store %"class.std::__2::unique_ptr.18"* %this, %"class.std::__2::unique_ptr.18"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.18"*, %"class.std::__2::unique_ptr.18"** %this.addr, align 4
  call void @_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.18"* %this1, %"class.draco::AttributeTransformData"* null) #8
  ret %"class.std::__2::unique_ptr.18"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexTypeVector"* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEEED2Ev(%"class.draco::IndexTypeVector"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector"*, align 4
  store %"class.draco::IndexTypeVector"* %this, %"class.draco::IndexTypeVector"** %this.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector"*, %"class.draco::IndexTypeVector"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector", %"class.draco::IndexTypeVector"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::vector.11"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEED2Ev(%"class.std::__2::vector.11"* %vector_) #8
  ret %"class.draco::IndexTypeVector"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  call void @_ZNSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr"* %this1, %"class.draco::DataBuffer"* null) #8
  ret %"class.std::__2::unique_ptr"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.18"* %this, %"class.draco::AttributeTransformData"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.18"*, align 4
  %__p.addr = alloca %"class.draco::AttributeTransformData"*, align 4
  %__tmp = alloca %"class.draco::AttributeTransformData"*, align 4
  store %"class.std::__2::unique_ptr.18"* %this, %"class.std::__2::unique_ptr.18"** %this.addr, align 4
  store %"class.draco::AttributeTransformData"* %__p, %"class.draco::AttributeTransformData"** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.18"*, %"class.std::__2::unique_ptr.18"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.18", %"class.std::__2::unique_ptr.18"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributeTransformData"** @_ZNSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.19"* %__ptr_) #8
  %0 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %call, align 4
  store %"class.draco::AttributeTransformData"* %0, %"class.draco::AttributeTransformData"** %__tmp, align 4
  %1 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %__p.addr, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr.18", %"class.std::__2::unique_ptr.18"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::AttributeTransformData"** @_ZNSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.19"* %__ptr_2) #8
  store %"class.draco::AttributeTransformData"* %1, %"class.draco::AttributeTransformData"** %call3, align 4
  %2 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %__tmp, align 4
  %tobool = icmp ne %"class.draco::AttributeTransformData"* %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__ptr_4 = getelementptr inbounds %"class.std::__2::unique_ptr.18", %"class.std::__2::unique_ptr.18"* %this1, i32 0, i32 0
  %call5 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.22"* @_ZNSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.19"* %__ptr_4) #8
  %3 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %__tmp, align 4
  call void @_ZNKSt3__214default_deleteIN5draco22AttributeTransformDataEEclEPS2_(%"struct.std::__2::default_delete.22"* %call5, %"class.draco::AttributeTransformData"* %3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::AttributeTransformData"** @_ZNSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.19"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.19"*, align 4
  store %"class.std::__2::__compressed_pair.19"* %this, %"class.std::__2::__compressed_pair.19"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.19"*, %"class.std::__2::__compressed_pair.19"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.19"* %this1 to %"struct.std::__2::__compressed_pair_elem.20"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributeTransformData"** @_ZNSt3__222__compressed_pair_elemIPN5draco22AttributeTransformDataELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.20"* %0) #8
  ret %"class.draco::AttributeTransformData"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.22"* @_ZNSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.19"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.19"*, align 4
  store %"class.std::__2::__compressed_pair.19"* %this, %"class.std::__2::__compressed_pair.19"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.19"*, %"class.std::__2::__compressed_pair.19"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.19"* %this1 to %"struct.std::__2::__compressed_pair_elem.21"*
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.22"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco22AttributeTransformDataEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.21"* %0) #8
  ret %"struct.std::__2::default_delete.22"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__214default_deleteIN5draco22AttributeTransformDataEEclEPS2_(%"struct.std::__2::default_delete.22"* %this, %"class.draco::AttributeTransformData"* %__ptr) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::default_delete.22"*, align 4
  %__ptr.addr = alloca %"class.draco::AttributeTransformData"*, align 4
  store %"struct.std::__2::default_delete.22"* %this, %"struct.std::__2::default_delete.22"** %this.addr, align 4
  store %"class.draco::AttributeTransformData"* %__ptr, %"class.draco::AttributeTransformData"** %__ptr.addr, align 4
  %this1 = load %"struct.std::__2::default_delete.22"*, %"struct.std::__2::default_delete.22"** %this.addr, align 4
  %0 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %__ptr.addr, align 4
  %isnull = icmp eq %"class.draco::AttributeTransformData"* %0, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %entry
  %call = call %"class.draco::AttributeTransformData"* @_ZN5draco22AttributeTransformDataD2Ev(%"class.draco::AttributeTransformData"* %0) #8
  %1 = bitcast %"class.draco::AttributeTransformData"* %0 to i8*
  call void @_ZdlPv(i8* %1) #10
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::AttributeTransformData"** @_ZNSt3__222__compressed_pair_elemIPN5draco22AttributeTransformDataELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.20"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.20"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.20"* %this, %"struct.std::__2::__compressed_pair_elem.20"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.20"*, %"struct.std::__2::__compressed_pair_elem.20"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.20", %"struct.std::__2::__compressed_pair_elem.20"* %this1, i32 0, i32 0
  ret %"class.draco::AttributeTransformData"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.22"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco22AttributeTransformDataEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.21"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.21"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.21"* %this, %"struct.std::__2::__compressed_pair_elem.21"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.21"*, %"struct.std::__2::__compressed_pair_elem.21"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.21"* %this1 to %"struct.std::__2::default_delete.22"*
  ret %"struct.std::__2::default_delete.22"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::AttributeTransformData"* @_ZN5draco22AttributeTransformDataD2Ev(%"class.draco::AttributeTransformData"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::AttributeTransformData"*, align 4
  store %"class.draco::AttributeTransformData"* %this, %"class.draco::AttributeTransformData"** %this.addr, align 4
  %this1 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %this.addr, align 4
  %buffer_ = getelementptr inbounds %"class.draco::AttributeTransformData", %"class.draco::AttributeTransformData"* %this1, i32 0, i32 1
  %call = call %"class.draco::DataBuffer"* @_ZN5draco10DataBufferD2Ev(%"class.draco::DataBuffer"* %buffer_) #8
  ret %"class.draco::AttributeTransformData"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::DataBuffer"* @_ZN5draco10DataBufferD2Ev(%"class.draco::DataBuffer"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::DataBuffer"*, align 4
  store %"class.draco::DataBuffer"* %this, %"class.draco::DataBuffer"** %this.addr, align 4
  %this1 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %this.addr, align 4
  %data_ = getelementptr inbounds %"class.draco::DataBuffer", %"class.draco::DataBuffer"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::vector.1"* @_ZNSt3__26vectorIhNS_9allocatorIhEEED2Ev(%"class.std::__2::vector.1"* %data_) #8
  ret %"class.draco::DataBuffer"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector.1"* @_ZNSt3__26vectorIhNS_9allocatorIhEEED2Ev(%"class.std::__2::vector.1"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.1"*, align 4
  store %"class.std::__2::vector.1"* %this, %"class.std::__2::vector.1"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %this.addr, align 4
  call void @_ZNKSt3__26vectorIhNS_9allocatorIhEEE17__annotate_deleteEv(%"class.std::__2::vector.1"* %this1) #8
  %0 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base.2"*
  %call = call %"class.std::__2::__vector_base.2"* @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEED2Ev(%"class.std::__2::__vector_base.2"* %0) #8
  ret %"class.std::__2::vector.1"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIhNS_9allocatorIhEEE17__annotate_deleteEv(%"class.std::__2::vector.1"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.1"*, align 4
  store %"class.std::__2::vector.1"* %this, %"class.std::__2::vector.1"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %this.addr, align 4
  %call = call i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector.1"* %this1) #8
  %call2 = call i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector.1"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::vector.1"* %this1) #8
  %add.ptr = getelementptr inbounds i8, i8* %call2, i32 %call3
  %call4 = call i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector.1"* %this1) #8
  %call5 = call i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4sizeEv(%"class.std::__2::vector.1"* %this1) #8
  %add.ptr6 = getelementptr inbounds i8, i8* %call4, i32 %call5
  %call7 = call i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector.1"* %this1) #8
  %call8 = call i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::vector.1"* %this1) #8
  %add.ptr9 = getelementptr inbounds i8, i8* %call7, i32 %call8
  call void @_ZNKSt3__26vectorIhNS_9allocatorIhEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector.1"* %this1, i8* %call, i8* %add.ptr, i8* %add.ptr6, i8* %add.ptr9) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base.2"* @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEED2Ev(%"class.std::__2::__vector_base.2"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__vector_base.2"*, align 4
  %this.addr = alloca %"class.std::__2::__vector_base.2"*, align 4
  store %"class.std::__2::__vector_base.2"* %this, %"class.std::__2::__vector_base.2"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.2"*, %"class.std::__2::__vector_base.2"** %this.addr, align 4
  store %"class.std::__2::__vector_base.2"* %this1, %"class.std::__2::__vector_base.2"** %retval, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %this1, i32 0, i32 0
  %0 = load i8*, i8** %__begin_, align 4
  %cmp = icmp ne i8* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE5clearEv(%"class.std::__2::__vector_base.2"* %this1) #8
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE7__allocEv(%"class.std::__2::__vector_base.2"* %this1) #8
  %__begin_2 = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %this1, i32 0, i32 0
  %1 = load i8*, i8** %__begin_2, align 4
  %call3 = call i32 @_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::__vector_base.2"* %this1) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE10deallocateERS2_Phm(%"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %call, i8* %1, i32 %call3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"class.std::__2::__vector_base.2"*, %"class.std::__2::__vector_base.2"** %retval, align 4
  ret %"class.std::__2::__vector_base.2"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIhNS_9allocatorIhEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector.1"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.1"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector.1"* %this, %"class.std::__2::vector.1"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::vector.1"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.1"*, align 4
  store %"class.std::__2::vector.1"* %this, %"class.std::__2::vector.1"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base.2"*
  %call = call i32 @_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::__vector_base.2"* %0) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::__vector_base.2"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.2"*, align 4
  store %"class.std::__2::__vector_base.2"* %this, %"class.std::__2::__vector_base.2"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.2"*, %"class.std::__2::__vector_base.2"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE9__end_capEv(%"class.std::__2::__vector_base.2"* %this1) #8
  %0 = load i8*, i8** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %this1, i32 0, i32 0
  %1 = load i8*, i8** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint i8* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  ret i32 %sub.ptr.sub
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE9__end_capEv(%"class.std::__2::__vector_base.2"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.2"*, align 4
  store %"class.std::__2::__vector_base.2"* %this, %"class.std::__2::__vector_base.2"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.2"*, %"class.std::__2::__vector_base.2"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__217__compressed_pairIPhNS_9allocatorIhEEE5firstEv(%"class.std::__2::__compressed_pair.3"* %__end_cap_) #8
  ret i8** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__217__compressed_pairIPhNS_9allocatorIhEEE5firstEv(%"class.std::__2::__compressed_pair.3"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.3"*, align 4
  store %"class.std::__2::__compressed_pair.3"* %this, %"class.std::__2::__compressed_pair.3"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.3"*, %"class.std::__2::__compressed_pair.3"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.3"* %this1 to %"struct.std::__2::__compressed_pair_elem.4"*
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__222__compressed_pair_elemIPhLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.4"* %0) #8
  ret i8** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__222__compressed_pair_elemIPhLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.4"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.4"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.4"* %this, %"struct.std::__2::__compressed_pair_elem.4"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.4"*, %"struct.std::__2::__compressed_pair_elem.4"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.4", %"struct.std::__2::__compressed_pair_elem.4"* %this1, i32 0, i32 0
  ret i8** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE5clearEv(%"class.std::__2::__vector_base.2"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.2"*, align 4
  store %"class.std::__2::__vector_base.2"* %this, %"class.std::__2::__vector_base.2"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.2"*, %"class.std::__2::__vector_base.2"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %this1, i32 0, i32 0
  %0 = load i8*, i8** %__begin_, align 4
  call void @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE17__destruct_at_endEPh(%"class.std::__2::__vector_base.2"* %this1, i8* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE10deallocateERS2_Phm(%"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %__a, i8* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.6"*, align 4
  %__p.addr = alloca i8*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.6"* %__a, %"class.std::__2::allocator.6"** %__a.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.6"*, %"class.std::__2::allocator.6"** %__a.addr, align 4
  %1 = load i8*, i8** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorIhE10deallocateEPhm(%"class.std::__2::allocator.6"* %0, i8* %1, i32 %2) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE7__allocEv(%"class.std::__2::__vector_base.2"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.2"*, align 4
  store %"class.std::__2::__vector_base.2"* %this, %"class.std::__2::__vector_base.2"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.2"*, %"class.std::__2::__vector_base.2"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNSt3__217__compressed_pairIPhNS_9allocatorIhEEE6secondEv(%"class.std::__2::__compressed_pair.3"* %__end_cap_) #8
  ret %"class.std::__2::allocator.6"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE17__destruct_at_endEPh(%"class.std::__2::__vector_base.2"* %this, i8* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.2"*, align 4
  %__new_last.addr = alloca i8*, align 4
  %__soon_to_be_end = alloca i8*, align 4
  store %"class.std::__2::__vector_base.2"* %this, %"class.std::__2::__vector_base.2"** %this.addr, align 4
  store i8* %__new_last, i8** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.2"*, %"class.std::__2::__vector_base.2"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %this1, i32 0, i32 1
  %0 = load i8*, i8** %__end_, align 4
  store i8* %0, i8** %__soon_to_be_end, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load i8*, i8** %__new_last.addr, align 4
  %2 = load i8*, i8** %__soon_to_be_end, align 4
  %cmp = icmp ne i8* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE7__allocEv(%"class.std::__2::__vector_base.2"* %this1) #8
  %3 = load i8*, i8** %__soon_to_be_end, align 4
  %incdec.ptr = getelementptr inbounds i8, i8* %3, i32 -1
  store i8* %incdec.ptr, i8** %__soon_to_be_end, align 4
  %call2 = call i8* @_ZNSt3__212__to_addressIhEEPT_S2_(i8* %incdec.ptr) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE7destroyIhEEvRS2_PT_(%"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %call, i8* %call2)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %4 = load i8*, i8** %__new_last.addr, align 4
  %__end_3 = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %this1, i32 0, i32 1
  store i8* %4, i8** %__end_3, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE7destroyIhEEvRS2_PT_(%"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %__a, i8* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.6"*, align 4
  %__p.addr = alloca i8*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy.64", align 1
  store %"class.std::__2::allocator.6"* %__a, %"class.std::__2::allocator.6"** %__a.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy.64"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.6"*, %"class.std::__2::allocator.6"** %__a.addr, align 4
  %2 = load i8*, i8** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE9__destroyIhEEvNS_17integral_constantIbLb1EEERS2_PT_(%"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %1, i8* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE9__destroyIhEEvNS_17integral_constantIbLb1EEERS2_PT_(%"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %__a, i8* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.6"*, align 4
  %__p.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.6"* %__a, %"class.std::__2::allocator.6"** %__a.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.6"*, %"class.std::__2::allocator.6"** %__a.addr, align 4
  %2 = load i8*, i8** %__p.addr, align 4
  call void @_ZNSt3__29allocatorIhE7destroyEPh(%"class.std::__2::allocator.6"* %1, i8* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIhE7destroyEPh(%"class.std::__2::allocator.6"* %this, i8* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.6"*, align 4
  %__p.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.6"* %this, %"class.std::__2::allocator.6"** %this.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.6"*, %"class.std::__2::allocator.6"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIhE10deallocateEPhm(%"class.std::__2::allocator.6"* %this, i8* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.6"*, align 4
  %__p.addr = alloca i8*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.6"* %this, %"class.std::__2::allocator.6"** %this.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.6"*, %"class.std::__2::allocator.6"** %this.addr, align 4
  %0 = load i8*, i8** %__p.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %1, 1
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %0, i32 %mul, i32 1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNSt3__217__compressed_pairIPhNS_9allocatorIhEEE6secondEv(%"class.std::__2::__compressed_pair.3"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.3"*, align 4
  store %"class.std::__2::__compressed_pair.3"* %this, %"class.std::__2::__compressed_pair.3"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.3"*, %"class.std::__2::__compressed_pair.3"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.3"* %this1 to %"struct.std::__2::__compressed_pair_elem.5"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIhEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.5"* %0) #8
  ret %"class.std::__2::allocator.6"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIhEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.5"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.5"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.5"* %this, %"struct.std::__2::__compressed_pair_elem.5"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.5"*, %"struct.std::__2::__compressed_pair_elem.5"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.5"* %this1 to %"class.std::__2::allocator.6"*
  ret %"class.std::__2::allocator.6"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector.11"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEED2Ev(%"class.std::__2::vector.11"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.11"*, align 4
  store %"class.std::__2::vector.11"* %this, %"class.std::__2::vector.11"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.11"*, %"class.std::__2::vector.11"** %this.addr, align 4
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_deleteEv(%"class.std::__2::vector.11"* %this1) #8
  %0 = bitcast %"class.std::__2::vector.11"* %this1 to %"class.std::__2::__vector_base.12"*
  %call = call %"class.std::__2::__vector_base.12"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEED2Ev(%"class.std::__2::__vector_base.12"* %0) #8
  ret %"class.std::__2::vector.11"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_deleteEv(%"class.std::__2::vector.11"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.11"*, align 4
  store %"class.std::__2::vector.11"* %this, %"class.std::__2::vector.11"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.11"*, %"class.std::__2::vector.11"** %this.addr, align 4
  %call = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.11"* %this1) #8
  %0 = bitcast %"class.draco::IndexType"* %call to i8*
  %call2 = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.11"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.11"* %this1) #8
  %add.ptr = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %call2, i32 %call3
  %1 = bitcast %"class.draco::IndexType"* %add.ptr to i8*
  %call4 = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.11"* %this1) #8
  %call5 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.11"* %this1) #8
  %add.ptr6 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %call4, i32 %call5
  %2 = bitcast %"class.draco::IndexType"* %add.ptr6 to i8*
  %call7 = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.11"* %this1) #8
  %call8 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.11"* %this1) #8
  %add.ptr9 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %call7, i32 %call8
  %3 = bitcast %"class.draco::IndexType"* %add.ptr9 to i8*
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_(%"class.std::__2::vector.11"* %this1, i8* %0, i8* %1, i8* %2, i8* %3) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base.12"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEED2Ev(%"class.std::__2::__vector_base.12"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__vector_base.12"*, align 4
  %this.addr = alloca %"class.std::__2::__vector_base.12"*, align 4
  store %"class.std::__2::__vector_base.12"* %this, %"class.std::__2::__vector_base.12"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.12"*, %"class.std::__2::__vector_base.12"** %this.addr, align 4
  store %"class.std::__2::__vector_base.12"* %this1, %"class.std::__2::__vector_base.12"** %retval, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.12", %"class.std::__2::__vector_base.12"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  %cmp = icmp ne %"class.draco::IndexType"* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5clearEv(%"class.std::__2::__vector_base.12"* %this1) #8
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.16"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.12"* %this1) #8
  %__begin_2 = getelementptr inbounds %"class.std::__2::__vector_base.12", %"class.std::__2::__vector_base.12"* %this1, i32 0, i32 0
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_2, align 4
  %call3 = call i32 @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::__vector_base.12"* %this1) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE10deallocateERS6_PS5_m(%"class.std::__2::allocator.16"* nonnull align 1 dereferenceable(1) %call, %"class.draco::IndexType"* %1, i32 %call3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"class.std::__2::__vector_base.12"*, %"class.std::__2::__vector_base.12"** %retval, align 4
  ret %"class.std::__2::__vector_base.12"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_(%"class.std::__2::vector.11"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.11"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector.11"* %this, %"class.std::__2::vector.11"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector.11"*, %"class.std::__2::vector.11"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.11"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.11"*, align 4
  store %"class.std::__2::vector.11"* %this, %"class.std::__2::vector.11"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.11"*, %"class.std::__2::vector.11"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.11"* %this1 to %"class.std::__2::__vector_base.12"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.12", %"class.std::__2::__vector_base.12"* %0, i32 0, i32 0
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  %call = call %"class.draco::IndexType"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType"* %1) #8
  ret %"class.draco::IndexType"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.11"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.11"*, align 4
  store %"class.std::__2::vector.11"* %this, %"class.std::__2::vector.11"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.11"*, %"class.std::__2::vector.11"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.11"* %this1 to %"class.std::__2::__vector_base.12"*
  %call = call i32 @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::__vector_base.12"* %0) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.11"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.11"*, align 4
  store %"class.std::__2::vector.11"* %this, %"class.std::__2::vector.11"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.11"*, %"class.std::__2::vector.11"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.11"* %this1 to %"class.std::__2::__vector_base.12"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.12", %"class.std::__2::__vector_base.12"* %0, i32 0, i32 1
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector.11"* %this1 to %"class.std::__2::__vector_base.12"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.12", %"class.std::__2::__vector_base.12"* %2, i32 0, i32 0
  %3 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.draco::IndexType"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.draco::IndexType"* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType"* %__p) #0 comdat {
entry:
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  ret %"class.draco::IndexType"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::__vector_base.12"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.12"*, align 4
  store %"class.std::__2::__vector_base.12"* %this, %"class.std::__2::__vector_base.12"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.12"*, %"class.std::__2::__vector_base.12"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base.12"* %this1) #8
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.12", %"class.std::__2::__vector_base.12"* %this1, i32 0, i32 0
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.draco::IndexType"* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.draco::IndexType"* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base.12"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.12"*, align 4
  store %"class.std::__2::__vector_base.12"* %this, %"class.std::__2::__vector_base.12"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.12"*, %"class.std::__2::__vector_base.12"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.12", %"class.std::__2::__vector_base.12"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.13"* %__end_cap_) #8
  ret %"class.draco::IndexType"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.13"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.13"*, align 4
  store %"class.std::__2::__compressed_pair.13"* %this, %"class.std::__2::__compressed_pair.13"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.13"*, %"class.std::__2::__compressed_pair.13"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.13"* %this1 to %"struct.std::__2::__compressed_pair_elem.14"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.14"* %0) #8
  ret %"class.draco::IndexType"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.14"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.14"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.14"* %this, %"struct.std::__2::__compressed_pair_elem.14"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.14"*, %"struct.std::__2::__compressed_pair_elem.14"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.14", %"struct.std::__2::__compressed_pair_elem.14"* %this1, i32 0, i32 0
  ret %"class.draco::IndexType"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5clearEv(%"class.std::__2::__vector_base.12"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.12"*, align 4
  store %"class.std::__2::__vector_base.12"* %this, %"class.std::__2::__vector_base.12"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.12"*, %"class.std::__2::__vector_base.12"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.12", %"class.std::__2::__vector_base.12"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  call void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_(%"class.std::__2::__vector_base.12"* %this1, %"class.draco::IndexType"* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE10deallocateERS6_PS5_m(%"class.std::__2::allocator.16"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType"* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.16"*, align 4
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.16"* %__a, %"class.std::__2::allocator.16"** %__a.addr, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.16"*, %"class.std::__2::allocator.16"** %__a.addr, align 4
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE10deallocateEPS4_m(%"class.std::__2::allocator.16"* %0, %"class.draco::IndexType"* %1, i32 %2) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.16"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.12"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.12"*, align 4
  store %"class.std::__2::__vector_base.12"* %this, %"class.std::__2::__vector_base.12"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.12"*, %"class.std::__2::__vector_base.12"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.12", %"class.std::__2::__vector_base.12"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.16"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.13"* %__end_cap_) #8
  ret %"class.std::__2::allocator.16"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_(%"class.std::__2::__vector_base.12"* %this, %"class.draco::IndexType"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.12"*, align 4
  %__new_last.addr = alloca %"class.draco::IndexType"*, align 4
  %__soon_to_be_end = alloca %"class.draco::IndexType"*, align 4
  store %"class.std::__2::__vector_base.12"* %this, %"class.std::__2::__vector_base.12"** %this.addr, align 4
  store %"class.draco::IndexType"* %__new_last, %"class.draco::IndexType"** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.12"*, %"class.std::__2::__vector_base.12"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.12", %"class.std::__2::__vector_base.12"* %this1, i32 0, i32 1
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__end_, align 4
  store %"class.draco::IndexType"* %0, %"class.draco::IndexType"** %__soon_to_be_end, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__new_last.addr, align 4
  %2 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__soon_to_be_end, align 4
  %cmp = icmp ne %"class.draco::IndexType"* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.16"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.12"* %this1) #8
  %3 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__soon_to_be_end, align 4
  %incdec.ptr = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %3, i32 -1
  store %"class.draco::IndexType"* %incdec.ptr, %"class.draco::IndexType"** %__soon_to_be_end, align 4
  %call2 = call %"class.draco::IndexType"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType"* %incdec.ptr) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE7destroyIS5_EEvRS6_PT_(%"class.std::__2::allocator.16"* nonnull align 1 dereferenceable(1) %call, %"class.draco::IndexType"* %call2)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %4 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__new_last.addr, align 4
  %__end_3 = getelementptr inbounds %"class.std::__2::__vector_base.12", %"class.std::__2::__vector_base.12"* %this1, i32 0, i32 1
  store %"class.draco::IndexType"* %4, %"class.draco::IndexType"** %__end_3, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE7destroyIS5_EEvRS6_PT_(%"class.std::__2::allocator.16"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType"* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.16"*, align 4
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy.65", align 1
  store %"class.std::__2::allocator.16"* %__a, %"class.std::__2::allocator.16"** %__a.addr, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy.65"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.16"*, %"class.std::__2::allocator.16"** %__a.addr, align 4
  %2 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE9__destroyIS5_EEvNS_17integral_constantIbLb1EEERS6_PT_(%"class.std::__2::allocator.16"* nonnull align 1 dereferenceable(1) %1, %"class.draco::IndexType"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE9__destroyIS5_EEvNS_17integral_constantIbLb1EEERS6_PT_(%"class.std::__2::allocator.16"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType"* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.16"*, align 4
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.std::__2::allocator.16"* %__a, %"class.std::__2::allocator.16"** %__a.addr, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.16"*, %"class.std::__2::allocator.16"** %__a.addr, align 4
  %2 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  call void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE7destroyEPS4_(%"class.std::__2::allocator.16"* %1, %"class.draco::IndexType"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE7destroyEPS4_(%"class.std::__2::allocator.16"* %this, %"class.draco::IndexType"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.16"*, align 4
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.std::__2::allocator.16"* %this, %"class.std::__2::allocator.16"** %this.addr, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.16"*, %"class.std::__2::allocator.16"** %this.addr, align 4
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE10deallocateEPS4_m(%"class.std::__2::allocator.16"* %this, %"class.draco::IndexType"* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.16"*, align 4
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.16"* %this, %"class.std::__2::allocator.16"** %this.addr, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.16"*, %"class.std::__2::allocator.16"** %this.addr, align 4
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  %1 = bitcast %"class.draco::IndexType"* %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.16"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.13"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.13"*, align 4
  store %"class.std::__2::__compressed_pair.13"* %this, %"class.std::__2::__compressed_pair.13"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.13"*, %"class.std::__2::__compressed_pair.13"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.13"* %this1 to %"struct.std::__2::__compressed_pair_elem.15"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.16"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.15"* %0) #8
  ret %"class.std::__2::allocator.16"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.16"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.15"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.15"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.15"* %this, %"struct.std::__2::__compressed_pair_elem.15"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.15"*, %"struct.std::__2::__compressed_pair_elem.15"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.15"* %this1 to %"class.std::__2::allocator.16"*
  ret %"class.std::__2::allocator.16"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr"* %this, %"class.draco::DataBuffer"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  %__p.addr = alloca %"class.draco::DataBuffer"*, align 4
  %__tmp = alloca %"class.draco::DataBuffer"*, align 4
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  store %"class.draco::DataBuffer"* %__p, %"class.draco::DataBuffer"** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::DataBuffer"** @_ZNSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.8"* %__ptr_) #8
  %0 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %call, align 4
  store %"class.draco::DataBuffer"* %0, %"class.draco::DataBuffer"** %__tmp, align 4
  %1 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %__p.addr, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::DataBuffer"** @_ZNSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.8"* %__ptr_2) #8
  store %"class.draco::DataBuffer"* %1, %"class.draco::DataBuffer"** %call3, align 4
  %2 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %__tmp, align 4
  %tobool = icmp ne %"class.draco::DataBuffer"* %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__ptr_4 = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call5 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete"* @_ZNSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.8"* %__ptr_4) #8
  %3 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %__tmp, align 4
  call void @_ZNKSt3__214default_deleteIN5draco10DataBufferEEclEPS2_(%"struct.std::__2::default_delete"* %call5, %"class.draco::DataBuffer"* %3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::DataBuffer"** @_ZNSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.8"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.8"*, align 4
  store %"class.std::__2::__compressed_pair.8"* %this, %"class.std::__2::__compressed_pair.8"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.8"*, %"class.std::__2::__compressed_pair.8"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.8"* %this1 to %"struct.std::__2::__compressed_pair_elem.9"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::DataBuffer"** @_ZNSt3__222__compressed_pair_elemIPN5draco10DataBufferELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.9"* %0) #8
  ret %"class.draco::DataBuffer"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete"* @_ZNSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.8"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.8"*, align 4
  store %"class.std::__2::__compressed_pair.8"* %this, %"class.std::__2::__compressed_pair.8"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.8"*, %"class.std::__2::__compressed_pair.8"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.8"* %this1 to %"struct.std::__2::__compressed_pair_elem.10"*
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco10DataBufferEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.10"* %0) #8
  ret %"struct.std::__2::default_delete"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__214default_deleteIN5draco10DataBufferEEclEPS2_(%"struct.std::__2::default_delete"* %this, %"class.draco::DataBuffer"* %__ptr) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::default_delete"*, align 4
  %__ptr.addr = alloca %"class.draco::DataBuffer"*, align 4
  store %"struct.std::__2::default_delete"* %this, %"struct.std::__2::default_delete"** %this.addr, align 4
  store %"class.draco::DataBuffer"* %__ptr, %"class.draco::DataBuffer"** %__ptr.addr, align 4
  %this1 = load %"struct.std::__2::default_delete"*, %"struct.std::__2::default_delete"** %this.addr, align 4
  %0 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %__ptr.addr, align 4
  %isnull = icmp eq %"class.draco::DataBuffer"* %0, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %entry
  %call = call %"class.draco::DataBuffer"* @_ZN5draco10DataBufferD2Ev(%"class.draco::DataBuffer"* %0) #8
  %1 = bitcast %"class.draco::DataBuffer"* %0 to i8*
  call void @_ZdlPv(i8* %1) #10
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::DataBuffer"** @_ZNSt3__222__compressed_pair_elemIPN5draco10DataBufferELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.9"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.9"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.9"* %this, %"struct.std::__2::__compressed_pair_elem.9"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.9"*, %"struct.std::__2::__compressed_pair_elem.9"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.9", %"struct.std::__2::__compressed_pair_elem.9"* %this1, i32 0, i32 0
  ret %"class.draco::DataBuffer"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco10DataBufferEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.10"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.10"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.10"* %this, %"struct.std::__2::__compressed_pair_elem.10"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.10"*, %"struct.std::__2::__compressed_pair_elem.10"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.10"* %this1 to %"struct.std::__2::default_delete"*
  ret %"struct.std::__2::default_delete"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNKSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.40"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.40"*, align 4
  store %"class.std::__2::__compressed_pair.40"* %this, %"class.std::__2::__compressed_pair.40"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.40"*, %"class.std::__2::__compressed_pair.40"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.40"* %this1 to %"struct.std::__2::__compressed_pair_elem.41"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNKSt3__222__compressed_pair_elemIPN5draco14PointAttributeELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.41"* %0) #8
  ret %"class.draco::PointAttribute"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNKSt3__222__compressed_pair_elemIPN5draco14PointAttributeELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.41"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.41"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.41"* %this, %"struct.std::__2::__compressed_pair_elem.41"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.41"*, %"struct.std::__2::__compressed_pair_elem.41"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.41", %"struct.std::__2::__compressed_pair_elem.41"* %this1, i32 0, i32 0
  ret %"class.draco::PointAttribute"** %__value_
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind readnone speculatable willreturn }
attributes #2 = { nobuiltin allocsize(0) "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind willreturn }
attributes #4 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { noreturn "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { noinline noreturn nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #8 = { nounwind }
attributes #9 = { builtin allocsize(0) }
attributes #10 = { builtin nounwind }
attributes #11 = { noreturn }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
