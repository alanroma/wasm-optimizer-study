; ModuleID = './draco/src/draco/mesh/mesh_misc_functions.cc'
source_filename = "./draco/src/draco/mesh/mesh_misc_functions.cc"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%"class.std::__2::unique_ptr.110" = type { %"class.std::__2::__compressed_pair.111" }
%"class.std::__2::__compressed_pair.111" = type { %"struct.std::__2::__compressed_pair_elem.112" }
%"struct.std::__2::__compressed_pair_elem.112" = type { %"class.draco::CornerTable"* }
%"class.draco::CornerTable" = type { %"class.draco::IndexTypeVector.113", %"class.draco::IndexTypeVector.122", %"class.draco::IndexTypeVector.131", i32, i32, i32, %"class.draco::IndexTypeVector.132", %"class.draco::ValenceCache" }
%"class.draco::IndexTypeVector.113" = type { %"class.std::__2::vector.114" }
%"class.std::__2::vector.114" = type { %"class.std::__2::__vector_base.115" }
%"class.std::__2::__vector_base.115" = type { %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"*, %"class.std::__2::__compressed_pair.117" }
%"class.draco::IndexType.116" = type { i32 }
%"class.std::__2::__compressed_pair.117" = type { %"struct.std::__2::__compressed_pair_elem.118" }
%"struct.std::__2::__compressed_pair_elem.118" = type { %"class.draco::IndexType.116"* }
%"class.draco::IndexTypeVector.122" = type { %"class.std::__2::vector.123" }
%"class.std::__2::vector.123" = type { %"class.std::__2::__vector_base.124" }
%"class.std::__2::__vector_base.124" = type { %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"*, %"class.std::__2::__compressed_pair.126" }
%"class.draco::IndexType.125" = type { i32 }
%"class.std::__2::__compressed_pair.126" = type { %"struct.std::__2::__compressed_pair_elem.127" }
%"struct.std::__2::__compressed_pair_elem.127" = type { %"class.draco::IndexType.125"* }
%"class.draco::IndexTypeVector.131" = type { %"class.std::__2::vector.123" }
%"class.draco::IndexTypeVector.132" = type { %"class.std::__2::vector.114" }
%"class.draco::ValenceCache" = type { %"class.draco::CornerTable"*, %"class.draco::IndexTypeVector.133", %"class.draco::IndexTypeVector.141" }
%"class.draco::IndexTypeVector.133" = type { %"class.std::__2::vector.134" }
%"class.std::__2::vector.134" = type { %"class.std::__2::__vector_base.135" }
%"class.std::__2::__vector_base.135" = type { i8*, i8*, %"class.std::__2::__compressed_pair.136" }
%"class.std::__2::__compressed_pair.136" = type { %"struct.std::__2::__compressed_pair_elem.137" }
%"struct.std::__2::__compressed_pair_elem.137" = type { i8* }
%"class.draco::IndexTypeVector.141" = type { %"class.std::__2::vector.87" }
%"class.std::__2::vector.87" = type { %"class.std::__2::__vector_base.88" }
%"class.std::__2::__vector_base.88" = type { i32*, i32*, %"class.std::__2::__compressed_pair.89" }
%"class.std::__2::__compressed_pair.89" = type { %"struct.std::__2::__compressed_pair_elem.90" }
%"struct.std::__2::__compressed_pair_elem.90" = type { i32* }
%"class.draco::Mesh" = type { %"class.draco::PointCloud", %"class.std::__2::vector.94", %"class.draco::IndexTypeVector.101" }
%"class.draco::PointCloud" = type { i32 (...)**, %"class.std::__2::unique_ptr", %"class.std::__2::vector.51", [5 x %"class.std::__2::vector.87"], i32 }
%"class.std::__2::unique_ptr" = type { %"class.std::__2::__compressed_pair" }
%"class.std::__2::__compressed_pair" = type { %"struct.std::__2::__compressed_pair_elem" }
%"struct.std::__2::__compressed_pair_elem" = type { %"class.draco::GeometryMetadata"* }
%"class.draco::GeometryMetadata" = type { %"class.draco::Metadata", %"class.std::__2::vector" }
%"class.draco::Metadata" = type { %"class.std::__2::unordered_map", %"class.std::__2::unordered_map.17" }
%"class.std::__2::unordered_map" = type { %"class.std::__2::__hash_table" }
%"class.std::__2::__hash_table" = type { %"class.std::__2::unique_ptr.0", %"class.std::__2::__compressed_pair.7", %"class.std::__2::__compressed_pair.12", %"class.std::__2::__compressed_pair.14" }
%"class.std::__2::unique_ptr.0" = type { %"class.std::__2::__compressed_pair.1" }
%"class.std::__2::__compressed_pair.1" = type { %"struct.std::__2::__compressed_pair_elem.2", %"struct.std::__2::__compressed_pair_elem.3" }
%"struct.std::__2::__compressed_pair_elem.2" = type { %"struct.std::__2::__hash_node_base"** }
%"struct.std::__2::__hash_node_base" = type { %"struct.std::__2::__hash_node_base"* }
%"struct.std::__2::__compressed_pair_elem.3" = type { %"class.std::__2::__bucket_list_deallocator" }
%"class.std::__2::__bucket_list_deallocator" = type { %"class.std::__2::__compressed_pair.4" }
%"class.std::__2::__compressed_pair.4" = type { %"struct.std::__2::__compressed_pair_elem.5" }
%"struct.std::__2::__compressed_pair_elem.5" = type { i32 }
%"class.std::__2::__compressed_pair.7" = type { %"struct.std::__2::__compressed_pair_elem.8" }
%"struct.std::__2::__compressed_pair_elem.8" = type { %"struct.std::__2::__hash_node_base" }
%"class.std::__2::__compressed_pair.12" = type { %"struct.std::__2::__compressed_pair_elem.5" }
%"class.std::__2::__compressed_pair.14" = type { %"struct.std::__2::__compressed_pair_elem.15" }
%"struct.std::__2::__compressed_pair_elem.15" = type { float }
%"class.std::__2::unordered_map.17" = type { %"class.std::__2::__hash_table.18" }
%"class.std::__2::__hash_table.18" = type { %"class.std::__2::unique_ptr.19", %"class.std::__2::__compressed_pair.29", %"class.std::__2::__compressed_pair.34", %"class.std::__2::__compressed_pair.37" }
%"class.std::__2::unique_ptr.19" = type { %"class.std::__2::__compressed_pair.20" }
%"class.std::__2::__compressed_pair.20" = type { %"struct.std::__2::__compressed_pair_elem.21", %"struct.std::__2::__compressed_pair_elem.23" }
%"struct.std::__2::__compressed_pair_elem.21" = type { %"struct.std::__2::__hash_node_base.22"** }
%"struct.std::__2::__hash_node_base.22" = type { %"struct.std::__2::__hash_node_base.22"* }
%"struct.std::__2::__compressed_pair_elem.23" = type { %"class.std::__2::__bucket_list_deallocator.24" }
%"class.std::__2::__bucket_list_deallocator.24" = type { %"class.std::__2::__compressed_pair.25" }
%"class.std::__2::__compressed_pair.25" = type { %"struct.std::__2::__compressed_pair_elem.5" }
%"class.std::__2::__compressed_pair.29" = type { %"struct.std::__2::__compressed_pair_elem.30" }
%"struct.std::__2::__compressed_pair_elem.30" = type { %"struct.std::__2::__hash_node_base.22" }
%"class.std::__2::__compressed_pair.34" = type { %"struct.std::__2::__compressed_pair_elem.5" }
%"class.std::__2::__compressed_pair.37" = type { %"struct.std::__2::__compressed_pair_elem.15" }
%"class.std::__2::vector" = type { %"class.std::__2::__vector_base" }
%"class.std::__2::__vector_base" = type { %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"*, %"class.std::__2::__compressed_pair.44" }
%"class.std::__2::unique_ptr.40" = type { %"class.std::__2::__compressed_pair.41" }
%"class.std::__2::__compressed_pair.41" = type { %"struct.std::__2::__compressed_pair_elem.42" }
%"struct.std::__2::__compressed_pair_elem.42" = type { %"class.draco::AttributeMetadata"* }
%"class.draco::AttributeMetadata" = type { %"class.draco::Metadata", i32 }
%"class.std::__2::__compressed_pair.44" = type { %"struct.std::__2::__compressed_pair_elem.45" }
%"struct.std::__2::__compressed_pair_elem.45" = type { %"class.std::__2::unique_ptr.40"* }
%"class.std::__2::vector.51" = type { %"class.std::__2::__vector_base.52" }
%"class.std::__2::__vector_base.52" = type { %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"*, %"class.std::__2::__compressed_pair.82" }
%"class.std::__2::unique_ptr.53" = type { %"class.std::__2::__compressed_pair.54" }
%"class.std::__2::__compressed_pair.54" = type { %"struct.std::__2::__compressed_pair_elem.55" }
%"struct.std::__2::__compressed_pair_elem.55" = type { %"class.draco::PointAttribute"* }
%"class.draco::PointAttribute" = type <{ %"class.draco::GeometryAttribute", %"class.std::__2::unique_ptr.63", %"class.draco::IndexTypeVector", i32, i8, [3 x i8], %"class.std::__2::unique_ptr.75", [4 x i8] }>
%"class.draco::GeometryAttribute" = type { %"class.draco::DataBuffer"*, %"struct.draco::DataBufferDescriptor", i8, i32, i8, i64, i64, i32, i32 }
%"class.draco::DataBuffer" = type { %"class.std::__2::vector.56", %"struct.draco::DataBufferDescriptor" }
%"class.std::__2::vector.56" = type { %"class.std::__2::__vector_base.57" }
%"class.std::__2::__vector_base.57" = type { i8*, i8*, %"class.std::__2::__compressed_pair.58" }
%"class.std::__2::__compressed_pair.58" = type { %"struct.std::__2::__compressed_pair_elem.59" }
%"struct.std::__2::__compressed_pair_elem.59" = type { i8* }
%"struct.draco::DataBufferDescriptor" = type { i64, i64 }
%"class.std::__2::unique_ptr.63" = type { %"class.std::__2::__compressed_pair.64" }
%"class.std::__2::__compressed_pair.64" = type { %"struct.std::__2::__compressed_pair_elem.65" }
%"struct.std::__2::__compressed_pair_elem.65" = type { %"class.draco::DataBuffer"* }
%"class.draco::IndexTypeVector" = type { %"class.std::__2::vector.68" }
%"class.std::__2::vector.68" = type { %"class.std::__2::__vector_base.69" }
%"class.std::__2::__vector_base.69" = type { %"class.draco::IndexType"*, %"class.draco::IndexType"*, %"class.std::__2::__compressed_pair.70" }
%"class.draco::IndexType" = type { i32 }
%"class.std::__2::__compressed_pair.70" = type { %"struct.std::__2::__compressed_pair_elem.71" }
%"struct.std::__2::__compressed_pair_elem.71" = type { %"class.draco::IndexType"* }
%"class.std::__2::unique_ptr.75" = type { %"class.std::__2::__compressed_pair.76" }
%"class.std::__2::__compressed_pair.76" = type { %"struct.std::__2::__compressed_pair_elem.77" }
%"struct.std::__2::__compressed_pair_elem.77" = type { %"class.draco::AttributeTransformData"* }
%"class.draco::AttributeTransformData" = type { i32, %"class.draco::DataBuffer" }
%"class.std::__2::__compressed_pair.82" = type { %"struct.std::__2::__compressed_pair_elem.83" }
%"struct.std::__2::__compressed_pair_elem.83" = type { %"class.std::__2::unique_ptr.53"* }
%"class.std::__2::vector.94" = type { %"class.std::__2::__vector_base.95" }
%"class.std::__2::__vector_base.95" = type { %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"*, %"class.std::__2::__compressed_pair.96" }
%"struct.draco::Mesh::AttributeData" = type { i32 }
%"class.std::__2::__compressed_pair.96" = type { %"struct.std::__2::__compressed_pair_elem.97" }
%"struct.std::__2::__compressed_pair_elem.97" = type { %"struct.draco::Mesh::AttributeData"* }
%"class.draco::IndexTypeVector.101" = type { %"class.std::__2::vector.102" }
%"class.std::__2::vector.102" = type { %"class.std::__2::__vector_base.103" }
%"class.std::__2::__vector_base.103" = type { %"struct.std::__2::array"*, %"struct.std::__2::array"*, %"class.std::__2::__compressed_pair.105" }
%"struct.std::__2::array" = type { [3 x %"class.draco::IndexType.104"] }
%"class.draco::IndexType.104" = type { i32 }
%"class.std::__2::__compressed_pair.105" = type { %"struct.std::__2::__compressed_pair_elem.106" }
%"struct.std::__2::__compressed_pair_elem.106" = type { %"struct.std::__2::array"* }
%"class.draco::IndexTypeVector.144" = type { %"class.std::__2::vector.145" }
%"class.std::__2::vector.145" = type { %"class.std::__2::__vector_base.146" }
%"class.std::__2::__vector_base.146" = type { %"struct.std::__2::array.147"*, %"struct.std::__2::array.147"*, %"class.std::__2::__compressed_pair.148" }
%"struct.std::__2::array.147" = type { [3 x %"class.draco::IndexType.116"] }
%"class.std::__2::__compressed_pair.148" = type { %"struct.std::__2::__compressed_pair_elem.149" }
%"struct.std::__2::__compressed_pair_elem.149" = type { %"struct.std::__2::array.147"* }
%"class.draco::IndexType.153" = type { i32 }
%"struct.std::__2::__default_init_tag" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.142" = type { i8 }
%"struct.std::__2::default_delete.143" = type { i8 }
%"class.std::__2::allocator.151" = type { i8 }
%"struct.std::__2::integral_constant" = type { i8 }
%"struct.std::__2::__has_destroy" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.150" = type { i8 }
%"class.std::__2::__vector_base_common" = type { i8 }
%"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>>>::_ConstructTransaction" = type { %"class.std::__2::vector.145"*, %"struct.std::__2::array.147"*, %"struct.std::__2::array.147"* }
%"struct.std::__2::__less" = type { i8 }
%"struct.std::__2::__has_max_size" = type { i8 }
%"struct.std::__2::__has_construct" = type { i8 }

$_ZNSt3__210unique_ptrIN5draco11CornerTableENS_14default_deleteIS2_EEEC2ILb1EvEEDn = comdat any

$_ZNK5draco4Mesh9num_facesEv = comdat any

$_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEENSt3__25arrayINS1_IjNS_21VertexIndex_tag_type_EEELm3EEEEC2Em = comdat any

$_ZNSt3__25arrayIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEELm3EEC2Ev = comdat any

$_ZN5draco9IndexTypeIjNS_19FaceIndex_tag_type_EEC2Ej = comdat any

$_ZNK5draco9IndexTypeIjNS_19FaceIndex_tag_type_EEltERKj = comdat any

$_ZNK5draco4Mesh4faceENS_9IndexTypeIjNS_19FaceIndex_tag_type_EEE = comdat any

$_ZNK5draco14PointAttribute12mapped_indexENS_9IndexTypeIjNS_20PointIndex_tag_type_EEE = comdat any

$_ZNKSt3__25arrayIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELm3EEixEm = comdat any

$_ZNK5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EE5valueEv = comdat any

$_ZNSt3__25arrayIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEELm3EEixEm = comdat any

$_ZN5draco9IndexTypeIjNS_21VertexIndex_tag_type_EEaSERKj = comdat any

$_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEENSt3__25arrayINS1_IjNS_21VertexIndex_tag_type_EEELm3EEEEixERKS3_ = comdat any

$_ZNSt3__25arrayIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEELm3EEaSERKS5_ = comdat any

$_ZN5draco9IndexTypeIjNS_19FaceIndex_tag_type_EEppEv = comdat any

$_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEENSt3__25arrayINS1_IjNS_21VertexIndex_tag_type_EEELm3EEEED2Ev = comdat any

$_ZNK5draco9IndexTypeIjNS_20PointIndex_tag_type_EE5valueEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco11CornerTableENS_14default_deleteIS2_EEEC2IS3_NS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__27forwardIPN5draco11CornerTableEEEOT_RNS_16remove_referenceIS4_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco11CornerTableELi0ELb0EEC2IS3_vEEOT_ = comdat any

$_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco11CornerTableEEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEENSt3__25arrayINS1_IjNS_20PointIndex_tag_type_EEELm3EEEE4sizeEv = comdat any

$_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4sizeEv = comdat any

$_ZN5draco9IndexTypeIjNS_21VertexIndex_tag_type_EEC2Ev = comdat any

$_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEENSt3__25arrayINS1_IjNS_20PointIndex_tag_type_EEELm3EEEEixERKS3_ = comdat any

$_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEEixEm = comdat any

$_ZNK5draco9IndexTypeIjNS_19FaceIndex_tag_type_EE5valueEv = comdat any

$_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEC2Ej = comdat any

$_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEEEixERKS3_ = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEEixEm = comdat any

$_ZN5draco9IndexTypeIjNS_21VertexIndex_tag_type_EEaSERKS2_ = comdat any

$_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEED2Ev = comdat any

$_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE17__annotate_deleteEv = comdat any

$_ZNSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEED2Ev = comdat any

$_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE31__annotate_contiguous_containerEPKvSB_SB_SB_ = comdat any

$_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4dataEv = comdat any

$_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE8capacityEv = comdat any

$_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4sizeEv = comdat any

$_ZNSt3__212__to_addressINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEEEEPT_S8_ = comdat any

$_ZNKSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPNS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE5clearEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_21VertexIndex_tag_type_EEELm3EEEEEE10deallocateERS8_PS7_m = comdat any

$_ZNSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE7__allocEv = comdat any

$_ZNSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE17__destruct_at_endEPS6_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_21VertexIndex_tag_type_EEELm3EEEEEE7destroyIS7_EEvRS8_PT_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_21VertexIndex_tag_type_EEELm3EEEEEE9__destroyIS7_EEvNS_17integral_constantIbLb1EEERS8_PT_ = comdat any

$_ZNSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEEE7destroyEPS6_ = comdat any

$_ZNSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEEE10deallocateEPS6_m = comdat any

$_ZNSt3__219__libcpp_deallocateEPvmm = comdat any

$_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm = comdat any

$_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm = comdat any

$_ZNSt3__217_DeallocateCaller9__do_callEPv = comdat any

$_ZNSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_21VertexIndex_tag_type_EEELm3EEEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEEC2Em = comdat any

$_ZNSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEEC2Ev = comdat any

$_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE11__vallocateEm = comdat any

$_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE18__construct_at_endEm = comdat any

$_ZNSt3__220__vector_base_commonILb1EEC2Ev = comdat any

$_ZNSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEEC2IDnNS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPNS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEELi0ELb0EEC2IDnvEEOT_ = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_21VertexIndex_tag_type_EEELm3EEEEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEEEC2Ev = comdat any

$_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE8max_sizeEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_21VertexIndex_tag_type_EEELm3EEEEEE8allocateERS8_m = comdat any

$_ZNSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE9__end_capEv = comdat any

$_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE14__annotate_newEm = comdat any

$_ZNSt3__23minImEERKT_S3_S3_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_21VertexIndex_tag_type_EEELm3EEEEEE8max_sizeERKS8_ = comdat any

$_ZNKSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE7__allocEv = comdat any

$_ZNSt3__214numeric_limitsIlE3maxEv = comdat any

$_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_ = comdat any

$_ZNKSt3__26__lessImmEclERKmS3_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_21VertexIndex_tag_type_EEELm3EEEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS8_ = comdat any

$_ZNKSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEEE8max_sizeEv = comdat any

$_ZNKSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE6secondEv = comdat any

$_ZNKSt3__222__compressed_pair_elemINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_21VertexIndex_tag_type_EEELm3EEEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv = comdat any

$_ZNSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEEE8allocateEmPKv = comdat any

$_ZNSt3__220__throw_length_errorEPKc = comdat any

$_ZNSt3__217__libcpp_allocateEmm = comdat any

$_ZNSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE5firstEv = comdat any

$_ZNSt3__222__compressed_pair_elemIPNS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE21_ConstructTransactionC2ERS9_m = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_21VertexIndex_tag_type_EEELm3EEEEEE9constructIS7_JEEEvRS8_PT_DpOT0_ = comdat any

$_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE21_ConstructTransactionD2Ev = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_21VertexIndex_tag_type_EEELm3EEEEEE11__constructIS7_JEEEvNS_17integral_constantIbLb1EEERS8_PT_DpOT0_ = comdat any

$_ZNSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEEE9constructIS6_JEEEvPT_DpOT0_ = comdat any

$_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEEixEm = comdat any

@.str = private unnamed_addr constant [68 x i8] c"allocator<T>::allocate(size_t n) 'n' exceeds maximum supported size\00", align 1

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN5draco38CreateCornerTableFromPositionAttributeEPKNS_4MeshE(%"class.std::__2::unique_ptr.110"* noalias sret align 4 %agg.result, %"class.draco::Mesh"* %mesh) #0 {
entry:
  %result.ptr = alloca i8*, align 4
  %mesh.addr = alloca %"class.draco::Mesh"*, align 4
  %att = alloca %"class.draco::PointAttribute"*, align 4
  %faces = alloca %"class.draco::IndexTypeVector.144", align 4
  %new_face = alloca %"struct.std::__2::array.147", align 4
  %i = alloca %"class.draco::IndexType.153", align 4
  %ref.tmp = alloca i32, align 4
  %face = alloca %"struct.std::__2::array"*, align 4
  %agg.tmp = alloca %"class.draco::IndexType.153", align 4
  %j = alloca i32, align 4
  %ref.tmp12 = alloca i32, align 4
  %ref.tmp13 = alloca %"class.draco::IndexType", align 4
  %agg.tmp14 = alloca %"class.draco::IndexType.104", align 4
  %ref.tmp22 = alloca %"class.draco::IndexType.153", align 4
  %0 = bitcast %"class.std::__2::unique_ptr.110"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %"class.draco::Mesh"* %mesh, %"class.draco::Mesh"** %mesh.addr, align 4
  %1 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %mesh.addr, align 4
  %2 = bitcast %"class.draco::Mesh"* %1 to %"class.draco::PointCloud"*
  %call = call %"class.draco::PointAttribute"* @_ZNK5draco10PointCloud17GetNamedAttributeENS_17GeometryAttribute4TypeE(%"class.draco::PointCloud"* %2, i32 0)
  store %"class.draco::PointAttribute"* %call, %"class.draco::PointAttribute"** %att, align 4
  %3 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %att, align 4
  %cmp = icmp eq %"class.draco::PointAttribute"* %3, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call1 = call %"class.std::__2::unique_ptr.110"* @_ZNSt3__210unique_ptrIN5draco11CornerTableENS_14default_deleteIS2_EEEC2ILb1EvEEDn(%"class.std::__2::unique_ptr.110"* %agg.result, i8* null) #8
  br label %return

if.end:                                           ; preds = %entry
  %4 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %mesh.addr, align 4
  %call2 = call i32 @_ZNK5draco4Mesh9num_facesEv(%"class.draco::Mesh"* %4)
  %call3 = call %"class.draco::IndexTypeVector.144"* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEENSt3__25arrayINS1_IjNS_21VertexIndex_tag_type_EEELm3EEEEC2Em(%"class.draco::IndexTypeVector.144"* %faces, i32 %call2)
  %call4 = call %"struct.std::__2::array.147"* @_ZNSt3__25arrayIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEELm3EEC2Ev(%"struct.std::__2::array.147"* %new_face)
  %call5 = call %"class.draco::IndexType.153"* @_ZN5draco9IndexTypeIjNS_19FaceIndex_tag_type_EEC2Ej(%"class.draco::IndexType.153"* %i, i32 0)
  br label %for.cond

for.cond:                                         ; preds = %for.inc25, %if.end
  %5 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %mesh.addr, align 4
  %call6 = call i32 @_ZNK5draco4Mesh9num_facesEv(%"class.draco::Mesh"* %5)
  store i32 %call6, i32* %ref.tmp, align 4
  %call7 = call zeroext i1 @_ZNK5draco9IndexTypeIjNS_19FaceIndex_tag_type_EEltERKj(%"class.draco::IndexType.153"* %i, i32* nonnull align 4 dereferenceable(4) %ref.tmp)
  br i1 %call7, label %for.body, label %for.end27

for.body:                                         ; preds = %for.cond
  %6 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %mesh.addr, align 4
  %7 = bitcast %"class.draco::IndexType.153"* %agg.tmp to i8*
  %8 = bitcast %"class.draco::IndexType.153"* %i to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 4, i1 false)
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType.153", %"class.draco::IndexType.153"* %agg.tmp, i32 0, i32 0
  %9 = load i32, i32* %coerce.dive, align 4
  %call8 = call nonnull align 4 dereferenceable(12) %"struct.std::__2::array"* @_ZNK5draco4Mesh4faceENS_9IndexTypeIjNS_19FaceIndex_tag_type_EEE(%"class.draco::Mesh"* %6, i32 %9)
  store %"struct.std::__2::array"* %call8, %"struct.std::__2::array"** %face, align 4
  store i32 0, i32* %j, align 4
  br label %for.cond9

for.cond9:                                        ; preds = %for.inc, %for.body
  %10 = load i32, i32* %j, align 4
  %cmp10 = icmp slt i32 %10, 3
  br i1 %cmp10, label %for.body11, label %for.end

for.body11:                                       ; preds = %for.cond9
  %11 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %att, align 4
  %12 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %face, align 4
  %13 = load i32, i32* %j, align 4
  %call15 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.104"* @_ZNKSt3__25arrayIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELm3EEixEm(%"struct.std::__2::array"* %12, i32 %13) #8
  %14 = bitcast %"class.draco::IndexType.104"* %agg.tmp14 to i8*
  %15 = bitcast %"class.draco::IndexType.104"* %call15 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %14, i8* align 4 %15, i32 4, i1 false)
  %coerce.dive16 = getelementptr inbounds %"class.draco::IndexType.104", %"class.draco::IndexType.104"* %agg.tmp14, i32 0, i32 0
  %16 = load i32, i32* %coerce.dive16, align 4
  %call17 = call i32 @_ZNK5draco14PointAttribute12mapped_indexENS_9IndexTypeIjNS_20PointIndex_tag_type_EEE(%"class.draco::PointAttribute"* %11, i32 %16)
  %coerce.dive18 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %ref.tmp13, i32 0, i32 0
  store i32 %call17, i32* %coerce.dive18, align 4
  %call19 = call i32 @_ZNK5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EE5valueEv(%"class.draco::IndexType"* %ref.tmp13)
  store i32 %call19, i32* %ref.tmp12, align 4
  %17 = load i32, i32* %j, align 4
  %call20 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.116"* @_ZNSt3__25arrayIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEELm3EEixEm(%"struct.std::__2::array.147"* %new_face, i32 %17) #8
  %call21 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.116"* @_ZN5draco9IndexTypeIjNS_21VertexIndex_tag_type_EEaSERKj(%"class.draco::IndexType.116"* %call20, i32* nonnull align 4 dereferenceable(4) %ref.tmp12)
  br label %for.inc

for.inc:                                          ; preds = %for.body11
  %18 = load i32, i32* %j, align 4
  %inc = add nsw i32 %18, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond9

for.end:                                          ; preds = %for.cond9
  %19 = bitcast %"class.draco::IndexType.153"* %ref.tmp22 to i8*
  %20 = bitcast %"class.draco::IndexType.153"* %i to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %19, i8* align 4 %20, i32 4, i1 false)
  %call23 = call nonnull align 4 dereferenceable(12) %"struct.std::__2::array.147"* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEENSt3__25arrayINS1_IjNS_21VertexIndex_tag_type_EEELm3EEEEixERKS3_(%"class.draco::IndexTypeVector.144"* %faces, %"class.draco::IndexType.153"* nonnull align 4 dereferenceable(4) %ref.tmp22)
  %call24 = call nonnull align 4 dereferenceable(12) %"struct.std::__2::array.147"* @_ZNSt3__25arrayIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEELm3EEaSERKS5_(%"struct.std::__2::array.147"* %call23, %"struct.std::__2::array.147"* nonnull align 4 dereferenceable(12) %new_face)
  br label %for.inc25

for.inc25:                                        ; preds = %for.end
  %call26 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.153"* @_ZN5draco9IndexTypeIjNS_19FaceIndex_tag_type_EEppEv(%"class.draco::IndexType.153"* %i)
  br label %for.cond

for.end27:                                        ; preds = %for.cond
  call void @_ZN5draco11CornerTable6CreateERKNS_15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEENSt3__25arrayINS2_IjNS_21VertexIndex_tag_type_EEELm3EEEEE(%"class.std::__2::unique_ptr.110"* sret align 4 %agg.result, %"class.draco::IndexTypeVector.144"* nonnull align 4 dereferenceable(12) %faces)
  %call28 = call %"class.draco::IndexTypeVector.144"* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEENSt3__25arrayINS1_IjNS_21VertexIndex_tag_type_EEELm3EEEED2Ev(%"class.draco::IndexTypeVector.144"* %faces) #8
  br label %return

return:                                           ; preds = %for.end27, %if.then
  ret void
}

declare %"class.draco::PointAttribute"* @_ZNK5draco10PointCloud17GetNamedAttributeENS_17GeometryAttribute4TypeE(%"class.draco::PointCloud"*, i32) #1

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.110"* @_ZNSt3__210unique_ptrIN5draco11CornerTableENS_14default_deleteIS2_EEEC2ILb1EvEEDn(%"class.std::__2::unique_ptr.110"* returned %this, i8* %0) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.110"*, align 4
  %.addr = alloca i8*, align 4
  %ref.tmp = alloca %"class.draco::CornerTable"*, align 4
  %ref.tmp2 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::unique_ptr.110"* %this, %"class.std::__2::unique_ptr.110"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.110"*, %"class.std::__2::unique_ptr.110"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.110", %"class.std::__2::unique_ptr.110"* %this1, i32 0, i32 0
  store %"class.draco::CornerTable"* null, %"class.draco::CornerTable"** %ref.tmp, align 4
  %call = call %"class.std::__2::__compressed_pair.111"* @_ZNSt3__217__compressed_pairIPN5draco11CornerTableENS_14default_deleteIS2_EEEC2IS3_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.111"* %__ptr_, %"class.draco::CornerTable"** nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  ret %"class.std::__2::unique_ptr.110"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco4Mesh9num_facesEv(%"class.draco::Mesh"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::Mesh"*, align 4
  store %"class.draco::Mesh"* %this, %"class.draco::Mesh"** %this.addr, align 4
  %this1 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %this.addr, align 4
  %faces_ = getelementptr inbounds %"class.draco::Mesh", %"class.draco::Mesh"* %this1, i32 0, i32 2
  %call = call i32 @_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEENSt3__25arrayINS1_IjNS_20PointIndex_tag_type_EEELm3EEEE4sizeEv(%"class.draco::IndexTypeVector.101"* %faces_)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexTypeVector.144"* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEENSt3__25arrayINS1_IjNS_21VertexIndex_tag_type_EEELm3EEEEC2Em(%"class.draco::IndexTypeVector.144"* returned %this, i32 %size) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector.144"*, align 4
  %size.addr = alloca i32, align 4
  store %"class.draco::IndexTypeVector.144"* %this, %"class.draco::IndexTypeVector.144"** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector.144"*, %"class.draco::IndexTypeVector.144"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector.144", %"class.draco::IndexTypeVector.144"* %this1, i32 0, i32 0
  %0 = load i32, i32* %size.addr, align 4
  %call = call %"class.std::__2::vector.145"* @_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEEC2Em(%"class.std::__2::vector.145"* %vector_, i32 %0)
  ret %"class.draco::IndexTypeVector.144"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::array.147"* @_ZNSt3__25arrayIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEELm3EEC2Ev(%"struct.std::__2::array.147"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"struct.std::__2::array.147"*, align 4
  %this.addr = alloca %"struct.std::__2::array.147"*, align 4
  store %"struct.std::__2::array.147"* %this, %"struct.std::__2::array.147"** %this.addr, align 4
  %this1 = load %"struct.std::__2::array.147"*, %"struct.std::__2::array.147"** %this.addr, align 4
  store %"struct.std::__2::array.147"* %this1, %"struct.std::__2::array.147"** %retval, align 4
  %__elems_ = getelementptr inbounds %"struct.std::__2::array.147", %"struct.std::__2::array.147"* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %"class.draco::IndexType.116"], [3 x %"class.draco::IndexType.116"]* %__elems_, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %"class.draco::IndexType.116", %"class.draco::IndexType.116"* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %"class.draco::IndexType.116"* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %"class.draco::IndexType.116"* @_ZN5draco9IndexTypeIjNS_21VertexIndex_tag_type_EEC2Ev(%"class.draco::IndexType.116"* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %"class.draco::IndexType.116", %"class.draco::IndexType.116"* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %"class.draco::IndexType.116"* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %"struct.std::__2::array.147"*, %"struct.std::__2::array.147"** %retval, align 4
  ret %"struct.std::__2::array.147"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType.153"* @_ZN5draco9IndexTypeIjNS_19FaceIndex_tag_type_EEC2Ej(%"class.draco::IndexType.153"* returned %this, i32 %value) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.153"*, align 4
  %value.addr = alloca i32, align 4
  store %"class.draco::IndexType.153"* %this, %"class.draco::IndexType.153"** %this.addr, align 4
  store i32 %value, i32* %value.addr, align 4
  %this1 = load %"class.draco::IndexType.153"*, %"class.draco::IndexType.153"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.153", %"class.draco::IndexType.153"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value.addr, align 4
  store i32 %0, i32* %value_, align 4
  ret %"class.draco::IndexType.153"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK5draco9IndexTypeIjNS_19FaceIndex_tag_type_EEltERKj(%"class.draco::IndexType.153"* %this, i32* nonnull align 4 dereferenceable(4) %val) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.153"*, align 4
  %val.addr = alloca i32*, align 4
  store %"class.draco::IndexType.153"* %this, %"class.draco::IndexType.153"** %this.addr, align 4
  store i32* %val, i32** %val.addr, align 4
  %this1 = load %"class.draco::IndexType.153"*, %"class.draco::IndexType.153"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.153", %"class.draco::IndexType.153"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value_, align 4
  %1 = load i32*, i32** %val.addr, align 4
  %2 = load i32, i32* %1, align 4
  %cmp = icmp ult i32 %0, %2
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"struct.std::__2::array"* @_ZNK5draco4Mesh4faceENS_9IndexTypeIjNS_19FaceIndex_tag_type_EEE(%"class.draco::Mesh"* %this, i32 %face_id.coerce) #0 comdat {
entry:
  %face_id = alloca %"class.draco::IndexType.153", align 4
  %this.addr = alloca %"class.draco::Mesh"*, align 4
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType.153", %"class.draco::IndexType.153"* %face_id, i32 0, i32 0
  store i32 %face_id.coerce, i32* %coerce.dive, align 4
  store %"class.draco::Mesh"* %this, %"class.draco::Mesh"** %this.addr, align 4
  %this1 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %this.addr, align 4
  %faces_ = getelementptr inbounds %"class.draco::Mesh", %"class.draco::Mesh"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::array"* @_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEENSt3__25arrayINS1_IjNS_20PointIndex_tag_type_EEELm3EEEEixERKS3_(%"class.draco::IndexTypeVector.101"* %faces_, %"class.draco::IndexType.153"* nonnull align 4 dereferenceable(4) %face_id)
  ret %"struct.std::__2::array"* %call
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #2

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco14PointAttribute12mapped_indexENS_9IndexTypeIjNS_20PointIndex_tag_type_EEE(%"class.draco::PointAttribute"* %this, i32 %point_index.coerce) #0 comdat {
entry:
  %retval = alloca %"class.draco::IndexType", align 4
  %point_index = alloca %"class.draco::IndexType.104", align 4
  %this.addr = alloca %"class.draco::PointAttribute"*, align 4
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType.104", %"class.draco::IndexType.104"* %point_index, i32 0, i32 0
  store i32 %point_index.coerce, i32* %coerce.dive, align 4
  store %"class.draco::PointAttribute"* %this, %"class.draco::PointAttribute"** %this.addr, align 4
  %this1 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %this.addr, align 4
  %identity_mapping_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 4
  %0 = load i8, i8* %identity_mapping_, align 4
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call = call i32 @_ZNK5draco9IndexTypeIjNS_20PointIndex_tag_type_EE5valueEv(%"class.draco::IndexType.104"* %point_index)
  %call2 = call %"class.draco::IndexType"* @_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEC2Ej(%"class.draco::IndexType"* %retval, i32 %call)
  br label %return

if.end:                                           ; preds = %entry
  %indices_map_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 2
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEEEixERKS3_(%"class.draco::IndexTypeVector"* %indices_map_, %"class.draco::IndexType.104"* nonnull align 4 dereferenceable(4) %point_index)
  %1 = bitcast %"class.draco::IndexType"* %retval to i8*
  %2 = bitcast %"class.draco::IndexType"* %call3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 4, i1 false)
  br label %return

return:                                           ; preds = %if.end, %if.then
  %coerce.dive4 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %retval, i32 0, i32 0
  %3 = load i32, i32* %coerce.dive4, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.104"* @_ZNKSt3__25arrayIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELm3EEixEm(%"struct.std::__2::array"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::array"*, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::array"* %this, %"struct.std::__2::array"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %this.addr, align 4
  %__elems_ = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %this1, i32 0, i32 0
  %0 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %"class.draco::IndexType.104"], [3 x %"class.draco::IndexType.104"]* %__elems_, i32 0, i32 %0
  ret %"class.draco::IndexType.104"* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EE5valueEv(%"class.draco::IndexType"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.draco::IndexType"* %this, %"class.draco::IndexType"** %this.addr, align 4
  %this1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value_, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.116"* @_ZNSt3__25arrayIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEELm3EEixEm(%"struct.std::__2::array.147"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::array.147"*, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::array.147"* %this, %"struct.std::__2::array.147"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::array.147"*, %"struct.std::__2::array.147"** %this.addr, align 4
  %__elems_ = getelementptr inbounds %"struct.std::__2::array.147", %"struct.std::__2::array.147"* %this1, i32 0, i32 0
  %0 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %"class.draco::IndexType.116"], [3 x %"class.draco::IndexType.116"]* %__elems_, i32 0, i32 %0
  ret %"class.draco::IndexType.116"* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.116"* @_ZN5draco9IndexTypeIjNS_21VertexIndex_tag_type_EEaSERKj(%"class.draco::IndexType.116"* %this, i32* nonnull align 4 dereferenceable(4) %val) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.116"*, align 4
  %val.addr = alloca i32*, align 4
  store %"class.draco::IndexType.116"* %this, %"class.draco::IndexType.116"** %this.addr, align 4
  store i32* %val, i32** %val.addr, align 4
  %this1 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %this.addr, align 4
  %0 = load i32*, i32** %val.addr, align 4
  %1 = load i32, i32* %0, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.116", %"class.draco::IndexType.116"* %this1, i32 0, i32 0
  store i32 %1, i32* %value_, align 4
  ret %"class.draco::IndexType.116"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"struct.std::__2::array.147"* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEENSt3__25arrayINS1_IjNS_21VertexIndex_tag_type_EEELm3EEEEixERKS3_(%"class.draco::IndexTypeVector.144"* %this, %"class.draco::IndexType.153"* nonnull align 4 dereferenceable(4) %index) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector.144"*, align 4
  %index.addr = alloca %"class.draco::IndexType.153"*, align 4
  store %"class.draco::IndexTypeVector.144"* %this, %"class.draco::IndexTypeVector.144"** %this.addr, align 4
  store %"class.draco::IndexType.153"* %index, %"class.draco::IndexType.153"** %index.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector.144"*, %"class.draco::IndexTypeVector.144"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector.144", %"class.draco::IndexTypeVector.144"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexType.153"*, %"class.draco::IndexType.153"** %index.addr, align 4
  %call = call i32 @_ZNK5draco9IndexTypeIjNS_19FaceIndex_tag_type_EE5valueEv(%"class.draco::IndexType.153"* %0)
  %call2 = call nonnull align 4 dereferenceable(12) %"struct.std::__2::array.147"* @_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEEixEm(%"class.std::__2::vector.145"* %vector_, i32 %call) #8
  ret %"struct.std::__2::array.147"* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"struct.std::__2::array.147"* @_ZNSt3__25arrayIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEELm3EEaSERKS5_(%"struct.std::__2::array.147"* %this, %"struct.std::__2::array.147"* nonnull align 4 dereferenceable(12) %0) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::array.147"*, align 4
  %.addr = alloca %"struct.std::__2::array.147"*, align 4
  %__i0 = alloca i32, align 4
  store %"struct.std::__2::array.147"* %this, %"struct.std::__2::array.147"** %this.addr, align 4
  store %"struct.std::__2::array.147"* %0, %"struct.std::__2::array.147"** %.addr, align 4
  %this1 = load %"struct.std::__2::array.147"*, %"struct.std::__2::array.147"** %this.addr, align 4
  store i32 0, i32* %__i0, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %__i0, align 4
  %cmp = icmp ne i32 %1, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %__elems_ = getelementptr inbounds %"struct.std::__2::array.147", %"struct.std::__2::array.147"* %this1, i32 0, i32 0
  %2 = load i32, i32* %__i0, align 4
  %arrayidx = getelementptr inbounds [3 x %"class.draco::IndexType.116"], [3 x %"class.draco::IndexType.116"]* %__elems_, i32 0, i32 %2
  %3 = load %"struct.std::__2::array.147"*, %"struct.std::__2::array.147"** %.addr, align 4
  %__elems_2 = getelementptr inbounds %"struct.std::__2::array.147", %"struct.std::__2::array.147"* %3, i32 0, i32 0
  %4 = load i32, i32* %__i0, align 4
  %arrayidx3 = getelementptr inbounds [3 x %"class.draco::IndexType.116"], [3 x %"class.draco::IndexType.116"]* %__elems_2, i32 0, i32 %4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.116"* @_ZN5draco9IndexTypeIjNS_21VertexIndex_tag_type_EEaSERKS2_(%"class.draco::IndexType.116"* %arrayidx, %"class.draco::IndexType.116"* nonnull align 4 dereferenceable(4) %arrayidx3)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %__i0, align 4
  %inc = add i32 %5, 1
  store i32 %inc, i32* %__i0, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret %"struct.std::__2::array.147"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.153"* @_ZN5draco9IndexTypeIjNS_19FaceIndex_tag_type_EEppEv(%"class.draco::IndexType.153"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.153"*, align 4
  store %"class.draco::IndexType.153"* %this, %"class.draco::IndexType.153"** %this.addr, align 4
  %this1 = load %"class.draco::IndexType.153"*, %"class.draco::IndexType.153"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.153", %"class.draco::IndexType.153"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value_, align 4
  %inc = add i32 %0, 1
  store i32 %inc, i32* %value_, align 4
  ret %"class.draco::IndexType.153"* %this1
}

declare void @_ZN5draco11CornerTable6CreateERKNS_15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEENSt3__25arrayINS2_IjNS_21VertexIndex_tag_type_EEELm3EEEEE(%"class.std::__2::unique_ptr.110"* sret align 4, %"class.draco::IndexTypeVector.144"* nonnull align 4 dereferenceable(12)) #1

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexTypeVector.144"* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEENSt3__25arrayINS1_IjNS_21VertexIndex_tag_type_EEELm3EEEED2Ev(%"class.draco::IndexTypeVector.144"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector.144"*, align 4
  store %"class.draco::IndexTypeVector.144"* %this, %"class.draco::IndexTypeVector.144"** %this.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector.144"*, %"class.draco::IndexTypeVector.144"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector.144", %"class.draco::IndexTypeVector.144"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::vector.145"* @_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEED2Ev(%"class.std::__2::vector.145"* %vector_) #8
  ret %"class.draco::IndexTypeVector.144"* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN5draco34CreateCornerTableFromAllAttributesEPKNS_4MeshE(%"class.std::__2::unique_ptr.110"* noalias sret align 4 %agg.result, %"class.draco::Mesh"* %mesh) #0 {
entry:
  %result.ptr = alloca i8*, align 4
  %mesh.addr = alloca %"class.draco::Mesh"*, align 4
  %faces = alloca %"class.draco::IndexTypeVector.144", align 4
  %new_face = alloca %"struct.std::__2::array.147", align 4
  %i = alloca %"class.draco::IndexType.153", align 4
  %ref.tmp = alloca i32, align 4
  %face = alloca %"struct.std::__2::array"*, align 4
  %agg.tmp = alloca %"class.draco::IndexType.153", align 4
  %j = alloca i32, align 4
  %ref.tmp9 = alloca i32, align 4
  %0 = bitcast %"class.std::__2::unique_ptr.110"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %"class.draco::Mesh"* %mesh, %"class.draco::Mesh"** %mesh.addr, align 4
  %1 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %mesh.addr, align 4
  %call = call i32 @_ZNK5draco4Mesh9num_facesEv(%"class.draco::Mesh"* %1)
  %call1 = call %"class.draco::IndexTypeVector.144"* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEENSt3__25arrayINS1_IjNS_21VertexIndex_tag_type_EEELm3EEEEC2Em(%"class.draco::IndexTypeVector.144"* %faces, i32 %call)
  %call2 = call %"struct.std::__2::array.147"* @_ZNSt3__25arrayIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEELm3EEC2Ev(%"struct.std::__2::array.147"* %new_face)
  %call3 = call %"class.draco::IndexType.153"* @_ZN5draco9IndexTypeIjNS_19FaceIndex_tag_type_EEC2Ej(%"class.draco::IndexType.153"* %i, i32 0)
  br label %for.cond

for.cond:                                         ; preds = %for.inc16, %entry
  %2 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %mesh.addr, align 4
  %call4 = call i32 @_ZNK5draco4Mesh9num_facesEv(%"class.draco::Mesh"* %2)
  store i32 %call4, i32* %ref.tmp, align 4
  %call5 = call zeroext i1 @_ZNK5draco9IndexTypeIjNS_19FaceIndex_tag_type_EEltERKj(%"class.draco::IndexType.153"* %i, i32* nonnull align 4 dereferenceable(4) %ref.tmp)
  br i1 %call5, label %for.body, label %for.end18

for.body:                                         ; preds = %for.cond
  %3 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %mesh.addr, align 4
  %4 = bitcast %"class.draco::IndexType.153"* %agg.tmp to i8*
  %5 = bitcast %"class.draco::IndexType.153"* %i to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 4, i1 false)
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType.153", %"class.draco::IndexType.153"* %agg.tmp, i32 0, i32 0
  %6 = load i32, i32* %coerce.dive, align 4
  %call6 = call nonnull align 4 dereferenceable(12) %"struct.std::__2::array"* @_ZNK5draco4Mesh4faceENS_9IndexTypeIjNS_19FaceIndex_tag_type_EEE(%"class.draco::Mesh"* %3, i32 %6)
  store %"struct.std::__2::array"* %call6, %"struct.std::__2::array"** %face, align 4
  store i32 0, i32* %j, align 4
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc, %for.body
  %7 = load i32, i32* %j, align 4
  %cmp = icmp slt i32 %7, 3
  br i1 %cmp, label %for.body8, label %for.end

for.body8:                                        ; preds = %for.cond7
  %8 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %face, align 4
  %9 = load i32, i32* %j, align 4
  %call10 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.104"* @_ZNKSt3__25arrayIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELm3EEixEm(%"struct.std::__2::array"* %8, i32 %9) #8
  %call11 = call i32 @_ZNK5draco9IndexTypeIjNS_20PointIndex_tag_type_EE5valueEv(%"class.draco::IndexType.104"* %call10)
  store i32 %call11, i32* %ref.tmp9, align 4
  %10 = load i32, i32* %j, align 4
  %call12 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.116"* @_ZNSt3__25arrayIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEELm3EEixEm(%"struct.std::__2::array.147"* %new_face, i32 %10) #8
  %call13 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.116"* @_ZN5draco9IndexTypeIjNS_21VertexIndex_tag_type_EEaSERKj(%"class.draco::IndexType.116"* %call12, i32* nonnull align 4 dereferenceable(4) %ref.tmp9)
  br label %for.inc

for.inc:                                          ; preds = %for.body8
  %11 = load i32, i32* %j, align 4
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond7

for.end:                                          ; preds = %for.cond7
  %call14 = call nonnull align 4 dereferenceable(12) %"struct.std::__2::array.147"* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEENSt3__25arrayINS1_IjNS_21VertexIndex_tag_type_EEELm3EEEEixERKS3_(%"class.draco::IndexTypeVector.144"* %faces, %"class.draco::IndexType.153"* nonnull align 4 dereferenceable(4) %i)
  %call15 = call nonnull align 4 dereferenceable(12) %"struct.std::__2::array.147"* @_ZNSt3__25arrayIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEELm3EEaSERKS5_(%"struct.std::__2::array.147"* %call14, %"struct.std::__2::array.147"* nonnull align 4 dereferenceable(12) %new_face)
  br label %for.inc16

for.inc16:                                        ; preds = %for.end
  %call17 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.153"* @_ZN5draco9IndexTypeIjNS_19FaceIndex_tag_type_EEppEv(%"class.draco::IndexType.153"* %i)
  br label %for.cond

for.end18:                                        ; preds = %for.cond
  call void @_ZN5draco11CornerTable6CreateERKNS_15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEENSt3__25arrayINS2_IjNS_21VertexIndex_tag_type_EEELm3EEEEE(%"class.std::__2::unique_ptr.110"* sret align 4 %agg.result, %"class.draco::IndexTypeVector.144"* nonnull align 4 dereferenceable(12) %faces)
  %call19 = call %"class.draco::IndexTypeVector.144"* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEENSt3__25arrayINS1_IjNS_21VertexIndex_tag_type_EEELm3EEEED2Ev(%"class.draco::IndexTypeVector.144"* %faces) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco9IndexTypeIjNS_20PointIndex_tag_type_EE5valueEv(%"class.draco::IndexType.104"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.104"*, align 4
  store %"class.draco::IndexType.104"* %this, %"class.draco::IndexType.104"** %this.addr, align 4
  %this1 = load %"class.draco::IndexType.104"*, %"class.draco::IndexType.104"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.104", %"class.draco::IndexType.104"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value_, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.111"* @_ZNSt3__217__compressed_pairIPN5draco11CornerTableENS_14default_deleteIS2_EEEC2IS3_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.111"* returned %this, %"class.draco::CornerTable"** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.111"*, align 4
  %__t1.addr = alloca %"class.draco::CornerTable"**, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.111"* %this, %"class.std::__2::__compressed_pair.111"** %this.addr, align 4
  store %"class.draco::CornerTable"** %__t1, %"class.draco::CornerTable"*** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.111"*, %"class.std::__2::__compressed_pair.111"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.111"* %this1 to %"struct.std::__2::__compressed_pair_elem.112"*
  %1 = load %"class.draco::CornerTable"**, %"class.draco::CornerTable"*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::CornerTable"** @_ZNSt3__27forwardIPN5draco11CornerTableEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::CornerTable"** nonnull align 4 dereferenceable(4) %1) #8
  %call2 = call %"struct.std::__2::__compressed_pair_elem.112"* @_ZNSt3__222__compressed_pair_elemIPN5draco11CornerTableELi0ELb0EEC2IS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.112"* %0, %"class.draco::CornerTable"** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.111"* %this1 to %"struct.std::__2::__compressed_pair_elem.142"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #8
  %call4 = call %"struct.std::__2::__compressed_pair_elem.142"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco11CornerTableEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.142"* %2)
  ret %"class.std::__2::__compressed_pair.111"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::CornerTable"** @_ZNSt3__27forwardIPN5draco11CornerTableEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::CornerTable"** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.draco::CornerTable"**, align 4
  store %"class.draco::CornerTable"** %__t, %"class.draco::CornerTable"*** %__t.addr, align 4
  %0 = load %"class.draco::CornerTable"**, %"class.draco::CornerTable"*** %__t.addr, align 4
  ret %"class.draco::CornerTable"** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.112"* @_ZNSt3__222__compressed_pair_elemIPN5draco11CornerTableELi0ELb0EEC2IS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.112"* returned %this, %"class.draco::CornerTable"** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.112"*, align 4
  %__u.addr = alloca %"class.draco::CornerTable"**, align 4
  store %"struct.std::__2::__compressed_pair_elem.112"* %this, %"struct.std::__2::__compressed_pair_elem.112"** %this.addr, align 4
  store %"class.draco::CornerTable"** %__u, %"class.draco::CornerTable"*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.112"*, %"struct.std::__2::__compressed_pair_elem.112"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.112", %"struct.std::__2::__compressed_pair_elem.112"* %this1, i32 0, i32 0
  %0 = load %"class.draco::CornerTable"**, %"class.draco::CornerTable"*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::CornerTable"** @_ZNSt3__27forwardIPN5draco11CornerTableEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::CornerTable"** nonnull align 4 dereferenceable(4) %0) #8
  %1 = load %"class.draco::CornerTable"*, %"class.draco::CornerTable"** %call, align 4
  store %"class.draco::CornerTable"* %1, %"class.draco::CornerTable"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.112"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  store %"struct.std::__2::__default_init_tag"* %__t, %"struct.std::__2::__default_init_tag"** %__t.addr, align 4
  %0 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t.addr, align 4
  ret %"struct.std::__2::__default_init_tag"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.142"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco11CornerTableEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.142"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.142"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.142"* %this, %"struct.std::__2::__compressed_pair_elem.142"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.142"*, %"struct.std::__2::__compressed_pair_elem.142"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.142"* %this1 to %"struct.std::__2::default_delete.143"*
  ret %"struct.std::__2::__compressed_pair_elem.142"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEENSt3__25arrayINS1_IjNS_20PointIndex_tag_type_EEELm3EEEE4sizeEv(%"class.draco::IndexTypeVector.101"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector.101"*, align 4
  store %"class.draco::IndexTypeVector.101"* %this, %"class.draco::IndexTypeVector.101"** %this.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector.101"*, %"class.draco::IndexTypeVector.101"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector.101", %"class.draco::IndexTypeVector.101"* %this1, i32 0, i32 0
  %call = call i32 @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.102"* %vector_) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.102"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.102"*, align 4
  store %"class.std::__2::vector.102"* %this, %"class.std::__2::vector.102"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.102"*, %"class.std::__2::vector.102"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.102"* %this1 to %"class.std::__2::__vector_base.103"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.103", %"class.std::__2::__vector_base.103"* %0, i32 0, i32 1
  %1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector.102"* %this1 to %"class.std::__2::__vector_base.103"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.103", %"class.std::__2::__vector_base.103"* %2, i32 0, i32 0
  %3 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"struct.std::__2::array"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"struct.std::__2::array"* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 12
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType.116"* @_ZN5draco9IndexTypeIjNS_21VertexIndex_tag_type_EEC2Ev(%"class.draco::IndexType.116"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.116"*, align 4
  store %"class.draco::IndexType.116"* %this, %"class.draco::IndexType.116"** %this.addr, align 4
  %this1 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.116", %"class.draco::IndexType.116"* %this1, i32 0, i32 0
  store i32 0, i32* %value_, align 4
  ret %"class.draco::IndexType.116"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"struct.std::__2::array"* @_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEENSt3__25arrayINS1_IjNS_20PointIndex_tag_type_EEELm3EEEEixERKS3_(%"class.draco::IndexTypeVector.101"* %this, %"class.draco::IndexType.153"* nonnull align 4 dereferenceable(4) %index) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector.101"*, align 4
  %index.addr = alloca %"class.draco::IndexType.153"*, align 4
  store %"class.draco::IndexTypeVector.101"* %this, %"class.draco::IndexTypeVector.101"** %this.addr, align 4
  store %"class.draco::IndexType.153"* %index, %"class.draco::IndexType.153"** %index.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector.101"*, %"class.draco::IndexTypeVector.101"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector.101", %"class.draco::IndexTypeVector.101"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexType.153"*, %"class.draco::IndexType.153"** %index.addr, align 4
  %call = call i32 @_ZNK5draco9IndexTypeIjNS_19FaceIndex_tag_type_EE5valueEv(%"class.draco::IndexType.153"* %0)
  %call2 = call nonnull align 4 dereferenceable(12) %"struct.std::__2::array"* @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEEixEm(%"class.std::__2::vector.102"* %vector_, i32 %call) #8
  ret %"struct.std::__2::array"* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"struct.std::__2::array"* @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEEixEm(%"class.std::__2::vector.102"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.102"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector.102"* %this, %"class.std::__2::vector.102"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.102"*, %"class.std::__2::vector.102"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.102"* %this1 to %"class.std::__2::__vector_base.103"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.103", %"class.std::__2::__vector_base.103"* %0, i32 0, i32 0
  %1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__begin_, align 4
  %2 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %1, i32 %2
  ret %"struct.std::__2::array"* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco9IndexTypeIjNS_19FaceIndex_tag_type_EE5valueEv(%"class.draco::IndexType.153"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.153"*, align 4
  store %"class.draco::IndexType.153"* %this, %"class.draco::IndexType.153"** %this.addr, align 4
  %this1 = load %"class.draco::IndexType.153"*, %"class.draco::IndexType.153"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.153", %"class.draco::IndexType.153"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value_, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType"* @_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEC2Ej(%"class.draco::IndexType"* returned %this, i32 %value) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType"*, align 4
  %value.addr = alloca i32, align 4
  store %"class.draco::IndexType"* %this, %"class.draco::IndexType"** %this.addr, align 4
  store i32 %value, i32* %value.addr, align 4
  %this1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value.addr, align 4
  store i32 %0, i32* %value_, align 4
  ret %"class.draco::IndexType"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEEEixERKS3_(%"class.draco::IndexTypeVector"* %this, %"class.draco::IndexType.104"* nonnull align 4 dereferenceable(4) %index) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector"*, align 4
  %index.addr = alloca %"class.draco::IndexType.104"*, align 4
  store %"class.draco::IndexTypeVector"* %this, %"class.draco::IndexTypeVector"** %this.addr, align 4
  store %"class.draco::IndexType.104"* %index, %"class.draco::IndexType.104"** %index.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector"*, %"class.draco::IndexTypeVector"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector", %"class.draco::IndexTypeVector"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexType.104"*, %"class.draco::IndexType.104"** %index.addr, align 4
  %call = call i32 @_ZNK5draco9IndexTypeIjNS_20PointIndex_tag_type_EE5valueEv(%"class.draco::IndexType.104"* %0)
  %call2 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEEixEm(%"class.std::__2::vector.68"* %vector_, i32 %call) #8
  ret %"class.draco::IndexType"* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEEixEm(%"class.std::__2::vector.68"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.68"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector.68"* %this, %"class.std::__2::vector.68"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.68"* %this1 to %"class.std::__2::__vector_base.69"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %0, i32 0, i32 0
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  %2 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %1, i32 %2
  ret %"class.draco::IndexType"* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.116"* @_ZN5draco9IndexTypeIjNS_21VertexIndex_tag_type_EEaSERKS2_(%"class.draco::IndexType.116"* %this, %"class.draco::IndexType.116"* nonnull align 4 dereferenceable(4) %i) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.116"*, align 4
  %i.addr = alloca %"class.draco::IndexType.116"*, align 4
  store %"class.draco::IndexType.116"* %this, %"class.draco::IndexType.116"** %this.addr, align 4
  store %"class.draco::IndexType.116"* %i, %"class.draco::IndexType.116"** %i.addr, align 4
  %this1 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %this.addr, align 4
  %0 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %i.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.116", %"class.draco::IndexType.116"* %0, i32 0, i32 0
  %1 = load i32, i32* %value_, align 4
  %value_2 = getelementptr inbounds %"class.draco::IndexType.116", %"class.draco::IndexType.116"* %this1, i32 0, i32 0
  store i32 %1, i32* %value_2, align 4
  ret %"class.draco::IndexType.116"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector.145"* @_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEED2Ev(%"class.std::__2::vector.145"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.145"*, align 4
  store %"class.std::__2::vector.145"* %this, %"class.std::__2::vector.145"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.145"*, %"class.std::__2::vector.145"** %this.addr, align 4
  call void @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE17__annotate_deleteEv(%"class.std::__2::vector.145"* %this1) #8
  %0 = bitcast %"class.std::__2::vector.145"* %this1 to %"class.std::__2::__vector_base.146"*
  %call = call %"class.std::__2::__vector_base.146"* @_ZNSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEED2Ev(%"class.std::__2::__vector_base.146"* %0) #8
  ret %"class.std::__2::vector.145"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE17__annotate_deleteEv(%"class.std::__2::vector.145"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.145"*, align 4
  store %"class.std::__2::vector.145"* %this, %"class.std::__2::vector.145"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.145"*, %"class.std::__2::vector.145"** %this.addr, align 4
  %call = call %"struct.std::__2::array.147"* @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.145"* %this1) #8
  %0 = bitcast %"struct.std::__2::array.147"* %call to i8*
  %call2 = call %"struct.std::__2::array.147"* @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.145"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector.145"* %this1) #8
  %add.ptr = getelementptr inbounds %"struct.std::__2::array.147", %"struct.std::__2::array.147"* %call2, i32 %call3
  %1 = bitcast %"struct.std::__2::array.147"* %add.ptr to i8*
  %call4 = call %"struct.std::__2::array.147"* @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.145"* %this1) #8
  %call5 = call i32 @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.145"* %this1) #8
  %add.ptr6 = getelementptr inbounds %"struct.std::__2::array.147", %"struct.std::__2::array.147"* %call4, i32 %call5
  %2 = bitcast %"struct.std::__2::array.147"* %add.ptr6 to i8*
  %call7 = call %"struct.std::__2::array.147"* @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.145"* %this1) #8
  %call8 = call i32 @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector.145"* %this1) #8
  %add.ptr9 = getelementptr inbounds %"struct.std::__2::array.147", %"struct.std::__2::array.147"* %call7, i32 %call8
  %3 = bitcast %"struct.std::__2::array.147"* %add.ptr9 to i8*
  call void @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE31__annotate_contiguous_containerEPKvSB_SB_SB_(%"class.std::__2::vector.145"* %this1, i8* %0, i8* %1, i8* %2, i8* %3) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base.146"* @_ZNSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEED2Ev(%"class.std::__2::__vector_base.146"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__vector_base.146"*, align 4
  %this.addr = alloca %"class.std::__2::__vector_base.146"*, align 4
  store %"class.std::__2::__vector_base.146"* %this, %"class.std::__2::__vector_base.146"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.146"*, %"class.std::__2::__vector_base.146"** %this.addr, align 4
  store %"class.std::__2::__vector_base.146"* %this1, %"class.std::__2::__vector_base.146"** %retval, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.146", %"class.std::__2::__vector_base.146"* %this1, i32 0, i32 0
  %0 = load %"struct.std::__2::array.147"*, %"struct.std::__2::array.147"** %__begin_, align 4
  %cmp = icmp ne %"struct.std::__2::array.147"* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE5clearEv(%"class.std::__2::__vector_base.146"* %this1) #8
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.151"* @_ZNSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base.146"* %this1) #8
  %__begin_2 = getelementptr inbounds %"class.std::__2::__vector_base.146", %"class.std::__2::__vector_base.146"* %this1, i32 0, i32 0
  %1 = load %"struct.std::__2::array.147"*, %"struct.std::__2::array.147"** %__begin_2, align 4
  %call3 = call i32 @_ZNKSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::__vector_base.146"* %this1) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_21VertexIndex_tag_type_EEELm3EEEEEE10deallocateERS8_PS7_m(%"class.std::__2::allocator.151"* nonnull align 1 dereferenceable(1) %call, %"struct.std::__2::array.147"* %1, i32 %call3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"class.std::__2::__vector_base.146"*, %"class.std::__2::__vector_base.146"** %retval, align 4
  ret %"class.std::__2::__vector_base.146"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE31__annotate_contiguous_containerEPKvSB_SB_SB_(%"class.std::__2::vector.145"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.145"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector.145"* %this, %"class.std::__2::vector.145"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector.145"*, %"class.std::__2::vector.145"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::array.147"* @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.145"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.145"*, align 4
  store %"class.std::__2::vector.145"* %this, %"class.std::__2::vector.145"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.145"*, %"class.std::__2::vector.145"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.145"* %this1 to %"class.std::__2::__vector_base.146"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.146", %"class.std::__2::__vector_base.146"* %0, i32 0, i32 0
  %1 = load %"struct.std::__2::array.147"*, %"struct.std::__2::array.147"** %__begin_, align 4
  %call = call %"struct.std::__2::array.147"* @_ZNSt3__212__to_addressINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEEEEPT_S8_(%"struct.std::__2::array.147"* %1) #8
  ret %"struct.std::__2::array.147"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector.145"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.145"*, align 4
  store %"class.std::__2::vector.145"* %this, %"class.std::__2::vector.145"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.145"*, %"class.std::__2::vector.145"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.145"* %this1 to %"class.std::__2::__vector_base.146"*
  %call = call i32 @_ZNKSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::__vector_base.146"* %0) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.145"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.145"*, align 4
  store %"class.std::__2::vector.145"* %this, %"class.std::__2::vector.145"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.145"*, %"class.std::__2::vector.145"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.145"* %this1 to %"class.std::__2::__vector_base.146"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.146", %"class.std::__2::__vector_base.146"* %0, i32 0, i32 1
  %1 = load %"struct.std::__2::array.147"*, %"struct.std::__2::array.147"** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector.145"* %this1 to %"class.std::__2::__vector_base.146"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.146", %"class.std::__2::__vector_base.146"* %2, i32 0, i32 0
  %3 = load %"struct.std::__2::array.147"*, %"struct.std::__2::array.147"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"struct.std::__2::array.147"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"struct.std::__2::array.147"* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 12
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::array.147"* @_ZNSt3__212__to_addressINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEEEEPT_S8_(%"struct.std::__2::array.147"* %__p) #0 comdat {
entry:
  %__p.addr = alloca %"struct.std::__2::array.147"*, align 4
  store %"struct.std::__2::array.147"* %__p, %"struct.std::__2::array.147"** %__p.addr, align 4
  %0 = load %"struct.std::__2::array.147"*, %"struct.std::__2::array.147"** %__p.addr, align 4
  ret %"struct.std::__2::array.147"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::__vector_base.146"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.146"*, align 4
  store %"class.std::__2::__vector_base.146"* %this, %"class.std::__2::__vector_base.146"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.146"*, %"class.std::__2::__vector_base.146"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::array.147"** @_ZNKSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE9__end_capEv(%"class.std::__2::__vector_base.146"* %this1) #8
  %0 = load %"struct.std::__2::array.147"*, %"struct.std::__2::array.147"** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.146", %"class.std::__2::__vector_base.146"* %this1, i32 0, i32 0
  %1 = load %"struct.std::__2::array.147"*, %"struct.std::__2::array.147"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"struct.std::__2::array.147"* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint %"struct.std::__2::array.147"* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 12
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::array.147"** @_ZNKSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE9__end_capEv(%"class.std::__2::__vector_base.146"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.146"*, align 4
  store %"class.std::__2::__vector_base.146"* %this, %"class.std::__2::__vector_base.146"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.146"*, %"class.std::__2::__vector_base.146"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.146", %"class.std::__2::__vector_base.146"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::array.147"** @_ZNKSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.148"* %__end_cap_) #8
  ret %"struct.std::__2::array.147"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::array.147"** @_ZNKSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.148"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.148"*, align 4
  store %"class.std::__2::__compressed_pair.148"* %this, %"class.std::__2::__compressed_pair.148"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.148"*, %"class.std::__2::__compressed_pair.148"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.148"* %this1 to %"struct.std::__2::__compressed_pair_elem.149"*
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::array.147"** @_ZNKSt3__222__compressed_pair_elemIPNS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.149"* %0) #8
  ret %"struct.std::__2::array.147"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::array.147"** @_ZNKSt3__222__compressed_pair_elemIPNS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.149"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.149"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.149"* %this, %"struct.std::__2::__compressed_pair_elem.149"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.149"*, %"struct.std::__2::__compressed_pair_elem.149"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.149", %"struct.std::__2::__compressed_pair_elem.149"* %this1, i32 0, i32 0
  ret %"struct.std::__2::array.147"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE5clearEv(%"class.std::__2::__vector_base.146"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.146"*, align 4
  store %"class.std::__2::__vector_base.146"* %this, %"class.std::__2::__vector_base.146"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.146"*, %"class.std::__2::__vector_base.146"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.146", %"class.std::__2::__vector_base.146"* %this1, i32 0, i32 0
  %0 = load %"struct.std::__2::array.147"*, %"struct.std::__2::array.147"** %__begin_, align 4
  call void @_ZNSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE17__destruct_at_endEPS6_(%"class.std::__2::__vector_base.146"* %this1, %"struct.std::__2::array.147"* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_21VertexIndex_tag_type_EEELm3EEEEEE10deallocateERS8_PS7_m(%"class.std::__2::allocator.151"* nonnull align 1 dereferenceable(1) %__a, %"struct.std::__2::array.147"* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.151"*, align 4
  %__p.addr = alloca %"struct.std::__2::array.147"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.151"* %__a, %"class.std::__2::allocator.151"** %__a.addr, align 4
  store %"struct.std::__2::array.147"* %__p, %"struct.std::__2::array.147"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.151"*, %"class.std::__2::allocator.151"** %__a.addr, align 4
  %1 = load %"struct.std::__2::array.147"*, %"struct.std::__2::array.147"** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEEE10deallocateEPS6_m(%"class.std::__2::allocator.151"* %0, %"struct.std::__2::array.147"* %1, i32 %2) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.151"* @_ZNSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base.146"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.146"*, align 4
  store %"class.std::__2::__vector_base.146"* %this, %"class.std::__2::__vector_base.146"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.146"*, %"class.std::__2::__vector_base.146"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.146", %"class.std::__2::__vector_base.146"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.151"* @_ZNSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE6secondEv(%"class.std::__2::__compressed_pair.148"* %__end_cap_) #8
  ret %"class.std::__2::allocator.151"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE17__destruct_at_endEPS6_(%"class.std::__2::__vector_base.146"* %this, %"struct.std::__2::array.147"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.146"*, align 4
  %__new_last.addr = alloca %"struct.std::__2::array.147"*, align 4
  %__soon_to_be_end = alloca %"struct.std::__2::array.147"*, align 4
  store %"class.std::__2::__vector_base.146"* %this, %"class.std::__2::__vector_base.146"** %this.addr, align 4
  store %"struct.std::__2::array.147"* %__new_last, %"struct.std::__2::array.147"** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.146"*, %"class.std::__2::__vector_base.146"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.146", %"class.std::__2::__vector_base.146"* %this1, i32 0, i32 1
  %0 = load %"struct.std::__2::array.147"*, %"struct.std::__2::array.147"** %__end_, align 4
  store %"struct.std::__2::array.147"* %0, %"struct.std::__2::array.147"** %__soon_to_be_end, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load %"struct.std::__2::array.147"*, %"struct.std::__2::array.147"** %__new_last.addr, align 4
  %2 = load %"struct.std::__2::array.147"*, %"struct.std::__2::array.147"** %__soon_to_be_end, align 4
  %cmp = icmp ne %"struct.std::__2::array.147"* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.151"* @_ZNSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base.146"* %this1) #8
  %3 = load %"struct.std::__2::array.147"*, %"struct.std::__2::array.147"** %__soon_to_be_end, align 4
  %incdec.ptr = getelementptr inbounds %"struct.std::__2::array.147", %"struct.std::__2::array.147"* %3, i32 -1
  store %"struct.std::__2::array.147"* %incdec.ptr, %"struct.std::__2::array.147"** %__soon_to_be_end, align 4
  %call2 = call %"struct.std::__2::array.147"* @_ZNSt3__212__to_addressINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEEEEPT_S8_(%"struct.std::__2::array.147"* %incdec.ptr) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_21VertexIndex_tag_type_EEELm3EEEEEE7destroyIS7_EEvRS8_PT_(%"class.std::__2::allocator.151"* nonnull align 1 dereferenceable(1) %call, %"struct.std::__2::array.147"* %call2)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %4 = load %"struct.std::__2::array.147"*, %"struct.std::__2::array.147"** %__new_last.addr, align 4
  %__end_3 = getelementptr inbounds %"class.std::__2::__vector_base.146", %"class.std::__2::__vector_base.146"* %this1, i32 0, i32 1
  store %"struct.std::__2::array.147"* %4, %"struct.std::__2::array.147"** %__end_3, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_21VertexIndex_tag_type_EEELm3EEEEEE7destroyIS7_EEvRS8_PT_(%"class.std::__2::allocator.151"* nonnull align 1 dereferenceable(1) %__a, %"struct.std::__2::array.147"* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.151"*, align 4
  %__p.addr = alloca %"struct.std::__2::array.147"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy", align 1
  store %"class.std::__2::allocator.151"* %__a, %"class.std::__2::allocator.151"** %__a.addr, align 4
  store %"struct.std::__2::array.147"* %__p, %"struct.std::__2::array.147"** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.151"*, %"class.std::__2::allocator.151"** %__a.addr, align 4
  %2 = load %"struct.std::__2::array.147"*, %"struct.std::__2::array.147"** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_21VertexIndex_tag_type_EEELm3EEEEEE9__destroyIS7_EEvNS_17integral_constantIbLb1EEERS8_PT_(%"class.std::__2::allocator.151"* nonnull align 1 dereferenceable(1) %1, %"struct.std::__2::array.147"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_21VertexIndex_tag_type_EEELm3EEEEEE9__destroyIS7_EEvNS_17integral_constantIbLb1EEERS8_PT_(%"class.std::__2::allocator.151"* nonnull align 1 dereferenceable(1) %__a, %"struct.std::__2::array.147"* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.151"*, align 4
  %__p.addr = alloca %"struct.std::__2::array.147"*, align 4
  store %"class.std::__2::allocator.151"* %__a, %"class.std::__2::allocator.151"** %__a.addr, align 4
  store %"struct.std::__2::array.147"* %__p, %"struct.std::__2::array.147"** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.151"*, %"class.std::__2::allocator.151"** %__a.addr, align 4
  %2 = load %"struct.std::__2::array.147"*, %"struct.std::__2::array.147"** %__p.addr, align 4
  call void @_ZNSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEEE7destroyEPS6_(%"class.std::__2::allocator.151"* %1, %"struct.std::__2::array.147"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEEE7destroyEPS6_(%"class.std::__2::allocator.151"* %this, %"struct.std::__2::array.147"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.151"*, align 4
  %__p.addr = alloca %"struct.std::__2::array.147"*, align 4
  store %"class.std::__2::allocator.151"* %this, %"class.std::__2::allocator.151"** %this.addr, align 4
  store %"struct.std::__2::array.147"* %__p, %"struct.std::__2::array.147"** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.151"*, %"class.std::__2::allocator.151"** %this.addr, align 4
  %0 = load %"struct.std::__2::array.147"*, %"struct.std::__2::array.147"** %__p.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEEE10deallocateEPS6_m(%"class.std::__2::allocator.151"* %this, %"struct.std::__2::array.147"* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.151"*, align 4
  %__p.addr = alloca %"struct.std::__2::array.147"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.151"* %this, %"class.std::__2::allocator.151"** %this.addr, align 4
  store %"struct.std::__2::array.147"* %__p, %"struct.std::__2::array.147"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.151"*, %"class.std::__2::allocator.151"** %this.addr, align 4
  %0 = load %"struct.std::__2::array.147"*, %"struct.std::__2::array.147"** %__p.addr, align 4
  %1 = bitcast %"struct.std::__2::array.147"* %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 12
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %__ptr, i32 %__size, i32 %__align) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  %1 = load i32, i32* %__size.addr, align 4
  %2 = load i32, i32* %__align.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm(i8* %0, i32 %1, i32 %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm(i8* %__ptr, i32 %__size, i32 %__align) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  %1 = load i32, i32* %__size.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm(i8* %0, i32 %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm(i8* %__ptr, i32 %__size) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller9__do_callEPv(i8* %0)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller9__do_callEPv(i8* %__ptr) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  call void @_ZdlPv(i8* %0) #9
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.151"* @_ZNSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE6secondEv(%"class.std::__2::__compressed_pair.148"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.148"*, align 4
  store %"class.std::__2::__compressed_pair.148"* %this, %"class.std::__2::__compressed_pair.148"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.148"*, %"class.std::__2::__compressed_pair.148"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.148"* %this1 to %"struct.std::__2::__compressed_pair_elem.150"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.151"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_21VertexIndex_tag_type_EEELm3EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.150"* %0) #8
  ret %"class.std::__2::allocator.151"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.151"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_21VertexIndex_tag_type_EEELm3EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.150"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.150"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.150"* %this, %"struct.std::__2::__compressed_pair_elem.150"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.150"*, %"struct.std::__2::__compressed_pair_elem.150"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.150"* %this1 to %"class.std::__2::allocator.151"*
  ret %"class.std::__2::allocator.151"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector.145"* @_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEEC2Em(%"class.std::__2::vector.145"* returned %this, i32 %__n) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"class.std::__2::vector.145"*, align 4
  %this.addr = alloca %"class.std::__2::vector.145"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector.145"* %this, %"class.std::__2::vector.145"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.145"*, %"class.std::__2::vector.145"** %this.addr, align 4
  store %"class.std::__2::vector.145"* %this1, %"class.std::__2::vector.145"** %retval, align 4
  %0 = bitcast %"class.std::__2::vector.145"* %this1 to %"class.std::__2::__vector_base.146"*
  %call = call %"class.std::__2::__vector_base.146"* @_ZNSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEEC2Ev(%"class.std::__2::__vector_base.146"* %0) #8
  %1 = load i32, i32* %__n.addr, align 4
  %cmp = icmp ugt i32 %1, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE11__vallocateEm(%"class.std::__2::vector.145"* %this1, i32 %2)
  %3 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE18__construct_at_endEm(%"class.std::__2::vector.145"* %this1, i32 %3)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %4 = load %"class.std::__2::vector.145"*, %"class.std::__2::vector.145"** %retval, align 4
  ret %"class.std::__2::vector.145"* %4
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base.146"* @_ZNSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEEC2Ev(%"class.std::__2::__vector_base.146"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.146"*, align 4
  %ref.tmp = alloca i8*, align 4
  %ref.tmp2 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__vector_base.146"* %this, %"class.std::__2::__vector_base.146"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.146"*, %"class.std::__2::__vector_base.146"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__vector_base.146"* %this1 to %"class.std::__2::__vector_base_common"*
  %call = call %"class.std::__2::__vector_base_common"* @_ZNSt3__220__vector_base_commonILb1EEC2Ev(%"class.std::__2::__vector_base_common"* %0)
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.146", %"class.std::__2::__vector_base.146"* %this1, i32 0, i32 0
  store %"struct.std::__2::array.147"* null, %"struct.std::__2::array.147"** %__begin_, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.146", %"class.std::__2::__vector_base.146"* %this1, i32 0, i32 1
  store %"struct.std::__2::array.147"* null, %"struct.std::__2::array.147"** %__end_, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.146", %"class.std::__2::__vector_base.146"* %this1, i32 0, i32 2
  store i8* null, i8** %ref.tmp, align 4
  %call3 = call %"class.std::__2::__compressed_pair.148"* @_ZNSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEEC2IDnNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.148"* %__end_cap_, i8** nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  ret %"class.std::__2::__vector_base.146"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE11__vallocateEm(%"class.std::__2::vector.145"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.145"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector.145"* %this, %"class.std::__2::vector.145"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.145"*, %"class.std::__2::vector.145"** %this.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE8max_sizeEv(%"class.std::__2::vector.145"* %this1) #8
  %cmp = icmp ugt i32 %0, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %"class.std::__2::vector.145"* %this1 to %"class.std::__2::__vector_base_common"*
  call void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"* %1) #10
  unreachable

if.end:                                           ; preds = %entry
  %2 = bitcast %"class.std::__2::vector.145"* %this1 to %"class.std::__2::__vector_base.146"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.151"* @_ZNSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base.146"* %2) #8
  %3 = load i32, i32* %__n.addr, align 4
  %call3 = call %"struct.std::__2::array.147"* @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_21VertexIndex_tag_type_EEELm3EEEEEE8allocateERS8_m(%"class.std::__2::allocator.151"* nonnull align 1 dereferenceable(1) %call2, i32 %3)
  %4 = bitcast %"class.std::__2::vector.145"* %this1 to %"class.std::__2::__vector_base.146"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.146", %"class.std::__2::__vector_base.146"* %4, i32 0, i32 1
  store %"struct.std::__2::array.147"* %call3, %"struct.std::__2::array.147"** %__end_, align 4
  %5 = bitcast %"class.std::__2::vector.145"* %this1 to %"class.std::__2::__vector_base.146"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.146", %"class.std::__2::__vector_base.146"* %5, i32 0, i32 0
  store %"struct.std::__2::array.147"* %call3, %"struct.std::__2::array.147"** %__begin_, align 4
  %6 = bitcast %"class.std::__2::vector.145"* %this1 to %"class.std::__2::__vector_base.146"*
  %__begin_4 = getelementptr inbounds %"class.std::__2::__vector_base.146", %"class.std::__2::__vector_base.146"* %6, i32 0, i32 0
  %7 = load %"struct.std::__2::array.147"*, %"struct.std::__2::array.147"** %__begin_4, align 4
  %8 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds %"struct.std::__2::array.147", %"struct.std::__2::array.147"* %7, i32 %8
  %9 = bitcast %"class.std::__2::vector.145"* %this1 to %"class.std::__2::__vector_base.146"*
  %call5 = call nonnull align 4 dereferenceable(4) %"struct.std::__2::array.147"** @_ZNSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE9__end_capEv(%"class.std::__2::__vector_base.146"* %9) #8
  store %"struct.std::__2::array.147"* %add.ptr, %"struct.std::__2::array.147"** %call5, align 4
  call void @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE14__annotate_newEm(%"class.std::__2::vector.145"* %this1, i32 0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE18__construct_at_endEm(%"class.std::__2::vector.145"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.145"*, align 4
  %__n.addr = alloca i32, align 4
  %__tx = alloca %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>>>::_ConstructTransaction", align 4
  store %"class.std::__2::vector.145"* %this, %"class.std::__2::vector.145"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.145"*, %"class.std::__2::vector.145"** %this.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %call = call %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>>>::_ConstructTransaction"* @_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE21_ConstructTransactionC2ERS9_m(%"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>>>::_ConstructTransaction"* %__tx, %"class.std::__2::vector.145"* nonnull align 4 dereferenceable(12) %this1, i32 %0)
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %1 = load %"struct.std::__2::array.147"*, %"struct.std::__2::array.147"** %__pos_, align 4
  %__new_end_ = getelementptr inbounds %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>>>::_ConstructTransaction"* %__tx, i32 0, i32 2
  %2 = load %"struct.std::__2::array.147"*, %"struct.std::__2::array.147"** %__new_end_, align 4
  %cmp = icmp ne %"struct.std::__2::array.147"* %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = bitcast %"class.std::__2::vector.145"* %this1 to %"class.std::__2::__vector_base.146"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.151"* @_ZNSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base.146"* %3) #8
  %__pos_3 = getelementptr inbounds %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %4 = load %"struct.std::__2::array.147"*, %"struct.std::__2::array.147"** %__pos_3, align 4
  %call4 = call %"struct.std::__2::array.147"* @_ZNSt3__212__to_addressINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEEEEPT_S8_(%"struct.std::__2::array.147"* %4) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_21VertexIndex_tag_type_EEELm3EEEEEE9constructIS7_JEEEvRS8_PT_DpOT0_(%"class.std::__2::allocator.151"* nonnull align 1 dereferenceable(1) %call2, %"struct.std::__2::array.147"* %call4)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %__pos_5 = getelementptr inbounds %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %5 = load %"struct.std::__2::array.147"*, %"struct.std::__2::array.147"** %__pos_5, align 4
  %incdec.ptr = getelementptr inbounds %"struct.std::__2::array.147", %"struct.std::__2::array.147"* %5, i32 1
  store %"struct.std::__2::array.147"* %incdec.ptr, %"struct.std::__2::array.147"** %__pos_5, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call6 = call %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>>>::_ConstructTransaction"* @_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>>>::_ConstructTransaction"* %__tx) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base_common"* @_ZNSt3__220__vector_base_commonILb1EEC2Ev(%"class.std::__2::__vector_base_common"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base_common"*, align 4
  store %"class.std::__2::__vector_base_common"* %this, %"class.std::__2::__vector_base_common"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base_common"*, %"class.std::__2::__vector_base_common"** %this.addr, align 4
  ret %"class.std::__2::__vector_base_common"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.148"* @_ZNSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEEC2IDnNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.148"* returned %this, i8** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.148"*, align 4
  %__t1.addr = alloca i8**, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.148"* %this, %"class.std::__2::__compressed_pair.148"** %this.addr, align 4
  store i8** %__t1, i8*** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.148"*, %"class.std::__2::__compressed_pair.148"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.148"* %this1 to %"struct.std::__2::__compressed_pair_elem.149"*
  %1 = load i8**, i8*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %1) #8
  %call2 = call %"struct.std::__2::__compressed_pair_elem.149"* @_ZNSt3__222__compressed_pair_elemIPNS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEELi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.149"* %0, i8** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.148"* %this1 to %"struct.std::__2::__compressed_pair_elem.150"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #8
  %call4 = call %"struct.std::__2::__compressed_pair_elem.150"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_21VertexIndex_tag_type_EEELm3EEEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.150"* %2)
  ret %"class.std::__2::__compressed_pair.148"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca i8**, align 4
  store i8** %__t, i8*** %__t.addr, align 4
  %0 = load i8**, i8*** %__t.addr, align 4
  ret i8** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.149"* @_ZNSt3__222__compressed_pair_elemIPNS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEELi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.149"* returned %this, i8** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.149"*, align 4
  %__u.addr = alloca i8**, align 4
  store %"struct.std::__2::__compressed_pair_elem.149"* %this, %"struct.std::__2::__compressed_pair_elem.149"** %this.addr, align 4
  store i8** %__u, i8*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.149"*, %"struct.std::__2::__compressed_pair_elem.149"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.149", %"struct.std::__2::__compressed_pair_elem.149"* %this1, i32 0, i32 0
  %0 = load i8**, i8*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %0) #8
  store %"struct.std::__2::array.147"* null, %"struct.std::__2::array.147"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.149"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.150"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_21VertexIndex_tag_type_EEELm3EEEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.150"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.150"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.150"* %this, %"struct.std::__2::__compressed_pair_elem.150"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.150"*, %"struct.std::__2::__compressed_pair_elem.150"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.150"* %this1 to %"class.std::__2::allocator.151"*
  %call = call %"class.std::__2::allocator.151"* @_ZNSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEEEC2Ev(%"class.std::__2::allocator.151"* %1) #8
  ret %"struct.std::__2::__compressed_pair_elem.150"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::allocator.151"* @_ZNSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEEEC2Ev(%"class.std::__2::allocator.151"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.151"*, align 4
  store %"class.std::__2::allocator.151"* %this, %"class.std::__2::allocator.151"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.151"*, %"class.std::__2::allocator.151"** %this.addr, align 4
  ret %"class.std::__2::allocator.151"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE8max_sizeEv(%"class.std::__2::vector.145"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.145"*, align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp3 = alloca i32, align 4
  store %"class.std::__2::vector.145"* %this, %"class.std::__2::vector.145"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.145"*, %"class.std::__2::vector.145"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.145"* %this1 to %"class.std::__2::__vector_base.146"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.151"* @_ZNKSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base.146"* %0) #8
  %call2 = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_21VertexIndex_tag_type_EEELm3EEEEEE8max_sizeERKS8_(%"class.std::__2::allocator.151"* nonnull align 1 dereferenceable(1) %call) #8
  store i32 %call2, i32* %ref.tmp, align 4
  %call4 = call i32 @_ZNSt3__214numeric_limitsIlE3maxEv() #8
  store i32 %call4, i32* %ref.tmp3, align 4
  %call5 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %1 = load i32, i32* %call5, align 4
  ret i32 %1
}

; Function Attrs: noreturn
declare void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"*) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::array.147"* @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_21VertexIndex_tag_type_EEELm3EEEEEE8allocateERS8_m(%"class.std::__2::allocator.151"* nonnull align 1 dereferenceable(1) %__a, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.151"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.151"* %__a, %"class.std::__2::allocator.151"** %__a.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.151"*, %"class.std::__2::allocator.151"** %__a.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call %"struct.std::__2::array.147"* @_ZNSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEEE8allocateEmPKv(%"class.std::__2::allocator.151"* %0, i32 %1, i8* null)
  ret %"struct.std::__2::array.147"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::array.147"** @_ZNSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE9__end_capEv(%"class.std::__2::__vector_base.146"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.146"*, align 4
  store %"class.std::__2::__vector_base.146"* %this, %"class.std::__2::__vector_base.146"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.146"*, %"class.std::__2::__vector_base.146"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.146", %"class.std::__2::__vector_base.146"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::array.147"** @_ZNSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.148"* %__end_cap_) #8
  ret %"struct.std::__2::array.147"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE14__annotate_newEm(%"class.std::__2::vector.145"* %this, i32 %__current_size) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.145"*, align 4
  %__current_size.addr = alloca i32, align 4
  store %"class.std::__2::vector.145"* %this, %"class.std::__2::vector.145"** %this.addr, align 4
  store i32 %__current_size, i32* %__current_size.addr, align 4
  %this1 = load %"class.std::__2::vector.145"*, %"class.std::__2::vector.145"** %this.addr, align 4
  %call = call %"struct.std::__2::array.147"* @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.145"* %this1) #8
  %0 = bitcast %"struct.std::__2::array.147"* %call to i8*
  %call2 = call %"struct.std::__2::array.147"* @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.145"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector.145"* %this1) #8
  %add.ptr = getelementptr inbounds %"struct.std::__2::array.147", %"struct.std::__2::array.147"* %call2, i32 %call3
  %1 = bitcast %"struct.std::__2::array.147"* %add.ptr to i8*
  %call4 = call %"struct.std::__2::array.147"* @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.145"* %this1) #8
  %call5 = call i32 @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector.145"* %this1) #8
  %add.ptr6 = getelementptr inbounds %"struct.std::__2::array.147", %"struct.std::__2::array.147"* %call4, i32 %call5
  %2 = bitcast %"struct.std::__2::array.147"* %add.ptr6 to i8*
  %call7 = call %"struct.std::__2::array.147"* @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.145"* %this1) #8
  %3 = load i32, i32* %__current_size.addr, align 4
  %add.ptr8 = getelementptr inbounds %"struct.std::__2::array.147", %"struct.std::__2::array.147"* %call7, i32 %3
  %4 = bitcast %"struct.std::__2::array.147"* %add.ptr8 to i8*
  call void @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE31__annotate_contiguous_containerEPKvSB_SB_SB_(%"class.std::__2::vector.145"* %this1, i8* %0, i8* %1, i8* %2, i8* %4) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::__less", align 1
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__a.addr, align 4
  %1 = load i32*, i32** %__b.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_21VertexIndex_tag_type_EEELm3EEEEEE8max_sizeERKS8_(%"class.std::__2::allocator.151"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.151"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_max_size", align 1
  store %"class.std::__2::allocator.151"* %__a, %"class.std::__2::allocator.151"** %__a.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_max_size"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.151"*, %"class.std::__2::allocator.151"** %__a.addr, align 4
  %call = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_21VertexIndex_tag_type_EEELm3EEEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS8_(%"class.std::__2::allocator.151"* nonnull align 1 dereferenceable(1) %1) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.151"* @_ZNKSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base.146"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.146"*, align 4
  store %"class.std::__2::__vector_base.146"* %this, %"class.std::__2::__vector_base.146"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.146"*, %"class.std::__2::__vector_base.146"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.146", %"class.std::__2::__vector_base.146"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.151"* @_ZNKSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE6secondEv(%"class.std::__2::__compressed_pair.148"* %__end_cap_) #8
  ret %"class.std::__2::allocator.151"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__214numeric_limitsIlE3maxEv() #0 comdat {
entry:
  %call = call i32 @_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv() #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__comp = alloca %"struct.std::__2::__less", align 1
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__b.addr, align 4
  %1 = load i32*, i32** %__a.addr, align 4
  %call = call zeroext i1 @_ZNKSt3__26__lessImmEclERKmS3_(%"struct.std::__2::__less"* %__comp, i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  br i1 %call, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32*, i32** %__b.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %3 = load i32*, i32** %__a.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi i32* [ %2, %cond.true ], [ %3, %cond.false ]
  ret i32* %cond-lvalue
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__26__lessImmEclERKmS3_(%"struct.std::__2::__less"* %this, i32* nonnull align 4 dereferenceable(4) %__x, i32* nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__less"*, align 4
  %__x.addr = alloca i32*, align 4
  %__y.addr = alloca i32*, align 4
  store %"struct.std::__2::__less"* %this, %"struct.std::__2::__less"** %this.addr, align 4
  store i32* %__x, i32** %__x.addr, align 4
  store i32* %__y, i32** %__y.addr, align 4
  %this1 = load %"struct.std::__2::__less"*, %"struct.std::__2::__less"** %this.addr, align 4
  %0 = load i32*, i32** %__x.addr, align 4
  %1 = load i32, i32* %0, align 4
  %2 = load i32*, i32** %__y.addr, align 4
  %3 = load i32, i32* %2, align 4
  %cmp = icmp ult i32 %1, %3
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_21VertexIndex_tag_type_EEELm3EEEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS8_(%"class.std::__2::allocator.151"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.151"*, align 4
  store %"class.std::__2::allocator.151"* %__a, %"class.std::__2::allocator.151"** %__a.addr, align 4
  %1 = load %"class.std::__2::allocator.151"*, %"class.std::__2::allocator.151"** %__a.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEEE8max_sizeEv(%"class.std::__2::allocator.151"* %1) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEEE8max_sizeEv(%"class.std::__2::allocator.151"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.151"*, align 4
  store %"class.std::__2::allocator.151"* %this, %"class.std::__2::allocator.151"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.151"*, %"class.std::__2::allocator.151"** %this.addr, align 4
  ret i32 357913941
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.151"* @_ZNKSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE6secondEv(%"class.std::__2::__compressed_pair.148"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.148"*, align 4
  store %"class.std::__2::__compressed_pair.148"* %this, %"class.std::__2::__compressed_pair.148"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.148"*, %"class.std::__2::__compressed_pair.148"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.148"* %this1 to %"struct.std::__2::__compressed_pair_elem.150"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.151"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_21VertexIndex_tag_type_EEELm3EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.150"* %0) #8
  ret %"class.std::__2::allocator.151"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.151"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_21VertexIndex_tag_type_EEELm3EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.150"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.150"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.150"* %this, %"struct.std::__2::__compressed_pair_elem.150"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.150"*, %"struct.std::__2::__compressed_pair_elem.150"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.150"* %this1 to %"class.std::__2::allocator.151"*
  ret %"class.std::__2::allocator.151"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv() #0 comdat {
entry:
  ret i32 2147483647
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::array.147"* @_ZNSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEEE8allocateEmPKv(%"class.std::__2::allocator.151"* %this, i32 %__n, i8* %0) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.151"*, align 4
  %__n.addr = alloca i32, align 4
  %.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.151"* %this, %"class.std::__2::allocator.151"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::allocator.151"*, %"class.std::__2::allocator.151"** %this.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEEE8max_sizeEv(%"class.std::__2::allocator.151"* %this1) #8
  %cmp = icmp ugt i32 %1, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__220__throw_length_errorEPKc(i8* getelementptr inbounds ([68 x i8], [68 x i8]* @.str, i32 0, i32 0)) #10
  unreachable

if.end:                                           ; preds = %entry
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 12
  %call2 = call i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %mul, i32 4)
  %3 = bitcast i8* %call2 to %"struct.std::__2::array.147"*
  ret %"struct.std::__2::array.147"* %3
}

; Function Attrs: noinline noreturn nounwind optnone
define linkonce_odr hidden void @_ZNSt3__220__throw_length_errorEPKc(i8* %__msg) #5 comdat {
entry:
  %__msg.addr = alloca i8*, align 4
  store i8* %__msg, i8** %__msg.addr, align 4
  call void @abort() #10
  unreachable
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %__size, i32 %__align) #0 comdat {
entry:
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i32, i32* %__size.addr, align 4
  %call = call noalias nonnull i8* @_Znwm(i32 %0) #11
  ret i8* %call
}

; Function Attrs: noreturn
declare void @abort() #4

; Function Attrs: nobuiltin allocsize(0)
declare nonnull i8* @_Znwm(i32) #6

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::array.147"** @_ZNSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.148"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.148"*, align 4
  store %"class.std::__2::__compressed_pair.148"* %this, %"class.std::__2::__compressed_pair.148"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.148"*, %"class.std::__2::__compressed_pair.148"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.148"* %this1 to %"struct.std::__2::__compressed_pair_elem.149"*
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::array.147"** @_ZNSt3__222__compressed_pair_elemIPNS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.149"* %0) #8
  ret %"struct.std::__2::array.147"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::array.147"** @_ZNSt3__222__compressed_pair_elemIPNS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.149"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.149"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.149"* %this, %"struct.std::__2::__compressed_pair_elem.149"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.149"*, %"struct.std::__2::__compressed_pair_elem.149"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.149", %"struct.std::__2::__compressed_pair_elem.149"* %this1, i32 0, i32 0
  ret %"struct.std::__2::array.147"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>>>::_ConstructTransaction"* @_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE21_ConstructTransactionC2ERS9_m(%"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>>>::_ConstructTransaction"* returned %this, %"class.std::__2::vector.145"* nonnull align 4 dereferenceable(12) %__v, i32 %__n) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>>>::_ConstructTransaction"*, align 4
  %__v.addr = alloca %"class.std::__2::vector.145"*, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>>>::_ConstructTransaction"** %this.addr, align 4
  store %"class.std::__2::vector.145"* %__v, %"class.std::__2::vector.145"** %__v.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>>>::_ConstructTransaction"*, %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>>>::_ConstructTransaction"** %this.addr, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::vector.145"*, %"class.std::__2::vector.145"** %__v.addr, align 4
  store %"class.std::__2::vector.145"* %0, %"class.std::__2::vector.145"** %__v_, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %1 = load %"class.std::__2::vector.145"*, %"class.std::__2::vector.145"** %__v.addr, align 4
  %2 = bitcast %"class.std::__2::vector.145"* %1 to %"class.std::__2::__vector_base.146"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.146", %"class.std::__2::__vector_base.146"* %2, i32 0, i32 1
  %3 = load %"struct.std::__2::array.147"*, %"struct.std::__2::array.147"** %__end_, align 4
  store %"struct.std::__2::array.147"* %3, %"struct.std::__2::array.147"** %__pos_, align 4
  %__new_end_ = getelementptr inbounds %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>>>::_ConstructTransaction"* %this1, i32 0, i32 2
  %4 = load %"class.std::__2::vector.145"*, %"class.std::__2::vector.145"** %__v.addr, align 4
  %5 = bitcast %"class.std::__2::vector.145"* %4 to %"class.std::__2::__vector_base.146"*
  %__end_2 = getelementptr inbounds %"class.std::__2::__vector_base.146", %"class.std::__2::__vector_base.146"* %5, i32 0, i32 1
  %6 = load %"struct.std::__2::array.147"*, %"struct.std::__2::array.147"** %__end_2, align 4
  %7 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds %"struct.std::__2::array.147", %"struct.std::__2::array.147"* %6, i32 %7
  store %"struct.std::__2::array.147"* %add.ptr, %"struct.std::__2::array.147"** %__new_end_, align 4
  ret %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_21VertexIndex_tag_type_EEELm3EEEEEE9constructIS7_JEEEvRS8_PT_DpOT0_(%"class.std::__2::allocator.151"* nonnull align 1 dereferenceable(1) %__a, %"struct.std::__2::array.147"* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.151"*, align 4
  %__p.addr = alloca %"struct.std::__2::array.147"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_construct", align 1
  store %"class.std::__2::allocator.151"* %__a, %"class.std::__2::allocator.151"** %__a.addr, align 4
  store %"struct.std::__2::array.147"* %__p, %"struct.std::__2::array.147"** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_construct"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.151"*, %"class.std::__2::allocator.151"** %__a.addr, align 4
  %2 = load %"struct.std::__2::array.147"*, %"struct.std::__2::array.147"** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_21VertexIndex_tag_type_EEELm3EEEEEE11__constructIS7_JEEEvNS_17integral_constantIbLb1EEERS8_PT_DpOT0_(%"class.std::__2::allocator.151"* nonnull align 1 dereferenceable(1) %1, %"struct.std::__2::array.147"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>>>::_ConstructTransaction"* @_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>>>::_ConstructTransaction"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>>>::_ConstructTransaction"*, align 4
  store %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>>>::_ConstructTransaction"** %this.addr, align 4
  %this1 = load %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>>>::_ConstructTransaction"*, %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>>>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %0 = load %"struct.std::__2::array.147"*, %"struct.std::__2::array.147"** %__pos_, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::vector.145"*, %"class.std::__2::vector.145"** %__v_, align 4
  %2 = bitcast %"class.std::__2::vector.145"* %1 to %"class.std::__2::__vector_base.146"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.146", %"class.std::__2::__vector_base.146"* %2, i32 0, i32 1
  store %"struct.std::__2::array.147"* %0, %"struct.std::__2::array.147"** %__end_, align 4
  ret %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, 3>>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_21VertexIndex_tag_type_EEELm3EEEEEE11__constructIS7_JEEEvNS_17integral_constantIbLb1EEERS8_PT_DpOT0_(%"class.std::__2::allocator.151"* nonnull align 1 dereferenceable(1) %__a, %"struct.std::__2::array.147"* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.151"*, align 4
  %__p.addr = alloca %"struct.std::__2::array.147"*, align 4
  store %"class.std::__2::allocator.151"* %__a, %"class.std::__2::allocator.151"** %__a.addr, align 4
  store %"struct.std::__2::array.147"* %__p, %"struct.std::__2::array.147"** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.151"*, %"class.std::__2::allocator.151"** %__a.addr, align 4
  %2 = load %"struct.std::__2::array.147"*, %"struct.std::__2::array.147"** %__p.addr, align 4
  call void @_ZNSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEEE9constructIS6_JEEEvPT_DpOT0_(%"class.std::__2::allocator.151"* %1, %"struct.std::__2::array.147"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEEE9constructIS6_JEEEvPT_DpOT0_(%"class.std::__2::allocator.151"* %this, %"struct.std::__2::array.147"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.151"*, align 4
  %__p.addr = alloca %"struct.std::__2::array.147"*, align 4
  store %"class.std::__2::allocator.151"* %this, %"class.std::__2::allocator.151"** %this.addr, align 4
  store %"struct.std::__2::array.147"* %__p, %"struct.std::__2::array.147"** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.151"*, %"class.std::__2::allocator.151"** %this.addr, align 4
  %0 = load %"struct.std::__2::array.147"*, %"struct.std::__2::array.147"** %__p.addr, align 4
  %1 = bitcast %"struct.std::__2::array.147"* %0 to i8*
  %2 = bitcast i8* %1 to %"struct.std::__2::array.147"*
  %3 = bitcast %"struct.std::__2::array.147"* %2 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %3, i8 0, i32 12, i1 false)
  %call = call %"struct.std::__2::array.147"* @_ZNSt3__25arrayIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEELm3EEC2Ev(%"struct.std::__2::array.147"* %2)
  ret void
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #7

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"struct.std::__2::array.147"* @_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEEixEm(%"class.std::__2::vector.145"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.145"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector.145"* %this, %"class.std::__2::vector.145"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.145"*, %"class.std::__2::vector.145"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.145"* %this1 to %"class.std::__2::__vector_base.146"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.146", %"class.std::__2::__vector_base.146"* %0, i32 0, i32 0
  %1 = load %"struct.std::__2::array.147"*, %"struct.std::__2::array.147"** %__begin_, align 4
  %2 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds %"struct.std::__2::array.147", %"struct.std::__2::array.147"* %1, i32 %2
  ret %"struct.std::__2::array.147"* %arrayidx
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn }
attributes #3 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { noreturn "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { noinline noreturn nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nobuiltin allocsize(0) "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { argmemonly nounwind willreturn writeonly }
attributes #8 = { nounwind }
attributes #9 = { builtin nounwind }
attributes #10 = { noreturn }
attributes #11 = { builtin allocsize(0) }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
