; ModuleID = './draco/src/draco/core/quantization_utils.cc'
source_filename = "./draco/src/draco/core/quantization_utils.cc"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%"class.draco::Quantizer" = type { float }
%"class.draco::Dequantizer" = type { float }

@_ZN5draco9QuantizerC1Ev = hidden unnamed_addr alias %"class.draco::Quantizer"* (%"class.draco::Quantizer"*), %"class.draco::Quantizer"* (%"class.draco::Quantizer"*)* @_ZN5draco9QuantizerC2Ev
@_ZN5draco11DequantizerC1Ev = hidden unnamed_addr alias %"class.draco::Dequantizer"* (%"class.draco::Dequantizer"*), %"class.draco::Dequantizer"* (%"class.draco::Dequantizer"*)* @_ZN5draco11DequantizerC2Ev

; Function Attrs: noinline nounwind optnone
define hidden %"class.draco::Quantizer"* @_ZN5draco9QuantizerC2Ev(%"class.draco::Quantizer"* returned %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %"class.draco::Quantizer"*, align 4
  store %"class.draco::Quantizer"* %this, %"class.draco::Quantizer"** %this.addr, align 4
  %this1 = load %"class.draco::Quantizer"*, %"class.draco::Quantizer"** %this.addr, align 4
  %inverse_delta_ = getelementptr inbounds %"class.draco::Quantizer", %"class.draco::Quantizer"* %this1, i32 0, i32 0
  store float 1.000000e+00, float* %inverse_delta_, align 4
  ret %"class.draco::Quantizer"* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN5draco9Quantizer4InitEfi(%"class.draco::Quantizer"* %this, float %range, i32 %max_quantized_value) #0 {
entry:
  %this.addr = alloca %"class.draco::Quantizer"*, align 4
  %range.addr = alloca float, align 4
  %max_quantized_value.addr = alloca i32, align 4
  store %"class.draco::Quantizer"* %this, %"class.draco::Quantizer"** %this.addr, align 4
  store float %range, float* %range.addr, align 4
  store i32 %max_quantized_value, i32* %max_quantized_value.addr, align 4
  %this1 = load %"class.draco::Quantizer"*, %"class.draco::Quantizer"** %this.addr, align 4
  %0 = load i32, i32* %max_quantized_value.addr, align 4
  %conv = sitofp i32 %0 to float
  %1 = load float, float* %range.addr, align 4
  %div = fdiv float %conv, %1
  %inverse_delta_ = getelementptr inbounds %"class.draco::Quantizer", %"class.draco::Quantizer"* %this1, i32 0, i32 0
  store float %div, float* %inverse_delta_, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN5draco9Quantizer4InitEf(%"class.draco::Quantizer"* %this, float %delta) #0 {
entry:
  %this.addr = alloca %"class.draco::Quantizer"*, align 4
  %delta.addr = alloca float, align 4
  store %"class.draco::Quantizer"* %this, %"class.draco::Quantizer"** %this.addr, align 4
  store float %delta, float* %delta.addr, align 4
  %this1 = load %"class.draco::Quantizer"*, %"class.draco::Quantizer"** %this.addr, align 4
  %0 = load float, float* %delta.addr, align 4
  %div = fdiv float 1.000000e+00, %0
  %inverse_delta_ = getelementptr inbounds %"class.draco::Quantizer", %"class.draco::Quantizer"* %this1, i32 0, i32 0
  store float %div, float* %inverse_delta_, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden %"class.draco::Dequantizer"* @_ZN5draco11DequantizerC2Ev(%"class.draco::Dequantizer"* returned %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %"class.draco::Dequantizer"*, align 4
  store %"class.draco::Dequantizer"* %this, %"class.draco::Dequantizer"** %this.addr, align 4
  %this1 = load %"class.draco::Dequantizer"*, %"class.draco::Dequantizer"** %this.addr, align 4
  %delta_ = getelementptr inbounds %"class.draco::Dequantizer", %"class.draco::Dequantizer"* %this1, i32 0, i32 0
  store float 1.000000e+00, float* %delta_, align 4
  ret %"class.draco::Dequantizer"* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden zeroext i1 @_ZN5draco11Dequantizer4InitEfi(%"class.draco::Dequantizer"* %this, float %range, i32 %max_quantized_value) #0 {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::Dequantizer"*, align 4
  %range.addr = alloca float, align 4
  %max_quantized_value.addr = alloca i32, align 4
  store %"class.draco::Dequantizer"* %this, %"class.draco::Dequantizer"** %this.addr, align 4
  store float %range, float* %range.addr, align 4
  store i32 %max_quantized_value, i32* %max_quantized_value.addr, align 4
  %this1 = load %"class.draco::Dequantizer"*, %"class.draco::Dequantizer"** %this.addr, align 4
  %0 = load i32, i32* %max_quantized_value.addr, align 4
  %cmp = icmp sle i32 %0, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %1 = load float, float* %range.addr, align 4
  %2 = load i32, i32* %max_quantized_value.addr, align 4
  %conv = sitofp i32 %2 to float
  %div = fdiv float %1, %conv
  %delta_ = getelementptr inbounds %"class.draco::Dequantizer", %"class.draco::Dequantizer"* %this1, i32 0, i32 0
  store float %div, float* %delta_, align 4
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i1, i1* %retval, align 1
  ret i1 %3
}

; Function Attrs: noinline nounwind optnone
define hidden zeroext i1 @_ZN5draco11Dequantizer4InitEf(%"class.draco::Dequantizer"* %this, float %delta) #0 {
entry:
  %this.addr = alloca %"class.draco::Dequantizer"*, align 4
  %delta.addr = alloca float, align 4
  store %"class.draco::Dequantizer"* %this, %"class.draco::Dequantizer"** %this.addr, align 4
  store float %delta, float* %delta.addr, align 4
  %this1 = load %"class.draco::Dequantizer"*, %"class.draco::Dequantizer"** %this.addr, align 4
  %0 = load float, float* %delta.addr, align 4
  %delta_ = getelementptr inbounds %"class.draco::Dequantizer", %"class.draco::Dequantizer"* %this1, i32 0, i32 0
  store float %0, float* %delta_, align 4
  ret i1 true
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
