; ModuleID = './draco/src/draco/core/options.cc'
source_filename = "./draco/src/draco/core/options.cc"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%"struct.std::__2::piecewise_construct_t" = type { i8 }
%"class.draco::Options" = type { %"class.std::__2::map" }
%"class.std::__2::map" = type { %"class.std::__2::__tree" }
%"class.std::__2::__tree" = type { %"class.std::__2::__tree_end_node"*, %"class.std::__2::__compressed_pair", %"class.std::__2::__compressed_pair.1" }
%"class.std::__2::__tree_end_node" = type { %"class.std::__2::__tree_node_base"* }
%"class.std::__2::__tree_node_base" = type <{ %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_end_node"*, i8, [3 x i8] }>
%"class.std::__2::__compressed_pair" = type { %"struct.std::__2::__compressed_pair_elem" }
%"struct.std::__2::__compressed_pair_elem" = type { %"class.std::__2::__tree_end_node" }
%"class.std::__2::__compressed_pair.1" = type { %"struct.std::__2::__compressed_pair_elem.2" }
%"struct.std::__2::__compressed_pair_elem.2" = type { i32 }
%"class.std::__2::__map_value_compare" = type { i8 }
%"struct.std::__2::less" = type { i8 }
%"class.std::__2::__map_const_iterator" = type { %"class.std::__2::__tree_const_iterator" }
%"class.std::__2::__tree_const_iterator" = type { %"class.std::__2::__tree_end_node"* }
%"struct.std::__2::pair" = type { %"class.std::__2::basic_string", %"class.std::__2::basic_string" }
%"class.std::__2::basic_string" = type { %"class.std::__2::__compressed_pair.4" }
%"class.std::__2::__compressed_pair.4" = type { %"struct.std::__2::__compressed_pair_elem.5" }
%"struct.std::__2::__compressed_pair_elem.5" = type { %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep" }
%"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep" = type { %union.anon }
%union.anon = type { %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long" }
%"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long" = type { i8*, i32, i32 }
%"struct.std::__2::__value_type" = type { %"struct.std::__2::pair" }
%"struct.std::__2::pair.9" = type <{ %"class.std::__2::__tree_iterator", i8, [3 x i8] }>
%"class.std::__2::__tree_iterator" = type { %"class.std::__2::__tree_end_node"* }
%"class.std::__2::tuple" = type { %"struct.std::__2::__tuple_impl" }
%"struct.std::__2::__tuple_impl" = type { %"class.std::__2::__tuple_leaf" }
%"class.std::__2::__tuple_leaf" = type { %"class.std::__2::basic_string"* }
%"class.std::__2::tuple.10" = type { i8 }
%"struct.std::__2::integral_constant" = type { i8 }
%"struct.std::__2::__default_init_tag" = type { i8 }
%"class.std::__2::__basic_string_common" = type { i8 }
%"struct.std::__2::__value_init_tag" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.0" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.3" = type { i8 }
%"class.std::__2::allocator" = type { i8 }
%"class.std::__2::__tree_node" = type { %"class.std::__2::__tree_node_base.base", %"struct.std::__2::__value_type" }
%"class.std::__2::__tree_node_base.base" = type <{ %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_end_node"*, i8 }>
%"class.std::__2::unique_ptr" = type { %"class.std::__2::__compressed_pair.11" }
%"class.std::__2::__compressed_pair.11" = type { %"struct.std::__2::__compressed_pair_elem.12", %"struct.std::__2::__compressed_pair_elem.13" }
%"struct.std::__2::__compressed_pair_elem.12" = type { %"class.std::__2::__tree_node"* }
%"struct.std::__2::__compressed_pair_elem.13" = type { %"class.std::__2::__tree_node_destructor" }
%"class.std::__2::__tree_node_destructor" = type <{ %"class.std::__2::allocator"*, i8, [3 x i8] }>
%"class.std::__2::basic_string_view" = type { i8*, i32 }
%"struct.std::__2::__less" = type { i8 }
%"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short" = type { [11 x i8], %struct.anon }
%struct.anon = type { i8 }
%"struct.std::__2::__has_construct" = type { i8 }
%"struct.std::__2::__tuple_indices" = type { i8 }
%"struct.std::__2::__tuple_indices.14" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.6" = type { i8 }
%"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__raw" = type { [3 x i32] }
%"class.std::__2::allocator.7" = type { i8 }
%"struct.std::__2::integral_constant.15" = type { i8 }
%"struct.std::__2::__has_destroy" = type { i8 }
%"struct.std::__2::__tuple_types" = type { i8 }
%"struct.std::__2::__tuple_types.16" = type { i8 }

$_ZNSt3__23mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_NS_4lessIS6_EENS4_INS_4pairIKS6_S6_EEEEEC2Ev = comdat any

$_ZNKSt3__23mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_NS_4lessIS6_EENS4_INS_4pairIKS6_S6_EEEEE5beginEv = comdat any

$_ZNKSt3__23mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_NS_4lessIS6_EENS4_INS_4pairIKS6_S6_EEEEE3endEv = comdat any

$_ZNSt3__2neERKNS_20__map_const_iteratorINS_21__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPNS_11__tree_nodeIS9_PvEElEEEESH_ = comdat any

$_ZNKSt3__220__map_const_iteratorINS_21__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPNS_11__tree_nodeIS9_PvEElEEEdeEv = comdat any

$_ZNSt3__23mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_NS_4lessIS6_EENS4_INS_4pairIKS6_S6_EEEEEixERSA_ = comdat any

$_ZNSt3__220__map_const_iteratorINS_21__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPNS_11__tree_nodeIS9_PvEElEEEppEv = comdat any

$_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEaSEOS5_ = comdat any

$_ZNKSt3__23mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_NS_4lessIS6_EENS4_INS_4pairIKS6_S6_EEEEE4findERSA_ = comdat any

$_ZNSt3__2eqERKNS_20__map_const_iteratorINS_21__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPNS_11__tree_nodeIS9_PvEElEEEESH_ = comdat any

$_ZNKSt3__220__map_const_iteratorINS_21__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPNS_11__tree_nodeIS9_PvEElEEEptEv = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5c_strEv = comdat any

$_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2IDnEEPKc = comdat any

$_ZNSt3__219__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS6_S6_EENS_4lessIS6_EELb1EEC2ESA_ = comdat any

$_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEEC2ERKSC_ = comdat any

$_ZNSt3__217__compressed_pairINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEENS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS7_IcEEEESE_EES3_EEEEEC2ILb1EvEEv = comdat any

$_ZNSt3__217__compressed_pairImNS_19__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS7_S7_EENS_4lessIS7_EELb1EEEEC2IiRKSC_EEOT_OT0_ = comdat any

$_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE10__end_nodeEv = comdat any

$_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE12__begin_nodeEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEELi0ELb0EEC2ENS_16__value_init_tagE = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEELi1ELb1EEC2ENS_16__value_init_tagE = comdat any

$_ZNSt3__215__tree_end_nodeIPNS_16__tree_node_baseIPvEEEC2Ev = comdat any

$_ZNSt3__29allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEES7_EEPvEEEC2Ev = comdat any

$_ZNSt3__27forwardIiEEOT_RNS_16remove_referenceIS1_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemImLi0ELb0EEC2IivEEOT_ = comdat any

$_ZNSt3__27forwardIRKNS_19__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS7_S7_EENS_4lessIS7_EELb1EEEEEOT_RNS_16remove_referenceISF_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemINS_19__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS7_S7_EENS_4lessIS7_EELb1EEELi1ELb1EEC2IRKSC_vEEOT_ = comdat any

$_ZNSt3__214pointer_traitsIPNS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEEE10pointer_toERS6_ = comdat any

$_ZNSt3__217__compressed_pairINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEENS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS7_IcEEEESE_EES3_EEEEE5firstEv = comdat any

$_ZNSt3__29addressofINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEEEEPT_RS7_ = comdat any

$_ZNSt3__222__compressed_pair_elemINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEELi0ELb0EE5__getEv = comdat any

$_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE5beginEv = comdat any

$_ZNSt3__220__map_const_iteratorINS_21__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPNS_11__tree_nodeIS9_PvEElEEEC2ESE_ = comdat any

$_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE12__begin_nodeEv = comdat any

$_ZNSt3__221__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElEC2EPNS_15__tree_end_nodeIPNS_16__tree_node_baseISA_EEEE = comdat any

$_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE3endEv = comdat any

$_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE10__end_nodeEv = comdat any

$_ZNKSt3__217__compressed_pairINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEENS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS7_IcEEEESE_EES3_EEEEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__2neERKNS_21__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElEESF_ = comdat any

$_ZNSt3__2eqERKNS_21__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElEESF_ = comdat any

$_ZNSt3__221__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElEppEv = comdat any

$_ZNSt3__216__tree_next_iterIPNS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEES5_EET_T0_ = comdat any

$_ZNSt3__210__tree_minIPNS_16__tree_node_baseIPvEEEET_S5_ = comdat any

$_ZNSt3__220__tree_is_left_childIPNS_16__tree_node_baseIPvEEEEbT_ = comdat any

$_ZNKSt3__216__tree_node_baseIPvE15__parent_unsafeEv = comdat any

$_ZNKSt3__221__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElEptEv = comdat any

$_ZNKSt3__212__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_E11__get_valueEv = comdat any

$_ZNSt3__214pointer_traitsIPKNS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEE10pointer_toERS9_ = comdat any

$_ZNKSt3__221__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElE8__get_npEv = comdat any

$_ZNSt3__29addressofIKNS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEEEPT_RSA_ = comdat any

$_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE25__emplace_unique_key_argsIS7_JRKNS_21piecewise_construct_tENS_5tupleIJRKS7_EEENSJ_IJEEEEEENS_4pairINS_15__tree_iteratorIS8_PNS_11__tree_nodeIS8_PvEElEEbEERKT_DpOT0_ = comdat any

$_ZNSt3__216forward_as_tupleIJRKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEEENS_5tupleIJDpOT_EEESC_ = comdat any

$_ZNSt3__216forward_as_tupleIJEEENS_5tupleIJDpOT_EEES4_ = comdat any

$_ZNKSt3__215__tree_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElEptEv = comdat any

$_ZNSt3__212__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_E11__get_valueEv = comdat any

$_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE12__find_equalIS7_EERPNS_16__tree_node_baseIPvEERPNS_15__tree_end_nodeISJ_EERKT_ = comdat any

$_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE16__construct_nodeIJRKNS_21piecewise_construct_tENS_5tupleIJRKS7_EEENSJ_IJEEEEEENS_10unique_ptrINS_11__tree_nodeIS8_PvEENS_22__tree_node_destructorINS5_ISR_EEEEEEDpOT_ = comdat any

$_ZNSt3__27forwardIRKNS_21piecewise_construct_tEEEOT_RNS_16remove_referenceIS4_E4typeE = comdat any

$_ZNSt3__27forwardINS_5tupleIJRKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEEEEEOT_RNS_16remove_referenceISB_E4typeE = comdat any

$_ZNSt3__27forwardINS_5tupleIJEEEEEOT_RNS_16remove_referenceIS3_E4typeE = comdat any

$_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE16__insert_node_atEPNS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEERSJ_SJ_ = comdat any

$_ZNKSt3__210unique_ptrINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEE3getEv = comdat any

$_ZNSt3__210unique_ptrINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEE7releaseEv = comdat any

$_ZNSt3__210unique_ptrINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEED2Ev = comdat any

$_ZNSt3__215__tree_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElEC2ESC_ = comdat any

$_ZNSt3__24pairINS_15__tree_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPNS_11__tree_nodeIS9_PvEElEEbEC2ISE_RbLb0EEEOT_OT0_ = comdat any

$_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE6__rootEv = comdat any

$_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE10__root_ptrEv = comdat any

$_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE10value_compEv = comdat any

$_ZNKSt3__219__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS6_S6_EENS_4lessIS6_EELb1EEclERKS6_RKS8_ = comdat any

$_ZNSt3__29addressofIPNS_16__tree_node_baseIPvEEEEPT_RS5_ = comdat any

$_ZNKSt3__219__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS6_S6_EENS_4lessIS6_EELb1EEclERKS8_RKS6_ = comdat any

$_ZNSt3__217__compressed_pairImNS_19__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS7_S7_EENS_4lessIS7_EELb1EEEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_19__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS7_S7_EENS_4lessIS7_EELb1EEELi1ELb1EE5__getEv = comdat any

$_ZNKSt3__24lessINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEclERKS6_S9_ = comdat any

$_ZNSt3__2ltIcNS_11char_traitsIcEENS_9allocatorIcEEEEbRKNS_12basic_stringIT_T0_T1_EESB_ = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE7compareERKS5_ = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE7compareINS_17basic_string_viewIcS2_EEEENS_9enable_ifIXsr33__can_be_converted_to_string_viewIcS2_T_EE5valueEiE4typeERKSA_ = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEcvNS_17basic_string_viewIcS2_EEEv = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4sizeEv = comdat any

$_ZNKSt3__217basic_string_viewIcNS_11char_traitsIcEEE4sizeEv = comdat any

$_ZNSt3__211char_traitsIcE7compareEPKcS3_m = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv = comdat any

$_ZNKSt3__217basic_string_viewIcNS_11char_traitsIcEEE4dataEv = comdat any

$_ZNSt3__23minImEERKT_S3_S3_ = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE9__is_longEv = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE15__get_long_sizeEv = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE16__get_short_sizeEv = comdat any

$_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EE5__getEv = comdat any

$_ZNSt3__212__to_addressIKcEEPT_S3_ = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE13__get_pointerEv = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE18__get_long_pointerEv = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE19__get_short_pointerEv = comdat any

$_ZNSt3__214pointer_traitsIPKcE10pointer_toERS1_ = comdat any

$_ZNSt3__29addressofIKcEEPT_RS2_ = comdat any

$_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_ = comdat any

$_ZNKSt3__26__lessImmEclERKmS3_ = comdat any

$_ZNSt3__217basic_string_viewIcNS_11char_traitsIcEEEC2EPKcm = comdat any

$_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE12__node_allocEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEEE8allocateERSC_m = comdat any

$_ZNSt3__222__tree_node_destructorINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEEEC2ERSC_b = comdat any

$_ZNSt3__210unique_ptrINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEEC2ILb1EvEEPSB_NS_16__dependent_typeINS_27__unique_ptr_deleter_sfinaeISE_EEXT_EE20__good_rval_ref_typeE = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEEE9constructINS_4pairIKS8_S8_EEJRKNS_21piecewise_construct_tENS_5tupleIJRSG_EEENSL_IJEEEEEEvRSC_PT_DpOT0_ = comdat any

$_ZNSt3__222__tree_key_value_typesINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEE9__get_ptrERS8_ = comdat any

$_ZNKSt3__210unique_ptrINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEEptEv = comdat any

$_ZNSt3__210unique_ptrINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEE11get_deleterEv = comdat any

$_ZNSt3__217__compressed_pairINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEENS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS7_IcEEEESE_EES3_EEEEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__29allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEES7_EEPvEEE8allocateEmPKv = comdat any

$_ZNKSt3__29allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEES7_EEPvEEE8max_sizeEv = comdat any

$_ZNSt3__220__throw_length_errorEPKc = comdat any

$_ZNSt3__217__libcpp_allocateEmm = comdat any

$_ZNSt3__24moveIRNS_22__tree_node_destructorINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS2_IcEEEES9_EEPvEEEEEEEEONS_16remove_referenceIT_E4typeEOSH_ = comdat any

$_ZNSt3__217__compressed_pairIPNS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEEC2IRSC_SF_EEOT_OT0_ = comdat any

$_ZNSt3__27forwardIRPNS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEEEEOT_RNS_16remove_referenceISE_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPNS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEELi0ELb0EEC2IRSC_vEEOT_ = comdat any

$_ZNSt3__27forwardINS_22__tree_node_destructorINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS2_IcEEEES9_EEPvEEEEEEEEOT_RNS_16remove_referenceISF_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemINS_22__tree_node_destructorINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS2_IcEEEES9_EEPvEEEEEELi1ELb0EEC2ISE_vEEOT_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEEE11__constructINS_4pairIKS8_S8_EEJRKNS_21piecewise_construct_tENS_5tupleIJRSG_EEENSL_IJEEEEEEvNS_17integral_constantIbLb1EEERSC_PT_DpOT0_ = comdat any

$_ZNSt3__29allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEES7_EEPvEEE9constructINS_4pairIKS7_S7_EEJRKNS_21piecewise_construct_tENS_5tupleIJRSE_EEENSJ_IJEEEEEEvPT_DpOT0_ = comdat any

$_ZNSt3__24pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_EC2IJRS7_EJEEENS_21piecewise_construct_tENS_5tupleIJDpT_EEENSC_IJDpT0_EEE = comdat any

$_ZNSt3__24pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_EC2IJRS7_EJEJLm0EEJEEENS_21piecewise_construct_tERNS_5tupleIJDpT_EEERNSC_IJDpT0_EEENS_15__tuple_indicesIJXspT1_EEEENSL_IJXspT2_EEEE = comdat any

$_ZNSt3__27forwardIRKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEEOT_RNS_16remove_referenceIS9_E4typeE = comdat any

$_ZNSt3__23getILm0EJRKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEEERNS_13tuple_elementIXT_ENS_5tupleIJDpT0_EEEE4typeERSD_ = comdat any

$_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2Ev = comdat any

$_ZNSt3__212__tuple_leafILm0ERKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEELb0EE3getEv = comdat any

$_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_EC2INS_18__default_init_tagESA_EEOT_OT0_ = comdat any

$_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__zeroEv = comdat any

$_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIcEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__29allocatorIcEC2Ev = comdat any

$_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EE5__getEv = comdat any

$_ZNSt3__29addressofINS_4pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEEEPT_RSA_ = comdat any

$_ZNKSt3__217__compressed_pairIPNS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPNS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPNS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_22__tree_node_destructorINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS2_IcEEEES9_EEPvEEEEEELi1ELb0EE5__getEv = comdat any

$_ZNSt3__227__tree_balance_after_insertIPNS_16__tree_node_baseIPvEEEEvT_S5_ = comdat any

$_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE4sizeEv = comdat any

$_ZNSt3__218__tree_left_rotateIPNS_16__tree_node_baseIPvEEEEvT_ = comdat any

$_ZNSt3__219__tree_right_rotateIPNS_16__tree_node_baseIPvEEEEvT_ = comdat any

$_ZNSt3__216__tree_node_baseIPvE12__set_parentEPS2_ = comdat any

$_ZNSt3__217__compressed_pairImNS_19__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS7_S7_EENS_4lessIS7_EELb1EEEE5firstEv = comdat any

$_ZNSt3__222__compressed_pair_elemImLi0ELb0EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPNS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEE5firstEv = comdat any

$_ZNSt3__222__compressed_pair_elemIPNS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__210unique_ptrINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEE5resetEPSB_ = comdat any

$_ZNSt3__222__tree_node_destructorINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEEEclEPSB_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEEE7destroyINS_4pairIKS8_S8_EEEEvRSC_PT_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEEE10deallocateERSC_PSB_m = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEEE9__destroyINS_4pairIKS8_S8_EEEEvNS_17integral_constantIbLb0EEERSC_PT_ = comdat any

$_ZNSt3__24pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_ED2Ev = comdat any

$_ZNSt3__29allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEES7_EEPvEEE10deallocateEPSA_m = comdat any

$_ZNSt3__219__libcpp_deallocateEPvmm = comdat any

$_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm = comdat any

$_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm = comdat any

$_ZNSt3__217_DeallocateCaller9__do_callEPv = comdat any

$_ZNSt3__27forwardINS_15__tree_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPNS_11__tree_nodeIS9_PvEElEEEEOT_RNS_16remove_referenceISF_E4typeE = comdat any

$_ZNSt3__27forwardIRbEEOT_RNS_16remove_referenceIS2_E4typeE = comdat any

$_ZNSt3__25tupleIJRKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEEC2ILb1ELb0EEES8_ = comdat any

$_ZNSt3__212__tuple_implINS_15__tuple_indicesIJLm0EEEEJRKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEEC2IJLm0EEJSA_EJEJEJSA_EEENS1_IJXspT_EEEENS_13__tuple_typesIJDpT0_EEENS1_IJXspT1_EEEENSE_IJDpT2_EEEDpOT3_ = comdat any

$_ZNSt3__212__tuple_leafILm0ERKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEELb0EEC2IS8_vEEOT_ = comdat any

$_ZNSt3__214pointer_traitsIPNS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEE10pointer_toERS8_ = comdat any

$_ZNKSt3__215__tree_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElE8__get_npEv = comdat any

$_ZNSt3__29addressofINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEEEPT_RS9_ = comdat any

$_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE13__move_assignERS5_NS_17integral_constantIbLb1EEE = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIcEEE10deallocateERS2_Pcm = comdat any

$_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE7__allocEv = comdat any

$_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE18__get_long_pointerEv = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE14__get_long_capEv = comdat any

$_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE19__move_assign_allocERS5_ = comdat any

$_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE16__set_short_sizeEm = comdat any

$_ZNSt3__211char_traitsIcE6assignERcRKc = comdat any

$_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE19__get_short_pointerEv = comdat any

$_ZNSt3__29allocatorIcE10deallocateEPcm = comdat any

$_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIcEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE19__move_assign_allocERS5_NS_17integral_constantIbLb1EEE = comdat any

$_ZNSt3__24moveIRNS_9allocatorIcEEEEONS_16remove_referenceIT_E4typeEOS5_ = comdat any

$_ZNSt3__214pointer_traitsIPcE10pointer_toERc = comdat any

$_ZNSt3__29addressofIcEEPT_RS1_ = comdat any

$_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE4findIS7_EENS_21__tree_const_iteratorIS8_PNS_11__tree_nodeIS8_PvEElEERKT_ = comdat any

$_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE13__lower_boundIS7_EENS_21__tree_const_iteratorIS8_PNS_11__tree_nodeIS8_PvEElEERKT_SK_PNS_15__tree_end_nodeIPNS_16__tree_node_baseISI_EEEE = comdat any

$_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE10value_compEv = comdat any

$_ZNKSt3__221__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElEdeEv = comdat any

$_ZNKSt3__217__compressed_pairImNS_19__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS7_S7_EENS_4lessIS7_EELb1EEEE6secondEv = comdat any

$_ZNKSt3__222__compressed_pair_elemINS_19__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS7_S7_EENS_4lessIS7_EELb1EEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__214pointer_traitsIPKNS_4pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEE10pointer_toERSA_ = comdat any

$_ZNSt3__29addressofIKNS_4pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEEEPT_RSB_ = comdat any

$_ZNSt3__211char_traitsIcE6lengthEPKc = comdat any

@.str = private unnamed_addr constant [1 x i8] zeroinitializer, align 1
@_ZNSt3__2L19piecewise_constructE = internal constant %"struct.std::__2::piecewise_construct_t" undef, align 1
@.str.1 = private unnamed_addr constant [68 x i8] c"allocator<T>::allocate(size_t n) 'n' exceeds maximum supported size\00", align 1

@_ZN5draco7OptionsC1Ev = hidden unnamed_addr alias %"class.draco::Options"* (%"class.draco::Options"*), %"class.draco::Options"* (%"class.draco::Options"*)* @_ZN5draco7OptionsC2Ev

; Function Attrs: noinline nounwind optnone
define hidden %"class.draco::Options"* @_ZN5draco7OptionsC2Ev(%"class.draco::Options"* returned %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %"class.draco::Options"*, align 4
  store %"class.draco::Options"* %this, %"class.draco::Options"** %this.addr, align 4
  %this1 = load %"class.draco::Options"*, %"class.draco::Options"** %this.addr, align 4
  %options_ = getelementptr inbounds %"class.draco::Options", %"class.draco::Options"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::map"* @_ZNSt3__23mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_NS_4lessIS6_EENS4_INS_4pairIKS6_S6_EEEEEC2Ev(%"class.std::__2::map"* %options_) #8
  ret %"class.draco::Options"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::map"* @_ZNSt3__23mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_NS_4lessIS6_EENS4_INS_4pairIKS6_S6_EEEEEC2Ev(%"class.std::__2::map"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::map"*, align 4
  %ref.tmp = alloca %"class.std::__2::__map_value_compare", align 1
  %agg.tmp = alloca %"struct.std::__2::less", align 1
  store %"class.std::__2::map"* %this, %"class.std::__2::map"** %this.addr, align 4
  %this1 = load %"class.std::__2::map"*, %"class.std::__2::map"** %this.addr, align 4
  %__tree_ = getelementptr inbounds %"class.std::__2::map", %"class.std::__2::map"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::__map_value_compare"* @_ZNSt3__219__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS6_S6_EENS_4lessIS6_EELb1EEC2ESA_(%"class.std::__2::__map_value_compare"* %ref.tmp) #8
  %call2 = call %"class.std::__2::__tree"* @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEEC2ERKSC_(%"class.std::__2::__tree"* %__tree_, %"class.std::__2::__map_value_compare"* nonnull align 1 dereferenceable(1) %ref.tmp) #8
  ret %"class.std::__2::map"* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN5draco7Options15MergeAndReplaceERKS0_(%"class.draco::Options"* %this, %"class.draco::Options"* nonnull align 4 dereferenceable(12) %other_options) #0 {
entry:
  %this.addr = alloca %"class.draco::Options"*, align 4
  %other_options.addr = alloca %"class.draco::Options"*, align 4
  %__range1 = alloca %"class.std::__2::map"*, align 4
  %__begin1 = alloca %"class.std::__2::__map_const_iterator", align 4
  %__end1 = alloca %"class.std::__2::__map_const_iterator", align 4
  %item = alloca %"struct.std::__2::pair"*, align 4
  store %"class.draco::Options"* %this, %"class.draco::Options"** %this.addr, align 4
  store %"class.draco::Options"* %other_options, %"class.draco::Options"** %other_options.addr, align 4
  %this1 = load %"class.draco::Options"*, %"class.draco::Options"** %this.addr, align 4
  %0 = load %"class.draco::Options"*, %"class.draco::Options"** %other_options.addr, align 4
  %options_ = getelementptr inbounds %"class.draco::Options", %"class.draco::Options"* %0, i32 0, i32 0
  store %"class.std::__2::map"* %options_, %"class.std::__2::map"** %__range1, align 4
  %1 = load %"class.std::__2::map"*, %"class.std::__2::map"** %__range1, align 4
  %call = call %"class.std::__2::__tree_end_node"* @_ZNKSt3__23mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_NS_4lessIS6_EENS4_INS_4pairIKS6_S6_EEEEE5beginEv(%"class.std::__2::map"* %1) #8
  %coerce.dive = getelementptr inbounds %"class.std::__2::__map_const_iterator", %"class.std::__2::__map_const_iterator"* %__begin1, i32 0, i32 0
  %coerce.dive2 = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %coerce.dive, i32 0, i32 0
  store %"class.std::__2::__tree_end_node"* %call, %"class.std::__2::__tree_end_node"** %coerce.dive2, align 4
  %2 = load %"class.std::__2::map"*, %"class.std::__2::map"** %__range1, align 4
  %call3 = call %"class.std::__2::__tree_end_node"* @_ZNKSt3__23mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_NS_4lessIS6_EENS4_INS_4pairIKS6_S6_EEEEE3endEv(%"class.std::__2::map"* %2) #8
  %coerce.dive4 = getelementptr inbounds %"class.std::__2::__map_const_iterator", %"class.std::__2::__map_const_iterator"* %__end1, i32 0, i32 0
  %coerce.dive5 = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %coerce.dive4, i32 0, i32 0
  store %"class.std::__2::__tree_end_node"* %call3, %"class.std::__2::__tree_end_node"** %coerce.dive5, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %call6 = call zeroext i1 @_ZNSt3__2neERKNS_20__map_const_iteratorINS_21__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPNS_11__tree_nodeIS9_PvEElEEEESH_(%"class.std::__2::__map_const_iterator"* nonnull align 4 dereferenceable(4) %__begin1, %"class.std::__2::__map_const_iterator"* nonnull align 4 dereferenceable(4) %__end1)
  br i1 %call6, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %call7 = call nonnull align 4 dereferenceable(24) %"struct.std::__2::pair"* @_ZNKSt3__220__map_const_iteratorINS_21__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPNS_11__tree_nodeIS9_PvEElEEEdeEv(%"class.std::__2::__map_const_iterator"* %__begin1)
  store %"struct.std::__2::pair"* %call7, %"struct.std::__2::pair"** %item, align 4
  %3 = load %"struct.std::__2::pair"*, %"struct.std::__2::pair"** %item, align 4
  %second = getelementptr inbounds %"struct.std::__2::pair", %"struct.std::__2::pair"* %3, i32 0, i32 1
  %options_8 = getelementptr inbounds %"class.draco::Options", %"class.draco::Options"* %this1, i32 0, i32 0
  %4 = load %"struct.std::__2::pair"*, %"struct.std::__2::pair"** %item, align 4
  %first = getelementptr inbounds %"struct.std::__2::pair", %"struct.std::__2::pair"* %4, i32 0, i32 0
  %call9 = call nonnull align 4 dereferenceable(12) %"class.std::__2::basic_string"* @_ZNSt3__23mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_NS_4lessIS6_EENS4_INS_4pairIKS6_S6_EEEEEixERSA_(%"class.std::__2::map"* %options_8, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %first)
  %call10 = call nonnull align 4 dereferenceable(12) %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEaSERKS5_(%"class.std::__2::basic_string"* %call9, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %second)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %call11 = call nonnull align 4 dereferenceable(4) %"class.std::__2::__map_const_iterator"* @_ZNSt3__220__map_const_iteratorINS_21__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPNS_11__tree_nodeIS9_PvEElEEEppEv(%"class.std::__2::__map_const_iterator"* %__begin1)
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_end_node"* @_ZNKSt3__23mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_NS_4lessIS6_EENS4_INS_4pairIKS6_S6_EEEEE5beginEv(%"class.std::__2::map"* %this) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__map_const_iterator", align 4
  %this.addr = alloca %"class.std::__2::map"*, align 4
  %agg.tmp = alloca %"class.std::__2::__tree_const_iterator", align 4
  store %"class.std::__2::map"* %this, %"class.std::__2::map"** %this.addr, align 4
  %this1 = load %"class.std::__2::map"*, %"class.std::__2::map"** %this.addr, align 4
  %__tree_ = getelementptr inbounds %"class.std::__2::map", %"class.std::__2::map"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::__tree_end_node"* @_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE5beginEv(%"class.std::__2::__tree"* %__tree_) #8
  %coerce.dive = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %agg.tmp, i32 0, i32 0
  store %"class.std::__2::__tree_end_node"* %call, %"class.std::__2::__tree_end_node"** %coerce.dive, align 4
  %coerce.dive2 = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %agg.tmp, i32 0, i32 0
  %0 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %coerce.dive2, align 4
  %call3 = call %"class.std::__2::__map_const_iterator"* @_ZNSt3__220__map_const_iteratorINS_21__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPNS_11__tree_nodeIS9_PvEElEEEC2ESE_(%"class.std::__2::__map_const_iterator"* %retval, %"class.std::__2::__tree_end_node"* %0) #8
  %coerce.dive4 = getelementptr inbounds %"class.std::__2::__map_const_iterator", %"class.std::__2::__map_const_iterator"* %retval, i32 0, i32 0
  %coerce.dive5 = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %coerce.dive4, i32 0, i32 0
  %1 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %coerce.dive5, align 4
  ret %"class.std::__2::__tree_end_node"* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_end_node"* @_ZNKSt3__23mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_NS_4lessIS6_EENS4_INS_4pairIKS6_S6_EEEEE3endEv(%"class.std::__2::map"* %this) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__map_const_iterator", align 4
  %this.addr = alloca %"class.std::__2::map"*, align 4
  %agg.tmp = alloca %"class.std::__2::__tree_const_iterator", align 4
  store %"class.std::__2::map"* %this, %"class.std::__2::map"** %this.addr, align 4
  %this1 = load %"class.std::__2::map"*, %"class.std::__2::map"** %this.addr, align 4
  %__tree_ = getelementptr inbounds %"class.std::__2::map", %"class.std::__2::map"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::__tree_end_node"* @_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE3endEv(%"class.std::__2::__tree"* %__tree_) #8
  %coerce.dive = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %agg.tmp, i32 0, i32 0
  store %"class.std::__2::__tree_end_node"* %call, %"class.std::__2::__tree_end_node"** %coerce.dive, align 4
  %coerce.dive2 = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %agg.tmp, i32 0, i32 0
  %0 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %coerce.dive2, align 4
  %call3 = call %"class.std::__2::__map_const_iterator"* @_ZNSt3__220__map_const_iteratorINS_21__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPNS_11__tree_nodeIS9_PvEElEEEC2ESE_(%"class.std::__2::__map_const_iterator"* %retval, %"class.std::__2::__tree_end_node"* %0) #8
  %coerce.dive4 = getelementptr inbounds %"class.std::__2::__map_const_iterator", %"class.std::__2::__map_const_iterator"* %retval, i32 0, i32 0
  %coerce.dive5 = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %coerce.dive4, i32 0, i32 0
  %1 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %coerce.dive5, align 4
  ret %"class.std::__2::__tree_end_node"* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNSt3__2neERKNS_20__map_const_iteratorINS_21__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPNS_11__tree_nodeIS9_PvEElEEEESH_(%"class.std::__2::__map_const_iterator"* nonnull align 4 dereferenceable(4) %__x, %"class.std::__2::__map_const_iterator"* nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %__x.addr = alloca %"class.std::__2::__map_const_iterator"*, align 4
  %__y.addr = alloca %"class.std::__2::__map_const_iterator"*, align 4
  store %"class.std::__2::__map_const_iterator"* %__x, %"class.std::__2::__map_const_iterator"** %__x.addr, align 4
  store %"class.std::__2::__map_const_iterator"* %__y, %"class.std::__2::__map_const_iterator"** %__y.addr, align 4
  %0 = load %"class.std::__2::__map_const_iterator"*, %"class.std::__2::__map_const_iterator"** %__x.addr, align 4
  %__i_ = getelementptr inbounds %"class.std::__2::__map_const_iterator", %"class.std::__2::__map_const_iterator"* %0, i32 0, i32 0
  %1 = load %"class.std::__2::__map_const_iterator"*, %"class.std::__2::__map_const_iterator"** %__y.addr, align 4
  %__i_1 = getelementptr inbounds %"class.std::__2::__map_const_iterator", %"class.std::__2::__map_const_iterator"* %1, i32 0, i32 0
  %call = call zeroext i1 @_ZNSt3__2neERKNS_21__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElEESF_(%"class.std::__2::__tree_const_iterator"* nonnull align 4 dereferenceable(4) %__i_, %"class.std::__2::__tree_const_iterator"* nonnull align 4 dereferenceable(4) %__i_1)
  ret i1 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(24) %"struct.std::__2::pair"* @_ZNKSt3__220__map_const_iteratorINS_21__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPNS_11__tree_nodeIS9_PvEElEEEdeEv(%"class.std::__2::__map_const_iterator"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__map_const_iterator"*, align 4
  store %"class.std::__2::__map_const_iterator"* %this, %"class.std::__2::__map_const_iterator"** %this.addr, align 4
  %this1 = load %"class.std::__2::__map_const_iterator"*, %"class.std::__2::__map_const_iterator"** %this.addr, align 4
  %__i_ = getelementptr inbounds %"class.std::__2::__map_const_iterator", %"class.std::__2::__map_const_iterator"* %this1, i32 0, i32 0
  %call = call %"struct.std::__2::__value_type"* @_ZNKSt3__221__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElEptEv(%"class.std::__2::__tree_const_iterator"* %__i_)
  %call2 = call nonnull align 4 dereferenceable(24) %"struct.std::__2::pair"* @_ZNKSt3__212__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_E11__get_valueEv(%"struct.std::__2::__value_type"* %call)
  ret %"struct.std::__2::pair"* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"class.std::__2::basic_string"* @_ZNSt3__23mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_NS_4lessIS6_EENS4_INS_4pairIKS6_S6_EEEEEixERSA_(%"class.std::__2::map"* %this, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %__k) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::map"*, align 4
  %__k.addr = alloca %"class.std::__2::basic_string"*, align 4
  %ref.tmp = alloca %"struct.std::__2::pair.9", align 4
  %ref.tmp2 = alloca %"class.std::__2::tuple", align 4
  %ref.tmp5 = alloca %"class.std::__2::tuple.10", align 1
  %undef.agg.tmp = alloca %"class.std::__2::tuple.10", align 1
  store %"class.std::__2::map"* %this, %"class.std::__2::map"** %this.addr, align 4
  store %"class.std::__2::basic_string"* %__k, %"class.std::__2::basic_string"** %__k.addr, align 4
  %this1 = load %"class.std::__2::map"*, %"class.std::__2::map"** %this.addr, align 4
  %__tree_ = getelementptr inbounds %"class.std::__2::map", %"class.std::__2::map"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__k.addr, align 4
  %1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__k.addr, align 4
  %call = call %"class.std::__2::basic_string"* @_ZNSt3__216forward_as_tupleIJRKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEEENS_5tupleIJDpOT_EEESC_(%"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %1) #8
  %coerce.dive = getelementptr inbounds %"class.std::__2::tuple", %"class.std::__2::tuple"* %ref.tmp2, i32 0, i32 0
  %coerce.dive3 = getelementptr inbounds %"struct.std::__2::__tuple_impl", %"struct.std::__2::__tuple_impl"* %coerce.dive, i32 0, i32 0
  %coerce.dive4 = getelementptr inbounds %"class.std::__2::__tuple_leaf", %"class.std::__2::__tuple_leaf"* %coerce.dive3, i32 0, i32 0
  store %"class.std::__2::basic_string"* %call, %"class.std::__2::basic_string"** %coerce.dive4, align 4
  call void @_ZNSt3__216forward_as_tupleIJEEENS_5tupleIJDpOT_EEES4_() #8
  call void @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE25__emplace_unique_key_argsIS7_JRKNS_21piecewise_construct_tENS_5tupleIJRKS7_EEENSJ_IJEEEEEENS_4pairINS_15__tree_iteratorIS8_PNS_11__tree_nodeIS8_PvEElEEbEERKT_DpOT0_(%"struct.std::__2::pair.9"* sret align 4 %ref.tmp, %"class.std::__2::__tree"* %__tree_, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %0, %"struct.std::__2::piecewise_construct_t"* nonnull align 1 dereferenceable(1) @_ZNSt3__2L19piecewise_constructE, %"class.std::__2::tuple"* nonnull align 4 dereferenceable(4) %ref.tmp2, %"class.std::__2::tuple.10"* nonnull align 1 dereferenceable(1) %ref.tmp5)
  %first = getelementptr inbounds %"struct.std::__2::pair.9", %"struct.std::__2::pair.9"* %ref.tmp, i32 0, i32 0
  %call6 = call %"struct.std::__2::__value_type"* @_ZNKSt3__215__tree_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElEptEv(%"class.std::__2::__tree_iterator"* %first)
  %call7 = call nonnull align 4 dereferenceable(24) %"struct.std::__2::pair"* @_ZNSt3__212__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_E11__get_valueEv(%"struct.std::__2::__value_type"* %call6)
  %second = getelementptr inbounds %"struct.std::__2::pair", %"struct.std::__2::pair"* %call7, i32 0, i32 1
  ret %"class.std::__2::basic_string"* %second
}

declare nonnull align 4 dereferenceable(12) %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEaSERKS5_(%"class.std::__2::basic_string"*, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12)) #1

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__map_const_iterator"* @_ZNSt3__220__map_const_iteratorINS_21__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPNS_11__tree_nodeIS9_PvEElEEEppEv(%"class.std::__2::__map_const_iterator"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__map_const_iterator"*, align 4
  store %"class.std::__2::__map_const_iterator"* %this, %"class.std::__2::__map_const_iterator"** %this.addr, align 4
  %this1 = load %"class.std::__2::__map_const_iterator"*, %"class.std::__2::__map_const_iterator"** %this.addr, align 4
  %__i_ = getelementptr inbounds %"class.std::__2::__map_const_iterator", %"class.std::__2::__map_const_iterator"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_const_iterator"* @_ZNSt3__221__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElEppEv(%"class.std::__2::__tree_const_iterator"* %__i_)
  ret %"class.std::__2::__map_const_iterator"* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN5draco7Options6SetIntERKNSt3__212basic_stringIcNS1_11char_traitsIcEENS1_9allocatorIcEEEEi(%"class.draco::Options"* %this, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %name, i32 %val) #0 {
entry:
  %this.addr = alloca %"class.draco::Options"*, align 4
  %name.addr = alloca %"class.std::__2::basic_string"*, align 4
  %val.addr = alloca i32, align 4
  %ref.tmp = alloca %"class.std::__2::basic_string", align 4
  store %"class.draco::Options"* %this, %"class.draco::Options"** %this.addr, align 4
  store %"class.std::__2::basic_string"* %name, %"class.std::__2::basic_string"** %name.addr, align 4
  store i32 %val, i32* %val.addr, align 4
  %this1 = load %"class.draco::Options"*, %"class.draco::Options"** %this.addr, align 4
  %0 = load i32, i32* %val.addr, align 4
  call void @_ZNSt3__29to_stringEi(%"class.std::__2::basic_string"* sret align 4 %ref.tmp, i32 %0)
  %options_ = getelementptr inbounds %"class.draco::Options", %"class.draco::Options"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %name.addr, align 4
  %call = call nonnull align 4 dereferenceable(12) %"class.std::__2::basic_string"* @_ZNSt3__23mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_NS_4lessIS6_EENS4_INS_4pairIKS6_S6_EEEEEixERSA_(%"class.std::__2::map"* %options_, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %1)
  %call2 = call nonnull align 4 dereferenceable(12) %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEaSEOS5_(%"class.std::__2::basic_string"* %call, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %ref.tmp) #8
  %call3 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_string"* %ref.tmp) #8
  ret void
}

declare void @_ZNSt3__29to_stringEi(%"class.std::__2::basic_string"* sret align 4, i32) #1

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEaSEOS5_(%"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %__str) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  %__str.addr = alloca %"class.std::__2::basic_string"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  store %"class.std::__2::basic_string"* %__str, %"class.std::__2::basic_string"** %__str.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %0 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__str.addr, align 4
  call void @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE13__move_assignERS5_NS_17integral_constantIbLb1EEE(%"class.std::__2::basic_string"* %this1, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %0) #8
  ret %"class.std::__2::basic_string"* %this1
}

; Function Attrs: nounwind
declare %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_string"* returned) unnamed_addr #2

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN5draco7Options8SetFloatERKNSt3__212basic_stringIcNS1_11char_traitsIcEENS1_9allocatorIcEEEEf(%"class.draco::Options"* %this, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %name, float %val) #0 {
entry:
  %this.addr = alloca %"class.draco::Options"*, align 4
  %name.addr = alloca %"class.std::__2::basic_string"*, align 4
  %val.addr = alloca float, align 4
  %ref.tmp = alloca %"class.std::__2::basic_string", align 4
  store %"class.draco::Options"* %this, %"class.draco::Options"** %this.addr, align 4
  store %"class.std::__2::basic_string"* %name, %"class.std::__2::basic_string"** %name.addr, align 4
  store float %val, float* %val.addr, align 4
  %this1 = load %"class.draco::Options"*, %"class.draco::Options"** %this.addr, align 4
  %0 = load float, float* %val.addr, align 4
  call void @_ZNSt3__29to_stringEf(%"class.std::__2::basic_string"* sret align 4 %ref.tmp, float %0)
  %options_ = getelementptr inbounds %"class.draco::Options", %"class.draco::Options"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %name.addr, align 4
  %call = call nonnull align 4 dereferenceable(12) %"class.std::__2::basic_string"* @_ZNSt3__23mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_NS_4lessIS6_EENS4_INS_4pairIKS6_S6_EEEEEixERSA_(%"class.std::__2::map"* %options_, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %1)
  %call2 = call nonnull align 4 dereferenceable(12) %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEaSEOS5_(%"class.std::__2::basic_string"* %call, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %ref.tmp) #8
  %call3 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_string"* %ref.tmp) #8
  ret void
}

declare void @_ZNSt3__29to_stringEf(%"class.std::__2::basic_string"* sret align 4, float) #1

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN5draco7Options7SetBoolERKNSt3__212basic_stringIcNS1_11char_traitsIcEENS1_9allocatorIcEEEEb(%"class.draco::Options"* %this, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %name, i1 zeroext %val) #0 {
entry:
  %this.addr = alloca %"class.draco::Options"*, align 4
  %name.addr = alloca %"class.std::__2::basic_string"*, align 4
  %val.addr = alloca i8, align 1
  %ref.tmp = alloca %"class.std::__2::basic_string", align 4
  store %"class.draco::Options"* %this, %"class.draco::Options"** %this.addr, align 4
  store %"class.std::__2::basic_string"* %name, %"class.std::__2::basic_string"** %name.addr, align 4
  %frombool = zext i1 %val to i8
  store i8 %frombool, i8* %val.addr, align 1
  %this1 = load %"class.draco::Options"*, %"class.draco::Options"** %this.addr, align 4
  %0 = load i8, i8* %val.addr, align 1
  %tobool = trunc i8 %0 to i1
  %1 = zext i1 %tobool to i64
  %cond = select i1 %tobool, i32 1, i32 0
  call void @_ZNSt3__29to_stringEi(%"class.std::__2::basic_string"* sret align 4 %ref.tmp, i32 %cond)
  %options_ = getelementptr inbounds %"class.draco::Options", %"class.draco::Options"* %this1, i32 0, i32 0
  %2 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %name.addr, align 4
  %call = call nonnull align 4 dereferenceable(12) %"class.std::__2::basic_string"* @_ZNSt3__23mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_NS_4lessIS6_EENS4_INS_4pairIKS6_S6_EEEEEixERSA_(%"class.std::__2::map"* %options_, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %2)
  %call2 = call nonnull align 4 dereferenceable(12) %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEaSEOS5_(%"class.std::__2::basic_string"* %call, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %ref.tmp) #8
  %call3 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_string"* %ref.tmp) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN5draco7Options9SetStringERKNSt3__212basic_stringIcNS1_11char_traitsIcEENS1_9allocatorIcEEEES9_(%"class.draco::Options"* %this, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %name, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %val) #0 {
entry:
  %this.addr = alloca %"class.draco::Options"*, align 4
  %name.addr = alloca %"class.std::__2::basic_string"*, align 4
  %val.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.draco::Options"* %this, %"class.draco::Options"** %this.addr, align 4
  store %"class.std::__2::basic_string"* %name, %"class.std::__2::basic_string"** %name.addr, align 4
  store %"class.std::__2::basic_string"* %val, %"class.std::__2::basic_string"** %val.addr, align 4
  %this1 = load %"class.draco::Options"*, %"class.draco::Options"** %this.addr, align 4
  %0 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %val.addr, align 4
  %options_ = getelementptr inbounds %"class.draco::Options", %"class.draco::Options"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %name.addr, align 4
  %call = call nonnull align 4 dereferenceable(12) %"class.std::__2::basic_string"* @_ZNSt3__23mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_NS_4lessIS6_EENS4_INS_4pairIKS6_S6_EEEEEixERSA_(%"class.std::__2::map"* %options_, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %1)
  %call2 = call nonnull align 4 dereferenceable(12) %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEaSERKS5_(%"class.std::__2::basic_string"* %call, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %0)
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @_ZNK5draco7Options6GetIntERKNSt3__212basic_stringIcNS1_11char_traitsIcEENS1_9allocatorIcEEEE(%"class.draco::Options"* %this, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %name) #0 {
entry:
  %this.addr = alloca %"class.draco::Options"*, align 4
  %name.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.draco::Options"* %this, %"class.draco::Options"** %this.addr, align 4
  store %"class.std::__2::basic_string"* %name, %"class.std::__2::basic_string"** %name.addr, align 4
  %this1 = load %"class.draco::Options"*, %"class.draco::Options"** %this.addr, align 4
  %0 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %name.addr, align 4
  %call = call i32 @_ZNK5draco7Options6GetIntERKNSt3__212basic_stringIcNS1_11char_traitsIcEENS1_9allocatorIcEEEEi(%"class.draco::Options"* %this1, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %0, i32 -1)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @_ZNK5draco7Options6GetIntERKNSt3__212basic_stringIcNS1_11char_traitsIcEENS1_9allocatorIcEEEEi(%"class.draco::Options"* %this, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %name, i32 %default_val) #0 {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %"class.draco::Options"*, align 4
  %name.addr = alloca %"class.std::__2::basic_string"*, align 4
  %default_val.addr = alloca i32, align 4
  %it = alloca %"class.std::__2::__map_const_iterator", align 4
  %ref.tmp = alloca %"class.std::__2::__map_const_iterator", align 4
  store %"class.draco::Options"* %this, %"class.draco::Options"** %this.addr, align 4
  store %"class.std::__2::basic_string"* %name, %"class.std::__2::basic_string"** %name.addr, align 4
  store i32 %default_val, i32* %default_val.addr, align 4
  %this1 = load %"class.draco::Options"*, %"class.draco::Options"** %this.addr, align 4
  %options_ = getelementptr inbounds %"class.draco::Options", %"class.draco::Options"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %name.addr, align 4
  %call = call %"class.std::__2::__tree_end_node"* @_ZNKSt3__23mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_NS_4lessIS6_EENS4_INS_4pairIKS6_S6_EEEEE4findERSA_(%"class.std::__2::map"* %options_, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %0)
  %coerce.dive = getelementptr inbounds %"class.std::__2::__map_const_iterator", %"class.std::__2::__map_const_iterator"* %it, i32 0, i32 0
  %coerce.dive2 = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %coerce.dive, i32 0, i32 0
  store %"class.std::__2::__tree_end_node"* %call, %"class.std::__2::__tree_end_node"** %coerce.dive2, align 4
  %options_3 = getelementptr inbounds %"class.draco::Options", %"class.draco::Options"* %this1, i32 0, i32 0
  %call4 = call %"class.std::__2::__tree_end_node"* @_ZNKSt3__23mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_NS_4lessIS6_EENS4_INS_4pairIKS6_S6_EEEEE3endEv(%"class.std::__2::map"* %options_3) #8
  %coerce.dive5 = getelementptr inbounds %"class.std::__2::__map_const_iterator", %"class.std::__2::__map_const_iterator"* %ref.tmp, i32 0, i32 0
  %coerce.dive6 = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %coerce.dive5, i32 0, i32 0
  store %"class.std::__2::__tree_end_node"* %call4, %"class.std::__2::__tree_end_node"** %coerce.dive6, align 4
  %call7 = call zeroext i1 @_ZNSt3__2eqERKNS_20__map_const_iteratorINS_21__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPNS_11__tree_nodeIS9_PvEElEEEESH_(%"class.std::__2::__map_const_iterator"* nonnull align 4 dereferenceable(4) %it, %"class.std::__2::__map_const_iterator"* nonnull align 4 dereferenceable(4) %ref.tmp)
  br i1 %call7, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %default_val.addr, align 4
  store i32 %1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %call8 = call %"struct.std::__2::pair"* @_ZNKSt3__220__map_const_iteratorINS_21__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPNS_11__tree_nodeIS9_PvEElEEEptEv(%"class.std::__2::__map_const_iterator"* %it)
  %second = getelementptr inbounds %"struct.std::__2::pair", %"struct.std::__2::pair"* %call8, i32 0, i32 1
  %call9 = call i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5c_strEv(%"class.std::__2::basic_string"* %second) #8
  %call10 = call i32 @atoi(i8* %call9)
  store i32 %call10, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %2 = load i32, i32* %retval, align 4
  ret i32 %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_end_node"* @_ZNKSt3__23mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_NS_4lessIS6_EENS4_INS_4pairIKS6_S6_EEEEE4findERSA_(%"class.std::__2::map"* %this, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %__k) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__map_const_iterator", align 4
  %this.addr = alloca %"class.std::__2::map"*, align 4
  %__k.addr = alloca %"class.std::__2::basic_string"*, align 4
  %agg.tmp = alloca %"class.std::__2::__tree_const_iterator", align 4
  store %"class.std::__2::map"* %this, %"class.std::__2::map"** %this.addr, align 4
  store %"class.std::__2::basic_string"* %__k, %"class.std::__2::basic_string"** %__k.addr, align 4
  %this1 = load %"class.std::__2::map"*, %"class.std::__2::map"** %this.addr, align 4
  %__tree_ = getelementptr inbounds %"class.std::__2::map", %"class.std::__2::map"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__k.addr, align 4
  %call = call %"class.std::__2::__tree_end_node"* @_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE4findIS7_EENS_21__tree_const_iteratorIS8_PNS_11__tree_nodeIS8_PvEElEERKT_(%"class.std::__2::__tree"* %__tree_, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %0)
  %coerce.dive = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %agg.tmp, i32 0, i32 0
  store %"class.std::__2::__tree_end_node"* %call, %"class.std::__2::__tree_end_node"** %coerce.dive, align 4
  %coerce.dive2 = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %agg.tmp, i32 0, i32 0
  %1 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %coerce.dive2, align 4
  %call3 = call %"class.std::__2::__map_const_iterator"* @_ZNSt3__220__map_const_iteratorINS_21__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPNS_11__tree_nodeIS9_PvEElEEEC2ESE_(%"class.std::__2::__map_const_iterator"* %retval, %"class.std::__2::__tree_end_node"* %1) #8
  %coerce.dive4 = getelementptr inbounds %"class.std::__2::__map_const_iterator", %"class.std::__2::__map_const_iterator"* %retval, i32 0, i32 0
  %coerce.dive5 = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %coerce.dive4, i32 0, i32 0
  %2 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %coerce.dive5, align 4
  ret %"class.std::__2::__tree_end_node"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNSt3__2eqERKNS_20__map_const_iteratorINS_21__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPNS_11__tree_nodeIS9_PvEElEEEESH_(%"class.std::__2::__map_const_iterator"* nonnull align 4 dereferenceable(4) %__x, %"class.std::__2::__map_const_iterator"* nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %__x.addr = alloca %"class.std::__2::__map_const_iterator"*, align 4
  %__y.addr = alloca %"class.std::__2::__map_const_iterator"*, align 4
  store %"class.std::__2::__map_const_iterator"* %__x, %"class.std::__2::__map_const_iterator"** %__x.addr, align 4
  store %"class.std::__2::__map_const_iterator"* %__y, %"class.std::__2::__map_const_iterator"** %__y.addr, align 4
  %0 = load %"class.std::__2::__map_const_iterator"*, %"class.std::__2::__map_const_iterator"** %__x.addr, align 4
  %__i_ = getelementptr inbounds %"class.std::__2::__map_const_iterator", %"class.std::__2::__map_const_iterator"* %0, i32 0, i32 0
  %1 = load %"class.std::__2::__map_const_iterator"*, %"class.std::__2::__map_const_iterator"** %__y.addr, align 4
  %__i_1 = getelementptr inbounds %"class.std::__2::__map_const_iterator", %"class.std::__2::__map_const_iterator"* %1, i32 0, i32 0
  %call = call zeroext i1 @_ZNSt3__2eqERKNS_21__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElEESF_(%"class.std::__2::__tree_const_iterator"* nonnull align 4 dereferenceable(4) %__i_, %"class.std::__2::__tree_const_iterator"* nonnull align 4 dereferenceable(4) %__i_1)
  ret i1 %call
}

declare i32 @atoi(i8*) #1

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::pair"* @_ZNKSt3__220__map_const_iteratorINS_21__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPNS_11__tree_nodeIS9_PvEElEEEptEv(%"class.std::__2::__map_const_iterator"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__map_const_iterator"*, align 4
  store %"class.std::__2::__map_const_iterator"* %this, %"class.std::__2::__map_const_iterator"** %this.addr, align 4
  %this1 = load %"class.std::__2::__map_const_iterator"*, %"class.std::__2::__map_const_iterator"** %this.addr, align 4
  %__i_ = getelementptr inbounds %"class.std::__2::__map_const_iterator", %"class.std::__2::__map_const_iterator"* %this1, i32 0, i32 0
  %call = call %"struct.std::__2::__value_type"* @_ZNKSt3__221__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElEptEv(%"class.std::__2::__tree_const_iterator"* %__i_)
  %call2 = call nonnull align 4 dereferenceable(24) %"struct.std::__2::pair"* @_ZNKSt3__212__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_E11__get_valueEv(%"struct.std::__2::__value_type"* %call)
  %call3 = call %"struct.std::__2::pair"* @_ZNSt3__214pointer_traitsIPKNS_4pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEE10pointer_toERSA_(%"struct.std::__2::pair"* nonnull align 4 dereferenceable(24) %call2) #8
  ret %"struct.std::__2::pair"* %call3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5c_strEv(%"class.std::__2::basic_string"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %call = call i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv(%"class.std::__2::basic_string"* %this1) #8
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define hidden float @_ZNK5draco7Options8GetFloatERKNSt3__212basic_stringIcNS1_11char_traitsIcEENS1_9allocatorIcEEEE(%"class.draco::Options"* %this, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %name) #0 {
entry:
  %this.addr = alloca %"class.draco::Options"*, align 4
  %name.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.draco::Options"* %this, %"class.draco::Options"** %this.addr, align 4
  store %"class.std::__2::basic_string"* %name, %"class.std::__2::basic_string"** %name.addr, align 4
  %this1 = load %"class.draco::Options"*, %"class.draco::Options"** %this.addr, align 4
  %0 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %name.addr, align 4
  %call = call float @_ZNK5draco7Options8GetFloatERKNSt3__212basic_stringIcNS1_11char_traitsIcEENS1_9allocatorIcEEEEf(%"class.draco::Options"* %this1, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %0, float -1.000000e+00)
  ret float %call
}

; Function Attrs: noinline nounwind optnone
define hidden float @_ZNK5draco7Options8GetFloatERKNSt3__212basic_stringIcNS1_11char_traitsIcEENS1_9allocatorIcEEEEf(%"class.draco::Options"* %this, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %name, float %default_val) #0 {
entry:
  %retval = alloca float, align 4
  %this.addr = alloca %"class.draco::Options"*, align 4
  %name.addr = alloca %"class.std::__2::basic_string"*, align 4
  %default_val.addr = alloca float, align 4
  %it = alloca %"class.std::__2::__map_const_iterator", align 4
  %ref.tmp = alloca %"class.std::__2::__map_const_iterator", align 4
  store %"class.draco::Options"* %this, %"class.draco::Options"** %this.addr, align 4
  store %"class.std::__2::basic_string"* %name, %"class.std::__2::basic_string"** %name.addr, align 4
  store float %default_val, float* %default_val.addr, align 4
  %this1 = load %"class.draco::Options"*, %"class.draco::Options"** %this.addr, align 4
  %options_ = getelementptr inbounds %"class.draco::Options", %"class.draco::Options"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %name.addr, align 4
  %call = call %"class.std::__2::__tree_end_node"* @_ZNKSt3__23mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_NS_4lessIS6_EENS4_INS_4pairIKS6_S6_EEEEE4findERSA_(%"class.std::__2::map"* %options_, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %0)
  %coerce.dive = getelementptr inbounds %"class.std::__2::__map_const_iterator", %"class.std::__2::__map_const_iterator"* %it, i32 0, i32 0
  %coerce.dive2 = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %coerce.dive, i32 0, i32 0
  store %"class.std::__2::__tree_end_node"* %call, %"class.std::__2::__tree_end_node"** %coerce.dive2, align 4
  %options_3 = getelementptr inbounds %"class.draco::Options", %"class.draco::Options"* %this1, i32 0, i32 0
  %call4 = call %"class.std::__2::__tree_end_node"* @_ZNKSt3__23mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_NS_4lessIS6_EENS4_INS_4pairIKS6_S6_EEEEE3endEv(%"class.std::__2::map"* %options_3) #8
  %coerce.dive5 = getelementptr inbounds %"class.std::__2::__map_const_iterator", %"class.std::__2::__map_const_iterator"* %ref.tmp, i32 0, i32 0
  %coerce.dive6 = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %coerce.dive5, i32 0, i32 0
  store %"class.std::__2::__tree_end_node"* %call4, %"class.std::__2::__tree_end_node"** %coerce.dive6, align 4
  %call7 = call zeroext i1 @_ZNSt3__2eqERKNS_20__map_const_iteratorINS_21__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPNS_11__tree_nodeIS9_PvEElEEEESH_(%"class.std::__2::__map_const_iterator"* nonnull align 4 dereferenceable(4) %it, %"class.std::__2::__map_const_iterator"* nonnull align 4 dereferenceable(4) %ref.tmp)
  br i1 %call7, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load float, float* %default_val.addr, align 4
  store float %1, float* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %call8 = call %"struct.std::__2::pair"* @_ZNKSt3__220__map_const_iteratorINS_21__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPNS_11__tree_nodeIS9_PvEElEEEptEv(%"class.std::__2::__map_const_iterator"* %it)
  %second = getelementptr inbounds %"struct.std::__2::pair", %"struct.std::__2::pair"* %call8, i32 0, i32 1
  %call9 = call i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5c_strEv(%"class.std::__2::basic_string"* %second) #8
  %call10 = call double @atof(i8* %call9)
  %conv = fptrunc double %call10 to float
  store float %conv, float* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %2 = load float, float* %retval, align 4
  ret float %2
}

declare double @atof(i8*) #1

; Function Attrs: noinline nounwind optnone
define hidden zeroext i1 @_ZNK5draco7Options7GetBoolERKNSt3__212basic_stringIcNS1_11char_traitsIcEENS1_9allocatorIcEEEE(%"class.draco::Options"* %this, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %name) #0 {
entry:
  %this.addr = alloca %"class.draco::Options"*, align 4
  %name.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.draco::Options"* %this, %"class.draco::Options"** %this.addr, align 4
  store %"class.std::__2::basic_string"* %name, %"class.std::__2::basic_string"** %name.addr, align 4
  %this1 = load %"class.draco::Options"*, %"class.draco::Options"** %this.addr, align 4
  %0 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %name.addr, align 4
  %call = call zeroext i1 @_ZNK5draco7Options7GetBoolERKNSt3__212basic_stringIcNS1_11char_traitsIcEENS1_9allocatorIcEEEEb(%"class.draco::Options"* %this1, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %0, i1 zeroext false)
  ret i1 %call
}

; Function Attrs: noinline nounwind optnone
define hidden zeroext i1 @_ZNK5draco7Options7GetBoolERKNSt3__212basic_stringIcNS1_11char_traitsIcEENS1_9allocatorIcEEEEb(%"class.draco::Options"* %this, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %name, i1 zeroext %default_val) #0 {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::Options"*, align 4
  %name.addr = alloca %"class.std::__2::basic_string"*, align 4
  %default_val.addr = alloca i8, align 1
  %ret = alloca i32, align 4
  store %"class.draco::Options"* %this, %"class.draco::Options"** %this.addr, align 4
  store %"class.std::__2::basic_string"* %name, %"class.std::__2::basic_string"** %name.addr, align 4
  %frombool = zext i1 %default_val to i8
  store i8 %frombool, i8* %default_val.addr, align 1
  %this1 = load %"class.draco::Options"*, %"class.draco::Options"** %this.addr, align 4
  %0 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %name.addr, align 4
  %call = call i32 @_ZNK5draco7Options6GetIntERKNSt3__212basic_stringIcNS1_11char_traitsIcEENS1_9allocatorIcEEEEi(%"class.draco::Options"* %this1, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %0, i32 -1)
  store i32 %call, i32* %ret, align 4
  %1 = load i32, i32* %ret, align 4
  %cmp = icmp eq i32 %1, -1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load i8, i8* %default_val.addr, align 1
  %tobool = trunc i8 %2 to i1
  store i1 %tobool, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %3 = load i32, i32* %ret, align 4
  %tobool2 = icmp ne i32 %3, 0
  store i1 %tobool2, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %4 = load i1, i1* %retval, align 1
  ret i1 %4
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZNK5draco7Options9GetStringERKNSt3__212basic_stringIcNS1_11char_traitsIcEENS1_9allocatorIcEEEE(%"class.std::__2::basic_string"* noalias sret align 4 %agg.result, %"class.draco::Options"* %this, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %name) #0 {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %"class.draco::Options"*, align 4
  %name.addr = alloca %"class.std::__2::basic_string"*, align 4
  %ref.tmp = alloca %"class.std::__2::basic_string", align 4
  %0 = bitcast %"class.std::__2::basic_string"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %"class.draco::Options"* %this, %"class.draco::Options"** %this.addr, align 4
  store %"class.std::__2::basic_string"* %name, %"class.std::__2::basic_string"** %name.addr, align 4
  %this1 = load %"class.draco::Options"*, %"class.draco::Options"** %this.addr, align 4
  %1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %name.addr, align 4
  %call = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2IDnEEPKc(%"class.std::__2::basic_string"* %ref.tmp, i8* getelementptr inbounds ([1 x i8], [1 x i8]* @.str, i32 0, i32 0))
  call void @_ZNK5draco7Options9GetStringERKNSt3__212basic_stringIcNS1_11char_traitsIcEENS1_9allocatorIcEEEES9_(%"class.std::__2::basic_string"* sret align 4 %agg.result, %"class.draco::Options"* %this1, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %1, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %ref.tmp)
  %call2 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_string"* %ref.tmp) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZNK5draco7Options9GetStringERKNSt3__212basic_stringIcNS1_11char_traitsIcEENS1_9allocatorIcEEEES9_(%"class.std::__2::basic_string"* noalias sret align 4 %agg.result, %"class.draco::Options"* %this, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %name, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %default_val) #0 {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %"class.draco::Options"*, align 4
  %name.addr = alloca %"class.std::__2::basic_string"*, align 4
  %default_val.addr = alloca %"class.std::__2::basic_string"*, align 4
  %it = alloca %"class.std::__2::__map_const_iterator", align 4
  %ref.tmp = alloca %"class.std::__2::__map_const_iterator", align 4
  %0 = bitcast %"class.std::__2::basic_string"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %"class.draco::Options"* %this, %"class.draco::Options"** %this.addr, align 4
  store %"class.std::__2::basic_string"* %name, %"class.std::__2::basic_string"** %name.addr, align 4
  store %"class.std::__2::basic_string"* %default_val, %"class.std::__2::basic_string"** %default_val.addr, align 4
  %this1 = load %"class.draco::Options"*, %"class.draco::Options"** %this.addr, align 4
  %options_ = getelementptr inbounds %"class.draco::Options", %"class.draco::Options"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %name.addr, align 4
  %call = call %"class.std::__2::__tree_end_node"* @_ZNKSt3__23mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_NS_4lessIS6_EENS4_INS_4pairIKS6_S6_EEEEE4findERSA_(%"class.std::__2::map"* %options_, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %1)
  %coerce.dive = getelementptr inbounds %"class.std::__2::__map_const_iterator", %"class.std::__2::__map_const_iterator"* %it, i32 0, i32 0
  %coerce.dive2 = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %coerce.dive, i32 0, i32 0
  store %"class.std::__2::__tree_end_node"* %call, %"class.std::__2::__tree_end_node"** %coerce.dive2, align 4
  %options_3 = getelementptr inbounds %"class.draco::Options", %"class.draco::Options"* %this1, i32 0, i32 0
  %call4 = call %"class.std::__2::__tree_end_node"* @_ZNKSt3__23mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_NS_4lessIS6_EENS4_INS_4pairIKS6_S6_EEEEE3endEv(%"class.std::__2::map"* %options_3) #8
  %coerce.dive5 = getelementptr inbounds %"class.std::__2::__map_const_iterator", %"class.std::__2::__map_const_iterator"* %ref.tmp, i32 0, i32 0
  %coerce.dive6 = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %coerce.dive5, i32 0, i32 0
  store %"class.std::__2::__tree_end_node"* %call4, %"class.std::__2::__tree_end_node"** %coerce.dive6, align 4
  %call7 = call zeroext i1 @_ZNSt3__2eqERKNS_20__map_const_iteratorINS_21__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPNS_11__tree_nodeIS9_PvEElEEEESH_(%"class.std::__2::__map_const_iterator"* nonnull align 4 dereferenceable(4) %it, %"class.std::__2::__map_const_iterator"* nonnull align 4 dereferenceable(4) %ref.tmp)
  br i1 %call7, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %default_val.addr, align 4
  %call8 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKS5_(%"class.std::__2::basic_string"* %agg.result, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %2)
  br label %return

if.end:                                           ; preds = %entry
  %call9 = call %"struct.std::__2::pair"* @_ZNKSt3__220__map_const_iteratorINS_21__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPNS_11__tree_nodeIS9_PvEElEEEptEv(%"class.std::__2::__map_const_iterator"* %it)
  %second = getelementptr inbounds %"struct.std::__2::pair", %"struct.std::__2::pair"* %call9, i32 0, i32 1
  %call10 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKS5_(%"class.std::__2::basic_string"* %agg.result, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %second)
  br label %return

return:                                           ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2IDnEEPKc(%"class.std::__2::basic_string"* returned %this, i8* %__s) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  %__s.addr = alloca i8*, align 4
  %ref.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  %ref.tmp2 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  store i8* %__s, i8** %__s.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::basic_string"* %this1 to %"class.std::__2::__basic_string_common"*
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::__compressed_pair.4"* @_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_EC2INS_18__default_init_tagESA_EEOT_OT0_(%"class.std::__2::__compressed_pair.4"* %__r_, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  %1 = load i8*, i8** %__s.addr, align 4
  %2 = load i8*, i8** %__s.addr, align 4
  %call3 = call i32 @_ZNSt3__211char_traitsIcE6lengthEPKc(i8* %2) #8
  call void @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEPKcm(%"class.std::__2::basic_string"* %this1, i8* %1, i32 %call3)
  ret %"class.std::__2::basic_string"* %this1
}

declare %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKS5_(%"class.std::__2::basic_string"* returned, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12)) unnamed_addr #1

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__map_value_compare"* @_ZNSt3__219__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS6_S6_EENS_4lessIS6_EELb1EEC2ESA_(%"class.std::__2::__map_value_compare"* returned %this) unnamed_addr #0 comdat {
entry:
  %c = alloca %"struct.std::__2::less", align 1
  %this.addr = alloca %"class.std::__2::__map_value_compare"*, align 4
  store %"class.std::__2::__map_value_compare"* %this, %"class.std::__2::__map_value_compare"** %this.addr, align 4
  %this1 = load %"class.std::__2::__map_value_compare"*, %"class.std::__2::__map_value_compare"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__map_value_compare"* %this1 to %"struct.std::__2::less"*
  ret %"class.std::__2::__map_value_compare"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree"* @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEEC2ERKSC_(%"class.std::__2::__tree"* returned %this, %"class.std::__2::__map_value_compare"* nonnull align 1 dereferenceable(1) %__comp) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree"*, align 4
  %__comp.addr = alloca %"class.std::__2::__map_value_compare"*, align 4
  %ref.tmp = alloca i32, align 4
  store %"class.std::__2::__tree"* %this, %"class.std::__2::__tree"** %this.addr, align 4
  store %"class.std::__2::__map_value_compare"* %__comp, %"class.std::__2::__map_value_compare"** %__comp.addr, align 4
  %this1 = load %"class.std::__2::__tree"*, %"class.std::__2::__tree"** %this.addr, align 4
  %__pair1_ = getelementptr inbounds %"class.std::__2::__tree", %"class.std::__2::__tree"* %this1, i32 0, i32 1
  %call = call %"class.std::__2::__compressed_pair"* @_ZNSt3__217__compressed_pairINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEENS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS7_IcEEEESE_EES3_EEEEEC2ILb1EvEEv(%"class.std::__2::__compressed_pair"* %__pair1_)
  %__pair3_ = getelementptr inbounds %"class.std::__2::__tree", %"class.std::__2::__tree"* %this1, i32 0, i32 2
  store i32 0, i32* %ref.tmp, align 4
  %0 = load %"class.std::__2::__map_value_compare"*, %"class.std::__2::__map_value_compare"** %__comp.addr, align 4
  %call2 = call %"class.std::__2::__compressed_pair.1"* @_ZNSt3__217__compressed_pairImNS_19__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS7_S7_EENS_4lessIS7_EELb1EEEEC2IiRKSC_EEOT_OT0_(%"class.std::__2::__compressed_pair.1"* %__pair3_, i32* nonnull align 4 dereferenceable(4) %ref.tmp, %"class.std::__2::__map_value_compare"* nonnull align 1 dereferenceable(1) %0)
  %call3 = call %"class.std::__2::__tree_end_node"* @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE10__end_nodeEv(%"class.std::__2::__tree"* %this1) #8
  %call4 = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_end_node"** @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE12__begin_nodeEv(%"class.std::__2::__tree"* %this1) #8
  store %"class.std::__2::__tree_end_node"* %call3, %"class.std::__2::__tree_end_node"** %call4, align 4
  ret %"class.std::__2::__tree"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair"* @_ZNSt3__217__compressed_pairINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEENS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS7_IcEEEESE_EES3_EEEEEC2ILb1EvEEv(%"class.std::__2::__compressed_pair"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__value_init_tag", align 1
  %agg.tmp2 = alloca %"struct.std::__2::__value_init_tag", align 1
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %call = call %"struct.std::__2::__compressed_pair_elem"* @_ZNSt3__222__compressed_pair_elemINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEELi0ELb0EEC2ENS_16__value_init_tagE(%"struct.std::__2::__compressed_pair_elem"* %0)
  %1 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem.0"*
  %call3 = call %"struct.std::__2::__compressed_pair_elem.0"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEELi1ELb1EEC2ENS_16__value_init_tagE(%"struct.std::__2::__compressed_pair_elem.0"* %1)
  ret %"class.std::__2::__compressed_pair"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.1"* @_ZNSt3__217__compressed_pairImNS_19__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS7_S7_EENS_4lessIS7_EELb1EEEEC2IiRKSC_EEOT_OT0_(%"class.std::__2::__compressed_pair.1"* returned %this, i32* nonnull align 4 dereferenceable(4) %__t1, %"class.std::__2::__map_value_compare"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.1"*, align 4
  %__t1.addr = alloca i32*, align 4
  %__t2.addr = alloca %"class.std::__2::__map_value_compare"*, align 4
  store %"class.std::__2::__compressed_pair.1"* %this, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  store i32* %__t1, i32** %__t1.addr, align 4
  store %"class.std::__2::__map_value_compare"* %__t2, %"class.std::__2::__map_value_compare"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.1"*, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.1"* %this1 to %"struct.std::__2::__compressed_pair_elem.2"*
  %1 = load i32*, i32** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__27forwardIiEEOT_RNS_16remove_referenceIS1_E4typeE(i32* nonnull align 4 dereferenceable(4) %1) #8
  %call2 = call %"struct.std::__2::__compressed_pair_elem.2"* @_ZNSt3__222__compressed_pair_elemImLi0ELb0EEC2IivEEOT_(%"struct.std::__2::__compressed_pair_elem.2"* %0, i32* nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.1"* %this1 to %"struct.std::__2::__compressed_pair_elem.3"*
  %3 = load %"class.std::__2::__map_value_compare"*, %"class.std::__2::__map_value_compare"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"class.std::__2::__map_value_compare"* @_ZNSt3__27forwardIRKNS_19__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS7_S7_EENS_4lessIS7_EELb1EEEEEOT_RNS_16remove_referenceISF_E4typeE(%"class.std::__2::__map_value_compare"* nonnull align 1 dereferenceable(1) %3) #8
  %call4 = call %"struct.std::__2::__compressed_pair_elem.3"* @_ZNSt3__222__compressed_pair_elemINS_19__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS7_S7_EENS_4lessIS7_EELb1EEELi1ELb1EEC2IRKSC_vEEOT_(%"struct.std::__2::__compressed_pair_elem.3"* %2, %"class.std::__2::__map_value_compare"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::__compressed_pair.1"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_end_node"* @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE10__end_nodeEv(%"class.std::__2::__tree"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree"*, align 4
  store %"class.std::__2::__tree"* %this, %"class.std::__2::__tree"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree"*, %"class.std::__2::__tree"** %this.addr, align 4
  %__pair1_ = getelementptr inbounds %"class.std::__2::__tree", %"class.std::__2::__tree"* %this1, i32 0, i32 1
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_end_node"* @_ZNSt3__217__compressed_pairINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEENS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS7_IcEEEESE_EES3_EEEEE5firstEv(%"class.std::__2::__compressed_pair"* %__pair1_) #8
  %call2 = call %"class.std::__2::__tree_end_node"* @_ZNSt3__214pointer_traitsIPNS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEEE10pointer_toERS6_(%"class.std::__2::__tree_end_node"* nonnull align 4 dereferenceable(4) %call) #8
  ret %"class.std::__2::__tree_end_node"* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_end_node"** @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE12__begin_nodeEv(%"class.std::__2::__tree"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree"*, align 4
  store %"class.std::__2::__tree"* %this, %"class.std::__2::__tree"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree"*, %"class.std::__2::__tree"** %this.addr, align 4
  %__begin_node_ = getelementptr inbounds %"class.std::__2::__tree", %"class.std::__2::__tree"* %this1, i32 0, i32 0
  ret %"class.std::__2::__tree_end_node"** %__begin_node_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem"* @_ZNSt3__222__compressed_pair_elemINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEELi0ELb0EEC2ENS_16__value_init_tagE(%"struct.std::__2::__compressed_pair_elem"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__value_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem"*, align 4
  store %"struct.std::__2::__compressed_pair_elem"* %this, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem"*, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::__tree_end_node"* @_ZNSt3__215__tree_end_nodeIPNS_16__tree_node_baseIPvEEEC2Ev(%"class.std::__2::__tree_end_node"* %__value_) #8
  ret %"struct.std::__2::__compressed_pair_elem"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.0"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEELi1ELb1EEC2ENS_16__value_init_tagE(%"struct.std::__2::__compressed_pair_elem.0"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__value_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.0"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.0"* %this, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.0"*, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.0"* %this1 to %"class.std::__2::allocator"*
  %call = call %"class.std::__2::allocator"* @_ZNSt3__29allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEES7_EEPvEEEC2Ev(%"class.std::__2::allocator"* %1) #8
  ret %"struct.std::__2::__compressed_pair_elem.0"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_end_node"* @_ZNSt3__215__tree_end_nodeIPNS_16__tree_node_baseIPvEEEC2Ev(%"class.std::__2::__tree_end_node"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree_end_node"*, align 4
  store %"class.std::__2::__tree_end_node"* %this, %"class.std::__2::__tree_end_node"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %this.addr, align 4
  %__left_ = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %this1, i32 0, i32 0
  store %"class.std::__2::__tree_node_base"* null, %"class.std::__2::__tree_node_base"** %__left_, align 4
  ret %"class.std::__2::__tree_end_node"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::allocator"* @_ZNSt3__29allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEES7_EEPvEEEC2Ev(%"class.std::__2::allocator"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  ret %"class.std::__2::allocator"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__27forwardIiEEOT_RNS_16remove_referenceIS1_E4typeE(i32* nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca i32*, align 4
  store i32* %__t, i32** %__t.addr, align 4
  %0 = load i32*, i32** %__t.addr, align 4
  ret i32* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.2"* @_ZNSt3__222__compressed_pair_elemImLi0ELb0EEC2IivEEOT_(%"struct.std::__2::__compressed_pair_elem.2"* returned %this, i32* nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.2"*, align 4
  %__u.addr = alloca i32*, align 4
  store %"struct.std::__2::__compressed_pair_elem.2"* %this, %"struct.std::__2::__compressed_pair_elem.2"** %this.addr, align 4
  store i32* %__u, i32** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.2"*, %"struct.std::__2::__compressed_pair_elem.2"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.2", %"struct.std::__2::__compressed_pair_elem.2"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__27forwardIiEEOT_RNS_16remove_referenceIS1_E4typeE(i32* nonnull align 4 dereferenceable(4) %0) #8
  %1 = load i32, i32* %call, align 4
  store i32 %1, i32* %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.2"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::__map_value_compare"* @_ZNSt3__27forwardIRKNS_19__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS7_S7_EENS_4lessIS7_EELb1EEEEEOT_RNS_16remove_referenceISF_E4typeE(%"class.std::__2::__map_value_compare"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::__map_value_compare"*, align 4
  store %"class.std::__2::__map_value_compare"* %__t, %"class.std::__2::__map_value_compare"** %__t.addr, align 4
  %0 = load %"class.std::__2::__map_value_compare"*, %"class.std::__2::__map_value_compare"** %__t.addr, align 4
  ret %"class.std::__2::__map_value_compare"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.3"* @_ZNSt3__222__compressed_pair_elemINS_19__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS7_S7_EENS_4lessIS7_EELb1EEELi1ELb1EEC2IRKSC_vEEOT_(%"struct.std::__2::__compressed_pair_elem.3"* returned %this, %"class.std::__2::__map_value_compare"* nonnull align 1 dereferenceable(1) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.3"*, align 4
  %__u.addr = alloca %"class.std::__2::__map_value_compare"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.3"* %this, %"struct.std::__2::__compressed_pair_elem.3"** %this.addr, align 4
  store %"class.std::__2::__map_value_compare"* %__u, %"class.std::__2::__map_value_compare"** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.3"*, %"struct.std::__2::__compressed_pair_elem.3"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.3"* %this1 to %"class.std::__2::__map_value_compare"*
  %1 = load %"class.std::__2::__map_value_compare"*, %"class.std::__2::__map_value_compare"** %__u.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::__map_value_compare"* @_ZNSt3__27forwardIRKNS_19__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS7_S7_EENS_4lessIS7_EELb1EEEEEOT_RNS_16remove_referenceISF_E4typeE(%"class.std::__2::__map_value_compare"* nonnull align 1 dereferenceable(1) %1) #8
  ret %"struct.std::__2::__compressed_pair_elem.3"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_end_node"* @_ZNSt3__214pointer_traitsIPNS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEEE10pointer_toERS6_(%"class.std::__2::__tree_end_node"* nonnull align 4 dereferenceable(4) %__r) #0 comdat {
entry:
  %__r.addr = alloca %"class.std::__2::__tree_end_node"*, align 4
  store %"class.std::__2::__tree_end_node"* %__r, %"class.std::__2::__tree_end_node"** %__r.addr, align 4
  %0 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %__r.addr, align 4
  %call = call %"class.std::__2::__tree_end_node"* @_ZNSt3__29addressofINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEEEEPT_RS7_(%"class.std::__2::__tree_end_node"* nonnull align 4 dereferenceable(4) %0) #8
  ret %"class.std::__2::__tree_end_node"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_end_node"* @_ZNSt3__217__compressed_pairINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEENS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS7_IcEEEESE_EES3_EEEEE5firstEv(%"class.std::__2::__compressed_pair"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_end_node"* @_ZNSt3__222__compressed_pair_elemINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %0) #8
  ret %"class.std::__2::__tree_end_node"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_end_node"* @_ZNSt3__29addressofINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEEEEPT_RS7_(%"class.std::__2::__tree_end_node"* nonnull align 4 dereferenceable(4) %__x) #0 comdat {
entry:
  %__x.addr = alloca %"class.std::__2::__tree_end_node"*, align 4
  store %"class.std::__2::__tree_end_node"* %__x, %"class.std::__2::__tree_end_node"** %__x.addr, align 4
  %0 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %__x.addr, align 4
  ret %"class.std::__2::__tree_end_node"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_end_node"* @_ZNSt3__222__compressed_pair_elemINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem"*, align 4
  store %"struct.std::__2::__compressed_pair_elem"* %this, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem"*, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem"* %this1, i32 0, i32 0
  ret %"class.std::__2::__tree_end_node"* %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_end_node"* @_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE5beginEv(%"class.std::__2::__tree"* %this) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__tree_const_iterator", align 4
  %this.addr = alloca %"class.std::__2::__tree"*, align 4
  store %"class.std::__2::__tree"* %this, %"class.std::__2::__tree"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree"*, %"class.std::__2::__tree"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_end_node"** @_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE12__begin_nodeEv(%"class.std::__2::__tree"* %this1) #8
  %0 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %call, align 4
  %call2 = call %"class.std::__2::__tree_const_iterator"* @_ZNSt3__221__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElEC2EPNS_15__tree_end_nodeIPNS_16__tree_node_baseISA_EEEE(%"class.std::__2::__tree_const_iterator"* %retval, %"class.std::__2::__tree_end_node"* %0) #8
  %coerce.dive = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %retval, i32 0, i32 0
  %1 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %coerce.dive, align 4
  ret %"class.std::__2::__tree_end_node"* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__map_const_iterator"* @_ZNSt3__220__map_const_iteratorINS_21__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPNS_11__tree_nodeIS9_PvEElEEEC2ESE_(%"class.std::__2::__map_const_iterator"* returned %this, %"class.std::__2::__tree_end_node"* %__i.coerce) unnamed_addr #0 comdat {
entry:
  %__i = alloca %"class.std::__2::__tree_const_iterator", align 4
  %this.addr = alloca %"class.std::__2::__map_const_iterator"*, align 4
  %coerce.dive = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %__i, i32 0, i32 0
  store %"class.std::__2::__tree_end_node"* %__i.coerce, %"class.std::__2::__tree_end_node"** %coerce.dive, align 4
  store %"class.std::__2::__map_const_iterator"* %this, %"class.std::__2::__map_const_iterator"** %this.addr, align 4
  %this1 = load %"class.std::__2::__map_const_iterator"*, %"class.std::__2::__map_const_iterator"** %this.addr, align 4
  %__i_ = getelementptr inbounds %"class.std::__2::__map_const_iterator", %"class.std::__2::__map_const_iterator"* %this1, i32 0, i32 0
  %0 = bitcast %"class.std::__2::__tree_const_iterator"* %__i_ to i8*
  %1 = bitcast %"class.std::__2::__tree_const_iterator"* %__i to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %0, i8* align 4 %1, i32 4, i1 false)
  ret %"class.std::__2::__map_const_iterator"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_end_node"** @_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE12__begin_nodeEv(%"class.std::__2::__tree"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree"*, align 4
  store %"class.std::__2::__tree"* %this, %"class.std::__2::__tree"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree"*, %"class.std::__2::__tree"** %this.addr, align 4
  %__begin_node_ = getelementptr inbounds %"class.std::__2::__tree", %"class.std::__2::__tree"* %this1, i32 0, i32 0
  ret %"class.std::__2::__tree_end_node"** %__begin_node_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_const_iterator"* @_ZNSt3__221__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElEC2EPNS_15__tree_end_nodeIPNS_16__tree_node_baseISA_EEEE(%"class.std::__2::__tree_const_iterator"* returned %this, %"class.std::__2::__tree_end_node"* %__p) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree_const_iterator"*, align 4
  %__p.addr = alloca %"class.std::__2::__tree_end_node"*, align 4
  store %"class.std::__2::__tree_const_iterator"* %this, %"class.std::__2::__tree_const_iterator"** %this.addr, align 4
  store %"class.std::__2::__tree_end_node"* %__p, %"class.std::__2::__tree_end_node"** %__p.addr, align 4
  %this1 = load %"class.std::__2::__tree_const_iterator"*, %"class.std::__2::__tree_const_iterator"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %__p.addr, align 4
  store %"class.std::__2::__tree_end_node"* %0, %"class.std::__2::__tree_end_node"** %__ptr_, align 4
  ret %"class.std::__2::__tree_const_iterator"* %this1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_end_node"* @_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE3endEv(%"class.std::__2::__tree"* %this) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__tree_const_iterator", align 4
  %this.addr = alloca %"class.std::__2::__tree"*, align 4
  store %"class.std::__2::__tree"* %this, %"class.std::__2::__tree"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree"*, %"class.std::__2::__tree"** %this.addr, align 4
  %call = call %"class.std::__2::__tree_end_node"* @_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE10__end_nodeEv(%"class.std::__2::__tree"* %this1) #8
  %call2 = call %"class.std::__2::__tree_const_iterator"* @_ZNSt3__221__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElEC2EPNS_15__tree_end_nodeIPNS_16__tree_node_baseISA_EEEE(%"class.std::__2::__tree_const_iterator"* %retval, %"class.std::__2::__tree_end_node"* %call) #8
  %coerce.dive = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %retval, i32 0, i32 0
  %0 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %coerce.dive, align 4
  ret %"class.std::__2::__tree_end_node"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_end_node"* @_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE10__end_nodeEv(%"class.std::__2::__tree"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree"*, align 4
  store %"class.std::__2::__tree"* %this, %"class.std::__2::__tree"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree"*, %"class.std::__2::__tree"** %this.addr, align 4
  %__pair1_ = getelementptr inbounds %"class.std::__2::__tree", %"class.std::__2::__tree"* %this1, i32 0, i32 1
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_end_node"* @_ZNKSt3__217__compressed_pairINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEENS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS7_IcEEEESE_EES3_EEEEE5firstEv(%"class.std::__2::__compressed_pair"* %__pair1_) #8
  %call2 = call %"class.std::__2::__tree_end_node"* @_ZNSt3__214pointer_traitsIPNS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEEE10pointer_toERS6_(%"class.std::__2::__tree_end_node"* nonnull align 4 dereferenceable(4) %call) #8
  ret %"class.std::__2::__tree_end_node"* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_end_node"* @_ZNKSt3__217__compressed_pairINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEENS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS7_IcEEEESE_EES3_EEEEE5firstEv(%"class.std::__2::__compressed_pair"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_end_node"* @_ZNKSt3__222__compressed_pair_elemINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %0) #8
  ret %"class.std::__2::__tree_end_node"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_end_node"* @_ZNKSt3__222__compressed_pair_elemINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem"*, align 4
  store %"struct.std::__2::__compressed_pair_elem"* %this, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem"*, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem"* %this1, i32 0, i32 0
  ret %"class.std::__2::__tree_end_node"* %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNSt3__2neERKNS_21__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElEESF_(%"class.std::__2::__tree_const_iterator"* nonnull align 4 dereferenceable(4) %__x, %"class.std::__2::__tree_const_iterator"* nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %__x.addr = alloca %"class.std::__2::__tree_const_iterator"*, align 4
  %__y.addr = alloca %"class.std::__2::__tree_const_iterator"*, align 4
  store %"class.std::__2::__tree_const_iterator"* %__x, %"class.std::__2::__tree_const_iterator"** %__x.addr, align 4
  store %"class.std::__2::__tree_const_iterator"* %__y, %"class.std::__2::__tree_const_iterator"** %__y.addr, align 4
  %0 = load %"class.std::__2::__tree_const_iterator"*, %"class.std::__2::__tree_const_iterator"** %__x.addr, align 4
  %1 = load %"class.std::__2::__tree_const_iterator"*, %"class.std::__2::__tree_const_iterator"** %__y.addr, align 4
  %call = call zeroext i1 @_ZNSt3__2eqERKNS_21__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElEESF_(%"class.std::__2::__tree_const_iterator"* nonnull align 4 dereferenceable(4) %0, %"class.std::__2::__tree_const_iterator"* nonnull align 4 dereferenceable(4) %1)
  %lnot = xor i1 %call, true
  ret i1 %lnot
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNSt3__2eqERKNS_21__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElEESF_(%"class.std::__2::__tree_const_iterator"* nonnull align 4 dereferenceable(4) %__x, %"class.std::__2::__tree_const_iterator"* nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %__x.addr = alloca %"class.std::__2::__tree_const_iterator"*, align 4
  %__y.addr = alloca %"class.std::__2::__tree_const_iterator"*, align 4
  store %"class.std::__2::__tree_const_iterator"* %__x, %"class.std::__2::__tree_const_iterator"** %__x.addr, align 4
  store %"class.std::__2::__tree_const_iterator"* %__y, %"class.std::__2::__tree_const_iterator"** %__y.addr, align 4
  %0 = load %"class.std::__2::__tree_const_iterator"*, %"class.std::__2::__tree_const_iterator"** %__x.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %0, i32 0, i32 0
  %1 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %__ptr_, align 4
  %2 = load %"class.std::__2::__tree_const_iterator"*, %"class.std::__2::__tree_const_iterator"** %__y.addr, align 4
  %__ptr_1 = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %2, i32 0, i32 0
  %3 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %__ptr_1, align 4
  %cmp = icmp eq %"class.std::__2::__tree_end_node"* %1, %3
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_const_iterator"* @_ZNSt3__221__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElEppEv(%"class.std::__2::__tree_const_iterator"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree_const_iterator"*, align 4
  store %"class.std::__2::__tree_const_iterator"* %this, %"class.std::__2::__tree_const_iterator"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree_const_iterator"*, %"class.std::__2::__tree_const_iterator"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %__ptr_, align 4
  %1 = bitcast %"class.std::__2::__tree_end_node"* %0 to %"class.std::__2::__tree_node_base"*
  %call = call %"class.std::__2::__tree_end_node"* @_ZNSt3__216__tree_next_iterIPNS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEES5_EET_T0_(%"class.std::__2::__tree_node_base"* %1) #8
  %__ptr_2 = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %this1, i32 0, i32 0
  store %"class.std::__2::__tree_end_node"* %call, %"class.std::__2::__tree_end_node"** %__ptr_2, align 4
  ret %"class.std::__2::__tree_const_iterator"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_end_node"* @_ZNSt3__216__tree_next_iterIPNS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEES5_EET_T0_(%"class.std::__2::__tree_node_base"* %__x) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__tree_end_node"*, align 4
  %__x.addr = alloca %"class.std::__2::__tree_node_base"*, align 4
  store %"class.std::__2::__tree_node_base"* %__x, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %0 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %__right_ = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %0, i32 0, i32 1
  %1 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__right_, align 4
  %cmp = icmp ne %"class.std::__2::__tree_node_base"* %1, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %__right_1 = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %2, i32 0, i32 1
  %3 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__right_1, align 4
  %call = call %"class.std::__2::__tree_node_base"* @_ZNSt3__210__tree_minIPNS_16__tree_node_baseIPvEEEET_S5_(%"class.std::__2::__tree_node_base"* %3) #8
  %4 = bitcast %"class.std::__2::__tree_node_base"* %call to %"class.std::__2::__tree_end_node"*
  store %"class.std::__2::__tree_end_node"* %4, %"class.std::__2::__tree_end_node"** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.end
  %5 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %call2 = call zeroext i1 @_ZNSt3__220__tree_is_left_childIPNS_16__tree_node_baseIPvEEEEbT_(%"class.std::__2::__tree_node_base"* %5) #8
  %lnot = xor i1 %call2, true
  br i1 %lnot, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %6 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %call3 = call %"class.std::__2::__tree_node_base"* @_ZNKSt3__216__tree_node_baseIPvE15__parent_unsafeEv(%"class.std::__2::__tree_node_base"* %6)
  store %"class.std::__2::__tree_node_base"* %call3, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %7 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %__parent_ = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %7, i32 0, i32 2
  %8 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %__parent_, align 4
  store %"class.std::__2::__tree_end_node"* %8, %"class.std::__2::__tree_end_node"** %retval, align 4
  br label %return

return:                                           ; preds = %while.end, %if.then
  %9 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %retval, align 4
  ret %"class.std::__2::__tree_end_node"* %9
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_node_base"* @_ZNSt3__210__tree_minIPNS_16__tree_node_baseIPvEEEET_S5_(%"class.std::__2::__tree_node_base"* %__x) #0 comdat {
entry:
  %__x.addr = alloca %"class.std::__2::__tree_node_base"*, align 4
  store %"class.std::__2::__tree_node_base"* %__x, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %0 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %1 = bitcast %"class.std::__2::__tree_node_base"* %0 to %"class.std::__2::__tree_end_node"*
  %__left_ = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %1, i32 0, i32 0
  %2 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__left_, align 4
  %cmp = icmp ne %"class.std::__2::__tree_node_base"* %2, null
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %3 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %4 = bitcast %"class.std::__2::__tree_node_base"* %3 to %"class.std::__2::__tree_end_node"*
  %__left_1 = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %4, i32 0, i32 0
  %5 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__left_1, align 4
  store %"class.std::__2::__tree_node_base"* %5, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %6 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  ret %"class.std::__2::__tree_node_base"* %6
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNSt3__220__tree_is_left_childIPNS_16__tree_node_baseIPvEEEEbT_(%"class.std::__2::__tree_node_base"* %__x) #0 comdat {
entry:
  %__x.addr = alloca %"class.std::__2::__tree_node_base"*, align 4
  store %"class.std::__2::__tree_node_base"* %__x, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %0 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %1 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %__parent_ = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %1, i32 0, i32 2
  %2 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %__parent_, align 4
  %__left_ = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %2, i32 0, i32 0
  %3 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__left_, align 4
  %cmp = icmp eq %"class.std::__2::__tree_node_base"* %0, %3
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_node_base"* @_ZNKSt3__216__tree_node_baseIPvE15__parent_unsafeEv(%"class.std::__2::__tree_node_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree_node_base"*, align 4
  store %"class.std::__2::__tree_node_base"* %this, %"class.std::__2::__tree_node_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %this.addr, align 4
  %__parent_ = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %this1, i32 0, i32 2
  %0 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %__parent_, align 4
  %1 = bitcast %"class.std::__2::__tree_end_node"* %0 to %"class.std::__2::__tree_node_base"*
  ret %"class.std::__2::__tree_node_base"* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__value_type"* @_ZNKSt3__221__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElEptEv(%"class.std::__2::__tree_const_iterator"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree_const_iterator"*, align 4
  store %"class.std::__2::__tree_const_iterator"* %this, %"class.std::__2::__tree_const_iterator"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree_const_iterator"*, %"class.std::__2::__tree_const_iterator"** %this.addr, align 4
  %call = call %"class.std::__2::__tree_node"* @_ZNKSt3__221__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElE8__get_npEv(%"class.std::__2::__tree_const_iterator"* %this1)
  %__value_ = getelementptr inbounds %"class.std::__2::__tree_node", %"class.std::__2::__tree_node"* %call, i32 0, i32 1
  %call2 = call %"struct.std::__2::__value_type"* @_ZNSt3__214pointer_traitsIPKNS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEE10pointer_toERS9_(%"struct.std::__2::__value_type"* nonnull align 4 dereferenceable(24) %__value_) #8
  ret %"struct.std::__2::__value_type"* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(24) %"struct.std::__2::pair"* @_ZNKSt3__212__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_E11__get_valueEv(%"struct.std::__2::__value_type"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__value_type"*, align 4
  store %"struct.std::__2::__value_type"* %this, %"struct.std::__2::__value_type"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__value_type"*, %"struct.std::__2::__value_type"** %this.addr, align 4
  %__cc = getelementptr inbounds %"struct.std::__2::__value_type", %"struct.std::__2::__value_type"* %this1, i32 0, i32 0
  ret %"struct.std::__2::pair"* %__cc
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__value_type"* @_ZNSt3__214pointer_traitsIPKNS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEE10pointer_toERS9_(%"struct.std::__2::__value_type"* nonnull align 4 dereferenceable(24) %__r) #0 comdat {
entry:
  %__r.addr = alloca %"struct.std::__2::__value_type"*, align 4
  store %"struct.std::__2::__value_type"* %__r, %"struct.std::__2::__value_type"** %__r.addr, align 4
  %0 = load %"struct.std::__2::__value_type"*, %"struct.std::__2::__value_type"** %__r.addr, align 4
  %call = call %"struct.std::__2::__value_type"* @_ZNSt3__29addressofIKNS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEEEPT_RSA_(%"struct.std::__2::__value_type"* nonnull align 4 dereferenceable(24) %0) #8
  ret %"struct.std::__2::__value_type"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_node"* @_ZNKSt3__221__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElE8__get_npEv(%"class.std::__2::__tree_const_iterator"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree_const_iterator"*, align 4
  store %"class.std::__2::__tree_const_iterator"* %this, %"class.std::__2::__tree_const_iterator"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree_const_iterator"*, %"class.std::__2::__tree_const_iterator"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %__ptr_, align 4
  %1 = bitcast %"class.std::__2::__tree_end_node"* %0 to %"class.std::__2::__tree_node"*
  ret %"class.std::__2::__tree_node"* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__value_type"* @_ZNSt3__29addressofIKNS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEEEPT_RSA_(%"struct.std::__2::__value_type"* nonnull align 4 dereferenceable(24) %__x) #0 comdat {
entry:
  %__x.addr = alloca %"struct.std::__2::__value_type"*, align 4
  store %"struct.std::__2::__value_type"* %__x, %"struct.std::__2::__value_type"** %__x.addr, align 4
  %0 = load %"struct.std::__2::__value_type"*, %"struct.std::__2::__value_type"** %__x.addr, align 4
  ret %"struct.std::__2::__value_type"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE25__emplace_unique_key_argsIS7_JRKNS_21piecewise_construct_tENS_5tupleIJRKS7_EEENSJ_IJEEEEEENS_4pairINS_15__tree_iteratorIS8_PNS_11__tree_nodeIS8_PvEElEEbEERKT_DpOT0_(%"struct.std::__2::pair.9"* noalias sret align 4 %agg.result, %"class.std::__2::__tree"* %this, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %__k, %"struct.std::__2::piecewise_construct_t"* nonnull align 1 dereferenceable(1) %__args, %"class.std::__2::tuple"* nonnull align 4 dereferenceable(4) %__args1, %"class.std::__2::tuple.10"* nonnull align 1 dereferenceable(1) %__args3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree"*, align 4
  %__k.addr = alloca %"class.std::__2::basic_string"*, align 4
  %__args.addr = alloca %"struct.std::__2::piecewise_construct_t"*, align 4
  %__args.addr2 = alloca %"class.std::__2::tuple"*, align 4
  %__args.addr4 = alloca %"class.std::__2::tuple.10"*, align 4
  %__parent = alloca %"class.std::__2::__tree_end_node"*, align 4
  %__child = alloca %"class.std::__2::__tree_node_base"**, align 4
  %__r = alloca %"class.std::__2::__tree_node"*, align 4
  %__inserted = alloca i8, align 1
  %__h = alloca %"class.std::__2::unique_ptr", align 4
  %ref.tmp = alloca %"class.std::__2::__tree_iterator", align 4
  store %"class.std::__2::__tree"* %this, %"class.std::__2::__tree"** %this.addr, align 4
  store %"class.std::__2::basic_string"* %__k, %"class.std::__2::basic_string"** %__k.addr, align 4
  store %"struct.std::__2::piecewise_construct_t"* %__args, %"struct.std::__2::piecewise_construct_t"** %__args.addr, align 4
  store %"class.std::__2::tuple"* %__args1, %"class.std::__2::tuple"** %__args.addr2, align 4
  store %"class.std::__2::tuple.10"* %__args3, %"class.std::__2::tuple.10"** %__args.addr4, align 4
  %this5 = load %"class.std::__2::__tree"*, %"class.std::__2::__tree"** %this.addr, align 4
  %0 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__k.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_node_base"** @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE12__find_equalIS7_EERPNS_16__tree_node_baseIPvEERPNS_15__tree_end_nodeISJ_EERKT_(%"class.std::__2::__tree"* %this5, %"class.std::__2::__tree_end_node"** nonnull align 4 dereferenceable(4) %__parent, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %0)
  store %"class.std::__2::__tree_node_base"** %call, %"class.std::__2::__tree_node_base"*** %__child, align 4
  %1 = load %"class.std::__2::__tree_node_base"**, %"class.std::__2::__tree_node_base"*** %__child, align 4
  %2 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %1, align 4
  %3 = bitcast %"class.std::__2::__tree_node_base"* %2 to %"class.std::__2::__tree_node"*
  store %"class.std::__2::__tree_node"* %3, %"class.std::__2::__tree_node"** %__r, align 4
  store i8 0, i8* %__inserted, align 1
  %4 = load %"class.std::__2::__tree_node_base"**, %"class.std::__2::__tree_node_base"*** %__child, align 4
  %5 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %4, align 4
  %cmp = icmp eq %"class.std::__2::__tree_node_base"* %5, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %6 = load %"struct.std::__2::piecewise_construct_t"*, %"struct.std::__2::piecewise_construct_t"** %__args.addr, align 4
  %call6 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::piecewise_construct_t"* @_ZNSt3__27forwardIRKNS_21piecewise_construct_tEEEOT_RNS_16remove_referenceIS4_E4typeE(%"struct.std::__2::piecewise_construct_t"* nonnull align 1 dereferenceable(1) %6) #8
  %7 = load %"class.std::__2::tuple"*, %"class.std::__2::tuple"** %__args.addr2, align 4
  %call7 = call nonnull align 4 dereferenceable(4) %"class.std::__2::tuple"* @_ZNSt3__27forwardINS_5tupleIJRKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEEEEEOT_RNS_16remove_referenceISB_E4typeE(%"class.std::__2::tuple"* nonnull align 4 dereferenceable(4) %7) #8
  %8 = load %"class.std::__2::tuple.10"*, %"class.std::__2::tuple.10"** %__args.addr4, align 4
  %call8 = call nonnull align 1 dereferenceable(1) %"class.std::__2::tuple.10"* @_ZNSt3__27forwardINS_5tupleIJEEEEEOT_RNS_16remove_referenceIS3_E4typeE(%"class.std::__2::tuple.10"* nonnull align 1 dereferenceable(1) %8) #8
  call void @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE16__construct_nodeIJRKNS_21piecewise_construct_tENS_5tupleIJRKS7_EEENSJ_IJEEEEEENS_10unique_ptrINS_11__tree_nodeIS8_PvEENS_22__tree_node_destructorINS5_ISR_EEEEEEDpOT_(%"class.std::__2::unique_ptr"* sret align 4 %__h, %"class.std::__2::__tree"* %this5, %"struct.std::__2::piecewise_construct_t"* nonnull align 1 dereferenceable(1) %call6, %"class.std::__2::tuple"* nonnull align 4 dereferenceable(4) %call7, %"class.std::__2::tuple.10"* nonnull align 1 dereferenceable(1) %call8)
  %9 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %__parent, align 4
  %10 = load %"class.std::__2::__tree_node_base"**, %"class.std::__2::__tree_node_base"*** %__child, align 4
  %call9 = call %"class.std::__2::__tree_node"* @_ZNKSt3__210unique_ptrINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEE3getEv(%"class.std::__2::unique_ptr"* %__h) #8
  %11 = bitcast %"class.std::__2::__tree_node"* %call9 to %"class.std::__2::__tree_node_base"*
  call void @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE16__insert_node_atEPNS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEERSJ_SJ_(%"class.std::__2::__tree"* %this5, %"class.std::__2::__tree_end_node"* %9, %"class.std::__2::__tree_node_base"** nonnull align 4 dereferenceable(4) %10, %"class.std::__2::__tree_node_base"* %11) #8
  %call10 = call %"class.std::__2::__tree_node"* @_ZNSt3__210unique_ptrINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEE7releaseEv(%"class.std::__2::unique_ptr"* %__h) #8
  store %"class.std::__2::__tree_node"* %call10, %"class.std::__2::__tree_node"** %__r, align 4
  store i8 1, i8* %__inserted, align 1
  %call11 = call %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEED2Ev(%"class.std::__2::unique_ptr"* %__h) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %12 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %__r, align 4
  %call12 = call %"class.std::__2::__tree_iterator"* @_ZNSt3__215__tree_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElEC2ESC_(%"class.std::__2::__tree_iterator"* %ref.tmp, %"class.std::__2::__tree_node"* %12) #8
  %call13 = call %"struct.std::__2::pair.9"* @_ZNSt3__24pairINS_15__tree_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPNS_11__tree_nodeIS9_PvEElEEbEC2ISE_RbLb0EEEOT_OT0_(%"struct.std::__2::pair.9"* %agg.result, %"class.std::__2::__tree_iterator"* nonnull align 4 dereferenceable(4) %ref.tmp, i8* nonnull align 1 dereferenceable(1) %__inserted) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::basic_string"* @_ZNSt3__216forward_as_tupleIJRKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEEENS_5tupleIJDpOT_EEESC_(%"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %__t) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::tuple", align 4
  %__t.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %__t, %"class.std::__2::basic_string"** %__t.addr, align 4
  %0 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__t.addr, align 4
  %call = call nonnull align 4 dereferenceable(12) %"class.std::__2::basic_string"* @_ZNSt3__27forwardIRKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEEOT_RNS_16remove_referenceIS9_E4typeE(%"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %0) #8
  %call1 = call %"class.std::__2::tuple"* @_ZNSt3__25tupleIJRKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEEC2ILb1ELb0EEES8_(%"class.std::__2::tuple"* %retval, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %call) #8
  %coerce.dive = getelementptr inbounds %"class.std::__2::tuple", %"class.std::__2::tuple"* %retval, i32 0, i32 0
  %coerce.dive2 = getelementptr inbounds %"struct.std::__2::__tuple_impl", %"struct.std::__2::__tuple_impl"* %coerce.dive, i32 0, i32 0
  %coerce.dive3 = getelementptr inbounds %"class.std::__2::__tuple_leaf", %"class.std::__2::__tuple_leaf"* %coerce.dive2, i32 0, i32 0
  %1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %coerce.dive3, align 4
  ret %"class.std::__2::basic_string"* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216forward_as_tupleIJEEENS_5tupleIJDpOT_EEES4_() #0 comdat {
entry:
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__value_type"* @_ZNKSt3__215__tree_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElEptEv(%"class.std::__2::__tree_iterator"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree_iterator"*, align 4
  store %"class.std::__2::__tree_iterator"* %this, %"class.std::__2::__tree_iterator"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree_iterator"*, %"class.std::__2::__tree_iterator"** %this.addr, align 4
  %call = call %"class.std::__2::__tree_node"* @_ZNKSt3__215__tree_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElE8__get_npEv(%"class.std::__2::__tree_iterator"* %this1)
  %__value_ = getelementptr inbounds %"class.std::__2::__tree_node", %"class.std::__2::__tree_node"* %call, i32 0, i32 1
  %call2 = call %"struct.std::__2::__value_type"* @_ZNSt3__214pointer_traitsIPNS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEE10pointer_toERS8_(%"struct.std::__2::__value_type"* nonnull align 4 dereferenceable(24) %__value_) #8
  ret %"struct.std::__2::__value_type"* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(24) %"struct.std::__2::pair"* @_ZNSt3__212__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_E11__get_valueEv(%"struct.std::__2::__value_type"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__value_type"*, align 4
  store %"struct.std::__2::__value_type"* %this, %"struct.std::__2::__value_type"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__value_type"*, %"struct.std::__2::__value_type"** %this.addr, align 4
  %__cc = getelementptr inbounds %"struct.std::__2::__value_type", %"struct.std::__2::__value_type"* %this1, i32 0, i32 0
  ret %"struct.std::__2::pair"* %__cc
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_node_base"** @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE12__find_equalIS7_EERPNS_16__tree_node_baseIPvEERPNS_15__tree_end_nodeISJ_EERKT_(%"class.std::__2::__tree"* %this, %"class.std::__2::__tree_end_node"** nonnull align 4 dereferenceable(4) %__parent, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %__v) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__tree_node_base"**, align 4
  %this.addr = alloca %"class.std::__2::__tree"*, align 4
  %__parent.addr = alloca %"class.std::__2::__tree_end_node"**, align 4
  %__v.addr = alloca %"class.std::__2::basic_string"*, align 4
  %__nd = alloca %"class.std::__2::__tree_node"*, align 4
  %__nd_ptr = alloca %"class.std::__2::__tree_node_base"**, align 4
  store %"class.std::__2::__tree"* %this, %"class.std::__2::__tree"** %this.addr, align 4
  store %"class.std::__2::__tree_end_node"** %__parent, %"class.std::__2::__tree_end_node"*** %__parent.addr, align 4
  store %"class.std::__2::basic_string"* %__v, %"class.std::__2::basic_string"** %__v.addr, align 4
  %this1 = load %"class.std::__2::__tree"*, %"class.std::__2::__tree"** %this.addr, align 4
  %call = call %"class.std::__2::__tree_node"* @_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE6__rootEv(%"class.std::__2::__tree"* %this1) #8
  store %"class.std::__2::__tree_node"* %call, %"class.std::__2::__tree_node"** %__nd, align 4
  %call2 = call %"class.std::__2::__tree_node_base"** @_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE10__root_ptrEv(%"class.std::__2::__tree"* %this1) #8
  store %"class.std::__2::__tree_node_base"** %call2, %"class.std::__2::__tree_node_base"*** %__nd_ptr, align 4
  %0 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %__nd, align 4
  %cmp = icmp ne %"class.std::__2::__tree_node"* %0, null
  br i1 %cmp, label %if.then, label %if.end28

if.then:                                          ; preds = %entry
  br label %while.body

while.body:                                       ; preds = %if.then, %if.end27
  %call3 = call nonnull align 1 dereferenceable(1) %"class.std::__2::__map_value_compare"* @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE10value_compEv(%"class.std::__2::__tree"* %this1) #8
  %1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__v.addr, align 4
  %2 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %__nd, align 4
  %__value_ = getelementptr inbounds %"class.std::__2::__tree_node", %"class.std::__2::__tree_node"* %2, i32 0, i32 1
  %call4 = call zeroext i1 @_ZNKSt3__219__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS6_S6_EENS_4lessIS6_EELb1EEclERKS6_RKS8_(%"class.std::__2::__map_value_compare"* %call3, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %1, %"struct.std::__2::__value_type"* nonnull align 4 dereferenceable(24) %__value_)
  br i1 %call4, label %if.then5, label %if.else12

if.then5:                                         ; preds = %while.body
  %3 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %__nd, align 4
  %4 = bitcast %"class.std::__2::__tree_node"* %3 to %"class.std::__2::__tree_end_node"*
  %__left_ = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %4, i32 0, i32 0
  %5 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__left_, align 4
  %cmp6 = icmp ne %"class.std::__2::__tree_node_base"* %5, null
  br i1 %cmp6, label %if.then7, label %if.else

if.then7:                                         ; preds = %if.then5
  %6 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %__nd, align 4
  %7 = bitcast %"class.std::__2::__tree_node"* %6 to %"class.std::__2::__tree_end_node"*
  %__left_8 = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %7, i32 0, i32 0
  %call9 = call %"class.std::__2::__tree_node_base"** @_ZNSt3__29addressofIPNS_16__tree_node_baseIPvEEEEPT_RS5_(%"class.std::__2::__tree_node_base"** nonnull align 4 dereferenceable(4) %__left_8) #8
  store %"class.std::__2::__tree_node_base"** %call9, %"class.std::__2::__tree_node_base"*** %__nd_ptr, align 4
  %8 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %__nd, align 4
  %9 = bitcast %"class.std::__2::__tree_node"* %8 to %"class.std::__2::__tree_end_node"*
  %__left_10 = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %9, i32 0, i32 0
  %10 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__left_10, align 4
  %11 = bitcast %"class.std::__2::__tree_node_base"* %10 to %"class.std::__2::__tree_node"*
  store %"class.std::__2::__tree_node"* %11, %"class.std::__2::__tree_node"** %__nd, align 4
  br label %if.end

if.else:                                          ; preds = %if.then5
  %12 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %__nd, align 4
  %13 = bitcast %"class.std::__2::__tree_node"* %12 to %"class.std::__2::__tree_end_node"*
  %14 = load %"class.std::__2::__tree_end_node"**, %"class.std::__2::__tree_end_node"*** %__parent.addr, align 4
  store %"class.std::__2::__tree_end_node"* %13, %"class.std::__2::__tree_end_node"** %14, align 4
  %15 = load %"class.std::__2::__tree_end_node"**, %"class.std::__2::__tree_end_node"*** %__parent.addr, align 4
  %16 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %15, align 4
  %__left_11 = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %16, i32 0, i32 0
  store %"class.std::__2::__tree_node_base"** %__left_11, %"class.std::__2::__tree_node_base"*** %retval, align 4
  br label %return

if.end:                                           ; preds = %if.then7
  br label %if.end27

if.else12:                                        ; preds = %while.body
  %call13 = call nonnull align 1 dereferenceable(1) %"class.std::__2::__map_value_compare"* @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE10value_compEv(%"class.std::__2::__tree"* %this1) #8
  %17 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %__nd, align 4
  %__value_14 = getelementptr inbounds %"class.std::__2::__tree_node", %"class.std::__2::__tree_node"* %17, i32 0, i32 1
  %18 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__v.addr, align 4
  %call15 = call zeroext i1 @_ZNKSt3__219__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS6_S6_EENS_4lessIS6_EELb1EEclERKS8_RKS6_(%"class.std::__2::__map_value_compare"* %call13, %"struct.std::__2::__value_type"* nonnull align 4 dereferenceable(24) %__value_14, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %18)
  br i1 %call15, label %if.then16, label %if.else25

if.then16:                                        ; preds = %if.else12
  %19 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %__nd, align 4
  %20 = bitcast %"class.std::__2::__tree_node"* %19 to %"class.std::__2::__tree_node_base"*
  %__right_ = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %20, i32 0, i32 1
  %21 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__right_, align 4
  %cmp17 = icmp ne %"class.std::__2::__tree_node_base"* %21, null
  br i1 %cmp17, label %if.then18, label %if.else22

if.then18:                                        ; preds = %if.then16
  %22 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %__nd, align 4
  %23 = bitcast %"class.std::__2::__tree_node"* %22 to %"class.std::__2::__tree_node_base"*
  %__right_19 = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %23, i32 0, i32 1
  %call20 = call %"class.std::__2::__tree_node_base"** @_ZNSt3__29addressofIPNS_16__tree_node_baseIPvEEEEPT_RS5_(%"class.std::__2::__tree_node_base"** nonnull align 4 dereferenceable(4) %__right_19) #8
  store %"class.std::__2::__tree_node_base"** %call20, %"class.std::__2::__tree_node_base"*** %__nd_ptr, align 4
  %24 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %__nd, align 4
  %25 = bitcast %"class.std::__2::__tree_node"* %24 to %"class.std::__2::__tree_node_base"*
  %__right_21 = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %25, i32 0, i32 1
  %26 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__right_21, align 4
  %27 = bitcast %"class.std::__2::__tree_node_base"* %26 to %"class.std::__2::__tree_node"*
  store %"class.std::__2::__tree_node"* %27, %"class.std::__2::__tree_node"** %__nd, align 4
  br label %if.end24

if.else22:                                        ; preds = %if.then16
  %28 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %__nd, align 4
  %29 = bitcast %"class.std::__2::__tree_node"* %28 to %"class.std::__2::__tree_end_node"*
  %30 = load %"class.std::__2::__tree_end_node"**, %"class.std::__2::__tree_end_node"*** %__parent.addr, align 4
  store %"class.std::__2::__tree_end_node"* %29, %"class.std::__2::__tree_end_node"** %30, align 4
  %31 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %__nd, align 4
  %32 = bitcast %"class.std::__2::__tree_node"* %31 to %"class.std::__2::__tree_node_base"*
  %__right_23 = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %32, i32 0, i32 1
  store %"class.std::__2::__tree_node_base"** %__right_23, %"class.std::__2::__tree_node_base"*** %retval, align 4
  br label %return

if.end24:                                         ; preds = %if.then18
  br label %if.end26

if.else25:                                        ; preds = %if.else12
  %33 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %__nd, align 4
  %34 = bitcast %"class.std::__2::__tree_node"* %33 to %"class.std::__2::__tree_end_node"*
  %35 = load %"class.std::__2::__tree_end_node"**, %"class.std::__2::__tree_end_node"*** %__parent.addr, align 4
  store %"class.std::__2::__tree_end_node"* %34, %"class.std::__2::__tree_end_node"** %35, align 4
  %36 = load %"class.std::__2::__tree_node_base"**, %"class.std::__2::__tree_node_base"*** %__nd_ptr, align 4
  store %"class.std::__2::__tree_node_base"** %36, %"class.std::__2::__tree_node_base"*** %retval, align 4
  br label %return

if.end26:                                         ; preds = %if.end24
  br label %if.end27

if.end27:                                         ; preds = %if.end26, %if.end
  br label %while.body

if.end28:                                         ; preds = %entry
  %call29 = call %"class.std::__2::__tree_end_node"* @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE10__end_nodeEv(%"class.std::__2::__tree"* %this1) #8
  %37 = load %"class.std::__2::__tree_end_node"**, %"class.std::__2::__tree_end_node"*** %__parent.addr, align 4
  store %"class.std::__2::__tree_end_node"* %call29, %"class.std::__2::__tree_end_node"** %37, align 4
  %38 = load %"class.std::__2::__tree_end_node"**, %"class.std::__2::__tree_end_node"*** %__parent.addr, align 4
  %39 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %38, align 4
  %__left_30 = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %39, i32 0, i32 0
  store %"class.std::__2::__tree_node_base"** %__left_30, %"class.std::__2::__tree_node_base"*** %retval, align 4
  br label %return

return:                                           ; preds = %if.end28, %if.else25, %if.else22, %if.else
  %40 = load %"class.std::__2::__tree_node_base"**, %"class.std::__2::__tree_node_base"*** %retval, align 4
  ret %"class.std::__2::__tree_node_base"** %40
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE16__construct_nodeIJRKNS_21piecewise_construct_tENS_5tupleIJRKS7_EEENSJ_IJEEEEEENS_10unique_ptrINS_11__tree_nodeIS8_PvEENS_22__tree_node_destructorINS5_ISR_EEEEEEDpOT_(%"class.std::__2::unique_ptr"* noalias sret align 4 %agg.result, %"class.std::__2::__tree"* %this, %"struct.std::__2::piecewise_construct_t"* nonnull align 1 dereferenceable(1) %__args, %"class.std::__2::tuple"* nonnull align 4 dereferenceable(4) %__args1, %"class.std::__2::tuple.10"* nonnull align 1 dereferenceable(1) %__args3) #0 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %"class.std::__2::__tree"*, align 4
  %__args.addr = alloca %"struct.std::__2::piecewise_construct_t"*, align 4
  %__args.addr2 = alloca %"class.std::__2::tuple"*, align 4
  %__args.addr4 = alloca %"class.std::__2::tuple.10"*, align 4
  %__na = alloca %"class.std::__2::allocator"*, align 4
  %nrvo = alloca i1, align 1
  %ref.tmp = alloca %"class.std::__2::__tree_node_destructor", align 4
  %0 = bitcast %"class.std::__2::unique_ptr"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %"class.std::__2::__tree"* %this, %"class.std::__2::__tree"** %this.addr, align 4
  store %"struct.std::__2::piecewise_construct_t"* %__args, %"struct.std::__2::piecewise_construct_t"** %__args.addr, align 4
  store %"class.std::__2::tuple"* %__args1, %"class.std::__2::tuple"** %__args.addr2, align 4
  store %"class.std::__2::tuple.10"* %__args3, %"class.std::__2::tuple.10"** %__args.addr4, align 4
  %this5 = load %"class.std::__2::__tree"*, %"class.std::__2::__tree"** %this.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE12__node_allocEv(%"class.std::__2::__tree"* %this5) #8
  store %"class.std::__2::allocator"* %call, %"class.std::__2::allocator"** %__na, align 4
  store i1 false, i1* %nrvo, align 1
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__na, align 4
  %call6 = call %"class.std::__2::__tree_node"* @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEEE8allocateERSC_m(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %1, i32 1)
  %2 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__na, align 4
  %call7 = call %"class.std::__2::__tree_node_destructor"* @_ZNSt3__222__tree_node_destructorINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEEEC2ERSC_b(%"class.std::__2::__tree_node_destructor"* %ref.tmp, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %2, i1 zeroext false) #8
  %call8 = call %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEEC2ILb1EvEEPSB_NS_16__dependent_typeINS_27__unique_ptr_deleter_sfinaeISE_EEXT_EE20__good_rval_ref_typeE(%"class.std::__2::unique_ptr"* %agg.result, %"class.std::__2::__tree_node"* %call6, %"class.std::__2::__tree_node_destructor"* nonnull align 4 dereferenceable(5) %ref.tmp) #8
  %3 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__na, align 4
  %call9 = call %"class.std::__2::__tree_node"* @_ZNKSt3__210unique_ptrINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEEptEv(%"class.std::__2::unique_ptr"* %agg.result) #8
  %__value_ = getelementptr inbounds %"class.std::__2::__tree_node", %"class.std::__2::__tree_node"* %call9, i32 0, i32 1
  %call10 = call %"struct.std::__2::pair"* @_ZNSt3__222__tree_key_value_typesINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEE9__get_ptrERS8_(%"struct.std::__2::__value_type"* nonnull align 4 dereferenceable(24) %__value_)
  %4 = load %"struct.std::__2::piecewise_construct_t"*, %"struct.std::__2::piecewise_construct_t"** %__args.addr, align 4
  %call11 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::piecewise_construct_t"* @_ZNSt3__27forwardIRKNS_21piecewise_construct_tEEEOT_RNS_16remove_referenceIS4_E4typeE(%"struct.std::__2::piecewise_construct_t"* nonnull align 1 dereferenceable(1) %4) #8
  %5 = load %"class.std::__2::tuple"*, %"class.std::__2::tuple"** %__args.addr2, align 4
  %call12 = call nonnull align 4 dereferenceable(4) %"class.std::__2::tuple"* @_ZNSt3__27forwardINS_5tupleIJRKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEEEEEOT_RNS_16remove_referenceISB_E4typeE(%"class.std::__2::tuple"* nonnull align 4 dereferenceable(4) %5) #8
  %6 = load %"class.std::__2::tuple.10"*, %"class.std::__2::tuple.10"** %__args.addr4, align 4
  %call13 = call nonnull align 1 dereferenceable(1) %"class.std::__2::tuple.10"* @_ZNSt3__27forwardINS_5tupleIJEEEEEOT_RNS_16remove_referenceIS3_E4typeE(%"class.std::__2::tuple.10"* nonnull align 1 dereferenceable(1) %6) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEEE9constructINS_4pairIKS8_S8_EEJRKNS_21piecewise_construct_tENS_5tupleIJRSG_EEENSL_IJEEEEEEvRSC_PT_DpOT0_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %3, %"struct.std::__2::pair"* %call10, %"struct.std::__2::piecewise_construct_t"* nonnull align 1 dereferenceable(1) %call11, %"class.std::__2::tuple"* nonnull align 4 dereferenceable(4) %call12, %"class.std::__2::tuple.10"* nonnull align 1 dereferenceable(1) %call13)
  %call14 = call nonnull align 4 dereferenceable(5) %"class.std::__2::__tree_node_destructor"* @_ZNSt3__210unique_ptrINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEE11get_deleterEv(%"class.std::__2::unique_ptr"* %agg.result) #8
  %__value_constructed = getelementptr inbounds %"class.std::__2::__tree_node_destructor", %"class.std::__2::__tree_node_destructor"* %call14, i32 0, i32 1
  store i8 1, i8* %__value_constructed, align 4
  store i1 true, i1* %nrvo, align 1
  %nrvo.val = load i1, i1* %nrvo, align 1
  br i1 %nrvo.val, label %nrvo.skipdtor, label %nrvo.unused

nrvo.unused:                                      ; preds = %entry
  %call15 = call %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEED2Ev(%"class.std::__2::unique_ptr"* %agg.result) #8
  br label %nrvo.skipdtor

nrvo.skipdtor:                                    ; preds = %nrvo.unused, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::piecewise_construct_t"* @_ZNSt3__27forwardIRKNS_21piecewise_construct_tEEEOT_RNS_16remove_referenceIS4_E4typeE(%"struct.std::__2::piecewise_construct_t"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"struct.std::__2::piecewise_construct_t"*, align 4
  store %"struct.std::__2::piecewise_construct_t"* %__t, %"struct.std::__2::piecewise_construct_t"** %__t.addr, align 4
  %0 = load %"struct.std::__2::piecewise_construct_t"*, %"struct.std::__2::piecewise_construct_t"** %__t.addr, align 4
  ret %"struct.std::__2::piecewise_construct_t"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::tuple"* @_ZNSt3__27forwardINS_5tupleIJRKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEEEEEOT_RNS_16remove_referenceISB_E4typeE(%"class.std::__2::tuple"* nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::tuple"*, align 4
  store %"class.std::__2::tuple"* %__t, %"class.std::__2::tuple"** %__t.addr, align 4
  %0 = load %"class.std::__2::tuple"*, %"class.std::__2::tuple"** %__t.addr, align 4
  ret %"class.std::__2::tuple"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::tuple.10"* @_ZNSt3__27forwardINS_5tupleIJEEEEEOT_RNS_16remove_referenceIS3_E4typeE(%"class.std::__2::tuple.10"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::tuple.10"*, align 4
  store %"class.std::__2::tuple.10"* %__t, %"class.std::__2::tuple.10"** %__t.addr, align 4
  %0 = load %"class.std::__2::tuple.10"*, %"class.std::__2::tuple.10"** %__t.addr, align 4
  ret %"class.std::__2::tuple.10"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE16__insert_node_atEPNS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEERSJ_SJ_(%"class.std::__2::__tree"* %this, %"class.std::__2::__tree_end_node"* %__parent, %"class.std::__2::__tree_node_base"** nonnull align 4 dereferenceable(4) %__child, %"class.std::__2::__tree_node_base"* %__new_node) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree"*, align 4
  %__parent.addr = alloca %"class.std::__2::__tree_end_node"*, align 4
  %__child.addr = alloca %"class.std::__2::__tree_node_base"**, align 4
  %__new_node.addr = alloca %"class.std::__2::__tree_node_base"*, align 4
  store %"class.std::__2::__tree"* %this, %"class.std::__2::__tree"** %this.addr, align 4
  store %"class.std::__2::__tree_end_node"* %__parent, %"class.std::__2::__tree_end_node"** %__parent.addr, align 4
  store %"class.std::__2::__tree_node_base"** %__child, %"class.std::__2::__tree_node_base"*** %__child.addr, align 4
  store %"class.std::__2::__tree_node_base"* %__new_node, %"class.std::__2::__tree_node_base"** %__new_node.addr, align 4
  %this1 = load %"class.std::__2::__tree"*, %"class.std::__2::__tree"** %this.addr, align 4
  %0 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__new_node.addr, align 4
  %1 = bitcast %"class.std::__2::__tree_node_base"* %0 to %"class.std::__2::__tree_end_node"*
  %__left_ = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %1, i32 0, i32 0
  store %"class.std::__2::__tree_node_base"* null, %"class.std::__2::__tree_node_base"** %__left_, align 4
  %2 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__new_node.addr, align 4
  %__right_ = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %2, i32 0, i32 1
  store %"class.std::__2::__tree_node_base"* null, %"class.std::__2::__tree_node_base"** %__right_, align 4
  %3 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %__parent.addr, align 4
  %4 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__new_node.addr, align 4
  %__parent_ = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %4, i32 0, i32 2
  store %"class.std::__2::__tree_end_node"* %3, %"class.std::__2::__tree_end_node"** %__parent_, align 4
  %5 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__new_node.addr, align 4
  %6 = load %"class.std::__2::__tree_node_base"**, %"class.std::__2::__tree_node_base"*** %__child.addr, align 4
  store %"class.std::__2::__tree_node_base"* %5, %"class.std::__2::__tree_node_base"** %6, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_end_node"** @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE12__begin_nodeEv(%"class.std::__2::__tree"* %this1) #8
  %7 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %call, align 4
  %__left_2 = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %7, i32 0, i32 0
  %8 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__left_2, align 4
  %cmp = icmp ne %"class.std::__2::__tree_node_base"* %8, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_end_node"** @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE12__begin_nodeEv(%"class.std::__2::__tree"* %this1) #8
  %9 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %call3, align 4
  %__left_4 = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %9, i32 0, i32 0
  %10 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__left_4, align 4
  %11 = bitcast %"class.std::__2::__tree_node_base"* %10 to %"class.std::__2::__tree_end_node"*
  %call5 = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_end_node"** @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE12__begin_nodeEv(%"class.std::__2::__tree"* %this1) #8
  store %"class.std::__2::__tree_end_node"* %11, %"class.std::__2::__tree_end_node"** %call5, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %call6 = call %"class.std::__2::__tree_end_node"* @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE10__end_nodeEv(%"class.std::__2::__tree"* %this1) #8
  %__left_7 = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %call6, i32 0, i32 0
  %12 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__left_7, align 4
  %13 = load %"class.std::__2::__tree_node_base"**, %"class.std::__2::__tree_node_base"*** %__child.addr, align 4
  %14 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %13, align 4
  call void @_ZNSt3__227__tree_balance_after_insertIPNS_16__tree_node_baseIPvEEEEvT_S5_(%"class.std::__2::__tree_node_base"* %12, %"class.std::__2::__tree_node_base"* %14) #8
  %call8 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE4sizeEv(%"class.std::__2::__tree"* %this1) #8
  %15 = load i32, i32* %call8, align 4
  %inc = add i32 %15, 1
  store i32 %inc, i32* %call8, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_node"* @_ZNKSt3__210unique_ptrINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEE3getEv(%"class.std::__2::unique_ptr"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_node"** @_ZNKSt3__217__compressed_pairIPNS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEE5firstEv(%"class.std::__2::__compressed_pair.11"* %__ptr_) #8
  %0 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %call, align 4
  ret %"class.std::__2::__tree_node"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_node"* @_ZNSt3__210unique_ptrINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEE7releaseEv(%"class.std::__2::unique_ptr"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  %__t = alloca %"class.std::__2::__tree_node"*, align 4
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_node"** @_ZNSt3__217__compressed_pairIPNS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEE5firstEv(%"class.std::__2::__compressed_pair.11"* %__ptr_) #8
  %0 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %call, align 4
  store %"class.std::__2::__tree_node"* %0, %"class.std::__2::__tree_node"** %__t, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_node"** @_ZNSt3__217__compressed_pairIPNS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEE5firstEv(%"class.std::__2::__compressed_pair.11"* %__ptr_2) #8
  store %"class.std::__2::__tree_node"* null, %"class.std::__2::__tree_node"** %call3, align 4
  %1 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %__t, align 4
  ret %"class.std::__2::__tree_node"* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEED2Ev(%"class.std::__2::unique_ptr"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  call void @_ZNSt3__210unique_ptrINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEE5resetEPSB_(%"class.std::__2::unique_ptr"* %this1, %"class.std::__2::__tree_node"* null) #8
  ret %"class.std::__2::unique_ptr"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_iterator"* @_ZNSt3__215__tree_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElEC2ESC_(%"class.std::__2::__tree_iterator"* returned %this, %"class.std::__2::__tree_node"* %__p) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree_iterator"*, align 4
  %__p.addr = alloca %"class.std::__2::__tree_node"*, align 4
  store %"class.std::__2::__tree_iterator"* %this, %"class.std::__2::__tree_iterator"** %this.addr, align 4
  store %"class.std::__2::__tree_node"* %__p, %"class.std::__2::__tree_node"** %__p.addr, align 4
  %this1 = load %"class.std::__2::__tree_iterator"*, %"class.std::__2::__tree_iterator"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::__tree_iterator", %"class.std::__2::__tree_iterator"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %__p.addr, align 4
  %1 = bitcast %"class.std::__2::__tree_node"* %0 to %"class.std::__2::__tree_end_node"*
  store %"class.std::__2::__tree_end_node"* %1, %"class.std::__2::__tree_end_node"** %__ptr_, align 4
  ret %"class.std::__2::__tree_iterator"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::pair.9"* @_ZNSt3__24pairINS_15__tree_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPNS_11__tree_nodeIS9_PvEElEEbEC2ISE_RbLb0EEEOT_OT0_(%"struct.std::__2::pair.9"* returned %this, %"class.std::__2::__tree_iterator"* nonnull align 4 dereferenceable(4) %__u1, i8* nonnull align 1 dereferenceable(1) %__u2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::pair.9"*, align 4
  %__u1.addr = alloca %"class.std::__2::__tree_iterator"*, align 4
  %__u2.addr = alloca i8*, align 4
  store %"struct.std::__2::pair.9"* %this, %"struct.std::__2::pair.9"** %this.addr, align 4
  store %"class.std::__2::__tree_iterator"* %__u1, %"class.std::__2::__tree_iterator"** %__u1.addr, align 4
  store i8* %__u2, i8** %__u2.addr, align 4
  %this1 = load %"struct.std::__2::pair.9"*, %"struct.std::__2::pair.9"** %this.addr, align 4
  %first = getelementptr inbounds %"struct.std::__2::pair.9", %"struct.std::__2::pair.9"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::__tree_iterator"*, %"class.std::__2::__tree_iterator"** %__u1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_iterator"* @_ZNSt3__27forwardINS_15__tree_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPNS_11__tree_nodeIS9_PvEElEEEEOT_RNS_16remove_referenceISF_E4typeE(%"class.std::__2::__tree_iterator"* nonnull align 4 dereferenceable(4) %0) #8
  %1 = bitcast %"class.std::__2::__tree_iterator"* %first to i8*
  %2 = bitcast %"class.std::__2::__tree_iterator"* %call to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 4, i1 false)
  %second = getelementptr inbounds %"struct.std::__2::pair.9", %"struct.std::__2::pair.9"* %this1, i32 0, i32 1
  %3 = load i8*, i8** %__u2.addr, align 4
  %call2 = call nonnull align 1 dereferenceable(1) i8* @_ZNSt3__27forwardIRbEEOT_RNS_16remove_referenceIS2_E4typeE(i8* nonnull align 1 dereferenceable(1) %3) #8
  %4 = load i8, i8* %call2, align 1
  %tobool = trunc i8 %4 to i1
  %frombool = zext i1 %tobool to i8
  store i8 %frombool, i8* %second, align 4
  ret %"struct.std::__2::pair.9"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_node"* @_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE6__rootEv(%"class.std::__2::__tree"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree"*, align 4
  store %"class.std::__2::__tree"* %this, %"class.std::__2::__tree"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree"*, %"class.std::__2::__tree"** %this.addr, align 4
  %call = call %"class.std::__2::__tree_end_node"* @_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE10__end_nodeEv(%"class.std::__2::__tree"* %this1) #8
  %__left_ = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %call, i32 0, i32 0
  %0 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__left_, align 4
  %1 = bitcast %"class.std::__2::__tree_node_base"* %0 to %"class.std::__2::__tree_node"*
  ret %"class.std::__2::__tree_node"* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_node_base"** @_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE10__root_ptrEv(%"class.std::__2::__tree"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree"*, align 4
  store %"class.std::__2::__tree"* %this, %"class.std::__2::__tree"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree"*, %"class.std::__2::__tree"** %this.addr, align 4
  %call = call %"class.std::__2::__tree_end_node"* @_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE10__end_nodeEv(%"class.std::__2::__tree"* %this1) #8
  %__left_ = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %call, i32 0, i32 0
  %call2 = call %"class.std::__2::__tree_node_base"** @_ZNSt3__29addressofIPNS_16__tree_node_baseIPvEEEEPT_RS5_(%"class.std::__2::__tree_node_base"** nonnull align 4 dereferenceable(4) %__left_) #8
  ret %"class.std::__2::__tree_node_base"** %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::__map_value_compare"* @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE10value_compEv(%"class.std::__2::__tree"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree"*, align 4
  store %"class.std::__2::__tree"* %this, %"class.std::__2::__tree"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree"*, %"class.std::__2::__tree"** %this.addr, align 4
  %__pair3_ = getelementptr inbounds %"class.std::__2::__tree", %"class.std::__2::__tree"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::__map_value_compare"* @_ZNSt3__217__compressed_pairImNS_19__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS7_S7_EENS_4lessIS7_EELb1EEEE6secondEv(%"class.std::__2::__compressed_pair.1"* %__pair3_) #8
  ret %"class.std::__2::__map_value_compare"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__219__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS6_S6_EENS_4lessIS6_EELb1EEclERKS6_RKS8_(%"class.std::__2::__map_value_compare"* %this, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %__x, %"struct.std::__2::__value_type"* nonnull align 4 dereferenceable(24) %__y) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__map_value_compare"*, align 4
  %__x.addr = alloca %"class.std::__2::basic_string"*, align 4
  %__y.addr = alloca %"struct.std::__2::__value_type"*, align 4
  store %"class.std::__2::__map_value_compare"* %this, %"class.std::__2::__map_value_compare"** %this.addr, align 4
  store %"class.std::__2::basic_string"* %__x, %"class.std::__2::basic_string"** %__x.addr, align 4
  store %"struct.std::__2::__value_type"* %__y, %"struct.std::__2::__value_type"** %__y.addr, align 4
  %this1 = load %"class.std::__2::__map_value_compare"*, %"class.std::__2::__map_value_compare"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__map_value_compare"* %this1 to %"struct.std::__2::less"*
  %1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__x.addr, align 4
  %2 = load %"struct.std::__2::__value_type"*, %"struct.std::__2::__value_type"** %__y.addr, align 4
  %call = call nonnull align 4 dereferenceable(24) %"struct.std::__2::pair"* @_ZNKSt3__212__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_E11__get_valueEv(%"struct.std::__2::__value_type"* %2)
  %first = getelementptr inbounds %"struct.std::__2::pair", %"struct.std::__2::pair"* %call, i32 0, i32 0
  %call2 = call zeroext i1 @_ZNKSt3__24lessINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEclERKS6_S9_(%"struct.std::__2::less"* %0, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %1, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %first)
  ret i1 %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_node_base"** @_ZNSt3__29addressofIPNS_16__tree_node_baseIPvEEEEPT_RS5_(%"class.std::__2::__tree_node_base"** nonnull align 4 dereferenceable(4) %__x) #0 comdat {
entry:
  %__x.addr = alloca %"class.std::__2::__tree_node_base"**, align 4
  store %"class.std::__2::__tree_node_base"** %__x, %"class.std::__2::__tree_node_base"*** %__x.addr, align 4
  %0 = load %"class.std::__2::__tree_node_base"**, %"class.std::__2::__tree_node_base"*** %__x.addr, align 4
  ret %"class.std::__2::__tree_node_base"** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__219__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS6_S6_EENS_4lessIS6_EELb1EEclERKS8_RKS6_(%"class.std::__2::__map_value_compare"* %this, %"struct.std::__2::__value_type"* nonnull align 4 dereferenceable(24) %__x, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %__y) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__map_value_compare"*, align 4
  %__x.addr = alloca %"struct.std::__2::__value_type"*, align 4
  %__y.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::__map_value_compare"* %this, %"class.std::__2::__map_value_compare"** %this.addr, align 4
  store %"struct.std::__2::__value_type"* %__x, %"struct.std::__2::__value_type"** %__x.addr, align 4
  store %"class.std::__2::basic_string"* %__y, %"class.std::__2::basic_string"** %__y.addr, align 4
  %this1 = load %"class.std::__2::__map_value_compare"*, %"class.std::__2::__map_value_compare"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__map_value_compare"* %this1 to %"struct.std::__2::less"*
  %1 = load %"struct.std::__2::__value_type"*, %"struct.std::__2::__value_type"** %__x.addr, align 4
  %call = call nonnull align 4 dereferenceable(24) %"struct.std::__2::pair"* @_ZNKSt3__212__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_E11__get_valueEv(%"struct.std::__2::__value_type"* %1)
  %first = getelementptr inbounds %"struct.std::__2::pair", %"struct.std::__2::pair"* %call, i32 0, i32 0
  %2 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__y.addr, align 4
  %call2 = call zeroext i1 @_ZNKSt3__24lessINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEclERKS6_S9_(%"struct.std::__2::less"* %0, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %first, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %2)
  ret i1 %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::__map_value_compare"* @_ZNSt3__217__compressed_pairImNS_19__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS7_S7_EENS_4lessIS7_EELb1EEEE6secondEv(%"class.std::__2::__compressed_pair.1"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.1"*, align 4
  store %"class.std::__2::__compressed_pair.1"* %this, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.1"*, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.1"* %this1 to %"struct.std::__2::__compressed_pair_elem.3"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::__map_value_compare"* @_ZNSt3__222__compressed_pair_elemINS_19__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS7_S7_EENS_4lessIS7_EELb1EEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.3"* %0) #8
  ret %"class.std::__2::__map_value_compare"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::__map_value_compare"* @_ZNSt3__222__compressed_pair_elemINS_19__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS7_S7_EENS_4lessIS7_EELb1EEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.3"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.3"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.3"* %this, %"struct.std::__2::__compressed_pair_elem.3"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.3"*, %"struct.std::__2::__compressed_pair_elem.3"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.3"* %this1 to %"class.std::__2::__map_value_compare"*
  ret %"class.std::__2::__map_value_compare"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__24lessINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEclERKS6_S9_(%"struct.std::__2::less"* %this, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %__x, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %__y) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::less"*, align 4
  %__x.addr = alloca %"class.std::__2::basic_string"*, align 4
  %__y.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"struct.std::__2::less"* %this, %"struct.std::__2::less"** %this.addr, align 4
  store %"class.std::__2::basic_string"* %__x, %"class.std::__2::basic_string"** %__x.addr, align 4
  store %"class.std::__2::basic_string"* %__y, %"class.std::__2::basic_string"** %__y.addr, align 4
  %this1 = load %"struct.std::__2::less"*, %"struct.std::__2::less"** %this.addr, align 4
  %0 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__x.addr, align 4
  %1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__y.addr, align 4
  %call = call zeroext i1 @_ZNSt3__2ltIcNS_11char_traitsIcEENS_9allocatorIcEEEEbRKNS_12basic_stringIT_T0_T1_EESB_(%"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %0, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %1) #8
  ret i1 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNSt3__2ltIcNS_11char_traitsIcEENS_9allocatorIcEEEEbRKNS_12basic_stringIT_T0_T1_EESB_(%"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %__lhs, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %__rhs) #0 comdat {
entry:
  %__lhs.addr = alloca %"class.std::__2::basic_string"*, align 4
  %__rhs.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %__lhs, %"class.std::__2::basic_string"** %__lhs.addr, align 4
  store %"class.std::__2::basic_string"* %__rhs, %"class.std::__2::basic_string"** %__rhs.addr, align 4
  %0 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__lhs.addr, align 4
  %1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__rhs.addr, align 4
  %call = call i32 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE7compareERKS5_(%"class.std::__2::basic_string"* %0, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %1) #8
  %cmp = icmp slt i32 %call, 0
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE7compareERKS5_(%"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %__str) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  %__str.addr = alloca %"class.std::__2::basic_string"*, align 4
  %ref.tmp = alloca %"class.std::__2::basic_string_view", align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  store %"class.std::__2::basic_string"* %__str, %"class.std::__2::basic_string"** %__str.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %0 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__str.addr, align 4
  call void @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEcvNS_17basic_string_viewIcS2_EEEv(%"class.std::__2::basic_string_view"* sret align 4 %ref.tmp, %"class.std::__2::basic_string"* %0) #8
  %call = call i32 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE7compareINS_17basic_string_viewIcS2_EEEENS_9enable_ifIXsr33__can_be_converted_to_string_viewIcS2_T_EE5valueEiE4typeERKSA_(%"class.std::__2::basic_string"* %this1, %"class.std::__2::basic_string_view"* nonnull align 4 dereferenceable(8) %ref.tmp)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr i32 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE7compareINS_17basic_string_viewIcS2_EEEENS_9enable_ifIXsr33__can_be_converted_to_string_viewIcS2_T_EE5valueEiE4typeERKSA_(%"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string_view"* nonnull align 4 dereferenceable(8) %__t) #0 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  %__t.addr = alloca %"class.std::__2::basic_string_view"*, align 4
  %__sv = alloca %"class.std::__2::basic_string_view", align 4
  %__lhs_sz = alloca i32, align 4
  %__rhs_sz = alloca i32, align 4
  %__result = alloca i32, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  store %"class.std::__2::basic_string_view"* %__t, %"class.std::__2::basic_string_view"** %__t.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %0 = load %"class.std::__2::basic_string_view"*, %"class.std::__2::basic_string_view"** %__t.addr, align 4
  %1 = bitcast %"class.std::__2::basic_string_view"* %__sv to i8*
  %2 = bitcast %"class.std::__2::basic_string_view"* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 8, i1 false)
  %call = call i32 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4sizeEv(%"class.std::__2::basic_string"* %this1) #8
  store i32 %call, i32* %__lhs_sz, align 4
  %call2 = call i32 @_ZNKSt3__217basic_string_viewIcNS_11char_traitsIcEEE4sizeEv(%"class.std::__2::basic_string_view"* %__sv) #8
  store i32 %call2, i32* %__rhs_sz, align 4
  %call3 = call i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv(%"class.std::__2::basic_string"* %this1) #8
  %call4 = call i8* @_ZNKSt3__217basic_string_viewIcNS_11char_traitsIcEEE4dataEv(%"class.std::__2::basic_string_view"* %__sv) #8
  %call5 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %__lhs_sz, i32* nonnull align 4 dereferenceable(4) %__rhs_sz)
  %3 = load i32, i32* %call5, align 4
  %call6 = call i32 @_ZNSt3__211char_traitsIcE7compareEPKcS3_m(i8* %call3, i8* %call4, i32 %3) #8
  store i32 %call6, i32* %__result, align 4
  %4 = load i32, i32* %__result, align 4
  %cmp = icmp ne i32 %4, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %5 = load i32, i32* %__result, align 4
  store i32 %5, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %6 = load i32, i32* %__lhs_sz, align 4
  %7 = load i32, i32* %__rhs_sz, align 4
  %cmp7 = icmp ult i32 %6, %7
  br i1 %cmp7, label %if.then8, label %if.end9

if.then8:                                         ; preds = %if.end
  store i32 -1, i32* %retval, align 4
  br label %return

if.end9:                                          ; preds = %if.end
  %8 = load i32, i32* %__lhs_sz, align 4
  %9 = load i32, i32* %__rhs_sz, align 4
  %cmp10 = icmp ugt i32 %8, %9
  br i1 %cmp10, label %if.then11, label %if.end12

if.then11:                                        ; preds = %if.end9
  store i32 1, i32* %retval, align 4
  br label %return

if.end12:                                         ; preds = %if.end9
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end12, %if.then11, %if.then8, %if.then
  %10 = load i32, i32* %retval, align 4
  ret i32 %10
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEcvNS_17basic_string_viewIcS2_EEEv(%"class.std::__2::basic_string_view"* noalias sret align 4 %agg.result, %"class.std::__2::basic_string"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %call = call i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv(%"class.std::__2::basic_string"* %this1) #8
  %call2 = call i32 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4sizeEv(%"class.std::__2::basic_string"* %this1) #8
  %call3 = call %"class.std::__2::basic_string_view"* @_ZNSt3__217basic_string_viewIcNS_11char_traitsIcEEEC2EPKcm(%"class.std::__2::basic_string_view"* %agg.result, i8* %call, i32 %call2) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4sizeEv(%"class.std::__2::basic_string"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %call = call zeroext i1 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE9__is_longEv(%"class.std::__2::basic_string"* %this1) #8
  br i1 %call, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %call2 = call i32 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE15__get_long_sizeEv(%"class.std::__2::basic_string"* %this1) #8
  br label %cond.end

cond.false:                                       ; preds = %entry
  %call3 = call i32 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE16__get_short_sizeEv(%"class.std::__2::basic_string"* %this1) #8
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %call2, %cond.true ], [ %call3, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__217basic_string_viewIcNS_11char_traitsIcEEE4sizeEv(%"class.std::__2::basic_string_view"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string_view"*, align 4
  store %"class.std::__2::basic_string_view"* %this, %"class.std::__2::basic_string_view"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string_view"*, %"class.std::__2::basic_string_view"** %this.addr, align 4
  %__size = getelementptr inbounds %"class.std::__2::basic_string_view", %"class.std::__2::basic_string_view"* %this1, i32 0, i32 1
  %0 = load i32, i32* %__size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__211char_traitsIcE7compareEPKcS3_m(i8* %__s1, i8* %__s2, i32 %__n) #0 comdat {
entry:
  %retval = alloca i32, align 4
  %__s1.addr = alloca i8*, align 4
  %__s2.addr = alloca i8*, align 4
  %__n.addr = alloca i32, align 4
  store i8* %__s1, i8** %__s1.addr, align 4
  store i8* %__s2, i8** %__s2.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %cmp = icmp eq i32 %0, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load i8*, i8** %__s1.addr, align 4
  %2 = load i8*, i8** %__s2.addr, align 4
  %3 = load i32, i32* %__n.addr, align 4
  %call = call i32 @memcmp(i8* %1, i8* %2, i32 %3) #8
  store i32 %call, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %4 = load i32, i32* %retval, align 4
  ret i32 %4
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv(%"class.std::__2::basic_string"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %call = call i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE13__get_pointerEv(%"class.std::__2::basic_string"* %this1) #8
  %call2 = call i8* @_ZNSt3__212__to_addressIKcEEPT_S3_(i8* %call) #8
  ret i8* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__217basic_string_viewIcNS_11char_traitsIcEEE4dataEv(%"class.std::__2::basic_string_view"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string_view"*, align 4
  store %"class.std::__2::basic_string_view"* %this, %"class.std::__2::basic_string_view"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string_view"*, %"class.std::__2::basic_string_view"** %this.addr, align 4
  %__data = getelementptr inbounds %"class.std::__2::basic_string_view", %"class.std::__2::basic_string_view"* %this1, i32 0, i32 0
  %0 = load i8*, i8** %__data, align 4
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::__less", align 1
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__a.addr, align 4
  %1 = load i32*, i32** %__b.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE9__is_longEv(%"class.std::__2::basic_string"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair.4"* %__r_) #8
  %0 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call, i32 0, i32 0
  %__s = bitcast %union.anon* %0 to %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short"*
  %1 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short"* %__s, i32 0, i32 1
  %__size_ = getelementptr inbounds %struct.anon, %struct.anon* %1, i32 0, i32 0
  %2 = load i8, i8* %__size_, align 1
  %conv = zext i8 %2 to i32
  %and = and i32 %conv, 128
  %tobool = icmp ne i32 %and, 0
  ret i1 %tobool
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE15__get_long_sizeEv(%"class.std::__2::basic_string"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair.4"* %__r_) #8
  %0 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call, i32 0, i32 0
  %__l = bitcast %union.anon* %0 to %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long"*
  %__size_ = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long"* %__l, i32 0, i32 1
  %1 = load i32, i32* %__size_, align 4
  ret i32 %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE16__get_short_sizeEv(%"class.std::__2::basic_string"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair.4"* %__r_) #8
  %0 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call, i32 0, i32 0
  %__s = bitcast %union.anon* %0 to %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short"*
  %1 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short"* %__s, i32 0, i32 1
  %__size_ = getelementptr inbounds %struct.anon, %struct.anon* %1, i32 0, i32 0
  %2 = load i8, i8* %__size_, align 1
  %conv = zext i8 %2 to i32
  ret i32 %conv
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair.4"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.4"*, align 4
  store %"class.std::__2::__compressed_pair.4"* %this, %"class.std::__2::__compressed_pair.4"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.4"*, %"class.std::__2::__compressed_pair.4"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.4"* %this1 to %"struct.std::__2::__compressed_pair_elem.5"*
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.5"* %0) #8
  ret %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.5"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.5"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.5"* %this, %"struct.std::__2::__compressed_pair_elem.5"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.5"*, %"struct.std::__2::__compressed_pair_elem.5"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.5", %"struct.std::__2::__compressed_pair_elem.5"* %this1, i32 0, i32 0
  ret %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %__value_
}

; Function Attrs: nounwind
declare i32 @memcmp(i8*, i8*, i32) #2

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__212__to_addressIKcEEPT_S3_(i8* %__p) #0 comdat {
entry:
  %__p.addr = alloca i8*, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %0 = load i8*, i8** %__p.addr, align 4
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE13__get_pointerEv(%"class.std::__2::basic_string"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %call = call zeroext i1 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE9__is_longEv(%"class.std::__2::basic_string"* %this1) #8
  br i1 %call, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %call2 = call i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE18__get_long_pointerEv(%"class.std::__2::basic_string"* %this1) #8
  br label %cond.end

cond.false:                                       ; preds = %entry
  %call3 = call i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE19__get_short_pointerEv(%"class.std::__2::basic_string"* %this1) #8
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i8* [ %call2, %cond.true ], [ %call3, %cond.false ]
  ret i8* %cond
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE18__get_long_pointerEv(%"class.std::__2::basic_string"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair.4"* %__r_) #8
  %0 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call, i32 0, i32 0
  %__l = bitcast %union.anon* %0 to %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long"*
  %__data_ = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long"* %__l, i32 0, i32 0
  %1 = load i8*, i8** %__data_, align 4
  ret i8* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE19__get_short_pointerEv(%"class.std::__2::basic_string"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair.4"* %__r_) #8
  %0 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call, i32 0, i32 0
  %__s = bitcast %union.anon* %0 to %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short"*
  %__data_ = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short"* %__s, i32 0, i32 0
  %arrayidx = getelementptr inbounds [11 x i8], [11 x i8]* %__data_, i32 0, i32 0
  %call2 = call i8* @_ZNSt3__214pointer_traitsIPKcE10pointer_toERS1_(i8* nonnull align 1 dereferenceable(1) %arrayidx) #8
  ret i8* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__214pointer_traitsIPKcE10pointer_toERS1_(i8* nonnull align 1 dereferenceable(1) %__r) #0 comdat {
entry:
  %__r.addr = alloca i8*, align 4
  store i8* %__r, i8** %__r.addr, align 4
  %0 = load i8*, i8** %__r.addr, align 4
  %call = call i8* @_ZNSt3__29addressofIKcEEPT_RS2_(i8* nonnull align 1 dereferenceable(1) %0) #8
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__29addressofIKcEEPT_RS2_(i8* nonnull align 1 dereferenceable(1) %__x) #0 comdat {
entry:
  %__x.addr = alloca i8*, align 4
  store i8* %__x, i8** %__x.addr, align 4
  %0 = load i8*, i8** %__x.addr, align 4
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__comp = alloca %"struct.std::__2::__less", align 1
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__b.addr, align 4
  %1 = load i32*, i32** %__a.addr, align 4
  %call = call zeroext i1 @_ZNKSt3__26__lessImmEclERKmS3_(%"struct.std::__2::__less"* %__comp, i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  br i1 %call, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32*, i32** %__b.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %3 = load i32*, i32** %__a.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi i32* [ %2, %cond.true ], [ %3, %cond.false ]
  ret i32* %cond-lvalue
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__26__lessImmEclERKmS3_(%"struct.std::__2::__less"* %this, i32* nonnull align 4 dereferenceable(4) %__x, i32* nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__less"*, align 4
  %__x.addr = alloca i32*, align 4
  %__y.addr = alloca i32*, align 4
  store %"struct.std::__2::__less"* %this, %"struct.std::__2::__less"** %this.addr, align 4
  store i32* %__x, i32** %__x.addr, align 4
  store i32* %__y, i32** %__y.addr, align 4
  %this1 = load %"struct.std::__2::__less"*, %"struct.std::__2::__less"** %this.addr, align 4
  %0 = load i32*, i32** %__x.addr, align 4
  %1 = load i32, i32* %0, align 4
  %2 = load i32*, i32** %__y.addr, align 4
  %3 = load i32, i32* %2, align 4
  %cmp = icmp ult i32 %1, %3
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::basic_string_view"* @_ZNSt3__217basic_string_viewIcNS_11char_traitsIcEEEC2EPKcm(%"class.std::__2::basic_string_view"* returned %this, i8* %__s, i32 %__len) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string_view"*, align 4
  %__s.addr = alloca i8*, align 4
  %__len.addr = alloca i32, align 4
  store %"class.std::__2::basic_string_view"* %this, %"class.std::__2::basic_string_view"** %this.addr, align 4
  store i8* %__s, i8** %__s.addr, align 4
  store i32 %__len, i32* %__len.addr, align 4
  %this1 = load %"class.std::__2::basic_string_view"*, %"class.std::__2::basic_string_view"** %this.addr, align 4
  %__data = getelementptr inbounds %"class.std::__2::basic_string_view", %"class.std::__2::basic_string_view"* %this1, i32 0, i32 0
  %0 = load i8*, i8** %__s.addr, align 4
  store i8* %0, i8** %__data, align 4
  %__size = getelementptr inbounds %"class.std::__2::basic_string_view", %"class.std::__2::basic_string_view"* %this1, i32 0, i32 1
  %1 = load i32, i32* %__len.addr, align 4
  store i32 %1, i32* %__size, align 4
  ret %"class.std::__2::basic_string_view"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE12__node_allocEv(%"class.std::__2::__tree"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree"*, align 4
  store %"class.std::__2::__tree"* %this, %"class.std::__2::__tree"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree"*, %"class.std::__2::__tree"** %this.addr, align 4
  %__pair1_ = getelementptr inbounds %"class.std::__2::__tree", %"class.std::__2::__tree"* %this1, i32 0, i32 1
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__217__compressed_pairINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEENS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS7_IcEEEESE_EES3_EEEEE6secondEv(%"class.std::__2::__compressed_pair"* %__pair1_) #8
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_node"* @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEEE8allocateERSC_m(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call %"class.std::__2::__tree_node"* @_ZNSt3__29allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEES7_EEPvEEE8allocateEmPKv(%"class.std::__2::allocator"* %0, i32 %1, i8* null)
  ret %"class.std::__2::__tree_node"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_node_destructor"* @_ZNSt3__222__tree_node_destructorINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEEEC2ERSC_b(%"class.std::__2::__tree_node_destructor"* returned %this, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__na, i1 zeroext %__val) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree_node_destructor"*, align 4
  %__na.addr = alloca %"class.std::__2::allocator"*, align 4
  %__val.addr = alloca i8, align 1
  store %"class.std::__2::__tree_node_destructor"* %this, %"class.std::__2::__tree_node_destructor"** %this.addr, align 4
  store %"class.std::__2::allocator"* %__na, %"class.std::__2::allocator"** %__na.addr, align 4
  %frombool = zext i1 %__val to i8
  store i8 %frombool, i8* %__val.addr, align 1
  %this1 = load %"class.std::__2::__tree_node_destructor"*, %"class.std::__2::__tree_node_destructor"** %this.addr, align 4
  %__na_ = getelementptr inbounds %"class.std::__2::__tree_node_destructor", %"class.std::__2::__tree_node_destructor"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__na.addr, align 4
  store %"class.std::__2::allocator"* %0, %"class.std::__2::allocator"** %__na_, align 4
  %__value_constructed = getelementptr inbounds %"class.std::__2::__tree_node_destructor", %"class.std::__2::__tree_node_destructor"* %this1, i32 0, i32 1
  %1 = load i8, i8* %__val.addr, align 1
  %tobool = trunc i8 %1 to i1
  %frombool2 = zext i1 %tobool to i8
  store i8 %frombool2, i8* %__value_constructed, align 4
  ret %"class.std::__2::__tree_node_destructor"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEEC2ILb1EvEEPSB_NS_16__dependent_typeINS_27__unique_ptr_deleter_sfinaeISE_EEXT_EE20__good_rval_ref_typeE(%"class.std::__2::unique_ptr"* returned %this, %"class.std::__2::__tree_node"* %__p, %"class.std::__2::__tree_node_destructor"* nonnull align 4 dereferenceable(5) %__d) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  %__p.addr = alloca %"class.std::__2::__tree_node"*, align 4
  %__d.addr = alloca %"class.std::__2::__tree_node_destructor"*, align 4
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  store %"class.std::__2::__tree_node"* %__p, %"class.std::__2::__tree_node"** %__p.addr, align 4
  store %"class.std::__2::__tree_node_destructor"* %__d, %"class.std::__2::__tree_node_destructor"** %__d.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::__tree_node_destructor"*, %"class.std::__2::__tree_node_destructor"** %__d.addr, align 4
  %call = call nonnull align 4 dereferenceable(5) %"class.std::__2::__tree_node_destructor"* @_ZNSt3__24moveIRNS_22__tree_node_destructorINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS2_IcEEEES9_EEPvEEEEEEEEONS_16remove_referenceIT_E4typeEOSH_(%"class.std::__2::__tree_node_destructor"* nonnull align 4 dereferenceable(5) %0) #8
  %call2 = call %"class.std::__2::__compressed_pair.11"* @_ZNSt3__217__compressed_pairIPNS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEEC2IRSC_SF_EEOT_OT0_(%"class.std::__2::__compressed_pair.11"* %__ptr_, %"class.std::__2::__tree_node"** nonnull align 4 dereferenceable(4) %__p.addr, %"class.std::__2::__tree_node_destructor"* nonnull align 4 dereferenceable(5) %call)
  ret %"class.std::__2::unique_ptr"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEEE9constructINS_4pairIKS8_S8_EEJRKNS_21piecewise_construct_tENS_5tupleIJRSG_EEENSL_IJEEEEEEvRSC_PT_DpOT0_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, %"struct.std::__2::pair"* %__p, %"struct.std::__2::piecewise_construct_t"* nonnull align 1 dereferenceable(1) %__args, %"class.std::__2::tuple"* nonnull align 4 dereferenceable(4) %__args1, %"class.std::__2::tuple.10"* nonnull align 1 dereferenceable(1) %__args3) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca %"struct.std::__2::pair"*, align 4
  %__args.addr = alloca %"struct.std::__2::piecewise_construct_t"*, align 4
  %__args.addr2 = alloca %"class.std::__2::tuple"*, align 4
  %__args.addr4 = alloca %"class.std::__2::tuple.10"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_construct", align 1
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store %"struct.std::__2::pair"* %__p, %"struct.std::__2::pair"** %__p.addr, align 4
  store %"struct.std::__2::piecewise_construct_t"* %__args, %"struct.std::__2::piecewise_construct_t"** %__args.addr, align 4
  store %"class.std::__2::tuple"* %__args1, %"class.std::__2::tuple"** %__args.addr2, align 4
  store %"class.std::__2::tuple.10"* %__args3, %"class.std::__2::tuple.10"** %__args.addr4, align 4
  %0 = bitcast %"struct.std::__2::__has_construct"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %2 = load %"struct.std::__2::pair"*, %"struct.std::__2::pair"** %__p.addr, align 4
  %3 = load %"struct.std::__2::piecewise_construct_t"*, %"struct.std::__2::piecewise_construct_t"** %__args.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::piecewise_construct_t"* @_ZNSt3__27forwardIRKNS_21piecewise_construct_tEEEOT_RNS_16remove_referenceIS4_E4typeE(%"struct.std::__2::piecewise_construct_t"* nonnull align 1 dereferenceable(1) %3) #8
  %4 = load %"class.std::__2::tuple"*, %"class.std::__2::tuple"** %__args.addr2, align 4
  %call5 = call nonnull align 4 dereferenceable(4) %"class.std::__2::tuple"* @_ZNSt3__27forwardINS_5tupleIJRKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEEEEEOT_RNS_16remove_referenceISB_E4typeE(%"class.std::__2::tuple"* nonnull align 4 dereferenceable(4) %4) #8
  %5 = load %"class.std::__2::tuple.10"*, %"class.std::__2::tuple.10"** %__args.addr4, align 4
  %call6 = call nonnull align 1 dereferenceable(1) %"class.std::__2::tuple.10"* @_ZNSt3__27forwardINS_5tupleIJEEEEEOT_RNS_16remove_referenceIS3_E4typeE(%"class.std::__2::tuple.10"* nonnull align 1 dereferenceable(1) %5) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEEE11__constructINS_4pairIKS8_S8_EEJRKNS_21piecewise_construct_tENS_5tupleIJRSG_EEENSL_IJEEEEEEvNS_17integral_constantIbLb1EEERSC_PT_DpOT0_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %1, %"struct.std::__2::pair"* %2, %"struct.std::__2::piecewise_construct_t"* nonnull align 1 dereferenceable(1) %call, %"class.std::__2::tuple"* nonnull align 4 dereferenceable(4) %call5, %"class.std::__2::tuple.10"* nonnull align 1 dereferenceable(1) %call6)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::pair"* @_ZNSt3__222__tree_key_value_typesINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEE9__get_ptrERS8_(%"struct.std::__2::__value_type"* nonnull align 4 dereferenceable(24) %__n) #0 comdat {
entry:
  %__n.addr = alloca %"struct.std::__2::__value_type"*, align 4
  store %"struct.std::__2::__value_type"* %__n, %"struct.std::__2::__value_type"** %__n.addr, align 4
  %0 = load %"struct.std::__2::__value_type"*, %"struct.std::__2::__value_type"** %__n.addr, align 4
  %call = call nonnull align 4 dereferenceable(24) %"struct.std::__2::pair"* @_ZNSt3__212__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_E11__get_valueEv(%"struct.std::__2::__value_type"* %0)
  %call1 = call %"struct.std::__2::pair"* @_ZNSt3__29addressofINS_4pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEEEPT_RSA_(%"struct.std::__2::pair"* nonnull align 4 dereferenceable(24) %call) #8
  ret %"struct.std::__2::pair"* %call1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_node"* @_ZNKSt3__210unique_ptrINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEEptEv(%"class.std::__2::unique_ptr"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_node"** @_ZNKSt3__217__compressed_pairIPNS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEE5firstEv(%"class.std::__2::__compressed_pair.11"* %__ptr_) #8
  %0 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %call, align 4
  ret %"class.std::__2::__tree_node"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(5) %"class.std::__2::__tree_node_destructor"* @_ZNSt3__210unique_ptrINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEE11get_deleterEv(%"class.std::__2::unique_ptr"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(5) %"class.std::__2::__tree_node_destructor"* @_ZNSt3__217__compressed_pairIPNS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEE6secondEv(%"class.std::__2::__compressed_pair.11"* %__ptr_) #8
  ret %"class.std::__2::__tree_node_destructor"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__217__compressed_pairINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEENS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS7_IcEEEESE_EES3_EEEEE6secondEv(%"class.std::__2::__compressed_pair"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem.0"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.0"* %0) #8
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.0"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.0"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.0"* %this, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.0"*, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.0"* %this1 to %"class.std::__2::allocator"*
  ret %"class.std::__2::allocator"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_node"* @_ZNSt3__29allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEES7_EEPvEEE8allocateEmPKv(%"class.std::__2::allocator"* %this, i32 %__n, i8* %0) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  %__n.addr = alloca i32, align 4
  %.addr = alloca i8*, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEES7_EEPvEEE8max_sizeEv(%"class.std::__2::allocator"* %this1) #8
  %cmp = icmp ugt i32 %1, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__220__throw_length_errorEPKc(i8* getelementptr inbounds ([68 x i8], [68 x i8]* @.str.1, i32 0, i32 0)) #9
  unreachable

if.end:                                           ; preds = %entry
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 40
  %call2 = call i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %mul, i32 4)
  %3 = bitcast i8* %call2 to %"class.std::__2::__tree_node"*
  ret %"class.std::__2::__tree_node"* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__29allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEES7_EEPvEEE8max_sizeEv(%"class.std::__2::allocator"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  ret i32 107374182
}

; Function Attrs: noinline noreturn nounwind optnone
define linkonce_odr hidden void @_ZNSt3__220__throw_length_errorEPKc(i8* %__msg) #4 comdat {
entry:
  %__msg.addr = alloca i8*, align 4
  store i8* %__msg, i8** %__msg.addr, align 4
  call void @abort() #9
  unreachable
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %__size, i32 %__align) #0 comdat {
entry:
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i32, i32* %__size.addr, align 4
  %call = call noalias nonnull i8* @_Znwm(i32 %0) #10
  ret i8* %call
}

; Function Attrs: noreturn
declare void @abort() #5

; Function Attrs: nobuiltin allocsize(0)
declare nonnull i8* @_Znwm(i32) #6

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(5) %"class.std::__2::__tree_node_destructor"* @_ZNSt3__24moveIRNS_22__tree_node_destructorINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS2_IcEEEES9_EEPvEEEEEEEEONS_16remove_referenceIT_E4typeEOSH_(%"class.std::__2::__tree_node_destructor"* nonnull align 4 dereferenceable(5) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::__tree_node_destructor"*, align 4
  store %"class.std::__2::__tree_node_destructor"* %__t, %"class.std::__2::__tree_node_destructor"** %__t.addr, align 4
  %0 = load %"class.std::__2::__tree_node_destructor"*, %"class.std::__2::__tree_node_destructor"** %__t.addr, align 4
  ret %"class.std::__2::__tree_node_destructor"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.11"* @_ZNSt3__217__compressed_pairIPNS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEEC2IRSC_SF_EEOT_OT0_(%"class.std::__2::__compressed_pair.11"* returned %this, %"class.std::__2::__tree_node"** nonnull align 4 dereferenceable(4) %__t1, %"class.std::__2::__tree_node_destructor"* nonnull align 4 dereferenceable(5) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.11"*, align 4
  %__t1.addr = alloca %"class.std::__2::__tree_node"**, align 4
  %__t2.addr = alloca %"class.std::__2::__tree_node_destructor"*, align 4
  store %"class.std::__2::__compressed_pair.11"* %this, %"class.std::__2::__compressed_pair.11"** %this.addr, align 4
  store %"class.std::__2::__tree_node"** %__t1, %"class.std::__2::__tree_node"*** %__t1.addr, align 4
  store %"class.std::__2::__tree_node_destructor"* %__t2, %"class.std::__2::__tree_node_destructor"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.11"*, %"class.std::__2::__compressed_pair.11"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.11"* %this1 to %"struct.std::__2::__compressed_pair_elem.12"*
  %1 = load %"class.std::__2::__tree_node"**, %"class.std::__2::__tree_node"*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_node"** @_ZNSt3__27forwardIRPNS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEEEEOT_RNS_16remove_referenceISE_E4typeE(%"class.std::__2::__tree_node"** nonnull align 4 dereferenceable(4) %1) #8
  %call2 = call %"struct.std::__2::__compressed_pair_elem.12"* @_ZNSt3__222__compressed_pair_elemIPNS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEELi0ELb0EEC2IRSC_vEEOT_(%"struct.std::__2::__compressed_pair_elem.12"* %0, %"class.std::__2::__tree_node"** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.11"* %this1 to i8*
  %3 = getelementptr inbounds i8, i8* %2, i32 4
  %4 = bitcast i8* %3 to %"struct.std::__2::__compressed_pair_elem.13"*
  %5 = load %"class.std::__2::__tree_node_destructor"*, %"class.std::__2::__tree_node_destructor"** %__t2.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(5) %"class.std::__2::__tree_node_destructor"* @_ZNSt3__27forwardINS_22__tree_node_destructorINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS2_IcEEEES9_EEPvEEEEEEEEOT_RNS_16remove_referenceISF_E4typeE(%"class.std::__2::__tree_node_destructor"* nonnull align 4 dereferenceable(5) %5) #8
  %call4 = call %"struct.std::__2::__compressed_pair_elem.13"* @_ZNSt3__222__compressed_pair_elemINS_22__tree_node_destructorINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS2_IcEEEES9_EEPvEEEEEELi1ELb0EEC2ISE_vEEOT_(%"struct.std::__2::__compressed_pair_elem.13"* %4, %"class.std::__2::__tree_node_destructor"* nonnull align 4 dereferenceable(5) %call3)
  ret %"class.std::__2::__compressed_pair.11"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_node"** @_ZNSt3__27forwardIRPNS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEEEEOT_RNS_16remove_referenceISE_E4typeE(%"class.std::__2::__tree_node"** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::__tree_node"**, align 4
  store %"class.std::__2::__tree_node"** %__t, %"class.std::__2::__tree_node"*** %__t.addr, align 4
  %0 = load %"class.std::__2::__tree_node"**, %"class.std::__2::__tree_node"*** %__t.addr, align 4
  ret %"class.std::__2::__tree_node"** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.12"* @_ZNSt3__222__compressed_pair_elemIPNS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEELi0ELb0EEC2IRSC_vEEOT_(%"struct.std::__2::__compressed_pair_elem.12"* returned %this, %"class.std::__2::__tree_node"** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.12"*, align 4
  %__u.addr = alloca %"class.std::__2::__tree_node"**, align 4
  store %"struct.std::__2::__compressed_pair_elem.12"* %this, %"struct.std::__2::__compressed_pair_elem.12"** %this.addr, align 4
  store %"class.std::__2::__tree_node"** %__u, %"class.std::__2::__tree_node"*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.12"*, %"struct.std::__2::__compressed_pair_elem.12"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.12", %"struct.std::__2::__compressed_pair_elem.12"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::__tree_node"**, %"class.std::__2::__tree_node"*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_node"** @_ZNSt3__27forwardIRPNS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEEEEOT_RNS_16remove_referenceISE_E4typeE(%"class.std::__2::__tree_node"** nonnull align 4 dereferenceable(4) %0) #8
  %1 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %call, align 4
  store %"class.std::__2::__tree_node"* %1, %"class.std::__2::__tree_node"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.12"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(5) %"class.std::__2::__tree_node_destructor"* @_ZNSt3__27forwardINS_22__tree_node_destructorINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS2_IcEEEES9_EEPvEEEEEEEEOT_RNS_16remove_referenceISF_E4typeE(%"class.std::__2::__tree_node_destructor"* nonnull align 4 dereferenceable(5) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::__tree_node_destructor"*, align 4
  store %"class.std::__2::__tree_node_destructor"* %__t, %"class.std::__2::__tree_node_destructor"** %__t.addr, align 4
  %0 = load %"class.std::__2::__tree_node_destructor"*, %"class.std::__2::__tree_node_destructor"** %__t.addr, align 4
  ret %"class.std::__2::__tree_node_destructor"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.13"* @_ZNSt3__222__compressed_pair_elemINS_22__tree_node_destructorINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS2_IcEEEES9_EEPvEEEEEELi1ELb0EEC2ISE_vEEOT_(%"struct.std::__2::__compressed_pair_elem.13"* returned %this, %"class.std::__2::__tree_node_destructor"* nonnull align 4 dereferenceable(5) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.13"*, align 4
  %__u.addr = alloca %"class.std::__2::__tree_node_destructor"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.13"* %this, %"struct.std::__2::__compressed_pair_elem.13"** %this.addr, align 4
  store %"class.std::__2::__tree_node_destructor"* %__u, %"class.std::__2::__tree_node_destructor"** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.13"*, %"struct.std::__2::__compressed_pair_elem.13"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.13", %"struct.std::__2::__compressed_pair_elem.13"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::__tree_node_destructor"*, %"class.std::__2::__tree_node_destructor"** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(5) %"class.std::__2::__tree_node_destructor"* @_ZNSt3__27forwardINS_22__tree_node_destructorINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS2_IcEEEES9_EEPvEEEEEEEEOT_RNS_16remove_referenceISF_E4typeE(%"class.std::__2::__tree_node_destructor"* nonnull align 4 dereferenceable(5) %0) #8
  %1 = bitcast %"class.std::__2::__tree_node_destructor"* %__value_ to i8*
  %2 = bitcast %"class.std::__2::__tree_node_destructor"* %call to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 8, i1 false)
  ret %"struct.std::__2::__compressed_pair_elem.13"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEEE11__constructINS_4pairIKS8_S8_EEJRKNS_21piecewise_construct_tENS_5tupleIJRSG_EEENSL_IJEEEEEEvNS_17integral_constantIbLb1EEERSC_PT_DpOT0_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, %"struct.std::__2::pair"* %__p, %"struct.std::__2::piecewise_construct_t"* nonnull align 1 dereferenceable(1) %__args, %"class.std::__2::tuple"* nonnull align 4 dereferenceable(4) %__args1, %"class.std::__2::tuple.10"* nonnull align 1 dereferenceable(1) %__args3) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca %"struct.std::__2::pair"*, align 4
  %__args.addr = alloca %"struct.std::__2::piecewise_construct_t"*, align 4
  %__args.addr2 = alloca %"class.std::__2::tuple"*, align 4
  %__args.addr4 = alloca %"class.std::__2::tuple.10"*, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store %"struct.std::__2::pair"* %__p, %"struct.std::__2::pair"** %__p.addr, align 4
  store %"struct.std::__2::piecewise_construct_t"* %__args, %"struct.std::__2::piecewise_construct_t"** %__args.addr, align 4
  store %"class.std::__2::tuple"* %__args1, %"class.std::__2::tuple"** %__args.addr2, align 4
  store %"class.std::__2::tuple.10"* %__args3, %"class.std::__2::tuple.10"** %__args.addr4, align 4
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %2 = load %"struct.std::__2::pair"*, %"struct.std::__2::pair"** %__p.addr, align 4
  %3 = load %"struct.std::__2::piecewise_construct_t"*, %"struct.std::__2::piecewise_construct_t"** %__args.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::piecewise_construct_t"* @_ZNSt3__27forwardIRKNS_21piecewise_construct_tEEEOT_RNS_16remove_referenceIS4_E4typeE(%"struct.std::__2::piecewise_construct_t"* nonnull align 1 dereferenceable(1) %3) #8
  %4 = load %"class.std::__2::tuple"*, %"class.std::__2::tuple"** %__args.addr2, align 4
  %call5 = call nonnull align 4 dereferenceable(4) %"class.std::__2::tuple"* @_ZNSt3__27forwardINS_5tupleIJRKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEEEEEOT_RNS_16remove_referenceISB_E4typeE(%"class.std::__2::tuple"* nonnull align 4 dereferenceable(4) %4) #8
  %5 = load %"class.std::__2::tuple.10"*, %"class.std::__2::tuple.10"** %__args.addr4, align 4
  %call6 = call nonnull align 1 dereferenceable(1) %"class.std::__2::tuple.10"* @_ZNSt3__27forwardINS_5tupleIJEEEEEOT_RNS_16remove_referenceIS3_E4typeE(%"class.std::__2::tuple.10"* nonnull align 1 dereferenceable(1) %5) #8
  call void @_ZNSt3__29allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEES7_EEPvEEE9constructINS_4pairIKS7_S7_EEJRKNS_21piecewise_construct_tENS_5tupleIJRSE_EEENSJ_IJEEEEEEvPT_DpOT0_(%"class.std::__2::allocator"* %1, %"struct.std::__2::pair"* %2, %"struct.std::__2::piecewise_construct_t"* nonnull align 1 dereferenceable(1) %call, %"class.std::__2::tuple"* nonnull align 4 dereferenceable(4) %call5, %"class.std::__2::tuple.10"* nonnull align 1 dereferenceable(1) %call6)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEES7_EEPvEEE9constructINS_4pairIKS7_S7_EEJRKNS_21piecewise_construct_tENS_5tupleIJRSE_EEENSJ_IJEEEEEEvPT_DpOT0_(%"class.std::__2::allocator"* %this, %"struct.std::__2::pair"* %__p, %"struct.std::__2::piecewise_construct_t"* nonnull align 1 dereferenceable(1) %__args, %"class.std::__2::tuple"* nonnull align 4 dereferenceable(4) %__args1, %"class.std::__2::tuple.10"* nonnull align 1 dereferenceable(1) %__args3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca %"struct.std::__2::pair"*, align 4
  %__args.addr = alloca %"struct.std::__2::piecewise_construct_t"*, align 4
  %__args.addr2 = alloca %"class.std::__2::tuple"*, align 4
  %__args.addr4 = alloca %"class.std::__2::tuple.10"*, align 4
  %agg.tmp = alloca %"struct.std::__2::piecewise_construct_t", align 1
  %agg.tmp6 = alloca %"class.std::__2::tuple", align 4
  %agg.tmp8 = alloca %"class.std::__2::tuple.10", align 1
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  store %"struct.std::__2::pair"* %__p, %"struct.std::__2::pair"** %__p.addr, align 4
  store %"struct.std::__2::piecewise_construct_t"* %__args, %"struct.std::__2::piecewise_construct_t"** %__args.addr, align 4
  store %"class.std::__2::tuple"* %__args1, %"class.std::__2::tuple"** %__args.addr2, align 4
  store %"class.std::__2::tuple.10"* %__args3, %"class.std::__2::tuple.10"** %__args.addr4, align 4
  %this5 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  %0 = load %"struct.std::__2::pair"*, %"struct.std::__2::pair"** %__p.addr, align 4
  %1 = bitcast %"struct.std::__2::pair"* %0 to i8*
  %2 = bitcast i8* %1 to %"struct.std::__2::pair"*
  %3 = load %"struct.std::__2::piecewise_construct_t"*, %"struct.std::__2::piecewise_construct_t"** %__args.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::piecewise_construct_t"* @_ZNSt3__27forwardIRKNS_21piecewise_construct_tEEEOT_RNS_16remove_referenceIS4_E4typeE(%"struct.std::__2::piecewise_construct_t"* nonnull align 1 dereferenceable(1) %3) #8
  %4 = load %"class.std::__2::tuple"*, %"class.std::__2::tuple"** %__args.addr2, align 4
  %call7 = call nonnull align 4 dereferenceable(4) %"class.std::__2::tuple"* @_ZNSt3__27forwardINS_5tupleIJRKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEEEEEOT_RNS_16remove_referenceISB_E4typeE(%"class.std::__2::tuple"* nonnull align 4 dereferenceable(4) %4) #8
  %5 = bitcast %"class.std::__2::tuple"* %agg.tmp6 to i8*
  %6 = bitcast %"class.std::__2::tuple"* %call7 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %5, i8* align 4 %6, i32 4, i1 false)
  %7 = load %"class.std::__2::tuple.10"*, %"class.std::__2::tuple.10"** %__args.addr4, align 4
  %call9 = call nonnull align 1 dereferenceable(1) %"class.std::__2::tuple.10"* @_ZNSt3__27forwardINS_5tupleIJEEEEEOT_RNS_16remove_referenceIS3_E4typeE(%"class.std::__2::tuple.10"* nonnull align 1 dereferenceable(1) %7) #8
  %coerce.dive = getelementptr inbounds %"class.std::__2::tuple", %"class.std::__2::tuple"* %agg.tmp6, i32 0, i32 0
  %coerce.dive10 = getelementptr inbounds %"struct.std::__2::__tuple_impl", %"struct.std::__2::__tuple_impl"* %coerce.dive, i32 0, i32 0
  %coerce.dive11 = getelementptr inbounds %"class.std::__2::__tuple_leaf", %"class.std::__2::__tuple_leaf"* %coerce.dive10, i32 0, i32 0
  %8 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %coerce.dive11, align 4
  %call12 = call %"struct.std::__2::pair"* @_ZNSt3__24pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_EC2IJRS7_EJEEENS_21piecewise_construct_tENS_5tupleIJDpT_EEENSC_IJDpT0_EEE(%"struct.std::__2::pair"* %2, %"class.std::__2::basic_string"* %8)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::pair"* @_ZNSt3__24pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_EC2IJRS7_EJEEENS_21piecewise_construct_tENS_5tupleIJDpT_EEENSC_IJDpT0_EEE(%"struct.std::__2::pair"* returned %this, %"class.std::__2::basic_string"* %__first_args.coerce) unnamed_addr #0 comdat {
entry:
  %__pc = alloca %"struct.std::__2::piecewise_construct_t", align 1
  %__first_args = alloca %"class.std::__2::tuple", align 4
  %__second_args = alloca %"class.std::__2::tuple.10", align 1
  %this.addr = alloca %"struct.std::__2::pair"*, align 4
  %agg.tmp = alloca %"struct.std::__2::piecewise_construct_t", align 1
  %agg.tmp5 = alloca %"struct.std::__2::__tuple_indices", align 1
  %agg.tmp6 = alloca %"struct.std::__2::__tuple_indices.14", align 1
  %coerce.dive = getelementptr inbounds %"class.std::__2::tuple", %"class.std::__2::tuple"* %__first_args, i32 0, i32 0
  %coerce.dive1 = getelementptr inbounds %"struct.std::__2::__tuple_impl", %"struct.std::__2::__tuple_impl"* %coerce.dive, i32 0, i32 0
  %coerce.dive2 = getelementptr inbounds %"class.std::__2::__tuple_leaf", %"class.std::__2::__tuple_leaf"* %coerce.dive1, i32 0, i32 0
  store %"class.std::__2::basic_string"* %__first_args.coerce, %"class.std::__2::basic_string"** %coerce.dive2, align 4
  store %"struct.std::__2::pair"* %this, %"struct.std::__2::pair"** %this.addr, align 4
  %this4 = load %"struct.std::__2::pair"*, %"struct.std::__2::pair"** %this.addr, align 4
  %call = call %"struct.std::__2::pair"* @_ZNSt3__24pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_EC2IJRS7_EJEJLm0EEJEEENS_21piecewise_construct_tERNS_5tupleIJDpT_EEERNSC_IJDpT0_EEENS_15__tuple_indicesIJXspT1_EEEENSL_IJXspT2_EEEE(%"struct.std::__2::pair"* %this4, %"class.std::__2::tuple"* nonnull align 4 dereferenceable(4) %__first_args, %"class.std::__2::tuple.10"* nonnull align 1 dereferenceable(1) %__second_args)
  ret %"struct.std::__2::pair"* %this4
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::pair"* @_ZNSt3__24pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_EC2IJRS7_EJEJLm0EEJEEENS_21piecewise_construct_tERNS_5tupleIJDpT_EEERNSC_IJDpT0_EEENS_15__tuple_indicesIJXspT1_EEEENSL_IJXspT2_EEEE(%"struct.std::__2::pair"* returned %this, %"class.std::__2::tuple"* nonnull align 4 dereferenceable(4) %__first_args, %"class.std::__2::tuple.10"* nonnull align 1 dereferenceable(1) %__second_args) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::piecewise_construct_t", align 1
  %1 = alloca %"struct.std::__2::__tuple_indices", align 1
  %2 = alloca %"struct.std::__2::__tuple_indices.14", align 1
  %this.addr = alloca %"struct.std::__2::pair"*, align 4
  %__first_args.addr = alloca %"class.std::__2::tuple"*, align 4
  %__second_args.addr = alloca %"class.std::__2::tuple.10"*, align 4
  store %"struct.std::__2::pair"* %this, %"struct.std::__2::pair"** %this.addr, align 4
  store %"class.std::__2::tuple"* %__first_args, %"class.std::__2::tuple"** %__first_args.addr, align 4
  store %"class.std::__2::tuple.10"* %__second_args, %"class.std::__2::tuple.10"** %__second_args.addr, align 4
  %this3 = load %"struct.std::__2::pair"*, %"struct.std::__2::pair"** %this.addr, align 4
  %first = getelementptr inbounds %"struct.std::__2::pair", %"struct.std::__2::pair"* %this3, i32 0, i32 0
  %3 = load %"class.std::__2::tuple"*, %"class.std::__2::tuple"** %__first_args.addr, align 4
  %call = call nonnull align 4 dereferenceable(12) %"class.std::__2::basic_string"* @_ZNSt3__23getILm0EJRKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEEERNS_13tuple_elementIXT_ENS_5tupleIJDpT0_EEEE4typeERSD_(%"class.std::__2::tuple"* nonnull align 4 dereferenceable(4) %3) #8
  %call4 = call nonnull align 4 dereferenceable(12) %"class.std::__2::basic_string"* @_ZNSt3__27forwardIRKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEEOT_RNS_16remove_referenceIS9_E4typeE(%"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %call) #8
  %call5 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKS5_(%"class.std::__2::basic_string"* %first, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %call4)
  %second = getelementptr inbounds %"struct.std::__2::pair", %"struct.std::__2::pair"* %this3, i32 0, i32 1
  %call6 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2Ev(%"class.std::__2::basic_string"* %second) #8
  ret %"struct.std::__2::pair"* %this3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"class.std::__2::basic_string"* @_ZNSt3__27forwardIRKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEEOT_RNS_16remove_referenceIS9_E4typeE(%"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %__t, %"class.std::__2::basic_string"** %__t.addr, align 4
  %0 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__t.addr, align 4
  ret %"class.std::__2::basic_string"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"class.std::__2::basic_string"* @_ZNSt3__23getILm0EJRKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEEERNS_13tuple_elementIXT_ENS_5tupleIJDpT0_EEEE4typeERSD_(%"class.std::__2::tuple"* nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::tuple"*, align 4
  store %"class.std::__2::tuple"* %__t, %"class.std::__2::tuple"** %__t.addr, align 4
  %0 = load %"class.std::__2::tuple"*, %"class.std::__2::tuple"** %__t.addr, align 4
  %__base_ = getelementptr inbounds %"class.std::__2::tuple", %"class.std::__2::tuple"* %0, i32 0, i32 0
  %1 = bitcast %"struct.std::__2::__tuple_impl"* %__base_ to %"class.std::__2::__tuple_leaf"*
  %call = call nonnull align 4 dereferenceable(12) %"class.std::__2::basic_string"* @_ZNSt3__212__tuple_leafILm0ERKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEELb0EE3getEv(%"class.std::__2::__tuple_leaf"* %1) #8
  ret %"class.std::__2::basic_string"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2Ev(%"class.std::__2::basic_string"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  %ref.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  %ref.tmp2 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::basic_string"* %this1 to %"class.std::__2::__basic_string_common"*
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::__compressed_pair.4"* @_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_EC2INS_18__default_init_tagESA_EEOT_OT0_(%"class.std::__2::__compressed_pair.4"* %__r_, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  call void @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__zeroEv(%"class.std::__2::basic_string"* %this1) #8
  ret %"class.std::__2::basic_string"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"class.std::__2::basic_string"* @_ZNSt3__212__tuple_leafILm0ERKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEELb0EE3getEv(%"class.std::__2::__tuple_leaf"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tuple_leaf"*, align 4
  store %"class.std::__2::__tuple_leaf"* %this, %"class.std::__2::__tuple_leaf"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tuple_leaf"*, %"class.std::__2::__tuple_leaf"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"class.std::__2::__tuple_leaf", %"class.std::__2::__tuple_leaf"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__value_, align 4
  ret %"class.std::__2::basic_string"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.4"* @_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_EC2INS_18__default_init_tagESA_EEOT_OT0_(%"class.std::__2::__compressed_pair.4"* returned %this, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.4"*, align 4
  %__t1.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  %agg.tmp3 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.4"* %this, %"class.std::__2::__compressed_pair.4"** %this.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t1, %"struct.std::__2::__default_init_tag"** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.4"*, %"class.std::__2::__compressed_pair.4"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.4"* %this1 to %"struct.std::__2::__compressed_pair_elem.5"*
  %1 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t1.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %1) #8
  %call2 = call %"struct.std::__2::__compressed_pair_elem.5"* @_ZNSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.5"* %0)
  %2 = bitcast %"class.std::__2::__compressed_pair.4"* %this1 to %"struct.std::__2::__compressed_pair_elem.6"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call4 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #8
  %call5 = call %"struct.std::__2::__compressed_pair_elem.6"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIcEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.6"* %2)
  ret %"class.std::__2::__compressed_pair.4"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__zeroEv(%"class.std::__2::basic_string"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  %__a = alloca [3 x i32]*, align 4
  %__i = alloca i32, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair.4"* %__r_) #8
  %0 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call, i32 0, i32 0
  %__r = bitcast %union.anon* %0 to %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__raw"*
  %__words = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__raw", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__raw"* %__r, i32 0, i32 0
  store [3 x i32]* %__words, [3 x i32]** %__a, align 4
  store i32 0, i32* %__i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %__i, align 4
  %cmp = icmp ult i32 %1, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load [3 x i32]*, [3 x i32]** %__a, align 4
  %3 = load i32, i32* %__i, align 4
  %arrayidx = getelementptr inbounds [3 x i32], [3 x i32]* %2, i32 0, i32 %3
  store i32 0, i32* %arrayidx, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %4 = load i32, i32* %__i, align 4
  %inc = add i32 %4, 1
  store i32 %inc, i32* %__i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  store %"struct.std::__2::__default_init_tag"* %__t, %"struct.std::__2::__default_init_tag"** %__t.addr, align 4
  %0 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t.addr, align 4
  ret %"struct.std::__2::__default_init_tag"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.5"* @_ZNSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.5"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.5"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.5"* %this, %"struct.std::__2::__compressed_pair_elem.5"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.5"*, %"struct.std::__2::__compressed_pair_elem.5"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.5", %"struct.std::__2::__compressed_pair_elem.5"* %this1, i32 0, i32 0
  ret %"struct.std::__2::__compressed_pair_elem.5"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.6"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIcEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.6"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.6"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.6"* %this, %"struct.std::__2::__compressed_pair_elem.6"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.6"*, %"struct.std::__2::__compressed_pair_elem.6"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.6"* %this1 to %"class.std::__2::allocator.7"*
  %call = call %"class.std::__2::allocator.7"* @_ZNSt3__29allocatorIcEC2Ev(%"class.std::__2::allocator.7"* %1) #8
  ret %"struct.std::__2::__compressed_pair_elem.6"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::allocator.7"* @_ZNSt3__29allocatorIcEC2Ev(%"class.std::__2::allocator.7"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.7"*, align 4
  store %"class.std::__2::allocator.7"* %this, %"class.std::__2::allocator.7"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.7"*, %"class.std::__2::allocator.7"** %this.addr, align 4
  ret %"class.std::__2::allocator.7"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair.4"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.4"*, align 4
  store %"class.std::__2::__compressed_pair.4"* %this, %"class.std::__2::__compressed_pair.4"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.4"*, %"class.std::__2::__compressed_pair.4"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.4"* %this1 to %"struct.std::__2::__compressed_pair_elem.5"*
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.5"* %0) #8
  ret %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.5"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.5"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.5"* %this, %"struct.std::__2::__compressed_pair_elem.5"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.5"*, %"struct.std::__2::__compressed_pair_elem.5"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.5", %"struct.std::__2::__compressed_pair_elem.5"* %this1, i32 0, i32 0
  ret %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::pair"* @_ZNSt3__29addressofINS_4pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEEEPT_RSA_(%"struct.std::__2::pair"* nonnull align 4 dereferenceable(24) %__x) #0 comdat {
entry:
  %__x.addr = alloca %"struct.std::__2::pair"*, align 4
  store %"struct.std::__2::pair"* %__x, %"struct.std::__2::pair"** %__x.addr, align 4
  %0 = load %"struct.std::__2::pair"*, %"struct.std::__2::pair"** %__x.addr, align 4
  ret %"struct.std::__2::pair"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_node"** @_ZNKSt3__217__compressed_pairIPNS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEE5firstEv(%"class.std::__2::__compressed_pair.11"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.11"*, align 4
  store %"class.std::__2::__compressed_pair.11"* %this, %"class.std::__2::__compressed_pair.11"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.11"*, %"class.std::__2::__compressed_pair.11"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.11"* %this1 to %"struct.std::__2::__compressed_pair_elem.12"*
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_node"** @_ZNKSt3__222__compressed_pair_elemIPNS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.12"* %0) #8
  ret %"class.std::__2::__tree_node"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_node"** @_ZNKSt3__222__compressed_pair_elemIPNS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.12"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.12"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.12"* %this, %"struct.std::__2::__compressed_pair_elem.12"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.12"*, %"struct.std::__2::__compressed_pair_elem.12"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.12", %"struct.std::__2::__compressed_pair_elem.12"* %this1, i32 0, i32 0
  ret %"class.std::__2::__tree_node"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(5) %"class.std::__2::__tree_node_destructor"* @_ZNSt3__217__compressed_pairIPNS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEE6secondEv(%"class.std::__2::__compressed_pair.11"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.11"*, align 4
  store %"class.std::__2::__compressed_pair.11"* %this, %"class.std::__2::__compressed_pair.11"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.11"*, %"class.std::__2::__compressed_pair.11"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.11"* %this1 to i8*
  %add.ptr = getelementptr inbounds i8, i8* %0, i32 4
  %1 = bitcast i8* %add.ptr to %"struct.std::__2::__compressed_pair_elem.13"*
  %call = call nonnull align 4 dereferenceable(5) %"class.std::__2::__tree_node_destructor"* @_ZNSt3__222__compressed_pair_elemINS_22__tree_node_destructorINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS2_IcEEEES9_EEPvEEEEEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.13"* %1) #8
  ret %"class.std::__2::__tree_node_destructor"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(5) %"class.std::__2::__tree_node_destructor"* @_ZNSt3__222__compressed_pair_elemINS_22__tree_node_destructorINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS2_IcEEEES9_EEPvEEEEEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.13"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.13"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.13"* %this, %"struct.std::__2::__compressed_pair_elem.13"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.13"*, %"struct.std::__2::__compressed_pair_elem.13"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.13", %"struct.std::__2::__compressed_pair_elem.13"* %this1, i32 0, i32 0
  ret %"class.std::__2::__tree_node_destructor"* %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__227__tree_balance_after_insertIPNS_16__tree_node_baseIPvEEEEvT_S5_(%"class.std::__2::__tree_node_base"* %__root, %"class.std::__2::__tree_node_base"* %__x) #0 comdat {
entry:
  %__root.addr = alloca %"class.std::__2::__tree_node_base"*, align 4
  %__x.addr = alloca %"class.std::__2::__tree_node_base"*, align 4
  %__y = alloca %"class.std::__2::__tree_node_base"*, align 4
  %__y27 = alloca %"class.std::__2::__tree_node_base"*, align 4
  store %"class.std::__2::__tree_node_base"* %__root, %"class.std::__2::__tree_node_base"** %__root.addr, align 4
  store %"class.std::__2::__tree_node_base"* %__x, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %0 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %1 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__root.addr, align 4
  %cmp = icmp eq %"class.std::__2::__tree_node_base"* %0, %1
  %2 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %__is_black_ = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %2, i32 0, i32 3
  %frombool = zext i1 %cmp to i8
  store i8 %frombool, i8* %__is_black_, align 4
  br label %while.cond

while.cond:                                       ; preds = %if.end51, %entry
  %3 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %4 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__root.addr, align 4
  %cmp1 = icmp ne %"class.std::__2::__tree_node_base"* %3, %4
  br i1 %cmp1, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %while.cond
  %5 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %call = call %"class.std::__2::__tree_node_base"* @_ZNKSt3__216__tree_node_baseIPvE15__parent_unsafeEv(%"class.std::__2::__tree_node_base"* %5)
  %__is_black_2 = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %call, i32 0, i32 3
  %6 = load i8, i8* %__is_black_2, align 4
  %tobool = trunc i8 %6 to i1
  %lnot = xor i1 %tobool, true
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.cond
  %7 = phi i1 [ false, %while.cond ], [ %lnot, %land.rhs ]
  br i1 %7, label %while.body, label %while.end

while.body:                                       ; preds = %land.end
  %8 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %call3 = call %"class.std::__2::__tree_node_base"* @_ZNKSt3__216__tree_node_baseIPvE15__parent_unsafeEv(%"class.std::__2::__tree_node_base"* %8)
  %call4 = call zeroext i1 @_ZNSt3__220__tree_is_left_childIPNS_16__tree_node_baseIPvEEEEbT_(%"class.std::__2::__tree_node_base"* %call3) #8
  br i1 %call4, label %if.then, label %if.else26

if.then:                                          ; preds = %while.body
  %9 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %call5 = call %"class.std::__2::__tree_node_base"* @_ZNKSt3__216__tree_node_baseIPvE15__parent_unsafeEv(%"class.std::__2::__tree_node_base"* %9)
  %call6 = call %"class.std::__2::__tree_node_base"* @_ZNKSt3__216__tree_node_baseIPvE15__parent_unsafeEv(%"class.std::__2::__tree_node_base"* %call5)
  %__right_ = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %call6, i32 0, i32 1
  %10 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__right_, align 4
  store %"class.std::__2::__tree_node_base"* %10, %"class.std::__2::__tree_node_base"** %__y, align 4
  %11 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__y, align 4
  %cmp7 = icmp ne %"class.std::__2::__tree_node_base"* %11, null
  br i1 %cmp7, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %if.then
  %12 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__y, align 4
  %__is_black_8 = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %12, i32 0, i32 3
  %13 = load i8, i8* %__is_black_8, align 4
  %tobool9 = trunc i8 %13 to i1
  br i1 %tobool9, label %if.else, label %if.then10

if.then10:                                        ; preds = %land.lhs.true
  %14 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %call11 = call %"class.std::__2::__tree_node_base"* @_ZNKSt3__216__tree_node_baseIPvE15__parent_unsafeEv(%"class.std::__2::__tree_node_base"* %14)
  store %"class.std::__2::__tree_node_base"* %call11, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %15 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %__is_black_12 = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %15, i32 0, i32 3
  store i8 1, i8* %__is_black_12, align 4
  %16 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %call13 = call %"class.std::__2::__tree_node_base"* @_ZNKSt3__216__tree_node_baseIPvE15__parent_unsafeEv(%"class.std::__2::__tree_node_base"* %16)
  store %"class.std::__2::__tree_node_base"* %call13, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %17 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %18 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__root.addr, align 4
  %cmp14 = icmp eq %"class.std::__2::__tree_node_base"* %17, %18
  %19 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %__is_black_15 = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %19, i32 0, i32 3
  %frombool16 = zext i1 %cmp14 to i8
  store i8 %frombool16, i8* %__is_black_15, align 4
  %20 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__y, align 4
  %__is_black_17 = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %20, i32 0, i32 3
  store i8 1, i8* %__is_black_17, align 4
  br label %if.end25

if.else:                                          ; preds = %land.lhs.true, %if.then
  %21 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %call18 = call zeroext i1 @_ZNSt3__220__tree_is_left_childIPNS_16__tree_node_baseIPvEEEEbT_(%"class.std::__2::__tree_node_base"* %21) #8
  br i1 %call18, label %if.end, label %if.then19

if.then19:                                        ; preds = %if.else
  %22 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %call20 = call %"class.std::__2::__tree_node_base"* @_ZNKSt3__216__tree_node_baseIPvE15__parent_unsafeEv(%"class.std::__2::__tree_node_base"* %22)
  store %"class.std::__2::__tree_node_base"* %call20, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %23 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  call void @_ZNSt3__218__tree_left_rotateIPNS_16__tree_node_baseIPvEEEEvT_(%"class.std::__2::__tree_node_base"* %23) #8
  br label %if.end

if.end:                                           ; preds = %if.then19, %if.else
  %24 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %call21 = call %"class.std::__2::__tree_node_base"* @_ZNKSt3__216__tree_node_baseIPvE15__parent_unsafeEv(%"class.std::__2::__tree_node_base"* %24)
  store %"class.std::__2::__tree_node_base"* %call21, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %25 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %__is_black_22 = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %25, i32 0, i32 3
  store i8 1, i8* %__is_black_22, align 4
  %26 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %call23 = call %"class.std::__2::__tree_node_base"* @_ZNKSt3__216__tree_node_baseIPvE15__parent_unsafeEv(%"class.std::__2::__tree_node_base"* %26)
  store %"class.std::__2::__tree_node_base"* %call23, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %27 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %__is_black_24 = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %27, i32 0, i32 3
  store i8 0, i8* %__is_black_24, align 4
  %28 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  call void @_ZNSt3__219__tree_right_rotateIPNS_16__tree_node_baseIPvEEEEvT_(%"class.std::__2::__tree_node_base"* %28) #8
  br label %while.end

if.end25:                                         ; preds = %if.then10
  br label %if.end51

if.else26:                                        ; preds = %while.body
  %29 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %call28 = call %"class.std::__2::__tree_node_base"* @_ZNKSt3__216__tree_node_baseIPvE15__parent_unsafeEv(%"class.std::__2::__tree_node_base"* %29)
  %__parent_ = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %call28, i32 0, i32 2
  %30 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %__parent_, align 4
  %__left_ = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %30, i32 0, i32 0
  %31 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__left_, align 4
  store %"class.std::__2::__tree_node_base"* %31, %"class.std::__2::__tree_node_base"** %__y27, align 4
  %32 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__y27, align 4
  %cmp29 = icmp ne %"class.std::__2::__tree_node_base"* %32, null
  br i1 %cmp29, label %land.lhs.true30, label %if.else41

land.lhs.true30:                                  ; preds = %if.else26
  %33 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__y27, align 4
  %__is_black_31 = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %33, i32 0, i32 3
  %34 = load i8, i8* %__is_black_31, align 4
  %tobool32 = trunc i8 %34 to i1
  br i1 %tobool32, label %if.else41, label %if.then33

if.then33:                                        ; preds = %land.lhs.true30
  %35 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %call34 = call %"class.std::__2::__tree_node_base"* @_ZNKSt3__216__tree_node_baseIPvE15__parent_unsafeEv(%"class.std::__2::__tree_node_base"* %35)
  store %"class.std::__2::__tree_node_base"* %call34, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %36 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %__is_black_35 = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %36, i32 0, i32 3
  store i8 1, i8* %__is_black_35, align 4
  %37 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %call36 = call %"class.std::__2::__tree_node_base"* @_ZNKSt3__216__tree_node_baseIPvE15__parent_unsafeEv(%"class.std::__2::__tree_node_base"* %37)
  store %"class.std::__2::__tree_node_base"* %call36, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %38 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %39 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__root.addr, align 4
  %cmp37 = icmp eq %"class.std::__2::__tree_node_base"* %38, %39
  %40 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %__is_black_38 = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %40, i32 0, i32 3
  %frombool39 = zext i1 %cmp37 to i8
  store i8 %frombool39, i8* %__is_black_38, align 4
  %41 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__y27, align 4
  %__is_black_40 = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %41, i32 0, i32 3
  store i8 1, i8* %__is_black_40, align 4
  br label %if.end50

if.else41:                                        ; preds = %land.lhs.true30, %if.else26
  %42 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %call42 = call zeroext i1 @_ZNSt3__220__tree_is_left_childIPNS_16__tree_node_baseIPvEEEEbT_(%"class.std::__2::__tree_node_base"* %42) #8
  br i1 %call42, label %if.then43, label %if.end45

if.then43:                                        ; preds = %if.else41
  %43 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %call44 = call %"class.std::__2::__tree_node_base"* @_ZNKSt3__216__tree_node_baseIPvE15__parent_unsafeEv(%"class.std::__2::__tree_node_base"* %43)
  store %"class.std::__2::__tree_node_base"* %call44, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %44 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  call void @_ZNSt3__219__tree_right_rotateIPNS_16__tree_node_baseIPvEEEEvT_(%"class.std::__2::__tree_node_base"* %44) #8
  br label %if.end45

if.end45:                                         ; preds = %if.then43, %if.else41
  %45 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %call46 = call %"class.std::__2::__tree_node_base"* @_ZNKSt3__216__tree_node_baseIPvE15__parent_unsafeEv(%"class.std::__2::__tree_node_base"* %45)
  store %"class.std::__2::__tree_node_base"* %call46, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %46 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %__is_black_47 = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %46, i32 0, i32 3
  store i8 1, i8* %__is_black_47, align 4
  %47 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %call48 = call %"class.std::__2::__tree_node_base"* @_ZNKSt3__216__tree_node_baseIPvE15__parent_unsafeEv(%"class.std::__2::__tree_node_base"* %47)
  store %"class.std::__2::__tree_node_base"* %call48, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %48 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %__is_black_49 = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %48, i32 0, i32 3
  store i8 0, i8* %__is_black_49, align 4
  %49 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  call void @_ZNSt3__218__tree_left_rotateIPNS_16__tree_node_baseIPvEEEEvT_(%"class.std::__2::__tree_node_base"* %49) #8
  br label %while.end

if.end50:                                         ; preds = %if.then33
  br label %if.end51

if.end51:                                         ; preds = %if.end50, %if.end25
  br label %while.cond

while.end:                                        ; preds = %if.end45, %if.end, %land.end
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE4sizeEv(%"class.std::__2::__tree"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree"*, align 4
  store %"class.std::__2::__tree"* %this, %"class.std::__2::__tree"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree"*, %"class.std::__2::__tree"** %this.addr, align 4
  %__pair3_ = getelementptr inbounds %"class.std::__2::__tree", %"class.std::__2::__tree"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__217__compressed_pairImNS_19__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS7_S7_EENS_4lessIS7_EELb1EEEE5firstEv(%"class.std::__2::__compressed_pair.1"* %__pair3_) #8
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__218__tree_left_rotateIPNS_16__tree_node_baseIPvEEEEvT_(%"class.std::__2::__tree_node_base"* %__x) #0 comdat {
entry:
  %__x.addr = alloca %"class.std::__2::__tree_node_base"*, align 4
  %__y = alloca %"class.std::__2::__tree_node_base"*, align 4
  store %"class.std::__2::__tree_node_base"* %__x, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %0 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %__right_ = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %0, i32 0, i32 1
  %1 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__right_, align 4
  store %"class.std::__2::__tree_node_base"* %1, %"class.std::__2::__tree_node_base"** %__y, align 4
  %2 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__y, align 4
  %3 = bitcast %"class.std::__2::__tree_node_base"* %2 to %"class.std::__2::__tree_end_node"*
  %__left_ = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %3, i32 0, i32 0
  %4 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__left_, align 4
  %5 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %__right_1 = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %5, i32 0, i32 1
  store %"class.std::__2::__tree_node_base"* %4, %"class.std::__2::__tree_node_base"** %__right_1, align 4
  %6 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %__right_2 = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %6, i32 0, i32 1
  %7 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__right_2, align 4
  %cmp = icmp ne %"class.std::__2::__tree_node_base"* %7, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %8 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %__right_3 = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %8, i32 0, i32 1
  %9 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__right_3, align 4
  %10 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  call void @_ZNSt3__216__tree_node_baseIPvE12__set_parentEPS2_(%"class.std::__2::__tree_node_base"* %9, %"class.std::__2::__tree_node_base"* %10)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %11 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %__parent_ = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %11, i32 0, i32 2
  %12 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %__parent_, align 4
  %13 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__y, align 4
  %__parent_4 = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %13, i32 0, i32 2
  store %"class.std::__2::__tree_end_node"* %12, %"class.std::__2::__tree_end_node"** %__parent_4, align 4
  %14 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %call = call zeroext i1 @_ZNSt3__220__tree_is_left_childIPNS_16__tree_node_baseIPvEEEEbT_(%"class.std::__2::__tree_node_base"* %14) #8
  br i1 %call, label %if.then5, label %if.else

if.then5:                                         ; preds = %if.end
  %15 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__y, align 4
  %16 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %__parent_6 = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %16, i32 0, i32 2
  %17 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %__parent_6, align 4
  %__left_7 = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %17, i32 0, i32 0
  store %"class.std::__2::__tree_node_base"* %15, %"class.std::__2::__tree_node_base"** %__left_7, align 4
  br label %if.end10

if.else:                                          ; preds = %if.end
  %18 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__y, align 4
  %19 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %call8 = call %"class.std::__2::__tree_node_base"* @_ZNKSt3__216__tree_node_baseIPvE15__parent_unsafeEv(%"class.std::__2::__tree_node_base"* %19)
  %__right_9 = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %call8, i32 0, i32 1
  store %"class.std::__2::__tree_node_base"* %18, %"class.std::__2::__tree_node_base"** %__right_9, align 4
  br label %if.end10

if.end10:                                         ; preds = %if.else, %if.then5
  %20 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %21 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__y, align 4
  %22 = bitcast %"class.std::__2::__tree_node_base"* %21 to %"class.std::__2::__tree_end_node"*
  %__left_11 = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %22, i32 0, i32 0
  store %"class.std::__2::__tree_node_base"* %20, %"class.std::__2::__tree_node_base"** %__left_11, align 4
  %23 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %24 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__y, align 4
  call void @_ZNSt3__216__tree_node_baseIPvE12__set_parentEPS2_(%"class.std::__2::__tree_node_base"* %23, %"class.std::__2::__tree_node_base"* %24)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__219__tree_right_rotateIPNS_16__tree_node_baseIPvEEEEvT_(%"class.std::__2::__tree_node_base"* %__x) #0 comdat {
entry:
  %__x.addr = alloca %"class.std::__2::__tree_node_base"*, align 4
  %__y = alloca %"class.std::__2::__tree_node_base"*, align 4
  store %"class.std::__2::__tree_node_base"* %__x, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %0 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %1 = bitcast %"class.std::__2::__tree_node_base"* %0 to %"class.std::__2::__tree_end_node"*
  %__left_ = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %1, i32 0, i32 0
  %2 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__left_, align 4
  store %"class.std::__2::__tree_node_base"* %2, %"class.std::__2::__tree_node_base"** %__y, align 4
  %3 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__y, align 4
  %__right_ = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %3, i32 0, i32 1
  %4 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__right_, align 4
  %5 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %6 = bitcast %"class.std::__2::__tree_node_base"* %5 to %"class.std::__2::__tree_end_node"*
  %__left_1 = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %6, i32 0, i32 0
  store %"class.std::__2::__tree_node_base"* %4, %"class.std::__2::__tree_node_base"** %__left_1, align 4
  %7 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %8 = bitcast %"class.std::__2::__tree_node_base"* %7 to %"class.std::__2::__tree_end_node"*
  %__left_2 = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %8, i32 0, i32 0
  %9 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__left_2, align 4
  %cmp = icmp ne %"class.std::__2::__tree_node_base"* %9, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %10 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %11 = bitcast %"class.std::__2::__tree_node_base"* %10 to %"class.std::__2::__tree_end_node"*
  %__left_3 = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %11, i32 0, i32 0
  %12 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__left_3, align 4
  %13 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  call void @_ZNSt3__216__tree_node_baseIPvE12__set_parentEPS2_(%"class.std::__2::__tree_node_base"* %12, %"class.std::__2::__tree_node_base"* %13)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %14 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %__parent_ = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %14, i32 0, i32 2
  %15 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %__parent_, align 4
  %16 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__y, align 4
  %__parent_4 = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %16, i32 0, i32 2
  store %"class.std::__2::__tree_end_node"* %15, %"class.std::__2::__tree_end_node"** %__parent_4, align 4
  %17 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %call = call zeroext i1 @_ZNSt3__220__tree_is_left_childIPNS_16__tree_node_baseIPvEEEEbT_(%"class.std::__2::__tree_node_base"* %17) #8
  br i1 %call, label %if.then5, label %if.else

if.then5:                                         ; preds = %if.end
  %18 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__y, align 4
  %19 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %__parent_6 = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %19, i32 0, i32 2
  %20 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %__parent_6, align 4
  %__left_7 = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %20, i32 0, i32 0
  store %"class.std::__2::__tree_node_base"* %18, %"class.std::__2::__tree_node_base"** %__left_7, align 4
  br label %if.end10

if.else:                                          ; preds = %if.end
  %21 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__y, align 4
  %22 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %call8 = call %"class.std::__2::__tree_node_base"* @_ZNKSt3__216__tree_node_baseIPvE15__parent_unsafeEv(%"class.std::__2::__tree_node_base"* %22)
  %__right_9 = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %call8, i32 0, i32 1
  store %"class.std::__2::__tree_node_base"* %21, %"class.std::__2::__tree_node_base"** %__right_9, align 4
  br label %if.end10

if.end10:                                         ; preds = %if.else, %if.then5
  %23 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %24 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__y, align 4
  %__right_11 = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %24, i32 0, i32 1
  store %"class.std::__2::__tree_node_base"* %23, %"class.std::__2::__tree_node_base"** %__right_11, align 4
  %25 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__x.addr, align 4
  %26 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__y, align 4
  call void @_ZNSt3__216__tree_node_baseIPvE12__set_parentEPS2_(%"class.std::__2::__tree_node_base"* %25, %"class.std::__2::__tree_node_base"* %26)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216__tree_node_baseIPvE12__set_parentEPS2_(%"class.std::__2::__tree_node_base"* %this, %"class.std::__2::__tree_node_base"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree_node_base"*, align 4
  %__p.addr = alloca %"class.std::__2::__tree_node_base"*, align 4
  store %"class.std::__2::__tree_node_base"* %this, %"class.std::__2::__tree_node_base"** %this.addr, align 4
  store %"class.std::__2::__tree_node_base"* %__p, %"class.std::__2::__tree_node_base"** %__p.addr, align 4
  %this1 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %this.addr, align 4
  %0 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__p.addr, align 4
  %1 = bitcast %"class.std::__2::__tree_node_base"* %0 to %"class.std::__2::__tree_end_node"*
  %__parent_ = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %this1, i32 0, i32 2
  store %"class.std::__2::__tree_end_node"* %1, %"class.std::__2::__tree_end_node"** %__parent_, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__217__compressed_pairImNS_19__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS7_S7_EENS_4lessIS7_EELb1EEEE5firstEv(%"class.std::__2::__compressed_pair.1"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.1"*, align 4
  store %"class.std::__2::__compressed_pair.1"* %this, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.1"*, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.1"* %this1 to %"struct.std::__2::__compressed_pair_elem.2"*
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__222__compressed_pair_elemImLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.2"* %0) #8
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__222__compressed_pair_elemImLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.2"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.2"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.2"* %this, %"struct.std::__2::__compressed_pair_elem.2"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.2"*, %"struct.std::__2::__compressed_pair_elem.2"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.2", %"struct.std::__2::__compressed_pair_elem.2"* %this1, i32 0, i32 0
  ret i32* %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_node"** @_ZNSt3__217__compressed_pairIPNS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEE5firstEv(%"class.std::__2::__compressed_pair.11"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.11"*, align 4
  store %"class.std::__2::__compressed_pair.11"* %this, %"class.std::__2::__compressed_pair.11"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.11"*, %"class.std::__2::__compressed_pair.11"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.11"* %this1 to %"struct.std::__2::__compressed_pair_elem.12"*
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_node"** @_ZNSt3__222__compressed_pair_elemIPNS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.12"* %0) #8
  ret %"class.std::__2::__tree_node"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_node"** @_ZNSt3__222__compressed_pair_elemIPNS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.12"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.12"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.12"* %this, %"struct.std::__2::__compressed_pair_elem.12"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.12"*, %"struct.std::__2::__compressed_pair_elem.12"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.12", %"struct.std::__2::__compressed_pair_elem.12"* %this1, i32 0, i32 0
  ret %"class.std::__2::__tree_node"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__210unique_ptrINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEE5resetEPSB_(%"class.std::__2::unique_ptr"* %this, %"class.std::__2::__tree_node"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  %__p.addr = alloca %"class.std::__2::__tree_node"*, align 4
  %__tmp = alloca %"class.std::__2::__tree_node"*, align 4
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  store %"class.std::__2::__tree_node"* %__p, %"class.std::__2::__tree_node"** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_node"** @_ZNSt3__217__compressed_pairIPNS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEE5firstEv(%"class.std::__2::__compressed_pair.11"* %__ptr_) #8
  %0 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %call, align 4
  store %"class.std::__2::__tree_node"* %0, %"class.std::__2::__tree_node"** %__tmp, align 4
  %1 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %__p.addr, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_node"** @_ZNSt3__217__compressed_pairIPNS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEE5firstEv(%"class.std::__2::__compressed_pair.11"* %__ptr_2) #8
  store %"class.std::__2::__tree_node"* %1, %"class.std::__2::__tree_node"** %call3, align 4
  %2 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %__tmp, align 4
  %tobool = icmp ne %"class.std::__2::__tree_node"* %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__ptr_4 = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call5 = call nonnull align 4 dereferenceable(5) %"class.std::__2::__tree_node_destructor"* @_ZNSt3__217__compressed_pairIPNS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPvEENS_22__tree_node_destructorINS6_ISB_EEEEE6secondEv(%"class.std::__2::__compressed_pair.11"* %__ptr_4) #8
  %3 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %__tmp, align 4
  call void @_ZNSt3__222__tree_node_destructorINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEEEclEPSB_(%"class.std::__2::__tree_node_destructor"* %call5, %"class.std::__2::__tree_node"* %3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__222__tree_node_destructorINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEEEclEPSB_(%"class.std::__2::__tree_node_destructor"* %this, %"class.std::__2::__tree_node"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree_node_destructor"*, align 4
  %__p.addr = alloca %"class.std::__2::__tree_node"*, align 4
  store %"class.std::__2::__tree_node_destructor"* %this, %"class.std::__2::__tree_node_destructor"** %this.addr, align 4
  store %"class.std::__2::__tree_node"* %__p, %"class.std::__2::__tree_node"** %__p.addr, align 4
  %this1 = load %"class.std::__2::__tree_node_destructor"*, %"class.std::__2::__tree_node_destructor"** %this.addr, align 4
  %__value_constructed = getelementptr inbounds %"class.std::__2::__tree_node_destructor", %"class.std::__2::__tree_node_destructor"* %this1, i32 0, i32 1
  %0 = load i8, i8* %__value_constructed, align 4
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__na_ = getelementptr inbounds %"class.std::__2::__tree_node_destructor", %"class.std::__2::__tree_node_destructor"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__na_, align 4
  %2 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %__p.addr, align 4
  %__value_ = getelementptr inbounds %"class.std::__2::__tree_node", %"class.std::__2::__tree_node"* %2, i32 0, i32 1
  %call = call %"struct.std::__2::pair"* @_ZNSt3__222__tree_key_value_typesINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEE9__get_ptrERS8_(%"struct.std::__2::__value_type"* nonnull align 4 dereferenceable(24) %__value_)
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEEE7destroyINS_4pairIKS8_S8_EEEEvRSC_PT_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %1, %"struct.std::__2::pair"* %call)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %3 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %__p.addr, align 4
  %tobool2 = icmp ne %"class.std::__2::__tree_node"* %3, null
  br i1 %tobool2, label %if.then3, label %if.end5

if.then3:                                         ; preds = %if.end
  %__na_4 = getelementptr inbounds %"class.std::__2::__tree_node_destructor", %"class.std::__2::__tree_node_destructor"* %this1, i32 0, i32 0
  %4 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__na_4, align 4
  %5 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEEE10deallocateERSC_PSB_m(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %4, %"class.std::__2::__tree_node"* %5, i32 1) #8
  br label %if.end5

if.end5:                                          ; preds = %if.then3, %if.end
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEEE7destroyINS_4pairIKS8_S8_EEEEvRSC_PT_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, %"struct.std::__2::pair"* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca %"struct.std::__2::pair"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant.15", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy", align 1
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store %"struct.std::__2::pair"* %__p, %"struct.std::__2::pair"** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy"* %ref.tmp to %"struct.std::__2::integral_constant.15"*
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %2 = load %"struct.std::__2::pair"*, %"struct.std::__2::pair"** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEEE9__destroyINS_4pairIKS8_S8_EEEEvNS_17integral_constantIbLb0EEERSC_PT_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %1, %"struct.std::__2::pair"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEEE10deallocateERSC_PSB_m(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, %"class.std::__2::__tree_node"* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca %"class.std::__2::__tree_node"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store %"class.std::__2::__tree_node"* %__p, %"class.std::__2::__tree_node"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %1 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEES7_EEPvEEE10deallocateEPSA_m(%"class.std::__2::allocator"* %0, %"class.std::__2::__tree_node"* %1, i32 %2) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEEE9__destroyINS_4pairIKS8_S8_EEEEvNS_17integral_constantIbLb0EEERSC_PT_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %0, %"struct.std::__2::pair"* %__p) #0 comdat {
entry:
  %1 = alloca %"struct.std::__2::integral_constant.15", align 1
  %.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca %"struct.std::__2::pair"*, align 4
  store %"class.std::__2::allocator"* %0, %"class.std::__2::allocator"** %.addr, align 4
  store %"struct.std::__2::pair"* %__p, %"struct.std::__2::pair"** %__p.addr, align 4
  %2 = load %"struct.std::__2::pair"*, %"struct.std::__2::pair"** %__p.addr, align 4
  %call = call %"struct.std::__2::pair"* @_ZNSt3__24pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_ED2Ev(%"struct.std::__2::pair"* %2) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::pair"* @_ZNSt3__24pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_ED2Ev(%"struct.std::__2::pair"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::pair"*, align 4
  store %"struct.std::__2::pair"* %this, %"struct.std::__2::pair"** %this.addr, align 4
  %this1 = load %"struct.std::__2::pair"*, %"struct.std::__2::pair"** %this.addr, align 4
  %second = getelementptr inbounds %"struct.std::__2::pair", %"struct.std::__2::pair"* %this1, i32 0, i32 1
  %call = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_string"* %second) #8
  %first = getelementptr inbounds %"struct.std::__2::pair", %"struct.std::__2::pair"* %this1, i32 0, i32 0
  %call2 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_string"* %first) #8
  ret %"struct.std::__2::pair"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEES7_EEPvEEE10deallocateEPSA_m(%"class.std::__2::allocator"* %this, %"class.std::__2::__tree_node"* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca %"class.std::__2::__tree_node"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  store %"class.std::__2::__tree_node"* %__p, %"class.std::__2::__tree_node"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  %0 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %__p.addr, align 4
  %1 = bitcast %"class.std::__2::__tree_node"* %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 40
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %__ptr, i32 %__size, i32 %__align) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  %1 = load i32, i32* %__size.addr, align 4
  %2 = load i32, i32* %__align.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm(i8* %0, i32 %1, i32 %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm(i8* %__ptr, i32 %__size, i32 %__align) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  %1 = load i32, i32* %__size.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm(i8* %0, i32 %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm(i8* %__ptr, i32 %__size) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller9__do_callEPv(i8* %0)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller9__do_callEPv(i8* %__ptr) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  call void @_ZdlPv(i8* %0) #11
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #7

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_iterator"* @_ZNSt3__27forwardINS_15__tree_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES8_EEPNS_11__tree_nodeIS9_PvEElEEEEOT_RNS_16remove_referenceISF_E4typeE(%"class.std::__2::__tree_iterator"* nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::__tree_iterator"*, align 4
  store %"class.std::__2::__tree_iterator"* %__t, %"class.std::__2::__tree_iterator"** %__t.addr, align 4
  %0 = load %"class.std::__2::__tree_iterator"*, %"class.std::__2::__tree_iterator"** %__t.addr, align 4
  ret %"class.std::__2::__tree_iterator"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) i8* @_ZNSt3__27forwardIRbEEOT_RNS_16remove_referenceIS2_E4typeE(i8* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca i8*, align 4
  store i8* %__t, i8** %__t.addr, align 4
  %0 = load i8*, i8** %__t.addr, align 4
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::tuple"* @_ZNSt3__25tupleIJRKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEEC2ILb1ELb0EEES8_(%"class.std::__2::tuple"* returned %this, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %__t) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::tuple"*, align 4
  %__t.addr = alloca %"class.std::__2::basic_string"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__tuple_indices", align 1
  %agg.tmp2 = alloca %"struct.std::__2::__tuple_types", align 1
  %agg.tmp3 = alloca %"struct.std::__2::__tuple_indices.14", align 1
  %agg.tmp4 = alloca %"struct.std::__2::__tuple_types.16", align 1
  store %"class.std::__2::tuple"* %this, %"class.std::__2::tuple"** %this.addr, align 4
  store %"class.std::__2::basic_string"* %__t, %"class.std::__2::basic_string"** %__t.addr, align 4
  %this1 = load %"class.std::__2::tuple"*, %"class.std::__2::tuple"** %this.addr, align 4
  %__base_ = getelementptr inbounds %"class.std::__2::tuple", %"class.std::__2::tuple"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__t.addr, align 4
  %call = call %"struct.std::__2::__tuple_impl"* @_ZNSt3__212__tuple_implINS_15__tuple_indicesIJLm0EEEEJRKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEEC2IJLm0EEJSA_EJEJEJSA_EEENS1_IJXspT_EEEENS_13__tuple_typesIJDpT0_EEENS1_IJXspT1_EEEENSE_IJDpT2_EEEDpOT3_(%"struct.std::__2::__tuple_impl"* %__base_, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %0) #8
  ret %"class.std::__2::tuple"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__tuple_impl"* @_ZNSt3__212__tuple_implINS_15__tuple_indicesIJLm0EEEEJRKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEEC2IJLm0EEJSA_EJEJEJSA_EEENS1_IJXspT_EEEENS_13__tuple_typesIJDpT0_EEENS1_IJXspT1_EEEENSE_IJDpT2_EEEDpOT3_(%"struct.std::__2::__tuple_impl"* returned %this, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %__u) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__tuple_indices", align 1
  %1 = alloca %"struct.std::__2::__tuple_types", align 1
  %2 = alloca %"struct.std::__2::__tuple_indices.14", align 1
  %3 = alloca %"struct.std::__2::__tuple_types.16", align 1
  %this.addr = alloca %"struct.std::__2::__tuple_impl"*, align 4
  %__u.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"struct.std::__2::__tuple_impl"* %this, %"struct.std::__2::__tuple_impl"** %this.addr, align 4
  store %"class.std::__2::basic_string"* %__u, %"class.std::__2::basic_string"** %__u.addr, align 4
  %this4 = load %"struct.std::__2::__tuple_impl"*, %"struct.std::__2::__tuple_impl"** %this.addr, align 4
  %4 = bitcast %"struct.std::__2::__tuple_impl"* %this4 to %"class.std::__2::__tuple_leaf"*
  %5 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(12) %"class.std::__2::basic_string"* @_ZNSt3__27forwardIRKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEEOT_RNS_16remove_referenceIS9_E4typeE(%"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %5) #8
  %call5 = call %"class.std::__2::__tuple_leaf"* @_ZNSt3__212__tuple_leafILm0ERKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEELb0EEC2IS8_vEEOT_(%"class.std::__2::__tuple_leaf"* %4, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %call) #8
  ret %"struct.std::__2::__tuple_impl"* %this4
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tuple_leaf"* @_ZNSt3__212__tuple_leafILm0ERKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEELb0EEC2IS8_vEEOT_(%"class.std::__2::__tuple_leaf"* returned %this, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %__t) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tuple_leaf"*, align 4
  %__t.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::__tuple_leaf"* %this, %"class.std::__2::__tuple_leaf"** %this.addr, align 4
  store %"class.std::__2::basic_string"* %__t, %"class.std::__2::basic_string"** %__t.addr, align 4
  %this1 = load %"class.std::__2::__tuple_leaf"*, %"class.std::__2::__tuple_leaf"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"class.std::__2::__tuple_leaf", %"class.std::__2::__tuple_leaf"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__t.addr, align 4
  %call = call nonnull align 4 dereferenceable(12) %"class.std::__2::basic_string"* @_ZNSt3__27forwardIRKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEEOT_RNS_16remove_referenceIS9_E4typeE(%"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %0) #8
  store %"class.std::__2::basic_string"* %call, %"class.std::__2::basic_string"** %__value_, align 4
  ret %"class.std::__2::__tuple_leaf"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__value_type"* @_ZNSt3__214pointer_traitsIPNS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEE10pointer_toERS8_(%"struct.std::__2::__value_type"* nonnull align 4 dereferenceable(24) %__r) #0 comdat {
entry:
  %__r.addr = alloca %"struct.std::__2::__value_type"*, align 4
  store %"struct.std::__2::__value_type"* %__r, %"struct.std::__2::__value_type"** %__r.addr, align 4
  %0 = load %"struct.std::__2::__value_type"*, %"struct.std::__2::__value_type"** %__r.addr, align 4
  %call = call %"struct.std::__2::__value_type"* @_ZNSt3__29addressofINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEEEPT_RS9_(%"struct.std::__2::__value_type"* nonnull align 4 dereferenceable(24) %0) #8
  ret %"struct.std::__2::__value_type"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_node"* @_ZNKSt3__215__tree_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElE8__get_npEv(%"class.std::__2::__tree_iterator"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree_iterator"*, align 4
  store %"class.std::__2::__tree_iterator"* %this, %"class.std::__2::__tree_iterator"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree_iterator"*, %"class.std::__2::__tree_iterator"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::__tree_iterator", %"class.std::__2::__tree_iterator"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %__ptr_, align 4
  %1 = bitcast %"class.std::__2::__tree_end_node"* %0 to %"class.std::__2::__tree_node"*
  ret %"class.std::__2::__tree_node"* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__value_type"* @_ZNSt3__29addressofINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEEEPT_RS9_(%"struct.std::__2::__value_type"* nonnull align 4 dereferenceable(24) %__x) #0 comdat {
entry:
  %__x.addr = alloca %"struct.std::__2::__value_type"*, align 4
  store %"struct.std::__2::__value_type"* %__x, %"struct.std::__2::__value_type"** %__x.addr, align 4
  %0 = load %"struct.std::__2::__value_type"*, %"struct.std::__2::__value_type"** %__x.addr, align 4
  ret %"struct.std::__2::__value_type"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE13__move_assignERS5_NS_17integral_constantIbLb1EEE(%"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %__str) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  %__str.addr = alloca %"class.std::__2::basic_string"*, align 4
  %ref.tmp = alloca i8, align 1
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  store %"class.std::__2::basic_string"* %__str, %"class.std::__2::basic_string"** %__str.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %call = call zeroext i1 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE9__is_longEv(%"class.std::__2::basic_string"* %this1) #8
  br i1 %call, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.7"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE7__allocEv(%"class.std::__2::basic_string"* %this1) #8
  %call3 = call i8* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE18__get_long_pointerEv(%"class.std::__2::basic_string"* %this1) #8
  %call4 = call i32 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE14__get_long_capEv(%"class.std::__2::basic_string"* %this1) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE10deallocateERS2_Pcm(%"class.std::__2::allocator.7"* nonnull align 1 dereferenceable(1) %call2, i8* %call3, i32 %call4) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__str.addr, align 4
  call void @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE19__move_assign_allocERS5_(%"class.std::__2::basic_string"* %this1, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %1) #8
  %2 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__str.addr, align 4
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %2, i32 0, i32 0
  %call5 = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair.4"* %__r_) #8
  %__r_6 = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call7 = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair.4"* %__r_6) #8
  %3 = bitcast %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call7 to i8*
  %4 = bitcast %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 12, i1 false)
  %5 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__str.addr, align 4
  call void @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE16__set_short_sizeEm(%"class.std::__2::basic_string"* %5, i32 0) #8
  %6 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__str.addr, align 4
  %call8 = call i8* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE19__get_short_pointerEv(%"class.std::__2::basic_string"* %6) #8
  %arrayidx = getelementptr inbounds i8, i8* %call8, i32 0
  store i8 0, i8* %ref.tmp, align 1
  call void @_ZNSt3__211char_traitsIcE6assignERcRKc(i8* nonnull align 1 dereferenceable(1) %arrayidx, i8* nonnull align 1 dereferenceable(1) %ref.tmp) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE10deallocateERS2_Pcm(%"class.std::__2::allocator.7"* nonnull align 1 dereferenceable(1) %__a, i8* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.7"*, align 4
  %__p.addr = alloca i8*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.7"* %__a, %"class.std::__2::allocator.7"** %__a.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.7"*, %"class.std::__2::allocator.7"** %__a.addr, align 4
  %1 = load i8*, i8** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorIcE10deallocateEPcm(%"class.std::__2::allocator.7"* %0, i8* %1, i32 %2) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.7"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE7__allocEv(%"class.std::__2::basic_string"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.7"* @_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E6secondEv(%"class.std::__2::__compressed_pair.4"* %__r_) #8
  ret %"class.std::__2::allocator.7"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE18__get_long_pointerEv(%"class.std::__2::basic_string"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair.4"* %__r_) #8
  %0 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call, i32 0, i32 0
  %__l = bitcast %union.anon* %0 to %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long"*
  %__data_ = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long"* %__l, i32 0, i32 0
  %1 = load i8*, i8** %__data_, align 4
  ret i8* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE14__get_long_capEv(%"class.std::__2::basic_string"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair.4"* %__r_) #8
  %0 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call, i32 0, i32 0
  %__l = bitcast %union.anon* %0 to %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long"*
  %__cap_ = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long"* %__l, i32 0, i32 2
  %1 = load i32, i32* %__cap_, align 4
  %and = and i32 %1, 2147483647
  ret i32 %and
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE19__move_assign_allocERS5_(%"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %__str) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  %__str.addr = alloca %"class.std::__2::basic_string"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  store %"class.std::__2::basic_string"* %__str, %"class.std::__2::basic_string"** %__str.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %0 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__str.addr, align 4
  call void @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE19__move_assign_allocERS5_NS_17integral_constantIbLb1EEE(%"class.std::__2::basic_string"* %this1, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE16__set_short_sizeEm(%"class.std::__2::basic_string"* %this, i32 %__s) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  %__s.addr = alloca i32, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  store i32 %__s, i32* %__s.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %0 = load i32, i32* %__s.addr, align 4
  %conv = trunc i32 %0 to i8
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair.4"* %__r_) #8
  %1 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call, i32 0, i32 0
  %__s2 = bitcast %union.anon* %1 to %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short"*
  %2 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short"* %__s2, i32 0, i32 1
  %__size_ = getelementptr inbounds %struct.anon, %struct.anon* %2, i32 0, i32 0
  store i8 %conv, i8* %__size_, align 1
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__211char_traitsIcE6assignERcRKc(i8* nonnull align 1 dereferenceable(1) %__c1, i8* nonnull align 1 dereferenceable(1) %__c2) #0 comdat {
entry:
  %__c1.addr = alloca i8*, align 4
  %__c2.addr = alloca i8*, align 4
  store i8* %__c1, i8** %__c1.addr, align 4
  store i8* %__c2, i8** %__c2.addr, align 4
  %0 = load i8*, i8** %__c2.addr, align 4
  %1 = load i8, i8* %0, align 1
  %2 = load i8*, i8** %__c1.addr, align 4
  store i8 %1, i8* %2, align 1
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE19__get_short_pointerEv(%"class.std::__2::basic_string"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair.4"* %__r_) #8
  %0 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call, i32 0, i32 0
  %__s = bitcast %union.anon* %0 to %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short"*
  %__data_ = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short"* %__s, i32 0, i32 0
  %arrayidx = getelementptr inbounds [11 x i8], [11 x i8]* %__data_, i32 0, i32 0
  %call2 = call i8* @_ZNSt3__214pointer_traitsIPcE10pointer_toERc(i8* nonnull align 1 dereferenceable(1) %arrayidx) #8
  ret i8* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIcE10deallocateEPcm(%"class.std::__2::allocator.7"* %this, i8* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.7"*, align 4
  %__p.addr = alloca i8*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.7"* %this, %"class.std::__2::allocator.7"** %this.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.7"*, %"class.std::__2::allocator.7"** %this.addr, align 4
  %0 = load i8*, i8** %__p.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %1, 1
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %0, i32 %mul, i32 1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.7"* @_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E6secondEv(%"class.std::__2::__compressed_pair.4"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.4"*, align 4
  store %"class.std::__2::__compressed_pair.4"* %this, %"class.std::__2::__compressed_pair.4"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.4"*, %"class.std::__2::__compressed_pair.4"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.4"* %this1 to %"struct.std::__2::__compressed_pair_elem.6"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.7"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIcEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.6"* %0) #8
  ret %"class.std::__2::allocator.7"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.7"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIcEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.6"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.6"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.6"* %this, %"struct.std::__2::__compressed_pair_elem.6"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.6"*, %"struct.std::__2::__compressed_pair_elem.6"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.6"* %this1 to %"class.std::__2::allocator.7"*
  ret %"class.std::__2::allocator.7"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE19__move_assign_allocERS5_NS_17integral_constantIbLb1EEE(%"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %__c) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  %__c.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  store %"class.std::__2::basic_string"* %__c, %"class.std::__2::basic_string"** %__c.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__c.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.7"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE7__allocEv(%"class.std::__2::basic_string"* %1) #8
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.7"* @_ZNSt3__24moveIRNS_9allocatorIcEEEEONS_16remove_referenceIT_E4typeEOS5_(%"class.std::__2::allocator.7"* nonnull align 1 dereferenceable(1) %call) #8
  %call3 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.7"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE7__allocEv(%"class.std::__2::basic_string"* %this1) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.7"* @_ZNSt3__24moveIRNS_9allocatorIcEEEEONS_16remove_referenceIT_E4typeEOS5_(%"class.std::__2::allocator.7"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::allocator.7"*, align 4
  store %"class.std::__2::allocator.7"* %__t, %"class.std::__2::allocator.7"** %__t.addr, align 4
  %0 = load %"class.std::__2::allocator.7"*, %"class.std::__2::allocator.7"** %__t.addr, align 4
  ret %"class.std::__2::allocator.7"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__214pointer_traitsIPcE10pointer_toERc(i8* nonnull align 1 dereferenceable(1) %__r) #0 comdat {
entry:
  %__r.addr = alloca i8*, align 4
  store i8* %__r, i8** %__r.addr, align 4
  %0 = load i8*, i8** %__r.addr, align 4
  %call = call i8* @_ZNSt3__29addressofIcEEPT_RS1_(i8* nonnull align 1 dereferenceable(1) %0) #8
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__29addressofIcEEPT_RS1_(i8* nonnull align 1 dereferenceable(1) %__x) #0 comdat {
entry:
  %__x.addr = alloca i8*, align 4
  store i8* %__x, i8** %__x.addr, align 4
  %0 = load i8*, i8** %__x.addr, align 4
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_end_node"* @_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE4findIS7_EENS_21__tree_const_iteratorIS8_PNS_11__tree_nodeIS8_PvEElEERKT_(%"class.std::__2::__tree"* %this, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %__v) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__tree_const_iterator", align 4
  %this.addr = alloca %"class.std::__2::__tree"*, align 4
  %__v.addr = alloca %"class.std::__2::basic_string"*, align 4
  %__p = alloca %"class.std::__2::__tree_const_iterator", align 4
  %ref.tmp = alloca %"class.std::__2::__tree_const_iterator", align 4
  store %"class.std::__2::__tree"* %this, %"class.std::__2::__tree"** %this.addr, align 4
  store %"class.std::__2::basic_string"* %__v, %"class.std::__2::basic_string"** %__v.addr, align 4
  %this1 = load %"class.std::__2::__tree"*, %"class.std::__2::__tree"** %this.addr, align 4
  %0 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__v.addr, align 4
  %call = call %"class.std::__2::__tree_node"* @_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE6__rootEv(%"class.std::__2::__tree"* %this1) #8
  %call2 = call %"class.std::__2::__tree_end_node"* @_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE10__end_nodeEv(%"class.std::__2::__tree"* %this1) #8
  %call3 = call %"class.std::__2::__tree_end_node"* @_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE13__lower_boundIS7_EENS_21__tree_const_iteratorIS8_PNS_11__tree_nodeIS8_PvEElEERKT_SK_PNS_15__tree_end_nodeIPNS_16__tree_node_baseISI_EEEE(%"class.std::__2::__tree"* %this1, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %0, %"class.std::__2::__tree_node"* %call, %"class.std::__2::__tree_end_node"* %call2)
  %coerce.dive = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %__p, i32 0, i32 0
  store %"class.std::__2::__tree_end_node"* %call3, %"class.std::__2::__tree_end_node"** %coerce.dive, align 4
  %call4 = call %"class.std::__2::__tree_end_node"* @_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE3endEv(%"class.std::__2::__tree"* %this1) #8
  %coerce.dive5 = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %ref.tmp, i32 0, i32 0
  store %"class.std::__2::__tree_end_node"* %call4, %"class.std::__2::__tree_end_node"** %coerce.dive5, align 4
  %call6 = call zeroext i1 @_ZNSt3__2neERKNS_21__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElEESF_(%"class.std::__2::__tree_const_iterator"* nonnull align 4 dereferenceable(4) %__p, %"class.std::__2::__tree_const_iterator"* nonnull align 4 dereferenceable(4) %ref.tmp)
  br i1 %call6, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %call7 = call nonnull align 1 dereferenceable(1) %"class.std::__2::__map_value_compare"* @_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE10value_compEv(%"class.std::__2::__tree"* %this1) #8
  %1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__v.addr, align 4
  %call8 = call nonnull align 4 dereferenceable(24) %"struct.std::__2::__value_type"* @_ZNKSt3__221__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElEdeEv(%"class.std::__2::__tree_const_iterator"* %__p)
  %call9 = call zeroext i1 @_ZNKSt3__219__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS6_S6_EENS_4lessIS6_EELb1EEclERKS6_RKS8_(%"class.std::__2::__map_value_compare"* %call7, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %1, %"struct.std::__2::__value_type"* nonnull align 4 dereferenceable(24) %call8)
  %lnot = xor i1 %call9, true
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %2 = phi i1 [ false, %entry ], [ %lnot, %land.rhs ]
  br i1 %2, label %if.then, label %if.end

if.then:                                          ; preds = %land.end
  %3 = bitcast %"class.std::__2::__tree_const_iterator"* %retval to i8*
  %4 = bitcast %"class.std::__2::__tree_const_iterator"* %__p to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 4, i1 false)
  br label %return

if.end:                                           ; preds = %land.end
  %call10 = call %"class.std::__2::__tree_end_node"* @_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE3endEv(%"class.std::__2::__tree"* %this1) #8
  %coerce.dive11 = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %retval, i32 0, i32 0
  store %"class.std::__2::__tree_end_node"* %call10, %"class.std::__2::__tree_end_node"** %coerce.dive11, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %coerce.dive12 = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %retval, i32 0, i32 0
  %5 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %coerce.dive12, align 4
  ret %"class.std::__2::__tree_end_node"* %5
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_end_node"* @_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE13__lower_boundIS7_EENS_21__tree_const_iteratorIS8_PNS_11__tree_nodeIS8_PvEElEERKT_SK_PNS_15__tree_end_nodeIPNS_16__tree_node_baseISI_EEEE(%"class.std::__2::__tree"* %this, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %__v, %"class.std::__2::__tree_node"* %__root, %"class.std::__2::__tree_end_node"* %__result) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__tree_const_iterator", align 4
  %this.addr = alloca %"class.std::__2::__tree"*, align 4
  %__v.addr = alloca %"class.std::__2::basic_string"*, align 4
  %__root.addr = alloca %"class.std::__2::__tree_node"*, align 4
  %__result.addr = alloca %"class.std::__2::__tree_end_node"*, align 4
  store %"class.std::__2::__tree"* %this, %"class.std::__2::__tree"** %this.addr, align 4
  store %"class.std::__2::basic_string"* %__v, %"class.std::__2::basic_string"** %__v.addr, align 4
  store %"class.std::__2::__tree_node"* %__root, %"class.std::__2::__tree_node"** %__root.addr, align 4
  store %"class.std::__2::__tree_end_node"* %__result, %"class.std::__2::__tree_end_node"** %__result.addr, align 4
  %this1 = load %"class.std::__2::__tree"*, %"class.std::__2::__tree"** %this.addr, align 4
  br label %while.cond

while.cond:                                       ; preds = %if.end, %entry
  %0 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %__root.addr, align 4
  %cmp = icmp ne %"class.std::__2::__tree_node"* %0, null
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::__map_value_compare"* @_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE10value_compEv(%"class.std::__2::__tree"* %this1) #8
  %1 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %__root.addr, align 4
  %__value_ = getelementptr inbounds %"class.std::__2::__tree_node", %"class.std::__2::__tree_node"* %1, i32 0, i32 1
  %2 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__v.addr, align 4
  %call2 = call zeroext i1 @_ZNKSt3__219__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS6_S6_EENS_4lessIS6_EELb1EEclERKS8_RKS6_(%"class.std::__2::__map_value_compare"* %call, %"struct.std::__2::__value_type"* nonnull align 4 dereferenceable(24) %__value_, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %2)
  br i1 %call2, label %if.else, label %if.then

if.then:                                          ; preds = %while.body
  %3 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %__root.addr, align 4
  %4 = bitcast %"class.std::__2::__tree_node"* %3 to %"class.std::__2::__tree_end_node"*
  store %"class.std::__2::__tree_end_node"* %4, %"class.std::__2::__tree_end_node"** %__result.addr, align 4
  %5 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %__root.addr, align 4
  %6 = bitcast %"class.std::__2::__tree_node"* %5 to %"class.std::__2::__tree_end_node"*
  %__left_ = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %6, i32 0, i32 0
  %7 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__left_, align 4
  %8 = bitcast %"class.std::__2::__tree_node_base"* %7 to %"class.std::__2::__tree_node"*
  store %"class.std::__2::__tree_node"* %8, %"class.std::__2::__tree_node"** %__root.addr, align 4
  br label %if.end

if.else:                                          ; preds = %while.body
  %9 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %__root.addr, align 4
  %10 = bitcast %"class.std::__2::__tree_node"* %9 to %"class.std::__2::__tree_node_base"*
  %__right_ = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %10, i32 0, i32 1
  %11 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__right_, align 4
  %12 = bitcast %"class.std::__2::__tree_node_base"* %11 to %"class.std::__2::__tree_node"*
  store %"class.std::__2::__tree_node"* %12, %"class.std::__2::__tree_node"** %__root.addr, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %13 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %__result.addr, align 4
  %call3 = call %"class.std::__2::__tree_const_iterator"* @_ZNSt3__221__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElEC2EPNS_15__tree_end_nodeIPNS_16__tree_node_baseISA_EEEE(%"class.std::__2::__tree_const_iterator"* %retval, %"class.std::__2::__tree_end_node"* %13) #8
  %coerce.dive = getelementptr inbounds %"class.std::__2::__tree_const_iterator", %"class.std::__2::__tree_const_iterator"* %retval, i32 0, i32 0
  %14 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %coerce.dive, align 4
  ret %"class.std::__2::__tree_end_node"* %14
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::__map_value_compare"* @_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE10value_compEv(%"class.std::__2::__tree"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree"*, align 4
  store %"class.std::__2::__tree"* %this, %"class.std::__2::__tree"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree"*, %"class.std::__2::__tree"** %this.addr, align 4
  %__pair3_ = getelementptr inbounds %"class.std::__2::__tree", %"class.std::__2::__tree"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::__map_value_compare"* @_ZNKSt3__217__compressed_pairImNS_19__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS7_S7_EENS_4lessIS7_EELb1EEEE6secondEv(%"class.std::__2::__compressed_pair.1"* %__pair3_) #8
  ret %"class.std::__2::__map_value_compare"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(24) %"struct.std::__2::__value_type"* @_ZNKSt3__221__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElEdeEv(%"class.std::__2::__tree_const_iterator"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree_const_iterator"*, align 4
  store %"class.std::__2::__tree_const_iterator"* %this, %"class.std::__2::__tree_const_iterator"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree_const_iterator"*, %"class.std::__2::__tree_const_iterator"** %this.addr, align 4
  %call = call %"class.std::__2::__tree_node"* @_ZNKSt3__221__tree_const_iteratorINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEPNS_11__tree_nodeIS8_PvEElE8__get_npEv(%"class.std::__2::__tree_const_iterator"* %this1)
  %__value_ = getelementptr inbounds %"class.std::__2::__tree_node", %"class.std::__2::__tree_node"* %call, i32 0, i32 1
  ret %"struct.std::__2::__value_type"* %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::__map_value_compare"* @_ZNKSt3__217__compressed_pairImNS_19__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS7_S7_EENS_4lessIS7_EELb1EEEE6secondEv(%"class.std::__2::__compressed_pair.1"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.1"*, align 4
  store %"class.std::__2::__compressed_pair.1"* %this, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.1"*, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.1"* %this1 to %"struct.std::__2::__compressed_pair_elem.3"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::__map_value_compare"* @_ZNKSt3__222__compressed_pair_elemINS_19__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS7_S7_EENS_4lessIS7_EELb1EEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.3"* %0) #8
  ret %"class.std::__2::__map_value_compare"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::__map_value_compare"* @_ZNKSt3__222__compressed_pair_elemINS_19__map_value_compareINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12__value_typeIS7_S7_EENS_4lessIS7_EELb1EEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.3"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.3"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.3"* %this, %"struct.std::__2::__compressed_pair_elem.3"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.3"*, %"struct.std::__2::__compressed_pair_elem.3"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.3"* %this1 to %"class.std::__2::__map_value_compare"*
  ret %"class.std::__2::__map_value_compare"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::pair"* @_ZNSt3__214pointer_traitsIPKNS_4pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEE10pointer_toERSA_(%"struct.std::__2::pair"* nonnull align 4 dereferenceable(24) %__r) #0 comdat {
entry:
  %__r.addr = alloca %"struct.std::__2::pair"*, align 4
  store %"struct.std::__2::pair"* %__r, %"struct.std::__2::pair"** %__r.addr, align 4
  %0 = load %"struct.std::__2::pair"*, %"struct.std::__2::pair"** %__r.addr, align 4
  %call = call %"struct.std::__2::pair"* @_ZNSt3__29addressofIKNS_4pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEEEPT_RSB_(%"struct.std::__2::pair"* nonnull align 4 dereferenceable(24) %0) #8
  ret %"struct.std::__2::pair"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::pair"* @_ZNSt3__29addressofIKNS_4pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEEEPT_RSB_(%"struct.std::__2::pair"* nonnull align 4 dereferenceable(24) %__x) #0 comdat {
entry:
  %__x.addr = alloca %"struct.std::__2::pair"*, align 4
  store %"struct.std::__2::pair"* %__x, %"struct.std::__2::pair"** %__x.addr, align 4
  %0 = load %"struct.std::__2::pair"*, %"struct.std::__2::pair"** %__x.addr, align 4
  ret %"struct.std::__2::pair"* %0
}

declare void @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEPKcm(%"class.std::__2::basic_string"*, i8*, i32) #1

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__211char_traitsIcE6lengthEPKc(i8* %__s) #0 comdat {
entry:
  %__s.addr = alloca i8*, align 4
  store i8* %__s, i8** %__s.addr, align 4
  %0 = load i8*, i8** %__s.addr, align 4
  %call = call i32 @strlen(i8* %0) #8
  ret i32 %call
}

; Function Attrs: nounwind
declare i32 @strlen(i8*) #2

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind willreturn }
attributes #4 = { noinline noreturn nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { noreturn "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nobuiltin allocsize(0) "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #8 = { nounwind }
attributes #9 = { noreturn }
attributes #10 = { builtin allocsize(0) }
attributes #11 = { builtin nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
