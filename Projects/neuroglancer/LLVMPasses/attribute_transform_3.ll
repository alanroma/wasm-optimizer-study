; ModuleID = './draco/src/draco/attributes/attribute_transform.cc'
source_filename = "./draco/src/draco/attributes/attribute_transform.cc"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%"class.draco::IndexType" = type { i32 }
%"class.draco::AttributeTransform" = type { i32 (...)** }
%"class.draco::PointAttribute" = type <{ %"class.draco::GeometryAttribute", %"class.std::__2::unique_ptr", %"class.draco::IndexTypeVector", i32, i8, [3 x i8], %"class.std::__2::unique_ptr.11", [4 x i8] }>
%"class.draco::GeometryAttribute" = type { %"class.draco::DataBuffer"*, %"struct.draco::DataBufferDescriptor", i8, i32, i8, i64, i64, i32, i32 }
%"class.draco::DataBuffer" = type { %"class.std::__2::vector", %"struct.draco::DataBufferDescriptor" }
%"class.std::__2::vector" = type { %"class.std::__2::__vector_base" }
%"class.std::__2::__vector_base" = type { i8*, i8*, %"class.std::__2::__compressed_pair" }
%"class.std::__2::__compressed_pair" = type { %"struct.std::__2::__compressed_pair_elem" }
%"struct.std::__2::__compressed_pair_elem" = type { i8* }
%"struct.draco::DataBufferDescriptor" = type { i64, i64 }
%"class.std::__2::unique_ptr" = type { %"class.std::__2::__compressed_pair.1" }
%"class.std::__2::__compressed_pair.1" = type { %"struct.std::__2::__compressed_pair_elem.2" }
%"struct.std::__2::__compressed_pair_elem.2" = type { %"class.draco::DataBuffer"* }
%"class.draco::IndexTypeVector" = type { %"class.std::__2::vector.4" }
%"class.std::__2::vector.4" = type { %"class.std::__2::__vector_base.5" }
%"class.std::__2::__vector_base.5" = type { %"class.draco::IndexType"*, %"class.draco::IndexType"*, %"class.std::__2::__compressed_pair.6" }
%"class.std::__2::__compressed_pair.6" = type { %"struct.std::__2::__compressed_pair_elem.7" }
%"struct.std::__2::__compressed_pair_elem.7" = type { %"class.draco::IndexType"* }
%"class.std::__2::unique_ptr.11" = type { %"class.std::__2::__compressed_pair.12" }
%"class.std::__2::__compressed_pair.12" = type { %"struct.std::__2::__compressed_pair_elem.13" }
%"struct.std::__2::__compressed_pair_elem.13" = type { %"class.draco::AttributeTransformData"* }
%"class.draco::AttributeTransformData" = type { i32, %"class.draco::DataBuffer" }
%"struct.std::__2::__default_init_tag" = type { i8 }
%"struct.std::__2::default_delete.15" = type { i8 }
%"class.std::__2::unique_ptr.16" = type { %"class.std::__2::__compressed_pair.17" }
%"class.std::__2::__compressed_pair.17" = type { %"struct.std::__2::__compressed_pair_elem.18" }
%"struct.std::__2::__compressed_pair_elem.18" = type { %"class.draco::PointAttribute"* }
%"struct.std::__2::__compressed_pair_elem.14" = type { i8 }
%"class.std::__2::allocator" = type { i8 }
%"struct.std::__2::integral_constant" = type { i8 }
%"struct.std::__2::__has_destroy" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.0" = type { i8 }
%"class.std::__2::allocator.9" = type { i8 }
%"struct.std::__2::__split_buffer" = type { %"class.draco::IndexType"*, %"class.draco::IndexType"*, %"class.draco::IndexType"*, %"class.std::__2::__compressed_pair.21" }
%"class.std::__2::__compressed_pair.21" = type { %"struct.std::__2::__compressed_pair_elem.7", %"struct.std::__2::__compressed_pair_elem.22" }
%"struct.std::__2::__compressed_pair_elem.22" = type { %"class.std::__2::allocator.9"* }
%"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction" = type { %"class.std::__2::vector.4"*, %"class.draco::IndexType"*, %"class.draco::IndexType"* }
%"class.std::__2::__vector_base_common" = type { i8 }
%"class.std::__2::__split_buffer_common" = type { i8 }
%"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction" = type { %"class.draco::IndexType"*, %"class.draco::IndexType"*, %"class.draco::IndexType"** }
%"struct.std::__2::__has_construct" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.8" = type { i8 }
%"struct.std::__2::__less" = type { i8 }
%"struct.std::__2::__has_max_size" = type { i8 }
%"struct.std::__2::integral_constant.23" = type { i8 }
%"struct.std::__2::__has_destroy.24" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.19" = type { i8 }
%"struct.std::__2::default_delete.20" = type { i8 }
%"struct.std::__2::default_delete" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.3" = type { i8 }

$_ZN5draco22AttributeTransformDataC2Ev = comdat any

$_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEEC2ILb1EvEEPS2_ = comdat any

$_ZNKSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE3getEv = comdat any

$_ZN5draco14PointAttribute25SetAttributeTransformDataENSt3__210unique_ptrINS_22AttributeTransformDataENS1_14default_deleteIS3_EEEE = comdat any

$_ZNSt3__24moveIRNS_10unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOS9_ = comdat any

$_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEEC2EOS5_ = comdat any

$_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEED2Ev = comdat any

$_ZNK5draco17GeometryAttribute14attribute_typeEv = comdat any

$_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEEC2ILb1EvEEPS2_ = comdat any

$_ZNKSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEEptEv = comdat any

$_ZN5draco14PointAttribute18SetExplicitMappingEm = comdat any

$_ZN5draco14PointAttribute18SetIdentityMappingEv = comdat any

$_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEED2Ev = comdat any

$_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEEaSEOS5_ = comdat any

$_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE5resetEPS2_ = comdat any

$_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE7releaseEv = comdat any

$_ZNSt3__27forwardINS_14default_deleteIN5draco22AttributeTransformDataEEEEEOT_RNS_16remove_referenceIS5_E4typeE = comdat any

$_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE11get_deleterEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE6secondEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNKSt3__214default_deleteIN5draco22AttributeTransformDataEEclEPS2_ = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco22AttributeTransformDataELi0ELb0EE5__getEv = comdat any

$_ZN5draco22AttributeTransformDataD2Ev = comdat any

$_ZN5draco10DataBufferD2Ev = comdat any

$_ZNSt3__26vectorIhNS_9allocatorIhEEED2Ev = comdat any

$_ZNKSt3__26vectorIhNS_9allocatorIhEEE17__annotate_deleteEv = comdat any

$_ZNSt3__213__vector_baseIhNS_9allocatorIhEEED2Ev = comdat any

$_ZNKSt3__26vectorIhNS_9allocatorIhEEE31__annotate_contiguous_containerEPKvS5_S5_S5_ = comdat any

$_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv = comdat any

$_ZNKSt3__26vectorIhNS_9allocatorIhEEE8capacityEv = comdat any

$_ZNKSt3__26vectorIhNS_9allocatorIhEEE4sizeEv = comdat any

$_ZNSt3__212__to_addressIhEEPT_S2_ = comdat any

$_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPhNS_9allocatorIhEEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPhLi0ELb0EE5__getEv = comdat any

$_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE5clearEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIhEEE10deallocateERS2_Phm = comdat any

$_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE7__allocEv = comdat any

$_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE17__destruct_at_endEPh = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIhEEE7destroyIhEEvRS2_PT_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIhEEE9__destroyIhEEvNS_17integral_constantIbLb1EEERS2_PT_ = comdat any

$_ZNSt3__29allocatorIhE7destroyEPh = comdat any

$_ZNSt3__29allocatorIhE10deallocateEPhm = comdat any

$_ZNSt3__219__libcpp_deallocateEPvmm = comdat any

$_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm = comdat any

$_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm = comdat any

$_ZNSt3__217_DeallocateCaller9__do_callEPv = comdat any

$_ZNSt3__217__compressed_pairIPhNS_9allocatorIhEEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIhEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco22AttributeTransformDataEEELi1ELb1EE5__getEv = comdat any

$_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEEE6resizeEmRKS5_ = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE6resizeEmRKS4_ = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8__appendEmRKS4_ = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_ = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE18__construct_at_endEmRKS4_ = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE11__recommendEm = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEEC2EmmS7_ = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE18__construct_at_endEmRKS4_ = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS4_RS6_EE = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEED2Ev = comdat any

$_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionC2ERS7_m = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE9constructIS5_JRKS5_EEEvRS6_PT_DpOT0_ = comdat any

$_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEPT_S6_ = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionD2Ev = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE11__constructIS5_JRKS5_EEEvNS_17integral_constantIbLb1EEERS6_PT_DpOT0_ = comdat any

$_ZNSt3__27forwardIRKN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEOT_RNS_16remove_referenceIS7_E4typeE = comdat any

$_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE9constructIS4_JRKS4_EEEvPT_DpOT0_ = comdat any

$_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb1EE5__getEv = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8max_sizeEv = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv = comdat any

$_ZNSt3__23maxImEERKT_S3_S3_ = comdat any

$_ZNSt3__23minImEERKT_S3_S3_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE8max_sizeERKS6_ = comdat any

$_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv = comdat any

$_ZNSt3__214numeric_limitsIlE3maxEv = comdat any

$_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_ = comdat any

$_ZNKSt3__26__lessImmEclERKmS3_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS6_ = comdat any

$_ZNKSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE8max_sizeEv = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv = comdat any

$_ZNKSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv = comdat any

$_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__23maxImNS_6__lessImmEEEERKT_S5_S5_T0_ = comdat any

$_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEEC2IDnS8_EEOT_OT0_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE8allocateERS6_m = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE7__allocEv = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE9__end_capEv = comdat any

$_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELi0ELb0EEC2IDnvEEOT_ = comdat any

$_ZNSt3__27forwardIRNS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEEEOT_RNS_16remove_referenceIS8_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb0EEC2IS7_vEEOT_ = comdat any

$_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE8allocateEmPKv = comdat any

$_ZNSt3__220__throw_length_errorEPKc = comdat any

$_ZNSt3__217__libcpp_allocateEmm = comdat any

$_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb0EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE5firstEv = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE21_ConstructTransactionC2EPPS4_m = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE21_ConstructTransactionD2Ev = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_deleteEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE46__construct_backward_with_exception_guaranteesIS5_EENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS6_PT_SC_EE5valuesr31is_trivially_move_constructibleISC_EE5valueEvE4typeERS6_SD_SD_RSD_ = comdat any

$_ZNSt3__24swapIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS7_EE5valueEvE4typeERS7_SA_ = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE14__annotate_newEm = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE26__invalidate_all_iteratorsEv = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_ = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv = comdat any

$_ZNSt3__24moveIRPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEONS_16remove_referenceIT_E4typeEOS8_ = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE5clearEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE10deallocateERS6_PS5_m = comdat any

$_ZNKSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE8capacityEv = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE17__destruct_at_endEPS4_ = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE17__destruct_at_endEPS4_NS_17integral_constantIbLb0EEE = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE7destroyIS5_EEvRS6_PT_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE9__destroyIS5_EEvNS_17integral_constantIbLb1EEERS6_PT_ = comdat any

$_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE7destroyEPS4_ = comdat any

$_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE10deallocateEPS4_m = comdat any

$_ZNKSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE5firstEv = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE27__invalidate_iterators_pastEPS4_ = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_ = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_shrinkEm = comdat any

$_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEEE5clearEv = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5clearEv = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5clearEv = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPN5draco22AttributeTransformDataELi0ELb0EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEEC2IRS3_NS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__27forwardIRPN5draco22AttributeTransformDataEEEOT_RNS_16remove_referenceIS5_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco22AttributeTransformDataELi0ELb0EEC2IRS3_vEEOT_ = comdat any

$_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco22AttributeTransformDataEEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEEC2IS3_S5_EEOT_OT0_ = comdat any

$_ZNSt3__27forwardIPN5draco22AttributeTransformDataEEEOT_RNS_16remove_referenceIS4_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco22AttributeTransformDataELi0ELb0EEC2IS3_vEEOT_ = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco22AttributeTransformDataEEELi1ELb1EEC2IS4_vEEOT_ = comdat any

$_ZNSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEEC2IRS3_NS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__27forwardIRPN5draco14PointAttributeEEEOT_RNS_16remove_referenceIS5_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco14PointAttributeELi0ELb0EEC2IRS3_vEEOT_ = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco14PointAttributeEEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEE5resetEPS2_ = comdat any

$_ZNSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE6secondEv = comdat any

$_ZNKSt3__214default_deleteIN5draco14PointAttributeEEclEPS2_ = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco14PointAttributeELi0ELb0EE5__getEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco14PointAttributeEEELi1ELb1EE5__getEv = comdat any

$_ZN5draco14PointAttributeD2Ev = comdat any

$_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEEED2Ev = comdat any

$_ZNSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEED2Ev = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEED2Ev = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEED2Ev = comdat any

$_ZNSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEE5resetEPS2_ = comdat any

$_ZNSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEE6secondEv = comdat any

$_ZNKSt3__214default_deleteIN5draco10DataBufferEEclEPS2_ = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco10DataBufferELi0ELb0EE5__getEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco10DataBufferEEELi1ELb1EE5__getEv = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPN5draco14PointAttributeELi0ELb0EE5__getEv = comdat any

@_ZN5dracoL27kInvalidAttributeValueIndexE = internal constant %"class.draco::IndexType" { i32 -1 }, align 4
@.str = private unnamed_addr constant [68 x i8] c"allocator<T>::allocate(size_t n) 'n' exceeds maximum supported size\00", align 1

; Function Attrs: noinline nounwind optnone
define hidden zeroext i1 @_ZNK5draco18AttributeTransform19TransferToAttributeEPNS_14PointAttributeE(%"class.draco::AttributeTransform"* %this, %"class.draco::PointAttribute"* %attribute) #0 {
entry:
  %this.addr = alloca %"class.draco::AttributeTransform"*, align 4
  %attribute.addr = alloca %"class.draco::PointAttribute"*, align 4
  %transform_data = alloca %"class.std::__2::unique_ptr.11", align 4
  %agg.tmp = alloca %"class.std::__2::unique_ptr.11", align 4
  store %"class.draco::AttributeTransform"* %this, %"class.draco::AttributeTransform"** %this.addr, align 4
  store %"class.draco::PointAttribute"* %attribute, %"class.draco::PointAttribute"** %attribute.addr, align 4
  %this1 = load %"class.draco::AttributeTransform"*, %"class.draco::AttributeTransform"** %this.addr, align 4
  %call = call noalias nonnull i8* @_Znwm(i32 40) #7
  %0 = bitcast i8* %call to %"class.draco::AttributeTransformData"*
  %call2 = call %"class.draco::AttributeTransformData"* @_ZN5draco22AttributeTransformDataC2Ev(%"class.draco::AttributeTransformData"* %0)
  %call3 = call %"class.std::__2::unique_ptr.11"* @_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEEC2ILb1EvEEPS2_(%"class.std::__2::unique_ptr.11"* %transform_data, %"class.draco::AttributeTransformData"* %0) #8
  %call4 = call %"class.draco::AttributeTransformData"* @_ZNKSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE3getEv(%"class.std::__2::unique_ptr.11"* %transform_data) #8
  %1 = bitcast %"class.draco::AttributeTransform"* %this1 to void (%"class.draco::AttributeTransform"*, %"class.draco::AttributeTransformData"*)***
  %vtable = load void (%"class.draco::AttributeTransform"*, %"class.draco::AttributeTransformData"*)**, void (%"class.draco::AttributeTransform"*, %"class.draco::AttributeTransformData"*)*** %1, align 4
  %vfn = getelementptr inbounds void (%"class.draco::AttributeTransform"*, %"class.draco::AttributeTransformData"*)*, void (%"class.draco::AttributeTransform"*, %"class.draco::AttributeTransformData"*)** %vtable, i64 4
  %2 = load void (%"class.draco::AttributeTransform"*, %"class.draco::AttributeTransformData"*)*, void (%"class.draco::AttributeTransform"*, %"class.draco::AttributeTransformData"*)** %vfn, align 4
  call void %2(%"class.draco::AttributeTransform"* %this1, %"class.draco::AttributeTransformData"* %call4)
  %3 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %attribute.addr, align 4
  %call5 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.11"* @_ZNSt3__24moveIRNS_10unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOS9_(%"class.std::__2::unique_ptr.11"* nonnull align 4 dereferenceable(4) %transform_data) #8
  %call6 = call %"class.std::__2::unique_ptr.11"* @_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEEC2EOS5_(%"class.std::__2::unique_ptr.11"* %agg.tmp, %"class.std::__2::unique_ptr.11"* nonnull align 4 dereferenceable(4) %call5) #8
  call void @_ZN5draco14PointAttribute25SetAttributeTransformDataENSt3__210unique_ptrINS_22AttributeTransformDataENS1_14default_deleteIS3_EEEE(%"class.draco::PointAttribute"* %3, %"class.std::__2::unique_ptr.11"* %agg.tmp)
  %call7 = call %"class.std::__2::unique_ptr.11"* @_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.11"* %agg.tmp) #8
  %call8 = call %"class.std::__2::unique_ptr.11"* @_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.11"* %transform_data) #8
  ret i1 true
}

; Function Attrs: nobuiltin allocsize(0)
declare nonnull i8* @_Znwm(i32) #1

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::AttributeTransformData"* @_ZN5draco22AttributeTransformDataC2Ev(%"class.draco::AttributeTransformData"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::AttributeTransformData"*, align 4
  store %"class.draco::AttributeTransformData"* %this, %"class.draco::AttributeTransformData"** %this.addr, align 4
  %this1 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %this.addr, align 4
  %transform_type_ = getelementptr inbounds %"class.draco::AttributeTransformData", %"class.draco::AttributeTransformData"* %this1, i32 0, i32 0
  store i32 -1, i32* %transform_type_, align 8
  %buffer_ = getelementptr inbounds %"class.draco::AttributeTransformData", %"class.draco::AttributeTransformData"* %this1, i32 0, i32 1
  %call = call %"class.draco::DataBuffer"* @_ZN5draco10DataBufferC1Ev(%"class.draco::DataBuffer"* %buffer_)
  ret %"class.draco::AttributeTransformData"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.11"* @_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEEC2ILb1EvEEPS2_(%"class.std::__2::unique_ptr.11"* returned %this, %"class.draco::AttributeTransformData"* %__p) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.11"*, align 4
  %__p.addr = alloca %"class.draco::AttributeTransformData"*, align 4
  %ref.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::unique_ptr.11"* %this, %"class.std::__2::unique_ptr.11"** %this.addr, align 4
  store %"class.draco::AttributeTransformData"* %__p, %"class.draco::AttributeTransformData"** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.11"*, %"class.std::__2::unique_ptr.11"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.11", %"class.std::__2::unique_ptr.11"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::__compressed_pair.12"* @_ZNSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEEC2IRS3_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.12"* %__ptr_, %"class.draco::AttributeTransformData"** nonnull align 4 dereferenceable(4) %__p.addr, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp)
  ret %"class.std::__2::unique_ptr.11"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::AttributeTransformData"* @_ZNKSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE3getEv(%"class.std::__2::unique_ptr.11"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.11"*, align 4
  store %"class.std::__2::unique_ptr.11"* %this, %"class.std::__2::unique_ptr.11"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.11"*, %"class.std::__2::unique_ptr.11"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.11", %"class.std::__2::unique_ptr.11"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributeTransformData"** @_ZNKSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.12"* %__ptr_) #8
  %0 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %call, align 4
  ret %"class.draco::AttributeTransformData"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco14PointAttribute25SetAttributeTransformDataENSt3__210unique_ptrINS_22AttributeTransformDataENS1_14default_deleteIS3_EEEE(%"class.draco::PointAttribute"* %this, %"class.std::__2::unique_ptr.11"* %transform_data) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointAttribute"*, align 4
  store %"class.draco::PointAttribute"* %this, %"class.draco::PointAttribute"** %this.addr, align 4
  %this1 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.11"* @_ZNSt3__24moveIRNS_10unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOS9_(%"class.std::__2::unique_ptr.11"* nonnull align 4 dereferenceable(4) %transform_data) #8
  %attribute_transform_data_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 6
  %call2 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.11"* @_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEEaSEOS5_(%"class.std::__2::unique_ptr.11"* %attribute_transform_data_, %"class.std::__2::unique_ptr.11"* nonnull align 4 dereferenceable(4) %call) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.11"* @_ZNSt3__24moveIRNS_10unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOS9_(%"class.std::__2::unique_ptr.11"* nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::unique_ptr.11"*, align 4
  store %"class.std::__2::unique_ptr.11"* %__t, %"class.std::__2::unique_ptr.11"** %__t.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.11"*, %"class.std::__2::unique_ptr.11"** %__t.addr, align 4
  ret %"class.std::__2::unique_ptr.11"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.11"* @_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEEC2EOS5_(%"class.std::__2::unique_ptr.11"* returned %this, %"class.std::__2::unique_ptr.11"* nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.11"*, align 4
  %__u.addr = alloca %"class.std::__2::unique_ptr.11"*, align 4
  %ref.tmp = alloca %"class.draco::AttributeTransformData"*, align 4
  store %"class.std::__2::unique_ptr.11"* %this, %"class.std::__2::unique_ptr.11"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.11"* %__u, %"class.std::__2::unique_ptr.11"** %__u.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.11"*, %"class.std::__2::unique_ptr.11"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.11", %"class.std::__2::unique_ptr.11"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::unique_ptr.11"*, %"class.std::__2::unique_ptr.11"** %__u.addr, align 4
  %call = call %"class.draco::AttributeTransformData"* @_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE7releaseEv(%"class.std::__2::unique_ptr.11"* %0) #8
  store %"class.draco::AttributeTransformData"* %call, %"class.draco::AttributeTransformData"** %ref.tmp, align 4
  %1 = load %"class.std::__2::unique_ptr.11"*, %"class.std::__2::unique_ptr.11"** %__u.addr, align 4
  %call2 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.15"* @_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE11get_deleterEv(%"class.std::__2::unique_ptr.11"* %1) #8
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.15"* @_ZNSt3__27forwardINS_14default_deleteIN5draco22AttributeTransformDataEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.15"* nonnull align 1 dereferenceable(1) %call2) #8
  %call4 = call %"class.std::__2::__compressed_pair.12"* @_ZNSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEEC2IS3_S5_EEOT_OT0_(%"class.std::__2::__compressed_pair.12"* %__ptr_, %"class.draco::AttributeTransformData"** nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::default_delete.15"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::unique_ptr.11"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.11"* @_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.11"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.11"*, align 4
  store %"class.std::__2::unique_ptr.11"* %this, %"class.std::__2::unique_ptr.11"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.11"*, %"class.std::__2::unique_ptr.11"** %this.addr, align 4
  call void @_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.11"* %this1, %"class.draco::AttributeTransformData"* null) #8
  ret %"class.std::__2::unique_ptr.11"* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZNK5draco18AttributeTransform21InitPortableAttributeEiiiRKNS_14PointAttributeEb(%"class.std::__2::unique_ptr.16"* noalias sret align 4 %agg.result, %"class.draco::AttributeTransform"* %this, i32 %num_entries, i32 %num_components, i32 %num_points, %"class.draco::PointAttribute"* nonnull align 8 dereferenceable(92) %attribute, i1 zeroext %is_unsigned) #0 {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %"class.draco::AttributeTransform"*, align 4
  %num_entries.addr = alloca i32, align 4
  %num_components.addr = alloca i32, align 4
  %num_points.addr = alloca i32, align 4
  %attribute.addr = alloca %"class.draco::PointAttribute"*, align 4
  %is_unsigned.addr = alloca i8, align 1
  %dt = alloca i32, align 4
  %va = alloca %"class.draco::GeometryAttribute", align 8
  %nrvo = alloca i1, align 1
  %0 = bitcast %"class.std::__2::unique_ptr.16"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %"class.draco::AttributeTransform"* %this, %"class.draco::AttributeTransform"** %this.addr, align 4
  store i32 %num_entries, i32* %num_entries.addr, align 4
  store i32 %num_components, i32* %num_components.addr, align 4
  store i32 %num_points, i32* %num_points.addr, align 4
  store %"class.draco::PointAttribute"* %attribute, %"class.draco::PointAttribute"** %attribute.addr, align 4
  %frombool = zext i1 %is_unsigned to i8
  store i8 %frombool, i8* %is_unsigned.addr, align 1
  %this1 = load %"class.draco::AttributeTransform"*, %"class.draco::AttributeTransform"** %this.addr, align 4
  %1 = load i8, i8* %is_unsigned.addr, align 1
  %tobool = trunc i8 %1 to i1
  %2 = zext i1 %tobool to i64
  %cond = select i1 %tobool, i32 6, i32 5
  store i32 %cond, i32* %dt, align 4
  %call = call %"class.draco::GeometryAttribute"* @_ZN5draco17GeometryAttributeC1Ev(%"class.draco::GeometryAttribute"* %va)
  %3 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %attribute.addr, align 4
  %4 = bitcast %"class.draco::PointAttribute"* %3 to %"class.draco::GeometryAttribute"*
  %call2 = call i32 @_ZNK5draco17GeometryAttribute14attribute_typeEv(%"class.draco::GeometryAttribute"* %4)
  %5 = load i32, i32* %num_components.addr, align 4
  %conv = trunc i32 %5 to i8
  %6 = load i32, i32* %dt, align 4
  %7 = load i32, i32* %num_components.addr, align 4
  %8 = load i32, i32* %dt, align 4
  %call3 = call i32 @_ZN5draco14DataTypeLengthENS_8DataTypeE(i32 %8)
  %mul = mul nsw i32 %7, %call3
  %conv4 = sext i32 %mul to i64
  call void @_ZN5draco17GeometryAttribute4InitENS0_4TypeEPNS_10DataBufferEaNS_8DataTypeEbxx(%"class.draco::GeometryAttribute"* %va, i32 %call2, %"class.draco::DataBuffer"* null, i8 signext %conv, i32 %6, i1 zeroext false, i64 %conv4, i64 0)
  store i1 false, i1* %nrvo, align 1
  %call5 = call noalias nonnull i8* @_Znwm(i32 96) #7
  %9 = bitcast i8* %call5 to %"class.draco::PointAttribute"*
  %call6 = call %"class.draco::PointAttribute"* @_ZN5draco14PointAttributeC1ERKNS_17GeometryAttributeE(%"class.draco::PointAttribute"* %9, %"class.draco::GeometryAttribute"* nonnull align 8 dereferenceable(64) %va)
  %call7 = call %"class.std::__2::unique_ptr.16"* @_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEEC2ILb1EvEEPS2_(%"class.std::__2::unique_ptr.16"* %agg.result, %"class.draco::PointAttribute"* %9) #8
  %call8 = call %"class.draco::PointAttribute"* @_ZNKSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr.16"* %agg.result) #8
  %10 = load i32, i32* %num_entries.addr, align 4
  %call9 = call zeroext i1 @_ZN5draco14PointAttribute5ResetEm(%"class.draco::PointAttribute"* %call8, i32 %10)
  %11 = load i32, i32* %num_points.addr, align 4
  %tobool10 = icmp ne i32 %11, 0
  br i1 %tobool10, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %call11 = call %"class.draco::PointAttribute"* @_ZNKSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr.16"* %agg.result) #8
  %12 = load i32, i32* %num_points.addr, align 4
  call void @_ZN5draco14PointAttribute18SetExplicitMappingEm(%"class.draco::PointAttribute"* %call11, i32 %12)
  br label %if.end

if.else:                                          ; preds = %entry
  %call12 = call %"class.draco::PointAttribute"* @_ZNKSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr.16"* %agg.result) #8
  call void @_ZN5draco14PointAttribute18SetIdentityMappingEv(%"class.draco::PointAttribute"* %call12)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  store i1 true, i1* %nrvo, align 1
  %nrvo.val = load i1, i1* %nrvo, align 1
  br i1 %nrvo.val, label %nrvo.skipdtor, label %nrvo.unused

nrvo.unused:                                      ; preds = %if.end
  %call13 = call %"class.std::__2::unique_ptr.16"* @_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.16"* %agg.result) #8
  br label %nrvo.skipdtor

nrvo.skipdtor:                                    ; preds = %nrvo.unused, %if.end
  ret void
}

declare %"class.draco::GeometryAttribute"* @_ZN5draco17GeometryAttributeC1Ev(%"class.draco::GeometryAttribute"* returned) unnamed_addr #2

declare void @_ZN5draco17GeometryAttribute4InitENS0_4TypeEPNS_10DataBufferEaNS_8DataTypeEbxx(%"class.draco::GeometryAttribute"*, i32, %"class.draco::DataBuffer"*, i8 signext, i32, i1 zeroext, i64, i64) #2

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco17GeometryAttribute14attribute_typeEv(%"class.draco::GeometryAttribute"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::GeometryAttribute"*, align 4
  store %"class.draco::GeometryAttribute"* %this, %"class.draco::GeometryAttribute"** %this.addr, align 4
  %this1 = load %"class.draco::GeometryAttribute"*, %"class.draco::GeometryAttribute"** %this.addr, align 4
  %attribute_type_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 7
  %0 = load i32, i32* %attribute_type_, align 8
  ret i32 %0
}

declare i32 @_ZN5draco14DataTypeLengthENS_8DataTypeE(i32) #2

declare %"class.draco::PointAttribute"* @_ZN5draco14PointAttributeC1ERKNS_17GeometryAttributeE(%"class.draco::PointAttribute"* returned, %"class.draco::GeometryAttribute"* nonnull align 8 dereferenceable(64)) unnamed_addr #2

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.16"* @_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEEC2ILb1EvEEPS2_(%"class.std::__2::unique_ptr.16"* returned %this, %"class.draco::PointAttribute"* %__p) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.16"*, align 4
  %__p.addr = alloca %"class.draco::PointAttribute"*, align 4
  %ref.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::unique_ptr.16"* %this, %"class.std::__2::unique_ptr.16"** %this.addr, align 4
  store %"class.draco::PointAttribute"* %__p, %"class.draco::PointAttribute"** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.16"*, %"class.std::__2::unique_ptr.16"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.16", %"class.std::__2::unique_ptr.16"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::__compressed_pair.17"* @_ZNSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEEC2IRS3_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.17"* %__ptr_, %"class.draco::PointAttribute"** nonnull align 4 dereferenceable(4) %__p.addr, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp)
  ret %"class.std::__2::unique_ptr.16"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::PointAttribute"* @_ZNKSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr.16"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.16"*, align 4
  store %"class.std::__2::unique_ptr.16"* %this, %"class.std::__2::unique_ptr.16"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.16"*, %"class.std::__2::unique_ptr.16"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.16", %"class.std::__2::unique_ptr.16"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNKSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.17"* %__ptr_) #8
  %0 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %call, align 4
  ret %"class.draco::PointAttribute"* %0
}

declare zeroext i1 @_ZN5draco14PointAttribute5ResetEm(%"class.draco::PointAttribute"*, i32) #2

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco14PointAttribute18SetExplicitMappingEm(%"class.draco::PointAttribute"* %this, i32 %num_points) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointAttribute"*, align 4
  %num_points.addr = alloca i32, align 4
  store %"class.draco::PointAttribute"* %this, %"class.draco::PointAttribute"** %this.addr, align 4
  store i32 %num_points, i32* %num_points.addr, align 4
  %this1 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %this.addr, align 4
  %identity_mapping_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 4
  store i8 0, i8* %identity_mapping_, align 4
  %indices_map_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 2
  %0 = load i32, i32* %num_points.addr, align 4
  call void @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEEE6resizeEmRKS5_(%"class.draco::IndexTypeVector"* %indices_map_, i32 %0, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) @_ZN5dracoL27kInvalidAttributeValueIndexE)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco14PointAttribute18SetIdentityMappingEv(%"class.draco::PointAttribute"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointAttribute"*, align 4
  store %"class.draco::PointAttribute"* %this, %"class.draco::PointAttribute"** %this.addr, align 4
  %this1 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %this.addr, align 4
  %identity_mapping_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 4
  store i8 1, i8* %identity_mapping_, align 4
  %indices_map_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 2
  call void @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEEE5clearEv(%"class.draco::IndexTypeVector"* %indices_map_)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.16"* @_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.16"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.16"*, align 4
  store %"class.std::__2::unique_ptr.16"* %this, %"class.std::__2::unique_ptr.16"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.16"*, %"class.std::__2::unique_ptr.16"** %this.addr, align 4
  call void @_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.16"* %this1, %"class.draco::PointAttribute"* null) #8
  ret %"class.std::__2::unique_ptr.16"* %this1
}

declare %"class.draco::DataBuffer"* @_ZN5draco10DataBufferC1Ev(%"class.draco::DataBuffer"* returned) unnamed_addr #2

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.11"* @_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEEaSEOS5_(%"class.std::__2::unique_ptr.11"* %this, %"class.std::__2::unique_ptr.11"* nonnull align 4 dereferenceable(4) %__u) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.11"*, align 4
  %__u.addr = alloca %"class.std::__2::unique_ptr.11"*, align 4
  store %"class.std::__2::unique_ptr.11"* %this, %"class.std::__2::unique_ptr.11"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.11"* %__u, %"class.std::__2::unique_ptr.11"** %__u.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.11"*, %"class.std::__2::unique_ptr.11"** %this.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.11"*, %"class.std::__2::unique_ptr.11"** %__u.addr, align 4
  %call = call %"class.draco::AttributeTransformData"* @_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE7releaseEv(%"class.std::__2::unique_ptr.11"* %0) #8
  call void @_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.11"* %this1, %"class.draco::AttributeTransformData"* %call) #8
  %1 = load %"class.std::__2::unique_ptr.11"*, %"class.std::__2::unique_ptr.11"** %__u.addr, align 4
  %call2 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.15"* @_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE11get_deleterEv(%"class.std::__2::unique_ptr.11"* %1) #8
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.15"* @_ZNSt3__27forwardINS_14default_deleteIN5draco22AttributeTransformDataEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.15"* nonnull align 1 dereferenceable(1) %call2) #8
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.11", %"class.std::__2::unique_ptr.11"* %this1, i32 0, i32 0
  %call4 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.15"* @_ZNSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.12"* %__ptr_) #8
  ret %"class.std::__2::unique_ptr.11"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.11"* %this, %"class.draco::AttributeTransformData"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.11"*, align 4
  %__p.addr = alloca %"class.draco::AttributeTransformData"*, align 4
  %__tmp = alloca %"class.draco::AttributeTransformData"*, align 4
  store %"class.std::__2::unique_ptr.11"* %this, %"class.std::__2::unique_ptr.11"** %this.addr, align 4
  store %"class.draco::AttributeTransformData"* %__p, %"class.draco::AttributeTransformData"** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.11"*, %"class.std::__2::unique_ptr.11"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.11", %"class.std::__2::unique_ptr.11"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributeTransformData"** @_ZNSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.12"* %__ptr_) #8
  %0 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %call, align 4
  store %"class.draco::AttributeTransformData"* %0, %"class.draco::AttributeTransformData"** %__tmp, align 4
  %1 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %__p.addr, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr.11", %"class.std::__2::unique_ptr.11"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::AttributeTransformData"** @_ZNSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.12"* %__ptr_2) #8
  store %"class.draco::AttributeTransformData"* %1, %"class.draco::AttributeTransformData"** %call3, align 4
  %2 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %__tmp, align 4
  %tobool = icmp ne %"class.draco::AttributeTransformData"* %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__ptr_4 = getelementptr inbounds %"class.std::__2::unique_ptr.11", %"class.std::__2::unique_ptr.11"* %this1, i32 0, i32 0
  %call5 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.15"* @_ZNSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.12"* %__ptr_4) #8
  %3 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %__tmp, align 4
  call void @_ZNKSt3__214default_deleteIN5draco22AttributeTransformDataEEclEPS2_(%"struct.std::__2::default_delete.15"* %call5, %"class.draco::AttributeTransformData"* %3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::AttributeTransformData"* @_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE7releaseEv(%"class.std::__2::unique_ptr.11"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.11"*, align 4
  %__t = alloca %"class.draco::AttributeTransformData"*, align 4
  store %"class.std::__2::unique_ptr.11"* %this, %"class.std::__2::unique_ptr.11"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.11"*, %"class.std::__2::unique_ptr.11"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.11", %"class.std::__2::unique_ptr.11"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributeTransformData"** @_ZNSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.12"* %__ptr_) #8
  %0 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %call, align 4
  store %"class.draco::AttributeTransformData"* %0, %"class.draco::AttributeTransformData"** %__t, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr.11", %"class.std::__2::unique_ptr.11"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::AttributeTransformData"** @_ZNSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.12"* %__ptr_2) #8
  store %"class.draco::AttributeTransformData"* null, %"class.draco::AttributeTransformData"** %call3, align 4
  %1 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %__t, align 4
  ret %"class.draco::AttributeTransformData"* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.15"* @_ZNSt3__27forwardINS_14default_deleteIN5draco22AttributeTransformDataEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.15"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"struct.std::__2::default_delete.15"*, align 4
  store %"struct.std::__2::default_delete.15"* %__t, %"struct.std::__2::default_delete.15"** %__t.addr, align 4
  %0 = load %"struct.std::__2::default_delete.15"*, %"struct.std::__2::default_delete.15"** %__t.addr, align 4
  ret %"struct.std::__2::default_delete.15"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.15"* @_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE11get_deleterEv(%"class.std::__2::unique_ptr.11"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.11"*, align 4
  store %"class.std::__2::unique_ptr.11"* %this, %"class.std::__2::unique_ptr.11"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.11"*, %"class.std::__2::unique_ptr.11"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.11", %"class.std::__2::unique_ptr.11"* %this1, i32 0, i32 0
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.15"* @_ZNSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.12"* %__ptr_) #8
  ret %"struct.std::__2::default_delete.15"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.15"* @_ZNSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.12"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.12"*, align 4
  store %"class.std::__2::__compressed_pair.12"* %this, %"class.std::__2::__compressed_pair.12"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.12"*, %"class.std::__2::__compressed_pair.12"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.12"* %this1 to %"struct.std::__2::__compressed_pair_elem.14"*
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.15"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco22AttributeTransformDataEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.14"* %0) #8
  ret %"struct.std::__2::default_delete.15"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::AttributeTransformData"** @_ZNSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.12"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.12"*, align 4
  store %"class.std::__2::__compressed_pair.12"* %this, %"class.std::__2::__compressed_pair.12"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.12"*, %"class.std::__2::__compressed_pair.12"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.12"* %this1 to %"struct.std::__2::__compressed_pair_elem.13"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributeTransformData"** @_ZNSt3__222__compressed_pair_elemIPN5draco22AttributeTransformDataELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.13"* %0) #8
  ret %"class.draco::AttributeTransformData"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__214default_deleteIN5draco22AttributeTransformDataEEclEPS2_(%"struct.std::__2::default_delete.15"* %this, %"class.draco::AttributeTransformData"* %__ptr) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::default_delete.15"*, align 4
  %__ptr.addr = alloca %"class.draco::AttributeTransformData"*, align 4
  store %"struct.std::__2::default_delete.15"* %this, %"struct.std::__2::default_delete.15"** %this.addr, align 4
  store %"class.draco::AttributeTransformData"* %__ptr, %"class.draco::AttributeTransformData"** %__ptr.addr, align 4
  %this1 = load %"struct.std::__2::default_delete.15"*, %"struct.std::__2::default_delete.15"** %this.addr, align 4
  %0 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %__ptr.addr, align 4
  %isnull = icmp eq %"class.draco::AttributeTransformData"* %0, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %entry
  %call = call %"class.draco::AttributeTransformData"* @_ZN5draco22AttributeTransformDataD2Ev(%"class.draco::AttributeTransformData"* %0) #8
  %1 = bitcast %"class.draco::AttributeTransformData"* %0 to i8*
  call void @_ZdlPv(i8* %1) #9
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::AttributeTransformData"** @_ZNSt3__222__compressed_pair_elemIPN5draco22AttributeTransformDataELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.13"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.13"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.13"* %this, %"struct.std::__2::__compressed_pair_elem.13"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.13"*, %"struct.std::__2::__compressed_pair_elem.13"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.13", %"struct.std::__2::__compressed_pair_elem.13"* %this1, i32 0, i32 0
  ret %"class.draco::AttributeTransformData"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::AttributeTransformData"* @_ZN5draco22AttributeTransformDataD2Ev(%"class.draco::AttributeTransformData"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::AttributeTransformData"*, align 4
  store %"class.draco::AttributeTransformData"* %this, %"class.draco::AttributeTransformData"** %this.addr, align 4
  %this1 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %this.addr, align 4
  %buffer_ = getelementptr inbounds %"class.draco::AttributeTransformData", %"class.draco::AttributeTransformData"* %this1, i32 0, i32 1
  %call = call %"class.draco::DataBuffer"* @_ZN5draco10DataBufferD2Ev(%"class.draco::DataBuffer"* %buffer_) #8
  ret %"class.draco::AttributeTransformData"* %this1
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::DataBuffer"* @_ZN5draco10DataBufferD2Ev(%"class.draco::DataBuffer"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::DataBuffer"*, align 4
  store %"class.draco::DataBuffer"* %this, %"class.draco::DataBuffer"** %this.addr, align 4
  %this1 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %this.addr, align 4
  %data_ = getelementptr inbounds %"class.draco::DataBuffer", %"class.draco::DataBuffer"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::vector"* @_ZNSt3__26vectorIhNS_9allocatorIhEEED2Ev(%"class.std::__2::vector"* %data_) #8
  ret %"class.draco::DataBuffer"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector"* @_ZNSt3__26vectorIhNS_9allocatorIhEEED2Ev(%"class.std::__2::vector"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  call void @_ZNKSt3__26vectorIhNS_9allocatorIhEEE17__annotate_deleteEv(%"class.std::__2::vector"* %this1) #8
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call %"class.std::__2::__vector_base"* @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEED2Ev(%"class.std::__2::__vector_base"* %0) #8
  ret %"class.std::__2::vector"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIhNS_9allocatorIhEEE17__annotate_deleteEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %call = call i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector"* %this1) #8
  %call2 = call i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::vector"* %this1) #8
  %add.ptr = getelementptr inbounds i8, i8* %call2, i32 %call3
  %call4 = call i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector"* %this1) #8
  %call5 = call i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4sizeEv(%"class.std::__2::vector"* %this1) #8
  %add.ptr6 = getelementptr inbounds i8, i8* %call4, i32 %call5
  %call7 = call i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector"* %this1) #8
  %call8 = call i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::vector"* %this1) #8
  %add.ptr9 = getelementptr inbounds i8, i8* %call7, i32 %call8
  call void @_ZNKSt3__26vectorIhNS_9allocatorIhEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector"* %this1, i8* %call, i8* %add.ptr, i8* %add.ptr6, i8* %add.ptr9) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base"* @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEED2Ev(%"class.std::__2::__vector_base"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__vector_base"*, align 4
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  store %"class.std::__2::__vector_base"* %this1, %"class.std::__2::__vector_base"** %retval, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  %0 = load i8*, i8** %__begin_, align 4
  %cmp = icmp ne i8* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE5clearEv(%"class.std::__2::__vector_base"* %this1) #8
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE7__allocEv(%"class.std::__2::__vector_base"* %this1) #8
  %__begin_2 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  %1 = load i8*, i8** %__begin_2, align 4
  %call3 = call i32 @_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::__vector_base"* %this1) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE10deallocateERS2_Phm(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call, i8* %1, i32 %call3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %retval, align 4
  ret %"class.std::__2::__vector_base"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIhNS_9allocatorIhEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 0
  %1 = load i8*, i8** %__begin_, align 4
  %call = call i8* @_ZNSt3__212__to_addressIhEEPT_S2_(i8* %1) #8
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call i32 @_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::__vector_base"* %0) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4sizeEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 1
  %1 = load i8*, i8** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %2, i32 0, i32 0
  %3 = load i8*, i8** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint i8* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  ret i32 %sub.ptr.sub
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__212__to_addressIhEEPT_S2_(i8* %__p) #0 comdat {
entry:
  %__p.addr = alloca i8*, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %0 = load i8*, i8** %__p.addr, align 4
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE9__end_capEv(%"class.std::__2::__vector_base"* %this1) #8
  %0 = load i8*, i8** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  %1 = load i8*, i8** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint i8* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  ret i32 %sub.ptr.sub
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE9__end_capEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__217__compressed_pairIPhNS_9allocatorIhEEE5firstEv(%"class.std::__2::__compressed_pair"* %__end_cap_) #8
  ret i8** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__217__compressed_pairIPhNS_9allocatorIhEEE5firstEv(%"class.std::__2::__compressed_pair"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__222__compressed_pair_elemIPhLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %0) #8
  ret i8** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__222__compressed_pair_elemIPhLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem"*, align 4
  store %"struct.std::__2::__compressed_pair_elem"* %this, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem"*, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem"* %this1, i32 0, i32 0
  ret i8** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE5clearEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  %0 = load i8*, i8** %__begin_, align 4
  call void @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE17__destruct_at_endEPh(%"class.std::__2::__vector_base"* %this1, i8* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE10deallocateERS2_Phm(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, i8* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i8*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %1 = load i8*, i8** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorIhE10deallocateEPhm(%"class.std::__2::allocator"* %0, i8* %1, i32 %2) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE7__allocEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__217__compressed_pairIPhNS_9allocatorIhEEE6secondEv(%"class.std::__2::__compressed_pair"* %__end_cap_) #8
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE17__destruct_at_endEPh(%"class.std::__2::__vector_base"* %this, i8* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  %__new_last.addr = alloca i8*, align 4
  %__soon_to_be_end = alloca i8*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  store i8* %__new_last, i8** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 1
  %0 = load i8*, i8** %__end_, align 4
  store i8* %0, i8** %__soon_to_be_end, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load i8*, i8** %__new_last.addr, align 4
  %2 = load i8*, i8** %__soon_to_be_end, align 4
  %cmp = icmp ne i8* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE7__allocEv(%"class.std::__2::__vector_base"* %this1) #8
  %3 = load i8*, i8** %__soon_to_be_end, align 4
  %incdec.ptr = getelementptr inbounds i8, i8* %3, i32 -1
  store i8* %incdec.ptr, i8** %__soon_to_be_end, align 4
  %call2 = call i8* @_ZNSt3__212__to_addressIhEEPT_S2_(i8* %incdec.ptr) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE7destroyIhEEvRS2_PT_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call, i8* %call2)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %4 = load i8*, i8** %__new_last.addr, align 4
  %__end_3 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 1
  store i8* %4, i8** %__end_3, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE7destroyIhEEvRS2_PT_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, i8* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i8*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy", align 1
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %2 = load i8*, i8** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE9__destroyIhEEvNS_17integral_constantIbLb1EEERS2_PT_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %1, i8* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE9__destroyIhEEvNS_17integral_constantIbLb1EEERS2_PT_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, i8* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i8*, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %2 = load i8*, i8** %__p.addr, align 4
  call void @_ZNSt3__29allocatorIhE7destroyEPh(%"class.std::__2::allocator"* %1, i8* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIhE7destroyEPh(%"class.std::__2::allocator"* %this, i8* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i8*, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIhE10deallocateEPhm(%"class.std::__2::allocator"* %this, i8* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i8*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  %0 = load i8*, i8** %__p.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %1, 1
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %0, i32 %mul, i32 1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %__ptr, i32 %__size, i32 %__align) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  %1 = load i32, i32* %__size.addr, align 4
  %2 = load i32, i32* %__align.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm(i8* %0, i32 %1, i32 %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm(i8* %__ptr, i32 %__size, i32 %__align) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  %1 = load i32, i32* %__size.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm(i8* %0, i32 %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm(i8* %__ptr, i32 %__size) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller9__do_callEPv(i8* %0)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller9__do_callEPv(i8* %__ptr) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  call void @_ZdlPv(i8* %0) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__217__compressed_pairIPhNS_9allocatorIhEEE6secondEv(%"class.std::__2::__compressed_pair"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem.0"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIhEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.0"* %0) #8
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIhEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.0"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.0"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.0"* %this, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.0"*, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.0"* %this1 to %"class.std::__2::allocator"*
  ret %"class.std::__2::allocator"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.15"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco22AttributeTransformDataEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.14"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.14"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.14"* %this, %"struct.std::__2::__compressed_pair_elem.14"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.14"*, %"struct.std::__2::__compressed_pair_elem.14"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.14"* %this1 to %"struct.std::__2::default_delete.15"*
  ret %"struct.std::__2::default_delete.15"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEEE6resizeEmRKS5_(%"class.draco::IndexTypeVector"* %this, i32 %size, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %val) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector"*, align 4
  %size.addr = alloca i32, align 4
  %val.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.draco::IndexTypeVector"* %this, %"class.draco::IndexTypeVector"** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  store %"class.draco::IndexType"* %val, %"class.draco::IndexType"** %val.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector"*, %"class.draco::IndexTypeVector"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector", %"class.draco::IndexTypeVector"* %this1, i32 0, i32 0
  %0 = load i32, i32* %size.addr, align 4
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %val.addr, align 4
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE6resizeEmRKS4_(%"class.std::__2::vector.4"* %vector_, i32 %0, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE6resizeEmRKS4_(%"class.std::__2::vector.4"* %this, i32 %__sz, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %__x) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.4"*, align 4
  %__sz.addr = alloca i32, align 4
  %__x.addr = alloca %"class.draco::IndexType"*, align 4
  %__cs = alloca i32, align 4
  store %"class.std::__2::vector.4"* %this, %"class.std::__2::vector.4"** %this.addr, align 4
  store i32 %__sz, i32* %__sz.addr, align 4
  store %"class.draco::IndexType"* %__x, %"class.draco::IndexType"** %__x.addr, align 4
  %this1 = load %"class.std::__2::vector.4"*, %"class.std::__2::vector.4"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.4"* %this1) #8
  store i32 %call, i32* %__cs, align 4
  %0 = load i32, i32* %__cs, align 4
  %1 = load i32, i32* %__sz.addr, align 4
  %cmp = icmp ult i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %__sz.addr, align 4
  %3 = load i32, i32* %__cs, align 4
  %sub = sub i32 %2, %3
  %4 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__x.addr, align 4
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8__appendEmRKS4_(%"class.std::__2::vector.4"* %this1, i32 %sub, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %4)
  br label %if.end4

if.else:                                          ; preds = %entry
  %5 = load i32, i32* %__cs, align 4
  %6 = load i32, i32* %__sz.addr, align 4
  %cmp2 = icmp ugt i32 %5, %6
  br i1 %cmp2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.else
  %7 = bitcast %"class.std::__2::vector.4"* %this1 to %"class.std::__2::__vector_base.5"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.5", %"class.std::__2::__vector_base.5"* %7, i32 0, i32 0
  %8 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  %9 = load i32, i32* %__sz.addr, align 4
  %add.ptr = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %8, i32 %9
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_(%"class.std::__2::vector.4"* %this1, %"class.draco::IndexType"* %add.ptr) #8
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.else
  br label %if.end4

if.end4:                                          ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.4"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.4"*, align 4
  store %"class.std::__2::vector.4"* %this, %"class.std::__2::vector.4"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.4"*, %"class.std::__2::vector.4"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.4"* %this1 to %"class.std::__2::__vector_base.5"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.5", %"class.std::__2::__vector_base.5"* %0, i32 0, i32 1
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector.4"* %this1 to %"class.std::__2::__vector_base.5"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.5", %"class.std::__2::__vector_base.5"* %2, i32 0, i32 0
  %3 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.draco::IndexType"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.draco::IndexType"* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8__appendEmRKS4_(%"class.std::__2::vector.4"* %this, i32 %__n, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %__x) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.4"*, align 4
  %__n.addr = alloca i32, align 4
  %__x.addr = alloca %"class.draco::IndexType"*, align 4
  %__a = alloca %"class.std::__2::allocator.9"*, align 4
  %__v = alloca %"struct.std::__2::__split_buffer", align 4
  store %"class.std::__2::vector.4"* %this, %"class.std::__2::vector.4"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store %"class.draco::IndexType"* %__x, %"class.draco::IndexType"** %__x.addr, align 4
  %this1 = load %"class.std::__2::vector.4"*, %"class.std::__2::vector.4"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.4"* %this1 to %"class.std::__2::__vector_base.5"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base.5"* %0) #8
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %call, align 4
  %2 = bitcast %"class.std::__2::vector.4"* %this1 to %"class.std::__2::__vector_base.5"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.5", %"class.std::__2::__vector_base.5"* %2, i32 0, i32 1
  %3 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__end_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.draco::IndexType"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.draco::IndexType"* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  %4 = load i32, i32* %__n.addr, align 4
  %cmp = icmp uge i32 %sub.ptr.div, %4
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %5 = load i32, i32* %__n.addr, align 4
  %6 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__x.addr, align 4
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE18__construct_at_endEmRKS4_(%"class.std::__2::vector.4"* %this1, i32 %5, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %6)
  br label %if.end

if.else:                                          ; preds = %entry
  %7 = bitcast %"class.std::__2::vector.4"* %this1 to %"class.std::__2::__vector_base.5"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.9"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.5"* %7) #8
  store %"class.std::__2::allocator.9"* %call2, %"class.std::__2::allocator.9"** %__a, align 4
  %call3 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.4"* %this1) #8
  %8 = load i32, i32* %__n.addr, align 4
  %add = add i32 %call3, %8
  %call4 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE11__recommendEm(%"class.std::__2::vector.4"* %this1, i32 %add)
  %call5 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.4"* %this1) #8
  %9 = load %"class.std::__2::allocator.9"*, %"class.std::__2::allocator.9"** %__a, align 4
  %call6 = call %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEEC2EmmS7_(%"struct.std::__2::__split_buffer"* %__v, i32 %call4, i32 %call5, %"class.std::__2::allocator.9"* nonnull align 1 dereferenceable(1) %9)
  %10 = load i32, i32* %__n.addr, align 4
  %11 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__x.addr, align 4
  call void @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE18__construct_at_endEmRKS4_(%"struct.std::__2::__split_buffer"* %__v, i32 %10, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %11)
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS4_RS6_EE(%"class.std::__2::vector.4"* %this1, %"struct.std::__2::__split_buffer"* nonnull align 4 dereferenceable(20) %__v)
  %call7 = call %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEED2Ev(%"struct.std::__2::__split_buffer"* %__v) #8
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_(%"class.std::__2::vector.4"* %this, %"class.draco::IndexType"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.4"*, align 4
  %__new_last.addr = alloca %"class.draco::IndexType"*, align 4
  %__old_size = alloca i32, align 4
  store %"class.std::__2::vector.4"* %this, %"class.std::__2::vector.4"** %this.addr, align 4
  store %"class.draco::IndexType"* %__new_last, %"class.draco::IndexType"** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::vector.4"*, %"class.std::__2::vector.4"** %this.addr, align 4
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__new_last.addr, align 4
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE27__invalidate_iterators_pastEPS4_(%"class.std::__2::vector.4"* %this1, %"class.draco::IndexType"* %0)
  %call = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.4"* %this1) #8
  store i32 %call, i32* %__old_size, align 4
  %1 = bitcast %"class.std::__2::vector.4"* %this1 to %"class.std::__2::__vector_base.5"*
  %2 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__new_last.addr, align 4
  call void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_(%"class.std::__2::__vector_base.5"* %1, %"class.draco::IndexType"* %2) #8
  %3 = load i32, i32* %__old_size, align 4
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_shrinkEm(%"class.std::__2::vector.4"* %this1, i32 %3) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base.5"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.5"*, align 4
  store %"class.std::__2::__vector_base.5"* %this, %"class.std::__2::__vector_base.5"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.5"*, %"class.std::__2::__vector_base.5"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.5", %"class.std::__2::__vector_base.5"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.6"* %__end_cap_) #8
  ret %"class.draco::IndexType"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE18__construct_at_endEmRKS4_(%"class.std::__2::vector.4"* %this, i32 %__n, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %__x) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.4"*, align 4
  %__n.addr = alloca i32, align 4
  %__x.addr = alloca %"class.draco::IndexType"*, align 4
  %__tx = alloca %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction", align 4
  store %"class.std::__2::vector.4"* %this, %"class.std::__2::vector.4"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store %"class.draco::IndexType"* %__x, %"class.draco::IndexType"** %__x.addr, align 4
  %this1 = load %"class.std::__2::vector.4"*, %"class.std::__2::vector.4"** %this.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %call = call %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionC2ERS7_m(%"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %__tx, %"class.std::__2::vector.4"* nonnull align 4 dereferenceable(12) %this1, i32 %0)
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__pos_, align 4
  %__new_end_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %__tx, i32 0, i32 2
  %2 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__new_end_, align 4
  %cmp = icmp ne %"class.draco::IndexType"* %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = bitcast %"class.std::__2::vector.4"* %this1 to %"class.std::__2::__vector_base.5"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.9"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.5"* %3) #8
  %__pos_3 = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %4 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__pos_3, align 4
  %call4 = call %"class.draco::IndexType"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType"* %4) #8
  %5 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__x.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE9constructIS5_JRKS5_EEEvRS6_PT_DpOT0_(%"class.std::__2::allocator.9"* nonnull align 1 dereferenceable(1) %call2, %"class.draco::IndexType"* %call4, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %5)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %__pos_5 = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %6 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__pos_5, align 4
  %incdec.ptr = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %6, i32 1
  store %"class.draco::IndexType"* %incdec.ptr, %"class.draco::IndexType"** %__pos_5, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call6 = call %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %__tx) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.9"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.5"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.5"*, align 4
  store %"class.std::__2::__vector_base.5"* %this, %"class.std::__2::__vector_base.5"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.5"*, %"class.std::__2::__vector_base.5"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.5", %"class.std::__2::__vector_base.5"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.9"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.6"* %__end_cap_) #8
  ret %"class.std::__2::allocator.9"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE11__recommendEm(%"class.std::__2::vector.4"* %this, i32 %__new_size) #0 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %"class.std::__2::vector.4"*, align 4
  %__new_size.addr = alloca i32, align 4
  %__ms = alloca i32, align 4
  %__cap = alloca i32, align 4
  %ref.tmp = alloca i32, align 4
  store %"class.std::__2::vector.4"* %this, %"class.std::__2::vector.4"** %this.addr, align 4
  store i32 %__new_size, i32* %__new_size.addr, align 4
  %this1 = load %"class.std::__2::vector.4"*, %"class.std::__2::vector.4"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8max_sizeEv(%"class.std::__2::vector.4"* %this1) #8
  store i32 %call, i32* %__ms, align 4
  %0 = load i32, i32* %__new_size.addr, align 4
  %1 = load i32, i32* %__ms, align 4
  %cmp = icmp ugt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = bitcast %"class.std::__2::vector.4"* %this1 to %"class.std::__2::__vector_base_common"*
  call void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"* %2) #10
  unreachable

if.end:                                           ; preds = %entry
  %call2 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.4"* %this1) #8
  store i32 %call2, i32* %__cap, align 4
  %3 = load i32, i32* %__cap, align 4
  %4 = load i32, i32* %__ms, align 4
  %div = udiv i32 %4, 2
  %cmp3 = icmp uge i32 %3, %div
  br i1 %cmp3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %if.end
  %5 = load i32, i32* %__ms, align 4
  store i32 %5, i32* %retval, align 4
  br label %return

if.end5:                                          ; preds = %if.end
  %6 = load i32, i32* %__cap, align 4
  %mul = mul i32 2, %6
  store i32 %mul, i32* %ref.tmp, align 4
  %call6 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %__new_size.addr)
  %7 = load i32, i32* %call6, align 4
  store i32 %7, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end5, %if.then4
  %8 = load i32, i32* %retval, align 4
  ret i32 %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEEC2EmmS7_(%"struct.std::__2::__split_buffer"* returned %this, i32 %__cap, i32 %__start, %"class.std::__2::allocator.9"* nonnull align 1 dereferenceable(1) %__a) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"struct.std::__2::__split_buffer"*, align 4
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__cap.addr = alloca i32, align 4
  %__start.addr = alloca i32, align 4
  %__a.addr = alloca %"class.std::__2::allocator.9"*, align 4
  %ref.tmp = alloca i8*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store i32 %__cap, i32* %__cap.addr, align 4
  store i32 %__start, i32* %__start.addr, align 4
  store %"class.std::__2::allocator.9"* %__a, %"class.std::__2::allocator.9"** %__a.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer"* %this1, %"struct.std::__2::__split_buffer"** %retval, align 4
  %0 = bitcast %"struct.std::__2::__split_buffer"* %this1 to %"class.std::__2::__split_buffer_common"*
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  store i8* null, i8** %ref.tmp, align 4
  %1 = load %"class.std::__2::allocator.9"*, %"class.std::__2::allocator.9"** %__a.addr, align 4
  %call = call %"class.std::__2::__compressed_pair.21"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEEC2IDnS8_EEOT_OT0_(%"class.std::__2::__compressed_pair.21"* %__end_cap_, i8** nonnull align 4 dereferenceable(4) %ref.tmp, %"class.std::__2::allocator.9"* nonnull align 1 dereferenceable(1) %1)
  %2 = load i32, i32* %__cap.addr, align 4
  %cmp = icmp ne i32 %2, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.9"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE7__allocEv(%"struct.std::__2::__split_buffer"* %this1) #8
  %3 = load i32, i32* %__cap.addr, align 4
  %call3 = call %"class.draco::IndexType"* @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE8allocateERS6_m(%"class.std::__2::allocator.9"* nonnull align 1 dereferenceable(1) %call2, i32 %3)
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %"class.draco::IndexType"* [ %call3, %cond.true ], [ null, %cond.false ]
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  store %"class.draco::IndexType"* %cond, %"class.draco::IndexType"** %__first_, align 4
  %__first_4 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %4 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__first_4, align 4
  %5 = load i32, i32* %__start.addr, align 4
  %add.ptr = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %4, i32 %5
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 2
  store %"class.draco::IndexType"* %add.ptr, %"class.draco::IndexType"** %__end_, align 4
  %__begin_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 1
  store %"class.draco::IndexType"* %add.ptr, %"class.draco::IndexType"** %__begin_, align 4
  %__first_5 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %6 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__first_5, align 4
  %7 = load i32, i32* %__cap.addr, align 4
  %add.ptr6 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %6, i32 %7
  %call7 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this1) #8
  store %"class.draco::IndexType"* %add.ptr6, %"class.draco::IndexType"** %call7, align 4
  %8 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %retval, align 4
  ret %"struct.std::__2::__split_buffer"* %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE18__construct_at_endEmRKS4_(%"struct.std::__2::__split_buffer"* %this, i32 %__n, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %__x) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__n.addr = alloca i32, align 4
  %__x.addr = alloca %"class.draco::IndexType"*, align 4
  %__tx = alloca %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction", align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store %"class.draco::IndexType"* %__x, %"class.draco::IndexType"** %__x.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 2
  %0 = load i32, i32* %__n.addr, align 4
  %call = call %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE21_ConstructTransactionC2EPPS4_m(%"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"* %__tx, %"class.draco::IndexType"** %__end_, i32 %0) #8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %__pos_ = getelementptr inbounds %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"* %__tx, i32 0, i32 0
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__pos_, align 4
  %__end_2 = getelementptr inbounds %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %2 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__end_2, align 4
  %cmp = icmp ne %"class.draco::IndexType"* %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %call3 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.9"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE7__allocEv(%"struct.std::__2::__split_buffer"* %this1) #8
  %__pos_4 = getelementptr inbounds %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"* %__tx, i32 0, i32 0
  %3 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__pos_4, align 4
  %call5 = call %"class.draco::IndexType"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType"* %3) #8
  %4 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__x.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE9constructIS5_JRKS5_EEEvRS6_PT_DpOT0_(%"class.std::__2::allocator.9"* nonnull align 1 dereferenceable(1) %call3, %"class.draco::IndexType"* %call5, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %4)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %__pos_6 = getelementptr inbounds %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"* %__tx, i32 0, i32 0
  %5 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__pos_6, align 4
  %incdec.ptr = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %5, i32 1
  store %"class.draco::IndexType"* %incdec.ptr, %"class.draco::IndexType"** %__pos_6, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call7 = call %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"* %__tx) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS4_RS6_EE(%"class.std::__2::vector.4"* %this, %"struct.std::__2::__split_buffer"* nonnull align 4 dereferenceable(20) %__v) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.4"*, align 4
  %__v.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"class.std::__2::vector.4"* %this, %"class.std::__2::vector.4"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer"* %__v, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %this1 = load %"class.std::__2::vector.4"*, %"class.std::__2::vector.4"** %this.addr, align 4
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_deleteEv(%"class.std::__2::vector.4"* %this1) #8
  %0 = bitcast %"class.std::__2::vector.4"* %this1 to %"class.std::__2::__vector_base.5"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.9"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.5"* %0) #8
  %1 = bitcast %"class.std::__2::vector.4"* %this1 to %"class.std::__2::__vector_base.5"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.5", %"class.std::__2::__vector_base.5"* %1, i32 0, i32 0
  %2 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  %3 = bitcast %"class.std::__2::vector.4"* %this1 to %"class.std::__2::__vector_base.5"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.5", %"class.std::__2::__vector_base.5"* %3, i32 0, i32 1
  %4 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__end_, align 4
  %5 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__begin_2 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %5, i32 0, i32 1
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE46__construct_backward_with_exception_guaranteesIS5_EENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS6_PT_SC_EE5valuesr31is_trivially_move_constructibleISC_EE5valueEvE4typeERS6_SD_SD_RSD_(%"class.std::__2::allocator.9"* nonnull align 1 dereferenceable(1) %call, %"class.draco::IndexType"* %2, %"class.draco::IndexType"* %4, %"class.draco::IndexType"** nonnull align 4 dereferenceable(4) %__begin_2)
  %6 = bitcast %"class.std::__2::vector.4"* %this1 to %"class.std::__2::__vector_base.5"*
  %__begin_3 = getelementptr inbounds %"class.std::__2::__vector_base.5", %"class.std::__2::__vector_base.5"* %6, i32 0, i32 0
  %7 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__begin_4 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %7, i32 0, i32 1
  call void @_ZNSt3__24swapIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS7_EE5valueEvE4typeERS7_SA_(%"class.draco::IndexType"** nonnull align 4 dereferenceable(4) %__begin_3, %"class.draco::IndexType"** nonnull align 4 dereferenceable(4) %__begin_4) #8
  %8 = bitcast %"class.std::__2::vector.4"* %this1 to %"class.std::__2::__vector_base.5"*
  %__end_5 = getelementptr inbounds %"class.std::__2::__vector_base.5", %"class.std::__2::__vector_base.5"* %8, i32 0, i32 1
  %9 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__end_6 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %9, i32 0, i32 2
  call void @_ZNSt3__24swapIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS7_EE5valueEvE4typeERS7_SA_(%"class.draco::IndexType"** nonnull align 4 dereferenceable(4) %__end_5, %"class.draco::IndexType"** nonnull align 4 dereferenceable(4) %__end_6) #8
  %10 = bitcast %"class.std::__2::vector.4"* %this1 to %"class.std::__2::__vector_base.5"*
  %call7 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base.5"* %10) #8
  %11 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %call8 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE9__end_capEv(%"struct.std::__2::__split_buffer"* %11) #8
  call void @_ZNSt3__24swapIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS7_EE5valueEvE4typeERS7_SA_(%"class.draco::IndexType"** nonnull align 4 dereferenceable(4) %call7, %"class.draco::IndexType"** nonnull align 4 dereferenceable(4) %call8) #8
  %12 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__begin_9 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %12, i32 0, i32 1
  %13 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_9, align 4
  %14 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %14, i32 0, i32 0
  store %"class.draco::IndexType"* %13, %"class.draco::IndexType"** %__first_, align 4
  %call10 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.4"* %this1) #8
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE14__annotate_newEm(%"class.std::__2::vector.4"* %this1, i32 %call10) #8
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector.4"* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEED2Ev(%"struct.std::__2::__split_buffer"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"struct.std::__2::__split_buffer"*, align 4
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer"* %this1, %"struct.std::__2::__split_buffer"** %retval, align 4
  call void @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE5clearEv(%"struct.std::__2::__split_buffer"* %this1) #8
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__first_, align 4
  %tobool = icmp ne %"class.draco::IndexType"* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.9"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE7__allocEv(%"struct.std::__2::__split_buffer"* %this1) #8
  %__first_2 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__first_2, align 4
  %call3 = call i32 @_ZNKSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE8capacityEv(%"struct.std::__2::__split_buffer"* %this1)
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE10deallocateERS6_PS5_m(%"class.std::__2::allocator.9"* nonnull align 1 dereferenceable(1) %call, %"class.draco::IndexType"* %1, i32 %call3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %retval, align 4
  ret %"struct.std::__2::__split_buffer"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.6"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.6"*, align 4
  store %"class.std::__2::__compressed_pair.6"* %this, %"class.std::__2::__compressed_pair.6"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.6"*, %"class.std::__2::__compressed_pair.6"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.6"* %this1 to %"struct.std::__2::__compressed_pair_elem.7"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.7"* %0) #8
  ret %"class.draco::IndexType"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.7"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.7"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.7"* %this, %"struct.std::__2::__compressed_pair_elem.7"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.7"*, %"struct.std::__2::__compressed_pair_elem.7"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.7", %"struct.std::__2::__compressed_pair_elem.7"* %this1, i32 0, i32 0
  ret %"class.draco::IndexType"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionC2ERS7_m(%"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* returned %this, %"class.std::__2::vector.4"* nonnull align 4 dereferenceable(12) %__v, i32 %__n) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"*, align 4
  %__v.addr = alloca %"class.std::__2::vector.4"*, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"** %this.addr, align 4
  store %"class.std::__2::vector.4"* %__v, %"class.std::__2::vector.4"** %__v.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"*, %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"** %this.addr, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::vector.4"*, %"class.std::__2::vector.4"** %__v.addr, align 4
  store %"class.std::__2::vector.4"* %0, %"class.std::__2::vector.4"** %__v_, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %1 = load %"class.std::__2::vector.4"*, %"class.std::__2::vector.4"** %__v.addr, align 4
  %2 = bitcast %"class.std::__2::vector.4"* %1 to %"class.std::__2::__vector_base.5"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.5", %"class.std::__2::__vector_base.5"* %2, i32 0, i32 1
  %3 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__end_, align 4
  store %"class.draco::IndexType"* %3, %"class.draco::IndexType"** %__pos_, align 4
  %__new_end_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %this1, i32 0, i32 2
  %4 = load %"class.std::__2::vector.4"*, %"class.std::__2::vector.4"** %__v.addr, align 4
  %5 = bitcast %"class.std::__2::vector.4"* %4 to %"class.std::__2::__vector_base.5"*
  %__end_2 = getelementptr inbounds %"class.std::__2::__vector_base.5", %"class.std::__2::__vector_base.5"* %5, i32 0, i32 1
  %6 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__end_2, align 4
  %7 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %6, i32 %7
  store %"class.draco::IndexType"* %add.ptr, %"class.draco::IndexType"** %__new_end_, align 4
  ret %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE9constructIS5_JRKS5_EEEvRS6_PT_DpOT0_(%"class.std::__2::allocator.9"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType"* %__p, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %__args) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.9"*, align 4
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  %__args.addr = alloca %"class.draco::IndexType"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_construct", align 1
  store %"class.std::__2::allocator.9"* %__a, %"class.std::__2::allocator.9"** %__a.addr, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  store %"class.draco::IndexType"* %__args, %"class.draco::IndexType"** %__args.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_construct"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.9"*, %"class.std::__2::allocator.9"** %__a.addr, align 4
  %2 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  %3 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZNSt3__27forwardIRKN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %3) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE11__constructIS5_JRKS5_EEEvNS_17integral_constantIbLb1EEERS6_PT_DpOT0_(%"class.std::__2::allocator.9"* nonnull align 1 dereferenceable(1) %1, %"class.draco::IndexType"* %2, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType"* %__p) #0 comdat {
entry:
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  ret %"class.draco::IndexType"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"*, align 4
  store %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"** %this.addr, align 4
  %this1 = load %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"*, %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__pos_, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::vector.4"*, %"class.std::__2::vector.4"** %__v_, align 4
  %2 = bitcast %"class.std::__2::vector.4"* %1 to %"class.std::__2::__vector_base.5"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.5", %"class.std::__2::__vector_base.5"* %2, i32 0, i32 1
  store %"class.draco::IndexType"* %0, %"class.draco::IndexType"** %__end_, align 4
  ret %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE11__constructIS5_JRKS5_EEEvNS_17integral_constantIbLb1EEERS6_PT_DpOT0_(%"class.std::__2::allocator.9"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType"* %__p, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %__args) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.9"*, align 4
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  %__args.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.std::__2::allocator.9"* %__a, %"class.std::__2::allocator.9"** %__a.addr, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  store %"class.draco::IndexType"* %__args, %"class.draco::IndexType"** %__args.addr, align 4
  %1 = load %"class.std::__2::allocator.9"*, %"class.std::__2::allocator.9"** %__a.addr, align 4
  %2 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  %3 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZNSt3__27forwardIRKN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %3) #8
  call void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE9constructIS4_JRKS4_EEEvPT_DpOT0_(%"class.std::__2::allocator.9"* %1, %"class.draco::IndexType"* %2, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZNSt3__27forwardIRKN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.draco::IndexType"* %__t, %"class.draco::IndexType"** %__t.addr, align 4
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__t.addr, align 4
  ret %"class.draco::IndexType"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE9constructIS4_JRKS4_EEEvPT_DpOT0_(%"class.std::__2::allocator.9"* %this, %"class.draco::IndexType"* %__p, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %__args) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.9"*, align 4
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  %__args.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.std::__2::allocator.9"* %this, %"class.std::__2::allocator.9"** %this.addr, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  store %"class.draco::IndexType"* %__args, %"class.draco::IndexType"** %__args.addr, align 4
  %this1 = load %"class.std::__2::allocator.9"*, %"class.std::__2::allocator.9"** %this.addr, align 4
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  %1 = bitcast %"class.draco::IndexType"* %0 to i8*
  %2 = bitcast i8* %1 to %"class.draco::IndexType"*
  %3 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZNSt3__27forwardIRKN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %3) #8
  %4 = bitcast %"class.draco::IndexType"* %2 to i8*
  %5 = bitcast %"class.draco::IndexType"* %call to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 4, i1 false)
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.9"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.6"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.6"*, align 4
  store %"class.std::__2::__compressed_pair.6"* %this, %"class.std::__2::__compressed_pair.6"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.6"*, %"class.std::__2::__compressed_pair.6"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.6"* %this1 to %"struct.std::__2::__compressed_pair_elem.8"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.9"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.8"* %0) #8
  ret %"class.std::__2::allocator.9"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.9"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.8"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.8"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.8"* %this, %"struct.std::__2::__compressed_pair_elem.8"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.8"*, %"struct.std::__2::__compressed_pair_elem.8"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.8"* %this1 to %"class.std::__2::allocator.9"*
  ret %"class.std::__2::allocator.9"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8max_sizeEv(%"class.std::__2::vector.4"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.4"*, align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp3 = alloca i32, align 4
  store %"class.std::__2::vector.4"* %this, %"class.std::__2::vector.4"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.4"*, %"class.std::__2::vector.4"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.4"* %this1 to %"class.std::__2::__vector_base.5"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.9"* @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.5"* %0) #8
  %call2 = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE8max_sizeERKS6_(%"class.std::__2::allocator.9"* nonnull align 1 dereferenceable(1) %call) #8
  store i32 %call2, i32* %ref.tmp, align 4
  %call4 = call i32 @_ZNSt3__214numeric_limitsIlE3maxEv() #8
  store i32 %call4, i32* %ref.tmp3, align 4
  %call5 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %1 = load i32, i32* %call5, align 4
  ret i32 %1
}

; Function Attrs: noreturn
declare void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"*) #5

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.4"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.4"*, align 4
  store %"class.std::__2::vector.4"* %this, %"class.std::__2::vector.4"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.4"*, %"class.std::__2::vector.4"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.4"* %this1 to %"class.std::__2::__vector_base.5"*
  %call = call i32 @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::__vector_base.5"* %0) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::__less", align 1
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__a.addr, align 4
  %1 = load i32*, i32** %__b.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::__less", align 1
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__a.addr, align 4
  %1 = load i32*, i32** %__b.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE8max_sizeERKS6_(%"class.std::__2::allocator.9"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.9"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_max_size", align 1
  store %"class.std::__2::allocator.9"* %__a, %"class.std::__2::allocator.9"** %__a.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_max_size"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.9"*, %"class.std::__2::allocator.9"** %__a.addr, align 4
  %call = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS6_(%"class.std::__2::allocator.9"* nonnull align 1 dereferenceable(1) %1) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.9"* @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.5"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.5"*, align 4
  store %"class.std::__2::__vector_base.5"* %this, %"class.std::__2::__vector_base.5"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.5"*, %"class.std::__2::__vector_base.5"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.5", %"class.std::__2::__vector_base.5"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.9"* @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.6"* %__end_cap_) #8
  ret %"class.std::__2::allocator.9"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__214numeric_limitsIlE3maxEv() #0 comdat {
entry:
  %call = call i32 @_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv() #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__comp = alloca %"struct.std::__2::__less", align 1
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__b.addr, align 4
  %1 = load i32*, i32** %__a.addr, align 4
  %call = call zeroext i1 @_ZNKSt3__26__lessImmEclERKmS3_(%"struct.std::__2::__less"* %__comp, i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  br i1 %call, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32*, i32** %__b.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %3 = load i32*, i32** %__a.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi i32* [ %2, %cond.true ], [ %3, %cond.false ]
  ret i32* %cond-lvalue
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__26__lessImmEclERKmS3_(%"struct.std::__2::__less"* %this, i32* nonnull align 4 dereferenceable(4) %__x, i32* nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__less"*, align 4
  %__x.addr = alloca i32*, align 4
  %__y.addr = alloca i32*, align 4
  store %"struct.std::__2::__less"* %this, %"struct.std::__2::__less"** %this.addr, align 4
  store i32* %__x, i32** %__x.addr, align 4
  store i32* %__y, i32** %__y.addr, align 4
  %this1 = load %"struct.std::__2::__less"*, %"struct.std::__2::__less"** %this.addr, align 4
  %0 = load i32*, i32** %__x.addr, align 4
  %1 = load i32, i32* %0, align 4
  %2 = load i32*, i32** %__y.addr, align 4
  %3 = load i32, i32* %2, align 4
  %cmp = icmp ult i32 %1, %3
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS6_(%"class.std::__2::allocator.9"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.9"*, align 4
  store %"class.std::__2::allocator.9"* %__a, %"class.std::__2::allocator.9"** %__a.addr, align 4
  %1 = load %"class.std::__2::allocator.9"*, %"class.std::__2::allocator.9"** %__a.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE8max_sizeEv(%"class.std::__2::allocator.9"* %1) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE8max_sizeEv(%"class.std::__2::allocator.9"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.9"*, align 4
  store %"class.std::__2::allocator.9"* %this, %"class.std::__2::allocator.9"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.9"*, %"class.std::__2::allocator.9"** %this.addr, align 4
  ret i32 1073741823
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.9"* @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.6"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.6"*, align 4
  store %"class.std::__2::__compressed_pair.6"* %this, %"class.std::__2::__compressed_pair.6"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.6"*, %"class.std::__2::__compressed_pair.6"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.6"* %this1 to %"struct.std::__2::__compressed_pair_elem.8"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.9"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.8"* %0) #8
  ret %"class.std::__2::allocator.9"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.9"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.8"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.8"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.8"* %this, %"struct.std::__2::__compressed_pair_elem.8"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.8"*, %"struct.std::__2::__compressed_pair_elem.8"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.8"* %this1 to %"class.std::__2::allocator.9"*
  ret %"class.std::__2::allocator.9"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv() #0 comdat {
entry:
  ret i32 2147483647
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::__vector_base.5"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.5"*, align 4
  store %"class.std::__2::__vector_base.5"* %this, %"class.std::__2::__vector_base.5"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.5"*, %"class.std::__2::__vector_base.5"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base.5"* %this1) #8
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.5", %"class.std::__2::__vector_base.5"* %this1, i32 0, i32 0
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.draco::IndexType"* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.draco::IndexType"* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base.5"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.5"*, align 4
  store %"class.std::__2::__vector_base.5"* %this, %"class.std::__2::__vector_base.5"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.5"*, %"class.std::__2::__vector_base.5"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.5", %"class.std::__2::__vector_base.5"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.6"* %__end_cap_) #8
  ret %"class.draco::IndexType"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.6"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.6"*, align 4
  store %"class.std::__2::__compressed_pair.6"* %this, %"class.std::__2::__compressed_pair.6"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.6"*, %"class.std::__2::__compressed_pair.6"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.6"* %this1 to %"struct.std::__2::__compressed_pair_elem.7"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.7"* %0) #8
  ret %"class.draco::IndexType"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.7"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.7"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.7"* %this, %"struct.std::__2::__compressed_pair_elem.7"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.7"*, %"struct.std::__2::__compressed_pair_elem.7"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.7", %"struct.std::__2::__compressed_pair_elem.7"* %this1, i32 0, i32 0
  ret %"class.draco::IndexType"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__comp = alloca %"struct.std::__2::__less", align 1
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__a.addr, align 4
  %1 = load i32*, i32** %__b.addr, align 4
  %call = call zeroext i1 @_ZNKSt3__26__lessImmEclERKmS3_(%"struct.std::__2::__less"* %__comp, i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  br i1 %call, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32*, i32** %__b.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %3 = load i32*, i32** %__a.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi i32* [ %2, %cond.true ], [ %3, %cond.false ]
  ret i32* %cond-lvalue
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.21"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEEC2IDnS8_EEOT_OT0_(%"class.std::__2::__compressed_pair.21"* returned %this, i8** nonnull align 4 dereferenceable(4) %__t1, %"class.std::__2::allocator.9"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.21"*, align 4
  %__t1.addr = alloca i8**, align 4
  %__t2.addr = alloca %"class.std::__2::allocator.9"*, align 4
  store %"class.std::__2::__compressed_pair.21"* %this, %"class.std::__2::__compressed_pair.21"** %this.addr, align 4
  store i8** %__t1, i8*** %__t1.addr, align 4
  store %"class.std::__2::allocator.9"* %__t2, %"class.std::__2::allocator.9"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.21"*, %"class.std::__2::__compressed_pair.21"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.21"* %this1 to %"struct.std::__2::__compressed_pair_elem.7"*
  %1 = load i8**, i8*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %1) #8
  %call2 = call %"struct.std::__2::__compressed_pair_elem.7"* @_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.7"* %0, i8** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.21"* %this1 to i8*
  %3 = getelementptr inbounds i8, i8* %2, i32 4
  %4 = bitcast i8* %3 to %"struct.std::__2::__compressed_pair_elem.22"*
  %5 = load %"class.std::__2::allocator.9"*, %"class.std::__2::allocator.9"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.9"* @_ZNSt3__27forwardIRNS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEEEOT_RNS_16remove_referenceIS8_E4typeE(%"class.std::__2::allocator.9"* nonnull align 1 dereferenceable(1) %5) #8
  %call4 = call %"struct.std::__2::__compressed_pair_elem.22"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb0EEC2IS7_vEEOT_(%"struct.std::__2::__compressed_pair_elem.22"* %4, %"class.std::__2::allocator.9"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::__compressed_pair.21"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType"* @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE8allocateERS6_m(%"class.std::__2::allocator.9"* nonnull align 1 dereferenceable(1) %__a, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.9"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.9"* %__a, %"class.std::__2::allocator.9"** %__a.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.9"*, %"class.std::__2::allocator.9"** %__a.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call %"class.draco::IndexType"* @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE8allocateEmPKv(%"class.std::__2::allocator.9"* %0, i32 %1, i8* null)
  ret %"class.draco::IndexType"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.9"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE7__allocEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.9"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.21"* %__end_cap_) #8
  ret %"class.std::__2::allocator.9"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.21"* %__end_cap_) #8
  ret %"class.draco::IndexType"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca i8**, align 4
  store i8** %__t, i8*** %__t.addr, align 4
  %0 = load i8**, i8*** %__t.addr, align 4
  ret i8** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.7"* @_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.7"* returned %this, i8** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.7"*, align 4
  %__u.addr = alloca i8**, align 4
  store %"struct.std::__2::__compressed_pair_elem.7"* %this, %"struct.std::__2::__compressed_pair_elem.7"** %this.addr, align 4
  store i8** %__u, i8*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.7"*, %"struct.std::__2::__compressed_pair_elem.7"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.7", %"struct.std::__2::__compressed_pair_elem.7"* %this1, i32 0, i32 0
  %0 = load i8**, i8*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %0) #8
  store %"class.draco::IndexType"* null, %"class.draco::IndexType"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.7"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.9"* @_ZNSt3__27forwardIRNS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEEEOT_RNS_16remove_referenceIS8_E4typeE(%"class.std::__2::allocator.9"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::allocator.9"*, align 4
  store %"class.std::__2::allocator.9"* %__t, %"class.std::__2::allocator.9"** %__t.addr, align 4
  %0 = load %"class.std::__2::allocator.9"*, %"class.std::__2::allocator.9"** %__t.addr, align 4
  ret %"class.std::__2::allocator.9"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.22"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb0EEC2IS7_vEEOT_(%"struct.std::__2::__compressed_pair_elem.22"* returned %this, %"class.std::__2::allocator.9"* nonnull align 1 dereferenceable(1) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.22"*, align 4
  %__u.addr = alloca %"class.std::__2::allocator.9"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.22"* %this, %"struct.std::__2::__compressed_pair_elem.22"** %this.addr, align 4
  store %"class.std::__2::allocator.9"* %__u, %"class.std::__2::allocator.9"** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.22"*, %"struct.std::__2::__compressed_pair_elem.22"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.22", %"struct.std::__2::__compressed_pair_elem.22"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::allocator.9"*, %"class.std::__2::allocator.9"** %__u.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.9"* @_ZNSt3__27forwardIRNS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEEEOT_RNS_16remove_referenceIS8_E4typeE(%"class.std::__2::allocator.9"* nonnull align 1 dereferenceable(1) %0) #8
  store %"class.std::__2::allocator.9"* %call, %"class.std::__2::allocator.9"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.22"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType"* @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE8allocateEmPKv(%"class.std::__2::allocator.9"* %this, i32 %__n, i8* %0) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.9"*, align 4
  %__n.addr = alloca i32, align 4
  %.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.9"* %this, %"class.std::__2::allocator.9"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::allocator.9"*, %"class.std::__2::allocator.9"** %this.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE8max_sizeEv(%"class.std::__2::allocator.9"* %this1) #8
  %cmp = icmp ugt i32 %1, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__220__throw_length_errorEPKc(i8* getelementptr inbounds ([68 x i8], [68 x i8]* @.str, i32 0, i32 0)) #10
  unreachable

if.end:                                           ; preds = %entry
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  %call2 = call i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %mul, i32 4)
  %3 = bitcast i8* %call2 to %"class.draco::IndexType"*
  ret %"class.draco::IndexType"* %3
}

; Function Attrs: noinline noreturn nounwind optnone
define linkonce_odr hidden void @_ZNSt3__220__throw_length_errorEPKc(i8* %__msg) #6 comdat {
entry:
  %__msg.addr = alloca i8*, align 4
  store i8* %__msg, i8** %__msg.addr, align 4
  call void @abort() #10
  unreachable
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %__size, i32 %__align) #0 comdat {
entry:
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i32, i32* %__size.addr, align 4
  %call = call noalias nonnull i8* @_Znwm(i32 %0) #7
  ret i8* %call
}

; Function Attrs: noreturn
declare void @abort() #5

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.9"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.21"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.21"*, align 4
  store %"class.std::__2::__compressed_pair.21"* %this, %"class.std::__2::__compressed_pair.21"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.21"*, %"class.std::__2::__compressed_pair.21"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.21"* %this1 to i8*
  %add.ptr = getelementptr inbounds i8, i8* %0, i32 4
  %1 = bitcast i8* %add.ptr to %"struct.std::__2::__compressed_pair_elem.22"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.9"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.22"* %1) #8
  ret %"class.std::__2::allocator.9"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.9"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.22"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.22"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.22"* %this, %"struct.std::__2::__compressed_pair_elem.22"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.22"*, %"struct.std::__2::__compressed_pair_elem.22"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.22", %"struct.std::__2::__compressed_pair_elem.22"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::allocator.9"*, %"class.std::__2::allocator.9"** %__value_, align 4
  ret %"class.std::__2::allocator.9"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.21"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.21"*, align 4
  store %"class.std::__2::__compressed_pair.21"* %this, %"class.std::__2::__compressed_pair.21"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.21"*, %"class.std::__2::__compressed_pair.21"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.21"* %this1 to %"struct.std::__2::__compressed_pair_elem.7"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.7"* %0) #8
  ret %"class.draco::IndexType"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE21_ConstructTransactionC2EPPS4_m(%"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"* returned %this, %"class.draco::IndexType"** %__p, i32 %__n) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"*, align 4
  %__p.addr = alloca %"class.draco::IndexType"**, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"* %this, %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"** %this.addr, align 4
  store %"class.draco::IndexType"** %__p, %"class.draco::IndexType"*** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"*, %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexType"**, %"class.draco::IndexType"*** %__p.addr, align 4
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %0, align 4
  store %"class.draco::IndexType"* %1, %"class.draco::IndexType"** %__pos_, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"* %this1, i32 0, i32 1
  %2 = load %"class.draco::IndexType"**, %"class.draco::IndexType"*** %__p.addr, align 4
  %3 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %2, align 4
  %4 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %3, i32 %4
  store %"class.draco::IndexType"* %add.ptr, %"class.draco::IndexType"** %__end_, align 4
  %__dest_ = getelementptr inbounds %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"* %this1, i32 0, i32 2
  %5 = load %"class.draco::IndexType"**, %"class.draco::IndexType"*** %__p.addr, align 4
  store %"class.draco::IndexType"** %5, %"class.draco::IndexType"*** %__dest_, align 4
  ret %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"*, align 4
  store %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"* %this, %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"*, %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__pos_, align 4
  %__dest_ = getelementptr inbounds %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"* %this1, i32 0, i32 2
  %1 = load %"class.draco::IndexType"**, %"class.draco::IndexType"*** %__dest_, align 4
  store %"class.draco::IndexType"* %0, %"class.draco::IndexType"** %1, align 4
  ret %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_deleteEv(%"class.std::__2::vector.4"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.4"*, align 4
  store %"class.std::__2::vector.4"* %this, %"class.std::__2::vector.4"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.4"*, %"class.std::__2::vector.4"** %this.addr, align 4
  %call = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.4"* %this1) #8
  %0 = bitcast %"class.draco::IndexType"* %call to i8*
  %call2 = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.4"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.4"* %this1) #8
  %add.ptr = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %call2, i32 %call3
  %1 = bitcast %"class.draco::IndexType"* %add.ptr to i8*
  %call4 = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.4"* %this1) #8
  %call5 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.4"* %this1) #8
  %add.ptr6 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %call4, i32 %call5
  %2 = bitcast %"class.draco::IndexType"* %add.ptr6 to i8*
  %call7 = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.4"* %this1) #8
  %call8 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.4"* %this1) #8
  %add.ptr9 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %call7, i32 %call8
  %3 = bitcast %"class.draco::IndexType"* %add.ptr9 to i8*
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_(%"class.std::__2::vector.4"* %this1, i8* %0, i8* %1, i8* %2, i8* %3) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE46__construct_backward_with_exception_guaranteesIS5_EENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS6_PT_SC_EE5valuesr31is_trivially_move_constructibleISC_EE5valueEvE4typeERS6_SD_SD_RSD_(%"class.std::__2::allocator.9"* nonnull align 1 dereferenceable(1) %0, %"class.draco::IndexType"* %__begin1, %"class.draco::IndexType"* %__end1, %"class.draco::IndexType"** nonnull align 4 dereferenceable(4) %__end2) #0 comdat {
entry:
  %.addr = alloca %"class.std::__2::allocator.9"*, align 4
  %__begin1.addr = alloca %"class.draco::IndexType"*, align 4
  %__end1.addr = alloca %"class.draco::IndexType"*, align 4
  %__end2.addr = alloca %"class.draco::IndexType"**, align 4
  %_Np = alloca i32, align 4
  store %"class.std::__2::allocator.9"* %0, %"class.std::__2::allocator.9"** %.addr, align 4
  store %"class.draco::IndexType"* %__begin1, %"class.draco::IndexType"** %__begin1.addr, align 4
  store %"class.draco::IndexType"* %__end1, %"class.draco::IndexType"** %__end1.addr, align 4
  store %"class.draco::IndexType"** %__end2, %"class.draco::IndexType"*** %__end2.addr, align 4
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__end1.addr, align 4
  %2 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin1.addr, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.draco::IndexType"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.draco::IndexType"* %2 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  store i32 %sub.ptr.div, i32* %_Np, align 4
  %3 = load i32, i32* %_Np, align 4
  %4 = load %"class.draco::IndexType"**, %"class.draco::IndexType"*** %__end2.addr, align 4
  %5 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %4, align 4
  %idx.neg = sub i32 0, %3
  %add.ptr = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %5, i32 %idx.neg
  store %"class.draco::IndexType"* %add.ptr, %"class.draco::IndexType"** %4, align 4
  %6 = load i32, i32* %_Np, align 4
  %cmp = icmp sgt i32 %6, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %7 = load %"class.draco::IndexType"**, %"class.draco::IndexType"*** %__end2.addr, align 4
  %8 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %7, align 4
  %9 = bitcast %"class.draco::IndexType"* %8 to i8*
  %10 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin1.addr, align 4
  %11 = bitcast %"class.draco::IndexType"* %10 to i8*
  %12 = load i32, i32* %_Np, align 4
  %mul = mul i32 %12, 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %11, i32 %mul, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__24swapIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS7_EE5valueEvE4typeERS7_SA_(%"class.draco::IndexType"** nonnull align 4 dereferenceable(4) %__x, %"class.draco::IndexType"** nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %__x.addr = alloca %"class.draco::IndexType"**, align 4
  %__y.addr = alloca %"class.draco::IndexType"**, align 4
  %__t = alloca %"class.draco::IndexType"*, align 4
  store %"class.draco::IndexType"** %__x, %"class.draco::IndexType"*** %__x.addr, align 4
  store %"class.draco::IndexType"** %__y, %"class.draco::IndexType"*** %__y.addr, align 4
  %0 = load %"class.draco::IndexType"**, %"class.draco::IndexType"*** %__x.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNSt3__24moveIRPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEONS_16remove_referenceIT_E4typeEOS8_(%"class.draco::IndexType"** nonnull align 4 dereferenceable(4) %0) #8
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %call, align 4
  store %"class.draco::IndexType"* %1, %"class.draco::IndexType"** %__t, align 4
  %2 = load %"class.draco::IndexType"**, %"class.draco::IndexType"*** %__y.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNSt3__24moveIRPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEONS_16remove_referenceIT_E4typeEOS8_(%"class.draco::IndexType"** nonnull align 4 dereferenceable(4) %2) #8
  %3 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %call1, align 4
  %4 = load %"class.draco::IndexType"**, %"class.draco::IndexType"*** %__x.addr, align 4
  store %"class.draco::IndexType"* %3, %"class.draco::IndexType"** %4, align 4
  %call2 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNSt3__24moveIRPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEONS_16remove_referenceIT_E4typeEOS8_(%"class.draco::IndexType"** nonnull align 4 dereferenceable(4) %__t) #8
  %5 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %call2, align 4
  %6 = load %"class.draco::IndexType"**, %"class.draco::IndexType"*** %__y.addr, align 4
  store %"class.draco::IndexType"* %5, %"class.draco::IndexType"** %6, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE14__annotate_newEm(%"class.std::__2::vector.4"* %this, i32 %__current_size) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.4"*, align 4
  %__current_size.addr = alloca i32, align 4
  store %"class.std::__2::vector.4"* %this, %"class.std::__2::vector.4"** %this.addr, align 4
  store i32 %__current_size, i32* %__current_size.addr, align 4
  %this1 = load %"class.std::__2::vector.4"*, %"class.std::__2::vector.4"** %this.addr, align 4
  %call = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.4"* %this1) #8
  %0 = bitcast %"class.draco::IndexType"* %call to i8*
  %call2 = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.4"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.4"* %this1) #8
  %add.ptr = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %call2, i32 %call3
  %1 = bitcast %"class.draco::IndexType"* %add.ptr to i8*
  %call4 = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.4"* %this1) #8
  %call5 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.4"* %this1) #8
  %add.ptr6 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %call4, i32 %call5
  %2 = bitcast %"class.draco::IndexType"* %add.ptr6 to i8*
  %call7 = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.4"* %this1) #8
  %3 = load i32, i32* %__current_size.addr, align 4
  %add.ptr8 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %call7, i32 %3
  %4 = bitcast %"class.draco::IndexType"* %add.ptr8 to i8*
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_(%"class.std::__2::vector.4"* %this1, i8* %0, i8* %1, i8* %2, i8* %4) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector.4"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.4"*, align 4
  store %"class.std::__2::vector.4"* %this, %"class.std::__2::vector.4"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.4"*, %"class.std::__2::vector.4"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_(%"class.std::__2::vector.4"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.4"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector.4"* %this, %"class.std::__2::vector.4"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector.4"*, %"class.std::__2::vector.4"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.4"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.4"*, align 4
  store %"class.std::__2::vector.4"* %this, %"class.std::__2::vector.4"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.4"*, %"class.std::__2::vector.4"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.4"* %this1 to %"class.std::__2::__vector_base.5"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.5", %"class.std::__2::__vector_base.5"* %0, i32 0, i32 0
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  %call = call %"class.draco::IndexType"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType"* %1) #8
  ret %"class.draco::IndexType"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNSt3__24moveIRPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEONS_16remove_referenceIT_E4typeEOS8_(%"class.draco::IndexType"** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.draco::IndexType"**, align 4
  store %"class.draco::IndexType"** %__t, %"class.draco::IndexType"*** %__t.addr, align 4
  %0 = load %"class.draco::IndexType"**, %"class.draco::IndexType"*** %__t.addr, align 4
  ret %"class.draco::IndexType"** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE5clearEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 1
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  call void @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE17__destruct_at_endEPS4_(%"struct.std::__2::__split_buffer"* %this1, %"class.draco::IndexType"* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE10deallocateERS6_PS5_m(%"class.std::__2::allocator.9"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType"* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.9"*, align 4
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.9"* %__a, %"class.std::__2::allocator.9"** %__a.addr, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.9"*, %"class.std::__2::allocator.9"** %__a.addr, align 4
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE10deallocateEPS4_m(%"class.std::__2::allocator.9"* %0, %"class.draco::IndexType"* %1, i32 %2) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE8capacityEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this1) #8
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %call, align 4
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__first_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.draco::IndexType"* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.draco::IndexType"* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE17__destruct_at_endEPS4_(%"struct.std::__2::__split_buffer"* %this, %"class.draco::IndexType"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__new_last.addr = alloca %"class.draco::IndexType"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant.23", align 1
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store %"class.draco::IndexType"* %__new_last, %"class.draco::IndexType"** %__new_last.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__new_last.addr, align 4
  call void @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE17__destruct_at_endEPS4_NS_17integral_constantIbLb0EEE(%"struct.std::__2::__split_buffer"* %this1, %"class.draco::IndexType"* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE17__destruct_at_endEPS4_NS_17integral_constantIbLb0EEE(%"struct.std::__2::__split_buffer"* %this, %"class.draco::IndexType"* %__new_last) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant.23", align 1
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__new_last.addr = alloca %"class.draco::IndexType"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store %"class.draco::IndexType"* %__new_last, %"class.draco::IndexType"** %__new_last.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__new_last.addr, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 2
  %2 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__end_, align 4
  %cmp = icmp ne %"class.draco::IndexType"* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.9"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE7__allocEv(%"struct.std::__2::__split_buffer"* %this1) #8
  %__end_2 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 2
  %3 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__end_2, align 4
  %incdec.ptr = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %3, i32 -1
  store %"class.draco::IndexType"* %incdec.ptr, %"class.draco::IndexType"** %__end_2, align 4
  %call3 = call %"class.draco::IndexType"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType"* %incdec.ptr) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE7destroyIS5_EEvRS6_PT_(%"class.std::__2::allocator.9"* nonnull align 1 dereferenceable(1) %call, %"class.draco::IndexType"* %call3)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE7destroyIS5_EEvRS6_PT_(%"class.std::__2::allocator.9"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType"* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.9"*, align 4
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy.24", align 1
  store %"class.std::__2::allocator.9"* %__a, %"class.std::__2::allocator.9"** %__a.addr, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy.24"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.9"*, %"class.std::__2::allocator.9"** %__a.addr, align 4
  %2 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE9__destroyIS5_EEvNS_17integral_constantIbLb1EEERS6_PT_(%"class.std::__2::allocator.9"* nonnull align 1 dereferenceable(1) %1, %"class.draco::IndexType"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE9__destroyIS5_EEvNS_17integral_constantIbLb1EEERS6_PT_(%"class.std::__2::allocator.9"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType"* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.9"*, align 4
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.std::__2::allocator.9"* %__a, %"class.std::__2::allocator.9"** %__a.addr, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.9"*, %"class.std::__2::allocator.9"** %__a.addr, align 4
  %2 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  call void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE7destroyEPS4_(%"class.std::__2::allocator.9"* %1, %"class.draco::IndexType"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE7destroyEPS4_(%"class.std::__2::allocator.9"* %this, %"class.draco::IndexType"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.9"*, align 4
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.std::__2::allocator.9"* %this, %"class.std::__2::allocator.9"** %this.addr, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.9"*, %"class.std::__2::allocator.9"** %this.addr, align 4
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE10deallocateEPS4_m(%"class.std::__2::allocator.9"* %this, %"class.draco::IndexType"* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.9"*, align 4
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.9"* %this, %"class.std::__2::allocator.9"** %this.addr, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.9"*, %"class.std::__2::allocator.9"** %this.addr, align 4
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  %1 = bitcast %"class.draco::IndexType"* %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.21"* %__end_cap_) #8
  ret %"class.draco::IndexType"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.21"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.21"*, align 4
  store %"class.std::__2::__compressed_pair.21"* %this, %"class.std::__2::__compressed_pair.21"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.21"*, %"class.std::__2::__compressed_pair.21"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.21"* %this1 to %"struct.std::__2::__compressed_pair_elem.7"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.7"* %0) #8
  ret %"class.draco::IndexType"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE27__invalidate_iterators_pastEPS4_(%"class.std::__2::vector.4"* %this, %"class.draco::IndexType"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.4"*, align 4
  %__new_last.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.std::__2::vector.4"* %this, %"class.std::__2::vector.4"** %this.addr, align 4
  store %"class.draco::IndexType"* %__new_last, %"class.draco::IndexType"** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::vector.4"*, %"class.std::__2::vector.4"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_(%"class.std::__2::__vector_base.5"* %this, %"class.draco::IndexType"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.5"*, align 4
  %__new_last.addr = alloca %"class.draco::IndexType"*, align 4
  %__soon_to_be_end = alloca %"class.draco::IndexType"*, align 4
  store %"class.std::__2::__vector_base.5"* %this, %"class.std::__2::__vector_base.5"** %this.addr, align 4
  store %"class.draco::IndexType"* %__new_last, %"class.draco::IndexType"** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.5"*, %"class.std::__2::__vector_base.5"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.5", %"class.std::__2::__vector_base.5"* %this1, i32 0, i32 1
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__end_, align 4
  store %"class.draco::IndexType"* %0, %"class.draco::IndexType"** %__soon_to_be_end, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__new_last.addr, align 4
  %2 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__soon_to_be_end, align 4
  %cmp = icmp ne %"class.draco::IndexType"* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.9"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.5"* %this1) #8
  %3 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__soon_to_be_end, align 4
  %incdec.ptr = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %3, i32 -1
  store %"class.draco::IndexType"* %incdec.ptr, %"class.draco::IndexType"** %__soon_to_be_end, align 4
  %call2 = call %"class.draco::IndexType"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType"* %incdec.ptr) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE7destroyIS5_EEvRS6_PT_(%"class.std::__2::allocator.9"* nonnull align 1 dereferenceable(1) %call, %"class.draco::IndexType"* %call2)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %4 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__new_last.addr, align 4
  %__end_3 = getelementptr inbounds %"class.std::__2::__vector_base.5", %"class.std::__2::__vector_base.5"* %this1, i32 0, i32 1
  store %"class.draco::IndexType"* %4, %"class.draco::IndexType"** %__end_3, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_shrinkEm(%"class.std::__2::vector.4"* %this, i32 %__old_size) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.4"*, align 4
  %__old_size.addr = alloca i32, align 4
  store %"class.std::__2::vector.4"* %this, %"class.std::__2::vector.4"** %this.addr, align 4
  store i32 %__old_size, i32* %__old_size.addr, align 4
  %this1 = load %"class.std::__2::vector.4"*, %"class.std::__2::vector.4"** %this.addr, align 4
  %call = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.4"* %this1) #8
  %0 = bitcast %"class.draco::IndexType"* %call to i8*
  %call2 = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.4"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.4"* %this1) #8
  %add.ptr = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %call2, i32 %call3
  %1 = bitcast %"class.draco::IndexType"* %add.ptr to i8*
  %call4 = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.4"* %this1) #8
  %2 = load i32, i32* %__old_size.addr, align 4
  %add.ptr5 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %call4, i32 %2
  %3 = bitcast %"class.draco::IndexType"* %add.ptr5 to i8*
  %call6 = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.4"* %this1) #8
  %call7 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.4"* %this1) #8
  %add.ptr8 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %call6, i32 %call7
  %4 = bitcast %"class.draco::IndexType"* %add.ptr8 to i8*
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_(%"class.std::__2::vector.4"* %this1, i8* %0, i8* %1, i8* %3, i8* %4) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEEE5clearEv(%"class.draco::IndexTypeVector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector"*, align 4
  store %"class.draco::IndexTypeVector"* %this, %"class.draco::IndexTypeVector"** %this.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector"*, %"class.draco::IndexTypeVector"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector", %"class.draco::IndexTypeVector"* %this1, i32 0, i32 0
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5clearEv(%"class.std::__2::vector.4"* %vector_) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5clearEv(%"class.std::__2::vector.4"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.4"*, align 4
  %__old_size = alloca i32, align 4
  store %"class.std::__2::vector.4"* %this, %"class.std::__2::vector.4"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.4"*, %"class.std::__2::vector.4"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.4"* %this1) #8
  store i32 %call, i32* %__old_size, align 4
  %0 = bitcast %"class.std::__2::vector.4"* %this1 to %"class.std::__2::__vector_base.5"*
  call void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5clearEv(%"class.std::__2::__vector_base.5"* %0) #8
  %1 = load i32, i32* %__old_size, align 4
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_shrinkEm(%"class.std::__2::vector.4"* %this1, i32 %1) #8
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector.4"* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5clearEv(%"class.std::__2::__vector_base.5"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.5"*, align 4
  store %"class.std::__2::__vector_base.5"* %this, %"class.std::__2::__vector_base.5"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.5"*, %"class.std::__2::__vector_base.5"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.5", %"class.std::__2::__vector_base.5"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  call void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_(%"class.std::__2::__vector_base.5"* %this1, %"class.draco::IndexType"* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::AttributeTransformData"** @_ZNKSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.12"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.12"*, align 4
  store %"class.std::__2::__compressed_pair.12"* %this, %"class.std::__2::__compressed_pair.12"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.12"*, %"class.std::__2::__compressed_pair.12"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.12"* %this1 to %"struct.std::__2::__compressed_pair_elem.13"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributeTransformData"** @_ZNKSt3__222__compressed_pair_elemIPN5draco22AttributeTransformDataELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.13"* %0) #8
  ret %"class.draco::AttributeTransformData"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::AttributeTransformData"** @_ZNKSt3__222__compressed_pair_elemIPN5draco22AttributeTransformDataELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.13"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.13"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.13"* %this, %"struct.std::__2::__compressed_pair_elem.13"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.13"*, %"struct.std::__2::__compressed_pair_elem.13"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.13", %"struct.std::__2::__compressed_pair_elem.13"* %this1, i32 0, i32 0
  ret %"class.draco::AttributeTransformData"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.12"* @_ZNSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEEC2IRS3_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.12"* returned %this, %"class.draco::AttributeTransformData"** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.12"*, align 4
  %__t1.addr = alloca %"class.draco::AttributeTransformData"**, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.12"* %this, %"class.std::__2::__compressed_pair.12"** %this.addr, align 4
  store %"class.draco::AttributeTransformData"** %__t1, %"class.draco::AttributeTransformData"*** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.12"*, %"class.std::__2::__compressed_pair.12"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.12"* %this1 to %"struct.std::__2::__compressed_pair_elem.13"*
  %1 = load %"class.draco::AttributeTransformData"**, %"class.draco::AttributeTransformData"*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributeTransformData"** @_ZNSt3__27forwardIRPN5draco22AttributeTransformDataEEEOT_RNS_16remove_referenceIS5_E4typeE(%"class.draco::AttributeTransformData"** nonnull align 4 dereferenceable(4) %1) #8
  %call2 = call %"struct.std::__2::__compressed_pair_elem.13"* @_ZNSt3__222__compressed_pair_elemIPN5draco22AttributeTransformDataELi0ELb0EEC2IRS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.13"* %0, %"class.draco::AttributeTransformData"** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.12"* %this1 to %"struct.std::__2::__compressed_pair_elem.14"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #8
  %call4 = call %"struct.std::__2::__compressed_pair_elem.14"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco22AttributeTransformDataEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.14"* %2)
  ret %"class.std::__2::__compressed_pair.12"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::AttributeTransformData"** @_ZNSt3__27forwardIRPN5draco22AttributeTransformDataEEEOT_RNS_16remove_referenceIS5_E4typeE(%"class.draco::AttributeTransformData"** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.draco::AttributeTransformData"**, align 4
  store %"class.draco::AttributeTransformData"** %__t, %"class.draco::AttributeTransformData"*** %__t.addr, align 4
  %0 = load %"class.draco::AttributeTransformData"**, %"class.draco::AttributeTransformData"*** %__t.addr, align 4
  ret %"class.draco::AttributeTransformData"** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.13"* @_ZNSt3__222__compressed_pair_elemIPN5draco22AttributeTransformDataELi0ELb0EEC2IRS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.13"* returned %this, %"class.draco::AttributeTransformData"** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.13"*, align 4
  %__u.addr = alloca %"class.draco::AttributeTransformData"**, align 4
  store %"struct.std::__2::__compressed_pair_elem.13"* %this, %"struct.std::__2::__compressed_pair_elem.13"** %this.addr, align 4
  store %"class.draco::AttributeTransformData"** %__u, %"class.draco::AttributeTransformData"*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.13"*, %"struct.std::__2::__compressed_pair_elem.13"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.13", %"struct.std::__2::__compressed_pair_elem.13"* %this1, i32 0, i32 0
  %0 = load %"class.draco::AttributeTransformData"**, %"class.draco::AttributeTransformData"*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributeTransformData"** @_ZNSt3__27forwardIRPN5draco22AttributeTransformDataEEEOT_RNS_16remove_referenceIS5_E4typeE(%"class.draco::AttributeTransformData"** nonnull align 4 dereferenceable(4) %0) #8
  %1 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %call, align 4
  store %"class.draco::AttributeTransformData"* %1, %"class.draco::AttributeTransformData"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.13"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  store %"struct.std::__2::__default_init_tag"* %__t, %"struct.std::__2::__default_init_tag"** %__t.addr, align 4
  %0 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t.addr, align 4
  ret %"struct.std::__2::__default_init_tag"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.14"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco22AttributeTransformDataEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.14"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.14"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.14"* %this, %"struct.std::__2::__compressed_pair_elem.14"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.14"*, %"struct.std::__2::__compressed_pair_elem.14"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.14"* %this1 to %"struct.std::__2::default_delete.15"*
  ret %"struct.std::__2::__compressed_pair_elem.14"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.12"* @_ZNSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEEC2IS3_S5_EEOT_OT0_(%"class.std::__2::__compressed_pair.12"* returned %this, %"class.draco::AttributeTransformData"** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::default_delete.15"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.12"*, align 4
  %__t1.addr = alloca %"class.draco::AttributeTransformData"**, align 4
  %__t2.addr = alloca %"struct.std::__2::default_delete.15"*, align 4
  store %"class.std::__2::__compressed_pair.12"* %this, %"class.std::__2::__compressed_pair.12"** %this.addr, align 4
  store %"class.draco::AttributeTransformData"** %__t1, %"class.draco::AttributeTransformData"*** %__t1.addr, align 4
  store %"struct.std::__2::default_delete.15"* %__t2, %"struct.std::__2::default_delete.15"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.12"*, %"class.std::__2::__compressed_pair.12"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.12"* %this1 to %"struct.std::__2::__compressed_pair_elem.13"*
  %1 = load %"class.draco::AttributeTransformData"**, %"class.draco::AttributeTransformData"*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributeTransformData"** @_ZNSt3__27forwardIPN5draco22AttributeTransformDataEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::AttributeTransformData"** nonnull align 4 dereferenceable(4) %1) #8
  %call2 = call %"struct.std::__2::__compressed_pair_elem.13"* @_ZNSt3__222__compressed_pair_elemIPN5draco22AttributeTransformDataELi0ELb0EEC2IS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.13"* %0, %"class.draco::AttributeTransformData"** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.12"* %this1 to %"struct.std::__2::__compressed_pair_elem.14"*
  %3 = load %"struct.std::__2::default_delete.15"*, %"struct.std::__2::default_delete.15"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.15"* @_ZNSt3__27forwardINS_14default_deleteIN5draco22AttributeTransformDataEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.15"* nonnull align 1 dereferenceable(1) %3) #8
  %call4 = call %"struct.std::__2::__compressed_pair_elem.14"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco22AttributeTransformDataEEELi1ELb1EEC2IS4_vEEOT_(%"struct.std::__2::__compressed_pair_elem.14"* %2, %"struct.std::__2::default_delete.15"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::__compressed_pair.12"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::AttributeTransformData"** @_ZNSt3__27forwardIPN5draco22AttributeTransformDataEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::AttributeTransformData"** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.draco::AttributeTransformData"**, align 4
  store %"class.draco::AttributeTransformData"** %__t, %"class.draco::AttributeTransformData"*** %__t.addr, align 4
  %0 = load %"class.draco::AttributeTransformData"**, %"class.draco::AttributeTransformData"*** %__t.addr, align 4
  ret %"class.draco::AttributeTransformData"** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.13"* @_ZNSt3__222__compressed_pair_elemIPN5draco22AttributeTransformDataELi0ELb0EEC2IS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.13"* returned %this, %"class.draco::AttributeTransformData"** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.13"*, align 4
  %__u.addr = alloca %"class.draco::AttributeTransformData"**, align 4
  store %"struct.std::__2::__compressed_pair_elem.13"* %this, %"struct.std::__2::__compressed_pair_elem.13"** %this.addr, align 4
  store %"class.draco::AttributeTransformData"** %__u, %"class.draco::AttributeTransformData"*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.13"*, %"struct.std::__2::__compressed_pair_elem.13"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.13", %"struct.std::__2::__compressed_pair_elem.13"* %this1, i32 0, i32 0
  %0 = load %"class.draco::AttributeTransformData"**, %"class.draco::AttributeTransformData"*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributeTransformData"** @_ZNSt3__27forwardIPN5draco22AttributeTransformDataEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::AttributeTransformData"** nonnull align 4 dereferenceable(4) %0) #8
  %1 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %call, align 4
  store %"class.draco::AttributeTransformData"* %1, %"class.draco::AttributeTransformData"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.13"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.14"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco22AttributeTransformDataEEELi1ELb1EEC2IS4_vEEOT_(%"struct.std::__2::__compressed_pair_elem.14"* returned %this, %"struct.std::__2::default_delete.15"* nonnull align 1 dereferenceable(1) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.14"*, align 4
  %__u.addr = alloca %"struct.std::__2::default_delete.15"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.14"* %this, %"struct.std::__2::__compressed_pair_elem.14"** %this.addr, align 4
  store %"struct.std::__2::default_delete.15"* %__u, %"struct.std::__2::default_delete.15"** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.14"*, %"struct.std::__2::__compressed_pair_elem.14"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.14"* %this1 to %"struct.std::__2::default_delete.15"*
  %1 = load %"struct.std::__2::default_delete.15"*, %"struct.std::__2::default_delete.15"** %__u.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.15"* @_ZNSt3__27forwardINS_14default_deleteIN5draco22AttributeTransformDataEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.15"* nonnull align 1 dereferenceable(1) %1) #8
  ret %"struct.std::__2::__compressed_pair_elem.14"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.17"* @_ZNSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEEC2IRS3_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.17"* returned %this, %"class.draco::PointAttribute"** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.17"*, align 4
  %__t1.addr = alloca %"class.draco::PointAttribute"**, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.17"* %this, %"class.std::__2::__compressed_pair.17"** %this.addr, align 4
  store %"class.draco::PointAttribute"** %__t1, %"class.draco::PointAttribute"*** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.17"*, %"class.std::__2::__compressed_pair.17"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.17"* %this1 to %"struct.std::__2::__compressed_pair_elem.18"*
  %1 = load %"class.draco::PointAttribute"**, %"class.draco::PointAttribute"*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNSt3__27forwardIRPN5draco14PointAttributeEEEOT_RNS_16remove_referenceIS5_E4typeE(%"class.draco::PointAttribute"** nonnull align 4 dereferenceable(4) %1) #8
  %call2 = call %"struct.std::__2::__compressed_pair_elem.18"* @_ZNSt3__222__compressed_pair_elemIPN5draco14PointAttributeELi0ELb0EEC2IRS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.18"* %0, %"class.draco::PointAttribute"** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.17"* %this1 to %"struct.std::__2::__compressed_pair_elem.19"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #8
  %call4 = call %"struct.std::__2::__compressed_pair_elem.19"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco14PointAttributeEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.19"* %2)
  ret %"class.std::__2::__compressed_pair.17"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNSt3__27forwardIRPN5draco14PointAttributeEEEOT_RNS_16remove_referenceIS5_E4typeE(%"class.draco::PointAttribute"** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.draco::PointAttribute"**, align 4
  store %"class.draco::PointAttribute"** %__t, %"class.draco::PointAttribute"*** %__t.addr, align 4
  %0 = load %"class.draco::PointAttribute"**, %"class.draco::PointAttribute"*** %__t.addr, align 4
  ret %"class.draco::PointAttribute"** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.18"* @_ZNSt3__222__compressed_pair_elemIPN5draco14PointAttributeELi0ELb0EEC2IRS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.18"* returned %this, %"class.draco::PointAttribute"** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.18"*, align 4
  %__u.addr = alloca %"class.draco::PointAttribute"**, align 4
  store %"struct.std::__2::__compressed_pair_elem.18"* %this, %"struct.std::__2::__compressed_pair_elem.18"** %this.addr, align 4
  store %"class.draco::PointAttribute"** %__u, %"class.draco::PointAttribute"*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.18"*, %"struct.std::__2::__compressed_pair_elem.18"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.18", %"struct.std::__2::__compressed_pair_elem.18"* %this1, i32 0, i32 0
  %0 = load %"class.draco::PointAttribute"**, %"class.draco::PointAttribute"*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNSt3__27forwardIRPN5draco14PointAttributeEEEOT_RNS_16remove_referenceIS5_E4typeE(%"class.draco::PointAttribute"** nonnull align 4 dereferenceable(4) %0) #8
  %1 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %call, align 4
  store %"class.draco::PointAttribute"* %1, %"class.draco::PointAttribute"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.18"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.19"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco14PointAttributeEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.19"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.19"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.19"* %this, %"struct.std::__2::__compressed_pair_elem.19"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.19"*, %"struct.std::__2::__compressed_pair_elem.19"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.19"* %this1 to %"struct.std::__2::default_delete.20"*
  ret %"struct.std::__2::__compressed_pair_elem.19"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.16"* %this, %"class.draco::PointAttribute"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.16"*, align 4
  %__p.addr = alloca %"class.draco::PointAttribute"*, align 4
  %__tmp = alloca %"class.draco::PointAttribute"*, align 4
  store %"class.std::__2::unique_ptr.16"* %this, %"class.std::__2::unique_ptr.16"** %this.addr, align 4
  store %"class.draco::PointAttribute"* %__p, %"class.draco::PointAttribute"** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.16"*, %"class.std::__2::unique_ptr.16"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.16", %"class.std::__2::unique_ptr.16"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.17"* %__ptr_) #8
  %0 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %call, align 4
  store %"class.draco::PointAttribute"* %0, %"class.draco::PointAttribute"** %__tmp, align 4
  %1 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %__p.addr, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr.16", %"class.std::__2::unique_ptr.16"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.17"* %__ptr_2) #8
  store %"class.draco::PointAttribute"* %1, %"class.draco::PointAttribute"** %call3, align 4
  %2 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %__tmp, align 4
  %tobool = icmp ne %"class.draco::PointAttribute"* %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__ptr_4 = getelementptr inbounds %"class.std::__2::unique_ptr.16", %"class.std::__2::unique_ptr.16"* %this1, i32 0, i32 0
  %call5 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.20"* @_ZNSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.17"* %__ptr_4) #8
  %3 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %__tmp, align 4
  call void @_ZNKSt3__214default_deleteIN5draco14PointAttributeEEclEPS2_(%"struct.std::__2::default_delete.20"* %call5, %"class.draco::PointAttribute"* %3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.17"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.17"*, align 4
  store %"class.std::__2::__compressed_pair.17"* %this, %"class.std::__2::__compressed_pair.17"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.17"*, %"class.std::__2::__compressed_pair.17"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.17"* %this1 to %"struct.std::__2::__compressed_pair_elem.18"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNSt3__222__compressed_pair_elemIPN5draco14PointAttributeELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.18"* %0) #8
  ret %"class.draco::PointAttribute"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.20"* @_ZNSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.17"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.17"*, align 4
  store %"class.std::__2::__compressed_pair.17"* %this, %"class.std::__2::__compressed_pair.17"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.17"*, %"class.std::__2::__compressed_pair.17"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.17"* %this1 to %"struct.std::__2::__compressed_pair_elem.19"*
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.20"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco14PointAttributeEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.19"* %0) #8
  ret %"struct.std::__2::default_delete.20"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__214default_deleteIN5draco14PointAttributeEEclEPS2_(%"struct.std::__2::default_delete.20"* %this, %"class.draco::PointAttribute"* %__ptr) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::default_delete.20"*, align 4
  %__ptr.addr = alloca %"class.draco::PointAttribute"*, align 4
  store %"struct.std::__2::default_delete.20"* %this, %"struct.std::__2::default_delete.20"** %this.addr, align 4
  store %"class.draco::PointAttribute"* %__ptr, %"class.draco::PointAttribute"** %__ptr.addr, align 4
  %this1 = load %"struct.std::__2::default_delete.20"*, %"struct.std::__2::default_delete.20"** %this.addr, align 4
  %0 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %__ptr.addr, align 4
  %isnull = icmp eq %"class.draco::PointAttribute"* %0, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %entry
  %call = call %"class.draco::PointAttribute"* @_ZN5draco14PointAttributeD2Ev(%"class.draco::PointAttribute"* %0) #8
  %1 = bitcast %"class.draco::PointAttribute"* %0 to i8*
  call void @_ZdlPv(i8* %1) #9
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNSt3__222__compressed_pair_elemIPN5draco14PointAttributeELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.18"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.18"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.18"* %this, %"struct.std::__2::__compressed_pair_elem.18"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.18"*, %"struct.std::__2::__compressed_pair_elem.18"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.18", %"struct.std::__2::__compressed_pair_elem.18"* %this1, i32 0, i32 0
  ret %"class.draco::PointAttribute"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.20"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco14PointAttributeEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.19"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.19"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.19"* %this, %"struct.std::__2::__compressed_pair_elem.19"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.19"*, %"struct.std::__2::__compressed_pair_elem.19"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.19"* %this1 to %"struct.std::__2::default_delete.20"*
  ret %"struct.std::__2::default_delete.20"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::PointAttribute"* @_ZN5draco14PointAttributeD2Ev(%"class.draco::PointAttribute"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointAttribute"*, align 4
  store %"class.draco::PointAttribute"* %this, %"class.draco::PointAttribute"** %this.addr, align 4
  %this1 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %this.addr, align 4
  %attribute_transform_data_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 6
  %call = call %"class.std::__2::unique_ptr.11"* @_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.11"* %attribute_transform_data_) #8
  %indices_map_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 2
  %call2 = call %"class.draco::IndexTypeVector"* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEEED2Ev(%"class.draco::IndexTypeVector"* %indices_map_) #8
  %attribute_buffer_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 1
  %call3 = call %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr"* %attribute_buffer_) #8
  ret %"class.draco::PointAttribute"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexTypeVector"* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEEED2Ev(%"class.draco::IndexTypeVector"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector"*, align 4
  store %"class.draco::IndexTypeVector"* %this, %"class.draco::IndexTypeVector"** %this.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector"*, %"class.draco::IndexTypeVector"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector", %"class.draco::IndexTypeVector"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::vector.4"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEED2Ev(%"class.std::__2::vector.4"* %vector_) #8
  ret %"class.draco::IndexTypeVector"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  call void @_ZNSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr"* %this1, %"class.draco::DataBuffer"* null) #8
  ret %"class.std::__2::unique_ptr"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector.4"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEED2Ev(%"class.std::__2::vector.4"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.4"*, align 4
  store %"class.std::__2::vector.4"* %this, %"class.std::__2::vector.4"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.4"*, %"class.std::__2::vector.4"** %this.addr, align 4
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_deleteEv(%"class.std::__2::vector.4"* %this1) #8
  %0 = bitcast %"class.std::__2::vector.4"* %this1 to %"class.std::__2::__vector_base.5"*
  %call = call %"class.std::__2::__vector_base.5"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEED2Ev(%"class.std::__2::__vector_base.5"* %0) #8
  ret %"class.std::__2::vector.4"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base.5"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEED2Ev(%"class.std::__2::__vector_base.5"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__vector_base.5"*, align 4
  %this.addr = alloca %"class.std::__2::__vector_base.5"*, align 4
  store %"class.std::__2::__vector_base.5"* %this, %"class.std::__2::__vector_base.5"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.5"*, %"class.std::__2::__vector_base.5"** %this.addr, align 4
  store %"class.std::__2::__vector_base.5"* %this1, %"class.std::__2::__vector_base.5"** %retval, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.5", %"class.std::__2::__vector_base.5"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  %cmp = icmp ne %"class.draco::IndexType"* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5clearEv(%"class.std::__2::__vector_base.5"* %this1) #8
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.9"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.5"* %this1) #8
  %__begin_2 = getelementptr inbounds %"class.std::__2::__vector_base.5", %"class.std::__2::__vector_base.5"* %this1, i32 0, i32 0
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_2, align 4
  %call3 = call i32 @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::__vector_base.5"* %this1) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE10deallocateERS6_PS5_m(%"class.std::__2::allocator.9"* nonnull align 1 dereferenceable(1) %call, %"class.draco::IndexType"* %1, i32 %call3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"class.std::__2::__vector_base.5"*, %"class.std::__2::__vector_base.5"** %retval, align 4
  ret %"class.std::__2::__vector_base.5"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr"* %this, %"class.draco::DataBuffer"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  %__p.addr = alloca %"class.draco::DataBuffer"*, align 4
  %__tmp = alloca %"class.draco::DataBuffer"*, align 4
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  store %"class.draco::DataBuffer"* %__p, %"class.draco::DataBuffer"** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::DataBuffer"** @_ZNSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.1"* %__ptr_) #8
  %0 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %call, align 4
  store %"class.draco::DataBuffer"* %0, %"class.draco::DataBuffer"** %__tmp, align 4
  %1 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %__p.addr, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::DataBuffer"** @_ZNSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.1"* %__ptr_2) #8
  store %"class.draco::DataBuffer"* %1, %"class.draco::DataBuffer"** %call3, align 4
  %2 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %__tmp, align 4
  %tobool = icmp ne %"class.draco::DataBuffer"* %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__ptr_4 = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call5 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete"* @_ZNSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.1"* %__ptr_4) #8
  %3 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %__tmp, align 4
  call void @_ZNKSt3__214default_deleteIN5draco10DataBufferEEclEPS2_(%"struct.std::__2::default_delete"* %call5, %"class.draco::DataBuffer"* %3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::DataBuffer"** @_ZNSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.1"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.1"*, align 4
  store %"class.std::__2::__compressed_pair.1"* %this, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.1"*, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.1"* %this1 to %"struct.std::__2::__compressed_pair_elem.2"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::DataBuffer"** @_ZNSt3__222__compressed_pair_elemIPN5draco10DataBufferELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.2"* %0) #8
  ret %"class.draco::DataBuffer"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete"* @_ZNSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.1"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.1"*, align 4
  store %"class.std::__2::__compressed_pair.1"* %this, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.1"*, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.1"* %this1 to %"struct.std::__2::__compressed_pair_elem.3"*
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco10DataBufferEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.3"* %0) #8
  ret %"struct.std::__2::default_delete"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__214default_deleteIN5draco10DataBufferEEclEPS2_(%"struct.std::__2::default_delete"* %this, %"class.draco::DataBuffer"* %__ptr) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::default_delete"*, align 4
  %__ptr.addr = alloca %"class.draco::DataBuffer"*, align 4
  store %"struct.std::__2::default_delete"* %this, %"struct.std::__2::default_delete"** %this.addr, align 4
  store %"class.draco::DataBuffer"* %__ptr, %"class.draco::DataBuffer"** %__ptr.addr, align 4
  %this1 = load %"struct.std::__2::default_delete"*, %"struct.std::__2::default_delete"** %this.addr, align 4
  %0 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %__ptr.addr, align 4
  %isnull = icmp eq %"class.draco::DataBuffer"* %0, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %entry
  %call = call %"class.draco::DataBuffer"* @_ZN5draco10DataBufferD2Ev(%"class.draco::DataBuffer"* %0) #8
  %1 = bitcast %"class.draco::DataBuffer"* %0 to i8*
  call void @_ZdlPv(i8* %1) #9
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::DataBuffer"** @_ZNSt3__222__compressed_pair_elemIPN5draco10DataBufferELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.2"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.2"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.2"* %this, %"struct.std::__2::__compressed_pair_elem.2"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.2"*, %"struct.std::__2::__compressed_pair_elem.2"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.2", %"struct.std::__2::__compressed_pair_elem.2"* %this1, i32 0, i32 0
  ret %"class.draco::DataBuffer"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco10DataBufferEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.3"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.3"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.3"* %this, %"struct.std::__2::__compressed_pair_elem.3"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.3"*, %"struct.std::__2::__compressed_pair_elem.3"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.3"* %this1 to %"struct.std::__2::default_delete"*
  ret %"struct.std::__2::default_delete"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNKSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.17"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.17"*, align 4
  store %"class.std::__2::__compressed_pair.17"* %this, %"class.std::__2::__compressed_pair.17"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.17"*, %"class.std::__2::__compressed_pair.17"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.17"* %this1 to %"struct.std::__2::__compressed_pair_elem.18"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNKSt3__222__compressed_pair_elemIPN5draco14PointAttributeELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.18"* %0) #8
  ret %"class.draco::PointAttribute"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNKSt3__222__compressed_pair_elemIPN5draco14PointAttributeELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.18"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.18"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.18"* %this, %"struct.std::__2::__compressed_pair_elem.18"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.18"*, %"struct.std::__2::__compressed_pair_elem.18"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.18", %"struct.std::__2::__compressed_pair_elem.18"* %this1, i32 0, i32 0
  ret %"class.draco::PointAttribute"** %__value_
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nobuiltin allocsize(0) "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { argmemonly nounwind willreturn }
attributes #5 = { noreturn "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { noinline noreturn nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { builtin allocsize(0) }
attributes #8 = { nounwind }
attributes #9 = { builtin nounwind }
attributes #10 = { noreturn }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
