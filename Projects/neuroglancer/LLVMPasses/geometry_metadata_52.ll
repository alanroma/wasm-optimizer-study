; ModuleID = './draco/src/draco/metadata/geometry_metadata.cc'
source_filename = "./draco/src/draco/metadata/geometry_metadata.cc"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%"class.draco::AttributeMetadata" = type { %"class.draco::Metadata", i32 }
%"class.draco::Metadata" = type { %"class.std::__2::unordered_map", %"class.std::__2::unordered_map.14" }
%"class.std::__2::unordered_map" = type { %"class.std::__2::__hash_table" }
%"class.std::__2::__hash_table" = type { %"class.std::__2::unique_ptr", %"class.std::__2::__compressed_pair.4", %"class.std::__2::__compressed_pair.9", %"class.std::__2::__compressed_pair.11" }
%"class.std::__2::unique_ptr" = type { %"class.std::__2::__compressed_pair" }
%"class.std::__2::__compressed_pair" = type { %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem.0" }
%"struct.std::__2::__compressed_pair_elem" = type { %"struct.std::__2::__hash_node_base"** }
%"struct.std::__2::__hash_node_base" = type { %"struct.std::__2::__hash_node_base"* }
%"struct.std::__2::__compressed_pair_elem.0" = type { %"class.std::__2::__bucket_list_deallocator" }
%"class.std::__2::__bucket_list_deallocator" = type { %"class.std::__2::__compressed_pair.1" }
%"class.std::__2::__compressed_pair.1" = type { %"struct.std::__2::__compressed_pair_elem.2" }
%"struct.std::__2::__compressed_pair_elem.2" = type { i32 }
%"class.std::__2::__compressed_pair.4" = type { %"struct.std::__2::__compressed_pair_elem.5" }
%"struct.std::__2::__compressed_pair_elem.5" = type { %"struct.std::__2::__hash_node_base" }
%"class.std::__2::__compressed_pair.9" = type { %"struct.std::__2::__compressed_pair_elem.2" }
%"class.std::__2::__compressed_pair.11" = type { %"struct.std::__2::__compressed_pair_elem.12" }
%"struct.std::__2::__compressed_pair_elem.12" = type { float }
%"class.std::__2::unordered_map.14" = type { %"class.std::__2::__hash_table.15" }
%"class.std::__2::__hash_table.15" = type { %"class.std::__2::unique_ptr.16", %"class.std::__2::__compressed_pair.26", %"class.std::__2::__compressed_pair.31", %"class.std::__2::__compressed_pair.34" }
%"class.std::__2::unique_ptr.16" = type { %"class.std::__2::__compressed_pair.17" }
%"class.std::__2::__compressed_pair.17" = type { %"struct.std::__2::__compressed_pair_elem.18", %"struct.std::__2::__compressed_pair_elem.20" }
%"struct.std::__2::__compressed_pair_elem.18" = type { %"struct.std::__2::__hash_node_base.19"** }
%"struct.std::__2::__hash_node_base.19" = type { %"struct.std::__2::__hash_node_base.19"* }
%"struct.std::__2::__compressed_pair_elem.20" = type { %"class.std::__2::__bucket_list_deallocator.21" }
%"class.std::__2::__bucket_list_deallocator.21" = type { %"class.std::__2::__compressed_pair.22" }
%"class.std::__2::__compressed_pair.22" = type { %"struct.std::__2::__compressed_pair_elem.2" }
%"class.std::__2::__compressed_pair.26" = type { %"struct.std::__2::__compressed_pair_elem.27" }
%"struct.std::__2::__compressed_pair_elem.27" = type { %"struct.std::__2::__hash_node_base.19" }
%"class.std::__2::__compressed_pair.31" = type { %"struct.std::__2::__compressed_pair_elem.2" }
%"class.std::__2::__compressed_pair.34" = type { %"struct.std::__2::__compressed_pair_elem.12" }
%"class.draco::GeometryMetadata" = type { %"class.draco::Metadata", %"class.std::__2::vector" }
%"class.std::__2::vector" = type { %"class.std::__2::__vector_base" }
%"class.std::__2::__vector_base" = type { %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"*, %"class.std::__2::__compressed_pair.41" }
%"class.std::__2::unique_ptr.37" = type { %"class.std::__2::__compressed_pair.38" }
%"class.std::__2::__compressed_pair.38" = type { %"struct.std::__2::__compressed_pair_elem.39" }
%"struct.std::__2::__compressed_pair_elem.39" = type { %"class.draco::AttributeMetadata"* }
%"class.std::__2::__compressed_pair.41" = type { %"struct.std::__2::__compressed_pair_elem.42" }
%"struct.std::__2::__compressed_pair_elem.42" = type { %"class.std::__2::unique_ptr.37"* }
%"class.std::__2::basic_string" = type { %"class.std::__2::__compressed_pair.46" }
%"class.std::__2::__compressed_pair.46" = type { %"struct.std::__2::__compressed_pair_elem.47" }
%"struct.std::__2::__compressed_pair_elem.47" = type { %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep" }
%"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep" = type { %union.anon }
%union.anon = type { %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long" }
%"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long" = type { i8*, i32, i32 }
%"class.std::__2::__wrap_iter" = type { %"class.std::__2::unique_ptr.37"* }
%"struct.std::__2::__default_init_tag" = type { i8 }
%"class.std::__2::__basic_string_common" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.48" = type { i8 }
%"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__raw" = type { [3 x i32] }
%"class.std::__2::allocator.49" = type { i8 }
%"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short" = type { [11 x i8], %struct.anon }
%struct.anon = type { i8 }
%"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributeMetadata, std::__2::default_delete<draco::AttributeMetadata>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributeMetadata, std::__2::default_delete<draco::AttributeMetadata>>>>::_ConstructTransaction" = type { %"class.std::__2::vector"*, %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"* }
%"class.std::__2::allocator.44" = type { i8 }
%"struct.std::__2::__split_buffer" = type { %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"*, %"class.std::__2::__compressed_pair.51" }
%"class.std::__2::__compressed_pair.51" = type { %"struct.std::__2::__compressed_pair_elem.42", %"struct.std::__2::__compressed_pair_elem.52" }
%"struct.std::__2::__compressed_pair_elem.52" = type { %"class.std::__2::allocator.44"* }
%"struct.std::__2::integral_constant" = type { i8 }
%"struct.std::__2::__has_construct" = type { i8 }
%"struct.std::__2::default_delete" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.40" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.43" = type { i8 }
%"class.std::__2::__vector_base_common" = type { i8 }
%"class.std::__2::__split_buffer_common" = type { i8 }
%"struct.std::__2::__less" = type { i8 }
%"struct.std::__2::__has_max_size" = type { i8 }
%"struct.std::__2::integral_constant.53" = type { i8 }
%"struct.std::__2::__has_destroy" = type { i8 }
%"class.std::__2::allocator.29" = type { i8 }
%"struct.std::__2::__hash_node" = type { %"struct.std::__2::__hash_node_base.19", i32, %"struct.std::__2::__hash_value_type" }
%"struct.std::__2::__hash_value_type" = type { %"struct.std::__2::pair" }
%"struct.std::__2::pair" = type { %"class.std::__2::basic_string", %"class.std::__2::unique_ptr.54" }
%"class.std::__2::unique_ptr.54" = type { %"class.std::__2::__compressed_pair.55" }
%"class.std::__2::__compressed_pair.55" = type { %"struct.std::__2::__compressed_pair_elem.56" }
%"struct.std::__2::__compressed_pair_elem.56" = type { %"class.draco::Metadata"* }
%"struct.std::__2::__has_destroy.59" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.28" = type { i8 }
%"struct.std::__2::default_delete.58" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.57" = type { i8 }
%"class.std::__2::allocator.24" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.23" = type { i8 }
%"class.std::__2::allocator.7" = type { i8 }
%"struct.std::__2::__hash_node.60" = type { %"struct.std::__2::__hash_node_base", i32, %"struct.std::__2::__hash_value_type.61" }
%"struct.std::__2::__hash_value_type.61" = type { %"struct.std::__2::pair.62" }
%"struct.std::__2::pair.62" = type { %"class.std::__2::basic_string", %"class.draco::EntryValue" }
%"class.draco::EntryValue" = type { %"class.std::__2::vector.63" }
%"class.std::__2::vector.63" = type { %"class.std::__2::__vector_base.64" }
%"class.std::__2::__vector_base.64" = type { i8*, i8*, %"class.std::__2::__compressed_pair.65" }
%"class.std::__2::__compressed_pair.65" = type { %"struct.std::__2::__compressed_pair_elem.66" }
%"struct.std::__2::__compressed_pair_elem.66" = type { i8* }
%"struct.std::__2::__has_destroy.70" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.6" = type { i8 }
%"class.std::__2::allocator.68" = type { i8 }
%"struct.std::__2::__has_destroy.71" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.67" = type { i8 }
%"class.std::__2::allocator" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.3" = type { i8 }

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5beginEv = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE3endEv = comdat any

$_ZNSt3__2neIPKNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEEbRKNS_11__wrap_iterIT_EESD_ = comdat any

$_ZNKSt3__211__wrap_iterIPKNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEdeEv = comdat any

$_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2Ev = comdat any

$_ZNKSt3__210unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS2_EEEptEv = comdat any

$_ZNSt3__2eqINS_9allocatorIcEEEEbRKNS_12basic_stringIcNS_11char_traitsIcEET_EES9_ = comdat any

$_ZNKSt3__210unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS2_EEE3getEv = comdat any

$_ZNSt3__211__wrap_iterIPKNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEppEv = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE9push_backEOS6_ = comdat any

$_ZNSt3__24moveIRNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOS9_ = comdat any

$_ZNSt3__2eqIPKNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEES8_EEbRKNS_11__wrap_iterIT_EERKNS9_IT0_EE = comdat any

$_ZNKSt3__211__wrap_iterIPKNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEE4baseEv = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco17AttributeMetadataENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPN5draco17AttributeMetadataELi0ELb0EE5__getEv = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE11__make_iterEPKS6_ = comdat any

$_ZNSt3__211__wrap_iterIPKNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEC2ES8_ = comdat any

$_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_EC2INS_18__default_init_tagESA_EEOT_OT0_ = comdat any

$_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__zeroEv = comdat any

$_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIcEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__29allocatorIcEC2Ev = comdat any

$_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EE5__getEv = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4sizeEv = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE9__is_longEv = comdat any

$_ZNSt3__211char_traitsIcE7compareEPKcS3_m = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE15__get_long_sizeEv = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE16__get_short_sizeEv = comdat any

$_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EE5__getEv = comdat any

$_ZNSt3__212__to_addressIKcEEPT_S3_ = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE13__get_pointerEv = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE18__get_long_pointerEv = comdat any

$_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE19__get_short_pointerEv = comdat any

$_ZNSt3__214pointer_traitsIPKcE10pointer_toERS1_ = comdat any

$_ZNSt3__29addressofIKcEEPT_RS2_ = comdat any

$_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE9__end_capEv = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE22__construct_one_at_endIJS6_EEEvDpOT_ = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE21__push_back_slow_pathIS6_EEvOT_ = comdat any

$_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5firstEv = comdat any

$_ZNSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE21_ConstructTransactionC2ERS9_m = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEEE9constructIS7_JS7_EEEvRS8_PT_DpOT0_ = comdat any

$_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv = comdat any

$_ZNSt3__212__to_addressINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEEPT_S8_ = comdat any

$_ZNSt3__27forwardINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEEOT_RNS_16remove_referenceIS7_E4typeE = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE21_ConstructTransactionD2Ev = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEEE11__constructIS7_JS7_EEEvNS_17integral_constantIbLb1EEERS8_PT_DpOT0_ = comdat any

$_ZNSt3__29allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEE9constructIS6_JS6_EEEvPT_DpOT0_ = comdat any

$_ZNSt3__210unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS2_EEEC2EOS5_ = comdat any

$_ZNSt3__210unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS2_EEE7releaseEv = comdat any

$_ZNSt3__27forwardINS_14default_deleteIN5draco17AttributeMetadataEEEEEOT_RNS_16remove_referenceIS5_E4typeE = comdat any

$_ZNSt3__210unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS2_EEE11get_deleterEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco17AttributeMetadataENS_14default_deleteIS2_EEEC2IS3_S5_EEOT_OT0_ = comdat any

$_ZNSt3__217__compressed_pairIPN5draco17AttributeMetadataENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco17AttributeMetadataELi0ELb0EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco17AttributeMetadataENS_14default_deleteIS2_EEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco17AttributeMetadataEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__27forwardIPN5draco17AttributeMetadataEEEOT_RNS_16remove_referenceIS4_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco17AttributeMetadataELi0ELb0EEC2IS3_vEEOT_ = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco17AttributeMetadataEEELi1ELb1EEC2IS4_vEEOT_ = comdat any

$_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEELi1ELb1EE5__getEv = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE11__recommendEm = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv = comdat any

$_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEEC2EmmS9_ = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS6_RS8_EE = comdat any

$_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEED2Ev = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8max_sizeEv = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv = comdat any

$_ZNSt3__23maxImEERKT_S3_S3_ = comdat any

$_ZNSt3__23minImEERKT_S3_S3_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEEE8max_sizeERKS8_ = comdat any

$_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv = comdat any

$_ZNSt3__214numeric_limitsIlE3maxEv = comdat any

$_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_ = comdat any

$_ZNKSt3__26__lessImmEclERKmS3_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS8_ = comdat any

$_ZNKSt3__29allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEE8max_sizeEv = comdat any

$_ZNKSt3__217__compressed_pairIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE6secondEv = comdat any

$_ZNKSt3__222__compressed_pair_elemINS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv = comdat any

$_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__23maxImNS_6__lessImmEEEERKT_S5_S5_T0_ = comdat any

$_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEEC2IDnSA_EEOT_OT0_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEEE8allocateERS8_m = comdat any

$_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE7__allocEv = comdat any

$_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE9__end_capEv = comdat any

$_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEELi0ELb0EEC2IDnvEEOT_ = comdat any

$_ZNSt3__27forwardIRNS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEEEEOT_RNS_16remove_referenceISA_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIRNS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEELi1ELb0EEC2IS9_vEEOT_ = comdat any

$_ZNSt3__29allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEE8allocateEmPKv = comdat any

$_ZNSt3__220__throw_length_errorEPKc = comdat any

$_ZNSt3__217__libcpp_allocateEmm = comdat any

$_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemIRNS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEELi1ELb0EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE5firstEv = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__annotate_deleteEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEEE46__construct_backward_with_exception_guaranteesIPS7_EEvRS8_T_SD_RSD_ = comdat any

$_ZNSt3__24swapIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS9_EE5valueEvE4typeERS9_SC_ = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE14__annotate_newEm = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE26__invalidate_all_iteratorsEv = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE31__annotate_contiguous_containerEPKvSB_SB_SB_ = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv = comdat any

$_ZNSt3__24moveIRPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOSA_ = comdat any

$_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE5clearEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEEE10deallocateERS8_PS7_m = comdat any

$_ZNKSt3__214__split_bufferINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE8capacityEv = comdat any

$_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE17__destruct_at_endEPS6_ = comdat any

$_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE17__destruct_at_endEPS6_NS_17integral_constantIbLb0EEE = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEEE7destroyIS7_EEvRS8_PT_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEEE9__destroyIS7_EEvNS_17integral_constantIbLb1EEERS8_PT_ = comdat any

$_ZNSt3__29allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEE7destroyEPS6_ = comdat any

$_ZNSt3__210unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS2_EEED2Ev = comdat any

$_ZNSt3__210unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS2_EEE5resetEPS2_ = comdat any

$_ZNKSt3__214default_deleteIN5draco17AttributeMetadataEEclEPS2_ = comdat any

$_ZN5draco17AttributeMetadataD2Ev = comdat any

$_ZNSt3__213unordered_mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteIS9_EEEENS_4hashIS6_EENS_8equal_toIS6_EENS4_INS_4pairIKS6_SC_EEEEED2Ev = comdat any

$_ZNSt3__213unordered_mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueENS_4hashIS6_EENS_8equal_toIS6_EENS4_INS_4pairIKS6_S8_EEEEED2Ev = comdat any

$_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEEENS_22__unordered_map_hasherIS7_SE_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SE_NS_8equal_toIS7_EELb1EEENS5_ISE_EEED2Ev = comdat any

$_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEEENS_22__unordered_map_hasherIS7_SE_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SE_NS_8equal_toIS7_EELb1EEENS5_ISE_EEE17__deallocate_nodeEPNS_16__hash_node_baseIPNS_11__hash_nodeISE_PvEEEE = comdat any

$_ZNSt3__217__compressed_pairINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEENS7_ISI_EEE5firstEv = comdat any

$_ZNSt3__210unique_ptrIA_PNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS0_IN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISK_EEEEED2Ev = comdat any

$_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEEENS_22__unordered_map_hasherIS7_SE_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SE_NS_8equal_toIS7_EELb1EEENS5_ISE_EEE12__node_allocEv = comdat any

$_ZNSt3__216__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEE8__upcastEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEEE7destroyINS_4pairIKS8_SE_EEEEvRSI_PT_ = comdat any

$_ZNSt3__222__hash_key_value_typesINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEEEE9__get_ptrERSE_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEEE10deallocateERSI_PSH_m = comdat any

$_ZNSt3__217__compressed_pairINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEENS7_ISI_EEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__214pointer_traitsIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEE10pointer_toERSK_ = comdat any

$_ZNSt3__29addressofINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEPT_RSL_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEEE9__destroyINS_4pairIKS8_SE_EEEEvNS_17integral_constantIbLb0EEERSI_PT_ = comdat any

$_ZNSt3__24pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEED2Ev = comdat any

$_ZNSt3__210unique_ptrIN5draco8MetadataENS_14default_deleteIS2_EEED2Ev = comdat any

$_ZNSt3__210unique_ptrIN5draco8MetadataENS_14default_deleteIS2_EEE5resetEPS2_ = comdat any

$_ZNSt3__217__compressed_pairIPN5draco8MetadataENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco8MetadataENS_14default_deleteIS2_EEE6secondEv = comdat any

$_ZNKSt3__214default_deleteIN5draco8MetadataEEclEPS2_ = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco8MetadataELi0ELb0EE5__getEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco8MetadataEEELi1ELb1EE5__getEv = comdat any

$_ZN5draco8MetadataD2Ev = comdat any

$_ZNSt3__29addressofINS_4pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEEEPT_RSG_ = comdat any

$_ZNSt3__217__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteIS9_EEEEE11__get_valueEv = comdat any

$_ZNSt3__29allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEEEPvEEE10deallocateEPSG_m = comdat any

$_ZNSt3__219__libcpp_deallocateEPvmm = comdat any

$_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm = comdat any

$_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm = comdat any

$_ZNSt3__217_DeallocateCaller9__do_callEPv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__210unique_ptrIA_PNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS0_IN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISK_EEEEE5resetEDn = comdat any

$_ZNSt3__217__compressed_pairIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISL_EEEEE5firstEv = comdat any

$_ZNSt3__217__compressed_pairIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISL_EEEEE6secondEv = comdat any

$_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEEclEPSL_ = comdat any

$_ZNSt3__222__compressed_pair_elemIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_25__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS2_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISD_EEEEEEPvEEEEEEEELi1ELb0EE5__getEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEE10deallocateERSM_PSL_m = comdat any

$_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEE7__allocEv = comdat any

$_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEE4sizeEv = comdat any

$_ZNSt3__29allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEEE10deallocateEPSK_m = comdat any

$_ZNSt3__217__compressed_pairImNS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairImNS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEE5firstEv = comdat any

$_ZNSt3__222__compressed_pair_elemImLi0ELb0EE5__getEv = comdat any

$_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEENS_22__unordered_map_hasherIS7_SA_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SA_NS_8equal_toIS7_EELb1EEENS5_ISA_EEED2Ev = comdat any

$_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEENS_22__unordered_map_hasherIS7_SA_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SA_NS_8equal_toIS7_EELb1EEENS5_ISA_EEE17__deallocate_nodeEPNS_16__hash_node_baseIPNS_11__hash_nodeISA_PvEEEE = comdat any

$_ZNSt3__217__compressed_pairINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS7_ISE_EEE5firstEv = comdat any

$_ZNSt3__210unique_ptrIA_PNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISH_EEEEED2Ev = comdat any

$_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEENS_22__unordered_map_hasherIS7_SA_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SA_NS_8equal_toIS7_EELb1EEENS5_ISA_EEE12__node_allocEv = comdat any

$_ZNSt3__216__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEE8__upcastEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEE7destroyINS_4pairIKS8_SA_EEEEvRSE_PT_ = comdat any

$_ZNSt3__222__hash_key_value_typesINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEE9__get_ptrERSA_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEE10deallocateERSE_PSD_m = comdat any

$_ZNSt3__217__compressed_pairINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS7_ISE_EEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__214pointer_traitsIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEEE10pointer_toERSG_ = comdat any

$_ZNSt3__29addressofINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEEEEPT_RSH_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEE9__destroyINS_4pairIKS8_SA_EEEEvNS_17integral_constantIbLb0EEERSE_PT_ = comdat any

$_ZNSt3__24pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEED2Ev = comdat any

$_ZN5draco10EntryValueD2Ev = comdat any

$_ZNSt3__26vectorIhNS_9allocatorIhEEED2Ev = comdat any

$_ZNKSt3__26vectorIhNS_9allocatorIhEEE17__annotate_deleteEv = comdat any

$_ZNSt3__213__vector_baseIhNS_9allocatorIhEEED2Ev = comdat any

$_ZNKSt3__26vectorIhNS_9allocatorIhEEE31__annotate_contiguous_containerEPKvS5_S5_S5_ = comdat any

$_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv = comdat any

$_ZNKSt3__26vectorIhNS_9allocatorIhEEE8capacityEv = comdat any

$_ZNKSt3__26vectorIhNS_9allocatorIhEEE4sizeEv = comdat any

$_ZNSt3__212__to_addressIhEEPT_S2_ = comdat any

$_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPhNS_9allocatorIhEEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPhLi0ELb0EE5__getEv = comdat any

$_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE5clearEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIhEEE10deallocateERS2_Phm = comdat any

$_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE7__allocEv = comdat any

$_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE17__destruct_at_endEPh = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIhEEE7destroyIhEEvRS2_PT_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIhEEE9__destroyIhEEvNS_17integral_constantIbLb1EEERS2_PT_ = comdat any

$_ZNSt3__29allocatorIhE7destroyEPh = comdat any

$_ZNSt3__29allocatorIhE10deallocateEPhm = comdat any

$_ZNSt3__217__compressed_pairIPhNS_9allocatorIhEEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIhEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__29addressofINS_4pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEEEPT_RSC_ = comdat any

$_ZNSt3__217__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEE11__get_valueEv = comdat any

$_ZNSt3__29allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEEN5draco10EntryValueEEEPvEEE10deallocateEPSC_m = comdat any

$_ZNSt3__222__compressed_pair_elemINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__210unique_ptrIA_PNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISH_EEEEE5resetEDn = comdat any

$_ZNSt3__217__compressed_pairIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISH_EEEEE5firstEv = comdat any

$_ZNSt3__217__compressed_pairIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISH_EEEEE6secondEv = comdat any

$_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEEclEPSH_ = comdat any

$_ZNSt3__222__compressed_pair_elemIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_25__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS2_IcEEEEN5draco10EntryValueEEEPvEEEEEEEELi1ELb0EE5__getEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEE10deallocateERSI_PSH_m = comdat any

$_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEE7__allocEv = comdat any

$_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEE4sizeEv = comdat any

$_ZNSt3__29allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEEN5draco10EntryValueEEEPvEEEEE10deallocateEPSG_m = comdat any

$_ZNSt3__217__compressed_pairImNS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairImNS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEE5firstEv = comdat any

$_ZNSt3__29allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEE10deallocateEPS6_m = comdat any

$_ZNKSt3__214__split_bufferINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE5firstEv = comdat any

@.str = private unnamed_addr constant [68 x i8] c"allocator<T>::allocate(size_t n) 'n' exceeds maximum supported size\00", align 1

; Function Attrs: noinline nounwind optnone
define hidden %"class.draco::AttributeMetadata"* @_ZNK5draco16GeometryMetadata33GetAttributeMetadataByStringEntryERKNSt3__212basic_stringIcNS1_11char_traitsIcEENS1_9allocatorIcEEEES9_(%"class.draco::GeometryMetadata"* %this, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %entry_name, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %entry_value) #0 {
entry:
  %retval = alloca %"class.draco::AttributeMetadata"*, align 4
  %this.addr = alloca %"class.draco::GeometryMetadata"*, align 4
  %entry_name.addr = alloca %"class.std::__2::basic_string"*, align 4
  %entry_value.addr = alloca %"class.std::__2::basic_string"*, align 4
  %__range1 = alloca %"class.std::__2::vector"*, align 4
  %__begin1 = alloca %"class.std::__2::__wrap_iter", align 4
  %__end1 = alloca %"class.std::__2::__wrap_iter", align 4
  %att_metadata = alloca %"class.std::__2::unique_ptr.37"*, align 4
  %value = alloca %"class.std::__2::basic_string", align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %"class.draco::GeometryMetadata"* %this, %"class.draco::GeometryMetadata"** %this.addr, align 4
  store %"class.std::__2::basic_string"* %entry_name, %"class.std::__2::basic_string"** %entry_name.addr, align 4
  store %"class.std::__2::basic_string"* %entry_value, %"class.std::__2::basic_string"** %entry_value.addr, align 4
  %this1 = load %"class.draco::GeometryMetadata"*, %"class.draco::GeometryMetadata"** %this.addr, align 4
  %att_metadatas_ = getelementptr inbounds %"class.draco::GeometryMetadata", %"class.draco::GeometryMetadata"* %this1, i32 0, i32 1
  store %"class.std::__2::vector"* %att_metadatas_, %"class.std::__2::vector"** %__range1, align 4
  %0 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__range1, align 4
  %call = call %"class.std::__2::unique_ptr.37"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5beginEv(%"class.std::__2::vector"* %0) #7
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %__begin1, i32 0, i32 0
  store %"class.std::__2::unique_ptr.37"* %call, %"class.std::__2::unique_ptr.37"** %coerce.dive, align 4
  %1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__range1, align 4
  %call2 = call %"class.std::__2::unique_ptr.37"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE3endEv(%"class.std::__2::vector"* %1) #7
  %coerce.dive3 = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %__end1, i32 0, i32 0
  store %"class.std::__2::unique_ptr.37"* %call2, %"class.std::__2::unique_ptr.37"** %coerce.dive3, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %call4 = call zeroext i1 @_ZNSt3__2neIPKNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEEbRKNS_11__wrap_iterIT_EESD_(%"class.std::__2::__wrap_iter"* nonnull align 4 dereferenceable(4) %__begin1, %"class.std::__2::__wrap_iter"* nonnull align 4 dereferenceable(4) %__end1) #7
  br i1 %call4, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %call5 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.37"* @_ZNKSt3__211__wrap_iterIPKNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEdeEv(%"class.std::__2::__wrap_iter"* %__begin1) #7
  store %"class.std::__2::unique_ptr.37"* %call5, %"class.std::__2::unique_ptr.37"** %att_metadata, align 4
  %call6 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2Ev(%"class.std::__2::basic_string"* %value) #7
  %2 = load %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"** %att_metadata, align 4
  %call7 = call %"class.draco::AttributeMetadata"* @_ZNKSt3__210unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr.37"* %2) #7
  %3 = bitcast %"class.draco::AttributeMetadata"* %call7 to %"class.draco::Metadata"*
  %4 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %entry_name.addr, align 4
  %call8 = call zeroext i1 @_ZNK5draco8Metadata14GetEntryStringERKNSt3__212basic_stringIcNS1_11char_traitsIcEENS1_9allocatorIcEEEEPS7_(%"class.draco::Metadata"* %3, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %4, %"class.std::__2::basic_string"* %value)
  br i1 %call8, label %if.end, label %if.then

if.then:                                          ; preds = %for.body
  store i32 3, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %for.body
  %5 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %entry_value.addr, align 4
  %call9 = call zeroext i1 @_ZNSt3__2eqINS_9allocatorIcEEEEbRKNS_12basic_stringIcNS_11char_traitsIcEET_EES9_(%"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %value, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %5) #7
  br i1 %call9, label %if.then10, label %if.end12

if.then10:                                        ; preds = %if.end
  %6 = load %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"** %att_metadata, align 4
  %call11 = call %"class.draco::AttributeMetadata"* @_ZNKSt3__210unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS2_EEE3getEv(%"class.std::__2::unique_ptr.37"* %6) #7
  store %"class.draco::AttributeMetadata"* %call11, %"class.draco::AttributeMetadata"** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end12:                                         ; preds = %if.end
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end12, %if.then10, %if.then
  %call13 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_string"* %value) #7
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 3, label %for.inc
    i32 1, label %return
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc

for.inc:                                          ; preds = %cleanup.cont, %cleanup
  %call14 = call nonnull align 4 dereferenceable(4) %"class.std::__2::__wrap_iter"* @_ZNSt3__211__wrap_iterIPKNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEppEv(%"class.std::__2::__wrap_iter"* %__begin1) #7
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store %"class.draco::AttributeMetadata"* null, %"class.draco::AttributeMetadata"** %retval, align 4
  br label %return

return:                                           ; preds = %for.end, %cleanup
  %7 = load %"class.draco::AttributeMetadata"*, %"class.draco::AttributeMetadata"** %retval, align 4
  ret %"class.draco::AttributeMetadata"* %7

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.37"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5beginEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__wrap_iter", align 4
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"** %__begin_, align 4
  %call = call %"class.std::__2::unique_ptr.37"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE11__make_iterEPKS6_(%"class.std::__2::vector"* %this1, %"class.std::__2::unique_ptr.37"* %1) #7
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %retval, i32 0, i32 0
  store %"class.std::__2::unique_ptr.37"* %call, %"class.std::__2::unique_ptr.37"** %coerce.dive, align 4
  %coerce.dive2 = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %retval, i32 0, i32 0
  %2 = load %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"** %coerce.dive2, align 4
  ret %"class.std::__2::unique_ptr.37"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.37"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE3endEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__wrap_iter", align 4
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 1
  %1 = load %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"** %__end_, align 4
  %call = call %"class.std::__2::unique_ptr.37"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE11__make_iterEPKS6_(%"class.std::__2::vector"* %this1, %"class.std::__2::unique_ptr.37"* %1) #7
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %retval, i32 0, i32 0
  store %"class.std::__2::unique_ptr.37"* %call, %"class.std::__2::unique_ptr.37"** %coerce.dive, align 4
  %coerce.dive2 = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %retval, i32 0, i32 0
  %2 = load %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"** %coerce.dive2, align 4
  ret %"class.std::__2::unique_ptr.37"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNSt3__2neIPKNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEEbRKNS_11__wrap_iterIT_EESD_(%"class.std::__2::__wrap_iter"* nonnull align 4 dereferenceable(4) %__x, %"class.std::__2::__wrap_iter"* nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %__x.addr = alloca %"class.std::__2::__wrap_iter"*, align 4
  %__y.addr = alloca %"class.std::__2::__wrap_iter"*, align 4
  store %"class.std::__2::__wrap_iter"* %__x, %"class.std::__2::__wrap_iter"** %__x.addr, align 4
  store %"class.std::__2::__wrap_iter"* %__y, %"class.std::__2::__wrap_iter"** %__y.addr, align 4
  %0 = load %"class.std::__2::__wrap_iter"*, %"class.std::__2::__wrap_iter"** %__x.addr, align 4
  %1 = load %"class.std::__2::__wrap_iter"*, %"class.std::__2::__wrap_iter"** %__y.addr, align 4
  %call = call zeroext i1 @_ZNSt3__2eqIPKNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEES8_EEbRKNS_11__wrap_iterIT_EERKNS9_IT0_EE(%"class.std::__2::__wrap_iter"* nonnull align 4 dereferenceable(4) %0, %"class.std::__2::__wrap_iter"* nonnull align 4 dereferenceable(4) %1) #7
  %lnot = xor i1 %call, true
  ret i1 %lnot
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.37"* @_ZNKSt3__211__wrap_iterIPKNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEdeEv(%"class.std::__2::__wrap_iter"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__wrap_iter"*, align 4
  store %"class.std::__2::__wrap_iter"* %this, %"class.std::__2::__wrap_iter"** %this.addr, align 4
  %this1 = load %"class.std::__2::__wrap_iter"*, %"class.std::__2::__wrap_iter"** %this.addr, align 4
  %__i = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"** %__i, align 4
  ret %"class.std::__2::unique_ptr.37"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2Ev(%"class.std::__2::basic_string"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  %ref.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  %ref.tmp2 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::basic_string"* %this1 to %"class.std::__2::__basic_string_common"*
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::__compressed_pair.46"* @_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_EC2INS_18__default_init_tagESA_EEOT_OT0_(%"class.std::__2::__compressed_pair.46"* %__r_, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  call void @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__zeroEv(%"class.std::__2::basic_string"* %this1) #7
  ret %"class.std::__2::basic_string"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::AttributeMetadata"* @_ZNKSt3__210unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr.37"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.37"*, align 4
  store %"class.std::__2::unique_ptr.37"* %this, %"class.std::__2::unique_ptr.37"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.37", %"class.std::__2::unique_ptr.37"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributeMetadata"** @_ZNKSt3__217__compressed_pairIPN5draco17AttributeMetadataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.38"* %__ptr_) #7
  %0 = load %"class.draco::AttributeMetadata"*, %"class.draco::AttributeMetadata"** %call, align 4
  ret %"class.draco::AttributeMetadata"* %0
}

declare zeroext i1 @_ZNK5draco8Metadata14GetEntryStringERKNSt3__212basic_stringIcNS1_11char_traitsIcEENS1_9allocatorIcEEEEPS7_(%"class.draco::Metadata"*, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12), %"class.std::__2::basic_string"*) #1

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNSt3__2eqINS_9allocatorIcEEEEbRKNS_12basic_stringIcNS_11char_traitsIcEET_EES9_(%"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %__lhs, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %__rhs) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %__lhs.addr = alloca %"class.std::__2::basic_string"*, align 4
  %__rhs.addr = alloca %"class.std::__2::basic_string"*, align 4
  %__lhs_sz = alloca i32, align 4
  %__lp = alloca i8*, align 4
  %__rp = alloca i8*, align 4
  store %"class.std::__2::basic_string"* %__lhs, %"class.std::__2::basic_string"** %__lhs.addr, align 4
  store %"class.std::__2::basic_string"* %__rhs, %"class.std::__2::basic_string"** %__rhs.addr, align 4
  %0 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__lhs.addr, align 4
  %call = call i32 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4sizeEv(%"class.std::__2::basic_string"* %0) #7
  store i32 %call, i32* %__lhs_sz, align 4
  %1 = load i32, i32* %__lhs_sz, align 4
  %2 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__rhs.addr, align 4
  %call1 = call i32 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4sizeEv(%"class.std::__2::basic_string"* %2) #7
  %cmp = icmp ne i32 %1, %call1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %3 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__lhs.addr, align 4
  %call2 = call i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv(%"class.std::__2::basic_string"* %3) #7
  store i8* %call2, i8** %__lp, align 4
  %4 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__rhs.addr, align 4
  %call3 = call i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv(%"class.std::__2::basic_string"* %4) #7
  store i8* %call3, i8** %__rp, align 4
  %5 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %__lhs.addr, align 4
  %call4 = call zeroext i1 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE9__is_longEv(%"class.std::__2::basic_string"* %5) #7
  br i1 %call4, label %if.then5, label %if.end8

if.then5:                                         ; preds = %if.end
  %6 = load i8*, i8** %__lp, align 4
  %7 = load i8*, i8** %__rp, align 4
  %8 = load i32, i32* %__lhs_sz, align 4
  %call6 = call i32 @_ZNSt3__211char_traitsIcE7compareEPKcS3_m(i8* %6, i8* %7, i32 %8) #7
  %cmp7 = icmp eq i32 %call6, 0
  store i1 %cmp7, i1* %retval, align 1
  br label %return

if.end8:                                          ; preds = %if.end
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end8
  %9 = load i32, i32* %__lhs_sz, align 4
  %cmp9 = icmp ne i32 %9, 0
  br i1 %cmp9, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %10 = load i8*, i8** %__lp, align 4
  %11 = load i8, i8* %10, align 1
  %conv = sext i8 %11 to i32
  %12 = load i8*, i8** %__rp, align 4
  %13 = load i8, i8* %12, align 1
  %conv10 = sext i8 %13 to i32
  %cmp11 = icmp ne i32 %conv, %conv10
  br i1 %cmp11, label %if.then12, label %if.end13

if.then12:                                        ; preds = %for.body
  store i1 false, i1* %retval, align 1
  br label %return

if.end13:                                         ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end13
  %14 = load i32, i32* %__lhs_sz, align 4
  %dec = add i32 %14, -1
  store i32 %dec, i32* %__lhs_sz, align 4
  %15 = load i8*, i8** %__lp, align 4
  %incdec.ptr = getelementptr inbounds i8, i8* %15, i32 1
  store i8* %incdec.ptr, i8** %__lp, align 4
  %16 = load i8*, i8** %__rp, align 4
  %incdec.ptr14 = getelementptr inbounds i8, i8* %16, i32 1
  store i8* %incdec.ptr14, i8** %__rp, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %for.end, %if.then12, %if.then5, %if.then
  %17 = load i1, i1* %retval, align 1
  ret i1 %17
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::AttributeMetadata"* @_ZNKSt3__210unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS2_EEE3getEv(%"class.std::__2::unique_ptr.37"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.37"*, align 4
  store %"class.std::__2::unique_ptr.37"* %this, %"class.std::__2::unique_ptr.37"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.37", %"class.std::__2::unique_ptr.37"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributeMetadata"** @_ZNKSt3__217__compressed_pairIPN5draco17AttributeMetadataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.38"* %__ptr_) #7
  %0 = load %"class.draco::AttributeMetadata"*, %"class.draco::AttributeMetadata"** %call, align 4
  ret %"class.draco::AttributeMetadata"* %0
}

; Function Attrs: nounwind
declare %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_string"* returned) unnamed_addr #2

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__wrap_iter"* @_ZNSt3__211__wrap_iterIPKNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEppEv(%"class.std::__2::__wrap_iter"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__wrap_iter"*, align 4
  store %"class.std::__2::__wrap_iter"* %this, %"class.std::__2::__wrap_iter"** %this.addr, align 4
  %this1 = load %"class.std::__2::__wrap_iter"*, %"class.std::__2::__wrap_iter"** %this.addr, align 4
  %__i = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"** %__i, align 4
  %incdec.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.37", %"class.std::__2::unique_ptr.37"* %0, i32 1
  store %"class.std::__2::unique_ptr.37"* %incdec.ptr, %"class.std::__2::unique_ptr.37"** %__i, align 4
  ret %"class.std::__2::__wrap_iter"* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden zeroext i1 @_ZN5draco16GeometryMetadata20AddAttributeMetadataENSt3__210unique_ptrINS_17AttributeMetadataENS1_14default_deleteIS3_EEEE(%"class.draco::GeometryMetadata"* %this, %"class.std::__2::unique_ptr.37"* %att_metadata) #0 {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::GeometryMetadata"*, align 4
  store %"class.draco::GeometryMetadata"* %this, %"class.draco::GeometryMetadata"** %this.addr, align 4
  %this1 = load %"class.draco::GeometryMetadata"*, %"class.draco::GeometryMetadata"** %this.addr, align 4
  %call = call %"class.draco::AttributeMetadata"* @_ZNKSt3__210unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS2_EEE3getEv(%"class.std::__2::unique_ptr.37"* %att_metadata) #7
  %tobool = icmp ne %"class.draco::AttributeMetadata"* %call, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %att_metadatas_ = getelementptr inbounds %"class.draco::GeometryMetadata", %"class.draco::GeometryMetadata"* %this1, i32 0, i32 1
  %call2 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.37"* @_ZNSt3__24moveIRNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOS9_(%"class.std::__2::unique_ptr.37"* nonnull align 4 dereferenceable(4) %att_metadata) #7
  call void @_ZNSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE9push_backEOS6_(%"class.std::__2::vector"* %att_metadatas_, %"class.std::__2::unique_ptr.37"* nonnull align 4 dereferenceable(4) %call2)
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %0 = load i1, i1* %retval, align 1
  ret i1 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE9push_backEOS6_(%"class.std::__2::vector"* %this, %"class.std::__2::unique_ptr.37"* nonnull align 4 dereferenceable(4) %__x) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__x.addr = alloca %"class.std::__2::unique_ptr.37"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.37"* %__x, %"class.std::__2::unique_ptr.37"** %__x.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 1
  %1 = load %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.37"** @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE9__end_capEv(%"class.std::__2::__vector_base"* %2) #7
  %3 = load %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"** %call, align 4
  %cmp = icmp ult %"class.std::__2::unique_ptr.37"* %1, %3
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %4 = load %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"** %__x.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.37"* @_ZNSt3__24moveIRNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOS9_(%"class.std::__2::unique_ptr.37"* nonnull align 4 dereferenceable(4) %4) #7
  call void @_ZNSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE22__construct_one_at_endIJS6_EEEvDpOT_(%"class.std::__2::vector"* %this1, %"class.std::__2::unique_ptr.37"* nonnull align 4 dereferenceable(4) %call2)
  br label %if.end

if.else:                                          ; preds = %entry
  %5 = load %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"** %__x.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.37"* @_ZNSt3__24moveIRNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOS9_(%"class.std::__2::unique_ptr.37"* nonnull align 4 dereferenceable(4) %5) #7
  call void @_ZNSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE21__push_back_slow_pathIS6_EEvOT_(%"class.std::__2::vector"* %this1, %"class.std::__2::unique_ptr.37"* nonnull align 4 dereferenceable(4) %call3)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.37"* @_ZNSt3__24moveIRNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOS9_(%"class.std::__2::unique_ptr.37"* nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::unique_ptr.37"*, align 4
  store %"class.std::__2::unique_ptr.37"* %__t, %"class.std::__2::unique_ptr.37"** %__t.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"** %__t.addr, align 4
  ret %"class.std::__2::unique_ptr.37"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNSt3__2eqIPKNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEES8_EEbRKNS_11__wrap_iterIT_EERKNS9_IT0_EE(%"class.std::__2::__wrap_iter"* nonnull align 4 dereferenceable(4) %__x, %"class.std::__2::__wrap_iter"* nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %__x.addr = alloca %"class.std::__2::__wrap_iter"*, align 4
  %__y.addr = alloca %"class.std::__2::__wrap_iter"*, align 4
  store %"class.std::__2::__wrap_iter"* %__x, %"class.std::__2::__wrap_iter"** %__x.addr, align 4
  store %"class.std::__2::__wrap_iter"* %__y, %"class.std::__2::__wrap_iter"** %__y.addr, align 4
  %0 = load %"class.std::__2::__wrap_iter"*, %"class.std::__2::__wrap_iter"** %__x.addr, align 4
  %call = call %"class.std::__2::unique_ptr.37"* @_ZNKSt3__211__wrap_iterIPKNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEE4baseEv(%"class.std::__2::__wrap_iter"* %0) #7
  %1 = load %"class.std::__2::__wrap_iter"*, %"class.std::__2::__wrap_iter"** %__y.addr, align 4
  %call1 = call %"class.std::__2::unique_ptr.37"* @_ZNKSt3__211__wrap_iterIPKNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEE4baseEv(%"class.std::__2::__wrap_iter"* %1) #7
  %cmp = icmp eq %"class.std::__2::unique_ptr.37"* %call, %call1
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.37"* @_ZNKSt3__211__wrap_iterIPKNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEE4baseEv(%"class.std::__2::__wrap_iter"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__wrap_iter"*, align 4
  store %"class.std::__2::__wrap_iter"* %this, %"class.std::__2::__wrap_iter"** %this.addr, align 4
  %this1 = load %"class.std::__2::__wrap_iter"*, %"class.std::__2::__wrap_iter"** %this.addr, align 4
  %__i = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"** %__i, align 4
  ret %"class.std::__2::unique_ptr.37"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::AttributeMetadata"** @_ZNKSt3__217__compressed_pairIPN5draco17AttributeMetadataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.38"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.38"*, align 4
  store %"class.std::__2::__compressed_pair.38"* %this, %"class.std::__2::__compressed_pair.38"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.38"*, %"class.std::__2::__compressed_pair.38"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.38"* %this1 to %"struct.std::__2::__compressed_pair_elem.39"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributeMetadata"** @_ZNKSt3__222__compressed_pair_elemIPN5draco17AttributeMetadataELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.39"* %0) #7
  ret %"class.draco::AttributeMetadata"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::AttributeMetadata"** @_ZNKSt3__222__compressed_pair_elemIPN5draco17AttributeMetadataELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.39"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.39"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.39"* %this, %"struct.std::__2::__compressed_pair_elem.39"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.39"*, %"struct.std::__2::__compressed_pair_elem.39"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.39", %"struct.std::__2::__compressed_pair_elem.39"* %this1, i32 0, i32 0
  ret %"class.draco::AttributeMetadata"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.37"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE11__make_iterEPKS6_(%"class.std::__2::vector"* %this, %"class.std::__2::unique_ptr.37"* %__p) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__wrap_iter", align 4
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.37"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.37"* %__p, %"class.std::__2::unique_ptr.37"** %__p.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"** %__p.addr, align 4
  %call = call %"class.std::__2::__wrap_iter"* @_ZNSt3__211__wrap_iterIPKNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEC2ES8_(%"class.std::__2::__wrap_iter"* %retval, %"class.std::__2::unique_ptr.37"* %0) #7
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %retval, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"** %coerce.dive, align 4
  ret %"class.std::__2::unique_ptr.37"* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__wrap_iter"* @_ZNSt3__211__wrap_iterIPKNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEC2ES8_(%"class.std::__2::__wrap_iter"* returned %this, %"class.std::__2::unique_ptr.37"* %__x) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__wrap_iter"*, align 4
  %__x.addr = alloca %"class.std::__2::unique_ptr.37"*, align 4
  store %"class.std::__2::__wrap_iter"* %this, %"class.std::__2::__wrap_iter"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.37"* %__x, %"class.std::__2::unique_ptr.37"** %__x.addr, align 4
  %this1 = load %"class.std::__2::__wrap_iter"*, %"class.std::__2::__wrap_iter"** %this.addr, align 4
  %__i = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"** %__x.addr, align 4
  store %"class.std::__2::unique_ptr.37"* %0, %"class.std::__2::unique_ptr.37"** %__i, align 4
  ret %"class.std::__2::__wrap_iter"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.46"* @_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_EC2INS_18__default_init_tagESA_EEOT_OT0_(%"class.std::__2::__compressed_pair.46"* returned %this, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.46"*, align 4
  %__t1.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  %agg.tmp3 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.46"* %this, %"class.std::__2::__compressed_pair.46"** %this.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t1, %"struct.std::__2::__default_init_tag"** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.46"*, %"class.std::__2::__compressed_pair.46"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.46"* %this1 to %"struct.std::__2::__compressed_pair_elem.47"*
  %1 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t1.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %1) #7
  %call2 = call %"struct.std::__2::__compressed_pair_elem.47"* @_ZNSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.47"* %0)
  %2 = bitcast %"class.std::__2::__compressed_pair.46"* %this1 to %"struct.std::__2::__compressed_pair_elem.48"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call4 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #7
  %call5 = call %"struct.std::__2::__compressed_pair_elem.48"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIcEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.48"* %2)
  ret %"class.std::__2::__compressed_pair.46"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__zeroEv(%"class.std::__2::basic_string"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  %__a = alloca [3 x i32]*, align 4
  %__i = alloca i32, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair.46"* %__r_) #7
  %0 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call, i32 0, i32 0
  %__r = bitcast %union.anon* %0 to %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__raw"*
  %__words = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__raw", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__raw"* %__r, i32 0, i32 0
  store [3 x i32]* %__words, [3 x i32]** %__a, align 4
  store i32 0, i32* %__i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %__i, align 4
  %cmp = icmp ult i32 %1, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load [3 x i32]*, [3 x i32]** %__a, align 4
  %3 = load i32, i32* %__i, align 4
  %arrayidx = getelementptr inbounds [3 x i32], [3 x i32]* %2, i32 0, i32 %3
  store i32 0, i32* %arrayidx, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %4 = load i32, i32* %__i, align 4
  %inc = add i32 %4, 1
  store i32 %inc, i32* %__i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  store %"struct.std::__2::__default_init_tag"* %__t, %"struct.std::__2::__default_init_tag"** %__t.addr, align 4
  %0 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t.addr, align 4
  ret %"struct.std::__2::__default_init_tag"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.47"* @_ZNSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.47"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.47"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.47"* %this, %"struct.std::__2::__compressed_pair_elem.47"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.47"*, %"struct.std::__2::__compressed_pair_elem.47"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.47", %"struct.std::__2::__compressed_pair_elem.47"* %this1, i32 0, i32 0
  ret %"struct.std::__2::__compressed_pair_elem.47"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.48"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIcEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.48"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.48"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.48"* %this, %"struct.std::__2::__compressed_pair_elem.48"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.48"*, %"struct.std::__2::__compressed_pair_elem.48"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.48"* %this1 to %"class.std::__2::allocator.49"*
  %call = call %"class.std::__2::allocator.49"* @_ZNSt3__29allocatorIcEC2Ev(%"class.std::__2::allocator.49"* %1) #7
  ret %"struct.std::__2::__compressed_pair_elem.48"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::allocator.49"* @_ZNSt3__29allocatorIcEC2Ev(%"class.std::__2::allocator.49"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.49"*, align 4
  store %"class.std::__2::allocator.49"* %this, %"class.std::__2::allocator.49"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.49"*, %"class.std::__2::allocator.49"** %this.addr, align 4
  ret %"class.std::__2::allocator.49"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair.46"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.46"*, align 4
  store %"class.std::__2::__compressed_pair.46"* %this, %"class.std::__2::__compressed_pair.46"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.46"*, %"class.std::__2::__compressed_pair.46"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.46"* %this1 to %"struct.std::__2::__compressed_pair_elem.47"*
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.47"* %0) #7
  ret %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.47"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.47"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.47"* %this, %"struct.std::__2::__compressed_pair_elem.47"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.47"*, %"struct.std::__2::__compressed_pair_elem.47"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.47", %"struct.std::__2::__compressed_pair_elem.47"* %this1, i32 0, i32 0
  ret %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4sizeEv(%"class.std::__2::basic_string"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %call = call zeroext i1 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE9__is_longEv(%"class.std::__2::basic_string"* %this1) #7
  br i1 %call, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %call2 = call i32 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE15__get_long_sizeEv(%"class.std::__2::basic_string"* %this1) #7
  br label %cond.end

cond.false:                                       ; preds = %entry
  %call3 = call i32 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE16__get_short_sizeEv(%"class.std::__2::basic_string"* %this1) #7
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %call2, %cond.true ], [ %call3, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv(%"class.std::__2::basic_string"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %call = call i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE13__get_pointerEv(%"class.std::__2::basic_string"* %this1) #7
  %call2 = call i8* @_ZNSt3__212__to_addressIKcEEPT_S3_(i8* %call) #7
  ret i8* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE9__is_longEv(%"class.std::__2::basic_string"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair.46"* %__r_) #7
  %0 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call, i32 0, i32 0
  %__s = bitcast %union.anon* %0 to %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short"*
  %1 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short"* %__s, i32 0, i32 1
  %__size_ = getelementptr inbounds %struct.anon, %struct.anon* %1, i32 0, i32 0
  %2 = load i8, i8* %__size_, align 1
  %conv = zext i8 %2 to i32
  %and = and i32 %conv, 128
  %tobool = icmp ne i32 %and, 0
  ret i1 %tobool
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__211char_traitsIcE7compareEPKcS3_m(i8* %__s1, i8* %__s2, i32 %__n) #0 comdat {
entry:
  %retval = alloca i32, align 4
  %__s1.addr = alloca i8*, align 4
  %__s2.addr = alloca i8*, align 4
  %__n.addr = alloca i32, align 4
  store i8* %__s1, i8** %__s1.addr, align 4
  store i8* %__s2, i8** %__s2.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %cmp = icmp eq i32 %0, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load i8*, i8** %__s1.addr, align 4
  %2 = load i8*, i8** %__s2.addr, align 4
  %3 = load i32, i32* %__n.addr, align 4
  %call = call i32 @memcmp(i8* %1, i8* %2, i32 %3) #7
  store i32 %call, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %4 = load i32, i32* %retval, align 4
  ret i32 %4
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE15__get_long_sizeEv(%"class.std::__2::basic_string"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair.46"* %__r_) #7
  %0 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call, i32 0, i32 0
  %__l = bitcast %union.anon* %0 to %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long"*
  %__size_ = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long"* %__l, i32 0, i32 1
  %1 = load i32, i32* %__size_, align 4
  ret i32 %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE16__get_short_sizeEv(%"class.std::__2::basic_string"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair.46"* %__r_) #7
  %0 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call, i32 0, i32 0
  %__s = bitcast %union.anon* %0 to %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short"*
  %1 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short"* %__s, i32 0, i32 1
  %__size_ = getelementptr inbounds %struct.anon, %struct.anon* %1, i32 0, i32 0
  %2 = load i8, i8* %__size_, align 1
  %conv = zext i8 %2 to i32
  ret i32 %conv
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair.46"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.46"*, align 4
  store %"class.std::__2::__compressed_pair.46"* %this, %"class.std::__2::__compressed_pair.46"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.46"*, %"class.std::__2::__compressed_pair.46"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.46"* %this1 to %"struct.std::__2::__compressed_pair_elem.47"*
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.47"* %0) #7
  ret %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.47"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.47"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.47"* %this, %"struct.std::__2::__compressed_pair_elem.47"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.47"*, %"struct.std::__2::__compressed_pair_elem.47"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.47", %"struct.std::__2::__compressed_pair_elem.47"* %this1, i32 0, i32 0
  ret %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__212__to_addressIKcEEPT_S3_(i8* %__p) #0 comdat {
entry:
  %__p.addr = alloca i8*, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %0 = load i8*, i8** %__p.addr, align 4
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE13__get_pointerEv(%"class.std::__2::basic_string"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %call = call zeroext i1 @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE9__is_longEv(%"class.std::__2::basic_string"* %this1) #7
  br i1 %call, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %call2 = call i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE18__get_long_pointerEv(%"class.std::__2::basic_string"* %this1) #7
  br label %cond.end

cond.false:                                       ; preds = %entry
  %call3 = call i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE19__get_short_pointerEv(%"class.std::__2::basic_string"* %this1) #7
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i8* [ %call2, %cond.true ], [ %call3, %cond.false ]
  ret i8* %cond
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE18__get_long_pointerEv(%"class.std::__2::basic_string"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair.46"* %__r_) #7
  %0 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call, i32 0, i32 0
  %__l = bitcast %union.anon* %0 to %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long"*
  %__data_ = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long"* %__l, i32 0, i32 0
  %1 = load i8*, i8** %__data_, align 4
  ret i8* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE19__get_short_pointerEv(%"class.std::__2::basic_string"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNKSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair.46"* %__r_) #7
  %0 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call, i32 0, i32 0
  %__s = bitcast %union.anon* %0 to %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short"*
  %__data_ = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__short"* %__s, i32 0, i32 0
  %arrayidx = getelementptr inbounds [11 x i8], [11 x i8]* %__data_, i32 0, i32 0
  %call2 = call i8* @_ZNSt3__214pointer_traitsIPKcE10pointer_toERS1_(i8* nonnull align 1 dereferenceable(1) %arrayidx) #7
  ret i8* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__214pointer_traitsIPKcE10pointer_toERS1_(i8* nonnull align 1 dereferenceable(1) %__r) #0 comdat {
entry:
  %__r.addr = alloca i8*, align 4
  store i8* %__r, i8** %__r.addr, align 4
  %0 = load i8*, i8** %__r.addr, align 4
  %call = call i8* @_ZNSt3__29addressofIKcEEPT_RS2_(i8* nonnull align 1 dereferenceable(1) %0) #7
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__29addressofIKcEEPT_RS2_(i8* nonnull align 1 dereferenceable(1) %__x) #0 comdat {
entry:
  %__x.addr = alloca i8*, align 4
  store i8* %__x, i8** %__x.addr, align 4
  %0 = load i8*, i8** %__x.addr, align 4
  ret i8* %0
}

; Function Attrs: nounwind
declare i32 @memcmp(i8*, i8*, i32) #2

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.37"** @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE9__end_capEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.37"** @_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.41"* %__end_cap_) #7
  ret %"class.std::__2::unique_ptr.37"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE22__construct_one_at_endIJS6_EEEvDpOT_(%"class.std::__2::vector"* %this, %"class.std::__2::unique_ptr.37"* nonnull align 4 dereferenceable(4) %__args) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__args.addr = alloca %"class.std::__2::unique_ptr.37"*, align 4
  %__tx = alloca %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributeMetadata, std::__2::default_delete<draco::AttributeMetadata>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributeMetadata, std::__2::default_delete<draco::AttributeMetadata>>>>::_ConstructTransaction", align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.37"* %__args, %"class.std::__2::unique_ptr.37"** %__args.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %call = call %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributeMetadata, std::__2::default_delete<draco::AttributeMetadata>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributeMetadata, std::__2::default_delete<draco::AttributeMetadata>>>>::_ConstructTransaction"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE21_ConstructTransactionC2ERS9_m(%"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributeMetadata, std::__2::default_delete<draco::AttributeMetadata>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributeMetadata, std::__2::default_delete<draco::AttributeMetadata>>>>::_ConstructTransaction"* %__tx, %"class.std::__2::vector"* nonnull align 4 dereferenceable(12) %this1, i32 1)
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.44"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base"* %0) #7
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributeMetadata, std::__2::default_delete<draco::AttributeMetadata>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributeMetadata, std::__2::default_delete<draco::AttributeMetadata>>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributeMetadata, std::__2::default_delete<draco::AttributeMetadata>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributeMetadata, std::__2::default_delete<draco::AttributeMetadata>>>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %1 = load %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"** %__pos_, align 4
  %call3 = call %"class.std::__2::unique_ptr.37"* @_ZNSt3__212__to_addressINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEEPT_S8_(%"class.std::__2::unique_ptr.37"* %1) #7
  %2 = load %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"** %__args.addr, align 4
  %call4 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.37"* @_ZNSt3__27forwardINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"class.std::__2::unique_ptr.37"* nonnull align 4 dereferenceable(4) %2) #7
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEEE9constructIS7_JS7_EEEvRS8_PT_DpOT0_(%"class.std::__2::allocator.44"* nonnull align 1 dereferenceable(1) %call2, %"class.std::__2::unique_ptr.37"* %call3, %"class.std::__2::unique_ptr.37"* nonnull align 4 dereferenceable(4) %call4)
  %__pos_5 = getelementptr inbounds %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributeMetadata, std::__2::default_delete<draco::AttributeMetadata>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributeMetadata, std::__2::default_delete<draco::AttributeMetadata>>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributeMetadata, std::__2::default_delete<draco::AttributeMetadata>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributeMetadata, std::__2::default_delete<draco::AttributeMetadata>>>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %3 = load %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"** %__pos_5, align 4
  %incdec.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.37", %"class.std::__2::unique_ptr.37"* %3, i32 1
  store %"class.std::__2::unique_ptr.37"* %incdec.ptr, %"class.std::__2::unique_ptr.37"** %__pos_5, align 4
  %call6 = call %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributeMetadata, std::__2::default_delete<draco::AttributeMetadata>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributeMetadata, std::__2::default_delete<draco::AttributeMetadata>>>>::_ConstructTransaction"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributeMetadata, std::__2::default_delete<draco::AttributeMetadata>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributeMetadata, std::__2::default_delete<draco::AttributeMetadata>>>>::_ConstructTransaction"* %__tx) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE21__push_back_slow_pathIS6_EEvOT_(%"class.std::__2::vector"* %this, %"class.std::__2::unique_ptr.37"* nonnull align 4 dereferenceable(4) %__x) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__x.addr = alloca %"class.std::__2::unique_ptr.37"*, align 4
  %__a = alloca %"class.std::__2::allocator.44"*, align 4
  %__v = alloca %"struct.std::__2::__split_buffer", align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.37"* %__x, %"class.std::__2::unique_ptr.37"** %__x.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.44"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base"* %0) #7
  store %"class.std::__2::allocator.44"* %call, %"class.std::__2::allocator.44"** %__a, align 4
  %call2 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector"* %this1) #7
  %add = add i32 %call2, 1
  %call3 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE11__recommendEm(%"class.std::__2::vector"* %this1, i32 %add)
  %call4 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector"* %this1) #7
  %1 = load %"class.std::__2::allocator.44"*, %"class.std::__2::allocator.44"** %__a, align 4
  %call5 = call %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEEC2EmmS9_(%"struct.std::__2::__split_buffer"* %__v, i32 %call3, i32 %call4, %"class.std::__2::allocator.44"* nonnull align 1 dereferenceable(1) %1)
  %2 = load %"class.std::__2::allocator.44"*, %"class.std::__2::allocator.44"** %__a, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %__v, i32 0, i32 2
  %3 = load %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"** %__end_, align 4
  %call6 = call %"class.std::__2::unique_ptr.37"* @_ZNSt3__212__to_addressINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEEPT_S8_(%"class.std::__2::unique_ptr.37"* %3) #7
  %4 = load %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"** %__x.addr, align 4
  %call7 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.37"* @_ZNSt3__27forwardINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"class.std::__2::unique_ptr.37"* nonnull align 4 dereferenceable(4) %4) #7
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEEE9constructIS7_JS7_EEEvRS8_PT_DpOT0_(%"class.std::__2::allocator.44"* nonnull align 1 dereferenceable(1) %2, %"class.std::__2::unique_ptr.37"* %call6, %"class.std::__2::unique_ptr.37"* nonnull align 4 dereferenceable(4) %call7)
  %__end_8 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %__v, i32 0, i32 2
  %5 = load %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"** %__end_8, align 4
  %incdec.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.37", %"class.std::__2::unique_ptr.37"* %5, i32 1
  store %"class.std::__2::unique_ptr.37"* %incdec.ptr, %"class.std::__2::unique_ptr.37"** %__end_8, align 4
  call void @_ZNSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS6_RS8_EE(%"class.std::__2::vector"* %this1, %"struct.std::__2::__split_buffer"* nonnull align 4 dereferenceable(20) %__v)
  %call9 = call %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEED2Ev(%"struct.std::__2::__split_buffer"* %__v) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.37"** @_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.41"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.41"*, align 4
  store %"class.std::__2::__compressed_pair.41"* %this, %"class.std::__2::__compressed_pair.41"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.41"*, %"class.std::__2::__compressed_pair.41"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.41"* %this1 to %"struct.std::__2::__compressed_pair_elem.42"*
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.37"** @_ZNSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.42"* %0) #7
  ret %"class.std::__2::unique_ptr.37"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.37"** @_ZNSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.42"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.42"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.42"* %this, %"struct.std::__2::__compressed_pair_elem.42"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.42"*, %"struct.std::__2::__compressed_pair_elem.42"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.42", %"struct.std::__2::__compressed_pair_elem.42"* %this1, i32 0, i32 0
  ret %"class.std::__2::unique_ptr.37"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributeMetadata, std::__2::default_delete<draco::AttributeMetadata>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributeMetadata, std::__2::default_delete<draco::AttributeMetadata>>>>::_ConstructTransaction"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE21_ConstructTransactionC2ERS9_m(%"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributeMetadata, std::__2::default_delete<draco::AttributeMetadata>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributeMetadata, std::__2::default_delete<draco::AttributeMetadata>>>>::_ConstructTransaction"* returned %this, %"class.std::__2::vector"* nonnull align 4 dereferenceable(12) %__v, i32 %__n) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributeMetadata, std::__2::default_delete<draco::AttributeMetadata>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributeMetadata, std::__2::default_delete<draco::AttributeMetadata>>>>::_ConstructTransaction"*, align 4
  %__v.addr = alloca %"class.std::__2::vector"*, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributeMetadata, std::__2::default_delete<draco::AttributeMetadata>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributeMetadata, std::__2::default_delete<draco::AttributeMetadata>>>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributeMetadata, std::__2::default_delete<draco::AttributeMetadata>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributeMetadata, std::__2::default_delete<draco::AttributeMetadata>>>>::_ConstructTransaction"** %this.addr, align 4
  store %"class.std::__2::vector"* %__v, %"class.std::__2::vector"** %__v.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributeMetadata, std::__2::default_delete<draco::AttributeMetadata>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributeMetadata, std::__2::default_delete<draco::AttributeMetadata>>>>::_ConstructTransaction"*, %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributeMetadata, std::__2::default_delete<draco::AttributeMetadata>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributeMetadata, std::__2::default_delete<draco::AttributeMetadata>>>>::_ConstructTransaction"** %this.addr, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributeMetadata, std::__2::default_delete<draco::AttributeMetadata>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributeMetadata, std::__2::default_delete<draco::AttributeMetadata>>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributeMetadata, std::__2::default_delete<draco::AttributeMetadata>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributeMetadata, std::__2::default_delete<draco::AttributeMetadata>>>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__v.addr, align 4
  store %"class.std::__2::vector"* %0, %"class.std::__2::vector"** %__v_, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributeMetadata, std::__2::default_delete<draco::AttributeMetadata>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributeMetadata, std::__2::default_delete<draco::AttributeMetadata>>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributeMetadata, std::__2::default_delete<draco::AttributeMetadata>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributeMetadata, std::__2::default_delete<draco::AttributeMetadata>>>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__v.addr, align 4
  %2 = bitcast %"class.std::__2::vector"* %1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %2, i32 0, i32 1
  %3 = load %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"** %__end_, align 4
  store %"class.std::__2::unique_ptr.37"* %3, %"class.std::__2::unique_ptr.37"** %__pos_, align 4
  %__new_end_ = getelementptr inbounds %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributeMetadata, std::__2::default_delete<draco::AttributeMetadata>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributeMetadata, std::__2::default_delete<draco::AttributeMetadata>>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributeMetadata, std::__2::default_delete<draco::AttributeMetadata>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributeMetadata, std::__2::default_delete<draco::AttributeMetadata>>>>::_ConstructTransaction"* %this1, i32 0, i32 2
  %4 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__v.addr, align 4
  %5 = bitcast %"class.std::__2::vector"* %4 to %"class.std::__2::__vector_base"*
  %__end_2 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %5, i32 0, i32 1
  %6 = load %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"** %__end_2, align 4
  %7 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.37", %"class.std::__2::unique_ptr.37"* %6, i32 %7
  store %"class.std::__2::unique_ptr.37"* %add.ptr, %"class.std::__2::unique_ptr.37"** %__new_end_, align 4
  ret %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributeMetadata, std::__2::default_delete<draco::AttributeMetadata>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributeMetadata, std::__2::default_delete<draco::AttributeMetadata>>>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEEE9constructIS7_JS7_EEEvRS8_PT_DpOT0_(%"class.std::__2::allocator.44"* nonnull align 1 dereferenceable(1) %__a, %"class.std::__2::unique_ptr.37"* %__p, %"class.std::__2::unique_ptr.37"* nonnull align 4 dereferenceable(4) %__args) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.44"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.37"*, align 4
  %__args.addr = alloca %"class.std::__2::unique_ptr.37"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_construct", align 1
  store %"class.std::__2::allocator.44"* %__a, %"class.std::__2::allocator.44"** %__a.addr, align 4
  store %"class.std::__2::unique_ptr.37"* %__p, %"class.std::__2::unique_ptr.37"** %__p.addr, align 4
  store %"class.std::__2::unique_ptr.37"* %__args, %"class.std::__2::unique_ptr.37"** %__args.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_construct"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.44"*, %"class.std::__2::allocator.44"** %__a.addr, align 4
  %2 = load %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"** %__p.addr, align 4
  %3 = load %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.37"* @_ZNSt3__27forwardINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"class.std::__2::unique_ptr.37"* nonnull align 4 dereferenceable(4) %3) #7
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEEE11__constructIS7_JS7_EEEvNS_17integral_constantIbLb1EEERS8_PT_DpOT0_(%"class.std::__2::allocator.44"* nonnull align 1 dereferenceable(1) %1, %"class.std::__2::unique_ptr.37"* %2, %"class.std::__2::unique_ptr.37"* nonnull align 4 dereferenceable(4) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.44"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.44"* @_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE6secondEv(%"class.std::__2::__compressed_pair.41"* %__end_cap_) #7
  ret %"class.std::__2::allocator.44"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.37"* @_ZNSt3__212__to_addressINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEEPT_S8_(%"class.std::__2::unique_ptr.37"* %__p) #0 comdat {
entry:
  %__p.addr = alloca %"class.std::__2::unique_ptr.37"*, align 4
  store %"class.std::__2::unique_ptr.37"* %__p, %"class.std::__2::unique_ptr.37"** %__p.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"** %__p.addr, align 4
  ret %"class.std::__2::unique_ptr.37"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.37"* @_ZNSt3__27forwardINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"class.std::__2::unique_ptr.37"* nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::unique_ptr.37"*, align 4
  store %"class.std::__2::unique_ptr.37"* %__t, %"class.std::__2::unique_ptr.37"** %__t.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"** %__t.addr, align 4
  ret %"class.std::__2::unique_ptr.37"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributeMetadata, std::__2::default_delete<draco::AttributeMetadata>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributeMetadata, std::__2::default_delete<draco::AttributeMetadata>>>>::_ConstructTransaction"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributeMetadata, std::__2::default_delete<draco::AttributeMetadata>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributeMetadata, std::__2::default_delete<draco::AttributeMetadata>>>>::_ConstructTransaction"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributeMetadata, std::__2::default_delete<draco::AttributeMetadata>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributeMetadata, std::__2::default_delete<draco::AttributeMetadata>>>>::_ConstructTransaction"*, align 4
  store %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributeMetadata, std::__2::default_delete<draco::AttributeMetadata>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributeMetadata, std::__2::default_delete<draco::AttributeMetadata>>>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributeMetadata, std::__2::default_delete<draco::AttributeMetadata>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributeMetadata, std::__2::default_delete<draco::AttributeMetadata>>>>::_ConstructTransaction"** %this.addr, align 4
  %this1 = load %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributeMetadata, std::__2::default_delete<draco::AttributeMetadata>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributeMetadata, std::__2::default_delete<draco::AttributeMetadata>>>>::_ConstructTransaction"*, %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributeMetadata, std::__2::default_delete<draco::AttributeMetadata>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributeMetadata, std::__2::default_delete<draco::AttributeMetadata>>>>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributeMetadata, std::__2::default_delete<draco::AttributeMetadata>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributeMetadata, std::__2::default_delete<draco::AttributeMetadata>>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributeMetadata, std::__2::default_delete<draco::AttributeMetadata>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributeMetadata, std::__2::default_delete<draco::AttributeMetadata>>>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %0 = load %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"** %__pos_, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributeMetadata, std::__2::default_delete<draco::AttributeMetadata>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributeMetadata, std::__2::default_delete<draco::AttributeMetadata>>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributeMetadata, std::__2::default_delete<draco::AttributeMetadata>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributeMetadata, std::__2::default_delete<draco::AttributeMetadata>>>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__v_, align 4
  %2 = bitcast %"class.std::__2::vector"* %1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %2, i32 0, i32 1
  store %"class.std::__2::unique_ptr.37"* %0, %"class.std::__2::unique_ptr.37"** %__end_, align 4
  ret %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributeMetadata, std::__2::default_delete<draco::AttributeMetadata>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributeMetadata, std::__2::default_delete<draco::AttributeMetadata>>>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEEE11__constructIS7_JS7_EEEvNS_17integral_constantIbLb1EEERS8_PT_DpOT0_(%"class.std::__2::allocator.44"* nonnull align 1 dereferenceable(1) %__a, %"class.std::__2::unique_ptr.37"* %__p, %"class.std::__2::unique_ptr.37"* nonnull align 4 dereferenceable(4) %__args) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.44"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.37"*, align 4
  %__args.addr = alloca %"class.std::__2::unique_ptr.37"*, align 4
  store %"class.std::__2::allocator.44"* %__a, %"class.std::__2::allocator.44"** %__a.addr, align 4
  store %"class.std::__2::unique_ptr.37"* %__p, %"class.std::__2::unique_ptr.37"** %__p.addr, align 4
  store %"class.std::__2::unique_ptr.37"* %__args, %"class.std::__2::unique_ptr.37"** %__args.addr, align 4
  %1 = load %"class.std::__2::allocator.44"*, %"class.std::__2::allocator.44"** %__a.addr, align 4
  %2 = load %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"** %__p.addr, align 4
  %3 = load %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.37"* @_ZNSt3__27forwardINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"class.std::__2::unique_ptr.37"* nonnull align 4 dereferenceable(4) %3) #7
  call void @_ZNSt3__29allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEE9constructIS6_JS6_EEEvPT_DpOT0_(%"class.std::__2::allocator.44"* %1, %"class.std::__2::unique_ptr.37"* %2, %"class.std::__2::unique_ptr.37"* nonnull align 4 dereferenceable(4) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEE9constructIS6_JS6_EEEvPT_DpOT0_(%"class.std::__2::allocator.44"* %this, %"class.std::__2::unique_ptr.37"* %__p, %"class.std::__2::unique_ptr.37"* nonnull align 4 dereferenceable(4) %__args) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.44"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.37"*, align 4
  %__args.addr = alloca %"class.std::__2::unique_ptr.37"*, align 4
  store %"class.std::__2::allocator.44"* %this, %"class.std::__2::allocator.44"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.37"* %__p, %"class.std::__2::unique_ptr.37"** %__p.addr, align 4
  store %"class.std::__2::unique_ptr.37"* %__args, %"class.std::__2::unique_ptr.37"** %__args.addr, align 4
  %this1 = load %"class.std::__2::allocator.44"*, %"class.std::__2::allocator.44"** %this.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"** %__p.addr, align 4
  %1 = bitcast %"class.std::__2::unique_ptr.37"* %0 to i8*
  %2 = bitcast i8* %1 to %"class.std::__2::unique_ptr.37"*
  %3 = load %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.37"* @_ZNSt3__27forwardINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"class.std::__2::unique_ptr.37"* nonnull align 4 dereferenceable(4) %3) #7
  %call2 = call %"class.std::__2::unique_ptr.37"* @_ZNSt3__210unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS2_EEEC2EOS5_(%"class.std::__2::unique_ptr.37"* %2, %"class.std::__2::unique_ptr.37"* nonnull align 4 dereferenceable(4) %call) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.37"* @_ZNSt3__210unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS2_EEEC2EOS5_(%"class.std::__2::unique_ptr.37"* returned %this, %"class.std::__2::unique_ptr.37"* nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.37"*, align 4
  %__u.addr = alloca %"class.std::__2::unique_ptr.37"*, align 4
  %ref.tmp = alloca %"class.draco::AttributeMetadata"*, align 4
  store %"class.std::__2::unique_ptr.37"* %this, %"class.std::__2::unique_ptr.37"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.37"* %__u, %"class.std::__2::unique_ptr.37"** %__u.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.37", %"class.std::__2::unique_ptr.37"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"** %__u.addr, align 4
  %call = call %"class.draco::AttributeMetadata"* @_ZNSt3__210unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS2_EEE7releaseEv(%"class.std::__2::unique_ptr.37"* %0) #7
  store %"class.draco::AttributeMetadata"* %call, %"class.draco::AttributeMetadata"** %ref.tmp, align 4
  %1 = load %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"** %__u.addr, align 4
  %call2 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete"* @_ZNSt3__210unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS2_EEE11get_deleterEv(%"class.std::__2::unique_ptr.37"* %1) #7
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete"* @_ZNSt3__27forwardINS_14default_deleteIN5draco17AttributeMetadataEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete"* nonnull align 1 dereferenceable(1) %call2) #7
  %call4 = call %"class.std::__2::__compressed_pair.38"* @_ZNSt3__217__compressed_pairIPN5draco17AttributeMetadataENS_14default_deleteIS2_EEEC2IS3_S5_EEOT_OT0_(%"class.std::__2::__compressed_pair.38"* %__ptr_, %"class.draco::AttributeMetadata"** nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::default_delete"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::unique_ptr.37"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::AttributeMetadata"* @_ZNSt3__210unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS2_EEE7releaseEv(%"class.std::__2::unique_ptr.37"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.37"*, align 4
  %__t = alloca %"class.draco::AttributeMetadata"*, align 4
  store %"class.std::__2::unique_ptr.37"* %this, %"class.std::__2::unique_ptr.37"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.37", %"class.std::__2::unique_ptr.37"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributeMetadata"** @_ZNSt3__217__compressed_pairIPN5draco17AttributeMetadataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.38"* %__ptr_) #7
  %0 = load %"class.draco::AttributeMetadata"*, %"class.draco::AttributeMetadata"** %call, align 4
  store %"class.draco::AttributeMetadata"* %0, %"class.draco::AttributeMetadata"** %__t, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr.37", %"class.std::__2::unique_ptr.37"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::AttributeMetadata"** @_ZNSt3__217__compressed_pairIPN5draco17AttributeMetadataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.38"* %__ptr_2) #7
  store %"class.draco::AttributeMetadata"* null, %"class.draco::AttributeMetadata"** %call3, align 4
  %1 = load %"class.draco::AttributeMetadata"*, %"class.draco::AttributeMetadata"** %__t, align 4
  ret %"class.draco::AttributeMetadata"* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete"* @_ZNSt3__27forwardINS_14default_deleteIN5draco17AttributeMetadataEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"struct.std::__2::default_delete"*, align 4
  store %"struct.std::__2::default_delete"* %__t, %"struct.std::__2::default_delete"** %__t.addr, align 4
  %0 = load %"struct.std::__2::default_delete"*, %"struct.std::__2::default_delete"** %__t.addr, align 4
  ret %"struct.std::__2::default_delete"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete"* @_ZNSt3__210unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS2_EEE11get_deleterEv(%"class.std::__2::unique_ptr.37"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.37"*, align 4
  store %"class.std::__2::unique_ptr.37"* %this, %"class.std::__2::unique_ptr.37"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.37", %"class.std::__2::unique_ptr.37"* %this1, i32 0, i32 0
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete"* @_ZNSt3__217__compressed_pairIPN5draco17AttributeMetadataENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.38"* %__ptr_) #7
  ret %"struct.std::__2::default_delete"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.38"* @_ZNSt3__217__compressed_pairIPN5draco17AttributeMetadataENS_14default_deleteIS2_EEEC2IS3_S5_EEOT_OT0_(%"class.std::__2::__compressed_pair.38"* returned %this, %"class.draco::AttributeMetadata"** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::default_delete"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.38"*, align 4
  %__t1.addr = alloca %"class.draco::AttributeMetadata"**, align 4
  %__t2.addr = alloca %"struct.std::__2::default_delete"*, align 4
  store %"class.std::__2::__compressed_pair.38"* %this, %"class.std::__2::__compressed_pair.38"** %this.addr, align 4
  store %"class.draco::AttributeMetadata"** %__t1, %"class.draco::AttributeMetadata"*** %__t1.addr, align 4
  store %"struct.std::__2::default_delete"* %__t2, %"struct.std::__2::default_delete"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.38"*, %"class.std::__2::__compressed_pair.38"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.38"* %this1 to %"struct.std::__2::__compressed_pair_elem.39"*
  %1 = load %"class.draco::AttributeMetadata"**, %"class.draco::AttributeMetadata"*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributeMetadata"** @_ZNSt3__27forwardIPN5draco17AttributeMetadataEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::AttributeMetadata"** nonnull align 4 dereferenceable(4) %1) #7
  %call2 = call %"struct.std::__2::__compressed_pair_elem.39"* @_ZNSt3__222__compressed_pair_elemIPN5draco17AttributeMetadataELi0ELb0EEC2IS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.39"* %0, %"class.draco::AttributeMetadata"** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.38"* %this1 to %"struct.std::__2::__compressed_pair_elem.40"*
  %3 = load %"struct.std::__2::default_delete"*, %"struct.std::__2::default_delete"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete"* @_ZNSt3__27forwardINS_14default_deleteIN5draco17AttributeMetadataEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete"* nonnull align 1 dereferenceable(1) %3) #7
  %call4 = call %"struct.std::__2::__compressed_pair_elem.40"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco17AttributeMetadataEEELi1ELb1EEC2IS4_vEEOT_(%"struct.std::__2::__compressed_pair_elem.40"* %2, %"struct.std::__2::default_delete"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::__compressed_pair.38"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::AttributeMetadata"** @_ZNSt3__217__compressed_pairIPN5draco17AttributeMetadataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.38"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.38"*, align 4
  store %"class.std::__2::__compressed_pair.38"* %this, %"class.std::__2::__compressed_pair.38"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.38"*, %"class.std::__2::__compressed_pair.38"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.38"* %this1 to %"struct.std::__2::__compressed_pair_elem.39"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributeMetadata"** @_ZNSt3__222__compressed_pair_elemIPN5draco17AttributeMetadataELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.39"* %0) #7
  ret %"class.draco::AttributeMetadata"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::AttributeMetadata"** @_ZNSt3__222__compressed_pair_elemIPN5draco17AttributeMetadataELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.39"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.39"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.39"* %this, %"struct.std::__2::__compressed_pair_elem.39"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.39"*, %"struct.std::__2::__compressed_pair_elem.39"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.39", %"struct.std::__2::__compressed_pair_elem.39"* %this1, i32 0, i32 0
  ret %"class.draco::AttributeMetadata"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete"* @_ZNSt3__217__compressed_pairIPN5draco17AttributeMetadataENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.38"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.38"*, align 4
  store %"class.std::__2::__compressed_pair.38"* %this, %"class.std::__2::__compressed_pair.38"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.38"*, %"class.std::__2::__compressed_pair.38"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.38"* %this1 to %"struct.std::__2::__compressed_pair_elem.40"*
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco17AttributeMetadataEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.40"* %0) #7
  ret %"struct.std::__2::default_delete"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco17AttributeMetadataEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.40"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.40"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.40"* %this, %"struct.std::__2::__compressed_pair_elem.40"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.40"*, %"struct.std::__2::__compressed_pair_elem.40"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.40"* %this1 to %"struct.std::__2::default_delete"*
  ret %"struct.std::__2::default_delete"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::AttributeMetadata"** @_ZNSt3__27forwardIPN5draco17AttributeMetadataEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::AttributeMetadata"** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.draco::AttributeMetadata"**, align 4
  store %"class.draco::AttributeMetadata"** %__t, %"class.draco::AttributeMetadata"*** %__t.addr, align 4
  %0 = load %"class.draco::AttributeMetadata"**, %"class.draco::AttributeMetadata"*** %__t.addr, align 4
  ret %"class.draco::AttributeMetadata"** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.39"* @_ZNSt3__222__compressed_pair_elemIPN5draco17AttributeMetadataELi0ELb0EEC2IS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.39"* returned %this, %"class.draco::AttributeMetadata"** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.39"*, align 4
  %__u.addr = alloca %"class.draco::AttributeMetadata"**, align 4
  store %"struct.std::__2::__compressed_pair_elem.39"* %this, %"struct.std::__2::__compressed_pair_elem.39"** %this.addr, align 4
  store %"class.draco::AttributeMetadata"** %__u, %"class.draco::AttributeMetadata"*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.39"*, %"struct.std::__2::__compressed_pair_elem.39"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.39", %"struct.std::__2::__compressed_pair_elem.39"* %this1, i32 0, i32 0
  %0 = load %"class.draco::AttributeMetadata"**, %"class.draco::AttributeMetadata"*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributeMetadata"** @_ZNSt3__27forwardIPN5draco17AttributeMetadataEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::AttributeMetadata"** nonnull align 4 dereferenceable(4) %0) #7
  %1 = load %"class.draco::AttributeMetadata"*, %"class.draco::AttributeMetadata"** %call, align 4
  store %"class.draco::AttributeMetadata"* %1, %"class.draco::AttributeMetadata"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.39"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.40"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco17AttributeMetadataEEELi1ELb1EEC2IS4_vEEOT_(%"struct.std::__2::__compressed_pair_elem.40"* returned %this, %"struct.std::__2::default_delete"* nonnull align 1 dereferenceable(1) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.40"*, align 4
  %__u.addr = alloca %"struct.std::__2::default_delete"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.40"* %this, %"struct.std::__2::__compressed_pair_elem.40"** %this.addr, align 4
  store %"struct.std::__2::default_delete"* %__u, %"struct.std::__2::default_delete"** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.40"*, %"struct.std::__2::__compressed_pair_elem.40"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.40"* %this1 to %"struct.std::__2::default_delete"*
  %1 = load %"struct.std::__2::default_delete"*, %"struct.std::__2::default_delete"** %__u.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete"* @_ZNSt3__27forwardINS_14default_deleteIN5draco17AttributeMetadataEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete"* nonnull align 1 dereferenceable(1) %1) #7
  ret %"struct.std::__2::__compressed_pair_elem.40"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.44"* @_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE6secondEv(%"class.std::__2::__compressed_pair.41"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.41"*, align 4
  store %"class.std::__2::__compressed_pair.41"* %this, %"class.std::__2::__compressed_pair.41"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.41"*, %"class.std::__2::__compressed_pair.41"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.41"* %this1 to %"struct.std::__2::__compressed_pair_elem.43"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.44"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.43"* %0) #7
  ret %"class.std::__2::allocator.44"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.44"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.43"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.43"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.43"* %this, %"struct.std::__2::__compressed_pair_elem.43"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.43"*, %"struct.std::__2::__compressed_pair_elem.43"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.43"* %this1 to %"class.std::__2::allocator.44"*
  ret %"class.std::__2::allocator.44"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE11__recommendEm(%"class.std::__2::vector"* %this, i32 %__new_size) #0 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__new_size.addr = alloca i32, align 4
  %__ms = alloca i32, align 4
  %__cap = alloca i32, align 4
  %ref.tmp = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__new_size, i32* %__new_size.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8max_sizeEv(%"class.std::__2::vector"* %this1) #7
  store i32 %call, i32* %__ms, align 4
  %0 = load i32, i32* %__new_size.addr, align 4
  %1 = load i32, i32* %__ms, align 4
  %cmp = icmp ugt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base_common"*
  call void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"* %2) #8
  unreachable

if.end:                                           ; preds = %entry
  %call2 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector"* %this1) #7
  store i32 %call2, i32* %__cap, align 4
  %3 = load i32, i32* %__cap, align 4
  %4 = load i32, i32* %__ms, align 4
  %div = udiv i32 %4, 2
  %cmp3 = icmp uge i32 %3, %div
  br i1 %cmp3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %if.end
  %5 = load i32, i32* %__ms, align 4
  store i32 %5, i32* %retval, align 4
  br label %return

if.end5:                                          ; preds = %if.end
  %6 = load i32, i32* %__cap, align 4
  %mul = mul i32 2, %6
  store i32 %mul, i32* %ref.tmp, align 4
  %call6 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %__new_size.addr)
  %7 = load i32, i32* %call6, align 4
  store i32 %7, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end5, %if.then4
  %8 = load i32, i32* %retval, align 4
  ret i32 %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 1
  %1 = load %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %2, i32 0, i32 0
  %3 = load %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.std::__2::unique_ptr.37"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.std::__2::unique_ptr.37"* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEEC2EmmS9_(%"struct.std::__2::__split_buffer"* returned %this, i32 %__cap, i32 %__start, %"class.std::__2::allocator.44"* nonnull align 1 dereferenceable(1) %__a) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"struct.std::__2::__split_buffer"*, align 4
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__cap.addr = alloca i32, align 4
  %__start.addr = alloca i32, align 4
  %__a.addr = alloca %"class.std::__2::allocator.44"*, align 4
  %ref.tmp = alloca i8*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store i32 %__cap, i32* %__cap.addr, align 4
  store i32 %__start, i32* %__start.addr, align 4
  store %"class.std::__2::allocator.44"* %__a, %"class.std::__2::allocator.44"** %__a.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer"* %this1, %"struct.std::__2::__split_buffer"** %retval, align 4
  %0 = bitcast %"struct.std::__2::__split_buffer"* %this1 to %"class.std::__2::__split_buffer_common"*
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  store i8* null, i8** %ref.tmp, align 4
  %1 = load %"class.std::__2::allocator.44"*, %"class.std::__2::allocator.44"** %__a.addr, align 4
  %call = call %"class.std::__2::__compressed_pair.51"* @_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEEC2IDnSA_EEOT_OT0_(%"class.std::__2::__compressed_pair.51"* %__end_cap_, i8** nonnull align 4 dereferenceable(4) %ref.tmp, %"class.std::__2::allocator.44"* nonnull align 1 dereferenceable(1) %1)
  %2 = load i32, i32* %__cap.addr, align 4
  %cmp = icmp ne i32 %2, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.44"* @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE7__allocEv(%"struct.std::__2::__split_buffer"* %this1) #7
  %3 = load i32, i32* %__cap.addr, align 4
  %call3 = call %"class.std::__2::unique_ptr.37"* @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEEE8allocateERS8_m(%"class.std::__2::allocator.44"* nonnull align 1 dereferenceable(1) %call2, i32 %3)
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %"class.std::__2::unique_ptr.37"* [ %call3, %cond.true ], [ null, %cond.false ]
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  store %"class.std::__2::unique_ptr.37"* %cond, %"class.std::__2::unique_ptr.37"** %__first_, align 4
  %__first_4 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %4 = load %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"** %__first_4, align 4
  %5 = load i32, i32* %__start.addr, align 4
  %add.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.37", %"class.std::__2::unique_ptr.37"* %4, i32 %5
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 2
  store %"class.std::__2::unique_ptr.37"* %add.ptr, %"class.std::__2::unique_ptr.37"** %__end_, align 4
  %__begin_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 1
  store %"class.std::__2::unique_ptr.37"* %add.ptr, %"class.std::__2::unique_ptr.37"** %__begin_, align 4
  %__first_5 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %6 = load %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"** %__first_5, align 4
  %7 = load i32, i32* %__cap.addr, align 4
  %add.ptr6 = getelementptr inbounds %"class.std::__2::unique_ptr.37", %"class.std::__2::unique_ptr.37"* %6, i32 %7
  %call7 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.37"** @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this1) #7
  store %"class.std::__2::unique_ptr.37"* %add.ptr6, %"class.std::__2::unique_ptr.37"** %call7, align 4
  %8 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %retval, align 4
  ret %"struct.std::__2::__split_buffer"* %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS6_RS8_EE(%"class.std::__2::vector"* %this, %"struct.std::__2::__split_buffer"* nonnull align 4 dereferenceable(20) %__v) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__v.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer"* %__v, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  call void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__annotate_deleteEv(%"class.std::__2::vector"* %this1) #7
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.44"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base"* %0) #7
  %1 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %1, i32 0, i32 0
  %2 = load %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"** %__begin_, align 4
  %3 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %3, i32 0, i32 1
  %4 = load %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"** %__end_, align 4
  %5 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__begin_2 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %5, i32 0, i32 1
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEEE46__construct_backward_with_exception_guaranteesIPS7_EEvRS8_T_SD_RSD_(%"class.std::__2::allocator.44"* nonnull align 1 dereferenceable(1) %call, %"class.std::__2::unique_ptr.37"* %2, %"class.std::__2::unique_ptr.37"* %4, %"class.std::__2::unique_ptr.37"** nonnull align 4 dereferenceable(4) %__begin_2)
  %6 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_3 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %6, i32 0, i32 0
  %7 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__begin_4 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %7, i32 0, i32 1
  call void @_ZNSt3__24swapIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS9_EE5valueEvE4typeERS9_SC_(%"class.std::__2::unique_ptr.37"** nonnull align 4 dereferenceable(4) %__begin_3, %"class.std::__2::unique_ptr.37"** nonnull align 4 dereferenceable(4) %__begin_4) #7
  %8 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__end_5 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %8, i32 0, i32 1
  %9 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__end_6 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %9, i32 0, i32 2
  call void @_ZNSt3__24swapIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS9_EE5valueEvE4typeERS9_SC_(%"class.std::__2::unique_ptr.37"** nonnull align 4 dereferenceable(4) %__end_5, %"class.std::__2::unique_ptr.37"** nonnull align 4 dereferenceable(4) %__end_6) #7
  %10 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call7 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.37"** @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE9__end_capEv(%"class.std::__2::__vector_base"* %10) #7
  %11 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %call8 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.37"** @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE9__end_capEv(%"struct.std::__2::__split_buffer"* %11) #7
  call void @_ZNSt3__24swapIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS9_EE5valueEvE4typeERS9_SC_(%"class.std::__2::unique_ptr.37"** nonnull align 4 dereferenceable(4) %call7, %"class.std::__2::unique_ptr.37"** nonnull align 4 dereferenceable(4) %call8) #7
  %12 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__begin_9 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %12, i32 0, i32 1
  %13 = load %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"** %__begin_9, align 4
  %14 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %14, i32 0, i32 0
  store %"class.std::__2::unique_ptr.37"* %13, %"class.std::__2::unique_ptr.37"** %__first_, align 4
  %call10 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector"* %this1) #7
  call void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE14__annotate_newEm(%"class.std::__2::vector"* %this1, i32 %call10) #7
  call void @_ZNSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector"* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEED2Ev(%"struct.std::__2::__split_buffer"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"struct.std::__2::__split_buffer"*, align 4
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer"* %this1, %"struct.std::__2::__split_buffer"** %retval, align 4
  call void @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE5clearEv(%"struct.std::__2::__split_buffer"* %this1) #7
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"** %__first_, align 4
  %tobool = icmp ne %"class.std::__2::unique_ptr.37"* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.44"* @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE7__allocEv(%"struct.std::__2::__split_buffer"* %this1) #7
  %__first_2 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"** %__first_2, align 4
  %call3 = call i32 @_ZNKSt3__214__split_bufferINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE8capacityEv(%"struct.std::__2::__split_buffer"* %this1)
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEEE10deallocateERS8_PS7_m(%"class.std::__2::allocator.44"* nonnull align 1 dereferenceable(1) %call, %"class.std::__2::unique_ptr.37"* %1, i32 %call3) #7
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %retval, align 4
  ret %"struct.std::__2::__split_buffer"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8max_sizeEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp3 = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.44"* @_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base"* %0) #7
  %call2 = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEEE8max_sizeERKS8_(%"class.std::__2::allocator.44"* nonnull align 1 dereferenceable(1) %call) #7
  store i32 %call2, i32* %ref.tmp, align 4
  %call4 = call i32 @_ZNSt3__214numeric_limitsIlE3maxEv() #7
  store i32 %call4, i32* %ref.tmp3, align 4
  %call5 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %1 = load i32, i32* %call5, align 4
  ret i32 %1
}

; Function Attrs: noreturn
declare void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"*) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call i32 @_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::__vector_base"* %0) #7
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::__less", align 1
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__a.addr, align 4
  %1 = load i32*, i32** %__b.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::__less", align 1
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__a.addr, align 4
  %1 = load i32*, i32** %__b.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEEE8max_sizeERKS8_(%"class.std::__2::allocator.44"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.44"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_max_size", align 1
  store %"class.std::__2::allocator.44"* %__a, %"class.std::__2::allocator.44"** %__a.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_max_size"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.44"*, %"class.std::__2::allocator.44"** %__a.addr, align 4
  %call = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS8_(%"class.std::__2::allocator.44"* nonnull align 1 dereferenceable(1) %1) #7
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.44"* @_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.44"* @_ZNKSt3__217__compressed_pairIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE6secondEv(%"class.std::__2::__compressed_pair.41"* %__end_cap_) #7
  ret %"class.std::__2::allocator.44"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__214numeric_limitsIlE3maxEv() #0 comdat {
entry:
  %call = call i32 @_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv() #7
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__comp = alloca %"struct.std::__2::__less", align 1
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__b.addr, align 4
  %1 = load i32*, i32** %__a.addr, align 4
  %call = call zeroext i1 @_ZNKSt3__26__lessImmEclERKmS3_(%"struct.std::__2::__less"* %__comp, i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  br i1 %call, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32*, i32** %__b.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %3 = load i32*, i32** %__a.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi i32* [ %2, %cond.true ], [ %3, %cond.false ]
  ret i32* %cond-lvalue
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__26__lessImmEclERKmS3_(%"struct.std::__2::__less"* %this, i32* nonnull align 4 dereferenceable(4) %__x, i32* nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__less"*, align 4
  %__x.addr = alloca i32*, align 4
  %__y.addr = alloca i32*, align 4
  store %"struct.std::__2::__less"* %this, %"struct.std::__2::__less"** %this.addr, align 4
  store i32* %__x, i32** %__x.addr, align 4
  store i32* %__y, i32** %__y.addr, align 4
  %this1 = load %"struct.std::__2::__less"*, %"struct.std::__2::__less"** %this.addr, align 4
  %0 = load i32*, i32** %__x.addr, align 4
  %1 = load i32, i32* %0, align 4
  %2 = load i32*, i32** %__y.addr, align 4
  %3 = load i32, i32* %2, align 4
  %cmp = icmp ult i32 %1, %3
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS8_(%"class.std::__2::allocator.44"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.44"*, align 4
  store %"class.std::__2::allocator.44"* %__a, %"class.std::__2::allocator.44"** %__a.addr, align 4
  %1 = load %"class.std::__2::allocator.44"*, %"class.std::__2::allocator.44"** %__a.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEE8max_sizeEv(%"class.std::__2::allocator.44"* %1) #7
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__29allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEE8max_sizeEv(%"class.std::__2::allocator.44"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.44"*, align 4
  store %"class.std::__2::allocator.44"* %this, %"class.std::__2::allocator.44"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.44"*, %"class.std::__2::allocator.44"** %this.addr, align 4
  ret i32 1073741823
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.44"* @_ZNKSt3__217__compressed_pairIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE6secondEv(%"class.std::__2::__compressed_pair.41"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.41"*, align 4
  store %"class.std::__2::__compressed_pair.41"* %this, %"class.std::__2::__compressed_pair.41"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.41"*, %"class.std::__2::__compressed_pair.41"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.41"* %this1 to %"struct.std::__2::__compressed_pair_elem.43"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.44"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.43"* %0) #7
  ret %"class.std::__2::allocator.44"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.44"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.43"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.43"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.43"* %this, %"struct.std::__2::__compressed_pair_elem.43"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.43"*, %"struct.std::__2::__compressed_pair_elem.43"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.43"* %this1 to %"class.std::__2::allocator.44"*
  ret %"class.std::__2::allocator.44"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv() #0 comdat {
entry:
  ret i32 2147483647
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.37"** @_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE9__end_capEv(%"class.std::__2::__vector_base"* %this1) #7
  %0 = load %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.std::__2::unique_ptr.37"* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.std::__2::unique_ptr.37"* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.37"** @_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE9__end_capEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.37"** @_ZNKSt3__217__compressed_pairIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.41"* %__end_cap_) #7
  ret %"class.std::__2::unique_ptr.37"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.37"** @_ZNKSt3__217__compressed_pairIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.41"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.41"*, align 4
  store %"class.std::__2::__compressed_pair.41"* %this, %"class.std::__2::__compressed_pair.41"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.41"*, %"class.std::__2::__compressed_pair.41"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.41"* %this1 to %"struct.std::__2::__compressed_pair_elem.42"*
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.37"** @_ZNKSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.42"* %0) #7
  ret %"class.std::__2::unique_ptr.37"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.37"** @_ZNKSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.42"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.42"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.42"* %this, %"struct.std::__2::__compressed_pair_elem.42"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.42"*, %"struct.std::__2::__compressed_pair_elem.42"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.42", %"struct.std::__2::__compressed_pair_elem.42"* %this1, i32 0, i32 0
  ret %"class.std::__2::unique_ptr.37"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__comp = alloca %"struct.std::__2::__less", align 1
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__a.addr, align 4
  %1 = load i32*, i32** %__b.addr, align 4
  %call = call zeroext i1 @_ZNKSt3__26__lessImmEclERKmS3_(%"struct.std::__2::__less"* %__comp, i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  br i1 %call, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32*, i32** %__b.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %3 = load i32*, i32** %__a.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi i32* [ %2, %cond.true ], [ %3, %cond.false ]
  ret i32* %cond-lvalue
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.51"* @_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEEC2IDnSA_EEOT_OT0_(%"class.std::__2::__compressed_pair.51"* returned %this, i8** nonnull align 4 dereferenceable(4) %__t1, %"class.std::__2::allocator.44"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.51"*, align 4
  %__t1.addr = alloca i8**, align 4
  %__t2.addr = alloca %"class.std::__2::allocator.44"*, align 4
  store %"class.std::__2::__compressed_pair.51"* %this, %"class.std::__2::__compressed_pair.51"** %this.addr, align 4
  store i8** %__t1, i8*** %__t1.addr, align 4
  store %"class.std::__2::allocator.44"* %__t2, %"class.std::__2::allocator.44"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.51"*, %"class.std::__2::__compressed_pair.51"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.51"* %this1 to %"struct.std::__2::__compressed_pair_elem.42"*
  %1 = load i8**, i8*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %1) #7
  %call2 = call %"struct.std::__2::__compressed_pair_elem.42"* @_ZNSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEELi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.42"* %0, i8** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.51"* %this1 to i8*
  %3 = getelementptr inbounds i8, i8* %2, i32 4
  %4 = bitcast i8* %3 to %"struct.std::__2::__compressed_pair_elem.52"*
  %5 = load %"class.std::__2::allocator.44"*, %"class.std::__2::allocator.44"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.44"* @_ZNSt3__27forwardIRNS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEEEEOT_RNS_16remove_referenceISA_E4typeE(%"class.std::__2::allocator.44"* nonnull align 1 dereferenceable(1) %5) #7
  %call4 = call %"struct.std::__2::__compressed_pair_elem.52"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEELi1ELb0EEC2IS9_vEEOT_(%"struct.std::__2::__compressed_pair_elem.52"* %4, %"class.std::__2::allocator.44"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::__compressed_pair.51"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.37"* @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEEE8allocateERS8_m(%"class.std::__2::allocator.44"* nonnull align 1 dereferenceable(1) %__a, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.44"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.44"* %__a, %"class.std::__2::allocator.44"** %__a.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.44"*, %"class.std::__2::allocator.44"** %__a.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call %"class.std::__2::unique_ptr.37"* @_ZNSt3__29allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEE8allocateEmPKv(%"class.std::__2::allocator.44"* %0, i32 %1, i8* null)
  ret %"class.std::__2::unique_ptr.37"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.44"* @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE7__allocEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.44"* @_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE6secondEv(%"class.std::__2::__compressed_pair.51"* %__end_cap_) #7
  ret %"class.std::__2::allocator.44"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.37"** @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.37"** @_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.51"* %__end_cap_) #7
  ret %"class.std::__2::unique_ptr.37"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca i8**, align 4
  store i8** %__t, i8*** %__t.addr, align 4
  %0 = load i8**, i8*** %__t.addr, align 4
  ret i8** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.42"* @_ZNSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEELi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.42"* returned %this, i8** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.42"*, align 4
  %__u.addr = alloca i8**, align 4
  store %"struct.std::__2::__compressed_pair_elem.42"* %this, %"struct.std::__2::__compressed_pair_elem.42"** %this.addr, align 4
  store i8** %__u, i8*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.42"*, %"struct.std::__2::__compressed_pair_elem.42"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.42", %"struct.std::__2::__compressed_pair_elem.42"* %this1, i32 0, i32 0
  %0 = load i8**, i8*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %0) #7
  store %"class.std::__2::unique_ptr.37"* null, %"class.std::__2::unique_ptr.37"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.42"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.44"* @_ZNSt3__27forwardIRNS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEEEEOT_RNS_16remove_referenceISA_E4typeE(%"class.std::__2::allocator.44"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::allocator.44"*, align 4
  store %"class.std::__2::allocator.44"* %__t, %"class.std::__2::allocator.44"** %__t.addr, align 4
  %0 = load %"class.std::__2::allocator.44"*, %"class.std::__2::allocator.44"** %__t.addr, align 4
  ret %"class.std::__2::allocator.44"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.52"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEELi1ELb0EEC2IS9_vEEOT_(%"struct.std::__2::__compressed_pair_elem.52"* returned %this, %"class.std::__2::allocator.44"* nonnull align 1 dereferenceable(1) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.52"*, align 4
  %__u.addr = alloca %"class.std::__2::allocator.44"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.52"* %this, %"struct.std::__2::__compressed_pair_elem.52"** %this.addr, align 4
  store %"class.std::__2::allocator.44"* %__u, %"class.std::__2::allocator.44"** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.52"*, %"struct.std::__2::__compressed_pair_elem.52"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.52", %"struct.std::__2::__compressed_pair_elem.52"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::allocator.44"*, %"class.std::__2::allocator.44"** %__u.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.44"* @_ZNSt3__27forwardIRNS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEEEEOT_RNS_16remove_referenceISA_E4typeE(%"class.std::__2::allocator.44"* nonnull align 1 dereferenceable(1) %0) #7
  store %"class.std::__2::allocator.44"* %call, %"class.std::__2::allocator.44"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.52"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.37"* @_ZNSt3__29allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEE8allocateEmPKv(%"class.std::__2::allocator.44"* %this, i32 %__n, i8* %0) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.44"*, align 4
  %__n.addr = alloca i32, align 4
  %.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.44"* %this, %"class.std::__2::allocator.44"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::allocator.44"*, %"class.std::__2::allocator.44"** %this.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEE8max_sizeEv(%"class.std::__2::allocator.44"* %this1) #7
  %cmp = icmp ugt i32 %1, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__220__throw_length_errorEPKc(i8* getelementptr inbounds ([68 x i8], [68 x i8]* @.str, i32 0, i32 0)) #8
  unreachable

if.end:                                           ; preds = %entry
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  %call2 = call i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %mul, i32 4)
  %3 = bitcast i8* %call2 to %"class.std::__2::unique_ptr.37"*
  ret %"class.std::__2::unique_ptr.37"* %3
}

; Function Attrs: noinline noreturn nounwind optnone
define linkonce_odr hidden void @_ZNSt3__220__throw_length_errorEPKc(i8* %__msg) #4 comdat {
entry:
  %__msg.addr = alloca i8*, align 4
  store i8* %__msg, i8** %__msg.addr, align 4
  call void @abort() #8
  unreachable
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %__size, i32 %__align) #0 comdat {
entry:
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i32, i32* %__size.addr, align 4
  %call = call noalias nonnull i8* @_Znwm(i32 %0) #9
  ret i8* %call
}

; Function Attrs: noreturn
declare void @abort() #3

; Function Attrs: nobuiltin allocsize(0)
declare nonnull i8* @_Znwm(i32) #5

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.44"* @_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE6secondEv(%"class.std::__2::__compressed_pair.51"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.51"*, align 4
  store %"class.std::__2::__compressed_pair.51"* %this, %"class.std::__2::__compressed_pair.51"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.51"*, %"class.std::__2::__compressed_pair.51"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.51"* %this1 to i8*
  %add.ptr = getelementptr inbounds i8, i8* %0, i32 4
  %1 = bitcast i8* %add.ptr to %"struct.std::__2::__compressed_pair_elem.52"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.44"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.52"* %1) #7
  ret %"class.std::__2::allocator.44"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.44"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.52"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.52"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.52"* %this, %"struct.std::__2::__compressed_pair_elem.52"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.52"*, %"struct.std::__2::__compressed_pair_elem.52"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.52", %"struct.std::__2::__compressed_pair_elem.52"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::allocator.44"*, %"class.std::__2::allocator.44"** %__value_, align 4
  ret %"class.std::__2::allocator.44"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.37"** @_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.51"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.51"*, align 4
  store %"class.std::__2::__compressed_pair.51"* %this, %"class.std::__2::__compressed_pair.51"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.51"*, %"class.std::__2::__compressed_pair.51"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.51"* %this1 to %"struct.std::__2::__compressed_pair_elem.42"*
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.37"** @_ZNSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.42"* %0) #7
  ret %"class.std::__2::unique_ptr.37"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__annotate_deleteEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %call = call %"class.std::__2::unique_ptr.37"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector"* %this1) #7
  %0 = bitcast %"class.std::__2::unique_ptr.37"* %call to i8*
  %call2 = call %"class.std::__2::unique_ptr.37"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector"* %this1) #7
  %call3 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector"* %this1) #7
  %add.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.37", %"class.std::__2::unique_ptr.37"* %call2, i32 %call3
  %1 = bitcast %"class.std::__2::unique_ptr.37"* %add.ptr to i8*
  %call4 = call %"class.std::__2::unique_ptr.37"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector"* %this1) #7
  %call5 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector"* %this1) #7
  %add.ptr6 = getelementptr inbounds %"class.std::__2::unique_ptr.37", %"class.std::__2::unique_ptr.37"* %call4, i32 %call5
  %2 = bitcast %"class.std::__2::unique_ptr.37"* %add.ptr6 to i8*
  %call7 = call %"class.std::__2::unique_ptr.37"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector"* %this1) #7
  %call8 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector"* %this1) #7
  %add.ptr9 = getelementptr inbounds %"class.std::__2::unique_ptr.37", %"class.std::__2::unique_ptr.37"* %call7, i32 %call8
  %3 = bitcast %"class.std::__2::unique_ptr.37"* %add.ptr9 to i8*
  call void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE31__annotate_contiguous_containerEPKvSB_SB_SB_(%"class.std::__2::vector"* %this1, i8* %0, i8* %1, i8* %2, i8* %3) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEEE46__construct_backward_with_exception_guaranteesIPS7_EEvRS8_T_SD_RSD_(%"class.std::__2::allocator.44"* nonnull align 1 dereferenceable(1) %__a, %"class.std::__2::unique_ptr.37"* %__begin1, %"class.std::__2::unique_ptr.37"* %__end1, %"class.std::__2::unique_ptr.37"** nonnull align 4 dereferenceable(4) %__end2) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.44"*, align 4
  %__begin1.addr = alloca %"class.std::__2::unique_ptr.37"*, align 4
  %__end1.addr = alloca %"class.std::__2::unique_ptr.37"*, align 4
  %__end2.addr = alloca %"class.std::__2::unique_ptr.37"**, align 4
  store %"class.std::__2::allocator.44"* %__a, %"class.std::__2::allocator.44"** %__a.addr, align 4
  store %"class.std::__2::unique_ptr.37"* %__begin1, %"class.std::__2::unique_ptr.37"** %__begin1.addr, align 4
  store %"class.std::__2::unique_ptr.37"* %__end1, %"class.std::__2::unique_ptr.37"** %__end1.addr, align 4
  store %"class.std::__2::unique_ptr.37"** %__end2, %"class.std::__2::unique_ptr.37"*** %__end2.addr, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %0 = load %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"** %__end1.addr, align 4
  %1 = load %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"** %__begin1.addr, align 4
  %cmp = icmp ne %"class.std::__2::unique_ptr.37"* %0, %1
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %2 = load %"class.std::__2::allocator.44"*, %"class.std::__2::allocator.44"** %__a.addr, align 4
  %3 = load %"class.std::__2::unique_ptr.37"**, %"class.std::__2::unique_ptr.37"*** %__end2.addr, align 4
  %4 = load %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"** %3, align 4
  %add.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.37", %"class.std::__2::unique_ptr.37"* %4, i32 -1
  %call = call %"class.std::__2::unique_ptr.37"* @_ZNSt3__212__to_addressINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEEPT_S8_(%"class.std::__2::unique_ptr.37"* %add.ptr) #7
  %5 = load %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"** %__end1.addr, align 4
  %incdec.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.37", %"class.std::__2::unique_ptr.37"* %5, i32 -1
  store %"class.std::__2::unique_ptr.37"* %incdec.ptr, %"class.std::__2::unique_ptr.37"** %__end1.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.37"* @_ZNSt3__24moveIRNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOS9_(%"class.std::__2::unique_ptr.37"* nonnull align 4 dereferenceable(4) %incdec.ptr) #7
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEEE9constructIS7_JS7_EEEvRS8_PT_DpOT0_(%"class.std::__2::allocator.44"* nonnull align 1 dereferenceable(1) %2, %"class.std::__2::unique_ptr.37"* %call, %"class.std::__2::unique_ptr.37"* nonnull align 4 dereferenceable(4) %call1)
  %6 = load %"class.std::__2::unique_ptr.37"**, %"class.std::__2::unique_ptr.37"*** %__end2.addr, align 4
  %7 = load %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"** %6, align 4
  %incdec.ptr2 = getelementptr inbounds %"class.std::__2::unique_ptr.37", %"class.std::__2::unique_ptr.37"* %7, i32 -1
  store %"class.std::__2::unique_ptr.37"* %incdec.ptr2, %"class.std::__2::unique_ptr.37"** %6, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__24swapIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS9_EE5valueEvE4typeERS9_SC_(%"class.std::__2::unique_ptr.37"** nonnull align 4 dereferenceable(4) %__x, %"class.std::__2::unique_ptr.37"** nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %__x.addr = alloca %"class.std::__2::unique_ptr.37"**, align 4
  %__y.addr = alloca %"class.std::__2::unique_ptr.37"**, align 4
  %__t = alloca %"class.std::__2::unique_ptr.37"*, align 4
  store %"class.std::__2::unique_ptr.37"** %__x, %"class.std::__2::unique_ptr.37"*** %__x.addr, align 4
  store %"class.std::__2::unique_ptr.37"** %__y, %"class.std::__2::unique_ptr.37"*** %__y.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.37"**, %"class.std::__2::unique_ptr.37"*** %__x.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.37"** @_ZNSt3__24moveIRPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOSA_(%"class.std::__2::unique_ptr.37"** nonnull align 4 dereferenceable(4) %0) #7
  %1 = load %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"** %call, align 4
  store %"class.std::__2::unique_ptr.37"* %1, %"class.std::__2::unique_ptr.37"** %__t, align 4
  %2 = load %"class.std::__2::unique_ptr.37"**, %"class.std::__2::unique_ptr.37"*** %__y.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.37"** @_ZNSt3__24moveIRPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOSA_(%"class.std::__2::unique_ptr.37"** nonnull align 4 dereferenceable(4) %2) #7
  %3 = load %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"** %call1, align 4
  %4 = load %"class.std::__2::unique_ptr.37"**, %"class.std::__2::unique_ptr.37"*** %__x.addr, align 4
  store %"class.std::__2::unique_ptr.37"* %3, %"class.std::__2::unique_ptr.37"** %4, align 4
  %call2 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.37"** @_ZNSt3__24moveIRPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOSA_(%"class.std::__2::unique_ptr.37"** nonnull align 4 dereferenceable(4) %__t) #7
  %5 = load %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"** %call2, align 4
  %6 = load %"class.std::__2::unique_ptr.37"**, %"class.std::__2::unique_ptr.37"*** %__y.addr, align 4
  store %"class.std::__2::unique_ptr.37"* %5, %"class.std::__2::unique_ptr.37"** %6, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE14__annotate_newEm(%"class.std::__2::vector"* %this, i32 %__current_size) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__current_size.addr = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__current_size, i32* %__current_size.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %call = call %"class.std::__2::unique_ptr.37"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector"* %this1) #7
  %0 = bitcast %"class.std::__2::unique_ptr.37"* %call to i8*
  %call2 = call %"class.std::__2::unique_ptr.37"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector"* %this1) #7
  %call3 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector"* %this1) #7
  %add.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.37", %"class.std::__2::unique_ptr.37"* %call2, i32 %call3
  %1 = bitcast %"class.std::__2::unique_ptr.37"* %add.ptr to i8*
  %call4 = call %"class.std::__2::unique_ptr.37"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector"* %this1) #7
  %call5 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector"* %this1) #7
  %add.ptr6 = getelementptr inbounds %"class.std::__2::unique_ptr.37", %"class.std::__2::unique_ptr.37"* %call4, i32 %call5
  %2 = bitcast %"class.std::__2::unique_ptr.37"* %add.ptr6 to i8*
  %call7 = call %"class.std::__2::unique_ptr.37"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector"* %this1) #7
  %3 = load i32, i32* %__current_size.addr, align 4
  %add.ptr8 = getelementptr inbounds %"class.std::__2::unique_ptr.37", %"class.std::__2::unique_ptr.37"* %call7, i32 %3
  %4 = bitcast %"class.std::__2::unique_ptr.37"* %add.ptr8 to i8*
  call void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE31__annotate_contiguous_containerEPKvSB_SB_SB_(%"class.std::__2::vector"* %this1, i8* %0, i8* %1, i8* %2, i8* %4) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE31__annotate_contiguous_containerEPKvSB_SB_SB_(%"class.std::__2::vector"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.37"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"** %__begin_, align 4
  %call = call %"class.std::__2::unique_ptr.37"* @_ZNSt3__212__to_addressINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEEPT_S8_(%"class.std::__2::unique_ptr.37"* %1) #7
  ret %"class.std::__2::unique_ptr.37"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.37"** @_ZNSt3__24moveIRPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOSA_(%"class.std::__2::unique_ptr.37"** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::unique_ptr.37"**, align 4
  store %"class.std::__2::unique_ptr.37"** %__t, %"class.std::__2::unique_ptr.37"*** %__t.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.37"**, %"class.std::__2::unique_ptr.37"*** %__t.addr, align 4
  ret %"class.std::__2::unique_ptr.37"** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE5clearEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 1
  %0 = load %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"** %__begin_, align 4
  call void @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE17__destruct_at_endEPS6_(%"struct.std::__2::__split_buffer"* %this1, %"class.std::__2::unique_ptr.37"* %0) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEEE10deallocateERS8_PS7_m(%"class.std::__2::allocator.44"* nonnull align 1 dereferenceable(1) %__a, %"class.std::__2::unique_ptr.37"* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.44"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.37"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.44"* %__a, %"class.std::__2::allocator.44"** %__a.addr, align 4
  store %"class.std::__2::unique_ptr.37"* %__p, %"class.std::__2::unique_ptr.37"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.44"*, %"class.std::__2::allocator.44"** %__a.addr, align 4
  %1 = load %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEE10deallocateEPS6_m(%"class.std::__2::allocator.44"* %0, %"class.std::__2::unique_ptr.37"* %1, i32 %2) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__214__split_bufferINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE8capacityEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.37"** @_ZNKSt3__214__split_bufferINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this1) #7
  %0 = load %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"** %call, align 4
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"** %__first_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.std::__2::unique_ptr.37"* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.std::__2::unique_ptr.37"* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE17__destruct_at_endEPS6_(%"struct.std::__2::__split_buffer"* %this, %"class.std::__2::unique_ptr.37"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__new_last.addr = alloca %"class.std::__2::unique_ptr.37"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant.53", align 1
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.37"* %__new_last, %"class.std::__2::unique_ptr.37"** %__new_last.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"** %__new_last.addr, align 4
  call void @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE17__destruct_at_endEPS6_NS_17integral_constantIbLb0EEE(%"struct.std::__2::__split_buffer"* %this1, %"class.std::__2::unique_ptr.37"* %0) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE17__destruct_at_endEPS6_NS_17integral_constantIbLb0EEE(%"struct.std::__2::__split_buffer"* %this, %"class.std::__2::unique_ptr.37"* %__new_last) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant.53", align 1
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__new_last.addr = alloca %"class.std::__2::unique_ptr.37"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.37"* %__new_last, %"class.std::__2::unique_ptr.37"** %__new_last.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"** %__new_last.addr, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 2
  %2 = load %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"** %__end_, align 4
  %cmp = icmp ne %"class.std::__2::unique_ptr.37"* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.44"* @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE7__allocEv(%"struct.std::__2::__split_buffer"* %this1) #7
  %__end_2 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 2
  %3 = load %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"** %__end_2, align 4
  %incdec.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.37", %"class.std::__2::unique_ptr.37"* %3, i32 -1
  store %"class.std::__2::unique_ptr.37"* %incdec.ptr, %"class.std::__2::unique_ptr.37"** %__end_2, align 4
  %call3 = call %"class.std::__2::unique_ptr.37"* @_ZNSt3__212__to_addressINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEEPT_S8_(%"class.std::__2::unique_ptr.37"* %incdec.ptr) #7
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEEE7destroyIS7_EEvRS8_PT_(%"class.std::__2::allocator.44"* nonnull align 1 dereferenceable(1) %call, %"class.std::__2::unique_ptr.37"* %call3)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEEE7destroyIS7_EEvRS8_PT_(%"class.std::__2::allocator.44"* nonnull align 1 dereferenceable(1) %__a, %"class.std::__2::unique_ptr.37"* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.44"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.37"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy", align 1
  store %"class.std::__2::allocator.44"* %__a, %"class.std::__2::allocator.44"** %__a.addr, align 4
  store %"class.std::__2::unique_ptr.37"* %__p, %"class.std::__2::unique_ptr.37"** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.44"*, %"class.std::__2::allocator.44"** %__a.addr, align 4
  %2 = load %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEEE9__destroyIS7_EEvNS_17integral_constantIbLb1EEERS8_PT_(%"class.std::__2::allocator.44"* nonnull align 1 dereferenceable(1) %1, %"class.std::__2::unique_ptr.37"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEEE9__destroyIS7_EEvNS_17integral_constantIbLb1EEERS8_PT_(%"class.std::__2::allocator.44"* nonnull align 1 dereferenceable(1) %__a, %"class.std::__2::unique_ptr.37"* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.44"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.37"*, align 4
  store %"class.std::__2::allocator.44"* %__a, %"class.std::__2::allocator.44"** %__a.addr, align 4
  store %"class.std::__2::unique_ptr.37"* %__p, %"class.std::__2::unique_ptr.37"** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.44"*, %"class.std::__2::allocator.44"** %__a.addr, align 4
  %2 = load %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"** %__p.addr, align 4
  call void @_ZNSt3__29allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEE7destroyEPS6_(%"class.std::__2::allocator.44"* %1, %"class.std::__2::unique_ptr.37"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEE7destroyEPS6_(%"class.std::__2::allocator.44"* %this, %"class.std::__2::unique_ptr.37"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.44"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.37"*, align 4
  store %"class.std::__2::allocator.44"* %this, %"class.std::__2::allocator.44"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.37"* %__p, %"class.std::__2::unique_ptr.37"** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.44"*, %"class.std::__2::allocator.44"** %this.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"** %__p.addr, align 4
  %call = call %"class.std::__2::unique_ptr.37"* @_ZNSt3__210unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.37"* %0) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.37"* @_ZNSt3__210unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.37"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.37"*, align 4
  store %"class.std::__2::unique_ptr.37"* %this, %"class.std::__2::unique_ptr.37"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"** %this.addr, align 4
  call void @_ZNSt3__210unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.37"* %this1, %"class.draco::AttributeMetadata"* null) #7
  ret %"class.std::__2::unique_ptr.37"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__210unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.37"* %this, %"class.draco::AttributeMetadata"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.37"*, align 4
  %__p.addr = alloca %"class.draco::AttributeMetadata"*, align 4
  %__tmp = alloca %"class.draco::AttributeMetadata"*, align 4
  store %"class.std::__2::unique_ptr.37"* %this, %"class.std::__2::unique_ptr.37"** %this.addr, align 4
  store %"class.draco::AttributeMetadata"* %__p, %"class.draco::AttributeMetadata"** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.37", %"class.std::__2::unique_ptr.37"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributeMetadata"** @_ZNSt3__217__compressed_pairIPN5draco17AttributeMetadataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.38"* %__ptr_) #7
  %0 = load %"class.draco::AttributeMetadata"*, %"class.draco::AttributeMetadata"** %call, align 4
  store %"class.draco::AttributeMetadata"* %0, %"class.draco::AttributeMetadata"** %__tmp, align 4
  %1 = load %"class.draco::AttributeMetadata"*, %"class.draco::AttributeMetadata"** %__p.addr, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr.37", %"class.std::__2::unique_ptr.37"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::AttributeMetadata"** @_ZNSt3__217__compressed_pairIPN5draco17AttributeMetadataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.38"* %__ptr_2) #7
  store %"class.draco::AttributeMetadata"* %1, %"class.draco::AttributeMetadata"** %call3, align 4
  %2 = load %"class.draco::AttributeMetadata"*, %"class.draco::AttributeMetadata"** %__tmp, align 4
  %tobool = icmp ne %"class.draco::AttributeMetadata"* %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__ptr_4 = getelementptr inbounds %"class.std::__2::unique_ptr.37", %"class.std::__2::unique_ptr.37"* %this1, i32 0, i32 0
  %call5 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete"* @_ZNSt3__217__compressed_pairIPN5draco17AttributeMetadataENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.38"* %__ptr_4) #7
  %3 = load %"class.draco::AttributeMetadata"*, %"class.draco::AttributeMetadata"** %__tmp, align 4
  call void @_ZNKSt3__214default_deleteIN5draco17AttributeMetadataEEclEPS2_(%"struct.std::__2::default_delete"* %call5, %"class.draco::AttributeMetadata"* %3) #7
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__214default_deleteIN5draco17AttributeMetadataEEclEPS2_(%"struct.std::__2::default_delete"* %this, %"class.draco::AttributeMetadata"* %__ptr) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::default_delete"*, align 4
  %__ptr.addr = alloca %"class.draco::AttributeMetadata"*, align 4
  store %"struct.std::__2::default_delete"* %this, %"struct.std::__2::default_delete"** %this.addr, align 4
  store %"class.draco::AttributeMetadata"* %__ptr, %"class.draco::AttributeMetadata"** %__ptr.addr, align 4
  %this1 = load %"struct.std::__2::default_delete"*, %"struct.std::__2::default_delete"** %this.addr, align 4
  %0 = load %"class.draco::AttributeMetadata"*, %"class.draco::AttributeMetadata"** %__ptr.addr, align 4
  %isnull = icmp eq %"class.draco::AttributeMetadata"* %0, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %entry
  %call = call %"class.draco::AttributeMetadata"* @_ZN5draco17AttributeMetadataD2Ev(%"class.draco::AttributeMetadata"* %0) #7
  %1 = bitcast %"class.draco::AttributeMetadata"* %0 to i8*
  call void @_ZdlPv(i8* %1) #10
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::AttributeMetadata"* @_ZN5draco17AttributeMetadataD2Ev(%"class.draco::AttributeMetadata"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::AttributeMetadata"*, align 4
  store %"class.draco::AttributeMetadata"* %this, %"class.draco::AttributeMetadata"** %this.addr, align 4
  %this1 = load %"class.draco::AttributeMetadata"*, %"class.draco::AttributeMetadata"** %this.addr, align 4
  %0 = bitcast %"class.draco::AttributeMetadata"* %this1 to %"class.draco::Metadata"*
  %call = call %"class.draco::Metadata"* @_ZN5draco8MetadataD2Ev(%"class.draco::Metadata"* %0) #7
  ret %"class.draco::AttributeMetadata"* %this1
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #6

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unordered_map.14"* @_ZNSt3__213unordered_mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteIS9_EEEENS_4hashIS6_EENS_8equal_toIS6_EENS4_INS_4pairIKS6_SC_EEEEED2Ev(%"class.std::__2::unordered_map.14"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unordered_map.14"*, align 4
  store %"class.std::__2::unordered_map.14"* %this, %"class.std::__2::unordered_map.14"** %this.addr, align 4
  %this1 = load %"class.std::__2::unordered_map.14"*, %"class.std::__2::unordered_map.14"** %this.addr, align 4
  %__table_ = getelementptr inbounds %"class.std::__2::unordered_map.14", %"class.std::__2::unordered_map.14"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::__hash_table.15"* @_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEEENS_22__unordered_map_hasherIS7_SE_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SE_NS_8equal_toIS7_EELb1EEENS5_ISE_EEED2Ev(%"class.std::__2::__hash_table.15"* %__table_) #7
  ret %"class.std::__2::unordered_map.14"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unordered_map"* @_ZNSt3__213unordered_mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueENS_4hashIS6_EENS_8equal_toIS6_EENS4_INS_4pairIKS6_S8_EEEEED2Ev(%"class.std::__2::unordered_map"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unordered_map"*, align 4
  store %"class.std::__2::unordered_map"* %this, %"class.std::__2::unordered_map"** %this.addr, align 4
  %this1 = load %"class.std::__2::unordered_map"*, %"class.std::__2::unordered_map"** %this.addr, align 4
  %__table_ = getelementptr inbounds %"class.std::__2::unordered_map", %"class.std::__2::unordered_map"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::__hash_table"* @_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEENS_22__unordered_map_hasherIS7_SA_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SA_NS_8equal_toIS7_EELb1EEENS5_ISA_EEED2Ev(%"class.std::__2::__hash_table"* %__table_) #7
  ret %"class.std::__2::unordered_map"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__hash_table.15"* @_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEEENS_22__unordered_map_hasherIS7_SE_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SE_NS_8equal_toIS7_EELb1EEENS5_ISE_EEED2Ev(%"class.std::__2::__hash_table.15"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__hash_table.15"*, align 4
  store %"class.std::__2::__hash_table.15"* %this, %"class.std::__2::__hash_table.15"** %this.addr, align 4
  %this1 = load %"class.std::__2::__hash_table.15"*, %"class.std::__2::__hash_table.15"** %this.addr, align 4
  %__p1_ = getelementptr inbounds %"class.std::__2::__hash_table.15", %"class.std::__2::__hash_table.15"* %this1, i32 0, i32 1
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base.19"* @_ZNSt3__217__compressed_pairINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEENS7_ISI_EEE5firstEv(%"class.std::__2::__compressed_pair.26"* %__p1_) #7
  %__next_ = getelementptr inbounds %"struct.std::__2::__hash_node_base.19", %"struct.std::__2::__hash_node_base.19"* %call, i32 0, i32 0
  %0 = load %"struct.std::__2::__hash_node_base.19"*, %"struct.std::__2::__hash_node_base.19"** %__next_, align 4
  call void @_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEEENS_22__unordered_map_hasherIS7_SE_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SE_NS_8equal_toIS7_EELb1EEENS5_ISE_EEE17__deallocate_nodeEPNS_16__hash_node_baseIPNS_11__hash_nodeISE_PvEEEE(%"class.std::__2::__hash_table.15"* %this1, %"struct.std::__2::__hash_node_base.19"* %0) #7
  %__bucket_list_ = getelementptr inbounds %"class.std::__2::__hash_table.15", %"class.std::__2::__hash_table.15"* %this1, i32 0, i32 0
  %call2 = call %"class.std::__2::unique_ptr.16"* @_ZNSt3__210unique_ptrIA_PNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS0_IN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISK_EEEEED2Ev(%"class.std::__2::unique_ptr.16"* %__bucket_list_) #7
  ret %"class.std::__2::__hash_table.15"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEEENS_22__unordered_map_hasherIS7_SE_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SE_NS_8equal_toIS7_EELb1EEENS5_ISE_EEE17__deallocate_nodeEPNS_16__hash_node_baseIPNS_11__hash_nodeISE_PvEEEE(%"class.std::__2::__hash_table.15"* %this, %"struct.std::__2::__hash_node_base.19"* %__np) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__hash_table.15"*, align 4
  %__np.addr = alloca %"struct.std::__2::__hash_node_base.19"*, align 4
  %__na = alloca %"class.std::__2::allocator.29"*, align 4
  %__next = alloca %"struct.std::__2::__hash_node_base.19"*, align 4
  %__real_np = alloca %"struct.std::__2::__hash_node"*, align 4
  store %"class.std::__2::__hash_table.15"* %this, %"class.std::__2::__hash_table.15"** %this.addr, align 4
  store %"struct.std::__2::__hash_node_base.19"* %__np, %"struct.std::__2::__hash_node_base.19"** %__np.addr, align 4
  %this1 = load %"class.std::__2::__hash_table.15"*, %"class.std::__2::__hash_table.15"** %this.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.29"* @_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEEENS_22__unordered_map_hasherIS7_SE_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SE_NS_8equal_toIS7_EELb1EEENS5_ISE_EEE12__node_allocEv(%"class.std::__2::__hash_table.15"* %this1) #7
  store %"class.std::__2::allocator.29"* %call, %"class.std::__2::allocator.29"** %__na, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %0 = load %"struct.std::__2::__hash_node_base.19"*, %"struct.std::__2::__hash_node_base.19"** %__np.addr, align 4
  %cmp = icmp ne %"struct.std::__2::__hash_node_base.19"* %0, null
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %1 = load %"struct.std::__2::__hash_node_base.19"*, %"struct.std::__2::__hash_node_base.19"** %__np.addr, align 4
  %__next_ = getelementptr inbounds %"struct.std::__2::__hash_node_base.19", %"struct.std::__2::__hash_node_base.19"* %1, i32 0, i32 0
  %2 = load %"struct.std::__2::__hash_node_base.19"*, %"struct.std::__2::__hash_node_base.19"** %__next_, align 4
  store %"struct.std::__2::__hash_node_base.19"* %2, %"struct.std::__2::__hash_node_base.19"** %__next, align 4
  %3 = load %"struct.std::__2::__hash_node_base.19"*, %"struct.std::__2::__hash_node_base.19"** %__np.addr, align 4
  %call2 = call %"struct.std::__2::__hash_node"* @_ZNSt3__216__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEE8__upcastEv(%"struct.std::__2::__hash_node_base.19"* %3) #7
  store %"struct.std::__2::__hash_node"* %call2, %"struct.std::__2::__hash_node"** %__real_np, align 4
  %4 = load %"class.std::__2::allocator.29"*, %"class.std::__2::allocator.29"** %__na, align 4
  %5 = load %"struct.std::__2::__hash_node"*, %"struct.std::__2::__hash_node"** %__real_np, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__hash_node", %"struct.std::__2::__hash_node"* %5, i32 0, i32 2
  %call3 = call %"struct.std::__2::pair"* @_ZNSt3__222__hash_key_value_typesINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEEEE9__get_ptrERSE_(%"struct.std::__2::__hash_value_type"* nonnull align 4 dereferenceable(16) %__value_)
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEEE7destroyINS_4pairIKS8_SE_EEEEvRSI_PT_(%"class.std::__2::allocator.29"* nonnull align 1 dereferenceable(1) %4, %"struct.std::__2::pair"* %call3)
  %6 = load %"class.std::__2::allocator.29"*, %"class.std::__2::allocator.29"** %__na, align 4
  %7 = load %"struct.std::__2::__hash_node"*, %"struct.std::__2::__hash_node"** %__real_np, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEEE10deallocateERSI_PSH_m(%"class.std::__2::allocator.29"* nonnull align 1 dereferenceable(1) %6, %"struct.std::__2::__hash_node"* %7, i32 1) #7
  %8 = load %"struct.std::__2::__hash_node_base.19"*, %"struct.std::__2::__hash_node_base.19"** %__next, align 4
  store %"struct.std::__2::__hash_node_base.19"* %8, %"struct.std::__2::__hash_node_base.19"** %__np.addr, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base.19"* @_ZNSt3__217__compressed_pairINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEENS7_ISI_EEE5firstEv(%"class.std::__2::__compressed_pair.26"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.26"*, align 4
  store %"class.std::__2::__compressed_pair.26"* %this, %"class.std::__2::__compressed_pair.26"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.26"*, %"class.std::__2::__compressed_pair.26"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.26"* %this1 to %"struct.std::__2::__compressed_pair_elem.27"*
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base.19"* @_ZNSt3__222__compressed_pair_elemINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.27"* %0) #7
  ret %"struct.std::__2::__hash_node_base.19"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.16"* @_ZNSt3__210unique_ptrIA_PNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS0_IN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISK_EEEEED2Ev(%"class.std::__2::unique_ptr.16"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.16"*, align 4
  store %"class.std::__2::unique_ptr.16"* %this, %"class.std::__2::unique_ptr.16"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.16"*, %"class.std::__2::unique_ptr.16"** %this.addr, align 4
  call void @_ZNSt3__210unique_ptrIA_PNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS0_IN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISK_EEEEE5resetEDn(%"class.std::__2::unique_ptr.16"* %this1, i8* null) #7
  ret %"class.std::__2::unique_ptr.16"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.29"* @_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEEENS_22__unordered_map_hasherIS7_SE_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SE_NS_8equal_toIS7_EELb1EEENS5_ISE_EEE12__node_allocEv(%"class.std::__2::__hash_table.15"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__hash_table.15"*, align 4
  store %"class.std::__2::__hash_table.15"* %this, %"class.std::__2::__hash_table.15"** %this.addr, align 4
  %this1 = load %"class.std::__2::__hash_table.15"*, %"class.std::__2::__hash_table.15"** %this.addr, align 4
  %__p1_ = getelementptr inbounds %"class.std::__2::__hash_table.15", %"class.std::__2::__hash_table.15"* %this1, i32 0, i32 1
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.29"* @_ZNSt3__217__compressed_pairINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEENS7_ISI_EEE6secondEv(%"class.std::__2::__compressed_pair.26"* %__p1_) #7
  ret %"class.std::__2::allocator.29"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__hash_node"* @_ZNSt3__216__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEE8__upcastEv(%"struct.std::__2::__hash_node_base.19"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__hash_node_base.19"*, align 4
  store %"struct.std::__2::__hash_node_base.19"* %this, %"struct.std::__2::__hash_node_base.19"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__hash_node_base.19"*, %"struct.std::__2::__hash_node_base.19"** %this.addr, align 4
  %call = call %"struct.std::__2::__hash_node_base.19"* @_ZNSt3__214pointer_traitsIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEE10pointer_toERSK_(%"struct.std::__2::__hash_node_base.19"* nonnull align 4 dereferenceable(4) %this1) #7
  %0 = bitcast %"struct.std::__2::__hash_node_base.19"* %call to %"struct.std::__2::__hash_node"*
  ret %"struct.std::__2::__hash_node"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEEE7destroyINS_4pairIKS8_SE_EEEEvRSI_PT_(%"class.std::__2::allocator.29"* nonnull align 1 dereferenceable(1) %__a, %"struct.std::__2::pair"* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.29"*, align 4
  %__p.addr = alloca %"struct.std::__2::pair"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant.53", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy.59", align 1
  store %"class.std::__2::allocator.29"* %__a, %"class.std::__2::allocator.29"** %__a.addr, align 4
  store %"struct.std::__2::pair"* %__p, %"struct.std::__2::pair"** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy.59"* %ref.tmp to %"struct.std::__2::integral_constant.53"*
  %1 = load %"class.std::__2::allocator.29"*, %"class.std::__2::allocator.29"** %__a.addr, align 4
  %2 = load %"struct.std::__2::pair"*, %"struct.std::__2::pair"** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEEE9__destroyINS_4pairIKS8_SE_EEEEvNS_17integral_constantIbLb0EEERSI_PT_(%"class.std::__2::allocator.29"* nonnull align 1 dereferenceable(1) %1, %"struct.std::__2::pair"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::pair"* @_ZNSt3__222__hash_key_value_typesINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEEEE9__get_ptrERSE_(%"struct.std::__2::__hash_value_type"* nonnull align 4 dereferenceable(16) %__n) #0 comdat {
entry:
  %__n.addr = alloca %"struct.std::__2::__hash_value_type"*, align 4
  store %"struct.std::__2::__hash_value_type"* %__n, %"struct.std::__2::__hash_value_type"** %__n.addr, align 4
  %0 = load %"struct.std::__2::__hash_value_type"*, %"struct.std::__2::__hash_value_type"** %__n.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %"struct.std::__2::pair"* @_ZNSt3__217__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteIS9_EEEEE11__get_valueEv(%"struct.std::__2::__hash_value_type"* %0)
  %call1 = call %"struct.std::__2::pair"* @_ZNSt3__29addressofINS_4pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEEEPT_RSG_(%"struct.std::__2::pair"* nonnull align 4 dereferenceable(16) %call) #7
  ret %"struct.std::__2::pair"* %call1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEEE10deallocateERSI_PSH_m(%"class.std::__2::allocator.29"* nonnull align 1 dereferenceable(1) %__a, %"struct.std::__2::__hash_node"* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.29"*, align 4
  %__p.addr = alloca %"struct.std::__2::__hash_node"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.29"* %__a, %"class.std::__2::allocator.29"** %__a.addr, align 4
  store %"struct.std::__2::__hash_node"* %__p, %"struct.std::__2::__hash_node"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.29"*, %"class.std::__2::allocator.29"** %__a.addr, align 4
  %1 = load %"struct.std::__2::__hash_node"*, %"struct.std::__2::__hash_node"** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEEEPvEEE10deallocateEPSG_m(%"class.std::__2::allocator.29"* %0, %"struct.std::__2::__hash_node"* %1, i32 %2) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.29"* @_ZNSt3__217__compressed_pairINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEENS7_ISI_EEE6secondEv(%"class.std::__2::__compressed_pair.26"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.26"*, align 4
  store %"class.std::__2::__compressed_pair.26"* %this, %"class.std::__2::__compressed_pair.26"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.26"*, %"class.std::__2::__compressed_pair.26"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.26"* %this1 to %"struct.std::__2::__compressed_pair_elem.28"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.29"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.28"* %0) #7
  ret %"class.std::__2::allocator.29"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.29"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.28"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.28"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.28"* %this, %"struct.std::__2::__compressed_pair_elem.28"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.28"*, %"struct.std::__2::__compressed_pair_elem.28"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.28"* %this1 to %"class.std::__2::allocator.29"*
  ret %"class.std::__2::allocator.29"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__hash_node_base.19"* @_ZNSt3__214pointer_traitsIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEE10pointer_toERSK_(%"struct.std::__2::__hash_node_base.19"* nonnull align 4 dereferenceable(4) %__r) #0 comdat {
entry:
  %__r.addr = alloca %"struct.std::__2::__hash_node_base.19"*, align 4
  store %"struct.std::__2::__hash_node_base.19"* %__r, %"struct.std::__2::__hash_node_base.19"** %__r.addr, align 4
  %0 = load %"struct.std::__2::__hash_node_base.19"*, %"struct.std::__2::__hash_node_base.19"** %__r.addr, align 4
  %call = call %"struct.std::__2::__hash_node_base.19"* @_ZNSt3__29addressofINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEPT_RSL_(%"struct.std::__2::__hash_node_base.19"* nonnull align 4 dereferenceable(4) %0) #7
  ret %"struct.std::__2::__hash_node_base.19"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__hash_node_base.19"* @_ZNSt3__29addressofINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEPT_RSL_(%"struct.std::__2::__hash_node_base.19"* nonnull align 4 dereferenceable(4) %__x) #0 comdat {
entry:
  %__x.addr = alloca %"struct.std::__2::__hash_node_base.19"*, align 4
  store %"struct.std::__2::__hash_node_base.19"* %__x, %"struct.std::__2::__hash_node_base.19"** %__x.addr, align 4
  %0 = load %"struct.std::__2::__hash_node_base.19"*, %"struct.std::__2::__hash_node_base.19"** %__x.addr, align 4
  ret %"struct.std::__2::__hash_node_base.19"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEEE9__destroyINS_4pairIKS8_SE_EEEEvNS_17integral_constantIbLb0EEERSI_PT_(%"class.std::__2::allocator.29"* nonnull align 1 dereferenceable(1) %0, %"struct.std::__2::pair"* %__p) #0 comdat {
entry:
  %1 = alloca %"struct.std::__2::integral_constant.53", align 1
  %.addr = alloca %"class.std::__2::allocator.29"*, align 4
  %__p.addr = alloca %"struct.std::__2::pair"*, align 4
  store %"class.std::__2::allocator.29"* %0, %"class.std::__2::allocator.29"** %.addr, align 4
  store %"struct.std::__2::pair"* %__p, %"struct.std::__2::pair"** %__p.addr, align 4
  %2 = load %"struct.std::__2::pair"*, %"struct.std::__2::pair"** %__p.addr, align 4
  %call = call %"struct.std::__2::pair"* @_ZNSt3__24pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEED2Ev(%"struct.std::__2::pair"* %2) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::pair"* @_ZNSt3__24pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEED2Ev(%"struct.std::__2::pair"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::pair"*, align 4
  store %"struct.std::__2::pair"* %this, %"struct.std::__2::pair"** %this.addr, align 4
  %this1 = load %"struct.std::__2::pair"*, %"struct.std::__2::pair"** %this.addr, align 4
  %second = getelementptr inbounds %"struct.std::__2::pair", %"struct.std::__2::pair"* %this1, i32 0, i32 1
  %call = call %"class.std::__2::unique_ptr.54"* @_ZNSt3__210unique_ptrIN5draco8MetadataENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.54"* %second) #7
  %first = getelementptr inbounds %"struct.std::__2::pair", %"struct.std::__2::pair"* %this1, i32 0, i32 0
  %call2 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_string"* %first) #7
  ret %"struct.std::__2::pair"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.54"* @_ZNSt3__210unique_ptrIN5draco8MetadataENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.54"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.54"*, align 4
  store %"class.std::__2::unique_ptr.54"* %this, %"class.std::__2::unique_ptr.54"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.54"*, %"class.std::__2::unique_ptr.54"** %this.addr, align 4
  call void @_ZNSt3__210unique_ptrIN5draco8MetadataENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.54"* %this1, %"class.draco::Metadata"* null) #7
  ret %"class.std::__2::unique_ptr.54"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__210unique_ptrIN5draco8MetadataENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.54"* %this, %"class.draco::Metadata"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.54"*, align 4
  %__p.addr = alloca %"class.draco::Metadata"*, align 4
  %__tmp = alloca %"class.draco::Metadata"*, align 4
  store %"class.std::__2::unique_ptr.54"* %this, %"class.std::__2::unique_ptr.54"** %this.addr, align 4
  store %"class.draco::Metadata"* %__p, %"class.draco::Metadata"** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.54"*, %"class.std::__2::unique_ptr.54"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.54", %"class.std::__2::unique_ptr.54"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::Metadata"** @_ZNSt3__217__compressed_pairIPN5draco8MetadataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.55"* %__ptr_) #7
  %0 = load %"class.draco::Metadata"*, %"class.draco::Metadata"** %call, align 4
  store %"class.draco::Metadata"* %0, %"class.draco::Metadata"** %__tmp, align 4
  %1 = load %"class.draco::Metadata"*, %"class.draco::Metadata"** %__p.addr, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr.54", %"class.std::__2::unique_ptr.54"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::Metadata"** @_ZNSt3__217__compressed_pairIPN5draco8MetadataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.55"* %__ptr_2) #7
  store %"class.draco::Metadata"* %1, %"class.draco::Metadata"** %call3, align 4
  %2 = load %"class.draco::Metadata"*, %"class.draco::Metadata"** %__tmp, align 4
  %tobool = icmp ne %"class.draco::Metadata"* %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__ptr_4 = getelementptr inbounds %"class.std::__2::unique_ptr.54", %"class.std::__2::unique_ptr.54"* %this1, i32 0, i32 0
  %call5 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.58"* @_ZNSt3__217__compressed_pairIPN5draco8MetadataENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.55"* %__ptr_4) #7
  %3 = load %"class.draco::Metadata"*, %"class.draco::Metadata"** %__tmp, align 4
  call void @_ZNKSt3__214default_deleteIN5draco8MetadataEEclEPS2_(%"struct.std::__2::default_delete.58"* %call5, %"class.draco::Metadata"* %3) #7
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::Metadata"** @_ZNSt3__217__compressed_pairIPN5draco8MetadataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.55"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.55"*, align 4
  store %"class.std::__2::__compressed_pair.55"* %this, %"class.std::__2::__compressed_pair.55"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.55"*, %"class.std::__2::__compressed_pair.55"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.55"* %this1 to %"struct.std::__2::__compressed_pair_elem.56"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::Metadata"** @_ZNSt3__222__compressed_pair_elemIPN5draco8MetadataELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.56"* %0) #7
  ret %"class.draco::Metadata"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.58"* @_ZNSt3__217__compressed_pairIPN5draco8MetadataENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.55"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.55"*, align 4
  store %"class.std::__2::__compressed_pair.55"* %this, %"class.std::__2::__compressed_pair.55"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.55"*, %"class.std::__2::__compressed_pair.55"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.55"* %this1 to %"struct.std::__2::__compressed_pair_elem.57"*
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.58"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco8MetadataEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.57"* %0) #7
  ret %"struct.std::__2::default_delete.58"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__214default_deleteIN5draco8MetadataEEclEPS2_(%"struct.std::__2::default_delete.58"* %this, %"class.draco::Metadata"* %__ptr) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::default_delete.58"*, align 4
  %__ptr.addr = alloca %"class.draco::Metadata"*, align 4
  store %"struct.std::__2::default_delete.58"* %this, %"struct.std::__2::default_delete.58"** %this.addr, align 4
  store %"class.draco::Metadata"* %__ptr, %"class.draco::Metadata"** %__ptr.addr, align 4
  %this1 = load %"struct.std::__2::default_delete.58"*, %"struct.std::__2::default_delete.58"** %this.addr, align 4
  %0 = load %"class.draco::Metadata"*, %"class.draco::Metadata"** %__ptr.addr, align 4
  %isnull = icmp eq %"class.draco::Metadata"* %0, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %entry
  %call = call %"class.draco::Metadata"* @_ZN5draco8MetadataD2Ev(%"class.draco::Metadata"* %0) #7
  %1 = bitcast %"class.draco::Metadata"* %0 to i8*
  call void @_ZdlPv(i8* %1) #10
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::Metadata"** @_ZNSt3__222__compressed_pair_elemIPN5draco8MetadataELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.56"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.56"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.56"* %this, %"struct.std::__2::__compressed_pair_elem.56"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.56"*, %"struct.std::__2::__compressed_pair_elem.56"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.56", %"struct.std::__2::__compressed_pair_elem.56"* %this1, i32 0, i32 0
  ret %"class.draco::Metadata"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.58"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco8MetadataEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.57"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.57"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.57"* %this, %"struct.std::__2::__compressed_pair_elem.57"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.57"*, %"struct.std::__2::__compressed_pair_elem.57"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.57"* %this1 to %"struct.std::__2::default_delete.58"*
  ret %"struct.std::__2::default_delete.58"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::Metadata"* @_ZN5draco8MetadataD2Ev(%"class.draco::Metadata"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::Metadata"*, align 4
  store %"class.draco::Metadata"* %this, %"class.draco::Metadata"** %this.addr, align 4
  %this1 = load %"class.draco::Metadata"*, %"class.draco::Metadata"** %this.addr, align 4
  %sub_metadatas_ = getelementptr inbounds %"class.draco::Metadata", %"class.draco::Metadata"* %this1, i32 0, i32 1
  %call = call %"class.std::__2::unordered_map.14"* @_ZNSt3__213unordered_mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteIS9_EEEENS_4hashIS6_EENS_8equal_toIS6_EENS4_INS_4pairIKS6_SC_EEEEED2Ev(%"class.std::__2::unordered_map.14"* %sub_metadatas_) #7
  %entries_ = getelementptr inbounds %"class.draco::Metadata", %"class.draco::Metadata"* %this1, i32 0, i32 0
  %call2 = call %"class.std::__2::unordered_map"* @_ZNSt3__213unordered_mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueENS_4hashIS6_EENS_8equal_toIS6_EENS4_INS_4pairIKS6_S8_EEEEED2Ev(%"class.std::__2::unordered_map"* %entries_) #7
  ret %"class.draco::Metadata"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::pair"* @_ZNSt3__29addressofINS_4pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEEEPT_RSG_(%"struct.std::__2::pair"* nonnull align 4 dereferenceable(16) %__x) #0 comdat {
entry:
  %__x.addr = alloca %"struct.std::__2::pair"*, align 4
  store %"struct.std::__2::pair"* %__x, %"struct.std::__2::pair"** %__x.addr, align 4
  %0 = load %"struct.std::__2::pair"*, %"struct.std::__2::pair"** %__x.addr, align 4
  ret %"struct.std::__2::pair"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %"struct.std::__2::pair"* @_ZNSt3__217__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteIS9_EEEEE11__get_valueEv(%"struct.std::__2::__hash_value_type"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__hash_value_type"*, align 4
  store %"struct.std::__2::__hash_value_type"* %this, %"struct.std::__2::__hash_value_type"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__hash_value_type"*, %"struct.std::__2::__hash_value_type"** %this.addr, align 4
  %__cc = getelementptr inbounds %"struct.std::__2::__hash_value_type", %"struct.std::__2::__hash_value_type"* %this1, i32 0, i32 0
  ret %"struct.std::__2::pair"* %__cc
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEEEPvEEE10deallocateEPSG_m(%"class.std::__2::allocator.29"* %this, %"struct.std::__2::__hash_node"* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.29"*, align 4
  %__p.addr = alloca %"struct.std::__2::__hash_node"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.29"* %this, %"class.std::__2::allocator.29"** %this.addr, align 4
  store %"struct.std::__2::__hash_node"* %__p, %"struct.std::__2::__hash_node"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.29"*, %"class.std::__2::allocator.29"** %this.addr, align 4
  %0 = load %"struct.std::__2::__hash_node"*, %"struct.std::__2::__hash_node"** %__p.addr, align 4
  %1 = bitcast %"struct.std::__2::__hash_node"* %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 24
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %__ptr, i32 %__size, i32 %__align) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  %1 = load i32, i32* %__size.addr, align 4
  %2 = load i32, i32* %__align.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm(i8* %0, i32 %1, i32 %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm(i8* %__ptr, i32 %__size, i32 %__align) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  %1 = load i32, i32* %__size.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm(i8* %0, i32 %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm(i8* %__ptr, i32 %__size) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller9__do_callEPv(i8* %0)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller9__do_callEPv(i8* %__ptr) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  call void @_ZdlPv(i8* %0) #10
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base.19"* @_ZNSt3__222__compressed_pair_elemINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.27"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.27"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.27"* %this, %"struct.std::__2::__compressed_pair_elem.27"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.27"*, %"struct.std::__2::__compressed_pair_elem.27"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.27", %"struct.std::__2::__compressed_pair_elem.27"* %this1, i32 0, i32 0
  ret %"struct.std::__2::__hash_node_base.19"* %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__210unique_ptrIA_PNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS0_IN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISK_EEEEE5resetEDn(%"class.std::__2::unique_ptr.16"* %this, i8* %0) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.16"*, align 4
  %.addr = alloca i8*, align 4
  %__tmp = alloca %"struct.std::__2::__hash_node_base.19"**, align 4
  store %"class.std::__2::unique_ptr.16"* %this, %"class.std::__2::unique_ptr.16"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.16"*, %"class.std::__2::unique_ptr.16"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.16", %"class.std::__2::unique_ptr.16"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base.19"*** @_ZNSt3__217__compressed_pairIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISL_EEEEE5firstEv(%"class.std::__2::__compressed_pair.17"* %__ptr_) #7
  %1 = load %"struct.std::__2::__hash_node_base.19"**, %"struct.std::__2::__hash_node_base.19"*** %call, align 4
  store %"struct.std::__2::__hash_node_base.19"** %1, %"struct.std::__2::__hash_node_base.19"*** %__tmp, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr.16", %"class.std::__2::unique_ptr.16"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base.19"*** @_ZNSt3__217__compressed_pairIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISL_EEEEE5firstEv(%"class.std::__2::__compressed_pair.17"* %__ptr_2) #7
  store %"struct.std::__2::__hash_node_base.19"** null, %"struct.std::__2::__hash_node_base.19"*** %call3, align 4
  %2 = load %"struct.std::__2::__hash_node_base.19"**, %"struct.std::__2::__hash_node_base.19"*** %__tmp, align 4
  %tobool = icmp ne %"struct.std::__2::__hash_node_base.19"** %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__ptr_4 = getelementptr inbounds %"class.std::__2::unique_ptr.16", %"class.std::__2::unique_ptr.16"* %this1, i32 0, i32 0
  %call5 = call nonnull align 4 dereferenceable(4) %"class.std::__2::__bucket_list_deallocator.21"* @_ZNSt3__217__compressed_pairIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISL_EEEEE6secondEv(%"class.std::__2::__compressed_pair.17"* %__ptr_4) #7
  %3 = load %"struct.std::__2::__hash_node_base.19"**, %"struct.std::__2::__hash_node_base.19"*** %__tmp, align 4
  call void @_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEEclEPSL_(%"class.std::__2::__bucket_list_deallocator.21"* %call5, %"struct.std::__2::__hash_node_base.19"** %3) #7
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base.19"*** @_ZNSt3__217__compressed_pairIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISL_EEEEE5firstEv(%"class.std::__2::__compressed_pair.17"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.17"*, align 4
  store %"class.std::__2::__compressed_pair.17"* %this, %"class.std::__2::__compressed_pair.17"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.17"*, %"class.std::__2::__compressed_pair.17"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.17"* %this1 to %"struct.std::__2::__compressed_pair_elem.18"*
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base.19"*** @_ZNSt3__222__compressed_pair_elemIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.18"* %0) #7
  ret %"struct.std::__2::__hash_node_base.19"*** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__bucket_list_deallocator.21"* @_ZNSt3__217__compressed_pairIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISL_EEEEE6secondEv(%"class.std::__2::__compressed_pair.17"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.17"*, align 4
  store %"class.std::__2::__compressed_pair.17"* %this, %"class.std::__2::__compressed_pair.17"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.17"*, %"class.std::__2::__compressed_pair.17"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.17"* %this1 to i8*
  %add.ptr = getelementptr inbounds i8, i8* %0, i32 4
  %1 = bitcast i8* %add.ptr to %"struct.std::__2::__compressed_pair_elem.20"*
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::__bucket_list_deallocator.21"* @_ZNSt3__222__compressed_pair_elemINS_25__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS2_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISD_EEEEEEPvEEEEEEEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.20"* %1) #7
  ret %"class.std::__2::__bucket_list_deallocator.21"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEEclEPSL_(%"class.std::__2::__bucket_list_deallocator.21"* %this, %"struct.std::__2::__hash_node_base.19"** %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__bucket_list_deallocator.21"*, align 4
  %__p.addr = alloca %"struct.std::__2::__hash_node_base.19"**, align 4
  store %"class.std::__2::__bucket_list_deallocator.21"* %this, %"class.std::__2::__bucket_list_deallocator.21"** %this.addr, align 4
  store %"struct.std::__2::__hash_node_base.19"** %__p, %"struct.std::__2::__hash_node_base.19"*** %__p.addr, align 4
  %this1 = load %"class.std::__2::__bucket_list_deallocator.21"*, %"class.std::__2::__bucket_list_deallocator.21"** %this.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.24"* @_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEE7__allocEv(%"class.std::__2::__bucket_list_deallocator.21"* %this1) #7
  %0 = load %"struct.std::__2::__hash_node_base.19"**, %"struct.std::__2::__hash_node_base.19"*** %__p.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEE4sizeEv(%"class.std::__2::__bucket_list_deallocator.21"* %this1) #7
  %1 = load i32, i32* %call2, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEE10deallocateERSM_PSL_m(%"class.std::__2::allocator.24"* nonnull align 1 dereferenceable(1) %call, %"struct.std::__2::__hash_node_base.19"** %0, i32 %1) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base.19"*** @_ZNSt3__222__compressed_pair_elemIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.18"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.18"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.18"* %this, %"struct.std::__2::__compressed_pair_elem.18"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.18"*, %"struct.std::__2::__compressed_pair_elem.18"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.18", %"struct.std::__2::__compressed_pair_elem.18"* %this1, i32 0, i32 0
  ret %"struct.std::__2::__hash_node_base.19"*** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__bucket_list_deallocator.21"* @_ZNSt3__222__compressed_pair_elemINS_25__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS2_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISD_EEEEEEPvEEEEEEEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.20"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.20"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.20"* %this, %"struct.std::__2::__compressed_pair_elem.20"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.20"*, %"struct.std::__2::__compressed_pair_elem.20"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.20", %"struct.std::__2::__compressed_pair_elem.20"* %this1, i32 0, i32 0
  ret %"class.std::__2::__bucket_list_deallocator.21"* %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEE10deallocateERSM_PSL_m(%"class.std::__2::allocator.24"* nonnull align 1 dereferenceable(1) %__a, %"struct.std::__2::__hash_node_base.19"** %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.24"*, align 4
  %__p.addr = alloca %"struct.std::__2::__hash_node_base.19"**, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.24"* %__a, %"class.std::__2::allocator.24"** %__a.addr, align 4
  store %"struct.std::__2::__hash_node_base.19"** %__p, %"struct.std::__2::__hash_node_base.19"*** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.24"*, %"class.std::__2::allocator.24"** %__a.addr, align 4
  %1 = load %"struct.std::__2::__hash_node_base.19"**, %"struct.std::__2::__hash_node_base.19"*** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEEE10deallocateEPSK_m(%"class.std::__2::allocator.24"* %0, %"struct.std::__2::__hash_node_base.19"** %1, i32 %2) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.24"* @_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEE7__allocEv(%"class.std::__2::__bucket_list_deallocator.21"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__bucket_list_deallocator.21"*, align 4
  store %"class.std::__2::__bucket_list_deallocator.21"* %this, %"class.std::__2::__bucket_list_deallocator.21"** %this.addr, align 4
  %this1 = load %"class.std::__2::__bucket_list_deallocator.21"*, %"class.std::__2::__bucket_list_deallocator.21"** %this.addr, align 4
  %__data_ = getelementptr inbounds %"class.std::__2::__bucket_list_deallocator.21", %"class.std::__2::__bucket_list_deallocator.21"* %this1, i32 0, i32 0
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.24"* @_ZNSt3__217__compressed_pairImNS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEE6secondEv(%"class.std::__2::__compressed_pair.22"* %__data_) #7
  ret %"class.std::__2::allocator.24"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEE4sizeEv(%"class.std::__2::__bucket_list_deallocator.21"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__bucket_list_deallocator.21"*, align 4
  store %"class.std::__2::__bucket_list_deallocator.21"* %this, %"class.std::__2::__bucket_list_deallocator.21"** %this.addr, align 4
  %this1 = load %"class.std::__2::__bucket_list_deallocator.21"*, %"class.std::__2::__bucket_list_deallocator.21"** %this.addr, align 4
  %__data_ = getelementptr inbounds %"class.std::__2::__bucket_list_deallocator.21", %"class.std::__2::__bucket_list_deallocator.21"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__217__compressed_pairImNS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEE5firstEv(%"class.std::__2::__compressed_pair.22"* %__data_) #7
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEEE10deallocateEPSK_m(%"class.std::__2::allocator.24"* %this, %"struct.std::__2::__hash_node_base.19"** %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.24"*, align 4
  %__p.addr = alloca %"struct.std::__2::__hash_node_base.19"**, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.24"* %this, %"class.std::__2::allocator.24"** %this.addr, align 4
  store %"struct.std::__2::__hash_node_base.19"** %__p, %"struct.std::__2::__hash_node_base.19"*** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.24"*, %"class.std::__2::allocator.24"** %this.addr, align 4
  %0 = load %"struct.std::__2::__hash_node_base.19"**, %"struct.std::__2::__hash_node_base.19"*** %__p.addr, align 4
  %1 = bitcast %"struct.std::__2::__hash_node_base.19"** %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.24"* @_ZNSt3__217__compressed_pairImNS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEE6secondEv(%"class.std::__2::__compressed_pair.22"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.22"*, align 4
  store %"class.std::__2::__compressed_pair.22"* %this, %"class.std::__2::__compressed_pair.22"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.22"*, %"class.std::__2::__compressed_pair.22"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.22"* %this1 to %"struct.std::__2::__compressed_pair_elem.23"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.24"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.23"* %0) #7
  ret %"class.std::__2::allocator.24"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.24"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.23"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.23"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.23"* %this, %"struct.std::__2::__compressed_pair_elem.23"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.23"*, %"struct.std::__2::__compressed_pair_elem.23"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.23"* %this1 to %"class.std::__2::allocator.24"*
  ret %"class.std::__2::allocator.24"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__217__compressed_pairImNS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEE5firstEv(%"class.std::__2::__compressed_pair.22"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.22"*, align 4
  store %"class.std::__2::__compressed_pair.22"* %this, %"class.std::__2::__compressed_pair.22"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.22"*, %"class.std::__2::__compressed_pair.22"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.22"* %this1 to %"struct.std::__2::__compressed_pair_elem.2"*
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__222__compressed_pair_elemImLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.2"* %0) #7
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__222__compressed_pair_elemImLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.2"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.2"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.2"* %this, %"struct.std::__2::__compressed_pair_elem.2"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.2"*, %"struct.std::__2::__compressed_pair_elem.2"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.2", %"struct.std::__2::__compressed_pair_elem.2"* %this1, i32 0, i32 0
  ret i32* %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__hash_table"* @_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEENS_22__unordered_map_hasherIS7_SA_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SA_NS_8equal_toIS7_EELb1EEENS5_ISA_EEED2Ev(%"class.std::__2::__hash_table"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__hash_table"*, align 4
  store %"class.std::__2::__hash_table"* %this, %"class.std::__2::__hash_table"** %this.addr, align 4
  %this1 = load %"class.std::__2::__hash_table"*, %"class.std::__2::__hash_table"** %this.addr, align 4
  %__p1_ = getelementptr inbounds %"class.std::__2::__hash_table", %"class.std::__2::__hash_table"* %this1, i32 0, i32 1
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base"* @_ZNSt3__217__compressed_pairINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS7_ISE_EEE5firstEv(%"class.std::__2::__compressed_pair.4"* %__p1_) #7
  %__next_ = getelementptr inbounds %"struct.std::__2::__hash_node_base", %"struct.std::__2::__hash_node_base"* %call, i32 0, i32 0
  %0 = load %"struct.std::__2::__hash_node_base"*, %"struct.std::__2::__hash_node_base"** %__next_, align 4
  call void @_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEENS_22__unordered_map_hasherIS7_SA_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SA_NS_8equal_toIS7_EELb1EEENS5_ISA_EEE17__deallocate_nodeEPNS_16__hash_node_baseIPNS_11__hash_nodeISA_PvEEEE(%"class.std::__2::__hash_table"* %this1, %"struct.std::__2::__hash_node_base"* %0) #7
  %__bucket_list_ = getelementptr inbounds %"class.std::__2::__hash_table", %"class.std::__2::__hash_table"* %this1, i32 0, i32 0
  %call2 = call %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrIA_PNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISH_EEEEED2Ev(%"class.std::__2::unique_ptr"* %__bucket_list_) #7
  ret %"class.std::__2::__hash_table"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEENS_22__unordered_map_hasherIS7_SA_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SA_NS_8equal_toIS7_EELb1EEENS5_ISA_EEE17__deallocate_nodeEPNS_16__hash_node_baseIPNS_11__hash_nodeISA_PvEEEE(%"class.std::__2::__hash_table"* %this, %"struct.std::__2::__hash_node_base"* %__np) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__hash_table"*, align 4
  %__np.addr = alloca %"struct.std::__2::__hash_node_base"*, align 4
  %__na = alloca %"class.std::__2::allocator.7"*, align 4
  %__next = alloca %"struct.std::__2::__hash_node_base"*, align 4
  %__real_np = alloca %"struct.std::__2::__hash_node.60"*, align 4
  store %"class.std::__2::__hash_table"* %this, %"class.std::__2::__hash_table"** %this.addr, align 4
  store %"struct.std::__2::__hash_node_base"* %__np, %"struct.std::__2::__hash_node_base"** %__np.addr, align 4
  %this1 = load %"class.std::__2::__hash_table"*, %"class.std::__2::__hash_table"** %this.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.7"* @_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEENS_22__unordered_map_hasherIS7_SA_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SA_NS_8equal_toIS7_EELb1EEENS5_ISA_EEE12__node_allocEv(%"class.std::__2::__hash_table"* %this1) #7
  store %"class.std::__2::allocator.7"* %call, %"class.std::__2::allocator.7"** %__na, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %0 = load %"struct.std::__2::__hash_node_base"*, %"struct.std::__2::__hash_node_base"** %__np.addr, align 4
  %cmp = icmp ne %"struct.std::__2::__hash_node_base"* %0, null
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %1 = load %"struct.std::__2::__hash_node_base"*, %"struct.std::__2::__hash_node_base"** %__np.addr, align 4
  %__next_ = getelementptr inbounds %"struct.std::__2::__hash_node_base", %"struct.std::__2::__hash_node_base"* %1, i32 0, i32 0
  %2 = load %"struct.std::__2::__hash_node_base"*, %"struct.std::__2::__hash_node_base"** %__next_, align 4
  store %"struct.std::__2::__hash_node_base"* %2, %"struct.std::__2::__hash_node_base"** %__next, align 4
  %3 = load %"struct.std::__2::__hash_node_base"*, %"struct.std::__2::__hash_node_base"** %__np.addr, align 4
  %call2 = call %"struct.std::__2::__hash_node.60"* @_ZNSt3__216__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEE8__upcastEv(%"struct.std::__2::__hash_node_base"* %3) #7
  store %"struct.std::__2::__hash_node.60"* %call2, %"struct.std::__2::__hash_node.60"** %__real_np, align 4
  %4 = load %"class.std::__2::allocator.7"*, %"class.std::__2::allocator.7"** %__na, align 4
  %5 = load %"struct.std::__2::__hash_node.60"*, %"struct.std::__2::__hash_node.60"** %__real_np, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__hash_node.60", %"struct.std::__2::__hash_node.60"* %5, i32 0, i32 2
  %call3 = call %"struct.std::__2::pair.62"* @_ZNSt3__222__hash_key_value_typesINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEE9__get_ptrERSA_(%"struct.std::__2::__hash_value_type.61"* nonnull align 4 dereferenceable(24) %__value_)
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEE7destroyINS_4pairIKS8_SA_EEEEvRSE_PT_(%"class.std::__2::allocator.7"* nonnull align 1 dereferenceable(1) %4, %"struct.std::__2::pair.62"* %call3)
  %6 = load %"class.std::__2::allocator.7"*, %"class.std::__2::allocator.7"** %__na, align 4
  %7 = load %"struct.std::__2::__hash_node.60"*, %"struct.std::__2::__hash_node.60"** %__real_np, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEE10deallocateERSE_PSD_m(%"class.std::__2::allocator.7"* nonnull align 1 dereferenceable(1) %6, %"struct.std::__2::__hash_node.60"* %7, i32 1) #7
  %8 = load %"struct.std::__2::__hash_node_base"*, %"struct.std::__2::__hash_node_base"** %__next, align 4
  store %"struct.std::__2::__hash_node_base"* %8, %"struct.std::__2::__hash_node_base"** %__np.addr, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base"* @_ZNSt3__217__compressed_pairINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS7_ISE_EEE5firstEv(%"class.std::__2::__compressed_pair.4"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.4"*, align 4
  store %"class.std::__2::__compressed_pair.4"* %this, %"class.std::__2::__compressed_pair.4"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.4"*, %"class.std::__2::__compressed_pair.4"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.4"* %this1 to %"struct.std::__2::__compressed_pair_elem.5"*
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base"* @_ZNSt3__222__compressed_pair_elemINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.5"* %0) #7
  ret %"struct.std::__2::__hash_node_base"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrIA_PNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISH_EEEEED2Ev(%"class.std::__2::unique_ptr"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  call void @_ZNSt3__210unique_ptrIA_PNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISH_EEEEE5resetEDn(%"class.std::__2::unique_ptr"* %this1, i8* null) #7
  ret %"class.std::__2::unique_ptr"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.7"* @_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEENS_22__unordered_map_hasherIS7_SA_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SA_NS_8equal_toIS7_EELb1EEENS5_ISA_EEE12__node_allocEv(%"class.std::__2::__hash_table"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__hash_table"*, align 4
  store %"class.std::__2::__hash_table"* %this, %"class.std::__2::__hash_table"** %this.addr, align 4
  %this1 = load %"class.std::__2::__hash_table"*, %"class.std::__2::__hash_table"** %this.addr, align 4
  %__p1_ = getelementptr inbounds %"class.std::__2::__hash_table", %"class.std::__2::__hash_table"* %this1, i32 0, i32 1
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.7"* @_ZNSt3__217__compressed_pairINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS7_ISE_EEE6secondEv(%"class.std::__2::__compressed_pair.4"* %__p1_) #7
  ret %"class.std::__2::allocator.7"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__hash_node.60"* @_ZNSt3__216__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEE8__upcastEv(%"struct.std::__2::__hash_node_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__hash_node_base"*, align 4
  store %"struct.std::__2::__hash_node_base"* %this, %"struct.std::__2::__hash_node_base"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__hash_node_base"*, %"struct.std::__2::__hash_node_base"** %this.addr, align 4
  %call = call %"struct.std::__2::__hash_node_base"* @_ZNSt3__214pointer_traitsIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEEE10pointer_toERSG_(%"struct.std::__2::__hash_node_base"* nonnull align 4 dereferenceable(4) %this1) #7
  %0 = bitcast %"struct.std::__2::__hash_node_base"* %call to %"struct.std::__2::__hash_node.60"*
  ret %"struct.std::__2::__hash_node.60"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEE7destroyINS_4pairIKS8_SA_EEEEvRSE_PT_(%"class.std::__2::allocator.7"* nonnull align 1 dereferenceable(1) %__a, %"struct.std::__2::pair.62"* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.7"*, align 4
  %__p.addr = alloca %"struct.std::__2::pair.62"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant.53", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy.70", align 1
  store %"class.std::__2::allocator.7"* %__a, %"class.std::__2::allocator.7"** %__a.addr, align 4
  store %"struct.std::__2::pair.62"* %__p, %"struct.std::__2::pair.62"** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy.70"* %ref.tmp to %"struct.std::__2::integral_constant.53"*
  %1 = load %"class.std::__2::allocator.7"*, %"class.std::__2::allocator.7"** %__a.addr, align 4
  %2 = load %"struct.std::__2::pair.62"*, %"struct.std::__2::pair.62"** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEE9__destroyINS_4pairIKS8_SA_EEEEvNS_17integral_constantIbLb0EEERSE_PT_(%"class.std::__2::allocator.7"* nonnull align 1 dereferenceable(1) %1, %"struct.std::__2::pair.62"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::pair.62"* @_ZNSt3__222__hash_key_value_typesINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEE9__get_ptrERSA_(%"struct.std::__2::__hash_value_type.61"* nonnull align 4 dereferenceable(24) %__n) #0 comdat {
entry:
  %__n.addr = alloca %"struct.std::__2::__hash_value_type.61"*, align 4
  store %"struct.std::__2::__hash_value_type.61"* %__n, %"struct.std::__2::__hash_value_type.61"** %__n.addr, align 4
  %0 = load %"struct.std::__2::__hash_value_type.61"*, %"struct.std::__2::__hash_value_type.61"** %__n.addr, align 4
  %call = call nonnull align 4 dereferenceable(24) %"struct.std::__2::pair.62"* @_ZNSt3__217__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEE11__get_valueEv(%"struct.std::__2::__hash_value_type.61"* %0)
  %call1 = call %"struct.std::__2::pair.62"* @_ZNSt3__29addressofINS_4pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEEEPT_RSC_(%"struct.std::__2::pair.62"* nonnull align 4 dereferenceable(24) %call) #7
  ret %"struct.std::__2::pair.62"* %call1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEE10deallocateERSE_PSD_m(%"class.std::__2::allocator.7"* nonnull align 1 dereferenceable(1) %__a, %"struct.std::__2::__hash_node.60"* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.7"*, align 4
  %__p.addr = alloca %"struct.std::__2::__hash_node.60"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.7"* %__a, %"class.std::__2::allocator.7"** %__a.addr, align 4
  store %"struct.std::__2::__hash_node.60"* %__p, %"struct.std::__2::__hash_node.60"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.7"*, %"class.std::__2::allocator.7"** %__a.addr, align 4
  %1 = load %"struct.std::__2::__hash_node.60"*, %"struct.std::__2::__hash_node.60"** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEEN5draco10EntryValueEEEPvEEE10deallocateEPSC_m(%"class.std::__2::allocator.7"* %0, %"struct.std::__2::__hash_node.60"* %1, i32 %2) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.7"* @_ZNSt3__217__compressed_pairINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS7_ISE_EEE6secondEv(%"class.std::__2::__compressed_pair.4"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.4"*, align 4
  store %"class.std::__2::__compressed_pair.4"* %this, %"class.std::__2::__compressed_pair.4"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.4"*, %"class.std::__2::__compressed_pair.4"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.4"* %this1 to %"struct.std::__2::__compressed_pair_elem.6"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.7"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.6"* %0) #7
  ret %"class.std::__2::allocator.7"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.7"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.6"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.6"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.6"* %this, %"struct.std::__2::__compressed_pair_elem.6"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.6"*, %"struct.std::__2::__compressed_pair_elem.6"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.6"* %this1 to %"class.std::__2::allocator.7"*
  ret %"class.std::__2::allocator.7"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__hash_node_base"* @_ZNSt3__214pointer_traitsIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEEE10pointer_toERSG_(%"struct.std::__2::__hash_node_base"* nonnull align 4 dereferenceable(4) %__r) #0 comdat {
entry:
  %__r.addr = alloca %"struct.std::__2::__hash_node_base"*, align 4
  store %"struct.std::__2::__hash_node_base"* %__r, %"struct.std::__2::__hash_node_base"** %__r.addr, align 4
  %0 = load %"struct.std::__2::__hash_node_base"*, %"struct.std::__2::__hash_node_base"** %__r.addr, align 4
  %call = call %"struct.std::__2::__hash_node_base"* @_ZNSt3__29addressofINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEEEEPT_RSH_(%"struct.std::__2::__hash_node_base"* nonnull align 4 dereferenceable(4) %0) #7
  ret %"struct.std::__2::__hash_node_base"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__hash_node_base"* @_ZNSt3__29addressofINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEEEEPT_RSH_(%"struct.std::__2::__hash_node_base"* nonnull align 4 dereferenceable(4) %__x) #0 comdat {
entry:
  %__x.addr = alloca %"struct.std::__2::__hash_node_base"*, align 4
  store %"struct.std::__2::__hash_node_base"* %__x, %"struct.std::__2::__hash_node_base"** %__x.addr, align 4
  %0 = load %"struct.std::__2::__hash_node_base"*, %"struct.std::__2::__hash_node_base"** %__x.addr, align 4
  ret %"struct.std::__2::__hash_node_base"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEE9__destroyINS_4pairIKS8_SA_EEEEvNS_17integral_constantIbLb0EEERSE_PT_(%"class.std::__2::allocator.7"* nonnull align 1 dereferenceable(1) %0, %"struct.std::__2::pair.62"* %__p) #0 comdat {
entry:
  %1 = alloca %"struct.std::__2::integral_constant.53", align 1
  %.addr = alloca %"class.std::__2::allocator.7"*, align 4
  %__p.addr = alloca %"struct.std::__2::pair.62"*, align 4
  store %"class.std::__2::allocator.7"* %0, %"class.std::__2::allocator.7"** %.addr, align 4
  store %"struct.std::__2::pair.62"* %__p, %"struct.std::__2::pair.62"** %__p.addr, align 4
  %2 = load %"struct.std::__2::pair.62"*, %"struct.std::__2::pair.62"** %__p.addr, align 4
  %call = call %"struct.std::__2::pair.62"* @_ZNSt3__24pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEED2Ev(%"struct.std::__2::pair.62"* %2) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::pair.62"* @_ZNSt3__24pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEED2Ev(%"struct.std::__2::pair.62"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::pair.62"*, align 4
  store %"struct.std::__2::pair.62"* %this, %"struct.std::__2::pair.62"** %this.addr, align 4
  %this1 = load %"struct.std::__2::pair.62"*, %"struct.std::__2::pair.62"** %this.addr, align 4
  %second = getelementptr inbounds %"struct.std::__2::pair.62", %"struct.std::__2::pair.62"* %this1, i32 0, i32 1
  %call = call %"class.draco::EntryValue"* @_ZN5draco10EntryValueD2Ev(%"class.draco::EntryValue"* %second) #7
  %first = getelementptr inbounds %"struct.std::__2::pair.62", %"struct.std::__2::pair.62"* %this1, i32 0, i32 0
  %call2 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_string"* %first) #7
  ret %"struct.std::__2::pair.62"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::EntryValue"* @_ZN5draco10EntryValueD2Ev(%"class.draco::EntryValue"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::EntryValue"*, align 4
  store %"class.draco::EntryValue"* %this, %"class.draco::EntryValue"** %this.addr, align 4
  %this1 = load %"class.draco::EntryValue"*, %"class.draco::EntryValue"** %this.addr, align 4
  %data_ = getelementptr inbounds %"class.draco::EntryValue", %"class.draco::EntryValue"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::vector.63"* @_ZNSt3__26vectorIhNS_9allocatorIhEEED2Ev(%"class.std::__2::vector.63"* %data_) #7
  ret %"class.draco::EntryValue"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector.63"* @_ZNSt3__26vectorIhNS_9allocatorIhEEED2Ev(%"class.std::__2::vector.63"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.63"*, align 4
  store %"class.std::__2::vector.63"* %this, %"class.std::__2::vector.63"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.63"*, %"class.std::__2::vector.63"** %this.addr, align 4
  call void @_ZNKSt3__26vectorIhNS_9allocatorIhEEE17__annotate_deleteEv(%"class.std::__2::vector.63"* %this1) #7
  %0 = bitcast %"class.std::__2::vector.63"* %this1 to %"class.std::__2::__vector_base.64"*
  %call = call %"class.std::__2::__vector_base.64"* @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEED2Ev(%"class.std::__2::__vector_base.64"* %0) #7
  ret %"class.std::__2::vector.63"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIhNS_9allocatorIhEEE17__annotate_deleteEv(%"class.std::__2::vector.63"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.63"*, align 4
  store %"class.std::__2::vector.63"* %this, %"class.std::__2::vector.63"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.63"*, %"class.std::__2::vector.63"** %this.addr, align 4
  %call = call i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector.63"* %this1) #7
  %call2 = call i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector.63"* %this1) #7
  %call3 = call i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::vector.63"* %this1) #7
  %add.ptr = getelementptr inbounds i8, i8* %call2, i32 %call3
  %call4 = call i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector.63"* %this1) #7
  %call5 = call i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4sizeEv(%"class.std::__2::vector.63"* %this1) #7
  %add.ptr6 = getelementptr inbounds i8, i8* %call4, i32 %call5
  %call7 = call i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector.63"* %this1) #7
  %call8 = call i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::vector.63"* %this1) #7
  %add.ptr9 = getelementptr inbounds i8, i8* %call7, i32 %call8
  call void @_ZNKSt3__26vectorIhNS_9allocatorIhEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector.63"* %this1, i8* %call, i8* %add.ptr, i8* %add.ptr6, i8* %add.ptr9) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base.64"* @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEED2Ev(%"class.std::__2::__vector_base.64"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__vector_base.64"*, align 4
  %this.addr = alloca %"class.std::__2::__vector_base.64"*, align 4
  store %"class.std::__2::__vector_base.64"* %this, %"class.std::__2::__vector_base.64"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.64"*, %"class.std::__2::__vector_base.64"** %this.addr, align 4
  store %"class.std::__2::__vector_base.64"* %this1, %"class.std::__2::__vector_base.64"** %retval, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.64", %"class.std::__2::__vector_base.64"* %this1, i32 0, i32 0
  %0 = load i8*, i8** %__begin_, align 4
  %cmp = icmp ne i8* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE5clearEv(%"class.std::__2::__vector_base.64"* %this1) #7
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.68"* @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE7__allocEv(%"class.std::__2::__vector_base.64"* %this1) #7
  %__begin_2 = getelementptr inbounds %"class.std::__2::__vector_base.64", %"class.std::__2::__vector_base.64"* %this1, i32 0, i32 0
  %1 = load i8*, i8** %__begin_2, align 4
  %call3 = call i32 @_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::__vector_base.64"* %this1) #7
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE10deallocateERS2_Phm(%"class.std::__2::allocator.68"* nonnull align 1 dereferenceable(1) %call, i8* %1, i32 %call3) #7
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"class.std::__2::__vector_base.64"*, %"class.std::__2::__vector_base.64"** %retval, align 4
  ret %"class.std::__2::__vector_base.64"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIhNS_9allocatorIhEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector.63"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.63"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector.63"* %this, %"class.std::__2::vector.63"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector.63"*, %"class.std::__2::vector.63"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector.63"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.63"*, align 4
  store %"class.std::__2::vector.63"* %this, %"class.std::__2::vector.63"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.63"*, %"class.std::__2::vector.63"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.63"* %this1 to %"class.std::__2::__vector_base.64"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.64", %"class.std::__2::__vector_base.64"* %0, i32 0, i32 0
  %1 = load i8*, i8** %__begin_, align 4
  %call = call i8* @_ZNSt3__212__to_addressIhEEPT_S2_(i8* %1) #7
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::vector.63"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.63"*, align 4
  store %"class.std::__2::vector.63"* %this, %"class.std::__2::vector.63"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.63"*, %"class.std::__2::vector.63"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.63"* %this1 to %"class.std::__2::__vector_base.64"*
  %call = call i32 @_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::__vector_base.64"* %0) #7
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4sizeEv(%"class.std::__2::vector.63"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.63"*, align 4
  store %"class.std::__2::vector.63"* %this, %"class.std::__2::vector.63"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.63"*, %"class.std::__2::vector.63"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.63"* %this1 to %"class.std::__2::__vector_base.64"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.64", %"class.std::__2::__vector_base.64"* %0, i32 0, i32 1
  %1 = load i8*, i8** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector.63"* %this1 to %"class.std::__2::__vector_base.64"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.64", %"class.std::__2::__vector_base.64"* %2, i32 0, i32 0
  %3 = load i8*, i8** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint i8* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  ret i32 %sub.ptr.sub
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__212__to_addressIhEEPT_S2_(i8* %__p) #0 comdat {
entry:
  %__p.addr = alloca i8*, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %0 = load i8*, i8** %__p.addr, align 4
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::__vector_base.64"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.64"*, align 4
  store %"class.std::__2::__vector_base.64"* %this, %"class.std::__2::__vector_base.64"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.64"*, %"class.std::__2::__vector_base.64"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE9__end_capEv(%"class.std::__2::__vector_base.64"* %this1) #7
  %0 = load i8*, i8** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.64", %"class.std::__2::__vector_base.64"* %this1, i32 0, i32 0
  %1 = load i8*, i8** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint i8* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  ret i32 %sub.ptr.sub
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE9__end_capEv(%"class.std::__2::__vector_base.64"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.64"*, align 4
  store %"class.std::__2::__vector_base.64"* %this, %"class.std::__2::__vector_base.64"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.64"*, %"class.std::__2::__vector_base.64"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.64", %"class.std::__2::__vector_base.64"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__217__compressed_pairIPhNS_9allocatorIhEEE5firstEv(%"class.std::__2::__compressed_pair.65"* %__end_cap_) #7
  ret i8** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__217__compressed_pairIPhNS_9allocatorIhEEE5firstEv(%"class.std::__2::__compressed_pair.65"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.65"*, align 4
  store %"class.std::__2::__compressed_pair.65"* %this, %"class.std::__2::__compressed_pair.65"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.65"*, %"class.std::__2::__compressed_pair.65"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.65"* %this1 to %"struct.std::__2::__compressed_pair_elem.66"*
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__222__compressed_pair_elemIPhLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.66"* %0) #7
  ret i8** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__222__compressed_pair_elemIPhLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.66"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.66"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.66"* %this, %"struct.std::__2::__compressed_pair_elem.66"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.66"*, %"struct.std::__2::__compressed_pair_elem.66"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.66", %"struct.std::__2::__compressed_pair_elem.66"* %this1, i32 0, i32 0
  ret i8** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE5clearEv(%"class.std::__2::__vector_base.64"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.64"*, align 4
  store %"class.std::__2::__vector_base.64"* %this, %"class.std::__2::__vector_base.64"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.64"*, %"class.std::__2::__vector_base.64"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.64", %"class.std::__2::__vector_base.64"* %this1, i32 0, i32 0
  %0 = load i8*, i8** %__begin_, align 4
  call void @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE17__destruct_at_endEPh(%"class.std::__2::__vector_base.64"* %this1, i8* %0) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE10deallocateERS2_Phm(%"class.std::__2::allocator.68"* nonnull align 1 dereferenceable(1) %__a, i8* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.68"*, align 4
  %__p.addr = alloca i8*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.68"* %__a, %"class.std::__2::allocator.68"** %__a.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.68"*, %"class.std::__2::allocator.68"** %__a.addr, align 4
  %1 = load i8*, i8** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorIhE10deallocateEPhm(%"class.std::__2::allocator.68"* %0, i8* %1, i32 %2) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.68"* @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE7__allocEv(%"class.std::__2::__vector_base.64"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.64"*, align 4
  store %"class.std::__2::__vector_base.64"* %this, %"class.std::__2::__vector_base.64"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.64"*, %"class.std::__2::__vector_base.64"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.64", %"class.std::__2::__vector_base.64"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.68"* @_ZNSt3__217__compressed_pairIPhNS_9allocatorIhEEE6secondEv(%"class.std::__2::__compressed_pair.65"* %__end_cap_) #7
  ret %"class.std::__2::allocator.68"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE17__destruct_at_endEPh(%"class.std::__2::__vector_base.64"* %this, i8* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.64"*, align 4
  %__new_last.addr = alloca i8*, align 4
  %__soon_to_be_end = alloca i8*, align 4
  store %"class.std::__2::__vector_base.64"* %this, %"class.std::__2::__vector_base.64"** %this.addr, align 4
  store i8* %__new_last, i8** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.64"*, %"class.std::__2::__vector_base.64"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.64", %"class.std::__2::__vector_base.64"* %this1, i32 0, i32 1
  %0 = load i8*, i8** %__end_, align 4
  store i8* %0, i8** %__soon_to_be_end, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load i8*, i8** %__new_last.addr, align 4
  %2 = load i8*, i8** %__soon_to_be_end, align 4
  %cmp = icmp ne i8* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.68"* @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE7__allocEv(%"class.std::__2::__vector_base.64"* %this1) #7
  %3 = load i8*, i8** %__soon_to_be_end, align 4
  %incdec.ptr = getelementptr inbounds i8, i8* %3, i32 -1
  store i8* %incdec.ptr, i8** %__soon_to_be_end, align 4
  %call2 = call i8* @_ZNSt3__212__to_addressIhEEPT_S2_(i8* %incdec.ptr) #7
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE7destroyIhEEvRS2_PT_(%"class.std::__2::allocator.68"* nonnull align 1 dereferenceable(1) %call, i8* %call2)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %4 = load i8*, i8** %__new_last.addr, align 4
  %__end_3 = getelementptr inbounds %"class.std::__2::__vector_base.64", %"class.std::__2::__vector_base.64"* %this1, i32 0, i32 1
  store i8* %4, i8** %__end_3, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE7destroyIhEEvRS2_PT_(%"class.std::__2::allocator.68"* nonnull align 1 dereferenceable(1) %__a, i8* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.68"*, align 4
  %__p.addr = alloca i8*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy.71", align 1
  store %"class.std::__2::allocator.68"* %__a, %"class.std::__2::allocator.68"** %__a.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy.71"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.68"*, %"class.std::__2::allocator.68"** %__a.addr, align 4
  %2 = load i8*, i8** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE9__destroyIhEEvNS_17integral_constantIbLb1EEERS2_PT_(%"class.std::__2::allocator.68"* nonnull align 1 dereferenceable(1) %1, i8* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE9__destroyIhEEvNS_17integral_constantIbLb1EEERS2_PT_(%"class.std::__2::allocator.68"* nonnull align 1 dereferenceable(1) %__a, i8* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.68"*, align 4
  %__p.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.68"* %__a, %"class.std::__2::allocator.68"** %__a.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.68"*, %"class.std::__2::allocator.68"** %__a.addr, align 4
  %2 = load i8*, i8** %__p.addr, align 4
  call void @_ZNSt3__29allocatorIhE7destroyEPh(%"class.std::__2::allocator.68"* %1, i8* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIhE7destroyEPh(%"class.std::__2::allocator.68"* %this, i8* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.68"*, align 4
  %__p.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.68"* %this, %"class.std::__2::allocator.68"** %this.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.68"*, %"class.std::__2::allocator.68"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIhE10deallocateEPhm(%"class.std::__2::allocator.68"* %this, i8* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.68"*, align 4
  %__p.addr = alloca i8*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.68"* %this, %"class.std::__2::allocator.68"** %this.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.68"*, %"class.std::__2::allocator.68"** %this.addr, align 4
  %0 = load i8*, i8** %__p.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %1, 1
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %0, i32 %mul, i32 1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.68"* @_ZNSt3__217__compressed_pairIPhNS_9allocatorIhEEE6secondEv(%"class.std::__2::__compressed_pair.65"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.65"*, align 4
  store %"class.std::__2::__compressed_pair.65"* %this, %"class.std::__2::__compressed_pair.65"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.65"*, %"class.std::__2::__compressed_pair.65"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.65"* %this1 to %"struct.std::__2::__compressed_pair_elem.67"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.68"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIhEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.67"* %0) #7
  ret %"class.std::__2::allocator.68"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.68"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIhEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.67"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.67"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.67"* %this, %"struct.std::__2::__compressed_pair_elem.67"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.67"*, %"struct.std::__2::__compressed_pair_elem.67"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.67"* %this1 to %"class.std::__2::allocator.68"*
  ret %"class.std::__2::allocator.68"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::pair.62"* @_ZNSt3__29addressofINS_4pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEEEPT_RSC_(%"struct.std::__2::pair.62"* nonnull align 4 dereferenceable(24) %__x) #0 comdat {
entry:
  %__x.addr = alloca %"struct.std::__2::pair.62"*, align 4
  store %"struct.std::__2::pair.62"* %__x, %"struct.std::__2::pair.62"** %__x.addr, align 4
  %0 = load %"struct.std::__2::pair.62"*, %"struct.std::__2::pair.62"** %__x.addr, align 4
  ret %"struct.std::__2::pair.62"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(24) %"struct.std::__2::pair.62"* @_ZNSt3__217__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEE11__get_valueEv(%"struct.std::__2::__hash_value_type.61"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__hash_value_type.61"*, align 4
  store %"struct.std::__2::__hash_value_type.61"* %this, %"struct.std::__2::__hash_value_type.61"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__hash_value_type.61"*, %"struct.std::__2::__hash_value_type.61"** %this.addr, align 4
  %__cc = getelementptr inbounds %"struct.std::__2::__hash_value_type.61", %"struct.std::__2::__hash_value_type.61"* %this1, i32 0, i32 0
  ret %"struct.std::__2::pair.62"* %__cc
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEEN5draco10EntryValueEEEPvEEE10deallocateEPSC_m(%"class.std::__2::allocator.7"* %this, %"struct.std::__2::__hash_node.60"* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.7"*, align 4
  %__p.addr = alloca %"struct.std::__2::__hash_node.60"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.7"* %this, %"class.std::__2::allocator.7"** %this.addr, align 4
  store %"struct.std::__2::__hash_node.60"* %__p, %"struct.std::__2::__hash_node.60"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.7"*, %"class.std::__2::allocator.7"** %this.addr, align 4
  %0 = load %"struct.std::__2::__hash_node.60"*, %"struct.std::__2::__hash_node.60"** %__p.addr, align 4
  %1 = bitcast %"struct.std::__2::__hash_node.60"* %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 32
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base"* @_ZNSt3__222__compressed_pair_elemINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.5"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.5"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.5"* %this, %"struct.std::__2::__compressed_pair_elem.5"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.5"*, %"struct.std::__2::__compressed_pair_elem.5"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.5", %"struct.std::__2::__compressed_pair_elem.5"* %this1, i32 0, i32 0
  ret %"struct.std::__2::__hash_node_base"* %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__210unique_ptrIA_PNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISH_EEEEE5resetEDn(%"class.std::__2::unique_ptr"* %this, i8* %0) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  %.addr = alloca i8*, align 4
  %__tmp = alloca %"struct.std::__2::__hash_node_base"**, align 4
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base"*** @_ZNSt3__217__compressed_pairIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISH_EEEEE5firstEv(%"class.std::__2::__compressed_pair"* %__ptr_) #7
  %1 = load %"struct.std::__2::__hash_node_base"**, %"struct.std::__2::__hash_node_base"*** %call, align 4
  store %"struct.std::__2::__hash_node_base"** %1, %"struct.std::__2::__hash_node_base"*** %__tmp, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base"*** @_ZNSt3__217__compressed_pairIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISH_EEEEE5firstEv(%"class.std::__2::__compressed_pair"* %__ptr_2) #7
  store %"struct.std::__2::__hash_node_base"** null, %"struct.std::__2::__hash_node_base"*** %call3, align 4
  %2 = load %"struct.std::__2::__hash_node_base"**, %"struct.std::__2::__hash_node_base"*** %__tmp, align 4
  %tobool = icmp ne %"struct.std::__2::__hash_node_base"** %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__ptr_4 = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call5 = call nonnull align 4 dereferenceable(4) %"class.std::__2::__bucket_list_deallocator"* @_ZNSt3__217__compressed_pairIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISH_EEEEE6secondEv(%"class.std::__2::__compressed_pair"* %__ptr_4) #7
  %3 = load %"struct.std::__2::__hash_node_base"**, %"struct.std::__2::__hash_node_base"*** %__tmp, align 4
  call void @_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEEclEPSH_(%"class.std::__2::__bucket_list_deallocator"* %call5, %"struct.std::__2::__hash_node_base"** %3) #7
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base"*** @_ZNSt3__217__compressed_pairIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISH_EEEEE5firstEv(%"class.std::__2::__compressed_pair"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base"*** @_ZNSt3__222__compressed_pair_elemIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %0) #7
  ret %"struct.std::__2::__hash_node_base"*** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__bucket_list_deallocator"* @_ZNSt3__217__compressed_pairIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISH_EEEEE6secondEv(%"class.std::__2::__compressed_pair"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to i8*
  %add.ptr = getelementptr inbounds i8, i8* %0, i32 4
  %1 = bitcast i8* %add.ptr to %"struct.std::__2::__compressed_pair_elem.0"*
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::__bucket_list_deallocator"* @_ZNSt3__222__compressed_pair_elemINS_25__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS2_IcEEEEN5draco10EntryValueEEEPvEEEEEEEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.0"* %1) #7
  ret %"class.std::__2::__bucket_list_deallocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEEclEPSH_(%"class.std::__2::__bucket_list_deallocator"* %this, %"struct.std::__2::__hash_node_base"** %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__bucket_list_deallocator"*, align 4
  %__p.addr = alloca %"struct.std::__2::__hash_node_base"**, align 4
  store %"class.std::__2::__bucket_list_deallocator"* %this, %"class.std::__2::__bucket_list_deallocator"** %this.addr, align 4
  store %"struct.std::__2::__hash_node_base"** %__p, %"struct.std::__2::__hash_node_base"*** %__p.addr, align 4
  %this1 = load %"class.std::__2::__bucket_list_deallocator"*, %"class.std::__2::__bucket_list_deallocator"** %this.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEE7__allocEv(%"class.std::__2::__bucket_list_deallocator"* %this1) #7
  %0 = load %"struct.std::__2::__hash_node_base"**, %"struct.std::__2::__hash_node_base"*** %__p.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEE4sizeEv(%"class.std::__2::__bucket_list_deallocator"* %this1) #7
  %1 = load i32, i32* %call2, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEE10deallocateERSI_PSH_m(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call, %"struct.std::__2::__hash_node_base"** %0, i32 %1) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base"*** @_ZNSt3__222__compressed_pair_elemIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem"*, align 4
  store %"struct.std::__2::__compressed_pair_elem"* %this, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem"*, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem"* %this1, i32 0, i32 0
  ret %"struct.std::__2::__hash_node_base"*** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__bucket_list_deallocator"* @_ZNSt3__222__compressed_pair_elemINS_25__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS2_IcEEEEN5draco10EntryValueEEEPvEEEEEEEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.0"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.0"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.0"* %this, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.0"*, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.0", %"struct.std::__2::__compressed_pair_elem.0"* %this1, i32 0, i32 0
  ret %"class.std::__2::__bucket_list_deallocator"* %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEE10deallocateERSI_PSH_m(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, %"struct.std::__2::__hash_node_base"** %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca %"struct.std::__2::__hash_node_base"**, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store %"struct.std::__2::__hash_node_base"** %__p, %"struct.std::__2::__hash_node_base"*** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %1 = load %"struct.std::__2::__hash_node_base"**, %"struct.std::__2::__hash_node_base"*** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEEN5draco10EntryValueEEEPvEEEEE10deallocateEPSG_m(%"class.std::__2::allocator"* %0, %"struct.std::__2::__hash_node_base"** %1, i32 %2) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEE7__allocEv(%"class.std::__2::__bucket_list_deallocator"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__bucket_list_deallocator"*, align 4
  store %"class.std::__2::__bucket_list_deallocator"* %this, %"class.std::__2::__bucket_list_deallocator"** %this.addr, align 4
  %this1 = load %"class.std::__2::__bucket_list_deallocator"*, %"class.std::__2::__bucket_list_deallocator"** %this.addr, align 4
  %__data_ = getelementptr inbounds %"class.std::__2::__bucket_list_deallocator", %"class.std::__2::__bucket_list_deallocator"* %this1, i32 0, i32 0
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__217__compressed_pairImNS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEE6secondEv(%"class.std::__2::__compressed_pair.1"* %__data_) #7
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEE4sizeEv(%"class.std::__2::__bucket_list_deallocator"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__bucket_list_deallocator"*, align 4
  store %"class.std::__2::__bucket_list_deallocator"* %this, %"class.std::__2::__bucket_list_deallocator"** %this.addr, align 4
  %this1 = load %"class.std::__2::__bucket_list_deallocator"*, %"class.std::__2::__bucket_list_deallocator"** %this.addr, align 4
  %__data_ = getelementptr inbounds %"class.std::__2::__bucket_list_deallocator", %"class.std::__2::__bucket_list_deallocator"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__217__compressed_pairImNS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEE5firstEv(%"class.std::__2::__compressed_pair.1"* %__data_) #7
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEEN5draco10EntryValueEEEPvEEEEE10deallocateEPSG_m(%"class.std::__2::allocator"* %this, %"struct.std::__2::__hash_node_base"** %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca %"struct.std::__2::__hash_node_base"**, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  store %"struct.std::__2::__hash_node_base"** %__p, %"struct.std::__2::__hash_node_base"*** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  %0 = load %"struct.std::__2::__hash_node_base"**, %"struct.std::__2::__hash_node_base"*** %__p.addr, align 4
  %1 = bitcast %"struct.std::__2::__hash_node_base"** %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__217__compressed_pairImNS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEE6secondEv(%"class.std::__2::__compressed_pair.1"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.1"*, align 4
  store %"class.std::__2::__compressed_pair.1"* %this, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.1"*, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.1"* %this1 to %"struct.std::__2::__compressed_pair_elem.3"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.3"* %0) #7
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.3"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.3"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.3"* %this, %"struct.std::__2::__compressed_pair_elem.3"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.3"*, %"struct.std::__2::__compressed_pair_elem.3"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.3"* %this1 to %"class.std::__2::allocator"*
  ret %"class.std::__2::allocator"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__217__compressed_pairImNS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEE5firstEv(%"class.std::__2::__compressed_pair.1"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.1"*, align 4
  store %"class.std::__2::__compressed_pair.1"* %this, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.1"*, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.1"* %this1 to %"struct.std::__2::__compressed_pair_elem.2"*
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__222__compressed_pair_elemImLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.2"* %0) #7
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEE10deallocateEPS6_m(%"class.std::__2::allocator.44"* %this, %"class.std::__2::unique_ptr.37"* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.44"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.37"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.44"* %this, %"class.std::__2::allocator.44"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.37"* %__p, %"class.std::__2::unique_ptr.37"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.44"*, %"class.std::__2::allocator.44"** %this.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.37"*, %"class.std::__2::unique_ptr.37"** %__p.addr, align 4
  %1 = bitcast %"class.std::__2::unique_ptr.37"* %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.37"** @_ZNKSt3__214__split_bufferINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.37"** @_ZNKSt3__217__compressed_pairIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.51"* %__end_cap_) #7
  ret %"class.std::__2::unique_ptr.37"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.37"** @_ZNKSt3__217__compressed_pairIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.51"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.51"*, align 4
  store %"class.std::__2::__compressed_pair.51"* %this, %"class.std::__2::__compressed_pair.51"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.51"*, %"class.std::__2::__compressed_pair.51"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.51"* %this1 to %"struct.std::__2::__compressed_pair_elem.42"*
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.37"** @_ZNKSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.42"* %0) #7
  ret %"class.std::__2::unique_ptr.37"** %call
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { noreturn "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { noinline noreturn nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nobuiltin allocsize(0) "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { nounwind }
attributes #8 = { noreturn }
attributes #9 = { builtin allocsize(0) }
attributes #10 = { builtin nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
