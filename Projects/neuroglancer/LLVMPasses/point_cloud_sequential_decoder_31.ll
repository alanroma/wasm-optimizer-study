; ModuleID = './draco/src/draco/compression/point_cloud/point_cloud_sequential_decoder.cc'
source_filename = "./draco/src/draco/compression/point_cloud/point_cloud_sequential_decoder.cc"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%"class.draco::PointCloudSequentialDecoder" = type { %"class.draco::PointCloudDecoder" }
%"class.draco::PointCloudDecoder" = type { i32 (...)**, %"class.draco::PointCloud"*, %"class.std::__2::vector.94", %"class.std::__2::vector.87", %"class.draco::DecoderBuffer"*, i8, i8, %"class.draco::DracoOptions"* }
%"class.draco::PointCloud" = type { i32 (...)**, %"class.std::__2::unique_ptr", %"class.std::__2::vector.51", [5 x %"class.std::__2::vector.87"], i32 }
%"class.std::__2::unique_ptr" = type { %"class.std::__2::__compressed_pair" }
%"class.std::__2::__compressed_pair" = type { %"struct.std::__2::__compressed_pair_elem" }
%"struct.std::__2::__compressed_pair_elem" = type { %"class.draco::GeometryMetadata"* }
%"class.draco::GeometryMetadata" = type { %"class.draco::Metadata", %"class.std::__2::vector" }
%"class.draco::Metadata" = type { %"class.std::__2::unordered_map", %"class.std::__2::unordered_map.17" }
%"class.std::__2::unordered_map" = type { %"class.std::__2::__hash_table" }
%"class.std::__2::__hash_table" = type { %"class.std::__2::unique_ptr.0", %"class.std::__2::__compressed_pair.7", %"class.std::__2::__compressed_pair.12", %"class.std::__2::__compressed_pair.14" }
%"class.std::__2::unique_ptr.0" = type { %"class.std::__2::__compressed_pair.1" }
%"class.std::__2::__compressed_pair.1" = type { %"struct.std::__2::__compressed_pair_elem.2", %"struct.std::__2::__compressed_pair_elem.3" }
%"struct.std::__2::__compressed_pair_elem.2" = type { %"struct.std::__2::__hash_node_base"** }
%"struct.std::__2::__hash_node_base" = type { %"struct.std::__2::__hash_node_base"* }
%"struct.std::__2::__compressed_pair_elem.3" = type { %"class.std::__2::__bucket_list_deallocator" }
%"class.std::__2::__bucket_list_deallocator" = type { %"class.std::__2::__compressed_pair.4" }
%"class.std::__2::__compressed_pair.4" = type { %"struct.std::__2::__compressed_pair_elem.5" }
%"struct.std::__2::__compressed_pair_elem.5" = type { i32 }
%"class.std::__2::__compressed_pair.7" = type { %"struct.std::__2::__compressed_pair_elem.8" }
%"struct.std::__2::__compressed_pair_elem.8" = type { %"struct.std::__2::__hash_node_base" }
%"class.std::__2::__compressed_pair.12" = type { %"struct.std::__2::__compressed_pair_elem.5" }
%"class.std::__2::__compressed_pair.14" = type { %"struct.std::__2::__compressed_pair_elem.15" }
%"struct.std::__2::__compressed_pair_elem.15" = type { float }
%"class.std::__2::unordered_map.17" = type { %"class.std::__2::__hash_table.18" }
%"class.std::__2::__hash_table.18" = type { %"class.std::__2::unique_ptr.19", %"class.std::__2::__compressed_pair.29", %"class.std::__2::__compressed_pair.34", %"class.std::__2::__compressed_pair.37" }
%"class.std::__2::unique_ptr.19" = type { %"class.std::__2::__compressed_pair.20" }
%"class.std::__2::__compressed_pair.20" = type { %"struct.std::__2::__compressed_pair_elem.21", %"struct.std::__2::__compressed_pair_elem.23" }
%"struct.std::__2::__compressed_pair_elem.21" = type { %"struct.std::__2::__hash_node_base.22"** }
%"struct.std::__2::__hash_node_base.22" = type { %"struct.std::__2::__hash_node_base.22"* }
%"struct.std::__2::__compressed_pair_elem.23" = type { %"class.std::__2::__bucket_list_deallocator.24" }
%"class.std::__2::__bucket_list_deallocator.24" = type { %"class.std::__2::__compressed_pair.25" }
%"class.std::__2::__compressed_pair.25" = type { %"struct.std::__2::__compressed_pair_elem.5" }
%"class.std::__2::__compressed_pair.29" = type { %"struct.std::__2::__compressed_pair_elem.30" }
%"struct.std::__2::__compressed_pair_elem.30" = type { %"struct.std::__2::__hash_node_base.22" }
%"class.std::__2::__compressed_pair.34" = type { %"struct.std::__2::__compressed_pair_elem.5" }
%"class.std::__2::__compressed_pair.37" = type { %"struct.std::__2::__compressed_pair_elem.15" }
%"class.std::__2::vector" = type { %"class.std::__2::__vector_base" }
%"class.std::__2::__vector_base" = type { %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"*, %"class.std::__2::__compressed_pair.44" }
%"class.std::__2::unique_ptr.40" = type { %"class.std::__2::__compressed_pair.41" }
%"class.std::__2::__compressed_pair.41" = type { %"struct.std::__2::__compressed_pair_elem.42" }
%"struct.std::__2::__compressed_pair_elem.42" = type { %"class.draco::AttributeMetadata"* }
%"class.draco::AttributeMetadata" = type { %"class.draco::Metadata", i32 }
%"class.std::__2::__compressed_pair.44" = type { %"struct.std::__2::__compressed_pair_elem.45" }
%"struct.std::__2::__compressed_pair_elem.45" = type { %"class.std::__2::unique_ptr.40"* }
%"class.std::__2::vector.51" = type { %"class.std::__2::__vector_base.52" }
%"class.std::__2::__vector_base.52" = type { %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"*, %"class.std::__2::__compressed_pair.82" }
%"class.std::__2::unique_ptr.53" = type { %"class.std::__2::__compressed_pair.54" }
%"class.std::__2::__compressed_pair.54" = type { %"struct.std::__2::__compressed_pair_elem.55" }
%"struct.std::__2::__compressed_pair_elem.55" = type { %"class.draco::PointAttribute"* }
%"class.draco::PointAttribute" = type <{ %"class.draco::GeometryAttribute", %"class.std::__2::unique_ptr.63", %"class.draco::IndexTypeVector", i32, i8, [3 x i8], %"class.std::__2::unique_ptr.75", [4 x i8] }>
%"class.draco::GeometryAttribute" = type { %"class.draco::DataBuffer"*, %"struct.draco::DataBufferDescriptor", i8, i32, i8, i64, i64, i32, i32 }
%"class.draco::DataBuffer" = type { %"class.std::__2::vector.56", %"struct.draco::DataBufferDescriptor" }
%"class.std::__2::vector.56" = type { %"class.std::__2::__vector_base.57" }
%"class.std::__2::__vector_base.57" = type { i8*, i8*, %"class.std::__2::__compressed_pair.58" }
%"class.std::__2::__compressed_pair.58" = type { %"struct.std::__2::__compressed_pair_elem.59" }
%"struct.std::__2::__compressed_pair_elem.59" = type { i8* }
%"struct.draco::DataBufferDescriptor" = type { i64, i64 }
%"class.std::__2::unique_ptr.63" = type { %"class.std::__2::__compressed_pair.64" }
%"class.std::__2::__compressed_pair.64" = type { %"struct.std::__2::__compressed_pair_elem.65" }
%"struct.std::__2::__compressed_pair_elem.65" = type { %"class.draco::DataBuffer"* }
%"class.draco::IndexTypeVector" = type { %"class.std::__2::vector.68" }
%"class.std::__2::vector.68" = type { %"class.std::__2::__vector_base.69" }
%"class.std::__2::__vector_base.69" = type { %"class.draco::IndexType"*, %"class.draco::IndexType"*, %"class.std::__2::__compressed_pair.70" }
%"class.draco::IndexType" = type { i32 }
%"class.std::__2::__compressed_pair.70" = type { %"struct.std::__2::__compressed_pair_elem.71" }
%"struct.std::__2::__compressed_pair_elem.71" = type { %"class.draco::IndexType"* }
%"class.std::__2::unique_ptr.75" = type { %"class.std::__2::__compressed_pair.76" }
%"class.std::__2::__compressed_pair.76" = type { %"struct.std::__2::__compressed_pair_elem.77" }
%"struct.std::__2::__compressed_pair_elem.77" = type { %"class.draco::AttributeTransformData"* }
%"class.draco::AttributeTransformData" = type { i32, %"class.draco::DataBuffer" }
%"class.std::__2::__compressed_pair.82" = type { %"struct.std::__2::__compressed_pair_elem.83" }
%"struct.std::__2::__compressed_pair_elem.83" = type { %"class.std::__2::unique_ptr.53"* }
%"class.std::__2::vector.94" = type { %"class.std::__2::__vector_base.95" }
%"class.std::__2::__vector_base.95" = type { %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"*, %"class.std::__2::__compressed_pair.101" }
%"class.std::__2::unique_ptr.96" = type { %"class.std::__2::__compressed_pair.97" }
%"class.std::__2::__compressed_pair.97" = type { %"struct.std::__2::__compressed_pair_elem.98" }
%"struct.std::__2::__compressed_pair_elem.98" = type { %"class.draco::AttributesDecoderInterface"* }
%"class.draco::AttributesDecoderInterface" = type { i32 (...)** }
%"class.std::__2::__compressed_pair.101" = type { %"struct.std::__2::__compressed_pair_elem.102" }
%"struct.std::__2::__compressed_pair_elem.102" = type { %"class.std::__2::unique_ptr.96"* }
%"class.std::__2::vector.87" = type { %"class.std::__2::__vector_base.88" }
%"class.std::__2::__vector_base.88" = type { i32*, i32*, %"class.std::__2::__compressed_pair.89" }
%"class.std::__2::__compressed_pair.89" = type { %"struct.std::__2::__compressed_pair_elem.90" }
%"struct.std::__2::__compressed_pair_elem.90" = type { i32* }
%"class.draco::DecoderBuffer" = type { i8*, i64, i64, %"class.draco::DecoderBuffer::BitDecoder", i8, i16 }
%"class.draco::DecoderBuffer::BitDecoder" = type { i8*, i8*, i32 }
%"class.draco::DracoOptions" = type opaque
%"class.std::__2::unique_ptr.106" = type { %"class.std::__2::__compressed_pair.107" }
%"class.std::__2::__compressed_pair.107" = type { %"struct.std::__2::__compressed_pair_elem.108" }
%"struct.std::__2::__compressed_pair_elem.108" = type { %"class.draco::AttributesDecoder"* }
%"class.draco::AttributesDecoder" = type { %"class.draco::AttributesDecoderInterface", %"class.std::__2::vector.87", %"class.std::__2::vector.87", %"class.draco::PointCloudDecoder"*, %"class.draco::PointCloud"* }
%"class.std::__2::unique_ptr.131" = type { %"class.std::__2::__compressed_pair.132" }
%"class.std::__2::__compressed_pair.132" = type { %"struct.std::__2::__compressed_pair_elem.133" }
%"struct.std::__2::__compressed_pair_elem.133" = type { %"class.draco::PointsSequencer"* }
%"class.draco::PointsSequencer" = type { i32 (...)**, %"class.std::__2::vector.123"* }
%"class.std::__2::vector.123" = type { %"class.std::__2::__vector_base.124" }
%"class.std::__2::__vector_base.124" = type { %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"*, %"class.std::__2::__compressed_pair.126" }
%"class.draco::IndexType.125" = type { i32 }
%"class.std::__2::__compressed_pair.126" = type { %"struct.std::__2::__compressed_pair_elem.127" }
%"struct.std::__2::__compressed_pair_elem.127" = type { %"class.draco::IndexType.125"* }
%"class.draco::SequentialAttributeDecodersController" = type { %"class.draco::AttributesDecoder", %"class.std::__2::vector.111", %"class.std::__2::vector.123", %"class.std::__2::unique_ptr.131" }
%"class.std::__2::vector.111" = type { %"class.std::__2::__vector_base.112" }
%"class.std::__2::__vector_base.112" = type { %"class.std::__2::unique_ptr.113"*, %"class.std::__2::unique_ptr.113"*, %"class.std::__2::__compressed_pair.118" }
%"class.std::__2::unique_ptr.113" = type { %"class.std::__2::__compressed_pair.114" }
%"class.std::__2::__compressed_pair.114" = type { %"struct.std::__2::__compressed_pair_elem.115" }
%"struct.std::__2::__compressed_pair_elem.115" = type { %"class.draco::SequentialAttributeDecoder"* }
%"class.draco::SequentialAttributeDecoder" = type { i32 (...)**, %"class.draco::PointCloudDecoder"*, %"class.draco::PointAttribute"*, i32, %"class.std::__2::unique_ptr.53" }
%"class.std::__2::__compressed_pair.118" = type { %"struct.std::__2::__compressed_pair_elem.119" }
%"struct.std::__2::__compressed_pair_elem.119" = type { %"class.std::__2::unique_ptr.113"* }
%"class.draco::LinearSequencer" = type { %"class.draco::PointsSequencer", i32 }
%"struct.std::__2::__default_init_tag" = type { i8 }
%"struct.std::__2::default_delete.110" = type { i8 }
%"struct.std::__2::default_delete.100" = type { i8 }
%"class.std::__2::allocator.104" = type { i8 }
%"struct.std::__2::__split_buffer" = type { %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"*, %"class.std::__2::__compressed_pair.136" }
%"class.std::__2::__compressed_pair.136" = type { %"struct.std::__2::__compressed_pair_elem.102", %"struct.std::__2::__compressed_pair_elem.137" }
%"struct.std::__2::__compressed_pair_elem.137" = type { %"class.std::__2::allocator.104"* }
%"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction" = type { %"class.std::__2::vector.94"*, %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"* }
%"class.std::__2::__vector_base_common" = type { i8 }
%"class.std::__2::__split_buffer_common" = type { i8 }
%"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction" = type { %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** }
%"struct.std::__2::integral_constant" = type { i8 }
%"struct.std::__2::__has_construct" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.99" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.103" = type { i8 }
%"struct.std::__2::__less" = type { i8 }
%"struct.std::__2::__has_max_size" = type { i8 }
%"struct.std::__2::__has_construct.138" = type { i8 }
%"struct.std::__2::integral_constant.139" = type { i8 }
%"struct.std::__2::__has_destroy" = type { i8 }
%"class.std::__2::allocator.73" = type { i8 }
%"struct.std::__2::__has_destroy.140" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.72" = type { i8 }
%"class.std::__2::allocator.129" = type { i8 }
%"struct.std::__2::__split_buffer.141" = type { %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"*, %"class.std::__2::__compressed_pair.142" }
%"class.std::__2::__compressed_pair.142" = type { %"struct.std::__2::__compressed_pair_elem.127", %"struct.std::__2::__compressed_pair_elem.143" }
%"struct.std::__2::__compressed_pair_elem.143" = type { %"class.std::__2::allocator.129"* }
%"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction" = type { %"class.std::__2::vector.123"*, %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"* }
%"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction" = type { %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** }
%"struct.std::__2::__has_construct.144" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.128" = type { i8 }
%"struct.std::__2::__has_max_size.145" = type { i8 }
%"struct.std::__2::__has_destroy.146" = type { i8 }
%"class.std::__2::allocator.92" = type { i8 }
%"struct.std::__2::__has_destroy.147" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.91" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.134" = type { i8 }
%"struct.std::__2::default_delete.135" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.109" = type { i8 }

$_ZN5draco17PointCloudDecoder6bufferEv = comdat any

$_ZN5draco13DecoderBuffer6DecodeIiEEbPT_ = comdat any

$_ZN5draco17PointCloudDecoder11point_cloudEv = comdat any

$_ZN5draco10PointCloud14set_num_pointsEj = comdat any

$_ZN5draco17PointCloudDecoder20SetAttributesDecoderEiNSt3__210unique_ptrINS_26AttributesDecoderInterfaceENS1_14default_deleteIS3_EEEE = comdat any

$_ZNK5draco10PointCloud10num_pointsEv = comdat any

$_ZN5draco15LinearSequencerC2Ei = comdat any

$_ZNSt3__210unique_ptrIN5draco15PointsSequencerENS_14default_deleteIS2_EEEC2ILb1EvEEPS2_ = comdat any

$_ZNSt3__210unique_ptrIN5draco17AttributesDecoderENS_14default_deleteIS2_EEEC2ILb1EvEEPS2_ = comdat any

$_ZNSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEEC2INS1_17AttributesDecoderENS3_IS7_EEvvEEONS0_IT_T0_EE = comdat any

$_ZNSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEED2Ev = comdat any

$_ZNSt3__210unique_ptrIN5draco17AttributesDecoderENS_14default_deleteIS2_EEED2Ev = comdat any

$_ZNSt3__210unique_ptrIN5draco15PointsSequencerENS_14default_deleteIS2_EEED2Ev = comdat any

$_ZN5draco27PointCloudSequentialDecoderD2Ev = comdat any

$_ZN5draco27PointCloudSequentialDecoderD0Ev = comdat any

$_ZNK5draco17PointCloudDecoder15GetGeometryTypeEv = comdat any

$_ZN5draco17PointCloudDecoder17InitializeDecoderEv = comdat any

$_ZN5draco17PointCloudDecoder19OnAttributesDecodedEv = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE6resizeEm = comdat any

$_ZNSt3__24moveIRNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOS9_ = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEixEm = comdat any

$_ZNSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEEaSEOS5_ = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8__appendEm = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__destruct_at_endEPS6_ = comdat any

$_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE9__end_capEv = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE18__construct_at_endEm = comdat any

$_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE11__recommendEm = comdat any

$_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEEC2EmmS9_ = comdat any

$_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE18__construct_at_endEm = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS6_RS8_EE = comdat any

$_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEED2Ev = comdat any

$_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5firstEv = comdat any

$_ZNSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE21_ConstructTransactionC2ERS9_m = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE9constructIS7_JEEEvRS8_PT_DpOT0_ = comdat any

$_ZNSt3__212__to_addressINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEEPT_S8_ = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE21_ConstructTransactionD2Ev = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE11__constructIS7_JEEEvNS_17integral_constantIbLb1EEERS8_PT_DpOT0_ = comdat any

$_ZNSt3__29allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEE9constructIS6_JEEEvPT_DpOT0_ = comdat any

$_ZNSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEEC2ILb1EvEEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEEC2IS3_NS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__27forwardIPN5draco26AttributesDecoderInterfaceEEEOT_RNS_16remove_referenceIS4_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco26AttributesDecoderInterfaceELi0ELb0EEC2IS3_vEEOT_ = comdat any

$_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco26AttributesDecoderInterfaceEEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEELi1ELb1EE5__getEv = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8max_sizeEv = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv = comdat any

$_ZNSt3__23maxImEERKT_S3_S3_ = comdat any

$_ZNSt3__23minImEERKT_S3_S3_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE8max_sizeERKS8_ = comdat any

$_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv = comdat any

$_ZNSt3__214numeric_limitsIlE3maxEv = comdat any

$_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_ = comdat any

$_ZNKSt3__26__lessImmEclERKmS3_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS8_ = comdat any

$_ZNKSt3__29allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEE8max_sizeEv = comdat any

$_ZNKSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE6secondEv = comdat any

$_ZNKSt3__222__compressed_pair_elemINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv = comdat any

$_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__23maxImNS_6__lessImmEEEERKT_S5_S5_T0_ = comdat any

$_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEEC2IDnSA_EEOT_OT0_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE8allocateERS8_m = comdat any

$_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE7__allocEv = comdat any

$_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE9__end_capEv = comdat any

$_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEELi0ELb0EEC2IDnvEEOT_ = comdat any

$_ZNSt3__27forwardIRNS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEEEOT_RNS_16remove_referenceISA_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIRNS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEELi1ELb0EEC2IS9_vEEOT_ = comdat any

$_ZNSt3__29allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEE8allocateEmPKv = comdat any

$_ZNSt3__220__throw_length_errorEPKc = comdat any

$_ZNSt3__217__libcpp_allocateEmm = comdat any

$_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemIRNS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEELi1ELb0EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE5firstEv = comdat any

$_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE21_ConstructTransactionC2EPPS6_m = comdat any

$_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE21_ConstructTransactionD2Ev = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__annotate_deleteEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE46__construct_backward_with_exception_guaranteesIPS7_EEvRS8_T_SD_RSD_ = comdat any

$_ZNSt3__24swapIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS9_EE5valueEvE4typeERS9_SC_ = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE14__annotate_newEm = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE26__invalidate_all_iteratorsEv = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE31__annotate_contiguous_containerEPKvSB_SB_SB_ = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE9constructIS7_JS7_EEEvRS8_PT_DpOT0_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE11__constructIS7_JS7_EEEvNS_17integral_constantIbLb1EEERS8_PT_DpOT0_ = comdat any

$_ZNSt3__27forwardINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEEOT_RNS_16remove_referenceIS7_E4typeE = comdat any

$_ZNSt3__29allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEE9constructIS6_JS6_EEEvPT_DpOT0_ = comdat any

$_ZNSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEEC2EOS5_ = comdat any

$_ZNSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE7releaseEv = comdat any

$_ZNSt3__27forwardINS_14default_deleteIN5draco26AttributesDecoderInterfaceEEEEEOT_RNS_16remove_referenceIS5_E4typeE = comdat any

$_ZNSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE11get_deleterEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEEC2IS3_S5_EEOT_OT0_ = comdat any

$_ZNSt3__217__compressed_pairIPN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco26AttributesDecoderInterfaceELi0ELb0EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco26AttributesDecoderInterfaceEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco26AttributesDecoderInterfaceEEELi1ELb1EEC2IS4_vEEOT_ = comdat any

$_ZNSt3__24moveIRPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOSA_ = comdat any

$_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE5clearEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE10deallocateERS8_PS7_m = comdat any

$_ZNKSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE8capacityEv = comdat any

$_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE17__destruct_at_endEPS6_ = comdat any

$_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE17__destruct_at_endEPS6_NS_17integral_constantIbLb0EEE = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE7destroyIS7_EEvRS8_PT_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE9__destroyIS7_EEvNS_17integral_constantIbLb1EEERS8_PT_ = comdat any

$_ZNSt3__29allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEE7destroyEPS6_ = comdat any

$_ZNSt3__29allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEE10deallocateEPS6_m = comdat any

$_ZNSt3__219__libcpp_deallocateEPvmm = comdat any

$_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm = comdat any

$_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm = comdat any

$_ZNSt3__217_DeallocateCaller9__do_callEPv = comdat any

$_ZNKSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE5firstEv = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE27__invalidate_iterators_pastEPS6_ = comdat any

$_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__destruct_at_endEPS6_ = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__annotate_shrinkEm = comdat any

$_ZNSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE5resetEPS2_ = comdat any

$_ZNKSt3__214default_deleteIN5draco26AttributesDecoderInterfaceEEclEPS2_ = comdat any

$_ZN5draco15PointsSequencerC2Ev = comdat any

$_ZN5draco15LinearSequencerD2Ev = comdat any

$_ZN5draco15LinearSequencerD0Ev = comdat any

$_ZN5draco15LinearSequencer34UpdatePointToAttributeIndexMappingEPNS_14PointAttributeE = comdat any

$_ZN5draco15LinearSequencer24GenerateSequenceInternalEv = comdat any

$_ZN5draco15PointsSequencerD2Ev = comdat any

$_ZN5draco15PointsSequencerD0Ev = comdat any

$_ZN5draco15PointsSequencer34UpdatePointToAttributeIndexMappingEPNS_14PointAttributeE = comdat any

$_ZN5draco14PointAttribute18SetIdentityMappingEv = comdat any

$_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEEE5clearEv = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5clearEv = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5clearEv = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_shrinkEm = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE26__invalidate_all_iteratorsEv = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE7destroyIS5_EEvRS6_PT_ = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv = comdat any

$_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEPT_S6_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE9__destroyIS5_EEvNS_17integral_constantIbLb1EEERS6_PT_ = comdat any

$_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE7destroyEPS4_ = comdat any

$_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb1EE5__getEv = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_ = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELi0ELb0EE5__getEv = comdat any

$_ZNK5draco15PointsSequencer13out_point_idsEv = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE6resizeEm = comdat any

$_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEC2Ej = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE2atEm = comdat any

$_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEaSERKS2_ = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE8__appendEm = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_ = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE18__construct_at_endEm = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE11__recommendEm = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEEC2EmmS7_ = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE18__construct_at_endEm = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS4_RS6_EE = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEED2Ev = comdat any

$_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionC2ERS7_m = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE9constructIS5_JEEEvRS6_PT_DpOT0_ = comdat any

$_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEEEPT_S6_ = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionD2Ev = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE11__constructIS5_JEEEvNS_17integral_constantIbLb1EEERS6_PT_DpOT0_ = comdat any

$_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEE9constructIS4_JEEEvPT_DpOT0_ = comdat any

$_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEC2Ev = comdat any

$_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEELi1ELb1EE5__getEv = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE8max_sizeEv = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE8max_sizeERKS6_ = comdat any

$_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS6_ = comdat any

$_ZNKSt3__29allocatorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEE8max_sizeEv = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv = comdat any

$_ZNKSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEELi1ELb1EE5__getEv = comdat any

$_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEEC2IDnS8_EEOT_OT0_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE8allocateERS6_m = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE7__allocEv = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE9__end_capEv = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELi0ELb0EEC2IDnvEEOT_ = comdat any

$_ZNSt3__27forwardIRNS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEEEOT_RNS_16remove_referenceIS8_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEELi1ELb0EEC2IS7_vEEOT_ = comdat any

$_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEE8allocateEmPKv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEELi1ELb0EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE5firstEv = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE21_ConstructTransactionC2EPPS4_m = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE21_ConstructTransactionD2Ev = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_deleteEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE46__construct_backward_with_exception_guaranteesIS5_EENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS6_PT_SC_EE5valuesr31is_trivially_move_constructibleISC_EE5valueEvE4typeERS6_SD_SD_RSD_ = comdat any

$_ZNSt3__24swapIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS7_EE5valueEvE4typeERS7_SA_ = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE14__annotate_newEm = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE26__invalidate_all_iteratorsEv = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_ = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv = comdat any

$_ZNSt3__24moveIRPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEEEONS_16remove_referenceIT_E4typeEOS8_ = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE5clearEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE10deallocateERS6_PS5_m = comdat any

$_ZNKSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE8capacityEv = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE17__destruct_at_endEPS4_ = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE17__destruct_at_endEPS4_NS_17integral_constantIbLb0EEE = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE7destroyIS5_EEvRS6_PT_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE9__destroyIS5_EEvNS_17integral_constantIbLb1EEERS6_PT_ = comdat any

$_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEE7destroyEPS4_ = comdat any

$_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEE10deallocateEPS4_m = comdat any

$_ZNKSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE5firstEv = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE27__invalidate_iterators_pastEPS4_ = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_ = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_shrinkEm = comdat any

$_ZN5draco17PointCloudDecoderD2Ev = comdat any

$_ZNSt3__26vectorIiNS_9allocatorIiEEED2Ev = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEED2Ev = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEE17__annotate_deleteEv = comdat any

$_ZNSt3__213__vector_baseIiNS_9allocatorIiEEED2Ev = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEE31__annotate_contiguous_containerEPKvS5_S5_S5_ = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEE8capacityEv = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEE4sizeEv = comdat any

$_ZNSt3__212__to_addressIiEEPT_S2_ = comdat any

$_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPiNS_9allocatorIiEEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPiLi0ELb0EE5__getEv = comdat any

$_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE5clearEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIiEEE10deallocateERS2_Pim = comdat any

$_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE7__allocEv = comdat any

$_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE17__destruct_at_endEPi = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIiEEE7destroyIiEEvRS2_PT_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIiEEE9__destroyIiEEvNS_17integral_constantIbLb1EEERS2_PT_ = comdat any

$_ZNSt3__29allocatorIiE7destroyEPi = comdat any

$_ZNSt3__29allocatorIiE10deallocateEPim = comdat any

$_ZNSt3__217__compressed_pairIPiNS_9allocatorIiEEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIiEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEED2Ev = comdat any

$_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5clearEv = comdat any

$_ZN5draco13DecoderBuffer4PeekIiEEbPT_ = comdat any

$_ZNSt3__217__compressed_pairIPN5draco15PointsSequencerENS_14default_deleteIS2_EEEC2IRS3_NS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__27forwardIRPN5draco15PointsSequencerEEEOT_RNS_16remove_referenceIS5_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco15PointsSequencerELi0ELb0EEC2IRS3_vEEOT_ = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco15PointsSequencerEEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__210unique_ptrIN5draco15PointsSequencerENS_14default_deleteIS2_EEE5resetEPS2_ = comdat any

$_ZNSt3__217__compressed_pairIPN5draco15PointsSequencerENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco15PointsSequencerENS_14default_deleteIS2_EEE6secondEv = comdat any

$_ZNKSt3__214default_deleteIN5draco15PointsSequencerEEclEPS2_ = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco15PointsSequencerELi0ELb0EE5__getEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco15PointsSequencerEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco17AttributesDecoderENS_14default_deleteIS2_EEEC2IRS3_NS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__27forwardIRPN5draco17AttributesDecoderEEEOT_RNS_16remove_referenceIS5_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco17AttributesDecoderELi0ELb0EEC2IRS3_vEEOT_ = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco17AttributesDecoderEEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__210unique_ptrIN5draco17AttributesDecoderENS_14default_deleteIS2_EEE5resetEPS2_ = comdat any

$_ZNSt3__217__compressed_pairIPN5draco17AttributesDecoderENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco17AttributesDecoderENS_14default_deleteIS2_EEE6secondEv = comdat any

$_ZNKSt3__214default_deleteIN5draco17AttributesDecoderEEclEPS2_ = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco17AttributesDecoderELi0ELb0EE5__getEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco17AttributesDecoderEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__210unique_ptrIN5draco17AttributesDecoderENS_14default_deleteIS2_EEE7releaseEv = comdat any

$_ZNSt3__27forwardINS_14default_deleteIN5draco17AttributesDecoderEEEEEOT_RNS_16remove_referenceIS5_E4typeE = comdat any

$_ZNSt3__210unique_ptrIN5draco17AttributesDecoderENS_14default_deleteIS2_EEE11get_deleterEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEEC2IPNS1_17AttributesDecoderENS4_IS8_EEEEOT_OT0_ = comdat any

$_ZNSt3__27forwardIPN5draco17AttributesDecoderEEEOT_RNS_16remove_referenceIS4_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco26AttributesDecoderInterfaceELi0ELb0EEC2IPNS1_17AttributesDecoderEvEEOT_ = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco26AttributesDecoderInterfaceEEELi1ELb1EEC2INS1_INS2_17AttributesDecoderEEEvEEOT_ = comdat any

$_ZNSt3__214default_deleteIN5draco26AttributesDecoderInterfaceEEC2INS1_17AttributesDecoderEEERKNS0_IT_EEPNS_9enable_ifIXsr14is_convertibleIPS6_PS2_EE5valueEvE4typeE = comdat any

$_ZTVN5draco15LinearSequencerE = comdat any

$_ZTVN5draco15PointsSequencerE = comdat any

@_ZTVN5draco27PointCloudSequentialDecoderE = hidden unnamed_addr constant { [11 x i8*] } { [11 x i8*] [i8* null, i8* null, i8* bitcast (%"class.draco::PointCloudSequentialDecoder"* (%"class.draco::PointCloudSequentialDecoder"*)* @_ZN5draco27PointCloudSequentialDecoderD2Ev to i8*), i8* bitcast (void (%"class.draco::PointCloudSequentialDecoder"*)* @_ZN5draco27PointCloudSequentialDecoderD0Ev to i8*), i8* bitcast (i32 (%"class.draco::PointCloudDecoder"*)* @_ZNK5draco17PointCloudDecoder15GetGeometryTypeEv to i8*), i8* bitcast (i1 (%"class.draco::PointCloudDecoder"*)* @_ZN5draco17PointCloudDecoder17InitializeDecoderEv to i8*), i8* bitcast (i1 (%"class.draco::PointCloudSequentialDecoder"*, i32)* @_ZN5draco27PointCloudSequentialDecoder23CreateAttributesDecoderEi to i8*), i8* bitcast (i1 (%"class.draco::PointCloudSequentialDecoder"*)* @_ZN5draco27PointCloudSequentialDecoder18DecodeGeometryDataEv to i8*), i8* bitcast (i1 (%"class.draco::PointCloudDecoder"*)* @_ZN5draco17PointCloudDecoder21DecodePointAttributesEv to i8*), i8* bitcast (i1 (%"class.draco::PointCloudDecoder"*)* @_ZN5draco17PointCloudDecoder19DecodeAllAttributesEv to i8*), i8* bitcast (i1 (%"class.draco::PointCloudDecoder"*)* @_ZN5draco17PointCloudDecoder19OnAttributesDecodedEv to i8*)] }, align 4
@.str = private unnamed_addr constant [68 x i8] c"allocator<T>::allocate(size_t n) 'n' exceeds maximum supported size\00", align 1
@_ZTVN5draco15LinearSequencerE = linkonce_odr hidden unnamed_addr constant { [6 x i8*] } { [6 x i8*] [i8* null, i8* null, i8* bitcast (%"class.draco::LinearSequencer"* (%"class.draco::LinearSequencer"*)* @_ZN5draco15LinearSequencerD2Ev to i8*), i8* bitcast (void (%"class.draco::LinearSequencer"*)* @_ZN5draco15LinearSequencerD0Ev to i8*), i8* bitcast (i1 (%"class.draco::LinearSequencer"*, %"class.draco::PointAttribute"*)* @_ZN5draco15LinearSequencer34UpdatePointToAttributeIndexMappingEPNS_14PointAttributeE to i8*), i8* bitcast (i1 (%"class.draco::LinearSequencer"*)* @_ZN5draco15LinearSequencer24GenerateSequenceInternalEv to i8*)] }, comdat, align 4
@_ZTVN5draco15PointsSequencerE = linkonce_odr hidden unnamed_addr constant { [6 x i8*] } { [6 x i8*] [i8* null, i8* null, i8* bitcast (%"class.draco::PointsSequencer"* (%"class.draco::PointsSequencer"*)* @_ZN5draco15PointsSequencerD2Ev to i8*), i8* bitcast (void (%"class.draco::PointsSequencer"*)* @_ZN5draco15PointsSequencerD0Ev to i8*), i8* bitcast (i1 (%"class.draco::PointsSequencer"*, %"class.draco::PointAttribute"*)* @_ZN5draco15PointsSequencer34UpdatePointToAttributeIndexMappingEPNS_14PointAttributeE to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, comdat, align 4
@_ZTVN5draco17PointCloudDecoderE = external unnamed_addr constant { [11 x i8*] }, align 4

; Function Attrs: noinline nounwind optnone
define hidden zeroext i1 @_ZN5draco27PointCloudSequentialDecoder18DecodeGeometryDataEv(%"class.draco::PointCloudSequentialDecoder"* %this) unnamed_addr #0 {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::PointCloudSequentialDecoder"*, align 4
  %num_points = alloca i32, align 4
  store %"class.draco::PointCloudSequentialDecoder"* %this, %"class.draco::PointCloudSequentialDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::PointCloudSequentialDecoder"*, %"class.draco::PointCloudSequentialDecoder"** %this.addr, align 4
  %0 = bitcast %"class.draco::PointCloudSequentialDecoder"* %this1 to %"class.draco::PointCloudDecoder"*
  %call = call %"class.draco::DecoderBuffer"* @_ZN5draco17PointCloudDecoder6bufferEv(%"class.draco::PointCloudDecoder"* %0)
  %call2 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIiEEbPT_(%"class.draco::DecoderBuffer"* %call, i32* %num_points)
  br i1 %call2, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %1 = bitcast %"class.draco::PointCloudSequentialDecoder"* %this1 to %"class.draco::PointCloudDecoder"*
  %call3 = call %"class.draco::PointCloud"* @_ZN5draco17PointCloudDecoder11point_cloudEv(%"class.draco::PointCloudDecoder"* %1)
  %2 = load i32, i32* %num_points, align 4
  call void @_ZN5draco10PointCloud14set_num_pointsEj(%"class.draco::PointCloud"* %call3, i32 %2)
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i1, i1* %retval, align 1
  ret i1 %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::DecoderBuffer"* @_ZN5draco17PointCloudDecoder6bufferEv(%"class.draco::PointCloudDecoder"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointCloudDecoder"*, align 4
  store %"class.draco::PointCloudDecoder"* %this, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::PointCloudDecoder"*, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  %buffer_ = getelementptr inbounds %"class.draco::PointCloudDecoder", %"class.draco::PointCloudDecoder"* %this1, i32 0, i32 4
  %0 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer_, align 4
  ret %"class.draco::DecoderBuffer"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIiEEbPT_(%"class.draco::DecoderBuffer"* %this, i32* %out_val) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %out_val.addr = alloca i32*, align 4
  store %"class.draco::DecoderBuffer"* %this, %"class.draco::DecoderBuffer"** %this.addr, align 4
  store i32* %out_val, i32** %out_val.addr, align 4
  %this1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %this.addr, align 4
  %0 = load i32*, i32** %out_val.addr, align 4
  %call = call zeroext i1 @_ZN5draco13DecoderBuffer4PeekIiEEbPT_(%"class.draco::DecoderBuffer"* %this1, i32* %0)
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %pos_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %1 = load i64, i64* %pos_, align 8
  %add = add nsw i64 %1, 4
  store i64 %add, i64* %pos_, align 8
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %2 = load i1, i1* %retval, align 1
  ret i1 %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::PointCloud"* @_ZN5draco17PointCloudDecoder11point_cloudEv(%"class.draco::PointCloudDecoder"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointCloudDecoder"*, align 4
  store %"class.draco::PointCloudDecoder"* %this, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::PointCloudDecoder"*, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  %point_cloud_ = getelementptr inbounds %"class.draco::PointCloudDecoder", %"class.draco::PointCloudDecoder"* %this1, i32 0, i32 1
  %0 = load %"class.draco::PointCloud"*, %"class.draco::PointCloud"** %point_cloud_, align 4
  ret %"class.draco::PointCloud"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco10PointCloud14set_num_pointsEj(%"class.draco::PointCloud"* %this, i32 %num) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointCloud"*, align 4
  %num.addr = alloca i32, align 4
  store %"class.draco::PointCloud"* %this, %"class.draco::PointCloud"** %this.addr, align 4
  store i32 %num, i32* %num.addr, align 4
  %this1 = load %"class.draco::PointCloud"*, %"class.draco::PointCloud"** %this.addr, align 4
  %0 = load i32, i32* %num.addr, align 4
  %num_points_ = getelementptr inbounds %"class.draco::PointCloud", %"class.draco::PointCloud"* %this1, i32 0, i32 4
  store i32 %0, i32* %num_points_, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden zeroext i1 @_ZN5draco27PointCloudSequentialDecoder23CreateAttributesDecoderEi(%"class.draco::PointCloudSequentialDecoder"* %this, i32 %att_decoder_id) unnamed_addr #0 {
entry:
  %this.addr = alloca %"class.draco::PointCloudSequentialDecoder"*, align 4
  %att_decoder_id.addr = alloca i32, align 4
  %agg.tmp = alloca %"class.std::__2::unique_ptr.96", align 4
  %ref.tmp = alloca %"class.std::__2::unique_ptr.106", align 4
  %agg.tmp2 = alloca %"class.std::__2::unique_ptr.131", align 4
  store %"class.draco::PointCloudSequentialDecoder"* %this, %"class.draco::PointCloudSequentialDecoder"** %this.addr, align 4
  store i32 %att_decoder_id, i32* %att_decoder_id.addr, align 4
  %this1 = load %"class.draco::PointCloudSequentialDecoder"*, %"class.draco::PointCloudSequentialDecoder"** %this.addr, align 4
  %0 = bitcast %"class.draco::PointCloudSequentialDecoder"* %this1 to %"class.draco::PointCloudDecoder"*
  %1 = load i32, i32* %att_decoder_id.addr, align 4
  %call = call noalias nonnull i8* @_Znwm(i32 64) #8
  %2 = bitcast i8* %call to %"class.draco::SequentialAttributeDecodersController"*
  %call3 = call noalias nonnull i8* @_Znwm(i32 12) #8
  %3 = bitcast i8* %call3 to %"class.draco::LinearSequencer"*
  %4 = bitcast %"class.draco::PointCloudSequentialDecoder"* %this1 to %"class.draco::PointCloudDecoder"*
  %call4 = call %"class.draco::PointCloud"* @_ZN5draco17PointCloudDecoder11point_cloudEv(%"class.draco::PointCloudDecoder"* %4)
  %call5 = call i32 @_ZNK5draco10PointCloud10num_pointsEv(%"class.draco::PointCloud"* %call4)
  %call6 = call %"class.draco::LinearSequencer"* @_ZN5draco15LinearSequencerC2Ei(%"class.draco::LinearSequencer"* %3, i32 %call5)
  %5 = bitcast %"class.draco::LinearSequencer"* %3 to %"class.draco::PointsSequencer"*
  %call7 = call %"class.std::__2::unique_ptr.131"* @_ZNSt3__210unique_ptrIN5draco15PointsSequencerENS_14default_deleteIS2_EEEC2ILb1EvEEPS2_(%"class.std::__2::unique_ptr.131"* %agg.tmp2, %"class.draco::PointsSequencer"* %5) #9
  %call8 = call %"class.draco::SequentialAttributeDecodersController"* @_ZN5draco37SequentialAttributeDecodersControllerC1ENSt3__210unique_ptrINS_15PointsSequencerENS1_14default_deleteIS3_EEEE(%"class.draco::SequentialAttributeDecodersController"* %2, %"class.std::__2::unique_ptr.131"* %agg.tmp2)
  %6 = bitcast %"class.draco::SequentialAttributeDecodersController"* %2 to %"class.draco::AttributesDecoder"*
  %call9 = call %"class.std::__2::unique_ptr.106"* @_ZNSt3__210unique_ptrIN5draco17AttributesDecoderENS_14default_deleteIS2_EEEC2ILb1EvEEPS2_(%"class.std::__2::unique_ptr.106"* %ref.tmp, %"class.draco::AttributesDecoder"* %6) #9
  %call10 = call %"class.std::__2::unique_ptr.96"* @_ZNSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEEC2INS1_17AttributesDecoderENS3_IS7_EEvvEEONS0_IT_T0_EE(%"class.std::__2::unique_ptr.96"* %agg.tmp, %"class.std::__2::unique_ptr.106"* nonnull align 4 dereferenceable(4) %ref.tmp) #9
  %call11 = call zeroext i1 @_ZN5draco17PointCloudDecoder20SetAttributesDecoderEiNSt3__210unique_ptrINS_26AttributesDecoderInterfaceENS1_14default_deleteIS3_EEEE(%"class.draco::PointCloudDecoder"* %0, i32 %1, %"class.std::__2::unique_ptr.96"* %agg.tmp)
  %call12 = call %"class.std::__2::unique_ptr.96"* @_ZNSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.96"* %agg.tmp) #9
  %call13 = call %"class.std::__2::unique_ptr.106"* @_ZNSt3__210unique_ptrIN5draco17AttributesDecoderENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.106"* %ref.tmp) #9
  %call14 = call %"class.std::__2::unique_ptr.131"* @_ZNSt3__210unique_ptrIN5draco15PointsSequencerENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.131"* %agg.tmp2) #9
  ret i1 %call11
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco17PointCloudDecoder20SetAttributesDecoderEiNSt3__210unique_ptrINS_26AttributesDecoderInterfaceENS1_14default_deleteIS3_EEEE(%"class.draco::PointCloudDecoder"* %this, i32 %att_decoder_id, %"class.std::__2::unique_ptr.96"* %decoder) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::PointCloudDecoder"*, align 4
  %att_decoder_id.addr = alloca i32, align 4
  store %"class.draco::PointCloudDecoder"* %this, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  store i32 %att_decoder_id, i32* %att_decoder_id.addr, align 4
  %this1 = load %"class.draco::PointCloudDecoder"*, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  %0 = load i32, i32* %att_decoder_id.addr, align 4
  %cmp = icmp slt i32 %0, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %1 = load i32, i32* %att_decoder_id.addr, align 4
  %attributes_decoders_ = getelementptr inbounds %"class.draco::PointCloudDecoder", %"class.draco::PointCloudDecoder"* %this1, i32 0, i32 2
  %call = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.94"* %attributes_decoders_) #9
  %cmp2 = icmp sge i32 %1, %call
  br i1 %cmp2, label %if.then3, label %if.end5

if.then3:                                         ; preds = %if.end
  %attributes_decoders_4 = getelementptr inbounds %"class.draco::PointCloudDecoder", %"class.draco::PointCloudDecoder"* %this1, i32 0, i32 2
  %2 = load i32, i32* %att_decoder_id.addr, align 4
  %add = add nsw i32 %2, 1
  call void @_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE6resizeEm(%"class.std::__2::vector.94"* %attributes_decoders_4, i32 %add)
  br label %if.end5

if.end5:                                          ; preds = %if.then3, %if.end
  %call6 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"* @_ZNSt3__24moveIRNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOS9_(%"class.std::__2::unique_ptr.96"* nonnull align 4 dereferenceable(4) %decoder) #9
  %attributes_decoders_7 = getelementptr inbounds %"class.draco::PointCloudDecoder", %"class.draco::PointCloudDecoder"* %this1, i32 0, i32 2
  %3 = load i32, i32* %att_decoder_id.addr, align 4
  %call8 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEixEm(%"class.std::__2::vector.94"* %attributes_decoders_7, i32 %3) #9
  %call9 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"* @_ZNSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEEaSEOS5_(%"class.std::__2::unique_ptr.96"* %call8, %"class.std::__2::unique_ptr.96"* nonnull align 4 dereferenceable(4) %call6) #9
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end5, %if.then
  %4 = load i1, i1* %retval, align 1
  ret i1 %4
}

; Function Attrs: nobuiltin allocsize(0)
declare nonnull i8* @_Znwm(i32) #1

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco10PointCloud10num_pointsEv(%"class.draco::PointCloud"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointCloud"*, align 4
  store %"class.draco::PointCloud"* %this, %"class.draco::PointCloud"** %this.addr, align 4
  %this1 = load %"class.draco::PointCloud"*, %"class.draco::PointCloud"** %this.addr, align 4
  %num_points_ = getelementptr inbounds %"class.draco::PointCloud", %"class.draco::PointCloud"* %this1, i32 0, i32 4
  %0 = load i32, i32* %num_points_, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::LinearSequencer"* @_ZN5draco15LinearSequencerC2Ei(%"class.draco::LinearSequencer"* returned %this, i32 %num_points) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::LinearSequencer"*, align 4
  %num_points.addr = alloca i32, align 4
  store %"class.draco::LinearSequencer"* %this, %"class.draco::LinearSequencer"** %this.addr, align 4
  store i32 %num_points, i32* %num_points.addr, align 4
  %this1 = load %"class.draco::LinearSequencer"*, %"class.draco::LinearSequencer"** %this.addr, align 4
  %0 = bitcast %"class.draco::LinearSequencer"* %this1 to %"class.draco::PointsSequencer"*
  %call = call %"class.draco::PointsSequencer"* @_ZN5draco15PointsSequencerC2Ev(%"class.draco::PointsSequencer"* %0)
  %1 = bitcast %"class.draco::LinearSequencer"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [6 x i8*] }, { [6 x i8*] }* @_ZTVN5draco15LinearSequencerE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %num_points_ = getelementptr inbounds %"class.draco::LinearSequencer", %"class.draco::LinearSequencer"* %this1, i32 0, i32 1
  %2 = load i32, i32* %num_points.addr, align 4
  store i32 %2, i32* %num_points_, align 4
  ret %"class.draco::LinearSequencer"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.131"* @_ZNSt3__210unique_ptrIN5draco15PointsSequencerENS_14default_deleteIS2_EEEC2ILb1EvEEPS2_(%"class.std::__2::unique_ptr.131"* returned %this, %"class.draco::PointsSequencer"* %__p) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.131"*, align 4
  %__p.addr = alloca %"class.draco::PointsSequencer"*, align 4
  %ref.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::unique_ptr.131"* %this, %"class.std::__2::unique_ptr.131"** %this.addr, align 4
  store %"class.draco::PointsSequencer"* %__p, %"class.draco::PointsSequencer"** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.131"*, %"class.std::__2::unique_ptr.131"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.131", %"class.std::__2::unique_ptr.131"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::__compressed_pair.132"* @_ZNSt3__217__compressed_pairIPN5draco15PointsSequencerENS_14default_deleteIS2_EEEC2IRS3_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.132"* %__ptr_, %"class.draco::PointsSequencer"** nonnull align 4 dereferenceable(4) %__p.addr, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp)
  ret %"class.std::__2::unique_ptr.131"* %this1
}

declare %"class.draco::SequentialAttributeDecodersController"* @_ZN5draco37SequentialAttributeDecodersControllerC1ENSt3__210unique_ptrINS_15PointsSequencerENS1_14default_deleteIS3_EEEE(%"class.draco::SequentialAttributeDecodersController"* returned, %"class.std::__2::unique_ptr.131"*) unnamed_addr #2

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.106"* @_ZNSt3__210unique_ptrIN5draco17AttributesDecoderENS_14default_deleteIS2_EEEC2ILb1EvEEPS2_(%"class.std::__2::unique_ptr.106"* returned %this, %"class.draco::AttributesDecoder"* %__p) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.106"*, align 4
  %__p.addr = alloca %"class.draco::AttributesDecoder"*, align 4
  %ref.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::unique_ptr.106"* %this, %"class.std::__2::unique_ptr.106"** %this.addr, align 4
  store %"class.draco::AttributesDecoder"* %__p, %"class.draco::AttributesDecoder"** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.106"*, %"class.std::__2::unique_ptr.106"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.106", %"class.std::__2::unique_ptr.106"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::__compressed_pair.107"* @_ZNSt3__217__compressed_pairIPN5draco17AttributesDecoderENS_14default_deleteIS2_EEEC2IRS3_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.107"* %__ptr_, %"class.draco::AttributesDecoder"** nonnull align 4 dereferenceable(4) %__p.addr, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp)
  ret %"class.std::__2::unique_ptr.106"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.96"* @_ZNSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEEC2INS1_17AttributesDecoderENS3_IS7_EEvvEEONS0_IT_T0_EE(%"class.std::__2::unique_ptr.96"* returned %this, %"class.std::__2::unique_ptr.106"* nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  %__u.addr = alloca %"class.std::__2::unique_ptr.106"*, align 4
  %ref.tmp = alloca %"class.draco::AttributesDecoder"*, align 4
  store %"class.std::__2::unique_ptr.96"* %this, %"class.std::__2::unique_ptr.96"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.106"* %__u, %"class.std::__2::unique_ptr.106"** %__u.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::unique_ptr.106"*, %"class.std::__2::unique_ptr.106"** %__u.addr, align 4
  %call = call %"class.draco::AttributesDecoder"* @_ZNSt3__210unique_ptrIN5draco17AttributesDecoderENS_14default_deleteIS2_EEE7releaseEv(%"class.std::__2::unique_ptr.106"* %0) #9
  store %"class.draco::AttributesDecoder"* %call, %"class.draco::AttributesDecoder"** %ref.tmp, align 4
  %1 = load %"class.std::__2::unique_ptr.106"*, %"class.std::__2::unique_ptr.106"** %__u.addr, align 4
  %call2 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.110"* @_ZNSt3__210unique_ptrIN5draco17AttributesDecoderENS_14default_deleteIS2_EEE11get_deleterEv(%"class.std::__2::unique_ptr.106"* %1) #9
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.110"* @_ZNSt3__27forwardINS_14default_deleteIN5draco17AttributesDecoderEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.110"* nonnull align 1 dereferenceable(1) %call2) #9
  %call4 = call %"class.std::__2::__compressed_pair.97"* @_ZNSt3__217__compressed_pairIPN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEEC2IPNS1_17AttributesDecoderENS4_IS8_EEEEOT_OT0_(%"class.std::__2::__compressed_pair.97"* %__ptr_, %"class.draco::AttributesDecoder"** nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::default_delete.110"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::unique_ptr.96"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.96"* @_ZNSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.96"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  store %"class.std::__2::unique_ptr.96"* %this, %"class.std::__2::unique_ptr.96"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %this.addr, align 4
  call void @_ZNSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.96"* %this1, %"class.draco::AttributesDecoderInterface"* null) #9
  ret %"class.std::__2::unique_ptr.96"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.106"* @_ZNSt3__210unique_ptrIN5draco17AttributesDecoderENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.106"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.106"*, align 4
  store %"class.std::__2::unique_ptr.106"* %this, %"class.std::__2::unique_ptr.106"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.106"*, %"class.std::__2::unique_ptr.106"** %this.addr, align 4
  call void @_ZNSt3__210unique_ptrIN5draco17AttributesDecoderENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.106"* %this1, %"class.draco::AttributesDecoder"* null) #9
  ret %"class.std::__2::unique_ptr.106"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.131"* @_ZNSt3__210unique_ptrIN5draco15PointsSequencerENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.131"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.131"*, align 4
  store %"class.std::__2::unique_ptr.131"* %this, %"class.std::__2::unique_ptr.131"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.131"*, %"class.std::__2::unique_ptr.131"** %this.addr, align 4
  call void @_ZNSt3__210unique_ptrIN5draco15PointsSequencerENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.131"* %this1, %"class.draco::PointsSequencer"* null) #9
  ret %"class.std::__2::unique_ptr.131"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::PointCloudSequentialDecoder"* @_ZN5draco27PointCloudSequentialDecoderD2Ev(%"class.draco::PointCloudSequentialDecoder"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointCloudSequentialDecoder"*, align 4
  store %"class.draco::PointCloudSequentialDecoder"* %this, %"class.draco::PointCloudSequentialDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::PointCloudSequentialDecoder"*, %"class.draco::PointCloudSequentialDecoder"** %this.addr, align 4
  %0 = bitcast %"class.draco::PointCloudSequentialDecoder"* %this1 to %"class.draco::PointCloudDecoder"*
  %call = call %"class.draco::PointCloudDecoder"* @_ZN5draco17PointCloudDecoderD2Ev(%"class.draco::PointCloudDecoder"* %0) #9
  ret %"class.draco::PointCloudSequentialDecoder"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco27PointCloudSequentialDecoderD0Ev(%"class.draco::PointCloudSequentialDecoder"* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointCloudSequentialDecoder"*, align 4
  store %"class.draco::PointCloudSequentialDecoder"* %this, %"class.draco::PointCloudSequentialDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::PointCloudSequentialDecoder"*, %"class.draco::PointCloudSequentialDecoder"** %this.addr, align 4
  %call = call %"class.draco::PointCloudSequentialDecoder"* @_ZN5draco27PointCloudSequentialDecoderD2Ev(%"class.draco::PointCloudSequentialDecoder"* %this1) #9
  %0 = bitcast %"class.draco::PointCloudSequentialDecoder"* %this1 to i8*
  call void @_ZdlPv(i8* %0) #10
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco17PointCloudDecoder15GetGeometryTypeEv(%"class.draco::PointCloudDecoder"* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointCloudDecoder"*, align 4
  store %"class.draco::PointCloudDecoder"* %this, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::PointCloudDecoder"*, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  ret i32 0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco17PointCloudDecoder17InitializeDecoderEv(%"class.draco::PointCloudDecoder"* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointCloudDecoder"*, align 4
  store %"class.draco::PointCloudDecoder"* %this, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::PointCloudDecoder"*, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  ret i1 true
}

declare zeroext i1 @_ZN5draco17PointCloudDecoder21DecodePointAttributesEv(%"class.draco::PointCloudDecoder"*) unnamed_addr #2

declare zeroext i1 @_ZN5draco17PointCloudDecoder19DecodeAllAttributesEv(%"class.draco::PointCloudDecoder"*) unnamed_addr #2

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco17PointCloudDecoder19OnAttributesDecodedEv(%"class.draco::PointCloudDecoder"* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointCloudDecoder"*, align 4
  store %"class.draco::PointCloudDecoder"* %this, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::PointCloudDecoder"*, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  ret i1 true
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.94"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %0, i32 0, i32 1
  %1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %2, i32 0, i32 0
  %3 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.std::__2::unique_ptr.96"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.std::__2::unique_ptr.96"* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE6resizeEm(%"class.std::__2::vector.94"* %this, i32 %__sz) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  %__sz.addr = alloca i32, align 4
  %__cs = alloca i32, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  store i32 %__sz, i32* %__sz.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.94"* %this1) #9
  store i32 %call, i32* %__cs, align 4
  %0 = load i32, i32* %__cs, align 4
  %1 = load i32, i32* %__sz.addr, align 4
  %cmp = icmp ult i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %__sz.addr, align 4
  %3 = load i32, i32* %__cs, align 4
  %sub = sub i32 %2, %3
  call void @_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8__appendEm(%"class.std::__2::vector.94"* %this1, i32 %sub)
  br label %if.end4

if.else:                                          ; preds = %entry
  %4 = load i32, i32* %__cs, align 4
  %5 = load i32, i32* %__sz.addr, align 4
  %cmp2 = icmp ugt i32 %4, %5
  br i1 %cmp2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.else
  %6 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %6, i32 0, i32 0
  %7 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__begin_, align 4
  %8 = load i32, i32* %__sz.addr, align 4
  %add.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %7, i32 %8
  call void @_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__destruct_at_endEPS6_(%"class.std::__2::vector.94"* %this1, %"class.std::__2::unique_ptr.96"* %add.ptr) #9
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.else
  br label %if.end4

if.end4:                                          ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"* @_ZNSt3__24moveIRNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOS9_(%"class.std::__2::unique_ptr.96"* nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  store %"class.std::__2::unique_ptr.96"* %__t, %"class.std::__2::unique_ptr.96"** %__t.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__t.addr, align 4
  ret %"class.std::__2::unique_ptr.96"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEixEm(%"class.std::__2::vector.94"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %0, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__begin_, align 4
  %2 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %1, i32 %2
  ret %"class.std::__2::unique_ptr.96"* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"* @_ZNSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEEaSEOS5_(%"class.std::__2::unique_ptr.96"* %this, %"class.std::__2::unique_ptr.96"* nonnull align 4 dereferenceable(4) %__u) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  %__u.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  store %"class.std::__2::unique_ptr.96"* %this, %"class.std::__2::unique_ptr.96"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.96"* %__u, %"class.std::__2::unique_ptr.96"** %__u.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %this.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__u.addr, align 4
  %call = call %"class.draco::AttributesDecoderInterface"* @_ZNSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE7releaseEv(%"class.std::__2::unique_ptr.96"* %0) #9
  call void @_ZNSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.96"* %this1, %"class.draco::AttributesDecoderInterface"* %call) #9
  %1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__u.addr, align 4
  %call2 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.100"* @_ZNSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE11get_deleterEv(%"class.std::__2::unique_ptr.96"* %1) #9
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.100"* @_ZNSt3__27forwardINS_14default_deleteIN5draco26AttributesDecoderInterfaceEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.100"* nonnull align 1 dereferenceable(1) %call2) #9
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %this1, i32 0, i32 0
  %call4 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.100"* @_ZNSt3__217__compressed_pairIPN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.97"* %__ptr_) #9
  ret %"class.std::__2::unique_ptr.96"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8__appendEm(%"class.std::__2::vector.94"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  %__n.addr = alloca i32, align 4
  %__a = alloca %"class.std::__2::allocator.104"*, align 4
  %__v = alloca %"struct.std::__2::__split_buffer", align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"** @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE9__end_capEv(%"class.std::__2::__vector_base.95"* %0) #9
  %1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %call, align 4
  %2 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %2, i32 0, i32 1
  %3 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__end_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.std::__2::unique_ptr.96"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.std::__2::unique_ptr.96"* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  %4 = load i32, i32* %__n.addr, align 4
  %cmp = icmp uge i32 %sub.ptr.div, %4
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %5 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE18__construct_at_endEm(%"class.std::__2::vector.94"* %this1, i32 %5)
  br label %if.end

if.else:                                          ; preds = %entry
  %6 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base.95"* %6) #9
  store %"class.std::__2::allocator.104"* %call2, %"class.std::__2::allocator.104"** %__a, align 4
  %call3 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.94"* %this1) #9
  %7 = load i32, i32* %__n.addr, align 4
  %add = add i32 %call3, %7
  %call4 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE11__recommendEm(%"class.std::__2::vector.94"* %this1, i32 %add)
  %call5 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.94"* %this1) #9
  %8 = load %"class.std::__2::allocator.104"*, %"class.std::__2::allocator.104"** %__a, align 4
  %call6 = call %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEEC2EmmS9_(%"struct.std::__2::__split_buffer"* %__v, i32 %call4, i32 %call5, %"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %8)
  %9 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE18__construct_at_endEm(%"struct.std::__2::__split_buffer"* %__v, i32 %9)
  call void @_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS6_RS8_EE(%"class.std::__2::vector.94"* %this1, %"struct.std::__2::__split_buffer"* nonnull align 4 dereferenceable(20) %__v)
  %call7 = call %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEED2Ev(%"struct.std::__2::__split_buffer"* %__v) #9
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__destruct_at_endEPS6_(%"class.std::__2::vector.94"* %this, %"class.std::__2::unique_ptr.96"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  %__new_last.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  %__old_size = alloca i32, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.96"* %__new_last, %"class.std::__2::unique_ptr.96"** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__new_last.addr, align 4
  call void @_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE27__invalidate_iterators_pastEPS6_(%"class.std::__2::vector.94"* %this1, %"class.std::__2::unique_ptr.96"* %0)
  %call = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.94"* %this1) #9
  store i32 %call, i32* %__old_size, align 4
  %1 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %2 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__new_last.addr, align 4
  call void @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__destruct_at_endEPS6_(%"class.std::__2::__vector_base.95"* %1, %"class.std::__2::unique_ptr.96"* %2) #9
  %3 = load i32, i32* %__old_size, align 4
  call void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__annotate_shrinkEm(%"class.std::__2::vector.94"* %this1, i32 %3) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"** @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE9__end_capEv(%"class.std::__2::__vector_base.95"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.95"*, align 4
  store %"class.std::__2::__vector_base.95"* %this, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.95"*, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"** @_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.101"* %__end_cap_) #9
  ret %"class.std::__2::unique_ptr.96"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE18__construct_at_endEm(%"class.std::__2::vector.94"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  %__n.addr = alloca i32, align 4
  %__tx = alloca %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction", align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %call = call %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE21_ConstructTransactionC2ERS9_m(%"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction"* %__tx, %"class.std::__2::vector.94"* nonnull align 4 dereferenceable(12) %this1, i32 %0)
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__pos_, align 4
  %__new_end_ = getelementptr inbounds %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction"* %__tx, i32 0, i32 2
  %2 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__new_end_, align 4
  %cmp = icmp ne %"class.std::__2::unique_ptr.96"* %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base.95"* %3) #9
  %__pos_3 = getelementptr inbounds %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %4 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__pos_3, align 4
  %call4 = call %"class.std::__2::unique_ptr.96"* @_ZNSt3__212__to_addressINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEEPT_S8_(%"class.std::__2::unique_ptr.96"* %4) #9
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE9constructIS7_JEEEvRS8_PT_DpOT0_(%"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %call2, %"class.std::__2::unique_ptr.96"* %call4)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %__pos_5 = getelementptr inbounds %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %5 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__pos_5, align 4
  %incdec.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %5, i32 1
  store %"class.std::__2::unique_ptr.96"* %incdec.ptr, %"class.std::__2::unique_ptr.96"** %__pos_5, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call6 = call %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction"* %__tx) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base.95"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.95"*, align 4
  store %"class.std::__2::__vector_base.95"* %this, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.95"*, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE6secondEv(%"class.std::__2::__compressed_pair.101"* %__end_cap_) #9
  ret %"class.std::__2::allocator.104"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE11__recommendEm(%"class.std::__2::vector.94"* %this, i32 %__new_size) #0 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  %__new_size.addr = alloca i32, align 4
  %__ms = alloca i32, align 4
  %__cap = alloca i32, align 4
  %ref.tmp = alloca i32, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  store i32 %__new_size, i32* %__new_size.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8max_sizeEv(%"class.std::__2::vector.94"* %this1) #9
  store i32 %call, i32* %__ms, align 4
  %0 = load i32, i32* %__new_size.addr, align 4
  %1 = load i32, i32* %__ms, align 4
  %cmp = icmp ugt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base_common"*
  call void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"* %2) #11
  unreachable

if.end:                                           ; preds = %entry
  %call2 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector.94"* %this1) #9
  store i32 %call2, i32* %__cap, align 4
  %3 = load i32, i32* %__cap, align 4
  %4 = load i32, i32* %__ms, align 4
  %div = udiv i32 %4, 2
  %cmp3 = icmp uge i32 %3, %div
  br i1 %cmp3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %if.end
  %5 = load i32, i32* %__ms, align 4
  store i32 %5, i32* %retval, align 4
  br label %return

if.end5:                                          ; preds = %if.end
  %6 = load i32, i32* %__cap, align 4
  %mul = mul i32 2, %6
  store i32 %mul, i32* %ref.tmp, align 4
  %call6 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %__new_size.addr)
  %7 = load i32, i32* %call6, align 4
  store i32 %7, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end5, %if.then4
  %8 = load i32, i32* %retval, align 4
  ret i32 %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEEC2EmmS9_(%"struct.std::__2::__split_buffer"* returned %this, i32 %__cap, i32 %__start, %"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %__a) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"struct.std::__2::__split_buffer"*, align 4
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__cap.addr = alloca i32, align 4
  %__start.addr = alloca i32, align 4
  %__a.addr = alloca %"class.std::__2::allocator.104"*, align 4
  %ref.tmp = alloca i8*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store i32 %__cap, i32* %__cap.addr, align 4
  store i32 %__start, i32* %__start.addr, align 4
  store %"class.std::__2::allocator.104"* %__a, %"class.std::__2::allocator.104"** %__a.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer"* %this1, %"struct.std::__2::__split_buffer"** %retval, align 4
  %0 = bitcast %"struct.std::__2::__split_buffer"* %this1 to %"class.std::__2::__split_buffer_common"*
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  store i8* null, i8** %ref.tmp, align 4
  %1 = load %"class.std::__2::allocator.104"*, %"class.std::__2::allocator.104"** %__a.addr, align 4
  %call = call %"class.std::__2::__compressed_pair.136"* @_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEEC2IDnSA_EEOT_OT0_(%"class.std::__2::__compressed_pair.136"* %__end_cap_, i8** nonnull align 4 dereferenceable(4) %ref.tmp, %"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %1)
  %2 = load i32, i32* %__cap.addr, align 4
  %cmp = icmp ne i32 %2, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE7__allocEv(%"struct.std::__2::__split_buffer"* %this1) #9
  %3 = load i32, i32* %__cap.addr, align 4
  %call3 = call %"class.std::__2::unique_ptr.96"* @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE8allocateERS8_m(%"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %call2, i32 %3)
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %"class.std::__2::unique_ptr.96"* [ %call3, %cond.true ], [ null, %cond.false ]
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  store %"class.std::__2::unique_ptr.96"* %cond, %"class.std::__2::unique_ptr.96"** %__first_, align 4
  %__first_4 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %4 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__first_4, align 4
  %5 = load i32, i32* %__start.addr, align 4
  %add.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %4, i32 %5
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 2
  store %"class.std::__2::unique_ptr.96"* %add.ptr, %"class.std::__2::unique_ptr.96"** %__end_, align 4
  %__begin_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 1
  store %"class.std::__2::unique_ptr.96"* %add.ptr, %"class.std::__2::unique_ptr.96"** %__begin_, align 4
  %__first_5 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %6 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__first_5, align 4
  %7 = load i32, i32* %__cap.addr, align 4
  %add.ptr6 = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %6, i32 %7
  %call7 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"** @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this1) #9
  store %"class.std::__2::unique_ptr.96"* %add.ptr6, %"class.std::__2::unique_ptr.96"** %call7, align 4
  %8 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %retval, align 4
  ret %"struct.std::__2::__split_buffer"* %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE18__construct_at_endEm(%"struct.std::__2::__split_buffer"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__n.addr = alloca i32, align 4
  %__tx = alloca %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction", align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 2
  %0 = load i32, i32* %__n.addr, align 4
  %call = call %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE21_ConstructTransactionC2EPPS6_m(%"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction"* %__tx, %"class.std::__2::unique_ptr.96"** %__end_, i32 %0) #9
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %__pos_ = getelementptr inbounds %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction"* %__tx, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__pos_, align 4
  %__end_2 = getelementptr inbounds %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %2 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__end_2, align 4
  %cmp = icmp ne %"class.std::__2::unique_ptr.96"* %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %call3 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE7__allocEv(%"struct.std::__2::__split_buffer"* %this1) #9
  %__pos_4 = getelementptr inbounds %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction"* %__tx, i32 0, i32 0
  %3 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__pos_4, align 4
  %call5 = call %"class.std::__2::unique_ptr.96"* @_ZNSt3__212__to_addressINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEEPT_S8_(%"class.std::__2::unique_ptr.96"* %3) #9
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE9constructIS7_JEEEvRS8_PT_DpOT0_(%"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %call3, %"class.std::__2::unique_ptr.96"* %call5)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %__pos_6 = getelementptr inbounds %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction"* %__tx, i32 0, i32 0
  %4 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__pos_6, align 4
  %incdec.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %4, i32 1
  store %"class.std::__2::unique_ptr.96"* %incdec.ptr, %"class.std::__2::unique_ptr.96"** %__pos_6, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call7 = call %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction"* %__tx) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS6_RS8_EE(%"class.std::__2::vector.94"* %this, %"struct.std::__2::__split_buffer"* nonnull align 4 dereferenceable(20) %__v) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  %__v.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer"* %__v, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  call void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__annotate_deleteEv(%"class.std::__2::vector.94"* %this1) #9
  %0 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base.95"* %0) #9
  %1 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %1, i32 0, i32 0
  %2 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__begin_, align 4
  %3 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %3, i32 0, i32 1
  %4 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__end_, align 4
  %5 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__begin_2 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %5, i32 0, i32 1
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE46__construct_backward_with_exception_guaranteesIPS7_EEvRS8_T_SD_RSD_(%"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %call, %"class.std::__2::unique_ptr.96"* %2, %"class.std::__2::unique_ptr.96"* %4, %"class.std::__2::unique_ptr.96"** nonnull align 4 dereferenceable(4) %__begin_2)
  %6 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %__begin_3 = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %6, i32 0, i32 0
  %7 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__begin_4 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %7, i32 0, i32 1
  call void @_ZNSt3__24swapIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS9_EE5valueEvE4typeERS9_SC_(%"class.std::__2::unique_ptr.96"** nonnull align 4 dereferenceable(4) %__begin_3, %"class.std::__2::unique_ptr.96"** nonnull align 4 dereferenceable(4) %__begin_4) #9
  %8 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %__end_5 = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %8, i32 0, i32 1
  %9 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__end_6 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %9, i32 0, i32 2
  call void @_ZNSt3__24swapIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS9_EE5valueEvE4typeERS9_SC_(%"class.std::__2::unique_ptr.96"** nonnull align 4 dereferenceable(4) %__end_5, %"class.std::__2::unique_ptr.96"** nonnull align 4 dereferenceable(4) %__end_6) #9
  %10 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %call7 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"** @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE9__end_capEv(%"class.std::__2::__vector_base.95"* %10) #9
  %11 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %call8 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"** @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE9__end_capEv(%"struct.std::__2::__split_buffer"* %11) #9
  call void @_ZNSt3__24swapIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS9_EE5valueEvE4typeERS9_SC_(%"class.std::__2::unique_ptr.96"** nonnull align 4 dereferenceable(4) %call7, %"class.std::__2::unique_ptr.96"** nonnull align 4 dereferenceable(4) %call8) #9
  %12 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__begin_9 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %12, i32 0, i32 1
  %13 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__begin_9, align 4
  %14 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %14, i32 0, i32 0
  store %"class.std::__2::unique_ptr.96"* %13, %"class.std::__2::unique_ptr.96"** %__first_, align 4
  %call10 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.94"* %this1) #9
  call void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE14__annotate_newEm(%"class.std::__2::vector.94"* %this1, i32 %call10) #9
  call void @_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector.94"* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEED2Ev(%"struct.std::__2::__split_buffer"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"struct.std::__2::__split_buffer"*, align 4
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer"* %this1, %"struct.std::__2::__split_buffer"** %retval, align 4
  call void @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE5clearEv(%"struct.std::__2::__split_buffer"* %this1) #9
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__first_, align 4
  %tobool = icmp ne %"class.std::__2::unique_ptr.96"* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE7__allocEv(%"struct.std::__2::__split_buffer"* %this1) #9
  %__first_2 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__first_2, align 4
  %call3 = call i32 @_ZNKSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE8capacityEv(%"struct.std::__2::__split_buffer"* %this1)
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE10deallocateERS8_PS7_m(%"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %call, %"class.std::__2::unique_ptr.96"* %1, i32 %call3) #9
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %retval, align 4
  ret %"struct.std::__2::__split_buffer"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"** @_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.101"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.101"*, align 4
  store %"class.std::__2::__compressed_pair.101"* %this, %"class.std::__2::__compressed_pair.101"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.101"*, %"class.std::__2::__compressed_pair.101"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.101"* %this1 to %"struct.std::__2::__compressed_pair_elem.102"*
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"** @_ZNSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.102"* %0) #9
  ret %"class.std::__2::unique_ptr.96"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"** @_ZNSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.102"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.102"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.102"* %this, %"struct.std::__2::__compressed_pair_elem.102"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.102"*, %"struct.std::__2::__compressed_pair_elem.102"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.102", %"struct.std::__2::__compressed_pair_elem.102"* %this1, i32 0, i32 0
  ret %"class.std::__2::unique_ptr.96"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE21_ConstructTransactionC2ERS9_m(%"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction"* returned %this, %"class.std::__2::vector.94"* nonnull align 4 dereferenceable(12) %__v, i32 %__n) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction"*, align 4
  %__v.addr = alloca %"class.std::__2::vector.94"*, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction"** %this.addr, align 4
  store %"class.std::__2::vector.94"* %__v, %"class.std::__2::vector.94"** %__v.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction"*, %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction"** %this.addr, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %__v.addr, align 4
  store %"class.std::__2::vector.94"* %0, %"class.std::__2::vector.94"** %__v_, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %__v.addr, align 4
  %2 = bitcast %"class.std::__2::vector.94"* %1 to %"class.std::__2::__vector_base.95"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %2, i32 0, i32 1
  %3 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__end_, align 4
  store %"class.std::__2::unique_ptr.96"* %3, %"class.std::__2::unique_ptr.96"** %__pos_, align 4
  %__new_end_ = getelementptr inbounds %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction"* %this1, i32 0, i32 2
  %4 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %__v.addr, align 4
  %5 = bitcast %"class.std::__2::vector.94"* %4 to %"class.std::__2::__vector_base.95"*
  %__end_2 = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %5, i32 0, i32 1
  %6 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__end_2, align 4
  %7 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %6, i32 %7
  store %"class.std::__2::unique_ptr.96"* %add.ptr, %"class.std::__2::unique_ptr.96"** %__new_end_, align 4
  ret %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE9constructIS7_JEEEvRS8_PT_DpOT0_(%"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %__a, %"class.std::__2::unique_ptr.96"* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.104"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_construct", align 1
  store %"class.std::__2::allocator.104"* %__a, %"class.std::__2::allocator.104"** %__a.addr, align 4
  store %"class.std::__2::unique_ptr.96"* %__p, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_construct"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.104"*, %"class.std::__2::allocator.104"** %__a.addr, align 4
  %2 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE11__constructIS7_JEEEvNS_17integral_constantIbLb1EEERS8_PT_DpOT0_(%"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %1, %"class.std::__2::unique_ptr.96"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.96"* @_ZNSt3__212__to_addressINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEEPT_S8_(%"class.std::__2::unique_ptr.96"* %__p) #0 comdat {
entry:
  %__p.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  store %"class.std::__2::unique_ptr.96"* %__p, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  ret %"class.std::__2::unique_ptr.96"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction"*, align 4
  store %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction"** %this.addr, align 4
  %this1 = load %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction"*, %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %0 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__pos_, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %__v_, align 4
  %2 = bitcast %"class.std::__2::vector.94"* %1 to %"class.std::__2::__vector_base.95"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %2, i32 0, i32 1
  store %"class.std::__2::unique_ptr.96"* %0, %"class.std::__2::unique_ptr.96"** %__end_, align 4
  ret %"struct.std::__2::vector<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE11__constructIS7_JEEEvNS_17integral_constantIbLb1EEERS8_PT_DpOT0_(%"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %__a, %"class.std::__2::unique_ptr.96"* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.104"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  store %"class.std::__2::allocator.104"* %__a, %"class.std::__2::allocator.104"** %__a.addr, align 4
  store %"class.std::__2::unique_ptr.96"* %__p, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.104"*, %"class.std::__2::allocator.104"** %__a.addr, align 4
  %2 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  call void @_ZNSt3__29allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEE9constructIS6_JEEEvPT_DpOT0_(%"class.std::__2::allocator.104"* %1, %"class.std::__2::unique_ptr.96"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEE9constructIS6_JEEEvPT_DpOT0_(%"class.std::__2::allocator.104"* %this, %"class.std::__2::unique_ptr.96"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.104"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  store %"class.std::__2::allocator.104"* %this, %"class.std::__2::allocator.104"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.96"* %__p, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.104"*, %"class.std::__2::allocator.104"** %this.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  %1 = bitcast %"class.std::__2::unique_ptr.96"* %0 to i8*
  %2 = bitcast i8* %1 to %"class.std::__2::unique_ptr.96"*
  %call = call %"class.std::__2::unique_ptr.96"* @_ZNSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEEC2ILb1EvEEv(%"class.std::__2::unique_ptr.96"* %2) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.96"* @_ZNSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEEC2ILb1EvEEv(%"class.std::__2::unique_ptr.96"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  %ref.tmp = alloca %"class.draco::AttributesDecoderInterface"*, align 4
  %ref.tmp2 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::unique_ptr.96"* %this, %"class.std::__2::unique_ptr.96"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %this1, i32 0, i32 0
  store %"class.draco::AttributesDecoderInterface"* null, %"class.draco::AttributesDecoderInterface"** %ref.tmp, align 4
  %call = call %"class.std::__2::__compressed_pair.97"* @_ZNSt3__217__compressed_pairIPN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEEC2IS3_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.97"* %__ptr_, %"class.draco::AttributesDecoderInterface"** nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  ret %"class.std::__2::unique_ptr.96"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.97"* @_ZNSt3__217__compressed_pairIPN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEEC2IS3_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.97"* returned %this, %"class.draco::AttributesDecoderInterface"** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.97"*, align 4
  %__t1.addr = alloca %"class.draco::AttributesDecoderInterface"**, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.97"* %this, %"class.std::__2::__compressed_pair.97"** %this.addr, align 4
  store %"class.draco::AttributesDecoderInterface"** %__t1, %"class.draco::AttributesDecoderInterface"*** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.97"*, %"class.std::__2::__compressed_pair.97"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.97"* %this1 to %"struct.std::__2::__compressed_pair_elem.98"*
  %1 = load %"class.draco::AttributesDecoderInterface"**, %"class.draco::AttributesDecoderInterface"*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributesDecoderInterface"** @_ZNSt3__27forwardIPN5draco26AttributesDecoderInterfaceEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::AttributesDecoderInterface"** nonnull align 4 dereferenceable(4) %1) #9
  %call2 = call %"struct.std::__2::__compressed_pair_elem.98"* @_ZNSt3__222__compressed_pair_elemIPN5draco26AttributesDecoderInterfaceELi0ELb0EEC2IS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.98"* %0, %"class.draco::AttributesDecoderInterface"** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.97"* %this1 to %"struct.std::__2::__compressed_pair_elem.99"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #9
  %call4 = call %"struct.std::__2::__compressed_pair_elem.99"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco26AttributesDecoderInterfaceEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.99"* %2)
  ret %"class.std::__2::__compressed_pair.97"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::AttributesDecoderInterface"** @_ZNSt3__27forwardIPN5draco26AttributesDecoderInterfaceEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::AttributesDecoderInterface"** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.draco::AttributesDecoderInterface"**, align 4
  store %"class.draco::AttributesDecoderInterface"** %__t, %"class.draco::AttributesDecoderInterface"*** %__t.addr, align 4
  %0 = load %"class.draco::AttributesDecoderInterface"**, %"class.draco::AttributesDecoderInterface"*** %__t.addr, align 4
  ret %"class.draco::AttributesDecoderInterface"** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.98"* @_ZNSt3__222__compressed_pair_elemIPN5draco26AttributesDecoderInterfaceELi0ELb0EEC2IS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.98"* returned %this, %"class.draco::AttributesDecoderInterface"** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.98"*, align 4
  %__u.addr = alloca %"class.draco::AttributesDecoderInterface"**, align 4
  store %"struct.std::__2::__compressed_pair_elem.98"* %this, %"struct.std::__2::__compressed_pair_elem.98"** %this.addr, align 4
  store %"class.draco::AttributesDecoderInterface"** %__u, %"class.draco::AttributesDecoderInterface"*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.98"*, %"struct.std::__2::__compressed_pair_elem.98"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.98", %"struct.std::__2::__compressed_pair_elem.98"* %this1, i32 0, i32 0
  %0 = load %"class.draco::AttributesDecoderInterface"**, %"class.draco::AttributesDecoderInterface"*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributesDecoderInterface"** @_ZNSt3__27forwardIPN5draco26AttributesDecoderInterfaceEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::AttributesDecoderInterface"** nonnull align 4 dereferenceable(4) %0) #9
  %1 = load %"class.draco::AttributesDecoderInterface"*, %"class.draco::AttributesDecoderInterface"** %call, align 4
  store %"class.draco::AttributesDecoderInterface"* %1, %"class.draco::AttributesDecoderInterface"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.98"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  store %"struct.std::__2::__default_init_tag"* %__t, %"struct.std::__2::__default_init_tag"** %__t.addr, align 4
  %0 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t.addr, align 4
  ret %"struct.std::__2::__default_init_tag"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.99"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco26AttributesDecoderInterfaceEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.99"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.99"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.99"* %this, %"struct.std::__2::__compressed_pair_elem.99"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.99"*, %"struct.std::__2::__compressed_pair_elem.99"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.99"* %this1 to %"struct.std::__2::default_delete.100"*
  ret %"struct.std::__2::__compressed_pair_elem.99"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE6secondEv(%"class.std::__2::__compressed_pair.101"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.101"*, align 4
  store %"class.std::__2::__compressed_pair.101"* %this, %"class.std::__2::__compressed_pair.101"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.101"*, %"class.std::__2::__compressed_pair.101"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.101"* %this1 to %"struct.std::__2::__compressed_pair_elem.103"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.103"* %0) #9
  ret %"class.std::__2::allocator.104"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.103"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.103"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.103"* %this, %"struct.std::__2::__compressed_pair_elem.103"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.103"*, %"struct.std::__2::__compressed_pair_elem.103"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.103"* %this1 to %"class.std::__2::allocator.104"*
  ret %"class.std::__2::allocator.104"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8max_sizeEv(%"class.std::__2::vector.94"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp3 = alloca i32, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base.95"* %0) #9
  %call2 = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE8max_sizeERKS8_(%"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %call) #9
  store i32 %call2, i32* %ref.tmp, align 4
  %call4 = call i32 @_ZNSt3__214numeric_limitsIlE3maxEv() #9
  store i32 %call4, i32* %ref.tmp3, align 4
  %call5 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %1 = load i32, i32* %call5, align 4
  ret i32 %1
}

; Function Attrs: noreturn
declare void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"*) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector.94"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %call = call i32 @_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::__vector_base.95"* %0) #9
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::__less", align 1
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__a.addr, align 4
  %1 = load i32*, i32** %__b.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::__less", align 1
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__a.addr, align 4
  %1 = load i32*, i32** %__b.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE8max_sizeERKS8_(%"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.104"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_max_size", align 1
  store %"class.std::__2::allocator.104"* %__a, %"class.std::__2::allocator.104"** %__a.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_max_size"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.104"*, %"class.std::__2::allocator.104"** %__a.addr, align 4
  %call = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS8_(%"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %1) #9
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base.95"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.95"*, align 4
  store %"class.std::__2::__vector_base.95"* %this, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.95"*, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNKSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE6secondEv(%"class.std::__2::__compressed_pair.101"* %__end_cap_) #9
  ret %"class.std::__2::allocator.104"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__214numeric_limitsIlE3maxEv() #0 comdat {
entry:
  %call = call i32 @_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv() #9
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__comp = alloca %"struct.std::__2::__less", align 1
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__b.addr, align 4
  %1 = load i32*, i32** %__a.addr, align 4
  %call = call zeroext i1 @_ZNKSt3__26__lessImmEclERKmS3_(%"struct.std::__2::__less"* %__comp, i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  br i1 %call, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32*, i32** %__b.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %3 = load i32*, i32** %__a.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi i32* [ %2, %cond.true ], [ %3, %cond.false ]
  ret i32* %cond-lvalue
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__26__lessImmEclERKmS3_(%"struct.std::__2::__less"* %this, i32* nonnull align 4 dereferenceable(4) %__x, i32* nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__less"*, align 4
  %__x.addr = alloca i32*, align 4
  %__y.addr = alloca i32*, align 4
  store %"struct.std::__2::__less"* %this, %"struct.std::__2::__less"** %this.addr, align 4
  store i32* %__x, i32** %__x.addr, align 4
  store i32* %__y, i32** %__y.addr, align 4
  %this1 = load %"struct.std::__2::__less"*, %"struct.std::__2::__less"** %this.addr, align 4
  %0 = load i32*, i32** %__x.addr, align 4
  %1 = load i32, i32* %0, align 4
  %2 = load i32*, i32** %__y.addr, align 4
  %3 = load i32, i32* %2, align 4
  %cmp = icmp ult i32 %1, %3
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS8_(%"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.104"*, align 4
  store %"class.std::__2::allocator.104"* %__a, %"class.std::__2::allocator.104"** %__a.addr, align 4
  %1 = load %"class.std::__2::allocator.104"*, %"class.std::__2::allocator.104"** %__a.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEE8max_sizeEv(%"class.std::__2::allocator.104"* %1) #9
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__29allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEE8max_sizeEv(%"class.std::__2::allocator.104"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.104"*, align 4
  store %"class.std::__2::allocator.104"* %this, %"class.std::__2::allocator.104"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.104"*, %"class.std::__2::allocator.104"** %this.addr, align 4
  ret i32 1073741823
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNKSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE6secondEv(%"class.std::__2::__compressed_pair.101"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.101"*, align 4
  store %"class.std::__2::__compressed_pair.101"* %this, %"class.std::__2::__compressed_pair.101"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.101"*, %"class.std::__2::__compressed_pair.101"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.101"* %this1 to %"struct.std::__2::__compressed_pair_elem.103"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.103"* %0) #9
  ret %"class.std::__2::allocator.104"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.103"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.103"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.103"* %this, %"struct.std::__2::__compressed_pair_elem.103"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.103"*, %"struct.std::__2::__compressed_pair_elem.103"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.103"* %this1 to %"class.std::__2::allocator.104"*
  ret %"class.std::__2::allocator.104"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv() #0 comdat {
entry:
  ret i32 2147483647
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::__vector_base.95"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.95"*, align 4
  store %"class.std::__2::__vector_base.95"* %this, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.95"*, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"** @_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE9__end_capEv(%"class.std::__2::__vector_base.95"* %this1) #9
  %0 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.std::__2::unique_ptr.96"* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.std::__2::unique_ptr.96"* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"** @_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE9__end_capEv(%"class.std::__2::__vector_base.95"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.95"*, align 4
  store %"class.std::__2::__vector_base.95"* %this, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.95"*, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"** @_ZNKSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.101"* %__end_cap_) #9
  ret %"class.std::__2::unique_ptr.96"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"** @_ZNKSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.101"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.101"*, align 4
  store %"class.std::__2::__compressed_pair.101"* %this, %"class.std::__2::__compressed_pair.101"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.101"*, %"class.std::__2::__compressed_pair.101"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.101"* %this1 to %"struct.std::__2::__compressed_pair_elem.102"*
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"** @_ZNKSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.102"* %0) #9
  ret %"class.std::__2::unique_ptr.96"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"** @_ZNKSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.102"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.102"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.102"* %this, %"struct.std::__2::__compressed_pair_elem.102"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.102"*, %"struct.std::__2::__compressed_pair_elem.102"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.102", %"struct.std::__2::__compressed_pair_elem.102"* %this1, i32 0, i32 0
  ret %"class.std::__2::unique_ptr.96"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__comp = alloca %"struct.std::__2::__less", align 1
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__a.addr, align 4
  %1 = load i32*, i32** %__b.addr, align 4
  %call = call zeroext i1 @_ZNKSt3__26__lessImmEclERKmS3_(%"struct.std::__2::__less"* %__comp, i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  br i1 %call, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32*, i32** %__b.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %3 = load i32*, i32** %__a.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi i32* [ %2, %cond.true ], [ %3, %cond.false ]
  ret i32* %cond-lvalue
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.136"* @_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEEC2IDnSA_EEOT_OT0_(%"class.std::__2::__compressed_pair.136"* returned %this, i8** nonnull align 4 dereferenceable(4) %__t1, %"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.136"*, align 4
  %__t1.addr = alloca i8**, align 4
  %__t2.addr = alloca %"class.std::__2::allocator.104"*, align 4
  store %"class.std::__2::__compressed_pair.136"* %this, %"class.std::__2::__compressed_pair.136"** %this.addr, align 4
  store i8** %__t1, i8*** %__t1.addr, align 4
  store %"class.std::__2::allocator.104"* %__t2, %"class.std::__2::allocator.104"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.136"*, %"class.std::__2::__compressed_pair.136"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.136"* %this1 to %"struct.std::__2::__compressed_pair_elem.102"*
  %1 = load i8**, i8*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %1) #9
  %call2 = call %"struct.std::__2::__compressed_pair_elem.102"* @_ZNSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEELi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.102"* %0, i8** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.136"* %this1 to i8*
  %3 = getelementptr inbounds i8, i8* %2, i32 4
  %4 = bitcast i8* %3 to %"struct.std::__2::__compressed_pair_elem.137"*
  %5 = load %"class.std::__2::allocator.104"*, %"class.std::__2::allocator.104"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNSt3__27forwardIRNS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEEEOT_RNS_16remove_referenceISA_E4typeE(%"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %5) #9
  %call4 = call %"struct.std::__2::__compressed_pair_elem.137"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEELi1ELb0EEC2IS9_vEEOT_(%"struct.std::__2::__compressed_pair_elem.137"* %4, %"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::__compressed_pair.136"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.96"* @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE8allocateERS8_m(%"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %__a, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.104"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.104"* %__a, %"class.std::__2::allocator.104"** %__a.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.104"*, %"class.std::__2::allocator.104"** %__a.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call %"class.std::__2::unique_ptr.96"* @_ZNSt3__29allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEE8allocateEmPKv(%"class.std::__2::allocator.104"* %0, i32 %1, i8* null)
  ret %"class.std::__2::unique_ptr.96"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE7__allocEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE6secondEv(%"class.std::__2::__compressed_pair.136"* %__end_cap_) #9
  ret %"class.std::__2::allocator.104"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"** @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"** @_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.136"* %__end_cap_) #9
  ret %"class.std::__2::unique_ptr.96"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca i8**, align 4
  store i8** %__t, i8*** %__t.addr, align 4
  %0 = load i8**, i8*** %__t.addr, align 4
  ret i8** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.102"* @_ZNSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEELi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.102"* returned %this, i8** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.102"*, align 4
  %__u.addr = alloca i8**, align 4
  store %"struct.std::__2::__compressed_pair_elem.102"* %this, %"struct.std::__2::__compressed_pair_elem.102"** %this.addr, align 4
  store i8** %__u, i8*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.102"*, %"struct.std::__2::__compressed_pair_elem.102"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.102", %"struct.std::__2::__compressed_pair_elem.102"* %this1, i32 0, i32 0
  %0 = load i8**, i8*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %0) #9
  store %"class.std::__2::unique_ptr.96"* null, %"class.std::__2::unique_ptr.96"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.102"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNSt3__27forwardIRNS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEEEOT_RNS_16remove_referenceISA_E4typeE(%"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::allocator.104"*, align 4
  store %"class.std::__2::allocator.104"* %__t, %"class.std::__2::allocator.104"** %__t.addr, align 4
  %0 = load %"class.std::__2::allocator.104"*, %"class.std::__2::allocator.104"** %__t.addr, align 4
  ret %"class.std::__2::allocator.104"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.137"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEELi1ELb0EEC2IS9_vEEOT_(%"struct.std::__2::__compressed_pair_elem.137"* returned %this, %"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.137"*, align 4
  %__u.addr = alloca %"class.std::__2::allocator.104"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.137"* %this, %"struct.std::__2::__compressed_pair_elem.137"** %this.addr, align 4
  store %"class.std::__2::allocator.104"* %__u, %"class.std::__2::allocator.104"** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.137"*, %"struct.std::__2::__compressed_pair_elem.137"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.137", %"struct.std::__2::__compressed_pair_elem.137"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::allocator.104"*, %"class.std::__2::allocator.104"** %__u.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNSt3__27forwardIRNS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEEEOT_RNS_16remove_referenceISA_E4typeE(%"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %0) #9
  store %"class.std::__2::allocator.104"* %call, %"class.std::__2::allocator.104"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.137"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.96"* @_ZNSt3__29allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEE8allocateEmPKv(%"class.std::__2::allocator.104"* %this, i32 %__n, i8* %0) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.104"*, align 4
  %__n.addr = alloca i32, align 4
  %.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.104"* %this, %"class.std::__2::allocator.104"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::allocator.104"*, %"class.std::__2::allocator.104"** %this.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEE8max_sizeEv(%"class.std::__2::allocator.104"* %this1) #9
  %cmp = icmp ugt i32 %1, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__220__throw_length_errorEPKc(i8* getelementptr inbounds ([68 x i8], [68 x i8]* @.str, i32 0, i32 0)) #11
  unreachable

if.end:                                           ; preds = %entry
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  %call2 = call i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %mul, i32 4)
  %3 = bitcast i8* %call2 to %"class.std::__2::unique_ptr.96"*
  ret %"class.std::__2::unique_ptr.96"* %3
}

; Function Attrs: noinline noreturn nounwind optnone
define linkonce_odr hidden void @_ZNSt3__220__throw_length_errorEPKc(i8* %__msg) #4 comdat {
entry:
  %__msg.addr = alloca i8*, align 4
  store i8* %__msg, i8** %__msg.addr, align 4
  call void @abort() #11
  unreachable
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %__size, i32 %__align) #0 comdat {
entry:
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i32, i32* %__size.addr, align 4
  %call = call noalias nonnull i8* @_Znwm(i32 %0) #8
  ret i8* %call
}

; Function Attrs: noreturn
declare void @abort() #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE6secondEv(%"class.std::__2::__compressed_pair.136"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.136"*, align 4
  store %"class.std::__2::__compressed_pair.136"* %this, %"class.std::__2::__compressed_pair.136"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.136"*, %"class.std::__2::__compressed_pair.136"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.136"* %this1 to i8*
  %add.ptr = getelementptr inbounds i8, i8* %0, i32 4
  %1 = bitcast i8* %add.ptr to %"struct.std::__2::__compressed_pair_elem.137"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.137"* %1) #9
  ret %"class.std::__2::allocator.104"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.137"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.137"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.137"* %this, %"struct.std::__2::__compressed_pair_elem.137"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.137"*, %"struct.std::__2::__compressed_pair_elem.137"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.137", %"struct.std::__2::__compressed_pair_elem.137"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::allocator.104"*, %"class.std::__2::allocator.104"** %__value_, align 4
  ret %"class.std::__2::allocator.104"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"** @_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.136"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.136"*, align 4
  store %"class.std::__2::__compressed_pair.136"* %this, %"class.std::__2::__compressed_pair.136"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.136"*, %"class.std::__2::__compressed_pair.136"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.136"* %this1 to %"struct.std::__2::__compressed_pair_elem.102"*
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"** @_ZNSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.102"* %0) #9
  ret %"class.std::__2::unique_ptr.96"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE21_ConstructTransactionC2EPPS6_m(%"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction"* returned %this, %"class.std::__2::unique_ptr.96"** %__p, i32 %__n) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.96"**, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction"* %this, %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.96"** %__p, %"class.std::__2::unique_ptr.96"*** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction"*, %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::unique_ptr.96"**, %"class.std::__2::unique_ptr.96"*** %__p.addr, align 4
  %1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %0, align 4
  store %"class.std::__2::unique_ptr.96"* %1, %"class.std::__2::unique_ptr.96"** %__pos_, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction"* %this1, i32 0, i32 1
  %2 = load %"class.std::__2::unique_ptr.96"**, %"class.std::__2::unique_ptr.96"*** %__p.addr, align 4
  %3 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %2, align 4
  %4 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %3, i32 %4
  store %"class.std::__2::unique_ptr.96"* %add.ptr, %"class.std::__2::unique_ptr.96"** %__end_, align 4
  %__dest_ = getelementptr inbounds %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction"* %this1, i32 0, i32 2
  %5 = load %"class.std::__2::unique_ptr.96"**, %"class.std::__2::unique_ptr.96"*** %__p.addr, align 4
  store %"class.std::__2::unique_ptr.96"** %5, %"class.std::__2::unique_ptr.96"*** %__dest_, align 4
  ret %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction"*, align 4
  store %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction"* %this, %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction"*, %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__pos_, align 4
  %__dest_ = getelementptr inbounds %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction"* %this1, i32 0, i32 2
  %1 = load %"class.std::__2::unique_ptr.96"**, %"class.std::__2::unique_ptr.96"*** %__dest_, align 4
  store %"class.std::__2::unique_ptr.96"* %0, %"class.std::__2::unique_ptr.96"** %1, align 4
  ret %"struct.std::__2::__split_buffer<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>, std::__2::allocator<std::__2::unique_ptr<draco::AttributesDecoderInterface, std::__2::default_delete<draco::AttributesDecoderInterface>>> &>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__annotate_deleteEv(%"class.std::__2::vector.94"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  %call = call %"class.std::__2::unique_ptr.96"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.94"* %this1) #9
  %0 = bitcast %"class.std::__2::unique_ptr.96"* %call to i8*
  %call2 = call %"class.std::__2::unique_ptr.96"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.94"* %this1) #9
  %call3 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector.94"* %this1) #9
  %add.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %call2, i32 %call3
  %1 = bitcast %"class.std::__2::unique_ptr.96"* %add.ptr to i8*
  %call4 = call %"class.std::__2::unique_ptr.96"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.94"* %this1) #9
  %call5 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.94"* %this1) #9
  %add.ptr6 = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %call4, i32 %call5
  %2 = bitcast %"class.std::__2::unique_ptr.96"* %add.ptr6 to i8*
  %call7 = call %"class.std::__2::unique_ptr.96"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.94"* %this1) #9
  %call8 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector.94"* %this1) #9
  %add.ptr9 = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %call7, i32 %call8
  %3 = bitcast %"class.std::__2::unique_ptr.96"* %add.ptr9 to i8*
  call void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE31__annotate_contiguous_containerEPKvSB_SB_SB_(%"class.std::__2::vector.94"* %this1, i8* %0, i8* %1, i8* %2, i8* %3) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE46__construct_backward_with_exception_guaranteesIPS7_EEvRS8_T_SD_RSD_(%"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %__a, %"class.std::__2::unique_ptr.96"* %__begin1, %"class.std::__2::unique_ptr.96"* %__end1, %"class.std::__2::unique_ptr.96"** nonnull align 4 dereferenceable(4) %__end2) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.104"*, align 4
  %__begin1.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  %__end1.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  %__end2.addr = alloca %"class.std::__2::unique_ptr.96"**, align 4
  store %"class.std::__2::allocator.104"* %__a, %"class.std::__2::allocator.104"** %__a.addr, align 4
  store %"class.std::__2::unique_ptr.96"* %__begin1, %"class.std::__2::unique_ptr.96"** %__begin1.addr, align 4
  store %"class.std::__2::unique_ptr.96"* %__end1, %"class.std::__2::unique_ptr.96"** %__end1.addr, align 4
  store %"class.std::__2::unique_ptr.96"** %__end2, %"class.std::__2::unique_ptr.96"*** %__end2.addr, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %0 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__end1.addr, align 4
  %1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__begin1.addr, align 4
  %cmp = icmp ne %"class.std::__2::unique_ptr.96"* %0, %1
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %2 = load %"class.std::__2::allocator.104"*, %"class.std::__2::allocator.104"** %__a.addr, align 4
  %3 = load %"class.std::__2::unique_ptr.96"**, %"class.std::__2::unique_ptr.96"*** %__end2.addr, align 4
  %4 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %3, align 4
  %add.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %4, i32 -1
  %call = call %"class.std::__2::unique_ptr.96"* @_ZNSt3__212__to_addressINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEEPT_S8_(%"class.std::__2::unique_ptr.96"* %add.ptr) #9
  %5 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__end1.addr, align 4
  %incdec.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %5, i32 -1
  store %"class.std::__2::unique_ptr.96"* %incdec.ptr, %"class.std::__2::unique_ptr.96"** %__end1.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"* @_ZNSt3__24moveIRNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOS9_(%"class.std::__2::unique_ptr.96"* nonnull align 4 dereferenceable(4) %incdec.ptr) #9
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE9constructIS7_JS7_EEEvRS8_PT_DpOT0_(%"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %2, %"class.std::__2::unique_ptr.96"* %call, %"class.std::__2::unique_ptr.96"* nonnull align 4 dereferenceable(4) %call1)
  %6 = load %"class.std::__2::unique_ptr.96"**, %"class.std::__2::unique_ptr.96"*** %__end2.addr, align 4
  %7 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %6, align 4
  %incdec.ptr2 = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %7, i32 -1
  store %"class.std::__2::unique_ptr.96"* %incdec.ptr2, %"class.std::__2::unique_ptr.96"** %6, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__24swapIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS9_EE5valueEvE4typeERS9_SC_(%"class.std::__2::unique_ptr.96"** nonnull align 4 dereferenceable(4) %__x, %"class.std::__2::unique_ptr.96"** nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %__x.addr = alloca %"class.std::__2::unique_ptr.96"**, align 4
  %__y.addr = alloca %"class.std::__2::unique_ptr.96"**, align 4
  %__t = alloca %"class.std::__2::unique_ptr.96"*, align 4
  store %"class.std::__2::unique_ptr.96"** %__x, %"class.std::__2::unique_ptr.96"*** %__x.addr, align 4
  store %"class.std::__2::unique_ptr.96"** %__y, %"class.std::__2::unique_ptr.96"*** %__y.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.96"**, %"class.std::__2::unique_ptr.96"*** %__x.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"** @_ZNSt3__24moveIRPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOSA_(%"class.std::__2::unique_ptr.96"** nonnull align 4 dereferenceable(4) %0) #9
  %1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %call, align 4
  store %"class.std::__2::unique_ptr.96"* %1, %"class.std::__2::unique_ptr.96"** %__t, align 4
  %2 = load %"class.std::__2::unique_ptr.96"**, %"class.std::__2::unique_ptr.96"*** %__y.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"** @_ZNSt3__24moveIRPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOSA_(%"class.std::__2::unique_ptr.96"** nonnull align 4 dereferenceable(4) %2) #9
  %3 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %call1, align 4
  %4 = load %"class.std::__2::unique_ptr.96"**, %"class.std::__2::unique_ptr.96"*** %__x.addr, align 4
  store %"class.std::__2::unique_ptr.96"* %3, %"class.std::__2::unique_ptr.96"** %4, align 4
  %call2 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"** @_ZNSt3__24moveIRPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOSA_(%"class.std::__2::unique_ptr.96"** nonnull align 4 dereferenceable(4) %__t) #9
  %5 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %call2, align 4
  %6 = load %"class.std::__2::unique_ptr.96"**, %"class.std::__2::unique_ptr.96"*** %__y.addr, align 4
  store %"class.std::__2::unique_ptr.96"* %5, %"class.std::__2::unique_ptr.96"** %6, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE14__annotate_newEm(%"class.std::__2::vector.94"* %this, i32 %__current_size) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  %__current_size.addr = alloca i32, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  store i32 %__current_size, i32* %__current_size.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  %call = call %"class.std::__2::unique_ptr.96"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.94"* %this1) #9
  %0 = bitcast %"class.std::__2::unique_ptr.96"* %call to i8*
  %call2 = call %"class.std::__2::unique_ptr.96"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.94"* %this1) #9
  %call3 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector.94"* %this1) #9
  %add.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %call2, i32 %call3
  %1 = bitcast %"class.std::__2::unique_ptr.96"* %add.ptr to i8*
  %call4 = call %"class.std::__2::unique_ptr.96"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.94"* %this1) #9
  %call5 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector.94"* %this1) #9
  %add.ptr6 = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %call4, i32 %call5
  %2 = bitcast %"class.std::__2::unique_ptr.96"* %add.ptr6 to i8*
  %call7 = call %"class.std::__2::unique_ptr.96"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.94"* %this1) #9
  %3 = load i32, i32* %__current_size.addr, align 4
  %add.ptr8 = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %call7, i32 %3
  %4 = bitcast %"class.std::__2::unique_ptr.96"* %add.ptr8 to i8*
  call void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE31__annotate_contiguous_containerEPKvSB_SB_SB_(%"class.std::__2::vector.94"* %this1, i8* %0, i8* %1, i8* %2, i8* %4) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector.94"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE31__annotate_contiguous_containerEPKvSB_SB_SB_(%"class.std::__2::vector.94"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.96"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.94"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %0, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__begin_, align 4
  %call = call %"class.std::__2::unique_ptr.96"* @_ZNSt3__212__to_addressINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEEPT_S8_(%"class.std::__2::unique_ptr.96"* %1) #9
  ret %"class.std::__2::unique_ptr.96"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE9constructIS7_JS7_EEEvRS8_PT_DpOT0_(%"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %__a, %"class.std::__2::unique_ptr.96"* %__p, %"class.std::__2::unique_ptr.96"* nonnull align 4 dereferenceable(4) %__args) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.104"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  %__args.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_construct.138", align 1
  store %"class.std::__2::allocator.104"* %__a, %"class.std::__2::allocator.104"** %__a.addr, align 4
  store %"class.std::__2::unique_ptr.96"* %__p, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  store %"class.std::__2::unique_ptr.96"* %__args, %"class.std::__2::unique_ptr.96"** %__args.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_construct.138"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.104"*, %"class.std::__2::allocator.104"** %__a.addr, align 4
  %2 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  %3 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"* @_ZNSt3__27forwardINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"class.std::__2::unique_ptr.96"* nonnull align 4 dereferenceable(4) %3) #9
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE11__constructIS7_JS7_EEEvNS_17integral_constantIbLb1EEERS8_PT_DpOT0_(%"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %1, %"class.std::__2::unique_ptr.96"* %2, %"class.std::__2::unique_ptr.96"* nonnull align 4 dereferenceable(4) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE11__constructIS7_JS7_EEEvNS_17integral_constantIbLb1EEERS8_PT_DpOT0_(%"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %__a, %"class.std::__2::unique_ptr.96"* %__p, %"class.std::__2::unique_ptr.96"* nonnull align 4 dereferenceable(4) %__args) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.104"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  %__args.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  store %"class.std::__2::allocator.104"* %__a, %"class.std::__2::allocator.104"** %__a.addr, align 4
  store %"class.std::__2::unique_ptr.96"* %__p, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  store %"class.std::__2::unique_ptr.96"* %__args, %"class.std::__2::unique_ptr.96"** %__args.addr, align 4
  %1 = load %"class.std::__2::allocator.104"*, %"class.std::__2::allocator.104"** %__a.addr, align 4
  %2 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  %3 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"* @_ZNSt3__27forwardINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"class.std::__2::unique_ptr.96"* nonnull align 4 dereferenceable(4) %3) #9
  call void @_ZNSt3__29allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEE9constructIS6_JS6_EEEvPT_DpOT0_(%"class.std::__2::allocator.104"* %1, %"class.std::__2::unique_ptr.96"* %2, %"class.std::__2::unique_ptr.96"* nonnull align 4 dereferenceable(4) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"* @_ZNSt3__27forwardINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"class.std::__2::unique_ptr.96"* nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  store %"class.std::__2::unique_ptr.96"* %__t, %"class.std::__2::unique_ptr.96"** %__t.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__t.addr, align 4
  ret %"class.std::__2::unique_ptr.96"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEE9constructIS6_JS6_EEEvPT_DpOT0_(%"class.std::__2::allocator.104"* %this, %"class.std::__2::unique_ptr.96"* %__p, %"class.std::__2::unique_ptr.96"* nonnull align 4 dereferenceable(4) %__args) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.104"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  %__args.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  store %"class.std::__2::allocator.104"* %this, %"class.std::__2::allocator.104"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.96"* %__p, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  store %"class.std::__2::unique_ptr.96"* %__args, %"class.std::__2::unique_ptr.96"** %__args.addr, align 4
  %this1 = load %"class.std::__2::allocator.104"*, %"class.std::__2::allocator.104"** %this.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  %1 = bitcast %"class.std::__2::unique_ptr.96"* %0 to i8*
  %2 = bitcast i8* %1 to %"class.std::__2::unique_ptr.96"*
  %3 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"* @_ZNSt3__27forwardINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"class.std::__2::unique_ptr.96"* nonnull align 4 dereferenceable(4) %3) #9
  %call2 = call %"class.std::__2::unique_ptr.96"* @_ZNSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEEC2EOS5_(%"class.std::__2::unique_ptr.96"* %2, %"class.std::__2::unique_ptr.96"* nonnull align 4 dereferenceable(4) %call) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.96"* @_ZNSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEEC2EOS5_(%"class.std::__2::unique_ptr.96"* returned %this, %"class.std::__2::unique_ptr.96"* nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  %__u.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  %ref.tmp = alloca %"class.draco::AttributesDecoderInterface"*, align 4
  store %"class.std::__2::unique_ptr.96"* %this, %"class.std::__2::unique_ptr.96"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.96"* %__u, %"class.std::__2::unique_ptr.96"** %__u.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__u.addr, align 4
  %call = call %"class.draco::AttributesDecoderInterface"* @_ZNSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE7releaseEv(%"class.std::__2::unique_ptr.96"* %0) #9
  store %"class.draco::AttributesDecoderInterface"* %call, %"class.draco::AttributesDecoderInterface"** %ref.tmp, align 4
  %1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__u.addr, align 4
  %call2 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.100"* @_ZNSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE11get_deleterEv(%"class.std::__2::unique_ptr.96"* %1) #9
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.100"* @_ZNSt3__27forwardINS_14default_deleteIN5draco26AttributesDecoderInterfaceEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.100"* nonnull align 1 dereferenceable(1) %call2) #9
  %call4 = call %"class.std::__2::__compressed_pair.97"* @_ZNSt3__217__compressed_pairIPN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEEC2IS3_S5_EEOT_OT0_(%"class.std::__2::__compressed_pair.97"* %__ptr_, %"class.draco::AttributesDecoderInterface"** nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::default_delete.100"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::unique_ptr.96"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::AttributesDecoderInterface"* @_ZNSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE7releaseEv(%"class.std::__2::unique_ptr.96"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  %__t = alloca %"class.draco::AttributesDecoderInterface"*, align 4
  store %"class.std::__2::unique_ptr.96"* %this, %"class.std::__2::unique_ptr.96"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributesDecoderInterface"** @_ZNSt3__217__compressed_pairIPN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.97"* %__ptr_) #9
  %0 = load %"class.draco::AttributesDecoderInterface"*, %"class.draco::AttributesDecoderInterface"** %call, align 4
  store %"class.draco::AttributesDecoderInterface"* %0, %"class.draco::AttributesDecoderInterface"** %__t, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::AttributesDecoderInterface"** @_ZNSt3__217__compressed_pairIPN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.97"* %__ptr_2) #9
  store %"class.draco::AttributesDecoderInterface"* null, %"class.draco::AttributesDecoderInterface"** %call3, align 4
  %1 = load %"class.draco::AttributesDecoderInterface"*, %"class.draco::AttributesDecoderInterface"** %__t, align 4
  ret %"class.draco::AttributesDecoderInterface"* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.100"* @_ZNSt3__27forwardINS_14default_deleteIN5draco26AttributesDecoderInterfaceEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.100"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"struct.std::__2::default_delete.100"*, align 4
  store %"struct.std::__2::default_delete.100"* %__t, %"struct.std::__2::default_delete.100"** %__t.addr, align 4
  %0 = load %"struct.std::__2::default_delete.100"*, %"struct.std::__2::default_delete.100"** %__t.addr, align 4
  ret %"struct.std::__2::default_delete.100"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.100"* @_ZNSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE11get_deleterEv(%"class.std::__2::unique_ptr.96"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  store %"class.std::__2::unique_ptr.96"* %this, %"class.std::__2::unique_ptr.96"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %this1, i32 0, i32 0
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.100"* @_ZNSt3__217__compressed_pairIPN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.97"* %__ptr_) #9
  ret %"struct.std::__2::default_delete.100"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.97"* @_ZNSt3__217__compressed_pairIPN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEEC2IS3_S5_EEOT_OT0_(%"class.std::__2::__compressed_pair.97"* returned %this, %"class.draco::AttributesDecoderInterface"** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::default_delete.100"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.97"*, align 4
  %__t1.addr = alloca %"class.draco::AttributesDecoderInterface"**, align 4
  %__t2.addr = alloca %"struct.std::__2::default_delete.100"*, align 4
  store %"class.std::__2::__compressed_pair.97"* %this, %"class.std::__2::__compressed_pair.97"** %this.addr, align 4
  store %"class.draco::AttributesDecoderInterface"** %__t1, %"class.draco::AttributesDecoderInterface"*** %__t1.addr, align 4
  store %"struct.std::__2::default_delete.100"* %__t2, %"struct.std::__2::default_delete.100"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.97"*, %"class.std::__2::__compressed_pair.97"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.97"* %this1 to %"struct.std::__2::__compressed_pair_elem.98"*
  %1 = load %"class.draco::AttributesDecoderInterface"**, %"class.draco::AttributesDecoderInterface"*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributesDecoderInterface"** @_ZNSt3__27forwardIPN5draco26AttributesDecoderInterfaceEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::AttributesDecoderInterface"** nonnull align 4 dereferenceable(4) %1) #9
  %call2 = call %"struct.std::__2::__compressed_pair_elem.98"* @_ZNSt3__222__compressed_pair_elemIPN5draco26AttributesDecoderInterfaceELi0ELb0EEC2IS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.98"* %0, %"class.draco::AttributesDecoderInterface"** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.97"* %this1 to %"struct.std::__2::__compressed_pair_elem.99"*
  %3 = load %"struct.std::__2::default_delete.100"*, %"struct.std::__2::default_delete.100"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.100"* @_ZNSt3__27forwardINS_14default_deleteIN5draco26AttributesDecoderInterfaceEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.100"* nonnull align 1 dereferenceable(1) %3) #9
  %call4 = call %"struct.std::__2::__compressed_pair_elem.99"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco26AttributesDecoderInterfaceEEELi1ELb1EEC2IS4_vEEOT_(%"struct.std::__2::__compressed_pair_elem.99"* %2, %"struct.std::__2::default_delete.100"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::__compressed_pair.97"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::AttributesDecoderInterface"** @_ZNSt3__217__compressed_pairIPN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.97"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.97"*, align 4
  store %"class.std::__2::__compressed_pair.97"* %this, %"class.std::__2::__compressed_pair.97"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.97"*, %"class.std::__2::__compressed_pair.97"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.97"* %this1 to %"struct.std::__2::__compressed_pair_elem.98"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributesDecoderInterface"** @_ZNSt3__222__compressed_pair_elemIPN5draco26AttributesDecoderInterfaceELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.98"* %0) #9
  ret %"class.draco::AttributesDecoderInterface"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::AttributesDecoderInterface"** @_ZNSt3__222__compressed_pair_elemIPN5draco26AttributesDecoderInterfaceELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.98"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.98"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.98"* %this, %"struct.std::__2::__compressed_pair_elem.98"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.98"*, %"struct.std::__2::__compressed_pair_elem.98"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.98", %"struct.std::__2::__compressed_pair_elem.98"* %this1, i32 0, i32 0
  ret %"class.draco::AttributesDecoderInterface"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.100"* @_ZNSt3__217__compressed_pairIPN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.97"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.97"*, align 4
  store %"class.std::__2::__compressed_pair.97"* %this, %"class.std::__2::__compressed_pair.97"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.97"*, %"class.std::__2::__compressed_pair.97"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.97"* %this1 to %"struct.std::__2::__compressed_pair_elem.99"*
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.100"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco26AttributesDecoderInterfaceEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.99"* %0) #9
  ret %"struct.std::__2::default_delete.100"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.100"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco26AttributesDecoderInterfaceEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.99"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.99"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.99"* %this, %"struct.std::__2::__compressed_pair_elem.99"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.99"*, %"struct.std::__2::__compressed_pair_elem.99"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.99"* %this1 to %"struct.std::__2::default_delete.100"*
  ret %"struct.std::__2::default_delete.100"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.99"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco26AttributesDecoderInterfaceEEELi1ELb1EEC2IS4_vEEOT_(%"struct.std::__2::__compressed_pair_elem.99"* returned %this, %"struct.std::__2::default_delete.100"* nonnull align 1 dereferenceable(1) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.99"*, align 4
  %__u.addr = alloca %"struct.std::__2::default_delete.100"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.99"* %this, %"struct.std::__2::__compressed_pair_elem.99"** %this.addr, align 4
  store %"struct.std::__2::default_delete.100"* %__u, %"struct.std::__2::default_delete.100"** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.99"*, %"struct.std::__2::__compressed_pair_elem.99"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.99"* %this1 to %"struct.std::__2::default_delete.100"*
  %1 = load %"struct.std::__2::default_delete.100"*, %"struct.std::__2::default_delete.100"** %__u.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.100"* @_ZNSt3__27forwardINS_14default_deleteIN5draco26AttributesDecoderInterfaceEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.100"* nonnull align 1 dereferenceable(1) %1) #9
  ret %"struct.std::__2::__compressed_pair_elem.99"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"** @_ZNSt3__24moveIRPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOSA_(%"class.std::__2::unique_ptr.96"** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::unique_ptr.96"**, align 4
  store %"class.std::__2::unique_ptr.96"** %__t, %"class.std::__2::unique_ptr.96"*** %__t.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.96"**, %"class.std::__2::unique_ptr.96"*** %__t.addr, align 4
  ret %"class.std::__2::unique_ptr.96"** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE5clearEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 1
  %0 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__begin_, align 4
  call void @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE17__destruct_at_endEPS6_(%"struct.std::__2::__split_buffer"* %this1, %"class.std::__2::unique_ptr.96"* %0) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE10deallocateERS8_PS7_m(%"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %__a, %"class.std::__2::unique_ptr.96"* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.104"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.104"* %__a, %"class.std::__2::allocator.104"** %__a.addr, align 4
  store %"class.std::__2::unique_ptr.96"* %__p, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.104"*, %"class.std::__2::allocator.104"** %__a.addr, align 4
  %1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEE10deallocateEPS6_m(%"class.std::__2::allocator.104"* %0, %"class.std::__2::unique_ptr.96"* %1, i32 %2) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE8capacityEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"** @_ZNKSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this1) #9
  %0 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %call, align 4
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__first_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.std::__2::unique_ptr.96"* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.std::__2::unique_ptr.96"* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE17__destruct_at_endEPS6_(%"struct.std::__2::__split_buffer"* %this, %"class.std::__2::unique_ptr.96"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__new_last.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant.139", align 1
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.96"* %__new_last, %"class.std::__2::unique_ptr.96"** %__new_last.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__new_last.addr, align 4
  call void @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE17__destruct_at_endEPS6_NS_17integral_constantIbLb0EEE(%"struct.std::__2::__split_buffer"* %this1, %"class.std::__2::unique_ptr.96"* %0) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE17__destruct_at_endEPS6_NS_17integral_constantIbLb0EEE(%"struct.std::__2::__split_buffer"* %this, %"class.std::__2::unique_ptr.96"* %__new_last) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant.139", align 1
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__new_last.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.96"* %__new_last, %"class.std::__2::unique_ptr.96"** %__new_last.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__new_last.addr, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 2
  %2 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__end_, align 4
  %cmp = icmp ne %"class.std::__2::unique_ptr.96"* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE7__allocEv(%"struct.std::__2::__split_buffer"* %this1) #9
  %__end_2 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 2
  %3 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__end_2, align 4
  %incdec.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %3, i32 -1
  store %"class.std::__2::unique_ptr.96"* %incdec.ptr, %"class.std::__2::unique_ptr.96"** %__end_2, align 4
  %call3 = call %"class.std::__2::unique_ptr.96"* @_ZNSt3__212__to_addressINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEEPT_S8_(%"class.std::__2::unique_ptr.96"* %incdec.ptr) #9
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE7destroyIS7_EEvRS8_PT_(%"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %call, %"class.std::__2::unique_ptr.96"* %call3)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE7destroyIS7_EEvRS8_PT_(%"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %__a, %"class.std::__2::unique_ptr.96"* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.104"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy", align 1
  store %"class.std::__2::allocator.104"* %__a, %"class.std::__2::allocator.104"** %__a.addr, align 4
  store %"class.std::__2::unique_ptr.96"* %__p, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.104"*, %"class.std::__2::allocator.104"** %__a.addr, align 4
  %2 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE9__destroyIS7_EEvNS_17integral_constantIbLb1EEERS8_PT_(%"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %1, %"class.std::__2::unique_ptr.96"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE9__destroyIS7_EEvNS_17integral_constantIbLb1EEERS8_PT_(%"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %__a, %"class.std::__2::unique_ptr.96"* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.104"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  store %"class.std::__2::allocator.104"* %__a, %"class.std::__2::allocator.104"** %__a.addr, align 4
  store %"class.std::__2::unique_ptr.96"* %__p, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.104"*, %"class.std::__2::allocator.104"** %__a.addr, align 4
  %2 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  call void @_ZNSt3__29allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEE7destroyEPS6_(%"class.std::__2::allocator.104"* %1, %"class.std::__2::unique_ptr.96"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEE7destroyEPS6_(%"class.std::__2::allocator.104"* %this, %"class.std::__2::unique_ptr.96"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.104"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  store %"class.std::__2::allocator.104"* %this, %"class.std::__2::allocator.104"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.96"* %__p, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.104"*, %"class.std::__2::allocator.104"** %this.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  %call = call %"class.std::__2::unique_ptr.96"* @_ZNSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.96"* %0) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEE10deallocateEPS6_m(%"class.std::__2::allocator.104"* %this, %"class.std::__2::unique_ptr.96"* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.104"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.104"* %this, %"class.std::__2::allocator.104"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.96"* %__p, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.104"*, %"class.std::__2::allocator.104"** %this.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  %1 = bitcast %"class.std::__2::unique_ptr.96"* %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %__ptr, i32 %__size, i32 %__align) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  %1 = load i32, i32* %__size.addr, align 4
  %2 = load i32, i32* %__align.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm(i8* %0, i32 %1, i32 %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm(i8* %__ptr, i32 %__size, i32 %__align) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  %1 = load i32, i32* %__size.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm(i8* %0, i32 %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm(i8* %__ptr, i32 %__size) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller9__do_callEPv(i8* %0)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller9__do_callEPv(i8* %__ptr) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  call void @_ZdlPv(i8* %0) #10
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #5

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"** @_ZNKSt3__214__split_bufferINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"** @_ZNKSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.136"* %__end_cap_) #9
  ret %"class.std::__2::unique_ptr.96"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"** @_ZNKSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEERNS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.136"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.136"*, align 4
  store %"class.std::__2::__compressed_pair.136"* %this, %"class.std::__2::__compressed_pair.136"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.136"*, %"class.std::__2::__compressed_pair.136"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.136"* %this1 to %"struct.std::__2::__compressed_pair_elem.102"*
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"** @_ZNKSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.102"* %0) #9
  ret %"class.std::__2::unique_ptr.96"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE27__invalidate_iterators_pastEPS6_(%"class.std::__2::vector.94"* %this, %"class.std::__2::unique_ptr.96"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  %__new_last.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.96"* %__new_last, %"class.std::__2::unique_ptr.96"** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__destruct_at_endEPS6_(%"class.std::__2::__vector_base.95"* %this, %"class.std::__2::unique_ptr.96"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.95"*, align 4
  %__new_last.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  %__soon_to_be_end = alloca %"class.std::__2::unique_ptr.96"*, align 4
  store %"class.std::__2::__vector_base.95"* %this, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.96"* %__new_last, %"class.std::__2::unique_ptr.96"** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.95"*, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %this1, i32 0, i32 1
  %0 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__end_, align 4
  store %"class.std::__2::unique_ptr.96"* %0, %"class.std::__2::unique_ptr.96"** %__soon_to_be_end, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__new_last.addr, align 4
  %2 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__soon_to_be_end, align 4
  %cmp = icmp ne %"class.std::__2::unique_ptr.96"* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base.95"* %this1) #9
  %3 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__soon_to_be_end, align 4
  %incdec.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %3, i32 -1
  store %"class.std::__2::unique_ptr.96"* %incdec.ptr, %"class.std::__2::unique_ptr.96"** %__soon_to_be_end, align 4
  %call2 = call %"class.std::__2::unique_ptr.96"* @_ZNSt3__212__to_addressINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEEPT_S8_(%"class.std::__2::unique_ptr.96"* %incdec.ptr) #9
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE7destroyIS7_EEvRS8_PT_(%"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %call, %"class.std::__2::unique_ptr.96"* %call2)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %4 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__new_last.addr, align 4
  %__end_3 = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %this1, i32 0, i32 1
  store %"class.std::__2::unique_ptr.96"* %4, %"class.std::__2::unique_ptr.96"** %__end_3, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__annotate_shrinkEm(%"class.std::__2::vector.94"* %this, i32 %__old_size) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  %__old_size.addr = alloca i32, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  store i32 %__old_size, i32* %__old_size.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  %call = call %"class.std::__2::unique_ptr.96"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.94"* %this1) #9
  %0 = bitcast %"class.std::__2::unique_ptr.96"* %call to i8*
  %call2 = call %"class.std::__2::unique_ptr.96"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.94"* %this1) #9
  %call3 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector.94"* %this1) #9
  %add.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %call2, i32 %call3
  %1 = bitcast %"class.std::__2::unique_ptr.96"* %add.ptr to i8*
  %call4 = call %"class.std::__2::unique_ptr.96"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.94"* %this1) #9
  %2 = load i32, i32* %__old_size.addr, align 4
  %add.ptr5 = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %call4, i32 %2
  %3 = bitcast %"class.std::__2::unique_ptr.96"* %add.ptr5 to i8*
  %call6 = call %"class.std::__2::unique_ptr.96"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.94"* %this1) #9
  %call7 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.94"* %this1) #9
  %add.ptr8 = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %call6, i32 %call7
  %4 = bitcast %"class.std::__2::unique_ptr.96"* %add.ptr8 to i8*
  call void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE31__annotate_contiguous_containerEPKvSB_SB_SB_(%"class.std::__2::vector.94"* %this1, i8* %0, i8* %1, i8* %3, i8* %4) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.96"* %this, %"class.draco::AttributesDecoderInterface"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  %__p.addr = alloca %"class.draco::AttributesDecoderInterface"*, align 4
  %__tmp = alloca %"class.draco::AttributesDecoderInterface"*, align 4
  store %"class.std::__2::unique_ptr.96"* %this, %"class.std::__2::unique_ptr.96"** %this.addr, align 4
  store %"class.draco::AttributesDecoderInterface"* %__p, %"class.draco::AttributesDecoderInterface"** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributesDecoderInterface"** @_ZNSt3__217__compressed_pairIPN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.97"* %__ptr_) #9
  %0 = load %"class.draco::AttributesDecoderInterface"*, %"class.draco::AttributesDecoderInterface"** %call, align 4
  store %"class.draco::AttributesDecoderInterface"* %0, %"class.draco::AttributesDecoderInterface"** %__tmp, align 4
  %1 = load %"class.draco::AttributesDecoderInterface"*, %"class.draco::AttributesDecoderInterface"** %__p.addr, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::AttributesDecoderInterface"** @_ZNSt3__217__compressed_pairIPN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.97"* %__ptr_2) #9
  store %"class.draco::AttributesDecoderInterface"* %1, %"class.draco::AttributesDecoderInterface"** %call3, align 4
  %2 = load %"class.draco::AttributesDecoderInterface"*, %"class.draco::AttributesDecoderInterface"** %__tmp, align 4
  %tobool = icmp ne %"class.draco::AttributesDecoderInterface"* %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__ptr_4 = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %this1, i32 0, i32 0
  %call5 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.100"* @_ZNSt3__217__compressed_pairIPN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.97"* %__ptr_4) #9
  %3 = load %"class.draco::AttributesDecoderInterface"*, %"class.draco::AttributesDecoderInterface"** %__tmp, align 4
  call void @_ZNKSt3__214default_deleteIN5draco26AttributesDecoderInterfaceEEclEPS2_(%"struct.std::__2::default_delete.100"* %call5, %"class.draco::AttributesDecoderInterface"* %3) #9
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__214default_deleteIN5draco26AttributesDecoderInterfaceEEclEPS2_(%"struct.std::__2::default_delete.100"* %this, %"class.draco::AttributesDecoderInterface"* %__ptr) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::default_delete.100"*, align 4
  %__ptr.addr = alloca %"class.draco::AttributesDecoderInterface"*, align 4
  store %"struct.std::__2::default_delete.100"* %this, %"struct.std::__2::default_delete.100"** %this.addr, align 4
  store %"class.draco::AttributesDecoderInterface"* %__ptr, %"class.draco::AttributesDecoderInterface"** %__ptr.addr, align 4
  %this1 = load %"struct.std::__2::default_delete.100"*, %"struct.std::__2::default_delete.100"** %this.addr, align 4
  %0 = load %"class.draco::AttributesDecoderInterface"*, %"class.draco::AttributesDecoderInterface"** %__ptr.addr, align 4
  %isnull = icmp eq %"class.draco::AttributesDecoderInterface"* %0, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %entry
  %1 = bitcast %"class.draco::AttributesDecoderInterface"* %0 to void (%"class.draco::AttributesDecoderInterface"*)***
  %vtable = load void (%"class.draco::AttributesDecoderInterface"*)**, void (%"class.draco::AttributesDecoderInterface"*)*** %1, align 4
  %vfn = getelementptr inbounds void (%"class.draco::AttributesDecoderInterface"*)*, void (%"class.draco::AttributesDecoderInterface"*)** %vtable, i64 1
  %2 = load void (%"class.draco::AttributesDecoderInterface"*)*, void (%"class.draco::AttributesDecoderInterface"*)** %vfn, align 4
  call void %2(%"class.draco::AttributesDecoderInterface"* %0) #9
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::PointsSequencer"* @_ZN5draco15PointsSequencerC2Ev(%"class.draco::PointsSequencer"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointsSequencer"*, align 4
  store %"class.draco::PointsSequencer"* %this, %"class.draco::PointsSequencer"** %this.addr, align 4
  %this1 = load %"class.draco::PointsSequencer"*, %"class.draco::PointsSequencer"** %this.addr, align 4
  %0 = bitcast %"class.draco::PointsSequencer"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [6 x i8*] }, { [6 x i8*] }* @_ZTVN5draco15PointsSequencerE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %out_point_ids_ = getelementptr inbounds %"class.draco::PointsSequencer", %"class.draco::PointsSequencer"* %this1, i32 0, i32 1
  store %"class.std::__2::vector.123"* null, %"class.std::__2::vector.123"** %out_point_ids_, align 4
  ret %"class.draco::PointsSequencer"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::LinearSequencer"* @_ZN5draco15LinearSequencerD2Ev(%"class.draco::LinearSequencer"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::LinearSequencer"*, align 4
  store %"class.draco::LinearSequencer"* %this, %"class.draco::LinearSequencer"** %this.addr, align 4
  %this1 = load %"class.draco::LinearSequencer"*, %"class.draco::LinearSequencer"** %this.addr, align 4
  %0 = bitcast %"class.draco::LinearSequencer"* %this1 to %"class.draco::PointsSequencer"*
  %call = call %"class.draco::PointsSequencer"* @_ZN5draco15PointsSequencerD2Ev(%"class.draco::PointsSequencer"* %0) #9
  ret %"class.draco::LinearSequencer"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco15LinearSequencerD0Ev(%"class.draco::LinearSequencer"* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::LinearSequencer"*, align 4
  store %"class.draco::LinearSequencer"* %this, %"class.draco::LinearSequencer"** %this.addr, align 4
  %this1 = load %"class.draco::LinearSequencer"*, %"class.draco::LinearSequencer"** %this.addr, align 4
  %call = call %"class.draco::LinearSequencer"* @_ZN5draco15LinearSequencerD2Ev(%"class.draco::LinearSequencer"* %this1) #9
  %0 = bitcast %"class.draco::LinearSequencer"* %this1 to i8*
  call void @_ZdlPv(i8* %0) #10
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco15LinearSequencer34UpdatePointToAttributeIndexMappingEPNS_14PointAttributeE(%"class.draco::LinearSequencer"* %this, %"class.draco::PointAttribute"* %attribute) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::LinearSequencer"*, align 4
  %attribute.addr = alloca %"class.draco::PointAttribute"*, align 4
  store %"class.draco::LinearSequencer"* %this, %"class.draco::LinearSequencer"** %this.addr, align 4
  store %"class.draco::PointAttribute"* %attribute, %"class.draco::PointAttribute"** %attribute.addr, align 4
  %this1 = load %"class.draco::LinearSequencer"*, %"class.draco::LinearSequencer"** %this.addr, align 4
  %0 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %attribute.addr, align 4
  call void @_ZN5draco14PointAttribute18SetIdentityMappingEv(%"class.draco::PointAttribute"* %0)
  ret i1 true
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco15LinearSequencer24GenerateSequenceInternalEv(%"class.draco::LinearSequencer"* %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::LinearSequencer"*, align 4
  %i = alloca i32, align 4
  %ref.tmp = alloca %"class.draco::IndexType.125", align 4
  store %"class.draco::LinearSequencer"* %this, %"class.draco::LinearSequencer"** %this.addr, align 4
  %this1 = load %"class.draco::LinearSequencer"*, %"class.draco::LinearSequencer"** %this.addr, align 4
  %num_points_ = getelementptr inbounds %"class.draco::LinearSequencer", %"class.draco::LinearSequencer"* %this1, i32 0, i32 1
  %0 = load i32, i32* %num_points_, align 4
  %cmp = icmp slt i32 %0, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %1 = bitcast %"class.draco::LinearSequencer"* %this1 to %"class.draco::PointsSequencer"*
  %call = call %"class.std::__2::vector.123"* @_ZNK5draco15PointsSequencer13out_point_idsEv(%"class.draco::PointsSequencer"* %1)
  %num_points_2 = getelementptr inbounds %"class.draco::LinearSequencer", %"class.draco::LinearSequencer"* %this1, i32 0, i32 1
  %2 = load i32, i32* %num_points_2, align 4
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE6resizeEm(%"class.std::__2::vector.123"* %call, i32 %2)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %3 = load i32, i32* %i, align 4
  %num_points_3 = getelementptr inbounds %"class.draco::LinearSequencer", %"class.draco::LinearSequencer"* %this1, i32 0, i32 1
  %4 = load i32, i32* %num_points_3, align 4
  %cmp4 = icmp slt i32 %3, %4
  br i1 %cmp4, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %5 = load i32, i32* %i, align 4
  %call5 = call %"class.draco::IndexType.125"* @_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEC2Ej(%"class.draco::IndexType.125"* %ref.tmp, i32 %5)
  %6 = bitcast %"class.draco::LinearSequencer"* %this1 to %"class.draco::PointsSequencer"*
  %call6 = call %"class.std::__2::vector.123"* @_ZNK5draco15PointsSequencer13out_point_idsEv(%"class.draco::PointsSequencer"* %6)
  %7 = load i32, i32* %i, align 4
  %call7 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.125"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE2atEm(%"class.std::__2::vector.123"* %call6, i32 %7)
  %call8 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.125"* @_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEaSERKS2_(%"class.draco::IndexType.125"* %call7, %"class.draco::IndexType.125"* nonnull align 4 dereferenceable(4) %ref.tmp)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %8 = load i32, i32* %i, align 4
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %for.end, %if.then
  %9 = load i1, i1* %retval, align 1
  ret i1 %9
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::PointsSequencer"* @_ZN5draco15PointsSequencerD2Ev(%"class.draco::PointsSequencer"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointsSequencer"*, align 4
  store %"class.draco::PointsSequencer"* %this, %"class.draco::PointsSequencer"** %this.addr, align 4
  %this1 = load %"class.draco::PointsSequencer"*, %"class.draco::PointsSequencer"** %this.addr, align 4
  ret %"class.draco::PointsSequencer"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco15PointsSequencerD0Ev(%"class.draco::PointsSequencer"* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointsSequencer"*, align 4
  store %"class.draco::PointsSequencer"* %this, %"class.draco::PointsSequencer"** %this.addr, align 4
  %this1 = load %"class.draco::PointsSequencer"*, %"class.draco::PointsSequencer"** %this.addr, align 4
  call void @llvm.trap() #12
  unreachable
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco15PointsSequencer34UpdatePointToAttributeIndexMappingEPNS_14PointAttributeE(%"class.draco::PointsSequencer"* %this, %"class.draco::PointAttribute"* %0) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointsSequencer"*, align 4
  %.addr = alloca %"class.draco::PointAttribute"*, align 4
  store %"class.draco::PointsSequencer"* %this, %"class.draco::PointsSequencer"** %this.addr, align 4
  store %"class.draco::PointAttribute"* %0, %"class.draco::PointAttribute"** %.addr, align 4
  %this1 = load %"class.draco::PointsSequencer"*, %"class.draco::PointsSequencer"** %this.addr, align 4
  ret i1 false
}

declare void @__cxa_pure_virtual() unnamed_addr

; Function Attrs: cold noreturn nounwind
declare void @llvm.trap() #6

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco14PointAttribute18SetIdentityMappingEv(%"class.draco::PointAttribute"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointAttribute"*, align 4
  store %"class.draco::PointAttribute"* %this, %"class.draco::PointAttribute"** %this.addr, align 4
  %this1 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %this.addr, align 4
  %identity_mapping_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 4
  store i8 1, i8* %identity_mapping_, align 4
  %indices_map_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 2
  call void @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEEE5clearEv(%"class.draco::IndexTypeVector"* %indices_map_)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEEE5clearEv(%"class.draco::IndexTypeVector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector"*, align 4
  store %"class.draco::IndexTypeVector"* %this, %"class.draco::IndexTypeVector"** %this.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector"*, %"class.draco::IndexTypeVector"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector", %"class.draco::IndexTypeVector"* %this1, i32 0, i32 0
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5clearEv(%"class.std::__2::vector.68"* %vector_) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5clearEv(%"class.std::__2::vector.68"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.68"*, align 4
  %__old_size = alloca i32, align 4
  store %"class.std::__2::vector.68"* %this, %"class.std::__2::vector.68"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.68"* %this1) #9
  store i32 %call, i32* %__old_size, align 4
  %0 = bitcast %"class.std::__2::vector.68"* %this1 to %"class.std::__2::__vector_base.69"*
  call void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5clearEv(%"class.std::__2::__vector_base.69"* %0) #9
  %1 = load i32, i32* %__old_size, align 4
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_shrinkEm(%"class.std::__2::vector.68"* %this1, i32 %1) #9
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector.68"* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.68"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.68"*, align 4
  store %"class.std::__2::vector.68"* %this, %"class.std::__2::vector.68"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.68"* %this1 to %"class.std::__2::__vector_base.69"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %0, i32 0, i32 1
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector.68"* %this1 to %"class.std::__2::__vector_base.69"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %2, i32 0, i32 0
  %3 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.draco::IndexType"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.draco::IndexType"* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5clearEv(%"class.std::__2::__vector_base.69"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.69"*, align 4
  store %"class.std::__2::__vector_base.69"* %this, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.69"*, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  call void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_(%"class.std::__2::__vector_base.69"* %this1, %"class.draco::IndexType"* %0) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_shrinkEm(%"class.std::__2::vector.68"* %this, i32 %__old_size) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.68"*, align 4
  %__old_size.addr = alloca i32, align 4
  store %"class.std::__2::vector.68"* %this, %"class.std::__2::vector.68"** %this.addr, align 4
  store i32 %__old_size, i32* %__old_size.addr, align 4
  %this1 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %this.addr, align 4
  %call = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.68"* %this1) #9
  %0 = bitcast %"class.draco::IndexType"* %call to i8*
  %call2 = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.68"* %this1) #9
  %call3 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.68"* %this1) #9
  %add.ptr = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %call2, i32 %call3
  %1 = bitcast %"class.draco::IndexType"* %add.ptr to i8*
  %call4 = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.68"* %this1) #9
  %2 = load i32, i32* %__old_size.addr, align 4
  %add.ptr5 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %call4, i32 %2
  %3 = bitcast %"class.draco::IndexType"* %add.ptr5 to i8*
  %call6 = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.68"* %this1) #9
  %call7 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.68"* %this1) #9
  %add.ptr8 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %call6, i32 %call7
  %4 = bitcast %"class.draco::IndexType"* %add.ptr8 to i8*
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_(%"class.std::__2::vector.68"* %this1, i8* %0, i8* %1, i8* %3, i8* %4) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector.68"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.68"*, align 4
  store %"class.std::__2::vector.68"* %this, %"class.std::__2::vector.68"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_(%"class.std::__2::__vector_base.69"* %this, %"class.draco::IndexType"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.69"*, align 4
  %__new_last.addr = alloca %"class.draco::IndexType"*, align 4
  %__soon_to_be_end = alloca %"class.draco::IndexType"*, align 4
  store %"class.std::__2::__vector_base.69"* %this, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  store %"class.draco::IndexType"* %__new_last, %"class.draco::IndexType"** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.69"*, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %this1, i32 0, i32 1
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__end_, align 4
  store %"class.draco::IndexType"* %0, %"class.draco::IndexType"** %__soon_to_be_end, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__new_last.addr, align 4
  %2 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__soon_to_be_end, align 4
  %cmp = icmp ne %"class.draco::IndexType"* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.69"* %this1) #9
  %3 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__soon_to_be_end, align 4
  %incdec.ptr = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %3, i32 -1
  store %"class.draco::IndexType"* %incdec.ptr, %"class.draco::IndexType"** %__soon_to_be_end, align 4
  %call2 = call %"class.draco::IndexType"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType"* %incdec.ptr) #9
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE7destroyIS5_EEvRS6_PT_(%"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %call, %"class.draco::IndexType"* %call2)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %4 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__new_last.addr, align 4
  %__end_3 = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %this1, i32 0, i32 1
  store %"class.draco::IndexType"* %4, %"class.draco::IndexType"** %__end_3, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE7destroyIS5_EEvRS6_PT_(%"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType"* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.73"*, align 4
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy.140", align 1
  store %"class.std::__2::allocator.73"* %__a, %"class.std::__2::allocator.73"** %__a.addr, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy.140"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.73"*, %"class.std::__2::allocator.73"** %__a.addr, align 4
  %2 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE9__destroyIS5_EEvNS_17integral_constantIbLb1EEERS6_PT_(%"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %1, %"class.draco::IndexType"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.69"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.69"*, align 4
  store %"class.std::__2::__vector_base.69"* %this, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.69"*, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.70"* %__end_cap_) #9
  ret %"class.std::__2::allocator.73"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType"* %__p) #0 comdat {
entry:
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  ret %"class.draco::IndexType"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE9__destroyIS5_EEvNS_17integral_constantIbLb1EEERS6_PT_(%"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType"* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.73"*, align 4
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.std::__2::allocator.73"* %__a, %"class.std::__2::allocator.73"** %__a.addr, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.73"*, %"class.std::__2::allocator.73"** %__a.addr, align 4
  %2 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  call void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE7destroyEPS4_(%"class.std::__2::allocator.73"* %1, %"class.draco::IndexType"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE7destroyEPS4_(%"class.std::__2::allocator.73"* %this, %"class.draco::IndexType"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.73"*, align 4
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.std::__2::allocator.73"* %this, %"class.std::__2::allocator.73"** %this.addr, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.73"*, %"class.std::__2::allocator.73"** %this.addr, align 4
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.70"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.70"*, align 4
  store %"class.std::__2::__compressed_pair.70"* %this, %"class.std::__2::__compressed_pair.70"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.70"*, %"class.std::__2::__compressed_pair.70"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.70"* %this1 to %"struct.std::__2::__compressed_pair_elem.72"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.72"* %0) #9
  ret %"class.std::__2::allocator.73"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.72"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.72"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.72"* %this, %"struct.std::__2::__compressed_pair_elem.72"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.72"*, %"struct.std::__2::__compressed_pair_elem.72"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.72"* %this1 to %"class.std::__2::allocator.73"*
  ret %"class.std::__2::allocator.73"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_(%"class.std::__2::vector.68"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.68"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector.68"* %this, %"class.std::__2::vector.68"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.68"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.68"*, align 4
  store %"class.std::__2::vector.68"* %this, %"class.std::__2::vector.68"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.68"* %this1 to %"class.std::__2::__vector_base.69"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %0, i32 0, i32 0
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  %call = call %"class.draco::IndexType"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType"* %1) #9
  ret %"class.draco::IndexType"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.68"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.68"*, align 4
  store %"class.std::__2::vector.68"* %this, %"class.std::__2::vector.68"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.68"* %this1 to %"class.std::__2::__vector_base.69"*
  %call = call i32 @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::__vector_base.69"* %0) #9
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::__vector_base.69"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.69"*, align 4
  store %"class.std::__2::__vector_base.69"* %this, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.69"*, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base.69"* %this1) #9
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %this1, i32 0, i32 0
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.draco::IndexType"* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.draco::IndexType"* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base.69"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.69"*, align 4
  store %"class.std::__2::__vector_base.69"* %this, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.69"*, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.70"* %__end_cap_) #9
  ret %"class.draco::IndexType"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.70"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.70"*, align 4
  store %"class.std::__2::__compressed_pair.70"* %this, %"class.std::__2::__compressed_pair.70"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.70"*, %"class.std::__2::__compressed_pair.70"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.70"* %this1 to %"struct.std::__2::__compressed_pair_elem.71"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.71"* %0) #9
  ret %"class.draco::IndexType"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.71"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.71"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.71"* %this, %"struct.std::__2::__compressed_pair_elem.71"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.71"*, %"struct.std::__2::__compressed_pair_elem.71"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.71", %"struct.std::__2::__compressed_pair_elem.71"* %this1, i32 0, i32 0
  ret %"class.draco::IndexType"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector.123"* @_ZNK5draco15PointsSequencer13out_point_idsEv(%"class.draco::PointsSequencer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointsSequencer"*, align 4
  store %"class.draco::PointsSequencer"* %this, %"class.draco::PointsSequencer"** %this.addr, align 4
  %this1 = load %"class.draco::PointsSequencer"*, %"class.draco::PointsSequencer"** %this.addr, align 4
  %out_point_ids_ = getelementptr inbounds %"class.draco::PointsSequencer", %"class.draco::PointsSequencer"* %this1, i32 0, i32 1
  %0 = load %"class.std::__2::vector.123"*, %"class.std::__2::vector.123"** %out_point_ids_, align 4
  ret %"class.std::__2::vector.123"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE6resizeEm(%"class.std::__2::vector.123"* %this, i32 %__sz) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.123"*, align 4
  %__sz.addr = alloca i32, align 4
  %__cs = alloca i32, align 4
  store %"class.std::__2::vector.123"* %this, %"class.std::__2::vector.123"** %this.addr, align 4
  store i32 %__sz, i32* %__sz.addr, align 4
  %this1 = load %"class.std::__2::vector.123"*, %"class.std::__2::vector.123"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.123"* %this1) #9
  store i32 %call, i32* %__cs, align 4
  %0 = load i32, i32* %__cs, align 4
  %1 = load i32, i32* %__sz.addr, align 4
  %cmp = icmp ult i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %__sz.addr, align 4
  %3 = load i32, i32* %__cs, align 4
  %sub = sub i32 %2, %3
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE8__appendEm(%"class.std::__2::vector.123"* %this1, i32 %sub)
  br label %if.end4

if.else:                                          ; preds = %entry
  %4 = load i32, i32* %__cs, align 4
  %5 = load i32, i32* %__sz.addr, align 4
  %cmp2 = icmp ugt i32 %4, %5
  br i1 %cmp2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.else
  %6 = bitcast %"class.std::__2::vector.123"* %this1 to %"class.std::__2::__vector_base.124"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.124", %"class.std::__2::__vector_base.124"* %6, i32 0, i32 0
  %7 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %__begin_, align 4
  %8 = load i32, i32* %__sz.addr, align 4
  %add.ptr = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %7, i32 %8
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_(%"class.std::__2::vector.123"* %this1, %"class.draco::IndexType.125"* %add.ptr) #9
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.else
  br label %if.end4

if.end4:                                          ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType.125"* @_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEC2Ej(%"class.draco::IndexType.125"* returned %this, i32 %value) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.125"*, align 4
  %value.addr = alloca i32, align 4
  store %"class.draco::IndexType.125"* %this, %"class.draco::IndexType.125"** %this.addr, align 4
  store i32 %value, i32* %value.addr, align 4
  %this1 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value.addr, align 4
  store i32 %0, i32* %value_, align 4
  ret %"class.draco::IndexType.125"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.125"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE2atEm(%"class.std::__2::vector.123"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.123"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector.123"* %this, %"class.std::__2::vector.123"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.123"*, %"class.std::__2::vector.123"** %this.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.123"* %this1) #9
  %cmp = icmp uge i32 %0, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %"class.std::__2::vector.123"* %this1 to %"class.std::__2::__vector_base_common"*
  call void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_out_of_rangeEv(%"class.std::__2::__vector_base_common"* %1) #11
  unreachable

if.end:                                           ; preds = %entry
  %2 = bitcast %"class.std::__2::vector.123"* %this1 to %"class.std::__2::__vector_base.124"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.124", %"class.std::__2::__vector_base.124"* %2, i32 0, i32 0
  %3 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %__begin_, align 4
  %4 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %3, i32 %4
  ret %"class.draco::IndexType.125"* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.125"* @_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEaSERKS2_(%"class.draco::IndexType.125"* %this, %"class.draco::IndexType.125"* nonnull align 4 dereferenceable(4) %i) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.125"*, align 4
  %i.addr = alloca %"class.draco::IndexType.125"*, align 4
  store %"class.draco::IndexType.125"* %this, %"class.draco::IndexType.125"** %this.addr, align 4
  store %"class.draco::IndexType.125"* %i, %"class.draco::IndexType.125"** %i.addr, align 4
  %this1 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %this.addr, align 4
  %0 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %i.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %0, i32 0, i32 0
  %1 = load i32, i32* %value_, align 4
  %value_2 = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %this1, i32 0, i32 0
  store i32 %1, i32* %value_2, align 4
  ret %"class.draco::IndexType.125"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.123"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.123"*, align 4
  store %"class.std::__2::vector.123"* %this, %"class.std::__2::vector.123"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.123"*, %"class.std::__2::vector.123"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.123"* %this1 to %"class.std::__2::__vector_base.124"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.124", %"class.std::__2::__vector_base.124"* %0, i32 0, i32 1
  %1 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector.123"* %this1 to %"class.std::__2::__vector_base.124"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.124", %"class.std::__2::__vector_base.124"* %2, i32 0, i32 0
  %3 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.draco::IndexType.125"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.draco::IndexType.125"* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE8__appendEm(%"class.std::__2::vector.123"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.123"*, align 4
  %__n.addr = alloca i32, align 4
  %__a = alloca %"class.std::__2::allocator.129"*, align 4
  %__v = alloca %"struct.std::__2::__split_buffer.141", align 4
  store %"class.std::__2::vector.123"* %this, %"class.std::__2::vector.123"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.123"*, %"class.std::__2::vector.123"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.123"* %this1 to %"class.std::__2::__vector_base.124"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.125"** @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base.124"* %0) #9
  %1 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %call, align 4
  %2 = bitcast %"class.std::__2::vector.123"* %this1 to %"class.std::__2::__vector_base.124"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.124", %"class.std::__2::__vector_base.124"* %2, i32 0, i32 1
  %3 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %__end_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.draco::IndexType.125"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.draco::IndexType.125"* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  %4 = load i32, i32* %__n.addr, align 4
  %cmp = icmp uge i32 %sub.ptr.div, %4
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %5 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE18__construct_at_endEm(%"class.std::__2::vector.123"* %this1, i32 %5)
  br label %if.end

if.else:                                          ; preds = %entry
  %6 = bitcast %"class.std::__2::vector.123"* %this1 to %"class.std::__2::__vector_base.124"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.129"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.124"* %6) #9
  store %"class.std::__2::allocator.129"* %call2, %"class.std::__2::allocator.129"** %__a, align 4
  %call3 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.123"* %this1) #9
  %7 = load i32, i32* %__n.addr, align 4
  %add = add i32 %call3, %7
  %call4 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE11__recommendEm(%"class.std::__2::vector.123"* %this1, i32 %add)
  %call5 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.123"* %this1) #9
  %8 = load %"class.std::__2::allocator.129"*, %"class.std::__2::allocator.129"** %__a, align 4
  %call6 = call %"struct.std::__2::__split_buffer.141"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEEC2EmmS7_(%"struct.std::__2::__split_buffer.141"* %__v, i32 %call4, i32 %call5, %"class.std::__2::allocator.129"* nonnull align 1 dereferenceable(1) %8)
  %9 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE18__construct_at_endEm(%"struct.std::__2::__split_buffer.141"* %__v, i32 %9)
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS4_RS6_EE(%"class.std::__2::vector.123"* %this1, %"struct.std::__2::__split_buffer.141"* nonnull align 4 dereferenceable(20) %__v)
  %call7 = call %"struct.std::__2::__split_buffer.141"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEED2Ev(%"struct.std::__2::__split_buffer.141"* %__v) #9
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_(%"class.std::__2::vector.123"* %this, %"class.draco::IndexType.125"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.123"*, align 4
  %__new_last.addr = alloca %"class.draco::IndexType.125"*, align 4
  %__old_size = alloca i32, align 4
  store %"class.std::__2::vector.123"* %this, %"class.std::__2::vector.123"** %this.addr, align 4
  store %"class.draco::IndexType.125"* %__new_last, %"class.draco::IndexType.125"** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::vector.123"*, %"class.std::__2::vector.123"** %this.addr, align 4
  %0 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %__new_last.addr, align 4
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE27__invalidate_iterators_pastEPS4_(%"class.std::__2::vector.123"* %this1, %"class.draco::IndexType.125"* %0)
  %call = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.123"* %this1) #9
  store i32 %call, i32* %__old_size, align 4
  %1 = bitcast %"class.std::__2::vector.123"* %this1 to %"class.std::__2::__vector_base.124"*
  %2 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %__new_last.addr, align 4
  call void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_(%"class.std::__2::__vector_base.124"* %1, %"class.draco::IndexType.125"* %2) #9
  %3 = load i32, i32* %__old_size, align 4
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_shrinkEm(%"class.std::__2::vector.123"* %this1, i32 %3) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.125"** @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base.124"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.124"*, align 4
  store %"class.std::__2::__vector_base.124"* %this, %"class.std::__2::__vector_base.124"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.124"*, %"class.std::__2::__vector_base.124"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.124", %"class.std::__2::__vector_base.124"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.125"** @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.126"* %__end_cap_) #9
  ret %"class.draco::IndexType.125"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE18__construct_at_endEm(%"class.std::__2::vector.123"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.123"*, align 4
  %__n.addr = alloca i32, align 4
  %__tx = alloca %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction", align 4
  store %"class.std::__2::vector.123"* %this, %"class.std::__2::vector.123"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.123"*, %"class.std::__2::vector.123"** %this.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %call = call %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionC2ERS7_m(%"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"* %__tx, %"class.std::__2::vector.123"* nonnull align 4 dereferenceable(12) %this1, i32 %0)
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %1 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %__pos_, align 4
  %__new_end_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"* %__tx, i32 0, i32 2
  %2 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %__new_end_, align 4
  %cmp = icmp ne %"class.draco::IndexType.125"* %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = bitcast %"class.std::__2::vector.123"* %this1 to %"class.std::__2::__vector_base.124"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.129"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.124"* %3) #9
  %__pos_3 = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %4 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %__pos_3, align 4
  %call4 = call %"class.draco::IndexType.125"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType.125"* %4) #9
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE9constructIS5_JEEEvRS6_PT_DpOT0_(%"class.std::__2::allocator.129"* nonnull align 1 dereferenceable(1) %call2, %"class.draco::IndexType.125"* %call4)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %__pos_5 = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %5 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %__pos_5, align 4
  %incdec.ptr = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %5, i32 1
  store %"class.draco::IndexType.125"* %incdec.ptr, %"class.draco::IndexType.125"** %__pos_5, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call6 = call %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"* %__tx) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.129"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.124"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.124"*, align 4
  store %"class.std::__2::__vector_base.124"* %this, %"class.std::__2::__vector_base.124"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.124"*, %"class.std::__2::__vector_base.124"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.124", %"class.std::__2::__vector_base.124"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.129"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.126"* %__end_cap_) #9
  ret %"class.std::__2::allocator.129"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE11__recommendEm(%"class.std::__2::vector.123"* %this, i32 %__new_size) #0 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %"class.std::__2::vector.123"*, align 4
  %__new_size.addr = alloca i32, align 4
  %__ms = alloca i32, align 4
  %__cap = alloca i32, align 4
  %ref.tmp = alloca i32, align 4
  store %"class.std::__2::vector.123"* %this, %"class.std::__2::vector.123"** %this.addr, align 4
  store i32 %__new_size, i32* %__new_size.addr, align 4
  %this1 = load %"class.std::__2::vector.123"*, %"class.std::__2::vector.123"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE8max_sizeEv(%"class.std::__2::vector.123"* %this1) #9
  store i32 %call, i32* %__ms, align 4
  %0 = load i32, i32* %__new_size.addr, align 4
  %1 = load i32, i32* %__ms, align 4
  %cmp = icmp ugt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = bitcast %"class.std::__2::vector.123"* %this1 to %"class.std::__2::__vector_base_common"*
  call void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"* %2) #11
  unreachable

if.end:                                           ; preds = %entry
  %call2 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.123"* %this1) #9
  store i32 %call2, i32* %__cap, align 4
  %3 = load i32, i32* %__cap, align 4
  %4 = load i32, i32* %__ms, align 4
  %div = udiv i32 %4, 2
  %cmp3 = icmp uge i32 %3, %div
  br i1 %cmp3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %if.end
  %5 = load i32, i32* %__ms, align 4
  store i32 %5, i32* %retval, align 4
  br label %return

if.end5:                                          ; preds = %if.end
  %6 = load i32, i32* %__cap, align 4
  %mul = mul i32 2, %6
  store i32 %mul, i32* %ref.tmp, align 4
  %call6 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %__new_size.addr)
  %7 = load i32, i32* %call6, align 4
  store i32 %7, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end5, %if.then4
  %8 = load i32, i32* %retval, align 4
  ret i32 %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer.141"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEEC2EmmS7_(%"struct.std::__2::__split_buffer.141"* returned %this, i32 %__cap, i32 %__start, %"class.std::__2::allocator.129"* nonnull align 1 dereferenceable(1) %__a) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"struct.std::__2::__split_buffer.141"*, align 4
  %this.addr = alloca %"struct.std::__2::__split_buffer.141"*, align 4
  %__cap.addr = alloca i32, align 4
  %__start.addr = alloca i32, align 4
  %__a.addr = alloca %"class.std::__2::allocator.129"*, align 4
  %ref.tmp = alloca i8*, align 4
  store %"struct.std::__2::__split_buffer.141"* %this, %"struct.std::__2::__split_buffer.141"** %this.addr, align 4
  store i32 %__cap, i32* %__cap.addr, align 4
  store i32 %__start, i32* %__start.addr, align 4
  store %"class.std::__2::allocator.129"* %__a, %"class.std::__2::allocator.129"** %__a.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.141"*, %"struct.std::__2::__split_buffer.141"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer.141"* %this1, %"struct.std::__2::__split_buffer.141"** %retval, align 4
  %0 = bitcast %"struct.std::__2::__split_buffer.141"* %this1 to %"class.std::__2::__split_buffer_common"*
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer.141", %"struct.std::__2::__split_buffer.141"* %this1, i32 0, i32 3
  store i8* null, i8** %ref.tmp, align 4
  %1 = load %"class.std::__2::allocator.129"*, %"class.std::__2::allocator.129"** %__a.addr, align 4
  %call = call %"class.std::__2::__compressed_pair.142"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEEC2IDnS8_EEOT_OT0_(%"class.std::__2::__compressed_pair.142"* %__end_cap_, i8** nonnull align 4 dereferenceable(4) %ref.tmp, %"class.std::__2::allocator.129"* nonnull align 1 dereferenceable(1) %1)
  %2 = load i32, i32* %__cap.addr, align 4
  %cmp = icmp ne i32 %2, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.129"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE7__allocEv(%"struct.std::__2::__split_buffer.141"* %this1) #9
  %3 = load i32, i32* %__cap.addr, align 4
  %call3 = call %"class.draco::IndexType.125"* @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE8allocateERS6_m(%"class.std::__2::allocator.129"* nonnull align 1 dereferenceable(1) %call2, i32 %3)
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %"class.draco::IndexType.125"* [ %call3, %cond.true ], [ null, %cond.false ]
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer.141", %"struct.std::__2::__split_buffer.141"* %this1, i32 0, i32 0
  store %"class.draco::IndexType.125"* %cond, %"class.draco::IndexType.125"** %__first_, align 4
  %__first_4 = getelementptr inbounds %"struct.std::__2::__split_buffer.141", %"struct.std::__2::__split_buffer.141"* %this1, i32 0, i32 0
  %4 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %__first_4, align 4
  %5 = load i32, i32* %__start.addr, align 4
  %add.ptr = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %4, i32 %5
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer.141", %"struct.std::__2::__split_buffer.141"* %this1, i32 0, i32 2
  store %"class.draco::IndexType.125"* %add.ptr, %"class.draco::IndexType.125"** %__end_, align 4
  %__begin_ = getelementptr inbounds %"struct.std::__2::__split_buffer.141", %"struct.std::__2::__split_buffer.141"* %this1, i32 0, i32 1
  store %"class.draco::IndexType.125"* %add.ptr, %"class.draco::IndexType.125"** %__begin_, align 4
  %__first_5 = getelementptr inbounds %"struct.std::__2::__split_buffer.141", %"struct.std::__2::__split_buffer.141"* %this1, i32 0, i32 0
  %6 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %__first_5, align 4
  %7 = load i32, i32* %__cap.addr, align 4
  %add.ptr6 = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %6, i32 %7
  %call7 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.125"** @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE9__end_capEv(%"struct.std::__2::__split_buffer.141"* %this1) #9
  store %"class.draco::IndexType.125"* %add.ptr6, %"class.draco::IndexType.125"** %call7, align 4
  %8 = load %"struct.std::__2::__split_buffer.141"*, %"struct.std::__2::__split_buffer.141"** %retval, align 4
  ret %"struct.std::__2::__split_buffer.141"* %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE18__construct_at_endEm(%"struct.std::__2::__split_buffer.141"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.141"*, align 4
  %__n.addr = alloca i32, align 4
  %__tx = alloca %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction", align 4
  store %"struct.std::__2::__split_buffer.141"* %this, %"struct.std::__2::__split_buffer.141"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.141"*, %"struct.std::__2::__split_buffer.141"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer.141", %"struct.std::__2::__split_buffer.141"* %this1, i32 0, i32 2
  %0 = load i32, i32* %__n.addr, align 4
  %call = call %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE21_ConstructTransactionC2EPPS4_m(%"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction"* %__tx, %"class.draco::IndexType.125"** %__end_, i32 %0) #9
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %__pos_ = getelementptr inbounds %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction"* %__tx, i32 0, i32 0
  %1 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %__pos_, align 4
  %__end_2 = getelementptr inbounds %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %2 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %__end_2, align 4
  %cmp = icmp ne %"class.draco::IndexType.125"* %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %call3 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.129"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE7__allocEv(%"struct.std::__2::__split_buffer.141"* %this1) #9
  %__pos_4 = getelementptr inbounds %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction"* %__tx, i32 0, i32 0
  %3 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %__pos_4, align 4
  %call5 = call %"class.draco::IndexType.125"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType.125"* %3) #9
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE9constructIS5_JEEEvRS6_PT_DpOT0_(%"class.std::__2::allocator.129"* nonnull align 1 dereferenceable(1) %call3, %"class.draco::IndexType.125"* %call5)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %__pos_6 = getelementptr inbounds %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction"* %__tx, i32 0, i32 0
  %4 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %__pos_6, align 4
  %incdec.ptr = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %4, i32 1
  store %"class.draco::IndexType.125"* %incdec.ptr, %"class.draco::IndexType.125"** %__pos_6, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call7 = call %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction"* %__tx) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS4_RS6_EE(%"class.std::__2::vector.123"* %this, %"struct.std::__2::__split_buffer.141"* nonnull align 4 dereferenceable(20) %__v) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.123"*, align 4
  %__v.addr = alloca %"struct.std::__2::__split_buffer.141"*, align 4
  store %"class.std::__2::vector.123"* %this, %"class.std::__2::vector.123"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer.141"* %__v, %"struct.std::__2::__split_buffer.141"** %__v.addr, align 4
  %this1 = load %"class.std::__2::vector.123"*, %"class.std::__2::vector.123"** %this.addr, align 4
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_deleteEv(%"class.std::__2::vector.123"* %this1) #9
  %0 = bitcast %"class.std::__2::vector.123"* %this1 to %"class.std::__2::__vector_base.124"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.129"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.124"* %0) #9
  %1 = bitcast %"class.std::__2::vector.123"* %this1 to %"class.std::__2::__vector_base.124"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.124", %"class.std::__2::__vector_base.124"* %1, i32 0, i32 0
  %2 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %__begin_, align 4
  %3 = bitcast %"class.std::__2::vector.123"* %this1 to %"class.std::__2::__vector_base.124"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.124", %"class.std::__2::__vector_base.124"* %3, i32 0, i32 1
  %4 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %__end_, align 4
  %5 = load %"struct.std::__2::__split_buffer.141"*, %"struct.std::__2::__split_buffer.141"** %__v.addr, align 4
  %__begin_2 = getelementptr inbounds %"struct.std::__2::__split_buffer.141", %"struct.std::__2::__split_buffer.141"* %5, i32 0, i32 1
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE46__construct_backward_with_exception_guaranteesIS5_EENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS6_PT_SC_EE5valuesr31is_trivially_move_constructibleISC_EE5valueEvE4typeERS6_SD_SD_RSD_(%"class.std::__2::allocator.129"* nonnull align 1 dereferenceable(1) %call, %"class.draco::IndexType.125"* %2, %"class.draco::IndexType.125"* %4, %"class.draco::IndexType.125"** nonnull align 4 dereferenceable(4) %__begin_2)
  %6 = bitcast %"class.std::__2::vector.123"* %this1 to %"class.std::__2::__vector_base.124"*
  %__begin_3 = getelementptr inbounds %"class.std::__2::__vector_base.124", %"class.std::__2::__vector_base.124"* %6, i32 0, i32 0
  %7 = load %"struct.std::__2::__split_buffer.141"*, %"struct.std::__2::__split_buffer.141"** %__v.addr, align 4
  %__begin_4 = getelementptr inbounds %"struct.std::__2::__split_buffer.141", %"struct.std::__2::__split_buffer.141"* %7, i32 0, i32 1
  call void @_ZNSt3__24swapIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS7_EE5valueEvE4typeERS7_SA_(%"class.draco::IndexType.125"** nonnull align 4 dereferenceable(4) %__begin_3, %"class.draco::IndexType.125"** nonnull align 4 dereferenceable(4) %__begin_4) #9
  %8 = bitcast %"class.std::__2::vector.123"* %this1 to %"class.std::__2::__vector_base.124"*
  %__end_5 = getelementptr inbounds %"class.std::__2::__vector_base.124", %"class.std::__2::__vector_base.124"* %8, i32 0, i32 1
  %9 = load %"struct.std::__2::__split_buffer.141"*, %"struct.std::__2::__split_buffer.141"** %__v.addr, align 4
  %__end_6 = getelementptr inbounds %"struct.std::__2::__split_buffer.141", %"struct.std::__2::__split_buffer.141"* %9, i32 0, i32 2
  call void @_ZNSt3__24swapIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS7_EE5valueEvE4typeERS7_SA_(%"class.draco::IndexType.125"** nonnull align 4 dereferenceable(4) %__end_5, %"class.draco::IndexType.125"** nonnull align 4 dereferenceable(4) %__end_6) #9
  %10 = bitcast %"class.std::__2::vector.123"* %this1 to %"class.std::__2::__vector_base.124"*
  %call7 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.125"** @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base.124"* %10) #9
  %11 = load %"struct.std::__2::__split_buffer.141"*, %"struct.std::__2::__split_buffer.141"** %__v.addr, align 4
  %call8 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.125"** @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE9__end_capEv(%"struct.std::__2::__split_buffer.141"* %11) #9
  call void @_ZNSt3__24swapIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS7_EE5valueEvE4typeERS7_SA_(%"class.draco::IndexType.125"** nonnull align 4 dereferenceable(4) %call7, %"class.draco::IndexType.125"** nonnull align 4 dereferenceable(4) %call8) #9
  %12 = load %"struct.std::__2::__split_buffer.141"*, %"struct.std::__2::__split_buffer.141"** %__v.addr, align 4
  %__begin_9 = getelementptr inbounds %"struct.std::__2::__split_buffer.141", %"struct.std::__2::__split_buffer.141"* %12, i32 0, i32 1
  %13 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %__begin_9, align 4
  %14 = load %"struct.std::__2::__split_buffer.141"*, %"struct.std::__2::__split_buffer.141"** %__v.addr, align 4
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer.141", %"struct.std::__2::__split_buffer.141"* %14, i32 0, i32 0
  store %"class.draco::IndexType.125"* %13, %"class.draco::IndexType.125"** %__first_, align 4
  %call10 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.123"* %this1) #9
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE14__annotate_newEm(%"class.std::__2::vector.123"* %this1, i32 %call10) #9
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector.123"* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer.141"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEED2Ev(%"struct.std::__2::__split_buffer.141"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"struct.std::__2::__split_buffer.141"*, align 4
  %this.addr = alloca %"struct.std::__2::__split_buffer.141"*, align 4
  store %"struct.std::__2::__split_buffer.141"* %this, %"struct.std::__2::__split_buffer.141"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.141"*, %"struct.std::__2::__split_buffer.141"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer.141"* %this1, %"struct.std::__2::__split_buffer.141"** %retval, align 4
  call void @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE5clearEv(%"struct.std::__2::__split_buffer.141"* %this1) #9
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer.141", %"struct.std::__2::__split_buffer.141"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %__first_, align 4
  %tobool = icmp ne %"class.draco::IndexType.125"* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.129"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE7__allocEv(%"struct.std::__2::__split_buffer.141"* %this1) #9
  %__first_2 = getelementptr inbounds %"struct.std::__2::__split_buffer.141", %"struct.std::__2::__split_buffer.141"* %this1, i32 0, i32 0
  %1 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %__first_2, align 4
  %call3 = call i32 @_ZNKSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE8capacityEv(%"struct.std::__2::__split_buffer.141"* %this1)
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE10deallocateERS6_PS5_m(%"class.std::__2::allocator.129"* nonnull align 1 dereferenceable(1) %call, %"class.draco::IndexType.125"* %1, i32 %call3) #9
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"struct.std::__2::__split_buffer.141"*, %"struct.std::__2::__split_buffer.141"** %retval, align 4
  ret %"struct.std::__2::__split_buffer.141"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.125"** @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.126"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.126"*, align 4
  store %"class.std::__2::__compressed_pair.126"* %this, %"class.std::__2::__compressed_pair.126"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.126"*, %"class.std::__2::__compressed_pair.126"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.126"* %this1 to %"struct.std::__2::__compressed_pair_elem.127"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.125"** @_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.127"* %0) #9
  ret %"class.draco::IndexType.125"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.125"** @_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.127"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.127"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.127"* %this, %"struct.std::__2::__compressed_pair_elem.127"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.127"*, %"struct.std::__2::__compressed_pair_elem.127"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.127", %"struct.std::__2::__compressed_pair_elem.127"* %this1, i32 0, i32 0
  ret %"class.draco::IndexType.125"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionC2ERS7_m(%"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"* returned %this, %"class.std::__2::vector.123"* nonnull align 4 dereferenceable(12) %__v, i32 %__n) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"*, align 4
  %__v.addr = alloca %"class.std::__2::vector.123"*, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"** %this.addr, align 4
  store %"class.std::__2::vector.123"* %__v, %"class.std::__2::vector.123"** %__v.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"*, %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"** %this.addr, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::vector.123"*, %"class.std::__2::vector.123"** %__v.addr, align 4
  store %"class.std::__2::vector.123"* %0, %"class.std::__2::vector.123"** %__v_, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %1 = load %"class.std::__2::vector.123"*, %"class.std::__2::vector.123"** %__v.addr, align 4
  %2 = bitcast %"class.std::__2::vector.123"* %1 to %"class.std::__2::__vector_base.124"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.124", %"class.std::__2::__vector_base.124"* %2, i32 0, i32 1
  %3 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %__end_, align 4
  store %"class.draco::IndexType.125"* %3, %"class.draco::IndexType.125"** %__pos_, align 4
  %__new_end_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"* %this1, i32 0, i32 2
  %4 = load %"class.std::__2::vector.123"*, %"class.std::__2::vector.123"** %__v.addr, align 4
  %5 = bitcast %"class.std::__2::vector.123"* %4 to %"class.std::__2::__vector_base.124"*
  %__end_2 = getelementptr inbounds %"class.std::__2::__vector_base.124", %"class.std::__2::__vector_base.124"* %5, i32 0, i32 1
  %6 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %__end_2, align 4
  %7 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %6, i32 %7
  store %"class.draco::IndexType.125"* %add.ptr, %"class.draco::IndexType.125"** %__new_end_, align 4
  ret %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE9constructIS5_JEEEvRS6_PT_DpOT0_(%"class.std::__2::allocator.129"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType.125"* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.129"*, align 4
  %__p.addr = alloca %"class.draco::IndexType.125"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_construct.144", align 1
  store %"class.std::__2::allocator.129"* %__a, %"class.std::__2::allocator.129"** %__a.addr, align 4
  store %"class.draco::IndexType.125"* %__p, %"class.draco::IndexType.125"** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_construct.144"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.129"*, %"class.std::__2::allocator.129"** %__a.addr, align 4
  %2 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE11__constructIS5_JEEEvNS_17integral_constantIbLb1EEERS6_PT_DpOT0_(%"class.std::__2::allocator.129"* nonnull align 1 dereferenceable(1) %1, %"class.draco::IndexType.125"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType.125"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType.125"* %__p) #0 comdat {
entry:
  %__p.addr = alloca %"class.draco::IndexType.125"*, align 4
  store %"class.draco::IndexType.125"* %__p, %"class.draco::IndexType.125"** %__p.addr, align 4
  %0 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %__p.addr, align 4
  ret %"class.draco::IndexType.125"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"*, align 4
  store %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"** %this.addr, align 4
  %this1 = load %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"*, %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %0 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %__pos_, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::vector.123"*, %"class.std::__2::vector.123"** %__v_, align 4
  %2 = bitcast %"class.std::__2::vector.123"* %1 to %"class.std::__2::__vector_base.124"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.124", %"class.std::__2::__vector_base.124"* %2, i32 0, i32 1
  store %"class.draco::IndexType.125"* %0, %"class.draco::IndexType.125"** %__end_, align 4
  ret %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE11__constructIS5_JEEEvNS_17integral_constantIbLb1EEERS6_PT_DpOT0_(%"class.std::__2::allocator.129"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType.125"* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.129"*, align 4
  %__p.addr = alloca %"class.draco::IndexType.125"*, align 4
  store %"class.std::__2::allocator.129"* %__a, %"class.std::__2::allocator.129"** %__a.addr, align 4
  store %"class.draco::IndexType.125"* %__p, %"class.draco::IndexType.125"** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.129"*, %"class.std::__2::allocator.129"** %__a.addr, align 4
  %2 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %__p.addr, align 4
  call void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEE9constructIS4_JEEEvPT_DpOT0_(%"class.std::__2::allocator.129"* %1, %"class.draco::IndexType.125"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEE9constructIS4_JEEEvPT_DpOT0_(%"class.std::__2::allocator.129"* %this, %"class.draco::IndexType.125"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.129"*, align 4
  %__p.addr = alloca %"class.draco::IndexType.125"*, align 4
  store %"class.std::__2::allocator.129"* %this, %"class.std::__2::allocator.129"** %this.addr, align 4
  store %"class.draco::IndexType.125"* %__p, %"class.draco::IndexType.125"** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.129"*, %"class.std::__2::allocator.129"** %this.addr, align 4
  %0 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %__p.addr, align 4
  %1 = bitcast %"class.draco::IndexType.125"* %0 to i8*
  %2 = bitcast i8* %1 to %"class.draco::IndexType.125"*
  %call = call %"class.draco::IndexType.125"* @_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEC2Ev(%"class.draco::IndexType.125"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType.125"* @_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEC2Ev(%"class.draco::IndexType.125"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.125"*, align 4
  store %"class.draco::IndexType.125"* %this, %"class.draco::IndexType.125"** %this.addr, align 4
  %this1 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %this1, i32 0, i32 0
  store i32 0, i32* %value_, align 4
  ret %"class.draco::IndexType.125"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.129"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.126"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.126"*, align 4
  store %"class.std::__2::__compressed_pair.126"* %this, %"class.std::__2::__compressed_pair.126"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.126"*, %"class.std::__2::__compressed_pair.126"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.126"* %this1 to %"struct.std::__2::__compressed_pair_elem.128"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.129"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.128"* %0) #9
  ret %"class.std::__2::allocator.129"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.129"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.128"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.128"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.128"* %this, %"struct.std::__2::__compressed_pair_elem.128"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.128"*, %"struct.std::__2::__compressed_pair_elem.128"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.128"* %this1 to %"class.std::__2::allocator.129"*
  ret %"class.std::__2::allocator.129"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE8max_sizeEv(%"class.std::__2::vector.123"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.123"*, align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp3 = alloca i32, align 4
  store %"class.std::__2::vector.123"* %this, %"class.std::__2::vector.123"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.123"*, %"class.std::__2::vector.123"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.123"* %this1 to %"class.std::__2::__vector_base.124"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.129"* @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.124"* %0) #9
  %call2 = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE8max_sizeERKS6_(%"class.std::__2::allocator.129"* nonnull align 1 dereferenceable(1) %call) #9
  store i32 %call2, i32* %ref.tmp, align 4
  %call4 = call i32 @_ZNSt3__214numeric_limitsIlE3maxEv() #9
  store i32 %call4, i32* %ref.tmp3, align 4
  %call5 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %1 = load i32, i32* %call5, align 4
  ret i32 %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.123"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.123"*, align 4
  store %"class.std::__2::vector.123"* %this, %"class.std::__2::vector.123"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.123"*, %"class.std::__2::vector.123"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.123"* %this1 to %"class.std::__2::__vector_base.124"*
  %call = call i32 @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::__vector_base.124"* %0) #9
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE8max_sizeERKS6_(%"class.std::__2::allocator.129"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.129"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_max_size.145", align 1
  store %"class.std::__2::allocator.129"* %__a, %"class.std::__2::allocator.129"** %__a.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_max_size.145"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.129"*, %"class.std::__2::allocator.129"** %__a.addr, align 4
  %call = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS6_(%"class.std::__2::allocator.129"* nonnull align 1 dereferenceable(1) %1) #9
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.129"* @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.124"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.124"*, align 4
  store %"class.std::__2::__vector_base.124"* %this, %"class.std::__2::__vector_base.124"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.124"*, %"class.std::__2::__vector_base.124"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.124", %"class.std::__2::__vector_base.124"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.129"* @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.126"* %__end_cap_) #9
  ret %"class.std::__2::allocator.129"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS6_(%"class.std::__2::allocator.129"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.129"*, align 4
  store %"class.std::__2::allocator.129"* %__a, %"class.std::__2::allocator.129"** %__a.addr, align 4
  %1 = load %"class.std::__2::allocator.129"*, %"class.std::__2::allocator.129"** %__a.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEE8max_sizeEv(%"class.std::__2::allocator.129"* %1) #9
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__29allocatorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEE8max_sizeEv(%"class.std::__2::allocator.129"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.129"*, align 4
  store %"class.std::__2::allocator.129"* %this, %"class.std::__2::allocator.129"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.129"*, %"class.std::__2::allocator.129"** %this.addr, align 4
  ret i32 1073741823
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.129"* @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.126"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.126"*, align 4
  store %"class.std::__2::__compressed_pair.126"* %this, %"class.std::__2::__compressed_pair.126"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.126"*, %"class.std::__2::__compressed_pair.126"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.126"* %this1 to %"struct.std::__2::__compressed_pair_elem.128"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.129"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.128"* %0) #9
  ret %"class.std::__2::allocator.129"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.129"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.128"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.128"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.128"* %this, %"struct.std::__2::__compressed_pair_elem.128"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.128"*, %"struct.std::__2::__compressed_pair_elem.128"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.128"* %this1 to %"class.std::__2::allocator.129"*
  ret %"class.std::__2::allocator.129"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::__vector_base.124"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.124"*, align 4
  store %"class.std::__2::__vector_base.124"* %this, %"class.std::__2::__vector_base.124"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.124"*, %"class.std::__2::__vector_base.124"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.125"** @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base.124"* %this1) #9
  %0 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.124", %"class.std::__2::__vector_base.124"* %this1, i32 0, i32 0
  %1 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.draco::IndexType.125"* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.draco::IndexType.125"* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.125"** @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base.124"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.124"*, align 4
  store %"class.std::__2::__vector_base.124"* %this, %"class.std::__2::__vector_base.124"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.124"*, %"class.std::__2::__vector_base.124"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.124", %"class.std::__2::__vector_base.124"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.125"** @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.126"* %__end_cap_) #9
  ret %"class.draco::IndexType.125"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.125"** @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.126"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.126"*, align 4
  store %"class.std::__2::__compressed_pair.126"* %this, %"class.std::__2::__compressed_pair.126"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.126"*, %"class.std::__2::__compressed_pair.126"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.126"* %this1 to %"struct.std::__2::__compressed_pair_elem.127"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.125"** @_ZNKSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.127"* %0) #9
  ret %"class.draco::IndexType.125"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.125"** @_ZNKSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.127"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.127"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.127"* %this, %"struct.std::__2::__compressed_pair_elem.127"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.127"*, %"struct.std::__2::__compressed_pair_elem.127"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.127", %"struct.std::__2::__compressed_pair_elem.127"* %this1, i32 0, i32 0
  ret %"class.draco::IndexType.125"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.142"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEEC2IDnS8_EEOT_OT0_(%"class.std::__2::__compressed_pair.142"* returned %this, i8** nonnull align 4 dereferenceable(4) %__t1, %"class.std::__2::allocator.129"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.142"*, align 4
  %__t1.addr = alloca i8**, align 4
  %__t2.addr = alloca %"class.std::__2::allocator.129"*, align 4
  store %"class.std::__2::__compressed_pair.142"* %this, %"class.std::__2::__compressed_pair.142"** %this.addr, align 4
  store i8** %__t1, i8*** %__t1.addr, align 4
  store %"class.std::__2::allocator.129"* %__t2, %"class.std::__2::allocator.129"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.142"*, %"class.std::__2::__compressed_pair.142"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.142"* %this1 to %"struct.std::__2::__compressed_pair_elem.127"*
  %1 = load i8**, i8*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %1) #9
  %call2 = call %"struct.std::__2::__compressed_pair_elem.127"* @_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.127"* %0, i8** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.142"* %this1 to i8*
  %3 = getelementptr inbounds i8, i8* %2, i32 4
  %4 = bitcast i8* %3 to %"struct.std::__2::__compressed_pair_elem.143"*
  %5 = load %"class.std::__2::allocator.129"*, %"class.std::__2::allocator.129"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.129"* @_ZNSt3__27forwardIRNS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEEEOT_RNS_16remove_referenceIS8_E4typeE(%"class.std::__2::allocator.129"* nonnull align 1 dereferenceable(1) %5) #9
  %call4 = call %"struct.std::__2::__compressed_pair_elem.143"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEELi1ELb0EEC2IS7_vEEOT_(%"struct.std::__2::__compressed_pair_elem.143"* %4, %"class.std::__2::allocator.129"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::__compressed_pair.142"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType.125"* @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE8allocateERS6_m(%"class.std::__2::allocator.129"* nonnull align 1 dereferenceable(1) %__a, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.129"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.129"* %__a, %"class.std::__2::allocator.129"** %__a.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.129"*, %"class.std::__2::allocator.129"** %__a.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call %"class.draco::IndexType.125"* @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEE8allocateEmPKv(%"class.std::__2::allocator.129"* %0, i32 %1, i8* null)
  ret %"class.draco::IndexType.125"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.129"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE7__allocEv(%"struct.std::__2::__split_buffer.141"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.141"*, align 4
  store %"struct.std::__2::__split_buffer.141"* %this, %"struct.std::__2::__split_buffer.141"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.141"*, %"struct.std::__2::__split_buffer.141"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer.141", %"struct.std::__2::__split_buffer.141"* %this1, i32 0, i32 3
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.129"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.142"* %__end_cap_) #9
  ret %"class.std::__2::allocator.129"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.125"** @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE9__end_capEv(%"struct.std::__2::__split_buffer.141"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.141"*, align 4
  store %"struct.std::__2::__split_buffer.141"* %this, %"struct.std::__2::__split_buffer.141"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.141"*, %"struct.std::__2::__split_buffer.141"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer.141", %"struct.std::__2::__split_buffer.141"* %this1, i32 0, i32 3
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.125"** @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.142"* %__end_cap_) #9
  ret %"class.draco::IndexType.125"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.127"* @_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.127"* returned %this, i8** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.127"*, align 4
  %__u.addr = alloca i8**, align 4
  store %"struct.std::__2::__compressed_pair_elem.127"* %this, %"struct.std::__2::__compressed_pair_elem.127"** %this.addr, align 4
  store i8** %__u, i8*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.127"*, %"struct.std::__2::__compressed_pair_elem.127"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.127", %"struct.std::__2::__compressed_pair_elem.127"* %this1, i32 0, i32 0
  %0 = load i8**, i8*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %0) #9
  store %"class.draco::IndexType.125"* null, %"class.draco::IndexType.125"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.127"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.129"* @_ZNSt3__27forwardIRNS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEEEOT_RNS_16remove_referenceIS8_E4typeE(%"class.std::__2::allocator.129"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::allocator.129"*, align 4
  store %"class.std::__2::allocator.129"* %__t, %"class.std::__2::allocator.129"** %__t.addr, align 4
  %0 = load %"class.std::__2::allocator.129"*, %"class.std::__2::allocator.129"** %__t.addr, align 4
  ret %"class.std::__2::allocator.129"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.143"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEELi1ELb0EEC2IS7_vEEOT_(%"struct.std::__2::__compressed_pair_elem.143"* returned %this, %"class.std::__2::allocator.129"* nonnull align 1 dereferenceable(1) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.143"*, align 4
  %__u.addr = alloca %"class.std::__2::allocator.129"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.143"* %this, %"struct.std::__2::__compressed_pair_elem.143"** %this.addr, align 4
  store %"class.std::__2::allocator.129"* %__u, %"class.std::__2::allocator.129"** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.143"*, %"struct.std::__2::__compressed_pair_elem.143"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.143", %"struct.std::__2::__compressed_pair_elem.143"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::allocator.129"*, %"class.std::__2::allocator.129"** %__u.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.129"* @_ZNSt3__27forwardIRNS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEEEOT_RNS_16remove_referenceIS8_E4typeE(%"class.std::__2::allocator.129"* nonnull align 1 dereferenceable(1) %0) #9
  store %"class.std::__2::allocator.129"* %call, %"class.std::__2::allocator.129"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.143"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType.125"* @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEE8allocateEmPKv(%"class.std::__2::allocator.129"* %this, i32 %__n, i8* %0) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.129"*, align 4
  %__n.addr = alloca i32, align 4
  %.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.129"* %this, %"class.std::__2::allocator.129"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::allocator.129"*, %"class.std::__2::allocator.129"** %this.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEE8max_sizeEv(%"class.std::__2::allocator.129"* %this1) #9
  %cmp = icmp ugt i32 %1, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__220__throw_length_errorEPKc(i8* getelementptr inbounds ([68 x i8], [68 x i8]* @.str, i32 0, i32 0)) #11
  unreachable

if.end:                                           ; preds = %entry
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  %call2 = call i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %mul, i32 4)
  %3 = bitcast i8* %call2 to %"class.draco::IndexType.125"*
  ret %"class.draco::IndexType.125"* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.129"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.142"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.142"*, align 4
  store %"class.std::__2::__compressed_pair.142"* %this, %"class.std::__2::__compressed_pair.142"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.142"*, %"class.std::__2::__compressed_pair.142"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.142"* %this1 to i8*
  %add.ptr = getelementptr inbounds i8, i8* %0, i32 4
  %1 = bitcast i8* %add.ptr to %"struct.std::__2::__compressed_pair_elem.143"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.129"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.143"* %1) #9
  ret %"class.std::__2::allocator.129"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.129"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.143"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.143"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.143"* %this, %"struct.std::__2::__compressed_pair_elem.143"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.143"*, %"struct.std::__2::__compressed_pair_elem.143"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.143", %"struct.std::__2::__compressed_pair_elem.143"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::allocator.129"*, %"class.std::__2::allocator.129"** %__value_, align 4
  ret %"class.std::__2::allocator.129"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.125"** @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.142"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.142"*, align 4
  store %"class.std::__2::__compressed_pair.142"* %this, %"class.std::__2::__compressed_pair.142"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.142"*, %"class.std::__2::__compressed_pair.142"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.142"* %this1 to %"struct.std::__2::__compressed_pair_elem.127"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.125"** @_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.127"* %0) #9
  ret %"class.draco::IndexType.125"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE21_ConstructTransactionC2EPPS4_m(%"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction"* returned %this, %"class.draco::IndexType.125"** %__p, i32 %__n) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction"*, align 4
  %__p.addr = alloca %"class.draco::IndexType.125"**, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction"* %this, %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction"** %this.addr, align 4
  store %"class.draco::IndexType.125"** %__p, %"class.draco::IndexType.125"*** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction"*, %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexType.125"**, %"class.draco::IndexType.125"*** %__p.addr, align 4
  %1 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %0, align 4
  store %"class.draco::IndexType.125"* %1, %"class.draco::IndexType.125"** %__pos_, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction"* %this1, i32 0, i32 1
  %2 = load %"class.draco::IndexType.125"**, %"class.draco::IndexType.125"*** %__p.addr, align 4
  %3 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %2, align 4
  %4 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %3, i32 %4
  store %"class.draco::IndexType.125"* %add.ptr, %"class.draco::IndexType.125"** %__end_, align 4
  %__dest_ = getelementptr inbounds %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction"* %this1, i32 0, i32 2
  %5 = load %"class.draco::IndexType.125"**, %"class.draco::IndexType.125"*** %__p.addr, align 4
  store %"class.draco::IndexType.125"** %5, %"class.draco::IndexType.125"*** %__dest_, align 4
  ret %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction"*, align 4
  store %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction"* %this, %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction"*, %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %__pos_, align 4
  %__dest_ = getelementptr inbounds %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction"* %this1, i32 0, i32 2
  %1 = load %"class.draco::IndexType.125"**, %"class.draco::IndexType.125"*** %__dest_, align 4
  store %"class.draco::IndexType.125"* %0, %"class.draco::IndexType.125"** %1, align 4
  ret %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>> &>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_deleteEv(%"class.std::__2::vector.123"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.123"*, align 4
  store %"class.std::__2::vector.123"* %this, %"class.std::__2::vector.123"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.123"*, %"class.std::__2::vector.123"** %this.addr, align 4
  %call = call %"class.draco::IndexType.125"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.123"* %this1) #9
  %0 = bitcast %"class.draco::IndexType.125"* %call to i8*
  %call2 = call %"class.draco::IndexType.125"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.123"* %this1) #9
  %call3 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.123"* %this1) #9
  %add.ptr = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %call2, i32 %call3
  %1 = bitcast %"class.draco::IndexType.125"* %add.ptr to i8*
  %call4 = call %"class.draco::IndexType.125"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.123"* %this1) #9
  %call5 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.123"* %this1) #9
  %add.ptr6 = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %call4, i32 %call5
  %2 = bitcast %"class.draco::IndexType.125"* %add.ptr6 to i8*
  %call7 = call %"class.draco::IndexType.125"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.123"* %this1) #9
  %call8 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.123"* %this1) #9
  %add.ptr9 = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %call7, i32 %call8
  %3 = bitcast %"class.draco::IndexType.125"* %add.ptr9 to i8*
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_(%"class.std::__2::vector.123"* %this1, i8* %0, i8* %1, i8* %2, i8* %3) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE46__construct_backward_with_exception_guaranteesIS5_EENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS6_PT_SC_EE5valuesr31is_trivially_move_constructibleISC_EE5valueEvE4typeERS6_SD_SD_RSD_(%"class.std::__2::allocator.129"* nonnull align 1 dereferenceable(1) %0, %"class.draco::IndexType.125"* %__begin1, %"class.draco::IndexType.125"* %__end1, %"class.draco::IndexType.125"** nonnull align 4 dereferenceable(4) %__end2) #0 comdat {
entry:
  %.addr = alloca %"class.std::__2::allocator.129"*, align 4
  %__begin1.addr = alloca %"class.draco::IndexType.125"*, align 4
  %__end1.addr = alloca %"class.draco::IndexType.125"*, align 4
  %__end2.addr = alloca %"class.draco::IndexType.125"**, align 4
  %_Np = alloca i32, align 4
  store %"class.std::__2::allocator.129"* %0, %"class.std::__2::allocator.129"** %.addr, align 4
  store %"class.draco::IndexType.125"* %__begin1, %"class.draco::IndexType.125"** %__begin1.addr, align 4
  store %"class.draco::IndexType.125"* %__end1, %"class.draco::IndexType.125"** %__end1.addr, align 4
  store %"class.draco::IndexType.125"** %__end2, %"class.draco::IndexType.125"*** %__end2.addr, align 4
  %1 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %__end1.addr, align 4
  %2 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %__begin1.addr, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.draco::IndexType.125"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.draco::IndexType.125"* %2 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  store i32 %sub.ptr.div, i32* %_Np, align 4
  %3 = load i32, i32* %_Np, align 4
  %4 = load %"class.draco::IndexType.125"**, %"class.draco::IndexType.125"*** %__end2.addr, align 4
  %5 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %4, align 4
  %idx.neg = sub i32 0, %3
  %add.ptr = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %5, i32 %idx.neg
  store %"class.draco::IndexType.125"* %add.ptr, %"class.draco::IndexType.125"** %4, align 4
  %6 = load i32, i32* %_Np, align 4
  %cmp = icmp sgt i32 %6, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %7 = load %"class.draco::IndexType.125"**, %"class.draco::IndexType.125"*** %__end2.addr, align 4
  %8 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %7, align 4
  %9 = bitcast %"class.draco::IndexType.125"* %8 to i8*
  %10 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %__begin1.addr, align 4
  %11 = bitcast %"class.draco::IndexType.125"* %10 to i8*
  %12 = load i32, i32* %_Np, align 4
  %mul = mul i32 %12, 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %11, i32 %mul, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__24swapIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS7_EE5valueEvE4typeERS7_SA_(%"class.draco::IndexType.125"** nonnull align 4 dereferenceable(4) %__x, %"class.draco::IndexType.125"** nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %__x.addr = alloca %"class.draco::IndexType.125"**, align 4
  %__y.addr = alloca %"class.draco::IndexType.125"**, align 4
  %__t = alloca %"class.draco::IndexType.125"*, align 4
  store %"class.draco::IndexType.125"** %__x, %"class.draco::IndexType.125"*** %__x.addr, align 4
  store %"class.draco::IndexType.125"** %__y, %"class.draco::IndexType.125"*** %__y.addr, align 4
  %0 = load %"class.draco::IndexType.125"**, %"class.draco::IndexType.125"*** %__x.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.125"** @_ZNSt3__24moveIRPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEEEONS_16remove_referenceIT_E4typeEOS8_(%"class.draco::IndexType.125"** nonnull align 4 dereferenceable(4) %0) #9
  %1 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %call, align 4
  store %"class.draco::IndexType.125"* %1, %"class.draco::IndexType.125"** %__t, align 4
  %2 = load %"class.draco::IndexType.125"**, %"class.draco::IndexType.125"*** %__y.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.125"** @_ZNSt3__24moveIRPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEEEONS_16remove_referenceIT_E4typeEOS8_(%"class.draco::IndexType.125"** nonnull align 4 dereferenceable(4) %2) #9
  %3 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %call1, align 4
  %4 = load %"class.draco::IndexType.125"**, %"class.draco::IndexType.125"*** %__x.addr, align 4
  store %"class.draco::IndexType.125"* %3, %"class.draco::IndexType.125"** %4, align 4
  %call2 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.125"** @_ZNSt3__24moveIRPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEEEONS_16remove_referenceIT_E4typeEOS8_(%"class.draco::IndexType.125"** nonnull align 4 dereferenceable(4) %__t) #9
  %5 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %call2, align 4
  %6 = load %"class.draco::IndexType.125"**, %"class.draco::IndexType.125"*** %__y.addr, align 4
  store %"class.draco::IndexType.125"* %5, %"class.draco::IndexType.125"** %6, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE14__annotate_newEm(%"class.std::__2::vector.123"* %this, i32 %__current_size) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.123"*, align 4
  %__current_size.addr = alloca i32, align 4
  store %"class.std::__2::vector.123"* %this, %"class.std::__2::vector.123"** %this.addr, align 4
  store i32 %__current_size, i32* %__current_size.addr, align 4
  %this1 = load %"class.std::__2::vector.123"*, %"class.std::__2::vector.123"** %this.addr, align 4
  %call = call %"class.draco::IndexType.125"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.123"* %this1) #9
  %0 = bitcast %"class.draco::IndexType.125"* %call to i8*
  %call2 = call %"class.draco::IndexType.125"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.123"* %this1) #9
  %call3 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.123"* %this1) #9
  %add.ptr = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %call2, i32 %call3
  %1 = bitcast %"class.draco::IndexType.125"* %add.ptr to i8*
  %call4 = call %"class.draco::IndexType.125"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.123"* %this1) #9
  %call5 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.123"* %this1) #9
  %add.ptr6 = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %call4, i32 %call5
  %2 = bitcast %"class.draco::IndexType.125"* %add.ptr6 to i8*
  %call7 = call %"class.draco::IndexType.125"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.123"* %this1) #9
  %3 = load i32, i32* %__current_size.addr, align 4
  %add.ptr8 = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %call7, i32 %3
  %4 = bitcast %"class.draco::IndexType.125"* %add.ptr8 to i8*
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_(%"class.std::__2::vector.123"* %this1, i8* %0, i8* %1, i8* %2, i8* %4) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector.123"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.123"*, align 4
  store %"class.std::__2::vector.123"* %this, %"class.std::__2::vector.123"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.123"*, %"class.std::__2::vector.123"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_(%"class.std::__2::vector.123"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.123"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector.123"* %this, %"class.std::__2::vector.123"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector.123"*, %"class.std::__2::vector.123"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType.125"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.123"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.123"*, align 4
  store %"class.std::__2::vector.123"* %this, %"class.std::__2::vector.123"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.123"*, %"class.std::__2::vector.123"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.123"* %this1 to %"class.std::__2::__vector_base.124"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.124", %"class.std::__2::__vector_base.124"* %0, i32 0, i32 0
  %1 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %__begin_, align 4
  %call = call %"class.draco::IndexType.125"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType.125"* %1) #9
  ret %"class.draco::IndexType.125"* %call
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #7

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.125"** @_ZNSt3__24moveIRPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEEEONS_16remove_referenceIT_E4typeEOS8_(%"class.draco::IndexType.125"** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.draco::IndexType.125"**, align 4
  store %"class.draco::IndexType.125"** %__t, %"class.draco::IndexType.125"*** %__t.addr, align 4
  %0 = load %"class.draco::IndexType.125"**, %"class.draco::IndexType.125"*** %__t.addr, align 4
  ret %"class.draco::IndexType.125"** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE5clearEv(%"struct.std::__2::__split_buffer.141"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.141"*, align 4
  store %"struct.std::__2::__split_buffer.141"* %this, %"struct.std::__2::__split_buffer.141"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.141"*, %"struct.std::__2::__split_buffer.141"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"struct.std::__2::__split_buffer.141", %"struct.std::__2::__split_buffer.141"* %this1, i32 0, i32 1
  %0 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %__begin_, align 4
  call void @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE17__destruct_at_endEPS4_(%"struct.std::__2::__split_buffer.141"* %this1, %"class.draco::IndexType.125"* %0) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE10deallocateERS6_PS5_m(%"class.std::__2::allocator.129"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType.125"* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.129"*, align 4
  %__p.addr = alloca %"class.draco::IndexType.125"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.129"* %__a, %"class.std::__2::allocator.129"** %__a.addr, align 4
  store %"class.draco::IndexType.125"* %__p, %"class.draco::IndexType.125"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.129"*, %"class.std::__2::allocator.129"** %__a.addr, align 4
  %1 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEE10deallocateEPS4_m(%"class.std::__2::allocator.129"* %0, %"class.draco::IndexType.125"* %1, i32 %2) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE8capacityEv(%"struct.std::__2::__split_buffer.141"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.141"*, align 4
  store %"struct.std::__2::__split_buffer.141"* %this, %"struct.std::__2::__split_buffer.141"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.141"*, %"struct.std::__2::__split_buffer.141"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.125"** @_ZNKSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE9__end_capEv(%"struct.std::__2::__split_buffer.141"* %this1) #9
  %0 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %call, align 4
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer.141", %"struct.std::__2::__split_buffer.141"* %this1, i32 0, i32 0
  %1 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %__first_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.draco::IndexType.125"* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.draco::IndexType.125"* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE17__destruct_at_endEPS4_(%"struct.std::__2::__split_buffer.141"* %this, %"class.draco::IndexType.125"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.141"*, align 4
  %__new_last.addr = alloca %"class.draco::IndexType.125"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant.139", align 1
  store %"struct.std::__2::__split_buffer.141"* %this, %"struct.std::__2::__split_buffer.141"** %this.addr, align 4
  store %"class.draco::IndexType.125"* %__new_last, %"class.draco::IndexType.125"** %__new_last.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.141"*, %"struct.std::__2::__split_buffer.141"** %this.addr, align 4
  %0 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %__new_last.addr, align 4
  call void @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE17__destruct_at_endEPS4_NS_17integral_constantIbLb0EEE(%"struct.std::__2::__split_buffer.141"* %this1, %"class.draco::IndexType.125"* %0) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE17__destruct_at_endEPS4_NS_17integral_constantIbLb0EEE(%"struct.std::__2::__split_buffer.141"* %this, %"class.draco::IndexType.125"* %__new_last) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant.139", align 1
  %this.addr = alloca %"struct.std::__2::__split_buffer.141"*, align 4
  %__new_last.addr = alloca %"class.draco::IndexType.125"*, align 4
  store %"struct.std::__2::__split_buffer.141"* %this, %"struct.std::__2::__split_buffer.141"** %this.addr, align 4
  store %"class.draco::IndexType.125"* %__new_last, %"class.draco::IndexType.125"** %__new_last.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.141"*, %"struct.std::__2::__split_buffer.141"** %this.addr, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %__new_last.addr, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer.141", %"struct.std::__2::__split_buffer.141"* %this1, i32 0, i32 2
  %2 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %__end_, align 4
  %cmp = icmp ne %"class.draco::IndexType.125"* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.129"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE7__allocEv(%"struct.std::__2::__split_buffer.141"* %this1) #9
  %__end_2 = getelementptr inbounds %"struct.std::__2::__split_buffer.141", %"struct.std::__2::__split_buffer.141"* %this1, i32 0, i32 2
  %3 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %__end_2, align 4
  %incdec.ptr = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %3, i32 -1
  store %"class.draco::IndexType.125"* %incdec.ptr, %"class.draco::IndexType.125"** %__end_2, align 4
  %call3 = call %"class.draco::IndexType.125"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType.125"* %incdec.ptr) #9
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE7destroyIS5_EEvRS6_PT_(%"class.std::__2::allocator.129"* nonnull align 1 dereferenceable(1) %call, %"class.draco::IndexType.125"* %call3)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE7destroyIS5_EEvRS6_PT_(%"class.std::__2::allocator.129"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType.125"* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.129"*, align 4
  %__p.addr = alloca %"class.draco::IndexType.125"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy.146", align 1
  store %"class.std::__2::allocator.129"* %__a, %"class.std::__2::allocator.129"** %__a.addr, align 4
  store %"class.draco::IndexType.125"* %__p, %"class.draco::IndexType.125"** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy.146"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.129"*, %"class.std::__2::allocator.129"** %__a.addr, align 4
  %2 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE9__destroyIS5_EEvNS_17integral_constantIbLb1EEERS6_PT_(%"class.std::__2::allocator.129"* nonnull align 1 dereferenceable(1) %1, %"class.draco::IndexType.125"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE9__destroyIS5_EEvNS_17integral_constantIbLb1EEERS6_PT_(%"class.std::__2::allocator.129"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType.125"* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.129"*, align 4
  %__p.addr = alloca %"class.draco::IndexType.125"*, align 4
  store %"class.std::__2::allocator.129"* %__a, %"class.std::__2::allocator.129"** %__a.addr, align 4
  store %"class.draco::IndexType.125"* %__p, %"class.draco::IndexType.125"** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.129"*, %"class.std::__2::allocator.129"** %__a.addr, align 4
  %2 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %__p.addr, align 4
  call void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEE7destroyEPS4_(%"class.std::__2::allocator.129"* %1, %"class.draco::IndexType.125"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEE7destroyEPS4_(%"class.std::__2::allocator.129"* %this, %"class.draco::IndexType.125"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.129"*, align 4
  %__p.addr = alloca %"class.draco::IndexType.125"*, align 4
  store %"class.std::__2::allocator.129"* %this, %"class.std::__2::allocator.129"** %this.addr, align 4
  store %"class.draco::IndexType.125"* %__p, %"class.draco::IndexType.125"** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.129"*, %"class.std::__2::allocator.129"** %this.addr, align 4
  %0 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %__p.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEE10deallocateEPS4_m(%"class.std::__2::allocator.129"* %this, %"class.draco::IndexType.125"* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.129"*, align 4
  %__p.addr = alloca %"class.draco::IndexType.125"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.129"* %this, %"class.std::__2::allocator.129"** %this.addr, align 4
  store %"class.draco::IndexType.125"* %__p, %"class.draco::IndexType.125"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.129"*, %"class.std::__2::allocator.129"** %this.addr, align 4
  %0 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %__p.addr, align 4
  %1 = bitcast %"class.draco::IndexType.125"* %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.125"** @_ZNKSt3__214__split_bufferIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE9__end_capEv(%"struct.std::__2::__split_buffer.141"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.141"*, align 4
  store %"struct.std::__2::__split_buffer.141"* %this, %"struct.std::__2::__split_buffer.141"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.141"*, %"struct.std::__2::__split_buffer.141"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer.141", %"struct.std::__2::__split_buffer.141"* %this1, i32 0, i32 3
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.125"** @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.142"* %__end_cap_) #9
  ret %"class.draco::IndexType.125"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.125"** @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEERNS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.142"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.142"*, align 4
  store %"class.std::__2::__compressed_pair.142"* %this, %"class.std::__2::__compressed_pair.142"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.142"*, %"class.std::__2::__compressed_pair.142"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.142"* %this1 to %"struct.std::__2::__compressed_pair_elem.127"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.125"** @_ZNKSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.127"* %0) #9
  ret %"class.draco::IndexType.125"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE27__invalidate_iterators_pastEPS4_(%"class.std::__2::vector.123"* %this, %"class.draco::IndexType.125"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.123"*, align 4
  %__new_last.addr = alloca %"class.draco::IndexType.125"*, align 4
  store %"class.std::__2::vector.123"* %this, %"class.std::__2::vector.123"** %this.addr, align 4
  store %"class.draco::IndexType.125"* %__new_last, %"class.draco::IndexType.125"** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::vector.123"*, %"class.std::__2::vector.123"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_(%"class.std::__2::__vector_base.124"* %this, %"class.draco::IndexType.125"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.124"*, align 4
  %__new_last.addr = alloca %"class.draco::IndexType.125"*, align 4
  %__soon_to_be_end = alloca %"class.draco::IndexType.125"*, align 4
  store %"class.std::__2::__vector_base.124"* %this, %"class.std::__2::__vector_base.124"** %this.addr, align 4
  store %"class.draco::IndexType.125"* %__new_last, %"class.draco::IndexType.125"** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.124"*, %"class.std::__2::__vector_base.124"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.124", %"class.std::__2::__vector_base.124"* %this1, i32 0, i32 1
  %0 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %__end_, align 4
  store %"class.draco::IndexType.125"* %0, %"class.draco::IndexType.125"** %__soon_to_be_end, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %__new_last.addr, align 4
  %2 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %__soon_to_be_end, align 4
  %cmp = icmp ne %"class.draco::IndexType.125"* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.129"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.124"* %this1) #9
  %3 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %__soon_to_be_end, align 4
  %incdec.ptr = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %3, i32 -1
  store %"class.draco::IndexType.125"* %incdec.ptr, %"class.draco::IndexType.125"** %__soon_to_be_end, align 4
  %call2 = call %"class.draco::IndexType.125"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType.125"* %incdec.ptr) #9
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE7destroyIS5_EEvRS6_PT_(%"class.std::__2::allocator.129"* nonnull align 1 dereferenceable(1) %call, %"class.draco::IndexType.125"* %call2)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %4 = load %"class.draco::IndexType.125"*, %"class.draco::IndexType.125"** %__new_last.addr, align 4
  %__end_3 = getelementptr inbounds %"class.std::__2::__vector_base.124", %"class.std::__2::__vector_base.124"* %this1, i32 0, i32 1
  store %"class.draco::IndexType.125"* %4, %"class.draco::IndexType.125"** %__end_3, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_shrinkEm(%"class.std::__2::vector.123"* %this, i32 %__old_size) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.123"*, align 4
  %__old_size.addr = alloca i32, align 4
  store %"class.std::__2::vector.123"* %this, %"class.std::__2::vector.123"** %this.addr, align 4
  store i32 %__old_size, i32* %__old_size.addr, align 4
  %this1 = load %"class.std::__2::vector.123"*, %"class.std::__2::vector.123"** %this.addr, align 4
  %call = call %"class.draco::IndexType.125"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.123"* %this1) #9
  %0 = bitcast %"class.draco::IndexType.125"* %call to i8*
  %call2 = call %"class.draco::IndexType.125"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.123"* %this1) #9
  %call3 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.123"* %this1) #9
  %add.ptr = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %call2, i32 %call3
  %1 = bitcast %"class.draco::IndexType.125"* %add.ptr to i8*
  %call4 = call %"class.draco::IndexType.125"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.123"* %this1) #9
  %2 = load i32, i32* %__old_size.addr, align 4
  %add.ptr5 = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %call4, i32 %2
  %3 = bitcast %"class.draco::IndexType.125"* %add.ptr5 to i8*
  %call6 = call %"class.draco::IndexType.125"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.123"* %this1) #9
  %call7 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.123"* %this1) #9
  %add.ptr8 = getelementptr inbounds %"class.draco::IndexType.125", %"class.draco::IndexType.125"* %call6, i32 %call7
  %4 = bitcast %"class.draco::IndexType.125"* %add.ptr8 to i8*
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_(%"class.std::__2::vector.123"* %this1, i8* %0, i8* %1, i8* %3, i8* %4) #9
  ret void
}

; Function Attrs: noreturn
declare void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_out_of_rangeEv(%"class.std::__2::__vector_base_common"*) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::PointCloudDecoder"* @_ZN5draco17PointCloudDecoderD2Ev(%"class.draco::PointCloudDecoder"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointCloudDecoder"*, align 4
  store %"class.draco::PointCloudDecoder"* %this, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::PointCloudDecoder"*, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  %0 = bitcast %"class.draco::PointCloudDecoder"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [11 x i8*] }, { [11 x i8*] }* @_ZTVN5draco17PointCloudDecoderE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %attribute_to_decoder_map_ = getelementptr inbounds %"class.draco::PointCloudDecoder", %"class.draco::PointCloudDecoder"* %this1, i32 0, i32 3
  %call = call %"class.std::__2::vector.87"* @_ZNSt3__26vectorIiNS_9allocatorIiEEED2Ev(%"class.std::__2::vector.87"* %attribute_to_decoder_map_) #9
  %attributes_decoders_ = getelementptr inbounds %"class.draco::PointCloudDecoder", %"class.draco::PointCloudDecoder"* %this1, i32 0, i32 2
  %call2 = call %"class.std::__2::vector.94"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEED2Ev(%"class.std::__2::vector.94"* %attributes_decoders_) #9
  ret %"class.draco::PointCloudDecoder"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector.87"* @_ZNSt3__26vectorIiNS_9allocatorIiEEED2Ev(%"class.std::__2::vector.87"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  call void @_ZNKSt3__26vectorIiNS_9allocatorIiEEE17__annotate_deleteEv(%"class.std::__2::vector.87"* %this1) #9
  %0 = bitcast %"class.std::__2::vector.87"* %this1 to %"class.std::__2::__vector_base.88"*
  %call = call %"class.std::__2::__vector_base.88"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEED2Ev(%"class.std::__2::__vector_base.88"* %0) #9
  ret %"class.std::__2::vector.87"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector.94"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEED2Ev(%"class.std::__2::vector.94"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  call void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__annotate_deleteEv(%"class.std::__2::vector.94"* %this1) #9
  %0 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %call = call %"class.std::__2::__vector_base.95"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEED2Ev(%"class.std::__2::__vector_base.95"* %0) #9
  ret %"class.std::__2::vector.94"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIiNS_9allocatorIiEEE17__annotate_deleteEv(%"class.std::__2::vector.87"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  %call = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector.87"* %this1) #9
  %0 = bitcast i32* %call to i8*
  %call2 = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector.87"* %this1) #9
  %call3 = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::vector.87"* %this1) #9
  %add.ptr = getelementptr inbounds i32, i32* %call2, i32 %call3
  %1 = bitcast i32* %add.ptr to i8*
  %call4 = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector.87"* %this1) #9
  %call5 = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4sizeEv(%"class.std::__2::vector.87"* %this1) #9
  %add.ptr6 = getelementptr inbounds i32, i32* %call4, i32 %call5
  %2 = bitcast i32* %add.ptr6 to i8*
  %call7 = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector.87"* %this1) #9
  %call8 = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::vector.87"* %this1) #9
  %add.ptr9 = getelementptr inbounds i32, i32* %call7, i32 %call8
  %3 = bitcast i32* %add.ptr9 to i8*
  call void @_ZNKSt3__26vectorIiNS_9allocatorIiEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector.87"* %this1, i8* %0, i8* %1, i8* %2, i8* %3) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base.88"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEED2Ev(%"class.std::__2::__vector_base.88"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__vector_base.88"*, align 4
  %this.addr = alloca %"class.std::__2::__vector_base.88"*, align 4
  store %"class.std::__2::__vector_base.88"* %this, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.88"*, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  store %"class.std::__2::__vector_base.88"* %this1, %"class.std::__2::__vector_base.88"** %retval, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__begin_, align 4
  %cmp = icmp ne i32* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE5clearEv(%"class.std::__2::__vector_base.88"* %this1) #9
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE7__allocEv(%"class.std::__2::__vector_base.88"* %this1) #9
  %__begin_2 = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %this1, i32 0, i32 0
  %1 = load i32*, i32** %__begin_2, align 4
  %call3 = call i32 @_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::__vector_base.88"* %this1) #9
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE10deallocateERS2_Pim(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %call, i32* %1, i32 %call3) #9
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"class.std::__2::__vector_base.88"*, %"class.std::__2::__vector_base.88"** %retval, align 4
  ret %"class.std::__2::__vector_base.88"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIiNS_9allocatorIiEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector.87"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector.87"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.87"* %this1 to %"class.std::__2::__vector_base.88"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %0, i32 0, i32 0
  %1 = load i32*, i32** %__begin_, align 4
  %call = call i32* @_ZNSt3__212__to_addressIiEEPT_S2_(i32* %1) #9
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::vector.87"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.87"* %this1 to %"class.std::__2::__vector_base.88"*
  %call = call i32 @_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::__vector_base.88"* %0) #9
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4sizeEv(%"class.std::__2::vector.87"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.87"* %this1 to %"class.std::__2::__vector_base.88"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %0, i32 0, i32 1
  %1 = load i32*, i32** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector.87"* %this1 to %"class.std::__2::__vector_base.88"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %2, i32 0, i32 0
  %3 = load i32*, i32** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint i32* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint i32* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNSt3__212__to_addressIiEEPT_S2_(i32* %__p) #0 comdat {
entry:
  %__p.addr = alloca i32*, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %0 = load i32*, i32** %__p.addr, align 4
  ret i32* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::__vector_base.88"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.88"*, align 4
  store %"class.std::__2::__vector_base.88"* %this, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.88"*, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE9__end_capEv(%"class.std::__2::__vector_base.88"* %this1) #9
  %0 = load i32*, i32** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %this1, i32 0, i32 0
  %1 = load i32*, i32** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint i32* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint i32* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE9__end_capEv(%"class.std::__2::__vector_base.88"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.88"*, align 4
  store %"class.std::__2::__vector_base.88"* %this, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.88"*, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__217__compressed_pairIPiNS_9allocatorIiEEE5firstEv(%"class.std::__2::__compressed_pair.89"* %__end_cap_) #9
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__217__compressed_pairIPiNS_9allocatorIiEEE5firstEv(%"class.std::__2::__compressed_pair.89"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.89"*, align 4
  store %"class.std::__2::__compressed_pair.89"* %this, %"class.std::__2::__compressed_pair.89"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.89"*, %"class.std::__2::__compressed_pair.89"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.89"* %this1 to %"struct.std::__2::__compressed_pair_elem.90"*
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__222__compressed_pair_elemIPiLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.90"* %0) #9
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__222__compressed_pair_elemIPiLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.90"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.90"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.90"* %this, %"struct.std::__2::__compressed_pair_elem.90"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.90"*, %"struct.std::__2::__compressed_pair_elem.90"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.90", %"struct.std::__2::__compressed_pair_elem.90"* %this1, i32 0, i32 0
  ret i32** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE5clearEv(%"class.std::__2::__vector_base.88"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.88"*, align 4
  store %"class.std::__2::__vector_base.88"* %this, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.88"*, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__begin_, align 4
  call void @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE17__destruct_at_endEPi(%"class.std::__2::__vector_base.88"* %this1, i32* %0) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE10deallocateERS2_Pim(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %__a, i32* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.92"*, align 4
  %__p.addr = alloca i32*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.92"* %__a, %"class.std::__2::allocator.92"** %__a.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.92"*, %"class.std::__2::allocator.92"** %__a.addr, align 4
  %1 = load i32*, i32** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorIiE10deallocateEPim(%"class.std::__2::allocator.92"* %0, i32* %1, i32 %2) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE7__allocEv(%"class.std::__2::__vector_base.88"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.88"*, align 4
  store %"class.std::__2::__vector_base.88"* %this, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.88"*, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__217__compressed_pairIPiNS_9allocatorIiEEE6secondEv(%"class.std::__2::__compressed_pair.89"* %__end_cap_) #9
  ret %"class.std::__2::allocator.92"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE17__destruct_at_endEPi(%"class.std::__2::__vector_base.88"* %this, i32* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.88"*, align 4
  %__new_last.addr = alloca i32*, align 4
  %__soon_to_be_end = alloca i32*, align 4
  store %"class.std::__2::__vector_base.88"* %this, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  store i32* %__new_last, i32** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.88"*, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %this1, i32 0, i32 1
  %0 = load i32*, i32** %__end_, align 4
  store i32* %0, i32** %__soon_to_be_end, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load i32*, i32** %__new_last.addr, align 4
  %2 = load i32*, i32** %__soon_to_be_end, align 4
  %cmp = icmp ne i32* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE7__allocEv(%"class.std::__2::__vector_base.88"* %this1) #9
  %3 = load i32*, i32** %__soon_to_be_end, align 4
  %incdec.ptr = getelementptr inbounds i32, i32* %3, i32 -1
  store i32* %incdec.ptr, i32** %__soon_to_be_end, align 4
  %call2 = call i32* @_ZNSt3__212__to_addressIiEEPT_S2_(i32* %incdec.ptr) #9
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE7destroyIiEEvRS2_PT_(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %call, i32* %call2)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %4 = load i32*, i32** %__new_last.addr, align 4
  %__end_3 = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %this1, i32 0, i32 1
  store i32* %4, i32** %__end_3, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE7destroyIiEEvRS2_PT_(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %__a, i32* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.92"*, align 4
  %__p.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy.147", align 1
  store %"class.std::__2::allocator.92"* %__a, %"class.std::__2::allocator.92"** %__a.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy.147"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.92"*, %"class.std::__2::allocator.92"** %__a.addr, align 4
  %2 = load i32*, i32** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE9__destroyIiEEvNS_17integral_constantIbLb1EEERS2_PT_(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %1, i32* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE9__destroyIiEEvNS_17integral_constantIbLb1EEERS2_PT_(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %__a, i32* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.92"*, align 4
  %__p.addr = alloca i32*, align 4
  store %"class.std::__2::allocator.92"* %__a, %"class.std::__2::allocator.92"** %__a.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.92"*, %"class.std::__2::allocator.92"** %__a.addr, align 4
  %2 = load i32*, i32** %__p.addr, align 4
  call void @_ZNSt3__29allocatorIiE7destroyEPi(%"class.std::__2::allocator.92"* %1, i32* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIiE7destroyEPi(%"class.std::__2::allocator.92"* %this, i32* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.92"*, align 4
  %__p.addr = alloca i32*, align 4
  store %"class.std::__2::allocator.92"* %this, %"class.std::__2::allocator.92"** %this.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.92"*, %"class.std::__2::allocator.92"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIiE10deallocateEPim(%"class.std::__2::allocator.92"* %this, i32* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.92"*, align 4
  %__p.addr = alloca i32*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.92"* %this, %"class.std::__2::allocator.92"** %this.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.92"*, %"class.std::__2::allocator.92"** %this.addr, align 4
  %0 = load i32*, i32** %__p.addr, align 4
  %1 = bitcast i32* %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__217__compressed_pairIPiNS_9allocatorIiEEE6secondEv(%"class.std::__2::__compressed_pair.89"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.89"*, align 4
  store %"class.std::__2::__compressed_pair.89"* %this, %"class.std::__2::__compressed_pair.89"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.89"*, %"class.std::__2::__compressed_pair.89"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.89"* %this1 to %"struct.std::__2::__compressed_pair_elem.91"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIiEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.91"* %0) #9
  ret %"class.std::__2::allocator.92"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIiEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.91"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.91"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.91"* %this, %"struct.std::__2::__compressed_pair_elem.91"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.91"*, %"struct.std::__2::__compressed_pair_elem.91"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.91"* %this1 to %"class.std::__2::allocator.92"*
  ret %"class.std::__2::allocator.92"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base.95"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEED2Ev(%"class.std::__2::__vector_base.95"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__vector_base.95"*, align 4
  %this.addr = alloca %"class.std::__2::__vector_base.95"*, align 4
  store %"class.std::__2::__vector_base.95"* %this, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.95"*, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  store %"class.std::__2::__vector_base.95"* %this1, %"class.std::__2::__vector_base.95"** %retval, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__begin_, align 4
  %cmp = icmp ne %"class.std::__2::unique_ptr.96"* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5clearEv(%"class.std::__2::__vector_base.95"* %this1) #9
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base.95"* %this1) #9
  %__begin_2 = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__begin_2, align 4
  %call3 = call i32 @_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::__vector_base.95"* %this1) #9
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE10deallocateERS8_PS7_m(%"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %call, %"class.std::__2::unique_ptr.96"* %1, i32 %call3) #9
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"class.std::__2::__vector_base.95"*, %"class.std::__2::__vector_base.95"** %retval, align 4
  ret %"class.std::__2::__vector_base.95"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5clearEv(%"class.std::__2::__vector_base.95"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.95"*, align 4
  store %"class.std::__2::__vector_base.95"* %this, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.95"*, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__begin_, align 4
  call void @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__destruct_at_endEPS6_(%"class.std::__2::__vector_base.95"* %this1, %"class.std::__2::unique_ptr.96"* %0) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco13DecoderBuffer4PeekIiEEbPT_(%"class.draco::DecoderBuffer"* %this, i32* %out_val) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %out_val.addr = alloca i32*, align 4
  %size_to_decode = alloca i32, align 4
  store %"class.draco::DecoderBuffer"* %this, %"class.draco::DecoderBuffer"** %this.addr, align 4
  store i32* %out_val, i32** %out_val.addr, align 4
  %this1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %this.addr, align 4
  store i32 4, i32* %size_to_decode, align 4
  %data_size_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 1
  %0 = load i64, i64* %data_size_, align 8
  %pos_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %1 = load i64, i64* %pos_, align 8
  %add = add nsw i64 %1, 4
  %cmp = icmp slt i64 %0, %add
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %2 = load i32*, i32** %out_val.addr, align 4
  %3 = bitcast i32* %2 to i8*
  %data_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 0
  %4 = load i8*, i8** %data_, align 8
  %pos_2 = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %5 = load i64, i64* %pos_2, align 8
  %idx.ext = trunc i64 %5 to i32
  %add.ptr = getelementptr inbounds i8, i8* %4, i32 %idx.ext
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 1 %add.ptr, i32 4, i1 false)
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %6 = load i1, i1* %retval, align 1
  ret i1 %6
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.132"* @_ZNSt3__217__compressed_pairIPN5draco15PointsSequencerENS_14default_deleteIS2_EEEC2IRS3_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.132"* returned %this, %"class.draco::PointsSequencer"** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.132"*, align 4
  %__t1.addr = alloca %"class.draco::PointsSequencer"**, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.132"* %this, %"class.std::__2::__compressed_pair.132"** %this.addr, align 4
  store %"class.draco::PointsSequencer"** %__t1, %"class.draco::PointsSequencer"*** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.132"*, %"class.std::__2::__compressed_pair.132"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.132"* %this1 to %"struct.std::__2::__compressed_pair_elem.133"*
  %1 = load %"class.draco::PointsSequencer"**, %"class.draco::PointsSequencer"*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointsSequencer"** @_ZNSt3__27forwardIRPN5draco15PointsSequencerEEEOT_RNS_16remove_referenceIS5_E4typeE(%"class.draco::PointsSequencer"** nonnull align 4 dereferenceable(4) %1) #9
  %call2 = call %"struct.std::__2::__compressed_pair_elem.133"* @_ZNSt3__222__compressed_pair_elemIPN5draco15PointsSequencerELi0ELb0EEC2IRS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.133"* %0, %"class.draco::PointsSequencer"** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.132"* %this1 to %"struct.std::__2::__compressed_pair_elem.134"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #9
  %call4 = call %"struct.std::__2::__compressed_pair_elem.134"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco15PointsSequencerEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.134"* %2)
  ret %"class.std::__2::__compressed_pair.132"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::PointsSequencer"** @_ZNSt3__27forwardIRPN5draco15PointsSequencerEEEOT_RNS_16remove_referenceIS5_E4typeE(%"class.draco::PointsSequencer"** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.draco::PointsSequencer"**, align 4
  store %"class.draco::PointsSequencer"** %__t, %"class.draco::PointsSequencer"*** %__t.addr, align 4
  %0 = load %"class.draco::PointsSequencer"**, %"class.draco::PointsSequencer"*** %__t.addr, align 4
  ret %"class.draco::PointsSequencer"** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.133"* @_ZNSt3__222__compressed_pair_elemIPN5draco15PointsSequencerELi0ELb0EEC2IRS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.133"* returned %this, %"class.draco::PointsSequencer"** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.133"*, align 4
  %__u.addr = alloca %"class.draco::PointsSequencer"**, align 4
  store %"struct.std::__2::__compressed_pair_elem.133"* %this, %"struct.std::__2::__compressed_pair_elem.133"** %this.addr, align 4
  store %"class.draco::PointsSequencer"** %__u, %"class.draco::PointsSequencer"*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.133"*, %"struct.std::__2::__compressed_pair_elem.133"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.133", %"struct.std::__2::__compressed_pair_elem.133"* %this1, i32 0, i32 0
  %0 = load %"class.draco::PointsSequencer"**, %"class.draco::PointsSequencer"*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointsSequencer"** @_ZNSt3__27forwardIRPN5draco15PointsSequencerEEEOT_RNS_16remove_referenceIS5_E4typeE(%"class.draco::PointsSequencer"** nonnull align 4 dereferenceable(4) %0) #9
  %1 = load %"class.draco::PointsSequencer"*, %"class.draco::PointsSequencer"** %call, align 4
  store %"class.draco::PointsSequencer"* %1, %"class.draco::PointsSequencer"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.133"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.134"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco15PointsSequencerEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.134"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.134"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.134"* %this, %"struct.std::__2::__compressed_pair_elem.134"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.134"*, %"struct.std::__2::__compressed_pair_elem.134"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.134"* %this1 to %"struct.std::__2::default_delete.135"*
  ret %"struct.std::__2::__compressed_pair_elem.134"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__210unique_ptrIN5draco15PointsSequencerENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.131"* %this, %"class.draco::PointsSequencer"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.131"*, align 4
  %__p.addr = alloca %"class.draco::PointsSequencer"*, align 4
  %__tmp = alloca %"class.draco::PointsSequencer"*, align 4
  store %"class.std::__2::unique_ptr.131"* %this, %"class.std::__2::unique_ptr.131"** %this.addr, align 4
  store %"class.draco::PointsSequencer"* %__p, %"class.draco::PointsSequencer"** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.131"*, %"class.std::__2::unique_ptr.131"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.131", %"class.std::__2::unique_ptr.131"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointsSequencer"** @_ZNSt3__217__compressed_pairIPN5draco15PointsSequencerENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.132"* %__ptr_) #9
  %0 = load %"class.draco::PointsSequencer"*, %"class.draco::PointsSequencer"** %call, align 4
  store %"class.draco::PointsSequencer"* %0, %"class.draco::PointsSequencer"** %__tmp, align 4
  %1 = load %"class.draco::PointsSequencer"*, %"class.draco::PointsSequencer"** %__p.addr, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr.131", %"class.std::__2::unique_ptr.131"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::PointsSequencer"** @_ZNSt3__217__compressed_pairIPN5draco15PointsSequencerENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.132"* %__ptr_2) #9
  store %"class.draco::PointsSequencer"* %1, %"class.draco::PointsSequencer"** %call3, align 4
  %2 = load %"class.draco::PointsSequencer"*, %"class.draco::PointsSequencer"** %__tmp, align 4
  %tobool = icmp ne %"class.draco::PointsSequencer"* %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__ptr_4 = getelementptr inbounds %"class.std::__2::unique_ptr.131", %"class.std::__2::unique_ptr.131"* %this1, i32 0, i32 0
  %call5 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.135"* @_ZNSt3__217__compressed_pairIPN5draco15PointsSequencerENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.132"* %__ptr_4) #9
  %3 = load %"class.draco::PointsSequencer"*, %"class.draco::PointsSequencer"** %__tmp, align 4
  call void @_ZNKSt3__214default_deleteIN5draco15PointsSequencerEEclEPS2_(%"struct.std::__2::default_delete.135"* %call5, %"class.draco::PointsSequencer"* %3) #9
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::PointsSequencer"** @_ZNSt3__217__compressed_pairIPN5draco15PointsSequencerENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.132"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.132"*, align 4
  store %"class.std::__2::__compressed_pair.132"* %this, %"class.std::__2::__compressed_pair.132"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.132"*, %"class.std::__2::__compressed_pair.132"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.132"* %this1 to %"struct.std::__2::__compressed_pair_elem.133"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointsSequencer"** @_ZNSt3__222__compressed_pair_elemIPN5draco15PointsSequencerELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.133"* %0) #9
  ret %"class.draco::PointsSequencer"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.135"* @_ZNSt3__217__compressed_pairIPN5draco15PointsSequencerENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.132"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.132"*, align 4
  store %"class.std::__2::__compressed_pair.132"* %this, %"class.std::__2::__compressed_pair.132"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.132"*, %"class.std::__2::__compressed_pair.132"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.132"* %this1 to %"struct.std::__2::__compressed_pair_elem.134"*
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.135"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco15PointsSequencerEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.134"* %0) #9
  ret %"struct.std::__2::default_delete.135"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__214default_deleteIN5draco15PointsSequencerEEclEPS2_(%"struct.std::__2::default_delete.135"* %this, %"class.draco::PointsSequencer"* %__ptr) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::default_delete.135"*, align 4
  %__ptr.addr = alloca %"class.draco::PointsSequencer"*, align 4
  store %"struct.std::__2::default_delete.135"* %this, %"struct.std::__2::default_delete.135"** %this.addr, align 4
  store %"class.draco::PointsSequencer"* %__ptr, %"class.draco::PointsSequencer"** %__ptr.addr, align 4
  %this1 = load %"struct.std::__2::default_delete.135"*, %"struct.std::__2::default_delete.135"** %this.addr, align 4
  %0 = load %"class.draco::PointsSequencer"*, %"class.draco::PointsSequencer"** %__ptr.addr, align 4
  %isnull = icmp eq %"class.draco::PointsSequencer"* %0, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %entry
  %1 = bitcast %"class.draco::PointsSequencer"* %0 to void (%"class.draco::PointsSequencer"*)***
  %vtable = load void (%"class.draco::PointsSequencer"*)**, void (%"class.draco::PointsSequencer"*)*** %1, align 4
  %vfn = getelementptr inbounds void (%"class.draco::PointsSequencer"*)*, void (%"class.draco::PointsSequencer"*)** %vtable, i64 1
  %2 = load void (%"class.draco::PointsSequencer"*)*, void (%"class.draco::PointsSequencer"*)** %vfn, align 4
  call void %2(%"class.draco::PointsSequencer"* %0) #9
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::PointsSequencer"** @_ZNSt3__222__compressed_pair_elemIPN5draco15PointsSequencerELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.133"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.133"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.133"* %this, %"struct.std::__2::__compressed_pair_elem.133"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.133"*, %"struct.std::__2::__compressed_pair_elem.133"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.133", %"struct.std::__2::__compressed_pair_elem.133"* %this1, i32 0, i32 0
  ret %"class.draco::PointsSequencer"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.135"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco15PointsSequencerEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.134"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.134"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.134"* %this, %"struct.std::__2::__compressed_pair_elem.134"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.134"*, %"struct.std::__2::__compressed_pair_elem.134"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.134"* %this1 to %"struct.std::__2::default_delete.135"*
  ret %"struct.std::__2::default_delete.135"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.107"* @_ZNSt3__217__compressed_pairIPN5draco17AttributesDecoderENS_14default_deleteIS2_EEEC2IRS3_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.107"* returned %this, %"class.draco::AttributesDecoder"** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.107"*, align 4
  %__t1.addr = alloca %"class.draco::AttributesDecoder"**, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.107"* %this, %"class.std::__2::__compressed_pair.107"** %this.addr, align 4
  store %"class.draco::AttributesDecoder"** %__t1, %"class.draco::AttributesDecoder"*** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.107"*, %"class.std::__2::__compressed_pair.107"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.107"* %this1 to %"struct.std::__2::__compressed_pair_elem.108"*
  %1 = load %"class.draco::AttributesDecoder"**, %"class.draco::AttributesDecoder"*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributesDecoder"** @_ZNSt3__27forwardIRPN5draco17AttributesDecoderEEEOT_RNS_16remove_referenceIS5_E4typeE(%"class.draco::AttributesDecoder"** nonnull align 4 dereferenceable(4) %1) #9
  %call2 = call %"struct.std::__2::__compressed_pair_elem.108"* @_ZNSt3__222__compressed_pair_elemIPN5draco17AttributesDecoderELi0ELb0EEC2IRS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.108"* %0, %"class.draco::AttributesDecoder"** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.107"* %this1 to %"struct.std::__2::__compressed_pair_elem.109"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #9
  %call4 = call %"struct.std::__2::__compressed_pair_elem.109"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco17AttributesDecoderEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.109"* %2)
  ret %"class.std::__2::__compressed_pair.107"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::AttributesDecoder"** @_ZNSt3__27forwardIRPN5draco17AttributesDecoderEEEOT_RNS_16remove_referenceIS5_E4typeE(%"class.draco::AttributesDecoder"** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.draco::AttributesDecoder"**, align 4
  store %"class.draco::AttributesDecoder"** %__t, %"class.draco::AttributesDecoder"*** %__t.addr, align 4
  %0 = load %"class.draco::AttributesDecoder"**, %"class.draco::AttributesDecoder"*** %__t.addr, align 4
  ret %"class.draco::AttributesDecoder"** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.108"* @_ZNSt3__222__compressed_pair_elemIPN5draco17AttributesDecoderELi0ELb0EEC2IRS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.108"* returned %this, %"class.draco::AttributesDecoder"** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.108"*, align 4
  %__u.addr = alloca %"class.draco::AttributesDecoder"**, align 4
  store %"struct.std::__2::__compressed_pair_elem.108"* %this, %"struct.std::__2::__compressed_pair_elem.108"** %this.addr, align 4
  store %"class.draco::AttributesDecoder"** %__u, %"class.draco::AttributesDecoder"*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.108"*, %"struct.std::__2::__compressed_pair_elem.108"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.108", %"struct.std::__2::__compressed_pair_elem.108"* %this1, i32 0, i32 0
  %0 = load %"class.draco::AttributesDecoder"**, %"class.draco::AttributesDecoder"*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributesDecoder"** @_ZNSt3__27forwardIRPN5draco17AttributesDecoderEEEOT_RNS_16remove_referenceIS5_E4typeE(%"class.draco::AttributesDecoder"** nonnull align 4 dereferenceable(4) %0) #9
  %1 = load %"class.draco::AttributesDecoder"*, %"class.draco::AttributesDecoder"** %call, align 4
  store %"class.draco::AttributesDecoder"* %1, %"class.draco::AttributesDecoder"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.108"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.109"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco17AttributesDecoderEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.109"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.109"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.109"* %this, %"struct.std::__2::__compressed_pair_elem.109"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.109"*, %"struct.std::__2::__compressed_pair_elem.109"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.109"* %this1 to %"struct.std::__2::default_delete.110"*
  ret %"struct.std::__2::__compressed_pair_elem.109"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__210unique_ptrIN5draco17AttributesDecoderENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.106"* %this, %"class.draco::AttributesDecoder"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.106"*, align 4
  %__p.addr = alloca %"class.draco::AttributesDecoder"*, align 4
  %__tmp = alloca %"class.draco::AttributesDecoder"*, align 4
  store %"class.std::__2::unique_ptr.106"* %this, %"class.std::__2::unique_ptr.106"** %this.addr, align 4
  store %"class.draco::AttributesDecoder"* %__p, %"class.draco::AttributesDecoder"** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.106"*, %"class.std::__2::unique_ptr.106"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.106", %"class.std::__2::unique_ptr.106"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributesDecoder"** @_ZNSt3__217__compressed_pairIPN5draco17AttributesDecoderENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.107"* %__ptr_) #9
  %0 = load %"class.draco::AttributesDecoder"*, %"class.draco::AttributesDecoder"** %call, align 4
  store %"class.draco::AttributesDecoder"* %0, %"class.draco::AttributesDecoder"** %__tmp, align 4
  %1 = load %"class.draco::AttributesDecoder"*, %"class.draco::AttributesDecoder"** %__p.addr, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr.106", %"class.std::__2::unique_ptr.106"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::AttributesDecoder"** @_ZNSt3__217__compressed_pairIPN5draco17AttributesDecoderENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.107"* %__ptr_2) #9
  store %"class.draco::AttributesDecoder"* %1, %"class.draco::AttributesDecoder"** %call3, align 4
  %2 = load %"class.draco::AttributesDecoder"*, %"class.draco::AttributesDecoder"** %__tmp, align 4
  %tobool = icmp ne %"class.draco::AttributesDecoder"* %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__ptr_4 = getelementptr inbounds %"class.std::__2::unique_ptr.106", %"class.std::__2::unique_ptr.106"* %this1, i32 0, i32 0
  %call5 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.110"* @_ZNSt3__217__compressed_pairIPN5draco17AttributesDecoderENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.107"* %__ptr_4) #9
  %3 = load %"class.draco::AttributesDecoder"*, %"class.draco::AttributesDecoder"** %__tmp, align 4
  call void @_ZNKSt3__214default_deleteIN5draco17AttributesDecoderEEclEPS2_(%"struct.std::__2::default_delete.110"* %call5, %"class.draco::AttributesDecoder"* %3) #9
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::AttributesDecoder"** @_ZNSt3__217__compressed_pairIPN5draco17AttributesDecoderENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.107"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.107"*, align 4
  store %"class.std::__2::__compressed_pair.107"* %this, %"class.std::__2::__compressed_pair.107"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.107"*, %"class.std::__2::__compressed_pair.107"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.107"* %this1 to %"struct.std::__2::__compressed_pair_elem.108"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributesDecoder"** @_ZNSt3__222__compressed_pair_elemIPN5draco17AttributesDecoderELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.108"* %0) #9
  ret %"class.draco::AttributesDecoder"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.110"* @_ZNSt3__217__compressed_pairIPN5draco17AttributesDecoderENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.107"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.107"*, align 4
  store %"class.std::__2::__compressed_pair.107"* %this, %"class.std::__2::__compressed_pair.107"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.107"*, %"class.std::__2::__compressed_pair.107"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.107"* %this1 to %"struct.std::__2::__compressed_pair_elem.109"*
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.110"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco17AttributesDecoderEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.109"* %0) #9
  ret %"struct.std::__2::default_delete.110"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__214default_deleteIN5draco17AttributesDecoderEEclEPS2_(%"struct.std::__2::default_delete.110"* %this, %"class.draco::AttributesDecoder"* %__ptr) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::default_delete.110"*, align 4
  %__ptr.addr = alloca %"class.draco::AttributesDecoder"*, align 4
  store %"struct.std::__2::default_delete.110"* %this, %"struct.std::__2::default_delete.110"** %this.addr, align 4
  store %"class.draco::AttributesDecoder"* %__ptr, %"class.draco::AttributesDecoder"** %__ptr.addr, align 4
  %this1 = load %"struct.std::__2::default_delete.110"*, %"struct.std::__2::default_delete.110"** %this.addr, align 4
  %0 = load %"class.draco::AttributesDecoder"*, %"class.draco::AttributesDecoder"** %__ptr.addr, align 4
  %isnull = icmp eq %"class.draco::AttributesDecoder"* %0, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %entry
  %1 = bitcast %"class.draco::AttributesDecoder"* %0 to void (%"class.draco::AttributesDecoder"*)***
  %vtable = load void (%"class.draco::AttributesDecoder"*)**, void (%"class.draco::AttributesDecoder"*)*** %1, align 4
  %vfn = getelementptr inbounds void (%"class.draco::AttributesDecoder"*)*, void (%"class.draco::AttributesDecoder"*)** %vtable, i64 1
  %2 = load void (%"class.draco::AttributesDecoder"*)*, void (%"class.draco::AttributesDecoder"*)** %vfn, align 4
  call void %2(%"class.draco::AttributesDecoder"* %0) #9
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::AttributesDecoder"** @_ZNSt3__222__compressed_pair_elemIPN5draco17AttributesDecoderELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.108"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.108"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.108"* %this, %"struct.std::__2::__compressed_pair_elem.108"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.108"*, %"struct.std::__2::__compressed_pair_elem.108"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.108", %"struct.std::__2::__compressed_pair_elem.108"* %this1, i32 0, i32 0
  ret %"class.draco::AttributesDecoder"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.110"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco17AttributesDecoderEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.109"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.109"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.109"* %this, %"struct.std::__2::__compressed_pair_elem.109"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.109"*, %"struct.std::__2::__compressed_pair_elem.109"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.109"* %this1 to %"struct.std::__2::default_delete.110"*
  ret %"struct.std::__2::default_delete.110"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::AttributesDecoder"* @_ZNSt3__210unique_ptrIN5draco17AttributesDecoderENS_14default_deleteIS2_EEE7releaseEv(%"class.std::__2::unique_ptr.106"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.106"*, align 4
  %__t = alloca %"class.draco::AttributesDecoder"*, align 4
  store %"class.std::__2::unique_ptr.106"* %this, %"class.std::__2::unique_ptr.106"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.106"*, %"class.std::__2::unique_ptr.106"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.106", %"class.std::__2::unique_ptr.106"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributesDecoder"** @_ZNSt3__217__compressed_pairIPN5draco17AttributesDecoderENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.107"* %__ptr_) #9
  %0 = load %"class.draco::AttributesDecoder"*, %"class.draco::AttributesDecoder"** %call, align 4
  store %"class.draco::AttributesDecoder"* %0, %"class.draco::AttributesDecoder"** %__t, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr.106", %"class.std::__2::unique_ptr.106"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::AttributesDecoder"** @_ZNSt3__217__compressed_pairIPN5draco17AttributesDecoderENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.107"* %__ptr_2) #9
  store %"class.draco::AttributesDecoder"* null, %"class.draco::AttributesDecoder"** %call3, align 4
  %1 = load %"class.draco::AttributesDecoder"*, %"class.draco::AttributesDecoder"** %__t, align 4
  ret %"class.draco::AttributesDecoder"* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.110"* @_ZNSt3__27forwardINS_14default_deleteIN5draco17AttributesDecoderEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.110"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"struct.std::__2::default_delete.110"*, align 4
  store %"struct.std::__2::default_delete.110"* %__t, %"struct.std::__2::default_delete.110"** %__t.addr, align 4
  %0 = load %"struct.std::__2::default_delete.110"*, %"struct.std::__2::default_delete.110"** %__t.addr, align 4
  ret %"struct.std::__2::default_delete.110"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.110"* @_ZNSt3__210unique_ptrIN5draco17AttributesDecoderENS_14default_deleteIS2_EEE11get_deleterEv(%"class.std::__2::unique_ptr.106"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.106"*, align 4
  store %"class.std::__2::unique_ptr.106"* %this, %"class.std::__2::unique_ptr.106"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.106"*, %"class.std::__2::unique_ptr.106"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.106", %"class.std::__2::unique_ptr.106"* %this1, i32 0, i32 0
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.110"* @_ZNSt3__217__compressed_pairIPN5draco17AttributesDecoderENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.107"* %__ptr_) #9
  ret %"struct.std::__2::default_delete.110"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.97"* @_ZNSt3__217__compressed_pairIPN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEEC2IPNS1_17AttributesDecoderENS4_IS8_EEEEOT_OT0_(%"class.std::__2::__compressed_pair.97"* returned %this, %"class.draco::AttributesDecoder"** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::default_delete.110"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.97"*, align 4
  %__t1.addr = alloca %"class.draco::AttributesDecoder"**, align 4
  %__t2.addr = alloca %"struct.std::__2::default_delete.110"*, align 4
  store %"class.std::__2::__compressed_pair.97"* %this, %"class.std::__2::__compressed_pair.97"** %this.addr, align 4
  store %"class.draco::AttributesDecoder"** %__t1, %"class.draco::AttributesDecoder"*** %__t1.addr, align 4
  store %"struct.std::__2::default_delete.110"* %__t2, %"struct.std::__2::default_delete.110"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.97"*, %"class.std::__2::__compressed_pair.97"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.97"* %this1 to %"struct.std::__2::__compressed_pair_elem.98"*
  %1 = load %"class.draco::AttributesDecoder"**, %"class.draco::AttributesDecoder"*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributesDecoder"** @_ZNSt3__27forwardIPN5draco17AttributesDecoderEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::AttributesDecoder"** nonnull align 4 dereferenceable(4) %1) #9
  %call2 = call %"struct.std::__2::__compressed_pair_elem.98"* @_ZNSt3__222__compressed_pair_elemIPN5draco26AttributesDecoderInterfaceELi0ELb0EEC2IPNS1_17AttributesDecoderEvEEOT_(%"struct.std::__2::__compressed_pair_elem.98"* %0, %"class.draco::AttributesDecoder"** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.97"* %this1 to %"struct.std::__2::__compressed_pair_elem.99"*
  %3 = load %"struct.std::__2::default_delete.110"*, %"struct.std::__2::default_delete.110"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.110"* @_ZNSt3__27forwardINS_14default_deleteIN5draco17AttributesDecoderEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.110"* nonnull align 1 dereferenceable(1) %3) #9
  %call4 = call %"struct.std::__2::__compressed_pair_elem.99"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco26AttributesDecoderInterfaceEEELi1ELb1EEC2INS1_INS2_17AttributesDecoderEEEvEEOT_(%"struct.std::__2::__compressed_pair_elem.99"* %2, %"struct.std::__2::default_delete.110"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::__compressed_pair.97"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::AttributesDecoder"** @_ZNSt3__27forwardIPN5draco17AttributesDecoderEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::AttributesDecoder"** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.draco::AttributesDecoder"**, align 4
  store %"class.draco::AttributesDecoder"** %__t, %"class.draco::AttributesDecoder"*** %__t.addr, align 4
  %0 = load %"class.draco::AttributesDecoder"**, %"class.draco::AttributesDecoder"*** %__t.addr, align 4
  ret %"class.draco::AttributesDecoder"** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.98"* @_ZNSt3__222__compressed_pair_elemIPN5draco26AttributesDecoderInterfaceELi0ELb0EEC2IPNS1_17AttributesDecoderEvEEOT_(%"struct.std::__2::__compressed_pair_elem.98"* returned %this, %"class.draco::AttributesDecoder"** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.98"*, align 4
  %__u.addr = alloca %"class.draco::AttributesDecoder"**, align 4
  store %"struct.std::__2::__compressed_pair_elem.98"* %this, %"struct.std::__2::__compressed_pair_elem.98"** %this.addr, align 4
  store %"class.draco::AttributesDecoder"** %__u, %"class.draco::AttributesDecoder"*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.98"*, %"struct.std::__2::__compressed_pair_elem.98"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.98", %"struct.std::__2::__compressed_pair_elem.98"* %this1, i32 0, i32 0
  %0 = load %"class.draco::AttributesDecoder"**, %"class.draco::AttributesDecoder"*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributesDecoder"** @_ZNSt3__27forwardIPN5draco17AttributesDecoderEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::AttributesDecoder"** nonnull align 4 dereferenceable(4) %0) #9
  %1 = load %"class.draco::AttributesDecoder"*, %"class.draco::AttributesDecoder"** %call, align 4
  %2 = bitcast %"class.draco::AttributesDecoder"* %1 to %"class.draco::AttributesDecoderInterface"*
  store %"class.draco::AttributesDecoderInterface"* %2, %"class.draco::AttributesDecoderInterface"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.98"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.99"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco26AttributesDecoderInterfaceEEELi1ELb1EEC2INS1_INS2_17AttributesDecoderEEEvEEOT_(%"struct.std::__2::__compressed_pair_elem.99"* returned %this, %"struct.std::__2::default_delete.110"* nonnull align 1 dereferenceable(1) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.99"*, align 4
  %__u.addr = alloca %"struct.std::__2::default_delete.110"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.99"* %this, %"struct.std::__2::__compressed_pair_elem.99"** %this.addr, align 4
  store %"struct.std::__2::default_delete.110"* %__u, %"struct.std::__2::default_delete.110"** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.99"*, %"struct.std::__2::__compressed_pair_elem.99"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.99"* %this1 to %"struct.std::__2::default_delete.100"*
  %1 = load %"struct.std::__2::default_delete.110"*, %"struct.std::__2::default_delete.110"** %__u.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.110"* @_ZNSt3__27forwardINS_14default_deleteIN5draco17AttributesDecoderEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.110"* nonnull align 1 dereferenceable(1) %1) #9
  %call2 = call %"struct.std::__2::default_delete.100"* @_ZNSt3__214default_deleteIN5draco26AttributesDecoderInterfaceEEC2INS1_17AttributesDecoderEEERKNS0_IT_EEPNS_9enable_ifIXsr14is_convertibleIPS6_PS2_EE5valueEvE4typeE(%"struct.std::__2::default_delete.100"* %0, %"struct.std::__2::default_delete.110"* nonnull align 1 dereferenceable(1) %call, i8* null) #9
  ret %"struct.std::__2::__compressed_pair_elem.99"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::default_delete.100"* @_ZNSt3__214default_deleteIN5draco26AttributesDecoderInterfaceEEC2INS1_17AttributesDecoderEEERKNS0_IT_EEPNS_9enable_ifIXsr14is_convertibleIPS6_PS2_EE5valueEvE4typeE(%"struct.std::__2::default_delete.100"* returned %this, %"struct.std::__2::default_delete.110"* nonnull align 1 dereferenceable(1) %0, i8* %1) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::default_delete.100"*, align 4
  %.addr = alloca %"struct.std::__2::default_delete.110"*, align 4
  %.addr1 = alloca i8*, align 4
  store %"struct.std::__2::default_delete.100"* %this, %"struct.std::__2::default_delete.100"** %this.addr, align 4
  store %"struct.std::__2::default_delete.110"* %0, %"struct.std::__2::default_delete.110"** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  %this2 = load %"struct.std::__2::default_delete.100"*, %"struct.std::__2::default_delete.100"** %this.addr, align 4
  ret %"struct.std::__2::default_delete.100"* %this2
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nobuiltin allocsize(0) "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { noreturn "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { noinline noreturn nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { cold noreturn nounwind }
attributes #7 = { argmemonly nounwind willreturn }
attributes #8 = { builtin allocsize(0) }
attributes #9 = { nounwind }
attributes #10 = { builtin nounwind }
attributes #11 = { noreturn }
attributes #12 = { noreturn nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
