; ModuleID = 'neuroglancer_draco.cc'
source_filename = "neuroglancer_draco.cc"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%"class.std::__2::unique_ptr" = type { %"class.std::__2::__compressed_pair" }
%"class.std::__2::__compressed_pair" = type { %"struct.std::__2::__compressed_pair_elem" }
%"struct.std::__2::__compressed_pair_elem" = type { i8* }
%"class.draco::DecoderBuffer" = type { i8*, i64, i64, %"class.draco::DecoderBuffer::BitDecoder", i8, i16 }
%"class.draco::DecoderBuffer::BitDecoder" = type { i8*, i8*, i32 }
%"class.draco::Decoder" = type { %"class.draco::DracoOptions" }
%"class.draco::DracoOptions" = type { %"class.draco::Options", %"class.std::__2::map.7" }
%"class.draco::Options" = type { %"class.std::__2::map" }
%"class.std::__2::map" = type { %"class.std::__2::__tree" }
%"class.std::__2::__tree" = type { %"class.std::__2::__tree_end_node"*, %"class.std::__2::__compressed_pair.1", %"class.std::__2::__compressed_pair.4" }
%"class.std::__2::__tree_end_node" = type { %"class.std::__2::__tree_node_base"* }
%"class.std::__2::__tree_node_base" = type <{ %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_end_node"*, i8, [3 x i8] }>
%"class.std::__2::__compressed_pair.1" = type { %"struct.std::__2::__compressed_pair_elem.2" }
%"struct.std::__2::__compressed_pair_elem.2" = type { %"class.std::__2::__tree_end_node" }
%"class.std::__2::__compressed_pair.4" = type { %"struct.std::__2::__compressed_pair_elem.5" }
%"struct.std::__2::__compressed_pair_elem.5" = type { i32 }
%"class.std::__2::map.7" = type { %"class.std::__2::__tree.8" }
%"class.std::__2::__tree.8" = type { %"class.std::__2::__tree_end_node"*, %"class.std::__2::__compressed_pair.9", %"class.std::__2::__compressed_pair.13" }
%"class.std::__2::__compressed_pair.9" = type { %"struct.std::__2::__compressed_pair_elem.2" }
%"class.std::__2::__compressed_pair.13" = type { %"struct.std::__2::__compressed_pair_elem.5" }
%"class.draco::StatusOr" = type { %"class.draco::Status", %"class.std::__2::unique_ptr.23" }
%"class.draco::Status" = type { i32, %"class.std::__2::basic_string" }
%"class.std::__2::basic_string" = type { %"class.std::__2::__compressed_pair.18" }
%"class.std::__2::__compressed_pair.18" = type { %"struct.std::__2::__compressed_pair_elem.19" }
%"struct.std::__2::__compressed_pair_elem.19" = type { %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep" }
%"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep" = type { %union.anon }
%union.anon = type { %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long" }
%"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long" = type { i8*, i32, i32 }
%"class.std::__2::unique_ptr.23" = type { %"class.std::__2::__compressed_pair.24" }
%"class.std::__2::__compressed_pair.24" = type { %"struct.std::__2::__compressed_pair_elem.25" }
%"struct.std::__2::__compressed_pair_elem.25" = type { %"class.draco::Mesh"* }
%"class.draco::Mesh" = type { %"class.draco::PointCloud", %"class.std::__2::vector.124", %"class.draco::IndexTypeVector.131" }
%"class.draco::PointCloud" = type { i32 (...)**, %"class.std::__2::unique_ptr.26", %"class.std::__2::vector.81", [5 x %"class.std::__2::vector.117"], i32 }
%"class.std::__2::unique_ptr.26" = type { %"class.std::__2::__compressed_pair.27" }
%"class.std::__2::__compressed_pair.27" = type { %"struct.std::__2::__compressed_pair_elem.28" }
%"struct.std::__2::__compressed_pair_elem.28" = type { %"class.draco::GeometryMetadata"* }
%"class.draco::GeometryMetadata" = type { %"class.draco::Metadata", %"class.std::__2::vector" }
%"class.draco::Metadata" = type { %"class.std::__2::unordered_map", %"class.std::__2::unordered_map.47" }
%"class.std::__2::unordered_map" = type { %"class.std::__2::__hash_table" }
%"class.std::__2::__hash_table" = type { %"class.std::__2::unique_ptr.29", %"class.std::__2::__compressed_pair.37", %"class.std::__2::__compressed_pair.42", %"class.std::__2::__compressed_pair.44" }
%"class.std::__2::unique_ptr.29" = type { %"class.std::__2::__compressed_pair.30" }
%"class.std::__2::__compressed_pair.30" = type { %"struct.std::__2::__compressed_pair_elem.31", %"struct.std::__2::__compressed_pair_elem.32" }
%"struct.std::__2::__compressed_pair_elem.31" = type { %"struct.std::__2::__hash_node_base"** }
%"struct.std::__2::__hash_node_base" = type { %"struct.std::__2::__hash_node_base"* }
%"struct.std::__2::__compressed_pair_elem.32" = type { %"class.std::__2::__bucket_list_deallocator" }
%"class.std::__2::__bucket_list_deallocator" = type { %"class.std::__2::__compressed_pair.33" }
%"class.std::__2::__compressed_pair.33" = type { %"struct.std::__2::__compressed_pair_elem.5" }
%"class.std::__2::__compressed_pair.37" = type { %"struct.std::__2::__compressed_pair_elem.38" }
%"struct.std::__2::__compressed_pair_elem.38" = type { %"struct.std::__2::__hash_node_base" }
%"class.std::__2::__compressed_pair.42" = type { %"struct.std::__2::__compressed_pair_elem.5" }
%"class.std::__2::__compressed_pair.44" = type { %"struct.std::__2::__compressed_pair_elem.45" }
%"struct.std::__2::__compressed_pair_elem.45" = type { float }
%"class.std::__2::unordered_map.47" = type { %"class.std::__2::__hash_table.48" }
%"class.std::__2::__hash_table.48" = type { %"class.std::__2::unique_ptr.49", %"class.std::__2::__compressed_pair.59", %"class.std::__2::__compressed_pair.64", %"class.std::__2::__compressed_pair.67" }
%"class.std::__2::unique_ptr.49" = type { %"class.std::__2::__compressed_pair.50" }
%"class.std::__2::__compressed_pair.50" = type { %"struct.std::__2::__compressed_pair_elem.51", %"struct.std::__2::__compressed_pair_elem.53" }
%"struct.std::__2::__compressed_pair_elem.51" = type { %"struct.std::__2::__hash_node_base.52"** }
%"struct.std::__2::__hash_node_base.52" = type { %"struct.std::__2::__hash_node_base.52"* }
%"struct.std::__2::__compressed_pair_elem.53" = type { %"class.std::__2::__bucket_list_deallocator.54" }
%"class.std::__2::__bucket_list_deallocator.54" = type { %"class.std::__2::__compressed_pair.55" }
%"class.std::__2::__compressed_pair.55" = type { %"struct.std::__2::__compressed_pair_elem.5" }
%"class.std::__2::__compressed_pair.59" = type { %"struct.std::__2::__compressed_pair_elem.60" }
%"struct.std::__2::__compressed_pair_elem.60" = type { %"struct.std::__2::__hash_node_base.52" }
%"class.std::__2::__compressed_pair.64" = type { %"struct.std::__2::__compressed_pair_elem.5" }
%"class.std::__2::__compressed_pair.67" = type { %"struct.std::__2::__compressed_pair_elem.45" }
%"class.std::__2::vector" = type { %"class.std::__2::__vector_base" }
%"class.std::__2::__vector_base" = type { %"class.std::__2::unique_ptr.70"*, %"class.std::__2::unique_ptr.70"*, %"class.std::__2::__compressed_pair.74" }
%"class.std::__2::unique_ptr.70" = type { %"class.std::__2::__compressed_pair.71" }
%"class.std::__2::__compressed_pair.71" = type { %"struct.std::__2::__compressed_pair_elem.72" }
%"struct.std::__2::__compressed_pair_elem.72" = type { %"class.draco::AttributeMetadata"* }
%"class.draco::AttributeMetadata" = type { %"class.draco::Metadata", i32 }
%"class.std::__2::__compressed_pair.74" = type { %"struct.std::__2::__compressed_pair_elem.75" }
%"struct.std::__2::__compressed_pair_elem.75" = type { %"class.std::__2::unique_ptr.70"* }
%"class.std::__2::vector.81" = type { %"class.std::__2::__vector_base.82" }
%"class.std::__2::__vector_base.82" = type { %"class.std::__2::unique_ptr.83"*, %"class.std::__2::unique_ptr.83"*, %"class.std::__2::__compressed_pair.112" }
%"class.std::__2::unique_ptr.83" = type { %"class.std::__2::__compressed_pair.84" }
%"class.std::__2::__compressed_pair.84" = type { %"struct.std::__2::__compressed_pair_elem.85" }
%"struct.std::__2::__compressed_pair_elem.85" = type { %"class.draco::PointAttribute"* }
%"class.draco::PointAttribute" = type <{ %"class.draco::GeometryAttribute", %"class.std::__2::unique_ptr.93", %"class.draco::IndexTypeVector", i32, i8, [3 x i8], %"class.std::__2::unique_ptr.105", [4 x i8] }>
%"class.draco::GeometryAttribute" = type { %"class.draco::DataBuffer"*, %"struct.draco::DataBufferDescriptor", i8, i32, i8, i64, i64, i32, i32 }
%"class.draco::DataBuffer" = type { %"class.std::__2::vector.86", %"struct.draco::DataBufferDescriptor" }
%"class.std::__2::vector.86" = type { %"class.std::__2::__vector_base.87" }
%"class.std::__2::__vector_base.87" = type { i8*, i8*, %"class.std::__2::__compressed_pair.88" }
%"class.std::__2::__compressed_pair.88" = type { %"struct.std::__2::__compressed_pair_elem.89" }
%"struct.std::__2::__compressed_pair_elem.89" = type { i8* }
%"struct.draco::DataBufferDescriptor" = type { i64, i64 }
%"class.std::__2::unique_ptr.93" = type { %"class.std::__2::__compressed_pair.94" }
%"class.std::__2::__compressed_pair.94" = type { %"struct.std::__2::__compressed_pair_elem.95" }
%"struct.std::__2::__compressed_pair_elem.95" = type { %"class.draco::DataBuffer"* }
%"class.draco::IndexTypeVector" = type { %"class.std::__2::vector.98" }
%"class.std::__2::vector.98" = type { %"class.std::__2::__vector_base.99" }
%"class.std::__2::__vector_base.99" = type { %"class.draco::IndexType"*, %"class.draco::IndexType"*, %"class.std::__2::__compressed_pair.100" }
%"class.draco::IndexType" = type { i32 }
%"class.std::__2::__compressed_pair.100" = type { %"struct.std::__2::__compressed_pair_elem.101" }
%"struct.std::__2::__compressed_pair_elem.101" = type { %"class.draco::IndexType"* }
%"class.std::__2::unique_ptr.105" = type { %"class.std::__2::__compressed_pair.106" }
%"class.std::__2::__compressed_pair.106" = type { %"struct.std::__2::__compressed_pair_elem.107" }
%"struct.std::__2::__compressed_pair_elem.107" = type { %"class.draco::AttributeTransformData"* }
%"class.draco::AttributeTransformData" = type { i32, %"class.draco::DataBuffer" }
%"class.std::__2::__compressed_pair.112" = type { %"struct.std::__2::__compressed_pair_elem.113" }
%"struct.std::__2::__compressed_pair_elem.113" = type { %"class.std::__2::unique_ptr.83"* }
%"class.std::__2::vector.117" = type { %"class.std::__2::__vector_base.118" }
%"class.std::__2::__vector_base.118" = type { i32*, i32*, %"class.std::__2::__compressed_pair.119" }
%"class.std::__2::__compressed_pair.119" = type { %"struct.std::__2::__compressed_pair_elem.120" }
%"struct.std::__2::__compressed_pair_elem.120" = type { i32* }
%"class.std::__2::vector.124" = type { %"class.std::__2::__vector_base.125" }
%"class.std::__2::__vector_base.125" = type { %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"*, %"class.std::__2::__compressed_pair.126" }
%"struct.draco::Mesh::AttributeData" = type { i32 }
%"class.std::__2::__compressed_pair.126" = type { %"struct.std::__2::__compressed_pair_elem.127" }
%"struct.std::__2::__compressed_pair_elem.127" = type { %"struct.draco::Mesh::AttributeData"* }
%"class.draco::IndexTypeVector.131" = type { %"class.std::__2::vector.132" }
%"class.std::__2::vector.132" = type { %"class.std::__2::__vector_base.133" }
%"class.std::__2::__vector_base.133" = type { %"struct.std::__2::array"*, %"struct.std::__2::array"*, %"class.std::__2::__compressed_pair.135" }
%"struct.std::__2::array" = type { [3 x %"class.draco::IndexType.134"] }
%"class.draco::IndexType.134" = type { i32 }
%"class.std::__2::__compressed_pair.135" = type { %"struct.std::__2::__compressed_pair_elem.136" }
%"struct.std::__2::__compressed_pair_elem.136" = type { %"struct.std::__2::array"* }
%"class.std::__2::unique_ptr.142" = type { %"class.std::__2::__compressed_pair.143" }
%"class.std::__2::__compressed_pair.143" = type { %"struct.std::__2::__compressed_pair_elem.144" }
%"struct.std::__2::__compressed_pair_elem.144" = type { i32* }
%"class.draco::IndexType.145" = type { i32 }
%class.anon = type { i32* }
%class.anon.146 = type { i32*, %class.anon*, i32**, %"class.std::__2::unique_ptr.142"* }
%class.anon.147 = type { [9 x i32]* }
%class.anon.148 = type { [9 x i32]*, %"class.std::__2::unique_ptr.142"*, %"class.std::__2::unique_ptr.142"* }
%"struct.std::__2::__default_init_tag" = type { i8 }
%"class.std::__2::__map_value_compare.15" = type { i8 }
%"struct.std::__2::less.16" = type { i8 }
%"struct.std::__2::__value_init_tag" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.10" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.14" = type { i8 }
%"class.std::__2::allocator.11" = type { i8 }
%"struct.std::__2::default_delete.141" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.140" = type { i8 }
%"class.std::__2::__tree_node" = type { %"class.std::__2::__tree_node_base.base", %"struct.std::__2::__value_type" }
%"class.std::__2::__tree_node_base.base" = type <{ %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_end_node"*, i8 }>
%"struct.std::__2::__value_type" = type { %"struct.std::__2::pair" }
%"struct.std::__2::pair" = type { i32, %"class.draco::Options" }
%"struct.std::__2::integral_constant" = type { i8 }
%"struct.std::__2::__has_destroy" = type { i8 }
%"class.std::__2::__tree_node.149" = type { %"class.std::__2::__tree_node_base.base", %"struct.std::__2::__value_type.150" }
%"struct.std::__2::__value_type.150" = type { %"struct.std::__2::pair.151" }
%"struct.std::__2::pair.151" = type { %"class.std::__2::basic_string", %"class.std::__2::basic_string" }
%"class.std::__2::allocator" = type { i8 }
%"struct.std::__2::__has_destroy.152" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.3" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.0" = type { i8 }
%"struct.(anonymous namespace)::FreeDeleter" = type { i8 }

$_ZN5draco7DecoderC2Ev = comdat any

$_ZNK5draco8StatusOrINSt3__210unique_ptrINS_4MeshENS1_14default_deleteIS3_EEEEE2okEv = comdat any

$_ZNKR5draco8StatusOrINSt3__210unique_ptrINS_4MeshENS1_14default_deleteIS3_EEEEE5valueEv = comdat any

$_ZNKSt3__210unique_ptrIN5draco4MeshENS_14default_deleteIS2_EEE3getEv = comdat any

$_ZNK5draco10PointCloud10num_pointsEv = comdat any

$_ZNK5draco4Mesh9num_facesEv = comdat any

$_ZNK5draco17GeometryAttribute14num_componentsEv = comdat any

$_ZNK5draco17GeometryAttribute9data_typeEv = comdat any

$_ZNK5draco4Mesh23GetAttributeElementTypeEi = comdat any

$_ZNK5draco17GeometryAttribute9unique_idEv = comdat any

$_ZNK5draco14PointAttribute4sizeEv = comdat any

$_ZNK5draco4Mesh4faceENS_9IndexTypeIjNS_19FaceIndex_tag_type_EEE = comdat any

$_ZN5draco9IndexTypeIjNS_19FaceIndex_tag_type_EEC2Ej = comdat any

$_ZNKSt3__25arrayIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELm3EEixEm = comdat any

$_ZNK5draco9IndexTypeIjNS_20PointIndex_tag_type_EE5valueEv = comdat any

$_ZNK5draco14PointAttribute19is_mapping_identityEv = comdat any

$_ZNK5draco14PointAttribute12mapped_indexENS_9IndexTypeIjNS_20PointIndex_tag_type_EEE = comdat any

$_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEC2Ej = comdat any

$_ZNK5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EE5valueEv = comdat any

$_ZNK5draco17GeometryAttribute10GetAddressENS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEE = comdat any

$_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEC2Ej = comdat any

$_ZN5draco8StatusOrINSt3__210unique_ptrINS_4MeshENS1_14default_deleteIS3_EEEEED2Ev = comdat any

$_ZN5draco7DecoderD2Ev = comdat any

$_ZN5draco13DecoderBufferD2Ev = comdat any

$_ZN5draco12DracoOptionsINS_17GeometryAttribute4TypeEEC2Ev = comdat any

$_ZNSt3__23mapIN5draco17GeometryAttribute4TypeENS1_7OptionsENS_4lessIS3_EENS_9allocatorINS_4pairIKS3_S4_EEEEEC2Ev = comdat any

$_ZNSt3__219__map_value_compareIN5draco17GeometryAttribute4TypeENS_12__value_typeIS3_NS1_7OptionsEEENS_4lessIS3_EELb1EEC2ES8_ = comdat any

$_ZNSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEEC2ERKSA_ = comdat any

$_ZNSt3__217__compressed_pairINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEENS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENSA_7OptionsEEES3_EEEEEC2ILb1EvEEv = comdat any

$_ZNSt3__217__compressed_pairImNS_19__map_value_compareIN5draco17GeometryAttribute4TypeENS_12__value_typeIS4_NS2_7OptionsEEENS_4lessIS4_EELb1EEEEC2IiRKSA_EEOT_OT0_ = comdat any

$_ZNSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE10__end_nodeEv = comdat any

$_ZNSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE12__begin_nodeEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEELi0ELb0EEC2ENS_16__value_init_tagE = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS4_7OptionsEEEPvEEEELi1ELb1EEC2ENS_16__value_init_tagE = comdat any

$_ZNSt3__215__tree_end_nodeIPNS_16__tree_node_baseIPvEEEC2Ev = comdat any

$_ZNSt3__29allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPvEEEC2Ev = comdat any

$_ZNSt3__27forwardIiEEOT_RNS_16remove_referenceIS1_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemImLi0ELb0EEC2IivEEOT_ = comdat any

$_ZNSt3__27forwardIRKNS_19__map_value_compareIN5draco17GeometryAttribute4TypeENS_12__value_typeIS4_NS2_7OptionsEEENS_4lessIS4_EELb1EEEEEOT_RNS_16remove_referenceISD_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemINS_19__map_value_compareIN5draco17GeometryAttribute4TypeENS_12__value_typeIS4_NS2_7OptionsEEENS_4lessIS4_EELb1EEELi1ELb1EEC2IRKSA_vEEOT_ = comdat any

$_ZNSt3__214pointer_traitsIPNS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEEE10pointer_toERS6_ = comdat any

$_ZNSt3__217__compressed_pairINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEENS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENSA_7OptionsEEES3_EEEEE5firstEv = comdat any

$_ZNSt3__29addressofINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEEEEPT_RS7_ = comdat any

$_ZNSt3__222__compressed_pair_elemINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEELi0ELb0EE5__getEv = comdat any

$_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEENSt3__25arrayINS1_IjNS_20PointIndex_tag_type_EEELm3EEEE4sizeEv = comdat any

$_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4sizeEv = comdat any

$_ZNKSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEEixEm = comdat any

$_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEENSt3__25arrayINS1_IjNS_20PointIndex_tag_type_EEELm3EEEEixERKS3_ = comdat any

$_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEEixEm = comdat any

$_ZNK5draco9IndexTypeIjNS_19FaceIndex_tag_type_EE5valueEv = comdat any

$_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEEEixERKS3_ = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEEixEm = comdat any

$_ZNK5draco17GeometryAttribute10GetBytePosENS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEE = comdat any

$_ZN5draco10DataBuffer4dataEv = comdat any

$_ZNSt3__26vectorIhNS_9allocatorIhEEEixEm = comdat any

$_ZNSt3__214numeric_limitsIjE3maxEv = comdat any

$_ZNSt3__223__libcpp_numeric_limitsIjLb1EE3maxEv = comdat any

$_ZNSt3__210unique_ptrIN5draco4MeshENS_14default_deleteIS2_EEED2Ev = comdat any

$_ZN5draco6StatusD2Ev = comdat any

$_ZNSt3__210unique_ptrIN5draco4MeshENS_14default_deleteIS2_EEE5resetEPS2_ = comdat any

$_ZNSt3__217__compressed_pairIPN5draco4MeshENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco4MeshENS_14default_deleteIS2_EEE6secondEv = comdat any

$_ZNKSt3__214default_deleteIN5draco4MeshEEclEPS2_ = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco4MeshELi0ELb0EE5__getEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco4MeshEEELi1ELb1EE5__getEv = comdat any

$_ZN5draco12DracoOptionsINS_17GeometryAttribute4TypeEED2Ev = comdat any

$_ZNSt3__23mapIN5draco17GeometryAttribute4TypeENS1_7OptionsENS_4lessIS3_EENS_9allocatorINS_4pairIKS3_S4_EEEEED2Ev = comdat any

$_ZN5draco7OptionsD2Ev = comdat any

$_ZNSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEED2Ev = comdat any

$_ZNSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE7destroyEPNS_11__tree_nodeIS6_PvEE = comdat any

$_ZNKSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE6__rootEv = comdat any

$_ZNSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE12__node_allocEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS4_7OptionsEEEPvEEEEE7destroyINS_4pairIKS6_S7_EEEEvRSB_PT_ = comdat any

$_ZNSt3__222__tree_key_value_typesINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEEE9__get_ptrERS6_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS4_7OptionsEEEPvEEEEE10deallocateERSB_PSA_m = comdat any

$_ZNSt3__217__compressed_pairINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEENS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENSA_7OptionsEEES3_EEEEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS4_7OptionsEEEPvEEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS4_7OptionsEEEPvEEEEE9__destroyINS_4pairIKS6_S7_EEEEvNS_17integral_constantIbLb0EEERSB_PT_ = comdat any

$_ZNSt3__24pairIKN5draco17GeometryAttribute4TypeENS1_7OptionsEED2Ev = comdat any

$_ZNSt3__29addressofINS_4pairIKN5draco17GeometryAttribute4TypeENS2_7OptionsEEEEEPT_RS8_ = comdat any

$_ZNSt3__212__value_typeIN5draco17GeometryAttribute4TypeENS1_7OptionsEE11__get_valueEv = comdat any

$_ZNSt3__29allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPvEEE10deallocateEPS9_m = comdat any

$_ZNSt3__219__libcpp_deallocateEPvmm = comdat any

$_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm = comdat any

$_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm = comdat any

$_ZNSt3__217_DeallocateCaller9__do_callEPv = comdat any

$_ZNKSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE10__end_nodeEv = comdat any

$_ZNKSt3__217__compressed_pairINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEENS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENSA_7OptionsEEES3_EEEEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__23mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_NS_4lessIS6_EENS4_INS_4pairIKS6_S6_EEEEED2Ev = comdat any

$_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEED2Ev = comdat any

$_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE7destroyEPNS_11__tree_nodeIS8_PvEE = comdat any

$_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE6__rootEv = comdat any

$_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE12__node_allocEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEEE7destroyINS_4pairIKS8_S8_EEEEvRSC_PT_ = comdat any

$_ZNSt3__222__tree_key_value_typesINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEE9__get_ptrERS8_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEEE10deallocateERSC_PSB_m = comdat any

$_ZNSt3__217__compressed_pairINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEENS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS7_IcEEEESE_EES3_EEEEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEEE9__destroyINS_4pairIKS8_S8_EEEEvNS_17integral_constantIbLb0EEERSC_PT_ = comdat any

$_ZNSt3__24pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_ED2Ev = comdat any

$_ZNSt3__29addressofINS_4pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEEEPT_RSA_ = comdat any

$_ZNSt3__212__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_E11__get_valueEv = comdat any

$_ZNSt3__29allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEES7_EEPvEEE10deallocateEPSA_m = comdat any

$_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE10__end_nodeEv = comdat any

$_ZNKSt3__217__compressed_pairINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEENS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS7_IcEEEESE_EES3_EEEEE5firstEv = comdat any

$_ZNSt3__27forwardIRPcEEOT_RNS_16remove_referenceIS3_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPcLi0ELb0EEC2IRS1_vEEOT_ = comdat any

$_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPcLi0ELb0EE5__getEv = comdat any

$_ZNK5draco6Status2okEv = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco4MeshENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPN5draco4MeshELi0ELb0EE5__getEv = comdat any

$_ZNSt3__27forwardIRPjEEOT_RNS_16remove_referenceIS3_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPjLi0ELb0EEC2IRS1_vEEOT_ = comdat any

$_ZNSt3__222__compressed_pair_elemIPjLi0ELb0EE5__getEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPjLi0ELb0EE5__getEv = comdat any

@_ZN12_GLOBAL__N_122first_bit_lookup_tableE = internal constant [256 x i8] c"\00\00\01\00\02\00\01\00\03\00\01\00\02\00\01\00\04\00\01\00\02\00\01\00\03\00\01\00\02\00\01\00\05\00\01\00\02\00\01\00\03\00\01\00\02\00\01\00\04\00\01\00\02\00\01\00\03\00\01\00\02\00\01\00\06\00\01\00\02\00\01\00\03\00\01\00\02\00\01\00\04\00\01\00\02\00\01\00\03\00\01\00\02\00\01\00\05\00\01\00\02\00\01\00\03\00\01\00\02\00\01\00\04\00\01\00\02\00\01\00\03\00\01\00\02\00\01\00\07\00\01\00\02\00\01\00\03\00\01\00\02\00\01\00\04\00\01\00\02\00\01\00\03\00\01\00\02\00\01\00\05\00\01\00\02\00\01\00\03\00\01\00\02\00\01\00\04\00\01\00\02\00\01\00\03\00\01\00\02\00\01\00\06\00\01\00\02\00\01\00\03\00\01\00\02\00\01\00\04\00\01\00\02\00\01\00\03\00\01\00\02\00\01\00\05\00\01\00\02\00\01\00\03\00\01\00\02\00\01\00\04\00\01\00\02\00\01\00\03\00\01\00\02\00\01\00", align 16

; Function Attrs: noinline nounwind optnone
define hidden i32 @neuroglancer_draco_decode(i8* %input, i32 %input_size, i1 zeroext %partition, i32 %vertex_quantization_bits) #0 {
entry:
  %retval = alloca i32, align 4
  %input.addr = alloca i8*, align 4
  %input_size.addr = alloca i32, align 4
  %partition.addr = alloca i8, align 1
  %vertex_quantization_bits.addr = alloca i32, align 4
  %input_deleter = alloca %"class.std::__2::unique_ptr", align 4
  %decoder_buffer = alloca %"class.draco::DecoderBuffer", align 8
  %decoder = alloca %"class.draco::Decoder", align 4
  %decoded_mesh_statusor = alloca %"class.draco::StatusOr", align 4
  %cleanup.dest.slot = alloca i32, align 4
  %decoded_mesh = alloca %"class.draco::Mesh"*, align 4
  %num_vertices = alloca i32, align 4
  %num_faces = alloca i32, align 4
  %position_att = alloca %"class.draco::PointAttribute"*, align 4
  %indices = alloca %"class.std::__2::unique_ptr.142", align 4
  %face_i = alloca i32, align 4
  %face = alloca %"struct.std::__2::array"*, align 4
  %agg.tmp = alloca %"class.draco::IndexType.145", align 4
  %i = alloca i32, align 4
  %i46 = alloca i32, align 4
  %ref.tmp = alloca %"class.draco::IndexType", align 4
  %agg.tmp51 = alloca %"class.draco::IndexType.134", align 4
  %vertex_positions = alloca i32*, align 4
  %agg.tmp63 = alloca %"class.draco::IndexType", align 4
  %get_vertex_mask = alloca %class.anon, align 4
  %partitioned_indices = alloca %"class.std::__2::unique_ptr.142", align 4
  %for_each_face = alloca %class.anon.146, align 4
  %subchunk_offsets = alloca [9 x i32], align 16
  %agg.tmp72 = alloca %class.anon.147, align 4
  %sum = alloca i32, align 4
  %i74 = alloca i32, align 4
  %count = alloca i32, align 4
  %agg.tmp85 = alloca %class.anon.148, align 4
  %subchunk_offsets88 = alloca [2 x i32], align 4
  store i8* %input, i8** %input.addr, align 4
  store i32 %input_size, i32* %input_size.addr, align 4
  %frombool = zext i1 %partition to i8
  store i8 %frombool, i8* %partition.addr, align 1
  store i32 %vertex_quantization_bits, i32* %vertex_quantization_bits.addr, align 4
  %0 = load i8*, i8** %input.addr, align 4
  %call = call %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrIA_cN12_GLOBAL__N_111FreeDeleterEEC2IPcLb1EvvEET_(%"class.std::__2::unique_ptr"* %input_deleter, i8* %0) #6
  %call1 = call %"class.draco::DecoderBuffer"* @_ZN5draco13DecoderBufferC1Ev(%"class.draco::DecoderBuffer"* %decoder_buffer)
  %1 = load i8*, i8** %input.addr, align 4
  %2 = load i32, i32* %input_size.addr, align 4
  call void @_ZN5draco13DecoderBuffer4InitEPKcm(%"class.draco::DecoderBuffer"* %decoder_buffer, i8* %1, i32 %2)
  %call2 = call %"class.draco::Decoder"* @_ZN5draco7DecoderC2Ev(%"class.draco::Decoder"* %decoder)
  call void @_ZN5draco7Decoder25SetSkipAttributeTransformENS_17GeometryAttribute4TypeE(%"class.draco::Decoder"* %decoder, i32 0)
  call void @_ZN5draco7Decoder20DecodeMeshFromBufferEPNS_13DecoderBufferE(%"class.draco::StatusOr"* sret align 4 %decoded_mesh_statusor, %"class.draco::Decoder"* %decoder, %"class.draco::DecoderBuffer"* %decoder_buffer)
  %call3 = call zeroext i1 @_ZNK5draco8StatusOrINSt3__210unique_ptrINS_4MeshENS1_14default_deleteIS3_EEEEE2okEv(%"class.draco::StatusOr"* %decoded_mesh_statusor)
  br i1 %call3, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %call4 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.23"* @_ZNKR5draco8StatusOrINSt3__210unique_ptrINS_4MeshENS1_14default_deleteIS3_EEEEE5valueEv(%"class.draco::StatusOr"* %decoded_mesh_statusor)
  %call5 = call %"class.draco::Mesh"* @_ZNKSt3__210unique_ptrIN5draco4MeshENS_14default_deleteIS2_EEE3getEv(%"class.std::__2::unique_ptr.23"* %call4) #6
  store %"class.draco::Mesh"* %call5, %"class.draco::Mesh"** %decoded_mesh, align 4
  %3 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %decoded_mesh, align 4
  %4 = bitcast %"class.draco::Mesh"* %3 to %"class.draco::PointCloud"*
  %call6 = call i32 @_ZNK5draco10PointCloud10num_pointsEv(%"class.draco::PointCloud"* %4)
  store i32 %call6, i32* %num_vertices, align 4
  %5 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %decoded_mesh, align 4
  %call7 = call i32 @_ZNK5draco4Mesh9num_facesEv(%"class.draco::Mesh"* %5)
  store i32 %call7, i32* %num_faces, align 4
  %6 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %decoded_mesh, align 4
  %7 = bitcast %"class.draco::Mesh"* %6 to %"class.draco::PointCloud"*
  %call8 = call %"class.draco::PointAttribute"* @_ZNK5draco10PointCloud17GetNamedAttributeENS_17GeometryAttribute4TypeE(%"class.draco::PointCloud"* %7, i32 0)
  store %"class.draco::PointAttribute"* %call8, %"class.draco::PointAttribute"** %position_att, align 4
  %8 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %position_att, align 4
  %tobool = icmp ne %"class.draco::PointAttribute"* %8, null
  br i1 %tobool, label %if.end10, label %if.then9

if.then9:                                         ; preds = %if.end
  store i32 3, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end10:                                         ; preds = %if.end
  %9 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %position_att, align 4
  %10 = bitcast %"class.draco::PointAttribute"* %9 to %"class.draco::GeometryAttribute"*
  %call11 = call signext i8 @_ZNK5draco17GeometryAttribute14num_componentsEv(%"class.draco::GeometryAttribute"* %10)
  %conv = sext i8 %call11 to i32
  %cmp = icmp ne i32 %conv, 3
  br i1 %cmp, label %if.then12, label %if.end13

if.then12:                                        ; preds = %if.end10
  store i32 4, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end13:                                         ; preds = %if.end10
  %11 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %position_att, align 4
  %12 = bitcast %"class.draco::PointAttribute"* %11 to %"class.draco::GeometryAttribute"*
  %call14 = call i32 @_ZNK5draco17GeometryAttribute9data_typeEv(%"class.draco::GeometryAttribute"* %12)
  %cmp15 = icmp ne i32 %call14, 5
  br i1 %cmp15, label %if.then16, label %if.end17

if.then16:                                        ; preds = %if.end13
  store i32 5, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end17:                                         ; preds = %if.end13
  %13 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %decoded_mesh, align 4
  %14 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %position_att, align 4
  %15 = bitcast %"class.draco::PointAttribute"* %14 to %"class.draco::GeometryAttribute"*
  %call18 = call i32 @_ZNK5draco17GeometryAttribute9unique_idEv(%"class.draco::GeometryAttribute"* %15)
  %call19 = call i32 @_ZNK5draco4Mesh23GetAttributeElementTypeEi(%"class.draco::Mesh"* %13, i32 %call18)
  %cmp20 = icmp ne i32 %call19, 1
  br i1 %cmp20, label %if.then21, label %if.end22

if.then21:                                        ; preds = %if.end17
  store i32 11, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end22:                                         ; preds = %if.end17
  %16 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %position_att, align 4
  %call23 = call i32 @_ZNK5draco14PointAttribute4sizeEv(%"class.draco::PointAttribute"* %16)
  %17 = load i32, i32* %num_vertices, align 4
  %cmp24 = icmp ne i32 %call23, %17
  br i1 %cmp24, label %if.then25, label %if.end27

if.then25:                                        ; preds = %if.end22
  %18 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %position_att, align 4
  %call26 = call i32 @_ZNK5draco14PointAttribute4sizeEv(%"class.draco::PointAttribute"* %18)
  %add = add i32 1000, %call26
  store i32 %add, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end27:                                         ; preds = %if.end22
  %19 = load i32, i32* %num_faces, align 4
  %mul = mul i32 12, %19
  %call28 = call i8* @malloc(i32 %mul)
  %20 = bitcast i8* %call28 to i32*
  %call29 = call %"class.std::__2::unique_ptr.142"* @_ZNSt3__210unique_ptrIA_jN12_GLOBAL__N_111FreeDeleterEEC2IPjLb1EvvEET_(%"class.std::__2::unique_ptr.142"* %indices, i32* %20) #6
  store i32 0, i32* %face_i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc41, %if.end27
  %21 = load i32, i32* %face_i, align 4
  %22 = load i32, i32* %num_faces, align 4
  %cmp30 = icmp ult i32 %21, %22
  br i1 %cmp30, label %for.body, label %for.end43

for.body:                                         ; preds = %for.cond
  %23 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %decoded_mesh, align 4
  %24 = load i32, i32* %face_i, align 4
  %call31 = call %"class.draco::IndexType.145"* @_ZN5draco9IndexTypeIjNS_19FaceIndex_tag_type_EEC2Ej(%"class.draco::IndexType.145"* %agg.tmp, i32 %24)
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType.145", %"class.draco::IndexType.145"* %agg.tmp, i32 0, i32 0
  %25 = load i32, i32* %coerce.dive, align 4
  %call32 = call nonnull align 4 dereferenceable(12) %"struct.std::__2::array"* @_ZNK5draco4Mesh4faceENS_9IndexTypeIjNS_19FaceIndex_tag_type_EEE(%"class.draco::Mesh"* %23, i32 %25)
  store %"struct.std::__2::array"* %call32, %"struct.std::__2::array"** %face, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond33

for.cond33:                                       ; preds = %for.inc, %for.body
  %26 = load i32, i32* %i, align 4
  %cmp34 = icmp slt i32 %26, 3
  br i1 %cmp34, label %for.body35, label %for.end

for.body35:                                       ; preds = %for.cond33
  %27 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %face, align 4
  %28 = load i32, i32* %i, align 4
  %call36 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.134"* @_ZNKSt3__25arrayIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELm3EEixEm(%"struct.std::__2::array"* %27, i32 %28) #6
  %call37 = call i32 @_ZNK5draco9IndexTypeIjNS_20PointIndex_tag_type_EE5valueEv(%"class.draco::IndexType.134"* %call36)
  %29 = load i32, i32* %face_i, align 4
  %mul38 = mul i32 %29, 3
  %30 = load i32, i32* %i, align 4
  %add39 = add i32 %mul38, %30
  %call40 = call nonnull align 4 dereferenceable(4) i32* @_ZNKSt3__210unique_ptrIA_jN12_GLOBAL__N_111FreeDeleterEEixEm(%"class.std::__2::unique_ptr.142"* %indices, i32 %add39)
  store i32 %call37, i32* %call40, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body35
  %31 = load i32, i32* %i, align 4
  %inc = add nsw i32 %31, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond33

for.end:                                          ; preds = %for.cond33
  br label %for.inc41

for.inc41:                                        ; preds = %for.end
  %32 = load i32, i32* %face_i, align 4
  %inc42 = add i32 %32, 1
  store i32 %inc42, i32* %face_i, align 4
  br label %for.cond

for.end43:                                        ; preds = %for.cond
  %33 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %position_att, align 4
  %call44 = call zeroext i1 @_ZNK5draco14PointAttribute19is_mapping_identityEv(%"class.draco::PointAttribute"* %33)
  br i1 %call44, label %if.end62, label %if.then45

if.then45:                                        ; preds = %for.end43
  store i32 0, i32* %i46, align 4
  br label %for.cond47

for.cond47:                                       ; preds = %for.inc59, %if.then45
  %34 = load i32, i32* %i46, align 4
  %35 = load i32, i32* %num_faces, align 4
  %mul48 = mul i32 %35, 3
  %cmp49 = icmp ult i32 %34, %mul48
  br i1 %cmp49, label %for.body50, label %for.end61

for.body50:                                       ; preds = %for.cond47
  %36 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %position_att, align 4
  %37 = load i32, i32* %i46, align 4
  %call52 = call nonnull align 4 dereferenceable(4) i32* @_ZNKSt3__210unique_ptrIA_jN12_GLOBAL__N_111FreeDeleterEEixEm(%"class.std::__2::unique_ptr.142"* %indices, i32 %37)
  %38 = load i32, i32* %call52, align 4
  %call53 = call %"class.draco::IndexType.134"* @_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEC2Ej(%"class.draco::IndexType.134"* %agg.tmp51, i32 %38)
  %coerce.dive54 = getelementptr inbounds %"class.draco::IndexType.134", %"class.draco::IndexType.134"* %agg.tmp51, i32 0, i32 0
  %39 = load i32, i32* %coerce.dive54, align 4
  %call55 = call i32 @_ZNK5draco14PointAttribute12mapped_indexENS_9IndexTypeIjNS_20PointIndex_tag_type_EEE(%"class.draco::PointAttribute"* %36, i32 %39)
  %coerce.dive56 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %ref.tmp, i32 0, i32 0
  store i32 %call55, i32* %coerce.dive56, align 4
  %call57 = call i32 @_ZNK5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EE5valueEv(%"class.draco::IndexType"* %ref.tmp)
  %40 = load i32, i32* %i46, align 4
  %call58 = call nonnull align 4 dereferenceable(4) i32* @_ZNKSt3__210unique_ptrIA_jN12_GLOBAL__N_111FreeDeleterEEixEm(%"class.std::__2::unique_ptr.142"* %indices, i32 %40)
  store i32 %call57, i32* %call58, align 4
  br label %for.inc59

for.inc59:                                        ; preds = %for.body50
  %41 = load i32, i32* %i46, align 4
  %inc60 = add i32 %41, 1
  store i32 %inc60, i32* %i46, align 4
  br label %for.cond47

for.end61:                                        ; preds = %for.cond47
  br label %if.end62

if.end62:                                         ; preds = %for.end61, %for.end43
  %42 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %position_att, align 4
  %43 = bitcast %"class.draco::PointAttribute"* %42 to %"class.draco::GeometryAttribute"*
  %call64 = call %"class.draco::IndexType"* @_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEC2Ej(%"class.draco::IndexType"* %agg.tmp63, i32 0)
  %coerce.dive65 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %agg.tmp63, i32 0, i32 0
  %44 = load i32, i32* %coerce.dive65, align 4
  %call66 = call i8* @_ZNK5draco17GeometryAttribute10GetAddressENS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEE(%"class.draco::GeometryAttribute"* %43, i32 %44)
  %45 = bitcast i8* %call66 to i32*
  store i32* %45, i32** %vertex_positions, align 4
  %46 = load i8, i8* %partition.addr, align 1
  %tobool67 = trunc i8 %46 to i1
  br i1 %tobool67, label %if.then68, label %if.else

if.then68:                                        ; preds = %if.end62
  %47 = getelementptr inbounds %class.anon, %class.anon* %get_vertex_mask, i32 0, i32 0
  store i32* %vertex_quantization_bits.addr, i32** %47, align 4
  %48 = load i32, i32* %num_faces, align 4
  %mul69 = mul i32 12, %48
  %call70 = call i8* @malloc(i32 %mul69)
  %49 = bitcast i8* %call70 to i32*
  %call71 = call %"class.std::__2::unique_ptr.142"* @_ZNSt3__210unique_ptrIA_jN12_GLOBAL__N_111FreeDeleterEEC2IPjLb1EvvEET_(%"class.std::__2::unique_ptr.142"* %partitioned_indices, i32* %49) #6
  %50 = getelementptr inbounds %class.anon.146, %class.anon.146* %for_each_face, i32 0, i32 0
  store i32* %num_faces, i32** %50, align 4
  %51 = getelementptr inbounds %class.anon.146, %class.anon.146* %for_each_face, i32 0, i32 1
  store %class.anon* %get_vertex_mask, %class.anon** %51, align 4
  %52 = getelementptr inbounds %class.anon.146, %class.anon.146* %for_each_face, i32 0, i32 2
  store i32** %vertex_positions, i32*** %52, align 4
  %53 = getelementptr inbounds %class.anon.146, %class.anon.146* %for_each_face, i32 0, i32 3
  store %"class.std::__2::unique_ptr.142"* %indices, %"class.std::__2::unique_ptr.142"** %53, align 4
  %54 = bitcast [9 x i32]* %subchunk_offsets to i8*
  call void @llvm.memset.p0i8.i32(i8* align 16 %54, i8 0, i32 36, i1 false)
  %55 = getelementptr inbounds %class.anon.147, %class.anon.147* %agg.tmp72, i32 0, i32 0
  store [9 x i32]* %subchunk_offsets, [9 x i32]** %55, align 4
  %coerce.dive73 = getelementptr inbounds %class.anon.147, %class.anon.147* %agg.tmp72, i32 0, i32 0
  %56 = load [9 x i32]*, [9 x i32]** %coerce.dive73, align 4
  call void @"_ZZ25neuroglancer_draco_decodeENK3$_1clIZ25neuroglancer_draco_decodeE3$_2EEDaT_"(%class.anon.146* %for_each_face, [9 x i32]* %56)
  store i32 0, i32* %sum, align 4
  store i32 0, i32* %i74, align 4
  br label %for.cond75

for.cond75:                                       ; preds = %for.inc82, %if.then68
  %57 = load i32, i32* %i74, align 4
  %cmp76 = icmp slt i32 %57, 8
  br i1 %cmp76, label %for.body77, label %for.end84

for.body77:                                       ; preds = %for.cond75
  %58 = load i32, i32* %i74, align 4
  %add78 = add nsw i32 %58, 1
  %arrayidx = getelementptr inbounds [9 x i32], [9 x i32]* %subchunk_offsets, i32 0, i32 %add78
  %59 = load i32, i32* %arrayidx, align 4
  store i32 %59, i32* %count, align 4
  %60 = load i32, i32* %sum, align 4
  %61 = load i32, i32* %i74, align 4
  %add79 = add nsw i32 %61, 1
  %arrayidx80 = getelementptr inbounds [9 x i32], [9 x i32]* %subchunk_offsets, i32 0, i32 %add79
  store i32 %60, i32* %arrayidx80, align 4
  %62 = load i32, i32* %count, align 4
  %63 = load i32, i32* %sum, align 4
  %add81 = add i32 %63, %62
  store i32 %add81, i32* %sum, align 4
  br label %for.inc82

for.inc82:                                        ; preds = %for.body77
  %64 = load i32, i32* %i74, align 4
  %inc83 = add nsw i32 %64, 1
  store i32 %inc83, i32* %i74, align 4
  br label %for.cond75

for.end84:                                        ; preds = %for.cond75
  %65 = getelementptr inbounds %class.anon.148, %class.anon.148* %agg.tmp85, i32 0, i32 0
  store [9 x i32]* %subchunk_offsets, [9 x i32]** %65, align 4
  %66 = getelementptr inbounds %class.anon.148, %class.anon.148* %agg.tmp85, i32 0, i32 1
  store %"class.std::__2::unique_ptr.142"* %partitioned_indices, %"class.std::__2::unique_ptr.142"** %66, align 4
  %67 = getelementptr inbounds %class.anon.148, %class.anon.148* %agg.tmp85, i32 0, i32 2
  store %"class.std::__2::unique_ptr.142"* %indices, %"class.std::__2::unique_ptr.142"** %67, align 4
  call void @"_ZZ25neuroglancer_draco_decodeENK3$_1clIZ25neuroglancer_draco_decodeE3$_3EEDaT_"(%class.anon.146* %for_each_face, %class.anon.148* byval(%class.anon.148) align 4 %agg.tmp85)
  %68 = load i32, i32* %num_faces, align 4
  %69 = load i32, i32* %num_vertices, align 4
  %call86 = call i32* @_ZNKSt3__210unique_ptrIA_jN12_GLOBAL__N_111FreeDeleterEE3getEv(%"class.std::__2::unique_ptr.142"* %partitioned_indices) #6
  %70 = bitcast i32* %call86 to i8*
  %71 = load i32*, i32** %vertex_positions, align 4
  %72 = bitcast i32* %71 to i8*
  %arraydecay = getelementptr inbounds [9 x i32], [9 x i32]* %subchunk_offsets, i32 0, i32 0
  %73 = bitcast i32* %arraydecay to i8*
  call void @neuroglancer_draco_receive_decoded_mesh(i32 %68, i32 %69, i8* %70, i8* %72, i8* %73)
  %call87 = call %"class.std::__2::unique_ptr.142"* @_ZNSt3__210unique_ptrIA_jN12_GLOBAL__N_111FreeDeleterEED2Ev(%"class.std::__2::unique_ptr.142"* %partitioned_indices) #6
  br label %if.end92

if.else:                                          ; preds = %if.end62
  %arrayinit.begin = getelementptr inbounds [2 x i32], [2 x i32]* %subchunk_offsets88, i32 0, i32 0
  store i32 0, i32* %arrayinit.begin, align 4
  %arrayinit.element = getelementptr inbounds i32, i32* %arrayinit.begin, i32 1
  %74 = load i32, i32* %num_faces, align 4
  %mul89 = mul i32 %74, 3
  store i32 %mul89, i32* %arrayinit.element, align 4
  %75 = load i32, i32* %num_faces, align 4
  %76 = load i32, i32* %num_vertices, align 4
  %call90 = call i32* @_ZNKSt3__210unique_ptrIA_jN12_GLOBAL__N_111FreeDeleterEE3getEv(%"class.std::__2::unique_ptr.142"* %indices) #6
  %77 = bitcast i32* %call90 to i8*
  %78 = load i32*, i32** %vertex_positions, align 4
  %79 = bitcast i32* %78 to i8*
  %arraydecay91 = getelementptr inbounds [2 x i32], [2 x i32]* %subchunk_offsets88, i32 0, i32 0
  %80 = bitcast i32* %arraydecay91 to i8*
  call void @neuroglancer_draco_receive_decoded_mesh(i32 %75, i32 %76, i8* %77, i8* %79, i8* %80)
  br label %if.end92

if.end92:                                         ; preds = %if.else, %for.end84
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %call93 = call %"class.std::__2::unique_ptr.142"* @_ZNSt3__210unique_ptrIA_jN12_GLOBAL__N_111FreeDeleterEED2Ev(%"class.std::__2::unique_ptr.142"* %indices) #6
  br label %cleanup

cleanup:                                          ; preds = %if.end92, %if.then25, %if.then21, %if.then16, %if.then12, %if.then9, %if.then
  %call94 = call %"class.draco::StatusOr"* @_ZN5draco8StatusOrINSt3__210unique_ptrINS_4MeshENS1_14default_deleteIS3_EEEEED2Ev(%"class.draco::StatusOr"* %decoded_mesh_statusor) #6
  %call96 = call %"class.draco::Decoder"* @_ZN5draco7DecoderD2Ev(%"class.draco::Decoder"* %decoder) #6
  %call98 = call %"class.draco::DecoderBuffer"* @_ZN5draco13DecoderBufferD2Ev(%"class.draco::DecoderBuffer"* %decoder_buffer) #6
  %call100 = call %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrIA_cN12_GLOBAL__N_111FreeDeleterEED2Ev(%"class.std::__2::unique_ptr"* %input_deleter) #6
  %81 = load i32, i32* %retval, align 4
  ret i32 %81
}

; Function Attrs: noinline nounwind optnone
define internal %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrIA_cN12_GLOBAL__N_111FreeDeleterEEC2IPcLb1EvvEET_(%"class.std::__2::unique_ptr"* returned %this, i8* %__p) unnamed_addr #0 {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  %__p.addr = alloca i8*, align 4
  %ref.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::__compressed_pair"* @_ZNSt3__217__compressed_pairIPcN12_GLOBAL__N_111FreeDeleterEEC2IRS1_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair"* %__ptr_, i8** nonnull align 4 dereferenceable(4) %__p.addr, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp)
  ret %"class.std::__2::unique_ptr"* %this1
}

declare %"class.draco::DecoderBuffer"* @_ZN5draco13DecoderBufferC1Ev(%"class.draco::DecoderBuffer"* returned) unnamed_addr #1

declare void @_ZN5draco13DecoderBuffer4InitEPKcm(%"class.draco::DecoderBuffer"*, i8*, i32) #1

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::Decoder"* @_ZN5draco7DecoderC2Ev(%"class.draco::Decoder"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::Decoder"*, align 4
  store %"class.draco::Decoder"* %this, %"class.draco::Decoder"** %this.addr, align 4
  %this1 = load %"class.draco::Decoder"*, %"class.draco::Decoder"** %this.addr, align 4
  %options_ = getelementptr inbounds %"class.draco::Decoder", %"class.draco::Decoder"* %this1, i32 0, i32 0
  %call = call %"class.draco::DracoOptions"* @_ZN5draco12DracoOptionsINS_17GeometryAttribute4TypeEEC2Ev(%"class.draco::DracoOptions"* %options_)
  ret %"class.draco::Decoder"* %this1
}

declare void @_ZN5draco7Decoder25SetSkipAttributeTransformENS_17GeometryAttribute4TypeE(%"class.draco::Decoder"*, i32) #1

declare void @_ZN5draco7Decoder20DecodeMeshFromBufferEPNS_13DecoderBufferE(%"class.draco::StatusOr"* sret align 4, %"class.draco::Decoder"*, %"class.draco::DecoderBuffer"*) #1

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK5draco8StatusOrINSt3__210unique_ptrINS_4MeshENS1_14default_deleteIS3_EEEEE2okEv(%"class.draco::StatusOr"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::StatusOr"*, align 4
  store %"class.draco::StatusOr"* %this, %"class.draco::StatusOr"** %this.addr, align 4
  %this1 = load %"class.draco::StatusOr"*, %"class.draco::StatusOr"** %this.addr, align 4
  %status_ = getelementptr inbounds %"class.draco::StatusOr", %"class.draco::StatusOr"* %this1, i32 0, i32 0
  %call = call zeroext i1 @_ZNK5draco6Status2okEv(%"class.draco::Status"* %status_)
  ret i1 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.23"* @_ZNKR5draco8StatusOrINSt3__210unique_ptrINS_4MeshENS1_14default_deleteIS3_EEEEE5valueEv(%"class.draco::StatusOr"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::StatusOr"*, align 4
  store %"class.draco::StatusOr"* %this, %"class.draco::StatusOr"** %this.addr, align 4
  %this1 = load %"class.draco::StatusOr"*, %"class.draco::StatusOr"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::StatusOr", %"class.draco::StatusOr"* %this1, i32 0, i32 1
  ret %"class.std::__2::unique_ptr.23"* %value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::Mesh"* @_ZNKSt3__210unique_ptrIN5draco4MeshENS_14default_deleteIS2_EEE3getEv(%"class.std::__2::unique_ptr.23"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.23"*, align 4
  store %"class.std::__2::unique_ptr.23"* %this, %"class.std::__2::unique_ptr.23"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.23"*, %"class.std::__2::unique_ptr.23"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.23", %"class.std::__2::unique_ptr.23"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::Mesh"** @_ZNKSt3__217__compressed_pairIPN5draco4MeshENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.24"* %__ptr_) #6
  %0 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %call, align 4
  ret %"class.draco::Mesh"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco10PointCloud10num_pointsEv(%"class.draco::PointCloud"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointCloud"*, align 4
  store %"class.draco::PointCloud"* %this, %"class.draco::PointCloud"** %this.addr, align 4
  %this1 = load %"class.draco::PointCloud"*, %"class.draco::PointCloud"** %this.addr, align 4
  %num_points_ = getelementptr inbounds %"class.draco::PointCloud", %"class.draco::PointCloud"* %this1, i32 0, i32 4
  %0 = load i32, i32* %num_points_, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco4Mesh9num_facesEv(%"class.draco::Mesh"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::Mesh"*, align 4
  store %"class.draco::Mesh"* %this, %"class.draco::Mesh"** %this.addr, align 4
  %this1 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %this.addr, align 4
  %faces_ = getelementptr inbounds %"class.draco::Mesh", %"class.draco::Mesh"* %this1, i32 0, i32 2
  %call = call i32 @_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEENSt3__25arrayINS1_IjNS_20PointIndex_tag_type_EEELm3EEEE4sizeEv(%"class.draco::IndexTypeVector.131"* %faces_)
  ret i32 %call
}

declare %"class.draco::PointAttribute"* @_ZNK5draco10PointCloud17GetNamedAttributeENS_17GeometryAttribute4TypeE(%"class.draco::PointCloud"*, i32) #1

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden signext i8 @_ZNK5draco17GeometryAttribute14num_componentsEv(%"class.draco::GeometryAttribute"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::GeometryAttribute"*, align 4
  store %"class.draco::GeometryAttribute"* %this, %"class.draco::GeometryAttribute"** %this.addr, align 4
  %this1 = load %"class.draco::GeometryAttribute"*, %"class.draco::GeometryAttribute"** %this.addr, align 4
  %num_components_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 2
  %0 = load i8, i8* %num_components_, align 8
  ret i8 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco17GeometryAttribute9data_typeEv(%"class.draco::GeometryAttribute"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::GeometryAttribute"*, align 4
  store %"class.draco::GeometryAttribute"* %this, %"class.draco::GeometryAttribute"** %this.addr, align 4
  %this1 = load %"class.draco::GeometryAttribute"*, %"class.draco::GeometryAttribute"** %this.addr, align 4
  %data_type_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 3
  %0 = load i32, i32* %data_type_, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco4Mesh23GetAttributeElementTypeEi(%"class.draco::Mesh"* %this, i32 %att_id) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::Mesh"*, align 4
  %att_id.addr = alloca i32, align 4
  store %"class.draco::Mesh"* %this, %"class.draco::Mesh"** %this.addr, align 4
  store i32 %att_id, i32* %att_id.addr, align 4
  %this1 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %this.addr, align 4
  %attribute_data_ = getelementptr inbounds %"class.draco::Mesh", %"class.draco::Mesh"* %this1, i32 0, i32 1
  %0 = load i32, i32* %att_id.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"struct.draco::Mesh::AttributeData"* @_ZNKSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEEixEm(%"class.std::__2::vector.124"* %attribute_data_, i32 %0) #6
  %element_type = getelementptr inbounds %"struct.draco::Mesh::AttributeData", %"struct.draco::Mesh::AttributeData"* %call, i32 0, i32 0
  %1 = load i32, i32* %element_type, align 4
  ret i32 %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco17GeometryAttribute9unique_idEv(%"class.draco::GeometryAttribute"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::GeometryAttribute"*, align 4
  store %"class.draco::GeometryAttribute"* %this, %"class.draco::GeometryAttribute"** %this.addr, align 4
  %this1 = load %"class.draco::GeometryAttribute"*, %"class.draco::GeometryAttribute"** %this.addr, align 4
  %unique_id_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 8
  %0 = load i32, i32* %unique_id_, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco14PointAttribute4sizeEv(%"class.draco::PointAttribute"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointAttribute"*, align 4
  store %"class.draco::PointAttribute"* %this, %"class.draco::PointAttribute"** %this.addr, align 4
  %this1 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %this.addr, align 4
  %num_unique_entries_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 3
  %0 = load i32, i32* %num_unique_entries_, align 8
  ret i32 %0
}

declare i8* @malloc(i32) #1

; Function Attrs: noinline nounwind optnone
define internal %"class.std::__2::unique_ptr.142"* @_ZNSt3__210unique_ptrIA_jN12_GLOBAL__N_111FreeDeleterEEC2IPjLb1EvvEET_(%"class.std::__2::unique_ptr.142"* returned %this, i32* %__p) unnamed_addr #0 {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.142"*, align 4
  %__p.addr = alloca i32*, align 4
  %ref.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::unique_ptr.142"* %this, %"class.std::__2::unique_ptr.142"** %this.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.142"*, %"class.std::__2::unique_ptr.142"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.142", %"class.std::__2::unique_ptr.142"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::__compressed_pair.143"* @_ZNSt3__217__compressed_pairIPjN12_GLOBAL__N_111FreeDeleterEEC2IRS1_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.143"* %__ptr_, i32** nonnull align 4 dereferenceable(4) %__p.addr, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp)
  ret %"class.std::__2::unique_ptr.142"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"struct.std::__2::array"* @_ZNK5draco4Mesh4faceENS_9IndexTypeIjNS_19FaceIndex_tag_type_EEE(%"class.draco::Mesh"* %this, i32 %face_id.coerce) #0 comdat {
entry:
  %face_id = alloca %"class.draco::IndexType.145", align 4
  %this.addr = alloca %"class.draco::Mesh"*, align 4
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType.145", %"class.draco::IndexType.145"* %face_id, i32 0, i32 0
  store i32 %face_id.coerce, i32* %coerce.dive, align 4
  store %"class.draco::Mesh"* %this, %"class.draco::Mesh"** %this.addr, align 4
  %this1 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %this.addr, align 4
  %faces_ = getelementptr inbounds %"class.draco::Mesh", %"class.draco::Mesh"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::array"* @_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEENSt3__25arrayINS1_IjNS_20PointIndex_tag_type_EEELm3EEEEixERKS3_(%"class.draco::IndexTypeVector.131"* %faces_, %"class.draco::IndexType.145"* nonnull align 4 dereferenceable(4) %face_id)
  ret %"struct.std::__2::array"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType.145"* @_ZN5draco9IndexTypeIjNS_19FaceIndex_tag_type_EEC2Ej(%"class.draco::IndexType.145"* returned %this, i32 %value) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.145"*, align 4
  %value.addr = alloca i32, align 4
  store %"class.draco::IndexType.145"* %this, %"class.draco::IndexType.145"** %this.addr, align 4
  store i32 %value, i32* %value.addr, align 4
  %this1 = load %"class.draco::IndexType.145"*, %"class.draco::IndexType.145"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.145", %"class.draco::IndexType.145"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value.addr, align 4
  store i32 %0, i32* %value_, align 4
  ret %"class.draco::IndexType.145"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.134"* @_ZNKSt3__25arrayIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELm3EEixEm(%"struct.std::__2::array"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::array"*, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::array"* %this, %"struct.std::__2::array"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %this.addr, align 4
  %__elems_ = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %this1, i32 0, i32 0
  %0 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %"class.draco::IndexType.134"], [3 x %"class.draco::IndexType.134"]* %__elems_, i32 0, i32 %0
  ret %"class.draco::IndexType.134"* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco9IndexTypeIjNS_20PointIndex_tag_type_EE5valueEv(%"class.draco::IndexType.134"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.134"*, align 4
  store %"class.draco::IndexType.134"* %this, %"class.draco::IndexType.134"** %this.addr, align 4
  %this1 = load %"class.draco::IndexType.134"*, %"class.draco::IndexType.134"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.134", %"class.draco::IndexType.134"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value_, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define internal nonnull align 4 dereferenceable(4) i32* @_ZNKSt3__210unique_ptrIA_jN12_GLOBAL__N_111FreeDeleterEEixEm(%"class.std::__2::unique_ptr.142"* %this, i32 %__i) #0 {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.142"*, align 4
  %__i.addr = alloca i32, align 4
  store %"class.std::__2::unique_ptr.142"* %this, %"class.std::__2::unique_ptr.142"** %this.addr, align 4
  store i32 %__i, i32* %__i.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.142"*, %"class.std::__2::unique_ptr.142"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.142", %"class.std::__2::unique_ptr.142"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__217__compressed_pairIPjN12_GLOBAL__N_111FreeDeleterEE5firstEv(%"class.std::__2::__compressed_pair.143"* %__ptr_) #6
  %0 = load i32*, i32** %call, align 4
  %1 = load i32, i32* %__i.addr, align 4
  %arrayidx = getelementptr inbounds i32, i32* %0, i32 %1
  ret i32* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK5draco14PointAttribute19is_mapping_identityEv(%"class.draco::PointAttribute"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointAttribute"*, align 4
  store %"class.draco::PointAttribute"* %this, %"class.draco::PointAttribute"** %this.addr, align 4
  %this1 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %this.addr, align 4
  %identity_mapping_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 4
  %0 = load i8, i8* %identity_mapping_, align 4
  %tobool = trunc i8 %0 to i1
  ret i1 %tobool
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco14PointAttribute12mapped_indexENS_9IndexTypeIjNS_20PointIndex_tag_type_EEE(%"class.draco::PointAttribute"* %this, i32 %point_index.coerce) #0 comdat {
entry:
  %retval = alloca %"class.draco::IndexType", align 4
  %point_index = alloca %"class.draco::IndexType.134", align 4
  %this.addr = alloca %"class.draco::PointAttribute"*, align 4
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType.134", %"class.draco::IndexType.134"* %point_index, i32 0, i32 0
  store i32 %point_index.coerce, i32* %coerce.dive, align 4
  store %"class.draco::PointAttribute"* %this, %"class.draco::PointAttribute"** %this.addr, align 4
  %this1 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %this.addr, align 4
  %identity_mapping_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 4
  %0 = load i8, i8* %identity_mapping_, align 4
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call = call i32 @_ZNK5draco9IndexTypeIjNS_20PointIndex_tag_type_EE5valueEv(%"class.draco::IndexType.134"* %point_index)
  %call2 = call %"class.draco::IndexType"* @_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEC2Ej(%"class.draco::IndexType"* %retval, i32 %call)
  br label %return

if.end:                                           ; preds = %entry
  %indices_map_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 2
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEEEixERKS3_(%"class.draco::IndexTypeVector"* %indices_map_, %"class.draco::IndexType.134"* nonnull align 4 dereferenceable(4) %point_index)
  %1 = bitcast %"class.draco::IndexType"* %retval to i8*
  %2 = bitcast %"class.draco::IndexType"* %call3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 4, i1 false)
  br label %return

return:                                           ; preds = %if.end, %if.then
  %coerce.dive4 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %retval, i32 0, i32 0
  %3 = load i32, i32* %coerce.dive4, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType.134"* @_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEC2Ej(%"class.draco::IndexType.134"* returned %this, i32 %value) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.134"*, align 4
  %value.addr = alloca i32, align 4
  store %"class.draco::IndexType.134"* %this, %"class.draco::IndexType.134"** %this.addr, align 4
  store i32 %value, i32* %value.addr, align 4
  %this1 = load %"class.draco::IndexType.134"*, %"class.draco::IndexType.134"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.134", %"class.draco::IndexType.134"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value.addr, align 4
  store i32 %0, i32* %value_, align 4
  ret %"class.draco::IndexType.134"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EE5valueEv(%"class.draco::IndexType"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.draco::IndexType"* %this, %"class.draco::IndexType"** %this.addr, align 4
  %this1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value_, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNK5draco17GeometryAttribute10GetAddressENS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEE(%"class.draco::GeometryAttribute"* %this, i32 %att_index.coerce) #0 comdat {
entry:
  %att_index = alloca %"class.draco::IndexType", align 4
  %this.addr = alloca %"class.draco::GeometryAttribute"*, align 4
  %byte_pos = alloca i64, align 8
  %agg.tmp = alloca %"class.draco::IndexType", align 4
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %att_index, i32 0, i32 0
  store i32 %att_index.coerce, i32* %coerce.dive, align 4
  store %"class.draco::GeometryAttribute"* %this, %"class.draco::GeometryAttribute"** %this.addr, align 4
  %this1 = load %"class.draco::GeometryAttribute"*, %"class.draco::GeometryAttribute"** %this.addr, align 4
  %0 = bitcast %"class.draco::IndexType"* %agg.tmp to i8*
  %1 = bitcast %"class.draco::IndexType"* %att_index to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %0, i8* align 4 %1, i32 4, i1 false)
  %coerce.dive2 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %agg.tmp, i32 0, i32 0
  %2 = load i32, i32* %coerce.dive2, align 4
  %call = call i64 @_ZNK5draco17GeometryAttribute10GetBytePosENS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEE(%"class.draco::GeometryAttribute"* %this1, i32 %2)
  store i64 %call, i64* %byte_pos, align 8
  %buffer_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 0
  %3 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %buffer_, align 8
  %call3 = call i8* @_ZN5draco10DataBuffer4dataEv(%"class.draco::DataBuffer"* %3)
  %4 = load i64, i64* %byte_pos, align 8
  %idx.ext = trunc i64 %4 to i32
  %add.ptr = getelementptr inbounds i8, i8* %call3, i32 %idx.ext
  ret i8* %add.ptr
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType"* @_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEC2Ej(%"class.draco::IndexType"* returned %this, i32 %value) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType"*, align 4
  %value.addr = alloca i32, align 4
  store %"class.draco::IndexType"* %this, %"class.draco::IndexType"** %this.addr, align 4
  store i32 %value, i32* %value.addr, align 4
  %this1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value.addr, align 4
  store i32 %0, i32* %value_, align 4
  ret %"class.draco::IndexType"* %this1
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #2

; Function Attrs: noinline nounwind optnone
define internal void @"_ZZ25neuroglancer_draco_decodeENK3$_1clIZ25neuroglancer_draco_decodeE3$_2EEDaT_"(%class.anon.146* %this, [9 x i32]* %func.coerce) #0 {
entry:
  %func = alloca %class.anon.147, align 4
  %this.addr = alloca %class.anon.146*, align 4
  %i = alloca i32, align 4
  %mask = alloca i32, align 4
  %j = alloca i32, align 4
  %coerce.dive = getelementptr inbounds %class.anon.147, %class.anon.147* %func, i32 0, i32 0
  store [9 x i32]* %func.coerce, [9 x i32]** %coerce.dive, align 4
  store %class.anon.146* %this, %class.anon.146** %this.addr, align 4
  %this1 = load %class.anon.146*, %class.anon.146** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc7, %entry
  %0 = load i32, i32* %i, align 4
  %1 = getelementptr inbounds %class.anon.146, %class.anon.146* %this1, i32 0, i32 0
  %2 = load i32*, i32** %1, align 4
  %3 = load i32, i32* %2, align 4
  %mul = mul i32 %3, 3
  %cmp = icmp ult i32 %0, %mul
  br i1 %cmp, label %for.body, label %for.end9

for.body:                                         ; preds = %for.cond
  store i32 255, i32* %mask, align 4
  store i32 0, i32* %j, align 4
  br label %for.cond2

for.cond2:                                        ; preds = %for.inc, %for.body
  %4 = load i32, i32* %j, align 4
  %cmp3 = icmp slt i32 %4, 3
  br i1 %cmp3, label %for.body4, label %for.end

for.body4:                                        ; preds = %for.cond2
  %5 = getelementptr inbounds %class.anon.146, %class.anon.146* %this1, i32 0, i32 1
  %6 = load %class.anon*, %class.anon** %5, align 4
  %7 = getelementptr inbounds %class.anon.146, %class.anon.146* %this1, i32 0, i32 2
  %8 = load i32**, i32*** %7, align 4
  %9 = load i32*, i32** %8, align 4
  %10 = getelementptr inbounds %class.anon.146, %class.anon.146* %this1, i32 0, i32 3
  %11 = load %"class.std::__2::unique_ptr.142"*, %"class.std::__2::unique_ptr.142"** %10, align 4
  %12 = load i32, i32* %i, align 4
  %13 = load i32, i32* %j, align 4
  %add = add i32 %12, %13
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNKSt3__210unique_ptrIA_jN12_GLOBAL__N_111FreeDeleterEEixEm(%"class.std::__2::unique_ptr.142"* %11, i32 %add)
  %14 = load i32, i32* %call, align 4
  %mul5 = mul i32 %14, 3
  %add.ptr = getelementptr inbounds i32, i32* %9, i32 %mul5
  %call6 = call i32 @"_ZZ25neuroglancer_draco_decodeENK3$_0clIKjEEDaPT_"(%class.anon* %6, i32* %add.ptr)
  %15 = load i32, i32* %mask, align 4
  %and = and i32 %15, %call6
  store i32 %and, i32* %mask, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body4
  %16 = load i32, i32* %j, align 4
  %inc = add nsw i32 %16, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond2

for.end:                                          ; preds = %for.cond2
  %17 = load i32, i32* %i, align 4
  %18 = load i32, i32* %mask, align 4
  %arrayidx = getelementptr inbounds [256 x i8], [256 x i8]* @_ZN12_GLOBAL__N_122first_bit_lookup_tableE, i32 0, i32 %18
  %19 = load i8, i8* %arrayidx, align 1
  %conv = zext i8 %19 to i32
  call void @"_ZZ25neuroglancer_draco_decodeENK3$_2clEmj"(%class.anon.147* %func, i32 %17, i32 %conv)
  br label %for.inc7

for.inc7:                                         ; preds = %for.end
  %20 = load i32, i32* %i, align 4
  %add8 = add i32 %20, 3
  store i32 %add8, i32* %i, align 4
  br label %for.cond

for.end9:                                         ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @"_ZZ25neuroglancer_draco_decodeENK3$_1clIZ25neuroglancer_draco_decodeE3$_3EEDaT_"(%class.anon.146* %this, %class.anon.148* byval(%class.anon.148) align 4 %func) #0 {
entry:
  %this.addr = alloca %class.anon.146*, align 4
  %i = alloca i32, align 4
  %mask = alloca i32, align 4
  %j = alloca i32, align 4
  store %class.anon.146* %this, %class.anon.146** %this.addr, align 4
  %this1 = load %class.anon.146*, %class.anon.146** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc7, %entry
  %0 = load i32, i32* %i, align 4
  %1 = getelementptr inbounds %class.anon.146, %class.anon.146* %this1, i32 0, i32 0
  %2 = load i32*, i32** %1, align 4
  %3 = load i32, i32* %2, align 4
  %mul = mul i32 %3, 3
  %cmp = icmp ult i32 %0, %mul
  br i1 %cmp, label %for.body, label %for.end9

for.body:                                         ; preds = %for.cond
  store i32 255, i32* %mask, align 4
  store i32 0, i32* %j, align 4
  br label %for.cond2

for.cond2:                                        ; preds = %for.inc, %for.body
  %4 = load i32, i32* %j, align 4
  %cmp3 = icmp slt i32 %4, 3
  br i1 %cmp3, label %for.body4, label %for.end

for.body4:                                        ; preds = %for.cond2
  %5 = getelementptr inbounds %class.anon.146, %class.anon.146* %this1, i32 0, i32 1
  %6 = load %class.anon*, %class.anon** %5, align 4
  %7 = getelementptr inbounds %class.anon.146, %class.anon.146* %this1, i32 0, i32 2
  %8 = load i32**, i32*** %7, align 4
  %9 = load i32*, i32** %8, align 4
  %10 = getelementptr inbounds %class.anon.146, %class.anon.146* %this1, i32 0, i32 3
  %11 = load %"class.std::__2::unique_ptr.142"*, %"class.std::__2::unique_ptr.142"** %10, align 4
  %12 = load i32, i32* %i, align 4
  %13 = load i32, i32* %j, align 4
  %add = add i32 %12, %13
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNKSt3__210unique_ptrIA_jN12_GLOBAL__N_111FreeDeleterEEixEm(%"class.std::__2::unique_ptr.142"* %11, i32 %add)
  %14 = load i32, i32* %call, align 4
  %mul5 = mul i32 %14, 3
  %add.ptr = getelementptr inbounds i32, i32* %9, i32 %mul5
  %call6 = call i32 @"_ZZ25neuroglancer_draco_decodeENK3$_0clIKjEEDaPT_"(%class.anon* %6, i32* %add.ptr)
  %15 = load i32, i32* %mask, align 4
  %and = and i32 %15, %call6
  store i32 %and, i32* %mask, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body4
  %16 = load i32, i32* %j, align 4
  %inc = add nsw i32 %16, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond2

for.end:                                          ; preds = %for.cond2
  %17 = load i32, i32* %i, align 4
  %18 = load i32, i32* %mask, align 4
  %arrayidx = getelementptr inbounds [256 x i8], [256 x i8]* @_ZN12_GLOBAL__N_122first_bit_lookup_tableE, i32 0, i32 %18
  %19 = load i8, i8* %arrayidx, align 1
  %conv = zext i8 %19 to i32
  call void @"_ZZ25neuroglancer_draco_decodeENK3$_3clEmj"(%class.anon.148* %func, i32 %17, i32 %conv)
  br label %for.inc7

for.inc7:                                         ; preds = %for.end
  %20 = load i32, i32* %i, align 4
  %add8 = add i32 %20, 3
  store i32 %add8, i32* %i, align 4
  br label %for.cond

for.end9:                                         ; preds = %for.cond
  ret void
}

declare void @neuroglancer_draco_receive_decoded_mesh(i32, i32, i8*, i8*, i8*) #1

; Function Attrs: noinline nounwind optnone
define internal i32* @_ZNKSt3__210unique_ptrIA_jN12_GLOBAL__N_111FreeDeleterEE3getEv(%"class.std::__2::unique_ptr.142"* %this) #0 {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.142"*, align 4
  store %"class.std::__2::unique_ptr.142"* %this, %"class.std::__2::unique_ptr.142"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.142"*, %"class.std::__2::unique_ptr.142"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.142", %"class.std::__2::unique_ptr.142"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__217__compressed_pairIPjN12_GLOBAL__N_111FreeDeleterEE5firstEv(%"class.std::__2::__compressed_pair.143"* %__ptr_) #6
  %0 = load i32*, i32** %call, align 4
  ret i32* %0
}

; Function Attrs: noinline nounwind optnone
define internal %"class.std::__2::unique_ptr.142"* @_ZNSt3__210unique_ptrIA_jN12_GLOBAL__N_111FreeDeleterEED2Ev(%"class.std::__2::unique_ptr.142"* returned %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.142"*, align 4
  store %"class.std::__2::unique_ptr.142"* %this, %"class.std::__2::unique_ptr.142"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.142"*, %"class.std::__2::unique_ptr.142"** %this.addr, align 4
  call void @_ZNSt3__210unique_ptrIA_jN12_GLOBAL__N_111FreeDeleterEE5resetEDn(%"class.std::__2::unique_ptr.142"* %this1, i8* null) #6
  ret %"class.std::__2::unique_ptr.142"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::StatusOr"* @_ZN5draco8StatusOrINSt3__210unique_ptrINS_4MeshENS1_14default_deleteIS3_EEEEED2Ev(%"class.draco::StatusOr"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::StatusOr"*, align 4
  store %"class.draco::StatusOr"* %this, %"class.draco::StatusOr"** %this.addr, align 4
  %this1 = load %"class.draco::StatusOr"*, %"class.draco::StatusOr"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::StatusOr", %"class.draco::StatusOr"* %this1, i32 0, i32 1
  %call = call %"class.std::__2::unique_ptr.23"* @_ZNSt3__210unique_ptrIN5draco4MeshENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.23"* %value_) #6
  %status_ = getelementptr inbounds %"class.draco::StatusOr", %"class.draco::StatusOr"* %this1, i32 0, i32 0
  %call2 = call %"class.draco::Status"* @_ZN5draco6StatusD2Ev(%"class.draco::Status"* %status_) #6
  ret %"class.draco::StatusOr"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::Decoder"* @_ZN5draco7DecoderD2Ev(%"class.draco::Decoder"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::Decoder"*, align 4
  store %"class.draco::Decoder"* %this, %"class.draco::Decoder"** %this.addr, align 4
  %this1 = load %"class.draco::Decoder"*, %"class.draco::Decoder"** %this.addr, align 4
  %options_ = getelementptr inbounds %"class.draco::Decoder", %"class.draco::Decoder"* %this1, i32 0, i32 0
  %call = call %"class.draco::DracoOptions"* @_ZN5draco12DracoOptionsINS_17GeometryAttribute4TypeEED2Ev(%"class.draco::DracoOptions"* %options_) #6
  ret %"class.draco::Decoder"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::DecoderBuffer"* @_ZN5draco13DecoderBufferD2Ev(%"class.draco::DecoderBuffer"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  store %"class.draco::DecoderBuffer"* %this, %"class.draco::DecoderBuffer"** %this.addr, align 4
  %this1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %this.addr, align 4
  %bit_decoder_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 3
  %call = call %"class.draco::DecoderBuffer::BitDecoder"* @_ZN5draco13DecoderBuffer10BitDecoderD1Ev(%"class.draco::DecoderBuffer::BitDecoder"* %bit_decoder_) #6
  ret %"class.draco::DecoderBuffer"* %this1
}

; Function Attrs: noinline nounwind optnone
define internal %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrIA_cN12_GLOBAL__N_111FreeDeleterEED2Ev(%"class.std::__2::unique_ptr"* returned %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  call void @_ZNSt3__210unique_ptrIA_cN12_GLOBAL__N_111FreeDeleterEE5resetEDn(%"class.std::__2::unique_ptr"* %this1, i8* null) #6
  ret %"class.std::__2::unique_ptr"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::DracoOptions"* @_ZN5draco12DracoOptionsINS_17GeometryAttribute4TypeEEC2Ev(%"class.draco::DracoOptions"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::DracoOptions"*, align 4
  store %"class.draco::DracoOptions"* %this, %"class.draco::DracoOptions"** %this.addr, align 4
  %this1 = load %"class.draco::DracoOptions"*, %"class.draco::DracoOptions"** %this.addr, align 4
  %global_options_ = getelementptr inbounds %"class.draco::DracoOptions", %"class.draco::DracoOptions"* %this1, i32 0, i32 0
  %call = call %"class.draco::Options"* @_ZN5draco7OptionsC1Ev(%"class.draco::Options"* %global_options_)
  %attribute_options_ = getelementptr inbounds %"class.draco::DracoOptions", %"class.draco::DracoOptions"* %this1, i32 0, i32 1
  %call2 = call %"class.std::__2::map.7"* @_ZNSt3__23mapIN5draco17GeometryAttribute4TypeENS1_7OptionsENS_4lessIS3_EENS_9allocatorINS_4pairIKS3_S4_EEEEEC2Ev(%"class.std::__2::map.7"* %attribute_options_) #6
  ret %"class.draco::DracoOptions"* %this1
}

declare %"class.draco::Options"* @_ZN5draco7OptionsC1Ev(%"class.draco::Options"* returned) unnamed_addr #1

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::map.7"* @_ZNSt3__23mapIN5draco17GeometryAttribute4TypeENS1_7OptionsENS_4lessIS3_EENS_9allocatorINS_4pairIKS3_S4_EEEEEC2Ev(%"class.std::__2::map.7"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::map.7"*, align 4
  %ref.tmp = alloca %"class.std::__2::__map_value_compare.15", align 1
  %agg.tmp = alloca %"struct.std::__2::less.16", align 1
  store %"class.std::__2::map.7"* %this, %"class.std::__2::map.7"** %this.addr, align 4
  %this1 = load %"class.std::__2::map.7"*, %"class.std::__2::map.7"** %this.addr, align 4
  %__tree_ = getelementptr inbounds %"class.std::__2::map.7", %"class.std::__2::map.7"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::__map_value_compare.15"* @_ZNSt3__219__map_value_compareIN5draco17GeometryAttribute4TypeENS_12__value_typeIS3_NS1_7OptionsEEENS_4lessIS3_EELb1EEC2ES8_(%"class.std::__2::__map_value_compare.15"* %ref.tmp) #6
  %call2 = call %"class.std::__2::__tree.8"* @_ZNSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEEC2ERKSA_(%"class.std::__2::__tree.8"* %__tree_, %"class.std::__2::__map_value_compare.15"* nonnull align 1 dereferenceable(1) %ref.tmp) #6
  ret %"class.std::__2::map.7"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__map_value_compare.15"* @_ZNSt3__219__map_value_compareIN5draco17GeometryAttribute4TypeENS_12__value_typeIS3_NS1_7OptionsEEENS_4lessIS3_EELb1EEC2ES8_(%"class.std::__2::__map_value_compare.15"* returned %this) unnamed_addr #0 comdat {
entry:
  %c = alloca %"struct.std::__2::less.16", align 1
  %this.addr = alloca %"class.std::__2::__map_value_compare.15"*, align 4
  store %"class.std::__2::__map_value_compare.15"* %this, %"class.std::__2::__map_value_compare.15"** %this.addr, align 4
  %this1 = load %"class.std::__2::__map_value_compare.15"*, %"class.std::__2::__map_value_compare.15"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__map_value_compare.15"* %this1 to %"struct.std::__2::less.16"*
  ret %"class.std::__2::__map_value_compare.15"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree.8"* @_ZNSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEEC2ERKSA_(%"class.std::__2::__tree.8"* returned %this, %"class.std::__2::__map_value_compare.15"* nonnull align 1 dereferenceable(1) %__comp) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree.8"*, align 4
  %__comp.addr = alloca %"class.std::__2::__map_value_compare.15"*, align 4
  %ref.tmp = alloca i32, align 4
  store %"class.std::__2::__tree.8"* %this, %"class.std::__2::__tree.8"** %this.addr, align 4
  store %"class.std::__2::__map_value_compare.15"* %__comp, %"class.std::__2::__map_value_compare.15"** %__comp.addr, align 4
  %this1 = load %"class.std::__2::__tree.8"*, %"class.std::__2::__tree.8"** %this.addr, align 4
  %__pair1_ = getelementptr inbounds %"class.std::__2::__tree.8", %"class.std::__2::__tree.8"* %this1, i32 0, i32 1
  %call = call %"class.std::__2::__compressed_pair.9"* @_ZNSt3__217__compressed_pairINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEENS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENSA_7OptionsEEES3_EEEEEC2ILb1EvEEv(%"class.std::__2::__compressed_pair.9"* %__pair1_)
  %__pair3_ = getelementptr inbounds %"class.std::__2::__tree.8", %"class.std::__2::__tree.8"* %this1, i32 0, i32 2
  store i32 0, i32* %ref.tmp, align 4
  %0 = load %"class.std::__2::__map_value_compare.15"*, %"class.std::__2::__map_value_compare.15"** %__comp.addr, align 4
  %call2 = call %"class.std::__2::__compressed_pair.13"* @_ZNSt3__217__compressed_pairImNS_19__map_value_compareIN5draco17GeometryAttribute4TypeENS_12__value_typeIS4_NS2_7OptionsEEENS_4lessIS4_EELb1EEEEC2IiRKSA_EEOT_OT0_(%"class.std::__2::__compressed_pair.13"* %__pair3_, i32* nonnull align 4 dereferenceable(4) %ref.tmp, %"class.std::__2::__map_value_compare.15"* nonnull align 1 dereferenceable(1) %0)
  %call3 = call %"class.std::__2::__tree_end_node"* @_ZNSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE10__end_nodeEv(%"class.std::__2::__tree.8"* %this1) #6
  %call4 = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_end_node"** @_ZNSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE12__begin_nodeEv(%"class.std::__2::__tree.8"* %this1) #6
  store %"class.std::__2::__tree_end_node"* %call3, %"class.std::__2::__tree_end_node"** %call4, align 4
  ret %"class.std::__2::__tree.8"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.9"* @_ZNSt3__217__compressed_pairINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEENS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENSA_7OptionsEEES3_EEEEEC2ILb1EvEEv(%"class.std::__2::__compressed_pair.9"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.9"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__value_init_tag", align 1
  %agg.tmp2 = alloca %"struct.std::__2::__value_init_tag", align 1
  store %"class.std::__2::__compressed_pair.9"* %this, %"class.std::__2::__compressed_pair.9"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.9"*, %"class.std::__2::__compressed_pair.9"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.9"* %this1 to %"struct.std::__2::__compressed_pair_elem.2"*
  %call = call %"struct.std::__2::__compressed_pair_elem.2"* @_ZNSt3__222__compressed_pair_elemINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEELi0ELb0EEC2ENS_16__value_init_tagE(%"struct.std::__2::__compressed_pair_elem.2"* %0)
  %1 = bitcast %"class.std::__2::__compressed_pair.9"* %this1 to %"struct.std::__2::__compressed_pair_elem.10"*
  %call3 = call %"struct.std::__2::__compressed_pair_elem.10"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS4_7OptionsEEEPvEEEELi1ELb1EEC2ENS_16__value_init_tagE(%"struct.std::__2::__compressed_pair_elem.10"* %1)
  ret %"class.std::__2::__compressed_pair.9"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.13"* @_ZNSt3__217__compressed_pairImNS_19__map_value_compareIN5draco17GeometryAttribute4TypeENS_12__value_typeIS4_NS2_7OptionsEEENS_4lessIS4_EELb1EEEEC2IiRKSA_EEOT_OT0_(%"class.std::__2::__compressed_pair.13"* returned %this, i32* nonnull align 4 dereferenceable(4) %__t1, %"class.std::__2::__map_value_compare.15"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.13"*, align 4
  %__t1.addr = alloca i32*, align 4
  %__t2.addr = alloca %"class.std::__2::__map_value_compare.15"*, align 4
  store %"class.std::__2::__compressed_pair.13"* %this, %"class.std::__2::__compressed_pair.13"** %this.addr, align 4
  store i32* %__t1, i32** %__t1.addr, align 4
  store %"class.std::__2::__map_value_compare.15"* %__t2, %"class.std::__2::__map_value_compare.15"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.13"*, %"class.std::__2::__compressed_pair.13"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.13"* %this1 to %"struct.std::__2::__compressed_pair_elem.5"*
  %1 = load i32*, i32** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__27forwardIiEEOT_RNS_16remove_referenceIS1_E4typeE(i32* nonnull align 4 dereferenceable(4) %1) #6
  %call2 = call %"struct.std::__2::__compressed_pair_elem.5"* @_ZNSt3__222__compressed_pair_elemImLi0ELb0EEC2IivEEOT_(%"struct.std::__2::__compressed_pair_elem.5"* %0, i32* nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.13"* %this1 to %"struct.std::__2::__compressed_pair_elem.14"*
  %3 = load %"class.std::__2::__map_value_compare.15"*, %"class.std::__2::__map_value_compare.15"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"class.std::__2::__map_value_compare.15"* @_ZNSt3__27forwardIRKNS_19__map_value_compareIN5draco17GeometryAttribute4TypeENS_12__value_typeIS4_NS2_7OptionsEEENS_4lessIS4_EELb1EEEEEOT_RNS_16remove_referenceISD_E4typeE(%"class.std::__2::__map_value_compare.15"* nonnull align 1 dereferenceable(1) %3) #6
  %call4 = call %"struct.std::__2::__compressed_pair_elem.14"* @_ZNSt3__222__compressed_pair_elemINS_19__map_value_compareIN5draco17GeometryAttribute4TypeENS_12__value_typeIS4_NS2_7OptionsEEENS_4lessIS4_EELb1EEELi1ELb1EEC2IRKSA_vEEOT_(%"struct.std::__2::__compressed_pair_elem.14"* %2, %"class.std::__2::__map_value_compare.15"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::__compressed_pair.13"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_end_node"* @_ZNSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE10__end_nodeEv(%"class.std::__2::__tree.8"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree.8"*, align 4
  store %"class.std::__2::__tree.8"* %this, %"class.std::__2::__tree.8"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree.8"*, %"class.std::__2::__tree.8"** %this.addr, align 4
  %__pair1_ = getelementptr inbounds %"class.std::__2::__tree.8", %"class.std::__2::__tree.8"* %this1, i32 0, i32 1
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_end_node"* @_ZNSt3__217__compressed_pairINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEENS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENSA_7OptionsEEES3_EEEEE5firstEv(%"class.std::__2::__compressed_pair.9"* %__pair1_) #6
  %call2 = call %"class.std::__2::__tree_end_node"* @_ZNSt3__214pointer_traitsIPNS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEEE10pointer_toERS6_(%"class.std::__2::__tree_end_node"* nonnull align 4 dereferenceable(4) %call) #6
  ret %"class.std::__2::__tree_end_node"* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_end_node"** @_ZNSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE12__begin_nodeEv(%"class.std::__2::__tree.8"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree.8"*, align 4
  store %"class.std::__2::__tree.8"* %this, %"class.std::__2::__tree.8"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree.8"*, %"class.std::__2::__tree.8"** %this.addr, align 4
  %__begin_node_ = getelementptr inbounds %"class.std::__2::__tree.8", %"class.std::__2::__tree.8"* %this1, i32 0, i32 0
  ret %"class.std::__2::__tree_end_node"** %__begin_node_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.2"* @_ZNSt3__222__compressed_pair_elemINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEELi0ELb0EEC2ENS_16__value_init_tagE(%"struct.std::__2::__compressed_pair_elem.2"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__value_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.2"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.2"* %this, %"struct.std::__2::__compressed_pair_elem.2"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.2"*, %"struct.std::__2::__compressed_pair_elem.2"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.2", %"struct.std::__2::__compressed_pair_elem.2"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::__tree_end_node"* @_ZNSt3__215__tree_end_nodeIPNS_16__tree_node_baseIPvEEEC2Ev(%"class.std::__2::__tree_end_node"* %__value_) #6
  ret %"struct.std::__2::__compressed_pair_elem.2"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.10"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS4_7OptionsEEEPvEEEELi1ELb1EEC2ENS_16__value_init_tagE(%"struct.std::__2::__compressed_pair_elem.10"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__value_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.10"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.10"* %this, %"struct.std::__2::__compressed_pair_elem.10"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.10"*, %"struct.std::__2::__compressed_pair_elem.10"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.10"* %this1 to %"class.std::__2::allocator.11"*
  %call = call %"class.std::__2::allocator.11"* @_ZNSt3__29allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPvEEEC2Ev(%"class.std::__2::allocator.11"* %1) #6
  ret %"struct.std::__2::__compressed_pair_elem.10"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_end_node"* @_ZNSt3__215__tree_end_nodeIPNS_16__tree_node_baseIPvEEEC2Ev(%"class.std::__2::__tree_end_node"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree_end_node"*, align 4
  store %"class.std::__2::__tree_end_node"* %this, %"class.std::__2::__tree_end_node"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %this.addr, align 4
  %__left_ = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %this1, i32 0, i32 0
  store %"class.std::__2::__tree_node_base"* null, %"class.std::__2::__tree_node_base"** %__left_, align 4
  ret %"class.std::__2::__tree_end_node"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::allocator.11"* @_ZNSt3__29allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPvEEEC2Ev(%"class.std::__2::allocator.11"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.11"*, align 4
  store %"class.std::__2::allocator.11"* %this, %"class.std::__2::allocator.11"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.11"*, %"class.std::__2::allocator.11"** %this.addr, align 4
  ret %"class.std::__2::allocator.11"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__27forwardIiEEOT_RNS_16remove_referenceIS1_E4typeE(i32* nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca i32*, align 4
  store i32* %__t, i32** %__t.addr, align 4
  %0 = load i32*, i32** %__t.addr, align 4
  ret i32* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.5"* @_ZNSt3__222__compressed_pair_elemImLi0ELb0EEC2IivEEOT_(%"struct.std::__2::__compressed_pair_elem.5"* returned %this, i32* nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.5"*, align 4
  %__u.addr = alloca i32*, align 4
  store %"struct.std::__2::__compressed_pair_elem.5"* %this, %"struct.std::__2::__compressed_pair_elem.5"** %this.addr, align 4
  store i32* %__u, i32** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.5"*, %"struct.std::__2::__compressed_pair_elem.5"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.5", %"struct.std::__2::__compressed_pair_elem.5"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__27forwardIiEEOT_RNS_16remove_referenceIS1_E4typeE(i32* nonnull align 4 dereferenceable(4) %0) #6
  %1 = load i32, i32* %call, align 4
  store i32 %1, i32* %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.5"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::__map_value_compare.15"* @_ZNSt3__27forwardIRKNS_19__map_value_compareIN5draco17GeometryAttribute4TypeENS_12__value_typeIS4_NS2_7OptionsEEENS_4lessIS4_EELb1EEEEEOT_RNS_16remove_referenceISD_E4typeE(%"class.std::__2::__map_value_compare.15"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::__map_value_compare.15"*, align 4
  store %"class.std::__2::__map_value_compare.15"* %__t, %"class.std::__2::__map_value_compare.15"** %__t.addr, align 4
  %0 = load %"class.std::__2::__map_value_compare.15"*, %"class.std::__2::__map_value_compare.15"** %__t.addr, align 4
  ret %"class.std::__2::__map_value_compare.15"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.14"* @_ZNSt3__222__compressed_pair_elemINS_19__map_value_compareIN5draco17GeometryAttribute4TypeENS_12__value_typeIS4_NS2_7OptionsEEENS_4lessIS4_EELb1EEELi1ELb1EEC2IRKSA_vEEOT_(%"struct.std::__2::__compressed_pair_elem.14"* returned %this, %"class.std::__2::__map_value_compare.15"* nonnull align 1 dereferenceable(1) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.14"*, align 4
  %__u.addr = alloca %"class.std::__2::__map_value_compare.15"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.14"* %this, %"struct.std::__2::__compressed_pair_elem.14"** %this.addr, align 4
  store %"class.std::__2::__map_value_compare.15"* %__u, %"class.std::__2::__map_value_compare.15"** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.14"*, %"struct.std::__2::__compressed_pair_elem.14"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.14"* %this1 to %"class.std::__2::__map_value_compare.15"*
  %1 = load %"class.std::__2::__map_value_compare.15"*, %"class.std::__2::__map_value_compare.15"** %__u.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::__map_value_compare.15"* @_ZNSt3__27forwardIRKNS_19__map_value_compareIN5draco17GeometryAttribute4TypeENS_12__value_typeIS4_NS2_7OptionsEEENS_4lessIS4_EELb1EEEEEOT_RNS_16remove_referenceISD_E4typeE(%"class.std::__2::__map_value_compare.15"* nonnull align 1 dereferenceable(1) %1) #6
  ret %"struct.std::__2::__compressed_pair_elem.14"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_end_node"* @_ZNSt3__214pointer_traitsIPNS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEEE10pointer_toERS6_(%"class.std::__2::__tree_end_node"* nonnull align 4 dereferenceable(4) %__r) #0 comdat {
entry:
  %__r.addr = alloca %"class.std::__2::__tree_end_node"*, align 4
  store %"class.std::__2::__tree_end_node"* %__r, %"class.std::__2::__tree_end_node"** %__r.addr, align 4
  %0 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %__r.addr, align 4
  %call = call %"class.std::__2::__tree_end_node"* @_ZNSt3__29addressofINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEEEEPT_RS7_(%"class.std::__2::__tree_end_node"* nonnull align 4 dereferenceable(4) %0) #6
  ret %"class.std::__2::__tree_end_node"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_end_node"* @_ZNSt3__217__compressed_pairINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEENS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENSA_7OptionsEEES3_EEEEE5firstEv(%"class.std::__2::__compressed_pair.9"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.9"*, align 4
  store %"class.std::__2::__compressed_pair.9"* %this, %"class.std::__2::__compressed_pair.9"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.9"*, %"class.std::__2::__compressed_pair.9"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.9"* %this1 to %"struct.std::__2::__compressed_pair_elem.2"*
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_end_node"* @_ZNSt3__222__compressed_pair_elemINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.2"* %0) #6
  ret %"class.std::__2::__tree_end_node"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_end_node"* @_ZNSt3__29addressofINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEEEEPT_RS7_(%"class.std::__2::__tree_end_node"* nonnull align 4 dereferenceable(4) %__x) #0 comdat {
entry:
  %__x.addr = alloca %"class.std::__2::__tree_end_node"*, align 4
  store %"class.std::__2::__tree_end_node"* %__x, %"class.std::__2::__tree_end_node"** %__x.addr, align 4
  %0 = load %"class.std::__2::__tree_end_node"*, %"class.std::__2::__tree_end_node"** %__x.addr, align 4
  ret %"class.std::__2::__tree_end_node"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_end_node"* @_ZNSt3__222__compressed_pair_elemINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.2"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.2"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.2"* %this, %"struct.std::__2::__compressed_pair_elem.2"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.2"*, %"struct.std::__2::__compressed_pair_elem.2"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.2", %"struct.std::__2::__compressed_pair_elem.2"* %this1, i32 0, i32 0
  ret %"class.std::__2::__tree_end_node"* %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEENSt3__25arrayINS1_IjNS_20PointIndex_tag_type_EEELm3EEEE4sizeEv(%"class.draco::IndexTypeVector.131"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector.131"*, align 4
  store %"class.draco::IndexTypeVector.131"* %this, %"class.draco::IndexTypeVector.131"** %this.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector.131"*, %"class.draco::IndexTypeVector.131"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector.131", %"class.draco::IndexTypeVector.131"* %this1, i32 0, i32 0
  %call = call i32 @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.132"* %vector_) #6
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.132"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.132"*, align 4
  store %"class.std::__2::vector.132"* %this, %"class.std::__2::vector.132"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.132"*, %"class.std::__2::vector.132"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.132"* %this1 to %"class.std::__2::__vector_base.133"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.133", %"class.std::__2::__vector_base.133"* %0, i32 0, i32 1
  %1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector.132"* %this1 to %"class.std::__2::__vector_base.133"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.133", %"class.std::__2::__vector_base.133"* %2, i32 0, i32 0
  %3 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"struct.std::__2::array"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"struct.std::__2::array"* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 12
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.draco::Mesh::AttributeData"* @_ZNKSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEEixEm(%"class.std::__2::vector.124"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.124"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector.124"* %this, %"class.std::__2::vector.124"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.124"*, %"class.std::__2::vector.124"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.124"* %this1 to %"class.std::__2::__vector_base.125"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.125", %"class.std::__2::__vector_base.125"* %0, i32 0, i32 0
  %1 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__begin_, align 4
  %2 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds %"struct.draco::Mesh::AttributeData", %"struct.draco::Mesh::AttributeData"* %1, i32 %2
  ret %"struct.draco::Mesh::AttributeData"* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"struct.std::__2::array"* @_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEENSt3__25arrayINS1_IjNS_20PointIndex_tag_type_EEELm3EEEEixERKS3_(%"class.draco::IndexTypeVector.131"* %this, %"class.draco::IndexType.145"* nonnull align 4 dereferenceable(4) %index) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector.131"*, align 4
  %index.addr = alloca %"class.draco::IndexType.145"*, align 4
  store %"class.draco::IndexTypeVector.131"* %this, %"class.draco::IndexTypeVector.131"** %this.addr, align 4
  store %"class.draco::IndexType.145"* %index, %"class.draco::IndexType.145"** %index.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector.131"*, %"class.draco::IndexTypeVector.131"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector.131", %"class.draco::IndexTypeVector.131"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexType.145"*, %"class.draco::IndexType.145"** %index.addr, align 4
  %call = call i32 @_ZNK5draco9IndexTypeIjNS_19FaceIndex_tag_type_EE5valueEv(%"class.draco::IndexType.145"* %0)
  %call2 = call nonnull align 4 dereferenceable(12) %"struct.std::__2::array"* @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEEixEm(%"class.std::__2::vector.132"* %vector_, i32 %call) #6
  ret %"struct.std::__2::array"* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"struct.std::__2::array"* @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEEixEm(%"class.std::__2::vector.132"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.132"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector.132"* %this, %"class.std::__2::vector.132"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.132"*, %"class.std::__2::vector.132"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.132"* %this1 to %"class.std::__2::__vector_base.133"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.133", %"class.std::__2::__vector_base.133"* %0, i32 0, i32 0
  %1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__begin_, align 4
  %2 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %1, i32 %2
  ret %"struct.std::__2::array"* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco9IndexTypeIjNS_19FaceIndex_tag_type_EE5valueEv(%"class.draco::IndexType.145"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.145"*, align 4
  store %"class.draco::IndexType.145"* %this, %"class.draco::IndexType.145"** %this.addr, align 4
  %this1 = load %"class.draco::IndexType.145"*, %"class.draco::IndexType.145"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.145", %"class.draco::IndexType.145"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value_, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEEEixERKS3_(%"class.draco::IndexTypeVector"* %this, %"class.draco::IndexType.134"* nonnull align 4 dereferenceable(4) %index) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector"*, align 4
  %index.addr = alloca %"class.draco::IndexType.134"*, align 4
  store %"class.draco::IndexTypeVector"* %this, %"class.draco::IndexTypeVector"** %this.addr, align 4
  store %"class.draco::IndexType.134"* %index, %"class.draco::IndexType.134"** %index.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector"*, %"class.draco::IndexTypeVector"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector", %"class.draco::IndexTypeVector"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexType.134"*, %"class.draco::IndexType.134"** %index.addr, align 4
  %call = call i32 @_ZNK5draco9IndexTypeIjNS_20PointIndex_tag_type_EE5valueEv(%"class.draco::IndexType.134"* %0)
  %call2 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEEixEm(%"class.std::__2::vector.98"* %vector_, i32 %call) #6
  ret %"class.draco::IndexType"* %call2
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEEixEm(%"class.std::__2::vector.98"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.98"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector.98"* %this, %"class.std::__2::vector.98"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.98"*, %"class.std::__2::vector.98"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.98"* %this1 to %"class.std::__2::__vector_base.99"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.99", %"class.std::__2::__vector_base.99"* %0, i32 0, i32 0
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  %2 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %1, i32 %2
  ret %"class.draco::IndexType"* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i64 @_ZNK5draco17GeometryAttribute10GetBytePosENS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEE(%"class.draco::GeometryAttribute"* %this, i32 %att_index.coerce) #0 comdat {
entry:
  %att_index = alloca %"class.draco::IndexType", align 4
  %this.addr = alloca %"class.draco::GeometryAttribute"*, align 4
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %att_index, i32 0, i32 0
  store i32 %att_index.coerce, i32* %coerce.dive, align 4
  store %"class.draco::GeometryAttribute"* %this, %"class.draco::GeometryAttribute"** %this.addr, align 4
  %this1 = load %"class.draco::GeometryAttribute"*, %"class.draco::GeometryAttribute"** %this.addr, align 4
  %byte_offset_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 6
  %0 = load i64, i64* %byte_offset_, align 8
  %byte_stride_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 5
  %1 = load i64, i64* %byte_stride_, align 8
  %call = call i32 @_ZNK5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EE5valueEv(%"class.draco::IndexType"* %att_index)
  %conv = zext i32 %call to i64
  %mul = mul nsw i64 %1, %conv
  %add = add nsw i64 %0, %mul
  ret i64 %add
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN5draco10DataBuffer4dataEv(%"class.draco::DataBuffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::DataBuffer"*, align 4
  store %"class.draco::DataBuffer"* %this, %"class.draco::DataBuffer"** %this.addr, align 4
  %this1 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %this.addr, align 4
  %data_ = getelementptr inbounds %"class.draco::DataBuffer", %"class.draco::DataBuffer"* %this1, i32 0, i32 0
  %call = call nonnull align 1 dereferenceable(1) i8* @_ZNSt3__26vectorIhNS_9allocatorIhEEEixEm(%"class.std::__2::vector.86"* %data_, i32 0) #6
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) i8* @_ZNSt3__26vectorIhNS_9allocatorIhEEEixEm(%"class.std::__2::vector.86"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.86"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector.86"* %this, %"class.std::__2::vector.86"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.86"*, %"class.std::__2::vector.86"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.86"* %this1 to %"class.std::__2::__vector_base.87"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.87", %"class.std::__2::__vector_base.87"* %0, i32 0, i32 0
  %1 = load i8*, i8** %__begin_, align 4
  %2 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds i8, i8* %1, i32 %2
  ret i8* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define internal i32 @"_ZZ25neuroglancer_draco_decodeENK3$_0clIKjEEDaPT_"(%class.anon* %this, i32* %v_pos) #0 {
entry:
  %this.addr = alloca %class.anon*, align 4
  %v_pos.addr = alloca i32*, align 4
  %partition_point = alloca i32, align 4
  %mask = alloca i32, align 4
  store %class.anon* %this, %class.anon** %this.addr, align 4
  store i32* %v_pos, i32** %v_pos.addr, align 4
  %this1 = load %class.anon*, %class.anon** %this.addr, align 4
  %call = call i32 @_ZNSt3__214numeric_limitsIjE3maxEv() #6
  %0 = getelementptr inbounds %class.anon, %class.anon* %this1, i32 0, i32 0
  %1 = load i32*, i32** %0, align 4
  %2 = load i32, i32* %1, align 4
  %sub = sub nsw i32 32, %2
  %shr = lshr i32 %call, %sub
  %div = udiv i32 %shr, 2
  %add = add i32 %div, 1
  store i32 %add, i32* %partition_point, align 4
  store i32 255, i32* %mask, align 4
  %3 = load i32*, i32** %v_pos.addr, align 4
  %arrayidx = getelementptr inbounds i32, i32* %3, i32 0
  %4 = load i32, i32* %arrayidx, align 4
  %5 = load i32, i32* %partition_point, align 4
  %cmp = icmp ult i32 %4, %5
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %6 = load i32, i32* %mask, align 4
  %and = and i32 %6, 85
  store i32 %and, i32* %mask, align 4
  br label %if.end6

if.else:                                          ; preds = %entry
  %7 = load i32*, i32** %v_pos.addr, align 4
  %arrayidx2 = getelementptr inbounds i32, i32* %7, i32 0
  %8 = load i32, i32* %arrayidx2, align 4
  %9 = load i32, i32* %partition_point, align 4
  %cmp3 = icmp ugt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %mask, align 4
  %and5 = and i32 %10, 170
  store i32 %and5, i32* %mask, align 4
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  br label %if.end6

if.end6:                                          ; preds = %if.end, %if.then
  %11 = load i32*, i32** %v_pos.addr, align 4
  %arrayidx7 = getelementptr inbounds i32, i32* %11, i32 1
  %12 = load i32, i32* %arrayidx7, align 4
  %13 = load i32, i32* %partition_point, align 4
  %cmp8 = icmp ult i32 %12, %13
  br i1 %cmp8, label %if.then9, label %if.else11

if.then9:                                         ; preds = %if.end6
  %14 = load i32, i32* %mask, align 4
  %and10 = and i32 %14, 51
  store i32 %and10, i32* %mask, align 4
  br label %if.end17

if.else11:                                        ; preds = %if.end6
  %15 = load i32*, i32** %v_pos.addr, align 4
  %arrayidx12 = getelementptr inbounds i32, i32* %15, i32 1
  %16 = load i32, i32* %arrayidx12, align 4
  %17 = load i32, i32* %partition_point, align 4
  %cmp13 = icmp ugt i32 %16, %17
  br i1 %cmp13, label %if.then14, label %if.end16

if.then14:                                        ; preds = %if.else11
  %18 = load i32, i32* %mask, align 4
  %and15 = and i32 %18, 204
  store i32 %and15, i32* %mask, align 4
  br label %if.end16

if.end16:                                         ; preds = %if.then14, %if.else11
  br label %if.end17

if.end17:                                         ; preds = %if.end16, %if.then9
  %19 = load i32*, i32** %v_pos.addr, align 4
  %arrayidx18 = getelementptr inbounds i32, i32* %19, i32 2
  %20 = load i32, i32* %arrayidx18, align 4
  %21 = load i32, i32* %partition_point, align 4
  %cmp19 = icmp ult i32 %20, %21
  br i1 %cmp19, label %if.then20, label %if.else22

if.then20:                                        ; preds = %if.end17
  %22 = load i32, i32* %mask, align 4
  %and21 = and i32 %22, 15
  store i32 %and21, i32* %mask, align 4
  br label %if.end28

if.else22:                                        ; preds = %if.end17
  %23 = load i32*, i32** %v_pos.addr, align 4
  %arrayidx23 = getelementptr inbounds i32, i32* %23, i32 2
  %24 = load i32, i32* %arrayidx23, align 4
  %25 = load i32, i32* %partition_point, align 4
  %cmp24 = icmp ugt i32 %24, %25
  br i1 %cmp24, label %if.then25, label %if.end27

if.then25:                                        ; preds = %if.else22
  %26 = load i32, i32* %mask, align 4
  %and26 = and i32 %26, 240
  store i32 %and26, i32* %mask, align 4
  br label %if.end27

if.end27:                                         ; preds = %if.then25, %if.else22
  br label %if.end28

if.end28:                                         ; preds = %if.end27, %if.then20
  %27 = load i32, i32* %mask, align 4
  ret i32 %27
}

; Function Attrs: noinline nounwind optnone
define internal void @"_ZZ25neuroglancer_draco_decodeENK3$_2clEmj"(%class.anon.147* %this, i32 %i, i32 %partition_i) #0 {
entry:
  %this.addr = alloca %class.anon.147*, align 4
  %i.addr = alloca i32, align 4
  %partition_i.addr = alloca i32, align 4
  store %class.anon.147* %this, %class.anon.147** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  store i32 %partition_i, i32* %partition_i.addr, align 4
  %this1 = load %class.anon.147*, %class.anon.147** %this.addr, align 4
  %0 = getelementptr inbounds %class.anon.147, %class.anon.147* %this1, i32 0, i32 0
  %1 = load [9 x i32]*, [9 x i32]** %0, align 4
  %2 = load i32, i32* %partition_i.addr, align 4
  %add = add i32 %2, 1
  %arrayidx = getelementptr inbounds [9 x i32], [9 x i32]* %1, i32 0, i32 %add
  %3 = load i32, i32* %arrayidx, align 4
  %add2 = add i32 %3, 3
  store i32 %add2, i32* %arrayidx, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__214numeric_limitsIjE3maxEv() #0 comdat {
entry:
  %call = call i32 @_ZNSt3__223__libcpp_numeric_limitsIjLb1EE3maxEv() #6
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__223__libcpp_numeric_limitsIjLb1EE3maxEv() #0 comdat {
entry:
  ret i32 -1
}

; Function Attrs: noinline nounwind optnone
define internal void @"_ZZ25neuroglancer_draco_decodeENK3$_3clEmj"(%class.anon.148* %this, i32 %i, i32 %partition_i) #0 {
entry:
  %this.addr = alloca %class.anon.148*, align 4
  %i.addr = alloca i32, align 4
  %partition_i.addr = alloca i32, align 4
  %offset = alloca i32, align 4
  %j = alloca i32, align 4
  store %class.anon.148* %this, %class.anon.148** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  store i32 %partition_i, i32* %partition_i.addr, align 4
  %this1 = load %class.anon.148*, %class.anon.148** %this.addr, align 4
  %0 = getelementptr inbounds %class.anon.148, %class.anon.148* %this1, i32 0, i32 0
  %1 = load [9 x i32]*, [9 x i32]** %0, align 4
  %2 = load i32, i32* %partition_i.addr, align 4
  %add = add i32 %2, 1
  %arrayidx = getelementptr inbounds [9 x i32], [9 x i32]* %1, i32 0, i32 %add
  %3 = load i32, i32* %arrayidx, align 4
  store i32 %3, i32* %offset, align 4
  %4 = getelementptr inbounds %class.anon.148, %class.anon.148* %this1, i32 0, i32 0
  %5 = load [9 x i32]*, [9 x i32]** %4, align 4
  %6 = load i32, i32* %partition_i.addr, align 4
  %add2 = add i32 %6, 1
  %arrayidx3 = getelementptr inbounds [9 x i32], [9 x i32]* %5, i32 0, i32 %add2
  %7 = load i32, i32* %arrayidx3, align 4
  %add4 = add i32 %7, 3
  store i32 %add4, i32* %arrayidx3, align 4
  store i32 0, i32* %j, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %8 = load i32, i32* %j, align 4
  %cmp = icmp slt i32 %8, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %9 = getelementptr inbounds %class.anon.148, %class.anon.148* %this1, i32 0, i32 2
  %10 = load %"class.std::__2::unique_ptr.142"*, %"class.std::__2::unique_ptr.142"** %9, align 4
  %11 = load i32, i32* %i.addr, align 4
  %12 = load i32, i32* %j, align 4
  %add5 = add i32 %11, %12
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNKSt3__210unique_ptrIA_jN12_GLOBAL__N_111FreeDeleterEEixEm(%"class.std::__2::unique_ptr.142"* %10, i32 %add5)
  %13 = load i32, i32* %call, align 4
  %14 = getelementptr inbounds %class.anon.148, %class.anon.148* %this1, i32 0, i32 1
  %15 = load %"class.std::__2::unique_ptr.142"*, %"class.std::__2::unique_ptr.142"** %14, align 4
  %16 = load i32, i32* %offset, align 4
  %17 = load i32, i32* %j, align 4
  %add6 = add i32 %16, %17
  %call7 = call nonnull align 4 dereferenceable(4) i32* @_ZNKSt3__210unique_ptrIA_jN12_GLOBAL__N_111FreeDeleterEEixEm(%"class.std::__2::unique_ptr.142"* %15, i32 %add6)
  store i32 %13, i32* %call7, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %18 = load i32, i32* %j, align 4
  %inc = add nsw i32 %18, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.23"* @_ZNSt3__210unique_ptrIN5draco4MeshENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.23"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.23"*, align 4
  store %"class.std::__2::unique_ptr.23"* %this, %"class.std::__2::unique_ptr.23"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.23"*, %"class.std::__2::unique_ptr.23"** %this.addr, align 4
  call void @_ZNSt3__210unique_ptrIN5draco4MeshENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.23"* %this1, %"class.draco::Mesh"* null) #6
  ret %"class.std::__2::unique_ptr.23"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::Status"* @_ZN5draco6StatusD2Ev(%"class.draco::Status"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::Status"*, align 4
  store %"class.draco::Status"* %this, %"class.draco::Status"** %this.addr, align 4
  %this1 = load %"class.draco::Status"*, %"class.draco::Status"** %this.addr, align 4
  %error_msg_ = getelementptr inbounds %"class.draco::Status", %"class.draco::Status"* %this1, i32 0, i32 1
  %call = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_string"* %error_msg_) #6
  ret %"class.draco::Status"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__210unique_ptrIN5draco4MeshENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.23"* %this, %"class.draco::Mesh"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.23"*, align 4
  %__p.addr = alloca %"class.draco::Mesh"*, align 4
  %__tmp = alloca %"class.draco::Mesh"*, align 4
  store %"class.std::__2::unique_ptr.23"* %this, %"class.std::__2::unique_ptr.23"** %this.addr, align 4
  store %"class.draco::Mesh"* %__p, %"class.draco::Mesh"** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.23"*, %"class.std::__2::unique_ptr.23"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.23", %"class.std::__2::unique_ptr.23"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::Mesh"** @_ZNSt3__217__compressed_pairIPN5draco4MeshENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.24"* %__ptr_) #6
  %0 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %call, align 4
  store %"class.draco::Mesh"* %0, %"class.draco::Mesh"** %__tmp, align 4
  %1 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %__p.addr, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr.23", %"class.std::__2::unique_ptr.23"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::Mesh"** @_ZNSt3__217__compressed_pairIPN5draco4MeshENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.24"* %__ptr_2) #6
  store %"class.draco::Mesh"* %1, %"class.draco::Mesh"** %call3, align 4
  %2 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %__tmp, align 4
  %tobool = icmp ne %"class.draco::Mesh"* %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__ptr_4 = getelementptr inbounds %"class.std::__2::unique_ptr.23", %"class.std::__2::unique_ptr.23"* %this1, i32 0, i32 0
  %call5 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.141"* @_ZNSt3__217__compressed_pairIPN5draco4MeshENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.24"* %__ptr_4) #6
  %3 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %__tmp, align 4
  call void @_ZNKSt3__214default_deleteIN5draco4MeshEEclEPS2_(%"struct.std::__2::default_delete.141"* %call5, %"class.draco::Mesh"* %3) #6
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::Mesh"** @_ZNSt3__217__compressed_pairIPN5draco4MeshENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.24"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.24"*, align 4
  store %"class.std::__2::__compressed_pair.24"* %this, %"class.std::__2::__compressed_pair.24"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.24"*, %"class.std::__2::__compressed_pair.24"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.24"* %this1 to %"struct.std::__2::__compressed_pair_elem.25"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::Mesh"** @_ZNSt3__222__compressed_pair_elemIPN5draco4MeshELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.25"* %0) #6
  ret %"class.draco::Mesh"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.141"* @_ZNSt3__217__compressed_pairIPN5draco4MeshENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.24"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.24"*, align 4
  store %"class.std::__2::__compressed_pair.24"* %this, %"class.std::__2::__compressed_pair.24"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.24"*, %"class.std::__2::__compressed_pair.24"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.24"* %this1 to %"struct.std::__2::__compressed_pair_elem.140"*
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.141"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco4MeshEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.140"* %0) #6
  ret %"struct.std::__2::default_delete.141"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__214default_deleteIN5draco4MeshEEclEPS2_(%"struct.std::__2::default_delete.141"* %this, %"class.draco::Mesh"* %__ptr) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::default_delete.141"*, align 4
  %__ptr.addr = alloca %"class.draco::Mesh"*, align 4
  store %"struct.std::__2::default_delete.141"* %this, %"struct.std::__2::default_delete.141"** %this.addr, align 4
  store %"class.draco::Mesh"* %__ptr, %"class.draco::Mesh"** %__ptr.addr, align 4
  %this1 = load %"struct.std::__2::default_delete.141"*, %"struct.std::__2::default_delete.141"** %this.addr, align 4
  %0 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %__ptr.addr, align 4
  %isnull = icmp eq %"class.draco::Mesh"* %0, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %entry
  %1 = bitcast %"class.draco::Mesh"* %0 to void (%"class.draco::Mesh"*)***
  %vtable = load void (%"class.draco::Mesh"*)**, void (%"class.draco::Mesh"*)*** %1, align 4
  %vfn = getelementptr inbounds void (%"class.draco::Mesh"*)*, void (%"class.draco::Mesh"*)** %vtable, i64 1
  %2 = load void (%"class.draco::Mesh"*)*, void (%"class.draco::Mesh"*)** %vfn, align 4
  call void %2(%"class.draco::Mesh"* %0) #6
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::Mesh"** @_ZNSt3__222__compressed_pair_elemIPN5draco4MeshELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.25"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.25"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.25"* %this, %"struct.std::__2::__compressed_pair_elem.25"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.25"*, %"struct.std::__2::__compressed_pair_elem.25"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.25", %"struct.std::__2::__compressed_pair_elem.25"* %this1, i32 0, i32 0
  ret %"class.draco::Mesh"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.141"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco4MeshEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.140"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.140"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.140"* %this, %"struct.std::__2::__compressed_pair_elem.140"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.140"*, %"struct.std::__2::__compressed_pair_elem.140"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.140"* %this1 to %"struct.std::__2::default_delete.141"*
  ret %"struct.std::__2::default_delete.141"* %0
}

; Function Attrs: nounwind
declare %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_string"* returned) unnamed_addr #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::DracoOptions"* @_ZN5draco12DracoOptionsINS_17GeometryAttribute4TypeEED2Ev(%"class.draco::DracoOptions"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::DracoOptions"*, align 4
  store %"class.draco::DracoOptions"* %this, %"class.draco::DracoOptions"** %this.addr, align 4
  %this1 = load %"class.draco::DracoOptions"*, %"class.draco::DracoOptions"** %this.addr, align 4
  %attribute_options_ = getelementptr inbounds %"class.draco::DracoOptions", %"class.draco::DracoOptions"* %this1, i32 0, i32 1
  %call = call %"class.std::__2::map.7"* @_ZNSt3__23mapIN5draco17GeometryAttribute4TypeENS1_7OptionsENS_4lessIS3_EENS_9allocatorINS_4pairIKS3_S4_EEEEED2Ev(%"class.std::__2::map.7"* %attribute_options_) #6
  %global_options_ = getelementptr inbounds %"class.draco::DracoOptions", %"class.draco::DracoOptions"* %this1, i32 0, i32 0
  %call2 = call %"class.draco::Options"* @_ZN5draco7OptionsD2Ev(%"class.draco::Options"* %global_options_) #6
  ret %"class.draco::DracoOptions"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::map.7"* @_ZNSt3__23mapIN5draco17GeometryAttribute4TypeENS1_7OptionsENS_4lessIS3_EENS_9allocatorINS_4pairIKS3_S4_EEEEED2Ev(%"class.std::__2::map.7"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::map.7"*, align 4
  store %"class.std::__2::map.7"* %this, %"class.std::__2::map.7"** %this.addr, align 4
  %this1 = load %"class.std::__2::map.7"*, %"class.std::__2::map.7"** %this.addr, align 4
  %__tree_ = getelementptr inbounds %"class.std::__2::map.7", %"class.std::__2::map.7"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::__tree.8"* @_ZNSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEED2Ev(%"class.std::__2::__tree.8"* %__tree_) #6
  ret %"class.std::__2::map.7"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::Options"* @_ZN5draco7OptionsD2Ev(%"class.draco::Options"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::Options"*, align 4
  store %"class.draco::Options"* %this, %"class.draco::Options"** %this.addr, align 4
  %this1 = load %"class.draco::Options"*, %"class.draco::Options"** %this.addr, align 4
  %options_ = getelementptr inbounds %"class.draco::Options", %"class.draco::Options"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::map"* @_ZNSt3__23mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_NS_4lessIS6_EENS4_INS_4pairIKS6_S6_EEEEED2Ev(%"class.std::__2::map"* %options_) #6
  ret %"class.draco::Options"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree.8"* @_ZNSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEED2Ev(%"class.std::__2::__tree.8"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree.8"*, align 4
  store %"class.std::__2::__tree.8"* %this, %"class.std::__2::__tree.8"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree.8"*, %"class.std::__2::__tree.8"** %this.addr, align 4
  %call = call %"class.std::__2::__tree_node"* @_ZNKSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE6__rootEv(%"class.std::__2::__tree.8"* %this1) #6
  call void @_ZNSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE7destroyEPNS_11__tree_nodeIS6_PvEE(%"class.std::__2::__tree.8"* %this1, %"class.std::__2::__tree_node"* %call) #6
  ret %"class.std::__2::__tree.8"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE7destroyEPNS_11__tree_nodeIS6_PvEE(%"class.std::__2::__tree.8"* %this, %"class.std::__2::__tree_node"* %__nd) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree.8"*, align 4
  %__nd.addr = alloca %"class.std::__2::__tree_node"*, align 4
  %__na = alloca %"class.std::__2::allocator.11"*, align 4
  store %"class.std::__2::__tree.8"* %this, %"class.std::__2::__tree.8"** %this.addr, align 4
  store %"class.std::__2::__tree_node"* %__nd, %"class.std::__2::__tree_node"** %__nd.addr, align 4
  %this1 = load %"class.std::__2::__tree.8"*, %"class.std::__2::__tree.8"** %this.addr, align 4
  %0 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %__nd.addr, align 4
  %cmp = icmp ne %"class.std::__2::__tree_node"* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %__nd.addr, align 4
  %2 = bitcast %"class.std::__2::__tree_node"* %1 to %"class.std::__2::__tree_end_node"*
  %__left_ = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %2, i32 0, i32 0
  %3 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__left_, align 4
  %4 = bitcast %"class.std::__2::__tree_node_base"* %3 to %"class.std::__2::__tree_node"*
  call void @_ZNSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE7destroyEPNS_11__tree_nodeIS6_PvEE(%"class.std::__2::__tree.8"* %this1, %"class.std::__2::__tree_node"* %4) #6
  %5 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %__nd.addr, align 4
  %6 = bitcast %"class.std::__2::__tree_node"* %5 to %"class.std::__2::__tree_node_base"*
  %__right_ = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %6, i32 0, i32 1
  %7 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__right_, align 4
  %8 = bitcast %"class.std::__2::__tree_node_base"* %7 to %"class.std::__2::__tree_node"*
  call void @_ZNSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE7destroyEPNS_11__tree_nodeIS6_PvEE(%"class.std::__2::__tree.8"* %this1, %"class.std::__2::__tree_node"* %8) #6
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.11"* @_ZNSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE12__node_allocEv(%"class.std::__2::__tree.8"* %this1) #6
  store %"class.std::__2::allocator.11"* %call, %"class.std::__2::allocator.11"** %__na, align 4
  %9 = load %"class.std::__2::allocator.11"*, %"class.std::__2::allocator.11"** %__na, align 4
  %10 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %__nd.addr, align 4
  %__value_ = getelementptr inbounds %"class.std::__2::__tree_node", %"class.std::__2::__tree_node"* %10, i32 0, i32 1
  %call2 = call %"struct.std::__2::pair"* @_ZNSt3__222__tree_key_value_typesINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEEE9__get_ptrERS6_(%"struct.std::__2::__value_type"* nonnull align 4 dereferenceable(16) %__value_)
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS4_7OptionsEEEPvEEEEE7destroyINS_4pairIKS6_S7_EEEEvRSB_PT_(%"class.std::__2::allocator.11"* nonnull align 1 dereferenceable(1) %9, %"struct.std::__2::pair"* %call2)
  %11 = load %"class.std::__2::allocator.11"*, %"class.std::__2::allocator.11"** %__na, align 4
  %12 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %__nd.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS4_7OptionsEEEPvEEEEE10deallocateERSB_PSA_m(%"class.std::__2::allocator.11"* nonnull align 1 dereferenceable(1) %11, %"class.std::__2::__tree_node"* %12, i32 1) #6
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_node"* @_ZNKSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE6__rootEv(%"class.std::__2::__tree.8"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree.8"*, align 4
  store %"class.std::__2::__tree.8"* %this, %"class.std::__2::__tree.8"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree.8"*, %"class.std::__2::__tree.8"** %this.addr, align 4
  %call = call %"class.std::__2::__tree_end_node"* @_ZNKSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE10__end_nodeEv(%"class.std::__2::__tree.8"* %this1) #6
  %__left_ = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %call, i32 0, i32 0
  %0 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__left_, align 4
  %1 = bitcast %"class.std::__2::__tree_node_base"* %0 to %"class.std::__2::__tree_node"*
  ret %"class.std::__2::__tree_node"* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.11"* @_ZNSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE12__node_allocEv(%"class.std::__2::__tree.8"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree.8"*, align 4
  store %"class.std::__2::__tree.8"* %this, %"class.std::__2::__tree.8"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree.8"*, %"class.std::__2::__tree.8"** %this.addr, align 4
  %__pair1_ = getelementptr inbounds %"class.std::__2::__tree.8", %"class.std::__2::__tree.8"* %this1, i32 0, i32 1
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.11"* @_ZNSt3__217__compressed_pairINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEENS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENSA_7OptionsEEES3_EEEEE6secondEv(%"class.std::__2::__compressed_pair.9"* %__pair1_) #6
  ret %"class.std::__2::allocator.11"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS4_7OptionsEEEPvEEEEE7destroyINS_4pairIKS6_S7_EEEEvRSB_PT_(%"class.std::__2::allocator.11"* nonnull align 1 dereferenceable(1) %__a, %"struct.std::__2::pair"* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.11"*, align 4
  %__p.addr = alloca %"struct.std::__2::pair"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy", align 1
  store %"class.std::__2::allocator.11"* %__a, %"class.std::__2::allocator.11"** %__a.addr, align 4
  store %"struct.std::__2::pair"* %__p, %"struct.std::__2::pair"** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.11"*, %"class.std::__2::allocator.11"** %__a.addr, align 4
  %2 = load %"struct.std::__2::pair"*, %"struct.std::__2::pair"** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS4_7OptionsEEEPvEEEEE9__destroyINS_4pairIKS6_S7_EEEEvNS_17integral_constantIbLb0EEERSB_PT_(%"class.std::__2::allocator.11"* nonnull align 1 dereferenceable(1) %1, %"struct.std::__2::pair"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::pair"* @_ZNSt3__222__tree_key_value_typesINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEEE9__get_ptrERS6_(%"struct.std::__2::__value_type"* nonnull align 4 dereferenceable(16) %__n) #0 comdat {
entry:
  %__n.addr = alloca %"struct.std::__2::__value_type"*, align 4
  store %"struct.std::__2::__value_type"* %__n, %"struct.std::__2::__value_type"** %__n.addr, align 4
  %0 = load %"struct.std::__2::__value_type"*, %"struct.std::__2::__value_type"** %__n.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %"struct.std::__2::pair"* @_ZNSt3__212__value_typeIN5draco17GeometryAttribute4TypeENS1_7OptionsEE11__get_valueEv(%"struct.std::__2::__value_type"* %0)
  %call1 = call %"struct.std::__2::pair"* @_ZNSt3__29addressofINS_4pairIKN5draco17GeometryAttribute4TypeENS2_7OptionsEEEEEPT_RS8_(%"struct.std::__2::pair"* nonnull align 4 dereferenceable(16) %call) #6
  ret %"struct.std::__2::pair"* %call1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS4_7OptionsEEEPvEEEEE10deallocateERSB_PSA_m(%"class.std::__2::allocator.11"* nonnull align 1 dereferenceable(1) %__a, %"class.std::__2::__tree_node"* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.11"*, align 4
  %__p.addr = alloca %"class.std::__2::__tree_node"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.11"* %__a, %"class.std::__2::allocator.11"** %__a.addr, align 4
  store %"class.std::__2::__tree_node"* %__p, %"class.std::__2::__tree_node"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.11"*, %"class.std::__2::allocator.11"** %__a.addr, align 4
  %1 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPvEEE10deallocateEPS9_m(%"class.std::__2::allocator.11"* %0, %"class.std::__2::__tree_node"* %1, i32 %2) #6
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.11"* @_ZNSt3__217__compressed_pairINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEENS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENSA_7OptionsEEES3_EEEEE6secondEv(%"class.std::__2::__compressed_pair.9"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.9"*, align 4
  store %"class.std::__2::__compressed_pair.9"* %this, %"class.std::__2::__compressed_pair.9"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.9"*, %"class.std::__2::__compressed_pair.9"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.9"* %this1 to %"struct.std::__2::__compressed_pair_elem.10"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.11"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS4_7OptionsEEEPvEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.10"* %0) #6
  ret %"class.std::__2::allocator.11"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.11"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS4_7OptionsEEEPvEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.10"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.10"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.10"* %this, %"struct.std::__2::__compressed_pair_elem.10"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.10"*, %"struct.std::__2::__compressed_pair_elem.10"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.10"* %this1 to %"class.std::__2::allocator.11"*
  ret %"class.std::__2::allocator.11"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS4_7OptionsEEEPvEEEEE9__destroyINS_4pairIKS6_S7_EEEEvNS_17integral_constantIbLb0EEERSB_PT_(%"class.std::__2::allocator.11"* nonnull align 1 dereferenceable(1) %0, %"struct.std::__2::pair"* %__p) #0 comdat {
entry:
  %1 = alloca %"struct.std::__2::integral_constant", align 1
  %.addr = alloca %"class.std::__2::allocator.11"*, align 4
  %__p.addr = alloca %"struct.std::__2::pair"*, align 4
  store %"class.std::__2::allocator.11"* %0, %"class.std::__2::allocator.11"** %.addr, align 4
  store %"struct.std::__2::pair"* %__p, %"struct.std::__2::pair"** %__p.addr, align 4
  %2 = load %"struct.std::__2::pair"*, %"struct.std::__2::pair"** %__p.addr, align 4
  %call = call %"struct.std::__2::pair"* @_ZNSt3__24pairIKN5draco17GeometryAttribute4TypeENS1_7OptionsEED2Ev(%"struct.std::__2::pair"* %2) #6
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::pair"* @_ZNSt3__24pairIKN5draco17GeometryAttribute4TypeENS1_7OptionsEED2Ev(%"struct.std::__2::pair"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::pair"*, align 4
  store %"struct.std::__2::pair"* %this, %"struct.std::__2::pair"** %this.addr, align 4
  %this1 = load %"struct.std::__2::pair"*, %"struct.std::__2::pair"** %this.addr, align 4
  %second = getelementptr inbounds %"struct.std::__2::pair", %"struct.std::__2::pair"* %this1, i32 0, i32 1
  %call = call %"class.draco::Options"* @_ZN5draco7OptionsD2Ev(%"class.draco::Options"* %second) #6
  ret %"struct.std::__2::pair"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::pair"* @_ZNSt3__29addressofINS_4pairIKN5draco17GeometryAttribute4TypeENS2_7OptionsEEEEEPT_RS8_(%"struct.std::__2::pair"* nonnull align 4 dereferenceable(16) %__x) #0 comdat {
entry:
  %__x.addr = alloca %"struct.std::__2::pair"*, align 4
  store %"struct.std::__2::pair"* %__x, %"struct.std::__2::pair"** %__x.addr, align 4
  %0 = load %"struct.std::__2::pair"*, %"struct.std::__2::pair"** %__x.addr, align 4
  ret %"struct.std::__2::pair"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %"struct.std::__2::pair"* @_ZNSt3__212__value_typeIN5draco17GeometryAttribute4TypeENS1_7OptionsEE11__get_valueEv(%"struct.std::__2::__value_type"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__value_type"*, align 4
  store %"struct.std::__2::__value_type"* %this, %"struct.std::__2::__value_type"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__value_type"*, %"struct.std::__2::__value_type"** %this.addr, align 4
  %__cc = getelementptr inbounds %"struct.std::__2::__value_type", %"struct.std::__2::__value_type"* %this1, i32 0, i32 0
  ret %"struct.std::__2::pair"* %__cc
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS3_7OptionsEEEPvEEE10deallocateEPS9_m(%"class.std::__2::allocator.11"* %this, %"class.std::__2::__tree_node"* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.11"*, align 4
  %__p.addr = alloca %"class.std::__2::__tree_node"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.11"* %this, %"class.std::__2::allocator.11"** %this.addr, align 4
  store %"class.std::__2::__tree_node"* %__p, %"class.std::__2::__tree_node"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.11"*, %"class.std::__2::allocator.11"** %this.addr, align 4
  %0 = load %"class.std::__2::__tree_node"*, %"class.std::__2::__tree_node"** %__p.addr, align 4
  %1 = bitcast %"class.std::__2::__tree_node"* %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 32
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %__ptr, i32 %__size, i32 %__align) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  %1 = load i32, i32* %__size.addr, align 4
  %2 = load i32, i32* %__align.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm(i8* %0, i32 %1, i32 %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm(i8* %__ptr, i32 %__size, i32 %__align) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  %1 = load i32, i32* %__size.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm(i8* %0, i32 %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm(i8* %__ptr, i32 %__size) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller9__do_callEPv(i8* %0)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller9__do_callEPv(i8* %__ptr) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  call void @_ZdlPv(i8* %0) #7
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #5

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_end_node"* @_ZNKSt3__26__treeINS_12__value_typeIN5draco17GeometryAttribute4TypeENS2_7OptionsEEENS_19__map_value_compareIS4_S6_NS_4lessIS4_EELb1EEENS_9allocatorIS6_EEE10__end_nodeEv(%"class.std::__2::__tree.8"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree.8"*, align 4
  store %"class.std::__2::__tree.8"* %this, %"class.std::__2::__tree.8"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree.8"*, %"class.std::__2::__tree.8"** %this.addr, align 4
  %__pair1_ = getelementptr inbounds %"class.std::__2::__tree.8", %"class.std::__2::__tree.8"* %this1, i32 0, i32 1
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_end_node"* @_ZNKSt3__217__compressed_pairINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEENS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENSA_7OptionsEEES3_EEEEE5firstEv(%"class.std::__2::__compressed_pair.9"* %__pair1_) #6
  %call2 = call %"class.std::__2::__tree_end_node"* @_ZNSt3__214pointer_traitsIPNS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEEE10pointer_toERS6_(%"class.std::__2::__tree_end_node"* nonnull align 4 dereferenceable(4) %call) #6
  ret %"class.std::__2::__tree_end_node"* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_end_node"* @_ZNKSt3__217__compressed_pairINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEENS_9allocatorINS_11__tree_nodeINS_12__value_typeIN5draco17GeometryAttribute4TypeENSA_7OptionsEEES3_EEEEE5firstEv(%"class.std::__2::__compressed_pair.9"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.9"*, align 4
  store %"class.std::__2::__compressed_pair.9"* %this, %"class.std::__2::__compressed_pair.9"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.9"*, %"class.std::__2::__compressed_pair.9"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.9"* %this1 to %"struct.std::__2::__compressed_pair_elem.2"*
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_end_node"* @_ZNKSt3__222__compressed_pair_elemINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.2"* %0) #6
  ret %"class.std::__2::__tree_end_node"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_end_node"* @_ZNKSt3__222__compressed_pair_elemINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.2"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.2"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.2"* %this, %"struct.std::__2::__compressed_pair_elem.2"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.2"*, %"struct.std::__2::__compressed_pair_elem.2"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.2", %"struct.std::__2::__compressed_pair_elem.2"* %this1, i32 0, i32 0
  ret %"class.std::__2::__tree_end_node"* %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::map"* @_ZNSt3__23mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_NS_4lessIS6_EENS4_INS_4pairIKS6_S6_EEEEED2Ev(%"class.std::__2::map"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::map"*, align 4
  store %"class.std::__2::map"* %this, %"class.std::__2::map"** %this.addr, align 4
  %this1 = load %"class.std::__2::map"*, %"class.std::__2::map"** %this.addr, align 4
  %__tree_ = getelementptr inbounds %"class.std::__2::map", %"class.std::__2::map"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::__tree"* @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEED2Ev(%"class.std::__2::__tree"* %__tree_) #6
  ret %"class.std::__2::map"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree"* @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEED2Ev(%"class.std::__2::__tree"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree"*, align 4
  store %"class.std::__2::__tree"* %this, %"class.std::__2::__tree"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree"*, %"class.std::__2::__tree"** %this.addr, align 4
  %call = call %"class.std::__2::__tree_node.149"* @_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE6__rootEv(%"class.std::__2::__tree"* %this1) #6
  call void @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE7destroyEPNS_11__tree_nodeIS8_PvEE(%"class.std::__2::__tree"* %this1, %"class.std::__2::__tree_node.149"* %call) #6
  ret %"class.std::__2::__tree"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE7destroyEPNS_11__tree_nodeIS8_PvEE(%"class.std::__2::__tree"* %this, %"class.std::__2::__tree_node.149"* %__nd) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree"*, align 4
  %__nd.addr = alloca %"class.std::__2::__tree_node.149"*, align 4
  %__na = alloca %"class.std::__2::allocator"*, align 4
  store %"class.std::__2::__tree"* %this, %"class.std::__2::__tree"** %this.addr, align 4
  store %"class.std::__2::__tree_node.149"* %__nd, %"class.std::__2::__tree_node.149"** %__nd.addr, align 4
  %this1 = load %"class.std::__2::__tree"*, %"class.std::__2::__tree"** %this.addr, align 4
  %0 = load %"class.std::__2::__tree_node.149"*, %"class.std::__2::__tree_node.149"** %__nd.addr, align 4
  %cmp = icmp ne %"class.std::__2::__tree_node.149"* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %"class.std::__2::__tree_node.149"*, %"class.std::__2::__tree_node.149"** %__nd.addr, align 4
  %2 = bitcast %"class.std::__2::__tree_node.149"* %1 to %"class.std::__2::__tree_end_node"*
  %__left_ = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %2, i32 0, i32 0
  %3 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__left_, align 4
  %4 = bitcast %"class.std::__2::__tree_node_base"* %3 to %"class.std::__2::__tree_node.149"*
  call void @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE7destroyEPNS_11__tree_nodeIS8_PvEE(%"class.std::__2::__tree"* %this1, %"class.std::__2::__tree_node.149"* %4) #6
  %5 = load %"class.std::__2::__tree_node.149"*, %"class.std::__2::__tree_node.149"** %__nd.addr, align 4
  %6 = bitcast %"class.std::__2::__tree_node.149"* %5 to %"class.std::__2::__tree_node_base"*
  %__right_ = getelementptr inbounds %"class.std::__2::__tree_node_base", %"class.std::__2::__tree_node_base"* %6, i32 0, i32 1
  %7 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__right_, align 4
  %8 = bitcast %"class.std::__2::__tree_node_base"* %7 to %"class.std::__2::__tree_node.149"*
  call void @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE7destroyEPNS_11__tree_nodeIS8_PvEE(%"class.std::__2::__tree"* %this1, %"class.std::__2::__tree_node.149"* %8) #6
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE12__node_allocEv(%"class.std::__2::__tree"* %this1) #6
  store %"class.std::__2::allocator"* %call, %"class.std::__2::allocator"** %__na, align 4
  %9 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__na, align 4
  %10 = load %"class.std::__2::__tree_node.149"*, %"class.std::__2::__tree_node.149"** %__nd.addr, align 4
  %__value_ = getelementptr inbounds %"class.std::__2::__tree_node.149", %"class.std::__2::__tree_node.149"* %10, i32 0, i32 1
  %call2 = call %"struct.std::__2::pair.151"* @_ZNSt3__222__tree_key_value_typesINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEE9__get_ptrERS8_(%"struct.std::__2::__value_type.150"* nonnull align 4 dereferenceable(24) %__value_)
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEEE7destroyINS_4pairIKS8_S8_EEEEvRSC_PT_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %9, %"struct.std::__2::pair.151"* %call2)
  %11 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__na, align 4
  %12 = load %"class.std::__2::__tree_node.149"*, %"class.std::__2::__tree_node.149"** %__nd.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEEE10deallocateERSC_PSB_m(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %11, %"class.std::__2::__tree_node.149"* %12, i32 1) #6
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_node.149"* @_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE6__rootEv(%"class.std::__2::__tree"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree"*, align 4
  store %"class.std::__2::__tree"* %this, %"class.std::__2::__tree"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree"*, %"class.std::__2::__tree"** %this.addr, align 4
  %call = call %"class.std::__2::__tree_end_node"* @_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE10__end_nodeEv(%"class.std::__2::__tree"* %this1) #6
  %__left_ = getelementptr inbounds %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_end_node"* %call, i32 0, i32 0
  %0 = load %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_node_base"** %__left_, align 4
  %1 = bitcast %"class.std::__2::__tree_node_base"* %0 to %"class.std::__2::__tree_node.149"*
  ret %"class.std::__2::__tree_node.149"* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE12__node_allocEv(%"class.std::__2::__tree"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree"*, align 4
  store %"class.std::__2::__tree"* %this, %"class.std::__2::__tree"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree"*, %"class.std::__2::__tree"** %this.addr, align 4
  %__pair1_ = getelementptr inbounds %"class.std::__2::__tree", %"class.std::__2::__tree"* %this1, i32 0, i32 1
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__217__compressed_pairINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEENS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS7_IcEEEESE_EES3_EEEEE6secondEv(%"class.std::__2::__compressed_pair.1"* %__pair1_) #6
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEEE7destroyINS_4pairIKS8_S8_EEEEvRSC_PT_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, %"struct.std::__2::pair.151"* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca %"struct.std::__2::pair.151"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy.152", align 1
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store %"struct.std::__2::pair.151"* %__p, %"struct.std::__2::pair.151"** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy.152"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %2 = load %"struct.std::__2::pair.151"*, %"struct.std::__2::pair.151"** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEEE9__destroyINS_4pairIKS8_S8_EEEEvNS_17integral_constantIbLb0EEERSC_PT_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %1, %"struct.std::__2::pair.151"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::pair.151"* @_ZNSt3__222__tree_key_value_typesINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEE9__get_ptrERS8_(%"struct.std::__2::__value_type.150"* nonnull align 4 dereferenceable(24) %__n) #0 comdat {
entry:
  %__n.addr = alloca %"struct.std::__2::__value_type.150"*, align 4
  store %"struct.std::__2::__value_type.150"* %__n, %"struct.std::__2::__value_type.150"** %__n.addr, align 4
  %0 = load %"struct.std::__2::__value_type.150"*, %"struct.std::__2::__value_type.150"** %__n.addr, align 4
  %call = call nonnull align 4 dereferenceable(24) %"struct.std::__2::pair.151"* @_ZNSt3__212__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_E11__get_valueEv(%"struct.std::__2::__value_type.150"* %0)
  %call1 = call %"struct.std::__2::pair.151"* @_ZNSt3__29addressofINS_4pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEEEPT_RSA_(%"struct.std::__2::pair.151"* nonnull align 4 dereferenceable(24) %call) #6
  ret %"struct.std::__2::pair.151"* %call1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEEE10deallocateERSC_PSB_m(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, %"class.std::__2::__tree_node.149"* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca %"class.std::__2::__tree_node.149"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store %"class.std::__2::__tree_node.149"* %__p, %"class.std::__2::__tree_node.149"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %1 = load %"class.std::__2::__tree_node.149"*, %"class.std::__2::__tree_node.149"** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEES7_EEPvEEE10deallocateEPSA_m(%"class.std::__2::allocator"* %0, %"class.std::__2::__tree_node.149"* %1, i32 %2) #6
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__217__compressed_pairINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEENS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS7_IcEEEESE_EES3_EEEEE6secondEv(%"class.std::__2::__compressed_pair.1"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.1"*, align 4
  store %"class.std::__2::__compressed_pair.1"* %this, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.1"*, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.1"* %this1 to %"struct.std::__2::__compressed_pair_elem.3"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.3"* %0) #6
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.3"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.3"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.3"* %this, %"struct.std::__2::__compressed_pair_elem.3"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.3"*, %"struct.std::__2::__compressed_pair_elem.3"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.3"* %this1 to %"class.std::__2::allocator"*
  ret %"class.std::__2::allocator"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEES8_EEPvEEEEE9__destroyINS_4pairIKS8_S8_EEEEvNS_17integral_constantIbLb0EEERSC_PT_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %0, %"struct.std::__2::pair.151"* %__p) #0 comdat {
entry:
  %1 = alloca %"struct.std::__2::integral_constant", align 1
  %.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca %"struct.std::__2::pair.151"*, align 4
  store %"class.std::__2::allocator"* %0, %"class.std::__2::allocator"** %.addr, align 4
  store %"struct.std::__2::pair.151"* %__p, %"struct.std::__2::pair.151"** %__p.addr, align 4
  %2 = load %"struct.std::__2::pair.151"*, %"struct.std::__2::pair.151"** %__p.addr, align 4
  %call = call %"struct.std::__2::pair.151"* @_ZNSt3__24pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_ED2Ev(%"struct.std::__2::pair.151"* %2) #6
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::pair.151"* @_ZNSt3__24pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_ED2Ev(%"struct.std::__2::pair.151"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::pair.151"*, align 4
  store %"struct.std::__2::pair.151"* %this, %"struct.std::__2::pair.151"** %this.addr, align 4
  %this1 = load %"struct.std::__2::pair.151"*, %"struct.std::__2::pair.151"** %this.addr, align 4
  %second = getelementptr inbounds %"struct.std::__2::pair.151", %"struct.std::__2::pair.151"* %this1, i32 0, i32 1
  %call = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_string"* %second) #6
  %first = getelementptr inbounds %"struct.std::__2::pair.151", %"struct.std::__2::pair.151"* %this1, i32 0, i32 0
  %call2 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_string"* %first) #6
  ret %"struct.std::__2::pair.151"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::pair.151"* @_ZNSt3__29addressofINS_4pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EEEEPT_RSA_(%"struct.std::__2::pair.151"* nonnull align 4 dereferenceable(24) %__x) #0 comdat {
entry:
  %__x.addr = alloca %"struct.std::__2::pair.151"*, align 4
  store %"struct.std::__2::pair.151"* %__x, %"struct.std::__2::pair.151"** %__x.addr, align 4
  %0 = load %"struct.std::__2::pair.151"*, %"struct.std::__2::pair.151"** %__x.addr, align 4
  ret %"struct.std::__2::pair.151"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(24) %"struct.std::__2::pair.151"* @_ZNSt3__212__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES6_E11__get_valueEv(%"struct.std::__2::__value_type.150"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__value_type.150"*, align 4
  store %"struct.std::__2::__value_type.150"* %this, %"struct.std::__2::__value_type.150"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__value_type.150"*, %"struct.std::__2::__value_type.150"** %this.addr, align 4
  %__cc = getelementptr inbounds %"struct.std::__2::__value_type.150", %"struct.std::__2::__value_type.150"* %this1, i32 0, i32 0
  ret %"struct.std::__2::pair.151"* %__cc
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEES7_EEPvEEE10deallocateEPSA_m(%"class.std::__2::allocator"* %this, %"class.std::__2::__tree_node.149"* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca %"class.std::__2::__tree_node.149"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  store %"class.std::__2::__tree_node.149"* %__p, %"class.std::__2::__tree_node.149"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  %0 = load %"class.std::__2::__tree_node.149"*, %"class.std::__2::__tree_node.149"** %__p.addr, align 4
  %1 = bitcast %"class.std::__2::__tree_node.149"* %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 40
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__tree_end_node"* @_ZNKSt3__26__treeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES7_EENS_19__map_value_compareIS7_S8_NS_4lessIS7_EELb1EEENS5_IS8_EEE10__end_nodeEv(%"class.std::__2::__tree"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__tree"*, align 4
  store %"class.std::__2::__tree"* %this, %"class.std::__2::__tree"** %this.addr, align 4
  %this1 = load %"class.std::__2::__tree"*, %"class.std::__2::__tree"** %this.addr, align 4
  %__pair1_ = getelementptr inbounds %"class.std::__2::__tree", %"class.std::__2::__tree"* %this1, i32 0, i32 1
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_end_node"* @_ZNKSt3__217__compressed_pairINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEENS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS7_IcEEEESE_EES3_EEEEE5firstEv(%"class.std::__2::__compressed_pair.1"* %__pair1_) #6
  %call2 = call %"class.std::__2::__tree_end_node"* @_ZNSt3__214pointer_traitsIPNS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEEE10pointer_toERS6_(%"class.std::__2::__tree_end_node"* nonnull align 4 dereferenceable(4) %call) #6
  ret %"class.std::__2::__tree_end_node"* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_end_node"* @_ZNKSt3__217__compressed_pairINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEENS_9allocatorINS_11__tree_nodeINS_12__value_typeINS_12basic_stringIcNS_11char_traitsIcEENS7_IcEEEESE_EES3_EEEEE5firstEv(%"class.std::__2::__compressed_pair.1"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.1"*, align 4
  store %"class.std::__2::__compressed_pair.1"* %this, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.1"*, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.1"* %this1 to %"struct.std::__2::__compressed_pair_elem.2"*
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::__tree_end_node"* @_ZNKSt3__222__compressed_pair_elemINS_15__tree_end_nodeIPNS_16__tree_node_baseIPvEEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.2"* %0) #6
  ret %"class.std::__2::__tree_end_node"* %call
}

; Function Attrs: nounwind
declare %"class.draco::DecoderBuffer::BitDecoder"* @_ZN5draco13DecoderBuffer10BitDecoderD1Ev(%"class.draco::DecoderBuffer::BitDecoder"* returned) unnamed_addr #4

; Function Attrs: noinline nounwind optnone
define internal %"class.std::__2::__compressed_pair"* @_ZNSt3__217__compressed_pairIPcN12_GLOBAL__N_111FreeDeleterEEC2IRS1_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair"* returned %this, i8** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  %__t1.addr = alloca i8**, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  store i8** %__t1, i8*** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %1 = load i8**, i8*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIRPcEEOT_RNS_16remove_referenceIS3_E4typeE(i8** nonnull align 4 dereferenceable(4) %1) #6
  %call2 = call %"struct.std::__2::__compressed_pair_elem"* @_ZNSt3__222__compressed_pair_elemIPcLi0ELb0EEC2IRS1_vEEOT_(%"struct.std::__2::__compressed_pair_elem"* %0, i8** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem.0"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #6
  %call4 = call %"struct.std::__2::__compressed_pair_elem.0"* @_ZNSt3__222__compressed_pair_elemIN12_GLOBAL__N_111FreeDeleterELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.0"* %2)
  ret %"class.std::__2::__compressed_pair"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIRPcEEOT_RNS_16remove_referenceIS3_E4typeE(i8** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca i8**, align 4
  store i8** %__t, i8*** %__t.addr, align 4
  %0 = load i8**, i8*** %__t.addr, align 4
  ret i8** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem"* @_ZNSt3__222__compressed_pair_elemIPcLi0ELb0EEC2IRS1_vEEOT_(%"struct.std::__2::__compressed_pair_elem"* returned %this, i8** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem"*, align 4
  %__u.addr = alloca i8**, align 4
  store %"struct.std::__2::__compressed_pair_elem"* %this, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  store i8** %__u, i8*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem"*, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem"* %this1, i32 0, i32 0
  %0 = load i8**, i8*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIRPcEEOT_RNS_16remove_referenceIS3_E4typeE(i8** nonnull align 4 dereferenceable(4) %0) #6
  %1 = load i8*, i8** %call, align 4
  store i8* %1, i8** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  store %"struct.std::__2::__default_init_tag"* %__t, %"struct.std::__2::__default_init_tag"** %__t.addr, align 4
  %0 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t.addr, align 4
  ret %"struct.std::__2::__default_init_tag"* %0
}

; Function Attrs: noinline nounwind optnone
define internal %"struct.std::__2::__compressed_pair_elem.0"* @_ZNSt3__222__compressed_pair_elemIN12_GLOBAL__N_111FreeDeleterELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.0"* returned %this) unnamed_addr #0 {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.0"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.0"* %this, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.0"*, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.0"* %this1 to %"struct.(anonymous namespace)::FreeDeleter"*
  ret %"struct.std::__2::__compressed_pair_elem.0"* %this1
}

; Function Attrs: noinline nounwind optnone
define internal void @_ZNSt3__210unique_ptrIA_cN12_GLOBAL__N_111FreeDeleterEE5resetEDn(%"class.std::__2::unique_ptr"* %this, i8* %0) #0 {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  %.addr = alloca i8*, align 4
  %__tmp = alloca i8*, align 4
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__217__compressed_pairIPcN12_GLOBAL__N_111FreeDeleterEE5firstEv(%"class.std::__2::__compressed_pair"* %__ptr_) #6
  %1 = load i8*, i8** %call, align 4
  store i8* %1, i8** %__tmp, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__217__compressed_pairIPcN12_GLOBAL__N_111FreeDeleterEE5firstEv(%"class.std::__2::__compressed_pair"* %__ptr_2) #6
  store i8* null, i8** %call3, align 4
  %2 = load i8*, i8** %__tmp, align 4
  %tobool = icmp ne i8* %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__ptr_4 = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call5 = call nonnull align 1 dereferenceable(1) %"struct.(anonymous namespace)::FreeDeleter"* @_ZNSt3__217__compressed_pairIPcN12_GLOBAL__N_111FreeDeleterEE6secondEv(%"class.std::__2::__compressed_pair"* %__ptr_4) #6
  %3 = load i8*, i8** %__tmp, align 4
  call void @_ZNK12_GLOBAL__N_111FreeDeleterclEPv(%"struct.(anonymous namespace)::FreeDeleter"* %call5, i8* %3)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal nonnull align 4 dereferenceable(4) i8** @_ZNSt3__217__compressed_pairIPcN12_GLOBAL__N_111FreeDeleterEE5firstEv(%"class.std::__2::__compressed_pair"* %this) #0 {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__222__compressed_pair_elemIPcLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %0) #6
  ret i8** %call
}

; Function Attrs: noinline nounwind optnone
define internal nonnull align 1 dereferenceable(1) %"struct.(anonymous namespace)::FreeDeleter"* @_ZNSt3__217__compressed_pairIPcN12_GLOBAL__N_111FreeDeleterEE6secondEv(%"class.std::__2::__compressed_pair"* %this) #0 {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem.0"*
  %call = call nonnull align 1 dereferenceable(1) %"struct.(anonymous namespace)::FreeDeleter"* @_ZNSt3__222__compressed_pair_elemIN12_GLOBAL__N_111FreeDeleterELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.0"* %0) #6
  ret %"struct.(anonymous namespace)::FreeDeleter"* %call
}

; Function Attrs: noinline nounwind optnone
define internal void @_ZNK12_GLOBAL__N_111FreeDeleterclEPv(%"struct.(anonymous namespace)::FreeDeleter"* %this, i8* %p) #0 {
entry:
  %this.addr = alloca %"struct.(anonymous namespace)::FreeDeleter"*, align 4
  %p.addr = alloca i8*, align 4
  store %"struct.(anonymous namespace)::FreeDeleter"* %this, %"struct.(anonymous namespace)::FreeDeleter"** %this.addr, align 4
  store i8* %p, i8** %p.addr, align 4
  %this1 = load %"struct.(anonymous namespace)::FreeDeleter"*, %"struct.(anonymous namespace)::FreeDeleter"** %this.addr, align 4
  %0 = load i8*, i8** %p.addr, align 4
  call void @free(i8* %0)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNSt3__222__compressed_pair_elemIPcLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem"*, align 4
  store %"struct.std::__2::__compressed_pair_elem"* %this, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem"*, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem"* %this1, i32 0, i32 0
  ret i8** %__value_
}

; Function Attrs: noinline nounwind optnone
define internal nonnull align 1 dereferenceable(1) %"struct.(anonymous namespace)::FreeDeleter"* @_ZNSt3__222__compressed_pair_elemIN12_GLOBAL__N_111FreeDeleterELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.0"* %this) #0 {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.0"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.0"* %this, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.0"*, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.0"* %this1 to %"struct.(anonymous namespace)::FreeDeleter"*
  ret %"struct.(anonymous namespace)::FreeDeleter"* %0
}

declare void @free(i8*) #1

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK5draco6Status2okEv(%"class.draco::Status"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::Status"*, align 4
  store %"class.draco::Status"* %this, %"class.draco::Status"** %this.addr, align 4
  %this1 = load %"class.draco::Status"*, %"class.draco::Status"** %this.addr, align 4
  %code_ = getelementptr inbounds %"class.draco::Status", %"class.draco::Status"* %this1, i32 0, i32 0
  %0 = load i32, i32* %code_, align 4
  %cmp = icmp eq i32 %0, 0
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::Mesh"** @_ZNKSt3__217__compressed_pairIPN5draco4MeshENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.24"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.24"*, align 4
  store %"class.std::__2::__compressed_pair.24"* %this, %"class.std::__2::__compressed_pair.24"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.24"*, %"class.std::__2::__compressed_pair.24"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.24"* %this1 to %"struct.std::__2::__compressed_pair_elem.25"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::Mesh"** @_ZNKSt3__222__compressed_pair_elemIPN5draco4MeshELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.25"* %0) #6
  ret %"class.draco::Mesh"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::Mesh"** @_ZNKSt3__222__compressed_pair_elemIPN5draco4MeshELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.25"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.25"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.25"* %this, %"struct.std::__2::__compressed_pair_elem.25"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.25"*, %"struct.std::__2::__compressed_pair_elem.25"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.25", %"struct.std::__2::__compressed_pair_elem.25"* %this1, i32 0, i32 0
  ret %"class.draco::Mesh"** %__value_
}

; Function Attrs: noinline nounwind optnone
define internal %"class.std::__2::__compressed_pair.143"* @_ZNSt3__217__compressed_pairIPjN12_GLOBAL__N_111FreeDeleterEEC2IRS1_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.143"* returned %this, i32** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.143"*, align 4
  %__t1.addr = alloca i32**, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.143"* %this, %"class.std::__2::__compressed_pair.143"** %this.addr, align 4
  store i32** %__t1, i32*** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.143"*, %"class.std::__2::__compressed_pair.143"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.143"* %this1 to %"struct.std::__2::__compressed_pair_elem.144"*
  %1 = load i32**, i32*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__27forwardIRPjEEOT_RNS_16remove_referenceIS3_E4typeE(i32** nonnull align 4 dereferenceable(4) %1) #6
  %call2 = call %"struct.std::__2::__compressed_pair_elem.144"* @_ZNSt3__222__compressed_pair_elemIPjLi0ELb0EEC2IRS1_vEEOT_(%"struct.std::__2::__compressed_pair_elem.144"* %0, i32** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.143"* %this1 to %"struct.std::__2::__compressed_pair_elem.0"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #6
  %call4 = call %"struct.std::__2::__compressed_pair_elem.0"* @_ZNSt3__222__compressed_pair_elemIN12_GLOBAL__N_111FreeDeleterELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.0"* %2)
  ret %"class.std::__2::__compressed_pair.143"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNSt3__27forwardIRPjEEOT_RNS_16remove_referenceIS3_E4typeE(i32** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca i32**, align 4
  store i32** %__t, i32*** %__t.addr, align 4
  %0 = load i32**, i32*** %__t.addr, align 4
  ret i32** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.144"* @_ZNSt3__222__compressed_pair_elemIPjLi0ELb0EEC2IRS1_vEEOT_(%"struct.std::__2::__compressed_pair_elem.144"* returned %this, i32** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.144"*, align 4
  %__u.addr = alloca i32**, align 4
  store %"struct.std::__2::__compressed_pair_elem.144"* %this, %"struct.std::__2::__compressed_pair_elem.144"** %this.addr, align 4
  store i32** %__u, i32*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.144"*, %"struct.std::__2::__compressed_pair_elem.144"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.144", %"struct.std::__2::__compressed_pair_elem.144"* %this1, i32 0, i32 0
  %0 = load i32**, i32*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__27forwardIRPjEEOT_RNS_16remove_referenceIS3_E4typeE(i32** nonnull align 4 dereferenceable(4) %0) #6
  %1 = load i32*, i32** %call, align 4
  store i32* %1, i32** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.144"* %this1
}

; Function Attrs: noinline nounwind optnone
define internal void @_ZNSt3__210unique_ptrIA_jN12_GLOBAL__N_111FreeDeleterEE5resetEDn(%"class.std::__2::unique_ptr.142"* %this, i8* %0) #0 {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.142"*, align 4
  %.addr = alloca i8*, align 4
  %__tmp = alloca i32*, align 4
  store %"class.std::__2::unique_ptr.142"* %this, %"class.std::__2::unique_ptr.142"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.142"*, %"class.std::__2::unique_ptr.142"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.142", %"class.std::__2::unique_ptr.142"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__217__compressed_pairIPjN12_GLOBAL__N_111FreeDeleterEE5firstEv(%"class.std::__2::__compressed_pair.143"* %__ptr_) #6
  %1 = load i32*, i32** %call, align 4
  store i32* %1, i32** %__tmp, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr.142", %"class.std::__2::unique_ptr.142"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__217__compressed_pairIPjN12_GLOBAL__N_111FreeDeleterEE5firstEv(%"class.std::__2::__compressed_pair.143"* %__ptr_2) #6
  store i32* null, i32** %call3, align 4
  %2 = load i32*, i32** %__tmp, align 4
  %tobool = icmp ne i32* %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__ptr_4 = getelementptr inbounds %"class.std::__2::unique_ptr.142", %"class.std::__2::unique_ptr.142"* %this1, i32 0, i32 0
  %call5 = call nonnull align 1 dereferenceable(1) %"struct.(anonymous namespace)::FreeDeleter"* @_ZNSt3__217__compressed_pairIPjN12_GLOBAL__N_111FreeDeleterEE6secondEv(%"class.std::__2::__compressed_pair.143"* %__ptr_4) #6
  %3 = load i32*, i32** %__tmp, align 4
  %4 = bitcast i32* %3 to i8*
  call void @_ZNK12_GLOBAL__N_111FreeDeleterclEPv(%"struct.(anonymous namespace)::FreeDeleter"* %call5, i8* %4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal nonnull align 4 dereferenceable(4) i32** @_ZNSt3__217__compressed_pairIPjN12_GLOBAL__N_111FreeDeleterEE5firstEv(%"class.std::__2::__compressed_pair.143"* %this) #0 {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.143"*, align 4
  store %"class.std::__2::__compressed_pair.143"* %this, %"class.std::__2::__compressed_pair.143"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.143"*, %"class.std::__2::__compressed_pair.143"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.143"* %this1 to %"struct.std::__2::__compressed_pair_elem.144"*
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__222__compressed_pair_elemIPjLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.144"* %0) #6
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define internal nonnull align 1 dereferenceable(1) %"struct.(anonymous namespace)::FreeDeleter"* @_ZNSt3__217__compressed_pairIPjN12_GLOBAL__N_111FreeDeleterEE6secondEv(%"class.std::__2::__compressed_pair.143"* %this) #0 {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.143"*, align 4
  store %"class.std::__2::__compressed_pair.143"* %this, %"class.std::__2::__compressed_pair.143"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.143"*, %"class.std::__2::__compressed_pair.143"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.143"* %this1 to %"struct.std::__2::__compressed_pair_elem.0"*
  %call = call nonnull align 1 dereferenceable(1) %"struct.(anonymous namespace)::FreeDeleter"* @_ZNSt3__222__compressed_pair_elemIN12_GLOBAL__N_111FreeDeleterELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.0"* %0) #6
  ret %"struct.(anonymous namespace)::FreeDeleter"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNSt3__222__compressed_pair_elemIPjLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.144"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.144"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.144"* %this, %"struct.std::__2::__compressed_pair_elem.144"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.144"*, %"struct.std::__2::__compressed_pair_elem.144"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.144", %"struct.std::__2::__compressed_pair_elem.144"* %this1, i32 0, i32 0
  ret i32** %__value_
}

; Function Attrs: noinline nounwind optnone
define internal nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__217__compressed_pairIPjN12_GLOBAL__N_111FreeDeleterEE5firstEv(%"class.std::__2::__compressed_pair.143"* %this) #0 {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.143"*, align 4
  store %"class.std::__2::__compressed_pair.143"* %this, %"class.std::__2::__compressed_pair.143"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.143"*, %"class.std::__2::__compressed_pair.143"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.143"* %this1 to %"struct.std::__2::__compressed_pair_elem.144"*
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__222__compressed_pair_elemIPjLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.144"* %0) #6
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__222__compressed_pair_elemIPjLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.144"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.144"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.144"* %this, %"struct.std::__2::__compressed_pair_elem.144"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.144"*, %"struct.std::__2::__compressed_pair_elem.144"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.144", %"struct.std::__2::__compressed_pair_elem.144"* %this1, i32 0, i32 0
  ret i32** %__value_
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn writeonly }
attributes #3 = { argmemonly nounwind willreturn }
attributes #4 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nounwind }
attributes #7 = { builtin nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
