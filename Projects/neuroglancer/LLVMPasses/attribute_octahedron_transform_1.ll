; ModuleID = './draco/src/draco/attributes/attribute_octahedron_transform.cc'
source_filename = "./draco/src/draco/attributes/attribute_octahedron_transform.cc"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%"class.draco::AttributeOctahedronTransform" = type { %"class.draco::AttributeTransform", i32 }
%"class.draco::AttributeTransform" = type { i32 (...)** }
%"class.draco::PointAttribute" = type <{ %"class.draco::GeometryAttribute", %"class.std::__2::unique_ptr", %"class.draco::IndexTypeVector", i32, i8, [3 x i8], %"class.std::__2::unique_ptr.11", [4 x i8] }>
%"class.draco::GeometryAttribute" = type { %"class.draco::DataBuffer"*, %"struct.draco::DataBufferDescriptor", i8, i32, i8, i64, i64, i32, i32 }
%"class.draco::DataBuffer" = type { %"class.std::__2::vector", %"struct.draco::DataBufferDescriptor" }
%"class.std::__2::vector" = type { %"class.std::__2::__vector_base" }
%"class.std::__2::__vector_base" = type { i8*, i8*, %"class.std::__2::__compressed_pair" }
%"class.std::__2::__compressed_pair" = type { %"struct.std::__2::__compressed_pair_elem" }
%"struct.std::__2::__compressed_pair_elem" = type { i8* }
%"struct.draco::DataBufferDescriptor" = type { i64, i64 }
%"class.std::__2::unique_ptr" = type { %"class.std::__2::__compressed_pair.1" }
%"class.std::__2::__compressed_pair.1" = type { %"struct.std::__2::__compressed_pair_elem.2" }
%"struct.std::__2::__compressed_pair_elem.2" = type { %"class.draco::DataBuffer"* }
%"class.draco::IndexTypeVector" = type { %"class.std::__2::vector.4" }
%"class.std::__2::vector.4" = type { %"class.std::__2::__vector_base.5" }
%"class.std::__2::__vector_base.5" = type { %"class.draco::IndexType"*, %"class.draco::IndexType"*, %"class.std::__2::__compressed_pair.6" }
%"class.draco::IndexType" = type { i32 }
%"class.std::__2::__compressed_pair.6" = type { %"struct.std::__2::__compressed_pair_elem.7" }
%"struct.std::__2::__compressed_pair_elem.7" = type { %"class.draco::IndexType"* }
%"class.std::__2::unique_ptr.11" = type { %"class.std::__2::__compressed_pair.12" }
%"class.std::__2::__compressed_pair.12" = type { %"struct.std::__2::__compressed_pair_elem.13" }
%"struct.std::__2::__compressed_pair_elem.13" = type { %"class.draco::AttributeTransformData"* }
%"class.draco::AttributeTransformData" = type { i32, %"class.draco::DataBuffer" }
%"class.draco::EncoderBuffer" = type <{ %"class.std::__2::vector.16", %"class.std::__2::unique_ptr.23", i64, i8, [7 x i8] }>
%"class.std::__2::vector.16" = type { %"class.std::__2::__vector_base.17" }
%"class.std::__2::__vector_base.17" = type { i8*, i8*, %"class.std::__2::__compressed_pair.18" }
%"class.std::__2::__compressed_pair.18" = type { %"struct.std::__2::__compressed_pair_elem.19" }
%"struct.std::__2::__compressed_pair_elem.19" = type { i8* }
%"class.std::__2::unique_ptr.23" = type { %"class.std::__2::__compressed_pair.24" }
%"class.std::__2::__compressed_pair.24" = type { %"struct.std::__2::__compressed_pair_elem.25" }
%"struct.std::__2::__compressed_pair_elem.25" = type { %"class.draco::EncoderBuffer::BitEncoder"* }
%"class.draco::EncoderBuffer::BitEncoder" = type { i8*, i32 }
%"class.std::__2::__wrap_iter" = type { i8* }
%"class.std::__2::__wrap_iter.41" = type { i8* }
%"class.std::__2::unique_ptr.36" = type { %"class.std::__2::__compressed_pair.37" }
%"class.std::__2::__compressed_pair.37" = type { %"struct.std::__2::__compressed_pair_elem.38" }
%"struct.std::__2::__compressed_pair_elem.38" = type { %"class.draco::PointAttribute"* }
%"class.std::__2::vector.28" = type { %"class.std::__2::__vector_base.29" }
%"class.std::__2::__vector_base.29" = type { %"class.draco::IndexType.30"*, %"class.draco::IndexType.30"*, %"class.std::__2::__compressed_pair.31" }
%"class.draco::IndexType.30" = type { i32 }
%"class.std::__2::__compressed_pair.31" = type { %"struct.std::__2::__compressed_pair_elem.32" }
%"struct.std::__2::__compressed_pair_elem.32" = type { %"class.draco::IndexType.30"* }
%"class.draco::OctahedronToolBox" = type { i32, i32, i32, i32 }
%"struct.std::__2::__default_init_tag" = type { i8 }
%"struct.std::__2::default_delete.40" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.39" = type { i8 }
%"class.std::__2::allocator.21" = type { i8 }
%"struct.std::__2::__split_buffer" = type { i8*, i8*, i8*, %"class.std::__2::__compressed_pair.42" }
%"class.std::__2::__compressed_pair.42" = type { %"struct.std::__2::__compressed_pair_elem.19", %"struct.std::__2::__compressed_pair_elem.43" }
%"struct.std::__2::__compressed_pair_elem.43" = type { %"class.std::__2::allocator.21"* }
%"struct.std::__2::random_access_iterator_tag" = type { i8 }
%"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction" = type { %"class.std::__2::vector.16"*, i8*, i8* }
%"class.std::__2::__vector_base_common" = type { i8 }
%"class.std::__2::__split_buffer_common" = type { i8 }
%"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction" = type { i8*, i8*, i8** }
%"struct.std::__2::integral_constant" = type { i8 }
%"struct.std::__2::__has_construct" = type { i8 }
%"struct.std::__2::__has_construct.44" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.20" = type { i8 }
%"struct.std::__2::__less" = type { i8 }
%"struct.std::__2::__has_max_size" = type { i8 }
%"struct.std::__2::integral_constant.45" = type { i8 }
%"struct.std::__2::__has_destroy" = type { i8 }
%"struct.std::__2::default_delete.15" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.14" = type { i8 }
%"class.std::__2::allocator" = type { i8 }
%"struct.std::__2::__has_destroy.46" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.0" = type { i8 }
%"class.std::__2::allocator.9" = type { i8 }
%"struct.std::__2::__has_destroy.47" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.8" = type { i8 }
%"struct.std::__2::default_delete" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.3" = type { i8 }

$_ZNK5draco14PointAttribute25GetAttributeTransformDataEv = comdat any

$_ZNK5draco22AttributeTransformData14transform_typeEv = comdat any

$_ZNK5draco22AttributeTransformData17GetParameterValueIiEET_i = comdat any

$_ZN5draco22AttributeTransformData18set_transform_typeENS_22AttributeTransformTypeE = comdat any

$_ZN5draco22AttributeTransformData20AppendParameterValueIiEEvRKT_ = comdat any

$_ZNK5draco28AttributeOctahedronTransform14is_initializedEv = comdat any

$_ZN5draco13EncoderBuffer6EncodeIhEEbRKT_ = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv = comdat any

$_ZNKSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEEptEv = comdat any

$_ZN5draco17GeometryAttribute10GetAddressENS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEE = comdat any

$_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEC2Ej = comdat any

$_ZN5draco17OctahedronToolBoxC2Ev = comdat any

$_ZN5draco17OctahedronToolBox19SetQuantizationBitsEi = comdat any

$_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEEC2ILb1EvEEDn = comdat any

$_ZNK5draco14PointAttribute12mapped_indexENS_9IndexTypeIjNS_20PointIndex_tag_type_EEE = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEEixEm = comdat any

$_ZNK5draco17GeometryAttribute8GetValueENS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEEPv = comdat any

$_ZNK5draco17OctahedronToolBox38FloatVectorToQuantizedOctahedralCoordsIfEEvPKT_PiS5_ = comdat any

$_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEEC2EOS5_ = comdat any

$_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEED2Ev = comdat any

$_ZN5draco28AttributeOctahedronTransformD2Ev = comdat any

$_ZN5draco28AttributeOctahedronTransformD0Ev = comdat any

$_ZNK5draco28AttributeOctahedronTransform4TypeEv = comdat any

$_ZNKSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE3getEv = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPN5draco22AttributeTransformDataELi0ELb0EE5__getEv = comdat any

$_ZNK5draco17GeometryAttribute10GetBytePosENS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEE = comdat any

$_ZN5draco10DataBuffer4dataEv = comdat any

$_ZNK5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EE5valueEv = comdat any

$_ZNSt3__26vectorIhNS_9allocatorIhEEEixEm = comdat any

$_ZNSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEEC2IS3_NS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__27forwardIPN5draco14PointAttributeEEEOT_RNS_16remove_referenceIS4_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco14PointAttributeELi0ELb0EEC2IS3_vEEOT_ = comdat any

$_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco14PointAttributeEEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNK5draco9IndexTypeIjNS_20PointIndex_tag_type_EE5valueEv = comdat any

$_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEEEixERKS3_ = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEEixEm = comdat any

$_ZNK5draco10DataBuffer4ReadExPvm = comdat any

$_ZNK5draco10DataBuffer4dataEv = comdat any

$_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv = comdat any

$_ZNSt3__212__to_addressIhEEPT_S2_ = comdat any

$_ZN5draco18AttributeTransformD2Ev = comdat any

$_ZN5draco22AttributeTransformData17SetParameterValueIiEEviRKT_ = comdat any

$_ZNK5draco10DataBuffer9data_sizeEv = comdat any

$_ZN5draco10DataBuffer5WriteExPKvm = comdat any

$_ZNKSt3__26vectorIhNS_9allocatorIhEEE4sizeEv = comdat any

$_ZNK5draco13EncoderBuffer18bit_encoder_activeEv = comdat any

$_ZNSt3__26vectorIcNS_9allocatorIcEEE6insertIPKhEENS_9enable_ifIXaasr27__is_cpp17_forward_iteratorIT_EE5valuesr16is_constructibleIcNS_15iterator_traitsIS8_E9referenceEEE5valueENS_11__wrap_iterIPcEEE4typeENSC_IPKcEES8_S8_ = comdat any

$_ZNSt3__26vectorIcNS_9allocatorIcEEE3endEv = comdat any

$_ZNSt3__211__wrap_iterIPKcEC2IPcEERKNS0_IT_EEPNS_9enable_ifIXsr14is_convertibleIS6_S2_EE5valueEvE4typeE = comdat any

$_ZNSt3__2miIPKcPcEEDTmicldtfp_4baseEcldtfp0_4baseEERKNS_11__wrap_iterIT_EERKNS5_IT0_EE = comdat any

$_ZNSt3__26vectorIcNS_9allocatorIcEEE5beginEv = comdat any

$_ZNSt3__28distanceIPKhEENS_15iterator_traitsIT_E15difference_typeES4_S4_ = comdat any

$_ZNSt3__213__vector_baseIcNS_9allocatorIcEEE9__end_capEv = comdat any

$_ZNSt3__27advanceIPKhEEvRT_NS_15iterator_traitsIS3_E15difference_typeE = comdat any

$_ZNSt3__26vectorIcNS_9allocatorIcEEE18__construct_at_endIPKhEENS_9enable_ifIXsr27__is_cpp17_forward_iteratorIT_EE5valueEvE4typeES8_S8_m = comdat any

$_ZNSt3__26vectorIcNS_9allocatorIcEEE12__move_rangeEPcS4_S4_ = comdat any

$_ZNSt3__24copyIPKhPcEET0_T_S5_S4_ = comdat any

$_ZNSt3__213__vector_baseIcNS_9allocatorIcEEE7__allocEv = comdat any

$_ZNKSt3__26vectorIcNS_9allocatorIcEEE11__recommendEm = comdat any

$_ZNKSt3__26vectorIcNS_9allocatorIcEEE4sizeEv = comdat any

$_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEEC2EmmS3_ = comdat any

$_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE18__construct_at_endIPKhEENS_9enable_ifIXsr27__is_cpp17_forward_iteratorIT_EE5valueEvE4typeES9_S9_ = comdat any

$_ZNSt3__26vectorIcNS_9allocatorIcEEE26__swap_out_circular_bufferERNS_14__split_bufferIcRS2_EEPc = comdat any

$_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEED2Ev = comdat any

$_ZNSt3__26vectorIcNS_9allocatorIcEEE11__make_iterEPc = comdat any

$_ZNKSt3__211__wrap_iterIPKcE4baseEv = comdat any

$_ZNKSt3__211__wrap_iterIPcE4baseEv = comdat any

$_ZNSt3__210__distanceIPKhEENS_15iterator_traitsIT_E15difference_typeES4_S4_NS_26random_access_iterator_tagE = comdat any

$_ZNSt3__217__compressed_pairIPcNS_9allocatorIcEEE5firstEv = comdat any

$_ZNSt3__222__compressed_pair_elemIPcLi0ELb0EE5__getEv = comdat any

$_ZNSt3__29__advanceIPKhEEvRT_NS_15iterator_traitsIS3_E15difference_typeENS_26random_access_iterator_tagE = comdat any

$_ZNSt3__26vectorIcNS_9allocatorIcEEE21_ConstructTransactionC2ERS3_m = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIcEEE25__construct_range_forwardIPKhPcEEvRS2_T_S9_RT0_ = comdat any

$_ZNSt3__26vectorIcNS_9allocatorIcEEE21_ConstructTransactionD2Ev = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIcEEE9constructIcJRKhEEEvRS2_PT_DpOT0_ = comdat any

$_ZNSt3__212__to_addressIcEEPT_S2_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIcEEE11__constructIcJRKhEEEvNS_17integral_constantIbLb1EEERS2_PT_DpOT0_ = comdat any

$_ZNSt3__27forwardIRKhEEOT_RNS_16remove_referenceIS3_E4typeE = comdat any

$_ZNSt3__29allocatorIcE9constructIcJRKhEEEvPT_DpOT0_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIcEEE9constructIcJcEEEvRS2_PT_DpOT0_ = comdat any

$_ZNSt3__24moveIRcEEONS_16remove_referenceIT_E4typeEOS3_ = comdat any

$_ZNSt3__213move_backwardIPcS1_EET0_T_S3_S2_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIcEEE11__constructIcJcEEEvNS_17integral_constantIbLb1EEERS2_PT_DpOT0_ = comdat any

$_ZNSt3__27forwardIcEEOT_RNS_16remove_referenceIS1_E4typeE = comdat any

$_ZNSt3__29allocatorIcE9constructIcJcEEEvPT_DpOT0_ = comdat any

$_ZNSt3__215__move_backwardIccEENS_9enable_ifIXaasr7is_sameINS_12remove_constIT_E4typeET0_EE5valuesr28is_trivially_copy_assignableIS6_EE5valueEPS6_E4typeEPS3_SA_S7_ = comdat any

$_ZNSt3__213__unwrap_iterIPcEET_S2_ = comdat any

$_ZNSt3__26__copyIPKhPcEET0_T_S5_S4_ = comdat any

$_ZNSt3__213__unwrap_iterIPKhEET_S3_ = comdat any

$_ZNSt3__216__copy_constexprIPKhPcEET0_T_S5_S4_ = comdat any

$_ZNSt3__217__compressed_pairIPcNS_9allocatorIcEEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIcEELi1ELb1EE5__getEv = comdat any

$_ZNKSt3__26vectorIcNS_9allocatorIcEEE8max_sizeEv = comdat any

$_ZNKSt3__26vectorIcNS_9allocatorIcEEE8capacityEv = comdat any

$_ZNSt3__23maxImEERKT_S3_S3_ = comdat any

$_ZNSt3__23minImEERKT_S3_S3_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIcEEE8max_sizeERKS2_ = comdat any

$_ZNKSt3__213__vector_baseIcNS_9allocatorIcEEE7__allocEv = comdat any

$_ZNSt3__214numeric_limitsIlE3maxEv = comdat any

$_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_ = comdat any

$_ZNKSt3__26__lessImmEclERKmS3_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIcEEE10__max_sizeENS_17integral_constantIbLb1EEERKS2_ = comdat any

$_ZNKSt3__29allocatorIcE8max_sizeEv = comdat any

$_ZNKSt3__217__compressed_pairIPcNS_9allocatorIcEEE6secondEv = comdat any

$_ZNKSt3__222__compressed_pair_elemINS_9allocatorIcEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv = comdat any

$_ZNKSt3__213__vector_baseIcNS_9allocatorIcEEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseIcNS_9allocatorIcEEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPcNS_9allocatorIcEEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPcLi0ELb0EE5__getEv = comdat any

$_ZNSt3__23maxImNS_6__lessImmEEEERKT_S5_S5_T0_ = comdat any

$_ZNSt3__217__compressed_pairIPcRNS_9allocatorIcEEEC2IDnS4_EEOT_OT0_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIcEEE8allocateERS2_m = comdat any

$_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE7__allocEv = comdat any

$_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE9__end_capEv = comdat any

$_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPcLi0ELb0EEC2IDnvEEOT_ = comdat any

$_ZNSt3__27forwardIRNS_9allocatorIcEEEEOT_RNS_16remove_referenceIS4_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIcEELi1ELb0EEC2IS3_vEEOT_ = comdat any

$_ZNSt3__29allocatorIcE8allocateEmPKv = comdat any

$_ZNSt3__220__throw_length_errorEPKc = comdat any

$_ZNSt3__217__libcpp_allocateEmm = comdat any

$_ZNSt3__217__compressed_pairIPcRNS_9allocatorIcEEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIcEELi1ELb0EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPcRNS_9allocatorIcEEE5firstEv = comdat any

$_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE21_ConstructTransactionC2EPPcm = comdat any

$_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE21_ConstructTransactionD2Ev = comdat any

$_ZNKSt3__26vectorIcNS_9allocatorIcEEE17__annotate_deleteEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIcEEE46__construct_backward_with_exception_guaranteesIcEENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS2_PT_S8_EE5valuesr31is_trivially_move_constructibleIS8_EE5valueEvE4typeERS2_S9_S9_RS9_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIcEEE45__construct_forward_with_exception_guaranteesIcEENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS2_PT_S8_EE5valuesr31is_trivially_move_constructibleIS8_EE5valueEvE4typeERS2_S9_S9_RS9_ = comdat any

$_ZNSt3__24swapIPcEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_ = comdat any

$_ZNKSt3__26vectorIcNS_9allocatorIcEEE14__annotate_newEm = comdat any

$_ZNSt3__26vectorIcNS_9allocatorIcEEE26__invalidate_all_iteratorsEv = comdat any

$_ZNKSt3__26vectorIcNS_9allocatorIcEEE31__annotate_contiguous_containerEPKvS5_S5_S5_ = comdat any

$_ZNKSt3__26vectorIcNS_9allocatorIcEEE4dataEv = comdat any

$_ZNSt3__24moveIRPcEEONS_16remove_referenceIT_E4typeEOS4_ = comdat any

$_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE5clearEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIcEEE10deallocateERS2_Pcm = comdat any

$_ZNKSt3__214__split_bufferIcRNS_9allocatorIcEEE8capacityEv = comdat any

$_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE17__destruct_at_endEPc = comdat any

$_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE17__destruct_at_endEPcNS_17integral_constantIbLb0EEE = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIcEEE7destroyIcEEvRS2_PT_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIcEEE9__destroyIcEEvNS_17integral_constantIbLb1EEERS2_PT_ = comdat any

$_ZNSt3__29allocatorIcE7destroyEPc = comdat any

$_ZNSt3__29allocatorIcE10deallocateEPcm = comdat any

$_ZNSt3__219__libcpp_deallocateEPvmm = comdat any

$_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm = comdat any

$_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm = comdat any

$_ZNSt3__217_DeallocateCaller9__do_callEPv = comdat any

$_ZNKSt3__214__split_bufferIcRNS_9allocatorIcEEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPcRNS_9allocatorIcEEE5firstEv = comdat any

$_ZNSt3__211__wrap_iterIPcEC2ES1_ = comdat any

$_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEE5resetEPS2_ = comdat any

$_ZNSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE6secondEv = comdat any

$_ZNKSt3__214default_deleteIN5draco14PointAttributeEEclEPS2_ = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco14PointAttributeELi0ELb0EE5__getEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco14PointAttributeEEELi1ELb1EE5__getEv = comdat any

$_ZN5draco14PointAttributeD2Ev = comdat any

$_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEED2Ev = comdat any

$_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEEED2Ev = comdat any

$_ZNSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEED2Ev = comdat any

$_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE5resetEPS2_ = comdat any

$_ZNSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE6secondEv = comdat any

$_ZNKSt3__214default_deleteIN5draco22AttributeTransformDataEEclEPS2_ = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco22AttributeTransformDataELi0ELb0EE5__getEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco22AttributeTransformDataEEELi1ELb1EE5__getEv = comdat any

$_ZN5draco22AttributeTransformDataD2Ev = comdat any

$_ZN5draco10DataBufferD2Ev = comdat any

$_ZNSt3__26vectorIhNS_9allocatorIhEEED2Ev = comdat any

$_ZNKSt3__26vectorIhNS_9allocatorIhEEE17__annotate_deleteEv = comdat any

$_ZNSt3__213__vector_baseIhNS_9allocatorIhEEED2Ev = comdat any

$_ZNKSt3__26vectorIhNS_9allocatorIhEEE31__annotate_contiguous_containerEPKvS5_S5_S5_ = comdat any

$_ZNKSt3__26vectorIhNS_9allocatorIhEEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPhNS_9allocatorIhEEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPhLi0ELb0EE5__getEv = comdat any

$_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE5clearEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIhEEE10deallocateERS2_Phm = comdat any

$_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE7__allocEv = comdat any

$_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE17__destruct_at_endEPh = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIhEEE7destroyIhEEvRS2_PT_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIhEEE9__destroyIhEEvNS_17integral_constantIbLb1EEERS2_PT_ = comdat any

$_ZNSt3__29allocatorIhE7destroyEPh = comdat any

$_ZNSt3__29allocatorIhE10deallocateEPhm = comdat any

$_ZNSt3__217__compressed_pairIPhNS_9allocatorIhEEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIhEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEED2Ev = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_deleteEv = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEED2Ev = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_ = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv = comdat any

$_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEPT_S6_ = comdat any

$_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5clearEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE10deallocateERS6_PS5_m = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE7destroyIS5_EEvRS6_PT_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE9__destroyIS5_EEvNS_17integral_constantIbLb1EEERS6_PT_ = comdat any

$_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE7destroyEPS4_ = comdat any

$_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE10deallocateEPS4_m = comdat any

$_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEE5resetEPS2_ = comdat any

$_ZNSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEE6secondEv = comdat any

$_ZNKSt3__214default_deleteIN5draco10DataBufferEEclEPS2_ = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco10DataBufferELi0ELb0EE5__getEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco10DataBufferEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEE7releaseEv = comdat any

$_ZNSt3__27forwardINS_14default_deleteIN5draco14PointAttributeEEEEEOT_RNS_16remove_referenceIS5_E4typeE = comdat any

$_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEE11get_deleterEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEEC2IS3_S5_EEOT_OT0_ = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco14PointAttributeEEELi1ELb1EEC2IS4_vEEOT_ = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPN5draco14PointAttributeELi0ELb0EE5__getEv = comdat any

$_Z3absd = comdat any

$_ZNK5draco17OctahedronToolBox40IntegerVectorToQuantizedOctahedralCoordsEPKiPiS3_ = comdat any

$_ZNK5draco17OctahedronToolBox28CanonicalizeOctahedralCoordsEiiPiS1_ = comdat any

@_ZTVN5draco28AttributeOctahedronTransformE = hidden unnamed_addr constant { [7 x i8*] } { [7 x i8*] [i8* null, i8* null, i8* bitcast (%"class.draco::AttributeOctahedronTransform"* (%"class.draco::AttributeOctahedronTransform"*)* @_ZN5draco28AttributeOctahedronTransformD2Ev to i8*), i8* bitcast (void (%"class.draco::AttributeOctahedronTransform"*)* @_ZN5draco28AttributeOctahedronTransformD0Ev to i8*), i8* bitcast (i32 (%"class.draco::AttributeOctahedronTransform"*)* @_ZNK5draco28AttributeOctahedronTransform4TypeEv to i8*), i8* bitcast (i1 (%"class.draco::AttributeOctahedronTransform"*, %"class.draco::PointAttribute"*)* @_ZN5draco28AttributeOctahedronTransform17InitFromAttributeERKNS_14PointAttributeE to i8*), i8* bitcast (void (%"class.draco::AttributeOctahedronTransform"*, %"class.draco::AttributeTransformData"*)* @_ZNK5draco28AttributeOctahedronTransform28CopyToAttributeTransformDataEPNS_22AttributeTransformDataE to i8*)] }, align 4
@.str = private unnamed_addr constant [68 x i8] c"allocator<T>::allocate(size_t n) 'n' exceeds maximum supported size\00", align 1

; Function Attrs: noinline nounwind optnone
define hidden zeroext i1 @_ZN5draco28AttributeOctahedronTransform17InitFromAttributeERKNS_14PointAttributeE(%"class.draco::AttributeOctahedronTransform"* %this, %"class.draco::PointAttribute"* nonnull align 8 dereferenceable(92) %attribute) unnamed_addr #0 {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::AttributeOctahedronTransform"*, align 4
  %attribute.addr = alloca %"class.draco::PointAttribute"*, align 4
  %transform_data = alloca %"class.draco::AttributeTransformData"*, align 4
  store %"class.draco::AttributeOctahedronTransform"* %this, %"class.draco::AttributeOctahedronTransform"** %this.addr, align 4
  store %"class.draco::PointAttribute"* %attribute, %"class.draco::PointAttribute"** %attribute.addr, align 4
  %this1 = load %"class.draco::AttributeOctahedronTransform"*, %"class.draco::AttributeOctahedronTransform"** %this.addr, align 4
  %0 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %attribute.addr, align 4
  %call = call %"class.draco::AttributeTransformData"* @_ZNK5draco14PointAttribute25GetAttributeTransformDataEv(%"class.draco::PointAttribute"* %0)
  store %"class.draco::AttributeTransformData"* %call, %"class.draco::AttributeTransformData"** %transform_data, align 4
  %1 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %transform_data, align 4
  %tobool = icmp ne %"class.draco::AttributeTransformData"* %1, null
  br i1 %tobool, label %lor.lhs.false, label %if.then

lor.lhs.false:                                    ; preds = %entry
  %2 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %transform_data, align 4
  %call2 = call i32 @_ZNK5draco22AttributeTransformData14transform_typeEv(%"class.draco::AttributeTransformData"* %2)
  %cmp = icmp ne i32 %call2, 2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  %3 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %transform_data, align 4
  %call3 = call i32 @_ZNK5draco22AttributeTransformData17GetParameterValueIiEET_i(%"class.draco::AttributeTransformData"* %3, i32 0)
  %quantization_bits_ = getelementptr inbounds %"class.draco::AttributeOctahedronTransform", %"class.draco::AttributeOctahedronTransform"* %this1, i32 0, i32 1
  store i32 %call3, i32* %quantization_bits_, align 4
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %4 = load i1, i1* %retval, align 1
  ret i1 %4
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::AttributeTransformData"* @_ZNK5draco14PointAttribute25GetAttributeTransformDataEv(%"class.draco::PointAttribute"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointAttribute"*, align 4
  store %"class.draco::PointAttribute"* %this, %"class.draco::PointAttribute"** %this.addr, align 4
  %this1 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %this.addr, align 4
  %attribute_transform_data_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 6
  %call = call %"class.draco::AttributeTransformData"* @_ZNKSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE3getEv(%"class.std::__2::unique_ptr.11"* %attribute_transform_data_) #9
  ret %"class.draco::AttributeTransformData"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco22AttributeTransformData14transform_typeEv(%"class.draco::AttributeTransformData"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::AttributeTransformData"*, align 4
  store %"class.draco::AttributeTransformData"* %this, %"class.draco::AttributeTransformData"** %this.addr, align 4
  %this1 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %this.addr, align 4
  %transform_type_ = getelementptr inbounds %"class.draco::AttributeTransformData", %"class.draco::AttributeTransformData"* %this1, i32 0, i32 0
  %0 = load i32, i32* %transform_type_, align 8
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco22AttributeTransformData17GetParameterValueIiEET_i(%"class.draco::AttributeTransformData"* %this, i32 %byte_offset) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::AttributeTransformData"*, align 4
  %byte_offset.addr = alloca i32, align 4
  %out_data = alloca i32, align 4
  store %"class.draco::AttributeTransformData"* %this, %"class.draco::AttributeTransformData"** %this.addr, align 4
  store i32 %byte_offset, i32* %byte_offset.addr, align 4
  %this1 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %this.addr, align 4
  %buffer_ = getelementptr inbounds %"class.draco::AttributeTransformData", %"class.draco::AttributeTransformData"* %this1, i32 0, i32 1
  %0 = load i32, i32* %byte_offset.addr, align 4
  %conv = sext i32 %0 to i64
  %1 = bitcast i32* %out_data to i8*
  call void @_ZNK5draco10DataBuffer4ReadExPvm(%"class.draco::DataBuffer"* %buffer_, i64 %conv, i8* %1, i32 4)
  %2 = load i32, i32* %out_data, align 4
  ret i32 %2
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZNK5draco28AttributeOctahedronTransform28CopyToAttributeTransformDataEPNS_22AttributeTransformDataE(%"class.draco::AttributeOctahedronTransform"* %this, %"class.draco::AttributeTransformData"* %out_data) unnamed_addr #0 {
entry:
  %this.addr = alloca %"class.draco::AttributeOctahedronTransform"*, align 4
  %out_data.addr = alloca %"class.draco::AttributeTransformData"*, align 4
  store %"class.draco::AttributeOctahedronTransform"* %this, %"class.draco::AttributeOctahedronTransform"** %this.addr, align 4
  store %"class.draco::AttributeTransformData"* %out_data, %"class.draco::AttributeTransformData"** %out_data.addr, align 4
  %this1 = load %"class.draco::AttributeOctahedronTransform"*, %"class.draco::AttributeOctahedronTransform"** %this.addr, align 4
  %0 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %out_data.addr, align 4
  call void @_ZN5draco22AttributeTransformData18set_transform_typeENS_22AttributeTransformTypeE(%"class.draco::AttributeTransformData"* %0, i32 2)
  %1 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %out_data.addr, align 4
  %quantization_bits_ = getelementptr inbounds %"class.draco::AttributeOctahedronTransform", %"class.draco::AttributeOctahedronTransform"* %this1, i32 0, i32 1
  call void @_ZN5draco22AttributeTransformData20AppendParameterValueIiEEvRKT_(%"class.draco::AttributeTransformData"* %1, i32* nonnull align 4 dereferenceable(4) %quantization_bits_)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco22AttributeTransformData18set_transform_typeENS_22AttributeTransformTypeE(%"class.draco::AttributeTransformData"* %this, i32 %type) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::AttributeTransformData"*, align 4
  %type.addr = alloca i32, align 4
  store %"class.draco::AttributeTransformData"* %this, %"class.draco::AttributeTransformData"** %this.addr, align 4
  store i32 %type, i32* %type.addr, align 4
  %this1 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %this.addr, align 4
  %0 = load i32, i32* %type.addr, align 4
  %transform_type_ = getelementptr inbounds %"class.draco::AttributeTransformData", %"class.draco::AttributeTransformData"* %this1, i32 0, i32 0
  store i32 %0, i32* %transform_type_, align 8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco22AttributeTransformData20AppendParameterValueIiEEvRKT_(%"class.draco::AttributeTransformData"* %this, i32* nonnull align 4 dereferenceable(4) %in_data) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::AttributeTransformData"*, align 4
  %in_data.addr = alloca i32*, align 4
  store %"class.draco::AttributeTransformData"* %this, %"class.draco::AttributeTransformData"** %this.addr, align 4
  store i32* %in_data, i32** %in_data.addr, align 4
  %this1 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %this.addr, align 4
  %buffer_ = getelementptr inbounds %"class.draco::AttributeTransformData", %"class.draco::AttributeTransformData"* %this1, i32 0, i32 1
  %call = call i32 @_ZNK5draco10DataBuffer9data_sizeEv(%"class.draco::DataBuffer"* %buffer_)
  %0 = load i32*, i32** %in_data.addr, align 4
  call void @_ZN5draco22AttributeTransformData17SetParameterValueIiEEviRKT_(%"class.draco::AttributeTransformData"* %this1, i32 %call, i32* nonnull align 4 dereferenceable(4) %0)
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN5draco28AttributeOctahedronTransform13SetParametersEi(%"class.draco::AttributeOctahedronTransform"* %this, i32 %quantization_bits) #0 {
entry:
  %this.addr = alloca %"class.draco::AttributeOctahedronTransform"*, align 4
  %quantization_bits.addr = alloca i32, align 4
  store %"class.draco::AttributeOctahedronTransform"* %this, %"class.draco::AttributeOctahedronTransform"** %this.addr, align 4
  store i32 %quantization_bits, i32* %quantization_bits.addr, align 4
  %this1 = load %"class.draco::AttributeOctahedronTransform"*, %"class.draco::AttributeOctahedronTransform"** %this.addr, align 4
  %0 = load i32, i32* %quantization_bits.addr, align 4
  %quantization_bits_ = getelementptr inbounds %"class.draco::AttributeOctahedronTransform", %"class.draco::AttributeOctahedronTransform"* %this1, i32 0, i32 1
  store i32 %0, i32* %quantization_bits_, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden zeroext i1 @_ZNK5draco28AttributeOctahedronTransform16EncodeParametersEPNS_13EncoderBufferE(%"class.draco::AttributeOctahedronTransform"* %this, %"class.draco::EncoderBuffer"* %encoder_buffer) #0 {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::AttributeOctahedronTransform"*, align 4
  %encoder_buffer.addr = alloca %"class.draco::EncoderBuffer"*, align 4
  %ref.tmp = alloca i8, align 1
  store %"class.draco::AttributeOctahedronTransform"* %this, %"class.draco::AttributeOctahedronTransform"** %this.addr, align 4
  store %"class.draco::EncoderBuffer"* %encoder_buffer, %"class.draco::EncoderBuffer"** %encoder_buffer.addr, align 4
  %this1 = load %"class.draco::AttributeOctahedronTransform"*, %"class.draco::AttributeOctahedronTransform"** %this.addr, align 4
  %call = call zeroext i1 @_ZNK5draco28AttributeOctahedronTransform14is_initializedEv(%"class.draco::AttributeOctahedronTransform"* %this1)
  br i1 %call, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %0 = load %"class.draco::EncoderBuffer"*, %"class.draco::EncoderBuffer"** %encoder_buffer.addr, align 4
  %quantization_bits_ = getelementptr inbounds %"class.draco::AttributeOctahedronTransform", %"class.draco::AttributeOctahedronTransform"* %this1, i32 0, i32 1
  %1 = load i32, i32* %quantization_bits_, align 4
  %conv = trunc i32 %1 to i8
  store i8 %conv, i8* %ref.tmp, align 1
  %call2 = call zeroext i1 @_ZN5draco13EncoderBuffer6EncodeIhEEbRKT_(%"class.draco::EncoderBuffer"* %0, i8* nonnull align 1 dereferenceable(1) %ref.tmp)
  store i1 true, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %2 = load i1, i1* %retval, align 1
  ret i1 %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK5draco28AttributeOctahedronTransform14is_initializedEv(%"class.draco::AttributeOctahedronTransform"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::AttributeOctahedronTransform"*, align 4
  store %"class.draco::AttributeOctahedronTransform"* %this, %"class.draco::AttributeOctahedronTransform"** %this.addr, align 4
  %this1 = load %"class.draco::AttributeOctahedronTransform"*, %"class.draco::AttributeOctahedronTransform"** %this.addr, align 4
  %quantization_bits_ = getelementptr inbounds %"class.draco::AttributeOctahedronTransform", %"class.draco::AttributeOctahedronTransform"* %this1, i32 0, i32 1
  %0 = load i32, i32* %quantization_bits_, align 4
  %cmp = icmp ne i32 %0, -1
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco13EncoderBuffer6EncodeIhEEbRKT_(%"class.draco::EncoderBuffer"* %this, i8* nonnull align 1 dereferenceable(1) %data) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::EncoderBuffer"*, align 4
  %data.addr = alloca i8*, align 4
  %src_data = alloca i8*, align 4
  %agg.tmp = alloca %"class.std::__2::__wrap_iter", align 4
  %ref.tmp = alloca %"class.std::__2::__wrap_iter.41", align 4
  %coerce = alloca %"class.std::__2::__wrap_iter.41", align 4
  store %"class.draco::EncoderBuffer"* %this, %"class.draco::EncoderBuffer"** %this.addr, align 4
  store i8* %data, i8** %data.addr, align 4
  %this1 = load %"class.draco::EncoderBuffer"*, %"class.draco::EncoderBuffer"** %this.addr, align 4
  %call = call zeroext i1 @_ZNK5draco13EncoderBuffer18bit_encoder_activeEv(%"class.draco::EncoderBuffer"* %this1)
  br i1 %call, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %0 = load i8*, i8** %data.addr, align 4
  store i8* %0, i8** %src_data, align 4
  %buffer_ = getelementptr inbounds %"class.draco::EncoderBuffer", %"class.draco::EncoderBuffer"* %this1, i32 0, i32 0
  %buffer_2 = getelementptr inbounds %"class.draco::EncoderBuffer", %"class.draco::EncoderBuffer"* %this1, i32 0, i32 0
  %call3 = call i8* @_ZNSt3__26vectorIcNS_9allocatorIcEEE3endEv(%"class.std::__2::vector.16"* %buffer_2) #9
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter.41", %"class.std::__2::__wrap_iter.41"* %ref.tmp, i32 0, i32 0
  store i8* %call3, i8** %coerce.dive, align 4
  %call4 = call %"class.std::__2::__wrap_iter"* @_ZNSt3__211__wrap_iterIPKcEC2IPcEERKNS0_IT_EEPNS_9enable_ifIXsr14is_convertibleIS6_S2_EE5valueEvE4typeE(%"class.std::__2::__wrap_iter"* %agg.tmp, %"class.std::__2::__wrap_iter.41"* nonnull align 4 dereferenceable(4) %ref.tmp, i8* null) #9
  %1 = load i8*, i8** %src_data, align 4
  %2 = load i8*, i8** %src_data, align 4
  %add.ptr = getelementptr inbounds i8, i8* %2, i32 1
  %coerce.dive5 = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %agg.tmp, i32 0, i32 0
  %3 = load i8*, i8** %coerce.dive5, align 4
  %call6 = call i8* @_ZNSt3__26vectorIcNS_9allocatorIcEEE6insertIPKhEENS_9enable_ifIXaasr27__is_cpp17_forward_iteratorIT_EE5valuesr16is_constructibleIcNS_15iterator_traitsIS8_E9referenceEEE5valueENS_11__wrap_iterIPcEEE4typeENSC_IPKcEES8_S8_(%"class.std::__2::vector.16"* %buffer_, i8* %3, i8* %1, i8* %add.ptr)
  %coerce.dive7 = getelementptr inbounds %"class.std::__2::__wrap_iter.41", %"class.std::__2::__wrap_iter.41"* %coerce, i32 0, i32 0
  store i8* %call6, i8** %coerce.dive7, align 4
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %4 = load i1, i1* %retval, align 1
  ret i1 %4
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZNK5draco28AttributeOctahedronTransform25GeneratePortableAttributeERKNS_14PointAttributeERKNSt3__26vectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS4_9allocatorIS8_EEEEi(%"class.std::__2::unique_ptr.36"* noalias sret align 4 %agg.result, %"class.draco::AttributeOctahedronTransform"* %this, %"class.draco::PointAttribute"* nonnull align 8 dereferenceable(92) %attribute, %"class.std::__2::vector.28"* nonnull align 4 dereferenceable(12) %point_ids, i32 %num_points) #0 {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %"class.draco::AttributeOctahedronTransform"*, align 4
  %attribute.addr = alloca %"class.draco::PointAttribute"*, align 4
  %point_ids.addr = alloca %"class.std::__2::vector.28"*, align 4
  %num_points.addr = alloca i32, align 4
  %num_entries = alloca i32, align 4
  %portable_attribute = alloca %"class.std::__2::unique_ptr.36", align 4
  %portable_attribute_data = alloca i32*, align 4
  %agg.tmp = alloca %"class.draco::IndexType", align 4
  %att_val = alloca [3 x float], align 4
  %dst_index = alloca i32, align 4
  %converter = alloca %"class.draco::OctahedronToolBox", align 4
  %cleanup.dest.slot = alloca i32, align 4
  %i = alloca i32, align 4
  %att_val_id = alloca %"class.draco::IndexType", align 4
  %agg.tmp9 = alloca %"class.draco::IndexType.30", align 4
  %agg.tmp14 = alloca %"class.draco::IndexType", align 4
  %s = alloca i32, align 4
  %t = alloca i32, align 4
  %0 = bitcast %"class.std::__2::unique_ptr.36"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %"class.draco::AttributeOctahedronTransform"* %this, %"class.draco::AttributeOctahedronTransform"** %this.addr, align 4
  store %"class.draco::PointAttribute"* %attribute, %"class.draco::PointAttribute"** %attribute.addr, align 4
  store %"class.std::__2::vector.28"* %point_ids, %"class.std::__2::vector.28"** %point_ids.addr, align 4
  store i32 %num_points, i32* %num_points.addr, align 4
  %this1 = load %"class.draco::AttributeOctahedronTransform"*, %"class.draco::AttributeOctahedronTransform"** %this.addr, align 4
  %1 = load %"class.std::__2::vector.28"*, %"class.std::__2::vector.28"** %point_ids.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.28"* %1) #9
  store i32 %call, i32* %num_entries, align 4
  %2 = bitcast %"class.draco::AttributeOctahedronTransform"* %this1 to %"class.draco::AttributeTransform"*
  %3 = load i32, i32* %num_entries, align 4
  %4 = load i32, i32* %num_points.addr, align 4
  %5 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %attribute.addr, align 4
  call void @_ZNK5draco18AttributeTransform21InitPortableAttributeEiiiRKNS_14PointAttributeEb(%"class.std::__2::unique_ptr.36"* sret align 4 %portable_attribute, %"class.draco::AttributeTransform"* %2, i32 %3, i32 2, i32 %4, %"class.draco::PointAttribute"* nonnull align 8 dereferenceable(92) %5, i1 zeroext true)
  %call2 = call %"class.draco::PointAttribute"* @_ZNKSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr.36"* %portable_attribute) #9
  %6 = bitcast %"class.draco::PointAttribute"* %call2 to %"class.draco::GeometryAttribute"*
  %call3 = call %"class.draco::IndexType"* @_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEC2Ej(%"class.draco::IndexType"* %agg.tmp, i32 0)
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %agg.tmp, i32 0, i32 0
  %7 = load i32, i32* %coerce.dive, align 4
  %call4 = call i8* @_ZN5draco17GeometryAttribute10GetAddressENS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEE(%"class.draco::GeometryAttribute"* %6, i32 %7)
  %8 = bitcast i8* %call4 to i32*
  store i32* %8, i32** %portable_attribute_data, align 4
  store i32 0, i32* %dst_index, align 4
  %call5 = call %"class.draco::OctahedronToolBox"* @_ZN5draco17OctahedronToolBoxC2Ev(%"class.draco::OctahedronToolBox"* %converter)
  %quantization_bits_ = getelementptr inbounds %"class.draco::AttributeOctahedronTransform", %"class.draco::AttributeOctahedronTransform"* %this1, i32 0, i32 1
  %9 = load i32, i32* %quantization_bits_, align 4
  %call6 = call zeroext i1 @_ZN5draco17OctahedronToolBox19SetQuantizationBitsEi(%"class.draco::OctahedronToolBox"* %converter, i32 %9)
  br i1 %call6, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  %call7 = call %"class.std::__2::unique_ptr.36"* @_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEEC2ILb1EvEEDn(%"class.std::__2::unique_ptr.36"* %agg.result, i8* null) #9
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %10 = load i32, i32* %i, align 4
  %11 = load %"class.std::__2::vector.28"*, %"class.std::__2::vector.28"** %point_ids.addr, align 4
  %call8 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.28"* %11) #9
  %cmp = icmp ult i32 %10, %call8
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %12 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %attribute.addr, align 4
  %13 = load %"class.std::__2::vector.28"*, %"class.std::__2::vector.28"** %point_ids.addr, align 4
  %14 = load i32, i32* %i, align 4
  %call10 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.30"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEEixEm(%"class.std::__2::vector.28"* %13, i32 %14) #9
  %15 = bitcast %"class.draco::IndexType.30"* %agg.tmp9 to i8*
  %16 = bitcast %"class.draco::IndexType.30"* %call10 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %15, i8* align 4 %16, i32 4, i1 false)
  %coerce.dive11 = getelementptr inbounds %"class.draco::IndexType.30", %"class.draco::IndexType.30"* %agg.tmp9, i32 0, i32 0
  %17 = load i32, i32* %coerce.dive11, align 4
  %call12 = call i32 @_ZNK5draco14PointAttribute12mapped_indexENS_9IndexTypeIjNS_20PointIndex_tag_type_EEE(%"class.draco::PointAttribute"* %12, i32 %17)
  %coerce.dive13 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %att_val_id, i32 0, i32 0
  store i32 %call12, i32* %coerce.dive13, align 4
  %18 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %attribute.addr, align 4
  %19 = bitcast %"class.draco::PointAttribute"* %18 to %"class.draco::GeometryAttribute"*
  %20 = bitcast %"class.draco::IndexType"* %agg.tmp14 to i8*
  %21 = bitcast %"class.draco::IndexType"* %att_val_id to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %20, i8* align 4 %21, i32 4, i1 false)
  %arraydecay = getelementptr inbounds [3 x float], [3 x float]* %att_val, i32 0, i32 0
  %22 = bitcast float* %arraydecay to i8*
  %coerce.dive15 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %agg.tmp14, i32 0, i32 0
  %23 = load i32, i32* %coerce.dive15, align 4
  call void @_ZNK5draco17GeometryAttribute8GetValueENS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEEPv(%"class.draco::GeometryAttribute"* %19, i32 %23, i8* %22)
  %arraydecay16 = getelementptr inbounds [3 x float], [3 x float]* %att_val, i32 0, i32 0
  call void @_ZNK5draco17OctahedronToolBox38FloatVectorToQuantizedOctahedralCoordsIfEEvPKT_PiS5_(%"class.draco::OctahedronToolBox"* %converter, float* %arraydecay16, i32* %s, i32* %t)
  %24 = load i32, i32* %s, align 4
  %25 = load i32*, i32** %portable_attribute_data, align 4
  %26 = load i32, i32* %dst_index, align 4
  %inc = add nsw i32 %26, 1
  store i32 %inc, i32* %dst_index, align 4
  %arrayidx = getelementptr inbounds i32, i32* %25, i32 %26
  store i32 %24, i32* %arrayidx, align 4
  %27 = load i32, i32* %t, align 4
  %28 = load i32*, i32** %portable_attribute_data, align 4
  %29 = load i32, i32* %dst_index, align 4
  %inc17 = add nsw i32 %29, 1
  store i32 %inc17, i32* %dst_index, align 4
  %arrayidx18 = getelementptr inbounds i32, i32* %28, i32 %29
  store i32 %27, i32* %arrayidx18, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %30 = load i32, i32* %i, align 4
  %inc19 = add i32 %30, 1
  store i32 %inc19, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call20 = call %"class.std::__2::unique_ptr.36"* @_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEEC2EOS5_(%"class.std::__2::unique_ptr.36"* %agg.result, %"class.std::__2::unique_ptr.36"* nonnull align 4 dereferenceable(4) %portable_attribute) #9
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end, %if.then
  %call21 = call %"class.std::__2::unique_ptr.36"* @_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.36"* %portable_attribute) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.28"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.28"*, align 4
  store %"class.std::__2::vector.28"* %this, %"class.std::__2::vector.28"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.28"*, %"class.std::__2::vector.28"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.28"* %this1 to %"class.std::__2::__vector_base.29"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.29", %"class.std::__2::__vector_base.29"* %0, i32 0, i32 1
  %1 = load %"class.draco::IndexType.30"*, %"class.draco::IndexType.30"** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector.28"* %this1 to %"class.std::__2::__vector_base.29"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.29", %"class.std::__2::__vector_base.29"* %2, i32 0, i32 0
  %3 = load %"class.draco::IndexType.30"*, %"class.draco::IndexType.30"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.draco::IndexType.30"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.draco::IndexType.30"* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

declare void @_ZNK5draco18AttributeTransform21InitPortableAttributeEiiiRKNS_14PointAttributeEb(%"class.std::__2::unique_ptr.36"* sret align 4, %"class.draco::AttributeTransform"*, i32, i32, i32, %"class.draco::PointAttribute"* nonnull align 8 dereferenceable(92), i1 zeroext) #1

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::PointAttribute"* @_ZNKSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr.36"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.36"*, align 4
  store %"class.std::__2::unique_ptr.36"* %this, %"class.std::__2::unique_ptr.36"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.36"*, %"class.std::__2::unique_ptr.36"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.36", %"class.std::__2::unique_ptr.36"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNKSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.37"* %__ptr_) #9
  %0 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %call, align 4
  ret %"class.draco::PointAttribute"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN5draco17GeometryAttribute10GetAddressENS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEE(%"class.draco::GeometryAttribute"* %this, i32 %att_index.coerce) #0 comdat {
entry:
  %att_index = alloca %"class.draco::IndexType", align 4
  %this.addr = alloca %"class.draco::GeometryAttribute"*, align 4
  %byte_pos = alloca i64, align 8
  %agg.tmp = alloca %"class.draco::IndexType", align 4
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %att_index, i32 0, i32 0
  store i32 %att_index.coerce, i32* %coerce.dive, align 4
  store %"class.draco::GeometryAttribute"* %this, %"class.draco::GeometryAttribute"** %this.addr, align 4
  %this1 = load %"class.draco::GeometryAttribute"*, %"class.draco::GeometryAttribute"** %this.addr, align 4
  %0 = bitcast %"class.draco::IndexType"* %agg.tmp to i8*
  %1 = bitcast %"class.draco::IndexType"* %att_index to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %0, i8* align 4 %1, i32 4, i1 false)
  %coerce.dive2 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %agg.tmp, i32 0, i32 0
  %2 = load i32, i32* %coerce.dive2, align 4
  %call = call i64 @_ZNK5draco17GeometryAttribute10GetBytePosENS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEE(%"class.draco::GeometryAttribute"* %this1, i32 %2)
  store i64 %call, i64* %byte_pos, align 8
  %buffer_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 0
  %3 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %buffer_, align 8
  %call3 = call i8* @_ZN5draco10DataBuffer4dataEv(%"class.draco::DataBuffer"* %3)
  %4 = load i64, i64* %byte_pos, align 8
  %idx.ext = trunc i64 %4 to i32
  %add.ptr = getelementptr inbounds i8, i8* %call3, i32 %idx.ext
  ret i8* %add.ptr
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType"* @_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEC2Ej(%"class.draco::IndexType"* returned %this, i32 %value) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType"*, align 4
  %value.addr = alloca i32, align 4
  store %"class.draco::IndexType"* %this, %"class.draco::IndexType"** %this.addr, align 4
  store i32 %value, i32* %value.addr, align 4
  %this1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value.addr, align 4
  store i32 %0, i32* %value_, align 4
  ret %"class.draco::IndexType"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::OctahedronToolBox"* @_ZN5draco17OctahedronToolBoxC2Ev(%"class.draco::OctahedronToolBox"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::OctahedronToolBox"*, align 4
  store %"class.draco::OctahedronToolBox"* %this, %"class.draco::OctahedronToolBox"** %this.addr, align 4
  %this1 = load %"class.draco::OctahedronToolBox"*, %"class.draco::OctahedronToolBox"** %this.addr, align 4
  %quantization_bits_ = getelementptr inbounds %"class.draco::OctahedronToolBox", %"class.draco::OctahedronToolBox"* %this1, i32 0, i32 0
  store i32 -1, i32* %quantization_bits_, align 4
  %max_quantized_value_ = getelementptr inbounds %"class.draco::OctahedronToolBox", %"class.draco::OctahedronToolBox"* %this1, i32 0, i32 1
  store i32 -1, i32* %max_quantized_value_, align 4
  %max_value_ = getelementptr inbounds %"class.draco::OctahedronToolBox", %"class.draco::OctahedronToolBox"* %this1, i32 0, i32 2
  store i32 -1, i32* %max_value_, align 4
  %center_value_ = getelementptr inbounds %"class.draco::OctahedronToolBox", %"class.draco::OctahedronToolBox"* %this1, i32 0, i32 3
  store i32 -1, i32* %center_value_, align 4
  ret %"class.draco::OctahedronToolBox"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco17OctahedronToolBox19SetQuantizationBitsEi(%"class.draco::OctahedronToolBox"* %this, i32 %q) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::OctahedronToolBox"*, align 4
  %q.addr = alloca i32, align 4
  store %"class.draco::OctahedronToolBox"* %this, %"class.draco::OctahedronToolBox"** %this.addr, align 4
  store i32 %q, i32* %q.addr, align 4
  %this1 = load %"class.draco::OctahedronToolBox"*, %"class.draco::OctahedronToolBox"** %this.addr, align 4
  %0 = load i32, i32* %q.addr, align 4
  %cmp = icmp slt i32 %0, 2
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %1 = load i32, i32* %q.addr, align 4
  %cmp2 = icmp sgt i32 %1, 30
  br i1 %cmp2, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  %2 = load i32, i32* %q.addr, align 4
  %quantization_bits_ = getelementptr inbounds %"class.draco::OctahedronToolBox", %"class.draco::OctahedronToolBox"* %this1, i32 0, i32 0
  store i32 %2, i32* %quantization_bits_, align 4
  %quantization_bits_3 = getelementptr inbounds %"class.draco::OctahedronToolBox", %"class.draco::OctahedronToolBox"* %this1, i32 0, i32 0
  %3 = load i32, i32* %quantization_bits_3, align 4
  %shl = shl i32 1, %3
  %sub = sub nsw i32 %shl, 1
  %max_quantized_value_ = getelementptr inbounds %"class.draco::OctahedronToolBox", %"class.draco::OctahedronToolBox"* %this1, i32 0, i32 1
  store i32 %sub, i32* %max_quantized_value_, align 4
  %max_quantized_value_4 = getelementptr inbounds %"class.draco::OctahedronToolBox", %"class.draco::OctahedronToolBox"* %this1, i32 0, i32 1
  %4 = load i32, i32* %max_quantized_value_4, align 4
  %sub5 = sub nsw i32 %4, 1
  %max_value_ = getelementptr inbounds %"class.draco::OctahedronToolBox", %"class.draco::OctahedronToolBox"* %this1, i32 0, i32 2
  store i32 %sub5, i32* %max_value_, align 4
  %max_value_6 = getelementptr inbounds %"class.draco::OctahedronToolBox", %"class.draco::OctahedronToolBox"* %this1, i32 0, i32 2
  %5 = load i32, i32* %max_value_6, align 4
  %div = sdiv i32 %5, 2
  %center_value_ = getelementptr inbounds %"class.draco::OctahedronToolBox", %"class.draco::OctahedronToolBox"* %this1, i32 0, i32 3
  store i32 %div, i32* %center_value_, align 4
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %6 = load i1, i1* %retval, align 1
  ret i1 %6
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.36"* @_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEEC2ILb1EvEEDn(%"class.std::__2::unique_ptr.36"* returned %this, i8* %0) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.36"*, align 4
  %.addr = alloca i8*, align 4
  %ref.tmp = alloca %"class.draco::PointAttribute"*, align 4
  %ref.tmp2 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::unique_ptr.36"* %this, %"class.std::__2::unique_ptr.36"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.36"*, %"class.std::__2::unique_ptr.36"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.36", %"class.std::__2::unique_ptr.36"* %this1, i32 0, i32 0
  store %"class.draco::PointAttribute"* null, %"class.draco::PointAttribute"** %ref.tmp, align 4
  %call = call %"class.std::__2::__compressed_pair.37"* @_ZNSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEEC2IS3_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.37"* %__ptr_, %"class.draco::PointAttribute"** nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  ret %"class.std::__2::unique_ptr.36"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco14PointAttribute12mapped_indexENS_9IndexTypeIjNS_20PointIndex_tag_type_EEE(%"class.draco::PointAttribute"* %this, i32 %point_index.coerce) #0 comdat {
entry:
  %retval = alloca %"class.draco::IndexType", align 4
  %point_index = alloca %"class.draco::IndexType.30", align 4
  %this.addr = alloca %"class.draco::PointAttribute"*, align 4
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType.30", %"class.draco::IndexType.30"* %point_index, i32 0, i32 0
  store i32 %point_index.coerce, i32* %coerce.dive, align 4
  store %"class.draco::PointAttribute"* %this, %"class.draco::PointAttribute"** %this.addr, align 4
  %this1 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %this.addr, align 4
  %identity_mapping_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 4
  %0 = load i8, i8* %identity_mapping_, align 4
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call = call i32 @_ZNK5draco9IndexTypeIjNS_20PointIndex_tag_type_EE5valueEv(%"class.draco::IndexType.30"* %point_index)
  %call2 = call %"class.draco::IndexType"* @_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEC2Ej(%"class.draco::IndexType"* %retval, i32 %call)
  br label %return

if.end:                                           ; preds = %entry
  %indices_map_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 2
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEEEixERKS3_(%"class.draco::IndexTypeVector"* %indices_map_, %"class.draco::IndexType.30"* nonnull align 4 dereferenceable(4) %point_index)
  %1 = bitcast %"class.draco::IndexType"* %retval to i8*
  %2 = bitcast %"class.draco::IndexType"* %call3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 4, i1 false)
  br label %return

return:                                           ; preds = %if.end, %if.then
  %coerce.dive4 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %retval, i32 0, i32 0
  %3 = load i32, i32* %coerce.dive4, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.30"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEEixEm(%"class.std::__2::vector.28"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.28"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector.28"* %this, %"class.std::__2::vector.28"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.28"*, %"class.std::__2::vector.28"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.28"* %this1 to %"class.std::__2::__vector_base.29"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.29", %"class.std::__2::__vector_base.29"* %0, i32 0, i32 0
  %1 = load %"class.draco::IndexType.30"*, %"class.draco::IndexType.30"** %__begin_, align 4
  %2 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds %"class.draco::IndexType.30", %"class.draco::IndexType.30"* %1, i32 %2
  ret %"class.draco::IndexType.30"* %arrayidx
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #2

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK5draco17GeometryAttribute8GetValueENS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEEPv(%"class.draco::GeometryAttribute"* %this, i32 %att_index.coerce, i8* %out_data) #0 comdat {
entry:
  %att_index = alloca %"class.draco::IndexType", align 4
  %this.addr = alloca %"class.draco::GeometryAttribute"*, align 4
  %out_data.addr = alloca i8*, align 4
  %byte_pos = alloca i64, align 8
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %att_index, i32 0, i32 0
  store i32 %att_index.coerce, i32* %coerce.dive, align 4
  store %"class.draco::GeometryAttribute"* %this, %"class.draco::GeometryAttribute"** %this.addr, align 4
  store i8* %out_data, i8** %out_data.addr, align 4
  %this1 = load %"class.draco::GeometryAttribute"*, %"class.draco::GeometryAttribute"** %this.addr, align 4
  %byte_offset_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 6
  %0 = load i64, i64* %byte_offset_, align 8
  %byte_stride_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 5
  %1 = load i64, i64* %byte_stride_, align 8
  %call = call i32 @_ZNK5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EE5valueEv(%"class.draco::IndexType"* %att_index)
  %conv = zext i32 %call to i64
  %mul = mul nsw i64 %1, %conv
  %add = add nsw i64 %0, %mul
  store i64 %add, i64* %byte_pos, align 8
  %buffer_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 0
  %2 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %buffer_, align 8
  %3 = load i64, i64* %byte_pos, align 8
  %4 = load i8*, i8** %out_data.addr, align 4
  %byte_stride_2 = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 5
  %5 = load i64, i64* %byte_stride_2, align 8
  %conv3 = trunc i64 %5 to i32
  call void @_ZNK5draco10DataBuffer4ReadExPvm(%"class.draco::DataBuffer"* %2, i64 %3, i8* %4, i32 %conv3)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK5draco17OctahedronToolBox38FloatVectorToQuantizedOctahedralCoordsIfEEvPKT_PiS5_(%"class.draco::OctahedronToolBox"* %this, float* %vector, i32* %out_s, i32* %out_t) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::OctahedronToolBox"*, align 4
  %vector.addr = alloca float*, align 4
  %out_s.addr = alloca i32*, align 4
  %out_t.addr = alloca i32*, align 4
  %abs_sum = alloca double, align 8
  %scaled_vector = alloca [3 x double], align 16
  %scale = alloca double, align 8
  %int_vec = alloca [3 x i32], align 4
  store %"class.draco::OctahedronToolBox"* %this, %"class.draco::OctahedronToolBox"** %this.addr, align 4
  store float* %vector, float** %vector.addr, align 4
  store i32* %out_s, i32** %out_s.addr, align 4
  store i32* %out_t, i32** %out_t.addr, align 4
  %this1 = load %"class.draco::OctahedronToolBox"*, %"class.draco::OctahedronToolBox"** %this.addr, align 4
  %0 = load float*, float** %vector.addr, align 4
  %arrayidx = getelementptr inbounds float, float* %0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %conv = fpext float %1 to double
  %call = call double @_Z3absd(double %conv) #9
  %2 = load float*, float** %vector.addr, align 4
  %arrayidx2 = getelementptr inbounds float, float* %2, i32 1
  %3 = load float, float* %arrayidx2, align 4
  %conv3 = fpext float %3 to double
  %call4 = call double @_Z3absd(double %conv3) #9
  %add = fadd double %call, %call4
  %4 = load float*, float** %vector.addr, align 4
  %arrayidx5 = getelementptr inbounds float, float* %4, i32 2
  %5 = load float, float* %arrayidx5, align 4
  %conv6 = fpext float %5 to double
  %call7 = call double @_Z3absd(double %conv6) #9
  %add8 = fadd double %add, %call7
  store double %add8, double* %abs_sum, align 8
  %6 = load double, double* %abs_sum, align 8
  %cmp = fcmp ogt double %6, 0x3EB0C6F7A0B5ED8D
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %7 = load double, double* %abs_sum, align 8
  %div = fdiv double 1.000000e+00, %7
  store double %div, double* %scale, align 8
  %8 = load float*, float** %vector.addr, align 4
  %arrayidx9 = getelementptr inbounds float, float* %8, i32 0
  %9 = load float, float* %arrayidx9, align 4
  %conv10 = fpext float %9 to double
  %10 = load double, double* %scale, align 8
  %mul = fmul double %conv10, %10
  %arrayidx11 = getelementptr inbounds [3 x double], [3 x double]* %scaled_vector, i32 0, i32 0
  store double %mul, double* %arrayidx11, align 16
  %11 = load float*, float** %vector.addr, align 4
  %arrayidx12 = getelementptr inbounds float, float* %11, i32 1
  %12 = load float, float* %arrayidx12, align 4
  %conv13 = fpext float %12 to double
  %13 = load double, double* %scale, align 8
  %mul14 = fmul double %conv13, %13
  %arrayidx15 = getelementptr inbounds [3 x double], [3 x double]* %scaled_vector, i32 0, i32 1
  store double %mul14, double* %arrayidx15, align 8
  %14 = load float*, float** %vector.addr, align 4
  %arrayidx16 = getelementptr inbounds float, float* %14, i32 2
  %15 = load float, float* %arrayidx16, align 4
  %conv17 = fpext float %15 to double
  %16 = load double, double* %scale, align 8
  %mul18 = fmul double %conv17, %16
  %arrayidx19 = getelementptr inbounds [3 x double], [3 x double]* %scaled_vector, i32 0, i32 2
  store double %mul18, double* %arrayidx19, align 16
  br label %if.end

if.else:                                          ; preds = %entry
  %arrayidx20 = getelementptr inbounds [3 x double], [3 x double]* %scaled_vector, i32 0, i32 0
  store double 1.000000e+00, double* %arrayidx20, align 16
  %arrayidx21 = getelementptr inbounds [3 x double], [3 x double]* %scaled_vector, i32 0, i32 1
  store double 0.000000e+00, double* %arrayidx21, align 8
  %arrayidx22 = getelementptr inbounds [3 x double], [3 x double]* %scaled_vector, i32 0, i32 2
  store double 0.000000e+00, double* %arrayidx22, align 16
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %arrayidx23 = getelementptr inbounds [3 x double], [3 x double]* %scaled_vector, i32 0, i32 0
  %17 = load double, double* %arrayidx23, align 16
  %center_value_ = getelementptr inbounds %"class.draco::OctahedronToolBox", %"class.draco::OctahedronToolBox"* %this1, i32 0, i32 3
  %18 = load i32, i32* %center_value_, align 4
  %conv24 = sitofp i32 %18 to double
  %mul25 = fmul double %17, %conv24
  %add26 = fadd double %mul25, 5.000000e-01
  %19 = call double @llvm.floor.f64(double %add26)
  %conv27 = fptosi double %19 to i32
  %arrayidx28 = getelementptr inbounds [3 x i32], [3 x i32]* %int_vec, i32 0, i32 0
  store i32 %conv27, i32* %arrayidx28, align 4
  %arrayidx29 = getelementptr inbounds [3 x double], [3 x double]* %scaled_vector, i32 0, i32 1
  %20 = load double, double* %arrayidx29, align 8
  %center_value_30 = getelementptr inbounds %"class.draco::OctahedronToolBox", %"class.draco::OctahedronToolBox"* %this1, i32 0, i32 3
  %21 = load i32, i32* %center_value_30, align 4
  %conv31 = sitofp i32 %21 to double
  %mul32 = fmul double %20, %conv31
  %add33 = fadd double %mul32, 5.000000e-01
  %22 = call double @llvm.floor.f64(double %add33)
  %conv34 = fptosi double %22 to i32
  %arrayidx35 = getelementptr inbounds [3 x i32], [3 x i32]* %int_vec, i32 0, i32 1
  store i32 %conv34, i32* %arrayidx35, align 4
  %center_value_36 = getelementptr inbounds %"class.draco::OctahedronToolBox", %"class.draco::OctahedronToolBox"* %this1, i32 0, i32 3
  %23 = load i32, i32* %center_value_36, align 4
  %arrayidx37 = getelementptr inbounds [3 x i32], [3 x i32]* %int_vec, i32 0, i32 0
  %24 = load i32, i32* %arrayidx37, align 4
  %call38 = call i32 @abs(i32 %24) #10
  %sub = sub nsw i32 %23, %call38
  %arrayidx39 = getelementptr inbounds [3 x i32], [3 x i32]* %int_vec, i32 0, i32 1
  %25 = load i32, i32* %arrayidx39, align 4
  %call40 = call i32 @abs(i32 %25) #10
  %sub41 = sub nsw i32 %sub, %call40
  %arrayidx42 = getelementptr inbounds [3 x i32], [3 x i32]* %int_vec, i32 0, i32 2
  store i32 %sub41, i32* %arrayidx42, align 4
  %arrayidx43 = getelementptr inbounds [3 x i32], [3 x i32]* %int_vec, i32 0, i32 2
  %26 = load i32, i32* %arrayidx43, align 4
  %cmp44 = icmp slt i32 %26, 0
  br i1 %cmp44, label %if.then45, label %if.end58

if.then45:                                        ; preds = %if.end
  %arrayidx46 = getelementptr inbounds [3 x i32], [3 x i32]* %int_vec, i32 0, i32 1
  %27 = load i32, i32* %arrayidx46, align 4
  %cmp47 = icmp sgt i32 %27, 0
  br i1 %cmp47, label %if.then48, label %if.else52

if.then48:                                        ; preds = %if.then45
  %arrayidx49 = getelementptr inbounds [3 x i32], [3 x i32]* %int_vec, i32 0, i32 2
  %28 = load i32, i32* %arrayidx49, align 4
  %arrayidx50 = getelementptr inbounds [3 x i32], [3 x i32]* %int_vec, i32 0, i32 1
  %29 = load i32, i32* %arrayidx50, align 4
  %add51 = add nsw i32 %29, %28
  store i32 %add51, i32* %arrayidx50, align 4
  br label %if.end56

if.else52:                                        ; preds = %if.then45
  %arrayidx53 = getelementptr inbounds [3 x i32], [3 x i32]* %int_vec, i32 0, i32 2
  %30 = load i32, i32* %arrayidx53, align 4
  %arrayidx54 = getelementptr inbounds [3 x i32], [3 x i32]* %int_vec, i32 0, i32 1
  %31 = load i32, i32* %arrayidx54, align 4
  %sub55 = sub nsw i32 %31, %30
  store i32 %sub55, i32* %arrayidx54, align 4
  br label %if.end56

if.end56:                                         ; preds = %if.else52, %if.then48
  %arrayidx57 = getelementptr inbounds [3 x i32], [3 x i32]* %int_vec, i32 0, i32 2
  store i32 0, i32* %arrayidx57, align 4
  br label %if.end58

if.end58:                                         ; preds = %if.end56, %if.end
  %arrayidx59 = getelementptr inbounds [3 x double], [3 x double]* %scaled_vector, i32 0, i32 2
  %32 = load double, double* %arrayidx59, align 16
  %cmp60 = fcmp olt double %32, 0.000000e+00
  br i1 %cmp60, label %if.then61, label %if.end64

if.then61:                                        ; preds = %if.end58
  %arrayidx62 = getelementptr inbounds [3 x i32], [3 x i32]* %int_vec, i32 0, i32 2
  %33 = load i32, i32* %arrayidx62, align 4
  %mul63 = mul nsw i32 %33, -1
  store i32 %mul63, i32* %arrayidx62, align 4
  br label %if.end64

if.end64:                                         ; preds = %if.then61, %if.end58
  %arraydecay = getelementptr inbounds [3 x i32], [3 x i32]* %int_vec, i32 0, i32 0
  %34 = load i32*, i32** %out_s.addr, align 4
  %35 = load i32*, i32** %out_t.addr, align 4
  call void @_ZNK5draco17OctahedronToolBox40IntegerVectorToQuantizedOctahedralCoordsEPKiPiS3_(%"class.draco::OctahedronToolBox"* %this1, i32* %arraydecay, i32* %34, i32* %35)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.36"* @_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEEC2EOS5_(%"class.std::__2::unique_ptr.36"* returned %this, %"class.std::__2::unique_ptr.36"* nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.36"*, align 4
  %__u.addr = alloca %"class.std::__2::unique_ptr.36"*, align 4
  %ref.tmp = alloca %"class.draco::PointAttribute"*, align 4
  store %"class.std::__2::unique_ptr.36"* %this, %"class.std::__2::unique_ptr.36"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.36"* %__u, %"class.std::__2::unique_ptr.36"** %__u.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.36"*, %"class.std::__2::unique_ptr.36"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.36", %"class.std::__2::unique_ptr.36"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::unique_ptr.36"*, %"class.std::__2::unique_ptr.36"** %__u.addr, align 4
  %call = call %"class.draco::PointAttribute"* @_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEE7releaseEv(%"class.std::__2::unique_ptr.36"* %0) #9
  store %"class.draco::PointAttribute"* %call, %"class.draco::PointAttribute"** %ref.tmp, align 4
  %1 = load %"class.std::__2::unique_ptr.36"*, %"class.std::__2::unique_ptr.36"** %__u.addr, align 4
  %call2 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.40"* @_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEE11get_deleterEv(%"class.std::__2::unique_ptr.36"* %1) #9
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.40"* @_ZNSt3__27forwardINS_14default_deleteIN5draco14PointAttributeEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.40"* nonnull align 1 dereferenceable(1) %call2) #9
  %call4 = call %"class.std::__2::__compressed_pair.37"* @_ZNSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEEC2IS3_S5_EEOT_OT0_(%"class.std::__2::__compressed_pair.37"* %__ptr_, %"class.draco::PointAttribute"** nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::default_delete.40"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::unique_ptr.36"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.36"* @_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.36"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.36"*, align 4
  store %"class.std::__2::unique_ptr.36"* %this, %"class.std::__2::unique_ptr.36"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.36"*, %"class.std::__2::unique_ptr.36"** %this.addr, align 4
  call void @_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.36"* %this1, %"class.draco::PointAttribute"* null) #9
  ret %"class.std::__2::unique_ptr.36"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::AttributeOctahedronTransform"* @_ZN5draco28AttributeOctahedronTransformD2Ev(%"class.draco::AttributeOctahedronTransform"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::AttributeOctahedronTransform"*, align 4
  store %"class.draco::AttributeOctahedronTransform"* %this, %"class.draco::AttributeOctahedronTransform"** %this.addr, align 4
  %this1 = load %"class.draco::AttributeOctahedronTransform"*, %"class.draco::AttributeOctahedronTransform"** %this.addr, align 4
  %0 = bitcast %"class.draco::AttributeOctahedronTransform"* %this1 to %"class.draco::AttributeTransform"*
  %call = call %"class.draco::AttributeTransform"* @_ZN5draco18AttributeTransformD2Ev(%"class.draco::AttributeTransform"* %0) #9
  ret %"class.draco::AttributeOctahedronTransform"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco28AttributeOctahedronTransformD0Ev(%"class.draco::AttributeOctahedronTransform"* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::AttributeOctahedronTransform"*, align 4
  store %"class.draco::AttributeOctahedronTransform"* %this, %"class.draco::AttributeOctahedronTransform"** %this.addr, align 4
  %this1 = load %"class.draco::AttributeOctahedronTransform"*, %"class.draco::AttributeOctahedronTransform"** %this.addr, align 4
  %call = call %"class.draco::AttributeOctahedronTransform"* @_ZN5draco28AttributeOctahedronTransformD2Ev(%"class.draco::AttributeOctahedronTransform"* %this1) #9
  %0 = bitcast %"class.draco::AttributeOctahedronTransform"* %this1 to i8*
  call void @_ZdlPv(i8* %0) #11
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco28AttributeOctahedronTransform4TypeEv(%"class.draco::AttributeOctahedronTransform"* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::AttributeOctahedronTransform"*, align 4
  store %"class.draco::AttributeOctahedronTransform"* %this, %"class.draco::AttributeOctahedronTransform"** %this.addr, align 4
  %this1 = load %"class.draco::AttributeOctahedronTransform"*, %"class.draco::AttributeOctahedronTransform"** %this.addr, align 4
  ret i32 2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::AttributeTransformData"* @_ZNKSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE3getEv(%"class.std::__2::unique_ptr.11"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.11"*, align 4
  store %"class.std::__2::unique_ptr.11"* %this, %"class.std::__2::unique_ptr.11"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.11"*, %"class.std::__2::unique_ptr.11"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.11", %"class.std::__2::unique_ptr.11"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributeTransformData"** @_ZNKSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.12"* %__ptr_) #9
  %0 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %call, align 4
  ret %"class.draco::AttributeTransformData"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::AttributeTransformData"** @_ZNKSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.12"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.12"*, align 4
  store %"class.std::__2::__compressed_pair.12"* %this, %"class.std::__2::__compressed_pair.12"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.12"*, %"class.std::__2::__compressed_pair.12"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.12"* %this1 to %"struct.std::__2::__compressed_pair_elem.13"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributeTransformData"** @_ZNKSt3__222__compressed_pair_elemIPN5draco22AttributeTransformDataELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.13"* %0) #9
  ret %"class.draco::AttributeTransformData"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::AttributeTransformData"** @_ZNKSt3__222__compressed_pair_elemIPN5draco22AttributeTransformDataELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.13"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.13"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.13"* %this, %"struct.std::__2::__compressed_pair_elem.13"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.13"*, %"struct.std::__2::__compressed_pair_elem.13"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.13", %"struct.std::__2::__compressed_pair_elem.13"* %this1, i32 0, i32 0
  ret %"class.draco::AttributeTransformData"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i64 @_ZNK5draco17GeometryAttribute10GetBytePosENS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEE(%"class.draco::GeometryAttribute"* %this, i32 %att_index.coerce) #0 comdat {
entry:
  %att_index = alloca %"class.draco::IndexType", align 4
  %this.addr = alloca %"class.draco::GeometryAttribute"*, align 4
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %att_index, i32 0, i32 0
  store i32 %att_index.coerce, i32* %coerce.dive, align 4
  store %"class.draco::GeometryAttribute"* %this, %"class.draco::GeometryAttribute"** %this.addr, align 4
  %this1 = load %"class.draco::GeometryAttribute"*, %"class.draco::GeometryAttribute"** %this.addr, align 4
  %byte_offset_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 6
  %0 = load i64, i64* %byte_offset_, align 8
  %byte_stride_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 5
  %1 = load i64, i64* %byte_stride_, align 8
  %call = call i32 @_ZNK5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EE5valueEv(%"class.draco::IndexType"* %att_index)
  %conv = zext i32 %call to i64
  %mul = mul nsw i64 %1, %conv
  %add = add nsw i64 %0, %mul
  ret i64 %add
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN5draco10DataBuffer4dataEv(%"class.draco::DataBuffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::DataBuffer"*, align 4
  store %"class.draco::DataBuffer"* %this, %"class.draco::DataBuffer"** %this.addr, align 4
  %this1 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %this.addr, align 4
  %data_ = getelementptr inbounds %"class.draco::DataBuffer", %"class.draco::DataBuffer"* %this1, i32 0, i32 0
  %call = call nonnull align 1 dereferenceable(1) i8* @_ZNSt3__26vectorIhNS_9allocatorIhEEEixEm(%"class.std::__2::vector"* %data_, i32 0) #9
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EE5valueEv(%"class.draco::IndexType"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.draco::IndexType"* %this, %"class.draco::IndexType"** %this.addr, align 4
  %this1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value_, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) i8* @_ZNSt3__26vectorIhNS_9allocatorIhEEEixEm(%"class.std::__2::vector"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 0
  %1 = load i8*, i8** %__begin_, align 4
  %2 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds i8, i8* %1, i32 %2
  ret i8* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.37"* @_ZNSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEEC2IS3_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.37"* returned %this, %"class.draco::PointAttribute"** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.37"*, align 4
  %__t1.addr = alloca %"class.draco::PointAttribute"**, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.37"* %this, %"class.std::__2::__compressed_pair.37"** %this.addr, align 4
  store %"class.draco::PointAttribute"** %__t1, %"class.draco::PointAttribute"*** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.37"*, %"class.std::__2::__compressed_pair.37"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.37"* %this1 to %"struct.std::__2::__compressed_pair_elem.38"*
  %1 = load %"class.draco::PointAttribute"**, %"class.draco::PointAttribute"*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNSt3__27forwardIPN5draco14PointAttributeEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::PointAttribute"** nonnull align 4 dereferenceable(4) %1) #9
  %call2 = call %"struct.std::__2::__compressed_pair_elem.38"* @_ZNSt3__222__compressed_pair_elemIPN5draco14PointAttributeELi0ELb0EEC2IS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.38"* %0, %"class.draco::PointAttribute"** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.37"* %this1 to %"struct.std::__2::__compressed_pair_elem.39"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #9
  %call4 = call %"struct.std::__2::__compressed_pair_elem.39"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco14PointAttributeEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.39"* %2)
  ret %"class.std::__2::__compressed_pair.37"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNSt3__27forwardIPN5draco14PointAttributeEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::PointAttribute"** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.draco::PointAttribute"**, align 4
  store %"class.draco::PointAttribute"** %__t, %"class.draco::PointAttribute"*** %__t.addr, align 4
  %0 = load %"class.draco::PointAttribute"**, %"class.draco::PointAttribute"*** %__t.addr, align 4
  ret %"class.draco::PointAttribute"** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.38"* @_ZNSt3__222__compressed_pair_elemIPN5draco14PointAttributeELi0ELb0EEC2IS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.38"* returned %this, %"class.draco::PointAttribute"** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.38"*, align 4
  %__u.addr = alloca %"class.draco::PointAttribute"**, align 4
  store %"struct.std::__2::__compressed_pair_elem.38"* %this, %"struct.std::__2::__compressed_pair_elem.38"** %this.addr, align 4
  store %"class.draco::PointAttribute"** %__u, %"class.draco::PointAttribute"*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.38"*, %"struct.std::__2::__compressed_pair_elem.38"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.38", %"struct.std::__2::__compressed_pair_elem.38"* %this1, i32 0, i32 0
  %0 = load %"class.draco::PointAttribute"**, %"class.draco::PointAttribute"*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNSt3__27forwardIPN5draco14PointAttributeEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::PointAttribute"** nonnull align 4 dereferenceable(4) %0) #9
  %1 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %call, align 4
  store %"class.draco::PointAttribute"* %1, %"class.draco::PointAttribute"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.38"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  store %"struct.std::__2::__default_init_tag"* %__t, %"struct.std::__2::__default_init_tag"** %__t.addr, align 4
  %0 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t.addr, align 4
  ret %"struct.std::__2::__default_init_tag"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.39"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco14PointAttributeEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.39"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.39"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.39"* %this, %"struct.std::__2::__compressed_pair_elem.39"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.39"*, %"struct.std::__2::__compressed_pair_elem.39"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.39"* %this1 to %"struct.std::__2::default_delete.40"*
  ret %"struct.std::__2::__compressed_pair_elem.39"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco9IndexTypeIjNS_20PointIndex_tag_type_EE5valueEv(%"class.draco::IndexType.30"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.30"*, align 4
  store %"class.draco::IndexType.30"* %this, %"class.draco::IndexType.30"** %this.addr, align 4
  %this1 = load %"class.draco::IndexType.30"*, %"class.draco::IndexType.30"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.30", %"class.draco::IndexType.30"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value_, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEEEixERKS3_(%"class.draco::IndexTypeVector"* %this, %"class.draco::IndexType.30"* nonnull align 4 dereferenceable(4) %index) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector"*, align 4
  %index.addr = alloca %"class.draco::IndexType.30"*, align 4
  store %"class.draco::IndexTypeVector"* %this, %"class.draco::IndexTypeVector"** %this.addr, align 4
  store %"class.draco::IndexType.30"* %index, %"class.draco::IndexType.30"** %index.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector"*, %"class.draco::IndexTypeVector"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector", %"class.draco::IndexTypeVector"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexType.30"*, %"class.draco::IndexType.30"** %index.addr, align 4
  %call = call i32 @_ZNK5draco9IndexTypeIjNS_20PointIndex_tag_type_EE5valueEv(%"class.draco::IndexType.30"* %0)
  %call2 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEEixEm(%"class.std::__2::vector.4"* %vector_, i32 %call) #9
  ret %"class.draco::IndexType"* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEEixEm(%"class.std::__2::vector.4"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.4"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector.4"* %this, %"class.std::__2::vector.4"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.4"*, %"class.std::__2::vector.4"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.4"* %this1 to %"class.std::__2::__vector_base.5"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.5", %"class.std::__2::__vector_base.5"* %0, i32 0, i32 0
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  %2 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %1, i32 %2
  ret %"class.draco::IndexType"* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK5draco10DataBuffer4ReadExPvm(%"class.draco::DataBuffer"* %this, i64 %byte_pos, i8* %out_data, i32 %data_size) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::DataBuffer"*, align 4
  %byte_pos.addr = alloca i64, align 8
  %out_data.addr = alloca i8*, align 4
  %data_size.addr = alloca i32, align 4
  store %"class.draco::DataBuffer"* %this, %"class.draco::DataBuffer"** %this.addr, align 4
  store i64 %byte_pos, i64* %byte_pos.addr, align 8
  store i8* %out_data, i8** %out_data.addr, align 4
  store i32 %data_size, i32* %data_size.addr, align 4
  %this1 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %this.addr, align 4
  %0 = load i8*, i8** %out_data.addr, align 4
  %call = call i8* @_ZNK5draco10DataBuffer4dataEv(%"class.draco::DataBuffer"* %this1)
  %1 = load i64, i64* %byte_pos.addr, align 8
  %idx.ext = trunc i64 %1 to i32
  %add.ptr = getelementptr inbounds i8, i8* %call, i32 %idx.ext
  %2 = load i32, i32* %data_size.addr, align 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %0, i8* align 1 %add.ptr, i32 %2, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNK5draco10DataBuffer4dataEv(%"class.draco::DataBuffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::DataBuffer"*, align 4
  store %"class.draco::DataBuffer"* %this, %"class.draco::DataBuffer"** %this.addr, align 4
  %this1 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %this.addr, align 4
  %data_ = getelementptr inbounds %"class.draco::DataBuffer", %"class.draco::DataBuffer"* %this1, i32 0, i32 0
  %call = call i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector"* %data_) #9
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 0
  %1 = load i8*, i8** %__begin_, align 4
  %call = call i8* @_ZNSt3__212__to_addressIhEEPT_S2_(i8* %1) #9
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__212__to_addressIhEEPT_S2_(i8* %__p) #0 comdat {
entry:
  %__p.addr = alloca i8*, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %0 = load i8*, i8** %__p.addr, align 4
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::AttributeTransform"* @_ZN5draco18AttributeTransformD2Ev(%"class.draco::AttributeTransform"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::AttributeTransform"*, align 4
  store %"class.draco::AttributeTransform"* %this, %"class.draco::AttributeTransform"** %this.addr, align 4
  %this1 = load %"class.draco::AttributeTransform"*, %"class.draco::AttributeTransform"** %this.addr, align 4
  ret %"class.draco::AttributeTransform"* %this1
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco22AttributeTransformData17SetParameterValueIiEEviRKT_(%"class.draco::AttributeTransformData"* %this, i32 %byte_offset, i32* nonnull align 4 dereferenceable(4) %in_data) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::AttributeTransformData"*, align 4
  %byte_offset.addr = alloca i32, align 4
  %in_data.addr = alloca i32*, align 4
  store %"class.draco::AttributeTransformData"* %this, %"class.draco::AttributeTransformData"** %this.addr, align 4
  store i32 %byte_offset, i32* %byte_offset.addr, align 4
  store i32* %in_data, i32** %in_data.addr, align 4
  %this1 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %this.addr, align 4
  %0 = load i32, i32* %byte_offset.addr, align 4
  %add = add i32 %0, 4
  %buffer_ = getelementptr inbounds %"class.draco::AttributeTransformData", %"class.draco::AttributeTransformData"* %this1, i32 0, i32 1
  %call = call i32 @_ZNK5draco10DataBuffer9data_sizeEv(%"class.draco::DataBuffer"* %buffer_)
  %cmp = icmp ugt i32 %add, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %buffer_2 = getelementptr inbounds %"class.draco::AttributeTransformData", %"class.draco::AttributeTransformData"* %this1, i32 0, i32 1
  %1 = load i32, i32* %byte_offset.addr, align 4
  %add3 = add i32 %1, 4
  %conv = zext i32 %add3 to i64
  call void @_ZN5draco10DataBuffer6ResizeEx(%"class.draco::DataBuffer"* %buffer_2, i64 %conv)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %buffer_4 = getelementptr inbounds %"class.draco::AttributeTransformData", %"class.draco::AttributeTransformData"* %this1, i32 0, i32 1
  %2 = load i32, i32* %byte_offset.addr, align 4
  %conv5 = sext i32 %2 to i64
  %3 = load i32*, i32** %in_data.addr, align 4
  %4 = bitcast i32* %3 to i8*
  call void @_ZN5draco10DataBuffer5WriteExPKvm(%"class.draco::DataBuffer"* %buffer_4, i64 %conv5, i8* %4, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco10DataBuffer9data_sizeEv(%"class.draco::DataBuffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::DataBuffer"*, align 4
  store %"class.draco::DataBuffer"* %this, %"class.draco::DataBuffer"** %this.addr, align 4
  %this1 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %this.addr, align 4
  %data_ = getelementptr inbounds %"class.draco::DataBuffer", %"class.draco::DataBuffer"* %this1, i32 0, i32 0
  %call = call i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4sizeEv(%"class.std::__2::vector"* %data_) #9
  ret i32 %call
}

declare void @_ZN5draco10DataBuffer6ResizeEx(%"class.draco::DataBuffer"*, i64) #1

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco10DataBuffer5WriteExPKvm(%"class.draco::DataBuffer"* %this, i64 %byte_pos, i8* %in_data, i32 %data_size) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::DataBuffer"*, align 4
  %byte_pos.addr = alloca i64, align 8
  %in_data.addr = alloca i8*, align 4
  %data_size.addr = alloca i32, align 4
  store %"class.draco::DataBuffer"* %this, %"class.draco::DataBuffer"** %this.addr, align 4
  store i64 %byte_pos, i64* %byte_pos.addr, align 8
  store i8* %in_data, i8** %in_data.addr, align 4
  store i32 %data_size, i32* %data_size.addr, align 4
  %this1 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %this.addr, align 4
  %call = call i8* @_ZN5draco10DataBuffer4dataEv(%"class.draco::DataBuffer"* %this1)
  %0 = load i64, i64* %byte_pos.addr, align 8
  %idx.ext = trunc i64 %0 to i32
  %add.ptr = getelementptr inbounds i8, i8* %call, i32 %idx.ext
  %1 = load i8*, i8** %in_data.addr, align 4
  %2 = load i32, i32* %data_size.addr, align 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %add.ptr, i8* align 1 %1, i32 %2, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4sizeEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 1
  %1 = load i8*, i8** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %2, i32 0, i32 0
  %3 = load i8*, i8** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint i8* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  ret i32 %sub.ptr.sub
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK5draco13EncoderBuffer18bit_encoder_activeEv(%"class.draco::EncoderBuffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::EncoderBuffer"*, align 4
  store %"class.draco::EncoderBuffer"* %this, %"class.draco::EncoderBuffer"** %this.addr, align 4
  %this1 = load %"class.draco::EncoderBuffer"*, %"class.draco::EncoderBuffer"** %this.addr, align 4
  %bit_encoder_reserved_bytes_ = getelementptr inbounds %"class.draco::EncoderBuffer", %"class.draco::EncoderBuffer"* %this1, i32 0, i32 2
  %0 = load i64, i64* %bit_encoder_reserved_bytes_, align 8
  %cmp = icmp sgt i64 %0, 0
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__26vectorIcNS_9allocatorIcEEE6insertIPKhEENS_9enable_ifIXaasr27__is_cpp17_forward_iteratorIT_EE5valuesr16is_constructibleIcNS_15iterator_traitsIS8_E9referenceEEE5valueENS_11__wrap_iterIPcEEE4typeENSC_IPKcEES8_S8_(%"class.std::__2::vector.16"* %this, i8* %__position.coerce, i8* %__first, i8* %__last) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__wrap_iter.41", align 4
  %__position = alloca %"class.std::__2::__wrap_iter", align 4
  %this.addr = alloca %"class.std::__2::vector.16"*, align 4
  %__first.addr = alloca i8*, align 4
  %__last.addr = alloca i8*, align 4
  %__p = alloca i8*, align 4
  %ref.tmp = alloca %"class.std::__2::__wrap_iter.41", align 4
  %__n = alloca i32, align 4
  %__old_n = alloca i32, align 4
  %__old_last = alloca i8*, align 4
  %__m = alloca i8*, align 4
  %__dx = alloca i32, align 4
  %__diff = alloca i32, align 4
  %__a = alloca %"class.std::__2::allocator.21"*, align 4
  %__v = alloca %"struct.std::__2::__split_buffer", align 4
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %__position, i32 0, i32 0
  store i8* %__position.coerce, i8** %coerce.dive, align 4
  store %"class.std::__2::vector.16"* %this, %"class.std::__2::vector.16"** %this.addr, align 4
  store i8* %__first, i8** %__first.addr, align 4
  store i8* %__last, i8** %__last.addr, align 4
  %this1 = load %"class.std::__2::vector.16"*, %"class.std::__2::vector.16"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.16"* %this1 to %"class.std::__2::__vector_base.17"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.17", %"class.std::__2::__vector_base.17"* %0, i32 0, i32 0
  %1 = load i8*, i8** %__begin_, align 4
  %call = call i8* @_ZNSt3__26vectorIcNS_9allocatorIcEEE5beginEv(%"class.std::__2::vector.16"* %this1) #9
  %coerce.dive2 = getelementptr inbounds %"class.std::__2::__wrap_iter.41", %"class.std::__2::__wrap_iter.41"* %ref.tmp, i32 0, i32 0
  store i8* %call, i8** %coerce.dive2, align 4
  %call3 = call i32 @_ZNSt3__2miIPKcPcEEDTmicldtfp_4baseEcldtfp0_4baseEERKNS_11__wrap_iterIT_EERKNS5_IT0_EE(%"class.std::__2::__wrap_iter"* nonnull align 4 dereferenceable(4) %__position, %"class.std::__2::__wrap_iter.41"* nonnull align 4 dereferenceable(4) %ref.tmp) #9
  %add.ptr = getelementptr inbounds i8, i8* %1, i32 %call3
  store i8* %add.ptr, i8** %__p, align 4
  %2 = load i8*, i8** %__first.addr, align 4
  %3 = load i8*, i8** %__last.addr, align 4
  %call4 = call i32 @_ZNSt3__28distanceIPKhEENS_15iterator_traitsIT_E15difference_typeES4_S4_(i8* %2, i8* %3)
  store i32 %call4, i32* %__n, align 4
  %4 = load i32, i32* %__n, align 4
  %cmp = icmp sgt i32 %4, 0
  br i1 %cmp, label %if.then, label %if.end35

if.then:                                          ; preds = %entry
  %5 = load i32, i32* %__n, align 4
  %6 = bitcast %"class.std::__2::vector.16"* %this1 to %"class.std::__2::__vector_base.17"*
  %call5 = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__213__vector_baseIcNS_9allocatorIcEEE9__end_capEv(%"class.std::__2::__vector_base.17"* %6) #9
  %7 = load i8*, i8** %call5, align 4
  %8 = bitcast %"class.std::__2::vector.16"* %this1 to %"class.std::__2::__vector_base.17"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.17", %"class.std::__2::__vector_base.17"* %8, i32 0, i32 1
  %9 = load i8*, i8** %__end_, align 4
  %sub.ptr.lhs.cast = ptrtoint i8* %7 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %9 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %cmp6 = icmp sle i32 %5, %sub.ptr.sub
  br i1 %cmp6, label %if.then7, label %if.else

if.then7:                                         ; preds = %if.then
  %10 = load i32, i32* %__n, align 4
  store i32 %10, i32* %__old_n, align 4
  %11 = bitcast %"class.std::__2::vector.16"* %this1 to %"class.std::__2::__vector_base.17"*
  %__end_8 = getelementptr inbounds %"class.std::__2::__vector_base.17", %"class.std::__2::__vector_base.17"* %11, i32 0, i32 1
  %12 = load i8*, i8** %__end_8, align 4
  store i8* %12, i8** %__old_last, align 4
  %13 = load i8*, i8** %__last.addr, align 4
  store i8* %13, i8** %__m, align 4
  %14 = bitcast %"class.std::__2::vector.16"* %this1 to %"class.std::__2::__vector_base.17"*
  %__end_9 = getelementptr inbounds %"class.std::__2::__vector_base.17", %"class.std::__2::__vector_base.17"* %14, i32 0, i32 1
  %15 = load i8*, i8** %__end_9, align 4
  %16 = load i8*, i8** %__p, align 4
  %sub.ptr.lhs.cast10 = ptrtoint i8* %15 to i32
  %sub.ptr.rhs.cast11 = ptrtoint i8* %16 to i32
  %sub.ptr.sub12 = sub i32 %sub.ptr.lhs.cast10, %sub.ptr.rhs.cast11
  store i32 %sub.ptr.sub12, i32* %__dx, align 4
  %17 = load i32, i32* %__n, align 4
  %18 = load i32, i32* %__dx, align 4
  %cmp13 = icmp sgt i32 %17, %18
  br i1 %cmp13, label %if.then14, label %if.end

if.then14:                                        ; preds = %if.then7
  %19 = load i8*, i8** %__first.addr, align 4
  store i8* %19, i8** %__m, align 4
  %20 = bitcast %"class.std::__2::vector.16"* %this1 to %"class.std::__2::__vector_base.17"*
  %__end_15 = getelementptr inbounds %"class.std::__2::__vector_base.17", %"class.std::__2::__vector_base.17"* %20, i32 0, i32 1
  %21 = load i8*, i8** %__end_15, align 4
  %22 = load i8*, i8** %__p, align 4
  %sub.ptr.lhs.cast16 = ptrtoint i8* %21 to i32
  %sub.ptr.rhs.cast17 = ptrtoint i8* %22 to i32
  %sub.ptr.sub18 = sub i32 %sub.ptr.lhs.cast16, %sub.ptr.rhs.cast17
  store i32 %sub.ptr.sub18, i32* %__diff, align 4
  %23 = load i32, i32* %__diff, align 4
  call void @_ZNSt3__27advanceIPKhEEvRT_NS_15iterator_traitsIS3_E15difference_typeE(i8** nonnull align 4 dereferenceable(4) %__m, i32 %23)
  %24 = load i8*, i8** %__m, align 4
  %25 = load i8*, i8** %__last.addr, align 4
  %26 = load i32, i32* %__n, align 4
  %27 = load i32, i32* %__diff, align 4
  %sub = sub nsw i32 %26, %27
  call void @_ZNSt3__26vectorIcNS_9allocatorIcEEE18__construct_at_endIPKhEENS_9enable_ifIXsr27__is_cpp17_forward_iteratorIT_EE5valueEvE4typeES8_S8_m(%"class.std::__2::vector.16"* %this1, i8* %24, i8* %25, i32 %sub)
  %28 = load i32, i32* %__dx, align 4
  store i32 %28, i32* %__n, align 4
  br label %if.end

if.end:                                           ; preds = %if.then14, %if.then7
  %29 = load i32, i32* %__n, align 4
  %cmp19 = icmp sgt i32 %29, 0
  br i1 %cmp19, label %if.then20, label %if.end23

if.then20:                                        ; preds = %if.end
  %30 = load i8*, i8** %__p, align 4
  %31 = load i8*, i8** %__old_last, align 4
  %32 = load i8*, i8** %__p, align 4
  %33 = load i32, i32* %__old_n, align 4
  %add.ptr21 = getelementptr inbounds i8, i8* %32, i32 %33
  call void @_ZNSt3__26vectorIcNS_9allocatorIcEEE12__move_rangeEPcS4_S4_(%"class.std::__2::vector.16"* %this1, i8* %30, i8* %31, i8* %add.ptr21)
  %34 = load i8*, i8** %__first.addr, align 4
  %35 = load i8*, i8** %__m, align 4
  %36 = load i8*, i8** %__p, align 4
  %call22 = call i8* @_ZNSt3__24copyIPKhPcEET0_T_S5_S4_(i8* %34, i8* %35, i8* %36)
  br label %if.end23

if.end23:                                         ; preds = %if.then20, %if.end
  br label %if.end34

if.else:                                          ; preds = %if.then
  %37 = bitcast %"class.std::__2::vector.16"* %this1 to %"class.std::__2::__vector_base.17"*
  %call24 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.21"* @_ZNSt3__213__vector_baseIcNS_9allocatorIcEEE7__allocEv(%"class.std::__2::__vector_base.17"* %37) #9
  store %"class.std::__2::allocator.21"* %call24, %"class.std::__2::allocator.21"** %__a, align 4
  %call25 = call i32 @_ZNKSt3__26vectorIcNS_9allocatorIcEEE4sizeEv(%"class.std::__2::vector.16"* %this1) #9
  %38 = load i32, i32* %__n, align 4
  %add = add i32 %call25, %38
  %call26 = call i32 @_ZNKSt3__26vectorIcNS_9allocatorIcEEE11__recommendEm(%"class.std::__2::vector.16"* %this1, i32 %add)
  %39 = load i8*, i8** %__p, align 4
  %40 = bitcast %"class.std::__2::vector.16"* %this1 to %"class.std::__2::__vector_base.17"*
  %__begin_27 = getelementptr inbounds %"class.std::__2::__vector_base.17", %"class.std::__2::__vector_base.17"* %40, i32 0, i32 0
  %41 = load i8*, i8** %__begin_27, align 4
  %sub.ptr.lhs.cast28 = ptrtoint i8* %39 to i32
  %sub.ptr.rhs.cast29 = ptrtoint i8* %41 to i32
  %sub.ptr.sub30 = sub i32 %sub.ptr.lhs.cast28, %sub.ptr.rhs.cast29
  %42 = load %"class.std::__2::allocator.21"*, %"class.std::__2::allocator.21"** %__a, align 4
  %call31 = call %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEEC2EmmS3_(%"struct.std::__2::__split_buffer"* %__v, i32 %call26, i32 %sub.ptr.sub30, %"class.std::__2::allocator.21"* nonnull align 1 dereferenceable(1) %42)
  %43 = load i8*, i8** %__first.addr, align 4
  %44 = load i8*, i8** %__last.addr, align 4
  call void @_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE18__construct_at_endIPKhEENS_9enable_ifIXsr27__is_cpp17_forward_iteratorIT_EE5valueEvE4typeES9_S9_(%"struct.std::__2::__split_buffer"* %__v, i8* %43, i8* %44)
  %45 = load i8*, i8** %__p, align 4
  %call32 = call i8* @_ZNSt3__26vectorIcNS_9allocatorIcEEE26__swap_out_circular_bufferERNS_14__split_bufferIcRS2_EEPc(%"class.std::__2::vector.16"* %this1, %"struct.std::__2::__split_buffer"* nonnull align 4 dereferenceable(20) %__v, i8* %45)
  store i8* %call32, i8** %__p, align 4
  %call33 = call %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEED2Ev(%"struct.std::__2::__split_buffer"* %__v) #9
  br label %if.end34

if.end34:                                         ; preds = %if.else, %if.end23
  br label %if.end35

if.end35:                                         ; preds = %if.end34, %entry
  %46 = load i8*, i8** %__p, align 4
  %call36 = call i8* @_ZNSt3__26vectorIcNS_9allocatorIcEEE11__make_iterEPc(%"class.std::__2::vector.16"* %this1, i8* %46) #9
  %coerce.dive37 = getelementptr inbounds %"class.std::__2::__wrap_iter.41", %"class.std::__2::__wrap_iter.41"* %retval, i32 0, i32 0
  store i8* %call36, i8** %coerce.dive37, align 4
  %coerce.dive38 = getelementptr inbounds %"class.std::__2::__wrap_iter.41", %"class.std::__2::__wrap_iter.41"* %retval, i32 0, i32 0
  %47 = load i8*, i8** %coerce.dive38, align 4
  ret i8* %47
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__26vectorIcNS_9allocatorIcEEE3endEv(%"class.std::__2::vector.16"* %this) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__wrap_iter.41", align 4
  %this.addr = alloca %"class.std::__2::vector.16"*, align 4
  store %"class.std::__2::vector.16"* %this, %"class.std::__2::vector.16"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.16"*, %"class.std::__2::vector.16"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.16"* %this1 to %"class.std::__2::__vector_base.17"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.17", %"class.std::__2::__vector_base.17"* %0, i32 0, i32 1
  %1 = load i8*, i8** %__end_, align 4
  %call = call i8* @_ZNSt3__26vectorIcNS_9allocatorIcEEE11__make_iterEPc(%"class.std::__2::vector.16"* %this1, i8* %1) #9
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter.41", %"class.std::__2::__wrap_iter.41"* %retval, i32 0, i32 0
  store i8* %call, i8** %coerce.dive, align 4
  %coerce.dive2 = getelementptr inbounds %"class.std::__2::__wrap_iter.41", %"class.std::__2::__wrap_iter.41"* %retval, i32 0, i32 0
  %2 = load i8*, i8** %coerce.dive2, align 4
  ret i8* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__wrap_iter"* @_ZNSt3__211__wrap_iterIPKcEC2IPcEERKNS0_IT_EEPNS_9enable_ifIXsr14is_convertibleIS6_S2_EE5valueEvE4typeE(%"class.std::__2::__wrap_iter"* returned %this, %"class.std::__2::__wrap_iter.41"* nonnull align 4 dereferenceable(4) %__u, i8* %0) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__wrap_iter"*, align 4
  %__u.addr = alloca %"class.std::__2::__wrap_iter.41"*, align 4
  %.addr = alloca i8*, align 4
  store %"class.std::__2::__wrap_iter"* %this, %"class.std::__2::__wrap_iter"** %this.addr, align 4
  store %"class.std::__2::__wrap_iter.41"* %__u, %"class.std::__2::__wrap_iter.41"** %__u.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::__wrap_iter"*, %"class.std::__2::__wrap_iter"** %this.addr, align 4
  %__i = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::__wrap_iter.41"*, %"class.std::__2::__wrap_iter.41"** %__u.addr, align 4
  %call = call i8* @_ZNKSt3__211__wrap_iterIPcE4baseEv(%"class.std::__2::__wrap_iter.41"* %1) #9
  store i8* %call, i8** %__i, align 4
  ret %"class.std::__2::__wrap_iter"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__2miIPKcPcEEDTmicldtfp_4baseEcldtfp0_4baseEERKNS_11__wrap_iterIT_EERKNS5_IT0_EE(%"class.std::__2::__wrap_iter"* nonnull align 4 dereferenceable(4) %__x, %"class.std::__2::__wrap_iter.41"* nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %__x.addr = alloca %"class.std::__2::__wrap_iter"*, align 4
  %__y.addr = alloca %"class.std::__2::__wrap_iter.41"*, align 4
  store %"class.std::__2::__wrap_iter"* %__x, %"class.std::__2::__wrap_iter"** %__x.addr, align 4
  store %"class.std::__2::__wrap_iter.41"* %__y, %"class.std::__2::__wrap_iter.41"** %__y.addr, align 4
  %0 = load %"class.std::__2::__wrap_iter"*, %"class.std::__2::__wrap_iter"** %__x.addr, align 4
  %call = call i8* @_ZNKSt3__211__wrap_iterIPKcE4baseEv(%"class.std::__2::__wrap_iter"* %0) #9
  %1 = load %"class.std::__2::__wrap_iter.41"*, %"class.std::__2::__wrap_iter.41"** %__y.addr, align 4
  %call1 = call i8* @_ZNKSt3__211__wrap_iterIPcE4baseEv(%"class.std::__2::__wrap_iter.41"* %1) #9
  %sub.ptr.lhs.cast = ptrtoint i8* %call to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %call1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  ret i32 %sub.ptr.sub
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__26vectorIcNS_9allocatorIcEEE5beginEv(%"class.std::__2::vector.16"* %this) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__wrap_iter.41", align 4
  %this.addr = alloca %"class.std::__2::vector.16"*, align 4
  store %"class.std::__2::vector.16"* %this, %"class.std::__2::vector.16"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.16"*, %"class.std::__2::vector.16"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.16"* %this1 to %"class.std::__2::__vector_base.17"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.17", %"class.std::__2::__vector_base.17"* %0, i32 0, i32 0
  %1 = load i8*, i8** %__begin_, align 4
  %call = call i8* @_ZNSt3__26vectorIcNS_9allocatorIcEEE11__make_iterEPc(%"class.std::__2::vector.16"* %this1, i8* %1) #9
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter.41", %"class.std::__2::__wrap_iter.41"* %retval, i32 0, i32 0
  store i8* %call, i8** %coerce.dive, align 4
  %coerce.dive2 = getelementptr inbounds %"class.std::__2::__wrap_iter.41", %"class.std::__2::__wrap_iter.41"* %retval, i32 0, i32 0
  %2 = load i8*, i8** %coerce.dive2, align 4
  ret i8* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__28distanceIPKhEENS_15iterator_traitsIT_E15difference_typeES4_S4_(i8* %__first, i8* %__last) #0 comdat {
entry:
  %__first.addr = alloca i8*, align 4
  %__last.addr = alloca i8*, align 4
  %agg.tmp = alloca %"struct.std::__2::random_access_iterator_tag", align 1
  store i8* %__first, i8** %__first.addr, align 4
  store i8* %__last, i8** %__last.addr, align 4
  %0 = load i8*, i8** %__first.addr, align 4
  %1 = load i8*, i8** %__last.addr, align 4
  %call = call i32 @_ZNSt3__210__distanceIPKhEENS_15iterator_traitsIT_E15difference_typeES4_S4_NS_26random_access_iterator_tagE(i8* %0, i8* %1)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNSt3__213__vector_baseIcNS_9allocatorIcEEE9__end_capEv(%"class.std::__2::__vector_base.17"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.17"*, align 4
  store %"class.std::__2::__vector_base.17"* %this, %"class.std::__2::__vector_base.17"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.17"*, %"class.std::__2::__vector_base.17"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.17", %"class.std::__2::__vector_base.17"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__217__compressed_pairIPcNS_9allocatorIcEEE5firstEv(%"class.std::__2::__compressed_pair.18"* %__end_cap_) #9
  ret i8** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__27advanceIPKhEEvRT_NS_15iterator_traitsIS3_E15difference_typeE(i8** nonnull align 4 dereferenceable(4) %__i, i32 %__n) #0 comdat {
entry:
  %__i.addr = alloca i8**, align 4
  %__n.addr = alloca i32, align 4
  %agg.tmp = alloca %"struct.std::__2::random_access_iterator_tag", align 1
  store i8** %__i, i8*** %__i.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load i8**, i8*** %__i.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29__advanceIPKhEEvRT_NS_15iterator_traitsIS3_E15difference_typeENS_26random_access_iterator_tagE(i8** nonnull align 4 dereferenceable(4) %0, i32 %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIcNS_9allocatorIcEEE18__construct_at_endIPKhEENS_9enable_ifIXsr27__is_cpp17_forward_iteratorIT_EE5valueEvE4typeES8_S8_m(%"class.std::__2::vector.16"* %this, i8* %__first, i8* %__last, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.16"*, align 4
  %__first.addr = alloca i8*, align 4
  %__last.addr = alloca i8*, align 4
  %__n.addr = alloca i32, align 4
  %__tx = alloca %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction", align 4
  store %"class.std::__2::vector.16"* %this, %"class.std::__2::vector.16"** %this.addr, align 4
  store i8* %__first, i8** %__first.addr, align 4
  store i8* %__last, i8** %__last.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.16"*, %"class.std::__2::vector.16"** %this.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %call = call %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"* @_ZNSt3__26vectorIcNS_9allocatorIcEEE21_ConstructTransactionC2ERS3_m(%"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"* %__tx, %"class.std::__2::vector.16"* nonnull align 4 dereferenceable(12) %this1, i32 %0)
  %1 = bitcast %"class.std::__2::vector.16"* %this1 to %"class.std::__2::__vector_base.17"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.21"* @_ZNSt3__213__vector_baseIcNS_9allocatorIcEEE7__allocEv(%"class.std::__2::__vector_base.17"* %1) #9
  %2 = load i8*, i8** %__first.addr, align 4
  %3 = load i8*, i8** %__last.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction", %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE25__construct_range_forwardIPKhPcEEvRS2_T_S9_RT0_(%"class.std::__2::allocator.21"* nonnull align 1 dereferenceable(1) %call2, i8* %2, i8* %3, i8** nonnull align 4 dereferenceable(4) %__pos_)
  %call3 = call %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"* @_ZNSt3__26vectorIcNS_9allocatorIcEEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"* %__tx) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIcNS_9allocatorIcEEE12__move_rangeEPcS4_S4_(%"class.std::__2::vector.16"* %this, i8* %__from_s, i8* %__from_e, i8* %__to) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.16"*, align 4
  %__from_s.addr = alloca i8*, align 4
  %__from_e.addr = alloca i8*, align 4
  %__to.addr = alloca i8*, align 4
  %__old_last = alloca i8*, align 4
  %__n = alloca i32, align 4
  %__i = alloca i8*, align 4
  %__tx = alloca %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction", align 4
  store %"class.std::__2::vector.16"* %this, %"class.std::__2::vector.16"** %this.addr, align 4
  store i8* %__from_s, i8** %__from_s.addr, align 4
  store i8* %__from_e, i8** %__from_e.addr, align 4
  store i8* %__to, i8** %__to.addr, align 4
  %this1 = load %"class.std::__2::vector.16"*, %"class.std::__2::vector.16"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.16"* %this1 to %"class.std::__2::__vector_base.17"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.17", %"class.std::__2::__vector_base.17"* %0, i32 0, i32 1
  %1 = load i8*, i8** %__end_, align 4
  store i8* %1, i8** %__old_last, align 4
  %2 = load i8*, i8** %__old_last, align 4
  %3 = load i8*, i8** %__to.addr, align 4
  %sub.ptr.lhs.cast = ptrtoint i8* %2 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  store i32 %sub.ptr.sub, i32* %__n, align 4
  %4 = load i8*, i8** %__from_s.addr, align 4
  %5 = load i32, i32* %__n, align 4
  %add.ptr = getelementptr inbounds i8, i8* %4, i32 %5
  store i8* %add.ptr, i8** %__i, align 4
  %6 = load i8*, i8** %__from_e.addr, align 4
  %7 = load i8*, i8** %__i, align 4
  %sub.ptr.lhs.cast2 = ptrtoint i8* %6 to i32
  %sub.ptr.rhs.cast3 = ptrtoint i8* %7 to i32
  %sub.ptr.sub4 = sub i32 %sub.ptr.lhs.cast2, %sub.ptr.rhs.cast3
  %call = call %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"* @_ZNSt3__26vectorIcNS_9allocatorIcEEE21_ConstructTransactionC2ERS3_m(%"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"* %__tx, %"class.std::__2::vector.16"* nonnull align 4 dereferenceable(12) %this1, i32 %sub.ptr.sub4)
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %8 = load i8*, i8** %__i, align 4
  %9 = load i8*, i8** %__from_e.addr, align 4
  %cmp = icmp ult i8* %8, %9
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %10 = bitcast %"class.std::__2::vector.16"* %this1 to %"class.std::__2::__vector_base.17"*
  %call5 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.21"* @_ZNSt3__213__vector_baseIcNS_9allocatorIcEEE7__allocEv(%"class.std::__2::__vector_base.17"* %10) #9
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction", %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %11 = load i8*, i8** %__pos_, align 4
  %call6 = call i8* @_ZNSt3__212__to_addressIcEEPT_S2_(i8* %11) #9
  %12 = load i8*, i8** %__i, align 4
  %call7 = call nonnull align 1 dereferenceable(1) i8* @_ZNSt3__24moveIRcEEONS_16remove_referenceIT_E4typeEOS3_(i8* nonnull align 1 dereferenceable(1) %12) #9
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE9constructIcJcEEEvRS2_PT_DpOT0_(%"class.std::__2::allocator.21"* nonnull align 1 dereferenceable(1) %call5, i8* %call6, i8* nonnull align 1 dereferenceable(1) %call7)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %13 = load i8*, i8** %__i, align 4
  %incdec.ptr = getelementptr inbounds i8, i8* %13, i32 1
  store i8* %incdec.ptr, i8** %__i, align 4
  %__pos_8 = getelementptr inbounds %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction", %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %14 = load i8*, i8** %__pos_8, align 4
  %incdec.ptr9 = getelementptr inbounds i8, i8* %14, i32 1
  store i8* %incdec.ptr9, i8** %__pos_8, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call10 = call %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"* @_ZNSt3__26vectorIcNS_9allocatorIcEEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"* %__tx) #9
  %15 = load i8*, i8** %__from_s.addr, align 4
  %16 = load i8*, i8** %__from_s.addr, align 4
  %17 = load i32, i32* %__n, align 4
  %add.ptr11 = getelementptr inbounds i8, i8* %16, i32 %17
  %18 = load i8*, i8** %__old_last, align 4
  %call12 = call i8* @_ZNSt3__213move_backwardIPcS1_EET0_T_S3_S2_(i8* %15, i8* %add.ptr11, i8* %18)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__24copyIPKhPcEET0_T_S5_S4_(i8* %__first, i8* %__last, i8* %__result) #0 comdat {
entry:
  %__first.addr = alloca i8*, align 4
  %__last.addr = alloca i8*, align 4
  %__result.addr = alloca i8*, align 4
  store i8* %__first, i8** %__first.addr, align 4
  store i8* %__last, i8** %__last.addr, align 4
  store i8* %__result, i8** %__result.addr, align 4
  %0 = load i8*, i8** %__first.addr, align 4
  %call = call i8* @_ZNSt3__213__unwrap_iterIPKhEET_S3_(i8* %0)
  %1 = load i8*, i8** %__last.addr, align 4
  %call1 = call i8* @_ZNSt3__213__unwrap_iterIPKhEET_S3_(i8* %1)
  %2 = load i8*, i8** %__result.addr, align 4
  %call2 = call i8* @_ZNSt3__213__unwrap_iterIPcEET_S2_(i8* %2)
  %call3 = call i8* @_ZNSt3__26__copyIPKhPcEET0_T_S5_S4_(i8* %call, i8* %call1, i8* %call2)
  ret i8* %call3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.21"* @_ZNSt3__213__vector_baseIcNS_9allocatorIcEEE7__allocEv(%"class.std::__2::__vector_base.17"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.17"*, align 4
  store %"class.std::__2::__vector_base.17"* %this, %"class.std::__2::__vector_base.17"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.17"*, %"class.std::__2::__vector_base.17"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.17", %"class.std::__2::__vector_base.17"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.21"* @_ZNSt3__217__compressed_pairIPcNS_9allocatorIcEEE6secondEv(%"class.std::__2::__compressed_pair.18"* %__end_cap_) #9
  ret %"class.std::__2::allocator.21"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIcNS_9allocatorIcEEE11__recommendEm(%"class.std::__2::vector.16"* %this, i32 %__new_size) #0 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %"class.std::__2::vector.16"*, align 4
  %__new_size.addr = alloca i32, align 4
  %__ms = alloca i32, align 4
  %__cap = alloca i32, align 4
  %ref.tmp = alloca i32, align 4
  store %"class.std::__2::vector.16"* %this, %"class.std::__2::vector.16"** %this.addr, align 4
  store i32 %__new_size, i32* %__new_size.addr, align 4
  %this1 = load %"class.std::__2::vector.16"*, %"class.std::__2::vector.16"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIcNS_9allocatorIcEEE8max_sizeEv(%"class.std::__2::vector.16"* %this1) #9
  store i32 %call, i32* %__ms, align 4
  %0 = load i32, i32* %__new_size.addr, align 4
  %1 = load i32, i32* %__ms, align 4
  %cmp = icmp ugt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = bitcast %"class.std::__2::vector.16"* %this1 to %"class.std::__2::__vector_base_common"*
  call void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"* %2) #12
  unreachable

if.end:                                           ; preds = %entry
  %call2 = call i32 @_ZNKSt3__26vectorIcNS_9allocatorIcEEE8capacityEv(%"class.std::__2::vector.16"* %this1) #9
  store i32 %call2, i32* %__cap, align 4
  %3 = load i32, i32* %__cap, align 4
  %4 = load i32, i32* %__ms, align 4
  %div = udiv i32 %4, 2
  %cmp3 = icmp uge i32 %3, %div
  br i1 %cmp3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %if.end
  %5 = load i32, i32* %__ms, align 4
  store i32 %5, i32* %retval, align 4
  br label %return

if.end5:                                          ; preds = %if.end
  %6 = load i32, i32* %__cap, align 4
  %mul = mul i32 2, %6
  store i32 %mul, i32* %ref.tmp, align 4
  %call6 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %__new_size.addr)
  %7 = load i32, i32* %call6, align 4
  store i32 %7, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end5, %if.then4
  %8 = load i32, i32* %retval, align 4
  ret i32 %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIcNS_9allocatorIcEEE4sizeEv(%"class.std::__2::vector.16"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.16"*, align 4
  store %"class.std::__2::vector.16"* %this, %"class.std::__2::vector.16"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.16"*, %"class.std::__2::vector.16"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.16"* %this1 to %"class.std::__2::__vector_base.17"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.17", %"class.std::__2::__vector_base.17"* %0, i32 0, i32 1
  %1 = load i8*, i8** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector.16"* %this1 to %"class.std::__2::__vector_base.17"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.17", %"class.std::__2::__vector_base.17"* %2, i32 0, i32 0
  %3 = load i8*, i8** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint i8* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  ret i32 %sub.ptr.sub
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEEC2EmmS3_(%"struct.std::__2::__split_buffer"* returned %this, i32 %__cap, i32 %__start, %"class.std::__2::allocator.21"* nonnull align 1 dereferenceable(1) %__a) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"struct.std::__2::__split_buffer"*, align 4
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__cap.addr = alloca i32, align 4
  %__start.addr = alloca i32, align 4
  %__a.addr = alloca %"class.std::__2::allocator.21"*, align 4
  %ref.tmp = alloca i8*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store i32 %__cap, i32* %__cap.addr, align 4
  store i32 %__start, i32* %__start.addr, align 4
  store %"class.std::__2::allocator.21"* %__a, %"class.std::__2::allocator.21"** %__a.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer"* %this1, %"struct.std::__2::__split_buffer"** %retval, align 4
  %0 = bitcast %"struct.std::__2::__split_buffer"* %this1 to %"class.std::__2::__split_buffer_common"*
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  store i8* null, i8** %ref.tmp, align 4
  %1 = load %"class.std::__2::allocator.21"*, %"class.std::__2::allocator.21"** %__a.addr, align 4
  %call = call %"class.std::__2::__compressed_pair.42"* @_ZNSt3__217__compressed_pairIPcRNS_9allocatorIcEEEC2IDnS4_EEOT_OT0_(%"class.std::__2::__compressed_pair.42"* %__end_cap_, i8** nonnull align 4 dereferenceable(4) %ref.tmp, %"class.std::__2::allocator.21"* nonnull align 1 dereferenceable(1) %1)
  %2 = load i32, i32* %__cap.addr, align 4
  %cmp = icmp ne i32 %2, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.21"* @_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE7__allocEv(%"struct.std::__2::__split_buffer"* %this1) #9
  %3 = load i32, i32* %__cap.addr, align 4
  %call3 = call i8* @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE8allocateERS2_m(%"class.std::__2::allocator.21"* nonnull align 1 dereferenceable(1) %call2, i32 %3)
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i8* [ %call3, %cond.true ], [ null, %cond.false ]
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  store i8* %cond, i8** %__first_, align 4
  %__first_4 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %4 = load i8*, i8** %__first_4, align 4
  %5 = load i32, i32* %__start.addr, align 4
  %add.ptr = getelementptr inbounds i8, i8* %4, i32 %5
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 2
  store i8* %add.ptr, i8** %__end_, align 4
  %__begin_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 1
  store i8* %add.ptr, i8** %__begin_, align 4
  %__first_5 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %6 = load i8*, i8** %__first_5, align 4
  %7 = load i32, i32* %__cap.addr, align 4
  %add.ptr6 = getelementptr inbounds i8, i8* %6, i32 %7
  %call7 = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this1) #9
  store i8* %add.ptr6, i8** %call7, align 4
  %8 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %retval, align 4
  ret %"struct.std::__2::__split_buffer"* %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE18__construct_at_endIPKhEENS_9enable_ifIXsr27__is_cpp17_forward_iteratorIT_EE5valueEvE4typeES9_S9_(%"struct.std::__2::__split_buffer"* %this, i8* %__first, i8* %__last) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__first.addr = alloca i8*, align 4
  %__last.addr = alloca i8*, align 4
  %__tx = alloca %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction", align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store i8* %__first, i8** %__first.addr, align 4
  store i8* %__last, i8** %__last.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 2
  %0 = load i8*, i8** %__first.addr, align 4
  %1 = load i8*, i8** %__last.addr, align 4
  %call = call i32 @_ZNSt3__28distanceIPKhEENS_15iterator_traitsIT_E15difference_typeES4_S4_(i8* %0, i8* %1)
  %call2 = call %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE21_ConstructTransactionC2EPPcm(%"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"* %__tx, i8** %__end_, i32 %call) #9
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %__pos_ = getelementptr inbounds %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"* %__tx, i32 0, i32 0
  %2 = load i8*, i8** %__pos_, align 4
  %__end_3 = getelementptr inbounds %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %3 = load i8*, i8** %__end_3, align 4
  %cmp = icmp ne i8* %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %call4 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.21"* @_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE7__allocEv(%"struct.std::__2::__split_buffer"* %this1) #9
  %__pos_5 = getelementptr inbounds %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"* %__tx, i32 0, i32 0
  %4 = load i8*, i8** %__pos_5, align 4
  %call6 = call i8* @_ZNSt3__212__to_addressIcEEPT_S2_(i8* %4) #9
  %5 = load i8*, i8** %__first.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE9constructIcJRKhEEEvRS2_PT_DpOT0_(%"class.std::__2::allocator.21"* nonnull align 1 dereferenceable(1) %call4, i8* %call6, i8* nonnull align 1 dereferenceable(1) %5)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %__pos_7 = getelementptr inbounds %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"* %__tx, i32 0, i32 0
  %6 = load i8*, i8** %__pos_7, align 4
  %incdec.ptr = getelementptr inbounds i8, i8* %6, i32 1
  store i8* %incdec.ptr, i8** %__pos_7, align 4
  %7 = load i8*, i8** %__first.addr, align 4
  %incdec.ptr8 = getelementptr inbounds i8, i8* %7, i32 1
  store i8* %incdec.ptr8, i8** %__first.addr, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call9 = call %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE21_ConstructTransactionD2Ev(%"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"* %__tx) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__26vectorIcNS_9allocatorIcEEE26__swap_out_circular_bufferERNS_14__split_bufferIcRS2_EEPc(%"class.std::__2::vector.16"* %this, %"struct.std::__2::__split_buffer"* nonnull align 4 dereferenceable(20) %__v, i8* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.16"*, align 4
  %__v.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__p.addr = alloca i8*, align 4
  %__r = alloca i8*, align 4
  store %"class.std::__2::vector.16"* %this, %"class.std::__2::vector.16"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer"* %__v, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %this1 = load %"class.std::__2::vector.16"*, %"class.std::__2::vector.16"** %this.addr, align 4
  call void @_ZNKSt3__26vectorIcNS_9allocatorIcEEE17__annotate_deleteEv(%"class.std::__2::vector.16"* %this1) #9
  %0 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__begin_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %0, i32 0, i32 1
  %1 = load i8*, i8** %__begin_, align 4
  store i8* %1, i8** %__r, align 4
  %2 = bitcast %"class.std::__2::vector.16"* %this1 to %"class.std::__2::__vector_base.17"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.21"* @_ZNSt3__213__vector_baseIcNS_9allocatorIcEEE7__allocEv(%"class.std::__2::__vector_base.17"* %2) #9
  %3 = bitcast %"class.std::__2::vector.16"* %this1 to %"class.std::__2::__vector_base.17"*
  %__begin_2 = getelementptr inbounds %"class.std::__2::__vector_base.17", %"class.std::__2::__vector_base.17"* %3, i32 0, i32 0
  %4 = load i8*, i8** %__begin_2, align 4
  %5 = load i8*, i8** %__p.addr, align 4
  %6 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__begin_3 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %6, i32 0, i32 1
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE46__construct_backward_with_exception_guaranteesIcEENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS2_PT_S8_EE5valuesr31is_trivially_move_constructibleIS8_EE5valueEvE4typeERS2_S9_S9_RS9_(%"class.std::__2::allocator.21"* nonnull align 1 dereferenceable(1) %call, i8* %4, i8* %5, i8** nonnull align 4 dereferenceable(4) %__begin_3)
  %7 = bitcast %"class.std::__2::vector.16"* %this1 to %"class.std::__2::__vector_base.17"*
  %call4 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.21"* @_ZNSt3__213__vector_baseIcNS_9allocatorIcEEE7__allocEv(%"class.std::__2::__vector_base.17"* %7) #9
  %8 = load i8*, i8** %__p.addr, align 4
  %9 = bitcast %"class.std::__2::vector.16"* %this1 to %"class.std::__2::__vector_base.17"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.17", %"class.std::__2::__vector_base.17"* %9, i32 0, i32 1
  %10 = load i8*, i8** %__end_, align 4
  %11 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__end_5 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %11, i32 0, i32 2
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE45__construct_forward_with_exception_guaranteesIcEENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS2_PT_S8_EE5valuesr31is_trivially_move_constructibleIS8_EE5valueEvE4typeERS2_S9_S9_RS9_(%"class.std::__2::allocator.21"* nonnull align 1 dereferenceable(1) %call4, i8* %8, i8* %10, i8** nonnull align 4 dereferenceable(4) %__end_5)
  %12 = bitcast %"class.std::__2::vector.16"* %this1 to %"class.std::__2::__vector_base.17"*
  %__begin_6 = getelementptr inbounds %"class.std::__2::__vector_base.17", %"class.std::__2::__vector_base.17"* %12, i32 0, i32 0
  %13 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__begin_7 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %13, i32 0, i32 1
  call void @_ZNSt3__24swapIPcEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_(i8** nonnull align 4 dereferenceable(4) %__begin_6, i8** nonnull align 4 dereferenceable(4) %__begin_7) #9
  %14 = bitcast %"class.std::__2::vector.16"* %this1 to %"class.std::__2::__vector_base.17"*
  %__end_8 = getelementptr inbounds %"class.std::__2::__vector_base.17", %"class.std::__2::__vector_base.17"* %14, i32 0, i32 1
  %15 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__end_9 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %15, i32 0, i32 2
  call void @_ZNSt3__24swapIPcEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_(i8** nonnull align 4 dereferenceable(4) %__end_8, i8** nonnull align 4 dereferenceable(4) %__end_9) #9
  %16 = bitcast %"class.std::__2::vector.16"* %this1 to %"class.std::__2::__vector_base.17"*
  %call10 = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__213__vector_baseIcNS_9allocatorIcEEE9__end_capEv(%"class.std::__2::__vector_base.17"* %16) #9
  %17 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE9__end_capEv(%"struct.std::__2::__split_buffer"* %17) #9
  call void @_ZNSt3__24swapIPcEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_(i8** nonnull align 4 dereferenceable(4) %call10, i8** nonnull align 4 dereferenceable(4) %call11) #9
  %18 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__begin_12 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %18, i32 0, i32 1
  %19 = load i8*, i8** %__begin_12, align 4
  %20 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %20, i32 0, i32 0
  store i8* %19, i8** %__first_, align 4
  %call13 = call i32 @_ZNKSt3__26vectorIcNS_9allocatorIcEEE4sizeEv(%"class.std::__2::vector.16"* %this1) #9
  call void @_ZNKSt3__26vectorIcNS_9allocatorIcEEE14__annotate_newEm(%"class.std::__2::vector.16"* %this1, i32 %call13) #9
  call void @_ZNSt3__26vectorIcNS_9allocatorIcEEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector.16"* %this1)
  %21 = load i8*, i8** %__r, align 4
  ret i8* %21
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEED2Ev(%"struct.std::__2::__split_buffer"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"struct.std::__2::__split_buffer"*, align 4
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer"* %this1, %"struct.std::__2::__split_buffer"** %retval, align 4
  call void @_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE5clearEv(%"struct.std::__2::__split_buffer"* %this1) #9
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %0 = load i8*, i8** %__first_, align 4
  %tobool = icmp ne i8* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.21"* @_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE7__allocEv(%"struct.std::__2::__split_buffer"* %this1) #9
  %__first_2 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %1 = load i8*, i8** %__first_2, align 4
  %call3 = call i32 @_ZNKSt3__214__split_bufferIcRNS_9allocatorIcEEE8capacityEv(%"struct.std::__2::__split_buffer"* %this1)
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE10deallocateERS2_Pcm(%"class.std::__2::allocator.21"* nonnull align 1 dereferenceable(1) %call, i8* %1, i32 %call3) #9
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %retval, align 4
  ret %"struct.std::__2::__split_buffer"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__26vectorIcNS_9allocatorIcEEE11__make_iterEPc(%"class.std::__2::vector.16"* %this, i8* %__p) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__wrap_iter.41", align 4
  %this.addr = alloca %"class.std::__2::vector.16"*, align 4
  %__p.addr = alloca i8*, align 4
  store %"class.std::__2::vector.16"* %this, %"class.std::__2::vector.16"** %this.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %this1 = load %"class.std::__2::vector.16"*, %"class.std::__2::vector.16"** %this.addr, align 4
  %0 = load i8*, i8** %__p.addr, align 4
  %call = call %"class.std::__2::__wrap_iter.41"* @_ZNSt3__211__wrap_iterIPcEC2ES1_(%"class.std::__2::__wrap_iter.41"* %retval, i8* %0) #9
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter.41", %"class.std::__2::__wrap_iter.41"* %retval, i32 0, i32 0
  %1 = load i8*, i8** %coerce.dive, align 4
  ret i8* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__211__wrap_iterIPKcE4baseEv(%"class.std::__2::__wrap_iter"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__wrap_iter"*, align 4
  store %"class.std::__2::__wrap_iter"* %this, %"class.std::__2::__wrap_iter"** %this.addr, align 4
  %this1 = load %"class.std::__2::__wrap_iter"*, %"class.std::__2::__wrap_iter"** %this.addr, align 4
  %__i = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %this1, i32 0, i32 0
  %0 = load i8*, i8** %__i, align 4
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__211__wrap_iterIPcE4baseEv(%"class.std::__2::__wrap_iter.41"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__wrap_iter.41"*, align 4
  store %"class.std::__2::__wrap_iter.41"* %this, %"class.std::__2::__wrap_iter.41"** %this.addr, align 4
  %this1 = load %"class.std::__2::__wrap_iter.41"*, %"class.std::__2::__wrap_iter.41"** %this.addr, align 4
  %__i = getelementptr inbounds %"class.std::__2::__wrap_iter.41", %"class.std::__2::__wrap_iter.41"* %this1, i32 0, i32 0
  %0 = load i8*, i8** %__i, align 4
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__210__distanceIPKhEENS_15iterator_traitsIT_E15difference_typeES4_S4_NS_26random_access_iterator_tagE(i8* %__first, i8* %__last) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::random_access_iterator_tag", align 1
  %__first.addr = alloca i8*, align 4
  %__last.addr = alloca i8*, align 4
  store i8* %__first, i8** %__first.addr, align 4
  store i8* %__last, i8** %__last.addr, align 4
  %1 = load i8*, i8** %__last.addr, align 4
  %2 = load i8*, i8** %__first.addr, align 4
  %sub.ptr.lhs.cast = ptrtoint i8* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %2 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  ret i32 %sub.ptr.sub
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNSt3__217__compressed_pairIPcNS_9allocatorIcEEE5firstEv(%"class.std::__2::__compressed_pair.18"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.18"*, align 4
  store %"class.std::__2::__compressed_pair.18"* %this, %"class.std::__2::__compressed_pair.18"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.18"*, %"class.std::__2::__compressed_pair.18"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.18"* %this1 to %"struct.std::__2::__compressed_pair_elem.19"*
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__222__compressed_pair_elemIPcLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.19"* %0) #9
  ret i8** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNSt3__222__compressed_pair_elemIPcLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.19"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.19"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.19"* %this, %"struct.std::__2::__compressed_pair_elem.19"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.19"*, %"struct.std::__2::__compressed_pair_elem.19"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.19", %"struct.std::__2::__compressed_pair_elem.19"* %this1, i32 0, i32 0
  ret i8** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29__advanceIPKhEEvRT_NS_15iterator_traitsIS3_E15difference_typeENS_26random_access_iterator_tagE(i8** nonnull align 4 dereferenceable(4) %__i, i32 %__n) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::random_access_iterator_tag", align 1
  %__i.addr = alloca i8**, align 4
  %__n.addr = alloca i32, align 4
  store i8** %__i, i8*** %__i.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %2 = load i8**, i8*** %__i.addr, align 4
  %3 = load i8*, i8** %2, align 4
  %add.ptr = getelementptr inbounds i8, i8* %3, i32 %1
  store i8* %add.ptr, i8** %2, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"* @_ZNSt3__26vectorIcNS_9allocatorIcEEE21_ConstructTransactionC2ERS3_m(%"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"* returned %this, %"class.std::__2::vector.16"* nonnull align 4 dereferenceable(12) %__v, i32 %__n) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"*, align 4
  %__v.addr = alloca %"class.std::__2::vector.16"*, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"** %this.addr, align 4
  store %"class.std::__2::vector.16"* %__v, %"class.std::__2::vector.16"** %__v.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"*, %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"** %this.addr, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction", %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::vector.16"*, %"class.std::__2::vector.16"** %__v.addr, align 4
  store %"class.std::__2::vector.16"* %0, %"class.std::__2::vector.16"** %__v_, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction", %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %1 = load %"class.std::__2::vector.16"*, %"class.std::__2::vector.16"** %__v.addr, align 4
  %2 = bitcast %"class.std::__2::vector.16"* %1 to %"class.std::__2::__vector_base.17"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.17", %"class.std::__2::__vector_base.17"* %2, i32 0, i32 1
  %3 = load i8*, i8** %__end_, align 4
  store i8* %3, i8** %__pos_, align 4
  %__new_end_ = getelementptr inbounds %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction", %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"* %this1, i32 0, i32 2
  %4 = load %"class.std::__2::vector.16"*, %"class.std::__2::vector.16"** %__v.addr, align 4
  %5 = bitcast %"class.std::__2::vector.16"* %4 to %"class.std::__2::__vector_base.17"*
  %__end_2 = getelementptr inbounds %"class.std::__2::__vector_base.17", %"class.std::__2::__vector_base.17"* %5, i32 0, i32 1
  %6 = load i8*, i8** %__end_2, align 4
  %7 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds i8, i8* %6, i32 %7
  store i8* %add.ptr, i8** %__new_end_, align 4
  ret %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE25__construct_range_forwardIPKhPcEEvRS2_T_S9_RT0_(%"class.std::__2::allocator.21"* nonnull align 1 dereferenceable(1) %__a, i8* %__begin1, i8* %__end1, i8** nonnull align 4 dereferenceable(4) %__begin2) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.21"*, align 4
  %__begin1.addr = alloca i8*, align 4
  %__end1.addr = alloca i8*, align 4
  %__begin2.addr = alloca i8**, align 4
  store %"class.std::__2::allocator.21"* %__a, %"class.std::__2::allocator.21"** %__a.addr, align 4
  store i8* %__begin1, i8** %__begin1.addr, align 4
  store i8* %__end1, i8** %__end1.addr, align 4
  store i8** %__begin2, i8*** %__begin2.addr, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i8*, i8** %__begin1.addr, align 4
  %1 = load i8*, i8** %__end1.addr, align 4
  %cmp = icmp ne i8* %0, %1
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load %"class.std::__2::allocator.21"*, %"class.std::__2::allocator.21"** %__a.addr, align 4
  %3 = load i8**, i8*** %__begin2.addr, align 4
  %4 = load i8*, i8** %3, align 4
  %call = call i8* @_ZNSt3__212__to_addressIcEEPT_S2_(i8* %4) #9
  %5 = load i8*, i8** %__begin1.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE9constructIcJRKhEEEvRS2_PT_DpOT0_(%"class.std::__2::allocator.21"* nonnull align 1 dereferenceable(1) %2, i8* %call, i8* nonnull align 1 dereferenceable(1) %5)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i8*, i8** %__begin1.addr, align 4
  %incdec.ptr = getelementptr inbounds i8, i8* %6, i32 1
  store i8* %incdec.ptr, i8** %__begin1.addr, align 4
  %7 = load i8**, i8*** %__begin2.addr, align 4
  %8 = load i8*, i8** %7, align 4
  %incdec.ptr1 = getelementptr inbounds i8, i8* %8, i32 1
  store i8* %incdec.ptr1, i8** %7, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"* @_ZNSt3__26vectorIcNS_9allocatorIcEEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"*, align 4
  store %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"** %this.addr, align 4
  %this1 = load %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"*, %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction", %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %0 = load i8*, i8** %__pos_, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction", %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::vector.16"*, %"class.std::__2::vector.16"** %__v_, align 4
  %2 = bitcast %"class.std::__2::vector.16"* %1 to %"class.std::__2::__vector_base.17"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.17", %"class.std::__2::__vector_base.17"* %2, i32 0, i32 1
  store i8* %0, i8** %__end_, align 4
  ret %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE9constructIcJRKhEEEvRS2_PT_DpOT0_(%"class.std::__2::allocator.21"* nonnull align 1 dereferenceable(1) %__a, i8* %__p, i8* nonnull align 1 dereferenceable(1) %__args) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.21"*, align 4
  %__p.addr = alloca i8*, align 4
  %__args.addr = alloca i8*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_construct", align 1
  store %"class.std::__2::allocator.21"* %__a, %"class.std::__2::allocator.21"** %__a.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  store i8* %__args, i8** %__args.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_construct"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.21"*, %"class.std::__2::allocator.21"** %__a.addr, align 4
  %2 = load i8*, i8** %__p.addr, align 4
  %3 = load i8*, i8** %__args.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) i8* @_ZNSt3__27forwardIRKhEEOT_RNS_16remove_referenceIS3_E4typeE(i8* nonnull align 1 dereferenceable(1) %3) #9
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE11__constructIcJRKhEEEvNS_17integral_constantIbLb1EEERS2_PT_DpOT0_(%"class.std::__2::allocator.21"* nonnull align 1 dereferenceable(1) %1, i8* %2, i8* nonnull align 1 dereferenceable(1) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__212__to_addressIcEEPT_S2_(i8* %__p) #0 comdat {
entry:
  %__p.addr = alloca i8*, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %0 = load i8*, i8** %__p.addr, align 4
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE11__constructIcJRKhEEEvNS_17integral_constantIbLb1EEERS2_PT_DpOT0_(%"class.std::__2::allocator.21"* nonnull align 1 dereferenceable(1) %__a, i8* %__p, i8* nonnull align 1 dereferenceable(1) %__args) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.21"*, align 4
  %__p.addr = alloca i8*, align 4
  %__args.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.21"* %__a, %"class.std::__2::allocator.21"** %__a.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  store i8* %__args, i8** %__args.addr, align 4
  %1 = load %"class.std::__2::allocator.21"*, %"class.std::__2::allocator.21"** %__a.addr, align 4
  %2 = load i8*, i8** %__p.addr, align 4
  %3 = load i8*, i8** %__args.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) i8* @_ZNSt3__27forwardIRKhEEOT_RNS_16remove_referenceIS3_E4typeE(i8* nonnull align 1 dereferenceable(1) %3) #9
  call void @_ZNSt3__29allocatorIcE9constructIcJRKhEEEvPT_DpOT0_(%"class.std::__2::allocator.21"* %1, i8* %2, i8* nonnull align 1 dereferenceable(1) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) i8* @_ZNSt3__27forwardIRKhEEOT_RNS_16remove_referenceIS3_E4typeE(i8* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca i8*, align 4
  store i8* %__t, i8** %__t.addr, align 4
  %0 = load i8*, i8** %__t.addr, align 4
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIcE9constructIcJRKhEEEvPT_DpOT0_(%"class.std::__2::allocator.21"* %this, i8* %__p, i8* nonnull align 1 dereferenceable(1) %__args) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.21"*, align 4
  %__p.addr = alloca i8*, align 4
  %__args.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.21"* %this, %"class.std::__2::allocator.21"** %this.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  store i8* %__args, i8** %__args.addr, align 4
  %this1 = load %"class.std::__2::allocator.21"*, %"class.std::__2::allocator.21"** %this.addr, align 4
  %0 = load i8*, i8** %__p.addr, align 4
  %1 = load i8*, i8** %__args.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) i8* @_ZNSt3__27forwardIRKhEEOT_RNS_16remove_referenceIS3_E4typeE(i8* nonnull align 1 dereferenceable(1) %1) #9
  %2 = load i8, i8* %call, align 1
  store i8 %2, i8* %0, align 1
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE9constructIcJcEEEvRS2_PT_DpOT0_(%"class.std::__2::allocator.21"* nonnull align 1 dereferenceable(1) %__a, i8* %__p, i8* nonnull align 1 dereferenceable(1) %__args) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.21"*, align 4
  %__p.addr = alloca i8*, align 4
  %__args.addr = alloca i8*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_construct.44", align 1
  store %"class.std::__2::allocator.21"* %__a, %"class.std::__2::allocator.21"** %__a.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  store i8* %__args, i8** %__args.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_construct.44"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.21"*, %"class.std::__2::allocator.21"** %__a.addr, align 4
  %2 = load i8*, i8** %__p.addr, align 4
  %3 = load i8*, i8** %__args.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) i8* @_ZNSt3__27forwardIcEEOT_RNS_16remove_referenceIS1_E4typeE(i8* nonnull align 1 dereferenceable(1) %3) #9
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE11__constructIcJcEEEvNS_17integral_constantIbLb1EEERS2_PT_DpOT0_(%"class.std::__2::allocator.21"* nonnull align 1 dereferenceable(1) %1, i8* %2, i8* nonnull align 1 dereferenceable(1) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) i8* @_ZNSt3__24moveIRcEEONS_16remove_referenceIT_E4typeEOS3_(i8* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca i8*, align 4
  store i8* %__t, i8** %__t.addr, align 4
  %0 = load i8*, i8** %__t.addr, align 4
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__213move_backwardIPcS1_EET0_T_S3_S2_(i8* %__first, i8* %__last, i8* %__result) #0 comdat {
entry:
  %__first.addr = alloca i8*, align 4
  %__last.addr = alloca i8*, align 4
  %__result.addr = alloca i8*, align 4
  store i8* %__first, i8** %__first.addr, align 4
  store i8* %__last, i8** %__last.addr, align 4
  store i8* %__result, i8** %__result.addr, align 4
  %0 = load i8*, i8** %__first.addr, align 4
  %call = call i8* @_ZNSt3__213__unwrap_iterIPcEET_S2_(i8* %0)
  %1 = load i8*, i8** %__last.addr, align 4
  %call1 = call i8* @_ZNSt3__213__unwrap_iterIPcEET_S2_(i8* %1)
  %2 = load i8*, i8** %__result.addr, align 4
  %call2 = call i8* @_ZNSt3__213__unwrap_iterIPcEET_S2_(i8* %2)
  %call3 = call i8* @_ZNSt3__215__move_backwardIccEENS_9enable_ifIXaasr7is_sameINS_12remove_constIT_E4typeET0_EE5valuesr28is_trivially_copy_assignableIS6_EE5valueEPS6_E4typeEPS3_SA_S7_(i8* %call, i8* %call1, i8* %call2)
  ret i8* %call3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE11__constructIcJcEEEvNS_17integral_constantIbLb1EEERS2_PT_DpOT0_(%"class.std::__2::allocator.21"* nonnull align 1 dereferenceable(1) %__a, i8* %__p, i8* nonnull align 1 dereferenceable(1) %__args) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.21"*, align 4
  %__p.addr = alloca i8*, align 4
  %__args.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.21"* %__a, %"class.std::__2::allocator.21"** %__a.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  store i8* %__args, i8** %__args.addr, align 4
  %1 = load %"class.std::__2::allocator.21"*, %"class.std::__2::allocator.21"** %__a.addr, align 4
  %2 = load i8*, i8** %__p.addr, align 4
  %3 = load i8*, i8** %__args.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) i8* @_ZNSt3__27forwardIcEEOT_RNS_16remove_referenceIS1_E4typeE(i8* nonnull align 1 dereferenceable(1) %3) #9
  call void @_ZNSt3__29allocatorIcE9constructIcJcEEEvPT_DpOT0_(%"class.std::__2::allocator.21"* %1, i8* %2, i8* nonnull align 1 dereferenceable(1) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) i8* @_ZNSt3__27forwardIcEEOT_RNS_16remove_referenceIS1_E4typeE(i8* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca i8*, align 4
  store i8* %__t, i8** %__t.addr, align 4
  %0 = load i8*, i8** %__t.addr, align 4
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIcE9constructIcJcEEEvPT_DpOT0_(%"class.std::__2::allocator.21"* %this, i8* %__p, i8* nonnull align 1 dereferenceable(1) %__args) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.21"*, align 4
  %__p.addr = alloca i8*, align 4
  %__args.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.21"* %this, %"class.std::__2::allocator.21"** %this.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  store i8* %__args, i8** %__args.addr, align 4
  %this1 = load %"class.std::__2::allocator.21"*, %"class.std::__2::allocator.21"** %this.addr, align 4
  %0 = load i8*, i8** %__p.addr, align 4
  %1 = load i8*, i8** %__args.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) i8* @_ZNSt3__27forwardIcEEOT_RNS_16remove_referenceIS1_E4typeE(i8* nonnull align 1 dereferenceable(1) %1) #9
  %2 = load i8, i8* %call, align 1
  store i8 %2, i8* %0, align 1
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__215__move_backwardIccEENS_9enable_ifIXaasr7is_sameINS_12remove_constIT_E4typeET0_EE5valuesr28is_trivially_copy_assignableIS6_EE5valueEPS6_E4typeEPS3_SA_S7_(i8* %__first, i8* %__last, i8* %__result) #0 comdat {
entry:
  %__first.addr = alloca i8*, align 4
  %__last.addr = alloca i8*, align 4
  %__result.addr = alloca i8*, align 4
  %__n = alloca i32, align 4
  store i8* %__first, i8** %__first.addr, align 4
  store i8* %__last, i8** %__last.addr, align 4
  store i8* %__result, i8** %__result.addr, align 4
  %0 = load i8*, i8** %__last.addr, align 4
  %1 = load i8*, i8** %__first.addr, align 4
  %sub.ptr.lhs.cast = ptrtoint i8* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  store i32 %sub.ptr.sub, i32* %__n, align 4
  %2 = load i32, i32* %__n, align 4
  %cmp = icmp ugt i32 %2, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %3 = load i32, i32* %__n, align 4
  %4 = load i8*, i8** %__result.addr, align 4
  %idx.neg = sub i32 0, %3
  %add.ptr = getelementptr inbounds i8, i8* %4, i32 %idx.neg
  store i8* %add.ptr, i8** %__result.addr, align 4
  %5 = load i8*, i8** %__result.addr, align 4
  %6 = load i8*, i8** %__first.addr, align 4
  %7 = load i32, i32* %__n, align 4
  %mul = mul i32 %7, 1
  call void @llvm.memmove.p0i8.p0i8.i32(i8* align 1 %5, i8* align 1 %6, i32 %mul, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %8 = load i8*, i8** %__result.addr, align 4
  ret i8* %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__213__unwrap_iterIPcEET_S2_(i8* %__i) #0 comdat {
entry:
  %__i.addr = alloca i8*, align 4
  store i8* %__i, i8** %__i.addr, align 4
  %0 = load i8*, i8** %__i.addr, align 4
  ret i8* %0
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memmove.p0i8.p0i8.i32(i8* nocapture, i8* nocapture readonly, i32, i1 immarg) #2

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__26__copyIPKhPcEET0_T_S5_S4_(i8* %__first, i8* %__last, i8* %__result) #0 comdat {
entry:
  %__first.addr = alloca i8*, align 4
  %__last.addr = alloca i8*, align 4
  %__result.addr = alloca i8*, align 4
  store i8* %__first, i8** %__first.addr, align 4
  store i8* %__last, i8** %__last.addr, align 4
  store i8* %__result, i8** %__result.addr, align 4
  %0 = load i8*, i8** %__first.addr, align 4
  %1 = load i8*, i8** %__last.addr, align 4
  %2 = load i8*, i8** %__result.addr, align 4
  %call = call i8* @_ZNSt3__216__copy_constexprIPKhPcEET0_T_S5_S4_(i8* %0, i8* %1, i8* %2)
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__213__unwrap_iterIPKhEET_S3_(i8* %__i) #0 comdat {
entry:
  %__i.addr = alloca i8*, align 4
  store i8* %__i, i8** %__i.addr, align 4
  %0 = load i8*, i8** %__i.addr, align 4
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__216__copy_constexprIPKhPcEET0_T_S5_S4_(i8* %__first, i8* %__last, i8* %__result) #0 comdat {
entry:
  %__first.addr = alloca i8*, align 4
  %__last.addr = alloca i8*, align 4
  %__result.addr = alloca i8*, align 4
  store i8* %__first, i8** %__first.addr, align 4
  store i8* %__last, i8** %__last.addr, align 4
  store i8* %__result, i8** %__result.addr, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i8*, i8** %__first.addr, align 4
  %1 = load i8*, i8** %__last.addr, align 4
  %cmp = icmp ne i8* %0, %1
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load i8*, i8** %__first.addr, align 4
  %3 = load i8, i8* %2, align 1
  %4 = load i8*, i8** %__result.addr, align 4
  store i8 %3, i8* %4, align 1
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i8*, i8** %__first.addr, align 4
  %incdec.ptr = getelementptr inbounds i8, i8* %5, i32 1
  store i8* %incdec.ptr, i8** %__first.addr, align 4
  %6 = load i8*, i8** %__result.addr, align 4
  %incdec.ptr1 = getelementptr inbounds i8, i8* %6, i32 1
  store i8* %incdec.ptr1, i8** %__result.addr, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = load i8*, i8** %__result.addr, align 4
  ret i8* %7
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.21"* @_ZNSt3__217__compressed_pairIPcNS_9allocatorIcEEE6secondEv(%"class.std::__2::__compressed_pair.18"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.18"*, align 4
  store %"class.std::__2::__compressed_pair.18"* %this, %"class.std::__2::__compressed_pair.18"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.18"*, %"class.std::__2::__compressed_pair.18"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.18"* %this1 to %"struct.std::__2::__compressed_pair_elem.20"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.21"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIcEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.20"* %0) #9
  ret %"class.std::__2::allocator.21"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.21"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIcEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.20"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.20"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.20"* %this, %"struct.std::__2::__compressed_pair_elem.20"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.20"*, %"struct.std::__2::__compressed_pair_elem.20"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.20"* %this1 to %"class.std::__2::allocator.21"*
  ret %"class.std::__2::allocator.21"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIcNS_9allocatorIcEEE8max_sizeEv(%"class.std::__2::vector.16"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.16"*, align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp3 = alloca i32, align 4
  store %"class.std::__2::vector.16"* %this, %"class.std::__2::vector.16"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.16"*, %"class.std::__2::vector.16"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.16"* %this1 to %"class.std::__2::__vector_base.17"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.21"* @_ZNKSt3__213__vector_baseIcNS_9allocatorIcEEE7__allocEv(%"class.std::__2::__vector_base.17"* %0) #9
  %call2 = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE8max_sizeERKS2_(%"class.std::__2::allocator.21"* nonnull align 1 dereferenceable(1) %call) #9
  store i32 %call2, i32* %ref.tmp, align 4
  %call4 = call i32 @_ZNSt3__214numeric_limitsIlE3maxEv() #9
  store i32 %call4, i32* %ref.tmp3, align 4
  %call5 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %1 = load i32, i32* %call5, align 4
  ret i32 %1
}

; Function Attrs: noreturn
declare void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"*) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIcNS_9allocatorIcEEE8capacityEv(%"class.std::__2::vector.16"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.16"*, align 4
  store %"class.std::__2::vector.16"* %this, %"class.std::__2::vector.16"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.16"*, %"class.std::__2::vector.16"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.16"* %this1 to %"class.std::__2::__vector_base.17"*
  %call = call i32 @_ZNKSt3__213__vector_baseIcNS_9allocatorIcEEE8capacityEv(%"class.std::__2::__vector_base.17"* %0) #9
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::__less", align 1
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__a.addr, align 4
  %1 = load i32*, i32** %__b.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::__less", align 1
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__a.addr, align 4
  %1 = load i32*, i32** %__b.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE8max_sizeERKS2_(%"class.std::__2::allocator.21"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.21"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_max_size", align 1
  store %"class.std::__2::allocator.21"* %__a, %"class.std::__2::allocator.21"** %__a.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_max_size"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.21"*, %"class.std::__2::allocator.21"** %__a.addr, align 4
  %call = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE10__max_sizeENS_17integral_constantIbLb1EEERKS2_(%"class.std::__2::allocator.21"* nonnull align 1 dereferenceable(1) %1) #9
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.21"* @_ZNKSt3__213__vector_baseIcNS_9allocatorIcEEE7__allocEv(%"class.std::__2::__vector_base.17"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.17"*, align 4
  store %"class.std::__2::__vector_base.17"* %this, %"class.std::__2::__vector_base.17"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.17"*, %"class.std::__2::__vector_base.17"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.17", %"class.std::__2::__vector_base.17"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.21"* @_ZNKSt3__217__compressed_pairIPcNS_9allocatorIcEEE6secondEv(%"class.std::__2::__compressed_pair.18"* %__end_cap_) #9
  ret %"class.std::__2::allocator.21"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__214numeric_limitsIlE3maxEv() #0 comdat {
entry:
  %call = call i32 @_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv() #9
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__comp = alloca %"struct.std::__2::__less", align 1
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__b.addr, align 4
  %1 = load i32*, i32** %__a.addr, align 4
  %call = call zeroext i1 @_ZNKSt3__26__lessImmEclERKmS3_(%"struct.std::__2::__less"* %__comp, i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  br i1 %call, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32*, i32** %__b.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %3 = load i32*, i32** %__a.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi i32* [ %2, %cond.true ], [ %3, %cond.false ]
  ret i32* %cond-lvalue
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__26__lessImmEclERKmS3_(%"struct.std::__2::__less"* %this, i32* nonnull align 4 dereferenceable(4) %__x, i32* nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__less"*, align 4
  %__x.addr = alloca i32*, align 4
  %__y.addr = alloca i32*, align 4
  store %"struct.std::__2::__less"* %this, %"struct.std::__2::__less"** %this.addr, align 4
  store i32* %__x, i32** %__x.addr, align 4
  store i32* %__y, i32** %__y.addr, align 4
  %this1 = load %"struct.std::__2::__less"*, %"struct.std::__2::__less"** %this.addr, align 4
  %0 = load i32*, i32** %__x.addr, align 4
  %1 = load i32, i32* %0, align 4
  %2 = load i32*, i32** %__y.addr, align 4
  %3 = load i32, i32* %2, align 4
  %cmp = icmp ult i32 %1, %3
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE10__max_sizeENS_17integral_constantIbLb1EEERKS2_(%"class.std::__2::allocator.21"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.21"*, align 4
  store %"class.std::__2::allocator.21"* %__a, %"class.std::__2::allocator.21"** %__a.addr, align 4
  %1 = load %"class.std::__2::allocator.21"*, %"class.std::__2::allocator.21"** %__a.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorIcE8max_sizeEv(%"class.std::__2::allocator.21"* %1) #9
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__29allocatorIcE8max_sizeEv(%"class.std::__2::allocator.21"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.21"*, align 4
  store %"class.std::__2::allocator.21"* %this, %"class.std::__2::allocator.21"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.21"*, %"class.std::__2::allocator.21"** %this.addr, align 4
  ret i32 -1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.21"* @_ZNKSt3__217__compressed_pairIPcNS_9allocatorIcEEE6secondEv(%"class.std::__2::__compressed_pair.18"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.18"*, align 4
  store %"class.std::__2::__compressed_pair.18"* %this, %"class.std::__2::__compressed_pair.18"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.18"*, %"class.std::__2::__compressed_pair.18"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.18"* %this1 to %"struct.std::__2::__compressed_pair_elem.20"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.21"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorIcEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.20"* %0) #9
  ret %"class.std::__2::allocator.21"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.21"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorIcEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.20"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.20"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.20"* %this, %"struct.std::__2::__compressed_pair_elem.20"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.20"*, %"struct.std::__2::__compressed_pair_elem.20"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.20"* %this1 to %"class.std::__2::allocator.21"*
  ret %"class.std::__2::allocator.21"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv() #0 comdat {
entry:
  ret i32 2147483647
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseIcNS_9allocatorIcEEE8capacityEv(%"class.std::__2::__vector_base.17"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.17"*, align 4
  store %"class.std::__2::__vector_base.17"* %this, %"class.std::__2::__vector_base.17"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.17"*, %"class.std::__2::__vector_base.17"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__213__vector_baseIcNS_9allocatorIcEEE9__end_capEv(%"class.std::__2::__vector_base.17"* %this1) #9
  %0 = load i8*, i8** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.17", %"class.std::__2::__vector_base.17"* %this1, i32 0, i32 0
  %1 = load i8*, i8** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint i8* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  ret i32 %sub.ptr.sub
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__213__vector_baseIcNS_9allocatorIcEEE9__end_capEv(%"class.std::__2::__vector_base.17"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.17"*, align 4
  store %"class.std::__2::__vector_base.17"* %this, %"class.std::__2::__vector_base.17"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.17"*, %"class.std::__2::__vector_base.17"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.17", %"class.std::__2::__vector_base.17"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__217__compressed_pairIPcNS_9allocatorIcEEE5firstEv(%"class.std::__2::__compressed_pair.18"* %__end_cap_) #9
  ret i8** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__217__compressed_pairIPcNS_9allocatorIcEEE5firstEv(%"class.std::__2::__compressed_pair.18"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.18"*, align 4
  store %"class.std::__2::__compressed_pair.18"* %this, %"class.std::__2::__compressed_pair.18"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.18"*, %"class.std::__2::__compressed_pair.18"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.18"* %this1 to %"struct.std::__2::__compressed_pair_elem.19"*
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__222__compressed_pair_elemIPcLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.19"* %0) #9
  ret i8** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__222__compressed_pair_elemIPcLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.19"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.19"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.19"* %this, %"struct.std::__2::__compressed_pair_elem.19"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.19"*, %"struct.std::__2::__compressed_pair_elem.19"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.19", %"struct.std::__2::__compressed_pair_elem.19"* %this1, i32 0, i32 0
  ret i8** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__comp = alloca %"struct.std::__2::__less", align 1
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__a.addr, align 4
  %1 = load i32*, i32** %__b.addr, align 4
  %call = call zeroext i1 @_ZNKSt3__26__lessImmEclERKmS3_(%"struct.std::__2::__less"* %__comp, i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  br i1 %call, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32*, i32** %__b.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %3 = load i32*, i32** %__a.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi i32* [ %2, %cond.true ], [ %3, %cond.false ]
  ret i32* %cond-lvalue
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.42"* @_ZNSt3__217__compressed_pairIPcRNS_9allocatorIcEEEC2IDnS4_EEOT_OT0_(%"class.std::__2::__compressed_pair.42"* returned %this, i8** nonnull align 4 dereferenceable(4) %__t1, %"class.std::__2::allocator.21"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.42"*, align 4
  %__t1.addr = alloca i8**, align 4
  %__t2.addr = alloca %"class.std::__2::allocator.21"*, align 4
  store %"class.std::__2::__compressed_pair.42"* %this, %"class.std::__2::__compressed_pair.42"** %this.addr, align 4
  store i8** %__t1, i8*** %__t1.addr, align 4
  store %"class.std::__2::allocator.21"* %__t2, %"class.std::__2::allocator.21"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.42"*, %"class.std::__2::__compressed_pair.42"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.42"* %this1 to %"struct.std::__2::__compressed_pair_elem.19"*
  %1 = load i8**, i8*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %1) #9
  %call2 = call %"struct.std::__2::__compressed_pair_elem.19"* @_ZNSt3__222__compressed_pair_elemIPcLi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.19"* %0, i8** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.42"* %this1 to i8*
  %3 = getelementptr inbounds i8, i8* %2, i32 4
  %4 = bitcast i8* %3 to %"struct.std::__2::__compressed_pair_elem.43"*
  %5 = load %"class.std::__2::allocator.21"*, %"class.std::__2::allocator.21"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.21"* @_ZNSt3__27forwardIRNS_9allocatorIcEEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.std::__2::allocator.21"* nonnull align 1 dereferenceable(1) %5) #9
  %call4 = call %"struct.std::__2::__compressed_pair_elem.43"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIcEELi1ELb0EEC2IS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.43"* %4, %"class.std::__2::allocator.21"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::__compressed_pair.42"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE8allocateERS2_m(%"class.std::__2::allocator.21"* nonnull align 1 dereferenceable(1) %__a, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.21"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.21"* %__a, %"class.std::__2::allocator.21"** %__a.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.21"*, %"class.std::__2::allocator.21"** %__a.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call i8* @_ZNSt3__29allocatorIcE8allocateEmPKv(%"class.std::__2::allocator.21"* %0, i32 %1, i8* null)
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.21"* @_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE7__allocEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.21"* @_ZNSt3__217__compressed_pairIPcRNS_9allocatorIcEEE6secondEv(%"class.std::__2::__compressed_pair.42"* %__end_cap_) #9
  ret %"class.std::__2::allocator.21"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__217__compressed_pairIPcRNS_9allocatorIcEEE5firstEv(%"class.std::__2::__compressed_pair.42"* %__end_cap_) #9
  ret i8** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca i8**, align 4
  store i8** %__t, i8*** %__t.addr, align 4
  %0 = load i8**, i8*** %__t.addr, align 4
  ret i8** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.19"* @_ZNSt3__222__compressed_pair_elemIPcLi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.19"* returned %this, i8** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.19"*, align 4
  %__u.addr = alloca i8**, align 4
  store %"struct.std::__2::__compressed_pair_elem.19"* %this, %"struct.std::__2::__compressed_pair_elem.19"** %this.addr, align 4
  store i8** %__u, i8*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.19"*, %"struct.std::__2::__compressed_pair_elem.19"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.19", %"struct.std::__2::__compressed_pair_elem.19"* %this1, i32 0, i32 0
  %0 = load i8**, i8*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %0) #9
  store i8* null, i8** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.19"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.21"* @_ZNSt3__27forwardIRNS_9allocatorIcEEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.std::__2::allocator.21"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::allocator.21"*, align 4
  store %"class.std::__2::allocator.21"* %__t, %"class.std::__2::allocator.21"** %__t.addr, align 4
  %0 = load %"class.std::__2::allocator.21"*, %"class.std::__2::allocator.21"** %__t.addr, align 4
  ret %"class.std::__2::allocator.21"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.43"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIcEELi1ELb0EEC2IS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.43"* returned %this, %"class.std::__2::allocator.21"* nonnull align 1 dereferenceable(1) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.43"*, align 4
  %__u.addr = alloca %"class.std::__2::allocator.21"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.43"* %this, %"struct.std::__2::__compressed_pair_elem.43"** %this.addr, align 4
  store %"class.std::__2::allocator.21"* %__u, %"class.std::__2::allocator.21"** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.43"*, %"struct.std::__2::__compressed_pair_elem.43"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.43", %"struct.std::__2::__compressed_pair_elem.43"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::allocator.21"*, %"class.std::__2::allocator.21"** %__u.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.21"* @_ZNSt3__27forwardIRNS_9allocatorIcEEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.std::__2::allocator.21"* nonnull align 1 dereferenceable(1) %0) #9
  store %"class.std::__2::allocator.21"* %call, %"class.std::__2::allocator.21"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.43"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__29allocatorIcE8allocateEmPKv(%"class.std::__2::allocator.21"* %this, i32 %__n, i8* %0) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.21"*, align 4
  %__n.addr = alloca i32, align 4
  %.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.21"* %this, %"class.std::__2::allocator.21"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::allocator.21"*, %"class.std::__2::allocator.21"** %this.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorIcE8max_sizeEv(%"class.std::__2::allocator.21"* %this1) #9
  %cmp = icmp ugt i32 %1, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__220__throw_length_errorEPKc(i8* getelementptr inbounds ([68 x i8], [68 x i8]* @.str, i32 0, i32 0)) #12
  unreachable

if.end:                                           ; preds = %entry
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 1
  %call2 = call i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %mul, i32 1)
  ret i8* %call2
}

; Function Attrs: noinline noreturn nounwind optnone
define linkonce_odr hidden void @_ZNSt3__220__throw_length_errorEPKc(i8* %__msg) #5 comdat {
entry:
  %__msg.addr = alloca i8*, align 4
  store i8* %__msg, i8** %__msg.addr, align 4
  call void @abort() #12
  unreachable
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %__size, i32 %__align) #0 comdat {
entry:
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i32, i32* %__size.addr, align 4
  %call = call noalias nonnull i8* @_Znwm(i32 %0) #13
  ret i8* %call
}

; Function Attrs: noreturn
declare void @abort() #4

; Function Attrs: nobuiltin allocsize(0)
declare nonnull i8* @_Znwm(i32) #6

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.21"* @_ZNSt3__217__compressed_pairIPcRNS_9allocatorIcEEE6secondEv(%"class.std::__2::__compressed_pair.42"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.42"*, align 4
  store %"class.std::__2::__compressed_pair.42"* %this, %"class.std::__2::__compressed_pair.42"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.42"*, %"class.std::__2::__compressed_pair.42"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.42"* %this1 to i8*
  %add.ptr = getelementptr inbounds i8, i8* %0, i32 4
  %1 = bitcast i8* %add.ptr to %"struct.std::__2::__compressed_pair_elem.43"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.21"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIcEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.43"* %1) #9
  ret %"class.std::__2::allocator.21"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.21"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIcEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.43"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.43"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.43"* %this, %"struct.std::__2::__compressed_pair_elem.43"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.43"*, %"struct.std::__2::__compressed_pair_elem.43"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.43", %"struct.std::__2::__compressed_pair_elem.43"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::allocator.21"*, %"class.std::__2::allocator.21"** %__value_, align 4
  ret %"class.std::__2::allocator.21"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNSt3__217__compressed_pairIPcRNS_9allocatorIcEEE5firstEv(%"class.std::__2::__compressed_pair.42"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.42"*, align 4
  store %"class.std::__2::__compressed_pair.42"* %this, %"class.std::__2::__compressed_pair.42"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.42"*, %"class.std::__2::__compressed_pair.42"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.42"* %this1 to %"struct.std::__2::__compressed_pair_elem.19"*
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__222__compressed_pair_elemIPcLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.19"* %0) #9
  ret i8** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE21_ConstructTransactionC2EPPcm(%"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"* returned %this, i8** %__p, i32 %__n) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"*, align 4
  %__p.addr = alloca i8**, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"* %this, %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"** %this.addr, align 4
  store i8** %__p, i8*** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"*, %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load i8**, i8*** %__p.addr, align 4
  %1 = load i8*, i8** %0, align 4
  store i8* %1, i8** %__pos_, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"* %this1, i32 0, i32 1
  %2 = load i8**, i8*** %__p.addr, align 4
  %3 = load i8*, i8** %2, align 4
  %4 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds i8, i8* %3, i32 %4
  store i8* %add.ptr, i8** %__end_, align 4
  %__dest_ = getelementptr inbounds %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"* %this1, i32 0, i32 2
  %5 = load i8**, i8*** %__p.addr, align 4
  store i8** %5, i8*** %__dest_, align 4
  ret %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE21_ConstructTransactionD2Ev(%"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"*, align 4
  store %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"* %this, %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"*, %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load i8*, i8** %__pos_, align 4
  %__dest_ = getelementptr inbounds %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"* %this1, i32 0, i32 2
  %1 = load i8**, i8*** %__dest_, align 4
  store i8* %0, i8** %1, align 4
  ret %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIcNS_9allocatorIcEEE17__annotate_deleteEv(%"class.std::__2::vector.16"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.16"*, align 4
  store %"class.std::__2::vector.16"* %this, %"class.std::__2::vector.16"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.16"*, %"class.std::__2::vector.16"** %this.addr, align 4
  %call = call i8* @_ZNKSt3__26vectorIcNS_9allocatorIcEEE4dataEv(%"class.std::__2::vector.16"* %this1) #9
  %call2 = call i8* @_ZNKSt3__26vectorIcNS_9allocatorIcEEE4dataEv(%"class.std::__2::vector.16"* %this1) #9
  %call3 = call i32 @_ZNKSt3__26vectorIcNS_9allocatorIcEEE8capacityEv(%"class.std::__2::vector.16"* %this1) #9
  %add.ptr = getelementptr inbounds i8, i8* %call2, i32 %call3
  %call4 = call i8* @_ZNKSt3__26vectorIcNS_9allocatorIcEEE4dataEv(%"class.std::__2::vector.16"* %this1) #9
  %call5 = call i32 @_ZNKSt3__26vectorIcNS_9allocatorIcEEE4sizeEv(%"class.std::__2::vector.16"* %this1) #9
  %add.ptr6 = getelementptr inbounds i8, i8* %call4, i32 %call5
  %call7 = call i8* @_ZNKSt3__26vectorIcNS_9allocatorIcEEE4dataEv(%"class.std::__2::vector.16"* %this1) #9
  %call8 = call i32 @_ZNKSt3__26vectorIcNS_9allocatorIcEEE8capacityEv(%"class.std::__2::vector.16"* %this1) #9
  %add.ptr9 = getelementptr inbounds i8, i8* %call7, i32 %call8
  call void @_ZNKSt3__26vectorIcNS_9allocatorIcEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector.16"* %this1, i8* %call, i8* %add.ptr, i8* %add.ptr6, i8* %add.ptr9) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE46__construct_backward_with_exception_guaranteesIcEENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS2_PT_S8_EE5valuesr31is_trivially_move_constructibleIS8_EE5valueEvE4typeERS2_S9_S9_RS9_(%"class.std::__2::allocator.21"* nonnull align 1 dereferenceable(1) %0, i8* %__begin1, i8* %__end1, i8** nonnull align 4 dereferenceable(4) %__end2) #0 comdat {
entry:
  %.addr = alloca %"class.std::__2::allocator.21"*, align 4
  %__begin1.addr = alloca i8*, align 4
  %__end1.addr = alloca i8*, align 4
  %__end2.addr = alloca i8**, align 4
  %_Np = alloca i32, align 4
  store %"class.std::__2::allocator.21"* %0, %"class.std::__2::allocator.21"** %.addr, align 4
  store i8* %__begin1, i8** %__begin1.addr, align 4
  store i8* %__end1, i8** %__end1.addr, align 4
  store i8** %__end2, i8*** %__end2.addr, align 4
  %1 = load i8*, i8** %__end1.addr, align 4
  %2 = load i8*, i8** %__begin1.addr, align 4
  %sub.ptr.lhs.cast = ptrtoint i8* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %2 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  store i32 %sub.ptr.sub, i32* %_Np, align 4
  %3 = load i32, i32* %_Np, align 4
  %4 = load i8**, i8*** %__end2.addr, align 4
  %5 = load i8*, i8** %4, align 4
  %idx.neg = sub i32 0, %3
  %add.ptr = getelementptr inbounds i8, i8* %5, i32 %idx.neg
  store i8* %add.ptr, i8** %4, align 4
  %6 = load i32, i32* %_Np, align 4
  %cmp = icmp sgt i32 %6, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %7 = load i8**, i8*** %__end2.addr, align 4
  %8 = load i8*, i8** %7, align 4
  %9 = load i8*, i8** %__begin1.addr, align 4
  %10 = load i32, i32* %_Np, align 4
  %mul = mul i32 %10, 1
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %8, i8* align 1 %9, i32 %mul, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE45__construct_forward_with_exception_guaranteesIcEENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS2_PT_S8_EE5valuesr31is_trivially_move_constructibleIS8_EE5valueEvE4typeERS2_S9_S9_RS9_(%"class.std::__2::allocator.21"* nonnull align 1 dereferenceable(1) %0, i8* %__begin1, i8* %__end1, i8** nonnull align 4 dereferenceable(4) %__begin2) #0 comdat {
entry:
  %.addr = alloca %"class.std::__2::allocator.21"*, align 4
  %__begin1.addr = alloca i8*, align 4
  %__end1.addr = alloca i8*, align 4
  %__begin2.addr = alloca i8**, align 4
  %_Np = alloca i32, align 4
  store %"class.std::__2::allocator.21"* %0, %"class.std::__2::allocator.21"** %.addr, align 4
  store i8* %__begin1, i8** %__begin1.addr, align 4
  store i8* %__end1, i8** %__end1.addr, align 4
  store i8** %__begin2, i8*** %__begin2.addr, align 4
  %1 = load i8*, i8** %__end1.addr, align 4
  %2 = load i8*, i8** %__begin1.addr, align 4
  %sub.ptr.lhs.cast = ptrtoint i8* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %2 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  store i32 %sub.ptr.sub, i32* %_Np, align 4
  %3 = load i32, i32* %_Np, align 4
  %cmp = icmp sgt i32 %3, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = load i8**, i8*** %__begin2.addr, align 4
  %5 = load i8*, i8** %4, align 4
  %6 = load i8*, i8** %__begin1.addr, align 4
  %7 = load i32, i32* %_Np, align 4
  %mul = mul i32 %7, 1
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %5, i8* align 1 %6, i32 %mul, i1 false)
  %8 = load i32, i32* %_Np, align 4
  %9 = load i8**, i8*** %__begin2.addr, align 4
  %10 = load i8*, i8** %9, align 4
  %add.ptr = getelementptr inbounds i8, i8* %10, i32 %8
  store i8* %add.ptr, i8** %9, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__24swapIPcEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_(i8** nonnull align 4 dereferenceable(4) %__x, i8** nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %__x.addr = alloca i8**, align 4
  %__y.addr = alloca i8**, align 4
  %__t = alloca i8*, align 4
  store i8** %__x, i8*** %__x.addr, align 4
  store i8** %__y, i8*** %__y.addr, align 4
  %0 = load i8**, i8*** %__x.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__24moveIRPcEEONS_16remove_referenceIT_E4typeEOS4_(i8** nonnull align 4 dereferenceable(4) %0) #9
  %1 = load i8*, i8** %call, align 4
  store i8* %1, i8** %__t, align 4
  %2 = load i8**, i8*** %__y.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__24moveIRPcEEONS_16remove_referenceIT_E4typeEOS4_(i8** nonnull align 4 dereferenceable(4) %2) #9
  %3 = load i8*, i8** %call1, align 4
  %4 = load i8**, i8*** %__x.addr, align 4
  store i8* %3, i8** %4, align 4
  %call2 = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__24moveIRPcEEONS_16remove_referenceIT_E4typeEOS4_(i8** nonnull align 4 dereferenceable(4) %__t) #9
  %5 = load i8*, i8** %call2, align 4
  %6 = load i8**, i8*** %__y.addr, align 4
  store i8* %5, i8** %6, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIcNS_9allocatorIcEEE14__annotate_newEm(%"class.std::__2::vector.16"* %this, i32 %__current_size) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.16"*, align 4
  %__current_size.addr = alloca i32, align 4
  store %"class.std::__2::vector.16"* %this, %"class.std::__2::vector.16"** %this.addr, align 4
  store i32 %__current_size, i32* %__current_size.addr, align 4
  %this1 = load %"class.std::__2::vector.16"*, %"class.std::__2::vector.16"** %this.addr, align 4
  %call = call i8* @_ZNKSt3__26vectorIcNS_9allocatorIcEEE4dataEv(%"class.std::__2::vector.16"* %this1) #9
  %call2 = call i8* @_ZNKSt3__26vectorIcNS_9allocatorIcEEE4dataEv(%"class.std::__2::vector.16"* %this1) #9
  %call3 = call i32 @_ZNKSt3__26vectorIcNS_9allocatorIcEEE8capacityEv(%"class.std::__2::vector.16"* %this1) #9
  %add.ptr = getelementptr inbounds i8, i8* %call2, i32 %call3
  %call4 = call i8* @_ZNKSt3__26vectorIcNS_9allocatorIcEEE4dataEv(%"class.std::__2::vector.16"* %this1) #9
  %call5 = call i32 @_ZNKSt3__26vectorIcNS_9allocatorIcEEE8capacityEv(%"class.std::__2::vector.16"* %this1) #9
  %add.ptr6 = getelementptr inbounds i8, i8* %call4, i32 %call5
  %call7 = call i8* @_ZNKSt3__26vectorIcNS_9allocatorIcEEE4dataEv(%"class.std::__2::vector.16"* %this1) #9
  %0 = load i32, i32* %__current_size.addr, align 4
  %add.ptr8 = getelementptr inbounds i8, i8* %call7, i32 %0
  call void @_ZNKSt3__26vectorIcNS_9allocatorIcEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector.16"* %this1, i8* %call, i8* %add.ptr, i8* %add.ptr6, i8* %add.ptr8) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIcNS_9allocatorIcEEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector.16"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.16"*, align 4
  store %"class.std::__2::vector.16"* %this, %"class.std::__2::vector.16"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.16"*, %"class.std::__2::vector.16"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIcNS_9allocatorIcEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector.16"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.16"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector.16"* %this, %"class.std::__2::vector.16"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector.16"*, %"class.std::__2::vector.16"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__26vectorIcNS_9allocatorIcEEE4dataEv(%"class.std::__2::vector.16"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.16"*, align 4
  store %"class.std::__2::vector.16"* %this, %"class.std::__2::vector.16"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.16"*, %"class.std::__2::vector.16"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.16"* %this1 to %"class.std::__2::__vector_base.17"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.17", %"class.std::__2::__vector_base.17"* %0, i32 0, i32 0
  %1 = load i8*, i8** %__begin_, align 4
  %call = call i8* @_ZNSt3__212__to_addressIcEEPT_S2_(i8* %1) #9
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNSt3__24moveIRPcEEONS_16remove_referenceIT_E4typeEOS4_(i8** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca i8**, align 4
  store i8** %__t, i8*** %__t.addr, align 4
  %0 = load i8**, i8*** %__t.addr, align 4
  ret i8** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE5clearEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 1
  %0 = load i8*, i8** %__begin_, align 4
  call void @_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE17__destruct_at_endEPc(%"struct.std::__2::__split_buffer"* %this1, i8* %0) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE10deallocateERS2_Pcm(%"class.std::__2::allocator.21"* nonnull align 1 dereferenceable(1) %__a, i8* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.21"*, align 4
  %__p.addr = alloca i8*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.21"* %__a, %"class.std::__2::allocator.21"** %__a.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.21"*, %"class.std::__2::allocator.21"** %__a.addr, align 4
  %1 = load i8*, i8** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorIcE10deallocateEPcm(%"class.std::__2::allocator.21"* %0, i8* %1, i32 %2) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__214__split_bufferIcRNS_9allocatorIcEEE8capacityEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__214__split_bufferIcRNS_9allocatorIcEEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this1) #9
  %0 = load i8*, i8** %call, align 4
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %1 = load i8*, i8** %__first_, align 4
  %sub.ptr.lhs.cast = ptrtoint i8* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  ret i32 %sub.ptr.sub
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE17__destruct_at_endEPc(%"struct.std::__2::__split_buffer"* %this, i8* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__new_last.addr = alloca i8*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant.45", align 1
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store i8* %__new_last, i8** %__new_last.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %0 = load i8*, i8** %__new_last.addr, align 4
  call void @_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE17__destruct_at_endEPcNS_17integral_constantIbLb0EEE(%"struct.std::__2::__split_buffer"* %this1, i8* %0) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE17__destruct_at_endEPcNS_17integral_constantIbLb0EEE(%"struct.std::__2::__split_buffer"* %this, i8* %__new_last) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant.45", align 1
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__new_last.addr = alloca i8*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store i8* %__new_last, i8** %__new_last.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load i8*, i8** %__new_last.addr, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 2
  %2 = load i8*, i8** %__end_, align 4
  %cmp = icmp ne i8* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.21"* @_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE7__allocEv(%"struct.std::__2::__split_buffer"* %this1) #9
  %__end_2 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 2
  %3 = load i8*, i8** %__end_2, align 4
  %incdec.ptr = getelementptr inbounds i8, i8* %3, i32 -1
  store i8* %incdec.ptr, i8** %__end_2, align 4
  %call3 = call i8* @_ZNSt3__212__to_addressIcEEPT_S2_(i8* %incdec.ptr) #9
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE7destroyIcEEvRS2_PT_(%"class.std::__2::allocator.21"* nonnull align 1 dereferenceable(1) %call, i8* %call3)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE7destroyIcEEvRS2_PT_(%"class.std::__2::allocator.21"* nonnull align 1 dereferenceable(1) %__a, i8* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.21"*, align 4
  %__p.addr = alloca i8*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy", align 1
  store %"class.std::__2::allocator.21"* %__a, %"class.std::__2::allocator.21"** %__a.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.21"*, %"class.std::__2::allocator.21"** %__a.addr, align 4
  %2 = load i8*, i8** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE9__destroyIcEEvNS_17integral_constantIbLb1EEERS2_PT_(%"class.std::__2::allocator.21"* nonnull align 1 dereferenceable(1) %1, i8* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE9__destroyIcEEvNS_17integral_constantIbLb1EEERS2_PT_(%"class.std::__2::allocator.21"* nonnull align 1 dereferenceable(1) %__a, i8* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.21"*, align 4
  %__p.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.21"* %__a, %"class.std::__2::allocator.21"** %__a.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.21"*, %"class.std::__2::allocator.21"** %__a.addr, align 4
  %2 = load i8*, i8** %__p.addr, align 4
  call void @_ZNSt3__29allocatorIcE7destroyEPc(%"class.std::__2::allocator.21"* %1, i8* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIcE7destroyEPc(%"class.std::__2::allocator.21"* %this, i8* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.21"*, align 4
  %__p.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.21"* %this, %"class.std::__2::allocator.21"** %this.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.21"*, %"class.std::__2::allocator.21"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIcE10deallocateEPcm(%"class.std::__2::allocator.21"* %this, i8* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.21"*, align 4
  %__p.addr = alloca i8*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.21"* %this, %"class.std::__2::allocator.21"** %this.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.21"*, %"class.std::__2::allocator.21"** %this.addr, align 4
  %0 = load i8*, i8** %__p.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %1, 1
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %0, i32 %mul, i32 1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %__ptr, i32 %__size, i32 %__align) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  %1 = load i32, i32* %__size.addr, align 4
  %2 = load i32, i32* %__align.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm(i8* %0, i32 %1, i32 %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm(i8* %__ptr, i32 %__size, i32 %__align) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  %1 = load i32, i32* %__size.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm(i8* %0, i32 %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm(i8* %__ptr, i32 %__size) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller9__do_callEPv(i8* %0)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller9__do_callEPv(i8* %__ptr) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  call void @_ZdlPv(i8* %0) #11
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__214__split_bufferIcRNS_9allocatorIcEEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__217__compressed_pairIPcRNS_9allocatorIcEEE5firstEv(%"class.std::__2::__compressed_pair.42"* %__end_cap_) #9
  ret i8** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__217__compressed_pairIPcRNS_9allocatorIcEEE5firstEv(%"class.std::__2::__compressed_pair.42"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.42"*, align 4
  store %"class.std::__2::__compressed_pair.42"* %this, %"class.std::__2::__compressed_pair.42"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.42"*, %"class.std::__2::__compressed_pair.42"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.42"* %this1 to %"struct.std::__2::__compressed_pair_elem.19"*
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__222__compressed_pair_elemIPcLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.19"* %0) #9
  ret i8** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__wrap_iter.41"* @_ZNSt3__211__wrap_iterIPcEC2ES1_(%"class.std::__2::__wrap_iter.41"* returned %this, i8* %__x) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__wrap_iter.41"*, align 4
  %__x.addr = alloca i8*, align 4
  store %"class.std::__2::__wrap_iter.41"* %this, %"class.std::__2::__wrap_iter.41"** %this.addr, align 4
  store i8* %__x, i8** %__x.addr, align 4
  %this1 = load %"class.std::__2::__wrap_iter.41"*, %"class.std::__2::__wrap_iter.41"** %this.addr, align 4
  %__i = getelementptr inbounds %"class.std::__2::__wrap_iter.41", %"class.std::__2::__wrap_iter.41"* %this1, i32 0, i32 0
  %0 = load i8*, i8** %__x.addr, align 4
  store i8* %0, i8** %__i, align 4
  ret %"class.std::__2::__wrap_iter.41"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.36"* %this, %"class.draco::PointAttribute"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.36"*, align 4
  %__p.addr = alloca %"class.draco::PointAttribute"*, align 4
  %__tmp = alloca %"class.draco::PointAttribute"*, align 4
  store %"class.std::__2::unique_ptr.36"* %this, %"class.std::__2::unique_ptr.36"** %this.addr, align 4
  store %"class.draco::PointAttribute"* %__p, %"class.draco::PointAttribute"** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.36"*, %"class.std::__2::unique_ptr.36"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.36", %"class.std::__2::unique_ptr.36"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.37"* %__ptr_) #9
  %0 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %call, align 4
  store %"class.draco::PointAttribute"* %0, %"class.draco::PointAttribute"** %__tmp, align 4
  %1 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %__p.addr, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr.36", %"class.std::__2::unique_ptr.36"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.37"* %__ptr_2) #9
  store %"class.draco::PointAttribute"* %1, %"class.draco::PointAttribute"** %call3, align 4
  %2 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %__tmp, align 4
  %tobool = icmp ne %"class.draco::PointAttribute"* %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__ptr_4 = getelementptr inbounds %"class.std::__2::unique_ptr.36", %"class.std::__2::unique_ptr.36"* %this1, i32 0, i32 0
  %call5 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.40"* @_ZNSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.37"* %__ptr_4) #9
  %3 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %__tmp, align 4
  call void @_ZNKSt3__214default_deleteIN5draco14PointAttributeEEclEPS2_(%"struct.std::__2::default_delete.40"* %call5, %"class.draco::PointAttribute"* %3) #9
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.37"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.37"*, align 4
  store %"class.std::__2::__compressed_pair.37"* %this, %"class.std::__2::__compressed_pair.37"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.37"*, %"class.std::__2::__compressed_pair.37"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.37"* %this1 to %"struct.std::__2::__compressed_pair_elem.38"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNSt3__222__compressed_pair_elemIPN5draco14PointAttributeELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.38"* %0) #9
  ret %"class.draco::PointAttribute"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.40"* @_ZNSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.37"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.37"*, align 4
  store %"class.std::__2::__compressed_pair.37"* %this, %"class.std::__2::__compressed_pair.37"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.37"*, %"class.std::__2::__compressed_pair.37"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.37"* %this1 to %"struct.std::__2::__compressed_pair_elem.39"*
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.40"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco14PointAttributeEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.39"* %0) #9
  ret %"struct.std::__2::default_delete.40"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__214default_deleteIN5draco14PointAttributeEEclEPS2_(%"struct.std::__2::default_delete.40"* %this, %"class.draco::PointAttribute"* %__ptr) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::default_delete.40"*, align 4
  %__ptr.addr = alloca %"class.draco::PointAttribute"*, align 4
  store %"struct.std::__2::default_delete.40"* %this, %"struct.std::__2::default_delete.40"** %this.addr, align 4
  store %"class.draco::PointAttribute"* %__ptr, %"class.draco::PointAttribute"** %__ptr.addr, align 4
  %this1 = load %"struct.std::__2::default_delete.40"*, %"struct.std::__2::default_delete.40"** %this.addr, align 4
  %0 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %__ptr.addr, align 4
  %isnull = icmp eq %"class.draco::PointAttribute"* %0, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %entry
  %call = call %"class.draco::PointAttribute"* @_ZN5draco14PointAttributeD2Ev(%"class.draco::PointAttribute"* %0) #9
  %1 = bitcast %"class.draco::PointAttribute"* %0 to i8*
  call void @_ZdlPv(i8* %1) #11
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNSt3__222__compressed_pair_elemIPN5draco14PointAttributeELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.38"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.38"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.38"* %this, %"struct.std::__2::__compressed_pair_elem.38"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.38"*, %"struct.std::__2::__compressed_pair_elem.38"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.38", %"struct.std::__2::__compressed_pair_elem.38"* %this1, i32 0, i32 0
  ret %"class.draco::PointAttribute"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.40"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco14PointAttributeEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.39"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.39"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.39"* %this, %"struct.std::__2::__compressed_pair_elem.39"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.39"*, %"struct.std::__2::__compressed_pair_elem.39"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.39"* %this1 to %"struct.std::__2::default_delete.40"*
  ret %"struct.std::__2::default_delete.40"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::PointAttribute"* @_ZN5draco14PointAttributeD2Ev(%"class.draco::PointAttribute"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointAttribute"*, align 4
  store %"class.draco::PointAttribute"* %this, %"class.draco::PointAttribute"** %this.addr, align 4
  %this1 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %this.addr, align 4
  %attribute_transform_data_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 6
  %call = call %"class.std::__2::unique_ptr.11"* @_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.11"* %attribute_transform_data_) #9
  %indices_map_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 2
  %call2 = call %"class.draco::IndexTypeVector"* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEEED2Ev(%"class.draco::IndexTypeVector"* %indices_map_) #9
  %attribute_buffer_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 1
  %call3 = call %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr"* %attribute_buffer_) #9
  ret %"class.draco::PointAttribute"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.11"* @_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.11"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.11"*, align 4
  store %"class.std::__2::unique_ptr.11"* %this, %"class.std::__2::unique_ptr.11"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.11"*, %"class.std::__2::unique_ptr.11"** %this.addr, align 4
  call void @_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.11"* %this1, %"class.draco::AttributeTransformData"* null) #9
  ret %"class.std::__2::unique_ptr.11"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexTypeVector"* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEEED2Ev(%"class.draco::IndexTypeVector"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector"*, align 4
  store %"class.draco::IndexTypeVector"* %this, %"class.draco::IndexTypeVector"** %this.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector"*, %"class.draco::IndexTypeVector"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector", %"class.draco::IndexTypeVector"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::vector.4"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEED2Ev(%"class.std::__2::vector.4"* %vector_) #9
  ret %"class.draco::IndexTypeVector"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  call void @_ZNSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr"* %this1, %"class.draco::DataBuffer"* null) #9
  ret %"class.std::__2::unique_ptr"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.11"* %this, %"class.draco::AttributeTransformData"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.11"*, align 4
  %__p.addr = alloca %"class.draco::AttributeTransformData"*, align 4
  %__tmp = alloca %"class.draco::AttributeTransformData"*, align 4
  store %"class.std::__2::unique_ptr.11"* %this, %"class.std::__2::unique_ptr.11"** %this.addr, align 4
  store %"class.draco::AttributeTransformData"* %__p, %"class.draco::AttributeTransformData"** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.11"*, %"class.std::__2::unique_ptr.11"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.11", %"class.std::__2::unique_ptr.11"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributeTransformData"** @_ZNSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.12"* %__ptr_) #9
  %0 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %call, align 4
  store %"class.draco::AttributeTransformData"* %0, %"class.draco::AttributeTransformData"** %__tmp, align 4
  %1 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %__p.addr, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr.11", %"class.std::__2::unique_ptr.11"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::AttributeTransformData"** @_ZNSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.12"* %__ptr_2) #9
  store %"class.draco::AttributeTransformData"* %1, %"class.draco::AttributeTransformData"** %call3, align 4
  %2 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %__tmp, align 4
  %tobool = icmp ne %"class.draco::AttributeTransformData"* %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__ptr_4 = getelementptr inbounds %"class.std::__2::unique_ptr.11", %"class.std::__2::unique_ptr.11"* %this1, i32 0, i32 0
  %call5 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.15"* @_ZNSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.12"* %__ptr_4) #9
  %3 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %__tmp, align 4
  call void @_ZNKSt3__214default_deleteIN5draco22AttributeTransformDataEEclEPS2_(%"struct.std::__2::default_delete.15"* %call5, %"class.draco::AttributeTransformData"* %3) #9
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::AttributeTransformData"** @_ZNSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.12"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.12"*, align 4
  store %"class.std::__2::__compressed_pair.12"* %this, %"class.std::__2::__compressed_pair.12"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.12"*, %"class.std::__2::__compressed_pair.12"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.12"* %this1 to %"struct.std::__2::__compressed_pair_elem.13"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributeTransformData"** @_ZNSt3__222__compressed_pair_elemIPN5draco22AttributeTransformDataELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.13"* %0) #9
  ret %"class.draco::AttributeTransformData"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.15"* @_ZNSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.12"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.12"*, align 4
  store %"class.std::__2::__compressed_pair.12"* %this, %"class.std::__2::__compressed_pair.12"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.12"*, %"class.std::__2::__compressed_pair.12"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.12"* %this1 to %"struct.std::__2::__compressed_pair_elem.14"*
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.15"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco22AttributeTransformDataEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.14"* %0) #9
  ret %"struct.std::__2::default_delete.15"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__214default_deleteIN5draco22AttributeTransformDataEEclEPS2_(%"struct.std::__2::default_delete.15"* %this, %"class.draco::AttributeTransformData"* %__ptr) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::default_delete.15"*, align 4
  %__ptr.addr = alloca %"class.draco::AttributeTransformData"*, align 4
  store %"struct.std::__2::default_delete.15"* %this, %"struct.std::__2::default_delete.15"** %this.addr, align 4
  store %"class.draco::AttributeTransformData"* %__ptr, %"class.draco::AttributeTransformData"** %__ptr.addr, align 4
  %this1 = load %"struct.std::__2::default_delete.15"*, %"struct.std::__2::default_delete.15"** %this.addr, align 4
  %0 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %__ptr.addr, align 4
  %isnull = icmp eq %"class.draco::AttributeTransformData"* %0, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %entry
  %call = call %"class.draco::AttributeTransformData"* @_ZN5draco22AttributeTransformDataD2Ev(%"class.draco::AttributeTransformData"* %0) #9
  %1 = bitcast %"class.draco::AttributeTransformData"* %0 to i8*
  call void @_ZdlPv(i8* %1) #11
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::AttributeTransformData"** @_ZNSt3__222__compressed_pair_elemIPN5draco22AttributeTransformDataELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.13"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.13"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.13"* %this, %"struct.std::__2::__compressed_pair_elem.13"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.13"*, %"struct.std::__2::__compressed_pair_elem.13"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.13", %"struct.std::__2::__compressed_pair_elem.13"* %this1, i32 0, i32 0
  ret %"class.draco::AttributeTransformData"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.15"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco22AttributeTransformDataEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.14"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.14"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.14"* %this, %"struct.std::__2::__compressed_pair_elem.14"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.14"*, %"struct.std::__2::__compressed_pair_elem.14"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.14"* %this1 to %"struct.std::__2::default_delete.15"*
  ret %"struct.std::__2::default_delete.15"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::AttributeTransformData"* @_ZN5draco22AttributeTransformDataD2Ev(%"class.draco::AttributeTransformData"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::AttributeTransformData"*, align 4
  store %"class.draco::AttributeTransformData"* %this, %"class.draco::AttributeTransformData"** %this.addr, align 4
  %this1 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %this.addr, align 4
  %buffer_ = getelementptr inbounds %"class.draco::AttributeTransformData", %"class.draco::AttributeTransformData"* %this1, i32 0, i32 1
  %call = call %"class.draco::DataBuffer"* @_ZN5draco10DataBufferD2Ev(%"class.draco::DataBuffer"* %buffer_) #9
  ret %"class.draco::AttributeTransformData"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::DataBuffer"* @_ZN5draco10DataBufferD2Ev(%"class.draco::DataBuffer"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::DataBuffer"*, align 4
  store %"class.draco::DataBuffer"* %this, %"class.draco::DataBuffer"** %this.addr, align 4
  %this1 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %this.addr, align 4
  %data_ = getelementptr inbounds %"class.draco::DataBuffer", %"class.draco::DataBuffer"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::vector"* @_ZNSt3__26vectorIhNS_9allocatorIhEEED2Ev(%"class.std::__2::vector"* %data_) #9
  ret %"class.draco::DataBuffer"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector"* @_ZNSt3__26vectorIhNS_9allocatorIhEEED2Ev(%"class.std::__2::vector"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  call void @_ZNKSt3__26vectorIhNS_9allocatorIhEEE17__annotate_deleteEv(%"class.std::__2::vector"* %this1) #9
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call %"class.std::__2::__vector_base"* @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEED2Ev(%"class.std::__2::__vector_base"* %0) #9
  ret %"class.std::__2::vector"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIhNS_9allocatorIhEEE17__annotate_deleteEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %call = call i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector"* %this1) #9
  %call2 = call i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector"* %this1) #9
  %call3 = call i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::vector"* %this1) #9
  %add.ptr = getelementptr inbounds i8, i8* %call2, i32 %call3
  %call4 = call i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector"* %this1) #9
  %call5 = call i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4sizeEv(%"class.std::__2::vector"* %this1) #9
  %add.ptr6 = getelementptr inbounds i8, i8* %call4, i32 %call5
  %call7 = call i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector"* %this1) #9
  %call8 = call i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::vector"* %this1) #9
  %add.ptr9 = getelementptr inbounds i8, i8* %call7, i32 %call8
  call void @_ZNKSt3__26vectorIhNS_9allocatorIhEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector"* %this1, i8* %call, i8* %add.ptr, i8* %add.ptr6, i8* %add.ptr9) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base"* @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEED2Ev(%"class.std::__2::__vector_base"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__vector_base"*, align 4
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  store %"class.std::__2::__vector_base"* %this1, %"class.std::__2::__vector_base"** %retval, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  %0 = load i8*, i8** %__begin_, align 4
  %cmp = icmp ne i8* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE5clearEv(%"class.std::__2::__vector_base"* %this1) #9
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE7__allocEv(%"class.std::__2::__vector_base"* %this1) #9
  %__begin_2 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  %1 = load i8*, i8** %__begin_2, align 4
  %call3 = call i32 @_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::__vector_base"* %this1) #9
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE10deallocateERS2_Phm(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call, i8* %1, i32 %call3) #9
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %retval, align 4
  ret %"class.std::__2::__vector_base"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIhNS_9allocatorIhEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call i32 @_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::__vector_base"* %0) #9
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE9__end_capEv(%"class.std::__2::__vector_base"* %this1) #9
  %0 = load i8*, i8** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  %1 = load i8*, i8** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint i8* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  ret i32 %sub.ptr.sub
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE9__end_capEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__217__compressed_pairIPhNS_9allocatorIhEEE5firstEv(%"class.std::__2::__compressed_pair"* %__end_cap_) #9
  ret i8** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__217__compressed_pairIPhNS_9allocatorIhEEE5firstEv(%"class.std::__2::__compressed_pair"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__222__compressed_pair_elemIPhLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %0) #9
  ret i8** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__222__compressed_pair_elemIPhLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem"*, align 4
  store %"struct.std::__2::__compressed_pair_elem"* %this, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem"*, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem"* %this1, i32 0, i32 0
  ret i8** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE5clearEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  %0 = load i8*, i8** %__begin_, align 4
  call void @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE17__destruct_at_endEPh(%"class.std::__2::__vector_base"* %this1, i8* %0) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE10deallocateERS2_Phm(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, i8* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i8*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %1 = load i8*, i8** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorIhE10deallocateEPhm(%"class.std::__2::allocator"* %0, i8* %1, i32 %2) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE7__allocEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__217__compressed_pairIPhNS_9allocatorIhEEE6secondEv(%"class.std::__2::__compressed_pair"* %__end_cap_) #9
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE17__destruct_at_endEPh(%"class.std::__2::__vector_base"* %this, i8* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  %__new_last.addr = alloca i8*, align 4
  %__soon_to_be_end = alloca i8*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  store i8* %__new_last, i8** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 1
  %0 = load i8*, i8** %__end_, align 4
  store i8* %0, i8** %__soon_to_be_end, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load i8*, i8** %__new_last.addr, align 4
  %2 = load i8*, i8** %__soon_to_be_end, align 4
  %cmp = icmp ne i8* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE7__allocEv(%"class.std::__2::__vector_base"* %this1) #9
  %3 = load i8*, i8** %__soon_to_be_end, align 4
  %incdec.ptr = getelementptr inbounds i8, i8* %3, i32 -1
  store i8* %incdec.ptr, i8** %__soon_to_be_end, align 4
  %call2 = call i8* @_ZNSt3__212__to_addressIhEEPT_S2_(i8* %incdec.ptr) #9
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE7destroyIhEEvRS2_PT_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call, i8* %call2)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %4 = load i8*, i8** %__new_last.addr, align 4
  %__end_3 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 1
  store i8* %4, i8** %__end_3, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE7destroyIhEEvRS2_PT_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, i8* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i8*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy.46", align 1
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy.46"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %2 = load i8*, i8** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE9__destroyIhEEvNS_17integral_constantIbLb1EEERS2_PT_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %1, i8* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE9__destroyIhEEvNS_17integral_constantIbLb1EEERS2_PT_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, i8* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i8*, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %2 = load i8*, i8** %__p.addr, align 4
  call void @_ZNSt3__29allocatorIhE7destroyEPh(%"class.std::__2::allocator"* %1, i8* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIhE7destroyEPh(%"class.std::__2::allocator"* %this, i8* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i8*, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIhE10deallocateEPhm(%"class.std::__2::allocator"* %this, i8* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i8*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  %0 = load i8*, i8** %__p.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %1, 1
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %0, i32 %mul, i32 1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__217__compressed_pairIPhNS_9allocatorIhEEE6secondEv(%"class.std::__2::__compressed_pair"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem.0"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIhEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.0"* %0) #9
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIhEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.0"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.0"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.0"* %this, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.0"*, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.0"* %this1 to %"class.std::__2::allocator"*
  ret %"class.std::__2::allocator"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector.4"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEED2Ev(%"class.std::__2::vector.4"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.4"*, align 4
  store %"class.std::__2::vector.4"* %this, %"class.std::__2::vector.4"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.4"*, %"class.std::__2::vector.4"** %this.addr, align 4
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_deleteEv(%"class.std::__2::vector.4"* %this1) #9
  %0 = bitcast %"class.std::__2::vector.4"* %this1 to %"class.std::__2::__vector_base.5"*
  %call = call %"class.std::__2::__vector_base.5"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEED2Ev(%"class.std::__2::__vector_base.5"* %0) #9
  ret %"class.std::__2::vector.4"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_deleteEv(%"class.std::__2::vector.4"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.4"*, align 4
  store %"class.std::__2::vector.4"* %this, %"class.std::__2::vector.4"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.4"*, %"class.std::__2::vector.4"** %this.addr, align 4
  %call = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.4"* %this1) #9
  %0 = bitcast %"class.draco::IndexType"* %call to i8*
  %call2 = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.4"* %this1) #9
  %call3 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.4"* %this1) #9
  %add.ptr = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %call2, i32 %call3
  %1 = bitcast %"class.draco::IndexType"* %add.ptr to i8*
  %call4 = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.4"* %this1) #9
  %call5 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.4"* %this1) #9
  %add.ptr6 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %call4, i32 %call5
  %2 = bitcast %"class.draco::IndexType"* %add.ptr6 to i8*
  %call7 = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.4"* %this1) #9
  %call8 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.4"* %this1) #9
  %add.ptr9 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %call7, i32 %call8
  %3 = bitcast %"class.draco::IndexType"* %add.ptr9 to i8*
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_(%"class.std::__2::vector.4"* %this1, i8* %0, i8* %1, i8* %2, i8* %3) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base.5"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEED2Ev(%"class.std::__2::__vector_base.5"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__vector_base.5"*, align 4
  %this.addr = alloca %"class.std::__2::__vector_base.5"*, align 4
  store %"class.std::__2::__vector_base.5"* %this, %"class.std::__2::__vector_base.5"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.5"*, %"class.std::__2::__vector_base.5"** %this.addr, align 4
  store %"class.std::__2::__vector_base.5"* %this1, %"class.std::__2::__vector_base.5"** %retval, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.5", %"class.std::__2::__vector_base.5"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  %cmp = icmp ne %"class.draco::IndexType"* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5clearEv(%"class.std::__2::__vector_base.5"* %this1) #9
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.9"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.5"* %this1) #9
  %__begin_2 = getelementptr inbounds %"class.std::__2::__vector_base.5", %"class.std::__2::__vector_base.5"* %this1, i32 0, i32 0
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_2, align 4
  %call3 = call i32 @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::__vector_base.5"* %this1) #9
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE10deallocateERS6_PS5_m(%"class.std::__2::allocator.9"* nonnull align 1 dereferenceable(1) %call, %"class.draco::IndexType"* %1, i32 %call3) #9
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"class.std::__2::__vector_base.5"*, %"class.std::__2::__vector_base.5"** %retval, align 4
  ret %"class.std::__2::__vector_base.5"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_(%"class.std::__2::vector.4"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.4"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector.4"* %this, %"class.std::__2::vector.4"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector.4"*, %"class.std::__2::vector.4"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.4"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.4"*, align 4
  store %"class.std::__2::vector.4"* %this, %"class.std::__2::vector.4"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.4"*, %"class.std::__2::vector.4"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.4"* %this1 to %"class.std::__2::__vector_base.5"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.5", %"class.std::__2::__vector_base.5"* %0, i32 0, i32 0
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  %call = call %"class.draco::IndexType"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType"* %1) #9
  ret %"class.draco::IndexType"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.4"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.4"*, align 4
  store %"class.std::__2::vector.4"* %this, %"class.std::__2::vector.4"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.4"*, %"class.std::__2::vector.4"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.4"* %this1 to %"class.std::__2::__vector_base.5"*
  %call = call i32 @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::__vector_base.5"* %0) #9
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.4"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.4"*, align 4
  store %"class.std::__2::vector.4"* %this, %"class.std::__2::vector.4"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.4"*, %"class.std::__2::vector.4"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.4"* %this1 to %"class.std::__2::__vector_base.5"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.5", %"class.std::__2::__vector_base.5"* %0, i32 0, i32 1
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector.4"* %this1 to %"class.std::__2::__vector_base.5"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.5", %"class.std::__2::__vector_base.5"* %2, i32 0, i32 0
  %3 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.draco::IndexType"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.draco::IndexType"* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType"* %__p) #0 comdat {
entry:
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  ret %"class.draco::IndexType"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::__vector_base.5"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.5"*, align 4
  store %"class.std::__2::__vector_base.5"* %this, %"class.std::__2::__vector_base.5"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.5"*, %"class.std::__2::__vector_base.5"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base.5"* %this1) #9
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.5", %"class.std::__2::__vector_base.5"* %this1, i32 0, i32 0
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.draco::IndexType"* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.draco::IndexType"* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base.5"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.5"*, align 4
  store %"class.std::__2::__vector_base.5"* %this, %"class.std::__2::__vector_base.5"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.5"*, %"class.std::__2::__vector_base.5"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.5", %"class.std::__2::__vector_base.5"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.6"* %__end_cap_) #9
  ret %"class.draco::IndexType"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.6"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.6"*, align 4
  store %"class.std::__2::__compressed_pair.6"* %this, %"class.std::__2::__compressed_pair.6"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.6"*, %"class.std::__2::__compressed_pair.6"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.6"* %this1 to %"struct.std::__2::__compressed_pair_elem.7"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.7"* %0) #9
  ret %"class.draco::IndexType"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.7"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.7"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.7"* %this, %"struct.std::__2::__compressed_pair_elem.7"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.7"*, %"struct.std::__2::__compressed_pair_elem.7"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.7", %"struct.std::__2::__compressed_pair_elem.7"* %this1, i32 0, i32 0
  ret %"class.draco::IndexType"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5clearEv(%"class.std::__2::__vector_base.5"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.5"*, align 4
  store %"class.std::__2::__vector_base.5"* %this, %"class.std::__2::__vector_base.5"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.5"*, %"class.std::__2::__vector_base.5"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.5", %"class.std::__2::__vector_base.5"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  call void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_(%"class.std::__2::__vector_base.5"* %this1, %"class.draco::IndexType"* %0) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE10deallocateERS6_PS5_m(%"class.std::__2::allocator.9"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType"* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.9"*, align 4
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.9"* %__a, %"class.std::__2::allocator.9"** %__a.addr, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.9"*, %"class.std::__2::allocator.9"** %__a.addr, align 4
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE10deallocateEPS4_m(%"class.std::__2::allocator.9"* %0, %"class.draco::IndexType"* %1, i32 %2) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.9"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.5"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.5"*, align 4
  store %"class.std::__2::__vector_base.5"* %this, %"class.std::__2::__vector_base.5"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.5"*, %"class.std::__2::__vector_base.5"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.5", %"class.std::__2::__vector_base.5"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.9"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.6"* %__end_cap_) #9
  ret %"class.std::__2::allocator.9"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_(%"class.std::__2::__vector_base.5"* %this, %"class.draco::IndexType"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.5"*, align 4
  %__new_last.addr = alloca %"class.draco::IndexType"*, align 4
  %__soon_to_be_end = alloca %"class.draco::IndexType"*, align 4
  store %"class.std::__2::__vector_base.5"* %this, %"class.std::__2::__vector_base.5"** %this.addr, align 4
  store %"class.draco::IndexType"* %__new_last, %"class.draco::IndexType"** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.5"*, %"class.std::__2::__vector_base.5"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.5", %"class.std::__2::__vector_base.5"* %this1, i32 0, i32 1
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__end_, align 4
  store %"class.draco::IndexType"* %0, %"class.draco::IndexType"** %__soon_to_be_end, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__new_last.addr, align 4
  %2 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__soon_to_be_end, align 4
  %cmp = icmp ne %"class.draco::IndexType"* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.9"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.5"* %this1) #9
  %3 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__soon_to_be_end, align 4
  %incdec.ptr = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %3, i32 -1
  store %"class.draco::IndexType"* %incdec.ptr, %"class.draco::IndexType"** %__soon_to_be_end, align 4
  %call2 = call %"class.draco::IndexType"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType"* %incdec.ptr) #9
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE7destroyIS5_EEvRS6_PT_(%"class.std::__2::allocator.9"* nonnull align 1 dereferenceable(1) %call, %"class.draco::IndexType"* %call2)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %4 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__new_last.addr, align 4
  %__end_3 = getelementptr inbounds %"class.std::__2::__vector_base.5", %"class.std::__2::__vector_base.5"* %this1, i32 0, i32 1
  store %"class.draco::IndexType"* %4, %"class.draco::IndexType"** %__end_3, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE7destroyIS5_EEvRS6_PT_(%"class.std::__2::allocator.9"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType"* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.9"*, align 4
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy.47", align 1
  store %"class.std::__2::allocator.9"* %__a, %"class.std::__2::allocator.9"** %__a.addr, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy.47"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.9"*, %"class.std::__2::allocator.9"** %__a.addr, align 4
  %2 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE9__destroyIS5_EEvNS_17integral_constantIbLb1EEERS6_PT_(%"class.std::__2::allocator.9"* nonnull align 1 dereferenceable(1) %1, %"class.draco::IndexType"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE9__destroyIS5_EEvNS_17integral_constantIbLb1EEERS6_PT_(%"class.std::__2::allocator.9"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType"* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.9"*, align 4
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.std::__2::allocator.9"* %__a, %"class.std::__2::allocator.9"** %__a.addr, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.9"*, %"class.std::__2::allocator.9"** %__a.addr, align 4
  %2 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  call void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE7destroyEPS4_(%"class.std::__2::allocator.9"* %1, %"class.draco::IndexType"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE7destroyEPS4_(%"class.std::__2::allocator.9"* %this, %"class.draco::IndexType"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.9"*, align 4
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.std::__2::allocator.9"* %this, %"class.std::__2::allocator.9"** %this.addr, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.9"*, %"class.std::__2::allocator.9"** %this.addr, align 4
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE10deallocateEPS4_m(%"class.std::__2::allocator.9"* %this, %"class.draco::IndexType"* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.9"*, align 4
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.9"* %this, %"class.std::__2::allocator.9"** %this.addr, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.9"*, %"class.std::__2::allocator.9"** %this.addr, align 4
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  %1 = bitcast %"class.draco::IndexType"* %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.9"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.6"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.6"*, align 4
  store %"class.std::__2::__compressed_pair.6"* %this, %"class.std::__2::__compressed_pair.6"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.6"*, %"class.std::__2::__compressed_pair.6"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.6"* %this1 to %"struct.std::__2::__compressed_pair_elem.8"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.9"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.8"* %0) #9
  ret %"class.std::__2::allocator.9"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.9"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.8"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.8"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.8"* %this, %"struct.std::__2::__compressed_pair_elem.8"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.8"*, %"struct.std::__2::__compressed_pair_elem.8"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.8"* %this1 to %"class.std::__2::allocator.9"*
  ret %"class.std::__2::allocator.9"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr"* %this, %"class.draco::DataBuffer"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  %__p.addr = alloca %"class.draco::DataBuffer"*, align 4
  %__tmp = alloca %"class.draco::DataBuffer"*, align 4
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  store %"class.draco::DataBuffer"* %__p, %"class.draco::DataBuffer"** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::DataBuffer"** @_ZNSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.1"* %__ptr_) #9
  %0 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %call, align 4
  store %"class.draco::DataBuffer"* %0, %"class.draco::DataBuffer"** %__tmp, align 4
  %1 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %__p.addr, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::DataBuffer"** @_ZNSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.1"* %__ptr_2) #9
  store %"class.draco::DataBuffer"* %1, %"class.draco::DataBuffer"** %call3, align 4
  %2 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %__tmp, align 4
  %tobool = icmp ne %"class.draco::DataBuffer"* %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__ptr_4 = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call5 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete"* @_ZNSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.1"* %__ptr_4) #9
  %3 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %__tmp, align 4
  call void @_ZNKSt3__214default_deleteIN5draco10DataBufferEEclEPS2_(%"struct.std::__2::default_delete"* %call5, %"class.draco::DataBuffer"* %3) #9
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::DataBuffer"** @_ZNSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.1"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.1"*, align 4
  store %"class.std::__2::__compressed_pair.1"* %this, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.1"*, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.1"* %this1 to %"struct.std::__2::__compressed_pair_elem.2"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::DataBuffer"** @_ZNSt3__222__compressed_pair_elemIPN5draco10DataBufferELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.2"* %0) #9
  ret %"class.draco::DataBuffer"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete"* @_ZNSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.1"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.1"*, align 4
  store %"class.std::__2::__compressed_pair.1"* %this, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.1"*, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.1"* %this1 to %"struct.std::__2::__compressed_pair_elem.3"*
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco10DataBufferEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.3"* %0) #9
  ret %"struct.std::__2::default_delete"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__214default_deleteIN5draco10DataBufferEEclEPS2_(%"struct.std::__2::default_delete"* %this, %"class.draco::DataBuffer"* %__ptr) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::default_delete"*, align 4
  %__ptr.addr = alloca %"class.draco::DataBuffer"*, align 4
  store %"struct.std::__2::default_delete"* %this, %"struct.std::__2::default_delete"** %this.addr, align 4
  store %"class.draco::DataBuffer"* %__ptr, %"class.draco::DataBuffer"** %__ptr.addr, align 4
  %this1 = load %"struct.std::__2::default_delete"*, %"struct.std::__2::default_delete"** %this.addr, align 4
  %0 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %__ptr.addr, align 4
  %isnull = icmp eq %"class.draco::DataBuffer"* %0, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %entry
  %call = call %"class.draco::DataBuffer"* @_ZN5draco10DataBufferD2Ev(%"class.draco::DataBuffer"* %0) #9
  %1 = bitcast %"class.draco::DataBuffer"* %0 to i8*
  call void @_ZdlPv(i8* %1) #11
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::DataBuffer"** @_ZNSt3__222__compressed_pair_elemIPN5draco10DataBufferELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.2"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.2"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.2"* %this, %"struct.std::__2::__compressed_pair_elem.2"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.2"*, %"struct.std::__2::__compressed_pair_elem.2"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.2", %"struct.std::__2::__compressed_pair_elem.2"* %this1, i32 0, i32 0
  ret %"class.draco::DataBuffer"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco10DataBufferEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.3"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.3"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.3"* %this, %"struct.std::__2::__compressed_pair_elem.3"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.3"*, %"struct.std::__2::__compressed_pair_elem.3"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.3"* %this1 to %"struct.std::__2::default_delete"*
  ret %"struct.std::__2::default_delete"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::PointAttribute"* @_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEE7releaseEv(%"class.std::__2::unique_ptr.36"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.36"*, align 4
  %__t = alloca %"class.draco::PointAttribute"*, align 4
  store %"class.std::__2::unique_ptr.36"* %this, %"class.std::__2::unique_ptr.36"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.36"*, %"class.std::__2::unique_ptr.36"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.36", %"class.std::__2::unique_ptr.36"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.37"* %__ptr_) #9
  %0 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %call, align 4
  store %"class.draco::PointAttribute"* %0, %"class.draco::PointAttribute"** %__t, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr.36", %"class.std::__2::unique_ptr.36"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.37"* %__ptr_2) #9
  store %"class.draco::PointAttribute"* null, %"class.draco::PointAttribute"** %call3, align 4
  %1 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %__t, align 4
  ret %"class.draco::PointAttribute"* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.40"* @_ZNSt3__27forwardINS_14default_deleteIN5draco14PointAttributeEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.40"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"struct.std::__2::default_delete.40"*, align 4
  store %"struct.std::__2::default_delete.40"* %__t, %"struct.std::__2::default_delete.40"** %__t.addr, align 4
  %0 = load %"struct.std::__2::default_delete.40"*, %"struct.std::__2::default_delete.40"** %__t.addr, align 4
  ret %"struct.std::__2::default_delete.40"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.40"* @_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEE11get_deleterEv(%"class.std::__2::unique_ptr.36"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.36"*, align 4
  store %"class.std::__2::unique_ptr.36"* %this, %"class.std::__2::unique_ptr.36"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.36"*, %"class.std::__2::unique_ptr.36"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.36", %"class.std::__2::unique_ptr.36"* %this1, i32 0, i32 0
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.40"* @_ZNSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.37"* %__ptr_) #9
  ret %"struct.std::__2::default_delete.40"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.37"* @_ZNSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEEC2IS3_S5_EEOT_OT0_(%"class.std::__2::__compressed_pair.37"* returned %this, %"class.draco::PointAttribute"** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::default_delete.40"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.37"*, align 4
  %__t1.addr = alloca %"class.draco::PointAttribute"**, align 4
  %__t2.addr = alloca %"struct.std::__2::default_delete.40"*, align 4
  store %"class.std::__2::__compressed_pair.37"* %this, %"class.std::__2::__compressed_pair.37"** %this.addr, align 4
  store %"class.draco::PointAttribute"** %__t1, %"class.draco::PointAttribute"*** %__t1.addr, align 4
  store %"struct.std::__2::default_delete.40"* %__t2, %"struct.std::__2::default_delete.40"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.37"*, %"class.std::__2::__compressed_pair.37"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.37"* %this1 to %"struct.std::__2::__compressed_pair_elem.38"*
  %1 = load %"class.draco::PointAttribute"**, %"class.draco::PointAttribute"*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNSt3__27forwardIPN5draco14PointAttributeEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::PointAttribute"** nonnull align 4 dereferenceable(4) %1) #9
  %call2 = call %"struct.std::__2::__compressed_pair_elem.38"* @_ZNSt3__222__compressed_pair_elemIPN5draco14PointAttributeELi0ELb0EEC2IS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.38"* %0, %"class.draco::PointAttribute"** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.37"* %this1 to %"struct.std::__2::__compressed_pair_elem.39"*
  %3 = load %"struct.std::__2::default_delete.40"*, %"struct.std::__2::default_delete.40"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.40"* @_ZNSt3__27forwardINS_14default_deleteIN5draco14PointAttributeEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.40"* nonnull align 1 dereferenceable(1) %3) #9
  %call4 = call %"struct.std::__2::__compressed_pair_elem.39"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco14PointAttributeEEELi1ELb1EEC2IS4_vEEOT_(%"struct.std::__2::__compressed_pair_elem.39"* %2, %"struct.std::__2::default_delete.40"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::__compressed_pair.37"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.39"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco14PointAttributeEEELi1ELb1EEC2IS4_vEEOT_(%"struct.std::__2::__compressed_pair_elem.39"* returned %this, %"struct.std::__2::default_delete.40"* nonnull align 1 dereferenceable(1) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.39"*, align 4
  %__u.addr = alloca %"struct.std::__2::default_delete.40"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.39"* %this, %"struct.std::__2::__compressed_pair_elem.39"** %this.addr, align 4
  store %"struct.std::__2::default_delete.40"* %__u, %"struct.std::__2::default_delete.40"** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.39"*, %"struct.std::__2::__compressed_pair_elem.39"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.39"* %this1 to %"struct.std::__2::default_delete.40"*
  %1 = load %"struct.std::__2::default_delete.40"*, %"struct.std::__2::default_delete.40"** %__u.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.40"* @_ZNSt3__27forwardINS_14default_deleteIN5draco14PointAttributeEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.40"* nonnull align 1 dereferenceable(1) %1) #9
  ret %"struct.std::__2::__compressed_pair_elem.39"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNKSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.37"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.37"*, align 4
  store %"class.std::__2::__compressed_pair.37"* %this, %"class.std::__2::__compressed_pair.37"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.37"*, %"class.std::__2::__compressed_pair.37"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.37"* %this1 to %"struct.std::__2::__compressed_pair_elem.38"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNKSt3__222__compressed_pair_elemIPN5draco14PointAttributeELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.38"* %0) #9
  ret %"class.draco::PointAttribute"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNKSt3__222__compressed_pair_elemIPN5draco14PointAttributeELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.38"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.38"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.38"* %this, %"struct.std::__2::__compressed_pair_elem.38"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.38"*, %"struct.std::__2::__compressed_pair_elem.38"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.38", %"struct.std::__2::__compressed_pair_elem.38"* %this1, i32 0, i32 0
  ret %"class.draco::PointAttribute"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden double @_Z3absd(double %__lcpp_x) #0 comdat {
entry:
  %__lcpp_x.addr = alloca double, align 8
  store double %__lcpp_x, double* %__lcpp_x.addr, align 8
  %0 = load double, double* %__lcpp_x.addr, align 8
  %1 = call double @llvm.fabs.f64(double %0)
  ret double %1
}

; Function Attrs: nounwind readnone speculatable willreturn
declare double @llvm.floor.f64(double) #7

; Function Attrs: nounwind readnone
declare i32 @abs(i32) #8

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK5draco17OctahedronToolBox40IntegerVectorToQuantizedOctahedralCoordsEPKiPiS3_(%"class.draco::OctahedronToolBox"* %this, i32* %int_vec, i32* %out_s, i32* %out_t) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::OctahedronToolBox"*, align 4
  %int_vec.addr = alloca i32*, align 4
  %out_s.addr = alloca i32*, align 4
  %out_t.addr = alloca i32*, align 4
  %s = alloca i32, align 4
  %t = alloca i32, align 4
  store %"class.draco::OctahedronToolBox"* %this, %"class.draco::OctahedronToolBox"** %this.addr, align 4
  store i32* %int_vec, i32** %int_vec.addr, align 4
  store i32* %out_s, i32** %out_s.addr, align 4
  store i32* %out_t, i32** %out_t.addr, align 4
  %this1 = load %"class.draco::OctahedronToolBox"*, %"class.draco::OctahedronToolBox"** %this.addr, align 4
  %0 = load i32*, i32** %int_vec.addr, align 4
  %arrayidx = getelementptr inbounds i32, i32* %0, i32 0
  %1 = load i32, i32* %arrayidx, align 4
  %cmp = icmp sge i32 %1, 0
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32*, i32** %int_vec.addr, align 4
  %arrayidx2 = getelementptr inbounds i32, i32* %2, i32 1
  %3 = load i32, i32* %arrayidx2, align 4
  %center_value_ = getelementptr inbounds %"class.draco::OctahedronToolBox", %"class.draco::OctahedronToolBox"* %this1, i32 0, i32 3
  %4 = load i32, i32* %center_value_, align 4
  %add = add nsw i32 %3, %4
  store i32 %add, i32* %s, align 4
  %5 = load i32*, i32** %int_vec.addr, align 4
  %arrayidx3 = getelementptr inbounds i32, i32* %5, i32 2
  %6 = load i32, i32* %arrayidx3, align 4
  %center_value_4 = getelementptr inbounds %"class.draco::OctahedronToolBox", %"class.draco::OctahedronToolBox"* %this1, i32 0, i32 3
  %7 = load i32, i32* %center_value_4, align 4
  %add5 = add nsw i32 %6, %7
  store i32 %add5, i32* %t, align 4
  br label %if.end24

if.else:                                          ; preds = %entry
  %8 = load i32*, i32** %int_vec.addr, align 4
  %arrayidx6 = getelementptr inbounds i32, i32* %8, i32 1
  %9 = load i32, i32* %arrayidx6, align 4
  %cmp7 = icmp slt i32 %9, 0
  br i1 %cmp7, label %if.then8, label %if.else10

if.then8:                                         ; preds = %if.else
  %10 = load i32*, i32** %int_vec.addr, align 4
  %arrayidx9 = getelementptr inbounds i32, i32* %10, i32 2
  %11 = load i32, i32* %arrayidx9, align 4
  %call = call i32 @abs(i32 %11) #10
  store i32 %call, i32* %s, align 4
  br label %if.end

if.else10:                                        ; preds = %if.else
  %max_value_ = getelementptr inbounds %"class.draco::OctahedronToolBox", %"class.draco::OctahedronToolBox"* %this1, i32 0, i32 2
  %12 = load i32, i32* %max_value_, align 4
  %13 = load i32*, i32** %int_vec.addr, align 4
  %arrayidx11 = getelementptr inbounds i32, i32* %13, i32 2
  %14 = load i32, i32* %arrayidx11, align 4
  %call12 = call i32 @abs(i32 %14) #10
  %sub = sub nsw i32 %12, %call12
  store i32 %sub, i32* %s, align 4
  br label %if.end

if.end:                                           ; preds = %if.else10, %if.then8
  %15 = load i32*, i32** %int_vec.addr, align 4
  %arrayidx13 = getelementptr inbounds i32, i32* %15, i32 2
  %16 = load i32, i32* %arrayidx13, align 4
  %cmp14 = icmp slt i32 %16, 0
  br i1 %cmp14, label %if.then15, label %if.else18

if.then15:                                        ; preds = %if.end
  %17 = load i32*, i32** %int_vec.addr, align 4
  %arrayidx16 = getelementptr inbounds i32, i32* %17, i32 1
  %18 = load i32, i32* %arrayidx16, align 4
  %call17 = call i32 @abs(i32 %18) #10
  store i32 %call17, i32* %t, align 4
  br label %if.end23

if.else18:                                        ; preds = %if.end
  %max_value_19 = getelementptr inbounds %"class.draco::OctahedronToolBox", %"class.draco::OctahedronToolBox"* %this1, i32 0, i32 2
  %19 = load i32, i32* %max_value_19, align 4
  %20 = load i32*, i32** %int_vec.addr, align 4
  %arrayidx20 = getelementptr inbounds i32, i32* %20, i32 1
  %21 = load i32, i32* %arrayidx20, align 4
  %call21 = call i32 @abs(i32 %21) #10
  %sub22 = sub nsw i32 %19, %call21
  store i32 %sub22, i32* %t, align 4
  br label %if.end23

if.end23:                                         ; preds = %if.else18, %if.then15
  br label %if.end24

if.end24:                                         ; preds = %if.end23, %if.then
  %22 = load i32, i32* %s, align 4
  %23 = load i32, i32* %t, align 4
  %24 = load i32*, i32** %out_s.addr, align 4
  %25 = load i32*, i32** %out_t.addr, align 4
  call void @_ZNK5draco17OctahedronToolBox28CanonicalizeOctahedralCoordsEiiPiS1_(%"class.draco::OctahedronToolBox"* %this1, i32 %22, i32 %23, i32* %24, i32* %25)
  ret void
}

; Function Attrs: nounwind readnone speculatable willreturn
declare double @llvm.fabs.f64(double) #7

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK5draco17OctahedronToolBox28CanonicalizeOctahedralCoordsEiiPiS1_(%"class.draco::OctahedronToolBox"* %this, i32 %s, i32 %t, i32* %out_s, i32* %out_t) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::OctahedronToolBox"*, align 4
  %s.addr = alloca i32, align 4
  %t.addr = alloca i32, align 4
  %out_s.addr = alloca i32*, align 4
  %out_t.addr = alloca i32*, align 4
  store %"class.draco::OctahedronToolBox"* %this, %"class.draco::OctahedronToolBox"** %this.addr, align 4
  store i32 %s, i32* %s.addr, align 4
  store i32 %t, i32* %t.addr, align 4
  store i32* %out_s, i32** %out_s.addr, align 4
  store i32* %out_t, i32** %out_t.addr, align 4
  %this1 = load %"class.draco::OctahedronToolBox"*, %"class.draco::OctahedronToolBox"** %this.addr, align 4
  %0 = load i32, i32* %s.addr, align 4
  %cmp = icmp eq i32 %0, 0
  br i1 %cmp, label %land.lhs.true, label %lor.lhs.false

land.lhs.true:                                    ; preds = %entry
  %1 = load i32, i32* %t.addr, align 4
  %cmp2 = icmp eq i32 %1, 0
  br i1 %cmp2, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %land.lhs.true, %entry
  %2 = load i32, i32* %s.addr, align 4
  %cmp3 = icmp eq i32 %2, 0
  br i1 %cmp3, label %land.lhs.true4, label %lor.lhs.false6

land.lhs.true4:                                   ; preds = %lor.lhs.false
  %3 = load i32, i32* %t.addr, align 4
  %max_value_ = getelementptr inbounds %"class.draco::OctahedronToolBox", %"class.draco::OctahedronToolBox"* %this1, i32 0, i32 2
  %4 = load i32, i32* %max_value_, align 4
  %cmp5 = icmp eq i32 %3, %4
  br i1 %cmp5, label %if.then, label %lor.lhs.false6

lor.lhs.false6:                                   ; preds = %land.lhs.true4, %lor.lhs.false
  %5 = load i32, i32* %s.addr, align 4
  %max_value_7 = getelementptr inbounds %"class.draco::OctahedronToolBox", %"class.draco::OctahedronToolBox"* %this1, i32 0, i32 2
  %6 = load i32, i32* %max_value_7, align 4
  %cmp8 = icmp eq i32 %5, %6
  br i1 %cmp8, label %land.lhs.true9, label %if.else

land.lhs.true9:                                   ; preds = %lor.lhs.false6
  %7 = load i32, i32* %t.addr, align 4
  %cmp10 = icmp eq i32 %7, 0
  br i1 %cmp10, label %if.then, label %if.else

if.then:                                          ; preds = %land.lhs.true9, %land.lhs.true4, %land.lhs.true
  %max_value_11 = getelementptr inbounds %"class.draco::OctahedronToolBox", %"class.draco::OctahedronToolBox"* %this1, i32 0, i32 2
  %8 = load i32, i32* %max_value_11, align 4
  store i32 %8, i32* %s.addr, align 4
  %max_value_12 = getelementptr inbounds %"class.draco::OctahedronToolBox", %"class.draco::OctahedronToolBox"* %this1, i32 0, i32 2
  %9 = load i32, i32* %max_value_12, align 4
  store i32 %9, i32* %t.addr, align 4
  br label %if.end54

if.else:                                          ; preds = %land.lhs.true9, %lor.lhs.false6
  %10 = load i32, i32* %s.addr, align 4
  %cmp13 = icmp eq i32 %10, 0
  br i1 %cmp13, label %land.lhs.true14, label %if.else20

land.lhs.true14:                                  ; preds = %if.else
  %11 = load i32, i32* %t.addr, align 4
  %center_value_ = getelementptr inbounds %"class.draco::OctahedronToolBox", %"class.draco::OctahedronToolBox"* %this1, i32 0, i32 3
  %12 = load i32, i32* %center_value_, align 4
  %cmp15 = icmp sgt i32 %11, %12
  br i1 %cmp15, label %if.then16, label %if.else20

if.then16:                                        ; preds = %land.lhs.true14
  %center_value_17 = getelementptr inbounds %"class.draco::OctahedronToolBox", %"class.draco::OctahedronToolBox"* %this1, i32 0, i32 3
  %13 = load i32, i32* %center_value_17, align 4
  %14 = load i32, i32* %t.addr, align 4
  %center_value_18 = getelementptr inbounds %"class.draco::OctahedronToolBox", %"class.draco::OctahedronToolBox"* %this1, i32 0, i32 3
  %15 = load i32, i32* %center_value_18, align 4
  %sub = sub nsw i32 %14, %15
  %sub19 = sub nsw i32 %13, %sub
  store i32 %sub19, i32* %t.addr, align 4
  br label %if.end53

if.else20:                                        ; preds = %land.lhs.true14, %if.else
  %16 = load i32, i32* %s.addr, align 4
  %max_value_21 = getelementptr inbounds %"class.draco::OctahedronToolBox", %"class.draco::OctahedronToolBox"* %this1, i32 0, i32 2
  %17 = load i32, i32* %max_value_21, align 4
  %cmp22 = icmp eq i32 %16, %17
  br i1 %cmp22, label %land.lhs.true23, label %if.else30

land.lhs.true23:                                  ; preds = %if.else20
  %18 = load i32, i32* %t.addr, align 4
  %center_value_24 = getelementptr inbounds %"class.draco::OctahedronToolBox", %"class.draco::OctahedronToolBox"* %this1, i32 0, i32 3
  %19 = load i32, i32* %center_value_24, align 4
  %cmp25 = icmp slt i32 %18, %19
  br i1 %cmp25, label %if.then26, label %if.else30

if.then26:                                        ; preds = %land.lhs.true23
  %center_value_27 = getelementptr inbounds %"class.draco::OctahedronToolBox", %"class.draco::OctahedronToolBox"* %this1, i32 0, i32 3
  %20 = load i32, i32* %center_value_27, align 4
  %center_value_28 = getelementptr inbounds %"class.draco::OctahedronToolBox", %"class.draco::OctahedronToolBox"* %this1, i32 0, i32 3
  %21 = load i32, i32* %center_value_28, align 4
  %22 = load i32, i32* %t.addr, align 4
  %sub29 = sub nsw i32 %21, %22
  %add = add nsw i32 %20, %sub29
  store i32 %add, i32* %t.addr, align 4
  br label %if.end52

if.else30:                                        ; preds = %land.lhs.true23, %if.else20
  %23 = load i32, i32* %t.addr, align 4
  %max_value_31 = getelementptr inbounds %"class.draco::OctahedronToolBox", %"class.draco::OctahedronToolBox"* %this1, i32 0, i32 2
  %24 = load i32, i32* %max_value_31, align 4
  %cmp32 = icmp eq i32 %23, %24
  br i1 %cmp32, label %land.lhs.true33, label %if.else41

land.lhs.true33:                                  ; preds = %if.else30
  %25 = load i32, i32* %s.addr, align 4
  %center_value_34 = getelementptr inbounds %"class.draco::OctahedronToolBox", %"class.draco::OctahedronToolBox"* %this1, i32 0, i32 3
  %26 = load i32, i32* %center_value_34, align 4
  %cmp35 = icmp slt i32 %25, %26
  br i1 %cmp35, label %if.then36, label %if.else41

if.then36:                                        ; preds = %land.lhs.true33
  %center_value_37 = getelementptr inbounds %"class.draco::OctahedronToolBox", %"class.draco::OctahedronToolBox"* %this1, i32 0, i32 3
  %27 = load i32, i32* %center_value_37, align 4
  %center_value_38 = getelementptr inbounds %"class.draco::OctahedronToolBox", %"class.draco::OctahedronToolBox"* %this1, i32 0, i32 3
  %28 = load i32, i32* %center_value_38, align 4
  %29 = load i32, i32* %s.addr, align 4
  %sub39 = sub nsw i32 %28, %29
  %add40 = add nsw i32 %27, %sub39
  store i32 %add40, i32* %s.addr, align 4
  br label %if.end51

if.else41:                                        ; preds = %land.lhs.true33, %if.else30
  %30 = load i32, i32* %t.addr, align 4
  %cmp42 = icmp eq i32 %30, 0
  br i1 %cmp42, label %land.lhs.true43, label %if.end

land.lhs.true43:                                  ; preds = %if.else41
  %31 = load i32, i32* %s.addr, align 4
  %center_value_44 = getelementptr inbounds %"class.draco::OctahedronToolBox", %"class.draco::OctahedronToolBox"* %this1, i32 0, i32 3
  %32 = load i32, i32* %center_value_44, align 4
  %cmp45 = icmp sgt i32 %31, %32
  br i1 %cmp45, label %if.then46, label %if.end

if.then46:                                        ; preds = %land.lhs.true43
  %center_value_47 = getelementptr inbounds %"class.draco::OctahedronToolBox", %"class.draco::OctahedronToolBox"* %this1, i32 0, i32 3
  %33 = load i32, i32* %center_value_47, align 4
  %34 = load i32, i32* %s.addr, align 4
  %center_value_48 = getelementptr inbounds %"class.draco::OctahedronToolBox", %"class.draco::OctahedronToolBox"* %this1, i32 0, i32 3
  %35 = load i32, i32* %center_value_48, align 4
  %sub49 = sub nsw i32 %34, %35
  %sub50 = sub nsw i32 %33, %sub49
  store i32 %sub50, i32* %s.addr, align 4
  br label %if.end

if.end:                                           ; preds = %if.then46, %land.lhs.true43, %if.else41
  br label %if.end51

if.end51:                                         ; preds = %if.end, %if.then36
  br label %if.end52

if.end52:                                         ; preds = %if.end51, %if.then26
  br label %if.end53

if.end53:                                         ; preds = %if.end52, %if.then16
  br label %if.end54

if.end54:                                         ; preds = %if.end53, %if.then
  %36 = load i32, i32* %s.addr, align 4
  %37 = load i32*, i32** %out_s.addr, align 4
  store i32 %36, i32* %37, align 4
  %38 = load i32, i32* %t.addr, align 4
  %39 = load i32*, i32** %out_t.addr, align 4
  store i32 %38, i32* %39, align 4
  ret void
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn }
attributes #3 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { noreturn "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { noinline noreturn nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nobuiltin allocsize(0) "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { nounwind readnone speculatable willreturn }
attributes #8 = { nounwind readnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #9 = { nounwind }
attributes #10 = { nounwind readnone }
attributes #11 = { builtin nounwind }
attributes #12 = { noreturn }
attributes #13 = { builtin allocsize(0) }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
