; ModuleID = './draco/src/draco/mesh/mesh.cc'
source_filename = "./draco/src/draco/mesh/mesh.cc"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%"class.draco::Mesh" = type { %"class.draco::PointCloud", %"class.std::__2::vector.94", %"class.draco::IndexTypeVector.101" }
%"class.draco::PointCloud" = type { i32 (...)**, %"class.std::__2::unique_ptr", %"class.std::__2::vector.51", [5 x %"class.std::__2::vector.87"], i32 }
%"class.std::__2::unique_ptr" = type { %"class.std::__2::__compressed_pair" }
%"class.std::__2::__compressed_pair" = type { %"struct.std::__2::__compressed_pair_elem" }
%"struct.std::__2::__compressed_pair_elem" = type { %"class.draco::GeometryMetadata"* }
%"class.draco::GeometryMetadata" = type { %"class.draco::Metadata", %"class.std::__2::vector" }
%"class.draco::Metadata" = type { %"class.std::__2::unordered_map", %"class.std::__2::unordered_map.17" }
%"class.std::__2::unordered_map" = type { %"class.std::__2::__hash_table" }
%"class.std::__2::__hash_table" = type { %"class.std::__2::unique_ptr.0", %"class.std::__2::__compressed_pair.7", %"class.std::__2::__compressed_pair.12", %"class.std::__2::__compressed_pair.14" }
%"class.std::__2::unique_ptr.0" = type { %"class.std::__2::__compressed_pair.1" }
%"class.std::__2::__compressed_pair.1" = type { %"struct.std::__2::__compressed_pair_elem.2", %"struct.std::__2::__compressed_pair_elem.3" }
%"struct.std::__2::__compressed_pair_elem.2" = type { %"struct.std::__2::__hash_node_base"** }
%"struct.std::__2::__hash_node_base" = type { %"struct.std::__2::__hash_node_base"* }
%"struct.std::__2::__compressed_pair_elem.3" = type { %"class.std::__2::__bucket_list_deallocator" }
%"class.std::__2::__bucket_list_deallocator" = type { %"class.std::__2::__compressed_pair.4" }
%"class.std::__2::__compressed_pair.4" = type { %"struct.std::__2::__compressed_pair_elem.5" }
%"struct.std::__2::__compressed_pair_elem.5" = type { i32 }
%"class.std::__2::__compressed_pair.7" = type { %"struct.std::__2::__compressed_pair_elem.8" }
%"struct.std::__2::__compressed_pair_elem.8" = type { %"struct.std::__2::__hash_node_base" }
%"class.std::__2::__compressed_pair.12" = type { %"struct.std::__2::__compressed_pair_elem.5" }
%"class.std::__2::__compressed_pair.14" = type { %"struct.std::__2::__compressed_pair_elem.15" }
%"struct.std::__2::__compressed_pair_elem.15" = type { float }
%"class.std::__2::unordered_map.17" = type { %"class.std::__2::__hash_table.18" }
%"class.std::__2::__hash_table.18" = type { %"class.std::__2::unique_ptr.19", %"class.std::__2::__compressed_pair.29", %"class.std::__2::__compressed_pair.34", %"class.std::__2::__compressed_pair.37" }
%"class.std::__2::unique_ptr.19" = type { %"class.std::__2::__compressed_pair.20" }
%"class.std::__2::__compressed_pair.20" = type { %"struct.std::__2::__compressed_pair_elem.21", %"struct.std::__2::__compressed_pair_elem.23" }
%"struct.std::__2::__compressed_pair_elem.21" = type { %"struct.std::__2::__hash_node_base.22"** }
%"struct.std::__2::__hash_node_base.22" = type { %"struct.std::__2::__hash_node_base.22"* }
%"struct.std::__2::__compressed_pair_elem.23" = type { %"class.std::__2::__bucket_list_deallocator.24" }
%"class.std::__2::__bucket_list_deallocator.24" = type { %"class.std::__2::__compressed_pair.25" }
%"class.std::__2::__compressed_pair.25" = type { %"struct.std::__2::__compressed_pair_elem.5" }
%"class.std::__2::__compressed_pair.29" = type { %"struct.std::__2::__compressed_pair_elem.30" }
%"struct.std::__2::__compressed_pair_elem.30" = type { %"struct.std::__2::__hash_node_base.22" }
%"class.std::__2::__compressed_pair.34" = type { %"struct.std::__2::__compressed_pair_elem.5" }
%"class.std::__2::__compressed_pair.37" = type { %"struct.std::__2::__compressed_pair_elem.15" }
%"class.std::__2::vector" = type { %"class.std::__2::__vector_base" }
%"class.std::__2::__vector_base" = type { %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"*, %"class.std::__2::__compressed_pair.44" }
%"class.std::__2::unique_ptr.40" = type { %"class.std::__2::__compressed_pair.41" }
%"class.std::__2::__compressed_pair.41" = type { %"struct.std::__2::__compressed_pair_elem.42" }
%"struct.std::__2::__compressed_pair_elem.42" = type { %"class.draco::AttributeMetadata"* }
%"class.draco::AttributeMetadata" = type { %"class.draco::Metadata", i32 }
%"class.std::__2::__compressed_pair.44" = type { %"struct.std::__2::__compressed_pair_elem.45" }
%"struct.std::__2::__compressed_pair_elem.45" = type { %"class.std::__2::unique_ptr.40"* }
%"class.std::__2::vector.51" = type { %"class.std::__2::__vector_base.52" }
%"class.std::__2::__vector_base.52" = type { %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"*, %"class.std::__2::__compressed_pair.82" }
%"class.std::__2::unique_ptr.53" = type { %"class.std::__2::__compressed_pair.54" }
%"class.std::__2::__compressed_pair.54" = type { %"struct.std::__2::__compressed_pair_elem.55" }
%"struct.std::__2::__compressed_pair_elem.55" = type { %"class.draco::PointAttribute"* }
%"class.draco::PointAttribute" = type <{ %"class.draco::GeometryAttribute", %"class.std::__2::unique_ptr.63", %"class.draco::IndexTypeVector", i32, i8, [3 x i8], %"class.std::__2::unique_ptr.75", [4 x i8] }>
%"class.draco::GeometryAttribute" = type { %"class.draco::DataBuffer"*, %"struct.draco::DataBufferDescriptor", i8, i32, i8, i64, i64, i32, i32 }
%"class.draco::DataBuffer" = type { %"class.std::__2::vector.56", %"struct.draco::DataBufferDescriptor" }
%"class.std::__2::vector.56" = type { %"class.std::__2::__vector_base.57" }
%"class.std::__2::__vector_base.57" = type { i8*, i8*, %"class.std::__2::__compressed_pair.58" }
%"class.std::__2::__compressed_pair.58" = type { %"struct.std::__2::__compressed_pair_elem.59" }
%"struct.std::__2::__compressed_pair_elem.59" = type { i8* }
%"struct.draco::DataBufferDescriptor" = type { i64, i64 }
%"class.std::__2::unique_ptr.63" = type { %"class.std::__2::__compressed_pair.64" }
%"class.std::__2::__compressed_pair.64" = type { %"struct.std::__2::__compressed_pair_elem.65" }
%"struct.std::__2::__compressed_pair_elem.65" = type { %"class.draco::DataBuffer"* }
%"class.draco::IndexTypeVector" = type { %"class.std::__2::vector.68" }
%"class.std::__2::vector.68" = type { %"class.std::__2::__vector_base.69" }
%"class.std::__2::__vector_base.69" = type { %"class.draco::IndexType"*, %"class.draco::IndexType"*, %"class.std::__2::__compressed_pair.70" }
%"class.draco::IndexType" = type { i32 }
%"class.std::__2::__compressed_pair.70" = type { %"struct.std::__2::__compressed_pair_elem.71" }
%"struct.std::__2::__compressed_pair_elem.71" = type { %"class.draco::IndexType"* }
%"class.std::__2::unique_ptr.75" = type { %"class.std::__2::__compressed_pair.76" }
%"class.std::__2::__compressed_pair.76" = type { %"struct.std::__2::__compressed_pair_elem.77" }
%"struct.std::__2::__compressed_pair_elem.77" = type { %"class.draco::AttributeTransformData"* }
%"class.draco::AttributeTransformData" = type { i32, %"class.draco::DataBuffer" }
%"class.std::__2::__compressed_pair.82" = type { %"struct.std::__2::__compressed_pair_elem.83" }
%"struct.std::__2::__compressed_pair_elem.83" = type { %"class.std::__2::unique_ptr.53"* }
%"class.std::__2::vector.87" = type { %"class.std::__2::__vector_base.88" }
%"class.std::__2::__vector_base.88" = type { i32*, i32*, %"class.std::__2::__compressed_pair.89" }
%"class.std::__2::__compressed_pair.89" = type { %"struct.std::__2::__compressed_pair_elem.90" }
%"struct.std::__2::__compressed_pair_elem.90" = type { i32* }
%"class.std::__2::vector.94" = type { %"class.std::__2::__vector_base.95" }
%"class.std::__2::__vector_base.95" = type { %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"*, %"class.std::__2::__compressed_pair.96" }
%"struct.draco::Mesh::AttributeData" = type { i32 }
%"class.std::__2::__compressed_pair.96" = type { %"struct.std::__2::__compressed_pair_elem.97" }
%"struct.std::__2::__compressed_pair_elem.97" = type { %"struct.draco::Mesh::AttributeData"* }
%"class.draco::IndexTypeVector.101" = type { %"class.std::__2::vector.102" }
%"class.std::__2::vector.102" = type { %"class.std::__2::__vector_base.103" }
%"class.std::__2::__vector_base.103" = type { %"struct.std::__2::array"*, %"struct.std::__2::array"*, %"class.std::__2::__compressed_pair.105" }
%"struct.std::__2::array" = type { [3 x %"class.draco::IndexType.104"] }
%"class.draco::IndexType.104" = type { i32 }
%"class.std::__2::__compressed_pair.105" = type { %"struct.std::__2::__compressed_pair_elem.106" }
%"struct.std::__2::__compressed_pair_elem.106" = type { %"struct.std::__2::array"* }
%"class.std::__2::__wrap_iter" = type { %"struct.draco::Mesh::AttributeData"* }
%"class.std::__2::__wrap_iter.134" = type { %"struct.draco::Mesh::AttributeData"* }
%"struct.std::__2::__default_init_tag" = type { i8 }
%"class.std::__2::__vector_base_common" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.98" = type { i8 }
%"class.std::__2::allocator.99" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.107" = type { i8 }
%"class.std::__2::allocator.108" = type { i8 }
%"struct.std::__2::integral_constant" = type { i8 }
%"struct.std::__2::__has_destroy" = type { i8 }
%"struct.std::__2::__has_destroy.110" = type { i8 }
%"class.std::__2::allocator.92" = type { i8 }
%"struct.std::__2::__has_destroy.111" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.91" = type { i8 }
%"class.std::__2::allocator.85" = type { i8 }
%"struct.std::__2::__has_destroy.112" = type { i8 }
%"struct.std::__2::default_delete.81" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.80" = type { i8 }
%"struct.std::__2::default_delete.79" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.78" = type { i8 }
%"class.std::__2::allocator.61" = type { i8 }
%"struct.std::__2::__has_destroy.113" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.60" = type { i8 }
%"class.std::__2::allocator.73" = type { i8 }
%"struct.std::__2::__has_destroy.114" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.72" = type { i8 }
%"struct.std::__2::default_delete.67" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.66" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.84" = type { i8 }
%"struct.std::__2::default_delete.50" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.49" = type { i8 }
%"class.std::__2::allocator.47" = type { i8 }
%"struct.std::__2::__has_destroy.115" = type { i8 }
%"struct.std::__2::default_delete" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.43" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.46" = type { i8 }
%"class.std::__2::allocator.32" = type { i8 }
%"struct.std::__2::__hash_node" = type { %"struct.std::__2::__hash_node_base.22", i32, %"struct.std::__2::__hash_value_type" }
%"struct.std::__2::__hash_value_type" = type { %"struct.std::__2::pair" }
%"struct.std::__2::pair" = type { %"class.std::__2::basic_string", %"class.std::__2::unique_ptr.121" }
%"class.std::__2::basic_string" = type { %"class.std::__2::__compressed_pair.116" }
%"class.std::__2::__compressed_pair.116" = type { %"struct.std::__2::__compressed_pair_elem.117" }
%"struct.std::__2::__compressed_pair_elem.117" = type { %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep" }
%"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep" = type { %union.anon }
%union.anon = type { %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long" }
%"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long" = type { i8*, i32, i32 }
%"class.std::__2::unique_ptr.121" = type { %"class.std::__2::__compressed_pair.122" }
%"class.std::__2::__compressed_pair.122" = type { %"struct.std::__2::__compressed_pair_elem.123" }
%"struct.std::__2::__compressed_pair_elem.123" = type { %"class.draco::Metadata"* }
%"struct.std::__2::integral_constant.126" = type { i8 }
%"struct.std::__2::__has_destroy.127" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.31" = type { i8 }
%"struct.std::__2::default_delete.125" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.124" = type { i8 }
%"class.std::__2::allocator.27" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.26" = type { i8 }
%"class.std::__2::allocator.10" = type { i8 }
%"struct.std::__2::__hash_node.128" = type { %"struct.std::__2::__hash_node_base", i32, %"struct.std::__2::__hash_value_type.129" }
%"struct.std::__2::__hash_value_type.129" = type { %"struct.std::__2::pair.130" }
%"struct.std::__2::pair.130" = type { %"class.std::__2::basic_string", %"class.draco::EntryValue" }
%"class.draco::EntryValue" = type { %"class.std::__2::vector.56" }
%"struct.std::__2::__has_destroy.131" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.9" = type { i8 }
%"class.std::__2::allocator" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.6" = type { i8 }
%"struct.std::__2::__split_buffer" = type { %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"*, %"class.std::__2::__compressed_pair.132" }
%"class.std::__2::__compressed_pair.132" = type { %"struct.std::__2::__compressed_pair_elem.97", %"struct.std::__2::__compressed_pair_elem.133" }
%"struct.std::__2::__compressed_pair_elem.133" = type { %"class.std::__2::allocator.99"* }
%"struct.std::__2::vector<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData>>::_ConstructTransaction" = type { %"class.std::__2::vector.94"*, %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"* }
%"class.std::__2::__split_buffer_common" = type { i8 }
%"struct.std::__2::__split_buffer<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData> &>::_ConstructTransaction" = type { %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** }
%"struct.std::__2::__has_construct" = type { i8 }
%"struct.std::__2::__less" = type { i8 }
%"struct.std::__2::__has_max_size" = type { i8 }

$_ZNSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEEC2Ev = comdat any

$_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEENSt3__25arrayINS1_IjNS_20PointIndex_tag_type_EEELm3EEEEC2Ev = comdat any

$_ZN5draco4MeshD2Ev = comdat any

$_ZN5draco4MeshD0Ev = comdat any

$_ZN5draco4Mesh12SetAttributeEiNSt3__210unique_ptrINS_14PointAttributeENS1_14default_deleteIS3_EEEE = comdat any

$_ZN5draco4Mesh15DeleteAttributeEi = comdat any

$_ZNSt3__213__vector_baseIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEEC2Ev = comdat any

$_ZNSt3__220__vector_base_commonILb1EEC2Ev = comdat any

$_ZNSt3__217__compressed_pairIPN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEEC2IDnNS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco4Mesh13AttributeDataELi0ELb0EEC2IDnvEEOT_ = comdat any

$_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco4Mesh13AttributeDataEEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__29allocatorIN5draco4Mesh13AttributeDataEEC2Ev = comdat any

$_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEEC2Ev = comdat any

$_ZNSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEEC2Ev = comdat any

$_ZNSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEEC2IDnNS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__222__compressed_pair_elemIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEELi0ELb0EEC2IDnvEEOT_ = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEEC2Ev = comdat any

$_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEENSt3__25arrayINS1_IjNS_20PointIndex_tag_type_EEELm3EEEED2Ev = comdat any

$_ZNSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEED2Ev = comdat any

$_ZN5draco10PointCloudD2Ev = comdat any

$_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEED2Ev = comdat any

$_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE17__annotate_deleteEv = comdat any

$_ZNSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEED2Ev = comdat any

$_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE31__annotate_contiguous_containerEPKvSB_SB_SB_ = comdat any

$_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4dataEv = comdat any

$_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE8capacityEv = comdat any

$_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4sizeEv = comdat any

$_ZNSt3__212__to_addressINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEEEPT_S8_ = comdat any

$_ZNKSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE5clearEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE10deallocateERS8_PS7_m = comdat any

$_ZNSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE7__allocEv = comdat any

$_ZNSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE17__destruct_at_endEPS6_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE7destroyIS7_EEvRS8_PT_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE9__destroyIS7_EEvNS_17integral_constantIbLb1EEERS8_PT_ = comdat any

$_ZNSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEE7destroyEPS6_ = comdat any

$_ZNSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEE10deallocateEPS6_m = comdat any

$_ZNSt3__219__libcpp_deallocateEPvmm = comdat any

$_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm = comdat any

$_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm = comdat any

$_ZNSt3__217_DeallocateCaller9__do_callEPv = comdat any

$_ZNSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEELi1ELb1EE5__getEv = comdat any

$_ZNKSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE17__annotate_deleteEv = comdat any

$_ZNSt3__213__vector_baseIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEED2Ev = comdat any

$_ZNKSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE31__annotate_contiguous_containerEPKvS8_S8_S8_ = comdat any

$_ZNKSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE4dataEv = comdat any

$_ZNKSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE8capacityEv = comdat any

$_ZNKSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE4sizeEv = comdat any

$_ZNSt3__212__to_addressIN5draco4Mesh13AttributeDataEEEPT_S5_ = comdat any

$_ZNKSt3__213__vector_baseIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPN5draco4Mesh13AttributeDataELi0ELb0EE5__getEv = comdat any

$_ZNSt3__213__vector_baseIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE5clearEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco4Mesh13AttributeDataEEEE10deallocateERS5_PS4_m = comdat any

$_ZNSt3__213__vector_baseIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE7__allocEv = comdat any

$_ZNSt3__213__vector_baseIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE17__destruct_at_endEPS3_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco4Mesh13AttributeDataEEEE7destroyIS4_EEvRS5_PT_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco4Mesh13AttributeDataEEEE9__destroyIS4_EEvNS_17integral_constantIbLb1EEERS5_PT_ = comdat any

$_ZNSt3__29allocatorIN5draco4Mesh13AttributeDataEE7destroyEPS3_ = comdat any

$_ZNSt3__29allocatorIN5draco4Mesh13AttributeDataEE10deallocateEPS3_m = comdat any

$_ZNSt3__217__compressed_pairIPN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco4Mesh13AttributeDataEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__26vectorIiNS_9allocatorIiEEED2Ev = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEED2Ev = comdat any

$_ZNSt3__210unique_ptrIN5draco16GeometryMetadataENS_14default_deleteIS2_EEED2Ev = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEE17__annotate_deleteEv = comdat any

$_ZNSt3__213__vector_baseIiNS_9allocatorIiEEED2Ev = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEE31__annotate_contiguous_containerEPKvS5_S5_S5_ = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEE8capacityEv = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEE4sizeEv = comdat any

$_ZNSt3__212__to_addressIiEEPT_S2_ = comdat any

$_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPiNS_9allocatorIiEEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPiLi0ELb0EE5__getEv = comdat any

$_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE5clearEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIiEEE10deallocateERS2_Pim = comdat any

$_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE7__allocEv = comdat any

$_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE17__destruct_at_endEPi = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIiEEE7destroyIiEEvRS2_PT_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIiEEE9__destroyIiEEvNS_17integral_constantIbLb1EEERS2_PT_ = comdat any

$_ZNSt3__29allocatorIiE7destroyEPi = comdat any

$_ZNSt3__29allocatorIiE10deallocateEPim = comdat any

$_ZNSt3__217__compressed_pairIPiNS_9allocatorIiEEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIiEELi1ELb1EE5__getEv = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__annotate_deleteEv = comdat any

$_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEED2Ev = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE31__annotate_contiguous_containerEPKvSB_SB_SB_ = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv = comdat any

$_ZNSt3__212__to_addressINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEEEPT_S8_ = comdat any

$_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5clearEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS4_EEEEEEE10deallocateERS8_PS7_m = comdat any

$_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv = comdat any

$_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__destruct_at_endEPS6_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS4_EEEEEEE7destroyIS7_EEvRS8_PT_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS4_EEEEEEE9__destroyIS7_EEvNS_17integral_constantIbLb1EEERS8_PT_ = comdat any

$_ZNSt3__29allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEE7destroyEPS6_ = comdat any

$_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEED2Ev = comdat any

$_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEE5resetEPS2_ = comdat any

$_ZNSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE6secondEv = comdat any

$_ZNKSt3__214default_deleteIN5draco14PointAttributeEEclEPS2_ = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco14PointAttributeELi0ELb0EE5__getEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco14PointAttributeEEELi1ELb1EE5__getEv = comdat any

$_ZN5draco14PointAttributeD2Ev = comdat any

$_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEED2Ev = comdat any

$_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEEED2Ev = comdat any

$_ZNSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEED2Ev = comdat any

$_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE5resetEPS2_ = comdat any

$_ZNSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE6secondEv = comdat any

$_ZNKSt3__214default_deleteIN5draco22AttributeTransformDataEEclEPS2_ = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco22AttributeTransformDataELi0ELb0EE5__getEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco22AttributeTransformDataEEELi1ELb1EE5__getEv = comdat any

$_ZN5draco22AttributeTransformDataD2Ev = comdat any

$_ZN5draco10DataBufferD2Ev = comdat any

$_ZNSt3__26vectorIhNS_9allocatorIhEEED2Ev = comdat any

$_ZNKSt3__26vectorIhNS_9allocatorIhEEE17__annotate_deleteEv = comdat any

$_ZNSt3__213__vector_baseIhNS_9allocatorIhEEED2Ev = comdat any

$_ZNKSt3__26vectorIhNS_9allocatorIhEEE31__annotate_contiguous_containerEPKvS5_S5_S5_ = comdat any

$_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv = comdat any

$_ZNKSt3__26vectorIhNS_9allocatorIhEEE8capacityEv = comdat any

$_ZNKSt3__26vectorIhNS_9allocatorIhEEE4sizeEv = comdat any

$_ZNSt3__212__to_addressIhEEPT_S2_ = comdat any

$_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPhNS_9allocatorIhEEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPhLi0ELb0EE5__getEv = comdat any

$_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE5clearEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIhEEE10deallocateERS2_Phm = comdat any

$_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE7__allocEv = comdat any

$_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE17__destruct_at_endEPh = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIhEEE7destroyIhEEvRS2_PT_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIhEEE9__destroyIhEEvNS_17integral_constantIbLb1EEERS2_PT_ = comdat any

$_ZNSt3__29allocatorIhE7destroyEPh = comdat any

$_ZNSt3__29allocatorIhE10deallocateEPhm = comdat any

$_ZNSt3__217__compressed_pairIPhNS_9allocatorIhEEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIhEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEED2Ev = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_deleteEv = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEED2Ev = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_ = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv = comdat any

$_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEPT_S6_ = comdat any

$_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5clearEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE10deallocateERS6_PS5_m = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE7destroyIS5_EEvRS6_PT_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE9__destroyIS5_EEvNS_17integral_constantIbLb1EEERS6_PT_ = comdat any

$_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE7destroyEPS4_ = comdat any

$_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE10deallocateEPS4_m = comdat any

$_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEE5resetEPS2_ = comdat any

$_ZNSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEE6secondEv = comdat any

$_ZNKSt3__214default_deleteIN5draco10DataBufferEEclEPS2_ = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco10DataBufferELi0ELb0EE5__getEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco10DataBufferEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__29allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEE10deallocateEPS6_m = comdat any

$_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS4_EEEEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__210unique_ptrIN5draco16GeometryMetadataENS_14default_deleteIS2_EEE5resetEPS2_ = comdat any

$_ZNSt3__217__compressed_pairIPN5draco16GeometryMetadataENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco16GeometryMetadataENS_14default_deleteIS2_EEE6secondEv = comdat any

$_ZNKSt3__214default_deleteIN5draco16GeometryMetadataEEclEPS2_ = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco16GeometryMetadataELi0ELb0EE5__getEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco16GeometryMetadataEEELi1ELb1EE5__getEv = comdat any

$_ZN5draco16GeometryMetadataD2Ev = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEED2Ev = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__annotate_deleteEv = comdat any

$_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEED2Ev = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE31__annotate_contiguous_containerEPKvSB_SB_SB_ = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv = comdat any

$_ZNSt3__212__to_addressINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEEPT_S8_ = comdat any

$_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5clearEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEEE10deallocateERS8_PS7_m = comdat any

$_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv = comdat any

$_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__destruct_at_endEPS6_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEEE7destroyIS7_EEvRS8_PT_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEEE9__destroyIS7_EEvNS_17integral_constantIbLb1EEERS8_PT_ = comdat any

$_ZNSt3__29allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEE7destroyEPS6_ = comdat any

$_ZNSt3__210unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS2_EEED2Ev = comdat any

$_ZNSt3__210unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS2_EEE5resetEPS2_ = comdat any

$_ZNSt3__217__compressed_pairIPN5draco17AttributeMetadataENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco17AttributeMetadataENS_14default_deleteIS2_EEE6secondEv = comdat any

$_ZNKSt3__214default_deleteIN5draco17AttributeMetadataEEclEPS2_ = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco17AttributeMetadataELi0ELb0EE5__getEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco17AttributeMetadataEEELi1ELb1EE5__getEv = comdat any

$_ZN5draco17AttributeMetadataD2Ev = comdat any

$_ZNSt3__29allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEE10deallocateEPS6_m = comdat any

$_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__213unordered_mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteIS9_EEEENS_4hashIS6_EENS_8equal_toIS6_EENS4_INS_4pairIKS6_SC_EEEEED2Ev = comdat any

$_ZNSt3__213unordered_mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueENS_4hashIS6_EENS_8equal_toIS6_EENS4_INS_4pairIKS6_S8_EEEEED2Ev = comdat any

$_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEEENS_22__unordered_map_hasherIS7_SE_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SE_NS_8equal_toIS7_EELb1EEENS5_ISE_EEED2Ev = comdat any

$_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEEENS_22__unordered_map_hasherIS7_SE_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SE_NS_8equal_toIS7_EELb1EEENS5_ISE_EEE17__deallocate_nodeEPNS_16__hash_node_baseIPNS_11__hash_nodeISE_PvEEEE = comdat any

$_ZNSt3__217__compressed_pairINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEENS7_ISI_EEE5firstEv = comdat any

$_ZNSt3__210unique_ptrIA_PNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS0_IN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISK_EEEEED2Ev = comdat any

$_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEEENS_22__unordered_map_hasherIS7_SE_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SE_NS_8equal_toIS7_EELb1EEENS5_ISE_EEE12__node_allocEv = comdat any

$_ZNSt3__216__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEE8__upcastEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEEE7destroyINS_4pairIKS8_SE_EEEEvRSI_PT_ = comdat any

$_ZNSt3__222__hash_key_value_typesINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEEEE9__get_ptrERSE_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEEE10deallocateERSI_PSH_m = comdat any

$_ZNSt3__217__compressed_pairINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEENS7_ISI_EEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__214pointer_traitsIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEE10pointer_toERSK_ = comdat any

$_ZNSt3__29addressofINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEPT_RSL_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEEE9__destroyINS_4pairIKS8_SE_EEEEvNS_17integral_constantIbLb0EEERSI_PT_ = comdat any

$_ZNSt3__24pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEED2Ev = comdat any

$_ZNSt3__210unique_ptrIN5draco8MetadataENS_14default_deleteIS2_EEED2Ev = comdat any

$_ZNSt3__210unique_ptrIN5draco8MetadataENS_14default_deleteIS2_EEE5resetEPS2_ = comdat any

$_ZNSt3__217__compressed_pairIPN5draco8MetadataENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco8MetadataENS_14default_deleteIS2_EEE6secondEv = comdat any

$_ZNKSt3__214default_deleteIN5draco8MetadataEEclEPS2_ = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco8MetadataELi0ELb0EE5__getEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco8MetadataEEELi1ELb1EE5__getEv = comdat any

$_ZN5draco8MetadataD2Ev = comdat any

$_ZNSt3__29addressofINS_4pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEEEPT_RSG_ = comdat any

$_ZNSt3__217__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteIS9_EEEEE11__get_valueEv = comdat any

$_ZNSt3__29allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEEEPvEEE10deallocateEPSG_m = comdat any

$_ZNSt3__222__compressed_pair_elemINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__210unique_ptrIA_PNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS0_IN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISK_EEEEE5resetEDn = comdat any

$_ZNSt3__217__compressed_pairIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISL_EEEEE5firstEv = comdat any

$_ZNSt3__217__compressed_pairIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISL_EEEEE6secondEv = comdat any

$_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEEclEPSL_ = comdat any

$_ZNSt3__222__compressed_pair_elemIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_25__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS2_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISD_EEEEEEPvEEEEEEEELi1ELb0EE5__getEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEE10deallocateERSM_PSL_m = comdat any

$_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEE7__allocEv = comdat any

$_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEE4sizeEv = comdat any

$_ZNSt3__29allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEEE10deallocateEPSK_m = comdat any

$_ZNSt3__217__compressed_pairImNS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairImNS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEE5firstEv = comdat any

$_ZNSt3__222__compressed_pair_elemImLi0ELb0EE5__getEv = comdat any

$_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEENS_22__unordered_map_hasherIS7_SA_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SA_NS_8equal_toIS7_EELb1EEENS5_ISA_EEED2Ev = comdat any

$_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEENS_22__unordered_map_hasherIS7_SA_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SA_NS_8equal_toIS7_EELb1EEENS5_ISA_EEE17__deallocate_nodeEPNS_16__hash_node_baseIPNS_11__hash_nodeISA_PvEEEE = comdat any

$_ZNSt3__217__compressed_pairINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS7_ISE_EEE5firstEv = comdat any

$_ZNSt3__210unique_ptrIA_PNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISH_EEEEED2Ev = comdat any

$_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEENS_22__unordered_map_hasherIS7_SA_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SA_NS_8equal_toIS7_EELb1EEENS5_ISA_EEE12__node_allocEv = comdat any

$_ZNSt3__216__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEE8__upcastEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEE7destroyINS_4pairIKS8_SA_EEEEvRSE_PT_ = comdat any

$_ZNSt3__222__hash_key_value_typesINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEE9__get_ptrERSA_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEE10deallocateERSE_PSD_m = comdat any

$_ZNSt3__217__compressed_pairINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS7_ISE_EEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__214pointer_traitsIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEEE10pointer_toERSG_ = comdat any

$_ZNSt3__29addressofINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEEEEPT_RSH_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEE9__destroyINS_4pairIKS8_SA_EEEEvNS_17integral_constantIbLb0EEERSE_PT_ = comdat any

$_ZNSt3__24pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEED2Ev = comdat any

$_ZN5draco10EntryValueD2Ev = comdat any

$_ZNSt3__29addressofINS_4pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEEEPT_RSC_ = comdat any

$_ZNSt3__217__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEE11__get_valueEv = comdat any

$_ZNSt3__29allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEEN5draco10EntryValueEEEPvEEE10deallocateEPSC_m = comdat any

$_ZNSt3__222__compressed_pair_elemINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__210unique_ptrIA_PNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISH_EEEEE5resetEDn = comdat any

$_ZNSt3__217__compressed_pairIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISH_EEEEE5firstEv = comdat any

$_ZNSt3__217__compressed_pairIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISH_EEEEE6secondEv = comdat any

$_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEEclEPSH_ = comdat any

$_ZNSt3__222__compressed_pair_elemIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_25__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS2_IcEEEEN5draco10EntryValueEEEPvEEEEEEEELi1ELb0EE5__getEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEE10deallocateERSI_PSH_m = comdat any

$_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEE7__allocEv = comdat any

$_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEE4sizeEv = comdat any

$_ZNSt3__29allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEEN5draco10EntryValueEEEPvEEEEE10deallocateEPSG_m = comdat any

$_ZNSt3__217__compressed_pairImNS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairImNS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEE5firstEv = comdat any

$_ZNSt3__24moveIRNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOS9_ = comdat any

$_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEEC2EOS5_ = comdat any

$_ZNSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE6resizeEm = comdat any

$_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEE7releaseEv = comdat any

$_ZNSt3__27forwardINS_14default_deleteIN5draco14PointAttributeEEEEEOT_RNS_16remove_referenceIS5_E4typeE = comdat any

$_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEE11get_deleterEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEEC2IS3_S5_EEOT_OT0_ = comdat any

$_ZNSt3__27forwardIPN5draco14PointAttributeEEEOT_RNS_16remove_referenceIS4_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco14PointAttributeELi0ELb0EEC2IS3_vEEOT_ = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco14PointAttributeEEELi1ELb1EEC2IS4_vEEOT_ = comdat any

$_ZNSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE8__appendEm = comdat any

$_ZNSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE17__destruct_at_endEPS3_ = comdat any

$_ZNSt3__213__vector_baseIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE9__end_capEv = comdat any

$_ZNSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE18__construct_at_endEm = comdat any

$_ZNKSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE11__recommendEm = comdat any

$_ZNSt3__214__split_bufferIN5draco4Mesh13AttributeDataERNS_9allocatorIS3_EEEC2EmmS6_ = comdat any

$_ZNSt3__214__split_bufferIN5draco4Mesh13AttributeDataERNS_9allocatorIS3_EEE18__construct_at_endEm = comdat any

$_ZNSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS3_RS5_EE = comdat any

$_ZNSt3__214__split_bufferIN5draco4Mesh13AttributeDataERNS_9allocatorIS3_EEED2Ev = comdat any

$_ZNSt3__217__compressed_pairIPN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE5firstEv = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco4Mesh13AttributeDataELi0ELb0EE5__getEv = comdat any

$_ZNSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE21_ConstructTransactionC2ERS6_m = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco4Mesh13AttributeDataEEEE9constructIS4_JEEEvRS5_PT_DpOT0_ = comdat any

$_ZNSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE21_ConstructTransactionD2Ev = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco4Mesh13AttributeDataEEEE11__constructIS4_JEEEvNS_17integral_constantIbLb1EEERS5_PT_DpOT0_ = comdat any

$_ZNSt3__29allocatorIN5draco4Mesh13AttributeDataEE9constructIS3_JEEEvPT_DpOT0_ = comdat any

$_ZN5draco4Mesh13AttributeDataC2Ev = comdat any

$_ZNKSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE8max_sizeEv = comdat any

$_ZNSt3__23maxImEERKT_S3_S3_ = comdat any

$_ZNSt3__23minImEERKT_S3_S3_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco4Mesh13AttributeDataEEEE8max_sizeERKS5_ = comdat any

$_ZNKSt3__213__vector_baseIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE7__allocEv = comdat any

$_ZNSt3__214numeric_limitsIlE3maxEv = comdat any

$_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_ = comdat any

$_ZNKSt3__26__lessImmEclERKmS3_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco4Mesh13AttributeDataEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS5_ = comdat any

$_ZNKSt3__29allocatorIN5draco4Mesh13AttributeDataEE8max_sizeEv = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE6secondEv = comdat any

$_ZNKSt3__222__compressed_pair_elemINS_9allocatorIN5draco4Mesh13AttributeDataEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv = comdat any

$_ZNSt3__23maxImNS_6__lessImmEEEERKT_S5_S5_T0_ = comdat any

$_ZNSt3__217__compressed_pairIPN5draco4Mesh13AttributeDataERNS_9allocatorIS3_EEEC2IDnS7_EEOT_OT0_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco4Mesh13AttributeDataEEEE8allocateERS5_m = comdat any

$_ZNSt3__214__split_bufferIN5draco4Mesh13AttributeDataERNS_9allocatorIS3_EEE7__allocEv = comdat any

$_ZNSt3__214__split_bufferIN5draco4Mesh13AttributeDataERNS_9allocatorIS3_EEE9__end_capEv = comdat any

$_ZNSt3__27forwardIRNS_9allocatorIN5draco4Mesh13AttributeDataEEEEEOT_RNS_16remove_referenceIS7_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIN5draco4Mesh13AttributeDataEEELi1ELb0EEC2IS6_vEEOT_ = comdat any

$_ZNSt3__29allocatorIN5draco4Mesh13AttributeDataEE8allocateEmPKv = comdat any

$_ZNSt3__220__throw_length_errorEPKc = comdat any

$_ZNSt3__217__libcpp_allocateEmm = comdat any

$_ZNSt3__217__compressed_pairIPN5draco4Mesh13AttributeDataERNS_9allocatorIS3_EEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIN5draco4Mesh13AttributeDataEEELi1ELb0EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco4Mesh13AttributeDataERNS_9allocatorIS3_EEE5firstEv = comdat any

$_ZNSt3__214__split_bufferIN5draco4Mesh13AttributeDataERNS_9allocatorIS3_EEE21_ConstructTransactionC2EPPS3_m = comdat any

$_ZNSt3__214__split_bufferIN5draco4Mesh13AttributeDataERNS_9allocatorIS3_EEE21_ConstructTransactionD2Ev = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco4Mesh13AttributeDataEEEE46__construct_backward_with_exception_guaranteesIS4_EENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS5_PT_SB_EE5valuesr31is_trivially_move_constructibleISB_EE5valueEvE4typeERS5_SC_SC_RSC_ = comdat any

$_ZNSt3__24swapIPN5draco4Mesh13AttributeDataEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS6_EE5valueEvE4typeERS6_S9_ = comdat any

$_ZNKSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE14__annotate_newEm = comdat any

$_ZNSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE26__invalidate_all_iteratorsEv = comdat any

$_ZNSt3__24moveIRPN5draco4Mesh13AttributeDataEEEONS_16remove_referenceIT_E4typeEOS7_ = comdat any

$_ZNSt3__214__split_bufferIN5draco4Mesh13AttributeDataERNS_9allocatorIS3_EEE5clearEv = comdat any

$_ZNKSt3__214__split_bufferIN5draco4Mesh13AttributeDataERNS_9allocatorIS3_EEE8capacityEv = comdat any

$_ZNSt3__214__split_bufferIN5draco4Mesh13AttributeDataERNS_9allocatorIS3_EEE17__destruct_at_endEPS3_ = comdat any

$_ZNSt3__214__split_bufferIN5draco4Mesh13AttributeDataERNS_9allocatorIS3_EEE17__destruct_at_endEPS3_NS_17integral_constantIbLb0EEE = comdat any

$_ZNKSt3__214__split_bufferIN5draco4Mesh13AttributeDataERNS_9allocatorIS3_EEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco4Mesh13AttributeDataERNS_9allocatorIS3_EEE5firstEv = comdat any

$_ZNSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE27__invalidate_iterators_pastEPS3_ = comdat any

$_ZNKSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE17__annotate_shrinkEm = comdat any

$_ZNSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE5eraseENS_11__wrap_iterIPKS3_EE = comdat any

$_ZNSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE5beginEv = comdat any

$_ZNKSt3__211__wrap_iterIPN5draco4Mesh13AttributeDataEEplEl = comdat any

$_ZNSt3__211__wrap_iterIPKN5draco4Mesh13AttributeDataEEC2IPS3_EERKNS0_IT_EEPNS_9enable_ifIXsr14is_convertibleIS9_S5_EE5valueEvE4typeE = comdat any

$_ZNSt3__2miIPKN5draco4Mesh13AttributeDataES5_EEDTmicldtfp_4baseEcldtfp0_4baseEERKNS_11__wrap_iterIT_EERKNS7_IT0_EE = comdat any

$_ZNKSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE6cbeginEv = comdat any

$_ZNSt3__24moveIPN5draco4Mesh13AttributeDataES4_EET0_T_S6_S5_ = comdat any

$_ZNSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE11__make_iterEPS3_ = comdat any

$_ZNKSt3__211__wrap_iterIPKN5draco4Mesh13AttributeDataEE4baseEv = comdat any

$_ZNKSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE5beginEv = comdat any

$_ZNKSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE11__make_iterEPKS3_ = comdat any

$_ZNSt3__211__wrap_iterIPKN5draco4Mesh13AttributeDataEEC2ES5_ = comdat any

$_ZNSt3__26__moveIN5draco4Mesh13AttributeDataES3_EENS_9enable_ifIXaasr7is_sameINS_12remove_constIT_E4typeET0_EE5valuesr28is_trivially_copy_assignableIS9_EE5valueEPS9_E4typeEPS6_SD_SA_ = comdat any

$_ZNSt3__213__unwrap_iterIPN5draco4Mesh13AttributeDataEEET_S5_ = comdat any

$_ZNSt3__211__wrap_iterIPN5draco4Mesh13AttributeDataEEC2ES4_ = comdat any

$_ZNSt3__211__wrap_iterIPN5draco4Mesh13AttributeDataEEpLEl = comdat any

$_ZNKSt3__211__wrap_iterIPN5draco4Mesh13AttributeDataEE4baseEv = comdat any

$_ZTVN5draco4MeshE = comdat any

@_ZTVN5draco4MeshE = linkonce_odr hidden unnamed_addr constant { [6 x i8*] } { [6 x i8*] [i8* null, i8* null, i8* bitcast (%"class.draco::Mesh"* (%"class.draco::Mesh"*)* @_ZN5draco4MeshD2Ev to i8*), i8* bitcast (void (%"class.draco::Mesh"*)* @_ZN5draco4MeshD0Ev to i8*), i8* bitcast (void (%"class.draco::Mesh"*, i32, %"class.std::__2::unique_ptr.53"*)* @_ZN5draco4Mesh12SetAttributeEiNSt3__210unique_ptrINS_14PointAttributeENS1_14default_deleteIS3_EEEE to i8*), i8* bitcast (void (%"class.draco::Mesh"*, i32)* @_ZN5draco4Mesh15DeleteAttributeEi to i8*)] }, comdat, align 4
@_ZTVN5draco10PointCloudE = external unnamed_addr constant { [6 x i8*] }, align 4
@.str = private unnamed_addr constant [68 x i8] c"allocator<T>::allocate(size_t n) 'n' exceeds maximum supported size\00", align 1

@_ZN5draco4MeshC1Ev = hidden unnamed_addr alias %"class.draco::Mesh"* (%"class.draco::Mesh"*), %"class.draco::Mesh"* (%"class.draco::Mesh"*)* @_ZN5draco4MeshC2Ev

; Function Attrs: noinline nounwind optnone
define hidden %"class.draco::Mesh"* @_ZN5draco4MeshC2Ev(%"class.draco::Mesh"* returned %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %"class.draco::Mesh"*, align 4
  store %"class.draco::Mesh"* %this, %"class.draco::Mesh"** %this.addr, align 4
  %this1 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %this.addr, align 4
  %0 = bitcast %"class.draco::Mesh"* %this1 to %"class.draco::PointCloud"*
  %call = call %"class.draco::PointCloud"* @_ZN5draco10PointCloudC2Ev(%"class.draco::PointCloud"* %0)
  %1 = bitcast %"class.draco::Mesh"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [6 x i8*] }, { [6 x i8*] }* @_ZTVN5draco4MeshE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %attribute_data_ = getelementptr inbounds %"class.draco::Mesh", %"class.draco::Mesh"* %this1, i32 0, i32 1
  %call2 = call %"class.std::__2::vector.94"* @_ZNSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEEC2Ev(%"class.std::__2::vector.94"* %attribute_data_) #8
  %faces_ = getelementptr inbounds %"class.draco::Mesh", %"class.draco::Mesh"* %this1, i32 0, i32 2
  %call3 = call %"class.draco::IndexTypeVector.101"* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEENSt3__25arrayINS1_IjNS_20PointIndex_tag_type_EEELm3EEEEC2Ev(%"class.draco::IndexTypeVector.101"* %faces_)
  ret %"class.draco::Mesh"* %this1
}

declare %"class.draco::PointCloud"* @_ZN5draco10PointCloudC2Ev(%"class.draco::PointCloud"* returned) unnamed_addr #1

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector.94"* @_ZNSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEEC2Ev(%"class.std::__2::vector.94"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %call = call %"class.std::__2::__vector_base.95"* @_ZNSt3__213__vector_baseIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEEC2Ev(%"class.std::__2::__vector_base.95"* %0) #8
  ret %"class.std::__2::vector.94"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexTypeVector.101"* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEENSt3__25arrayINS1_IjNS_20PointIndex_tag_type_EEELm3EEEEC2Ev(%"class.draco::IndexTypeVector.101"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector.101"*, align 4
  store %"class.draco::IndexTypeVector.101"* %this, %"class.draco::IndexTypeVector.101"** %this.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector.101"*, %"class.draco::IndexTypeVector.101"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector.101", %"class.draco::IndexTypeVector.101"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::vector.102"* @_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEEC2Ev(%"class.std::__2::vector.102"* %vector_) #8
  ret %"class.draco::IndexTypeVector.101"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::Mesh"* @_ZN5draco4MeshD2Ev(%"class.draco::Mesh"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::Mesh"*, align 4
  store %"class.draco::Mesh"* %this, %"class.draco::Mesh"** %this.addr, align 4
  %this1 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %this.addr, align 4
  %0 = bitcast %"class.draco::Mesh"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [6 x i8*] }, { [6 x i8*] }* @_ZTVN5draco4MeshE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %faces_ = getelementptr inbounds %"class.draco::Mesh", %"class.draco::Mesh"* %this1, i32 0, i32 2
  %call = call %"class.draco::IndexTypeVector.101"* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEENSt3__25arrayINS1_IjNS_20PointIndex_tag_type_EEELm3EEEED2Ev(%"class.draco::IndexTypeVector.101"* %faces_) #8
  %attribute_data_ = getelementptr inbounds %"class.draco::Mesh", %"class.draco::Mesh"* %this1, i32 0, i32 1
  %call2 = call %"class.std::__2::vector.94"* @_ZNSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEED2Ev(%"class.std::__2::vector.94"* %attribute_data_) #8
  %1 = bitcast %"class.draco::Mesh"* %this1 to %"class.draco::PointCloud"*
  %call3 = call %"class.draco::PointCloud"* @_ZN5draco10PointCloudD2Ev(%"class.draco::PointCloud"* %1) #8
  ret %"class.draco::Mesh"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco4MeshD0Ev(%"class.draco::Mesh"* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::Mesh"*, align 4
  store %"class.draco::Mesh"* %this, %"class.draco::Mesh"** %this.addr, align 4
  %this1 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %this.addr, align 4
  %call = call %"class.draco::Mesh"* @_ZN5draco4MeshD2Ev(%"class.draco::Mesh"* %this1) #8
  %0 = bitcast %"class.draco::Mesh"* %this1 to i8*
  call void @_ZdlPv(i8* %0) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco4Mesh12SetAttributeEiNSt3__210unique_ptrINS_14PointAttributeENS1_14default_deleteIS3_EEEE(%"class.draco::Mesh"* %this, i32 %att_id, %"class.std::__2::unique_ptr.53"* %pa) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::Mesh"*, align 4
  %att_id.addr = alloca i32, align 4
  %agg.tmp = alloca %"class.std::__2::unique_ptr.53", align 4
  store %"class.draco::Mesh"* %this, %"class.draco::Mesh"** %this.addr, align 4
  store i32 %att_id, i32* %att_id.addr, align 4
  %this1 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %this.addr, align 4
  %0 = bitcast %"class.draco::Mesh"* %this1 to %"class.draco::PointCloud"*
  %1 = load i32, i32* %att_id.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.53"* @_ZNSt3__24moveIRNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOS9_(%"class.std::__2::unique_ptr.53"* nonnull align 4 dereferenceable(4) %pa) #8
  %call2 = call %"class.std::__2::unique_ptr.53"* @_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEEC2EOS5_(%"class.std::__2::unique_ptr.53"* %agg.tmp, %"class.std::__2::unique_ptr.53"* nonnull align 4 dereferenceable(4) %call) #8
  call void @_ZN5draco10PointCloud12SetAttributeEiNSt3__210unique_ptrINS_14PointAttributeENS1_14default_deleteIS3_EEEE(%"class.draco::PointCloud"* %0, i32 %1, %"class.std::__2::unique_ptr.53"* %agg.tmp)
  %call3 = call %"class.std::__2::unique_ptr.53"* @_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.53"* %agg.tmp) #8
  %attribute_data_ = getelementptr inbounds %"class.draco::Mesh", %"class.draco::Mesh"* %this1, i32 0, i32 1
  %call4 = call i32 @_ZNKSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE4sizeEv(%"class.std::__2::vector.94"* %attribute_data_) #8
  %2 = load i32, i32* %att_id.addr, align 4
  %cmp = icmp sle i32 %call4, %2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %attribute_data_5 = getelementptr inbounds %"class.draco::Mesh", %"class.draco::Mesh"* %this1, i32 0, i32 1
  %3 = load i32, i32* %att_id.addr, align 4
  %add = add nsw i32 %3, 1
  call void @_ZNSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE6resizeEm(%"class.std::__2::vector.94"* %attribute_data_5, i32 %add)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco4Mesh15DeleteAttributeEi(%"class.draco::Mesh"* %this, i32 %att_id) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::Mesh"*, align 4
  %att_id.addr = alloca i32, align 4
  %agg.tmp = alloca %"class.std::__2::__wrap_iter", align 4
  %ref.tmp = alloca %"class.std::__2::__wrap_iter.134", align 4
  %ref.tmp4 = alloca %"class.std::__2::__wrap_iter.134", align 4
  %coerce = alloca %"class.std::__2::__wrap_iter.134", align 4
  store %"class.draco::Mesh"* %this, %"class.draco::Mesh"** %this.addr, align 4
  store i32 %att_id, i32* %att_id.addr, align 4
  %this1 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %this.addr, align 4
  %0 = bitcast %"class.draco::Mesh"* %this1 to %"class.draco::PointCloud"*
  %1 = load i32, i32* %att_id.addr, align 4
  call void @_ZN5draco10PointCloud15DeleteAttributeEi(%"class.draco::PointCloud"* %0, i32 %1)
  %2 = load i32, i32* %att_id.addr, align 4
  %cmp = icmp sge i32 %2, 0
  br i1 %cmp, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %3 = load i32, i32* %att_id.addr, align 4
  %attribute_data_ = getelementptr inbounds %"class.draco::Mesh", %"class.draco::Mesh"* %this1, i32 0, i32 1
  %call = call i32 @_ZNKSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE4sizeEv(%"class.std::__2::vector.94"* %attribute_data_) #8
  %cmp2 = icmp slt i32 %3, %call
  br i1 %cmp2, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %attribute_data_3 = getelementptr inbounds %"class.draco::Mesh", %"class.draco::Mesh"* %this1, i32 0, i32 1
  %attribute_data_5 = getelementptr inbounds %"class.draco::Mesh", %"class.draco::Mesh"* %this1, i32 0, i32 1
  %call6 = call %"struct.draco::Mesh::AttributeData"* @_ZNSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE5beginEv(%"class.std::__2::vector.94"* %attribute_data_5) #8
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter.134", %"class.std::__2::__wrap_iter.134"* %ref.tmp4, i32 0, i32 0
  store %"struct.draco::Mesh::AttributeData"* %call6, %"struct.draco::Mesh::AttributeData"** %coerce.dive, align 4
  %4 = load i32, i32* %att_id.addr, align 4
  %call7 = call %"struct.draco::Mesh::AttributeData"* @_ZNKSt3__211__wrap_iterIPN5draco4Mesh13AttributeDataEEplEl(%"class.std::__2::__wrap_iter.134"* %ref.tmp4, i32 %4) #8
  %coerce.dive8 = getelementptr inbounds %"class.std::__2::__wrap_iter.134", %"class.std::__2::__wrap_iter.134"* %ref.tmp, i32 0, i32 0
  store %"struct.draco::Mesh::AttributeData"* %call7, %"struct.draco::Mesh::AttributeData"** %coerce.dive8, align 4
  %call9 = call %"class.std::__2::__wrap_iter"* @_ZNSt3__211__wrap_iterIPKN5draco4Mesh13AttributeDataEEC2IPS3_EERKNS0_IT_EEPNS_9enable_ifIXsr14is_convertibleIS9_S5_EE5valueEvE4typeE(%"class.std::__2::__wrap_iter"* %agg.tmp, %"class.std::__2::__wrap_iter.134"* nonnull align 4 dereferenceable(4) %ref.tmp, i8* null) #8
  %coerce.dive10 = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %agg.tmp, i32 0, i32 0
  %5 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %coerce.dive10, align 4
  %call11 = call %"struct.draco::Mesh::AttributeData"* @_ZNSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE5eraseENS_11__wrap_iterIPKS3_EE(%"class.std::__2::vector.94"* %attribute_data_3, %"struct.draco::Mesh::AttributeData"* %5)
  %coerce.dive12 = getelementptr inbounds %"class.std::__2::__wrap_iter.134", %"class.std::__2::__wrap_iter.134"* %coerce, i32 0, i32 0
  store %"struct.draco::Mesh::AttributeData"* %call11, %"struct.draco::Mesh::AttributeData"** %coerce.dive12, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base.95"* @_ZNSt3__213__vector_baseIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEEC2Ev(%"class.std::__2::__vector_base.95"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.95"*, align 4
  %ref.tmp = alloca i8*, align 4
  %ref.tmp2 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__vector_base.95"* %this, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.95"*, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__vector_base.95"* %this1 to %"class.std::__2::__vector_base_common"*
  %call = call %"class.std::__2::__vector_base_common"* @_ZNSt3__220__vector_base_commonILb1EEC2Ev(%"class.std::__2::__vector_base_common"* %0)
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %this1, i32 0, i32 0
  store %"struct.draco::Mesh::AttributeData"* null, %"struct.draco::Mesh::AttributeData"** %__begin_, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %this1, i32 0, i32 1
  store %"struct.draco::Mesh::AttributeData"* null, %"struct.draco::Mesh::AttributeData"** %__end_, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %this1, i32 0, i32 2
  store i8* null, i8** %ref.tmp, align 4
  %call3 = call %"class.std::__2::__compressed_pair.96"* @_ZNSt3__217__compressed_pairIPN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEEC2IDnNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.96"* %__end_cap_, i8** nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  ret %"class.std::__2::__vector_base.95"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base_common"* @_ZNSt3__220__vector_base_commonILb1EEC2Ev(%"class.std::__2::__vector_base_common"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base_common"*, align 4
  store %"class.std::__2::__vector_base_common"* %this, %"class.std::__2::__vector_base_common"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base_common"*, %"class.std::__2::__vector_base_common"** %this.addr, align 4
  ret %"class.std::__2::__vector_base_common"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.96"* @_ZNSt3__217__compressed_pairIPN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEEC2IDnNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.96"* returned %this, i8** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.96"*, align 4
  %__t1.addr = alloca i8**, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.96"* %this, %"class.std::__2::__compressed_pair.96"** %this.addr, align 4
  store i8** %__t1, i8*** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.96"*, %"class.std::__2::__compressed_pair.96"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.96"* %this1 to %"struct.std::__2::__compressed_pair_elem.97"*
  %1 = load i8**, i8*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %1) #8
  %call2 = call %"struct.std::__2::__compressed_pair_elem.97"* @_ZNSt3__222__compressed_pair_elemIPN5draco4Mesh13AttributeDataELi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.97"* %0, i8** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.96"* %this1 to %"struct.std::__2::__compressed_pair_elem.98"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #8
  %call4 = call %"struct.std::__2::__compressed_pair_elem.98"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco4Mesh13AttributeDataEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.98"* %2)
  ret %"class.std::__2::__compressed_pair.96"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca i8**, align 4
  store i8** %__t, i8*** %__t.addr, align 4
  %0 = load i8**, i8*** %__t.addr, align 4
  ret i8** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.97"* @_ZNSt3__222__compressed_pair_elemIPN5draco4Mesh13AttributeDataELi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.97"* returned %this, i8** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.97"*, align 4
  %__u.addr = alloca i8**, align 4
  store %"struct.std::__2::__compressed_pair_elem.97"* %this, %"struct.std::__2::__compressed_pair_elem.97"** %this.addr, align 4
  store i8** %__u, i8*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.97"*, %"struct.std::__2::__compressed_pair_elem.97"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.97", %"struct.std::__2::__compressed_pair_elem.97"* %this1, i32 0, i32 0
  %0 = load i8**, i8*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %0) #8
  store %"struct.draco::Mesh::AttributeData"* null, %"struct.draco::Mesh::AttributeData"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.97"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  store %"struct.std::__2::__default_init_tag"* %__t, %"struct.std::__2::__default_init_tag"** %__t.addr, align 4
  %0 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t.addr, align 4
  ret %"struct.std::__2::__default_init_tag"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.98"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco4Mesh13AttributeDataEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.98"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.98"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.98"* %this, %"struct.std::__2::__compressed_pair_elem.98"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.98"*, %"struct.std::__2::__compressed_pair_elem.98"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.98"* %this1 to %"class.std::__2::allocator.99"*
  %call = call %"class.std::__2::allocator.99"* @_ZNSt3__29allocatorIN5draco4Mesh13AttributeDataEEC2Ev(%"class.std::__2::allocator.99"* %1) #8
  ret %"struct.std::__2::__compressed_pair_elem.98"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::allocator.99"* @_ZNSt3__29allocatorIN5draco4Mesh13AttributeDataEEC2Ev(%"class.std::__2::allocator.99"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.99"*, align 4
  store %"class.std::__2::allocator.99"* %this, %"class.std::__2::allocator.99"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.99"*, %"class.std::__2::allocator.99"** %this.addr, align 4
  ret %"class.std::__2::allocator.99"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector.102"* @_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEEC2Ev(%"class.std::__2::vector.102"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.102"*, align 4
  store %"class.std::__2::vector.102"* %this, %"class.std::__2::vector.102"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.102"*, %"class.std::__2::vector.102"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.102"* %this1 to %"class.std::__2::__vector_base.103"*
  %call = call %"class.std::__2::__vector_base.103"* @_ZNSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEEC2Ev(%"class.std::__2::__vector_base.103"* %0) #8
  ret %"class.std::__2::vector.102"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base.103"* @_ZNSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEEC2Ev(%"class.std::__2::__vector_base.103"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.103"*, align 4
  %ref.tmp = alloca i8*, align 4
  %ref.tmp2 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__vector_base.103"* %this, %"class.std::__2::__vector_base.103"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.103"*, %"class.std::__2::__vector_base.103"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__vector_base.103"* %this1 to %"class.std::__2::__vector_base_common"*
  %call = call %"class.std::__2::__vector_base_common"* @_ZNSt3__220__vector_base_commonILb1EEC2Ev(%"class.std::__2::__vector_base_common"* %0)
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.103", %"class.std::__2::__vector_base.103"* %this1, i32 0, i32 0
  store %"struct.std::__2::array"* null, %"struct.std::__2::array"** %__begin_, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.103", %"class.std::__2::__vector_base.103"* %this1, i32 0, i32 1
  store %"struct.std::__2::array"* null, %"struct.std::__2::array"** %__end_, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.103", %"class.std::__2::__vector_base.103"* %this1, i32 0, i32 2
  store i8* null, i8** %ref.tmp, align 4
  %call3 = call %"class.std::__2::__compressed_pair.105"* @_ZNSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEEC2IDnNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.105"* %__end_cap_, i8** nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  ret %"class.std::__2::__vector_base.103"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.105"* @_ZNSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEEC2IDnNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.105"* returned %this, i8** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.105"*, align 4
  %__t1.addr = alloca i8**, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.105"* %this, %"class.std::__2::__compressed_pair.105"** %this.addr, align 4
  store i8** %__t1, i8*** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.105"*, %"class.std::__2::__compressed_pair.105"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.105"* %this1 to %"struct.std::__2::__compressed_pair_elem.106"*
  %1 = load i8**, i8*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %1) #8
  %call2 = call %"struct.std::__2::__compressed_pair_elem.106"* @_ZNSt3__222__compressed_pair_elemIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEELi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.106"* %0, i8** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.105"* %this1 to %"struct.std::__2::__compressed_pair_elem.107"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #8
  %call4 = call %"struct.std::__2::__compressed_pair_elem.107"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.107"* %2)
  ret %"class.std::__2::__compressed_pair.105"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.106"* @_ZNSt3__222__compressed_pair_elemIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEELi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.106"* returned %this, i8** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.106"*, align 4
  %__u.addr = alloca i8**, align 4
  store %"struct.std::__2::__compressed_pair_elem.106"* %this, %"struct.std::__2::__compressed_pair_elem.106"** %this.addr, align 4
  store i8** %__u, i8*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.106"*, %"struct.std::__2::__compressed_pair_elem.106"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.106", %"struct.std::__2::__compressed_pair_elem.106"* %this1, i32 0, i32 0
  %0 = load i8**, i8*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %0) #8
  store %"struct.std::__2::array"* null, %"struct.std::__2::array"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.106"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.107"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.107"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.107"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.107"* %this, %"struct.std::__2::__compressed_pair_elem.107"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.107"*, %"struct.std::__2::__compressed_pair_elem.107"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.107"* %this1 to %"class.std::__2::allocator.108"*
  %call = call %"class.std::__2::allocator.108"* @_ZNSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEEC2Ev(%"class.std::__2::allocator.108"* %1) #8
  ret %"struct.std::__2::__compressed_pair_elem.107"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::allocator.108"* @_ZNSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEEC2Ev(%"class.std::__2::allocator.108"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.108"*, align 4
  store %"class.std::__2::allocator.108"* %this, %"class.std::__2::allocator.108"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.108"*, %"class.std::__2::allocator.108"** %this.addr, align 4
  ret %"class.std::__2::allocator.108"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexTypeVector.101"* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEENSt3__25arrayINS1_IjNS_20PointIndex_tag_type_EEELm3EEEED2Ev(%"class.draco::IndexTypeVector.101"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector.101"*, align 4
  store %"class.draco::IndexTypeVector.101"* %this, %"class.draco::IndexTypeVector.101"** %this.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector.101"*, %"class.draco::IndexTypeVector.101"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector.101", %"class.draco::IndexTypeVector.101"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::vector.102"* @_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEED2Ev(%"class.std::__2::vector.102"* %vector_) #8
  ret %"class.draco::IndexTypeVector.101"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector.94"* @_ZNSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEED2Ev(%"class.std::__2::vector.94"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  call void @_ZNKSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE17__annotate_deleteEv(%"class.std::__2::vector.94"* %this1) #8
  %0 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %call = call %"class.std::__2::__vector_base.95"* @_ZNSt3__213__vector_baseIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEED2Ev(%"class.std::__2::__vector_base.95"* %0) #8
  ret %"class.std::__2::vector.94"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::PointCloud"* @_ZN5draco10PointCloudD2Ev(%"class.draco::PointCloud"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"class.draco::PointCloud"*, align 4
  %this.addr = alloca %"class.draco::PointCloud"*, align 4
  store %"class.draco::PointCloud"* %this, %"class.draco::PointCloud"** %this.addr, align 4
  %this1 = load %"class.draco::PointCloud"*, %"class.draco::PointCloud"** %this.addr, align 4
  store %"class.draco::PointCloud"* %this1, %"class.draco::PointCloud"** %retval, align 4
  %0 = bitcast %"class.draco::PointCloud"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [6 x i8*] }, { [6 x i8*] }* @_ZTVN5draco10PointCloudE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %named_attribute_index_ = getelementptr inbounds %"class.draco::PointCloud", %"class.draco::PointCloud"* %this1, i32 0, i32 3
  %array.begin = getelementptr inbounds [5 x %"class.std::__2::vector.87"], [5 x %"class.std::__2::vector.87"]* %named_attribute_index_, i32 0, i32 0
  %1 = getelementptr inbounds %"class.std::__2::vector.87", %"class.std::__2::vector.87"* %array.begin, i32 5
  br label %arraydestroy.body

arraydestroy.body:                                ; preds = %arraydestroy.body, %entry
  %arraydestroy.elementPast = phi %"class.std::__2::vector.87"* [ %1, %entry ], [ %arraydestroy.element, %arraydestroy.body ]
  %arraydestroy.element = getelementptr inbounds %"class.std::__2::vector.87", %"class.std::__2::vector.87"* %arraydestroy.elementPast, i32 -1
  %call = call %"class.std::__2::vector.87"* @_ZNSt3__26vectorIiNS_9allocatorIiEEED2Ev(%"class.std::__2::vector.87"* %arraydestroy.element) #8
  %arraydestroy.done = icmp eq %"class.std::__2::vector.87"* %arraydestroy.element, %array.begin
  br i1 %arraydestroy.done, label %arraydestroy.done2, label %arraydestroy.body

arraydestroy.done2:                               ; preds = %arraydestroy.body
  %attributes_ = getelementptr inbounds %"class.draco::PointCloud", %"class.draco::PointCloud"* %this1, i32 0, i32 2
  %call3 = call %"class.std::__2::vector.51"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEED2Ev(%"class.std::__2::vector.51"* %attributes_) #8
  %metadata_ = getelementptr inbounds %"class.draco::PointCloud", %"class.draco::PointCloud"* %this1, i32 0, i32 1
  %call4 = call %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrIN5draco16GeometryMetadataENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr"* %metadata_) #8
  %2 = load %"class.draco::PointCloud"*, %"class.draco::PointCloud"** %retval, align 4
  ret %"class.draco::PointCloud"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector.102"* @_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEED2Ev(%"class.std::__2::vector.102"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.102"*, align 4
  store %"class.std::__2::vector.102"* %this, %"class.std::__2::vector.102"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.102"*, %"class.std::__2::vector.102"** %this.addr, align 4
  call void @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE17__annotate_deleteEv(%"class.std::__2::vector.102"* %this1) #8
  %0 = bitcast %"class.std::__2::vector.102"* %this1 to %"class.std::__2::__vector_base.103"*
  %call = call %"class.std::__2::__vector_base.103"* @_ZNSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEED2Ev(%"class.std::__2::__vector_base.103"* %0) #8
  ret %"class.std::__2::vector.102"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE17__annotate_deleteEv(%"class.std::__2::vector.102"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.102"*, align 4
  store %"class.std::__2::vector.102"* %this, %"class.std::__2::vector.102"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.102"*, %"class.std::__2::vector.102"** %this.addr, align 4
  %call = call %"struct.std::__2::array"* @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.102"* %this1) #8
  %0 = bitcast %"struct.std::__2::array"* %call to i8*
  %call2 = call %"struct.std::__2::array"* @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.102"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector.102"* %this1) #8
  %add.ptr = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %call2, i32 %call3
  %1 = bitcast %"struct.std::__2::array"* %add.ptr to i8*
  %call4 = call %"struct.std::__2::array"* @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.102"* %this1) #8
  %call5 = call i32 @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.102"* %this1) #8
  %add.ptr6 = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %call4, i32 %call5
  %2 = bitcast %"struct.std::__2::array"* %add.ptr6 to i8*
  %call7 = call %"struct.std::__2::array"* @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.102"* %this1) #8
  %call8 = call i32 @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector.102"* %this1) #8
  %add.ptr9 = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %call7, i32 %call8
  %3 = bitcast %"struct.std::__2::array"* %add.ptr9 to i8*
  call void @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE31__annotate_contiguous_containerEPKvSB_SB_SB_(%"class.std::__2::vector.102"* %this1, i8* %0, i8* %1, i8* %2, i8* %3) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base.103"* @_ZNSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEED2Ev(%"class.std::__2::__vector_base.103"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__vector_base.103"*, align 4
  %this.addr = alloca %"class.std::__2::__vector_base.103"*, align 4
  store %"class.std::__2::__vector_base.103"* %this, %"class.std::__2::__vector_base.103"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.103"*, %"class.std::__2::__vector_base.103"** %this.addr, align 4
  store %"class.std::__2::__vector_base.103"* %this1, %"class.std::__2::__vector_base.103"** %retval, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.103", %"class.std::__2::__vector_base.103"* %this1, i32 0, i32 0
  %0 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__begin_, align 4
  %cmp = icmp ne %"struct.std::__2::array"* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE5clearEv(%"class.std::__2::__vector_base.103"* %this1) #8
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.108"* @_ZNSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base.103"* %this1) #8
  %__begin_2 = getelementptr inbounds %"class.std::__2::__vector_base.103", %"class.std::__2::__vector_base.103"* %this1, i32 0, i32 0
  %1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__begin_2, align 4
  %call3 = call i32 @_ZNKSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::__vector_base.103"* %this1) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE10deallocateERS8_PS7_m(%"class.std::__2::allocator.108"* nonnull align 1 dereferenceable(1) %call, %"struct.std::__2::array"* %1, i32 %call3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"class.std::__2::__vector_base.103"*, %"class.std::__2::__vector_base.103"** %retval, align 4
  ret %"class.std::__2::__vector_base.103"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE31__annotate_contiguous_containerEPKvSB_SB_SB_(%"class.std::__2::vector.102"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.102"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector.102"* %this, %"class.std::__2::vector.102"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector.102"*, %"class.std::__2::vector.102"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::array"* @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.102"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.102"*, align 4
  store %"class.std::__2::vector.102"* %this, %"class.std::__2::vector.102"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.102"*, %"class.std::__2::vector.102"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.102"* %this1 to %"class.std::__2::__vector_base.103"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.103", %"class.std::__2::__vector_base.103"* %0, i32 0, i32 0
  %1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__begin_, align 4
  %call = call %"struct.std::__2::array"* @_ZNSt3__212__to_addressINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEEEPT_S8_(%"struct.std::__2::array"* %1) #8
  ret %"struct.std::__2::array"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector.102"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.102"*, align 4
  store %"class.std::__2::vector.102"* %this, %"class.std::__2::vector.102"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.102"*, %"class.std::__2::vector.102"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.102"* %this1 to %"class.std::__2::__vector_base.103"*
  %call = call i32 @_ZNKSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::__vector_base.103"* %0) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.102"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.102"*, align 4
  store %"class.std::__2::vector.102"* %this, %"class.std::__2::vector.102"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.102"*, %"class.std::__2::vector.102"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.102"* %this1 to %"class.std::__2::__vector_base.103"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.103", %"class.std::__2::__vector_base.103"* %0, i32 0, i32 1
  %1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector.102"* %this1 to %"class.std::__2::__vector_base.103"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.103", %"class.std::__2::__vector_base.103"* %2, i32 0, i32 0
  %3 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"struct.std::__2::array"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"struct.std::__2::array"* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 12
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::array"* @_ZNSt3__212__to_addressINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEEEPT_S8_(%"struct.std::__2::array"* %__p) #0 comdat {
entry:
  %__p.addr = alloca %"struct.std::__2::array"*, align 4
  store %"struct.std::__2::array"* %__p, %"struct.std::__2::array"** %__p.addr, align 4
  %0 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__p.addr, align 4
  ret %"struct.std::__2::array"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::__vector_base.103"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.103"*, align 4
  store %"class.std::__2::__vector_base.103"* %this, %"class.std::__2::__vector_base.103"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.103"*, %"class.std::__2::__vector_base.103"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNKSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE9__end_capEv(%"class.std::__2::__vector_base.103"* %this1) #8
  %0 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.103", %"class.std::__2::__vector_base.103"* %this1, i32 0, i32 0
  %1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"struct.std::__2::array"* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint %"struct.std::__2::array"* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 12
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNKSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE9__end_capEv(%"class.std::__2::__vector_base.103"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.103"*, align 4
  store %"class.std::__2::__vector_base.103"* %this, %"class.std::__2::__vector_base.103"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.103"*, %"class.std::__2::__vector_base.103"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.103", %"class.std::__2::__vector_base.103"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNKSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.105"* %__end_cap_) #8
  ret %"struct.std::__2::array"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNKSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.105"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.105"*, align 4
  store %"class.std::__2::__compressed_pair.105"* %this, %"class.std::__2::__compressed_pair.105"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.105"*, %"class.std::__2::__compressed_pair.105"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.105"* %this1 to %"struct.std::__2::__compressed_pair_elem.106"*
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNKSt3__222__compressed_pair_elemIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.106"* %0) #8
  ret %"struct.std::__2::array"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNKSt3__222__compressed_pair_elemIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.106"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.106"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.106"* %this, %"struct.std::__2::__compressed_pair_elem.106"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.106"*, %"struct.std::__2::__compressed_pair_elem.106"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.106", %"struct.std::__2::__compressed_pair_elem.106"* %this1, i32 0, i32 0
  ret %"struct.std::__2::array"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE5clearEv(%"class.std::__2::__vector_base.103"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.103"*, align 4
  store %"class.std::__2::__vector_base.103"* %this, %"class.std::__2::__vector_base.103"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.103"*, %"class.std::__2::__vector_base.103"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.103", %"class.std::__2::__vector_base.103"* %this1, i32 0, i32 0
  %0 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__begin_, align 4
  call void @_ZNSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE17__destruct_at_endEPS6_(%"class.std::__2::__vector_base.103"* %this1, %"struct.std::__2::array"* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE10deallocateERS8_PS7_m(%"class.std::__2::allocator.108"* nonnull align 1 dereferenceable(1) %__a, %"struct.std::__2::array"* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.108"*, align 4
  %__p.addr = alloca %"struct.std::__2::array"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.108"* %__a, %"class.std::__2::allocator.108"** %__a.addr, align 4
  store %"struct.std::__2::array"* %__p, %"struct.std::__2::array"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.108"*, %"class.std::__2::allocator.108"** %__a.addr, align 4
  %1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEE10deallocateEPS6_m(%"class.std::__2::allocator.108"* %0, %"struct.std::__2::array"* %1, i32 %2) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.108"* @_ZNSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base.103"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.103"*, align 4
  store %"class.std::__2::__vector_base.103"* %this, %"class.std::__2::__vector_base.103"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.103"*, %"class.std::__2::__vector_base.103"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.103", %"class.std::__2::__vector_base.103"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.108"* @_ZNSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE6secondEv(%"class.std::__2::__compressed_pair.105"* %__end_cap_) #8
  ret %"class.std::__2::allocator.108"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE17__destruct_at_endEPS6_(%"class.std::__2::__vector_base.103"* %this, %"struct.std::__2::array"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.103"*, align 4
  %__new_last.addr = alloca %"struct.std::__2::array"*, align 4
  %__soon_to_be_end = alloca %"struct.std::__2::array"*, align 4
  store %"class.std::__2::__vector_base.103"* %this, %"class.std::__2::__vector_base.103"** %this.addr, align 4
  store %"struct.std::__2::array"* %__new_last, %"struct.std::__2::array"** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.103"*, %"class.std::__2::__vector_base.103"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.103", %"class.std::__2::__vector_base.103"* %this1, i32 0, i32 1
  %0 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__end_, align 4
  store %"struct.std::__2::array"* %0, %"struct.std::__2::array"** %__soon_to_be_end, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__new_last.addr, align 4
  %2 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__soon_to_be_end, align 4
  %cmp = icmp ne %"struct.std::__2::array"* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.108"* @_ZNSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base.103"* %this1) #8
  %3 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__soon_to_be_end, align 4
  %incdec.ptr = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %3, i32 -1
  store %"struct.std::__2::array"* %incdec.ptr, %"struct.std::__2::array"** %__soon_to_be_end, align 4
  %call2 = call %"struct.std::__2::array"* @_ZNSt3__212__to_addressINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEEEPT_S8_(%"struct.std::__2::array"* %incdec.ptr) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE7destroyIS7_EEvRS8_PT_(%"class.std::__2::allocator.108"* nonnull align 1 dereferenceable(1) %call, %"struct.std::__2::array"* %call2)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %4 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__new_last.addr, align 4
  %__end_3 = getelementptr inbounds %"class.std::__2::__vector_base.103", %"class.std::__2::__vector_base.103"* %this1, i32 0, i32 1
  store %"struct.std::__2::array"* %4, %"struct.std::__2::array"** %__end_3, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE7destroyIS7_EEvRS8_PT_(%"class.std::__2::allocator.108"* nonnull align 1 dereferenceable(1) %__a, %"struct.std::__2::array"* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.108"*, align 4
  %__p.addr = alloca %"struct.std::__2::array"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy", align 1
  store %"class.std::__2::allocator.108"* %__a, %"class.std::__2::allocator.108"** %__a.addr, align 4
  store %"struct.std::__2::array"* %__p, %"struct.std::__2::array"** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.108"*, %"class.std::__2::allocator.108"** %__a.addr, align 4
  %2 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE9__destroyIS7_EEvNS_17integral_constantIbLb1EEERS8_PT_(%"class.std::__2::allocator.108"* nonnull align 1 dereferenceable(1) %1, %"struct.std::__2::array"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE9__destroyIS7_EEvNS_17integral_constantIbLb1EEERS8_PT_(%"class.std::__2::allocator.108"* nonnull align 1 dereferenceable(1) %__a, %"struct.std::__2::array"* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.108"*, align 4
  %__p.addr = alloca %"struct.std::__2::array"*, align 4
  store %"class.std::__2::allocator.108"* %__a, %"class.std::__2::allocator.108"** %__a.addr, align 4
  store %"struct.std::__2::array"* %__p, %"struct.std::__2::array"** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.108"*, %"class.std::__2::allocator.108"** %__a.addr, align 4
  %2 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__p.addr, align 4
  call void @_ZNSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEE7destroyEPS6_(%"class.std::__2::allocator.108"* %1, %"struct.std::__2::array"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEE7destroyEPS6_(%"class.std::__2::allocator.108"* %this, %"struct.std::__2::array"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.108"*, align 4
  %__p.addr = alloca %"struct.std::__2::array"*, align 4
  store %"class.std::__2::allocator.108"* %this, %"class.std::__2::allocator.108"** %this.addr, align 4
  store %"struct.std::__2::array"* %__p, %"struct.std::__2::array"** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.108"*, %"class.std::__2::allocator.108"** %this.addr, align 4
  %0 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__p.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEE10deallocateEPS6_m(%"class.std::__2::allocator.108"* %this, %"struct.std::__2::array"* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.108"*, align 4
  %__p.addr = alloca %"struct.std::__2::array"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.108"* %this, %"class.std::__2::allocator.108"** %this.addr, align 4
  store %"struct.std::__2::array"* %__p, %"struct.std::__2::array"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.108"*, %"class.std::__2::allocator.108"** %this.addr, align 4
  %0 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__p.addr, align 4
  %1 = bitcast %"struct.std::__2::array"* %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 12
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %__ptr, i32 %__size, i32 %__align) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  %1 = load i32, i32* %__size.addr, align 4
  %2 = load i32, i32* %__align.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm(i8* %0, i32 %1, i32 %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm(i8* %__ptr, i32 %__size, i32 %__align) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  %1 = load i32, i32* %__size.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm(i8* %0, i32 %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm(i8* %__ptr, i32 %__size) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller9__do_callEPv(i8* %0)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller9__do_callEPv(i8* %__ptr) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  call void @_ZdlPv(i8* %0) #9
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #2

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.108"* @_ZNSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE6secondEv(%"class.std::__2::__compressed_pair.105"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.105"*, align 4
  store %"class.std::__2::__compressed_pair.105"* %this, %"class.std::__2::__compressed_pair.105"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.105"*, %"class.std::__2::__compressed_pair.105"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.105"* %this1 to %"struct.std::__2::__compressed_pair_elem.107"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.108"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.107"* %0) #8
  ret %"class.std::__2::allocator.108"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.108"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.107"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.107"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.107"* %this, %"struct.std::__2::__compressed_pair_elem.107"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.107"*, %"struct.std::__2::__compressed_pair_elem.107"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.107"* %this1 to %"class.std::__2::allocator.108"*
  ret %"class.std::__2::allocator.108"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE17__annotate_deleteEv(%"class.std::__2::vector.94"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  %call = call %"struct.draco::Mesh::AttributeData"* @_ZNKSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE4dataEv(%"class.std::__2::vector.94"* %this1) #8
  %0 = bitcast %"struct.draco::Mesh::AttributeData"* %call to i8*
  %call2 = call %"struct.draco::Mesh::AttributeData"* @_ZNKSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE4dataEv(%"class.std::__2::vector.94"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE8capacityEv(%"class.std::__2::vector.94"* %this1) #8
  %add.ptr = getelementptr inbounds %"struct.draco::Mesh::AttributeData", %"struct.draco::Mesh::AttributeData"* %call2, i32 %call3
  %1 = bitcast %"struct.draco::Mesh::AttributeData"* %add.ptr to i8*
  %call4 = call %"struct.draco::Mesh::AttributeData"* @_ZNKSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE4dataEv(%"class.std::__2::vector.94"* %this1) #8
  %call5 = call i32 @_ZNKSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE4sizeEv(%"class.std::__2::vector.94"* %this1) #8
  %add.ptr6 = getelementptr inbounds %"struct.draco::Mesh::AttributeData", %"struct.draco::Mesh::AttributeData"* %call4, i32 %call5
  %2 = bitcast %"struct.draco::Mesh::AttributeData"* %add.ptr6 to i8*
  %call7 = call %"struct.draco::Mesh::AttributeData"* @_ZNKSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE4dataEv(%"class.std::__2::vector.94"* %this1) #8
  %call8 = call i32 @_ZNKSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE8capacityEv(%"class.std::__2::vector.94"* %this1) #8
  %add.ptr9 = getelementptr inbounds %"struct.draco::Mesh::AttributeData", %"struct.draco::Mesh::AttributeData"* %call7, i32 %call8
  %3 = bitcast %"struct.draco::Mesh::AttributeData"* %add.ptr9 to i8*
  call void @_ZNKSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE31__annotate_contiguous_containerEPKvS8_S8_S8_(%"class.std::__2::vector.94"* %this1, i8* %0, i8* %1, i8* %2, i8* %3) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base.95"* @_ZNSt3__213__vector_baseIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEED2Ev(%"class.std::__2::__vector_base.95"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__vector_base.95"*, align 4
  %this.addr = alloca %"class.std::__2::__vector_base.95"*, align 4
  store %"class.std::__2::__vector_base.95"* %this, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.95"*, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  store %"class.std::__2::__vector_base.95"* %this1, %"class.std::__2::__vector_base.95"** %retval, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %this1, i32 0, i32 0
  %0 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__begin_, align 4
  %cmp = icmp ne %"struct.draco::Mesh::AttributeData"* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__213__vector_baseIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE5clearEv(%"class.std::__2::__vector_base.95"* %this1) #8
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.99"* @_ZNSt3__213__vector_baseIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE7__allocEv(%"class.std::__2::__vector_base.95"* %this1) #8
  %__begin_2 = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %this1, i32 0, i32 0
  %1 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__begin_2, align 4
  %call3 = call i32 @_ZNKSt3__213__vector_baseIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE8capacityEv(%"class.std::__2::__vector_base.95"* %this1) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco4Mesh13AttributeDataEEEE10deallocateERS5_PS4_m(%"class.std::__2::allocator.99"* nonnull align 1 dereferenceable(1) %call, %"struct.draco::Mesh::AttributeData"* %1, i32 %call3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"class.std::__2::__vector_base.95"*, %"class.std::__2::__vector_base.95"** %retval, align 4
  ret %"class.std::__2::__vector_base.95"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE31__annotate_contiguous_containerEPKvS8_S8_S8_(%"class.std::__2::vector.94"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.draco::Mesh::AttributeData"* @_ZNKSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE4dataEv(%"class.std::__2::vector.94"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %0, i32 0, i32 0
  %1 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__begin_, align 4
  %call = call %"struct.draco::Mesh::AttributeData"* @_ZNSt3__212__to_addressIN5draco4Mesh13AttributeDataEEEPT_S5_(%"struct.draco::Mesh::AttributeData"* %1) #8
  ret %"struct.draco::Mesh::AttributeData"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE8capacityEv(%"class.std::__2::vector.94"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %call = call i32 @_ZNKSt3__213__vector_baseIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE8capacityEv(%"class.std::__2::__vector_base.95"* %0) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE4sizeEv(%"class.std::__2::vector.94"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %0, i32 0, i32 1
  %1 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %2, i32 0, i32 0
  %3 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"struct.draco::Mesh::AttributeData"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"struct.draco::Mesh::AttributeData"* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.draco::Mesh::AttributeData"* @_ZNSt3__212__to_addressIN5draco4Mesh13AttributeDataEEEPT_S5_(%"struct.draco::Mesh::AttributeData"* %__p) #0 comdat {
entry:
  %__p.addr = alloca %"struct.draco::Mesh::AttributeData"*, align 4
  store %"struct.draco::Mesh::AttributeData"* %__p, %"struct.draco::Mesh::AttributeData"** %__p.addr, align 4
  %0 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__p.addr, align 4
  ret %"struct.draco::Mesh::AttributeData"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE8capacityEv(%"class.std::__2::__vector_base.95"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.95"*, align 4
  store %"class.std::__2::__vector_base.95"* %this, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.95"*, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"struct.draco::Mesh::AttributeData"** @_ZNKSt3__213__vector_baseIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE9__end_capEv(%"class.std::__2::__vector_base.95"* %this1) #8
  %0 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %this1, i32 0, i32 0
  %1 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"struct.draco::Mesh::AttributeData"* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint %"struct.draco::Mesh::AttributeData"* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.draco::Mesh::AttributeData"** @_ZNKSt3__213__vector_baseIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE9__end_capEv(%"class.std::__2::__vector_base.95"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.95"*, align 4
  store %"class.std::__2::__vector_base.95"* %this, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.95"*, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) %"struct.draco::Mesh::AttributeData"** @_ZNKSt3__217__compressed_pairIPN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE5firstEv(%"class.std::__2::__compressed_pair.96"* %__end_cap_) #8
  ret %"struct.draco::Mesh::AttributeData"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.draco::Mesh::AttributeData"** @_ZNKSt3__217__compressed_pairIPN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE5firstEv(%"class.std::__2::__compressed_pair.96"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.96"*, align 4
  store %"class.std::__2::__compressed_pair.96"* %this, %"class.std::__2::__compressed_pair.96"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.96"*, %"class.std::__2::__compressed_pair.96"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.96"* %this1 to %"struct.std::__2::__compressed_pair_elem.97"*
  %call = call nonnull align 4 dereferenceable(4) %"struct.draco::Mesh::AttributeData"** @_ZNKSt3__222__compressed_pair_elemIPN5draco4Mesh13AttributeDataELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.97"* %0) #8
  ret %"struct.draco::Mesh::AttributeData"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.draco::Mesh::AttributeData"** @_ZNKSt3__222__compressed_pair_elemIPN5draco4Mesh13AttributeDataELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.97"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.97"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.97"* %this, %"struct.std::__2::__compressed_pair_elem.97"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.97"*, %"struct.std::__2::__compressed_pair_elem.97"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.97", %"struct.std::__2::__compressed_pair_elem.97"* %this1, i32 0, i32 0
  ret %"struct.draco::Mesh::AttributeData"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE5clearEv(%"class.std::__2::__vector_base.95"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.95"*, align 4
  store %"class.std::__2::__vector_base.95"* %this, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.95"*, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %this1, i32 0, i32 0
  %0 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__begin_, align 4
  call void @_ZNSt3__213__vector_baseIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE17__destruct_at_endEPS3_(%"class.std::__2::__vector_base.95"* %this1, %"struct.draco::Mesh::AttributeData"* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco4Mesh13AttributeDataEEEE10deallocateERS5_PS4_m(%"class.std::__2::allocator.99"* nonnull align 1 dereferenceable(1) %__a, %"struct.draco::Mesh::AttributeData"* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.99"*, align 4
  %__p.addr = alloca %"struct.draco::Mesh::AttributeData"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.99"* %__a, %"class.std::__2::allocator.99"** %__a.addr, align 4
  store %"struct.draco::Mesh::AttributeData"* %__p, %"struct.draco::Mesh::AttributeData"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.99"*, %"class.std::__2::allocator.99"** %__a.addr, align 4
  %1 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorIN5draco4Mesh13AttributeDataEE10deallocateEPS3_m(%"class.std::__2::allocator.99"* %0, %"struct.draco::Mesh::AttributeData"* %1, i32 %2) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.99"* @_ZNSt3__213__vector_baseIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE7__allocEv(%"class.std::__2::__vector_base.95"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.95"*, align 4
  store %"class.std::__2::__vector_base.95"* %this, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.95"*, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.99"* @_ZNSt3__217__compressed_pairIPN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE6secondEv(%"class.std::__2::__compressed_pair.96"* %__end_cap_) #8
  ret %"class.std::__2::allocator.99"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE17__destruct_at_endEPS3_(%"class.std::__2::__vector_base.95"* %this, %"struct.draco::Mesh::AttributeData"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.95"*, align 4
  %__new_last.addr = alloca %"struct.draco::Mesh::AttributeData"*, align 4
  %__soon_to_be_end = alloca %"struct.draco::Mesh::AttributeData"*, align 4
  store %"class.std::__2::__vector_base.95"* %this, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  store %"struct.draco::Mesh::AttributeData"* %__new_last, %"struct.draco::Mesh::AttributeData"** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.95"*, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %this1, i32 0, i32 1
  %0 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__end_, align 4
  store %"struct.draco::Mesh::AttributeData"* %0, %"struct.draco::Mesh::AttributeData"** %__soon_to_be_end, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__new_last.addr, align 4
  %2 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__soon_to_be_end, align 4
  %cmp = icmp ne %"struct.draco::Mesh::AttributeData"* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.99"* @_ZNSt3__213__vector_baseIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE7__allocEv(%"class.std::__2::__vector_base.95"* %this1) #8
  %3 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__soon_to_be_end, align 4
  %incdec.ptr = getelementptr inbounds %"struct.draco::Mesh::AttributeData", %"struct.draco::Mesh::AttributeData"* %3, i32 -1
  store %"struct.draco::Mesh::AttributeData"* %incdec.ptr, %"struct.draco::Mesh::AttributeData"** %__soon_to_be_end, align 4
  %call2 = call %"struct.draco::Mesh::AttributeData"* @_ZNSt3__212__to_addressIN5draco4Mesh13AttributeDataEEEPT_S5_(%"struct.draco::Mesh::AttributeData"* %incdec.ptr) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco4Mesh13AttributeDataEEEE7destroyIS4_EEvRS5_PT_(%"class.std::__2::allocator.99"* nonnull align 1 dereferenceable(1) %call, %"struct.draco::Mesh::AttributeData"* %call2)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %4 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__new_last.addr, align 4
  %__end_3 = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %this1, i32 0, i32 1
  store %"struct.draco::Mesh::AttributeData"* %4, %"struct.draco::Mesh::AttributeData"** %__end_3, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco4Mesh13AttributeDataEEEE7destroyIS4_EEvRS5_PT_(%"class.std::__2::allocator.99"* nonnull align 1 dereferenceable(1) %__a, %"struct.draco::Mesh::AttributeData"* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.99"*, align 4
  %__p.addr = alloca %"struct.draco::Mesh::AttributeData"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy.110", align 1
  store %"class.std::__2::allocator.99"* %__a, %"class.std::__2::allocator.99"** %__a.addr, align 4
  store %"struct.draco::Mesh::AttributeData"* %__p, %"struct.draco::Mesh::AttributeData"** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy.110"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.99"*, %"class.std::__2::allocator.99"** %__a.addr, align 4
  %2 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco4Mesh13AttributeDataEEEE9__destroyIS4_EEvNS_17integral_constantIbLb1EEERS5_PT_(%"class.std::__2::allocator.99"* nonnull align 1 dereferenceable(1) %1, %"struct.draco::Mesh::AttributeData"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco4Mesh13AttributeDataEEEE9__destroyIS4_EEvNS_17integral_constantIbLb1EEERS5_PT_(%"class.std::__2::allocator.99"* nonnull align 1 dereferenceable(1) %__a, %"struct.draco::Mesh::AttributeData"* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.99"*, align 4
  %__p.addr = alloca %"struct.draco::Mesh::AttributeData"*, align 4
  store %"class.std::__2::allocator.99"* %__a, %"class.std::__2::allocator.99"** %__a.addr, align 4
  store %"struct.draco::Mesh::AttributeData"* %__p, %"struct.draco::Mesh::AttributeData"** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.99"*, %"class.std::__2::allocator.99"** %__a.addr, align 4
  %2 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__p.addr, align 4
  call void @_ZNSt3__29allocatorIN5draco4Mesh13AttributeDataEE7destroyEPS3_(%"class.std::__2::allocator.99"* %1, %"struct.draco::Mesh::AttributeData"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIN5draco4Mesh13AttributeDataEE7destroyEPS3_(%"class.std::__2::allocator.99"* %this, %"struct.draco::Mesh::AttributeData"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.99"*, align 4
  %__p.addr = alloca %"struct.draco::Mesh::AttributeData"*, align 4
  store %"class.std::__2::allocator.99"* %this, %"class.std::__2::allocator.99"** %this.addr, align 4
  store %"struct.draco::Mesh::AttributeData"* %__p, %"struct.draco::Mesh::AttributeData"** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.99"*, %"class.std::__2::allocator.99"** %this.addr, align 4
  %0 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__p.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIN5draco4Mesh13AttributeDataEE10deallocateEPS3_m(%"class.std::__2::allocator.99"* %this, %"struct.draco::Mesh::AttributeData"* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.99"*, align 4
  %__p.addr = alloca %"struct.draco::Mesh::AttributeData"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.99"* %this, %"class.std::__2::allocator.99"** %this.addr, align 4
  store %"struct.draco::Mesh::AttributeData"* %__p, %"struct.draco::Mesh::AttributeData"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.99"*, %"class.std::__2::allocator.99"** %this.addr, align 4
  %0 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__p.addr, align 4
  %1 = bitcast %"struct.draco::Mesh::AttributeData"* %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.99"* @_ZNSt3__217__compressed_pairIPN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE6secondEv(%"class.std::__2::__compressed_pair.96"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.96"*, align 4
  store %"class.std::__2::__compressed_pair.96"* %this, %"class.std::__2::__compressed_pair.96"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.96"*, %"class.std::__2::__compressed_pair.96"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.96"* %this1 to %"struct.std::__2::__compressed_pair_elem.98"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.99"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco4Mesh13AttributeDataEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.98"* %0) #8
  ret %"class.std::__2::allocator.99"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.99"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco4Mesh13AttributeDataEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.98"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.98"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.98"* %this, %"struct.std::__2::__compressed_pair_elem.98"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.98"*, %"struct.std::__2::__compressed_pair_elem.98"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.98"* %this1 to %"class.std::__2::allocator.99"*
  ret %"class.std::__2::allocator.99"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector.87"* @_ZNSt3__26vectorIiNS_9allocatorIiEEED2Ev(%"class.std::__2::vector.87"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  call void @_ZNKSt3__26vectorIiNS_9allocatorIiEEE17__annotate_deleteEv(%"class.std::__2::vector.87"* %this1) #8
  %0 = bitcast %"class.std::__2::vector.87"* %this1 to %"class.std::__2::__vector_base.88"*
  %call = call %"class.std::__2::__vector_base.88"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEED2Ev(%"class.std::__2::__vector_base.88"* %0) #8
  ret %"class.std::__2::vector.87"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector.51"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEED2Ev(%"class.std::__2::vector.51"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.51"*, align 4
  store %"class.std::__2::vector.51"* %this, %"class.std::__2::vector.51"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.51"*, %"class.std::__2::vector.51"** %this.addr, align 4
  call void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__annotate_deleteEv(%"class.std::__2::vector.51"* %this1) #8
  %0 = bitcast %"class.std::__2::vector.51"* %this1 to %"class.std::__2::__vector_base.52"*
  %call = call %"class.std::__2::__vector_base.52"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEED2Ev(%"class.std::__2::__vector_base.52"* %0) #8
  ret %"class.std::__2::vector.51"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrIN5draco16GeometryMetadataENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  call void @_ZNSt3__210unique_ptrIN5draco16GeometryMetadataENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr"* %this1, %"class.draco::GeometryMetadata"* null) #8
  ret %"class.std::__2::unique_ptr"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIiNS_9allocatorIiEEE17__annotate_deleteEv(%"class.std::__2::vector.87"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  %call = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector.87"* %this1) #8
  %0 = bitcast i32* %call to i8*
  %call2 = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector.87"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::vector.87"* %this1) #8
  %add.ptr = getelementptr inbounds i32, i32* %call2, i32 %call3
  %1 = bitcast i32* %add.ptr to i8*
  %call4 = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector.87"* %this1) #8
  %call5 = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4sizeEv(%"class.std::__2::vector.87"* %this1) #8
  %add.ptr6 = getelementptr inbounds i32, i32* %call4, i32 %call5
  %2 = bitcast i32* %add.ptr6 to i8*
  %call7 = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector.87"* %this1) #8
  %call8 = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::vector.87"* %this1) #8
  %add.ptr9 = getelementptr inbounds i32, i32* %call7, i32 %call8
  %3 = bitcast i32* %add.ptr9 to i8*
  call void @_ZNKSt3__26vectorIiNS_9allocatorIiEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector.87"* %this1, i8* %0, i8* %1, i8* %2, i8* %3) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base.88"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEED2Ev(%"class.std::__2::__vector_base.88"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__vector_base.88"*, align 4
  %this.addr = alloca %"class.std::__2::__vector_base.88"*, align 4
  store %"class.std::__2::__vector_base.88"* %this, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.88"*, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  store %"class.std::__2::__vector_base.88"* %this1, %"class.std::__2::__vector_base.88"** %retval, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__begin_, align 4
  %cmp = icmp ne i32* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE5clearEv(%"class.std::__2::__vector_base.88"* %this1) #8
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE7__allocEv(%"class.std::__2::__vector_base.88"* %this1) #8
  %__begin_2 = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %this1, i32 0, i32 0
  %1 = load i32*, i32** %__begin_2, align 4
  %call3 = call i32 @_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::__vector_base.88"* %this1) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE10deallocateERS2_Pim(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %call, i32* %1, i32 %call3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"class.std::__2::__vector_base.88"*, %"class.std::__2::__vector_base.88"** %retval, align 4
  ret %"class.std::__2::__vector_base.88"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIiNS_9allocatorIiEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector.87"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector.87"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.87"* %this1 to %"class.std::__2::__vector_base.88"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %0, i32 0, i32 0
  %1 = load i32*, i32** %__begin_, align 4
  %call = call i32* @_ZNSt3__212__to_addressIiEEPT_S2_(i32* %1) #8
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::vector.87"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.87"* %this1 to %"class.std::__2::__vector_base.88"*
  %call = call i32 @_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::__vector_base.88"* %0) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4sizeEv(%"class.std::__2::vector.87"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.87"* %this1 to %"class.std::__2::__vector_base.88"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %0, i32 0, i32 1
  %1 = load i32*, i32** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector.87"* %this1 to %"class.std::__2::__vector_base.88"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %2, i32 0, i32 0
  %3 = load i32*, i32** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint i32* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint i32* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNSt3__212__to_addressIiEEPT_S2_(i32* %__p) #0 comdat {
entry:
  %__p.addr = alloca i32*, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %0 = load i32*, i32** %__p.addr, align 4
  ret i32* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::__vector_base.88"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.88"*, align 4
  store %"class.std::__2::__vector_base.88"* %this, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.88"*, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE9__end_capEv(%"class.std::__2::__vector_base.88"* %this1) #8
  %0 = load i32*, i32** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %this1, i32 0, i32 0
  %1 = load i32*, i32** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint i32* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint i32* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE9__end_capEv(%"class.std::__2::__vector_base.88"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.88"*, align 4
  store %"class.std::__2::__vector_base.88"* %this, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.88"*, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__217__compressed_pairIPiNS_9allocatorIiEEE5firstEv(%"class.std::__2::__compressed_pair.89"* %__end_cap_) #8
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__217__compressed_pairIPiNS_9allocatorIiEEE5firstEv(%"class.std::__2::__compressed_pair.89"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.89"*, align 4
  store %"class.std::__2::__compressed_pair.89"* %this, %"class.std::__2::__compressed_pair.89"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.89"*, %"class.std::__2::__compressed_pair.89"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.89"* %this1 to %"struct.std::__2::__compressed_pair_elem.90"*
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__222__compressed_pair_elemIPiLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.90"* %0) #8
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__222__compressed_pair_elemIPiLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.90"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.90"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.90"* %this, %"struct.std::__2::__compressed_pair_elem.90"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.90"*, %"struct.std::__2::__compressed_pair_elem.90"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.90", %"struct.std::__2::__compressed_pair_elem.90"* %this1, i32 0, i32 0
  ret i32** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE5clearEv(%"class.std::__2::__vector_base.88"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.88"*, align 4
  store %"class.std::__2::__vector_base.88"* %this, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.88"*, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__begin_, align 4
  call void @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE17__destruct_at_endEPi(%"class.std::__2::__vector_base.88"* %this1, i32* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE10deallocateERS2_Pim(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %__a, i32* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.92"*, align 4
  %__p.addr = alloca i32*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.92"* %__a, %"class.std::__2::allocator.92"** %__a.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.92"*, %"class.std::__2::allocator.92"** %__a.addr, align 4
  %1 = load i32*, i32** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorIiE10deallocateEPim(%"class.std::__2::allocator.92"* %0, i32* %1, i32 %2) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE7__allocEv(%"class.std::__2::__vector_base.88"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.88"*, align 4
  store %"class.std::__2::__vector_base.88"* %this, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.88"*, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__217__compressed_pairIPiNS_9allocatorIiEEE6secondEv(%"class.std::__2::__compressed_pair.89"* %__end_cap_) #8
  ret %"class.std::__2::allocator.92"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE17__destruct_at_endEPi(%"class.std::__2::__vector_base.88"* %this, i32* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.88"*, align 4
  %__new_last.addr = alloca i32*, align 4
  %__soon_to_be_end = alloca i32*, align 4
  store %"class.std::__2::__vector_base.88"* %this, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  store i32* %__new_last, i32** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.88"*, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %this1, i32 0, i32 1
  %0 = load i32*, i32** %__end_, align 4
  store i32* %0, i32** %__soon_to_be_end, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load i32*, i32** %__new_last.addr, align 4
  %2 = load i32*, i32** %__soon_to_be_end, align 4
  %cmp = icmp ne i32* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE7__allocEv(%"class.std::__2::__vector_base.88"* %this1) #8
  %3 = load i32*, i32** %__soon_to_be_end, align 4
  %incdec.ptr = getelementptr inbounds i32, i32* %3, i32 -1
  store i32* %incdec.ptr, i32** %__soon_to_be_end, align 4
  %call2 = call i32* @_ZNSt3__212__to_addressIiEEPT_S2_(i32* %incdec.ptr) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE7destroyIiEEvRS2_PT_(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %call, i32* %call2)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %4 = load i32*, i32** %__new_last.addr, align 4
  %__end_3 = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %this1, i32 0, i32 1
  store i32* %4, i32** %__end_3, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE7destroyIiEEvRS2_PT_(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %__a, i32* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.92"*, align 4
  %__p.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy.111", align 1
  store %"class.std::__2::allocator.92"* %__a, %"class.std::__2::allocator.92"** %__a.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy.111"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.92"*, %"class.std::__2::allocator.92"** %__a.addr, align 4
  %2 = load i32*, i32** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE9__destroyIiEEvNS_17integral_constantIbLb1EEERS2_PT_(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %1, i32* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE9__destroyIiEEvNS_17integral_constantIbLb1EEERS2_PT_(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %__a, i32* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.92"*, align 4
  %__p.addr = alloca i32*, align 4
  store %"class.std::__2::allocator.92"* %__a, %"class.std::__2::allocator.92"** %__a.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.92"*, %"class.std::__2::allocator.92"** %__a.addr, align 4
  %2 = load i32*, i32** %__p.addr, align 4
  call void @_ZNSt3__29allocatorIiE7destroyEPi(%"class.std::__2::allocator.92"* %1, i32* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIiE7destroyEPi(%"class.std::__2::allocator.92"* %this, i32* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.92"*, align 4
  %__p.addr = alloca i32*, align 4
  store %"class.std::__2::allocator.92"* %this, %"class.std::__2::allocator.92"** %this.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.92"*, %"class.std::__2::allocator.92"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIiE10deallocateEPim(%"class.std::__2::allocator.92"* %this, i32* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.92"*, align 4
  %__p.addr = alloca i32*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.92"* %this, %"class.std::__2::allocator.92"** %this.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.92"*, %"class.std::__2::allocator.92"** %this.addr, align 4
  %0 = load i32*, i32** %__p.addr, align 4
  %1 = bitcast i32* %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__217__compressed_pairIPiNS_9allocatorIiEEE6secondEv(%"class.std::__2::__compressed_pair.89"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.89"*, align 4
  store %"class.std::__2::__compressed_pair.89"* %this, %"class.std::__2::__compressed_pair.89"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.89"*, %"class.std::__2::__compressed_pair.89"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.89"* %this1 to %"struct.std::__2::__compressed_pair_elem.91"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIiEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.91"* %0) #8
  ret %"class.std::__2::allocator.92"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIiEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.91"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.91"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.91"* %this, %"struct.std::__2::__compressed_pair_elem.91"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.91"*, %"struct.std::__2::__compressed_pair_elem.91"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.91"* %this1 to %"class.std::__2::allocator.92"*
  ret %"class.std::__2::allocator.92"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__annotate_deleteEv(%"class.std::__2::vector.51"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.51"*, align 4
  store %"class.std::__2::vector.51"* %this, %"class.std::__2::vector.51"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.51"*, %"class.std::__2::vector.51"** %this.addr, align 4
  %call = call %"class.std::__2::unique_ptr.53"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.51"* %this1) #8
  %0 = bitcast %"class.std::__2::unique_ptr.53"* %call to i8*
  %call2 = call %"class.std::__2::unique_ptr.53"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.51"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector.51"* %this1) #8
  %add.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.53", %"class.std::__2::unique_ptr.53"* %call2, i32 %call3
  %1 = bitcast %"class.std::__2::unique_ptr.53"* %add.ptr to i8*
  %call4 = call %"class.std::__2::unique_ptr.53"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.51"* %this1) #8
  %call5 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.51"* %this1) #8
  %add.ptr6 = getelementptr inbounds %"class.std::__2::unique_ptr.53", %"class.std::__2::unique_ptr.53"* %call4, i32 %call5
  %2 = bitcast %"class.std::__2::unique_ptr.53"* %add.ptr6 to i8*
  %call7 = call %"class.std::__2::unique_ptr.53"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.51"* %this1) #8
  %call8 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector.51"* %this1) #8
  %add.ptr9 = getelementptr inbounds %"class.std::__2::unique_ptr.53", %"class.std::__2::unique_ptr.53"* %call7, i32 %call8
  %3 = bitcast %"class.std::__2::unique_ptr.53"* %add.ptr9 to i8*
  call void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE31__annotate_contiguous_containerEPKvSB_SB_SB_(%"class.std::__2::vector.51"* %this1, i8* %0, i8* %1, i8* %2, i8* %3) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base.52"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEED2Ev(%"class.std::__2::__vector_base.52"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__vector_base.52"*, align 4
  %this.addr = alloca %"class.std::__2::__vector_base.52"*, align 4
  store %"class.std::__2::__vector_base.52"* %this, %"class.std::__2::__vector_base.52"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.52"*, %"class.std::__2::__vector_base.52"** %this.addr, align 4
  store %"class.std::__2::__vector_base.52"* %this1, %"class.std::__2::__vector_base.52"** %retval, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.52", %"class.std::__2::__vector_base.52"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__begin_, align 4
  %cmp = icmp ne %"class.std::__2::unique_ptr.53"* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5clearEv(%"class.std::__2::__vector_base.52"* %this1) #8
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.85"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base.52"* %this1) #8
  %__begin_2 = getelementptr inbounds %"class.std::__2::__vector_base.52", %"class.std::__2::__vector_base.52"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__begin_2, align 4
  %call3 = call i32 @_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::__vector_base.52"* %this1) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS4_EEEEEEE10deallocateERS8_PS7_m(%"class.std::__2::allocator.85"* nonnull align 1 dereferenceable(1) %call, %"class.std::__2::unique_ptr.53"* %1, i32 %call3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"class.std::__2::__vector_base.52"*, %"class.std::__2::__vector_base.52"** %retval, align 4
  ret %"class.std::__2::__vector_base.52"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE31__annotate_contiguous_containerEPKvSB_SB_SB_(%"class.std::__2::vector.51"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.51"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector.51"* %this, %"class.std::__2::vector.51"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector.51"*, %"class.std::__2::vector.51"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.53"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.51"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.51"*, align 4
  store %"class.std::__2::vector.51"* %this, %"class.std::__2::vector.51"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.51"*, %"class.std::__2::vector.51"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.51"* %this1 to %"class.std::__2::__vector_base.52"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.52", %"class.std::__2::__vector_base.52"* %0, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__begin_, align 4
  %call = call %"class.std::__2::unique_ptr.53"* @_ZNSt3__212__to_addressINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEEEPT_S8_(%"class.std::__2::unique_ptr.53"* %1) #8
  ret %"class.std::__2::unique_ptr.53"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector.51"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.51"*, align 4
  store %"class.std::__2::vector.51"* %this, %"class.std::__2::vector.51"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.51"*, %"class.std::__2::vector.51"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.51"* %this1 to %"class.std::__2::__vector_base.52"*
  %call = call i32 @_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::__vector_base.52"* %0) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.51"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.51"*, align 4
  store %"class.std::__2::vector.51"* %this, %"class.std::__2::vector.51"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.51"*, %"class.std::__2::vector.51"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.51"* %this1 to %"class.std::__2::__vector_base.52"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.52", %"class.std::__2::__vector_base.52"* %0, i32 0, i32 1
  %1 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector.51"* %this1 to %"class.std::__2::__vector_base.52"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.52", %"class.std::__2::__vector_base.52"* %2, i32 0, i32 0
  %3 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.std::__2::unique_ptr.53"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.std::__2::unique_ptr.53"* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.53"* @_ZNSt3__212__to_addressINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEEEPT_S8_(%"class.std::__2::unique_ptr.53"* %__p) #0 comdat {
entry:
  %__p.addr = alloca %"class.std::__2::unique_ptr.53"*, align 4
  store %"class.std::__2::unique_ptr.53"* %__p, %"class.std::__2::unique_ptr.53"** %__p.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__p.addr, align 4
  ret %"class.std::__2::unique_ptr.53"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::__vector_base.52"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.52"*, align 4
  store %"class.std::__2::__vector_base.52"* %this, %"class.std::__2::__vector_base.52"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.52"*, %"class.std::__2::__vector_base.52"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.53"** @_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE9__end_capEv(%"class.std::__2::__vector_base.52"* %this1) #8
  %0 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.52", %"class.std::__2::__vector_base.52"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.std::__2::unique_ptr.53"* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.std::__2::unique_ptr.53"* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.53"** @_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE9__end_capEv(%"class.std::__2::__vector_base.52"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.52"*, align 4
  store %"class.std::__2::__vector_base.52"* %this, %"class.std::__2::__vector_base.52"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.52"*, %"class.std::__2::__vector_base.52"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.52", %"class.std::__2::__vector_base.52"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.53"** @_ZNKSt3__217__compressed_pairIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.82"* %__end_cap_) #8
  ret %"class.std::__2::unique_ptr.53"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.53"** @_ZNKSt3__217__compressed_pairIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.82"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.82"*, align 4
  store %"class.std::__2::__compressed_pair.82"* %this, %"class.std::__2::__compressed_pair.82"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.82"*, %"class.std::__2::__compressed_pair.82"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.82"* %this1 to %"struct.std::__2::__compressed_pair_elem.83"*
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.53"** @_ZNKSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.83"* %0) #8
  ret %"class.std::__2::unique_ptr.53"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.53"** @_ZNKSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.83"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.83"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.83"* %this, %"struct.std::__2::__compressed_pair_elem.83"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.83"*, %"struct.std::__2::__compressed_pair_elem.83"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.83", %"struct.std::__2::__compressed_pair_elem.83"* %this1, i32 0, i32 0
  ret %"class.std::__2::unique_ptr.53"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5clearEv(%"class.std::__2::__vector_base.52"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.52"*, align 4
  store %"class.std::__2::__vector_base.52"* %this, %"class.std::__2::__vector_base.52"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.52"*, %"class.std::__2::__vector_base.52"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.52", %"class.std::__2::__vector_base.52"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__begin_, align 4
  call void @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__destruct_at_endEPS6_(%"class.std::__2::__vector_base.52"* %this1, %"class.std::__2::unique_ptr.53"* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS4_EEEEEEE10deallocateERS8_PS7_m(%"class.std::__2::allocator.85"* nonnull align 1 dereferenceable(1) %__a, %"class.std::__2::unique_ptr.53"* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.85"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.53"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.85"* %__a, %"class.std::__2::allocator.85"** %__a.addr, align 4
  store %"class.std::__2::unique_ptr.53"* %__p, %"class.std::__2::unique_ptr.53"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.85"*, %"class.std::__2::allocator.85"** %__a.addr, align 4
  %1 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEE10deallocateEPS6_m(%"class.std::__2::allocator.85"* %0, %"class.std::__2::unique_ptr.53"* %1, i32 %2) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.85"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base.52"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.52"*, align 4
  store %"class.std::__2::__vector_base.52"* %this, %"class.std::__2::__vector_base.52"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.52"*, %"class.std::__2::__vector_base.52"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.52", %"class.std::__2::__vector_base.52"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.85"* @_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE6secondEv(%"class.std::__2::__compressed_pair.82"* %__end_cap_) #8
  ret %"class.std::__2::allocator.85"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__destruct_at_endEPS6_(%"class.std::__2::__vector_base.52"* %this, %"class.std::__2::unique_ptr.53"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.52"*, align 4
  %__new_last.addr = alloca %"class.std::__2::unique_ptr.53"*, align 4
  %__soon_to_be_end = alloca %"class.std::__2::unique_ptr.53"*, align 4
  store %"class.std::__2::__vector_base.52"* %this, %"class.std::__2::__vector_base.52"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.53"* %__new_last, %"class.std::__2::unique_ptr.53"** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.52"*, %"class.std::__2::__vector_base.52"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.52", %"class.std::__2::__vector_base.52"* %this1, i32 0, i32 1
  %0 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__end_, align 4
  store %"class.std::__2::unique_ptr.53"* %0, %"class.std::__2::unique_ptr.53"** %__soon_to_be_end, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__new_last.addr, align 4
  %2 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__soon_to_be_end, align 4
  %cmp = icmp ne %"class.std::__2::unique_ptr.53"* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.85"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base.52"* %this1) #8
  %3 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__soon_to_be_end, align 4
  %incdec.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.53", %"class.std::__2::unique_ptr.53"* %3, i32 -1
  store %"class.std::__2::unique_ptr.53"* %incdec.ptr, %"class.std::__2::unique_ptr.53"** %__soon_to_be_end, align 4
  %call2 = call %"class.std::__2::unique_ptr.53"* @_ZNSt3__212__to_addressINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEEEPT_S8_(%"class.std::__2::unique_ptr.53"* %incdec.ptr) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS4_EEEEEEE7destroyIS7_EEvRS8_PT_(%"class.std::__2::allocator.85"* nonnull align 1 dereferenceable(1) %call, %"class.std::__2::unique_ptr.53"* %call2)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %4 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__new_last.addr, align 4
  %__end_3 = getelementptr inbounds %"class.std::__2::__vector_base.52", %"class.std::__2::__vector_base.52"* %this1, i32 0, i32 1
  store %"class.std::__2::unique_ptr.53"* %4, %"class.std::__2::unique_ptr.53"** %__end_3, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS4_EEEEEEE7destroyIS7_EEvRS8_PT_(%"class.std::__2::allocator.85"* nonnull align 1 dereferenceable(1) %__a, %"class.std::__2::unique_ptr.53"* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.85"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.53"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy.112", align 1
  store %"class.std::__2::allocator.85"* %__a, %"class.std::__2::allocator.85"** %__a.addr, align 4
  store %"class.std::__2::unique_ptr.53"* %__p, %"class.std::__2::unique_ptr.53"** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy.112"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.85"*, %"class.std::__2::allocator.85"** %__a.addr, align 4
  %2 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS4_EEEEEEE9__destroyIS7_EEvNS_17integral_constantIbLb1EEERS8_PT_(%"class.std::__2::allocator.85"* nonnull align 1 dereferenceable(1) %1, %"class.std::__2::unique_ptr.53"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS4_EEEEEEE9__destroyIS7_EEvNS_17integral_constantIbLb1EEERS8_PT_(%"class.std::__2::allocator.85"* nonnull align 1 dereferenceable(1) %__a, %"class.std::__2::unique_ptr.53"* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.85"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.53"*, align 4
  store %"class.std::__2::allocator.85"* %__a, %"class.std::__2::allocator.85"** %__a.addr, align 4
  store %"class.std::__2::unique_ptr.53"* %__p, %"class.std::__2::unique_ptr.53"** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.85"*, %"class.std::__2::allocator.85"** %__a.addr, align 4
  %2 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__p.addr, align 4
  call void @_ZNSt3__29allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEE7destroyEPS6_(%"class.std::__2::allocator.85"* %1, %"class.std::__2::unique_ptr.53"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEE7destroyEPS6_(%"class.std::__2::allocator.85"* %this, %"class.std::__2::unique_ptr.53"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.85"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.53"*, align 4
  store %"class.std::__2::allocator.85"* %this, %"class.std::__2::allocator.85"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.53"* %__p, %"class.std::__2::unique_ptr.53"** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.85"*, %"class.std::__2::allocator.85"** %this.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__p.addr, align 4
  %call = call %"class.std::__2::unique_ptr.53"* @_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.53"* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.53"* @_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.53"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.53"*, align 4
  store %"class.std::__2::unique_ptr.53"* %this, %"class.std::__2::unique_ptr.53"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %this.addr, align 4
  call void @_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.53"* %this1, %"class.draco::PointAttribute"* null) #8
  ret %"class.std::__2::unique_ptr.53"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.53"* %this, %"class.draco::PointAttribute"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.53"*, align 4
  %__p.addr = alloca %"class.draco::PointAttribute"*, align 4
  %__tmp = alloca %"class.draco::PointAttribute"*, align 4
  store %"class.std::__2::unique_ptr.53"* %this, %"class.std::__2::unique_ptr.53"** %this.addr, align 4
  store %"class.draco::PointAttribute"* %__p, %"class.draco::PointAttribute"** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.53", %"class.std::__2::unique_ptr.53"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.54"* %__ptr_) #8
  %0 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %call, align 4
  store %"class.draco::PointAttribute"* %0, %"class.draco::PointAttribute"** %__tmp, align 4
  %1 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %__p.addr, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr.53", %"class.std::__2::unique_ptr.53"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.54"* %__ptr_2) #8
  store %"class.draco::PointAttribute"* %1, %"class.draco::PointAttribute"** %call3, align 4
  %2 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %__tmp, align 4
  %tobool = icmp ne %"class.draco::PointAttribute"* %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__ptr_4 = getelementptr inbounds %"class.std::__2::unique_ptr.53", %"class.std::__2::unique_ptr.53"* %this1, i32 0, i32 0
  %call5 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.81"* @_ZNSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.54"* %__ptr_4) #8
  %3 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %__tmp, align 4
  call void @_ZNKSt3__214default_deleteIN5draco14PointAttributeEEclEPS2_(%"struct.std::__2::default_delete.81"* %call5, %"class.draco::PointAttribute"* %3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.54"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.54"*, align 4
  store %"class.std::__2::__compressed_pair.54"* %this, %"class.std::__2::__compressed_pair.54"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.54"*, %"class.std::__2::__compressed_pair.54"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.54"* %this1 to %"struct.std::__2::__compressed_pair_elem.55"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNSt3__222__compressed_pair_elemIPN5draco14PointAttributeELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.55"* %0) #8
  ret %"class.draco::PointAttribute"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.81"* @_ZNSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.54"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.54"*, align 4
  store %"class.std::__2::__compressed_pair.54"* %this, %"class.std::__2::__compressed_pair.54"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.54"*, %"class.std::__2::__compressed_pair.54"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.54"* %this1 to %"struct.std::__2::__compressed_pair_elem.80"*
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.81"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco14PointAttributeEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.80"* %0) #8
  ret %"struct.std::__2::default_delete.81"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__214default_deleteIN5draco14PointAttributeEEclEPS2_(%"struct.std::__2::default_delete.81"* %this, %"class.draco::PointAttribute"* %__ptr) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::default_delete.81"*, align 4
  %__ptr.addr = alloca %"class.draco::PointAttribute"*, align 4
  store %"struct.std::__2::default_delete.81"* %this, %"struct.std::__2::default_delete.81"** %this.addr, align 4
  store %"class.draco::PointAttribute"* %__ptr, %"class.draco::PointAttribute"** %__ptr.addr, align 4
  %this1 = load %"struct.std::__2::default_delete.81"*, %"struct.std::__2::default_delete.81"** %this.addr, align 4
  %0 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %__ptr.addr, align 4
  %isnull = icmp eq %"class.draco::PointAttribute"* %0, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %entry
  %call = call %"class.draco::PointAttribute"* @_ZN5draco14PointAttributeD2Ev(%"class.draco::PointAttribute"* %0) #8
  %1 = bitcast %"class.draco::PointAttribute"* %0 to i8*
  call void @_ZdlPv(i8* %1) #9
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNSt3__222__compressed_pair_elemIPN5draco14PointAttributeELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.55"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.55"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.55"* %this, %"struct.std::__2::__compressed_pair_elem.55"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.55"*, %"struct.std::__2::__compressed_pair_elem.55"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.55", %"struct.std::__2::__compressed_pair_elem.55"* %this1, i32 0, i32 0
  ret %"class.draco::PointAttribute"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.81"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco14PointAttributeEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.80"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.80"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.80"* %this, %"struct.std::__2::__compressed_pair_elem.80"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.80"*, %"struct.std::__2::__compressed_pair_elem.80"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.80"* %this1 to %"struct.std::__2::default_delete.81"*
  ret %"struct.std::__2::default_delete.81"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::PointAttribute"* @_ZN5draco14PointAttributeD2Ev(%"class.draco::PointAttribute"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointAttribute"*, align 4
  store %"class.draco::PointAttribute"* %this, %"class.draco::PointAttribute"** %this.addr, align 4
  %this1 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %this.addr, align 4
  %attribute_transform_data_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 6
  %call = call %"class.std::__2::unique_ptr.75"* @_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.75"* %attribute_transform_data_) #8
  %indices_map_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 2
  %call2 = call %"class.draco::IndexTypeVector"* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEEED2Ev(%"class.draco::IndexTypeVector"* %indices_map_) #8
  %attribute_buffer_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 1
  %call3 = call %"class.std::__2::unique_ptr.63"* @_ZNSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.63"* %attribute_buffer_) #8
  ret %"class.draco::PointAttribute"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.75"* @_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.75"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.75"*, align 4
  store %"class.std::__2::unique_ptr.75"* %this, %"class.std::__2::unique_ptr.75"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.75"*, %"class.std::__2::unique_ptr.75"** %this.addr, align 4
  call void @_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.75"* %this1, %"class.draco::AttributeTransformData"* null) #8
  ret %"class.std::__2::unique_ptr.75"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexTypeVector"* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEEED2Ev(%"class.draco::IndexTypeVector"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector"*, align 4
  store %"class.draco::IndexTypeVector"* %this, %"class.draco::IndexTypeVector"** %this.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector"*, %"class.draco::IndexTypeVector"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector", %"class.draco::IndexTypeVector"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::vector.68"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEED2Ev(%"class.std::__2::vector.68"* %vector_) #8
  ret %"class.draco::IndexTypeVector"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.63"* @_ZNSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.63"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.63"*, align 4
  store %"class.std::__2::unique_ptr.63"* %this, %"class.std::__2::unique_ptr.63"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.63"*, %"class.std::__2::unique_ptr.63"** %this.addr, align 4
  call void @_ZNSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.63"* %this1, %"class.draco::DataBuffer"* null) #8
  ret %"class.std::__2::unique_ptr.63"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__210unique_ptrIN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.75"* %this, %"class.draco::AttributeTransformData"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.75"*, align 4
  %__p.addr = alloca %"class.draco::AttributeTransformData"*, align 4
  %__tmp = alloca %"class.draco::AttributeTransformData"*, align 4
  store %"class.std::__2::unique_ptr.75"* %this, %"class.std::__2::unique_ptr.75"** %this.addr, align 4
  store %"class.draco::AttributeTransformData"* %__p, %"class.draco::AttributeTransformData"** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.75"*, %"class.std::__2::unique_ptr.75"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.75", %"class.std::__2::unique_ptr.75"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributeTransformData"** @_ZNSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.76"* %__ptr_) #8
  %0 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %call, align 4
  store %"class.draco::AttributeTransformData"* %0, %"class.draco::AttributeTransformData"** %__tmp, align 4
  %1 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %__p.addr, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr.75", %"class.std::__2::unique_ptr.75"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::AttributeTransformData"** @_ZNSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.76"* %__ptr_2) #8
  store %"class.draco::AttributeTransformData"* %1, %"class.draco::AttributeTransformData"** %call3, align 4
  %2 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %__tmp, align 4
  %tobool = icmp ne %"class.draco::AttributeTransformData"* %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__ptr_4 = getelementptr inbounds %"class.std::__2::unique_ptr.75", %"class.std::__2::unique_ptr.75"* %this1, i32 0, i32 0
  %call5 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.79"* @_ZNSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.76"* %__ptr_4) #8
  %3 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %__tmp, align 4
  call void @_ZNKSt3__214default_deleteIN5draco22AttributeTransformDataEEclEPS2_(%"struct.std::__2::default_delete.79"* %call5, %"class.draco::AttributeTransformData"* %3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::AttributeTransformData"** @_ZNSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.76"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.76"*, align 4
  store %"class.std::__2::__compressed_pair.76"* %this, %"class.std::__2::__compressed_pair.76"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.76"*, %"class.std::__2::__compressed_pair.76"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.76"* %this1 to %"struct.std::__2::__compressed_pair_elem.77"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributeTransformData"** @_ZNSt3__222__compressed_pair_elemIPN5draco22AttributeTransformDataELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.77"* %0) #8
  ret %"class.draco::AttributeTransformData"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.79"* @_ZNSt3__217__compressed_pairIPN5draco22AttributeTransformDataENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.76"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.76"*, align 4
  store %"class.std::__2::__compressed_pair.76"* %this, %"class.std::__2::__compressed_pair.76"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.76"*, %"class.std::__2::__compressed_pair.76"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.76"* %this1 to %"struct.std::__2::__compressed_pair_elem.78"*
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.79"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco22AttributeTransformDataEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.78"* %0) #8
  ret %"struct.std::__2::default_delete.79"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__214default_deleteIN5draco22AttributeTransformDataEEclEPS2_(%"struct.std::__2::default_delete.79"* %this, %"class.draco::AttributeTransformData"* %__ptr) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::default_delete.79"*, align 4
  %__ptr.addr = alloca %"class.draco::AttributeTransformData"*, align 4
  store %"struct.std::__2::default_delete.79"* %this, %"struct.std::__2::default_delete.79"** %this.addr, align 4
  store %"class.draco::AttributeTransformData"* %__ptr, %"class.draco::AttributeTransformData"** %__ptr.addr, align 4
  %this1 = load %"struct.std::__2::default_delete.79"*, %"struct.std::__2::default_delete.79"** %this.addr, align 4
  %0 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %__ptr.addr, align 4
  %isnull = icmp eq %"class.draco::AttributeTransformData"* %0, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %entry
  %call = call %"class.draco::AttributeTransformData"* @_ZN5draco22AttributeTransformDataD2Ev(%"class.draco::AttributeTransformData"* %0) #8
  %1 = bitcast %"class.draco::AttributeTransformData"* %0 to i8*
  call void @_ZdlPv(i8* %1) #9
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::AttributeTransformData"** @_ZNSt3__222__compressed_pair_elemIPN5draco22AttributeTransformDataELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.77"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.77"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.77"* %this, %"struct.std::__2::__compressed_pair_elem.77"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.77"*, %"struct.std::__2::__compressed_pair_elem.77"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.77", %"struct.std::__2::__compressed_pair_elem.77"* %this1, i32 0, i32 0
  ret %"class.draco::AttributeTransformData"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.79"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco22AttributeTransformDataEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.78"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.78"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.78"* %this, %"struct.std::__2::__compressed_pair_elem.78"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.78"*, %"struct.std::__2::__compressed_pair_elem.78"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.78"* %this1 to %"struct.std::__2::default_delete.79"*
  ret %"struct.std::__2::default_delete.79"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::AttributeTransformData"* @_ZN5draco22AttributeTransformDataD2Ev(%"class.draco::AttributeTransformData"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::AttributeTransformData"*, align 4
  store %"class.draco::AttributeTransformData"* %this, %"class.draco::AttributeTransformData"** %this.addr, align 4
  %this1 = load %"class.draco::AttributeTransformData"*, %"class.draco::AttributeTransformData"** %this.addr, align 4
  %buffer_ = getelementptr inbounds %"class.draco::AttributeTransformData", %"class.draco::AttributeTransformData"* %this1, i32 0, i32 1
  %call = call %"class.draco::DataBuffer"* @_ZN5draco10DataBufferD2Ev(%"class.draco::DataBuffer"* %buffer_) #8
  ret %"class.draco::AttributeTransformData"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::DataBuffer"* @_ZN5draco10DataBufferD2Ev(%"class.draco::DataBuffer"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::DataBuffer"*, align 4
  store %"class.draco::DataBuffer"* %this, %"class.draco::DataBuffer"** %this.addr, align 4
  %this1 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %this.addr, align 4
  %data_ = getelementptr inbounds %"class.draco::DataBuffer", %"class.draco::DataBuffer"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::vector.56"* @_ZNSt3__26vectorIhNS_9allocatorIhEEED2Ev(%"class.std::__2::vector.56"* %data_) #8
  ret %"class.draco::DataBuffer"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector.56"* @_ZNSt3__26vectorIhNS_9allocatorIhEEED2Ev(%"class.std::__2::vector.56"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.56"*, align 4
  store %"class.std::__2::vector.56"* %this, %"class.std::__2::vector.56"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.56"*, %"class.std::__2::vector.56"** %this.addr, align 4
  call void @_ZNKSt3__26vectorIhNS_9allocatorIhEEE17__annotate_deleteEv(%"class.std::__2::vector.56"* %this1) #8
  %0 = bitcast %"class.std::__2::vector.56"* %this1 to %"class.std::__2::__vector_base.57"*
  %call = call %"class.std::__2::__vector_base.57"* @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEED2Ev(%"class.std::__2::__vector_base.57"* %0) #8
  ret %"class.std::__2::vector.56"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIhNS_9allocatorIhEEE17__annotate_deleteEv(%"class.std::__2::vector.56"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.56"*, align 4
  store %"class.std::__2::vector.56"* %this, %"class.std::__2::vector.56"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.56"*, %"class.std::__2::vector.56"** %this.addr, align 4
  %call = call i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector.56"* %this1) #8
  %call2 = call i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector.56"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::vector.56"* %this1) #8
  %add.ptr = getelementptr inbounds i8, i8* %call2, i32 %call3
  %call4 = call i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector.56"* %this1) #8
  %call5 = call i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4sizeEv(%"class.std::__2::vector.56"* %this1) #8
  %add.ptr6 = getelementptr inbounds i8, i8* %call4, i32 %call5
  %call7 = call i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector.56"* %this1) #8
  %call8 = call i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::vector.56"* %this1) #8
  %add.ptr9 = getelementptr inbounds i8, i8* %call7, i32 %call8
  call void @_ZNKSt3__26vectorIhNS_9allocatorIhEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector.56"* %this1, i8* %call, i8* %add.ptr, i8* %add.ptr6, i8* %add.ptr9) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base.57"* @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEED2Ev(%"class.std::__2::__vector_base.57"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__vector_base.57"*, align 4
  %this.addr = alloca %"class.std::__2::__vector_base.57"*, align 4
  store %"class.std::__2::__vector_base.57"* %this, %"class.std::__2::__vector_base.57"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.57"*, %"class.std::__2::__vector_base.57"** %this.addr, align 4
  store %"class.std::__2::__vector_base.57"* %this1, %"class.std::__2::__vector_base.57"** %retval, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.57", %"class.std::__2::__vector_base.57"* %this1, i32 0, i32 0
  %0 = load i8*, i8** %__begin_, align 4
  %cmp = icmp ne i8* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE5clearEv(%"class.std::__2::__vector_base.57"* %this1) #8
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.61"* @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE7__allocEv(%"class.std::__2::__vector_base.57"* %this1) #8
  %__begin_2 = getelementptr inbounds %"class.std::__2::__vector_base.57", %"class.std::__2::__vector_base.57"* %this1, i32 0, i32 0
  %1 = load i8*, i8** %__begin_2, align 4
  %call3 = call i32 @_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::__vector_base.57"* %this1) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE10deallocateERS2_Phm(%"class.std::__2::allocator.61"* nonnull align 1 dereferenceable(1) %call, i8* %1, i32 %call3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"class.std::__2::__vector_base.57"*, %"class.std::__2::__vector_base.57"** %retval, align 4
  ret %"class.std::__2::__vector_base.57"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIhNS_9allocatorIhEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector.56"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.56"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector.56"* %this, %"class.std::__2::vector.56"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector.56"*, %"class.std::__2::vector.56"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector.56"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.56"*, align 4
  store %"class.std::__2::vector.56"* %this, %"class.std::__2::vector.56"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.56"*, %"class.std::__2::vector.56"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.56"* %this1 to %"class.std::__2::__vector_base.57"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.57", %"class.std::__2::__vector_base.57"* %0, i32 0, i32 0
  %1 = load i8*, i8** %__begin_, align 4
  %call = call i8* @_ZNSt3__212__to_addressIhEEPT_S2_(i8* %1) #8
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::vector.56"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.56"*, align 4
  store %"class.std::__2::vector.56"* %this, %"class.std::__2::vector.56"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.56"*, %"class.std::__2::vector.56"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.56"* %this1 to %"class.std::__2::__vector_base.57"*
  %call = call i32 @_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::__vector_base.57"* %0) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4sizeEv(%"class.std::__2::vector.56"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.56"*, align 4
  store %"class.std::__2::vector.56"* %this, %"class.std::__2::vector.56"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.56"*, %"class.std::__2::vector.56"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.56"* %this1 to %"class.std::__2::__vector_base.57"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.57", %"class.std::__2::__vector_base.57"* %0, i32 0, i32 1
  %1 = load i8*, i8** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector.56"* %this1 to %"class.std::__2::__vector_base.57"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.57", %"class.std::__2::__vector_base.57"* %2, i32 0, i32 0
  %3 = load i8*, i8** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint i8* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  ret i32 %sub.ptr.sub
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__212__to_addressIhEEPT_S2_(i8* %__p) #0 comdat {
entry:
  %__p.addr = alloca i8*, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %0 = load i8*, i8** %__p.addr, align 4
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::__vector_base.57"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.57"*, align 4
  store %"class.std::__2::__vector_base.57"* %this, %"class.std::__2::__vector_base.57"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.57"*, %"class.std::__2::__vector_base.57"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE9__end_capEv(%"class.std::__2::__vector_base.57"* %this1) #8
  %0 = load i8*, i8** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.57", %"class.std::__2::__vector_base.57"* %this1, i32 0, i32 0
  %1 = load i8*, i8** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint i8* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  ret i32 %sub.ptr.sub
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE9__end_capEv(%"class.std::__2::__vector_base.57"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.57"*, align 4
  store %"class.std::__2::__vector_base.57"* %this, %"class.std::__2::__vector_base.57"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.57"*, %"class.std::__2::__vector_base.57"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.57", %"class.std::__2::__vector_base.57"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__217__compressed_pairIPhNS_9allocatorIhEEE5firstEv(%"class.std::__2::__compressed_pair.58"* %__end_cap_) #8
  ret i8** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__217__compressed_pairIPhNS_9allocatorIhEEE5firstEv(%"class.std::__2::__compressed_pair.58"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.58"*, align 4
  store %"class.std::__2::__compressed_pair.58"* %this, %"class.std::__2::__compressed_pair.58"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.58"*, %"class.std::__2::__compressed_pair.58"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.58"* %this1 to %"struct.std::__2::__compressed_pair_elem.59"*
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__222__compressed_pair_elemIPhLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.59"* %0) #8
  ret i8** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__222__compressed_pair_elemIPhLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.59"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.59"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.59"* %this, %"struct.std::__2::__compressed_pair_elem.59"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.59"*, %"struct.std::__2::__compressed_pair_elem.59"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.59", %"struct.std::__2::__compressed_pair_elem.59"* %this1, i32 0, i32 0
  ret i8** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE5clearEv(%"class.std::__2::__vector_base.57"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.57"*, align 4
  store %"class.std::__2::__vector_base.57"* %this, %"class.std::__2::__vector_base.57"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.57"*, %"class.std::__2::__vector_base.57"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.57", %"class.std::__2::__vector_base.57"* %this1, i32 0, i32 0
  %0 = load i8*, i8** %__begin_, align 4
  call void @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE17__destruct_at_endEPh(%"class.std::__2::__vector_base.57"* %this1, i8* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE10deallocateERS2_Phm(%"class.std::__2::allocator.61"* nonnull align 1 dereferenceable(1) %__a, i8* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.61"*, align 4
  %__p.addr = alloca i8*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.61"* %__a, %"class.std::__2::allocator.61"** %__a.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.61"*, %"class.std::__2::allocator.61"** %__a.addr, align 4
  %1 = load i8*, i8** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorIhE10deallocateEPhm(%"class.std::__2::allocator.61"* %0, i8* %1, i32 %2) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.61"* @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE7__allocEv(%"class.std::__2::__vector_base.57"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.57"*, align 4
  store %"class.std::__2::__vector_base.57"* %this, %"class.std::__2::__vector_base.57"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.57"*, %"class.std::__2::__vector_base.57"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.57", %"class.std::__2::__vector_base.57"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.61"* @_ZNSt3__217__compressed_pairIPhNS_9allocatorIhEEE6secondEv(%"class.std::__2::__compressed_pair.58"* %__end_cap_) #8
  ret %"class.std::__2::allocator.61"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE17__destruct_at_endEPh(%"class.std::__2::__vector_base.57"* %this, i8* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.57"*, align 4
  %__new_last.addr = alloca i8*, align 4
  %__soon_to_be_end = alloca i8*, align 4
  store %"class.std::__2::__vector_base.57"* %this, %"class.std::__2::__vector_base.57"** %this.addr, align 4
  store i8* %__new_last, i8** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.57"*, %"class.std::__2::__vector_base.57"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.57", %"class.std::__2::__vector_base.57"* %this1, i32 0, i32 1
  %0 = load i8*, i8** %__end_, align 4
  store i8* %0, i8** %__soon_to_be_end, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load i8*, i8** %__new_last.addr, align 4
  %2 = load i8*, i8** %__soon_to_be_end, align 4
  %cmp = icmp ne i8* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.61"* @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE7__allocEv(%"class.std::__2::__vector_base.57"* %this1) #8
  %3 = load i8*, i8** %__soon_to_be_end, align 4
  %incdec.ptr = getelementptr inbounds i8, i8* %3, i32 -1
  store i8* %incdec.ptr, i8** %__soon_to_be_end, align 4
  %call2 = call i8* @_ZNSt3__212__to_addressIhEEPT_S2_(i8* %incdec.ptr) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE7destroyIhEEvRS2_PT_(%"class.std::__2::allocator.61"* nonnull align 1 dereferenceable(1) %call, i8* %call2)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %4 = load i8*, i8** %__new_last.addr, align 4
  %__end_3 = getelementptr inbounds %"class.std::__2::__vector_base.57", %"class.std::__2::__vector_base.57"* %this1, i32 0, i32 1
  store i8* %4, i8** %__end_3, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE7destroyIhEEvRS2_PT_(%"class.std::__2::allocator.61"* nonnull align 1 dereferenceable(1) %__a, i8* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.61"*, align 4
  %__p.addr = alloca i8*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy.113", align 1
  store %"class.std::__2::allocator.61"* %__a, %"class.std::__2::allocator.61"** %__a.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy.113"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.61"*, %"class.std::__2::allocator.61"** %__a.addr, align 4
  %2 = load i8*, i8** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE9__destroyIhEEvNS_17integral_constantIbLb1EEERS2_PT_(%"class.std::__2::allocator.61"* nonnull align 1 dereferenceable(1) %1, i8* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE9__destroyIhEEvNS_17integral_constantIbLb1EEERS2_PT_(%"class.std::__2::allocator.61"* nonnull align 1 dereferenceable(1) %__a, i8* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.61"*, align 4
  %__p.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.61"* %__a, %"class.std::__2::allocator.61"** %__a.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.61"*, %"class.std::__2::allocator.61"** %__a.addr, align 4
  %2 = load i8*, i8** %__p.addr, align 4
  call void @_ZNSt3__29allocatorIhE7destroyEPh(%"class.std::__2::allocator.61"* %1, i8* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIhE7destroyEPh(%"class.std::__2::allocator.61"* %this, i8* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.61"*, align 4
  %__p.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.61"* %this, %"class.std::__2::allocator.61"** %this.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.61"*, %"class.std::__2::allocator.61"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIhE10deallocateEPhm(%"class.std::__2::allocator.61"* %this, i8* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.61"*, align 4
  %__p.addr = alloca i8*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.61"* %this, %"class.std::__2::allocator.61"** %this.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.61"*, %"class.std::__2::allocator.61"** %this.addr, align 4
  %0 = load i8*, i8** %__p.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %1, 1
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %0, i32 %mul, i32 1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.61"* @_ZNSt3__217__compressed_pairIPhNS_9allocatorIhEEE6secondEv(%"class.std::__2::__compressed_pair.58"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.58"*, align 4
  store %"class.std::__2::__compressed_pair.58"* %this, %"class.std::__2::__compressed_pair.58"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.58"*, %"class.std::__2::__compressed_pair.58"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.58"* %this1 to %"struct.std::__2::__compressed_pair_elem.60"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.61"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIhEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.60"* %0) #8
  ret %"class.std::__2::allocator.61"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.61"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIhEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.60"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.60"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.60"* %this, %"struct.std::__2::__compressed_pair_elem.60"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.60"*, %"struct.std::__2::__compressed_pair_elem.60"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.60"* %this1 to %"class.std::__2::allocator.61"*
  ret %"class.std::__2::allocator.61"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector.68"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEED2Ev(%"class.std::__2::vector.68"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.68"*, align 4
  store %"class.std::__2::vector.68"* %this, %"class.std::__2::vector.68"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %this.addr, align 4
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_deleteEv(%"class.std::__2::vector.68"* %this1) #8
  %0 = bitcast %"class.std::__2::vector.68"* %this1 to %"class.std::__2::__vector_base.69"*
  %call = call %"class.std::__2::__vector_base.69"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEED2Ev(%"class.std::__2::__vector_base.69"* %0) #8
  ret %"class.std::__2::vector.68"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_deleteEv(%"class.std::__2::vector.68"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.68"*, align 4
  store %"class.std::__2::vector.68"* %this, %"class.std::__2::vector.68"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %this.addr, align 4
  %call = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.68"* %this1) #8
  %0 = bitcast %"class.draco::IndexType"* %call to i8*
  %call2 = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.68"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.68"* %this1) #8
  %add.ptr = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %call2, i32 %call3
  %1 = bitcast %"class.draco::IndexType"* %add.ptr to i8*
  %call4 = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.68"* %this1) #8
  %call5 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.68"* %this1) #8
  %add.ptr6 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %call4, i32 %call5
  %2 = bitcast %"class.draco::IndexType"* %add.ptr6 to i8*
  %call7 = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.68"* %this1) #8
  %call8 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.68"* %this1) #8
  %add.ptr9 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %call7, i32 %call8
  %3 = bitcast %"class.draco::IndexType"* %add.ptr9 to i8*
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_(%"class.std::__2::vector.68"* %this1, i8* %0, i8* %1, i8* %2, i8* %3) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base.69"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEED2Ev(%"class.std::__2::__vector_base.69"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__vector_base.69"*, align 4
  %this.addr = alloca %"class.std::__2::__vector_base.69"*, align 4
  store %"class.std::__2::__vector_base.69"* %this, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.69"*, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  store %"class.std::__2::__vector_base.69"* %this1, %"class.std::__2::__vector_base.69"** %retval, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  %cmp = icmp ne %"class.draco::IndexType"* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5clearEv(%"class.std::__2::__vector_base.69"* %this1) #8
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.69"* %this1) #8
  %__begin_2 = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %this1, i32 0, i32 0
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_2, align 4
  %call3 = call i32 @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::__vector_base.69"* %this1) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE10deallocateERS6_PS5_m(%"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %call, %"class.draco::IndexType"* %1, i32 %call3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"class.std::__2::__vector_base.69"*, %"class.std::__2::__vector_base.69"** %retval, align 4
  ret %"class.std::__2::__vector_base.69"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_(%"class.std::__2::vector.68"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.68"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector.68"* %this, %"class.std::__2::vector.68"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.68"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.68"*, align 4
  store %"class.std::__2::vector.68"* %this, %"class.std::__2::vector.68"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.68"* %this1 to %"class.std::__2::__vector_base.69"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %0, i32 0, i32 0
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  %call = call %"class.draco::IndexType"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType"* %1) #8
  ret %"class.draco::IndexType"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.68"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.68"*, align 4
  store %"class.std::__2::vector.68"* %this, %"class.std::__2::vector.68"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.68"* %this1 to %"class.std::__2::__vector_base.69"*
  %call = call i32 @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::__vector_base.69"* %0) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.68"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.68"*, align 4
  store %"class.std::__2::vector.68"* %this, %"class.std::__2::vector.68"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.68"* %this1 to %"class.std::__2::__vector_base.69"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %0, i32 0, i32 1
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector.68"* %this1 to %"class.std::__2::__vector_base.69"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %2, i32 0, i32 0
  %3 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.draco::IndexType"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.draco::IndexType"* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType"* %__p) #0 comdat {
entry:
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  ret %"class.draco::IndexType"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::__vector_base.69"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.69"*, align 4
  store %"class.std::__2::__vector_base.69"* %this, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.69"*, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base.69"* %this1) #8
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %this1, i32 0, i32 0
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.draco::IndexType"* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.draco::IndexType"* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base.69"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.69"*, align 4
  store %"class.std::__2::__vector_base.69"* %this, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.69"*, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.70"* %__end_cap_) #8
  ret %"class.draco::IndexType"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.70"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.70"*, align 4
  store %"class.std::__2::__compressed_pair.70"* %this, %"class.std::__2::__compressed_pair.70"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.70"*, %"class.std::__2::__compressed_pair.70"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.70"* %this1 to %"struct.std::__2::__compressed_pair_elem.71"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.71"* %0) #8
  ret %"class.draco::IndexType"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.71"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.71"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.71"* %this, %"struct.std::__2::__compressed_pair_elem.71"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.71"*, %"struct.std::__2::__compressed_pair_elem.71"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.71", %"struct.std::__2::__compressed_pair_elem.71"* %this1, i32 0, i32 0
  ret %"class.draco::IndexType"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5clearEv(%"class.std::__2::__vector_base.69"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.69"*, align 4
  store %"class.std::__2::__vector_base.69"* %this, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.69"*, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  call void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_(%"class.std::__2::__vector_base.69"* %this1, %"class.draco::IndexType"* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE10deallocateERS6_PS5_m(%"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType"* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.73"*, align 4
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.73"* %__a, %"class.std::__2::allocator.73"** %__a.addr, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.73"*, %"class.std::__2::allocator.73"** %__a.addr, align 4
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE10deallocateEPS4_m(%"class.std::__2::allocator.73"* %0, %"class.draco::IndexType"* %1, i32 %2) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.69"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.69"*, align 4
  store %"class.std::__2::__vector_base.69"* %this, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.69"*, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.70"* %__end_cap_) #8
  ret %"class.std::__2::allocator.73"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_(%"class.std::__2::__vector_base.69"* %this, %"class.draco::IndexType"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.69"*, align 4
  %__new_last.addr = alloca %"class.draco::IndexType"*, align 4
  %__soon_to_be_end = alloca %"class.draco::IndexType"*, align 4
  store %"class.std::__2::__vector_base.69"* %this, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  store %"class.draco::IndexType"* %__new_last, %"class.draco::IndexType"** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.69"*, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %this1, i32 0, i32 1
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__end_, align 4
  store %"class.draco::IndexType"* %0, %"class.draco::IndexType"** %__soon_to_be_end, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__new_last.addr, align 4
  %2 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__soon_to_be_end, align 4
  %cmp = icmp ne %"class.draco::IndexType"* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.69"* %this1) #8
  %3 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__soon_to_be_end, align 4
  %incdec.ptr = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %3, i32 -1
  store %"class.draco::IndexType"* %incdec.ptr, %"class.draco::IndexType"** %__soon_to_be_end, align 4
  %call2 = call %"class.draco::IndexType"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType"* %incdec.ptr) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE7destroyIS5_EEvRS6_PT_(%"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %call, %"class.draco::IndexType"* %call2)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %4 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__new_last.addr, align 4
  %__end_3 = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %this1, i32 0, i32 1
  store %"class.draco::IndexType"* %4, %"class.draco::IndexType"** %__end_3, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE7destroyIS5_EEvRS6_PT_(%"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType"* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.73"*, align 4
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy.114", align 1
  store %"class.std::__2::allocator.73"* %__a, %"class.std::__2::allocator.73"** %__a.addr, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy.114"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.73"*, %"class.std::__2::allocator.73"** %__a.addr, align 4
  %2 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE9__destroyIS5_EEvNS_17integral_constantIbLb1EEERS6_PT_(%"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %1, %"class.draco::IndexType"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE9__destroyIS5_EEvNS_17integral_constantIbLb1EEERS6_PT_(%"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType"* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.73"*, align 4
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.std::__2::allocator.73"* %__a, %"class.std::__2::allocator.73"** %__a.addr, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.73"*, %"class.std::__2::allocator.73"** %__a.addr, align 4
  %2 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  call void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE7destroyEPS4_(%"class.std::__2::allocator.73"* %1, %"class.draco::IndexType"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE7destroyEPS4_(%"class.std::__2::allocator.73"* %this, %"class.draco::IndexType"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.73"*, align 4
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.std::__2::allocator.73"* %this, %"class.std::__2::allocator.73"** %this.addr, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.73"*, %"class.std::__2::allocator.73"** %this.addr, align 4
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE10deallocateEPS4_m(%"class.std::__2::allocator.73"* %this, %"class.draco::IndexType"* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.73"*, align 4
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.73"* %this, %"class.std::__2::allocator.73"** %this.addr, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.73"*, %"class.std::__2::allocator.73"** %this.addr, align 4
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  %1 = bitcast %"class.draco::IndexType"* %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.70"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.70"*, align 4
  store %"class.std::__2::__compressed_pair.70"* %this, %"class.std::__2::__compressed_pair.70"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.70"*, %"class.std::__2::__compressed_pair.70"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.70"* %this1 to %"struct.std::__2::__compressed_pair_elem.72"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.72"* %0) #8
  ret %"class.std::__2::allocator.73"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.72"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.72"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.72"* %this, %"struct.std::__2::__compressed_pair_elem.72"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.72"*, %"struct.std::__2::__compressed_pair_elem.72"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.72"* %this1 to %"class.std::__2::allocator.73"*
  ret %"class.std::__2::allocator.73"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.63"* %this, %"class.draco::DataBuffer"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.63"*, align 4
  %__p.addr = alloca %"class.draco::DataBuffer"*, align 4
  %__tmp = alloca %"class.draco::DataBuffer"*, align 4
  store %"class.std::__2::unique_ptr.63"* %this, %"class.std::__2::unique_ptr.63"** %this.addr, align 4
  store %"class.draco::DataBuffer"* %__p, %"class.draco::DataBuffer"** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.63"*, %"class.std::__2::unique_ptr.63"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.63", %"class.std::__2::unique_ptr.63"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::DataBuffer"** @_ZNSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.64"* %__ptr_) #8
  %0 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %call, align 4
  store %"class.draco::DataBuffer"* %0, %"class.draco::DataBuffer"** %__tmp, align 4
  %1 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %__p.addr, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr.63", %"class.std::__2::unique_ptr.63"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::DataBuffer"** @_ZNSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.64"* %__ptr_2) #8
  store %"class.draco::DataBuffer"* %1, %"class.draco::DataBuffer"** %call3, align 4
  %2 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %__tmp, align 4
  %tobool = icmp ne %"class.draco::DataBuffer"* %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__ptr_4 = getelementptr inbounds %"class.std::__2::unique_ptr.63", %"class.std::__2::unique_ptr.63"* %this1, i32 0, i32 0
  %call5 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.67"* @_ZNSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.64"* %__ptr_4) #8
  %3 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %__tmp, align 4
  call void @_ZNKSt3__214default_deleteIN5draco10DataBufferEEclEPS2_(%"struct.std::__2::default_delete.67"* %call5, %"class.draco::DataBuffer"* %3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::DataBuffer"** @_ZNSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.64"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.64"*, align 4
  store %"class.std::__2::__compressed_pair.64"* %this, %"class.std::__2::__compressed_pair.64"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.64"*, %"class.std::__2::__compressed_pair.64"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.64"* %this1 to %"struct.std::__2::__compressed_pair_elem.65"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::DataBuffer"** @_ZNSt3__222__compressed_pair_elemIPN5draco10DataBufferELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.65"* %0) #8
  ret %"class.draco::DataBuffer"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.67"* @_ZNSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.64"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.64"*, align 4
  store %"class.std::__2::__compressed_pair.64"* %this, %"class.std::__2::__compressed_pair.64"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.64"*, %"class.std::__2::__compressed_pair.64"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.64"* %this1 to %"struct.std::__2::__compressed_pair_elem.66"*
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.67"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco10DataBufferEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.66"* %0) #8
  ret %"struct.std::__2::default_delete.67"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__214default_deleteIN5draco10DataBufferEEclEPS2_(%"struct.std::__2::default_delete.67"* %this, %"class.draco::DataBuffer"* %__ptr) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::default_delete.67"*, align 4
  %__ptr.addr = alloca %"class.draco::DataBuffer"*, align 4
  store %"struct.std::__2::default_delete.67"* %this, %"struct.std::__2::default_delete.67"** %this.addr, align 4
  store %"class.draco::DataBuffer"* %__ptr, %"class.draco::DataBuffer"** %__ptr.addr, align 4
  %this1 = load %"struct.std::__2::default_delete.67"*, %"struct.std::__2::default_delete.67"** %this.addr, align 4
  %0 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %__ptr.addr, align 4
  %isnull = icmp eq %"class.draco::DataBuffer"* %0, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %entry
  %call = call %"class.draco::DataBuffer"* @_ZN5draco10DataBufferD2Ev(%"class.draco::DataBuffer"* %0) #8
  %1 = bitcast %"class.draco::DataBuffer"* %0 to i8*
  call void @_ZdlPv(i8* %1) #9
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::DataBuffer"** @_ZNSt3__222__compressed_pair_elemIPN5draco10DataBufferELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.65"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.65"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.65"* %this, %"struct.std::__2::__compressed_pair_elem.65"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.65"*, %"struct.std::__2::__compressed_pair_elem.65"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.65", %"struct.std::__2::__compressed_pair_elem.65"* %this1, i32 0, i32 0
  ret %"class.draco::DataBuffer"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.67"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco10DataBufferEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.66"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.66"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.66"* %this, %"struct.std::__2::__compressed_pair_elem.66"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.66"*, %"struct.std::__2::__compressed_pair_elem.66"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.66"* %this1 to %"struct.std::__2::default_delete.67"*
  ret %"struct.std::__2::default_delete.67"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEE10deallocateEPS6_m(%"class.std::__2::allocator.85"* %this, %"class.std::__2::unique_ptr.53"* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.85"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.53"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.85"* %this, %"class.std::__2::allocator.85"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.53"* %__p, %"class.std::__2::unique_ptr.53"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.85"*, %"class.std::__2::allocator.85"** %this.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__p.addr, align 4
  %1 = bitcast %"class.std::__2::unique_ptr.53"* %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.85"* @_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE6secondEv(%"class.std::__2::__compressed_pair.82"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.82"*, align 4
  store %"class.std::__2::__compressed_pair.82"* %this, %"class.std::__2::__compressed_pair.82"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.82"*, %"class.std::__2::__compressed_pair.82"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.82"* %this1 to %"struct.std::__2::__compressed_pair_elem.84"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.85"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS4_EEEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.84"* %0) #8
  ret %"class.std::__2::allocator.85"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.85"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS4_EEEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.84"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.84"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.84"* %this, %"struct.std::__2::__compressed_pair_elem.84"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.84"*, %"struct.std::__2::__compressed_pair_elem.84"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.84"* %this1 to %"class.std::__2::allocator.85"*
  ret %"class.std::__2::allocator.85"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__210unique_ptrIN5draco16GeometryMetadataENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr"* %this, %"class.draco::GeometryMetadata"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  %__p.addr = alloca %"class.draco::GeometryMetadata"*, align 4
  %__tmp = alloca %"class.draco::GeometryMetadata"*, align 4
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  store %"class.draco::GeometryMetadata"* %__p, %"class.draco::GeometryMetadata"** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::GeometryMetadata"** @_ZNSt3__217__compressed_pairIPN5draco16GeometryMetadataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair"* %__ptr_) #8
  %0 = load %"class.draco::GeometryMetadata"*, %"class.draco::GeometryMetadata"** %call, align 4
  store %"class.draco::GeometryMetadata"* %0, %"class.draco::GeometryMetadata"** %__tmp, align 4
  %1 = load %"class.draco::GeometryMetadata"*, %"class.draco::GeometryMetadata"** %__p.addr, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::GeometryMetadata"** @_ZNSt3__217__compressed_pairIPN5draco16GeometryMetadataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair"* %__ptr_2) #8
  store %"class.draco::GeometryMetadata"* %1, %"class.draco::GeometryMetadata"** %call3, align 4
  %2 = load %"class.draco::GeometryMetadata"*, %"class.draco::GeometryMetadata"** %__tmp, align 4
  %tobool = icmp ne %"class.draco::GeometryMetadata"* %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__ptr_4 = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call5 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.50"* @_ZNSt3__217__compressed_pairIPN5draco16GeometryMetadataENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair"* %__ptr_4) #8
  %3 = load %"class.draco::GeometryMetadata"*, %"class.draco::GeometryMetadata"** %__tmp, align 4
  call void @_ZNKSt3__214default_deleteIN5draco16GeometryMetadataEEclEPS2_(%"struct.std::__2::default_delete.50"* %call5, %"class.draco::GeometryMetadata"* %3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::GeometryMetadata"** @_ZNSt3__217__compressed_pairIPN5draco16GeometryMetadataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::GeometryMetadata"** @_ZNSt3__222__compressed_pair_elemIPN5draco16GeometryMetadataELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %0) #8
  ret %"class.draco::GeometryMetadata"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.50"* @_ZNSt3__217__compressed_pairIPN5draco16GeometryMetadataENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem.49"*
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.50"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco16GeometryMetadataEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.49"* %0) #8
  ret %"struct.std::__2::default_delete.50"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__214default_deleteIN5draco16GeometryMetadataEEclEPS2_(%"struct.std::__2::default_delete.50"* %this, %"class.draco::GeometryMetadata"* %__ptr) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::default_delete.50"*, align 4
  %__ptr.addr = alloca %"class.draco::GeometryMetadata"*, align 4
  store %"struct.std::__2::default_delete.50"* %this, %"struct.std::__2::default_delete.50"** %this.addr, align 4
  store %"class.draco::GeometryMetadata"* %__ptr, %"class.draco::GeometryMetadata"** %__ptr.addr, align 4
  %this1 = load %"struct.std::__2::default_delete.50"*, %"struct.std::__2::default_delete.50"** %this.addr, align 4
  %0 = load %"class.draco::GeometryMetadata"*, %"class.draco::GeometryMetadata"** %__ptr.addr, align 4
  %isnull = icmp eq %"class.draco::GeometryMetadata"* %0, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %entry
  %call = call %"class.draco::GeometryMetadata"* @_ZN5draco16GeometryMetadataD2Ev(%"class.draco::GeometryMetadata"* %0) #8
  %1 = bitcast %"class.draco::GeometryMetadata"* %0 to i8*
  call void @_ZdlPv(i8* %1) #9
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::GeometryMetadata"** @_ZNSt3__222__compressed_pair_elemIPN5draco16GeometryMetadataELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem"*, align 4
  store %"struct.std::__2::__compressed_pair_elem"* %this, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem"*, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem"* %this1, i32 0, i32 0
  ret %"class.draco::GeometryMetadata"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.50"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco16GeometryMetadataEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.49"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.49"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.49"* %this, %"struct.std::__2::__compressed_pair_elem.49"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.49"*, %"struct.std::__2::__compressed_pair_elem.49"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.49"* %this1 to %"struct.std::__2::default_delete.50"*
  ret %"struct.std::__2::default_delete.50"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::GeometryMetadata"* @_ZN5draco16GeometryMetadataD2Ev(%"class.draco::GeometryMetadata"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::GeometryMetadata"*, align 4
  store %"class.draco::GeometryMetadata"* %this, %"class.draco::GeometryMetadata"** %this.addr, align 4
  %this1 = load %"class.draco::GeometryMetadata"*, %"class.draco::GeometryMetadata"** %this.addr, align 4
  %att_metadatas_ = getelementptr inbounds %"class.draco::GeometryMetadata", %"class.draco::GeometryMetadata"* %this1, i32 0, i32 1
  %call = call %"class.std::__2::vector"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEED2Ev(%"class.std::__2::vector"* %att_metadatas_) #8
  %0 = bitcast %"class.draco::GeometryMetadata"* %this1 to %"class.draco::Metadata"*
  %call2 = call %"class.draco::Metadata"* @_ZN5draco8MetadataD2Ev(%"class.draco::Metadata"* %0) #8
  ret %"class.draco::GeometryMetadata"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEED2Ev(%"class.std::__2::vector"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  call void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__annotate_deleteEv(%"class.std::__2::vector"* %this1) #8
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call %"class.std::__2::__vector_base"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEED2Ev(%"class.std::__2::__vector_base"* %0) #8
  ret %"class.std::__2::vector"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__annotate_deleteEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %call = call %"class.std::__2::unique_ptr.40"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector"* %this1) #8
  %0 = bitcast %"class.std::__2::unique_ptr.40"* %call to i8*
  %call2 = call %"class.std::__2::unique_ptr.40"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector"* %this1) #8
  %add.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.40", %"class.std::__2::unique_ptr.40"* %call2, i32 %call3
  %1 = bitcast %"class.std::__2::unique_ptr.40"* %add.ptr to i8*
  %call4 = call %"class.std::__2::unique_ptr.40"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector"* %this1) #8
  %call5 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector"* %this1) #8
  %add.ptr6 = getelementptr inbounds %"class.std::__2::unique_ptr.40", %"class.std::__2::unique_ptr.40"* %call4, i32 %call5
  %2 = bitcast %"class.std::__2::unique_ptr.40"* %add.ptr6 to i8*
  %call7 = call %"class.std::__2::unique_ptr.40"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector"* %this1) #8
  %call8 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector"* %this1) #8
  %add.ptr9 = getelementptr inbounds %"class.std::__2::unique_ptr.40", %"class.std::__2::unique_ptr.40"* %call7, i32 %call8
  %3 = bitcast %"class.std::__2::unique_ptr.40"* %add.ptr9 to i8*
  call void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE31__annotate_contiguous_containerEPKvSB_SB_SB_(%"class.std::__2::vector"* %this1, i8* %0, i8* %1, i8* %2, i8* %3) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEED2Ev(%"class.std::__2::__vector_base"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__vector_base"*, align 4
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  store %"class.std::__2::__vector_base"* %this1, %"class.std::__2::__vector_base"** %retval, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__begin_, align 4
  %cmp = icmp ne %"class.std::__2::unique_ptr.40"* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5clearEv(%"class.std::__2::__vector_base"* %this1) #8
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.47"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base"* %this1) #8
  %__begin_2 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__begin_2, align 4
  %call3 = call i32 @_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::__vector_base"* %this1) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEEE10deallocateERS8_PS7_m(%"class.std::__2::allocator.47"* nonnull align 1 dereferenceable(1) %call, %"class.std::__2::unique_ptr.40"* %1, i32 %call3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %retval, align 4
  ret %"class.std::__2::__vector_base"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE31__annotate_contiguous_containerEPKvSB_SB_SB_(%"class.std::__2::vector"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.40"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__begin_, align 4
  %call = call %"class.std::__2::unique_ptr.40"* @_ZNSt3__212__to_addressINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEEPT_S8_(%"class.std::__2::unique_ptr.40"* %1) #8
  ret %"class.std::__2::unique_ptr.40"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call i32 @_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::__vector_base"* %0) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 1
  %1 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %2, i32 0, i32 0
  %3 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.std::__2::unique_ptr.40"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.std::__2::unique_ptr.40"* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.40"* @_ZNSt3__212__to_addressINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEEPT_S8_(%"class.std::__2::unique_ptr.40"* %__p) #0 comdat {
entry:
  %__p.addr = alloca %"class.std::__2::unique_ptr.40"*, align 4
  store %"class.std::__2::unique_ptr.40"* %__p, %"class.std::__2::unique_ptr.40"** %__p.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__p.addr, align 4
  ret %"class.std::__2::unique_ptr.40"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.40"** @_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE9__end_capEv(%"class.std::__2::__vector_base"* %this1) #8
  %0 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.std::__2::unique_ptr.40"* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.std::__2::unique_ptr.40"* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.40"** @_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE9__end_capEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.40"** @_ZNKSt3__217__compressed_pairIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.44"* %__end_cap_) #8
  ret %"class.std::__2::unique_ptr.40"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.40"** @_ZNKSt3__217__compressed_pairIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.44"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.44"*, align 4
  store %"class.std::__2::__compressed_pair.44"* %this, %"class.std::__2::__compressed_pair.44"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.44"*, %"class.std::__2::__compressed_pair.44"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.44"* %this1 to %"struct.std::__2::__compressed_pair_elem.45"*
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.40"** @_ZNKSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.45"* %0) #8
  ret %"class.std::__2::unique_ptr.40"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.40"** @_ZNKSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.45"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.45"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.45"* %this, %"struct.std::__2::__compressed_pair_elem.45"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.45"*, %"struct.std::__2::__compressed_pair_elem.45"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.45", %"struct.std::__2::__compressed_pair_elem.45"* %this1, i32 0, i32 0
  ret %"class.std::__2::unique_ptr.40"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5clearEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__begin_, align 4
  call void @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__destruct_at_endEPS6_(%"class.std::__2::__vector_base"* %this1, %"class.std::__2::unique_ptr.40"* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEEE10deallocateERS8_PS7_m(%"class.std::__2::allocator.47"* nonnull align 1 dereferenceable(1) %__a, %"class.std::__2::unique_ptr.40"* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.47"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.40"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.47"* %__a, %"class.std::__2::allocator.47"** %__a.addr, align 4
  store %"class.std::__2::unique_ptr.40"* %__p, %"class.std::__2::unique_ptr.40"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.47"*, %"class.std::__2::allocator.47"** %__a.addr, align 4
  %1 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEE10deallocateEPS6_m(%"class.std::__2::allocator.47"* %0, %"class.std::__2::unique_ptr.40"* %1, i32 %2) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.47"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.47"* @_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE6secondEv(%"class.std::__2::__compressed_pair.44"* %__end_cap_) #8
  ret %"class.std::__2::allocator.47"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__destruct_at_endEPS6_(%"class.std::__2::__vector_base"* %this, %"class.std::__2::unique_ptr.40"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  %__new_last.addr = alloca %"class.std::__2::unique_ptr.40"*, align 4
  %__soon_to_be_end = alloca %"class.std::__2::unique_ptr.40"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.40"* %__new_last, %"class.std::__2::unique_ptr.40"** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 1
  %0 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__end_, align 4
  store %"class.std::__2::unique_ptr.40"* %0, %"class.std::__2::unique_ptr.40"** %__soon_to_be_end, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__new_last.addr, align 4
  %2 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__soon_to_be_end, align 4
  %cmp = icmp ne %"class.std::__2::unique_ptr.40"* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.47"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base"* %this1) #8
  %3 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__soon_to_be_end, align 4
  %incdec.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.40", %"class.std::__2::unique_ptr.40"* %3, i32 -1
  store %"class.std::__2::unique_ptr.40"* %incdec.ptr, %"class.std::__2::unique_ptr.40"** %__soon_to_be_end, align 4
  %call2 = call %"class.std::__2::unique_ptr.40"* @_ZNSt3__212__to_addressINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEEPT_S8_(%"class.std::__2::unique_ptr.40"* %incdec.ptr) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEEE7destroyIS7_EEvRS8_PT_(%"class.std::__2::allocator.47"* nonnull align 1 dereferenceable(1) %call, %"class.std::__2::unique_ptr.40"* %call2)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %4 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__new_last.addr, align 4
  %__end_3 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 1
  store %"class.std::__2::unique_ptr.40"* %4, %"class.std::__2::unique_ptr.40"** %__end_3, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEEE7destroyIS7_EEvRS8_PT_(%"class.std::__2::allocator.47"* nonnull align 1 dereferenceable(1) %__a, %"class.std::__2::unique_ptr.40"* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.47"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.40"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy.115", align 1
  store %"class.std::__2::allocator.47"* %__a, %"class.std::__2::allocator.47"** %__a.addr, align 4
  store %"class.std::__2::unique_ptr.40"* %__p, %"class.std::__2::unique_ptr.40"** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy.115"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.47"*, %"class.std::__2::allocator.47"** %__a.addr, align 4
  %2 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEEE9__destroyIS7_EEvNS_17integral_constantIbLb1EEERS8_PT_(%"class.std::__2::allocator.47"* nonnull align 1 dereferenceable(1) %1, %"class.std::__2::unique_ptr.40"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEEE9__destroyIS7_EEvNS_17integral_constantIbLb1EEERS8_PT_(%"class.std::__2::allocator.47"* nonnull align 1 dereferenceable(1) %__a, %"class.std::__2::unique_ptr.40"* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.47"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.40"*, align 4
  store %"class.std::__2::allocator.47"* %__a, %"class.std::__2::allocator.47"** %__a.addr, align 4
  store %"class.std::__2::unique_ptr.40"* %__p, %"class.std::__2::unique_ptr.40"** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.47"*, %"class.std::__2::allocator.47"** %__a.addr, align 4
  %2 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__p.addr, align 4
  call void @_ZNSt3__29allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEE7destroyEPS6_(%"class.std::__2::allocator.47"* %1, %"class.std::__2::unique_ptr.40"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEE7destroyEPS6_(%"class.std::__2::allocator.47"* %this, %"class.std::__2::unique_ptr.40"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.47"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.40"*, align 4
  store %"class.std::__2::allocator.47"* %this, %"class.std::__2::allocator.47"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.40"* %__p, %"class.std::__2::unique_ptr.40"** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.47"*, %"class.std::__2::allocator.47"** %this.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__p.addr, align 4
  %call = call %"class.std::__2::unique_ptr.40"* @_ZNSt3__210unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.40"* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.40"* @_ZNSt3__210unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.40"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.40"*, align 4
  store %"class.std::__2::unique_ptr.40"* %this, %"class.std::__2::unique_ptr.40"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %this.addr, align 4
  call void @_ZNSt3__210unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.40"* %this1, %"class.draco::AttributeMetadata"* null) #8
  ret %"class.std::__2::unique_ptr.40"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__210unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.40"* %this, %"class.draco::AttributeMetadata"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.40"*, align 4
  %__p.addr = alloca %"class.draco::AttributeMetadata"*, align 4
  %__tmp = alloca %"class.draco::AttributeMetadata"*, align 4
  store %"class.std::__2::unique_ptr.40"* %this, %"class.std::__2::unique_ptr.40"** %this.addr, align 4
  store %"class.draco::AttributeMetadata"* %__p, %"class.draco::AttributeMetadata"** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.40", %"class.std::__2::unique_ptr.40"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributeMetadata"** @_ZNSt3__217__compressed_pairIPN5draco17AttributeMetadataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.41"* %__ptr_) #8
  %0 = load %"class.draco::AttributeMetadata"*, %"class.draco::AttributeMetadata"** %call, align 4
  store %"class.draco::AttributeMetadata"* %0, %"class.draco::AttributeMetadata"** %__tmp, align 4
  %1 = load %"class.draco::AttributeMetadata"*, %"class.draco::AttributeMetadata"** %__p.addr, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr.40", %"class.std::__2::unique_ptr.40"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::AttributeMetadata"** @_ZNSt3__217__compressed_pairIPN5draco17AttributeMetadataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.41"* %__ptr_2) #8
  store %"class.draco::AttributeMetadata"* %1, %"class.draco::AttributeMetadata"** %call3, align 4
  %2 = load %"class.draco::AttributeMetadata"*, %"class.draco::AttributeMetadata"** %__tmp, align 4
  %tobool = icmp ne %"class.draco::AttributeMetadata"* %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__ptr_4 = getelementptr inbounds %"class.std::__2::unique_ptr.40", %"class.std::__2::unique_ptr.40"* %this1, i32 0, i32 0
  %call5 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete"* @_ZNSt3__217__compressed_pairIPN5draco17AttributeMetadataENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.41"* %__ptr_4) #8
  %3 = load %"class.draco::AttributeMetadata"*, %"class.draco::AttributeMetadata"** %__tmp, align 4
  call void @_ZNKSt3__214default_deleteIN5draco17AttributeMetadataEEclEPS2_(%"struct.std::__2::default_delete"* %call5, %"class.draco::AttributeMetadata"* %3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::AttributeMetadata"** @_ZNSt3__217__compressed_pairIPN5draco17AttributeMetadataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.41"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.41"*, align 4
  store %"class.std::__2::__compressed_pair.41"* %this, %"class.std::__2::__compressed_pair.41"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.41"*, %"class.std::__2::__compressed_pair.41"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.41"* %this1 to %"struct.std::__2::__compressed_pair_elem.42"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributeMetadata"** @_ZNSt3__222__compressed_pair_elemIPN5draco17AttributeMetadataELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.42"* %0) #8
  ret %"class.draco::AttributeMetadata"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete"* @_ZNSt3__217__compressed_pairIPN5draco17AttributeMetadataENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.41"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.41"*, align 4
  store %"class.std::__2::__compressed_pair.41"* %this, %"class.std::__2::__compressed_pair.41"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.41"*, %"class.std::__2::__compressed_pair.41"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.41"* %this1 to %"struct.std::__2::__compressed_pair_elem.43"*
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco17AttributeMetadataEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.43"* %0) #8
  ret %"struct.std::__2::default_delete"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__214default_deleteIN5draco17AttributeMetadataEEclEPS2_(%"struct.std::__2::default_delete"* %this, %"class.draco::AttributeMetadata"* %__ptr) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::default_delete"*, align 4
  %__ptr.addr = alloca %"class.draco::AttributeMetadata"*, align 4
  store %"struct.std::__2::default_delete"* %this, %"struct.std::__2::default_delete"** %this.addr, align 4
  store %"class.draco::AttributeMetadata"* %__ptr, %"class.draco::AttributeMetadata"** %__ptr.addr, align 4
  %this1 = load %"struct.std::__2::default_delete"*, %"struct.std::__2::default_delete"** %this.addr, align 4
  %0 = load %"class.draco::AttributeMetadata"*, %"class.draco::AttributeMetadata"** %__ptr.addr, align 4
  %isnull = icmp eq %"class.draco::AttributeMetadata"* %0, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %entry
  %call = call %"class.draco::AttributeMetadata"* @_ZN5draco17AttributeMetadataD2Ev(%"class.draco::AttributeMetadata"* %0) #8
  %1 = bitcast %"class.draco::AttributeMetadata"* %0 to i8*
  call void @_ZdlPv(i8* %1) #9
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::AttributeMetadata"** @_ZNSt3__222__compressed_pair_elemIPN5draco17AttributeMetadataELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.42"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.42"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.42"* %this, %"struct.std::__2::__compressed_pair_elem.42"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.42"*, %"struct.std::__2::__compressed_pair_elem.42"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.42", %"struct.std::__2::__compressed_pair_elem.42"* %this1, i32 0, i32 0
  ret %"class.draco::AttributeMetadata"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco17AttributeMetadataEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.43"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.43"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.43"* %this, %"struct.std::__2::__compressed_pair_elem.43"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.43"*, %"struct.std::__2::__compressed_pair_elem.43"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.43"* %this1 to %"struct.std::__2::default_delete"*
  ret %"struct.std::__2::default_delete"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::AttributeMetadata"* @_ZN5draco17AttributeMetadataD2Ev(%"class.draco::AttributeMetadata"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::AttributeMetadata"*, align 4
  store %"class.draco::AttributeMetadata"* %this, %"class.draco::AttributeMetadata"** %this.addr, align 4
  %this1 = load %"class.draco::AttributeMetadata"*, %"class.draco::AttributeMetadata"** %this.addr, align 4
  %0 = bitcast %"class.draco::AttributeMetadata"* %this1 to %"class.draco::Metadata"*
  %call = call %"class.draco::Metadata"* @_ZN5draco8MetadataD2Ev(%"class.draco::Metadata"* %0) #8
  ret %"class.draco::AttributeMetadata"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEE10deallocateEPS6_m(%"class.std::__2::allocator.47"* %this, %"class.std::__2::unique_ptr.40"* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.47"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.40"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.47"* %this, %"class.std::__2::allocator.47"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.40"* %__p, %"class.std::__2::unique_ptr.40"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.47"*, %"class.std::__2::allocator.47"** %this.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__p.addr, align 4
  %1 = bitcast %"class.std::__2::unique_ptr.40"* %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.47"* @_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE6secondEv(%"class.std::__2::__compressed_pair.44"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.44"*, align 4
  store %"class.std::__2::__compressed_pair.44"* %this, %"class.std::__2::__compressed_pair.44"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.44"*, %"class.std::__2::__compressed_pair.44"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.44"* %this1 to %"struct.std::__2::__compressed_pair_elem.46"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.47"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.46"* %0) #8
  ret %"class.std::__2::allocator.47"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.47"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.46"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.46"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.46"* %this, %"struct.std::__2::__compressed_pair_elem.46"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.46"*, %"struct.std::__2::__compressed_pair_elem.46"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.46"* %this1 to %"class.std::__2::allocator.47"*
  ret %"class.std::__2::allocator.47"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unordered_map.17"* @_ZNSt3__213unordered_mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteIS9_EEEENS_4hashIS6_EENS_8equal_toIS6_EENS4_INS_4pairIKS6_SC_EEEEED2Ev(%"class.std::__2::unordered_map.17"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unordered_map.17"*, align 4
  store %"class.std::__2::unordered_map.17"* %this, %"class.std::__2::unordered_map.17"** %this.addr, align 4
  %this1 = load %"class.std::__2::unordered_map.17"*, %"class.std::__2::unordered_map.17"** %this.addr, align 4
  %__table_ = getelementptr inbounds %"class.std::__2::unordered_map.17", %"class.std::__2::unordered_map.17"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::__hash_table.18"* @_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEEENS_22__unordered_map_hasherIS7_SE_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SE_NS_8equal_toIS7_EELb1EEENS5_ISE_EEED2Ev(%"class.std::__2::__hash_table.18"* %__table_) #8
  ret %"class.std::__2::unordered_map.17"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unordered_map"* @_ZNSt3__213unordered_mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueENS_4hashIS6_EENS_8equal_toIS6_EENS4_INS_4pairIKS6_S8_EEEEED2Ev(%"class.std::__2::unordered_map"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unordered_map"*, align 4
  store %"class.std::__2::unordered_map"* %this, %"class.std::__2::unordered_map"** %this.addr, align 4
  %this1 = load %"class.std::__2::unordered_map"*, %"class.std::__2::unordered_map"** %this.addr, align 4
  %__table_ = getelementptr inbounds %"class.std::__2::unordered_map", %"class.std::__2::unordered_map"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::__hash_table"* @_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEENS_22__unordered_map_hasherIS7_SA_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SA_NS_8equal_toIS7_EELb1EEENS5_ISA_EEED2Ev(%"class.std::__2::__hash_table"* %__table_) #8
  ret %"class.std::__2::unordered_map"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__hash_table.18"* @_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEEENS_22__unordered_map_hasherIS7_SE_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SE_NS_8equal_toIS7_EELb1EEENS5_ISE_EEED2Ev(%"class.std::__2::__hash_table.18"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__hash_table.18"*, align 4
  store %"class.std::__2::__hash_table.18"* %this, %"class.std::__2::__hash_table.18"** %this.addr, align 4
  %this1 = load %"class.std::__2::__hash_table.18"*, %"class.std::__2::__hash_table.18"** %this.addr, align 4
  %__p1_ = getelementptr inbounds %"class.std::__2::__hash_table.18", %"class.std::__2::__hash_table.18"* %this1, i32 0, i32 1
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base.22"* @_ZNSt3__217__compressed_pairINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEENS7_ISI_EEE5firstEv(%"class.std::__2::__compressed_pair.29"* %__p1_) #8
  %__next_ = getelementptr inbounds %"struct.std::__2::__hash_node_base.22", %"struct.std::__2::__hash_node_base.22"* %call, i32 0, i32 0
  %0 = load %"struct.std::__2::__hash_node_base.22"*, %"struct.std::__2::__hash_node_base.22"** %__next_, align 4
  call void @_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEEENS_22__unordered_map_hasherIS7_SE_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SE_NS_8equal_toIS7_EELb1EEENS5_ISE_EEE17__deallocate_nodeEPNS_16__hash_node_baseIPNS_11__hash_nodeISE_PvEEEE(%"class.std::__2::__hash_table.18"* %this1, %"struct.std::__2::__hash_node_base.22"* %0) #8
  %__bucket_list_ = getelementptr inbounds %"class.std::__2::__hash_table.18", %"class.std::__2::__hash_table.18"* %this1, i32 0, i32 0
  %call2 = call %"class.std::__2::unique_ptr.19"* @_ZNSt3__210unique_ptrIA_PNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS0_IN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISK_EEEEED2Ev(%"class.std::__2::unique_ptr.19"* %__bucket_list_) #8
  ret %"class.std::__2::__hash_table.18"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEEENS_22__unordered_map_hasherIS7_SE_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SE_NS_8equal_toIS7_EELb1EEENS5_ISE_EEE17__deallocate_nodeEPNS_16__hash_node_baseIPNS_11__hash_nodeISE_PvEEEE(%"class.std::__2::__hash_table.18"* %this, %"struct.std::__2::__hash_node_base.22"* %__np) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__hash_table.18"*, align 4
  %__np.addr = alloca %"struct.std::__2::__hash_node_base.22"*, align 4
  %__na = alloca %"class.std::__2::allocator.32"*, align 4
  %__next = alloca %"struct.std::__2::__hash_node_base.22"*, align 4
  %__real_np = alloca %"struct.std::__2::__hash_node"*, align 4
  store %"class.std::__2::__hash_table.18"* %this, %"class.std::__2::__hash_table.18"** %this.addr, align 4
  store %"struct.std::__2::__hash_node_base.22"* %__np, %"struct.std::__2::__hash_node_base.22"** %__np.addr, align 4
  %this1 = load %"class.std::__2::__hash_table.18"*, %"class.std::__2::__hash_table.18"** %this.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.32"* @_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEEENS_22__unordered_map_hasherIS7_SE_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SE_NS_8equal_toIS7_EELb1EEENS5_ISE_EEE12__node_allocEv(%"class.std::__2::__hash_table.18"* %this1) #8
  store %"class.std::__2::allocator.32"* %call, %"class.std::__2::allocator.32"** %__na, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %0 = load %"struct.std::__2::__hash_node_base.22"*, %"struct.std::__2::__hash_node_base.22"** %__np.addr, align 4
  %cmp = icmp ne %"struct.std::__2::__hash_node_base.22"* %0, null
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %1 = load %"struct.std::__2::__hash_node_base.22"*, %"struct.std::__2::__hash_node_base.22"** %__np.addr, align 4
  %__next_ = getelementptr inbounds %"struct.std::__2::__hash_node_base.22", %"struct.std::__2::__hash_node_base.22"* %1, i32 0, i32 0
  %2 = load %"struct.std::__2::__hash_node_base.22"*, %"struct.std::__2::__hash_node_base.22"** %__next_, align 4
  store %"struct.std::__2::__hash_node_base.22"* %2, %"struct.std::__2::__hash_node_base.22"** %__next, align 4
  %3 = load %"struct.std::__2::__hash_node_base.22"*, %"struct.std::__2::__hash_node_base.22"** %__np.addr, align 4
  %call2 = call %"struct.std::__2::__hash_node"* @_ZNSt3__216__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEE8__upcastEv(%"struct.std::__2::__hash_node_base.22"* %3) #8
  store %"struct.std::__2::__hash_node"* %call2, %"struct.std::__2::__hash_node"** %__real_np, align 4
  %4 = load %"class.std::__2::allocator.32"*, %"class.std::__2::allocator.32"** %__na, align 4
  %5 = load %"struct.std::__2::__hash_node"*, %"struct.std::__2::__hash_node"** %__real_np, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__hash_node", %"struct.std::__2::__hash_node"* %5, i32 0, i32 2
  %call3 = call %"struct.std::__2::pair"* @_ZNSt3__222__hash_key_value_typesINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEEEE9__get_ptrERSE_(%"struct.std::__2::__hash_value_type"* nonnull align 4 dereferenceable(16) %__value_)
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEEE7destroyINS_4pairIKS8_SE_EEEEvRSI_PT_(%"class.std::__2::allocator.32"* nonnull align 1 dereferenceable(1) %4, %"struct.std::__2::pair"* %call3)
  %6 = load %"class.std::__2::allocator.32"*, %"class.std::__2::allocator.32"** %__na, align 4
  %7 = load %"struct.std::__2::__hash_node"*, %"struct.std::__2::__hash_node"** %__real_np, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEEE10deallocateERSI_PSH_m(%"class.std::__2::allocator.32"* nonnull align 1 dereferenceable(1) %6, %"struct.std::__2::__hash_node"* %7, i32 1) #8
  %8 = load %"struct.std::__2::__hash_node_base.22"*, %"struct.std::__2::__hash_node_base.22"** %__next, align 4
  store %"struct.std::__2::__hash_node_base.22"* %8, %"struct.std::__2::__hash_node_base.22"** %__np.addr, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base.22"* @_ZNSt3__217__compressed_pairINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEENS7_ISI_EEE5firstEv(%"class.std::__2::__compressed_pair.29"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.29"*, align 4
  store %"class.std::__2::__compressed_pair.29"* %this, %"class.std::__2::__compressed_pair.29"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.29"*, %"class.std::__2::__compressed_pair.29"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.29"* %this1 to %"struct.std::__2::__compressed_pair_elem.30"*
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base.22"* @_ZNSt3__222__compressed_pair_elemINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.30"* %0) #8
  ret %"struct.std::__2::__hash_node_base.22"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.19"* @_ZNSt3__210unique_ptrIA_PNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS0_IN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISK_EEEEED2Ev(%"class.std::__2::unique_ptr.19"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.19"*, align 4
  store %"class.std::__2::unique_ptr.19"* %this, %"class.std::__2::unique_ptr.19"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.19"*, %"class.std::__2::unique_ptr.19"** %this.addr, align 4
  call void @_ZNSt3__210unique_ptrIA_PNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS0_IN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISK_EEEEE5resetEDn(%"class.std::__2::unique_ptr.19"* %this1, i8* null) #8
  ret %"class.std::__2::unique_ptr.19"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.32"* @_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEEENS_22__unordered_map_hasherIS7_SE_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SE_NS_8equal_toIS7_EELb1EEENS5_ISE_EEE12__node_allocEv(%"class.std::__2::__hash_table.18"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__hash_table.18"*, align 4
  store %"class.std::__2::__hash_table.18"* %this, %"class.std::__2::__hash_table.18"** %this.addr, align 4
  %this1 = load %"class.std::__2::__hash_table.18"*, %"class.std::__2::__hash_table.18"** %this.addr, align 4
  %__p1_ = getelementptr inbounds %"class.std::__2::__hash_table.18", %"class.std::__2::__hash_table.18"* %this1, i32 0, i32 1
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.32"* @_ZNSt3__217__compressed_pairINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEENS7_ISI_EEE6secondEv(%"class.std::__2::__compressed_pair.29"* %__p1_) #8
  ret %"class.std::__2::allocator.32"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__hash_node"* @_ZNSt3__216__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEE8__upcastEv(%"struct.std::__2::__hash_node_base.22"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__hash_node_base.22"*, align 4
  store %"struct.std::__2::__hash_node_base.22"* %this, %"struct.std::__2::__hash_node_base.22"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__hash_node_base.22"*, %"struct.std::__2::__hash_node_base.22"** %this.addr, align 4
  %call = call %"struct.std::__2::__hash_node_base.22"* @_ZNSt3__214pointer_traitsIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEE10pointer_toERSK_(%"struct.std::__2::__hash_node_base.22"* nonnull align 4 dereferenceable(4) %this1) #8
  %0 = bitcast %"struct.std::__2::__hash_node_base.22"* %call to %"struct.std::__2::__hash_node"*
  ret %"struct.std::__2::__hash_node"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEEE7destroyINS_4pairIKS8_SE_EEEEvRSI_PT_(%"class.std::__2::allocator.32"* nonnull align 1 dereferenceable(1) %__a, %"struct.std::__2::pair"* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.32"*, align 4
  %__p.addr = alloca %"struct.std::__2::pair"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant.126", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy.127", align 1
  store %"class.std::__2::allocator.32"* %__a, %"class.std::__2::allocator.32"** %__a.addr, align 4
  store %"struct.std::__2::pair"* %__p, %"struct.std::__2::pair"** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy.127"* %ref.tmp to %"struct.std::__2::integral_constant.126"*
  %1 = load %"class.std::__2::allocator.32"*, %"class.std::__2::allocator.32"** %__a.addr, align 4
  %2 = load %"struct.std::__2::pair"*, %"struct.std::__2::pair"** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEEE9__destroyINS_4pairIKS8_SE_EEEEvNS_17integral_constantIbLb0EEERSI_PT_(%"class.std::__2::allocator.32"* nonnull align 1 dereferenceable(1) %1, %"struct.std::__2::pair"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::pair"* @_ZNSt3__222__hash_key_value_typesINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEEEE9__get_ptrERSE_(%"struct.std::__2::__hash_value_type"* nonnull align 4 dereferenceable(16) %__n) #0 comdat {
entry:
  %__n.addr = alloca %"struct.std::__2::__hash_value_type"*, align 4
  store %"struct.std::__2::__hash_value_type"* %__n, %"struct.std::__2::__hash_value_type"** %__n.addr, align 4
  %0 = load %"struct.std::__2::__hash_value_type"*, %"struct.std::__2::__hash_value_type"** %__n.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %"struct.std::__2::pair"* @_ZNSt3__217__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteIS9_EEEEE11__get_valueEv(%"struct.std::__2::__hash_value_type"* %0)
  %call1 = call %"struct.std::__2::pair"* @_ZNSt3__29addressofINS_4pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEEEPT_RSG_(%"struct.std::__2::pair"* nonnull align 4 dereferenceable(16) %call) #8
  ret %"struct.std::__2::pair"* %call1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEEE10deallocateERSI_PSH_m(%"class.std::__2::allocator.32"* nonnull align 1 dereferenceable(1) %__a, %"struct.std::__2::__hash_node"* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.32"*, align 4
  %__p.addr = alloca %"struct.std::__2::__hash_node"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.32"* %__a, %"class.std::__2::allocator.32"** %__a.addr, align 4
  store %"struct.std::__2::__hash_node"* %__p, %"struct.std::__2::__hash_node"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.32"*, %"class.std::__2::allocator.32"** %__a.addr, align 4
  %1 = load %"struct.std::__2::__hash_node"*, %"struct.std::__2::__hash_node"** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEEEPvEEE10deallocateEPSG_m(%"class.std::__2::allocator.32"* %0, %"struct.std::__2::__hash_node"* %1, i32 %2) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.32"* @_ZNSt3__217__compressed_pairINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEENS7_ISI_EEE6secondEv(%"class.std::__2::__compressed_pair.29"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.29"*, align 4
  store %"class.std::__2::__compressed_pair.29"* %this, %"class.std::__2::__compressed_pair.29"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.29"*, %"class.std::__2::__compressed_pair.29"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.29"* %this1 to %"struct.std::__2::__compressed_pair_elem.31"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.32"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.31"* %0) #8
  ret %"class.std::__2::allocator.32"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.32"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.31"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.31"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.31"* %this, %"struct.std::__2::__compressed_pair_elem.31"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.31"*, %"struct.std::__2::__compressed_pair_elem.31"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.31"* %this1 to %"class.std::__2::allocator.32"*
  ret %"class.std::__2::allocator.32"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__hash_node_base.22"* @_ZNSt3__214pointer_traitsIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEE10pointer_toERSK_(%"struct.std::__2::__hash_node_base.22"* nonnull align 4 dereferenceable(4) %__r) #0 comdat {
entry:
  %__r.addr = alloca %"struct.std::__2::__hash_node_base.22"*, align 4
  store %"struct.std::__2::__hash_node_base.22"* %__r, %"struct.std::__2::__hash_node_base.22"** %__r.addr, align 4
  %0 = load %"struct.std::__2::__hash_node_base.22"*, %"struct.std::__2::__hash_node_base.22"** %__r.addr, align 4
  %call = call %"struct.std::__2::__hash_node_base.22"* @_ZNSt3__29addressofINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEPT_RSL_(%"struct.std::__2::__hash_node_base.22"* nonnull align 4 dereferenceable(4) %0) #8
  ret %"struct.std::__2::__hash_node_base.22"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__hash_node_base.22"* @_ZNSt3__29addressofINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEPT_RSL_(%"struct.std::__2::__hash_node_base.22"* nonnull align 4 dereferenceable(4) %__x) #0 comdat {
entry:
  %__x.addr = alloca %"struct.std::__2::__hash_node_base.22"*, align 4
  store %"struct.std::__2::__hash_node_base.22"* %__x, %"struct.std::__2::__hash_node_base.22"** %__x.addr, align 4
  %0 = load %"struct.std::__2::__hash_node_base.22"*, %"struct.std::__2::__hash_node_base.22"** %__x.addr, align 4
  ret %"struct.std::__2::__hash_node_base.22"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEEE9__destroyINS_4pairIKS8_SE_EEEEvNS_17integral_constantIbLb0EEERSI_PT_(%"class.std::__2::allocator.32"* nonnull align 1 dereferenceable(1) %0, %"struct.std::__2::pair"* %__p) #0 comdat {
entry:
  %1 = alloca %"struct.std::__2::integral_constant.126", align 1
  %.addr = alloca %"class.std::__2::allocator.32"*, align 4
  %__p.addr = alloca %"struct.std::__2::pair"*, align 4
  store %"class.std::__2::allocator.32"* %0, %"class.std::__2::allocator.32"** %.addr, align 4
  store %"struct.std::__2::pair"* %__p, %"struct.std::__2::pair"** %__p.addr, align 4
  %2 = load %"struct.std::__2::pair"*, %"struct.std::__2::pair"** %__p.addr, align 4
  %call = call %"struct.std::__2::pair"* @_ZNSt3__24pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEED2Ev(%"struct.std::__2::pair"* %2) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::pair"* @_ZNSt3__24pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEED2Ev(%"struct.std::__2::pair"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::pair"*, align 4
  store %"struct.std::__2::pair"* %this, %"struct.std::__2::pair"** %this.addr, align 4
  %this1 = load %"struct.std::__2::pair"*, %"struct.std::__2::pair"** %this.addr, align 4
  %second = getelementptr inbounds %"struct.std::__2::pair", %"struct.std::__2::pair"* %this1, i32 0, i32 1
  %call = call %"class.std::__2::unique_ptr.121"* @_ZNSt3__210unique_ptrIN5draco8MetadataENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.121"* %second) #8
  %first = getelementptr inbounds %"struct.std::__2::pair", %"struct.std::__2::pair"* %this1, i32 0, i32 0
  %call2 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_string"* %first) #8
  ret %"struct.std::__2::pair"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.121"* @_ZNSt3__210unique_ptrIN5draco8MetadataENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.121"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.121"*, align 4
  store %"class.std::__2::unique_ptr.121"* %this, %"class.std::__2::unique_ptr.121"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.121"*, %"class.std::__2::unique_ptr.121"** %this.addr, align 4
  call void @_ZNSt3__210unique_ptrIN5draco8MetadataENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.121"* %this1, %"class.draco::Metadata"* null) #8
  ret %"class.std::__2::unique_ptr.121"* %this1
}

; Function Attrs: nounwind
declare %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_string"* returned) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__210unique_ptrIN5draco8MetadataENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.121"* %this, %"class.draco::Metadata"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.121"*, align 4
  %__p.addr = alloca %"class.draco::Metadata"*, align 4
  %__tmp = alloca %"class.draco::Metadata"*, align 4
  store %"class.std::__2::unique_ptr.121"* %this, %"class.std::__2::unique_ptr.121"** %this.addr, align 4
  store %"class.draco::Metadata"* %__p, %"class.draco::Metadata"** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.121"*, %"class.std::__2::unique_ptr.121"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.121", %"class.std::__2::unique_ptr.121"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::Metadata"** @_ZNSt3__217__compressed_pairIPN5draco8MetadataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.122"* %__ptr_) #8
  %0 = load %"class.draco::Metadata"*, %"class.draco::Metadata"** %call, align 4
  store %"class.draco::Metadata"* %0, %"class.draco::Metadata"** %__tmp, align 4
  %1 = load %"class.draco::Metadata"*, %"class.draco::Metadata"** %__p.addr, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr.121", %"class.std::__2::unique_ptr.121"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::Metadata"** @_ZNSt3__217__compressed_pairIPN5draco8MetadataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.122"* %__ptr_2) #8
  store %"class.draco::Metadata"* %1, %"class.draco::Metadata"** %call3, align 4
  %2 = load %"class.draco::Metadata"*, %"class.draco::Metadata"** %__tmp, align 4
  %tobool = icmp ne %"class.draco::Metadata"* %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__ptr_4 = getelementptr inbounds %"class.std::__2::unique_ptr.121", %"class.std::__2::unique_ptr.121"* %this1, i32 0, i32 0
  %call5 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.125"* @_ZNSt3__217__compressed_pairIPN5draco8MetadataENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.122"* %__ptr_4) #8
  %3 = load %"class.draco::Metadata"*, %"class.draco::Metadata"** %__tmp, align 4
  call void @_ZNKSt3__214default_deleteIN5draco8MetadataEEclEPS2_(%"struct.std::__2::default_delete.125"* %call5, %"class.draco::Metadata"* %3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::Metadata"** @_ZNSt3__217__compressed_pairIPN5draco8MetadataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.122"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.122"*, align 4
  store %"class.std::__2::__compressed_pair.122"* %this, %"class.std::__2::__compressed_pair.122"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.122"*, %"class.std::__2::__compressed_pair.122"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.122"* %this1 to %"struct.std::__2::__compressed_pair_elem.123"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::Metadata"** @_ZNSt3__222__compressed_pair_elemIPN5draco8MetadataELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.123"* %0) #8
  ret %"class.draco::Metadata"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.125"* @_ZNSt3__217__compressed_pairIPN5draco8MetadataENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.122"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.122"*, align 4
  store %"class.std::__2::__compressed_pair.122"* %this, %"class.std::__2::__compressed_pair.122"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.122"*, %"class.std::__2::__compressed_pair.122"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.122"* %this1 to %"struct.std::__2::__compressed_pair_elem.124"*
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.125"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco8MetadataEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.124"* %0) #8
  ret %"struct.std::__2::default_delete.125"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__214default_deleteIN5draco8MetadataEEclEPS2_(%"struct.std::__2::default_delete.125"* %this, %"class.draco::Metadata"* %__ptr) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::default_delete.125"*, align 4
  %__ptr.addr = alloca %"class.draco::Metadata"*, align 4
  store %"struct.std::__2::default_delete.125"* %this, %"struct.std::__2::default_delete.125"** %this.addr, align 4
  store %"class.draco::Metadata"* %__ptr, %"class.draco::Metadata"** %__ptr.addr, align 4
  %this1 = load %"struct.std::__2::default_delete.125"*, %"struct.std::__2::default_delete.125"** %this.addr, align 4
  %0 = load %"class.draco::Metadata"*, %"class.draco::Metadata"** %__ptr.addr, align 4
  %isnull = icmp eq %"class.draco::Metadata"* %0, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %entry
  %call = call %"class.draco::Metadata"* @_ZN5draco8MetadataD2Ev(%"class.draco::Metadata"* %0) #8
  %1 = bitcast %"class.draco::Metadata"* %0 to i8*
  call void @_ZdlPv(i8* %1) #9
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::Metadata"** @_ZNSt3__222__compressed_pair_elemIPN5draco8MetadataELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.123"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.123"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.123"* %this, %"struct.std::__2::__compressed_pair_elem.123"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.123"*, %"struct.std::__2::__compressed_pair_elem.123"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.123", %"struct.std::__2::__compressed_pair_elem.123"* %this1, i32 0, i32 0
  ret %"class.draco::Metadata"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.125"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco8MetadataEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.124"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.124"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.124"* %this, %"struct.std::__2::__compressed_pair_elem.124"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.124"*, %"struct.std::__2::__compressed_pair_elem.124"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.124"* %this1 to %"struct.std::__2::default_delete.125"*
  ret %"struct.std::__2::default_delete.125"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::Metadata"* @_ZN5draco8MetadataD2Ev(%"class.draco::Metadata"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::Metadata"*, align 4
  store %"class.draco::Metadata"* %this, %"class.draco::Metadata"** %this.addr, align 4
  %this1 = load %"class.draco::Metadata"*, %"class.draco::Metadata"** %this.addr, align 4
  %sub_metadatas_ = getelementptr inbounds %"class.draco::Metadata", %"class.draco::Metadata"* %this1, i32 0, i32 1
  %call = call %"class.std::__2::unordered_map.17"* @_ZNSt3__213unordered_mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteIS9_EEEENS_4hashIS6_EENS_8equal_toIS6_EENS4_INS_4pairIKS6_SC_EEEEED2Ev(%"class.std::__2::unordered_map.17"* %sub_metadatas_) #8
  %entries_ = getelementptr inbounds %"class.draco::Metadata", %"class.draco::Metadata"* %this1, i32 0, i32 0
  %call2 = call %"class.std::__2::unordered_map"* @_ZNSt3__213unordered_mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueENS_4hashIS6_EENS_8equal_toIS6_EENS4_INS_4pairIKS6_S8_EEEEED2Ev(%"class.std::__2::unordered_map"* %entries_) #8
  ret %"class.draco::Metadata"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::pair"* @_ZNSt3__29addressofINS_4pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEEEPT_RSG_(%"struct.std::__2::pair"* nonnull align 4 dereferenceable(16) %__x) #0 comdat {
entry:
  %__x.addr = alloca %"struct.std::__2::pair"*, align 4
  store %"struct.std::__2::pair"* %__x, %"struct.std::__2::pair"** %__x.addr, align 4
  %0 = load %"struct.std::__2::pair"*, %"struct.std::__2::pair"** %__x.addr, align 4
  ret %"struct.std::__2::pair"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %"struct.std::__2::pair"* @_ZNSt3__217__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteIS9_EEEEE11__get_valueEv(%"struct.std::__2::__hash_value_type"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__hash_value_type"*, align 4
  store %"struct.std::__2::__hash_value_type"* %this, %"struct.std::__2::__hash_value_type"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__hash_value_type"*, %"struct.std::__2::__hash_value_type"** %this.addr, align 4
  %__cc = getelementptr inbounds %"struct.std::__2::__hash_value_type", %"struct.std::__2::__hash_value_type"* %this1, i32 0, i32 0
  ret %"struct.std::__2::pair"* %__cc
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEEEPvEEE10deallocateEPSG_m(%"class.std::__2::allocator.32"* %this, %"struct.std::__2::__hash_node"* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.32"*, align 4
  %__p.addr = alloca %"struct.std::__2::__hash_node"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.32"* %this, %"class.std::__2::allocator.32"** %this.addr, align 4
  store %"struct.std::__2::__hash_node"* %__p, %"struct.std::__2::__hash_node"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.32"*, %"class.std::__2::allocator.32"** %this.addr, align 4
  %0 = load %"struct.std::__2::__hash_node"*, %"struct.std::__2::__hash_node"** %__p.addr, align 4
  %1 = bitcast %"struct.std::__2::__hash_node"* %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 24
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base.22"* @_ZNSt3__222__compressed_pair_elemINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.30"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.30"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.30"* %this, %"struct.std::__2::__compressed_pair_elem.30"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.30"*, %"struct.std::__2::__compressed_pair_elem.30"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.30", %"struct.std::__2::__compressed_pair_elem.30"* %this1, i32 0, i32 0
  ret %"struct.std::__2::__hash_node_base.22"* %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__210unique_ptrIA_PNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS0_IN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISK_EEEEE5resetEDn(%"class.std::__2::unique_ptr.19"* %this, i8* %0) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.19"*, align 4
  %.addr = alloca i8*, align 4
  %__tmp = alloca %"struct.std::__2::__hash_node_base.22"**, align 4
  store %"class.std::__2::unique_ptr.19"* %this, %"class.std::__2::unique_ptr.19"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.19"*, %"class.std::__2::unique_ptr.19"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.19", %"class.std::__2::unique_ptr.19"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base.22"*** @_ZNSt3__217__compressed_pairIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISL_EEEEE5firstEv(%"class.std::__2::__compressed_pair.20"* %__ptr_) #8
  %1 = load %"struct.std::__2::__hash_node_base.22"**, %"struct.std::__2::__hash_node_base.22"*** %call, align 4
  store %"struct.std::__2::__hash_node_base.22"** %1, %"struct.std::__2::__hash_node_base.22"*** %__tmp, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr.19", %"class.std::__2::unique_ptr.19"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base.22"*** @_ZNSt3__217__compressed_pairIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISL_EEEEE5firstEv(%"class.std::__2::__compressed_pair.20"* %__ptr_2) #8
  store %"struct.std::__2::__hash_node_base.22"** null, %"struct.std::__2::__hash_node_base.22"*** %call3, align 4
  %2 = load %"struct.std::__2::__hash_node_base.22"**, %"struct.std::__2::__hash_node_base.22"*** %__tmp, align 4
  %tobool = icmp ne %"struct.std::__2::__hash_node_base.22"** %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__ptr_4 = getelementptr inbounds %"class.std::__2::unique_ptr.19", %"class.std::__2::unique_ptr.19"* %this1, i32 0, i32 0
  %call5 = call nonnull align 4 dereferenceable(4) %"class.std::__2::__bucket_list_deallocator.24"* @_ZNSt3__217__compressed_pairIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISL_EEEEE6secondEv(%"class.std::__2::__compressed_pair.20"* %__ptr_4) #8
  %3 = load %"struct.std::__2::__hash_node_base.22"**, %"struct.std::__2::__hash_node_base.22"*** %__tmp, align 4
  call void @_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEEclEPSL_(%"class.std::__2::__bucket_list_deallocator.24"* %call5, %"struct.std::__2::__hash_node_base.22"** %3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base.22"*** @_ZNSt3__217__compressed_pairIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISL_EEEEE5firstEv(%"class.std::__2::__compressed_pair.20"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.20"*, align 4
  store %"class.std::__2::__compressed_pair.20"* %this, %"class.std::__2::__compressed_pair.20"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.20"*, %"class.std::__2::__compressed_pair.20"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.20"* %this1 to %"struct.std::__2::__compressed_pair_elem.21"*
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base.22"*** @_ZNSt3__222__compressed_pair_elemIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.21"* %0) #8
  ret %"struct.std::__2::__hash_node_base.22"*** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__bucket_list_deallocator.24"* @_ZNSt3__217__compressed_pairIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISL_EEEEE6secondEv(%"class.std::__2::__compressed_pair.20"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.20"*, align 4
  store %"class.std::__2::__compressed_pair.20"* %this, %"class.std::__2::__compressed_pair.20"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.20"*, %"class.std::__2::__compressed_pair.20"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.20"* %this1 to i8*
  %add.ptr = getelementptr inbounds i8, i8* %0, i32 4
  %1 = bitcast i8* %add.ptr to %"struct.std::__2::__compressed_pair_elem.23"*
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::__bucket_list_deallocator.24"* @_ZNSt3__222__compressed_pair_elemINS_25__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS2_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISD_EEEEEEPvEEEEEEEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.23"* %1) #8
  ret %"class.std::__2::__bucket_list_deallocator.24"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEEclEPSL_(%"class.std::__2::__bucket_list_deallocator.24"* %this, %"struct.std::__2::__hash_node_base.22"** %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__bucket_list_deallocator.24"*, align 4
  %__p.addr = alloca %"struct.std::__2::__hash_node_base.22"**, align 4
  store %"class.std::__2::__bucket_list_deallocator.24"* %this, %"class.std::__2::__bucket_list_deallocator.24"** %this.addr, align 4
  store %"struct.std::__2::__hash_node_base.22"** %__p, %"struct.std::__2::__hash_node_base.22"*** %__p.addr, align 4
  %this1 = load %"class.std::__2::__bucket_list_deallocator.24"*, %"class.std::__2::__bucket_list_deallocator.24"** %this.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.27"* @_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEE7__allocEv(%"class.std::__2::__bucket_list_deallocator.24"* %this1) #8
  %0 = load %"struct.std::__2::__hash_node_base.22"**, %"struct.std::__2::__hash_node_base.22"*** %__p.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEE4sizeEv(%"class.std::__2::__bucket_list_deallocator.24"* %this1) #8
  %1 = load i32, i32* %call2, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEE10deallocateERSM_PSL_m(%"class.std::__2::allocator.27"* nonnull align 1 dereferenceable(1) %call, %"struct.std::__2::__hash_node_base.22"** %0, i32 %1) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base.22"*** @_ZNSt3__222__compressed_pair_elemIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.21"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.21"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.21"* %this, %"struct.std::__2::__compressed_pair_elem.21"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.21"*, %"struct.std::__2::__compressed_pair_elem.21"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.21", %"struct.std::__2::__compressed_pair_elem.21"* %this1, i32 0, i32 0
  ret %"struct.std::__2::__hash_node_base.22"*** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__bucket_list_deallocator.24"* @_ZNSt3__222__compressed_pair_elemINS_25__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS2_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISD_EEEEEEPvEEEEEEEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.23"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.23"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.23"* %this, %"struct.std::__2::__compressed_pair_elem.23"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.23"*, %"struct.std::__2::__compressed_pair_elem.23"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.23", %"struct.std::__2::__compressed_pair_elem.23"* %this1, i32 0, i32 0
  ret %"class.std::__2::__bucket_list_deallocator.24"* %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEE10deallocateERSM_PSL_m(%"class.std::__2::allocator.27"* nonnull align 1 dereferenceable(1) %__a, %"struct.std::__2::__hash_node_base.22"** %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.27"*, align 4
  %__p.addr = alloca %"struct.std::__2::__hash_node_base.22"**, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.27"* %__a, %"class.std::__2::allocator.27"** %__a.addr, align 4
  store %"struct.std::__2::__hash_node_base.22"** %__p, %"struct.std::__2::__hash_node_base.22"*** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.27"*, %"class.std::__2::allocator.27"** %__a.addr, align 4
  %1 = load %"struct.std::__2::__hash_node_base.22"**, %"struct.std::__2::__hash_node_base.22"*** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEEE10deallocateEPSK_m(%"class.std::__2::allocator.27"* %0, %"struct.std::__2::__hash_node_base.22"** %1, i32 %2) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.27"* @_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEE7__allocEv(%"class.std::__2::__bucket_list_deallocator.24"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__bucket_list_deallocator.24"*, align 4
  store %"class.std::__2::__bucket_list_deallocator.24"* %this, %"class.std::__2::__bucket_list_deallocator.24"** %this.addr, align 4
  %this1 = load %"class.std::__2::__bucket_list_deallocator.24"*, %"class.std::__2::__bucket_list_deallocator.24"** %this.addr, align 4
  %__data_ = getelementptr inbounds %"class.std::__2::__bucket_list_deallocator.24", %"class.std::__2::__bucket_list_deallocator.24"* %this1, i32 0, i32 0
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.27"* @_ZNSt3__217__compressed_pairImNS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEE6secondEv(%"class.std::__2::__compressed_pair.25"* %__data_) #8
  ret %"class.std::__2::allocator.27"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEE4sizeEv(%"class.std::__2::__bucket_list_deallocator.24"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__bucket_list_deallocator.24"*, align 4
  store %"class.std::__2::__bucket_list_deallocator.24"* %this, %"class.std::__2::__bucket_list_deallocator.24"** %this.addr, align 4
  %this1 = load %"class.std::__2::__bucket_list_deallocator.24"*, %"class.std::__2::__bucket_list_deallocator.24"** %this.addr, align 4
  %__data_ = getelementptr inbounds %"class.std::__2::__bucket_list_deallocator.24", %"class.std::__2::__bucket_list_deallocator.24"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__217__compressed_pairImNS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEE5firstEv(%"class.std::__2::__compressed_pair.25"* %__data_) #8
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEEE10deallocateEPSK_m(%"class.std::__2::allocator.27"* %this, %"struct.std::__2::__hash_node_base.22"** %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.27"*, align 4
  %__p.addr = alloca %"struct.std::__2::__hash_node_base.22"**, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.27"* %this, %"class.std::__2::allocator.27"** %this.addr, align 4
  store %"struct.std::__2::__hash_node_base.22"** %__p, %"struct.std::__2::__hash_node_base.22"*** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.27"*, %"class.std::__2::allocator.27"** %this.addr, align 4
  %0 = load %"struct.std::__2::__hash_node_base.22"**, %"struct.std::__2::__hash_node_base.22"*** %__p.addr, align 4
  %1 = bitcast %"struct.std::__2::__hash_node_base.22"** %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.27"* @_ZNSt3__217__compressed_pairImNS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEE6secondEv(%"class.std::__2::__compressed_pair.25"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.25"*, align 4
  store %"class.std::__2::__compressed_pair.25"* %this, %"class.std::__2::__compressed_pair.25"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.25"*, %"class.std::__2::__compressed_pair.25"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.25"* %this1 to %"struct.std::__2::__compressed_pair_elem.26"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.27"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.26"* %0) #8
  ret %"class.std::__2::allocator.27"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.27"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.26"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.26"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.26"* %this, %"struct.std::__2::__compressed_pair_elem.26"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.26"*, %"struct.std::__2::__compressed_pair_elem.26"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.26"* %this1 to %"class.std::__2::allocator.27"*
  ret %"class.std::__2::allocator.27"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__217__compressed_pairImNS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEE5firstEv(%"class.std::__2::__compressed_pair.25"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.25"*, align 4
  store %"class.std::__2::__compressed_pair.25"* %this, %"class.std::__2::__compressed_pair.25"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.25"*, %"class.std::__2::__compressed_pair.25"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.25"* %this1 to %"struct.std::__2::__compressed_pair_elem.5"*
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__222__compressed_pair_elemImLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.5"* %0) #8
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__222__compressed_pair_elemImLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.5"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.5"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.5"* %this, %"struct.std::__2::__compressed_pair_elem.5"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.5"*, %"struct.std::__2::__compressed_pair_elem.5"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.5", %"struct.std::__2::__compressed_pair_elem.5"* %this1, i32 0, i32 0
  ret i32* %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__hash_table"* @_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEENS_22__unordered_map_hasherIS7_SA_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SA_NS_8equal_toIS7_EELb1EEENS5_ISA_EEED2Ev(%"class.std::__2::__hash_table"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__hash_table"*, align 4
  store %"class.std::__2::__hash_table"* %this, %"class.std::__2::__hash_table"** %this.addr, align 4
  %this1 = load %"class.std::__2::__hash_table"*, %"class.std::__2::__hash_table"** %this.addr, align 4
  %__p1_ = getelementptr inbounds %"class.std::__2::__hash_table", %"class.std::__2::__hash_table"* %this1, i32 0, i32 1
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base"* @_ZNSt3__217__compressed_pairINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS7_ISE_EEE5firstEv(%"class.std::__2::__compressed_pair.7"* %__p1_) #8
  %__next_ = getelementptr inbounds %"struct.std::__2::__hash_node_base", %"struct.std::__2::__hash_node_base"* %call, i32 0, i32 0
  %0 = load %"struct.std::__2::__hash_node_base"*, %"struct.std::__2::__hash_node_base"** %__next_, align 4
  call void @_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEENS_22__unordered_map_hasherIS7_SA_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SA_NS_8equal_toIS7_EELb1EEENS5_ISA_EEE17__deallocate_nodeEPNS_16__hash_node_baseIPNS_11__hash_nodeISA_PvEEEE(%"class.std::__2::__hash_table"* %this1, %"struct.std::__2::__hash_node_base"* %0) #8
  %__bucket_list_ = getelementptr inbounds %"class.std::__2::__hash_table", %"class.std::__2::__hash_table"* %this1, i32 0, i32 0
  %call2 = call %"class.std::__2::unique_ptr.0"* @_ZNSt3__210unique_ptrIA_PNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISH_EEEEED2Ev(%"class.std::__2::unique_ptr.0"* %__bucket_list_) #8
  ret %"class.std::__2::__hash_table"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEENS_22__unordered_map_hasherIS7_SA_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SA_NS_8equal_toIS7_EELb1EEENS5_ISA_EEE17__deallocate_nodeEPNS_16__hash_node_baseIPNS_11__hash_nodeISA_PvEEEE(%"class.std::__2::__hash_table"* %this, %"struct.std::__2::__hash_node_base"* %__np) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__hash_table"*, align 4
  %__np.addr = alloca %"struct.std::__2::__hash_node_base"*, align 4
  %__na = alloca %"class.std::__2::allocator.10"*, align 4
  %__next = alloca %"struct.std::__2::__hash_node_base"*, align 4
  %__real_np = alloca %"struct.std::__2::__hash_node.128"*, align 4
  store %"class.std::__2::__hash_table"* %this, %"class.std::__2::__hash_table"** %this.addr, align 4
  store %"struct.std::__2::__hash_node_base"* %__np, %"struct.std::__2::__hash_node_base"** %__np.addr, align 4
  %this1 = load %"class.std::__2::__hash_table"*, %"class.std::__2::__hash_table"** %this.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.10"* @_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEENS_22__unordered_map_hasherIS7_SA_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SA_NS_8equal_toIS7_EELb1EEENS5_ISA_EEE12__node_allocEv(%"class.std::__2::__hash_table"* %this1) #8
  store %"class.std::__2::allocator.10"* %call, %"class.std::__2::allocator.10"** %__na, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %0 = load %"struct.std::__2::__hash_node_base"*, %"struct.std::__2::__hash_node_base"** %__np.addr, align 4
  %cmp = icmp ne %"struct.std::__2::__hash_node_base"* %0, null
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %1 = load %"struct.std::__2::__hash_node_base"*, %"struct.std::__2::__hash_node_base"** %__np.addr, align 4
  %__next_ = getelementptr inbounds %"struct.std::__2::__hash_node_base", %"struct.std::__2::__hash_node_base"* %1, i32 0, i32 0
  %2 = load %"struct.std::__2::__hash_node_base"*, %"struct.std::__2::__hash_node_base"** %__next_, align 4
  store %"struct.std::__2::__hash_node_base"* %2, %"struct.std::__2::__hash_node_base"** %__next, align 4
  %3 = load %"struct.std::__2::__hash_node_base"*, %"struct.std::__2::__hash_node_base"** %__np.addr, align 4
  %call2 = call %"struct.std::__2::__hash_node.128"* @_ZNSt3__216__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEE8__upcastEv(%"struct.std::__2::__hash_node_base"* %3) #8
  store %"struct.std::__2::__hash_node.128"* %call2, %"struct.std::__2::__hash_node.128"** %__real_np, align 4
  %4 = load %"class.std::__2::allocator.10"*, %"class.std::__2::allocator.10"** %__na, align 4
  %5 = load %"struct.std::__2::__hash_node.128"*, %"struct.std::__2::__hash_node.128"** %__real_np, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__hash_node.128", %"struct.std::__2::__hash_node.128"* %5, i32 0, i32 2
  %call3 = call %"struct.std::__2::pair.130"* @_ZNSt3__222__hash_key_value_typesINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEE9__get_ptrERSA_(%"struct.std::__2::__hash_value_type.129"* nonnull align 4 dereferenceable(24) %__value_)
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEE7destroyINS_4pairIKS8_SA_EEEEvRSE_PT_(%"class.std::__2::allocator.10"* nonnull align 1 dereferenceable(1) %4, %"struct.std::__2::pair.130"* %call3)
  %6 = load %"class.std::__2::allocator.10"*, %"class.std::__2::allocator.10"** %__na, align 4
  %7 = load %"struct.std::__2::__hash_node.128"*, %"struct.std::__2::__hash_node.128"** %__real_np, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEE10deallocateERSE_PSD_m(%"class.std::__2::allocator.10"* nonnull align 1 dereferenceable(1) %6, %"struct.std::__2::__hash_node.128"* %7, i32 1) #8
  %8 = load %"struct.std::__2::__hash_node_base"*, %"struct.std::__2::__hash_node_base"** %__next, align 4
  store %"struct.std::__2::__hash_node_base"* %8, %"struct.std::__2::__hash_node_base"** %__np.addr, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base"* @_ZNSt3__217__compressed_pairINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS7_ISE_EEE5firstEv(%"class.std::__2::__compressed_pair.7"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.7"*, align 4
  store %"class.std::__2::__compressed_pair.7"* %this, %"class.std::__2::__compressed_pair.7"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.7"*, %"class.std::__2::__compressed_pair.7"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.7"* %this1 to %"struct.std::__2::__compressed_pair_elem.8"*
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base"* @_ZNSt3__222__compressed_pair_elemINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.8"* %0) #8
  ret %"struct.std::__2::__hash_node_base"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.0"* @_ZNSt3__210unique_ptrIA_PNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISH_EEEEED2Ev(%"class.std::__2::unique_ptr.0"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.0"*, align 4
  store %"class.std::__2::unique_ptr.0"* %this, %"class.std::__2::unique_ptr.0"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.0"*, %"class.std::__2::unique_ptr.0"** %this.addr, align 4
  call void @_ZNSt3__210unique_ptrIA_PNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISH_EEEEE5resetEDn(%"class.std::__2::unique_ptr.0"* %this1, i8* null) #8
  ret %"class.std::__2::unique_ptr.0"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.10"* @_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEENS_22__unordered_map_hasherIS7_SA_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SA_NS_8equal_toIS7_EELb1EEENS5_ISA_EEE12__node_allocEv(%"class.std::__2::__hash_table"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__hash_table"*, align 4
  store %"class.std::__2::__hash_table"* %this, %"class.std::__2::__hash_table"** %this.addr, align 4
  %this1 = load %"class.std::__2::__hash_table"*, %"class.std::__2::__hash_table"** %this.addr, align 4
  %__p1_ = getelementptr inbounds %"class.std::__2::__hash_table", %"class.std::__2::__hash_table"* %this1, i32 0, i32 1
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.10"* @_ZNSt3__217__compressed_pairINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS7_ISE_EEE6secondEv(%"class.std::__2::__compressed_pair.7"* %__p1_) #8
  ret %"class.std::__2::allocator.10"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__hash_node.128"* @_ZNSt3__216__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEE8__upcastEv(%"struct.std::__2::__hash_node_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__hash_node_base"*, align 4
  store %"struct.std::__2::__hash_node_base"* %this, %"struct.std::__2::__hash_node_base"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__hash_node_base"*, %"struct.std::__2::__hash_node_base"** %this.addr, align 4
  %call = call %"struct.std::__2::__hash_node_base"* @_ZNSt3__214pointer_traitsIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEEE10pointer_toERSG_(%"struct.std::__2::__hash_node_base"* nonnull align 4 dereferenceable(4) %this1) #8
  %0 = bitcast %"struct.std::__2::__hash_node_base"* %call to %"struct.std::__2::__hash_node.128"*
  ret %"struct.std::__2::__hash_node.128"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEE7destroyINS_4pairIKS8_SA_EEEEvRSE_PT_(%"class.std::__2::allocator.10"* nonnull align 1 dereferenceable(1) %__a, %"struct.std::__2::pair.130"* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.10"*, align 4
  %__p.addr = alloca %"struct.std::__2::pair.130"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant.126", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy.131", align 1
  store %"class.std::__2::allocator.10"* %__a, %"class.std::__2::allocator.10"** %__a.addr, align 4
  store %"struct.std::__2::pair.130"* %__p, %"struct.std::__2::pair.130"** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy.131"* %ref.tmp to %"struct.std::__2::integral_constant.126"*
  %1 = load %"class.std::__2::allocator.10"*, %"class.std::__2::allocator.10"** %__a.addr, align 4
  %2 = load %"struct.std::__2::pair.130"*, %"struct.std::__2::pair.130"** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEE9__destroyINS_4pairIKS8_SA_EEEEvNS_17integral_constantIbLb0EEERSE_PT_(%"class.std::__2::allocator.10"* nonnull align 1 dereferenceable(1) %1, %"struct.std::__2::pair.130"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::pair.130"* @_ZNSt3__222__hash_key_value_typesINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEE9__get_ptrERSA_(%"struct.std::__2::__hash_value_type.129"* nonnull align 4 dereferenceable(24) %__n) #0 comdat {
entry:
  %__n.addr = alloca %"struct.std::__2::__hash_value_type.129"*, align 4
  store %"struct.std::__2::__hash_value_type.129"* %__n, %"struct.std::__2::__hash_value_type.129"** %__n.addr, align 4
  %0 = load %"struct.std::__2::__hash_value_type.129"*, %"struct.std::__2::__hash_value_type.129"** %__n.addr, align 4
  %call = call nonnull align 4 dereferenceable(24) %"struct.std::__2::pair.130"* @_ZNSt3__217__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEE11__get_valueEv(%"struct.std::__2::__hash_value_type.129"* %0)
  %call1 = call %"struct.std::__2::pair.130"* @_ZNSt3__29addressofINS_4pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEEEPT_RSC_(%"struct.std::__2::pair.130"* nonnull align 4 dereferenceable(24) %call) #8
  ret %"struct.std::__2::pair.130"* %call1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEE10deallocateERSE_PSD_m(%"class.std::__2::allocator.10"* nonnull align 1 dereferenceable(1) %__a, %"struct.std::__2::__hash_node.128"* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.10"*, align 4
  %__p.addr = alloca %"struct.std::__2::__hash_node.128"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.10"* %__a, %"class.std::__2::allocator.10"** %__a.addr, align 4
  store %"struct.std::__2::__hash_node.128"* %__p, %"struct.std::__2::__hash_node.128"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.10"*, %"class.std::__2::allocator.10"** %__a.addr, align 4
  %1 = load %"struct.std::__2::__hash_node.128"*, %"struct.std::__2::__hash_node.128"** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEEN5draco10EntryValueEEEPvEEE10deallocateEPSC_m(%"class.std::__2::allocator.10"* %0, %"struct.std::__2::__hash_node.128"* %1, i32 %2) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.10"* @_ZNSt3__217__compressed_pairINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS7_ISE_EEE6secondEv(%"class.std::__2::__compressed_pair.7"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.7"*, align 4
  store %"class.std::__2::__compressed_pair.7"* %this, %"class.std::__2::__compressed_pair.7"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.7"*, %"class.std::__2::__compressed_pair.7"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.7"* %this1 to %"struct.std::__2::__compressed_pair_elem.9"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.10"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.9"* %0) #8
  ret %"class.std::__2::allocator.10"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.10"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.9"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.9"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.9"* %this, %"struct.std::__2::__compressed_pair_elem.9"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.9"*, %"struct.std::__2::__compressed_pair_elem.9"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.9"* %this1 to %"class.std::__2::allocator.10"*
  ret %"class.std::__2::allocator.10"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__hash_node_base"* @_ZNSt3__214pointer_traitsIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEEE10pointer_toERSG_(%"struct.std::__2::__hash_node_base"* nonnull align 4 dereferenceable(4) %__r) #0 comdat {
entry:
  %__r.addr = alloca %"struct.std::__2::__hash_node_base"*, align 4
  store %"struct.std::__2::__hash_node_base"* %__r, %"struct.std::__2::__hash_node_base"** %__r.addr, align 4
  %0 = load %"struct.std::__2::__hash_node_base"*, %"struct.std::__2::__hash_node_base"** %__r.addr, align 4
  %call = call %"struct.std::__2::__hash_node_base"* @_ZNSt3__29addressofINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEEEEPT_RSH_(%"struct.std::__2::__hash_node_base"* nonnull align 4 dereferenceable(4) %0) #8
  ret %"struct.std::__2::__hash_node_base"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__hash_node_base"* @_ZNSt3__29addressofINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEEEEPT_RSH_(%"struct.std::__2::__hash_node_base"* nonnull align 4 dereferenceable(4) %__x) #0 comdat {
entry:
  %__x.addr = alloca %"struct.std::__2::__hash_node_base"*, align 4
  store %"struct.std::__2::__hash_node_base"* %__x, %"struct.std::__2::__hash_node_base"** %__x.addr, align 4
  %0 = load %"struct.std::__2::__hash_node_base"*, %"struct.std::__2::__hash_node_base"** %__x.addr, align 4
  ret %"struct.std::__2::__hash_node_base"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEE9__destroyINS_4pairIKS8_SA_EEEEvNS_17integral_constantIbLb0EEERSE_PT_(%"class.std::__2::allocator.10"* nonnull align 1 dereferenceable(1) %0, %"struct.std::__2::pair.130"* %__p) #0 comdat {
entry:
  %1 = alloca %"struct.std::__2::integral_constant.126", align 1
  %.addr = alloca %"class.std::__2::allocator.10"*, align 4
  %__p.addr = alloca %"struct.std::__2::pair.130"*, align 4
  store %"class.std::__2::allocator.10"* %0, %"class.std::__2::allocator.10"** %.addr, align 4
  store %"struct.std::__2::pair.130"* %__p, %"struct.std::__2::pair.130"** %__p.addr, align 4
  %2 = load %"struct.std::__2::pair.130"*, %"struct.std::__2::pair.130"** %__p.addr, align 4
  %call = call %"struct.std::__2::pair.130"* @_ZNSt3__24pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEED2Ev(%"struct.std::__2::pair.130"* %2) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::pair.130"* @_ZNSt3__24pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEED2Ev(%"struct.std::__2::pair.130"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::pair.130"*, align 4
  store %"struct.std::__2::pair.130"* %this, %"struct.std::__2::pair.130"** %this.addr, align 4
  %this1 = load %"struct.std::__2::pair.130"*, %"struct.std::__2::pair.130"** %this.addr, align 4
  %second = getelementptr inbounds %"struct.std::__2::pair.130", %"struct.std::__2::pair.130"* %this1, i32 0, i32 1
  %call = call %"class.draco::EntryValue"* @_ZN5draco10EntryValueD2Ev(%"class.draco::EntryValue"* %second) #8
  %first = getelementptr inbounds %"struct.std::__2::pair.130", %"struct.std::__2::pair.130"* %this1, i32 0, i32 0
  %call2 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_string"* %first) #8
  ret %"struct.std::__2::pair.130"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::EntryValue"* @_ZN5draco10EntryValueD2Ev(%"class.draco::EntryValue"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::EntryValue"*, align 4
  store %"class.draco::EntryValue"* %this, %"class.draco::EntryValue"** %this.addr, align 4
  %this1 = load %"class.draco::EntryValue"*, %"class.draco::EntryValue"** %this.addr, align 4
  %data_ = getelementptr inbounds %"class.draco::EntryValue", %"class.draco::EntryValue"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::vector.56"* @_ZNSt3__26vectorIhNS_9allocatorIhEEED2Ev(%"class.std::__2::vector.56"* %data_) #8
  ret %"class.draco::EntryValue"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::pair.130"* @_ZNSt3__29addressofINS_4pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEEEPT_RSC_(%"struct.std::__2::pair.130"* nonnull align 4 dereferenceable(24) %__x) #0 comdat {
entry:
  %__x.addr = alloca %"struct.std::__2::pair.130"*, align 4
  store %"struct.std::__2::pair.130"* %__x, %"struct.std::__2::pair.130"** %__x.addr, align 4
  %0 = load %"struct.std::__2::pair.130"*, %"struct.std::__2::pair.130"** %__x.addr, align 4
  ret %"struct.std::__2::pair.130"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(24) %"struct.std::__2::pair.130"* @_ZNSt3__217__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEE11__get_valueEv(%"struct.std::__2::__hash_value_type.129"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__hash_value_type.129"*, align 4
  store %"struct.std::__2::__hash_value_type.129"* %this, %"struct.std::__2::__hash_value_type.129"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__hash_value_type.129"*, %"struct.std::__2::__hash_value_type.129"** %this.addr, align 4
  %__cc = getelementptr inbounds %"struct.std::__2::__hash_value_type.129", %"struct.std::__2::__hash_value_type.129"* %this1, i32 0, i32 0
  ret %"struct.std::__2::pair.130"* %__cc
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEEN5draco10EntryValueEEEPvEEE10deallocateEPSC_m(%"class.std::__2::allocator.10"* %this, %"struct.std::__2::__hash_node.128"* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.10"*, align 4
  %__p.addr = alloca %"struct.std::__2::__hash_node.128"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.10"* %this, %"class.std::__2::allocator.10"** %this.addr, align 4
  store %"struct.std::__2::__hash_node.128"* %__p, %"struct.std::__2::__hash_node.128"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.10"*, %"class.std::__2::allocator.10"** %this.addr, align 4
  %0 = load %"struct.std::__2::__hash_node.128"*, %"struct.std::__2::__hash_node.128"** %__p.addr, align 4
  %1 = bitcast %"struct.std::__2::__hash_node.128"* %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 32
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base"* @_ZNSt3__222__compressed_pair_elemINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.8"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.8"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.8"* %this, %"struct.std::__2::__compressed_pair_elem.8"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.8"*, %"struct.std::__2::__compressed_pair_elem.8"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.8", %"struct.std::__2::__compressed_pair_elem.8"* %this1, i32 0, i32 0
  ret %"struct.std::__2::__hash_node_base"* %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__210unique_ptrIA_PNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISH_EEEEE5resetEDn(%"class.std::__2::unique_ptr.0"* %this, i8* %0) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.0"*, align 4
  %.addr = alloca i8*, align 4
  %__tmp = alloca %"struct.std::__2::__hash_node_base"**, align 4
  store %"class.std::__2::unique_ptr.0"* %this, %"class.std::__2::unique_ptr.0"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.0"*, %"class.std::__2::unique_ptr.0"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.0", %"class.std::__2::unique_ptr.0"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base"*** @_ZNSt3__217__compressed_pairIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISH_EEEEE5firstEv(%"class.std::__2::__compressed_pair.1"* %__ptr_) #8
  %1 = load %"struct.std::__2::__hash_node_base"**, %"struct.std::__2::__hash_node_base"*** %call, align 4
  store %"struct.std::__2::__hash_node_base"** %1, %"struct.std::__2::__hash_node_base"*** %__tmp, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr.0", %"class.std::__2::unique_ptr.0"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base"*** @_ZNSt3__217__compressed_pairIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISH_EEEEE5firstEv(%"class.std::__2::__compressed_pair.1"* %__ptr_2) #8
  store %"struct.std::__2::__hash_node_base"** null, %"struct.std::__2::__hash_node_base"*** %call3, align 4
  %2 = load %"struct.std::__2::__hash_node_base"**, %"struct.std::__2::__hash_node_base"*** %__tmp, align 4
  %tobool = icmp ne %"struct.std::__2::__hash_node_base"** %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__ptr_4 = getelementptr inbounds %"class.std::__2::unique_ptr.0", %"class.std::__2::unique_ptr.0"* %this1, i32 0, i32 0
  %call5 = call nonnull align 4 dereferenceable(4) %"class.std::__2::__bucket_list_deallocator"* @_ZNSt3__217__compressed_pairIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISH_EEEEE6secondEv(%"class.std::__2::__compressed_pair.1"* %__ptr_4) #8
  %3 = load %"struct.std::__2::__hash_node_base"**, %"struct.std::__2::__hash_node_base"*** %__tmp, align 4
  call void @_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEEclEPSH_(%"class.std::__2::__bucket_list_deallocator"* %call5, %"struct.std::__2::__hash_node_base"** %3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base"*** @_ZNSt3__217__compressed_pairIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISH_EEEEE5firstEv(%"class.std::__2::__compressed_pair.1"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.1"*, align 4
  store %"class.std::__2::__compressed_pair.1"* %this, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.1"*, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.1"* %this1 to %"struct.std::__2::__compressed_pair_elem.2"*
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base"*** @_ZNSt3__222__compressed_pair_elemIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.2"* %0) #8
  ret %"struct.std::__2::__hash_node_base"*** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__bucket_list_deallocator"* @_ZNSt3__217__compressed_pairIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISH_EEEEE6secondEv(%"class.std::__2::__compressed_pair.1"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.1"*, align 4
  store %"class.std::__2::__compressed_pair.1"* %this, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.1"*, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.1"* %this1 to i8*
  %add.ptr = getelementptr inbounds i8, i8* %0, i32 4
  %1 = bitcast i8* %add.ptr to %"struct.std::__2::__compressed_pair_elem.3"*
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::__bucket_list_deallocator"* @_ZNSt3__222__compressed_pair_elemINS_25__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS2_IcEEEEN5draco10EntryValueEEEPvEEEEEEEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.3"* %1) #8
  ret %"class.std::__2::__bucket_list_deallocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEEclEPSH_(%"class.std::__2::__bucket_list_deallocator"* %this, %"struct.std::__2::__hash_node_base"** %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__bucket_list_deallocator"*, align 4
  %__p.addr = alloca %"struct.std::__2::__hash_node_base"**, align 4
  store %"class.std::__2::__bucket_list_deallocator"* %this, %"class.std::__2::__bucket_list_deallocator"** %this.addr, align 4
  store %"struct.std::__2::__hash_node_base"** %__p, %"struct.std::__2::__hash_node_base"*** %__p.addr, align 4
  %this1 = load %"class.std::__2::__bucket_list_deallocator"*, %"class.std::__2::__bucket_list_deallocator"** %this.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEE7__allocEv(%"class.std::__2::__bucket_list_deallocator"* %this1) #8
  %0 = load %"struct.std::__2::__hash_node_base"**, %"struct.std::__2::__hash_node_base"*** %__p.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEE4sizeEv(%"class.std::__2::__bucket_list_deallocator"* %this1) #8
  %1 = load i32, i32* %call2, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEE10deallocateERSI_PSH_m(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call, %"struct.std::__2::__hash_node_base"** %0, i32 %1) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base"*** @_ZNSt3__222__compressed_pair_elemIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.2"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.2"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.2"* %this, %"struct.std::__2::__compressed_pair_elem.2"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.2"*, %"struct.std::__2::__compressed_pair_elem.2"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.2", %"struct.std::__2::__compressed_pair_elem.2"* %this1, i32 0, i32 0
  ret %"struct.std::__2::__hash_node_base"*** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__bucket_list_deallocator"* @_ZNSt3__222__compressed_pair_elemINS_25__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS2_IcEEEEN5draco10EntryValueEEEPvEEEEEEEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.3"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.3"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.3"* %this, %"struct.std::__2::__compressed_pair_elem.3"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.3"*, %"struct.std::__2::__compressed_pair_elem.3"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.3", %"struct.std::__2::__compressed_pair_elem.3"* %this1, i32 0, i32 0
  ret %"class.std::__2::__bucket_list_deallocator"* %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEE10deallocateERSI_PSH_m(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, %"struct.std::__2::__hash_node_base"** %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca %"struct.std::__2::__hash_node_base"**, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store %"struct.std::__2::__hash_node_base"** %__p, %"struct.std::__2::__hash_node_base"*** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %1 = load %"struct.std::__2::__hash_node_base"**, %"struct.std::__2::__hash_node_base"*** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEEN5draco10EntryValueEEEPvEEEEE10deallocateEPSG_m(%"class.std::__2::allocator"* %0, %"struct.std::__2::__hash_node_base"** %1, i32 %2) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEE7__allocEv(%"class.std::__2::__bucket_list_deallocator"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__bucket_list_deallocator"*, align 4
  store %"class.std::__2::__bucket_list_deallocator"* %this, %"class.std::__2::__bucket_list_deallocator"** %this.addr, align 4
  %this1 = load %"class.std::__2::__bucket_list_deallocator"*, %"class.std::__2::__bucket_list_deallocator"** %this.addr, align 4
  %__data_ = getelementptr inbounds %"class.std::__2::__bucket_list_deallocator", %"class.std::__2::__bucket_list_deallocator"* %this1, i32 0, i32 0
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__217__compressed_pairImNS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEE6secondEv(%"class.std::__2::__compressed_pair.4"* %__data_) #8
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEE4sizeEv(%"class.std::__2::__bucket_list_deallocator"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__bucket_list_deallocator"*, align 4
  store %"class.std::__2::__bucket_list_deallocator"* %this, %"class.std::__2::__bucket_list_deallocator"** %this.addr, align 4
  %this1 = load %"class.std::__2::__bucket_list_deallocator"*, %"class.std::__2::__bucket_list_deallocator"** %this.addr, align 4
  %__data_ = getelementptr inbounds %"class.std::__2::__bucket_list_deallocator", %"class.std::__2::__bucket_list_deallocator"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__217__compressed_pairImNS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEE5firstEv(%"class.std::__2::__compressed_pair.4"* %__data_) #8
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEEN5draco10EntryValueEEEPvEEEEE10deallocateEPSG_m(%"class.std::__2::allocator"* %this, %"struct.std::__2::__hash_node_base"** %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca %"struct.std::__2::__hash_node_base"**, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  store %"struct.std::__2::__hash_node_base"** %__p, %"struct.std::__2::__hash_node_base"*** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  %0 = load %"struct.std::__2::__hash_node_base"**, %"struct.std::__2::__hash_node_base"*** %__p.addr, align 4
  %1 = bitcast %"struct.std::__2::__hash_node_base"** %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__217__compressed_pairImNS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEE6secondEv(%"class.std::__2::__compressed_pair.4"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.4"*, align 4
  store %"class.std::__2::__compressed_pair.4"* %this, %"class.std::__2::__compressed_pair.4"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.4"*, %"class.std::__2::__compressed_pair.4"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.4"* %this1 to %"struct.std::__2::__compressed_pair_elem.6"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.6"* %0) #8
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.6"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.6"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.6"* %this, %"struct.std::__2::__compressed_pair_elem.6"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.6"*, %"struct.std::__2::__compressed_pair_elem.6"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.6"* %this1 to %"class.std::__2::allocator"*
  ret %"class.std::__2::allocator"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__217__compressed_pairImNS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEE5firstEv(%"class.std::__2::__compressed_pair.4"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.4"*, align 4
  store %"class.std::__2::__compressed_pair.4"* %this, %"class.std::__2::__compressed_pair.4"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.4"*, %"class.std::__2::__compressed_pair.4"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.4"* %this1 to %"struct.std::__2::__compressed_pair_elem.5"*
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__222__compressed_pair_elemImLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.5"* %0) #8
  ret i32* %call
}

declare void @_ZN5draco10PointCloud12SetAttributeEiNSt3__210unique_ptrINS_14PointAttributeENS1_14default_deleteIS3_EEEE(%"class.draco::PointCloud"*, i32, %"class.std::__2::unique_ptr.53"*) unnamed_addr #1

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.53"* @_ZNSt3__24moveIRNS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOS9_(%"class.std::__2::unique_ptr.53"* nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::unique_ptr.53"*, align 4
  store %"class.std::__2::unique_ptr.53"* %__t, %"class.std::__2::unique_ptr.53"** %__t.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__t.addr, align 4
  ret %"class.std::__2::unique_ptr.53"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.53"* @_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEEC2EOS5_(%"class.std::__2::unique_ptr.53"* returned %this, %"class.std::__2::unique_ptr.53"* nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.53"*, align 4
  %__u.addr = alloca %"class.std::__2::unique_ptr.53"*, align 4
  %ref.tmp = alloca %"class.draco::PointAttribute"*, align 4
  store %"class.std::__2::unique_ptr.53"* %this, %"class.std::__2::unique_ptr.53"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.53"* %__u, %"class.std::__2::unique_ptr.53"** %__u.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.53", %"class.std::__2::unique_ptr.53"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__u.addr, align 4
  %call = call %"class.draco::PointAttribute"* @_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEE7releaseEv(%"class.std::__2::unique_ptr.53"* %0) #8
  store %"class.draco::PointAttribute"* %call, %"class.draco::PointAttribute"** %ref.tmp, align 4
  %1 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__u.addr, align 4
  %call2 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.81"* @_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEE11get_deleterEv(%"class.std::__2::unique_ptr.53"* %1) #8
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.81"* @_ZNSt3__27forwardINS_14default_deleteIN5draco14PointAttributeEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.81"* nonnull align 1 dereferenceable(1) %call2) #8
  %call4 = call %"class.std::__2::__compressed_pair.54"* @_ZNSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEEC2IS3_S5_EEOT_OT0_(%"class.std::__2::__compressed_pair.54"* %__ptr_, %"class.draco::PointAttribute"** nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::default_delete.81"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::unique_ptr.53"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE6resizeEm(%"class.std::__2::vector.94"* %this, i32 %__sz) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  %__sz.addr = alloca i32, align 4
  %__cs = alloca i32, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  store i32 %__sz, i32* %__sz.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE4sizeEv(%"class.std::__2::vector.94"* %this1) #8
  store i32 %call, i32* %__cs, align 4
  %0 = load i32, i32* %__cs, align 4
  %1 = load i32, i32* %__sz.addr, align 4
  %cmp = icmp ult i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %__sz.addr, align 4
  %3 = load i32, i32* %__cs, align 4
  %sub = sub i32 %2, %3
  call void @_ZNSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE8__appendEm(%"class.std::__2::vector.94"* %this1, i32 %sub)
  br label %if.end4

if.else:                                          ; preds = %entry
  %4 = load i32, i32* %__cs, align 4
  %5 = load i32, i32* %__sz.addr, align 4
  %cmp2 = icmp ugt i32 %4, %5
  br i1 %cmp2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.else
  %6 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %6, i32 0, i32 0
  %7 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__begin_, align 4
  %8 = load i32, i32* %__sz.addr, align 4
  %add.ptr = getelementptr inbounds %"struct.draco::Mesh::AttributeData", %"struct.draco::Mesh::AttributeData"* %7, i32 %8
  call void @_ZNSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE17__destruct_at_endEPS3_(%"class.std::__2::vector.94"* %this1, %"struct.draco::Mesh::AttributeData"* %add.ptr) #8
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.else
  br label %if.end4

if.end4:                                          ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::PointAttribute"* @_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEE7releaseEv(%"class.std::__2::unique_ptr.53"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.53"*, align 4
  %__t = alloca %"class.draco::PointAttribute"*, align 4
  store %"class.std::__2::unique_ptr.53"* %this, %"class.std::__2::unique_ptr.53"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.53", %"class.std::__2::unique_ptr.53"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.54"* %__ptr_) #8
  %0 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %call, align 4
  store %"class.draco::PointAttribute"* %0, %"class.draco::PointAttribute"** %__t, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr.53", %"class.std::__2::unique_ptr.53"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.54"* %__ptr_2) #8
  store %"class.draco::PointAttribute"* null, %"class.draco::PointAttribute"** %call3, align 4
  %1 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %__t, align 4
  ret %"class.draco::PointAttribute"* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.81"* @_ZNSt3__27forwardINS_14default_deleteIN5draco14PointAttributeEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.81"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"struct.std::__2::default_delete.81"*, align 4
  store %"struct.std::__2::default_delete.81"* %__t, %"struct.std::__2::default_delete.81"** %__t.addr, align 4
  %0 = load %"struct.std::__2::default_delete.81"*, %"struct.std::__2::default_delete.81"** %__t.addr, align 4
  ret %"struct.std::__2::default_delete.81"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.81"* @_ZNSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEE11get_deleterEv(%"class.std::__2::unique_ptr.53"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.53"*, align 4
  store %"class.std::__2::unique_ptr.53"* %this, %"class.std::__2::unique_ptr.53"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.53", %"class.std::__2::unique_ptr.53"* %this1, i32 0, i32 0
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.81"* @_ZNSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.54"* %__ptr_) #8
  ret %"struct.std::__2::default_delete.81"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.54"* @_ZNSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEEC2IS3_S5_EEOT_OT0_(%"class.std::__2::__compressed_pair.54"* returned %this, %"class.draco::PointAttribute"** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::default_delete.81"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.54"*, align 4
  %__t1.addr = alloca %"class.draco::PointAttribute"**, align 4
  %__t2.addr = alloca %"struct.std::__2::default_delete.81"*, align 4
  store %"class.std::__2::__compressed_pair.54"* %this, %"class.std::__2::__compressed_pair.54"** %this.addr, align 4
  store %"class.draco::PointAttribute"** %__t1, %"class.draco::PointAttribute"*** %__t1.addr, align 4
  store %"struct.std::__2::default_delete.81"* %__t2, %"struct.std::__2::default_delete.81"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.54"*, %"class.std::__2::__compressed_pair.54"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.54"* %this1 to %"struct.std::__2::__compressed_pair_elem.55"*
  %1 = load %"class.draco::PointAttribute"**, %"class.draco::PointAttribute"*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNSt3__27forwardIPN5draco14PointAttributeEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::PointAttribute"** nonnull align 4 dereferenceable(4) %1) #8
  %call2 = call %"struct.std::__2::__compressed_pair_elem.55"* @_ZNSt3__222__compressed_pair_elemIPN5draco14PointAttributeELi0ELb0EEC2IS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.55"* %0, %"class.draco::PointAttribute"** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.54"* %this1 to %"struct.std::__2::__compressed_pair_elem.80"*
  %3 = load %"struct.std::__2::default_delete.81"*, %"struct.std::__2::default_delete.81"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.81"* @_ZNSt3__27forwardINS_14default_deleteIN5draco14PointAttributeEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.81"* nonnull align 1 dereferenceable(1) %3) #8
  %call4 = call %"struct.std::__2::__compressed_pair_elem.80"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco14PointAttributeEEELi1ELb1EEC2IS4_vEEOT_(%"struct.std::__2::__compressed_pair_elem.80"* %2, %"struct.std::__2::default_delete.81"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::__compressed_pair.54"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNSt3__27forwardIPN5draco14PointAttributeEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::PointAttribute"** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.draco::PointAttribute"**, align 4
  store %"class.draco::PointAttribute"** %__t, %"class.draco::PointAttribute"*** %__t.addr, align 4
  %0 = load %"class.draco::PointAttribute"**, %"class.draco::PointAttribute"*** %__t.addr, align 4
  ret %"class.draco::PointAttribute"** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.55"* @_ZNSt3__222__compressed_pair_elemIPN5draco14PointAttributeELi0ELb0EEC2IS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.55"* returned %this, %"class.draco::PointAttribute"** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.55"*, align 4
  %__u.addr = alloca %"class.draco::PointAttribute"**, align 4
  store %"struct.std::__2::__compressed_pair_elem.55"* %this, %"struct.std::__2::__compressed_pair_elem.55"** %this.addr, align 4
  store %"class.draco::PointAttribute"** %__u, %"class.draco::PointAttribute"*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.55"*, %"struct.std::__2::__compressed_pair_elem.55"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.55", %"struct.std::__2::__compressed_pair_elem.55"* %this1, i32 0, i32 0
  %0 = load %"class.draco::PointAttribute"**, %"class.draco::PointAttribute"*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNSt3__27forwardIPN5draco14PointAttributeEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::PointAttribute"** nonnull align 4 dereferenceable(4) %0) #8
  %1 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %call, align 4
  store %"class.draco::PointAttribute"* %1, %"class.draco::PointAttribute"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.55"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.80"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco14PointAttributeEEELi1ELb1EEC2IS4_vEEOT_(%"struct.std::__2::__compressed_pair_elem.80"* returned %this, %"struct.std::__2::default_delete.81"* nonnull align 1 dereferenceable(1) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.80"*, align 4
  %__u.addr = alloca %"struct.std::__2::default_delete.81"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.80"* %this, %"struct.std::__2::__compressed_pair_elem.80"** %this.addr, align 4
  store %"struct.std::__2::default_delete.81"* %__u, %"struct.std::__2::default_delete.81"** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.80"*, %"struct.std::__2::__compressed_pair_elem.80"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.80"* %this1 to %"struct.std::__2::default_delete.81"*
  %1 = load %"struct.std::__2::default_delete.81"*, %"struct.std::__2::default_delete.81"** %__u.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.81"* @_ZNSt3__27forwardINS_14default_deleteIN5draco14PointAttributeEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.81"* nonnull align 1 dereferenceable(1) %1) #8
  ret %"struct.std::__2::__compressed_pair_elem.80"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE8__appendEm(%"class.std::__2::vector.94"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  %__n.addr = alloca i32, align 4
  %__a = alloca %"class.std::__2::allocator.99"*, align 4
  %__v = alloca %"struct.std::__2::__split_buffer", align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %call = call nonnull align 4 dereferenceable(4) %"struct.draco::Mesh::AttributeData"** @_ZNSt3__213__vector_baseIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE9__end_capEv(%"class.std::__2::__vector_base.95"* %0) #8
  %1 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %call, align 4
  %2 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %2, i32 0, i32 1
  %3 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__end_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"struct.draco::Mesh::AttributeData"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"struct.draco::Mesh::AttributeData"* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  %4 = load i32, i32* %__n.addr, align 4
  %cmp = icmp uge i32 %sub.ptr.div, %4
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %5 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE18__construct_at_endEm(%"class.std::__2::vector.94"* %this1, i32 %5)
  br label %if.end

if.else:                                          ; preds = %entry
  %6 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.99"* @_ZNSt3__213__vector_baseIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE7__allocEv(%"class.std::__2::__vector_base.95"* %6) #8
  store %"class.std::__2::allocator.99"* %call2, %"class.std::__2::allocator.99"** %__a, align 4
  %call3 = call i32 @_ZNKSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE4sizeEv(%"class.std::__2::vector.94"* %this1) #8
  %7 = load i32, i32* %__n.addr, align 4
  %add = add i32 %call3, %7
  %call4 = call i32 @_ZNKSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE11__recommendEm(%"class.std::__2::vector.94"* %this1, i32 %add)
  %call5 = call i32 @_ZNKSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE4sizeEv(%"class.std::__2::vector.94"* %this1) #8
  %8 = load %"class.std::__2::allocator.99"*, %"class.std::__2::allocator.99"** %__a, align 4
  %call6 = call %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferIN5draco4Mesh13AttributeDataERNS_9allocatorIS3_EEEC2EmmS6_(%"struct.std::__2::__split_buffer"* %__v, i32 %call4, i32 %call5, %"class.std::__2::allocator.99"* nonnull align 1 dereferenceable(1) %8)
  %9 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__214__split_bufferIN5draco4Mesh13AttributeDataERNS_9allocatorIS3_EEE18__construct_at_endEm(%"struct.std::__2::__split_buffer"* %__v, i32 %9)
  call void @_ZNSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS3_RS5_EE(%"class.std::__2::vector.94"* %this1, %"struct.std::__2::__split_buffer"* nonnull align 4 dereferenceable(20) %__v)
  %call7 = call %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferIN5draco4Mesh13AttributeDataERNS_9allocatorIS3_EEED2Ev(%"struct.std::__2::__split_buffer"* %__v) #8
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE17__destruct_at_endEPS3_(%"class.std::__2::vector.94"* %this, %"struct.draco::Mesh::AttributeData"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  %__new_last.addr = alloca %"struct.draco::Mesh::AttributeData"*, align 4
  %__old_size = alloca i32, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  store %"struct.draco::Mesh::AttributeData"* %__new_last, %"struct.draco::Mesh::AttributeData"** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  %0 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__new_last.addr, align 4
  call void @_ZNSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE27__invalidate_iterators_pastEPS3_(%"class.std::__2::vector.94"* %this1, %"struct.draco::Mesh::AttributeData"* %0)
  %call = call i32 @_ZNKSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE4sizeEv(%"class.std::__2::vector.94"* %this1) #8
  store i32 %call, i32* %__old_size, align 4
  %1 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %2 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__new_last.addr, align 4
  call void @_ZNSt3__213__vector_baseIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE17__destruct_at_endEPS3_(%"class.std::__2::__vector_base.95"* %1, %"struct.draco::Mesh::AttributeData"* %2) #8
  %3 = load i32, i32* %__old_size, align 4
  call void @_ZNKSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE17__annotate_shrinkEm(%"class.std::__2::vector.94"* %this1, i32 %3) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.draco::Mesh::AttributeData"** @_ZNSt3__213__vector_baseIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE9__end_capEv(%"class.std::__2::__vector_base.95"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.95"*, align 4
  store %"class.std::__2::__vector_base.95"* %this, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.95"*, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) %"struct.draco::Mesh::AttributeData"** @_ZNSt3__217__compressed_pairIPN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE5firstEv(%"class.std::__2::__compressed_pair.96"* %__end_cap_) #8
  ret %"struct.draco::Mesh::AttributeData"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE18__construct_at_endEm(%"class.std::__2::vector.94"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  %__n.addr = alloca i32, align 4
  %__tx = alloca %"struct.std::__2::vector<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData>>::_ConstructTransaction", align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %call = call %"struct.std::__2::vector<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData>>::_ConstructTransaction"* @_ZNSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE21_ConstructTransactionC2ERS6_m(%"struct.std::__2::vector<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData>>::_ConstructTransaction"* %__tx, %"class.std::__2::vector.94"* nonnull align 4 dereferenceable(12) %this1, i32 %0)
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData>>::_ConstructTransaction", %"struct.std::__2::vector<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %1 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__pos_, align 4
  %__new_end_ = getelementptr inbounds %"struct.std::__2::vector<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData>>::_ConstructTransaction", %"struct.std::__2::vector<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData>>::_ConstructTransaction"* %__tx, i32 0, i32 2
  %2 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__new_end_, align 4
  %cmp = icmp ne %"struct.draco::Mesh::AttributeData"* %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.99"* @_ZNSt3__213__vector_baseIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE7__allocEv(%"class.std::__2::__vector_base.95"* %3) #8
  %__pos_3 = getelementptr inbounds %"struct.std::__2::vector<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData>>::_ConstructTransaction", %"struct.std::__2::vector<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %4 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__pos_3, align 4
  %call4 = call %"struct.draco::Mesh::AttributeData"* @_ZNSt3__212__to_addressIN5draco4Mesh13AttributeDataEEEPT_S5_(%"struct.draco::Mesh::AttributeData"* %4) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco4Mesh13AttributeDataEEEE9constructIS4_JEEEvRS5_PT_DpOT0_(%"class.std::__2::allocator.99"* nonnull align 1 dereferenceable(1) %call2, %"struct.draco::Mesh::AttributeData"* %call4)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %__pos_5 = getelementptr inbounds %"struct.std::__2::vector<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData>>::_ConstructTransaction", %"struct.std::__2::vector<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %5 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__pos_5, align 4
  %incdec.ptr = getelementptr inbounds %"struct.draco::Mesh::AttributeData", %"struct.draco::Mesh::AttributeData"* %5, i32 1
  store %"struct.draco::Mesh::AttributeData"* %incdec.ptr, %"struct.draco::Mesh::AttributeData"** %__pos_5, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call6 = call %"struct.std::__2::vector<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData>>::_ConstructTransaction"* @_ZNSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData>>::_ConstructTransaction"* %__tx) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE11__recommendEm(%"class.std::__2::vector.94"* %this, i32 %__new_size) #0 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  %__new_size.addr = alloca i32, align 4
  %__ms = alloca i32, align 4
  %__cap = alloca i32, align 4
  %ref.tmp = alloca i32, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  store i32 %__new_size, i32* %__new_size.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE8max_sizeEv(%"class.std::__2::vector.94"* %this1) #8
  store i32 %call, i32* %__ms, align 4
  %0 = load i32, i32* %__new_size.addr, align 4
  %1 = load i32, i32* %__ms, align 4
  %cmp = icmp ugt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base_common"*
  call void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"* %2) #10
  unreachable

if.end:                                           ; preds = %entry
  %call2 = call i32 @_ZNKSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE8capacityEv(%"class.std::__2::vector.94"* %this1) #8
  store i32 %call2, i32* %__cap, align 4
  %3 = load i32, i32* %__cap, align 4
  %4 = load i32, i32* %__ms, align 4
  %div = udiv i32 %4, 2
  %cmp3 = icmp uge i32 %3, %div
  br i1 %cmp3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %if.end
  %5 = load i32, i32* %__ms, align 4
  store i32 %5, i32* %retval, align 4
  br label %return

if.end5:                                          ; preds = %if.end
  %6 = load i32, i32* %__cap, align 4
  %mul = mul i32 2, %6
  store i32 %mul, i32* %ref.tmp, align 4
  %call6 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %__new_size.addr)
  %7 = load i32, i32* %call6, align 4
  store i32 %7, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end5, %if.then4
  %8 = load i32, i32* %retval, align 4
  ret i32 %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferIN5draco4Mesh13AttributeDataERNS_9allocatorIS3_EEEC2EmmS6_(%"struct.std::__2::__split_buffer"* returned %this, i32 %__cap, i32 %__start, %"class.std::__2::allocator.99"* nonnull align 1 dereferenceable(1) %__a) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"struct.std::__2::__split_buffer"*, align 4
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__cap.addr = alloca i32, align 4
  %__start.addr = alloca i32, align 4
  %__a.addr = alloca %"class.std::__2::allocator.99"*, align 4
  %ref.tmp = alloca i8*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store i32 %__cap, i32* %__cap.addr, align 4
  store i32 %__start, i32* %__start.addr, align 4
  store %"class.std::__2::allocator.99"* %__a, %"class.std::__2::allocator.99"** %__a.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer"* %this1, %"struct.std::__2::__split_buffer"** %retval, align 4
  %0 = bitcast %"struct.std::__2::__split_buffer"* %this1 to %"class.std::__2::__split_buffer_common"*
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  store i8* null, i8** %ref.tmp, align 4
  %1 = load %"class.std::__2::allocator.99"*, %"class.std::__2::allocator.99"** %__a.addr, align 4
  %call = call %"class.std::__2::__compressed_pair.132"* @_ZNSt3__217__compressed_pairIPN5draco4Mesh13AttributeDataERNS_9allocatorIS3_EEEC2IDnS7_EEOT_OT0_(%"class.std::__2::__compressed_pair.132"* %__end_cap_, i8** nonnull align 4 dereferenceable(4) %ref.tmp, %"class.std::__2::allocator.99"* nonnull align 1 dereferenceable(1) %1)
  %2 = load i32, i32* %__cap.addr, align 4
  %cmp = icmp ne i32 %2, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.99"* @_ZNSt3__214__split_bufferIN5draco4Mesh13AttributeDataERNS_9allocatorIS3_EEE7__allocEv(%"struct.std::__2::__split_buffer"* %this1) #8
  %3 = load i32, i32* %__cap.addr, align 4
  %call3 = call %"struct.draco::Mesh::AttributeData"* @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco4Mesh13AttributeDataEEEE8allocateERS5_m(%"class.std::__2::allocator.99"* nonnull align 1 dereferenceable(1) %call2, i32 %3)
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %"struct.draco::Mesh::AttributeData"* [ %call3, %cond.true ], [ null, %cond.false ]
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  store %"struct.draco::Mesh::AttributeData"* %cond, %"struct.draco::Mesh::AttributeData"** %__first_, align 4
  %__first_4 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %4 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__first_4, align 4
  %5 = load i32, i32* %__start.addr, align 4
  %add.ptr = getelementptr inbounds %"struct.draco::Mesh::AttributeData", %"struct.draco::Mesh::AttributeData"* %4, i32 %5
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 2
  store %"struct.draco::Mesh::AttributeData"* %add.ptr, %"struct.draco::Mesh::AttributeData"** %__end_, align 4
  %__begin_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 1
  store %"struct.draco::Mesh::AttributeData"* %add.ptr, %"struct.draco::Mesh::AttributeData"** %__begin_, align 4
  %__first_5 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %6 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__first_5, align 4
  %7 = load i32, i32* %__cap.addr, align 4
  %add.ptr6 = getelementptr inbounds %"struct.draco::Mesh::AttributeData", %"struct.draco::Mesh::AttributeData"* %6, i32 %7
  %call7 = call nonnull align 4 dereferenceable(4) %"struct.draco::Mesh::AttributeData"** @_ZNSt3__214__split_bufferIN5draco4Mesh13AttributeDataERNS_9allocatorIS3_EEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this1) #8
  store %"struct.draco::Mesh::AttributeData"* %add.ptr6, %"struct.draco::Mesh::AttributeData"** %call7, align 4
  %8 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %retval, align 4
  ret %"struct.std::__2::__split_buffer"* %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIN5draco4Mesh13AttributeDataERNS_9allocatorIS3_EEE18__construct_at_endEm(%"struct.std::__2::__split_buffer"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__n.addr = alloca i32, align 4
  %__tx = alloca %"struct.std::__2::__split_buffer<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData> &>::_ConstructTransaction", align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 2
  %0 = load i32, i32* %__n.addr, align 4
  %call = call %"struct.std::__2::__split_buffer<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferIN5draco4Mesh13AttributeDataERNS_9allocatorIS3_EEE21_ConstructTransactionC2EPPS3_m(%"struct.std::__2::__split_buffer<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData> &>::_ConstructTransaction"* %__tx, %"struct.draco::Mesh::AttributeData"** %__end_, i32 %0) #8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %__pos_ = getelementptr inbounds %"struct.std::__2::__split_buffer<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData> &>::_ConstructTransaction"* %__tx, i32 0, i32 0
  %1 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__pos_, align 4
  %__end_2 = getelementptr inbounds %"struct.std::__2::__split_buffer<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData> &>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %2 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__end_2, align 4
  %cmp = icmp ne %"struct.draco::Mesh::AttributeData"* %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %call3 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.99"* @_ZNSt3__214__split_bufferIN5draco4Mesh13AttributeDataERNS_9allocatorIS3_EEE7__allocEv(%"struct.std::__2::__split_buffer"* %this1) #8
  %__pos_4 = getelementptr inbounds %"struct.std::__2::__split_buffer<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData> &>::_ConstructTransaction"* %__tx, i32 0, i32 0
  %3 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__pos_4, align 4
  %call5 = call %"struct.draco::Mesh::AttributeData"* @_ZNSt3__212__to_addressIN5draco4Mesh13AttributeDataEEEPT_S5_(%"struct.draco::Mesh::AttributeData"* %3) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco4Mesh13AttributeDataEEEE9constructIS4_JEEEvRS5_PT_DpOT0_(%"class.std::__2::allocator.99"* nonnull align 1 dereferenceable(1) %call3, %"struct.draco::Mesh::AttributeData"* %call5)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %__pos_6 = getelementptr inbounds %"struct.std::__2::__split_buffer<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData> &>::_ConstructTransaction"* %__tx, i32 0, i32 0
  %4 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__pos_6, align 4
  %incdec.ptr = getelementptr inbounds %"struct.draco::Mesh::AttributeData", %"struct.draco::Mesh::AttributeData"* %4, i32 1
  store %"struct.draco::Mesh::AttributeData"* %incdec.ptr, %"struct.draco::Mesh::AttributeData"** %__pos_6, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call7 = call %"struct.std::__2::__split_buffer<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferIN5draco4Mesh13AttributeDataERNS_9allocatorIS3_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::__split_buffer<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData> &>::_ConstructTransaction"* %__tx) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS3_RS5_EE(%"class.std::__2::vector.94"* %this, %"struct.std::__2::__split_buffer"* nonnull align 4 dereferenceable(20) %__v) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  %__v.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer"* %__v, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  call void @_ZNKSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE17__annotate_deleteEv(%"class.std::__2::vector.94"* %this1) #8
  %0 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.99"* @_ZNSt3__213__vector_baseIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE7__allocEv(%"class.std::__2::__vector_base.95"* %0) #8
  %1 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %1, i32 0, i32 0
  %2 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__begin_, align 4
  %3 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %3, i32 0, i32 1
  %4 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__end_, align 4
  %5 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__begin_2 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %5, i32 0, i32 1
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco4Mesh13AttributeDataEEEE46__construct_backward_with_exception_guaranteesIS4_EENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS5_PT_SB_EE5valuesr31is_trivially_move_constructibleISB_EE5valueEvE4typeERS5_SC_SC_RSC_(%"class.std::__2::allocator.99"* nonnull align 1 dereferenceable(1) %call, %"struct.draco::Mesh::AttributeData"* %2, %"struct.draco::Mesh::AttributeData"* %4, %"struct.draco::Mesh::AttributeData"** nonnull align 4 dereferenceable(4) %__begin_2)
  %6 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %__begin_3 = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %6, i32 0, i32 0
  %7 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__begin_4 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %7, i32 0, i32 1
  call void @_ZNSt3__24swapIPN5draco4Mesh13AttributeDataEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS6_EE5valueEvE4typeERS6_S9_(%"struct.draco::Mesh::AttributeData"** nonnull align 4 dereferenceable(4) %__begin_3, %"struct.draco::Mesh::AttributeData"** nonnull align 4 dereferenceable(4) %__begin_4) #8
  %8 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %__end_5 = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %8, i32 0, i32 1
  %9 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__end_6 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %9, i32 0, i32 2
  call void @_ZNSt3__24swapIPN5draco4Mesh13AttributeDataEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS6_EE5valueEvE4typeERS6_S9_(%"struct.draco::Mesh::AttributeData"** nonnull align 4 dereferenceable(4) %__end_5, %"struct.draco::Mesh::AttributeData"** nonnull align 4 dereferenceable(4) %__end_6) #8
  %10 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %call7 = call nonnull align 4 dereferenceable(4) %"struct.draco::Mesh::AttributeData"** @_ZNSt3__213__vector_baseIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE9__end_capEv(%"class.std::__2::__vector_base.95"* %10) #8
  %11 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %call8 = call nonnull align 4 dereferenceable(4) %"struct.draco::Mesh::AttributeData"** @_ZNSt3__214__split_bufferIN5draco4Mesh13AttributeDataERNS_9allocatorIS3_EEE9__end_capEv(%"struct.std::__2::__split_buffer"* %11) #8
  call void @_ZNSt3__24swapIPN5draco4Mesh13AttributeDataEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS6_EE5valueEvE4typeERS6_S9_(%"struct.draco::Mesh::AttributeData"** nonnull align 4 dereferenceable(4) %call7, %"struct.draco::Mesh::AttributeData"** nonnull align 4 dereferenceable(4) %call8) #8
  %12 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__begin_9 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %12, i32 0, i32 1
  %13 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__begin_9, align 4
  %14 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %14, i32 0, i32 0
  store %"struct.draco::Mesh::AttributeData"* %13, %"struct.draco::Mesh::AttributeData"** %__first_, align 4
  %call10 = call i32 @_ZNKSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE4sizeEv(%"class.std::__2::vector.94"* %this1) #8
  call void @_ZNKSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE14__annotate_newEm(%"class.std::__2::vector.94"* %this1, i32 %call10) #8
  call void @_ZNSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector.94"* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferIN5draco4Mesh13AttributeDataERNS_9allocatorIS3_EEED2Ev(%"struct.std::__2::__split_buffer"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"struct.std::__2::__split_buffer"*, align 4
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer"* %this1, %"struct.std::__2::__split_buffer"** %retval, align 4
  call void @_ZNSt3__214__split_bufferIN5draco4Mesh13AttributeDataERNS_9allocatorIS3_EEE5clearEv(%"struct.std::__2::__split_buffer"* %this1) #8
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %0 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__first_, align 4
  %tobool = icmp ne %"struct.draco::Mesh::AttributeData"* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.99"* @_ZNSt3__214__split_bufferIN5draco4Mesh13AttributeDataERNS_9allocatorIS3_EEE7__allocEv(%"struct.std::__2::__split_buffer"* %this1) #8
  %__first_2 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %1 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__first_2, align 4
  %call3 = call i32 @_ZNKSt3__214__split_bufferIN5draco4Mesh13AttributeDataERNS_9allocatorIS3_EEE8capacityEv(%"struct.std::__2::__split_buffer"* %this1)
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco4Mesh13AttributeDataEEEE10deallocateERS5_PS4_m(%"class.std::__2::allocator.99"* nonnull align 1 dereferenceable(1) %call, %"struct.draco::Mesh::AttributeData"* %1, i32 %call3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %retval, align 4
  ret %"struct.std::__2::__split_buffer"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.draco::Mesh::AttributeData"** @_ZNSt3__217__compressed_pairIPN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE5firstEv(%"class.std::__2::__compressed_pair.96"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.96"*, align 4
  store %"class.std::__2::__compressed_pair.96"* %this, %"class.std::__2::__compressed_pair.96"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.96"*, %"class.std::__2::__compressed_pair.96"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.96"* %this1 to %"struct.std::__2::__compressed_pair_elem.97"*
  %call = call nonnull align 4 dereferenceable(4) %"struct.draco::Mesh::AttributeData"** @_ZNSt3__222__compressed_pair_elemIPN5draco4Mesh13AttributeDataELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.97"* %0) #8
  ret %"struct.draco::Mesh::AttributeData"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.draco::Mesh::AttributeData"** @_ZNSt3__222__compressed_pair_elemIPN5draco4Mesh13AttributeDataELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.97"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.97"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.97"* %this, %"struct.std::__2::__compressed_pair_elem.97"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.97"*, %"struct.std::__2::__compressed_pair_elem.97"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.97", %"struct.std::__2::__compressed_pair_elem.97"* %this1, i32 0, i32 0
  ret %"struct.draco::Mesh::AttributeData"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData>>::_ConstructTransaction"* @_ZNSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE21_ConstructTransactionC2ERS6_m(%"struct.std::__2::vector<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData>>::_ConstructTransaction"* returned %this, %"class.std::__2::vector.94"* nonnull align 4 dereferenceable(12) %__v, i32 %__n) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData>>::_ConstructTransaction"*, align 4
  %__v.addr = alloca %"class.std::__2::vector.94"*, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::vector<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData>>::_ConstructTransaction"** %this.addr, align 4
  store %"class.std::__2::vector.94"* %__v, %"class.std::__2::vector.94"** %__v.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::vector<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData>>::_ConstructTransaction"*, %"struct.std::__2::vector<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData>>::_ConstructTransaction"** %this.addr, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData>>::_ConstructTransaction", %"struct.std::__2::vector<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %__v.addr, align 4
  store %"class.std::__2::vector.94"* %0, %"class.std::__2::vector.94"** %__v_, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData>>::_ConstructTransaction", %"struct.std::__2::vector<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %__v.addr, align 4
  %2 = bitcast %"class.std::__2::vector.94"* %1 to %"class.std::__2::__vector_base.95"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %2, i32 0, i32 1
  %3 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__end_, align 4
  store %"struct.draco::Mesh::AttributeData"* %3, %"struct.draco::Mesh::AttributeData"** %__pos_, align 4
  %__new_end_ = getelementptr inbounds %"struct.std::__2::vector<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData>>::_ConstructTransaction", %"struct.std::__2::vector<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData>>::_ConstructTransaction"* %this1, i32 0, i32 2
  %4 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %__v.addr, align 4
  %5 = bitcast %"class.std::__2::vector.94"* %4 to %"class.std::__2::__vector_base.95"*
  %__end_2 = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %5, i32 0, i32 1
  %6 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__end_2, align 4
  %7 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds %"struct.draco::Mesh::AttributeData", %"struct.draco::Mesh::AttributeData"* %6, i32 %7
  store %"struct.draco::Mesh::AttributeData"* %add.ptr, %"struct.draco::Mesh::AttributeData"** %__new_end_, align 4
  ret %"struct.std::__2::vector<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco4Mesh13AttributeDataEEEE9constructIS4_JEEEvRS5_PT_DpOT0_(%"class.std::__2::allocator.99"* nonnull align 1 dereferenceable(1) %__a, %"struct.draco::Mesh::AttributeData"* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.99"*, align 4
  %__p.addr = alloca %"struct.draco::Mesh::AttributeData"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_construct", align 1
  store %"class.std::__2::allocator.99"* %__a, %"class.std::__2::allocator.99"** %__a.addr, align 4
  store %"struct.draco::Mesh::AttributeData"* %__p, %"struct.draco::Mesh::AttributeData"** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_construct"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.99"*, %"class.std::__2::allocator.99"** %__a.addr, align 4
  %2 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco4Mesh13AttributeDataEEEE11__constructIS4_JEEEvNS_17integral_constantIbLb1EEERS5_PT_DpOT0_(%"class.std::__2::allocator.99"* nonnull align 1 dereferenceable(1) %1, %"struct.draco::Mesh::AttributeData"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData>>::_ConstructTransaction"* @_ZNSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData>>::_ConstructTransaction"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData>>::_ConstructTransaction"*, align 4
  store %"struct.std::__2::vector<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData>>::_ConstructTransaction"** %this.addr, align 4
  %this1 = load %"struct.std::__2::vector<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData>>::_ConstructTransaction"*, %"struct.std::__2::vector<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData>>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData>>::_ConstructTransaction", %"struct.std::__2::vector<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %0 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__pos_, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData>>::_ConstructTransaction", %"struct.std::__2::vector<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %__v_, align 4
  %2 = bitcast %"class.std::__2::vector.94"* %1 to %"class.std::__2::__vector_base.95"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %2, i32 0, i32 1
  store %"struct.draco::Mesh::AttributeData"* %0, %"struct.draco::Mesh::AttributeData"** %__end_, align 4
  ret %"struct.std::__2::vector<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco4Mesh13AttributeDataEEEE11__constructIS4_JEEEvNS_17integral_constantIbLb1EEERS5_PT_DpOT0_(%"class.std::__2::allocator.99"* nonnull align 1 dereferenceable(1) %__a, %"struct.draco::Mesh::AttributeData"* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.99"*, align 4
  %__p.addr = alloca %"struct.draco::Mesh::AttributeData"*, align 4
  store %"class.std::__2::allocator.99"* %__a, %"class.std::__2::allocator.99"** %__a.addr, align 4
  store %"struct.draco::Mesh::AttributeData"* %__p, %"struct.draco::Mesh::AttributeData"** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.99"*, %"class.std::__2::allocator.99"** %__a.addr, align 4
  %2 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__p.addr, align 4
  call void @_ZNSt3__29allocatorIN5draco4Mesh13AttributeDataEE9constructIS3_JEEEvPT_DpOT0_(%"class.std::__2::allocator.99"* %1, %"struct.draco::Mesh::AttributeData"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIN5draco4Mesh13AttributeDataEE9constructIS3_JEEEvPT_DpOT0_(%"class.std::__2::allocator.99"* %this, %"struct.draco::Mesh::AttributeData"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.99"*, align 4
  %__p.addr = alloca %"struct.draco::Mesh::AttributeData"*, align 4
  store %"class.std::__2::allocator.99"* %this, %"class.std::__2::allocator.99"** %this.addr, align 4
  store %"struct.draco::Mesh::AttributeData"* %__p, %"struct.draco::Mesh::AttributeData"** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.99"*, %"class.std::__2::allocator.99"** %this.addr, align 4
  %0 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__p.addr, align 4
  %1 = bitcast %"struct.draco::Mesh::AttributeData"* %0 to i8*
  %2 = bitcast i8* %1 to %"struct.draco::Mesh::AttributeData"*
  %call = call %"struct.draco::Mesh::AttributeData"* @_ZN5draco4Mesh13AttributeDataC2Ev(%"struct.draco::Mesh::AttributeData"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.draco::Mesh::AttributeData"* @_ZN5draco4Mesh13AttributeDataC2Ev(%"struct.draco::Mesh::AttributeData"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.draco::Mesh::AttributeData"*, align 4
  store %"struct.draco::Mesh::AttributeData"* %this, %"struct.draco::Mesh::AttributeData"** %this.addr, align 4
  %this1 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %this.addr, align 4
  %element_type = getelementptr inbounds %"struct.draco::Mesh::AttributeData", %"struct.draco::Mesh::AttributeData"* %this1, i32 0, i32 0
  store i32 1, i32* %element_type, align 4
  ret %"struct.draco::Mesh::AttributeData"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE8max_sizeEv(%"class.std::__2::vector.94"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp3 = alloca i32, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.99"* @_ZNKSt3__213__vector_baseIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE7__allocEv(%"class.std::__2::__vector_base.95"* %0) #8
  %call2 = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco4Mesh13AttributeDataEEEE8max_sizeERKS5_(%"class.std::__2::allocator.99"* nonnull align 1 dereferenceable(1) %call) #8
  store i32 %call2, i32* %ref.tmp, align 4
  %call4 = call i32 @_ZNSt3__214numeric_limitsIlE3maxEv() #8
  store i32 %call4, i32* %ref.tmp3, align 4
  %call5 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %1 = load i32, i32* %call5, align 4
  ret i32 %1
}

; Function Attrs: noreturn
declare void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"*) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::__less", align 1
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__a.addr, align 4
  %1 = load i32*, i32** %__b.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::__less", align 1
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__a.addr, align 4
  %1 = load i32*, i32** %__b.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco4Mesh13AttributeDataEEEE8max_sizeERKS5_(%"class.std::__2::allocator.99"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.99"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_max_size", align 1
  store %"class.std::__2::allocator.99"* %__a, %"class.std::__2::allocator.99"** %__a.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_max_size"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.99"*, %"class.std::__2::allocator.99"** %__a.addr, align 4
  %call = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco4Mesh13AttributeDataEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS5_(%"class.std::__2::allocator.99"* nonnull align 1 dereferenceable(1) %1) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.99"* @_ZNKSt3__213__vector_baseIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE7__allocEv(%"class.std::__2::__vector_base.95"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.95"*, align 4
  store %"class.std::__2::__vector_base.95"* %this, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.95"*, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.99"* @_ZNKSt3__217__compressed_pairIPN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE6secondEv(%"class.std::__2::__compressed_pair.96"* %__end_cap_) #8
  ret %"class.std::__2::allocator.99"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__214numeric_limitsIlE3maxEv() #0 comdat {
entry:
  %call = call i32 @_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv() #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__comp = alloca %"struct.std::__2::__less", align 1
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__b.addr, align 4
  %1 = load i32*, i32** %__a.addr, align 4
  %call = call zeroext i1 @_ZNKSt3__26__lessImmEclERKmS3_(%"struct.std::__2::__less"* %__comp, i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  br i1 %call, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32*, i32** %__b.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %3 = load i32*, i32** %__a.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi i32* [ %2, %cond.true ], [ %3, %cond.false ]
  ret i32* %cond-lvalue
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__26__lessImmEclERKmS3_(%"struct.std::__2::__less"* %this, i32* nonnull align 4 dereferenceable(4) %__x, i32* nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__less"*, align 4
  %__x.addr = alloca i32*, align 4
  %__y.addr = alloca i32*, align 4
  store %"struct.std::__2::__less"* %this, %"struct.std::__2::__less"** %this.addr, align 4
  store i32* %__x, i32** %__x.addr, align 4
  store i32* %__y, i32** %__y.addr, align 4
  %this1 = load %"struct.std::__2::__less"*, %"struct.std::__2::__less"** %this.addr, align 4
  %0 = load i32*, i32** %__x.addr, align 4
  %1 = load i32, i32* %0, align 4
  %2 = load i32*, i32** %__y.addr, align 4
  %3 = load i32, i32* %2, align 4
  %cmp = icmp ult i32 %1, %3
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco4Mesh13AttributeDataEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS5_(%"class.std::__2::allocator.99"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.99"*, align 4
  store %"class.std::__2::allocator.99"* %__a, %"class.std::__2::allocator.99"** %__a.addr, align 4
  %1 = load %"class.std::__2::allocator.99"*, %"class.std::__2::allocator.99"** %__a.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorIN5draco4Mesh13AttributeDataEE8max_sizeEv(%"class.std::__2::allocator.99"* %1) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__29allocatorIN5draco4Mesh13AttributeDataEE8max_sizeEv(%"class.std::__2::allocator.99"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.99"*, align 4
  store %"class.std::__2::allocator.99"* %this, %"class.std::__2::allocator.99"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.99"*, %"class.std::__2::allocator.99"** %this.addr, align 4
  ret i32 1073741823
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.99"* @_ZNKSt3__217__compressed_pairIPN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE6secondEv(%"class.std::__2::__compressed_pair.96"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.96"*, align 4
  store %"class.std::__2::__compressed_pair.96"* %this, %"class.std::__2::__compressed_pair.96"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.96"*, %"class.std::__2::__compressed_pair.96"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.96"* %this1 to %"struct.std::__2::__compressed_pair_elem.98"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.99"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorIN5draco4Mesh13AttributeDataEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.98"* %0) #8
  ret %"class.std::__2::allocator.99"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.99"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorIN5draco4Mesh13AttributeDataEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.98"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.98"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.98"* %this, %"struct.std::__2::__compressed_pair_elem.98"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.98"*, %"struct.std::__2::__compressed_pair_elem.98"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.98"* %this1 to %"class.std::__2::allocator.99"*
  ret %"class.std::__2::allocator.99"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv() #0 comdat {
entry:
  ret i32 2147483647
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__comp = alloca %"struct.std::__2::__less", align 1
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__a.addr, align 4
  %1 = load i32*, i32** %__b.addr, align 4
  %call = call zeroext i1 @_ZNKSt3__26__lessImmEclERKmS3_(%"struct.std::__2::__less"* %__comp, i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  br i1 %call, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32*, i32** %__b.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %3 = load i32*, i32** %__a.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi i32* [ %2, %cond.true ], [ %3, %cond.false ]
  ret i32* %cond-lvalue
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.132"* @_ZNSt3__217__compressed_pairIPN5draco4Mesh13AttributeDataERNS_9allocatorIS3_EEEC2IDnS7_EEOT_OT0_(%"class.std::__2::__compressed_pair.132"* returned %this, i8** nonnull align 4 dereferenceable(4) %__t1, %"class.std::__2::allocator.99"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.132"*, align 4
  %__t1.addr = alloca i8**, align 4
  %__t2.addr = alloca %"class.std::__2::allocator.99"*, align 4
  store %"class.std::__2::__compressed_pair.132"* %this, %"class.std::__2::__compressed_pair.132"** %this.addr, align 4
  store i8** %__t1, i8*** %__t1.addr, align 4
  store %"class.std::__2::allocator.99"* %__t2, %"class.std::__2::allocator.99"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.132"*, %"class.std::__2::__compressed_pair.132"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.132"* %this1 to %"struct.std::__2::__compressed_pair_elem.97"*
  %1 = load i8**, i8*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %1) #8
  %call2 = call %"struct.std::__2::__compressed_pair_elem.97"* @_ZNSt3__222__compressed_pair_elemIPN5draco4Mesh13AttributeDataELi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.97"* %0, i8** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.132"* %this1 to i8*
  %3 = getelementptr inbounds i8, i8* %2, i32 4
  %4 = bitcast i8* %3 to %"struct.std::__2::__compressed_pair_elem.133"*
  %5 = load %"class.std::__2::allocator.99"*, %"class.std::__2::allocator.99"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.99"* @_ZNSt3__27forwardIRNS_9allocatorIN5draco4Mesh13AttributeDataEEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"class.std::__2::allocator.99"* nonnull align 1 dereferenceable(1) %5) #8
  %call4 = call %"struct.std::__2::__compressed_pair_elem.133"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIN5draco4Mesh13AttributeDataEEELi1ELb0EEC2IS6_vEEOT_(%"struct.std::__2::__compressed_pair_elem.133"* %4, %"class.std::__2::allocator.99"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::__compressed_pair.132"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.draco::Mesh::AttributeData"* @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco4Mesh13AttributeDataEEEE8allocateERS5_m(%"class.std::__2::allocator.99"* nonnull align 1 dereferenceable(1) %__a, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.99"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.99"* %__a, %"class.std::__2::allocator.99"** %__a.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.99"*, %"class.std::__2::allocator.99"** %__a.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call %"struct.draco::Mesh::AttributeData"* @_ZNSt3__29allocatorIN5draco4Mesh13AttributeDataEE8allocateEmPKv(%"class.std::__2::allocator.99"* %0, i32 %1, i8* null)
  ret %"struct.draco::Mesh::AttributeData"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.99"* @_ZNSt3__214__split_bufferIN5draco4Mesh13AttributeDataERNS_9allocatorIS3_EEE7__allocEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.99"* @_ZNSt3__217__compressed_pairIPN5draco4Mesh13AttributeDataERNS_9allocatorIS3_EEE6secondEv(%"class.std::__2::__compressed_pair.132"* %__end_cap_) #8
  ret %"class.std::__2::allocator.99"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.draco::Mesh::AttributeData"** @_ZNSt3__214__split_bufferIN5draco4Mesh13AttributeDataERNS_9allocatorIS3_EEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  %call = call nonnull align 4 dereferenceable(4) %"struct.draco::Mesh::AttributeData"** @_ZNSt3__217__compressed_pairIPN5draco4Mesh13AttributeDataERNS_9allocatorIS3_EEE5firstEv(%"class.std::__2::__compressed_pair.132"* %__end_cap_) #8
  ret %"struct.draco::Mesh::AttributeData"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.99"* @_ZNSt3__27forwardIRNS_9allocatorIN5draco4Mesh13AttributeDataEEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"class.std::__2::allocator.99"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::allocator.99"*, align 4
  store %"class.std::__2::allocator.99"* %__t, %"class.std::__2::allocator.99"** %__t.addr, align 4
  %0 = load %"class.std::__2::allocator.99"*, %"class.std::__2::allocator.99"** %__t.addr, align 4
  ret %"class.std::__2::allocator.99"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.133"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIN5draco4Mesh13AttributeDataEEELi1ELb0EEC2IS6_vEEOT_(%"struct.std::__2::__compressed_pair_elem.133"* returned %this, %"class.std::__2::allocator.99"* nonnull align 1 dereferenceable(1) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.133"*, align 4
  %__u.addr = alloca %"class.std::__2::allocator.99"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.133"* %this, %"struct.std::__2::__compressed_pair_elem.133"** %this.addr, align 4
  store %"class.std::__2::allocator.99"* %__u, %"class.std::__2::allocator.99"** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.133"*, %"struct.std::__2::__compressed_pair_elem.133"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.133", %"struct.std::__2::__compressed_pair_elem.133"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::allocator.99"*, %"class.std::__2::allocator.99"** %__u.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.99"* @_ZNSt3__27forwardIRNS_9allocatorIN5draco4Mesh13AttributeDataEEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"class.std::__2::allocator.99"* nonnull align 1 dereferenceable(1) %0) #8
  store %"class.std::__2::allocator.99"* %call, %"class.std::__2::allocator.99"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.133"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.draco::Mesh::AttributeData"* @_ZNSt3__29allocatorIN5draco4Mesh13AttributeDataEE8allocateEmPKv(%"class.std::__2::allocator.99"* %this, i32 %__n, i8* %0) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.99"*, align 4
  %__n.addr = alloca i32, align 4
  %.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.99"* %this, %"class.std::__2::allocator.99"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::allocator.99"*, %"class.std::__2::allocator.99"** %this.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorIN5draco4Mesh13AttributeDataEE8max_sizeEv(%"class.std::__2::allocator.99"* %this1) #8
  %cmp = icmp ugt i32 %1, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__220__throw_length_errorEPKc(i8* getelementptr inbounds ([68 x i8], [68 x i8]* @.str, i32 0, i32 0)) #10
  unreachable

if.end:                                           ; preds = %entry
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  %call2 = call i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %mul, i32 4)
  %3 = bitcast i8* %call2 to %"struct.draco::Mesh::AttributeData"*
  ret %"struct.draco::Mesh::AttributeData"* %3
}

; Function Attrs: noinline noreturn nounwind optnone
define linkonce_odr hidden void @_ZNSt3__220__throw_length_errorEPKc(i8* %__msg) #5 comdat {
entry:
  %__msg.addr = alloca i8*, align 4
  store i8* %__msg, i8** %__msg.addr, align 4
  call void @abort() #10
  unreachable
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %__size, i32 %__align) #0 comdat {
entry:
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i32, i32* %__size.addr, align 4
  %call = call noalias nonnull i8* @_Znwm(i32 %0) #11
  ret i8* %call
}

; Function Attrs: noreturn
declare void @abort() #4

; Function Attrs: nobuiltin allocsize(0)
declare nonnull i8* @_Znwm(i32) #6

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.99"* @_ZNSt3__217__compressed_pairIPN5draco4Mesh13AttributeDataERNS_9allocatorIS3_EEE6secondEv(%"class.std::__2::__compressed_pair.132"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.132"*, align 4
  store %"class.std::__2::__compressed_pair.132"* %this, %"class.std::__2::__compressed_pair.132"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.132"*, %"class.std::__2::__compressed_pair.132"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.132"* %this1 to i8*
  %add.ptr = getelementptr inbounds i8, i8* %0, i32 4
  %1 = bitcast i8* %add.ptr to %"struct.std::__2::__compressed_pair_elem.133"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.99"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIN5draco4Mesh13AttributeDataEEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.133"* %1) #8
  ret %"class.std::__2::allocator.99"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.99"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIN5draco4Mesh13AttributeDataEEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.133"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.133"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.133"* %this, %"struct.std::__2::__compressed_pair_elem.133"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.133"*, %"struct.std::__2::__compressed_pair_elem.133"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.133", %"struct.std::__2::__compressed_pair_elem.133"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::allocator.99"*, %"class.std::__2::allocator.99"** %__value_, align 4
  ret %"class.std::__2::allocator.99"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.draco::Mesh::AttributeData"** @_ZNSt3__217__compressed_pairIPN5draco4Mesh13AttributeDataERNS_9allocatorIS3_EEE5firstEv(%"class.std::__2::__compressed_pair.132"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.132"*, align 4
  store %"class.std::__2::__compressed_pair.132"* %this, %"class.std::__2::__compressed_pair.132"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.132"*, %"class.std::__2::__compressed_pair.132"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.132"* %this1 to %"struct.std::__2::__compressed_pair_elem.97"*
  %call = call nonnull align 4 dereferenceable(4) %"struct.draco::Mesh::AttributeData"** @_ZNSt3__222__compressed_pair_elemIPN5draco4Mesh13AttributeDataELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.97"* %0) #8
  ret %"struct.draco::Mesh::AttributeData"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferIN5draco4Mesh13AttributeDataERNS_9allocatorIS3_EEE21_ConstructTransactionC2EPPS3_m(%"struct.std::__2::__split_buffer<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData> &>::_ConstructTransaction"* returned %this, %"struct.draco::Mesh::AttributeData"** %__p, i32 %__n) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData> &>::_ConstructTransaction"*, align 4
  %__p.addr = alloca %"struct.draco::Mesh::AttributeData"**, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::__split_buffer<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData> &>::_ConstructTransaction"* %this, %"struct.std::__2::__split_buffer<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData> &>::_ConstructTransaction"** %this.addr, align 4
  store %"struct.draco::Mesh::AttributeData"** %__p, %"struct.draco::Mesh::AttributeData"*** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData> &>::_ConstructTransaction"*, %"struct.std::__2::__split_buffer<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData> &>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::__split_buffer<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData> &>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load %"struct.draco::Mesh::AttributeData"**, %"struct.draco::Mesh::AttributeData"*** %__p.addr, align 4
  %1 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %0, align 4
  store %"struct.draco::Mesh::AttributeData"* %1, %"struct.draco::Mesh::AttributeData"** %__pos_, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData> &>::_ConstructTransaction"* %this1, i32 0, i32 1
  %2 = load %"struct.draco::Mesh::AttributeData"**, %"struct.draco::Mesh::AttributeData"*** %__p.addr, align 4
  %3 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %2, align 4
  %4 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds %"struct.draco::Mesh::AttributeData", %"struct.draco::Mesh::AttributeData"* %3, i32 %4
  store %"struct.draco::Mesh::AttributeData"* %add.ptr, %"struct.draco::Mesh::AttributeData"** %__end_, align 4
  %__dest_ = getelementptr inbounds %"struct.std::__2::__split_buffer<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData> &>::_ConstructTransaction"* %this1, i32 0, i32 2
  %5 = load %"struct.draco::Mesh::AttributeData"**, %"struct.draco::Mesh::AttributeData"*** %__p.addr, align 4
  store %"struct.draco::Mesh::AttributeData"** %5, %"struct.draco::Mesh::AttributeData"*** %__dest_, align 4
  ret %"struct.std::__2::__split_buffer<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData> &>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferIN5draco4Mesh13AttributeDataERNS_9allocatorIS3_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::__split_buffer<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData> &>::_ConstructTransaction"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData> &>::_ConstructTransaction"*, align 4
  store %"struct.std::__2::__split_buffer<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData> &>::_ConstructTransaction"* %this, %"struct.std::__2::__split_buffer<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData> &>::_ConstructTransaction"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData> &>::_ConstructTransaction"*, %"struct.std::__2::__split_buffer<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData> &>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::__split_buffer<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData> &>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__pos_, align 4
  %__dest_ = getelementptr inbounds %"struct.std::__2::__split_buffer<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData> &>::_ConstructTransaction"* %this1, i32 0, i32 2
  %1 = load %"struct.draco::Mesh::AttributeData"**, %"struct.draco::Mesh::AttributeData"*** %__dest_, align 4
  store %"struct.draco::Mesh::AttributeData"* %0, %"struct.draco::Mesh::AttributeData"** %1, align 4
  ret %"struct.std::__2::__split_buffer<draco::Mesh::AttributeData, std::__2::allocator<draco::Mesh::AttributeData> &>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco4Mesh13AttributeDataEEEE46__construct_backward_with_exception_guaranteesIS4_EENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS5_PT_SB_EE5valuesr31is_trivially_move_constructibleISB_EE5valueEvE4typeERS5_SC_SC_RSC_(%"class.std::__2::allocator.99"* nonnull align 1 dereferenceable(1) %0, %"struct.draco::Mesh::AttributeData"* %__begin1, %"struct.draco::Mesh::AttributeData"* %__end1, %"struct.draco::Mesh::AttributeData"** nonnull align 4 dereferenceable(4) %__end2) #0 comdat {
entry:
  %.addr = alloca %"class.std::__2::allocator.99"*, align 4
  %__begin1.addr = alloca %"struct.draco::Mesh::AttributeData"*, align 4
  %__end1.addr = alloca %"struct.draco::Mesh::AttributeData"*, align 4
  %__end2.addr = alloca %"struct.draco::Mesh::AttributeData"**, align 4
  %_Np = alloca i32, align 4
  store %"class.std::__2::allocator.99"* %0, %"class.std::__2::allocator.99"** %.addr, align 4
  store %"struct.draco::Mesh::AttributeData"* %__begin1, %"struct.draco::Mesh::AttributeData"** %__begin1.addr, align 4
  store %"struct.draco::Mesh::AttributeData"* %__end1, %"struct.draco::Mesh::AttributeData"** %__end1.addr, align 4
  store %"struct.draco::Mesh::AttributeData"** %__end2, %"struct.draco::Mesh::AttributeData"*** %__end2.addr, align 4
  %1 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__end1.addr, align 4
  %2 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__begin1.addr, align 4
  %sub.ptr.lhs.cast = ptrtoint %"struct.draco::Mesh::AttributeData"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"struct.draco::Mesh::AttributeData"* %2 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  store i32 %sub.ptr.div, i32* %_Np, align 4
  %3 = load i32, i32* %_Np, align 4
  %4 = load %"struct.draco::Mesh::AttributeData"**, %"struct.draco::Mesh::AttributeData"*** %__end2.addr, align 4
  %5 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %4, align 4
  %idx.neg = sub i32 0, %3
  %add.ptr = getelementptr inbounds %"struct.draco::Mesh::AttributeData", %"struct.draco::Mesh::AttributeData"* %5, i32 %idx.neg
  store %"struct.draco::Mesh::AttributeData"* %add.ptr, %"struct.draco::Mesh::AttributeData"** %4, align 4
  %6 = load i32, i32* %_Np, align 4
  %cmp = icmp sgt i32 %6, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %7 = load %"struct.draco::Mesh::AttributeData"**, %"struct.draco::Mesh::AttributeData"*** %__end2.addr, align 4
  %8 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %7, align 4
  %9 = bitcast %"struct.draco::Mesh::AttributeData"* %8 to i8*
  %10 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__begin1.addr, align 4
  %11 = bitcast %"struct.draco::Mesh::AttributeData"* %10 to i8*
  %12 = load i32, i32* %_Np, align 4
  %mul = mul i32 %12, 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %11, i32 %mul, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__24swapIPN5draco4Mesh13AttributeDataEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS6_EE5valueEvE4typeERS6_S9_(%"struct.draco::Mesh::AttributeData"** nonnull align 4 dereferenceable(4) %__x, %"struct.draco::Mesh::AttributeData"** nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %__x.addr = alloca %"struct.draco::Mesh::AttributeData"**, align 4
  %__y.addr = alloca %"struct.draco::Mesh::AttributeData"**, align 4
  %__t = alloca %"struct.draco::Mesh::AttributeData"*, align 4
  store %"struct.draco::Mesh::AttributeData"** %__x, %"struct.draco::Mesh::AttributeData"*** %__x.addr, align 4
  store %"struct.draco::Mesh::AttributeData"** %__y, %"struct.draco::Mesh::AttributeData"*** %__y.addr, align 4
  %0 = load %"struct.draco::Mesh::AttributeData"**, %"struct.draco::Mesh::AttributeData"*** %__x.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"struct.draco::Mesh::AttributeData"** @_ZNSt3__24moveIRPN5draco4Mesh13AttributeDataEEEONS_16remove_referenceIT_E4typeEOS7_(%"struct.draco::Mesh::AttributeData"** nonnull align 4 dereferenceable(4) %0) #8
  %1 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %call, align 4
  store %"struct.draco::Mesh::AttributeData"* %1, %"struct.draco::Mesh::AttributeData"** %__t, align 4
  %2 = load %"struct.draco::Mesh::AttributeData"**, %"struct.draco::Mesh::AttributeData"*** %__y.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(4) %"struct.draco::Mesh::AttributeData"** @_ZNSt3__24moveIRPN5draco4Mesh13AttributeDataEEEONS_16remove_referenceIT_E4typeEOS7_(%"struct.draco::Mesh::AttributeData"** nonnull align 4 dereferenceable(4) %2) #8
  %3 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %call1, align 4
  %4 = load %"struct.draco::Mesh::AttributeData"**, %"struct.draco::Mesh::AttributeData"*** %__x.addr, align 4
  store %"struct.draco::Mesh::AttributeData"* %3, %"struct.draco::Mesh::AttributeData"** %4, align 4
  %call2 = call nonnull align 4 dereferenceable(4) %"struct.draco::Mesh::AttributeData"** @_ZNSt3__24moveIRPN5draco4Mesh13AttributeDataEEEONS_16remove_referenceIT_E4typeEOS7_(%"struct.draco::Mesh::AttributeData"** nonnull align 4 dereferenceable(4) %__t) #8
  %5 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %call2, align 4
  %6 = load %"struct.draco::Mesh::AttributeData"**, %"struct.draco::Mesh::AttributeData"*** %__y.addr, align 4
  store %"struct.draco::Mesh::AttributeData"* %5, %"struct.draco::Mesh::AttributeData"** %6, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE14__annotate_newEm(%"class.std::__2::vector.94"* %this, i32 %__current_size) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  %__current_size.addr = alloca i32, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  store i32 %__current_size, i32* %__current_size.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  %call = call %"struct.draco::Mesh::AttributeData"* @_ZNKSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE4dataEv(%"class.std::__2::vector.94"* %this1) #8
  %0 = bitcast %"struct.draco::Mesh::AttributeData"* %call to i8*
  %call2 = call %"struct.draco::Mesh::AttributeData"* @_ZNKSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE4dataEv(%"class.std::__2::vector.94"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE8capacityEv(%"class.std::__2::vector.94"* %this1) #8
  %add.ptr = getelementptr inbounds %"struct.draco::Mesh::AttributeData", %"struct.draco::Mesh::AttributeData"* %call2, i32 %call3
  %1 = bitcast %"struct.draco::Mesh::AttributeData"* %add.ptr to i8*
  %call4 = call %"struct.draco::Mesh::AttributeData"* @_ZNKSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE4dataEv(%"class.std::__2::vector.94"* %this1) #8
  %call5 = call i32 @_ZNKSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE8capacityEv(%"class.std::__2::vector.94"* %this1) #8
  %add.ptr6 = getelementptr inbounds %"struct.draco::Mesh::AttributeData", %"struct.draco::Mesh::AttributeData"* %call4, i32 %call5
  %2 = bitcast %"struct.draco::Mesh::AttributeData"* %add.ptr6 to i8*
  %call7 = call %"struct.draco::Mesh::AttributeData"* @_ZNKSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE4dataEv(%"class.std::__2::vector.94"* %this1) #8
  %3 = load i32, i32* %__current_size.addr, align 4
  %add.ptr8 = getelementptr inbounds %"struct.draco::Mesh::AttributeData", %"struct.draco::Mesh::AttributeData"* %call7, i32 %3
  %4 = bitcast %"struct.draco::Mesh::AttributeData"* %add.ptr8 to i8*
  call void @_ZNKSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE31__annotate_contiguous_containerEPKvS8_S8_S8_(%"class.std::__2::vector.94"* %this1, i8* %0, i8* %1, i8* %2, i8* %4) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector.94"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #7

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.draco::Mesh::AttributeData"** @_ZNSt3__24moveIRPN5draco4Mesh13AttributeDataEEEONS_16remove_referenceIT_E4typeEOS7_(%"struct.draco::Mesh::AttributeData"** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"struct.draco::Mesh::AttributeData"**, align 4
  store %"struct.draco::Mesh::AttributeData"** %__t, %"struct.draco::Mesh::AttributeData"*** %__t.addr, align 4
  %0 = load %"struct.draco::Mesh::AttributeData"**, %"struct.draco::Mesh::AttributeData"*** %__t.addr, align 4
  ret %"struct.draco::Mesh::AttributeData"** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIN5draco4Mesh13AttributeDataERNS_9allocatorIS3_EEE5clearEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 1
  %0 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__begin_, align 4
  call void @_ZNSt3__214__split_bufferIN5draco4Mesh13AttributeDataERNS_9allocatorIS3_EEE17__destruct_at_endEPS3_(%"struct.std::__2::__split_buffer"* %this1, %"struct.draco::Mesh::AttributeData"* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__214__split_bufferIN5draco4Mesh13AttributeDataERNS_9allocatorIS3_EEE8capacityEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"struct.draco::Mesh::AttributeData"** @_ZNKSt3__214__split_bufferIN5draco4Mesh13AttributeDataERNS_9allocatorIS3_EEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this1) #8
  %0 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %call, align 4
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %1 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__first_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"struct.draco::Mesh::AttributeData"* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint %"struct.draco::Mesh::AttributeData"* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIN5draco4Mesh13AttributeDataERNS_9allocatorIS3_EEE17__destruct_at_endEPS3_(%"struct.std::__2::__split_buffer"* %this, %"struct.draco::Mesh::AttributeData"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__new_last.addr = alloca %"struct.draco::Mesh::AttributeData"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant.126", align 1
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store %"struct.draco::Mesh::AttributeData"* %__new_last, %"struct.draco::Mesh::AttributeData"** %__new_last.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %0 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__new_last.addr, align 4
  call void @_ZNSt3__214__split_bufferIN5draco4Mesh13AttributeDataERNS_9allocatorIS3_EEE17__destruct_at_endEPS3_NS_17integral_constantIbLb0EEE(%"struct.std::__2::__split_buffer"* %this1, %"struct.draco::Mesh::AttributeData"* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIN5draco4Mesh13AttributeDataERNS_9allocatorIS3_EEE17__destruct_at_endEPS3_NS_17integral_constantIbLb0EEE(%"struct.std::__2::__split_buffer"* %this, %"struct.draco::Mesh::AttributeData"* %__new_last) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant.126", align 1
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__new_last.addr = alloca %"struct.draco::Mesh::AttributeData"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store %"struct.draco::Mesh::AttributeData"* %__new_last, %"struct.draco::Mesh::AttributeData"** %__new_last.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__new_last.addr, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 2
  %2 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__end_, align 4
  %cmp = icmp ne %"struct.draco::Mesh::AttributeData"* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.99"* @_ZNSt3__214__split_bufferIN5draco4Mesh13AttributeDataERNS_9allocatorIS3_EEE7__allocEv(%"struct.std::__2::__split_buffer"* %this1) #8
  %__end_2 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 2
  %3 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__end_2, align 4
  %incdec.ptr = getelementptr inbounds %"struct.draco::Mesh::AttributeData", %"struct.draco::Mesh::AttributeData"* %3, i32 -1
  store %"struct.draco::Mesh::AttributeData"* %incdec.ptr, %"struct.draco::Mesh::AttributeData"** %__end_2, align 4
  %call3 = call %"struct.draco::Mesh::AttributeData"* @_ZNSt3__212__to_addressIN5draco4Mesh13AttributeDataEEEPT_S5_(%"struct.draco::Mesh::AttributeData"* %incdec.ptr) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco4Mesh13AttributeDataEEEE7destroyIS4_EEvRS5_PT_(%"class.std::__2::allocator.99"* nonnull align 1 dereferenceable(1) %call, %"struct.draco::Mesh::AttributeData"* %call3)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.draco::Mesh::AttributeData"** @_ZNKSt3__214__split_bufferIN5draco4Mesh13AttributeDataERNS_9allocatorIS3_EEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  %call = call nonnull align 4 dereferenceable(4) %"struct.draco::Mesh::AttributeData"** @_ZNKSt3__217__compressed_pairIPN5draco4Mesh13AttributeDataERNS_9allocatorIS3_EEE5firstEv(%"class.std::__2::__compressed_pair.132"* %__end_cap_) #8
  ret %"struct.draco::Mesh::AttributeData"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.draco::Mesh::AttributeData"** @_ZNKSt3__217__compressed_pairIPN5draco4Mesh13AttributeDataERNS_9allocatorIS3_EEE5firstEv(%"class.std::__2::__compressed_pair.132"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.132"*, align 4
  store %"class.std::__2::__compressed_pair.132"* %this, %"class.std::__2::__compressed_pair.132"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.132"*, %"class.std::__2::__compressed_pair.132"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.132"* %this1 to %"struct.std::__2::__compressed_pair_elem.97"*
  %call = call nonnull align 4 dereferenceable(4) %"struct.draco::Mesh::AttributeData"** @_ZNKSt3__222__compressed_pair_elemIPN5draco4Mesh13AttributeDataELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.97"* %0) #8
  ret %"struct.draco::Mesh::AttributeData"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE27__invalidate_iterators_pastEPS3_(%"class.std::__2::vector.94"* %this, %"struct.draco::Mesh::AttributeData"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  %__new_last.addr = alloca %"struct.draco::Mesh::AttributeData"*, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  store %"struct.draco::Mesh::AttributeData"* %__new_last, %"struct.draco::Mesh::AttributeData"** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE17__annotate_shrinkEm(%"class.std::__2::vector.94"* %this, i32 %__old_size) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  %__old_size.addr = alloca i32, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  store i32 %__old_size, i32* %__old_size.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  %call = call %"struct.draco::Mesh::AttributeData"* @_ZNKSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE4dataEv(%"class.std::__2::vector.94"* %this1) #8
  %0 = bitcast %"struct.draco::Mesh::AttributeData"* %call to i8*
  %call2 = call %"struct.draco::Mesh::AttributeData"* @_ZNKSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE4dataEv(%"class.std::__2::vector.94"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE8capacityEv(%"class.std::__2::vector.94"* %this1) #8
  %add.ptr = getelementptr inbounds %"struct.draco::Mesh::AttributeData", %"struct.draco::Mesh::AttributeData"* %call2, i32 %call3
  %1 = bitcast %"struct.draco::Mesh::AttributeData"* %add.ptr to i8*
  %call4 = call %"struct.draco::Mesh::AttributeData"* @_ZNKSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE4dataEv(%"class.std::__2::vector.94"* %this1) #8
  %2 = load i32, i32* %__old_size.addr, align 4
  %add.ptr5 = getelementptr inbounds %"struct.draco::Mesh::AttributeData", %"struct.draco::Mesh::AttributeData"* %call4, i32 %2
  %3 = bitcast %"struct.draco::Mesh::AttributeData"* %add.ptr5 to i8*
  %call6 = call %"struct.draco::Mesh::AttributeData"* @_ZNKSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE4dataEv(%"class.std::__2::vector.94"* %this1) #8
  %call7 = call i32 @_ZNKSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE4sizeEv(%"class.std::__2::vector.94"* %this1) #8
  %add.ptr8 = getelementptr inbounds %"struct.draco::Mesh::AttributeData", %"struct.draco::Mesh::AttributeData"* %call6, i32 %call7
  %4 = bitcast %"struct.draco::Mesh::AttributeData"* %add.ptr8 to i8*
  call void @_ZNKSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE31__annotate_contiguous_containerEPKvS8_S8_S8_(%"class.std::__2::vector.94"* %this1, i8* %0, i8* %1, i8* %3, i8* %4) #8
  ret void
}

declare void @_ZN5draco10PointCloud15DeleteAttributeEi(%"class.draco::PointCloud"*, i32) unnamed_addr #1

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.draco::Mesh::AttributeData"* @_ZNSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE5eraseENS_11__wrap_iterIPKS3_EE(%"class.std::__2::vector.94"* %this, %"struct.draco::Mesh::AttributeData"* %__position.coerce) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__wrap_iter.134", align 4
  %__position = alloca %"class.std::__2::__wrap_iter", align 4
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  %__ps = alloca i32, align 4
  %ref.tmp = alloca %"class.std::__2::__wrap_iter", align 4
  %__p = alloca %"struct.draco::Mesh::AttributeData"*, align 4
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %__position, i32 0, i32 0
  store %"struct.draco::Mesh::AttributeData"* %__position.coerce, %"struct.draco::Mesh::AttributeData"** %coerce.dive, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  %call = call %"struct.draco::Mesh::AttributeData"* @_ZNKSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE6cbeginEv(%"class.std::__2::vector.94"* %this1) #8
  %coerce.dive2 = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %ref.tmp, i32 0, i32 0
  store %"struct.draco::Mesh::AttributeData"* %call, %"struct.draco::Mesh::AttributeData"** %coerce.dive2, align 4
  %call3 = call i32 @_ZNSt3__2miIPKN5draco4Mesh13AttributeDataES5_EEDTmicldtfp_4baseEcldtfp0_4baseEERKNS_11__wrap_iterIT_EERKNS7_IT0_EE(%"class.std::__2::__wrap_iter"* nonnull align 4 dereferenceable(4) %__position, %"class.std::__2::__wrap_iter"* nonnull align 4 dereferenceable(4) %ref.tmp) #8
  store i32 %call3, i32* %__ps, align 4
  %0 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %0, i32 0, i32 0
  %1 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__begin_, align 4
  %2 = load i32, i32* %__ps, align 4
  %add.ptr = getelementptr inbounds %"struct.draco::Mesh::AttributeData", %"struct.draco::Mesh::AttributeData"* %1, i32 %2
  store %"struct.draco::Mesh::AttributeData"* %add.ptr, %"struct.draco::Mesh::AttributeData"** %__p, align 4
  %3 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__p, align 4
  %add.ptr4 = getelementptr inbounds %"struct.draco::Mesh::AttributeData", %"struct.draco::Mesh::AttributeData"* %3, i32 1
  %4 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %4, i32 0, i32 1
  %5 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__end_, align 4
  %6 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__p, align 4
  %call5 = call %"struct.draco::Mesh::AttributeData"* @_ZNSt3__24moveIPN5draco4Mesh13AttributeDataES4_EET0_T_S6_S5_(%"struct.draco::Mesh::AttributeData"* %add.ptr4, %"struct.draco::Mesh::AttributeData"* %5, %"struct.draco::Mesh::AttributeData"* %6)
  call void @_ZNSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE17__destruct_at_endEPS3_(%"class.std::__2::vector.94"* %this1, %"struct.draco::Mesh::AttributeData"* %call5) #8
  %7 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__p, align 4
  %add.ptr6 = getelementptr inbounds %"struct.draco::Mesh::AttributeData", %"struct.draco::Mesh::AttributeData"* %7, i32 -1
  call void @_ZNSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE27__invalidate_iterators_pastEPS3_(%"class.std::__2::vector.94"* %this1, %"struct.draco::Mesh::AttributeData"* %add.ptr6)
  %8 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__p, align 4
  %call7 = call %"struct.draco::Mesh::AttributeData"* @_ZNSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE11__make_iterEPS3_(%"class.std::__2::vector.94"* %this1, %"struct.draco::Mesh::AttributeData"* %8) #8
  %coerce.dive8 = getelementptr inbounds %"class.std::__2::__wrap_iter.134", %"class.std::__2::__wrap_iter.134"* %retval, i32 0, i32 0
  store %"struct.draco::Mesh::AttributeData"* %call7, %"struct.draco::Mesh::AttributeData"** %coerce.dive8, align 4
  %coerce.dive9 = getelementptr inbounds %"class.std::__2::__wrap_iter.134", %"class.std::__2::__wrap_iter.134"* %retval, i32 0, i32 0
  %9 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %coerce.dive9, align 4
  ret %"struct.draco::Mesh::AttributeData"* %9
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.draco::Mesh::AttributeData"* @_ZNSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE5beginEv(%"class.std::__2::vector.94"* %this) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__wrap_iter.134", align 4
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %0, i32 0, i32 0
  %1 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__begin_, align 4
  %call = call %"struct.draco::Mesh::AttributeData"* @_ZNSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE11__make_iterEPS3_(%"class.std::__2::vector.94"* %this1, %"struct.draco::Mesh::AttributeData"* %1) #8
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter.134", %"class.std::__2::__wrap_iter.134"* %retval, i32 0, i32 0
  store %"struct.draco::Mesh::AttributeData"* %call, %"struct.draco::Mesh::AttributeData"** %coerce.dive, align 4
  %coerce.dive2 = getelementptr inbounds %"class.std::__2::__wrap_iter.134", %"class.std::__2::__wrap_iter.134"* %retval, i32 0, i32 0
  %2 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %coerce.dive2, align 4
  ret %"struct.draco::Mesh::AttributeData"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.draco::Mesh::AttributeData"* @_ZNKSt3__211__wrap_iterIPN5draco4Mesh13AttributeDataEEplEl(%"class.std::__2::__wrap_iter.134"* %this, i32 %__n) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__wrap_iter.134", align 4
  %this.addr = alloca %"class.std::__2::__wrap_iter.134"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::__wrap_iter.134"* %this, %"class.std::__2::__wrap_iter.134"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::__wrap_iter.134"*, %"class.std::__2::__wrap_iter.134"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__wrap_iter.134"* %retval to i8*
  %1 = bitcast %"class.std::__2::__wrap_iter.134"* %this1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %0, i8* align 4 %1, i32 4, i1 false)
  %2 = load i32, i32* %__n.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::__wrap_iter.134"* @_ZNSt3__211__wrap_iterIPN5draco4Mesh13AttributeDataEEpLEl(%"class.std::__2::__wrap_iter.134"* %retval, i32 %2) #8
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter.134", %"class.std::__2::__wrap_iter.134"* %retval, i32 0, i32 0
  %3 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %coerce.dive, align 4
  ret %"struct.draco::Mesh::AttributeData"* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__wrap_iter"* @_ZNSt3__211__wrap_iterIPKN5draco4Mesh13AttributeDataEEC2IPS3_EERKNS0_IT_EEPNS_9enable_ifIXsr14is_convertibleIS9_S5_EE5valueEvE4typeE(%"class.std::__2::__wrap_iter"* returned %this, %"class.std::__2::__wrap_iter.134"* nonnull align 4 dereferenceable(4) %__u, i8* %0) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__wrap_iter"*, align 4
  %__u.addr = alloca %"class.std::__2::__wrap_iter.134"*, align 4
  %.addr = alloca i8*, align 4
  store %"class.std::__2::__wrap_iter"* %this, %"class.std::__2::__wrap_iter"** %this.addr, align 4
  store %"class.std::__2::__wrap_iter.134"* %__u, %"class.std::__2::__wrap_iter.134"** %__u.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::__wrap_iter"*, %"class.std::__2::__wrap_iter"** %this.addr, align 4
  %__i = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::__wrap_iter.134"*, %"class.std::__2::__wrap_iter.134"** %__u.addr, align 4
  %call = call %"struct.draco::Mesh::AttributeData"* @_ZNKSt3__211__wrap_iterIPN5draco4Mesh13AttributeDataEE4baseEv(%"class.std::__2::__wrap_iter.134"* %1) #8
  store %"struct.draco::Mesh::AttributeData"* %call, %"struct.draco::Mesh::AttributeData"** %__i, align 4
  ret %"class.std::__2::__wrap_iter"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__2miIPKN5draco4Mesh13AttributeDataES5_EEDTmicldtfp_4baseEcldtfp0_4baseEERKNS_11__wrap_iterIT_EERKNS7_IT0_EE(%"class.std::__2::__wrap_iter"* nonnull align 4 dereferenceable(4) %__x, %"class.std::__2::__wrap_iter"* nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %__x.addr = alloca %"class.std::__2::__wrap_iter"*, align 4
  %__y.addr = alloca %"class.std::__2::__wrap_iter"*, align 4
  store %"class.std::__2::__wrap_iter"* %__x, %"class.std::__2::__wrap_iter"** %__x.addr, align 4
  store %"class.std::__2::__wrap_iter"* %__y, %"class.std::__2::__wrap_iter"** %__y.addr, align 4
  %0 = load %"class.std::__2::__wrap_iter"*, %"class.std::__2::__wrap_iter"** %__x.addr, align 4
  %call = call %"struct.draco::Mesh::AttributeData"* @_ZNKSt3__211__wrap_iterIPKN5draco4Mesh13AttributeDataEE4baseEv(%"class.std::__2::__wrap_iter"* %0) #8
  %1 = load %"class.std::__2::__wrap_iter"*, %"class.std::__2::__wrap_iter"** %__y.addr, align 4
  %call1 = call %"struct.draco::Mesh::AttributeData"* @_ZNKSt3__211__wrap_iterIPKN5draco4Mesh13AttributeDataEE4baseEv(%"class.std::__2::__wrap_iter"* %1) #8
  %sub.ptr.lhs.cast = ptrtoint %"struct.draco::Mesh::AttributeData"* %call to i32
  %sub.ptr.rhs.cast = ptrtoint %"struct.draco::Mesh::AttributeData"* %call1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.draco::Mesh::AttributeData"* @_ZNKSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE6cbeginEv(%"class.std::__2::vector.94"* %this) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__wrap_iter", align 4
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  %call = call %"struct.draco::Mesh::AttributeData"* @_ZNKSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE5beginEv(%"class.std::__2::vector.94"* %this1) #8
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %retval, i32 0, i32 0
  store %"struct.draco::Mesh::AttributeData"* %call, %"struct.draco::Mesh::AttributeData"** %coerce.dive, align 4
  %coerce.dive2 = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %retval, i32 0, i32 0
  %0 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %coerce.dive2, align 4
  ret %"struct.draco::Mesh::AttributeData"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.draco::Mesh::AttributeData"* @_ZNSt3__24moveIPN5draco4Mesh13AttributeDataES4_EET0_T_S6_S5_(%"struct.draco::Mesh::AttributeData"* %__first, %"struct.draco::Mesh::AttributeData"* %__last, %"struct.draco::Mesh::AttributeData"* %__result) #0 comdat {
entry:
  %__first.addr = alloca %"struct.draco::Mesh::AttributeData"*, align 4
  %__last.addr = alloca %"struct.draco::Mesh::AttributeData"*, align 4
  %__result.addr = alloca %"struct.draco::Mesh::AttributeData"*, align 4
  store %"struct.draco::Mesh::AttributeData"* %__first, %"struct.draco::Mesh::AttributeData"** %__first.addr, align 4
  store %"struct.draco::Mesh::AttributeData"* %__last, %"struct.draco::Mesh::AttributeData"** %__last.addr, align 4
  store %"struct.draco::Mesh::AttributeData"* %__result, %"struct.draco::Mesh::AttributeData"** %__result.addr, align 4
  %0 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__first.addr, align 4
  %call = call %"struct.draco::Mesh::AttributeData"* @_ZNSt3__213__unwrap_iterIPN5draco4Mesh13AttributeDataEEET_S5_(%"struct.draco::Mesh::AttributeData"* %0)
  %1 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__last.addr, align 4
  %call1 = call %"struct.draco::Mesh::AttributeData"* @_ZNSt3__213__unwrap_iterIPN5draco4Mesh13AttributeDataEEET_S5_(%"struct.draco::Mesh::AttributeData"* %1)
  %2 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__result.addr, align 4
  %call2 = call %"struct.draco::Mesh::AttributeData"* @_ZNSt3__213__unwrap_iterIPN5draco4Mesh13AttributeDataEEET_S5_(%"struct.draco::Mesh::AttributeData"* %2)
  %call3 = call %"struct.draco::Mesh::AttributeData"* @_ZNSt3__26__moveIN5draco4Mesh13AttributeDataES3_EENS_9enable_ifIXaasr7is_sameINS_12remove_constIT_E4typeET0_EE5valuesr28is_trivially_copy_assignableIS9_EE5valueEPS9_E4typeEPS6_SD_SA_(%"struct.draco::Mesh::AttributeData"* %call, %"struct.draco::Mesh::AttributeData"* %call1, %"struct.draco::Mesh::AttributeData"* %call2)
  ret %"struct.draco::Mesh::AttributeData"* %call3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.draco::Mesh::AttributeData"* @_ZNSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE11__make_iterEPS3_(%"class.std::__2::vector.94"* %this, %"struct.draco::Mesh::AttributeData"* %__p) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__wrap_iter.134", align 4
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  %__p.addr = alloca %"struct.draco::Mesh::AttributeData"*, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  store %"struct.draco::Mesh::AttributeData"* %__p, %"struct.draco::Mesh::AttributeData"** %__p.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  %0 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__p.addr, align 4
  %call = call %"class.std::__2::__wrap_iter.134"* @_ZNSt3__211__wrap_iterIPN5draco4Mesh13AttributeDataEEC2ES4_(%"class.std::__2::__wrap_iter.134"* %retval, %"struct.draco::Mesh::AttributeData"* %0) #8
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter.134", %"class.std::__2::__wrap_iter.134"* %retval, i32 0, i32 0
  %1 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %coerce.dive, align 4
  ret %"struct.draco::Mesh::AttributeData"* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.draco::Mesh::AttributeData"* @_ZNKSt3__211__wrap_iterIPKN5draco4Mesh13AttributeDataEE4baseEv(%"class.std::__2::__wrap_iter"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__wrap_iter"*, align 4
  store %"class.std::__2::__wrap_iter"* %this, %"class.std::__2::__wrap_iter"** %this.addr, align 4
  %this1 = load %"class.std::__2::__wrap_iter"*, %"class.std::__2::__wrap_iter"** %this.addr, align 4
  %__i = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %this1, i32 0, i32 0
  %0 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__i, align 4
  ret %"struct.draco::Mesh::AttributeData"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.draco::Mesh::AttributeData"* @_ZNKSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE5beginEv(%"class.std::__2::vector.94"* %this) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__wrap_iter", align 4
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %0, i32 0, i32 0
  %1 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__begin_, align 4
  %call = call %"struct.draco::Mesh::AttributeData"* @_ZNKSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE11__make_iterEPKS3_(%"class.std::__2::vector.94"* %this1, %"struct.draco::Mesh::AttributeData"* %1) #8
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %retval, i32 0, i32 0
  store %"struct.draco::Mesh::AttributeData"* %call, %"struct.draco::Mesh::AttributeData"** %coerce.dive, align 4
  %coerce.dive2 = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %retval, i32 0, i32 0
  %2 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %coerce.dive2, align 4
  ret %"struct.draco::Mesh::AttributeData"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.draco::Mesh::AttributeData"* @_ZNKSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEE11__make_iterEPKS3_(%"class.std::__2::vector.94"* %this, %"struct.draco::Mesh::AttributeData"* %__p) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__wrap_iter", align 4
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  %__p.addr = alloca %"struct.draco::Mesh::AttributeData"*, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  store %"struct.draco::Mesh::AttributeData"* %__p, %"struct.draco::Mesh::AttributeData"** %__p.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  %0 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__p.addr, align 4
  %call = call %"class.std::__2::__wrap_iter"* @_ZNSt3__211__wrap_iterIPKN5draco4Mesh13AttributeDataEEC2ES5_(%"class.std::__2::__wrap_iter"* %retval, %"struct.draco::Mesh::AttributeData"* %0) #8
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %retval, i32 0, i32 0
  %1 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %coerce.dive, align 4
  ret %"struct.draco::Mesh::AttributeData"* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__wrap_iter"* @_ZNSt3__211__wrap_iterIPKN5draco4Mesh13AttributeDataEEC2ES5_(%"class.std::__2::__wrap_iter"* returned %this, %"struct.draco::Mesh::AttributeData"* %__x) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__wrap_iter"*, align 4
  %__x.addr = alloca %"struct.draco::Mesh::AttributeData"*, align 4
  store %"class.std::__2::__wrap_iter"* %this, %"class.std::__2::__wrap_iter"** %this.addr, align 4
  store %"struct.draco::Mesh::AttributeData"* %__x, %"struct.draco::Mesh::AttributeData"** %__x.addr, align 4
  %this1 = load %"class.std::__2::__wrap_iter"*, %"class.std::__2::__wrap_iter"** %this.addr, align 4
  %__i = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %this1, i32 0, i32 0
  %0 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__x.addr, align 4
  store %"struct.draco::Mesh::AttributeData"* %0, %"struct.draco::Mesh::AttributeData"** %__i, align 4
  ret %"class.std::__2::__wrap_iter"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.draco::Mesh::AttributeData"* @_ZNSt3__26__moveIN5draco4Mesh13AttributeDataES3_EENS_9enable_ifIXaasr7is_sameINS_12remove_constIT_E4typeET0_EE5valuesr28is_trivially_copy_assignableIS9_EE5valueEPS9_E4typeEPS6_SD_SA_(%"struct.draco::Mesh::AttributeData"* %__first, %"struct.draco::Mesh::AttributeData"* %__last, %"struct.draco::Mesh::AttributeData"* %__result) #0 comdat {
entry:
  %__first.addr = alloca %"struct.draco::Mesh::AttributeData"*, align 4
  %__last.addr = alloca %"struct.draco::Mesh::AttributeData"*, align 4
  %__result.addr = alloca %"struct.draco::Mesh::AttributeData"*, align 4
  %__n = alloca i32, align 4
  store %"struct.draco::Mesh::AttributeData"* %__first, %"struct.draco::Mesh::AttributeData"** %__first.addr, align 4
  store %"struct.draco::Mesh::AttributeData"* %__last, %"struct.draco::Mesh::AttributeData"** %__last.addr, align 4
  store %"struct.draco::Mesh::AttributeData"* %__result, %"struct.draco::Mesh::AttributeData"** %__result.addr, align 4
  %0 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__last.addr, align 4
  %1 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__first.addr, align 4
  %sub.ptr.lhs.cast = ptrtoint %"struct.draco::Mesh::AttributeData"* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint %"struct.draco::Mesh::AttributeData"* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  store i32 %sub.ptr.div, i32* %__n, align 4
  %2 = load i32, i32* %__n, align 4
  %cmp = icmp ugt i32 %2, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %3 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__result.addr, align 4
  %4 = bitcast %"struct.draco::Mesh::AttributeData"* %3 to i8*
  %5 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__first.addr, align 4
  %6 = bitcast %"struct.draco::Mesh::AttributeData"* %5 to i8*
  %7 = load i32, i32* %__n, align 4
  %mul = mul i32 %7, 4
  call void @llvm.memmove.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %6, i32 %mul, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %8 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__result.addr, align 4
  %9 = load i32, i32* %__n, align 4
  %add.ptr = getelementptr inbounds %"struct.draco::Mesh::AttributeData", %"struct.draco::Mesh::AttributeData"* %8, i32 %9
  ret %"struct.draco::Mesh::AttributeData"* %add.ptr
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.draco::Mesh::AttributeData"* @_ZNSt3__213__unwrap_iterIPN5draco4Mesh13AttributeDataEEET_S5_(%"struct.draco::Mesh::AttributeData"* %__i) #0 comdat {
entry:
  %__i.addr = alloca %"struct.draco::Mesh::AttributeData"*, align 4
  store %"struct.draco::Mesh::AttributeData"* %__i, %"struct.draco::Mesh::AttributeData"** %__i.addr, align 4
  %0 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__i.addr, align 4
  ret %"struct.draco::Mesh::AttributeData"* %0
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memmove.p0i8.p0i8.i32(i8* nocapture, i8* nocapture readonly, i32, i1 immarg) #7

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__wrap_iter.134"* @_ZNSt3__211__wrap_iterIPN5draco4Mesh13AttributeDataEEC2ES4_(%"class.std::__2::__wrap_iter.134"* returned %this, %"struct.draco::Mesh::AttributeData"* %__x) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__wrap_iter.134"*, align 4
  %__x.addr = alloca %"struct.draco::Mesh::AttributeData"*, align 4
  store %"class.std::__2::__wrap_iter.134"* %this, %"class.std::__2::__wrap_iter.134"** %this.addr, align 4
  store %"struct.draco::Mesh::AttributeData"* %__x, %"struct.draco::Mesh::AttributeData"** %__x.addr, align 4
  %this1 = load %"class.std::__2::__wrap_iter.134"*, %"class.std::__2::__wrap_iter.134"** %this.addr, align 4
  %__i = getelementptr inbounds %"class.std::__2::__wrap_iter.134", %"class.std::__2::__wrap_iter.134"* %this1, i32 0, i32 0
  %0 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__x.addr, align 4
  store %"struct.draco::Mesh::AttributeData"* %0, %"struct.draco::Mesh::AttributeData"** %__i, align 4
  ret %"class.std::__2::__wrap_iter.134"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__wrap_iter.134"* @_ZNSt3__211__wrap_iterIPN5draco4Mesh13AttributeDataEEpLEl(%"class.std::__2::__wrap_iter.134"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__wrap_iter.134"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::__wrap_iter.134"* %this, %"class.std::__2::__wrap_iter.134"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::__wrap_iter.134"*, %"class.std::__2::__wrap_iter.134"** %this.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %__i = getelementptr inbounds %"class.std::__2::__wrap_iter.134", %"class.std::__2::__wrap_iter.134"* %this1, i32 0, i32 0
  %1 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__i, align 4
  %add.ptr = getelementptr inbounds %"struct.draco::Mesh::AttributeData", %"struct.draco::Mesh::AttributeData"* %1, i32 %0
  store %"struct.draco::Mesh::AttributeData"* %add.ptr, %"struct.draco::Mesh::AttributeData"** %__i, align 4
  ret %"class.std::__2::__wrap_iter.134"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.draco::Mesh::AttributeData"* @_ZNKSt3__211__wrap_iterIPN5draco4Mesh13AttributeDataEE4baseEv(%"class.std::__2::__wrap_iter.134"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__wrap_iter.134"*, align 4
  store %"class.std::__2::__wrap_iter.134"* %this, %"class.std::__2::__wrap_iter.134"** %this.addr, align 4
  %this1 = load %"class.std::__2::__wrap_iter.134"*, %"class.std::__2::__wrap_iter.134"** %this.addr, align 4
  %__i = getelementptr inbounds %"class.std::__2::__wrap_iter.134", %"class.std::__2::__wrap_iter.134"* %this1, i32 0, i32 0
  %0 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__i, align 4
  ret %"struct.draco::Mesh::AttributeData"* %0
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { noreturn "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { noinline noreturn nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nobuiltin allocsize(0) "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { argmemonly nounwind willreturn }
attributes #8 = { nounwind }
attributes #9 = { builtin nounwind }
attributes #10 = { noreturn }
attributes #11 = { builtin allocsize(0) }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
