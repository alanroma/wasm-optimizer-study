; ModuleID = './draco/src/draco/mesh/mesh_cleanup.cc'
source_filename = "./draco/src/draco/mesh/mesh_cleanup.cc"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%"class.draco::IndexType.104" = type { i32 }
%"class.draco::IndexType" = type { i32 }
%"class.draco::MeshCleanup" = type { i8 }
%"class.draco::Mesh" = type { %"class.draco::PointCloud", %"class.std::__2::vector.94", %"class.draco::IndexTypeVector.101" }
%"class.draco::PointCloud" = type { i32 (...)**, %"class.std::__2::unique_ptr", %"class.std::__2::vector.51", [5 x %"class.std::__2::vector.87"], i32 }
%"class.std::__2::unique_ptr" = type { %"class.std::__2::__compressed_pair" }
%"class.std::__2::__compressed_pair" = type { %"struct.std::__2::__compressed_pair_elem" }
%"struct.std::__2::__compressed_pair_elem" = type { %"class.draco::GeometryMetadata"* }
%"class.draco::GeometryMetadata" = type { %"class.draco::Metadata", %"class.std::__2::vector" }
%"class.draco::Metadata" = type { %"class.std::__2::unordered_map", %"class.std::__2::unordered_map.17" }
%"class.std::__2::unordered_map" = type { %"class.std::__2::__hash_table" }
%"class.std::__2::__hash_table" = type { %"class.std::__2::unique_ptr.0", %"class.std::__2::__compressed_pair.7", %"class.std::__2::__compressed_pair.12", %"class.std::__2::__compressed_pair.14" }
%"class.std::__2::unique_ptr.0" = type { %"class.std::__2::__compressed_pair.1" }
%"class.std::__2::__compressed_pair.1" = type { %"struct.std::__2::__compressed_pair_elem.2", %"struct.std::__2::__compressed_pair_elem.3" }
%"struct.std::__2::__compressed_pair_elem.2" = type { %"struct.std::__2::__hash_node_base"** }
%"struct.std::__2::__hash_node_base" = type { %"struct.std::__2::__hash_node_base"* }
%"struct.std::__2::__compressed_pair_elem.3" = type { %"class.std::__2::__bucket_list_deallocator" }
%"class.std::__2::__bucket_list_deallocator" = type { %"class.std::__2::__compressed_pair.4" }
%"class.std::__2::__compressed_pair.4" = type { %"struct.std::__2::__compressed_pair_elem.5" }
%"struct.std::__2::__compressed_pair_elem.5" = type { i32 }
%"class.std::__2::__compressed_pair.7" = type { %"struct.std::__2::__compressed_pair_elem.8" }
%"struct.std::__2::__compressed_pair_elem.8" = type { %"struct.std::__2::__hash_node_base" }
%"class.std::__2::__compressed_pair.12" = type { %"struct.std::__2::__compressed_pair_elem.5" }
%"class.std::__2::__compressed_pair.14" = type { %"struct.std::__2::__compressed_pair_elem.15" }
%"struct.std::__2::__compressed_pair_elem.15" = type { float }
%"class.std::__2::unordered_map.17" = type { %"class.std::__2::__hash_table.18" }
%"class.std::__2::__hash_table.18" = type { %"class.std::__2::unique_ptr.19", %"class.std::__2::__compressed_pair.29", %"class.std::__2::__compressed_pair.34", %"class.std::__2::__compressed_pair.37" }
%"class.std::__2::unique_ptr.19" = type { %"class.std::__2::__compressed_pair.20" }
%"class.std::__2::__compressed_pair.20" = type { %"struct.std::__2::__compressed_pair_elem.21", %"struct.std::__2::__compressed_pair_elem.23" }
%"struct.std::__2::__compressed_pair_elem.21" = type { %"struct.std::__2::__hash_node_base.22"** }
%"struct.std::__2::__hash_node_base.22" = type { %"struct.std::__2::__hash_node_base.22"* }
%"struct.std::__2::__compressed_pair_elem.23" = type { %"class.std::__2::__bucket_list_deallocator.24" }
%"class.std::__2::__bucket_list_deallocator.24" = type { %"class.std::__2::__compressed_pair.25" }
%"class.std::__2::__compressed_pair.25" = type { %"struct.std::__2::__compressed_pair_elem.5" }
%"class.std::__2::__compressed_pair.29" = type { %"struct.std::__2::__compressed_pair_elem.30" }
%"struct.std::__2::__compressed_pair_elem.30" = type { %"struct.std::__2::__hash_node_base.22" }
%"class.std::__2::__compressed_pair.34" = type { %"struct.std::__2::__compressed_pair_elem.5" }
%"class.std::__2::__compressed_pair.37" = type { %"struct.std::__2::__compressed_pair_elem.15" }
%"class.std::__2::vector" = type { %"class.std::__2::__vector_base" }
%"class.std::__2::__vector_base" = type { %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"*, %"class.std::__2::__compressed_pair.44" }
%"class.std::__2::unique_ptr.40" = type { %"class.std::__2::__compressed_pair.41" }
%"class.std::__2::__compressed_pair.41" = type { %"struct.std::__2::__compressed_pair_elem.42" }
%"struct.std::__2::__compressed_pair_elem.42" = type { %"class.draco::AttributeMetadata"* }
%"class.draco::AttributeMetadata" = type { %"class.draco::Metadata", i32 }
%"class.std::__2::__compressed_pair.44" = type { %"struct.std::__2::__compressed_pair_elem.45" }
%"struct.std::__2::__compressed_pair_elem.45" = type { %"class.std::__2::unique_ptr.40"* }
%"class.std::__2::vector.51" = type { %"class.std::__2::__vector_base.52" }
%"class.std::__2::__vector_base.52" = type { %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"*, %"class.std::__2::__compressed_pair.82" }
%"class.std::__2::unique_ptr.53" = type { %"class.std::__2::__compressed_pair.54" }
%"class.std::__2::__compressed_pair.54" = type { %"struct.std::__2::__compressed_pair_elem.55" }
%"struct.std::__2::__compressed_pair_elem.55" = type { %"class.draco::PointAttribute"* }
%"class.draco::PointAttribute" = type <{ %"class.draco::GeometryAttribute", %"class.std::__2::unique_ptr.63", %"class.draco::IndexTypeVector", i32, i8, [3 x i8], %"class.std::__2::unique_ptr.75", [4 x i8] }>
%"class.draco::GeometryAttribute" = type { %"class.draco::DataBuffer"*, %"struct.draco::DataBufferDescriptor", i8, i32, i8, i64, i64, i32, i32 }
%"class.draco::DataBuffer" = type { %"class.std::__2::vector.56", %"struct.draco::DataBufferDescriptor" }
%"class.std::__2::vector.56" = type { %"class.std::__2::__vector_base.57" }
%"class.std::__2::__vector_base.57" = type { i8*, i8*, %"class.std::__2::__compressed_pair.58" }
%"class.std::__2::__compressed_pair.58" = type { %"struct.std::__2::__compressed_pair_elem.59" }
%"struct.std::__2::__compressed_pair_elem.59" = type { i8* }
%"struct.draco::DataBufferDescriptor" = type { i64, i64 }
%"class.std::__2::unique_ptr.63" = type { %"class.std::__2::__compressed_pair.64" }
%"class.std::__2::__compressed_pair.64" = type { %"struct.std::__2::__compressed_pair_elem.65" }
%"struct.std::__2::__compressed_pair_elem.65" = type { %"class.draco::DataBuffer"* }
%"class.draco::IndexTypeVector" = type { %"class.std::__2::vector.68" }
%"class.std::__2::vector.68" = type { %"class.std::__2::__vector_base.69" }
%"class.std::__2::__vector_base.69" = type { %"class.draco::IndexType"*, %"class.draco::IndexType"*, %"class.std::__2::__compressed_pair.70" }
%"class.std::__2::__compressed_pair.70" = type { %"struct.std::__2::__compressed_pair_elem.71" }
%"struct.std::__2::__compressed_pair_elem.71" = type { %"class.draco::IndexType"* }
%"class.std::__2::unique_ptr.75" = type { %"class.std::__2::__compressed_pair.76" }
%"class.std::__2::__compressed_pair.76" = type { %"struct.std::__2::__compressed_pair_elem.77" }
%"struct.std::__2::__compressed_pair_elem.77" = type { %"class.draco::AttributeTransformData"* }
%"class.draco::AttributeTransformData" = type { i32, %"class.draco::DataBuffer" }
%"class.std::__2::__compressed_pair.82" = type { %"struct.std::__2::__compressed_pair_elem.83" }
%"struct.std::__2::__compressed_pair_elem.83" = type { %"class.std::__2::unique_ptr.53"* }
%"class.std::__2::vector.87" = type { %"class.std::__2::__vector_base.88" }
%"class.std::__2::__vector_base.88" = type { i32*, i32*, %"class.std::__2::__compressed_pair.89" }
%"class.std::__2::__compressed_pair.89" = type { %"struct.std::__2::__compressed_pair_elem.90" }
%"struct.std::__2::__compressed_pair_elem.90" = type { i32* }
%"class.std::__2::vector.94" = type { %"class.std::__2::__vector_base.95" }
%"class.std::__2::__vector_base.95" = type { %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"*, %"class.std::__2::__compressed_pair.96" }
%"struct.draco::Mesh::AttributeData" = type { i32 }
%"class.std::__2::__compressed_pair.96" = type { %"struct.std::__2::__compressed_pair_elem.97" }
%"struct.std::__2::__compressed_pair_elem.97" = type { %"struct.draco::Mesh::AttributeData"* }
%"class.draco::IndexTypeVector.101" = type { %"class.std::__2::vector.102" }
%"class.std::__2::vector.102" = type { %"class.std::__2::__vector_base.103" }
%"class.std::__2::__vector_base.103" = type { %"struct.std::__2::array"*, %"struct.std::__2::array"*, %"class.std::__2::__compressed_pair.105" }
%"struct.std::__2::array" = type { [3 x %"class.draco::IndexType.104"] }
%"class.std::__2::__compressed_pair.105" = type { %"struct.std::__2::__compressed_pair_elem.106" }
%"struct.std::__2::__compressed_pair_elem.106" = type { %"struct.std::__2::array"* }
%"struct.draco::MeshCleanupOptions" = type { i8, i8 }
%"class.std::__2::vector.110" = type { i32*, i32, %"class.std::__2::__compressed_pair.111" }
%"class.std::__2::__compressed_pair.111" = type { %"struct.std::__2::__compressed_pair_elem.5" }
%"struct.std::__2::array.115" = type { [3 x %"class.draco::IndexType"] }
%"class.draco::IndexType.116" = type { i32 }
%"class.std::__2::__bit_reference" = type { i32*, i32 }
%"class.draco::IndexTypeVector.117" = type { %"class.std::__2::vector.118" }
%"class.std::__2::vector.118" = type { %"class.std::__2::__vector_base.119" }
%"class.std::__2::__vector_base.119" = type { %"class.draco::IndexType.104"*, %"class.draco::IndexType.104"*, %"class.std::__2::__compressed_pair.120" }
%"class.std::__2::__compressed_pair.120" = type { %"struct.std::__2::__compressed_pair_elem.121" }
%"struct.std::__2::__compressed_pair_elem.121" = type { %"class.draco::IndexType.104"* }
%"class.draco::IndexTypeVector.125" = type { %"class.std::__2::vector.56" }
%"class.draco::IndexTypeVector.126" = type { %"class.std::__2::vector.68" }
%"struct.std::__2::__default_init_tag" = type { i8 }
%"class.std::__2::__vector_base_common" = type { i8 }
%"class.std::__2::__bit_iterator" = type { i32*, i32 }
%"class.std::__2::allocator.138" = type { i8 }
%"class.std::__2::__bit_iterator.140" = type { i32*, i32 }
%"class.std::__2::allocator.113" = type { i8 }
%"class.std::__2::allocator.108" = type { i8 }
%"struct.std::__2::__split_buffer" = type { %"struct.std::__2::array"*, %"struct.std::__2::array"*, %"struct.std::__2::array"*, %"class.std::__2::__compressed_pair.127" }
%"class.std::__2::__compressed_pair.127" = type { %"struct.std::__2::__compressed_pair_elem.106", %"struct.std::__2::__compressed_pair_elem.128" }
%"struct.std::__2::__compressed_pair_elem.128" = type { %"class.std::__2::allocator.108"* }
%"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction" = type { %"class.std::__2::vector.102"*, %"struct.std::__2::array"*, %"struct.std::__2::array"* }
%"class.std::__2::__split_buffer_common" = type { i8 }
%"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction" = type { %"struct.std::__2::array"*, %"struct.std::__2::array"*, %"struct.std::__2::array"** }
%"struct.std::__2::integral_constant" = type { i8 }
%"struct.std::__2::__has_construct" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.107" = type { i8 }
%"struct.std::__2::__less" = type { i8 }
%"struct.std::__2::__has_max_size" = type { i8 }
%"struct.std::__2::integral_constant.129" = type { i8 }
%"struct.std::__2::__has_destroy" = type { i8 }
%"class.std::__2::allocator.73" = type { i8 }
%"struct.std::__2::__split_buffer.130" = type { %"class.draco::IndexType"*, %"class.draco::IndexType"*, %"class.draco::IndexType"*, %"class.std::__2::__compressed_pair.131" }
%"class.std::__2::__compressed_pair.131" = type { %"struct.std::__2::__compressed_pair_elem.71", %"struct.std::__2::__compressed_pair_elem.132" }
%"struct.std::__2::__compressed_pair_elem.132" = type { %"class.std::__2::allocator.73"* }
%"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction" = type { %"class.std::__2::vector.68"*, %"class.draco::IndexType"*, %"class.draco::IndexType"* }
%"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction" = type { %"class.draco::IndexType"*, %"class.draco::IndexType"*, %"class.draco::IndexType"** }
%"struct.std::__2::__has_construct.133" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.72" = type { i8 }
%"struct.std::__2::__has_max_size.134" = type { i8 }
%"struct.std::__2::__has_destroy.135" = type { i8 }
%"class.std::__2::allocator.61" = type { i8 }
%"struct.std::__2::__has_destroy.136" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.60" = type { i8 }
%"class.std::__2::allocator.123" = type { i8 }
%"struct.std::__2::__has_destroy.137" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.122" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.112" = type { i8 }
%"struct.std::__2::__has_max_size.141" = type { i8 }
%"struct.std::__2::random_access_iterator_tag" = type { i8 }
%"struct.std::__2::__less.142" = type { i8 }
%"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction" = type { %"class.std::__2::vector.118"*, %"class.draco::IndexType.104"*, %"class.draco::IndexType.104"* }
%"struct.std::__2::__has_max_size.143" = type { i8 }
%"struct.std::__2::__has_construct.144" = type { i8 }
%"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction" = type { %"class.std::__2::vector.56"*, i8*, i8* }
%"struct.std::__2::__has_construct.145" = type { i8 }
%"struct.std::__2::__has_max_size.146" = type { i8 }
%"struct.std::__2::__has_construct.147" = type { i8 }

$_ZNSt3__26vectorIbNS_9allocatorIbEEEC2Ev = comdat any

$_ZNSt3__26vectorIbNS_9allocatorIbEEE6resizeEmb = comdat any

$_ZNK5draco10PointCloud10num_pointsEv = comdat any

$_ZNSt3__25arrayIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELm3EEC2Ev = comdat any

$_ZN5draco9IndexTypeIjNS_19FaceIndex_tag_type_EEC2Ej = comdat any

$_ZNK5draco9IndexTypeIjNS_19FaceIndex_tag_type_EEltERKj = comdat any

$_ZNK5draco4Mesh9num_facesEv = comdat any

$_ZNK5draco4Mesh4faceENS_9IndexTypeIjNS_19FaceIndex_tag_type_EEE = comdat any

$_ZNK5draco14PointAttribute12mapped_indexENS_9IndexTypeIjNS_20PointIndex_tag_type_EEE = comdat any

$_ZNKSt3__25arrayIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELm3EEixEm = comdat any

$_ZNSt3__25arrayIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELm3EEixEm = comdat any

$_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEaSERKS2_ = comdat any

$_ZNK5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEeqERKS2_ = comdat any

$_ZN5draco4Mesh7SetFaceENS_9IndexTypeIjNS_19FaceIndex_tag_type_EEERKNSt3__25arrayINS1_IjNS_20PointIndex_tag_type_EEELm3EEE = comdat any

$_ZNK5draco9IndexTypeIjNS_19FaceIndex_tag_type_EEmiERKj = comdat any

$_ZNSt3__26vectorIbNS_9allocatorIbEEEixEm = comdat any

$_ZNK5draco9IndexTypeIjNS_20PointIndex_tag_type_EE5valueEv = comdat any

$_ZNKSt3__215__bit_referenceINS_6vectorIbNS_9allocatorIbEEEELb1EEcvbEv = comdat any

$_ZNSt3__215__bit_referenceINS_6vectorIbNS_9allocatorIbEEEELb1EEaSEb = comdat any

$_ZN5draco9IndexTypeIjNS_19FaceIndex_tag_type_EEppEv = comdat any

$_ZN5draco4Mesh11SetNumFacesEm = comdat any

$_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEES3_EC2Em = comdat any

$_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEC2Ej = comdat any

$_ZNK5draco9IndexTypeIjNS_20PointIndex_tag_type_EEltERKj = comdat any

$_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEES3_EixERKS3_ = comdat any

$_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEaSERKj = comdat any

$_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEaSERKS2_ = comdat any

$_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEppEv = comdat any

$_ZNSt3__25arrayIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELm3EEixEm = comdat any

$_ZN5draco10PointCloud14set_num_pointsEj = comdat any

$_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEEhEC2Ev = comdat any

$_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEES3_EC2Ev = comdat any

$_ZNK5draco10PointCloud14num_attributesEv = comdat any

$_ZN5draco10PointCloud9attributeEi = comdat any

$_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEEhE6assignEmRKh = comdat any

$_ZNK5draco14PointAttribute4sizeEv = comdat any

$_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEES3_E5clearEv = comdat any

$_ZNK5draco9IndexTypeIjNS_20PointIndex_tag_type_EEneERKS2_ = comdat any

$_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEEhEixERKS3_ = comdat any

$_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEES3_E6resizeEm = comdat any

$_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEC2Ej = comdat any

$_ZNK5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEltERKj = comdat any

$_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEES3_EixERKS3_ = comdat any

$_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEaSERKj = comdat any

$_ZNK5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEgtERKj = comdat any

$_ZN5draco17GeometryAttribute10GetAddressENS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEE = comdat any

$_ZNK5draco14PointAttribute6bufferEv = comdat any

$_ZN5draco10DataBuffer5WriteExPKvm = comdat any

$_ZNK5draco17GeometryAttribute10GetBytePosENS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEE = comdat any

$_ZNK5draco17GeometryAttribute11byte_strideEv = comdat any

$_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEppEv = comdat any

$_ZN5draco14PointAttribute6ResizeEm = comdat any

$_ZNK5draco14PointAttribute19is_mapping_identityEv = comdat any

$_ZN5draco14PointAttribute18SetExplicitMappingEm = comdat any

$_ZN5draco14PointAttribute16SetPointMapEntryENS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEE = comdat any

$_ZNK5draco9IndexTypeIjNS_20PointIndex_tag_type_EEeqERKS2_ = comdat any

$_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEES3_ED2Ev = comdat any

$_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEEhED2Ev = comdat any

$_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEES3_ED2Ev = comdat any

$_ZNSt3__26vectorIbNS_9allocatorIbEEED2Ev = comdat any

$_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEC2Ev = comdat any

$_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEENSt3__25arrayINS1_IjNS_20PointIndex_tag_type_EEELm3EEEE4sizeEv = comdat any

$_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4sizeEv = comdat any

$_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEENSt3__25arrayINS1_IjNS_20PointIndex_tag_type_EEELm3EEEEixERKS3_ = comdat any

$_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEEixEm = comdat any

$_ZNK5draco9IndexTypeIjNS_19FaceIndex_tag_type_EE5valueEv = comdat any

$_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEEEixERKS3_ = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEEixEm = comdat any

$_ZNK5draco9IndexTypeIjNS_19FaceIndex_tag_type_EEgeERKj = comdat any

$_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEENSt3__25arrayINS1_IjNS_20PointIndex_tag_type_EEELm3EEEE6resizeEmRKS8_ = comdat any

$_ZNSt3__25arrayIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELm3EEC2Ev = comdat any

$_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEENSt3__25arrayINS1_IjNS_20PointIndex_tag_type_EEELm3EEEEixERKS3_ = comdat any

$_ZNSt3__25arrayIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELm3EEaSERKS5_ = comdat any

$_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE6resizeEmRKS6_ = comdat any

$_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE8__appendEmRKS6_ = comdat any

$_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE17__destruct_at_endEPS6_ = comdat any

$_ZNSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE9__end_capEv = comdat any

$_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE18__construct_at_endEmRKS6_ = comdat any

$_ZNSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE7__allocEv = comdat any

$_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE11__recommendEm = comdat any

$_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEEC2EmmS9_ = comdat any

$_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE18__construct_at_endEmRKS6_ = comdat any

$_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS6_RS8_EE = comdat any

$_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEED2Ev = comdat any

$_ZNSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE5firstEv = comdat any

$_ZNSt3__222__compressed_pair_elemIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE21_ConstructTransactionC2ERS9_m = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE9constructIS7_JRKS7_EEEvRS8_PT_DpOT0_ = comdat any

$_ZNSt3__212__to_addressINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEEEPT_S8_ = comdat any

$_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE21_ConstructTransactionD2Ev = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE11__constructIS7_JRKS7_EEEvNS_17integral_constantIbLb1EEERS8_PT_DpOT0_ = comdat any

$_ZNSt3__27forwardIRKNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEEEOT_RNS_16remove_referenceIS9_E4typeE = comdat any

$_ZNSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEE9constructIS6_JRKS6_EEEvPT_DpOT0_ = comdat any

$_ZNSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEELi1ELb1EE5__getEv = comdat any

$_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE8max_sizeEv = comdat any

$_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE8capacityEv = comdat any

$_ZNSt3__23maxImEERKT_S3_S3_ = comdat any

$_ZNSt3__23minImEERKT_S3_S3_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE8max_sizeERKS8_ = comdat any

$_ZNKSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE7__allocEv = comdat any

$_ZNSt3__214numeric_limitsIlE3maxEv = comdat any

$_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_ = comdat any

$_ZNKSt3__26__lessImmEclERKmS3_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS8_ = comdat any

$_ZNKSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEE8max_sizeEv = comdat any

$_ZNKSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE6secondEv = comdat any

$_ZNKSt3__222__compressed_pair_elemINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv = comdat any

$_ZNKSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__23maxImNS_6__lessImmEEEERKT_S5_S5_T0_ = comdat any

$_ZNSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEEC2IDnSA_EEOT_OT0_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE8allocateERS8_m = comdat any

$_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE7__allocEv = comdat any

$_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE9__end_capEv = comdat any

$_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEELi0ELb0EEC2IDnvEEOT_ = comdat any

$_ZNSt3__27forwardIRNS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEEEOT_RNS_16remove_referenceISA_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIRNS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEELi1ELb0EEC2IS9_vEEOT_ = comdat any

$_ZNSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEE8allocateEmPKv = comdat any

$_ZNSt3__220__throw_length_errorEPKc = comdat any

$_ZNSt3__217__libcpp_allocateEmm = comdat any

$_ZNSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemIRNS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEELi1ELb0EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE5firstEv = comdat any

$_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE21_ConstructTransactionC2EPPS6_m = comdat any

$_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE21_ConstructTransactionD2Ev = comdat any

$_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE17__annotate_deleteEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE46__construct_backward_with_exception_guaranteesIS7_EENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS8_PT_SE_EE5valuesr31is_trivially_move_constructibleISE_EE5valueEvE4typeERS8_SF_SF_RSF_ = comdat any

$_ZNSt3__24swapIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS9_EE5valueEvE4typeERS9_SC_ = comdat any

$_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE14__annotate_newEm = comdat any

$_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE26__invalidate_all_iteratorsEv = comdat any

$_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE31__annotate_contiguous_containerEPKvSB_SB_SB_ = comdat any

$_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4dataEv = comdat any

$_ZNSt3__24moveIRPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEEEONS_16remove_referenceIT_E4typeEOSA_ = comdat any

$_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE5clearEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE10deallocateERS8_PS7_m = comdat any

$_ZNKSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE8capacityEv = comdat any

$_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE17__destruct_at_endEPS6_ = comdat any

$_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE17__destruct_at_endEPS6_NS_17integral_constantIbLb0EEE = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE7destroyIS7_EEvRS8_PT_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE9__destroyIS7_EEvNS_17integral_constantIbLb1EEERS8_PT_ = comdat any

$_ZNSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEE7destroyEPS6_ = comdat any

$_ZNSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEE10deallocateEPS6_m = comdat any

$_ZNSt3__219__libcpp_deallocateEPvmm = comdat any

$_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm = comdat any

$_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm = comdat any

$_ZNSt3__217_DeallocateCaller9__do_callEPv = comdat any

$_ZNKSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE5firstEv = comdat any

$_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE27__invalidate_iterators_pastEPS6_ = comdat any

$_ZNSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE17__destruct_at_endEPS6_ = comdat any

$_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE17__annotate_shrinkEm = comdat any

$_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEC2Ev = comdat any

$_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEEixEm = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEixEm = comdat any

$_ZNKSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEE3getEv = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPN5draco14PointAttributeELi0ELb0EE5__getEv = comdat any

$_ZN5draco10DataBuffer4dataEv = comdat any

$_ZNSt3__26vectorIhNS_9allocatorIhEEEixEm = comdat any

$_ZNKSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEE3getEv = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPN5draco10DataBufferELi0ELb0EE5__getEv = comdat any

$_ZNK5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EE5valueEv = comdat any

$_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEEE6resizeEmRKS5_ = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE6resizeEmRKS4_ = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8__appendEmRKS4_ = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_ = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE18__construct_at_endEmRKS4_ = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE11__recommendEm = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEEC2EmmS7_ = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE18__construct_at_endEmRKS4_ = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS4_RS6_EE = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEED2Ev = comdat any

$_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionC2ERS7_m = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE9constructIS5_JRKS5_EEEvRS6_PT_DpOT0_ = comdat any

$_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEPT_S6_ = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionD2Ev = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE11__constructIS5_JRKS5_EEEvNS_17integral_constantIbLb1EEERS6_PT_DpOT0_ = comdat any

$_ZNSt3__27forwardIRKN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEOT_RNS_16remove_referenceIS7_E4typeE = comdat any

$_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE9constructIS4_JRKS4_EEEvPT_DpOT0_ = comdat any

$_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb1EE5__getEv = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8max_sizeEv = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE8max_sizeERKS6_ = comdat any

$_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS6_ = comdat any

$_ZNKSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE8max_sizeEv = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv = comdat any

$_ZNKSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb1EE5__getEv = comdat any

$_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEEC2IDnS8_EEOT_OT0_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE8allocateERS6_m = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE7__allocEv = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE9__end_capEv = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELi0ELb0EEC2IDnvEEOT_ = comdat any

$_ZNSt3__27forwardIRNS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEEEOT_RNS_16remove_referenceIS8_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb0EEC2IS7_vEEOT_ = comdat any

$_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE8allocateEmPKv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb0EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE5firstEv = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE21_ConstructTransactionC2EPPS4_m = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE21_ConstructTransactionD2Ev = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_deleteEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE46__construct_backward_with_exception_guaranteesIS5_EENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS6_PT_SC_EE5valuesr31is_trivially_move_constructibleISC_EE5valueEvE4typeERS6_SD_SD_RSD_ = comdat any

$_ZNSt3__24swapIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS7_EE5valueEvE4typeERS7_SA_ = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE14__annotate_newEm = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE26__invalidate_all_iteratorsEv = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_ = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv = comdat any

$_ZNSt3__24moveIRPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEONS_16remove_referenceIT_E4typeEOS8_ = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE5clearEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE10deallocateERS6_PS5_m = comdat any

$_ZNKSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE8capacityEv = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE17__destruct_at_endEPS4_ = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE17__destruct_at_endEPS4_NS_17integral_constantIbLb0EEE = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE7destroyIS5_EEvRS6_PT_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE9__destroyIS5_EEvNS_17integral_constantIbLb1EEERS6_PT_ = comdat any

$_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE7destroyEPS4_ = comdat any

$_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE10deallocateEPS4_m = comdat any

$_ZNKSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE5firstEv = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE27__invalidate_iterators_pastEPS4_ = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_ = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_shrinkEm = comdat any

$_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEEEixERKS3_ = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEEixEm = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEED2Ev = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEED2Ev = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5clearEv = comdat any

$_ZNSt3__26vectorIhNS_9allocatorIhEEED2Ev = comdat any

$_ZNKSt3__26vectorIhNS_9allocatorIhEEE17__annotate_deleteEv = comdat any

$_ZNSt3__213__vector_baseIhNS_9allocatorIhEEED2Ev = comdat any

$_ZNKSt3__26vectorIhNS_9allocatorIhEEE31__annotate_contiguous_containerEPKvS5_S5_S5_ = comdat any

$_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv = comdat any

$_ZNKSt3__26vectorIhNS_9allocatorIhEEE8capacityEv = comdat any

$_ZNKSt3__26vectorIhNS_9allocatorIhEEE4sizeEv = comdat any

$_ZNSt3__212__to_addressIhEEPT_S2_ = comdat any

$_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPhNS_9allocatorIhEEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPhLi0ELb0EE5__getEv = comdat any

$_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE5clearEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIhEEE10deallocateERS2_Phm = comdat any

$_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE7__allocEv = comdat any

$_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE17__destruct_at_endEPh = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIhEEE7destroyIhEEvRS2_PT_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIhEEE9__destroyIhEEvNS_17integral_constantIbLb1EEERS2_PT_ = comdat any

$_ZNSt3__29allocatorIhE7destroyEPh = comdat any

$_ZNSt3__29allocatorIhE10deallocateEPhm = comdat any

$_ZNSt3__217__compressed_pairIPhNS_9allocatorIhEEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIhEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEED2Ev = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_deleteEv = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEED2Ev = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_ = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv = comdat any

$_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEEEPT_S6_ = comdat any

$_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE5clearEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE10deallocateERS6_PS5_m = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE7destroyIS5_EEvRS6_PT_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE9__destroyIS5_EEvNS_17integral_constantIbLb1EEERS6_PT_ = comdat any

$_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEE7destroyEPS4_ = comdat any

$_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEE10deallocateEPS4_m = comdat any

$_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__220__vector_base_commonILb1EEC2Ev = comdat any

$_ZNSt3__217__compressed_pairImNS_9allocatorImEEEC2IiNS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__27forwardIiEEOT_RNS_16remove_referenceIS1_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemImLi0ELb0EEC2IivEEOT_ = comdat any

$_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorImEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__29allocatorImEC2Ev = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorImEEE10deallocateERS2_Pmm = comdat any

$_ZNSt3__26vectorIbNS_9allocatorIbEEE7__allocEv = comdat any

$_ZNSt3__26vectorIbNS_9allocatorIbEEE5__capEv = comdat any

$_ZNSt3__26vectorIbNS_9allocatorIbEEE26__invalidate_all_iteratorsEv = comdat any

$_ZNSt3__29allocatorImE10deallocateEPmm = comdat any

$_ZNSt3__217__compressed_pairImNS_9allocatorImEEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorImEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairImNS_9allocatorImEEE5firstEv = comdat any

$_ZNSt3__222__compressed_pair_elemImLi0ELb0EE5__getEv = comdat any

$_ZNKSt3__26vectorIbNS_9allocatorIbEEE4sizeEv = comdat any

$_ZNSt3__214__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb0ELm0EEC2Ev = comdat any

$_ZNKSt3__26vectorIbNS_9allocatorIbEEE8capacityEv = comdat any

$_ZNSt3__26vectorIbNS_9allocatorIbEEE3endEv = comdat any

$_ZNSt3__29allocatorIbEC2ImEERKNS0_IT_EE = comdat any

$_ZNSt3__26vectorIbNS_9allocatorIbEEEC2ERKS2_ = comdat any

$_ZNSt3__26vectorIbNS_9allocatorIbEEE7reserveEm = comdat any

$_ZNKSt3__26vectorIbNS_9allocatorIbEEE11__recommendEm = comdat any

$_ZNSt3__24copyINS_6vectorIbNS_9allocatorIbEEEELb1EEENS_14__bit_iteratorIT_Lb0EXLi0EEEENS5_IS6_XT0_EXLi0EEEES8_S7_ = comdat any

$_ZNKSt3__26vectorIbNS_9allocatorIbEEE6cbeginEv = comdat any

$_ZNKSt3__26vectorIbNS_9allocatorIbEEE4cendEv = comdat any

$_ZNSt3__26vectorIbNS_9allocatorIbEEE5beginEv = comdat any

$_ZNSt3__26vectorIbNS_9allocatorIbEEE4swapERS3_ = comdat any

$_ZNSt3__26fill_nINS_6vectorIbNS_9allocatorIbEEEEEEvNS_14__bit_iteratorIT_Lb0EXLi0EEEENS6_9size_typeEb = comdat any

$_ZNSt3__214__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb0ELm0EEC2ERKS5_ = comdat any

$_ZNSt3__26vectorIbNS_9allocatorIbEEE26__internal_cap_to_externalEm = comdat any

$_ZNKSt3__26vectorIbNS_9allocatorIbEEE5__capEv = comdat any

$_ZNKSt3__217__compressed_pairImNS_9allocatorImEEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemImLi0ELb0EE5__getEv = comdat any

$_ZNSt3__26vectorIbNS_9allocatorIbEEE11__make_iterEm = comdat any

$_ZNSt3__214__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb0ELm0EEC2EPmj = comdat any

$_ZNSt3__29allocatorImEC2IbEERKNS0_IT_EE = comdat any

$_ZNSt3__217__compressed_pairImNS_9allocatorImEEEC2IiS2_EEOT_OT0_ = comdat any

$_ZNSt3__27forwardINS_9allocatorImEEEEOT_RNS_16remove_referenceIS3_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorImEELi1ELb1EEC2IS2_vEEOT_ = comdat any

$_ZNSt3__26vectorIbNS_9allocatorIbEEE11__vallocateEm = comdat any

$_ZNSt3__26vectorIbNS_9allocatorIbEEE18__construct_at_endINS_14__bit_iteratorIS3_Lb0ELm0EEEEENS_9enable_ifIXsr27__is_cpp17_forward_iteratorIT_EE5valueEvE4typeES8_S8_ = comdat any

$_ZNKSt3__26vectorIbNS_9allocatorIbEEE8max_sizeEv = comdat any

$_ZNSt3__26vectorIbNS_9allocatorIbEEE26__external_cap_to_internalEm = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorImEEE8allocateERS2_m = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorImEEE8max_sizeERKS2_ = comdat any

$_ZNKSt3__26vectorIbNS_9allocatorIbEEE7__allocEv = comdat any

$_ZNSt3__214numeric_limitsImE3maxEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorImEEE10__max_sizeENS_17integral_constantIbLb1EEERKS2_ = comdat any

$_ZNKSt3__29allocatorImE8max_sizeEv = comdat any

$_ZNKSt3__217__compressed_pairImNS_9allocatorImEEE6secondEv = comdat any

$_ZNKSt3__222__compressed_pair_elemINS_9allocatorImEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__223__libcpp_numeric_limitsImLb1EE3maxEv = comdat any

$_ZNSt3__29allocatorImE8allocateEmPKv = comdat any

$_ZNSt3__28distanceINS_14__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb0ELm0EEEEENS_15iterator_traitsIT_E15difference_typeES8_S8_ = comdat any

$_ZNSt3__24copyINS_6vectorIbNS_9allocatorIbEEEELb0EEENS_14__bit_iteratorIT_Lb0EXLi0EEEENS5_IS6_XT0_EXLi0EEEES8_S7_ = comdat any

$_ZNSt3__210__distanceINS_14__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb0ELm0EEEEENS_15iterator_traitsIT_E15difference_typeES8_S8_NS_26random_access_iterator_tagE = comdat any

$_ZNSt3__2miERKNS_14__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb0ELm0EEES7_ = comdat any

$_ZNSt3__214__copy_alignedINS_6vectorIbNS_9allocatorIbEEEELb0EEENS_14__bit_iteratorIT_Lb0EXLi0EEEENS5_IS6_XT0_EXLi0EEEES8_S7_ = comdat any

$_ZNSt3__216__copy_unalignedINS_6vectorIbNS_9allocatorIbEEEELb0EEENS_14__bit_iteratorIT_Lb0EXLi0EEEENS5_IS6_XT0_EXLi0EEEES8_S7_ = comdat any

$_ZNSt3__23minIlEERKT_S3_S3_ = comdat any

$_ZNSt3__212__to_addressImEEPT_S2_ = comdat any

$_ZNSt3__23minIlNS_6__lessIllEEEERKT_S5_S5_T0_ = comdat any

$_ZNKSt3__26__lessIllEclERKlS3_ = comdat any

$_ZNSt3__26vectorIbNS_9allocatorIbEEE10__align_itEm = comdat any

$_ZNSt3__214__copy_alignedINS_6vectorIbNS_9allocatorIbEEEELb1EEENS_14__bit_iteratorIT_Lb0EXLi0EEEENS5_IS6_XT0_EXLi0EEEES8_S7_ = comdat any

$_ZNSt3__216__copy_unalignedINS_6vectorIbNS_9allocatorIbEEEELb1EEENS_14__bit_iteratorIT_Lb0EXLi0EEEENS5_IS6_XT0_EXLi0EEEES8_S7_ = comdat any

$_ZNSt3__2miERKNS_14__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb1ELm0EEES7_ = comdat any

$_ZNSt3__212__to_addressIKmEEPT_S3_ = comdat any

$_ZNKSt3__26vectorIbNS_9allocatorIbEEE11__make_iterEm = comdat any

$_ZNSt3__214__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb1ELm0EEC2EPKmj = comdat any

$_ZNSt3__24swapIPmEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_ = comdat any

$_ZNSt3__24swapImEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS2_EE5valueEvE4typeERS2_S5_ = comdat any

$_ZNSt3__216__swap_allocatorINS_9allocatorImEEEEvRT_S4_NS_17integral_constantIbLb0EEE = comdat any

$_ZNSt3__24moveIRPmEEONS_16remove_referenceIT_E4typeEOS4_ = comdat any

$_ZNSt3__24moveIRmEEONS_16remove_referenceIT_E4typeEOS3_ = comdat any

$_ZNSt3__213__fill_n_trueINS_6vectorIbNS_9allocatorIbEEEEEEvNS_14__bit_iteratorIT_Lb0EXLi0EEEENS6_9size_typeE = comdat any

$_ZNSt3__214__fill_n_falseINS_6vectorIbNS_9allocatorIbEEEEEEvNS_14__bit_iteratorIT_Lb0EXLi0EEEENS6_9size_typeE = comdat any

$_ZNSt3__26vectorIbNS_9allocatorIbEEE10__make_refEm = comdat any

$_ZNSt3__215__bit_referenceINS_6vectorIbNS_9allocatorIbEEEELb1EEC2EPmm = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEEC2Em = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEEC2Ev = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE11__vallocateEm = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE18__construct_at_endEm = comdat any

$_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEEC2IDnNS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELi0ELb0EEC2IDnvEEOT_ = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEEC2Ev = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE8max_sizeEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE8allocateERS6_m = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE14__annotate_newEm = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE8max_sizeERKS6_ = comdat any

$_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS6_ = comdat any

$_ZNKSt3__29allocatorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEE8max_sizeEv = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv = comdat any

$_ZNKSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEE8allocateEmPKv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionC2ERS7_m = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE9constructIS5_JEEEvRS6_PT_DpOT0_ = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionD2Ev = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE11__constructIS5_JEEEvNS_17integral_constantIbLb1EEERS6_PT_DpOT0_ = comdat any

$_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEE9constructIS4_JEEEvPT_DpOT0_ = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEEixEm = comdat any

$_ZNSt3__26vectorIhNS_9allocatorIhEEEC2Ev = comdat any

$_ZNSt3__213__vector_baseIhNS_9allocatorIhEEEC2Ev = comdat any

$_ZNSt3__217__compressed_pairIPhNS_9allocatorIhEEEC2IDnNS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__222__compressed_pair_elemIPhLi0ELb0EEC2IDnvEEOT_ = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIhEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__29allocatorIhEC2Ev = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEEC2Ev = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEEC2Ev = comdat any

$_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEEC2IDnNS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEC2Ev = comdat any

$_ZNSt3__26vectorIhNS_9allocatorIhEEE6assignEmRKh = comdat any

$_ZNSt3__26fill_nIPhmhEET_S2_T0_RKT1_ = comdat any

$_ZNSt3__26vectorIhNS_9allocatorIhEEE18__construct_at_endEmRKh = comdat any

$_ZNSt3__26vectorIhNS_9allocatorIhEEE17__destruct_at_endEPh = comdat any

$_ZNSt3__26vectorIhNS_9allocatorIhEEE13__vdeallocateEv = comdat any

$_ZNSt3__26vectorIhNS_9allocatorIhEEE11__vallocateEm = comdat any

$_ZNKSt3__26vectorIhNS_9allocatorIhEEE11__recommendEm = comdat any

$_ZNSt3__26vectorIhNS_9allocatorIhEEE26__invalidate_all_iteratorsEv = comdat any

$_ZNSt3__28__fill_nIPhmhEET_S2_T0_RKT1_ = comdat any

$_ZNSt3__221__convert_to_integralEm = comdat any

$_ZNSt3__26vectorIhNS_9allocatorIhEEE21_ConstructTransactionC2ERS3_m = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIhEEE9constructIhJRKhEEEvRS2_PT_DpOT0_ = comdat any

$_ZNSt3__26vectorIhNS_9allocatorIhEEE21_ConstructTransactionD2Ev = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIhEEE11__constructIhJRKhEEEvNS_17integral_constantIbLb1EEERS2_PT_DpOT0_ = comdat any

$_ZNSt3__27forwardIRKhEEOT_RNS_16remove_referenceIS3_E4typeE = comdat any

$_ZNSt3__29allocatorIhE9constructIhJRKhEEEvPT_DpOT0_ = comdat any

$_ZNSt3__26vectorIhNS_9allocatorIhEEE27__invalidate_iterators_pastEPh = comdat any

$_ZNKSt3__26vectorIhNS_9allocatorIhEEE17__annotate_shrinkEm = comdat any

$_ZNSt3__26vectorIhNS_9allocatorIhEEE5clearEv = comdat any

$_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE9__end_capEv = comdat any

$_ZNSt3__217__compressed_pairIPhNS_9allocatorIhEEE5firstEv = comdat any

$_ZNSt3__222__compressed_pair_elemIPhLi0ELb0EE5__getEv = comdat any

$_ZNKSt3__26vectorIhNS_9allocatorIhEEE8max_sizeEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIhEEE8allocateERS2_m = comdat any

$_ZNKSt3__26vectorIhNS_9allocatorIhEEE14__annotate_newEm = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIhEEE8max_sizeERKS2_ = comdat any

$_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE7__allocEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIhEEE10__max_sizeENS_17integral_constantIbLb1EEERKS2_ = comdat any

$_ZNKSt3__29allocatorIhE8max_sizeEv = comdat any

$_ZNKSt3__217__compressed_pairIPhNS_9allocatorIhEEE6secondEv = comdat any

$_ZNKSt3__222__compressed_pair_elemINS_9allocatorIhEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__29allocatorIhE8allocateEmPKv = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5clearEv = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE6resizeEm = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8__appendEm = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE18__construct_at_endEm = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE18__construct_at_endEm = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE9constructIS5_JEEEvRS6_PT_DpOT0_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE11__constructIS5_JEEEvNS_17integral_constantIbLb1EEERS6_PT_DpOT0_ = comdat any

$_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE9constructIS4_JEEEvPT_DpOT0_ = comdat any

$_ZZNSt3__216__copy_unalignedINS_6vectorIbNS_9allocatorIbEEEELb0EEENS_14__bit_iteratorIT_Lb0EXLi0EEEENS5_IS6_XT0_EXLi0EEEES8_S7_E15__bits_per_word = comdat any

$_ZZNSt3__216__copy_unalignedINS_6vectorIbNS_9allocatorIbEEEELb1EEENS_14__bit_iteratorIT_Lb0EXLi0EEEENS5_IS6_XT0_EXLi0EEEES8_S7_E15__bits_per_word = comdat any

@_ZN5dracoL18kInvalidPointIndexE = internal constant %"class.draco::IndexType.104" { i32 -1 }, align 4
@.str = private unnamed_addr constant [68 x i8] c"allocator<T>::allocate(size_t n) 'n' exceeds maximum supported size\00", align 1
@_ZN5dracoL27kInvalidAttributeValueIndexE = internal constant %"class.draco::IndexType" { i32 -1 }, align 4
@_ZZNSt3__216__copy_unalignedINS_6vectorIbNS_9allocatorIbEEEELb0EEENS_14__bit_iteratorIT_Lb0EXLi0EEEENS5_IS6_XT0_EXLi0EEEES8_S7_E15__bits_per_word = linkonce_odr hidden constant i32 32, comdat, align 4
@_ZZNSt3__216__copy_unalignedINS_6vectorIbNS_9allocatorIbEEEELb1EEENS_14__bit_iteratorIT_Lb0EXLi0EEEENS5_IS6_XT0_EXLi0EEEES8_S7_E15__bits_per_word = linkonce_odr hidden constant i32 32, comdat, align 4

; Function Attrs: noinline nounwind optnone
define hidden zeroext i1 @_ZN5draco11MeshCleanupclEPNS_4MeshERKNS_18MeshCleanupOptionsE(%"class.draco::MeshCleanup"* %this, %"class.draco::Mesh"* %mesh, %"struct.draco::MeshCleanupOptions"* nonnull align 1 dereferenceable(2) %options) #0 {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::MeshCleanup"*, align 4
  %mesh.addr = alloca %"class.draco::Mesh"*, align 4
  %options.addr = alloca %"struct.draco::MeshCleanupOptions"*, align 4
  %pos_att = alloca %"class.draco::PointAttribute"*, align 4
  %is_point_used = alloca %"class.std::__2::vector.110", align 4
  %num_degenerated_faces = alloca i32, align 4
  %num_new_points = alloca i32, align 4
  %pos_indices = alloca %"struct.std::__2::array.115", align 4
  %f = alloca %"class.draco::IndexType.116", align 4
  %ref.tmp = alloca i32, align 4
  %face = alloca %"struct.std::__2::array"*, align 4
  %agg.tmp = alloca %"class.draco::IndexType.116", align 4
  %p = alloca i32, align 4
  %ref.tmp19 = alloca %"class.draco::IndexType", align 4
  %agg.tmp20 = alloca %"class.draco::IndexType.104", align 4
  %is_face_valid = alloca i8, align 1
  %agg.tmp44 = alloca %"class.draco::IndexType.116", align 4
  %p56 = alloca i32, align 4
  %ref.tmp60 = alloca %"class.std::__2::__bit_reference", align 4
  %ref.tmp65 = alloca %"class.std::__2::__bit_reference", align 4
  %points_changed = alloca i8, align 1
  %num_original_points = alloca i32, align 4
  %point_map = alloca %"class.draco::IndexTypeVector.117", align 4
  %i = alloca %"class.draco::IndexType.104", align 4
  %ref.tmp94 = alloca %"class.std::__2::__bit_reference", align 4
  %ref.tmp98 = alloca i32, align 4
  %f109 = alloca %"class.draco::IndexType.116", align 4
  %ref.tmp112 = alloca i32, align 4
  %face116 = alloca %"struct.std::__2::array", align 4
  %agg.tmp117 = alloca %"class.draco::IndexType.116", align 4
  %p120 = alloca i32, align 4
  %agg.tmp131 = alloca %"class.draco::IndexType.116", align 4
  %i137 = alloca %"class.draco::IndexType.104", align 4
  %is_att_index_used = alloca %"class.draco::IndexTypeVector.125", align 4
  %att_index_map = alloca %"class.draco::IndexTypeVector.126", align 4
  %a = alloca i32, align 4
  %att = alloca %"class.draco::PointAttribute"*, align 4
  %ref.tmp156 = alloca i8, align 1
  %num_used_entries = alloca i32, align 4
  %i157 = alloca %"class.draco::IndexType.104", align 4
  %entry_id = alloca %"class.draco::IndexType", align 4
  %agg.tmp165 = alloca %"class.draco::IndexType.104", align 4
  %att_indices_changed = alloca i8, align 1
  %i183 = alloca %"class.draco::IndexType", align 4
  %ref.tmp186 = alloca i32, align 4
  %src_add = alloca i8*, align 4
  %agg.tmp197 = alloca %"class.draco::IndexType", align 4
  %agg.tmp201 = alloca %"class.draco::IndexType", align 4
  %i222 = alloca i32, align 4
  %agg.tmp226 = alloca %"class.draco::IndexType.104", align 4
  %agg.tmp228 = alloca %"class.draco::IndexType", align 4
  %i239 = alloca %"class.draco::IndexType.104", align 4
  %new_point_id = alloca %"class.draco::IndexType.104", align 4
  %original_entry_index = alloca %"class.draco::IndexType", align 4
  %agg.tmp248 = alloca %"class.draco::IndexType.104", align 4
  %new_entry_index = alloca %"class.draco::IndexType", align 4
  %agg.tmp253 = alloca %"class.draco::IndexType.104", align 4
  %agg.tmp254 = alloca %"class.draco::IndexType", align 4
  store %"class.draco::MeshCleanup"* %this, %"class.draco::MeshCleanup"** %this.addr, align 4
  store %"class.draco::Mesh"* %mesh, %"class.draco::Mesh"** %mesh.addr, align 4
  store %"struct.draco::MeshCleanupOptions"* %options, %"struct.draco::MeshCleanupOptions"** %options.addr, align 4
  %this1 = load %"class.draco::MeshCleanup"*, %"class.draco::MeshCleanup"** %this.addr, align 4
  %0 = load %"struct.draco::MeshCleanupOptions"*, %"struct.draco::MeshCleanupOptions"** %options.addr, align 4
  %remove_degenerated_faces = getelementptr inbounds %"struct.draco::MeshCleanupOptions", %"struct.draco::MeshCleanupOptions"* %0, i32 0, i32 0
  %1 = load i8, i8* %remove_degenerated_faces, align 1
  %tobool = trunc i8 %1 to i1
  br i1 %tobool, label %if.end, label %land.lhs.true

land.lhs.true:                                    ; preds = %entry
  %2 = load %"struct.draco::MeshCleanupOptions"*, %"struct.draco::MeshCleanupOptions"** %options.addr, align 4
  %remove_unused_attributes = getelementptr inbounds %"struct.draco::MeshCleanupOptions", %"struct.draco::MeshCleanupOptions"* %2, i32 0, i32 1
  %3 = load i8, i8* %remove_unused_attributes, align 1
  %tobool2 = trunc i8 %3 to i1
  br i1 %tobool2, label %if.end, label %if.then

if.then:                                          ; preds = %land.lhs.true
  store i1 true, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %land.lhs.true, %entry
  %4 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %mesh.addr, align 4
  %5 = bitcast %"class.draco::Mesh"* %4 to %"class.draco::PointCloud"*
  %call = call %"class.draco::PointAttribute"* @_ZNK5draco10PointCloud17GetNamedAttributeENS_17GeometryAttribute4TypeE(%"class.draco::PointCloud"* %5, i32 0)
  store %"class.draco::PointAttribute"* %call, %"class.draco::PointAttribute"** %pos_att, align 4
  %6 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %pos_att, align 4
  %cmp = icmp eq %"class.draco::PointAttribute"* %6, null
  br i1 %cmp, label %if.then3, label %if.end4

if.then3:                                         ; preds = %if.end
  store i1 false, i1* %retval, align 1
  br label %return

if.end4:                                          ; preds = %if.end
  %call5 = call %"class.std::__2::vector.110"* @_ZNSt3__26vectorIbNS_9allocatorIbEEEC2Ev(%"class.std::__2::vector.110"* %is_point_used) #8
  %7 = load %"struct.draco::MeshCleanupOptions"*, %"struct.draco::MeshCleanupOptions"** %options.addr, align 4
  %remove_unused_attributes6 = getelementptr inbounds %"struct.draco::MeshCleanupOptions", %"struct.draco::MeshCleanupOptions"* %7, i32 0, i32 1
  %8 = load i8, i8* %remove_unused_attributes6, align 1
  %tobool7 = trunc i8 %8 to i1
  br i1 %tobool7, label %if.then8, label %if.end10

if.then8:                                         ; preds = %if.end4
  %9 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %mesh.addr, align 4
  %10 = bitcast %"class.draco::Mesh"* %9 to %"class.draco::PointCloud"*
  %call9 = call i32 @_ZNK5draco10PointCloud10num_pointsEv(%"class.draco::PointCloud"* %10)
  call void @_ZNSt3__26vectorIbNS_9allocatorIbEEE6resizeEmb(%"class.std::__2::vector.110"* %is_point_used, i32 %call9, i1 zeroext false)
  br label %if.end10

if.end10:                                         ; preds = %if.then8, %if.end4
  store i32 0, i32* %num_degenerated_faces, align 4
  store i32 0, i32* %num_new_points, align 4
  %call11 = call %"struct.std::__2::array.115"* @_ZNSt3__25arrayIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELm3EEC2Ev(%"struct.std::__2::array.115"* %pos_indices)
  %call12 = call %"class.draco::IndexType.116"* @_ZN5draco9IndexTypeIjNS_19FaceIndex_tag_type_EEC2Ej(%"class.draco::IndexType.116"* %f, i32 0)
  br label %for.cond

for.cond:                                         ; preds = %for.inc75, %if.end10
  %11 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %mesh.addr, align 4
  %call13 = call i32 @_ZNK5draco4Mesh9num_facesEv(%"class.draco::Mesh"* %11)
  store i32 %call13, i32* %ref.tmp, align 4
  %call14 = call zeroext i1 @_ZNK5draco9IndexTypeIjNS_19FaceIndex_tag_type_EEltERKj(%"class.draco::IndexType.116"* %f, i32* nonnull align 4 dereferenceable(4) %ref.tmp)
  br i1 %call14, label %for.body, label %for.end77

for.body:                                         ; preds = %for.cond
  %12 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %mesh.addr, align 4
  %13 = bitcast %"class.draco::IndexType.116"* %agg.tmp to i8*
  %14 = bitcast %"class.draco::IndexType.116"* %f to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %13, i8* align 4 %14, i32 4, i1 false)
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType.116", %"class.draco::IndexType.116"* %agg.tmp, i32 0, i32 0
  %15 = load i32, i32* %coerce.dive, align 4
  %call15 = call nonnull align 4 dereferenceable(12) %"struct.std::__2::array"* @_ZNK5draco4Mesh4faceENS_9IndexTypeIjNS_19FaceIndex_tag_type_EEE(%"class.draco::Mesh"* %12, i32 %15)
  store %"struct.std::__2::array"* %call15, %"struct.std::__2::array"** %face, align 4
  store i32 0, i32* %p, align 4
  br label %for.cond16

for.cond16:                                       ; preds = %for.inc, %for.body
  %16 = load i32, i32* %p, align 4
  %cmp17 = icmp slt i32 %16, 3
  br i1 %cmp17, label %for.body18, label %for.end

for.body18:                                       ; preds = %for.cond16
  %17 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %pos_att, align 4
  %18 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %face, align 4
  %19 = load i32, i32* %p, align 4
  %call21 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.104"* @_ZNKSt3__25arrayIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELm3EEixEm(%"struct.std::__2::array"* %18, i32 %19) #8
  %20 = bitcast %"class.draco::IndexType.104"* %agg.tmp20 to i8*
  %21 = bitcast %"class.draco::IndexType.104"* %call21 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %20, i8* align 4 %21, i32 4, i1 false)
  %coerce.dive22 = getelementptr inbounds %"class.draco::IndexType.104", %"class.draco::IndexType.104"* %agg.tmp20, i32 0, i32 0
  %22 = load i32, i32* %coerce.dive22, align 4
  %call23 = call i32 @_ZNK5draco14PointAttribute12mapped_indexENS_9IndexTypeIjNS_20PointIndex_tag_type_EEE(%"class.draco::PointAttribute"* %17, i32 %22)
  %coerce.dive24 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %ref.tmp19, i32 0, i32 0
  store i32 %call23, i32* %coerce.dive24, align 4
  %23 = load i32, i32* %p, align 4
  %call25 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZNSt3__25arrayIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELm3EEixEm(%"struct.std::__2::array.115"* %pos_indices, i32 %23) #8
  %call26 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEaSERKS2_(%"class.draco::IndexType"* %call25, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %ref.tmp19)
  br label %for.inc

for.inc:                                          ; preds = %for.body18
  %24 = load i32, i32* %p, align 4
  %inc = add nsw i32 %24, 1
  store i32 %inc, i32* %p, align 4
  br label %for.cond16

for.end:                                          ; preds = %for.cond16
  store i8 1, i8* %is_face_valid, align 1
  %25 = load %"struct.draco::MeshCleanupOptions"*, %"struct.draco::MeshCleanupOptions"** %options.addr, align 4
  %remove_degenerated_faces27 = getelementptr inbounds %"struct.draco::MeshCleanupOptions", %"struct.draco::MeshCleanupOptions"* %25, i32 0, i32 0
  %26 = load i8, i8* %remove_degenerated_faces27, align 1
  %tobool28 = trunc i8 %26 to i1
  br i1 %tobool28, label %if.then29, label %if.end50

if.then29:                                        ; preds = %for.end
  %call30 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZNSt3__25arrayIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELm3EEixEm(%"struct.std::__2::array.115"* %pos_indices, i32 0) #8
  %call31 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZNSt3__25arrayIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELm3EEixEm(%"struct.std::__2::array.115"* %pos_indices, i32 1) #8
  %call32 = call zeroext i1 @_ZNK5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEeqERKS2_(%"class.draco::IndexType"* %call30, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %call31)
  br i1 %call32, label %if.then40, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.then29
  %call33 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZNSt3__25arrayIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELm3EEixEm(%"struct.std::__2::array.115"* %pos_indices, i32 0) #8
  %call34 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZNSt3__25arrayIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELm3EEixEm(%"struct.std::__2::array.115"* %pos_indices, i32 2) #8
  %call35 = call zeroext i1 @_ZNK5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEeqERKS2_(%"class.draco::IndexType"* %call33, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %call34)
  br i1 %call35, label %if.then40, label %lor.lhs.false36

lor.lhs.false36:                                  ; preds = %lor.lhs.false
  %call37 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZNSt3__25arrayIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELm3EEixEm(%"struct.std::__2::array.115"* %pos_indices, i32 1) #8
  %call38 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZNSt3__25arrayIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELm3EEixEm(%"struct.std::__2::array.115"* %pos_indices, i32 2) #8
  %call39 = call zeroext i1 @_ZNK5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEeqERKS2_(%"class.draco::IndexType"* %call37, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %call38)
  br i1 %call39, label %if.then40, label %if.else

if.then40:                                        ; preds = %lor.lhs.false36, %lor.lhs.false, %if.then29
  %27 = load i32, i32* %num_degenerated_faces, align 4
  %inc41 = add i32 %27, 1
  store i32 %inc41, i32* %num_degenerated_faces, align 4
  store i8 0, i8* %is_face_valid, align 1
  br label %if.end49

if.else:                                          ; preds = %lor.lhs.false36
  %28 = load i32, i32* %num_degenerated_faces, align 4
  %cmp42 = icmp ugt i32 %28, 0
  br i1 %cmp42, label %if.then43, label %if.end48

if.then43:                                        ; preds = %if.else
  %29 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %mesh.addr, align 4
  %call45 = call i32 @_ZNK5draco9IndexTypeIjNS_19FaceIndex_tag_type_EEmiERKj(%"class.draco::IndexType.116"* %f, i32* nonnull align 4 dereferenceable(4) %num_degenerated_faces)
  %coerce.dive46 = getelementptr inbounds %"class.draco::IndexType.116", %"class.draco::IndexType.116"* %agg.tmp44, i32 0, i32 0
  store i32 %call45, i32* %coerce.dive46, align 4
  %30 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %face, align 4
  %coerce.dive47 = getelementptr inbounds %"class.draco::IndexType.116", %"class.draco::IndexType.116"* %agg.tmp44, i32 0, i32 0
  %31 = load i32, i32* %coerce.dive47, align 4
  call void @_ZN5draco4Mesh7SetFaceENS_9IndexTypeIjNS_19FaceIndex_tag_type_EEERKNSt3__25arrayINS1_IjNS_20PointIndex_tag_type_EEELm3EEE(%"class.draco::Mesh"* %29, i32 %31, %"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %30)
  br label %if.end48

if.end48:                                         ; preds = %if.then43, %if.else
  br label %if.end49

if.end49:                                         ; preds = %if.end48, %if.then40
  br label %if.end50

if.end50:                                         ; preds = %if.end49, %for.end
  %32 = load %"struct.draco::MeshCleanupOptions"*, %"struct.draco::MeshCleanupOptions"** %options.addr, align 4
  %remove_unused_attributes51 = getelementptr inbounds %"struct.draco::MeshCleanupOptions", %"struct.draco::MeshCleanupOptions"* %32, i32 0, i32 1
  %33 = load i8, i8* %remove_unused_attributes51, align 1
  %tobool52 = trunc i8 %33 to i1
  br i1 %tobool52, label %land.lhs.true53, label %if.end74

land.lhs.true53:                                  ; preds = %if.end50
  %34 = load i8, i8* %is_face_valid, align 1
  %tobool54 = trunc i8 %34 to i1
  br i1 %tobool54, label %if.then55, label %if.end74

if.then55:                                        ; preds = %land.lhs.true53
  store i32 0, i32* %p56, align 4
  br label %for.cond57

for.cond57:                                       ; preds = %for.inc71, %if.then55
  %35 = load i32, i32* %p56, align 4
  %cmp58 = icmp slt i32 %35, 3
  br i1 %cmp58, label %for.body59, label %for.end73

for.body59:                                       ; preds = %for.cond57
  %36 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %face, align 4
  %37 = load i32, i32* %p56, align 4
  %call61 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.104"* @_ZNKSt3__25arrayIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELm3EEixEm(%"struct.std::__2::array"* %36, i32 %37) #8
  %call62 = call i32 @_ZNK5draco9IndexTypeIjNS_20PointIndex_tag_type_EE5valueEv(%"class.draco::IndexType.104"* %call61)
  call void @_ZNSt3__26vectorIbNS_9allocatorIbEEEixEm(%"class.std::__2::__bit_reference"* sret align 4 %ref.tmp60, %"class.std::__2::vector.110"* %is_point_used, i32 %call62)
  %call63 = call zeroext i1 @_ZNKSt3__215__bit_referenceINS_6vectorIbNS_9allocatorIbEEEELb1EEcvbEv(%"class.std::__2::__bit_reference"* %ref.tmp60) #8
  %lnot = xor i1 %call63, true
  br i1 %lnot, label %if.then64, label %if.end70

if.then64:                                        ; preds = %for.body59
  %38 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %face, align 4
  %39 = load i32, i32* %p56, align 4
  %call66 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.104"* @_ZNKSt3__25arrayIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELm3EEixEm(%"struct.std::__2::array"* %38, i32 %39) #8
  %call67 = call i32 @_ZNK5draco9IndexTypeIjNS_20PointIndex_tag_type_EE5valueEv(%"class.draco::IndexType.104"* %call66)
  call void @_ZNSt3__26vectorIbNS_9allocatorIbEEEixEm(%"class.std::__2::__bit_reference"* sret align 4 %ref.tmp65, %"class.std::__2::vector.110"* %is_point_used, i32 %call67)
  %call68 = call nonnull align 4 dereferenceable(8) %"class.std::__2::__bit_reference"* @_ZNSt3__215__bit_referenceINS_6vectorIbNS_9allocatorIbEEEELb1EEaSEb(%"class.std::__2::__bit_reference"* %ref.tmp65, i1 zeroext true) #8
  %40 = load i32, i32* %num_new_points, align 4
  %inc69 = add i32 %40, 1
  store i32 %inc69, i32* %num_new_points, align 4
  br label %if.end70

if.end70:                                         ; preds = %if.then64, %for.body59
  br label %for.inc71

for.inc71:                                        ; preds = %if.end70
  %41 = load i32, i32* %p56, align 4
  %inc72 = add nsw i32 %41, 1
  store i32 %inc72, i32* %p56, align 4
  br label %for.cond57

for.end73:                                        ; preds = %for.cond57
  br label %if.end74

if.end74:                                         ; preds = %for.end73, %land.lhs.true53, %if.end50
  br label %for.inc75

for.inc75:                                        ; preds = %if.end74
  %call76 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.116"* @_ZN5draco9IndexTypeIjNS_19FaceIndex_tag_type_EEppEv(%"class.draco::IndexType.116"* %f)
  br label %for.cond

for.end77:                                        ; preds = %for.cond
  %42 = load i32, i32* %num_degenerated_faces, align 4
  %cmp78 = icmp ugt i32 %42, 0
  br i1 %cmp78, label %if.then79, label %if.end81

if.then79:                                        ; preds = %for.end77
  %43 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %mesh.addr, align 4
  %44 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %mesh.addr, align 4
  %call80 = call i32 @_ZNK5draco4Mesh9num_facesEv(%"class.draco::Mesh"* %44)
  %45 = load i32, i32* %num_degenerated_faces, align 4
  %sub = sub i32 %call80, %45
  call void @_ZN5draco4Mesh11SetNumFacesEm(%"class.draco::Mesh"* %43, i32 %sub)
  br label %if.end81

if.end81:                                         ; preds = %if.then79, %for.end77
  %46 = load %"struct.draco::MeshCleanupOptions"*, %"struct.draco::MeshCleanupOptions"** %options.addr, align 4
  %remove_unused_attributes82 = getelementptr inbounds %"struct.draco::MeshCleanupOptions", %"struct.draco::MeshCleanupOptions"* %46, i32 0, i32 1
  %47 = load i8, i8* %remove_unused_attributes82, align 1
  %tobool83 = trunc i8 %47 to i1
  br i1 %tobool83, label %if.then84, label %if.end269

if.then84:                                        ; preds = %if.end81
  store i8 0, i8* %points_changed, align 1
  %48 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %mesh.addr, align 4
  %49 = bitcast %"class.draco::Mesh"* %48 to %"class.draco::PointCloud"*
  %call85 = call i32 @_ZNK5draco10PointCloud10num_pointsEv(%"class.draco::PointCloud"* %49)
  store i32 %call85, i32* %num_original_points, align 4
  %50 = load i32, i32* %num_original_points, align 4
  %call86 = call %"class.draco::IndexTypeVector.117"* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEES3_EC2Em(%"class.draco::IndexTypeVector.117"* %point_map, i32 %50)
  %51 = load i32, i32* %num_new_points, align 4
  %52 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %mesh.addr, align 4
  %53 = bitcast %"class.draco::Mesh"* %52 to %"class.draco::PointCloud"*
  %call87 = call i32 @_ZNK5draco10PointCloud10num_pointsEv(%"class.draco::PointCloud"* %53)
  %cmp88 = icmp ult i32 %51, %call87
  br i1 %cmp88, label %if.then89, label %if.else136

if.then89:                                        ; preds = %if.then84
  store i32 0, i32* %num_new_points, align 4
  %call90 = call %"class.draco::IndexType.104"* @_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEC2Ej(%"class.draco::IndexType.104"* %i, i32 0)
  br label %for.cond91

for.cond91:                                       ; preds = %for.inc106, %if.then89
  %call92 = call zeroext i1 @_ZNK5draco9IndexTypeIjNS_20PointIndex_tag_type_EEltERKj(%"class.draco::IndexType.104"* %i, i32* nonnull align 4 dereferenceable(4) %num_original_points)
  br i1 %call92, label %for.body93, label %for.end108

for.body93:                                       ; preds = %for.cond91
  %call95 = call i32 @_ZNK5draco9IndexTypeIjNS_20PointIndex_tag_type_EE5valueEv(%"class.draco::IndexType.104"* %i)
  call void @_ZNSt3__26vectorIbNS_9allocatorIbEEEixEm(%"class.std::__2::__bit_reference"* sret align 4 %ref.tmp94, %"class.std::__2::vector.110"* %is_point_used, i32 %call95)
  %call96 = call zeroext i1 @_ZNKSt3__215__bit_referenceINS_6vectorIbNS_9allocatorIbEEEELb1EEcvbEv(%"class.std::__2::__bit_reference"* %ref.tmp94) #8
  br i1 %call96, label %if.then97, label %if.else102

if.then97:                                        ; preds = %for.body93
  %54 = load i32, i32* %num_new_points, align 4
  %inc99 = add i32 %54, 1
  store i32 %inc99, i32* %num_new_points, align 4
  store i32 %54, i32* %ref.tmp98, align 4
  %call100 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.104"* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEES3_EixERKS3_(%"class.draco::IndexTypeVector.117"* %point_map, %"class.draco::IndexType.104"* nonnull align 4 dereferenceable(4) %i)
  %call101 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.104"* @_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEaSERKj(%"class.draco::IndexType.104"* %call100, i32* nonnull align 4 dereferenceable(4) %ref.tmp98)
  br label %if.end105

if.else102:                                       ; preds = %for.body93
  %call103 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.104"* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEES3_EixERKS3_(%"class.draco::IndexTypeVector.117"* %point_map, %"class.draco::IndexType.104"* nonnull align 4 dereferenceable(4) %i)
  %call104 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.104"* @_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEaSERKS2_(%"class.draco::IndexType.104"* %call103, %"class.draco::IndexType.104"* nonnull align 4 dereferenceable(4) @_ZN5dracoL18kInvalidPointIndexE)
  br label %if.end105

if.end105:                                        ; preds = %if.else102, %if.then97
  br label %for.inc106

for.inc106:                                       ; preds = %if.end105
  %call107 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.104"* @_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEppEv(%"class.draco::IndexType.104"* %i)
  br label %for.cond91

for.end108:                                       ; preds = %for.cond91
  %call110 = call %"class.draco::IndexType.116"* @_ZN5draco9IndexTypeIjNS_19FaceIndex_tag_type_EEC2Ej(%"class.draco::IndexType.116"* %f109, i32 0)
  br label %for.cond111

for.cond111:                                      ; preds = %for.inc133, %for.end108
  %55 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %mesh.addr, align 4
  %call113 = call i32 @_ZNK5draco4Mesh9num_facesEv(%"class.draco::Mesh"* %55)
  store i32 %call113, i32* %ref.tmp112, align 4
  %call114 = call zeroext i1 @_ZNK5draco9IndexTypeIjNS_19FaceIndex_tag_type_EEltERKj(%"class.draco::IndexType.116"* %f109, i32* nonnull align 4 dereferenceable(4) %ref.tmp112)
  br i1 %call114, label %for.body115, label %for.end135

for.body115:                                      ; preds = %for.cond111
  %56 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %mesh.addr, align 4
  %57 = bitcast %"class.draco::IndexType.116"* %agg.tmp117 to i8*
  %58 = bitcast %"class.draco::IndexType.116"* %f109 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %57, i8* align 4 %58, i32 4, i1 false)
  %coerce.dive118 = getelementptr inbounds %"class.draco::IndexType.116", %"class.draco::IndexType.116"* %agg.tmp117, i32 0, i32 0
  %59 = load i32, i32* %coerce.dive118, align 4
  %call119 = call nonnull align 4 dereferenceable(12) %"struct.std::__2::array"* @_ZNK5draco4Mesh4faceENS_9IndexTypeIjNS_19FaceIndex_tag_type_EEE(%"class.draco::Mesh"* %56, i32 %59)
  %60 = bitcast %"struct.std::__2::array"* %face116 to i8*
  %61 = bitcast %"struct.std::__2::array"* %call119 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %60, i8* align 4 %61, i32 12, i1 false)
  store i32 0, i32* %p120, align 4
  br label %for.cond121

for.cond121:                                      ; preds = %for.inc128, %for.body115
  %62 = load i32, i32* %p120, align 4
  %cmp122 = icmp slt i32 %62, 3
  br i1 %cmp122, label %for.body123, label %for.end130

for.body123:                                      ; preds = %for.cond121
  %63 = load i32, i32* %p120, align 4
  %call124 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.104"* @_ZNSt3__25arrayIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELm3EEixEm(%"struct.std::__2::array"* %face116, i32 %63) #8
  %call125 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.104"* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEES3_EixERKS3_(%"class.draco::IndexTypeVector.117"* %point_map, %"class.draco::IndexType.104"* nonnull align 4 dereferenceable(4) %call124)
  %64 = load i32, i32* %p120, align 4
  %call126 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.104"* @_ZNSt3__25arrayIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELm3EEixEm(%"struct.std::__2::array"* %face116, i32 %64) #8
  %call127 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.104"* @_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEaSERKS2_(%"class.draco::IndexType.104"* %call126, %"class.draco::IndexType.104"* nonnull align 4 dereferenceable(4) %call125)
  br label %for.inc128

for.inc128:                                       ; preds = %for.body123
  %65 = load i32, i32* %p120, align 4
  %inc129 = add nsw i32 %65, 1
  store i32 %inc129, i32* %p120, align 4
  br label %for.cond121

for.end130:                                       ; preds = %for.cond121
  %66 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %mesh.addr, align 4
  %67 = bitcast %"class.draco::IndexType.116"* %agg.tmp131 to i8*
  %68 = bitcast %"class.draco::IndexType.116"* %f109 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %67, i8* align 4 %68, i32 4, i1 false)
  %coerce.dive132 = getelementptr inbounds %"class.draco::IndexType.116", %"class.draco::IndexType.116"* %agg.tmp131, i32 0, i32 0
  %69 = load i32, i32* %coerce.dive132, align 4
  call void @_ZN5draco4Mesh7SetFaceENS_9IndexTypeIjNS_19FaceIndex_tag_type_EEERKNSt3__25arrayINS1_IjNS_20PointIndex_tag_type_EEELm3EEE(%"class.draco::Mesh"* %66, i32 %69, %"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %face116)
  br label %for.inc133

for.inc133:                                       ; preds = %for.end130
  %call134 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.116"* @_ZN5draco9IndexTypeIjNS_19FaceIndex_tag_type_EEppEv(%"class.draco::IndexType.116"* %f109)
  br label %for.cond111

for.end135:                                       ; preds = %for.cond111
  %70 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %mesh.addr, align 4
  %71 = bitcast %"class.draco::Mesh"* %70 to %"class.draco::PointCloud"*
  %72 = load i32, i32* %num_new_points, align 4
  call void @_ZN5draco10PointCloud14set_num_pointsEj(%"class.draco::PointCloud"* %71, i32 %72)
  store i8 1, i8* %points_changed, align 1
  br label %if.end147

if.else136:                                       ; preds = %if.then84
  %call138 = call %"class.draco::IndexType.104"* @_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEC2Ej(%"class.draco::IndexType.104"* %i137, i32 0)
  br label %for.cond139

for.cond139:                                      ; preds = %for.inc144, %if.else136
  %call140 = call zeroext i1 @_ZNK5draco9IndexTypeIjNS_20PointIndex_tag_type_EEltERKj(%"class.draco::IndexType.104"* %i137, i32* nonnull align 4 dereferenceable(4) %num_original_points)
  br i1 %call140, label %for.body141, label %for.end146

for.body141:                                      ; preds = %for.cond139
  %call142 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.104"* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEES3_EixERKS3_(%"class.draco::IndexTypeVector.117"* %point_map, %"class.draco::IndexType.104"* nonnull align 4 dereferenceable(4) %i137)
  %call143 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.104"* @_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEaSERKS2_(%"class.draco::IndexType.104"* %call142, %"class.draco::IndexType.104"* nonnull align 4 dereferenceable(4) %i137)
  br label %for.inc144

for.inc144:                                       ; preds = %for.body141
  %call145 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.104"* @_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEppEv(%"class.draco::IndexType.104"* %i137)
  br label %for.cond139

for.end146:                                       ; preds = %for.cond139
  br label %if.end147

if.end147:                                        ; preds = %for.end146, %for.end135
  %call148 = call %"class.draco::IndexTypeVector.125"* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEEhEC2Ev(%"class.draco::IndexTypeVector.125"* %is_att_index_used)
  %call149 = call %"class.draco::IndexTypeVector.126"* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEES3_EC2Ev(%"class.draco::IndexTypeVector.126"* %att_index_map)
  store i32 0, i32* %a, align 4
  br label %for.cond150

for.cond150:                                      ; preds = %for.inc263, %if.end147
  %73 = load i32, i32* %a, align 4
  %74 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %mesh.addr, align 4
  %75 = bitcast %"class.draco::Mesh"* %74 to %"class.draco::PointCloud"*
  %call151 = call i32 @_ZNK5draco10PointCloud14num_attributesEv(%"class.draco::PointCloud"* %75)
  %cmp152 = icmp slt i32 %73, %call151
  br i1 %cmp152, label %for.body153, label %for.end265

for.body153:                                      ; preds = %for.cond150
  %76 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %mesh.addr, align 4
  %77 = bitcast %"class.draco::Mesh"* %76 to %"class.draco::PointCloud"*
  %78 = load i32, i32* %a, align 4
  %call154 = call %"class.draco::PointAttribute"* @_ZN5draco10PointCloud9attributeEi(%"class.draco::PointCloud"* %77, i32 %78)
  store %"class.draco::PointAttribute"* %call154, %"class.draco::PointAttribute"** %att, align 4
  %79 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %att, align 4
  %call155 = call i32 @_ZNK5draco14PointAttribute4sizeEv(%"class.draco::PointAttribute"* %79)
  store i8 0, i8* %ref.tmp156, align 1
  call void @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEEhE6assignEmRKh(%"class.draco::IndexTypeVector.125"* %is_att_index_used, i32 %call155, i8* nonnull align 1 dereferenceable(1) %ref.tmp156)
  call void @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEES3_E5clearEv(%"class.draco::IndexTypeVector.126"* %att_index_map)
  store i32 0, i32* %num_used_entries, align 4
  %call158 = call %"class.draco::IndexType.104"* @_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEC2Ej(%"class.draco::IndexType.104"* %i157, i32 0)
  br label %for.cond159

for.cond159:                                      ; preds = %for.inc176, %for.body153
  %call160 = call zeroext i1 @_ZNK5draco9IndexTypeIjNS_20PointIndex_tag_type_EEltERKj(%"class.draco::IndexType.104"* %i157, i32* nonnull align 4 dereferenceable(4) %num_original_points)
  br i1 %call160, label %for.body161, label %for.end178

for.body161:                                      ; preds = %for.cond159
  %call162 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.104"* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEES3_EixERKS3_(%"class.draco::IndexTypeVector.117"* %point_map, %"class.draco::IndexType.104"* nonnull align 4 dereferenceable(4) %i157)
  %call163 = call zeroext i1 @_ZNK5draco9IndexTypeIjNS_20PointIndex_tag_type_EEneERKS2_(%"class.draco::IndexType.104"* %call162, %"class.draco::IndexType.104"* nonnull align 4 dereferenceable(4) @_ZN5dracoL18kInvalidPointIndexE)
  br i1 %call163, label %if.then164, label %if.end175

if.then164:                                       ; preds = %for.body161
  %80 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %att, align 4
  %81 = bitcast %"class.draco::IndexType.104"* %agg.tmp165 to i8*
  %82 = bitcast %"class.draco::IndexType.104"* %i157 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %81, i8* align 4 %82, i32 4, i1 false)
  %coerce.dive166 = getelementptr inbounds %"class.draco::IndexType.104", %"class.draco::IndexType.104"* %agg.tmp165, i32 0, i32 0
  %83 = load i32, i32* %coerce.dive166, align 4
  %call167 = call i32 @_ZNK5draco14PointAttribute12mapped_indexENS_9IndexTypeIjNS_20PointIndex_tag_type_EEE(%"class.draco::PointAttribute"* %80, i32 %83)
  %coerce.dive168 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %entry_id, i32 0, i32 0
  store i32 %call167, i32* %coerce.dive168, align 4
  %call169 = call nonnull align 1 dereferenceable(1) i8* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEEhEixERKS3_(%"class.draco::IndexTypeVector.125"* %is_att_index_used, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %entry_id)
  %84 = load i8, i8* %call169, align 1
  %tobool170 = icmp ne i8 %84, 0
  br i1 %tobool170, label %if.end174, label %if.then171

if.then171:                                       ; preds = %if.then164
  %call172 = call nonnull align 1 dereferenceable(1) i8* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEEhEixERKS3_(%"class.draco::IndexTypeVector.125"* %is_att_index_used, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %entry_id)
  store i8 1, i8* %call172, align 1
  %85 = load i32, i32* %num_used_entries, align 4
  %inc173 = add i32 %85, 1
  store i32 %inc173, i32* %num_used_entries, align 4
  br label %if.end174

if.end174:                                        ; preds = %if.then171, %if.then164
  br label %if.end175

if.end175:                                        ; preds = %if.end174, %for.body161
  br label %for.inc176

for.inc176:                                       ; preds = %if.end175
  %call177 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.104"* @_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEppEv(%"class.draco::IndexType.104"* %i157)
  br label %for.cond159

for.end178:                                       ; preds = %for.cond159
  store i8 0, i8* %att_indices_changed, align 1
  %86 = load i32, i32* %num_used_entries, align 4
  %87 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %att, align 4
  %call179 = call i32 @_ZNK5draco14PointAttribute4sizeEv(%"class.draco::PointAttribute"* %87)
  %cmp180 = icmp ult i32 %86, %call179
  br i1 %cmp180, label %if.then181, label %if.end212

if.then181:                                       ; preds = %for.end178
  %88 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %att, align 4
  %call182 = call i32 @_ZNK5draco14PointAttribute4sizeEv(%"class.draco::PointAttribute"* %88)
  call void @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEES3_E6resizeEm(%"class.draco::IndexTypeVector.126"* %att_index_map, i32 %call182)
  store i32 0, i32* %num_used_entries, align 4
  %call184 = call %"class.draco::IndexType"* @_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEC2Ej(%"class.draco::IndexType"* %i183, i32 0)
  br label %for.cond185

for.cond185:                                      ; preds = %for.inc209, %if.then181
  %89 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %att, align 4
  %call187 = call i32 @_ZNK5draco14PointAttribute4sizeEv(%"class.draco::PointAttribute"* %89)
  store i32 %call187, i32* %ref.tmp186, align 4
  %call188 = call zeroext i1 @_ZNK5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEltERKj(%"class.draco::IndexType"* %i183, i32* nonnull align 4 dereferenceable(4) %ref.tmp186)
  br i1 %call188, label %for.body189, label %for.end211

for.body189:                                      ; preds = %for.cond185
  %call190 = call nonnull align 1 dereferenceable(1) i8* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEEhEixERKS3_(%"class.draco::IndexTypeVector.125"* %is_att_index_used, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %i183)
  %90 = load i8, i8* %call190, align 1
  %tobool191 = icmp ne i8 %90, 0
  br i1 %tobool191, label %if.then192, label %if.end208

if.then192:                                       ; preds = %for.body189
  %call193 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEES3_EixERKS3_(%"class.draco::IndexTypeVector.126"* %att_index_map, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %i183)
  %call194 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEaSERKj(%"class.draco::IndexType"* %call193, i32* nonnull align 4 dereferenceable(4) %num_used_entries)
  %call195 = call zeroext i1 @_ZNK5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEgtERKj(%"class.draco::IndexType"* %i183, i32* nonnull align 4 dereferenceable(4) %num_used_entries)
  br i1 %call195, label %if.then196, label %if.end206

if.then196:                                       ; preds = %if.then192
  %91 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %att, align 4
  %92 = bitcast %"class.draco::PointAttribute"* %91 to %"class.draco::GeometryAttribute"*
  %93 = bitcast %"class.draco::IndexType"* %agg.tmp197 to i8*
  %94 = bitcast %"class.draco::IndexType"* %i183 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %93, i8* align 4 %94, i32 4, i1 false)
  %coerce.dive198 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %agg.tmp197, i32 0, i32 0
  %95 = load i32, i32* %coerce.dive198, align 4
  %call199 = call i8* @_ZN5draco17GeometryAttribute10GetAddressENS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEE(%"class.draco::GeometryAttribute"* %92, i32 %95)
  store i8* %call199, i8** %src_add, align 4
  %96 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %att, align 4
  %call200 = call %"class.draco::DataBuffer"* @_ZNK5draco14PointAttribute6bufferEv(%"class.draco::PointAttribute"* %96)
  %97 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %att, align 4
  %98 = bitcast %"class.draco::PointAttribute"* %97 to %"class.draco::GeometryAttribute"*
  %99 = load i32, i32* %num_used_entries, align 4
  %call202 = call %"class.draco::IndexType"* @_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEC2Ej(%"class.draco::IndexType"* %agg.tmp201, i32 %99)
  %coerce.dive203 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %agg.tmp201, i32 0, i32 0
  %100 = load i32, i32* %coerce.dive203, align 4
  %call204 = call i64 @_ZNK5draco17GeometryAttribute10GetBytePosENS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEE(%"class.draco::GeometryAttribute"* %98, i32 %100)
  %101 = load i8*, i8** %src_add, align 4
  %102 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %att, align 4
  %103 = bitcast %"class.draco::PointAttribute"* %102 to %"class.draco::GeometryAttribute"*
  %call205 = call i64 @_ZNK5draco17GeometryAttribute11byte_strideEv(%"class.draco::GeometryAttribute"* %103)
  %conv = trunc i64 %call205 to i32
  call void @_ZN5draco10DataBuffer5WriteExPKvm(%"class.draco::DataBuffer"* %call200, i64 %call204, i8* %101, i32 %conv)
  br label %if.end206

if.end206:                                        ; preds = %if.then196, %if.then192
  %104 = load i32, i32* %num_used_entries, align 4
  %inc207 = add i32 %104, 1
  store i32 %inc207, i32* %num_used_entries, align 4
  br label %if.end208

if.end208:                                        ; preds = %if.end206, %for.body189
  br label %for.inc209

for.inc209:                                       ; preds = %if.end208
  %call210 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEppEv(%"class.draco::IndexType"* %i183)
  br label %for.cond185

for.end211:                                       ; preds = %for.cond185
  %105 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %att, align 4
  %106 = load i32, i32* %num_used_entries, align 4
  call void @_ZN5draco14PointAttribute6ResizeEm(%"class.draco::PointAttribute"* %105, i32 %106)
  store i8 1, i8* %att_indices_changed, align 1
  br label %if.end212

if.end212:                                        ; preds = %for.end211, %for.end178
  %107 = load i8, i8* %points_changed, align 1
  %tobool213 = trunc i8 %107 to i1
  br i1 %tobool213, label %if.then216, label %lor.lhs.false214

lor.lhs.false214:                                 ; preds = %if.end212
  %108 = load i8, i8* %att_indices_changed, align 1
  %tobool215 = trunc i8 %108 to i1
  br i1 %tobool215, label %if.then216, label %if.end262

if.then216:                                       ; preds = %lor.lhs.false214, %if.end212
  %109 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %att, align 4
  %call217 = call zeroext i1 @_ZNK5draco14PointAttribute19is_mapping_identityEv(%"class.draco::PointAttribute"* %109)
  br i1 %call217, label %if.then218, label %if.end236

if.then218:                                       ; preds = %if.then216
  %110 = load i32, i32* %num_used_entries, align 4
  %111 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %mesh.addr, align 4
  %112 = bitcast %"class.draco::Mesh"* %111 to %"class.draco::PointCloud"*
  %call219 = call i32 @_ZNK5draco10PointCloud10num_pointsEv(%"class.draco::PointCloud"* %112)
  %cmp220 = icmp ne i32 %110, %call219
  br i1 %cmp220, label %if.then221, label %if.end235

if.then221:                                       ; preds = %if.then218
  %113 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %att, align 4
  %114 = load i32, i32* %num_original_points, align 4
  call void @_ZN5draco14PointAttribute18SetExplicitMappingEm(%"class.draco::PointAttribute"* %113, i32 %114)
  store i32 0, i32* %i222, align 4
  br label %for.cond223

for.cond223:                                      ; preds = %for.inc232, %if.then221
  %115 = load i32, i32* %i222, align 4
  %116 = load i32, i32* %num_original_points, align 4
  %cmp224 = icmp ult i32 %115, %116
  br i1 %cmp224, label %for.body225, label %for.end234

for.body225:                                      ; preds = %for.cond223
  %117 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %att, align 4
  %118 = load i32, i32* %i222, align 4
  %call227 = call %"class.draco::IndexType.104"* @_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEC2Ej(%"class.draco::IndexType.104"* %agg.tmp226, i32 %118)
  %119 = load i32, i32* %i222, align 4
  %call229 = call %"class.draco::IndexType"* @_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEC2Ej(%"class.draco::IndexType"* %agg.tmp228, i32 %119)
  %coerce.dive230 = getelementptr inbounds %"class.draco::IndexType.104", %"class.draco::IndexType.104"* %agg.tmp226, i32 0, i32 0
  %120 = load i32, i32* %coerce.dive230, align 4
  %coerce.dive231 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %agg.tmp228, i32 0, i32 0
  %121 = load i32, i32* %coerce.dive231, align 4
  call void @_ZN5draco14PointAttribute16SetPointMapEntryENS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEE(%"class.draco::PointAttribute"* %117, i32 %120, i32 %121)
  br label %for.inc232

for.inc232:                                       ; preds = %for.body225
  %122 = load i32, i32* %i222, align 4
  %inc233 = add i32 %122, 1
  store i32 %inc233, i32* %i222, align 4
  br label %for.cond223

for.end234:                                       ; preds = %for.cond223
  br label %if.end235

if.end235:                                        ; preds = %for.end234, %if.then218
  br label %if.end236

if.end236:                                        ; preds = %if.end235, %if.then216
  %123 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %att, align 4
  %call237 = call zeroext i1 @_ZNK5draco14PointAttribute19is_mapping_identityEv(%"class.draco::PointAttribute"* %123)
  br i1 %call237, label %if.end261, label %if.then238

if.then238:                                       ; preds = %if.end236
  %call240 = call %"class.draco::IndexType.104"* @_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEC2Ej(%"class.draco::IndexType.104"* %i239, i32 0)
  br label %for.cond241

for.cond241:                                      ; preds = %for.inc257, %if.then238
  %call242 = call zeroext i1 @_ZNK5draco9IndexTypeIjNS_20PointIndex_tag_type_EEltERKj(%"class.draco::IndexType.104"* %i239, i32* nonnull align 4 dereferenceable(4) %num_original_points)
  br i1 %call242, label %for.body243, label %for.end259

for.body243:                                      ; preds = %for.cond241
  %call244 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.104"* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEES3_EixERKS3_(%"class.draco::IndexTypeVector.117"* %point_map, %"class.draco::IndexType.104"* nonnull align 4 dereferenceable(4) %i239)
  %124 = bitcast %"class.draco::IndexType.104"* %new_point_id to i8*
  %125 = bitcast %"class.draco::IndexType.104"* %call244 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %124, i8* align 4 %125, i32 4, i1 false)
  %call245 = call zeroext i1 @_ZNK5draco9IndexTypeIjNS_20PointIndex_tag_type_EEeqERKS2_(%"class.draco::IndexType.104"* %new_point_id, %"class.draco::IndexType.104"* nonnull align 4 dereferenceable(4) @_ZN5dracoL18kInvalidPointIndexE)
  br i1 %call245, label %if.then246, label %if.end247

if.then246:                                       ; preds = %for.body243
  br label %for.inc257

if.end247:                                        ; preds = %for.body243
  %126 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %att, align 4
  %127 = bitcast %"class.draco::IndexType.104"* %agg.tmp248 to i8*
  %128 = bitcast %"class.draco::IndexType.104"* %i239 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %127, i8* align 4 %128, i32 4, i1 false)
  %coerce.dive249 = getelementptr inbounds %"class.draco::IndexType.104", %"class.draco::IndexType.104"* %agg.tmp248, i32 0, i32 0
  %129 = load i32, i32* %coerce.dive249, align 4
  %call250 = call i32 @_ZNK5draco14PointAttribute12mapped_indexENS_9IndexTypeIjNS_20PointIndex_tag_type_EEE(%"class.draco::PointAttribute"* %126, i32 %129)
  %coerce.dive251 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %original_entry_index, i32 0, i32 0
  store i32 %call250, i32* %coerce.dive251, align 4
  %call252 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEES3_EixERKS3_(%"class.draco::IndexTypeVector.126"* %att_index_map, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %original_entry_index)
  %130 = bitcast %"class.draco::IndexType"* %new_entry_index to i8*
  %131 = bitcast %"class.draco::IndexType"* %call252 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %130, i8* align 4 %131, i32 4, i1 false)
  %132 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %att, align 4
  %133 = bitcast %"class.draco::IndexType.104"* %agg.tmp253 to i8*
  %134 = bitcast %"class.draco::IndexType.104"* %new_point_id to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %133, i8* align 4 %134, i32 4, i1 false)
  %135 = bitcast %"class.draco::IndexType"* %agg.tmp254 to i8*
  %136 = bitcast %"class.draco::IndexType"* %new_entry_index to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %135, i8* align 4 %136, i32 4, i1 false)
  %coerce.dive255 = getelementptr inbounds %"class.draco::IndexType.104", %"class.draco::IndexType.104"* %agg.tmp253, i32 0, i32 0
  %137 = load i32, i32* %coerce.dive255, align 4
  %coerce.dive256 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %agg.tmp254, i32 0, i32 0
  %138 = load i32, i32* %coerce.dive256, align 4
  call void @_ZN5draco14PointAttribute16SetPointMapEntryENS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEE(%"class.draco::PointAttribute"* %132, i32 %137, i32 %138)
  br label %for.inc257

for.inc257:                                       ; preds = %if.end247, %if.then246
  %call258 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.104"* @_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEppEv(%"class.draco::IndexType.104"* %i239)
  br label %for.cond241

for.end259:                                       ; preds = %for.cond241
  %139 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %att, align 4
  %140 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %mesh.addr, align 4
  %141 = bitcast %"class.draco::Mesh"* %140 to %"class.draco::PointCloud"*
  %call260 = call i32 @_ZNK5draco10PointCloud10num_pointsEv(%"class.draco::PointCloud"* %141)
  call void @_ZN5draco14PointAttribute18SetExplicitMappingEm(%"class.draco::PointAttribute"* %139, i32 %call260)
  br label %if.end261

if.end261:                                        ; preds = %for.end259, %if.end236
  br label %if.end262

if.end262:                                        ; preds = %if.end261, %lor.lhs.false214
  br label %for.inc263

for.inc263:                                       ; preds = %if.end262
  %142 = load i32, i32* %a, align 4
  %inc264 = add nsw i32 %142, 1
  store i32 %inc264, i32* %a, align 4
  br label %for.cond150

for.end265:                                       ; preds = %for.cond150
  %call266 = call %"class.draco::IndexTypeVector.126"* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEES3_ED2Ev(%"class.draco::IndexTypeVector.126"* %att_index_map) #8
  %call267 = call %"class.draco::IndexTypeVector.125"* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEEhED2Ev(%"class.draco::IndexTypeVector.125"* %is_att_index_used) #8
  %call268 = call %"class.draco::IndexTypeVector.117"* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEES3_ED2Ev(%"class.draco::IndexTypeVector.117"* %point_map) #8
  br label %if.end269

if.end269:                                        ; preds = %for.end265, %if.end81
  store i1 true, i1* %retval, align 1
  %call270 = call %"class.std::__2::vector.110"* @_ZNSt3__26vectorIbNS_9allocatorIbEEED2Ev(%"class.std::__2::vector.110"* %is_point_used) #8
  br label %return

return:                                           ; preds = %if.end269, %if.then3, %if.then
  %143 = load i1, i1* %retval, align 1
  ret i1 %143
}

declare %"class.draco::PointAttribute"* @_ZNK5draco10PointCloud17GetNamedAttributeENS_17GeometryAttribute4TypeE(%"class.draco::PointCloud"*, i32) #1

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector.110"* @_ZNSt3__26vectorIbNS_9allocatorIbEEEC2Ev(%"class.std::__2::vector.110"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.110"*, align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp2 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::vector.110"* %this, %"class.std::__2::vector.110"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.110"*, %"class.std::__2::vector.110"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.110"* %this1 to %"class.std::__2::__vector_base_common"*
  %call = call %"class.std::__2::__vector_base_common"* @_ZNSt3__220__vector_base_commonILb1EEC2Ev(%"class.std::__2::__vector_base_common"* %0)
  %__begin_ = getelementptr inbounds %"class.std::__2::vector.110", %"class.std::__2::vector.110"* %this1, i32 0, i32 0
  store i32* null, i32** %__begin_, align 4
  %__size_ = getelementptr inbounds %"class.std::__2::vector.110", %"class.std::__2::vector.110"* %this1, i32 0, i32 1
  store i32 0, i32* %__size_, align 4
  %__cap_alloc_ = getelementptr inbounds %"class.std::__2::vector.110", %"class.std::__2::vector.110"* %this1, i32 0, i32 2
  store i32 0, i32* %ref.tmp, align 4
  %call3 = call %"class.std::__2::__compressed_pair.111"* @_ZNSt3__217__compressed_pairImNS_9allocatorImEEEC2IiNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.111"* %__cap_alloc_, i32* nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  ret %"class.std::__2::vector.110"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIbNS_9allocatorIbEEE6resizeEmb(%"class.std::__2::vector.110"* %this, i32 %__sz, i1 zeroext %__x) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.110"*, align 4
  %__sz.addr = alloca i32, align 4
  %__x.addr = alloca i8, align 1
  %__cs = alloca i32, align 4
  %__r = alloca %"class.std::__2::__bit_iterator", align 4
  %__c = alloca i32, align 4
  %__n = alloca i32, align 4
  %ref.tmp = alloca %"class.std::__2::__bit_iterator", align 4
  %__v = alloca %"class.std::__2::vector.110", align 4
  %ref.tmp8 = alloca %"class.std::__2::allocator.138", align 1
  %ref.tmp18 = alloca %"class.std::__2::__bit_iterator", align 4
  %agg.tmp = alloca %"class.std::__2::__bit_iterator.140", align 4
  %agg.tmp19 = alloca %"class.std::__2::__bit_iterator.140", align 4
  %agg.tmp20 = alloca %"class.std::__2::__bit_iterator", align 4
  %agg.tmp22 = alloca %"class.std::__2::__bit_iterator", align 4
  store %"class.std::__2::vector.110"* %this, %"class.std::__2::vector.110"** %this.addr, align 4
  store i32 %__sz, i32* %__sz.addr, align 4
  %frombool = zext i1 %__x to i8
  store i8 %frombool, i8* %__x.addr, align 1
  %this1 = load %"class.std::__2::vector.110"*, %"class.std::__2::vector.110"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIbNS_9allocatorIbEEE4sizeEv(%"class.std::__2::vector.110"* %this1) #8
  store i32 %call, i32* %__cs, align 4
  %0 = load i32, i32* %__cs, align 4
  %1 = load i32, i32* %__sz.addr, align 4
  %cmp = icmp ult i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else24

if.then:                                          ; preds = %entry
  %call2 = call %"class.std::__2::__bit_iterator"* @_ZNSt3__214__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb0ELm0EEC2Ev(%"class.std::__2::__bit_iterator"* %__r) #8
  %call3 = call i32 @_ZNKSt3__26vectorIbNS_9allocatorIbEEE8capacityEv(%"class.std::__2::vector.110"* %this1) #8
  store i32 %call3, i32* %__c, align 4
  %2 = load i32, i32* %__sz.addr, align 4
  %3 = load i32, i32* %__cs, align 4
  %sub = sub i32 %2, %3
  store i32 %sub, i32* %__n, align 4
  %4 = load i32, i32* %__n, align 4
  %5 = load i32, i32* %__c, align 4
  %cmp4 = icmp ule i32 %4, %5
  br i1 %cmp4, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %if.then
  %6 = load i32, i32* %__cs, align 4
  %7 = load i32, i32* %__c, align 4
  %8 = load i32, i32* %__n, align 4
  %sub5 = sub i32 %7, %8
  %cmp6 = icmp ule i32 %6, %sub5
  br i1 %cmp6, label %if.then7, label %if.else

if.then7:                                         ; preds = %land.lhs.true
  call void @_ZNSt3__26vectorIbNS_9allocatorIbEEE3endEv(%"class.std::__2::__bit_iterator"* sret align 4 %ref.tmp, %"class.std::__2::vector.110"* %this1) #8
  %9 = bitcast %"class.std::__2::__bit_iterator"* %__r to i8*
  %10 = bitcast %"class.std::__2::__bit_iterator"* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 8, i1 false)
  %11 = load i32, i32* %__n, align 4
  %__size_ = getelementptr inbounds %"class.std::__2::vector.110", %"class.std::__2::vector.110"* %this1, i32 0, i32 1
  %12 = load i32, i32* %__size_, align 4
  %add = add i32 %12, %11
  store i32 %add, i32* %__size_, align 4
  br label %if.end

if.else:                                          ; preds = %land.lhs.true, %if.then
  %call9 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.113"* @_ZNSt3__26vectorIbNS_9allocatorIbEEE7__allocEv(%"class.std::__2::vector.110"* %this1) #8
  %call10 = call %"class.std::__2::allocator.138"* @_ZNSt3__29allocatorIbEC2ImEERKNS0_IT_EE(%"class.std::__2::allocator.138"* %ref.tmp8, %"class.std::__2::allocator.113"* nonnull align 1 dereferenceable(1) %call9) #8
  %call11 = call %"class.std::__2::vector.110"* @_ZNSt3__26vectorIbNS_9allocatorIbEEEC2ERKS2_(%"class.std::__2::vector.110"* %__v, %"class.std::__2::allocator.138"* nonnull align 1 dereferenceable(1) %ref.tmp8) #8
  %__size_12 = getelementptr inbounds %"class.std::__2::vector.110", %"class.std::__2::vector.110"* %this1, i32 0, i32 1
  %13 = load i32, i32* %__size_12, align 4
  %14 = load i32, i32* %__n, align 4
  %add13 = add i32 %13, %14
  %call14 = call i32 @_ZNKSt3__26vectorIbNS_9allocatorIbEEE11__recommendEm(%"class.std::__2::vector.110"* %this1, i32 %add13)
  call void @_ZNSt3__26vectorIbNS_9allocatorIbEEE7reserveEm(%"class.std::__2::vector.110"* %__v, i32 %call14)
  %__size_15 = getelementptr inbounds %"class.std::__2::vector.110", %"class.std::__2::vector.110"* %this1, i32 0, i32 1
  %15 = load i32, i32* %__size_15, align 4
  %16 = load i32, i32* %__n, align 4
  %add16 = add i32 %15, %16
  %__size_17 = getelementptr inbounds %"class.std::__2::vector.110", %"class.std::__2::vector.110"* %__v, i32 0, i32 1
  store i32 %add16, i32* %__size_17, align 4
  call void @_ZNKSt3__26vectorIbNS_9allocatorIbEEE6cbeginEv(%"class.std::__2::__bit_iterator.140"* sret align 4 %agg.tmp, %"class.std::__2::vector.110"* %this1) #8
  call void @_ZNKSt3__26vectorIbNS_9allocatorIbEEE4cendEv(%"class.std::__2::__bit_iterator.140"* sret align 4 %agg.tmp19, %"class.std::__2::vector.110"* %this1) #8
  call void @_ZNSt3__26vectorIbNS_9allocatorIbEEE5beginEv(%"class.std::__2::__bit_iterator"* sret align 4 %agg.tmp20, %"class.std::__2::vector.110"* %__v) #8
  call void @_ZNSt3__24copyINS_6vectorIbNS_9allocatorIbEEEELb1EEENS_14__bit_iteratorIT_Lb0EXLi0EEEENS5_IS6_XT0_EXLi0EEEES8_S7_(%"class.std::__2::__bit_iterator"* sret align 4 %ref.tmp18, %"class.std::__2::__bit_iterator.140"* byval(%"class.std::__2::__bit_iterator.140") align 4 %agg.tmp, %"class.std::__2::__bit_iterator.140"* byval(%"class.std::__2::__bit_iterator.140") align 4 %agg.tmp19, %"class.std::__2::__bit_iterator"* %agg.tmp20)
  %17 = bitcast %"class.std::__2::__bit_iterator"* %__r to i8*
  %18 = bitcast %"class.std::__2::__bit_iterator"* %ref.tmp18 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %17, i8* align 4 %18, i32 8, i1 false)
  call void @_ZNSt3__26vectorIbNS_9allocatorIbEEE4swapERS3_(%"class.std::__2::vector.110"* %this1, %"class.std::__2::vector.110"* nonnull align 4 dereferenceable(12) %__v) #8
  %call21 = call %"class.std::__2::vector.110"* @_ZNSt3__26vectorIbNS_9allocatorIbEEED2Ev(%"class.std::__2::vector.110"* %__v) #8
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then7
  %call23 = call %"class.std::__2::__bit_iterator"* @_ZNSt3__214__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb0ELm0EEC2ERKS5_(%"class.std::__2::__bit_iterator"* %agg.tmp22, %"class.std::__2::__bit_iterator"* nonnull align 4 dereferenceable(8) %__r) #8
  %19 = load i32, i32* %__n, align 4
  %20 = load i8, i8* %__x.addr, align 1
  %tobool = trunc i8 %20 to i1
  call void @_ZNSt3__26fill_nINS_6vectorIbNS_9allocatorIbEEEEEEvNS_14__bit_iteratorIT_Lb0EXLi0EEEENS6_9size_typeEb(%"class.std::__2::__bit_iterator"* %agg.tmp22, i32 %19, i1 zeroext %tobool)
  br label %if.end26

if.else24:                                        ; preds = %entry
  %21 = load i32, i32* %__sz.addr, align 4
  %__size_25 = getelementptr inbounds %"class.std::__2::vector.110", %"class.std::__2::vector.110"* %this1, i32 0, i32 1
  store i32 %21, i32* %__size_25, align 4
  br label %if.end26

if.end26:                                         ; preds = %if.else24, %if.end
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco10PointCloud10num_pointsEv(%"class.draco::PointCloud"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointCloud"*, align 4
  store %"class.draco::PointCloud"* %this, %"class.draco::PointCloud"** %this.addr, align 4
  %this1 = load %"class.draco::PointCloud"*, %"class.draco::PointCloud"** %this.addr, align 4
  %num_points_ = getelementptr inbounds %"class.draco::PointCloud", %"class.draco::PointCloud"* %this1, i32 0, i32 4
  %0 = load i32, i32* %num_points_, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::array.115"* @_ZNSt3__25arrayIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELm3EEC2Ev(%"struct.std::__2::array.115"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"struct.std::__2::array.115"*, align 4
  %this.addr = alloca %"struct.std::__2::array.115"*, align 4
  store %"struct.std::__2::array.115"* %this, %"struct.std::__2::array.115"** %this.addr, align 4
  %this1 = load %"struct.std::__2::array.115"*, %"struct.std::__2::array.115"** %this.addr, align 4
  store %"struct.std::__2::array.115"* %this1, %"struct.std::__2::array.115"** %retval, align 4
  %__elems_ = getelementptr inbounds %"struct.std::__2::array.115", %"struct.std::__2::array.115"* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %"class.draco::IndexType"], [3 x %"class.draco::IndexType"]* %__elems_, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %"class.draco::IndexType"* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %"class.draco::IndexType"* @_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEC2Ev(%"class.draco::IndexType"* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %"class.draco::IndexType"* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %"struct.std::__2::array.115"*, %"struct.std::__2::array.115"** %retval, align 4
  ret %"struct.std::__2::array.115"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType.116"* @_ZN5draco9IndexTypeIjNS_19FaceIndex_tag_type_EEC2Ej(%"class.draco::IndexType.116"* returned %this, i32 %value) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.116"*, align 4
  %value.addr = alloca i32, align 4
  store %"class.draco::IndexType.116"* %this, %"class.draco::IndexType.116"** %this.addr, align 4
  store i32 %value, i32* %value.addr, align 4
  %this1 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.116", %"class.draco::IndexType.116"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value.addr, align 4
  store i32 %0, i32* %value_, align 4
  ret %"class.draco::IndexType.116"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK5draco9IndexTypeIjNS_19FaceIndex_tag_type_EEltERKj(%"class.draco::IndexType.116"* %this, i32* nonnull align 4 dereferenceable(4) %val) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.116"*, align 4
  %val.addr = alloca i32*, align 4
  store %"class.draco::IndexType.116"* %this, %"class.draco::IndexType.116"** %this.addr, align 4
  store i32* %val, i32** %val.addr, align 4
  %this1 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.116", %"class.draco::IndexType.116"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value_, align 4
  %1 = load i32*, i32** %val.addr, align 4
  %2 = load i32, i32* %1, align 4
  %cmp = icmp ult i32 %0, %2
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco4Mesh9num_facesEv(%"class.draco::Mesh"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::Mesh"*, align 4
  store %"class.draco::Mesh"* %this, %"class.draco::Mesh"** %this.addr, align 4
  %this1 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %this.addr, align 4
  %faces_ = getelementptr inbounds %"class.draco::Mesh", %"class.draco::Mesh"* %this1, i32 0, i32 2
  %call = call i32 @_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEENSt3__25arrayINS1_IjNS_20PointIndex_tag_type_EEELm3EEEE4sizeEv(%"class.draco::IndexTypeVector.101"* %faces_)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"struct.std::__2::array"* @_ZNK5draco4Mesh4faceENS_9IndexTypeIjNS_19FaceIndex_tag_type_EEE(%"class.draco::Mesh"* %this, i32 %face_id.coerce) #0 comdat {
entry:
  %face_id = alloca %"class.draco::IndexType.116", align 4
  %this.addr = alloca %"class.draco::Mesh"*, align 4
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType.116", %"class.draco::IndexType.116"* %face_id, i32 0, i32 0
  store i32 %face_id.coerce, i32* %coerce.dive, align 4
  store %"class.draco::Mesh"* %this, %"class.draco::Mesh"** %this.addr, align 4
  %this1 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %this.addr, align 4
  %faces_ = getelementptr inbounds %"class.draco::Mesh", %"class.draco::Mesh"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::array"* @_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEENSt3__25arrayINS1_IjNS_20PointIndex_tag_type_EEELm3EEEEixERKS3_(%"class.draco::IndexTypeVector.101"* %faces_, %"class.draco::IndexType.116"* nonnull align 4 dereferenceable(4) %face_id)
  ret %"struct.std::__2::array"* %call
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #2

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco14PointAttribute12mapped_indexENS_9IndexTypeIjNS_20PointIndex_tag_type_EEE(%"class.draco::PointAttribute"* %this, i32 %point_index.coerce) #0 comdat {
entry:
  %retval = alloca %"class.draco::IndexType", align 4
  %point_index = alloca %"class.draco::IndexType.104", align 4
  %this.addr = alloca %"class.draco::PointAttribute"*, align 4
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType.104", %"class.draco::IndexType.104"* %point_index, i32 0, i32 0
  store i32 %point_index.coerce, i32* %coerce.dive, align 4
  store %"class.draco::PointAttribute"* %this, %"class.draco::PointAttribute"** %this.addr, align 4
  %this1 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %this.addr, align 4
  %identity_mapping_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 4
  %0 = load i8, i8* %identity_mapping_, align 4
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call = call i32 @_ZNK5draco9IndexTypeIjNS_20PointIndex_tag_type_EE5valueEv(%"class.draco::IndexType.104"* %point_index)
  %call2 = call %"class.draco::IndexType"* @_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEC2Ej(%"class.draco::IndexType"* %retval, i32 %call)
  br label %return

if.end:                                           ; preds = %entry
  %indices_map_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 2
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEEEixERKS3_(%"class.draco::IndexTypeVector"* %indices_map_, %"class.draco::IndexType.104"* nonnull align 4 dereferenceable(4) %point_index)
  %1 = bitcast %"class.draco::IndexType"* %retval to i8*
  %2 = bitcast %"class.draco::IndexType"* %call3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 4, i1 false)
  br label %return

return:                                           ; preds = %if.end, %if.then
  %coerce.dive4 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %retval, i32 0, i32 0
  %3 = load i32, i32* %coerce.dive4, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.104"* @_ZNKSt3__25arrayIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELm3EEixEm(%"struct.std::__2::array"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::array"*, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::array"* %this, %"struct.std::__2::array"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %this.addr, align 4
  %__elems_ = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %this1, i32 0, i32 0
  %0 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %"class.draco::IndexType.104"], [3 x %"class.draco::IndexType.104"]* %__elems_, i32 0, i32 %0
  ret %"class.draco::IndexType.104"* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZNSt3__25arrayIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELm3EEixEm(%"struct.std::__2::array.115"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::array.115"*, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::array.115"* %this, %"struct.std::__2::array.115"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::array.115"*, %"struct.std::__2::array.115"** %this.addr, align 4
  %__elems_ = getelementptr inbounds %"struct.std::__2::array.115", %"struct.std::__2::array.115"* %this1, i32 0, i32 0
  %0 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %"class.draco::IndexType"], [3 x %"class.draco::IndexType"]* %__elems_, i32 0, i32 %0
  ret %"class.draco::IndexType"* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEaSERKS2_(%"class.draco::IndexType"* %this, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %i) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType"*, align 4
  %i.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.draco::IndexType"* %this, %"class.draco::IndexType"** %this.addr, align 4
  store %"class.draco::IndexType"* %i, %"class.draco::IndexType"** %i.addr, align 4
  %this1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %this.addr, align 4
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %i.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %0, i32 0, i32 0
  %1 = load i32, i32* %value_, align 4
  %value_2 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %this1, i32 0, i32 0
  store i32 %1, i32* %value_2, align 4
  ret %"class.draco::IndexType"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEeqERKS2_(%"class.draco::IndexType"* %this, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %i) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType"*, align 4
  %i.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.draco::IndexType"* %this, %"class.draco::IndexType"** %this.addr, align 4
  store %"class.draco::IndexType"* %i, %"class.draco::IndexType"** %i.addr, align 4
  %this1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value_, align 4
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %i.addr, align 4
  %value_2 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %1, i32 0, i32 0
  %2 = load i32, i32* %value_2, align 4
  %cmp = icmp eq i32 %0, %2
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco4Mesh7SetFaceENS_9IndexTypeIjNS_19FaceIndex_tag_type_EEERKNSt3__25arrayINS1_IjNS_20PointIndex_tag_type_EEELm3EEE(%"class.draco::Mesh"* %this, i32 %face_id.coerce, %"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %face) #0 comdat {
entry:
  %face_id = alloca %"class.draco::IndexType.116", align 4
  %this.addr = alloca %"class.draco::Mesh"*, align 4
  %face.addr = alloca %"struct.std::__2::array"*, align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp5 = alloca %"struct.std::__2::array", align 4
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType.116", %"class.draco::IndexType.116"* %face_id, i32 0, i32 0
  store i32 %face_id.coerce, i32* %coerce.dive, align 4
  store %"class.draco::Mesh"* %this, %"class.draco::Mesh"** %this.addr, align 4
  store %"struct.std::__2::array"* %face, %"struct.std::__2::array"** %face.addr, align 4
  %this1 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %this.addr, align 4
  %faces_ = getelementptr inbounds %"class.draco::Mesh", %"class.draco::Mesh"* %this1, i32 0, i32 2
  %call = call i32 @_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEENSt3__25arrayINS1_IjNS_20PointIndex_tag_type_EEELm3EEEE4sizeEv(%"class.draco::IndexTypeVector.101"* %faces_)
  store i32 %call, i32* %ref.tmp, align 4
  %call2 = call zeroext i1 @_ZNK5draco9IndexTypeIjNS_19FaceIndex_tag_type_EEgeERKj(%"class.draco::IndexType.116"* %face_id, i32* nonnull align 4 dereferenceable(4) %ref.tmp)
  br i1 %call2, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %faces_3 = getelementptr inbounds %"class.draco::Mesh", %"class.draco::Mesh"* %this1, i32 0, i32 2
  %call4 = call i32 @_ZNK5draco9IndexTypeIjNS_19FaceIndex_tag_type_EE5valueEv(%"class.draco::IndexType.116"* %face_id)
  %add = add i32 %call4, 1
  %0 = bitcast %"struct.std::__2::array"* %ref.tmp5 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %0, i8 0, i32 12, i1 false)
  %call6 = call %"struct.std::__2::array"* @_ZNSt3__25arrayIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELm3EEC2Ev(%"struct.std::__2::array"* %ref.tmp5)
  call void @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEENSt3__25arrayINS1_IjNS_20PointIndex_tag_type_EEELm3EEEE6resizeEmRKS8_(%"class.draco::IndexTypeVector.101"* %faces_3, i32 %add, %"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %ref.tmp5)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %face.addr, align 4
  %faces_7 = getelementptr inbounds %"class.draco::Mesh", %"class.draco::Mesh"* %this1, i32 0, i32 2
  %call8 = call nonnull align 4 dereferenceable(12) %"struct.std::__2::array"* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEENSt3__25arrayINS1_IjNS_20PointIndex_tag_type_EEELm3EEEEixERKS3_(%"class.draco::IndexTypeVector.101"* %faces_7, %"class.draco::IndexType.116"* nonnull align 4 dereferenceable(4) %face_id)
  %call9 = call nonnull align 4 dereferenceable(12) %"struct.std::__2::array"* @_ZNSt3__25arrayIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELm3EEaSERKS5_(%"struct.std::__2::array"* %call8, %"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco9IndexTypeIjNS_19FaceIndex_tag_type_EEmiERKj(%"class.draco::IndexType.116"* %this, i32* nonnull align 4 dereferenceable(4) %val) #0 comdat {
entry:
  %retval = alloca %"class.draco::IndexType.116", align 4
  %this.addr = alloca %"class.draco::IndexType.116"*, align 4
  %val.addr = alloca i32*, align 4
  store %"class.draco::IndexType.116"* %this, %"class.draco::IndexType.116"** %this.addr, align 4
  store i32* %val, i32** %val.addr, align 4
  %this1 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.116", %"class.draco::IndexType.116"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value_, align 4
  %1 = load i32*, i32** %val.addr, align 4
  %2 = load i32, i32* %1, align 4
  %sub = sub i32 %0, %2
  %call = call %"class.draco::IndexType.116"* @_ZN5draco9IndexTypeIjNS_19FaceIndex_tag_type_EEC2Ej(%"class.draco::IndexType.116"* %retval, i32 %sub)
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType.116", %"class.draco::IndexType.116"* %retval, i32 0, i32 0
  %3 = load i32, i32* %coerce.dive, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIbNS_9allocatorIbEEEixEm(%"class.std::__2::__bit_reference"* noalias sret align 4 %agg.result, %"class.std::__2::vector.110"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.110"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector.110"* %this, %"class.std::__2::vector.110"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.110"*, %"class.std::__2::vector.110"** %this.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__26vectorIbNS_9allocatorIbEEE10__make_refEm(%"class.std::__2::__bit_reference"* sret align 4 %agg.result, %"class.std::__2::vector.110"* %this1, i32 %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco9IndexTypeIjNS_20PointIndex_tag_type_EE5valueEv(%"class.draco::IndexType.104"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.104"*, align 4
  store %"class.draco::IndexType.104"* %this, %"class.draco::IndexType.104"** %this.addr, align 4
  %this1 = load %"class.draco::IndexType.104"*, %"class.draco::IndexType.104"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.104", %"class.draco::IndexType.104"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value_, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__215__bit_referenceINS_6vectorIbNS_9allocatorIbEEEELb1EEcvbEv(%"class.std::__2::__bit_reference"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__bit_reference"*, align 4
  store %"class.std::__2::__bit_reference"* %this, %"class.std::__2::__bit_reference"** %this.addr, align 4
  %this1 = load %"class.std::__2::__bit_reference"*, %"class.std::__2::__bit_reference"** %this.addr, align 4
  %__seg_ = getelementptr inbounds %"class.std::__2::__bit_reference", %"class.std::__2::__bit_reference"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__seg_, align 4
  %1 = load i32, i32* %0, align 4
  %__mask_ = getelementptr inbounds %"class.std::__2::__bit_reference", %"class.std::__2::__bit_reference"* %this1, i32 0, i32 1
  %2 = load i32, i32* %__mask_, align 4
  %and = and i32 %1, %2
  %tobool = icmp ne i32 %and, 0
  ret i1 %tobool
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(8) %"class.std::__2::__bit_reference"* @_ZNSt3__215__bit_referenceINS_6vectorIbNS_9allocatorIbEEEELb1EEaSEb(%"class.std::__2::__bit_reference"* %this, i1 zeroext %__x) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__bit_reference"*, align 4
  %__x.addr = alloca i8, align 1
  store %"class.std::__2::__bit_reference"* %this, %"class.std::__2::__bit_reference"** %this.addr, align 4
  %frombool = zext i1 %__x to i8
  store i8 %frombool, i8* %__x.addr, align 1
  %this1 = load %"class.std::__2::__bit_reference"*, %"class.std::__2::__bit_reference"** %this.addr, align 4
  %0 = load i8, i8* %__x.addr, align 1
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %__mask_ = getelementptr inbounds %"class.std::__2::__bit_reference", %"class.std::__2::__bit_reference"* %this1, i32 0, i32 1
  %1 = load i32, i32* %__mask_, align 4
  %__seg_ = getelementptr inbounds %"class.std::__2::__bit_reference", %"class.std::__2::__bit_reference"* %this1, i32 0, i32 0
  %2 = load i32*, i32** %__seg_, align 4
  %3 = load i32, i32* %2, align 4
  %or = or i32 %3, %1
  store i32 %or, i32* %2, align 4
  br label %if.end

if.else:                                          ; preds = %entry
  %__mask_2 = getelementptr inbounds %"class.std::__2::__bit_reference", %"class.std::__2::__bit_reference"* %this1, i32 0, i32 1
  %4 = load i32, i32* %__mask_2, align 4
  %neg = xor i32 %4, -1
  %__seg_3 = getelementptr inbounds %"class.std::__2::__bit_reference", %"class.std::__2::__bit_reference"* %this1, i32 0, i32 0
  %5 = load i32*, i32** %__seg_3, align 4
  %6 = load i32, i32* %5, align 4
  %and = and i32 %6, %neg
  store i32 %and, i32* %5, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret %"class.std::__2::__bit_reference"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.116"* @_ZN5draco9IndexTypeIjNS_19FaceIndex_tag_type_EEppEv(%"class.draco::IndexType.116"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.116"*, align 4
  store %"class.draco::IndexType.116"* %this, %"class.draco::IndexType.116"** %this.addr, align 4
  %this1 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.116", %"class.draco::IndexType.116"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value_, align 4
  %inc = add i32 %0, 1
  store i32 %inc, i32* %value_, align 4
  ret %"class.draco::IndexType.116"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco4Mesh11SetNumFacesEm(%"class.draco::Mesh"* %this, i32 %num_faces) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::Mesh"*, align 4
  %num_faces.addr = alloca i32, align 4
  %ref.tmp = alloca %"struct.std::__2::array", align 4
  store %"class.draco::Mesh"* %this, %"class.draco::Mesh"** %this.addr, align 4
  store i32 %num_faces, i32* %num_faces.addr, align 4
  %this1 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %this.addr, align 4
  %faces_ = getelementptr inbounds %"class.draco::Mesh", %"class.draco::Mesh"* %this1, i32 0, i32 2
  %0 = load i32, i32* %num_faces.addr, align 4
  %1 = bitcast %"struct.std::__2::array"* %ref.tmp to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %1, i8 0, i32 12, i1 false)
  %call = call %"struct.std::__2::array"* @_ZNSt3__25arrayIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELm3EEC2Ev(%"struct.std::__2::array"* %ref.tmp)
  call void @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEENSt3__25arrayINS1_IjNS_20PointIndex_tag_type_EEELm3EEEE6resizeEmRKS8_(%"class.draco::IndexTypeVector.101"* %faces_, i32 %0, %"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %ref.tmp)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexTypeVector.117"* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEES3_EC2Em(%"class.draco::IndexTypeVector.117"* returned %this, i32 %size) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector.117"*, align 4
  %size.addr = alloca i32, align 4
  store %"class.draco::IndexTypeVector.117"* %this, %"class.draco::IndexTypeVector.117"** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector.117"*, %"class.draco::IndexTypeVector.117"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector.117", %"class.draco::IndexTypeVector.117"* %this1, i32 0, i32 0
  %0 = load i32, i32* %size.addr, align 4
  %call = call %"class.std::__2::vector.118"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEEC2Em(%"class.std::__2::vector.118"* %vector_, i32 %0)
  ret %"class.draco::IndexTypeVector.117"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType.104"* @_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEC2Ej(%"class.draco::IndexType.104"* returned %this, i32 %value) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.104"*, align 4
  %value.addr = alloca i32, align 4
  store %"class.draco::IndexType.104"* %this, %"class.draco::IndexType.104"** %this.addr, align 4
  store i32 %value, i32* %value.addr, align 4
  %this1 = load %"class.draco::IndexType.104"*, %"class.draco::IndexType.104"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.104", %"class.draco::IndexType.104"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value.addr, align 4
  store i32 %0, i32* %value_, align 4
  ret %"class.draco::IndexType.104"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK5draco9IndexTypeIjNS_20PointIndex_tag_type_EEltERKj(%"class.draco::IndexType.104"* %this, i32* nonnull align 4 dereferenceable(4) %val) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.104"*, align 4
  %val.addr = alloca i32*, align 4
  store %"class.draco::IndexType.104"* %this, %"class.draco::IndexType.104"** %this.addr, align 4
  store i32* %val, i32** %val.addr, align 4
  %this1 = load %"class.draco::IndexType.104"*, %"class.draco::IndexType.104"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.104", %"class.draco::IndexType.104"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value_, align 4
  %1 = load i32*, i32** %val.addr, align 4
  %2 = load i32, i32* %1, align 4
  %cmp = icmp ult i32 %0, %2
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.104"* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEES3_EixERKS3_(%"class.draco::IndexTypeVector.117"* %this, %"class.draco::IndexType.104"* nonnull align 4 dereferenceable(4) %index) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector.117"*, align 4
  %index.addr = alloca %"class.draco::IndexType.104"*, align 4
  store %"class.draco::IndexTypeVector.117"* %this, %"class.draco::IndexTypeVector.117"** %this.addr, align 4
  store %"class.draco::IndexType.104"* %index, %"class.draco::IndexType.104"** %index.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector.117"*, %"class.draco::IndexTypeVector.117"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector.117", %"class.draco::IndexTypeVector.117"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexType.104"*, %"class.draco::IndexType.104"** %index.addr, align 4
  %call = call i32 @_ZNK5draco9IndexTypeIjNS_20PointIndex_tag_type_EE5valueEv(%"class.draco::IndexType.104"* %0)
  %call2 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.104"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEEixEm(%"class.std::__2::vector.118"* %vector_, i32 %call) #8
  ret %"class.draco::IndexType.104"* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.104"* @_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEaSERKj(%"class.draco::IndexType.104"* %this, i32* nonnull align 4 dereferenceable(4) %val) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.104"*, align 4
  %val.addr = alloca i32*, align 4
  store %"class.draco::IndexType.104"* %this, %"class.draco::IndexType.104"** %this.addr, align 4
  store i32* %val, i32** %val.addr, align 4
  %this1 = load %"class.draco::IndexType.104"*, %"class.draco::IndexType.104"** %this.addr, align 4
  %0 = load i32*, i32** %val.addr, align 4
  %1 = load i32, i32* %0, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.104", %"class.draco::IndexType.104"* %this1, i32 0, i32 0
  store i32 %1, i32* %value_, align 4
  ret %"class.draco::IndexType.104"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.104"* @_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEaSERKS2_(%"class.draco::IndexType.104"* %this, %"class.draco::IndexType.104"* nonnull align 4 dereferenceable(4) %i) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.104"*, align 4
  %i.addr = alloca %"class.draco::IndexType.104"*, align 4
  store %"class.draco::IndexType.104"* %this, %"class.draco::IndexType.104"** %this.addr, align 4
  store %"class.draco::IndexType.104"* %i, %"class.draco::IndexType.104"** %i.addr, align 4
  %this1 = load %"class.draco::IndexType.104"*, %"class.draco::IndexType.104"** %this.addr, align 4
  %0 = load %"class.draco::IndexType.104"*, %"class.draco::IndexType.104"** %i.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.104", %"class.draco::IndexType.104"* %0, i32 0, i32 0
  %1 = load i32, i32* %value_, align 4
  %value_2 = getelementptr inbounds %"class.draco::IndexType.104", %"class.draco::IndexType.104"* %this1, i32 0, i32 0
  store i32 %1, i32* %value_2, align 4
  ret %"class.draco::IndexType.104"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.104"* @_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEppEv(%"class.draco::IndexType.104"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.104"*, align 4
  store %"class.draco::IndexType.104"* %this, %"class.draco::IndexType.104"** %this.addr, align 4
  %this1 = load %"class.draco::IndexType.104"*, %"class.draco::IndexType.104"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.104", %"class.draco::IndexType.104"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value_, align 4
  %inc = add i32 %0, 1
  store i32 %inc, i32* %value_, align 4
  ret %"class.draco::IndexType.104"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.104"* @_ZNSt3__25arrayIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELm3EEixEm(%"struct.std::__2::array"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::array"*, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::array"* %this, %"struct.std::__2::array"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %this.addr, align 4
  %__elems_ = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %this1, i32 0, i32 0
  %0 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %"class.draco::IndexType.104"], [3 x %"class.draco::IndexType.104"]* %__elems_, i32 0, i32 %0
  ret %"class.draco::IndexType.104"* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco10PointCloud14set_num_pointsEj(%"class.draco::PointCloud"* %this, i32 %num) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointCloud"*, align 4
  %num.addr = alloca i32, align 4
  store %"class.draco::PointCloud"* %this, %"class.draco::PointCloud"** %this.addr, align 4
  store i32 %num, i32* %num.addr, align 4
  %this1 = load %"class.draco::PointCloud"*, %"class.draco::PointCloud"** %this.addr, align 4
  %0 = load i32, i32* %num.addr, align 4
  %num_points_ = getelementptr inbounds %"class.draco::PointCloud", %"class.draco::PointCloud"* %this1, i32 0, i32 4
  store i32 %0, i32* %num_points_, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexTypeVector.125"* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEEhEC2Ev(%"class.draco::IndexTypeVector.125"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector.125"*, align 4
  store %"class.draco::IndexTypeVector.125"* %this, %"class.draco::IndexTypeVector.125"** %this.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector.125"*, %"class.draco::IndexTypeVector.125"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector.125", %"class.draco::IndexTypeVector.125"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::vector.56"* @_ZNSt3__26vectorIhNS_9allocatorIhEEEC2Ev(%"class.std::__2::vector.56"* %vector_) #8
  ret %"class.draco::IndexTypeVector.125"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexTypeVector.126"* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEES3_EC2Ev(%"class.draco::IndexTypeVector.126"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector.126"*, align 4
  store %"class.draco::IndexTypeVector.126"* %this, %"class.draco::IndexTypeVector.126"** %this.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector.126"*, %"class.draco::IndexTypeVector.126"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector.126", %"class.draco::IndexTypeVector.126"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::vector.68"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEEC2Ev(%"class.std::__2::vector.68"* %vector_) #8
  ret %"class.draco::IndexTypeVector.126"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco10PointCloud14num_attributesEv(%"class.draco::PointCloud"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointCloud"*, align 4
  store %"class.draco::PointCloud"* %this, %"class.draco::PointCloud"** %this.addr, align 4
  %this1 = load %"class.draco::PointCloud"*, %"class.draco::PointCloud"** %this.addr, align 4
  %attributes_ = getelementptr inbounds %"class.draco::PointCloud", %"class.draco::PointCloud"* %this1, i32 0, i32 2
  %call = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.51"* %attributes_) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::PointAttribute"* @_ZN5draco10PointCloud9attributeEi(%"class.draco::PointCloud"* %this, i32 %att_id) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointCloud"*, align 4
  %att_id.addr = alloca i32, align 4
  store %"class.draco::PointCloud"* %this, %"class.draco::PointCloud"** %this.addr, align 4
  store i32 %att_id, i32* %att_id.addr, align 4
  %this1 = load %"class.draco::PointCloud"*, %"class.draco::PointCloud"** %this.addr, align 4
  %attributes_ = getelementptr inbounds %"class.draco::PointCloud", %"class.draco::PointCloud"* %this1, i32 0, i32 2
  %0 = load i32, i32* %att_id.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.53"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEixEm(%"class.std::__2::vector.51"* %attributes_, i32 %0) #8
  %call2 = call %"class.draco::PointAttribute"* @_ZNKSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEE3getEv(%"class.std::__2::unique_ptr.53"* %call) #8
  ret %"class.draco::PointAttribute"* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEEhE6assignEmRKh(%"class.draco::IndexTypeVector.125"* %this, i32 %size, i8* nonnull align 1 dereferenceable(1) %val) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector.125"*, align 4
  %size.addr = alloca i32, align 4
  %val.addr = alloca i8*, align 4
  store %"class.draco::IndexTypeVector.125"* %this, %"class.draco::IndexTypeVector.125"** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  store i8* %val, i8** %val.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector.125"*, %"class.draco::IndexTypeVector.125"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector.125", %"class.draco::IndexTypeVector.125"* %this1, i32 0, i32 0
  %0 = load i32, i32* %size.addr, align 4
  %1 = load i8*, i8** %val.addr, align 4
  call void @_ZNSt3__26vectorIhNS_9allocatorIhEEE6assignEmRKh(%"class.std::__2::vector.56"* %vector_, i32 %0, i8* nonnull align 1 dereferenceable(1) %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco14PointAttribute4sizeEv(%"class.draco::PointAttribute"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointAttribute"*, align 4
  store %"class.draco::PointAttribute"* %this, %"class.draco::PointAttribute"** %this.addr, align 4
  %this1 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %this.addr, align 4
  %num_unique_entries_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 3
  %0 = load i32, i32* %num_unique_entries_, align 8
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEES3_E5clearEv(%"class.draco::IndexTypeVector.126"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector.126"*, align 4
  store %"class.draco::IndexTypeVector.126"* %this, %"class.draco::IndexTypeVector.126"** %this.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector.126"*, %"class.draco::IndexTypeVector.126"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector.126", %"class.draco::IndexTypeVector.126"* %this1, i32 0, i32 0
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5clearEv(%"class.std::__2::vector.68"* %vector_) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK5draco9IndexTypeIjNS_20PointIndex_tag_type_EEneERKS2_(%"class.draco::IndexType.104"* %this, %"class.draco::IndexType.104"* nonnull align 4 dereferenceable(4) %i) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.104"*, align 4
  %i.addr = alloca %"class.draco::IndexType.104"*, align 4
  store %"class.draco::IndexType.104"* %this, %"class.draco::IndexType.104"** %this.addr, align 4
  store %"class.draco::IndexType.104"* %i, %"class.draco::IndexType.104"** %i.addr, align 4
  %this1 = load %"class.draco::IndexType.104"*, %"class.draco::IndexType.104"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.104", %"class.draco::IndexType.104"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value_, align 4
  %1 = load %"class.draco::IndexType.104"*, %"class.draco::IndexType.104"** %i.addr, align 4
  %value_2 = getelementptr inbounds %"class.draco::IndexType.104", %"class.draco::IndexType.104"* %1, i32 0, i32 0
  %2 = load i32, i32* %value_2, align 4
  %cmp = icmp ne i32 %0, %2
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) i8* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEEhEixERKS3_(%"class.draco::IndexTypeVector.125"* %this, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %index) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector.125"*, align 4
  %index.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.draco::IndexTypeVector.125"* %this, %"class.draco::IndexTypeVector.125"** %this.addr, align 4
  store %"class.draco::IndexType"* %index, %"class.draco::IndexType"** %index.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector.125"*, %"class.draco::IndexTypeVector.125"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector.125", %"class.draco::IndexTypeVector.125"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %index.addr, align 4
  %call = call i32 @_ZNK5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EE5valueEv(%"class.draco::IndexType"* %0)
  %call2 = call nonnull align 1 dereferenceable(1) i8* @_ZNSt3__26vectorIhNS_9allocatorIhEEEixEm(%"class.std::__2::vector.56"* %vector_, i32 %call) #8
  ret i8* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEES3_E6resizeEm(%"class.draco::IndexTypeVector.126"* %this, i32 %size) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector.126"*, align 4
  %size.addr = alloca i32, align 4
  store %"class.draco::IndexTypeVector.126"* %this, %"class.draco::IndexTypeVector.126"** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector.126"*, %"class.draco::IndexTypeVector.126"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector.126", %"class.draco::IndexTypeVector.126"* %this1, i32 0, i32 0
  %0 = load i32, i32* %size.addr, align 4
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE6resizeEm(%"class.std::__2::vector.68"* %vector_, i32 %0)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType"* @_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEC2Ej(%"class.draco::IndexType"* returned %this, i32 %value) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType"*, align 4
  %value.addr = alloca i32, align 4
  store %"class.draco::IndexType"* %this, %"class.draco::IndexType"** %this.addr, align 4
  store i32 %value, i32* %value.addr, align 4
  %this1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value.addr, align 4
  store i32 %0, i32* %value_, align 4
  ret %"class.draco::IndexType"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEltERKj(%"class.draco::IndexType"* %this, i32* nonnull align 4 dereferenceable(4) %val) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType"*, align 4
  %val.addr = alloca i32*, align 4
  store %"class.draco::IndexType"* %this, %"class.draco::IndexType"** %this.addr, align 4
  store i32* %val, i32** %val.addr, align 4
  %this1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value_, align 4
  %1 = load i32*, i32** %val.addr, align 4
  %2 = load i32, i32* %1, align 4
  %cmp = icmp ult i32 %0, %2
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEES3_EixERKS3_(%"class.draco::IndexTypeVector.126"* %this, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %index) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector.126"*, align 4
  %index.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.draco::IndexTypeVector.126"* %this, %"class.draco::IndexTypeVector.126"** %this.addr, align 4
  store %"class.draco::IndexType"* %index, %"class.draco::IndexType"** %index.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector.126"*, %"class.draco::IndexTypeVector.126"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector.126", %"class.draco::IndexTypeVector.126"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %index.addr, align 4
  %call = call i32 @_ZNK5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EE5valueEv(%"class.draco::IndexType"* %0)
  %call2 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEEixEm(%"class.std::__2::vector.68"* %vector_, i32 %call) #8
  ret %"class.draco::IndexType"* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEaSERKj(%"class.draco::IndexType"* %this, i32* nonnull align 4 dereferenceable(4) %val) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType"*, align 4
  %val.addr = alloca i32*, align 4
  store %"class.draco::IndexType"* %this, %"class.draco::IndexType"** %this.addr, align 4
  store i32* %val, i32** %val.addr, align 4
  %this1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %this.addr, align 4
  %0 = load i32*, i32** %val.addr, align 4
  %1 = load i32, i32* %0, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %this1, i32 0, i32 0
  store i32 %1, i32* %value_, align 4
  ret %"class.draco::IndexType"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEgtERKj(%"class.draco::IndexType"* %this, i32* nonnull align 4 dereferenceable(4) %val) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType"*, align 4
  %val.addr = alloca i32*, align 4
  store %"class.draco::IndexType"* %this, %"class.draco::IndexType"** %this.addr, align 4
  store i32* %val, i32** %val.addr, align 4
  %this1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value_, align 4
  %1 = load i32*, i32** %val.addr, align 4
  %2 = load i32, i32* %1, align 4
  %cmp = icmp ugt i32 %0, %2
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN5draco17GeometryAttribute10GetAddressENS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEE(%"class.draco::GeometryAttribute"* %this, i32 %att_index.coerce) #0 comdat {
entry:
  %att_index = alloca %"class.draco::IndexType", align 4
  %this.addr = alloca %"class.draco::GeometryAttribute"*, align 4
  %byte_pos = alloca i64, align 8
  %agg.tmp = alloca %"class.draco::IndexType", align 4
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %att_index, i32 0, i32 0
  store i32 %att_index.coerce, i32* %coerce.dive, align 4
  store %"class.draco::GeometryAttribute"* %this, %"class.draco::GeometryAttribute"** %this.addr, align 4
  %this1 = load %"class.draco::GeometryAttribute"*, %"class.draco::GeometryAttribute"** %this.addr, align 4
  %0 = bitcast %"class.draco::IndexType"* %agg.tmp to i8*
  %1 = bitcast %"class.draco::IndexType"* %att_index to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %0, i8* align 4 %1, i32 4, i1 false)
  %coerce.dive2 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %agg.tmp, i32 0, i32 0
  %2 = load i32, i32* %coerce.dive2, align 4
  %call = call i64 @_ZNK5draco17GeometryAttribute10GetBytePosENS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEE(%"class.draco::GeometryAttribute"* %this1, i32 %2)
  store i64 %call, i64* %byte_pos, align 8
  %buffer_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 0
  %3 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %buffer_, align 8
  %call3 = call i8* @_ZN5draco10DataBuffer4dataEv(%"class.draco::DataBuffer"* %3)
  %4 = load i64, i64* %byte_pos, align 8
  %idx.ext = trunc i64 %4 to i32
  %add.ptr = getelementptr inbounds i8, i8* %call3, i32 %idx.ext
  ret i8* %add.ptr
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::DataBuffer"* @_ZNK5draco14PointAttribute6bufferEv(%"class.draco::PointAttribute"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointAttribute"*, align 4
  store %"class.draco::PointAttribute"* %this, %"class.draco::PointAttribute"** %this.addr, align 4
  %this1 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %this.addr, align 4
  %attribute_buffer_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 1
  %call = call %"class.draco::DataBuffer"* @_ZNKSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEE3getEv(%"class.std::__2::unique_ptr.63"* %attribute_buffer_) #8
  ret %"class.draco::DataBuffer"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco10DataBuffer5WriteExPKvm(%"class.draco::DataBuffer"* %this, i64 %byte_pos, i8* %in_data, i32 %data_size) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::DataBuffer"*, align 4
  %byte_pos.addr = alloca i64, align 8
  %in_data.addr = alloca i8*, align 4
  %data_size.addr = alloca i32, align 4
  store %"class.draco::DataBuffer"* %this, %"class.draco::DataBuffer"** %this.addr, align 4
  store i64 %byte_pos, i64* %byte_pos.addr, align 8
  store i8* %in_data, i8** %in_data.addr, align 4
  store i32 %data_size, i32* %data_size.addr, align 4
  %this1 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %this.addr, align 4
  %call = call i8* @_ZN5draco10DataBuffer4dataEv(%"class.draco::DataBuffer"* %this1)
  %0 = load i64, i64* %byte_pos.addr, align 8
  %idx.ext = trunc i64 %0 to i32
  %add.ptr = getelementptr inbounds i8, i8* %call, i32 %idx.ext
  %1 = load i8*, i8** %in_data.addr, align 4
  %2 = load i32, i32* %data_size.addr, align 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %add.ptr, i8* align 1 %1, i32 %2, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i64 @_ZNK5draco17GeometryAttribute10GetBytePosENS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEE(%"class.draco::GeometryAttribute"* %this, i32 %att_index.coerce) #0 comdat {
entry:
  %att_index = alloca %"class.draco::IndexType", align 4
  %this.addr = alloca %"class.draco::GeometryAttribute"*, align 4
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %att_index, i32 0, i32 0
  store i32 %att_index.coerce, i32* %coerce.dive, align 4
  store %"class.draco::GeometryAttribute"* %this, %"class.draco::GeometryAttribute"** %this.addr, align 4
  %this1 = load %"class.draco::GeometryAttribute"*, %"class.draco::GeometryAttribute"** %this.addr, align 4
  %byte_offset_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 6
  %0 = load i64, i64* %byte_offset_, align 8
  %byte_stride_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 5
  %1 = load i64, i64* %byte_stride_, align 8
  %call = call i32 @_ZNK5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EE5valueEv(%"class.draco::IndexType"* %att_index)
  %conv = zext i32 %call to i64
  %mul = mul nsw i64 %1, %conv
  %add = add nsw i64 %0, %mul
  ret i64 %add
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i64 @_ZNK5draco17GeometryAttribute11byte_strideEv(%"class.draco::GeometryAttribute"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::GeometryAttribute"*, align 4
  store %"class.draco::GeometryAttribute"* %this, %"class.draco::GeometryAttribute"** %this.addr, align 4
  %this1 = load %"class.draco::GeometryAttribute"*, %"class.draco::GeometryAttribute"** %this.addr, align 4
  %byte_stride_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 5
  %0 = load i64, i64* %byte_stride_, align 8
  ret i64 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEppEv(%"class.draco::IndexType"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.draco::IndexType"* %this, %"class.draco::IndexType"** %this.addr, align 4
  %this1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value_, align 4
  %inc = add i32 %0, 1
  store i32 %inc, i32* %value_, align 4
  ret %"class.draco::IndexType"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco14PointAttribute6ResizeEm(%"class.draco::PointAttribute"* %this, i32 %new_num_unique_entries) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointAttribute"*, align 4
  %new_num_unique_entries.addr = alloca i32, align 4
  store %"class.draco::PointAttribute"* %this, %"class.draco::PointAttribute"** %this.addr, align 4
  store i32 %new_num_unique_entries, i32* %new_num_unique_entries.addr, align 4
  %this1 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %this.addr, align 4
  %0 = load i32, i32* %new_num_unique_entries.addr, align 4
  %num_unique_entries_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 3
  store i32 %0, i32* %num_unique_entries_, align 8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK5draco14PointAttribute19is_mapping_identityEv(%"class.draco::PointAttribute"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointAttribute"*, align 4
  store %"class.draco::PointAttribute"* %this, %"class.draco::PointAttribute"** %this.addr, align 4
  %this1 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %this.addr, align 4
  %identity_mapping_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 4
  %0 = load i8, i8* %identity_mapping_, align 4
  %tobool = trunc i8 %0 to i1
  ret i1 %tobool
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco14PointAttribute18SetExplicitMappingEm(%"class.draco::PointAttribute"* %this, i32 %num_points) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointAttribute"*, align 4
  %num_points.addr = alloca i32, align 4
  store %"class.draco::PointAttribute"* %this, %"class.draco::PointAttribute"** %this.addr, align 4
  store i32 %num_points, i32* %num_points.addr, align 4
  %this1 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %this.addr, align 4
  %identity_mapping_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 4
  store i8 0, i8* %identity_mapping_, align 4
  %indices_map_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 2
  %0 = load i32, i32* %num_points.addr, align 4
  call void @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEEE6resizeEmRKS5_(%"class.draco::IndexTypeVector"* %indices_map_, i32 %0, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) @_ZN5dracoL27kInvalidAttributeValueIndexE)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco14PointAttribute16SetPointMapEntryENS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEE(%"class.draco::PointAttribute"* %this, i32 %point_index.coerce, i32 %entry_index.coerce) #0 comdat {
entry:
  %point_index = alloca %"class.draco::IndexType.104", align 4
  %entry_index = alloca %"class.draco::IndexType", align 4
  %this.addr = alloca %"class.draco::PointAttribute"*, align 4
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType.104", %"class.draco::IndexType.104"* %point_index, i32 0, i32 0
  store i32 %point_index.coerce, i32* %coerce.dive, align 4
  %coerce.dive1 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %entry_index, i32 0, i32 0
  store i32 %entry_index.coerce, i32* %coerce.dive1, align 4
  store %"class.draco::PointAttribute"* %this, %"class.draco::PointAttribute"** %this.addr, align 4
  %this2 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %this.addr, align 4
  %indices_map_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this2, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEEEixERKS3_(%"class.draco::IndexTypeVector"* %indices_map_, %"class.draco::IndexType.104"* nonnull align 4 dereferenceable(4) %point_index)
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEaSERKS2_(%"class.draco::IndexType"* %call, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %entry_index)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK5draco9IndexTypeIjNS_20PointIndex_tag_type_EEeqERKS2_(%"class.draco::IndexType.104"* %this, %"class.draco::IndexType.104"* nonnull align 4 dereferenceable(4) %i) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.104"*, align 4
  %i.addr = alloca %"class.draco::IndexType.104"*, align 4
  store %"class.draco::IndexType.104"* %this, %"class.draco::IndexType.104"** %this.addr, align 4
  store %"class.draco::IndexType.104"* %i, %"class.draco::IndexType.104"** %i.addr, align 4
  %this1 = load %"class.draco::IndexType.104"*, %"class.draco::IndexType.104"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.104", %"class.draco::IndexType.104"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value_, align 4
  %1 = load %"class.draco::IndexType.104"*, %"class.draco::IndexType.104"** %i.addr, align 4
  %value_2 = getelementptr inbounds %"class.draco::IndexType.104", %"class.draco::IndexType.104"* %1, i32 0, i32 0
  %2 = load i32, i32* %value_2, align 4
  %cmp = icmp eq i32 %0, %2
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexTypeVector.126"* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEES3_ED2Ev(%"class.draco::IndexTypeVector.126"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector.126"*, align 4
  store %"class.draco::IndexTypeVector.126"* %this, %"class.draco::IndexTypeVector.126"** %this.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector.126"*, %"class.draco::IndexTypeVector.126"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector.126", %"class.draco::IndexTypeVector.126"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::vector.68"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEED2Ev(%"class.std::__2::vector.68"* %vector_) #8
  ret %"class.draco::IndexTypeVector.126"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexTypeVector.125"* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEEhED2Ev(%"class.draco::IndexTypeVector.125"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector.125"*, align 4
  store %"class.draco::IndexTypeVector.125"* %this, %"class.draco::IndexTypeVector.125"** %this.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector.125"*, %"class.draco::IndexTypeVector.125"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector.125", %"class.draco::IndexTypeVector.125"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::vector.56"* @_ZNSt3__26vectorIhNS_9allocatorIhEEED2Ev(%"class.std::__2::vector.56"* %vector_) #8
  ret %"class.draco::IndexTypeVector.125"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexTypeVector.117"* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEES3_ED2Ev(%"class.draco::IndexTypeVector.117"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector.117"*, align 4
  store %"class.draco::IndexTypeVector.117"* %this, %"class.draco::IndexTypeVector.117"** %this.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector.117"*, %"class.draco::IndexTypeVector.117"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector.117", %"class.draco::IndexTypeVector.117"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::vector.118"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEED2Ev(%"class.std::__2::vector.118"* %vector_) #8
  ret %"class.draco::IndexTypeVector.117"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector.110"* @_ZNSt3__26vectorIbNS_9allocatorIbEEED2Ev(%"class.std::__2::vector.110"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"class.std::__2::vector.110"*, align 4
  %this.addr = alloca %"class.std::__2::vector.110"*, align 4
  store %"class.std::__2::vector.110"* %this, %"class.std::__2::vector.110"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.110"*, %"class.std::__2::vector.110"** %this.addr, align 4
  store %"class.std::__2::vector.110"* %this1, %"class.std::__2::vector.110"** %retval, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::vector.110", %"class.std::__2::vector.110"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__begin_, align 4
  %cmp = icmp ne i32* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.113"* @_ZNSt3__26vectorIbNS_9allocatorIbEEE7__allocEv(%"class.std::__2::vector.110"* %this1) #8
  %__begin_2 = getelementptr inbounds %"class.std::__2::vector.110", %"class.std::__2::vector.110"* %this1, i32 0, i32 0
  %1 = load i32*, i32** %__begin_2, align 4
  %call3 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIbNS_9allocatorIbEEE5__capEv(%"class.std::__2::vector.110"* %this1) #8
  %2 = load i32, i32* %call3, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorImEEE10deallocateERS2_Pmm(%"class.std::__2::allocator.113"* nonnull align 1 dereferenceable(1) %call, i32* %1, i32 %2) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  call void @_ZNSt3__26vectorIbNS_9allocatorIbEEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector.110"* %this1)
  %3 = load %"class.std::__2::vector.110"*, %"class.std::__2::vector.110"** %retval, align 4
  ret %"class.std::__2::vector.110"* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType"* @_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEC2Ev(%"class.draco::IndexType"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.draco::IndexType"* %this, %"class.draco::IndexType"** %this.addr, align 4
  %this1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %this1, i32 0, i32 0
  store i32 0, i32* %value_, align 4
  ret %"class.draco::IndexType"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEENSt3__25arrayINS1_IjNS_20PointIndex_tag_type_EEELm3EEEE4sizeEv(%"class.draco::IndexTypeVector.101"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector.101"*, align 4
  store %"class.draco::IndexTypeVector.101"* %this, %"class.draco::IndexTypeVector.101"** %this.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector.101"*, %"class.draco::IndexTypeVector.101"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector.101", %"class.draco::IndexTypeVector.101"* %this1, i32 0, i32 0
  %call = call i32 @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.102"* %vector_) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.102"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.102"*, align 4
  store %"class.std::__2::vector.102"* %this, %"class.std::__2::vector.102"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.102"*, %"class.std::__2::vector.102"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.102"* %this1 to %"class.std::__2::__vector_base.103"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.103", %"class.std::__2::__vector_base.103"* %0, i32 0, i32 1
  %1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector.102"* %this1 to %"class.std::__2::__vector_base.103"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.103", %"class.std::__2::__vector_base.103"* %2, i32 0, i32 0
  %3 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"struct.std::__2::array"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"struct.std::__2::array"* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 12
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"struct.std::__2::array"* @_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEENSt3__25arrayINS1_IjNS_20PointIndex_tag_type_EEELm3EEEEixERKS3_(%"class.draco::IndexTypeVector.101"* %this, %"class.draco::IndexType.116"* nonnull align 4 dereferenceable(4) %index) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector.101"*, align 4
  %index.addr = alloca %"class.draco::IndexType.116"*, align 4
  store %"class.draco::IndexTypeVector.101"* %this, %"class.draco::IndexTypeVector.101"** %this.addr, align 4
  store %"class.draco::IndexType.116"* %index, %"class.draco::IndexType.116"** %index.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector.101"*, %"class.draco::IndexTypeVector.101"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector.101", %"class.draco::IndexTypeVector.101"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %index.addr, align 4
  %call = call i32 @_ZNK5draco9IndexTypeIjNS_19FaceIndex_tag_type_EE5valueEv(%"class.draco::IndexType.116"* %0)
  %call2 = call nonnull align 4 dereferenceable(12) %"struct.std::__2::array"* @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEEixEm(%"class.std::__2::vector.102"* %vector_, i32 %call) #8
  ret %"struct.std::__2::array"* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"struct.std::__2::array"* @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEEixEm(%"class.std::__2::vector.102"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.102"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector.102"* %this, %"class.std::__2::vector.102"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.102"*, %"class.std::__2::vector.102"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.102"* %this1 to %"class.std::__2::__vector_base.103"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.103", %"class.std::__2::__vector_base.103"* %0, i32 0, i32 0
  %1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__begin_, align 4
  %2 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %1, i32 %2
  ret %"struct.std::__2::array"* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco9IndexTypeIjNS_19FaceIndex_tag_type_EE5valueEv(%"class.draco::IndexType.116"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.116"*, align 4
  store %"class.draco::IndexType.116"* %this, %"class.draco::IndexType.116"** %this.addr, align 4
  %this1 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.116", %"class.draco::IndexType.116"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value_, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEEEixERKS3_(%"class.draco::IndexTypeVector"* %this, %"class.draco::IndexType.104"* nonnull align 4 dereferenceable(4) %index) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector"*, align 4
  %index.addr = alloca %"class.draco::IndexType.104"*, align 4
  store %"class.draco::IndexTypeVector"* %this, %"class.draco::IndexTypeVector"** %this.addr, align 4
  store %"class.draco::IndexType.104"* %index, %"class.draco::IndexType.104"** %index.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector"*, %"class.draco::IndexTypeVector"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector", %"class.draco::IndexTypeVector"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexType.104"*, %"class.draco::IndexType.104"** %index.addr, align 4
  %call = call i32 @_ZNK5draco9IndexTypeIjNS_20PointIndex_tag_type_EE5valueEv(%"class.draco::IndexType.104"* %0)
  %call2 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEEixEm(%"class.std::__2::vector.68"* %vector_, i32 %call) #8
  ret %"class.draco::IndexType"* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEEixEm(%"class.std::__2::vector.68"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.68"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector.68"* %this, %"class.std::__2::vector.68"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.68"* %this1 to %"class.std::__2::__vector_base.69"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %0, i32 0, i32 0
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  %2 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %1, i32 %2
  ret %"class.draco::IndexType"* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK5draco9IndexTypeIjNS_19FaceIndex_tag_type_EEgeERKj(%"class.draco::IndexType.116"* %this, i32* nonnull align 4 dereferenceable(4) %val) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.116"*, align 4
  %val.addr = alloca i32*, align 4
  store %"class.draco::IndexType.116"* %this, %"class.draco::IndexType.116"** %this.addr, align 4
  store i32* %val, i32** %val.addr, align 4
  %this1 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.116", %"class.draco::IndexType.116"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value_, align 4
  %1 = load i32*, i32** %val.addr, align 4
  %2 = load i32, i32* %1, align 4
  %cmp = icmp uge i32 %0, %2
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEENSt3__25arrayINS1_IjNS_20PointIndex_tag_type_EEELm3EEEE6resizeEmRKS8_(%"class.draco::IndexTypeVector.101"* %this, i32 %size, %"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %val) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector.101"*, align 4
  %size.addr = alloca i32, align 4
  %val.addr = alloca %"struct.std::__2::array"*, align 4
  store %"class.draco::IndexTypeVector.101"* %this, %"class.draco::IndexTypeVector.101"** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  store %"struct.std::__2::array"* %val, %"struct.std::__2::array"** %val.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector.101"*, %"class.draco::IndexTypeVector.101"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector.101", %"class.draco::IndexTypeVector.101"* %this1, i32 0, i32 0
  %0 = load i32, i32* %size.addr, align 4
  %1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %val.addr, align 4
  call void @_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE6resizeEmRKS6_(%"class.std::__2::vector.102"* %vector_, i32 %0, %"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %1)
  ret void
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::array"* @_ZNSt3__25arrayIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELm3EEC2Ev(%"struct.std::__2::array"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"struct.std::__2::array"*, align 4
  %this.addr = alloca %"struct.std::__2::array"*, align 4
  store %"struct.std::__2::array"* %this, %"struct.std::__2::array"** %this.addr, align 4
  %this1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %this.addr, align 4
  store %"struct.std::__2::array"* %this1, %"struct.std::__2::array"** %retval, align 4
  %__elems_ = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %"class.draco::IndexType.104"], [3 x %"class.draco::IndexType.104"]* %__elems_, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %"class.draco::IndexType.104", %"class.draco::IndexType.104"* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %"class.draco::IndexType.104"* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %"class.draco::IndexType.104"* @_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEC2Ev(%"class.draco::IndexType.104"* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %"class.draco::IndexType.104", %"class.draco::IndexType.104"* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %"class.draco::IndexType.104"* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %retval, align 4
  ret %"struct.std::__2::array"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"struct.std::__2::array"* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEENSt3__25arrayINS1_IjNS_20PointIndex_tag_type_EEELm3EEEEixERKS3_(%"class.draco::IndexTypeVector.101"* %this, %"class.draco::IndexType.116"* nonnull align 4 dereferenceable(4) %index) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector.101"*, align 4
  %index.addr = alloca %"class.draco::IndexType.116"*, align 4
  store %"class.draco::IndexTypeVector.101"* %this, %"class.draco::IndexTypeVector.101"** %this.addr, align 4
  store %"class.draco::IndexType.116"* %index, %"class.draco::IndexType.116"** %index.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector.101"*, %"class.draco::IndexTypeVector.101"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector.101", %"class.draco::IndexTypeVector.101"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexType.116"*, %"class.draco::IndexType.116"** %index.addr, align 4
  %call = call i32 @_ZNK5draco9IndexTypeIjNS_19FaceIndex_tag_type_EE5valueEv(%"class.draco::IndexType.116"* %0)
  %call2 = call nonnull align 4 dereferenceable(12) %"struct.std::__2::array"* @_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEEixEm(%"class.std::__2::vector.102"* %vector_, i32 %call) #8
  ret %"struct.std::__2::array"* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"struct.std::__2::array"* @_ZNSt3__25arrayIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELm3EEaSERKS5_(%"struct.std::__2::array"* %this, %"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %0) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::array"*, align 4
  %.addr = alloca %"struct.std::__2::array"*, align 4
  %__i0 = alloca i32, align 4
  store %"struct.std::__2::array"* %this, %"struct.std::__2::array"** %this.addr, align 4
  store %"struct.std::__2::array"* %0, %"struct.std::__2::array"** %.addr, align 4
  %this1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %this.addr, align 4
  store i32 0, i32* %__i0, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %__i0, align 4
  %cmp = icmp ne i32 %1, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %__elems_ = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %this1, i32 0, i32 0
  %2 = load i32, i32* %__i0, align 4
  %arrayidx = getelementptr inbounds [3 x %"class.draco::IndexType.104"], [3 x %"class.draco::IndexType.104"]* %__elems_, i32 0, i32 %2
  %3 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %.addr, align 4
  %__elems_2 = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %3, i32 0, i32 0
  %4 = load i32, i32* %__i0, align 4
  %arrayidx3 = getelementptr inbounds [3 x %"class.draco::IndexType.104"], [3 x %"class.draco::IndexType.104"]* %__elems_2, i32 0, i32 %4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.104"* @_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEaSERKS2_(%"class.draco::IndexType.104"* %arrayidx, %"class.draco::IndexType.104"* nonnull align 4 dereferenceable(4) %arrayidx3)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %__i0, align 4
  %inc = add i32 %5, 1
  store i32 %inc, i32* %__i0, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret %"struct.std::__2::array"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE6resizeEmRKS6_(%"class.std::__2::vector.102"* %this, i32 %__sz, %"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %__x) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.102"*, align 4
  %__sz.addr = alloca i32, align 4
  %__x.addr = alloca %"struct.std::__2::array"*, align 4
  %__cs = alloca i32, align 4
  store %"class.std::__2::vector.102"* %this, %"class.std::__2::vector.102"** %this.addr, align 4
  store i32 %__sz, i32* %__sz.addr, align 4
  store %"struct.std::__2::array"* %__x, %"struct.std::__2::array"** %__x.addr, align 4
  %this1 = load %"class.std::__2::vector.102"*, %"class.std::__2::vector.102"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.102"* %this1) #8
  store i32 %call, i32* %__cs, align 4
  %0 = load i32, i32* %__cs, align 4
  %1 = load i32, i32* %__sz.addr, align 4
  %cmp = icmp ult i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %__sz.addr, align 4
  %3 = load i32, i32* %__cs, align 4
  %sub = sub i32 %2, %3
  %4 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__x.addr, align 4
  call void @_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE8__appendEmRKS6_(%"class.std::__2::vector.102"* %this1, i32 %sub, %"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %4)
  br label %if.end4

if.else:                                          ; preds = %entry
  %5 = load i32, i32* %__cs, align 4
  %6 = load i32, i32* %__sz.addr, align 4
  %cmp2 = icmp ugt i32 %5, %6
  br i1 %cmp2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.else
  %7 = bitcast %"class.std::__2::vector.102"* %this1 to %"class.std::__2::__vector_base.103"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.103", %"class.std::__2::__vector_base.103"* %7, i32 0, i32 0
  %8 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__begin_, align 4
  %9 = load i32, i32* %__sz.addr, align 4
  %add.ptr = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %8, i32 %9
  call void @_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE17__destruct_at_endEPS6_(%"class.std::__2::vector.102"* %this1, %"struct.std::__2::array"* %add.ptr) #8
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.else
  br label %if.end4

if.end4:                                          ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE8__appendEmRKS6_(%"class.std::__2::vector.102"* %this, i32 %__n, %"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %__x) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.102"*, align 4
  %__n.addr = alloca i32, align 4
  %__x.addr = alloca %"struct.std::__2::array"*, align 4
  %__a = alloca %"class.std::__2::allocator.108"*, align 4
  %__v = alloca %"struct.std::__2::__split_buffer", align 4
  store %"class.std::__2::vector.102"* %this, %"class.std::__2::vector.102"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store %"struct.std::__2::array"* %__x, %"struct.std::__2::array"** %__x.addr, align 4
  %this1 = load %"class.std::__2::vector.102"*, %"class.std::__2::vector.102"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.102"* %this1 to %"class.std::__2::__vector_base.103"*
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE9__end_capEv(%"class.std::__2::__vector_base.103"* %0) #8
  %1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %call, align 4
  %2 = bitcast %"class.std::__2::vector.102"* %this1 to %"class.std::__2::__vector_base.103"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.103", %"class.std::__2::__vector_base.103"* %2, i32 0, i32 1
  %3 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__end_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"struct.std::__2::array"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"struct.std::__2::array"* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 12
  %4 = load i32, i32* %__n.addr, align 4
  %cmp = icmp uge i32 %sub.ptr.div, %4
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %5 = load i32, i32* %__n.addr, align 4
  %6 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__x.addr, align 4
  call void @_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE18__construct_at_endEmRKS6_(%"class.std::__2::vector.102"* %this1, i32 %5, %"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %6)
  br label %if.end

if.else:                                          ; preds = %entry
  %7 = bitcast %"class.std::__2::vector.102"* %this1 to %"class.std::__2::__vector_base.103"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.108"* @_ZNSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base.103"* %7) #8
  store %"class.std::__2::allocator.108"* %call2, %"class.std::__2::allocator.108"** %__a, align 4
  %call3 = call i32 @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.102"* %this1) #8
  %8 = load i32, i32* %__n.addr, align 4
  %add = add i32 %call3, %8
  %call4 = call i32 @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE11__recommendEm(%"class.std::__2::vector.102"* %this1, i32 %add)
  %call5 = call i32 @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.102"* %this1) #8
  %9 = load %"class.std::__2::allocator.108"*, %"class.std::__2::allocator.108"** %__a, align 4
  %call6 = call %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEEC2EmmS9_(%"struct.std::__2::__split_buffer"* %__v, i32 %call4, i32 %call5, %"class.std::__2::allocator.108"* nonnull align 1 dereferenceable(1) %9)
  %10 = load i32, i32* %__n.addr, align 4
  %11 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__x.addr, align 4
  call void @_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE18__construct_at_endEmRKS6_(%"struct.std::__2::__split_buffer"* %__v, i32 %10, %"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %11)
  call void @_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS6_RS8_EE(%"class.std::__2::vector.102"* %this1, %"struct.std::__2::__split_buffer"* nonnull align 4 dereferenceable(20) %__v)
  %call7 = call %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEED2Ev(%"struct.std::__2::__split_buffer"* %__v) #8
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE17__destruct_at_endEPS6_(%"class.std::__2::vector.102"* %this, %"struct.std::__2::array"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.102"*, align 4
  %__new_last.addr = alloca %"struct.std::__2::array"*, align 4
  %__old_size = alloca i32, align 4
  store %"class.std::__2::vector.102"* %this, %"class.std::__2::vector.102"** %this.addr, align 4
  store %"struct.std::__2::array"* %__new_last, %"struct.std::__2::array"** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::vector.102"*, %"class.std::__2::vector.102"** %this.addr, align 4
  %0 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__new_last.addr, align 4
  call void @_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE27__invalidate_iterators_pastEPS6_(%"class.std::__2::vector.102"* %this1, %"struct.std::__2::array"* %0)
  %call = call i32 @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.102"* %this1) #8
  store i32 %call, i32* %__old_size, align 4
  %1 = bitcast %"class.std::__2::vector.102"* %this1 to %"class.std::__2::__vector_base.103"*
  %2 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__new_last.addr, align 4
  call void @_ZNSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE17__destruct_at_endEPS6_(%"class.std::__2::__vector_base.103"* %1, %"struct.std::__2::array"* %2) #8
  %3 = load i32, i32* %__old_size, align 4
  call void @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE17__annotate_shrinkEm(%"class.std::__2::vector.102"* %this1, i32 %3) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE9__end_capEv(%"class.std::__2::__vector_base.103"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.103"*, align 4
  store %"class.std::__2::__vector_base.103"* %this, %"class.std::__2::__vector_base.103"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.103"*, %"class.std::__2::__vector_base.103"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.103", %"class.std::__2::__vector_base.103"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.105"* %__end_cap_) #8
  ret %"struct.std::__2::array"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE18__construct_at_endEmRKS6_(%"class.std::__2::vector.102"* %this, i32 %__n, %"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %__x) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.102"*, align 4
  %__n.addr = alloca i32, align 4
  %__x.addr = alloca %"struct.std::__2::array"*, align 4
  %__tx = alloca %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction", align 4
  store %"class.std::__2::vector.102"* %this, %"class.std::__2::vector.102"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store %"struct.std::__2::array"* %__x, %"struct.std::__2::array"** %__x.addr, align 4
  %this1 = load %"class.std::__2::vector.102"*, %"class.std::__2::vector.102"** %this.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %call = call %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"* @_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE21_ConstructTransactionC2ERS9_m(%"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"* %__tx, %"class.std::__2::vector.102"* nonnull align 4 dereferenceable(12) %this1, i32 %0)
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__pos_, align 4
  %__new_end_ = getelementptr inbounds %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"* %__tx, i32 0, i32 2
  %2 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__new_end_, align 4
  %cmp = icmp ne %"struct.std::__2::array"* %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = bitcast %"class.std::__2::vector.102"* %this1 to %"class.std::__2::__vector_base.103"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.108"* @_ZNSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base.103"* %3) #8
  %__pos_3 = getelementptr inbounds %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %4 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__pos_3, align 4
  %call4 = call %"struct.std::__2::array"* @_ZNSt3__212__to_addressINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEEEPT_S8_(%"struct.std::__2::array"* %4) #8
  %5 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__x.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE9constructIS7_JRKS7_EEEvRS8_PT_DpOT0_(%"class.std::__2::allocator.108"* nonnull align 1 dereferenceable(1) %call2, %"struct.std::__2::array"* %call4, %"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %5)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %__pos_5 = getelementptr inbounds %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %6 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__pos_5, align 4
  %incdec.ptr = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %6, i32 1
  store %"struct.std::__2::array"* %incdec.ptr, %"struct.std::__2::array"** %__pos_5, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call6 = call %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"* @_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"* %__tx) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.108"* @_ZNSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base.103"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.103"*, align 4
  store %"class.std::__2::__vector_base.103"* %this, %"class.std::__2::__vector_base.103"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.103"*, %"class.std::__2::__vector_base.103"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.103", %"class.std::__2::__vector_base.103"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.108"* @_ZNSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE6secondEv(%"class.std::__2::__compressed_pair.105"* %__end_cap_) #8
  ret %"class.std::__2::allocator.108"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE11__recommendEm(%"class.std::__2::vector.102"* %this, i32 %__new_size) #0 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %"class.std::__2::vector.102"*, align 4
  %__new_size.addr = alloca i32, align 4
  %__ms = alloca i32, align 4
  %__cap = alloca i32, align 4
  %ref.tmp = alloca i32, align 4
  store %"class.std::__2::vector.102"* %this, %"class.std::__2::vector.102"** %this.addr, align 4
  store i32 %__new_size, i32* %__new_size.addr, align 4
  %this1 = load %"class.std::__2::vector.102"*, %"class.std::__2::vector.102"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE8max_sizeEv(%"class.std::__2::vector.102"* %this1) #8
  store i32 %call, i32* %__ms, align 4
  %0 = load i32, i32* %__new_size.addr, align 4
  %1 = load i32, i32* %__ms, align 4
  %cmp = icmp ugt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = bitcast %"class.std::__2::vector.102"* %this1 to %"class.std::__2::__vector_base_common"*
  call void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"* %2) #9
  unreachable

if.end:                                           ; preds = %entry
  %call2 = call i32 @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector.102"* %this1) #8
  store i32 %call2, i32* %__cap, align 4
  %3 = load i32, i32* %__cap, align 4
  %4 = load i32, i32* %__ms, align 4
  %div = udiv i32 %4, 2
  %cmp3 = icmp uge i32 %3, %div
  br i1 %cmp3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %if.end
  %5 = load i32, i32* %__ms, align 4
  store i32 %5, i32* %retval, align 4
  br label %return

if.end5:                                          ; preds = %if.end
  %6 = load i32, i32* %__cap, align 4
  %mul = mul i32 2, %6
  store i32 %mul, i32* %ref.tmp, align 4
  %call6 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %__new_size.addr)
  %7 = load i32, i32* %call6, align 4
  store i32 %7, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end5, %if.then4
  %8 = load i32, i32* %retval, align 4
  ret i32 %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEEC2EmmS9_(%"struct.std::__2::__split_buffer"* returned %this, i32 %__cap, i32 %__start, %"class.std::__2::allocator.108"* nonnull align 1 dereferenceable(1) %__a) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"struct.std::__2::__split_buffer"*, align 4
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__cap.addr = alloca i32, align 4
  %__start.addr = alloca i32, align 4
  %__a.addr = alloca %"class.std::__2::allocator.108"*, align 4
  %ref.tmp = alloca i8*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store i32 %__cap, i32* %__cap.addr, align 4
  store i32 %__start, i32* %__start.addr, align 4
  store %"class.std::__2::allocator.108"* %__a, %"class.std::__2::allocator.108"** %__a.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer"* %this1, %"struct.std::__2::__split_buffer"** %retval, align 4
  %0 = bitcast %"struct.std::__2::__split_buffer"* %this1 to %"class.std::__2::__split_buffer_common"*
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  store i8* null, i8** %ref.tmp, align 4
  %1 = load %"class.std::__2::allocator.108"*, %"class.std::__2::allocator.108"** %__a.addr, align 4
  %call = call %"class.std::__2::__compressed_pair.127"* @_ZNSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEEC2IDnSA_EEOT_OT0_(%"class.std::__2::__compressed_pair.127"* %__end_cap_, i8** nonnull align 4 dereferenceable(4) %ref.tmp, %"class.std::__2::allocator.108"* nonnull align 1 dereferenceable(1) %1)
  %2 = load i32, i32* %__cap.addr, align 4
  %cmp = icmp ne i32 %2, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.108"* @_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE7__allocEv(%"struct.std::__2::__split_buffer"* %this1) #8
  %3 = load i32, i32* %__cap.addr, align 4
  %call3 = call %"struct.std::__2::array"* @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE8allocateERS8_m(%"class.std::__2::allocator.108"* nonnull align 1 dereferenceable(1) %call2, i32 %3)
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %"struct.std::__2::array"* [ %call3, %cond.true ], [ null, %cond.false ]
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  store %"struct.std::__2::array"* %cond, %"struct.std::__2::array"** %__first_, align 4
  %__first_4 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %4 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__first_4, align 4
  %5 = load i32, i32* %__start.addr, align 4
  %add.ptr = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %4, i32 %5
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 2
  store %"struct.std::__2::array"* %add.ptr, %"struct.std::__2::array"** %__end_, align 4
  %__begin_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 1
  store %"struct.std::__2::array"* %add.ptr, %"struct.std::__2::array"** %__begin_, align 4
  %__first_5 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %6 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__first_5, align 4
  %7 = load i32, i32* %__cap.addr, align 4
  %add.ptr6 = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %6, i32 %7
  %call7 = call nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this1) #8
  store %"struct.std::__2::array"* %add.ptr6, %"struct.std::__2::array"** %call7, align 4
  %8 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %retval, align 4
  ret %"struct.std::__2::__split_buffer"* %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE18__construct_at_endEmRKS6_(%"struct.std::__2::__split_buffer"* %this, i32 %__n, %"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %__x) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__n.addr = alloca i32, align 4
  %__x.addr = alloca %"struct.std::__2::array"*, align 4
  %__tx = alloca %"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction", align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store %"struct.std::__2::array"* %__x, %"struct.std::__2::array"** %__x.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 2
  %0 = load i32, i32* %__n.addr, align 4
  %call = call %"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE21_ConstructTransactionC2EPPS6_m(%"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction"* %__tx, %"struct.std::__2::array"** %__end_, i32 %0) #8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %__pos_ = getelementptr inbounds %"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction"* %__tx, i32 0, i32 0
  %1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__pos_, align 4
  %__end_2 = getelementptr inbounds %"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %2 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__end_2, align 4
  %cmp = icmp ne %"struct.std::__2::array"* %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %call3 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.108"* @_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE7__allocEv(%"struct.std::__2::__split_buffer"* %this1) #8
  %__pos_4 = getelementptr inbounds %"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction"* %__tx, i32 0, i32 0
  %3 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__pos_4, align 4
  %call5 = call %"struct.std::__2::array"* @_ZNSt3__212__to_addressINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEEEPT_S8_(%"struct.std::__2::array"* %3) #8
  %4 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__x.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE9constructIS7_JRKS7_EEEvRS8_PT_DpOT0_(%"class.std::__2::allocator.108"* nonnull align 1 dereferenceable(1) %call3, %"struct.std::__2::array"* %call5, %"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %4)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %__pos_6 = getelementptr inbounds %"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction"* %__tx, i32 0, i32 0
  %5 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__pos_6, align 4
  %incdec.ptr = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %5, i32 1
  store %"struct.std::__2::array"* %incdec.ptr, %"struct.std::__2::array"** %__pos_6, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call7 = call %"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction"* %__tx) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS6_RS8_EE(%"class.std::__2::vector.102"* %this, %"struct.std::__2::__split_buffer"* nonnull align 4 dereferenceable(20) %__v) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.102"*, align 4
  %__v.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"class.std::__2::vector.102"* %this, %"class.std::__2::vector.102"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer"* %__v, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %this1 = load %"class.std::__2::vector.102"*, %"class.std::__2::vector.102"** %this.addr, align 4
  call void @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE17__annotate_deleteEv(%"class.std::__2::vector.102"* %this1) #8
  %0 = bitcast %"class.std::__2::vector.102"* %this1 to %"class.std::__2::__vector_base.103"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.108"* @_ZNSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base.103"* %0) #8
  %1 = bitcast %"class.std::__2::vector.102"* %this1 to %"class.std::__2::__vector_base.103"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.103", %"class.std::__2::__vector_base.103"* %1, i32 0, i32 0
  %2 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__begin_, align 4
  %3 = bitcast %"class.std::__2::vector.102"* %this1 to %"class.std::__2::__vector_base.103"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.103", %"class.std::__2::__vector_base.103"* %3, i32 0, i32 1
  %4 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__end_, align 4
  %5 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__begin_2 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %5, i32 0, i32 1
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE46__construct_backward_with_exception_guaranteesIS7_EENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS8_PT_SE_EE5valuesr31is_trivially_move_constructibleISE_EE5valueEvE4typeERS8_SF_SF_RSF_(%"class.std::__2::allocator.108"* nonnull align 1 dereferenceable(1) %call, %"struct.std::__2::array"* %2, %"struct.std::__2::array"* %4, %"struct.std::__2::array"** nonnull align 4 dereferenceable(4) %__begin_2)
  %6 = bitcast %"class.std::__2::vector.102"* %this1 to %"class.std::__2::__vector_base.103"*
  %__begin_3 = getelementptr inbounds %"class.std::__2::__vector_base.103", %"class.std::__2::__vector_base.103"* %6, i32 0, i32 0
  %7 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__begin_4 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %7, i32 0, i32 1
  call void @_ZNSt3__24swapIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS9_EE5valueEvE4typeERS9_SC_(%"struct.std::__2::array"** nonnull align 4 dereferenceable(4) %__begin_3, %"struct.std::__2::array"** nonnull align 4 dereferenceable(4) %__begin_4) #8
  %8 = bitcast %"class.std::__2::vector.102"* %this1 to %"class.std::__2::__vector_base.103"*
  %__end_5 = getelementptr inbounds %"class.std::__2::__vector_base.103", %"class.std::__2::__vector_base.103"* %8, i32 0, i32 1
  %9 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__end_6 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %9, i32 0, i32 2
  call void @_ZNSt3__24swapIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS9_EE5valueEvE4typeERS9_SC_(%"struct.std::__2::array"** nonnull align 4 dereferenceable(4) %__end_5, %"struct.std::__2::array"** nonnull align 4 dereferenceable(4) %__end_6) #8
  %10 = bitcast %"class.std::__2::vector.102"* %this1 to %"class.std::__2::__vector_base.103"*
  %call7 = call nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE9__end_capEv(%"class.std::__2::__vector_base.103"* %10) #8
  %11 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %call8 = call nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE9__end_capEv(%"struct.std::__2::__split_buffer"* %11) #8
  call void @_ZNSt3__24swapIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS9_EE5valueEvE4typeERS9_SC_(%"struct.std::__2::array"** nonnull align 4 dereferenceable(4) %call7, %"struct.std::__2::array"** nonnull align 4 dereferenceable(4) %call8) #8
  %12 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__begin_9 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %12, i32 0, i32 1
  %13 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__begin_9, align 4
  %14 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %14, i32 0, i32 0
  store %"struct.std::__2::array"* %13, %"struct.std::__2::array"** %__first_, align 4
  %call10 = call i32 @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.102"* %this1) #8
  call void @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE14__annotate_newEm(%"class.std::__2::vector.102"* %this1, i32 %call10) #8
  call void @_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector.102"* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEED2Ev(%"struct.std::__2::__split_buffer"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"struct.std::__2::__split_buffer"*, align 4
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer"* %this1, %"struct.std::__2::__split_buffer"** %retval, align 4
  call void @_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE5clearEv(%"struct.std::__2::__split_buffer"* %this1) #8
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %0 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__first_, align 4
  %tobool = icmp ne %"struct.std::__2::array"* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.108"* @_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE7__allocEv(%"struct.std::__2::__split_buffer"* %this1) #8
  %__first_2 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__first_2, align 4
  %call3 = call i32 @_ZNKSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE8capacityEv(%"struct.std::__2::__split_buffer"* %this1)
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE10deallocateERS8_PS7_m(%"class.std::__2::allocator.108"* nonnull align 1 dereferenceable(1) %call, %"struct.std::__2::array"* %1, i32 %call3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %retval, align 4
  ret %"struct.std::__2::__split_buffer"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.105"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.105"*, align 4
  store %"class.std::__2::__compressed_pair.105"* %this, %"class.std::__2::__compressed_pair.105"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.105"*, %"class.std::__2::__compressed_pair.105"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.105"* %this1 to %"struct.std::__2::__compressed_pair_elem.106"*
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNSt3__222__compressed_pair_elemIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.106"* %0) #8
  ret %"struct.std::__2::array"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNSt3__222__compressed_pair_elemIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.106"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.106"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.106"* %this, %"struct.std::__2::__compressed_pair_elem.106"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.106"*, %"struct.std::__2::__compressed_pair_elem.106"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.106", %"struct.std::__2::__compressed_pair_elem.106"* %this1, i32 0, i32 0
  ret %"struct.std::__2::array"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"* @_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE21_ConstructTransactionC2ERS9_m(%"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"* returned %this, %"class.std::__2::vector.102"* nonnull align 4 dereferenceable(12) %__v, i32 %__n) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"*, align 4
  %__v.addr = alloca %"class.std::__2::vector.102"*, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"** %this.addr, align 4
  store %"class.std::__2::vector.102"* %__v, %"class.std::__2::vector.102"** %__v.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"*, %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"** %this.addr, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::vector.102"*, %"class.std::__2::vector.102"** %__v.addr, align 4
  store %"class.std::__2::vector.102"* %0, %"class.std::__2::vector.102"** %__v_, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %1 = load %"class.std::__2::vector.102"*, %"class.std::__2::vector.102"** %__v.addr, align 4
  %2 = bitcast %"class.std::__2::vector.102"* %1 to %"class.std::__2::__vector_base.103"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.103", %"class.std::__2::__vector_base.103"* %2, i32 0, i32 1
  %3 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__end_, align 4
  store %"struct.std::__2::array"* %3, %"struct.std::__2::array"** %__pos_, align 4
  %__new_end_ = getelementptr inbounds %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"* %this1, i32 0, i32 2
  %4 = load %"class.std::__2::vector.102"*, %"class.std::__2::vector.102"** %__v.addr, align 4
  %5 = bitcast %"class.std::__2::vector.102"* %4 to %"class.std::__2::__vector_base.103"*
  %__end_2 = getelementptr inbounds %"class.std::__2::__vector_base.103", %"class.std::__2::__vector_base.103"* %5, i32 0, i32 1
  %6 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__end_2, align 4
  %7 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %6, i32 %7
  store %"struct.std::__2::array"* %add.ptr, %"struct.std::__2::array"** %__new_end_, align 4
  ret %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE9constructIS7_JRKS7_EEEvRS8_PT_DpOT0_(%"class.std::__2::allocator.108"* nonnull align 1 dereferenceable(1) %__a, %"struct.std::__2::array"* %__p, %"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %__args) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.108"*, align 4
  %__p.addr = alloca %"struct.std::__2::array"*, align 4
  %__args.addr = alloca %"struct.std::__2::array"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_construct", align 1
  store %"class.std::__2::allocator.108"* %__a, %"class.std::__2::allocator.108"** %__a.addr, align 4
  store %"struct.std::__2::array"* %__p, %"struct.std::__2::array"** %__p.addr, align 4
  store %"struct.std::__2::array"* %__args, %"struct.std::__2::array"** %__args.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_construct"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.108"*, %"class.std::__2::allocator.108"** %__a.addr, align 4
  %2 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__p.addr, align 4
  %3 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::array"* @_ZNSt3__27forwardIRKNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEEEOT_RNS_16remove_referenceIS9_E4typeE(%"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %3) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE11__constructIS7_JRKS7_EEEvNS_17integral_constantIbLb1EEERS8_PT_DpOT0_(%"class.std::__2::allocator.108"* nonnull align 1 dereferenceable(1) %1, %"struct.std::__2::array"* %2, %"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::array"* @_ZNSt3__212__to_addressINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEEEPT_S8_(%"struct.std::__2::array"* %__p) #0 comdat {
entry:
  %__p.addr = alloca %"struct.std::__2::array"*, align 4
  store %"struct.std::__2::array"* %__p, %"struct.std::__2::array"** %__p.addr, align 4
  %0 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__p.addr, align 4
  ret %"struct.std::__2::array"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"* @_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"*, align 4
  store %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"** %this.addr, align 4
  %this1 = load %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"*, %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %0 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__pos_, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::vector.102"*, %"class.std::__2::vector.102"** %__v_, align 4
  %2 = bitcast %"class.std::__2::vector.102"* %1 to %"class.std::__2::__vector_base.103"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.103", %"class.std::__2::__vector_base.103"* %2, i32 0, i32 1
  store %"struct.std::__2::array"* %0, %"struct.std::__2::array"** %__end_, align 4
  ret %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE11__constructIS7_JRKS7_EEEvNS_17integral_constantIbLb1EEERS8_PT_DpOT0_(%"class.std::__2::allocator.108"* nonnull align 1 dereferenceable(1) %__a, %"struct.std::__2::array"* %__p, %"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %__args) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.108"*, align 4
  %__p.addr = alloca %"struct.std::__2::array"*, align 4
  %__args.addr = alloca %"struct.std::__2::array"*, align 4
  store %"class.std::__2::allocator.108"* %__a, %"class.std::__2::allocator.108"** %__a.addr, align 4
  store %"struct.std::__2::array"* %__p, %"struct.std::__2::array"** %__p.addr, align 4
  store %"struct.std::__2::array"* %__args, %"struct.std::__2::array"** %__args.addr, align 4
  %1 = load %"class.std::__2::allocator.108"*, %"class.std::__2::allocator.108"** %__a.addr, align 4
  %2 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__p.addr, align 4
  %3 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::array"* @_ZNSt3__27forwardIRKNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEEEOT_RNS_16remove_referenceIS9_E4typeE(%"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %3) #8
  call void @_ZNSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEE9constructIS6_JRKS6_EEEvPT_DpOT0_(%"class.std::__2::allocator.108"* %1, %"struct.std::__2::array"* %2, %"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"struct.std::__2::array"* @_ZNSt3__27forwardIRKNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEEEOT_RNS_16remove_referenceIS9_E4typeE(%"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"struct.std::__2::array"*, align 4
  store %"struct.std::__2::array"* %__t, %"struct.std::__2::array"** %__t.addr, align 4
  %0 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__t.addr, align 4
  ret %"struct.std::__2::array"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEE9constructIS6_JRKS6_EEEvPT_DpOT0_(%"class.std::__2::allocator.108"* %this, %"struct.std::__2::array"* %__p, %"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %__args) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.108"*, align 4
  %__p.addr = alloca %"struct.std::__2::array"*, align 4
  %__args.addr = alloca %"struct.std::__2::array"*, align 4
  store %"class.std::__2::allocator.108"* %this, %"class.std::__2::allocator.108"** %this.addr, align 4
  store %"struct.std::__2::array"* %__p, %"struct.std::__2::array"** %__p.addr, align 4
  store %"struct.std::__2::array"* %__args, %"struct.std::__2::array"** %__args.addr, align 4
  %this1 = load %"class.std::__2::allocator.108"*, %"class.std::__2::allocator.108"** %this.addr, align 4
  %0 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__p.addr, align 4
  %1 = bitcast %"struct.std::__2::array"* %0 to i8*
  %2 = bitcast i8* %1 to %"struct.std::__2::array"*
  %3 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::array"* @_ZNSt3__27forwardIRKNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEEEOT_RNS_16remove_referenceIS9_E4typeE(%"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %3) #8
  %4 = bitcast %"struct.std::__2::array"* %2 to i8*
  %5 = bitcast %"struct.std::__2::array"* %call to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 12, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.108"* @_ZNSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE6secondEv(%"class.std::__2::__compressed_pair.105"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.105"*, align 4
  store %"class.std::__2::__compressed_pair.105"* %this, %"class.std::__2::__compressed_pair.105"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.105"*, %"class.std::__2::__compressed_pair.105"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.105"* %this1 to %"struct.std::__2::__compressed_pair_elem.107"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.108"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.107"* %0) #8
  ret %"class.std::__2::allocator.108"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.108"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.107"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.107"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.107"* %this, %"struct.std::__2::__compressed_pair_elem.107"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.107"*, %"struct.std::__2::__compressed_pair_elem.107"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.107"* %this1 to %"class.std::__2::allocator.108"*
  ret %"class.std::__2::allocator.108"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE8max_sizeEv(%"class.std::__2::vector.102"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.102"*, align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp3 = alloca i32, align 4
  store %"class.std::__2::vector.102"* %this, %"class.std::__2::vector.102"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.102"*, %"class.std::__2::vector.102"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.102"* %this1 to %"class.std::__2::__vector_base.103"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.108"* @_ZNKSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base.103"* %0) #8
  %call2 = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE8max_sizeERKS8_(%"class.std::__2::allocator.108"* nonnull align 1 dereferenceable(1) %call) #8
  store i32 %call2, i32* %ref.tmp, align 4
  %call4 = call i32 @_ZNSt3__214numeric_limitsIlE3maxEv() #8
  store i32 %call4, i32* %ref.tmp3, align 4
  %call5 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %1 = load i32, i32* %call5, align 4
  ret i32 %1
}

; Function Attrs: noreturn
declare void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"*) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector.102"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.102"*, align 4
  store %"class.std::__2::vector.102"* %this, %"class.std::__2::vector.102"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.102"*, %"class.std::__2::vector.102"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.102"* %this1 to %"class.std::__2::__vector_base.103"*
  %call = call i32 @_ZNKSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::__vector_base.103"* %0) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::__less", align 1
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__a.addr, align 4
  %1 = load i32*, i32** %__b.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::__less", align 1
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__a.addr, align 4
  %1 = load i32*, i32** %__b.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE8max_sizeERKS8_(%"class.std::__2::allocator.108"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.108"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_max_size", align 1
  store %"class.std::__2::allocator.108"* %__a, %"class.std::__2::allocator.108"** %__a.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_max_size"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.108"*, %"class.std::__2::allocator.108"** %__a.addr, align 4
  %call = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS8_(%"class.std::__2::allocator.108"* nonnull align 1 dereferenceable(1) %1) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.108"* @_ZNKSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base.103"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.103"*, align 4
  store %"class.std::__2::__vector_base.103"* %this, %"class.std::__2::__vector_base.103"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.103"*, %"class.std::__2::__vector_base.103"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.103", %"class.std::__2::__vector_base.103"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.108"* @_ZNKSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE6secondEv(%"class.std::__2::__compressed_pair.105"* %__end_cap_) #8
  ret %"class.std::__2::allocator.108"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__214numeric_limitsIlE3maxEv() #0 comdat {
entry:
  %call = call i32 @_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv() #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__comp = alloca %"struct.std::__2::__less", align 1
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__b.addr, align 4
  %1 = load i32*, i32** %__a.addr, align 4
  %call = call zeroext i1 @_ZNKSt3__26__lessImmEclERKmS3_(%"struct.std::__2::__less"* %__comp, i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  br i1 %call, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32*, i32** %__b.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %3 = load i32*, i32** %__a.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi i32* [ %2, %cond.true ], [ %3, %cond.false ]
  ret i32* %cond-lvalue
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__26__lessImmEclERKmS3_(%"struct.std::__2::__less"* %this, i32* nonnull align 4 dereferenceable(4) %__x, i32* nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__less"*, align 4
  %__x.addr = alloca i32*, align 4
  %__y.addr = alloca i32*, align 4
  store %"struct.std::__2::__less"* %this, %"struct.std::__2::__less"** %this.addr, align 4
  store i32* %__x, i32** %__x.addr, align 4
  store i32* %__y, i32** %__y.addr, align 4
  %this1 = load %"struct.std::__2::__less"*, %"struct.std::__2::__less"** %this.addr, align 4
  %0 = load i32*, i32** %__x.addr, align 4
  %1 = load i32, i32* %0, align 4
  %2 = load i32*, i32** %__y.addr, align 4
  %3 = load i32, i32* %2, align 4
  %cmp = icmp ult i32 %1, %3
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS8_(%"class.std::__2::allocator.108"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.108"*, align 4
  store %"class.std::__2::allocator.108"* %__a, %"class.std::__2::allocator.108"** %__a.addr, align 4
  %1 = load %"class.std::__2::allocator.108"*, %"class.std::__2::allocator.108"** %__a.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEE8max_sizeEv(%"class.std::__2::allocator.108"* %1) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEE8max_sizeEv(%"class.std::__2::allocator.108"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.108"*, align 4
  store %"class.std::__2::allocator.108"* %this, %"class.std::__2::allocator.108"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.108"*, %"class.std::__2::allocator.108"** %this.addr, align 4
  ret i32 357913941
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.108"* @_ZNKSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE6secondEv(%"class.std::__2::__compressed_pair.105"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.105"*, align 4
  store %"class.std::__2::__compressed_pair.105"* %this, %"class.std::__2::__compressed_pair.105"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.105"*, %"class.std::__2::__compressed_pair.105"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.105"* %this1 to %"struct.std::__2::__compressed_pair_elem.107"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.108"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.107"* %0) #8
  ret %"class.std::__2::allocator.108"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.108"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.107"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.107"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.107"* %this, %"struct.std::__2::__compressed_pair_elem.107"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.107"*, %"struct.std::__2::__compressed_pair_elem.107"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.107"* %this1 to %"class.std::__2::allocator.108"*
  ret %"class.std::__2::allocator.108"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv() #0 comdat {
entry:
  ret i32 2147483647
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::__vector_base.103"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.103"*, align 4
  store %"class.std::__2::__vector_base.103"* %this, %"class.std::__2::__vector_base.103"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.103"*, %"class.std::__2::__vector_base.103"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNKSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE9__end_capEv(%"class.std::__2::__vector_base.103"* %this1) #8
  %0 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.103", %"class.std::__2::__vector_base.103"* %this1, i32 0, i32 0
  %1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"struct.std::__2::array"* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint %"struct.std::__2::array"* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 12
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNKSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE9__end_capEv(%"class.std::__2::__vector_base.103"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.103"*, align 4
  store %"class.std::__2::__vector_base.103"* %this, %"class.std::__2::__vector_base.103"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.103"*, %"class.std::__2::__vector_base.103"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.103", %"class.std::__2::__vector_base.103"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNKSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.105"* %__end_cap_) #8
  ret %"struct.std::__2::array"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNKSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.105"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.105"*, align 4
  store %"class.std::__2::__compressed_pair.105"* %this, %"class.std::__2::__compressed_pair.105"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.105"*, %"class.std::__2::__compressed_pair.105"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.105"* %this1 to %"struct.std::__2::__compressed_pair_elem.106"*
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNKSt3__222__compressed_pair_elemIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.106"* %0) #8
  ret %"struct.std::__2::array"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNKSt3__222__compressed_pair_elemIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.106"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.106"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.106"* %this, %"struct.std::__2::__compressed_pair_elem.106"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.106"*, %"struct.std::__2::__compressed_pair_elem.106"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.106", %"struct.std::__2::__compressed_pair_elem.106"* %this1, i32 0, i32 0
  ret %"struct.std::__2::array"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__comp = alloca %"struct.std::__2::__less", align 1
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__a.addr, align 4
  %1 = load i32*, i32** %__b.addr, align 4
  %call = call zeroext i1 @_ZNKSt3__26__lessImmEclERKmS3_(%"struct.std::__2::__less"* %__comp, i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  br i1 %call, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32*, i32** %__b.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %3 = load i32*, i32** %__a.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi i32* [ %2, %cond.true ], [ %3, %cond.false ]
  ret i32* %cond-lvalue
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.127"* @_ZNSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEEC2IDnSA_EEOT_OT0_(%"class.std::__2::__compressed_pair.127"* returned %this, i8** nonnull align 4 dereferenceable(4) %__t1, %"class.std::__2::allocator.108"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.127"*, align 4
  %__t1.addr = alloca i8**, align 4
  %__t2.addr = alloca %"class.std::__2::allocator.108"*, align 4
  store %"class.std::__2::__compressed_pair.127"* %this, %"class.std::__2::__compressed_pair.127"** %this.addr, align 4
  store i8** %__t1, i8*** %__t1.addr, align 4
  store %"class.std::__2::allocator.108"* %__t2, %"class.std::__2::allocator.108"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.127"*, %"class.std::__2::__compressed_pair.127"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.127"* %this1 to %"struct.std::__2::__compressed_pair_elem.106"*
  %1 = load i8**, i8*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %1) #8
  %call2 = call %"struct.std::__2::__compressed_pair_elem.106"* @_ZNSt3__222__compressed_pair_elemIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEELi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.106"* %0, i8** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.127"* %this1 to i8*
  %3 = getelementptr inbounds i8, i8* %2, i32 4
  %4 = bitcast i8* %3 to %"struct.std::__2::__compressed_pair_elem.128"*
  %5 = load %"class.std::__2::allocator.108"*, %"class.std::__2::allocator.108"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.108"* @_ZNSt3__27forwardIRNS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEEEOT_RNS_16remove_referenceISA_E4typeE(%"class.std::__2::allocator.108"* nonnull align 1 dereferenceable(1) %5) #8
  %call4 = call %"struct.std::__2::__compressed_pair_elem.128"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEELi1ELb0EEC2IS9_vEEOT_(%"struct.std::__2::__compressed_pair_elem.128"* %4, %"class.std::__2::allocator.108"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::__compressed_pair.127"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::array"* @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE8allocateERS8_m(%"class.std::__2::allocator.108"* nonnull align 1 dereferenceable(1) %__a, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.108"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.108"* %__a, %"class.std::__2::allocator.108"** %__a.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.108"*, %"class.std::__2::allocator.108"** %__a.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call %"struct.std::__2::array"* @_ZNSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEE8allocateEmPKv(%"class.std::__2::allocator.108"* %0, i32 %1, i8* null)
  ret %"struct.std::__2::array"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.108"* @_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE7__allocEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.108"* @_ZNSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE6secondEv(%"class.std::__2::__compressed_pair.127"* %__end_cap_) #8
  ret %"class.std::__2::allocator.108"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.127"* %__end_cap_) #8
  ret %"struct.std::__2::array"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca i8**, align 4
  store i8** %__t, i8*** %__t.addr, align 4
  %0 = load i8**, i8*** %__t.addr, align 4
  ret i8** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.106"* @_ZNSt3__222__compressed_pair_elemIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEELi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.106"* returned %this, i8** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.106"*, align 4
  %__u.addr = alloca i8**, align 4
  store %"struct.std::__2::__compressed_pair_elem.106"* %this, %"struct.std::__2::__compressed_pair_elem.106"** %this.addr, align 4
  store i8** %__u, i8*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.106"*, %"struct.std::__2::__compressed_pair_elem.106"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.106", %"struct.std::__2::__compressed_pair_elem.106"* %this1, i32 0, i32 0
  %0 = load i8**, i8*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %0) #8
  store %"struct.std::__2::array"* null, %"struct.std::__2::array"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.106"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.108"* @_ZNSt3__27forwardIRNS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEEEOT_RNS_16remove_referenceISA_E4typeE(%"class.std::__2::allocator.108"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::allocator.108"*, align 4
  store %"class.std::__2::allocator.108"* %__t, %"class.std::__2::allocator.108"** %__t.addr, align 4
  %0 = load %"class.std::__2::allocator.108"*, %"class.std::__2::allocator.108"** %__t.addr, align 4
  ret %"class.std::__2::allocator.108"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.128"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEELi1ELb0EEC2IS9_vEEOT_(%"struct.std::__2::__compressed_pair_elem.128"* returned %this, %"class.std::__2::allocator.108"* nonnull align 1 dereferenceable(1) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.128"*, align 4
  %__u.addr = alloca %"class.std::__2::allocator.108"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.128"* %this, %"struct.std::__2::__compressed_pair_elem.128"** %this.addr, align 4
  store %"class.std::__2::allocator.108"* %__u, %"class.std::__2::allocator.108"** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.128"*, %"struct.std::__2::__compressed_pair_elem.128"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.128", %"struct.std::__2::__compressed_pair_elem.128"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::allocator.108"*, %"class.std::__2::allocator.108"** %__u.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.108"* @_ZNSt3__27forwardIRNS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEEEOT_RNS_16remove_referenceISA_E4typeE(%"class.std::__2::allocator.108"* nonnull align 1 dereferenceable(1) %0) #8
  store %"class.std::__2::allocator.108"* %call, %"class.std::__2::allocator.108"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.128"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::array"* @_ZNSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEE8allocateEmPKv(%"class.std::__2::allocator.108"* %this, i32 %__n, i8* %0) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.108"*, align 4
  %__n.addr = alloca i32, align 4
  %.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.108"* %this, %"class.std::__2::allocator.108"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::allocator.108"*, %"class.std::__2::allocator.108"** %this.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEE8max_sizeEv(%"class.std::__2::allocator.108"* %this1) #8
  %cmp = icmp ugt i32 %1, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__220__throw_length_errorEPKc(i8* getelementptr inbounds ([68 x i8], [68 x i8]* @.str, i32 0, i32 0)) #9
  unreachable

if.end:                                           ; preds = %entry
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 12
  %call2 = call i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %mul, i32 4)
  %3 = bitcast i8* %call2 to %"struct.std::__2::array"*
  ret %"struct.std::__2::array"* %3
}

; Function Attrs: noinline noreturn nounwind optnone
define linkonce_odr hidden void @_ZNSt3__220__throw_length_errorEPKc(i8* %__msg) #5 comdat {
entry:
  %__msg.addr = alloca i8*, align 4
  store i8* %__msg, i8** %__msg.addr, align 4
  call void @abort() #9
  unreachable
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %__size, i32 %__align) #0 comdat {
entry:
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i32, i32* %__size.addr, align 4
  %call = call noalias nonnull i8* @_Znwm(i32 %0) #10
  ret i8* %call
}

; Function Attrs: noreturn
declare void @abort() #4

; Function Attrs: nobuiltin allocsize(0)
declare nonnull i8* @_Znwm(i32) #6

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.108"* @_ZNSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE6secondEv(%"class.std::__2::__compressed_pair.127"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.127"*, align 4
  store %"class.std::__2::__compressed_pair.127"* %this, %"class.std::__2::__compressed_pair.127"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.127"*, %"class.std::__2::__compressed_pair.127"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.127"* %this1 to i8*
  %add.ptr = getelementptr inbounds i8, i8* %0, i32 4
  %1 = bitcast i8* %add.ptr to %"struct.std::__2::__compressed_pair_elem.128"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.108"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.128"* %1) #8
  ret %"class.std::__2::allocator.108"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.108"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.128"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.128"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.128"* %this, %"struct.std::__2::__compressed_pair_elem.128"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.128"*, %"struct.std::__2::__compressed_pair_elem.128"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.128", %"struct.std::__2::__compressed_pair_elem.128"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::allocator.108"*, %"class.std::__2::allocator.108"** %__value_, align 4
  ret %"class.std::__2::allocator.108"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.127"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.127"*, align 4
  store %"class.std::__2::__compressed_pair.127"* %this, %"class.std::__2::__compressed_pair.127"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.127"*, %"class.std::__2::__compressed_pair.127"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.127"* %this1 to %"struct.std::__2::__compressed_pair_elem.106"*
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNSt3__222__compressed_pair_elemIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.106"* %0) #8
  ret %"struct.std::__2::array"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE21_ConstructTransactionC2EPPS6_m(%"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction"* returned %this, %"struct.std::__2::array"** %__p, i32 %__n) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction"*, align 4
  %__p.addr = alloca %"struct.std::__2::array"**, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction"* %this, %"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction"** %this.addr, align 4
  store %"struct.std::__2::array"** %__p, %"struct.std::__2::array"*** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction"*, %"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load %"struct.std::__2::array"**, %"struct.std::__2::array"*** %__p.addr, align 4
  %1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %0, align 4
  store %"struct.std::__2::array"* %1, %"struct.std::__2::array"** %__pos_, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction"* %this1, i32 0, i32 1
  %2 = load %"struct.std::__2::array"**, %"struct.std::__2::array"*** %__p.addr, align 4
  %3 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %2, align 4
  %4 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %3, i32 %4
  store %"struct.std::__2::array"* %add.ptr, %"struct.std::__2::array"** %__end_, align 4
  %__dest_ = getelementptr inbounds %"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction"* %this1, i32 0, i32 2
  %5 = load %"struct.std::__2::array"**, %"struct.std::__2::array"*** %__p.addr, align 4
  store %"struct.std::__2::array"** %5, %"struct.std::__2::array"*** %__dest_, align 4
  ret %"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction"*, align 4
  store %"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction"* %this, %"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction"*, %"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__pos_, align 4
  %__dest_ = getelementptr inbounds %"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction"* %this1, i32 0, i32 2
  %1 = load %"struct.std::__2::array"**, %"struct.std::__2::array"*** %__dest_, align 4
  store %"struct.std::__2::array"* %0, %"struct.std::__2::array"** %1, align 4
  ret %"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE17__annotate_deleteEv(%"class.std::__2::vector.102"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.102"*, align 4
  store %"class.std::__2::vector.102"* %this, %"class.std::__2::vector.102"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.102"*, %"class.std::__2::vector.102"** %this.addr, align 4
  %call = call %"struct.std::__2::array"* @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.102"* %this1) #8
  %0 = bitcast %"struct.std::__2::array"* %call to i8*
  %call2 = call %"struct.std::__2::array"* @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.102"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector.102"* %this1) #8
  %add.ptr = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %call2, i32 %call3
  %1 = bitcast %"struct.std::__2::array"* %add.ptr to i8*
  %call4 = call %"struct.std::__2::array"* @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.102"* %this1) #8
  %call5 = call i32 @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.102"* %this1) #8
  %add.ptr6 = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %call4, i32 %call5
  %2 = bitcast %"struct.std::__2::array"* %add.ptr6 to i8*
  %call7 = call %"struct.std::__2::array"* @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.102"* %this1) #8
  %call8 = call i32 @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector.102"* %this1) #8
  %add.ptr9 = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %call7, i32 %call8
  %3 = bitcast %"struct.std::__2::array"* %add.ptr9 to i8*
  call void @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE31__annotate_contiguous_containerEPKvSB_SB_SB_(%"class.std::__2::vector.102"* %this1, i8* %0, i8* %1, i8* %2, i8* %3) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE46__construct_backward_with_exception_guaranteesIS7_EENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS8_PT_SE_EE5valuesr31is_trivially_move_constructibleISE_EE5valueEvE4typeERS8_SF_SF_RSF_(%"class.std::__2::allocator.108"* nonnull align 1 dereferenceable(1) %0, %"struct.std::__2::array"* %__begin1, %"struct.std::__2::array"* %__end1, %"struct.std::__2::array"** nonnull align 4 dereferenceable(4) %__end2) #0 comdat {
entry:
  %.addr = alloca %"class.std::__2::allocator.108"*, align 4
  %__begin1.addr = alloca %"struct.std::__2::array"*, align 4
  %__end1.addr = alloca %"struct.std::__2::array"*, align 4
  %__end2.addr = alloca %"struct.std::__2::array"**, align 4
  %_Np = alloca i32, align 4
  store %"class.std::__2::allocator.108"* %0, %"class.std::__2::allocator.108"** %.addr, align 4
  store %"struct.std::__2::array"* %__begin1, %"struct.std::__2::array"** %__begin1.addr, align 4
  store %"struct.std::__2::array"* %__end1, %"struct.std::__2::array"** %__end1.addr, align 4
  store %"struct.std::__2::array"** %__end2, %"struct.std::__2::array"*** %__end2.addr, align 4
  %1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__end1.addr, align 4
  %2 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__begin1.addr, align 4
  %sub.ptr.lhs.cast = ptrtoint %"struct.std::__2::array"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"struct.std::__2::array"* %2 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 12
  store i32 %sub.ptr.div, i32* %_Np, align 4
  %3 = load i32, i32* %_Np, align 4
  %4 = load %"struct.std::__2::array"**, %"struct.std::__2::array"*** %__end2.addr, align 4
  %5 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %4, align 4
  %idx.neg = sub i32 0, %3
  %add.ptr = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %5, i32 %idx.neg
  store %"struct.std::__2::array"* %add.ptr, %"struct.std::__2::array"** %4, align 4
  %6 = load i32, i32* %_Np, align 4
  %cmp = icmp sgt i32 %6, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %7 = load %"struct.std::__2::array"**, %"struct.std::__2::array"*** %__end2.addr, align 4
  %8 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %7, align 4
  %9 = bitcast %"struct.std::__2::array"* %8 to i8*
  %10 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__begin1.addr, align 4
  %11 = bitcast %"struct.std::__2::array"* %10 to i8*
  %12 = load i32, i32* %_Np, align 4
  %mul = mul i32 %12, 12
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %11, i32 %mul, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__24swapIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS9_EE5valueEvE4typeERS9_SC_(%"struct.std::__2::array"** nonnull align 4 dereferenceable(4) %__x, %"struct.std::__2::array"** nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %__x.addr = alloca %"struct.std::__2::array"**, align 4
  %__y.addr = alloca %"struct.std::__2::array"**, align 4
  %__t = alloca %"struct.std::__2::array"*, align 4
  store %"struct.std::__2::array"** %__x, %"struct.std::__2::array"*** %__x.addr, align 4
  store %"struct.std::__2::array"** %__y, %"struct.std::__2::array"*** %__y.addr, align 4
  %0 = load %"struct.std::__2::array"**, %"struct.std::__2::array"*** %__x.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNSt3__24moveIRPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEEEONS_16remove_referenceIT_E4typeEOSA_(%"struct.std::__2::array"** nonnull align 4 dereferenceable(4) %0) #8
  %1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %call, align 4
  store %"struct.std::__2::array"* %1, %"struct.std::__2::array"** %__t, align 4
  %2 = load %"struct.std::__2::array"**, %"struct.std::__2::array"*** %__y.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNSt3__24moveIRPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEEEONS_16remove_referenceIT_E4typeEOSA_(%"struct.std::__2::array"** nonnull align 4 dereferenceable(4) %2) #8
  %3 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %call1, align 4
  %4 = load %"struct.std::__2::array"**, %"struct.std::__2::array"*** %__x.addr, align 4
  store %"struct.std::__2::array"* %3, %"struct.std::__2::array"** %4, align 4
  %call2 = call nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNSt3__24moveIRPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEEEONS_16remove_referenceIT_E4typeEOSA_(%"struct.std::__2::array"** nonnull align 4 dereferenceable(4) %__t) #8
  %5 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %call2, align 4
  %6 = load %"struct.std::__2::array"**, %"struct.std::__2::array"*** %__y.addr, align 4
  store %"struct.std::__2::array"* %5, %"struct.std::__2::array"** %6, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE14__annotate_newEm(%"class.std::__2::vector.102"* %this, i32 %__current_size) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.102"*, align 4
  %__current_size.addr = alloca i32, align 4
  store %"class.std::__2::vector.102"* %this, %"class.std::__2::vector.102"** %this.addr, align 4
  store i32 %__current_size, i32* %__current_size.addr, align 4
  %this1 = load %"class.std::__2::vector.102"*, %"class.std::__2::vector.102"** %this.addr, align 4
  %call = call %"struct.std::__2::array"* @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.102"* %this1) #8
  %0 = bitcast %"struct.std::__2::array"* %call to i8*
  %call2 = call %"struct.std::__2::array"* @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.102"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector.102"* %this1) #8
  %add.ptr = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %call2, i32 %call3
  %1 = bitcast %"struct.std::__2::array"* %add.ptr to i8*
  %call4 = call %"struct.std::__2::array"* @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.102"* %this1) #8
  %call5 = call i32 @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector.102"* %this1) #8
  %add.ptr6 = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %call4, i32 %call5
  %2 = bitcast %"struct.std::__2::array"* %add.ptr6 to i8*
  %call7 = call %"struct.std::__2::array"* @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.102"* %this1) #8
  %3 = load i32, i32* %__current_size.addr, align 4
  %add.ptr8 = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %call7, i32 %3
  %4 = bitcast %"struct.std::__2::array"* %add.ptr8 to i8*
  call void @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE31__annotate_contiguous_containerEPKvSB_SB_SB_(%"class.std::__2::vector.102"* %this1, i8* %0, i8* %1, i8* %2, i8* %4) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector.102"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.102"*, align 4
  store %"class.std::__2::vector.102"* %this, %"class.std::__2::vector.102"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.102"*, %"class.std::__2::vector.102"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE31__annotate_contiguous_containerEPKvSB_SB_SB_(%"class.std::__2::vector.102"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.102"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector.102"* %this, %"class.std::__2::vector.102"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector.102"*, %"class.std::__2::vector.102"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::array"* @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.102"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.102"*, align 4
  store %"class.std::__2::vector.102"* %this, %"class.std::__2::vector.102"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.102"*, %"class.std::__2::vector.102"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.102"* %this1 to %"class.std::__2::__vector_base.103"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.103", %"class.std::__2::__vector_base.103"* %0, i32 0, i32 0
  %1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__begin_, align 4
  %call = call %"struct.std::__2::array"* @_ZNSt3__212__to_addressINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEEEPT_S8_(%"struct.std::__2::array"* %1) #8
  ret %"struct.std::__2::array"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNSt3__24moveIRPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEEEONS_16remove_referenceIT_E4typeEOSA_(%"struct.std::__2::array"** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"struct.std::__2::array"**, align 4
  store %"struct.std::__2::array"** %__t, %"struct.std::__2::array"*** %__t.addr, align 4
  %0 = load %"struct.std::__2::array"**, %"struct.std::__2::array"*** %__t.addr, align 4
  ret %"struct.std::__2::array"** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE5clearEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 1
  %0 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__begin_, align 4
  call void @_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE17__destruct_at_endEPS6_(%"struct.std::__2::__split_buffer"* %this1, %"struct.std::__2::array"* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE10deallocateERS8_PS7_m(%"class.std::__2::allocator.108"* nonnull align 1 dereferenceable(1) %__a, %"struct.std::__2::array"* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.108"*, align 4
  %__p.addr = alloca %"struct.std::__2::array"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.108"* %__a, %"class.std::__2::allocator.108"** %__a.addr, align 4
  store %"struct.std::__2::array"* %__p, %"struct.std::__2::array"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.108"*, %"class.std::__2::allocator.108"** %__a.addr, align 4
  %1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEE10deallocateEPS6_m(%"class.std::__2::allocator.108"* %0, %"struct.std::__2::array"* %1, i32 %2) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE8capacityEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNKSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this1) #8
  %0 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %call, align 4
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__first_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"struct.std::__2::array"* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint %"struct.std::__2::array"* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 12
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE17__destruct_at_endEPS6_(%"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::array"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__new_last.addr = alloca %"struct.std::__2::array"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant.129", align 1
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store %"struct.std::__2::array"* %__new_last, %"struct.std::__2::array"** %__new_last.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %0 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__new_last.addr, align 4
  call void @_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE17__destruct_at_endEPS6_NS_17integral_constantIbLb0EEE(%"struct.std::__2::__split_buffer"* %this1, %"struct.std::__2::array"* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE17__destruct_at_endEPS6_NS_17integral_constantIbLb0EEE(%"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::array"* %__new_last) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant.129", align 1
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__new_last.addr = alloca %"struct.std::__2::array"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store %"struct.std::__2::array"* %__new_last, %"struct.std::__2::array"** %__new_last.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__new_last.addr, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 2
  %2 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__end_, align 4
  %cmp = icmp ne %"struct.std::__2::array"* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.108"* @_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE7__allocEv(%"struct.std::__2::__split_buffer"* %this1) #8
  %__end_2 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 2
  %3 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__end_2, align 4
  %incdec.ptr = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %3, i32 -1
  store %"struct.std::__2::array"* %incdec.ptr, %"struct.std::__2::array"** %__end_2, align 4
  %call3 = call %"struct.std::__2::array"* @_ZNSt3__212__to_addressINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEEEPT_S8_(%"struct.std::__2::array"* %incdec.ptr) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE7destroyIS7_EEvRS8_PT_(%"class.std::__2::allocator.108"* nonnull align 1 dereferenceable(1) %call, %"struct.std::__2::array"* %call3)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE7destroyIS7_EEvRS8_PT_(%"class.std::__2::allocator.108"* nonnull align 1 dereferenceable(1) %__a, %"struct.std::__2::array"* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.108"*, align 4
  %__p.addr = alloca %"struct.std::__2::array"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy", align 1
  store %"class.std::__2::allocator.108"* %__a, %"class.std::__2::allocator.108"** %__a.addr, align 4
  store %"struct.std::__2::array"* %__p, %"struct.std::__2::array"** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.108"*, %"class.std::__2::allocator.108"** %__a.addr, align 4
  %2 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE9__destroyIS7_EEvNS_17integral_constantIbLb1EEERS8_PT_(%"class.std::__2::allocator.108"* nonnull align 1 dereferenceable(1) %1, %"struct.std::__2::array"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE9__destroyIS7_EEvNS_17integral_constantIbLb1EEERS8_PT_(%"class.std::__2::allocator.108"* nonnull align 1 dereferenceable(1) %__a, %"struct.std::__2::array"* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.108"*, align 4
  %__p.addr = alloca %"struct.std::__2::array"*, align 4
  store %"class.std::__2::allocator.108"* %__a, %"class.std::__2::allocator.108"** %__a.addr, align 4
  store %"struct.std::__2::array"* %__p, %"struct.std::__2::array"** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.108"*, %"class.std::__2::allocator.108"** %__a.addr, align 4
  %2 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__p.addr, align 4
  call void @_ZNSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEE7destroyEPS6_(%"class.std::__2::allocator.108"* %1, %"struct.std::__2::array"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEE7destroyEPS6_(%"class.std::__2::allocator.108"* %this, %"struct.std::__2::array"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.108"*, align 4
  %__p.addr = alloca %"struct.std::__2::array"*, align 4
  store %"class.std::__2::allocator.108"* %this, %"class.std::__2::allocator.108"** %this.addr, align 4
  store %"struct.std::__2::array"* %__p, %"struct.std::__2::array"** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.108"*, %"class.std::__2::allocator.108"** %this.addr, align 4
  %0 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__p.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEE10deallocateEPS6_m(%"class.std::__2::allocator.108"* %this, %"struct.std::__2::array"* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.108"*, align 4
  %__p.addr = alloca %"struct.std::__2::array"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.108"* %this, %"class.std::__2::allocator.108"** %this.addr, align 4
  store %"struct.std::__2::array"* %__p, %"struct.std::__2::array"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.108"*, %"class.std::__2::allocator.108"** %this.addr, align 4
  %0 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__p.addr, align 4
  %1 = bitcast %"struct.std::__2::array"* %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 12
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %__ptr, i32 %__size, i32 %__align) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  %1 = load i32, i32* %__size.addr, align 4
  %2 = load i32, i32* %__align.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm(i8* %0, i32 %1, i32 %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm(i8* %__ptr, i32 %__size, i32 %__align) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  %1 = load i32, i32* %__size.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm(i8* %0, i32 %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm(i8* %__ptr, i32 %__size) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller9__do_callEPv(i8* %0)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller9__do_callEPv(i8* %__ptr) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  call void @_ZdlPv(i8* %0) #11
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #7

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNKSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNKSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.127"* %__end_cap_) #8
  ret %"struct.std::__2::array"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNKSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.127"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.127"*, align 4
  store %"class.std::__2::__compressed_pair.127"* %this, %"class.std::__2::__compressed_pair.127"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.127"*, %"class.std::__2::__compressed_pair.127"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.127"* %this1 to %"struct.std::__2::__compressed_pair_elem.106"*
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNKSt3__222__compressed_pair_elemIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.106"* %0) #8
  ret %"struct.std::__2::array"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE27__invalidate_iterators_pastEPS6_(%"class.std::__2::vector.102"* %this, %"struct.std::__2::array"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.102"*, align 4
  %__new_last.addr = alloca %"struct.std::__2::array"*, align 4
  store %"class.std::__2::vector.102"* %this, %"class.std::__2::vector.102"** %this.addr, align 4
  store %"struct.std::__2::array"* %__new_last, %"struct.std::__2::array"** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::vector.102"*, %"class.std::__2::vector.102"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE17__destruct_at_endEPS6_(%"class.std::__2::__vector_base.103"* %this, %"struct.std::__2::array"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.103"*, align 4
  %__new_last.addr = alloca %"struct.std::__2::array"*, align 4
  %__soon_to_be_end = alloca %"struct.std::__2::array"*, align 4
  store %"class.std::__2::__vector_base.103"* %this, %"class.std::__2::__vector_base.103"** %this.addr, align 4
  store %"struct.std::__2::array"* %__new_last, %"struct.std::__2::array"** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.103"*, %"class.std::__2::__vector_base.103"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.103", %"class.std::__2::__vector_base.103"* %this1, i32 0, i32 1
  %0 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__end_, align 4
  store %"struct.std::__2::array"* %0, %"struct.std::__2::array"** %__soon_to_be_end, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__new_last.addr, align 4
  %2 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__soon_to_be_end, align 4
  %cmp = icmp ne %"struct.std::__2::array"* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.108"* @_ZNSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base.103"* %this1) #8
  %3 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__soon_to_be_end, align 4
  %incdec.ptr = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %3, i32 -1
  store %"struct.std::__2::array"* %incdec.ptr, %"struct.std::__2::array"** %__soon_to_be_end, align 4
  %call2 = call %"struct.std::__2::array"* @_ZNSt3__212__to_addressINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEEEPT_S8_(%"struct.std::__2::array"* %incdec.ptr) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE7destroyIS7_EEvRS8_PT_(%"class.std::__2::allocator.108"* nonnull align 1 dereferenceable(1) %call, %"struct.std::__2::array"* %call2)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %4 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__new_last.addr, align 4
  %__end_3 = getelementptr inbounds %"class.std::__2::__vector_base.103", %"class.std::__2::__vector_base.103"* %this1, i32 0, i32 1
  store %"struct.std::__2::array"* %4, %"struct.std::__2::array"** %__end_3, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE17__annotate_shrinkEm(%"class.std::__2::vector.102"* %this, i32 %__old_size) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.102"*, align 4
  %__old_size.addr = alloca i32, align 4
  store %"class.std::__2::vector.102"* %this, %"class.std::__2::vector.102"** %this.addr, align 4
  store i32 %__old_size, i32* %__old_size.addr, align 4
  %this1 = load %"class.std::__2::vector.102"*, %"class.std::__2::vector.102"** %this.addr, align 4
  %call = call %"struct.std::__2::array"* @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.102"* %this1) #8
  %0 = bitcast %"struct.std::__2::array"* %call to i8*
  %call2 = call %"struct.std::__2::array"* @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.102"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector.102"* %this1) #8
  %add.ptr = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %call2, i32 %call3
  %1 = bitcast %"struct.std::__2::array"* %add.ptr to i8*
  %call4 = call %"struct.std::__2::array"* @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.102"* %this1) #8
  %2 = load i32, i32* %__old_size.addr, align 4
  %add.ptr5 = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %call4, i32 %2
  %3 = bitcast %"struct.std::__2::array"* %add.ptr5 to i8*
  %call6 = call %"struct.std::__2::array"* @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.102"* %this1) #8
  %call7 = call i32 @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.102"* %this1) #8
  %add.ptr8 = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %call6, i32 %call7
  %4 = bitcast %"struct.std::__2::array"* %add.ptr8 to i8*
  call void @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE31__annotate_contiguous_containerEPKvSB_SB_SB_(%"class.std::__2::vector.102"* %this1, i8* %0, i8* %1, i8* %3, i8* %4) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType.104"* @_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEC2Ev(%"class.draco::IndexType.104"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.104"*, align 4
  store %"class.draco::IndexType.104"* %this, %"class.draco::IndexType.104"** %this.addr, align 4
  %this1 = load %"class.draco::IndexType.104"*, %"class.draco::IndexType.104"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.104", %"class.draco::IndexType.104"* %this1, i32 0, i32 0
  store i32 0, i32* %value_, align 4
  ret %"class.draco::IndexType.104"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"struct.std::__2::array"* @_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEEixEm(%"class.std::__2::vector.102"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.102"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector.102"* %this, %"class.std::__2::vector.102"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.102"*, %"class.std::__2::vector.102"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.102"* %this1 to %"class.std::__2::__vector_base.103"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.103", %"class.std::__2::__vector_base.103"* %0, i32 0, i32 0
  %1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__begin_, align 4
  %2 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %1, i32 %2
  ret %"struct.std::__2::array"* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.51"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.51"*, align 4
  store %"class.std::__2::vector.51"* %this, %"class.std::__2::vector.51"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.51"*, %"class.std::__2::vector.51"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.51"* %this1 to %"class.std::__2::__vector_base.52"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.52", %"class.std::__2::__vector_base.52"* %0, i32 0, i32 1
  %1 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector.51"* %this1 to %"class.std::__2::__vector_base.52"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.52", %"class.std::__2::__vector_base.52"* %2, i32 0, i32 0
  %3 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.std::__2::unique_ptr.53"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.std::__2::unique_ptr.53"* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.53"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEixEm(%"class.std::__2::vector.51"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.51"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector.51"* %this, %"class.std::__2::vector.51"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.51"*, %"class.std::__2::vector.51"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.51"* %this1 to %"class.std::__2::__vector_base.52"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.52", %"class.std::__2::__vector_base.52"* %0, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__begin_, align 4
  %2 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds %"class.std::__2::unique_ptr.53", %"class.std::__2::unique_ptr.53"* %1, i32 %2
  ret %"class.std::__2::unique_ptr.53"* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::PointAttribute"* @_ZNKSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEE3getEv(%"class.std::__2::unique_ptr.53"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.53"*, align 4
  store %"class.std::__2::unique_ptr.53"* %this, %"class.std::__2::unique_ptr.53"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.53", %"class.std::__2::unique_ptr.53"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNKSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.54"* %__ptr_) #8
  %0 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %call, align 4
  ret %"class.draco::PointAttribute"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNKSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.54"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.54"*, align 4
  store %"class.std::__2::__compressed_pair.54"* %this, %"class.std::__2::__compressed_pair.54"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.54"*, %"class.std::__2::__compressed_pair.54"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.54"* %this1 to %"struct.std::__2::__compressed_pair_elem.55"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNKSt3__222__compressed_pair_elemIPN5draco14PointAttributeELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.55"* %0) #8
  ret %"class.draco::PointAttribute"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNKSt3__222__compressed_pair_elemIPN5draco14PointAttributeELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.55"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.55"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.55"* %this, %"struct.std::__2::__compressed_pair_elem.55"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.55"*, %"struct.std::__2::__compressed_pair_elem.55"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.55", %"struct.std::__2::__compressed_pair_elem.55"* %this1, i32 0, i32 0
  ret %"class.draco::PointAttribute"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN5draco10DataBuffer4dataEv(%"class.draco::DataBuffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::DataBuffer"*, align 4
  store %"class.draco::DataBuffer"* %this, %"class.draco::DataBuffer"** %this.addr, align 4
  %this1 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %this.addr, align 4
  %data_ = getelementptr inbounds %"class.draco::DataBuffer", %"class.draco::DataBuffer"* %this1, i32 0, i32 0
  %call = call nonnull align 1 dereferenceable(1) i8* @_ZNSt3__26vectorIhNS_9allocatorIhEEEixEm(%"class.std::__2::vector.56"* %data_, i32 0) #8
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) i8* @_ZNSt3__26vectorIhNS_9allocatorIhEEEixEm(%"class.std::__2::vector.56"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.56"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector.56"* %this, %"class.std::__2::vector.56"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.56"*, %"class.std::__2::vector.56"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.56"* %this1 to %"class.std::__2::__vector_base.57"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.57", %"class.std::__2::__vector_base.57"* %0, i32 0, i32 0
  %1 = load i8*, i8** %__begin_, align 4
  %2 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds i8, i8* %1, i32 %2
  ret i8* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::DataBuffer"* @_ZNKSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEE3getEv(%"class.std::__2::unique_ptr.63"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.63"*, align 4
  store %"class.std::__2::unique_ptr.63"* %this, %"class.std::__2::unique_ptr.63"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.63"*, %"class.std::__2::unique_ptr.63"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.63", %"class.std::__2::unique_ptr.63"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::DataBuffer"** @_ZNKSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.64"* %__ptr_) #8
  %0 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %call, align 4
  ret %"class.draco::DataBuffer"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::DataBuffer"** @_ZNKSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.64"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.64"*, align 4
  store %"class.std::__2::__compressed_pair.64"* %this, %"class.std::__2::__compressed_pair.64"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.64"*, %"class.std::__2::__compressed_pair.64"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.64"* %this1 to %"struct.std::__2::__compressed_pair_elem.65"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::DataBuffer"** @_ZNKSt3__222__compressed_pair_elemIPN5draco10DataBufferELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.65"* %0) #8
  ret %"class.draco::DataBuffer"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::DataBuffer"** @_ZNKSt3__222__compressed_pair_elemIPN5draco10DataBufferELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.65"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.65"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.65"* %this, %"struct.std::__2::__compressed_pair_elem.65"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.65"*, %"struct.std::__2::__compressed_pair_elem.65"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.65", %"struct.std::__2::__compressed_pair_elem.65"* %this1, i32 0, i32 0
  ret %"class.draco::DataBuffer"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EE5valueEv(%"class.draco::IndexType"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.draco::IndexType"* %this, %"class.draco::IndexType"** %this.addr, align 4
  %this1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value_, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEEE6resizeEmRKS5_(%"class.draco::IndexTypeVector"* %this, i32 %size, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %val) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector"*, align 4
  %size.addr = alloca i32, align 4
  %val.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.draco::IndexTypeVector"* %this, %"class.draco::IndexTypeVector"** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  store %"class.draco::IndexType"* %val, %"class.draco::IndexType"** %val.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector"*, %"class.draco::IndexTypeVector"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector", %"class.draco::IndexTypeVector"* %this1, i32 0, i32 0
  %0 = load i32, i32* %size.addr, align 4
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %val.addr, align 4
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE6resizeEmRKS4_(%"class.std::__2::vector.68"* %vector_, i32 %0, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE6resizeEmRKS4_(%"class.std::__2::vector.68"* %this, i32 %__sz, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %__x) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.68"*, align 4
  %__sz.addr = alloca i32, align 4
  %__x.addr = alloca %"class.draco::IndexType"*, align 4
  %__cs = alloca i32, align 4
  store %"class.std::__2::vector.68"* %this, %"class.std::__2::vector.68"** %this.addr, align 4
  store i32 %__sz, i32* %__sz.addr, align 4
  store %"class.draco::IndexType"* %__x, %"class.draco::IndexType"** %__x.addr, align 4
  %this1 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.68"* %this1) #8
  store i32 %call, i32* %__cs, align 4
  %0 = load i32, i32* %__cs, align 4
  %1 = load i32, i32* %__sz.addr, align 4
  %cmp = icmp ult i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %__sz.addr, align 4
  %3 = load i32, i32* %__cs, align 4
  %sub = sub i32 %2, %3
  %4 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__x.addr, align 4
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8__appendEmRKS4_(%"class.std::__2::vector.68"* %this1, i32 %sub, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %4)
  br label %if.end4

if.else:                                          ; preds = %entry
  %5 = load i32, i32* %__cs, align 4
  %6 = load i32, i32* %__sz.addr, align 4
  %cmp2 = icmp ugt i32 %5, %6
  br i1 %cmp2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.else
  %7 = bitcast %"class.std::__2::vector.68"* %this1 to %"class.std::__2::__vector_base.69"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %7, i32 0, i32 0
  %8 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  %9 = load i32, i32* %__sz.addr, align 4
  %add.ptr = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %8, i32 %9
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_(%"class.std::__2::vector.68"* %this1, %"class.draco::IndexType"* %add.ptr) #8
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.else
  br label %if.end4

if.end4:                                          ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.68"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.68"*, align 4
  store %"class.std::__2::vector.68"* %this, %"class.std::__2::vector.68"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.68"* %this1 to %"class.std::__2::__vector_base.69"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %0, i32 0, i32 1
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector.68"* %this1 to %"class.std::__2::__vector_base.69"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %2, i32 0, i32 0
  %3 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.draco::IndexType"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.draco::IndexType"* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8__appendEmRKS4_(%"class.std::__2::vector.68"* %this, i32 %__n, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %__x) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.68"*, align 4
  %__n.addr = alloca i32, align 4
  %__x.addr = alloca %"class.draco::IndexType"*, align 4
  %__a = alloca %"class.std::__2::allocator.73"*, align 4
  %__v = alloca %"struct.std::__2::__split_buffer.130", align 4
  store %"class.std::__2::vector.68"* %this, %"class.std::__2::vector.68"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store %"class.draco::IndexType"* %__x, %"class.draco::IndexType"** %__x.addr, align 4
  %this1 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.68"* %this1 to %"class.std::__2::__vector_base.69"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base.69"* %0) #8
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %call, align 4
  %2 = bitcast %"class.std::__2::vector.68"* %this1 to %"class.std::__2::__vector_base.69"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %2, i32 0, i32 1
  %3 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__end_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.draco::IndexType"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.draco::IndexType"* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  %4 = load i32, i32* %__n.addr, align 4
  %cmp = icmp uge i32 %sub.ptr.div, %4
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %5 = load i32, i32* %__n.addr, align 4
  %6 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__x.addr, align 4
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE18__construct_at_endEmRKS4_(%"class.std::__2::vector.68"* %this1, i32 %5, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %6)
  br label %if.end

if.else:                                          ; preds = %entry
  %7 = bitcast %"class.std::__2::vector.68"* %this1 to %"class.std::__2::__vector_base.69"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.69"* %7) #8
  store %"class.std::__2::allocator.73"* %call2, %"class.std::__2::allocator.73"** %__a, align 4
  %call3 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.68"* %this1) #8
  %8 = load i32, i32* %__n.addr, align 4
  %add = add i32 %call3, %8
  %call4 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE11__recommendEm(%"class.std::__2::vector.68"* %this1, i32 %add)
  %call5 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.68"* %this1) #8
  %9 = load %"class.std::__2::allocator.73"*, %"class.std::__2::allocator.73"** %__a, align 4
  %call6 = call %"struct.std::__2::__split_buffer.130"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEEC2EmmS7_(%"struct.std::__2::__split_buffer.130"* %__v, i32 %call4, i32 %call5, %"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %9)
  %10 = load i32, i32* %__n.addr, align 4
  %11 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__x.addr, align 4
  call void @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE18__construct_at_endEmRKS4_(%"struct.std::__2::__split_buffer.130"* %__v, i32 %10, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %11)
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS4_RS6_EE(%"class.std::__2::vector.68"* %this1, %"struct.std::__2::__split_buffer.130"* nonnull align 4 dereferenceable(20) %__v)
  %call7 = call %"struct.std::__2::__split_buffer.130"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEED2Ev(%"struct.std::__2::__split_buffer.130"* %__v) #8
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_(%"class.std::__2::vector.68"* %this, %"class.draco::IndexType"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.68"*, align 4
  %__new_last.addr = alloca %"class.draco::IndexType"*, align 4
  %__old_size = alloca i32, align 4
  store %"class.std::__2::vector.68"* %this, %"class.std::__2::vector.68"** %this.addr, align 4
  store %"class.draco::IndexType"* %__new_last, %"class.draco::IndexType"** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %this.addr, align 4
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__new_last.addr, align 4
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE27__invalidate_iterators_pastEPS4_(%"class.std::__2::vector.68"* %this1, %"class.draco::IndexType"* %0)
  %call = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.68"* %this1) #8
  store i32 %call, i32* %__old_size, align 4
  %1 = bitcast %"class.std::__2::vector.68"* %this1 to %"class.std::__2::__vector_base.69"*
  %2 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__new_last.addr, align 4
  call void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_(%"class.std::__2::__vector_base.69"* %1, %"class.draco::IndexType"* %2) #8
  %3 = load i32, i32* %__old_size, align 4
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_shrinkEm(%"class.std::__2::vector.68"* %this1, i32 %3) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base.69"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.69"*, align 4
  store %"class.std::__2::__vector_base.69"* %this, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.69"*, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.70"* %__end_cap_) #8
  ret %"class.draco::IndexType"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE18__construct_at_endEmRKS4_(%"class.std::__2::vector.68"* %this, i32 %__n, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %__x) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.68"*, align 4
  %__n.addr = alloca i32, align 4
  %__x.addr = alloca %"class.draco::IndexType"*, align 4
  %__tx = alloca %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction", align 4
  store %"class.std::__2::vector.68"* %this, %"class.std::__2::vector.68"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store %"class.draco::IndexType"* %__x, %"class.draco::IndexType"** %__x.addr, align 4
  %this1 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %this.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %call = call %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionC2ERS7_m(%"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %__tx, %"class.std::__2::vector.68"* nonnull align 4 dereferenceable(12) %this1, i32 %0)
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__pos_, align 4
  %__new_end_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %__tx, i32 0, i32 2
  %2 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__new_end_, align 4
  %cmp = icmp ne %"class.draco::IndexType"* %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = bitcast %"class.std::__2::vector.68"* %this1 to %"class.std::__2::__vector_base.69"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.69"* %3) #8
  %__pos_3 = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %4 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__pos_3, align 4
  %call4 = call %"class.draco::IndexType"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType"* %4) #8
  %5 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__x.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE9constructIS5_JRKS5_EEEvRS6_PT_DpOT0_(%"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %call2, %"class.draco::IndexType"* %call4, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %5)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %__pos_5 = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %6 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__pos_5, align 4
  %incdec.ptr = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %6, i32 1
  store %"class.draco::IndexType"* %incdec.ptr, %"class.draco::IndexType"** %__pos_5, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call6 = call %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %__tx) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.69"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.69"*, align 4
  store %"class.std::__2::__vector_base.69"* %this, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.69"*, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.70"* %__end_cap_) #8
  ret %"class.std::__2::allocator.73"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE11__recommendEm(%"class.std::__2::vector.68"* %this, i32 %__new_size) #0 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %"class.std::__2::vector.68"*, align 4
  %__new_size.addr = alloca i32, align 4
  %__ms = alloca i32, align 4
  %__cap = alloca i32, align 4
  %ref.tmp = alloca i32, align 4
  store %"class.std::__2::vector.68"* %this, %"class.std::__2::vector.68"** %this.addr, align 4
  store i32 %__new_size, i32* %__new_size.addr, align 4
  %this1 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8max_sizeEv(%"class.std::__2::vector.68"* %this1) #8
  store i32 %call, i32* %__ms, align 4
  %0 = load i32, i32* %__new_size.addr, align 4
  %1 = load i32, i32* %__ms, align 4
  %cmp = icmp ugt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = bitcast %"class.std::__2::vector.68"* %this1 to %"class.std::__2::__vector_base_common"*
  call void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"* %2) #9
  unreachable

if.end:                                           ; preds = %entry
  %call2 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.68"* %this1) #8
  store i32 %call2, i32* %__cap, align 4
  %3 = load i32, i32* %__cap, align 4
  %4 = load i32, i32* %__ms, align 4
  %div = udiv i32 %4, 2
  %cmp3 = icmp uge i32 %3, %div
  br i1 %cmp3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %if.end
  %5 = load i32, i32* %__ms, align 4
  store i32 %5, i32* %retval, align 4
  br label %return

if.end5:                                          ; preds = %if.end
  %6 = load i32, i32* %__cap, align 4
  %mul = mul i32 2, %6
  store i32 %mul, i32* %ref.tmp, align 4
  %call6 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %__new_size.addr)
  %7 = load i32, i32* %call6, align 4
  store i32 %7, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end5, %if.then4
  %8 = load i32, i32* %retval, align 4
  ret i32 %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer.130"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEEC2EmmS7_(%"struct.std::__2::__split_buffer.130"* returned %this, i32 %__cap, i32 %__start, %"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %__a) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"struct.std::__2::__split_buffer.130"*, align 4
  %this.addr = alloca %"struct.std::__2::__split_buffer.130"*, align 4
  %__cap.addr = alloca i32, align 4
  %__start.addr = alloca i32, align 4
  %__a.addr = alloca %"class.std::__2::allocator.73"*, align 4
  %ref.tmp = alloca i8*, align 4
  store %"struct.std::__2::__split_buffer.130"* %this, %"struct.std::__2::__split_buffer.130"** %this.addr, align 4
  store i32 %__cap, i32* %__cap.addr, align 4
  store i32 %__start, i32* %__start.addr, align 4
  store %"class.std::__2::allocator.73"* %__a, %"class.std::__2::allocator.73"** %__a.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.130"*, %"struct.std::__2::__split_buffer.130"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer.130"* %this1, %"struct.std::__2::__split_buffer.130"** %retval, align 4
  %0 = bitcast %"struct.std::__2::__split_buffer.130"* %this1 to %"class.std::__2::__split_buffer_common"*
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer.130", %"struct.std::__2::__split_buffer.130"* %this1, i32 0, i32 3
  store i8* null, i8** %ref.tmp, align 4
  %1 = load %"class.std::__2::allocator.73"*, %"class.std::__2::allocator.73"** %__a.addr, align 4
  %call = call %"class.std::__2::__compressed_pair.131"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEEC2IDnS8_EEOT_OT0_(%"class.std::__2::__compressed_pair.131"* %__end_cap_, i8** nonnull align 4 dereferenceable(4) %ref.tmp, %"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %1)
  %2 = load i32, i32* %__cap.addr, align 4
  %cmp = icmp ne i32 %2, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE7__allocEv(%"struct.std::__2::__split_buffer.130"* %this1) #8
  %3 = load i32, i32* %__cap.addr, align 4
  %call3 = call %"class.draco::IndexType"* @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE8allocateERS6_m(%"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %call2, i32 %3)
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %"class.draco::IndexType"* [ %call3, %cond.true ], [ null, %cond.false ]
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer.130", %"struct.std::__2::__split_buffer.130"* %this1, i32 0, i32 0
  store %"class.draco::IndexType"* %cond, %"class.draco::IndexType"** %__first_, align 4
  %__first_4 = getelementptr inbounds %"struct.std::__2::__split_buffer.130", %"struct.std::__2::__split_buffer.130"* %this1, i32 0, i32 0
  %4 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__first_4, align 4
  %5 = load i32, i32* %__start.addr, align 4
  %add.ptr = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %4, i32 %5
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer.130", %"struct.std::__2::__split_buffer.130"* %this1, i32 0, i32 2
  store %"class.draco::IndexType"* %add.ptr, %"class.draco::IndexType"** %__end_, align 4
  %__begin_ = getelementptr inbounds %"struct.std::__2::__split_buffer.130", %"struct.std::__2::__split_buffer.130"* %this1, i32 0, i32 1
  store %"class.draco::IndexType"* %add.ptr, %"class.draco::IndexType"** %__begin_, align 4
  %__first_5 = getelementptr inbounds %"struct.std::__2::__split_buffer.130", %"struct.std::__2::__split_buffer.130"* %this1, i32 0, i32 0
  %6 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__first_5, align 4
  %7 = load i32, i32* %__cap.addr, align 4
  %add.ptr6 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %6, i32 %7
  %call7 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE9__end_capEv(%"struct.std::__2::__split_buffer.130"* %this1) #8
  store %"class.draco::IndexType"* %add.ptr6, %"class.draco::IndexType"** %call7, align 4
  %8 = load %"struct.std::__2::__split_buffer.130"*, %"struct.std::__2::__split_buffer.130"** %retval, align 4
  ret %"struct.std::__2::__split_buffer.130"* %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE18__construct_at_endEmRKS4_(%"struct.std::__2::__split_buffer.130"* %this, i32 %__n, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %__x) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.130"*, align 4
  %__n.addr = alloca i32, align 4
  %__x.addr = alloca %"class.draco::IndexType"*, align 4
  %__tx = alloca %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction", align 4
  store %"struct.std::__2::__split_buffer.130"* %this, %"struct.std::__2::__split_buffer.130"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store %"class.draco::IndexType"* %__x, %"class.draco::IndexType"** %__x.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.130"*, %"struct.std::__2::__split_buffer.130"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer.130", %"struct.std::__2::__split_buffer.130"* %this1, i32 0, i32 2
  %0 = load i32, i32* %__n.addr, align 4
  %call = call %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE21_ConstructTransactionC2EPPS4_m(%"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"* %__tx, %"class.draco::IndexType"** %__end_, i32 %0) #8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %__pos_ = getelementptr inbounds %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"* %__tx, i32 0, i32 0
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__pos_, align 4
  %__end_2 = getelementptr inbounds %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %2 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__end_2, align 4
  %cmp = icmp ne %"class.draco::IndexType"* %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %call3 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE7__allocEv(%"struct.std::__2::__split_buffer.130"* %this1) #8
  %__pos_4 = getelementptr inbounds %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"* %__tx, i32 0, i32 0
  %3 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__pos_4, align 4
  %call5 = call %"class.draco::IndexType"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType"* %3) #8
  %4 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__x.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE9constructIS5_JRKS5_EEEvRS6_PT_DpOT0_(%"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %call3, %"class.draco::IndexType"* %call5, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %4)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %__pos_6 = getelementptr inbounds %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"* %__tx, i32 0, i32 0
  %5 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__pos_6, align 4
  %incdec.ptr = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %5, i32 1
  store %"class.draco::IndexType"* %incdec.ptr, %"class.draco::IndexType"** %__pos_6, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call7 = call %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"* %__tx) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS4_RS6_EE(%"class.std::__2::vector.68"* %this, %"struct.std::__2::__split_buffer.130"* nonnull align 4 dereferenceable(20) %__v) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.68"*, align 4
  %__v.addr = alloca %"struct.std::__2::__split_buffer.130"*, align 4
  store %"class.std::__2::vector.68"* %this, %"class.std::__2::vector.68"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer.130"* %__v, %"struct.std::__2::__split_buffer.130"** %__v.addr, align 4
  %this1 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %this.addr, align 4
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_deleteEv(%"class.std::__2::vector.68"* %this1) #8
  %0 = bitcast %"class.std::__2::vector.68"* %this1 to %"class.std::__2::__vector_base.69"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.69"* %0) #8
  %1 = bitcast %"class.std::__2::vector.68"* %this1 to %"class.std::__2::__vector_base.69"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %1, i32 0, i32 0
  %2 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  %3 = bitcast %"class.std::__2::vector.68"* %this1 to %"class.std::__2::__vector_base.69"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %3, i32 0, i32 1
  %4 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__end_, align 4
  %5 = load %"struct.std::__2::__split_buffer.130"*, %"struct.std::__2::__split_buffer.130"** %__v.addr, align 4
  %__begin_2 = getelementptr inbounds %"struct.std::__2::__split_buffer.130", %"struct.std::__2::__split_buffer.130"* %5, i32 0, i32 1
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE46__construct_backward_with_exception_guaranteesIS5_EENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS6_PT_SC_EE5valuesr31is_trivially_move_constructibleISC_EE5valueEvE4typeERS6_SD_SD_RSD_(%"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %call, %"class.draco::IndexType"* %2, %"class.draco::IndexType"* %4, %"class.draco::IndexType"** nonnull align 4 dereferenceable(4) %__begin_2)
  %6 = bitcast %"class.std::__2::vector.68"* %this1 to %"class.std::__2::__vector_base.69"*
  %__begin_3 = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %6, i32 0, i32 0
  %7 = load %"struct.std::__2::__split_buffer.130"*, %"struct.std::__2::__split_buffer.130"** %__v.addr, align 4
  %__begin_4 = getelementptr inbounds %"struct.std::__2::__split_buffer.130", %"struct.std::__2::__split_buffer.130"* %7, i32 0, i32 1
  call void @_ZNSt3__24swapIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS7_EE5valueEvE4typeERS7_SA_(%"class.draco::IndexType"** nonnull align 4 dereferenceable(4) %__begin_3, %"class.draco::IndexType"** nonnull align 4 dereferenceable(4) %__begin_4) #8
  %8 = bitcast %"class.std::__2::vector.68"* %this1 to %"class.std::__2::__vector_base.69"*
  %__end_5 = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %8, i32 0, i32 1
  %9 = load %"struct.std::__2::__split_buffer.130"*, %"struct.std::__2::__split_buffer.130"** %__v.addr, align 4
  %__end_6 = getelementptr inbounds %"struct.std::__2::__split_buffer.130", %"struct.std::__2::__split_buffer.130"* %9, i32 0, i32 2
  call void @_ZNSt3__24swapIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS7_EE5valueEvE4typeERS7_SA_(%"class.draco::IndexType"** nonnull align 4 dereferenceable(4) %__end_5, %"class.draco::IndexType"** nonnull align 4 dereferenceable(4) %__end_6) #8
  %10 = bitcast %"class.std::__2::vector.68"* %this1 to %"class.std::__2::__vector_base.69"*
  %call7 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base.69"* %10) #8
  %11 = load %"struct.std::__2::__split_buffer.130"*, %"struct.std::__2::__split_buffer.130"** %__v.addr, align 4
  %call8 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE9__end_capEv(%"struct.std::__2::__split_buffer.130"* %11) #8
  call void @_ZNSt3__24swapIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS7_EE5valueEvE4typeERS7_SA_(%"class.draco::IndexType"** nonnull align 4 dereferenceable(4) %call7, %"class.draco::IndexType"** nonnull align 4 dereferenceable(4) %call8) #8
  %12 = load %"struct.std::__2::__split_buffer.130"*, %"struct.std::__2::__split_buffer.130"** %__v.addr, align 4
  %__begin_9 = getelementptr inbounds %"struct.std::__2::__split_buffer.130", %"struct.std::__2::__split_buffer.130"* %12, i32 0, i32 1
  %13 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_9, align 4
  %14 = load %"struct.std::__2::__split_buffer.130"*, %"struct.std::__2::__split_buffer.130"** %__v.addr, align 4
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer.130", %"struct.std::__2::__split_buffer.130"* %14, i32 0, i32 0
  store %"class.draco::IndexType"* %13, %"class.draco::IndexType"** %__first_, align 4
  %call10 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.68"* %this1) #8
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE14__annotate_newEm(%"class.std::__2::vector.68"* %this1, i32 %call10) #8
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector.68"* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer.130"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEED2Ev(%"struct.std::__2::__split_buffer.130"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"struct.std::__2::__split_buffer.130"*, align 4
  %this.addr = alloca %"struct.std::__2::__split_buffer.130"*, align 4
  store %"struct.std::__2::__split_buffer.130"* %this, %"struct.std::__2::__split_buffer.130"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.130"*, %"struct.std::__2::__split_buffer.130"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer.130"* %this1, %"struct.std::__2::__split_buffer.130"** %retval, align 4
  call void @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE5clearEv(%"struct.std::__2::__split_buffer.130"* %this1) #8
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer.130", %"struct.std::__2::__split_buffer.130"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__first_, align 4
  %tobool = icmp ne %"class.draco::IndexType"* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE7__allocEv(%"struct.std::__2::__split_buffer.130"* %this1) #8
  %__first_2 = getelementptr inbounds %"struct.std::__2::__split_buffer.130", %"struct.std::__2::__split_buffer.130"* %this1, i32 0, i32 0
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__first_2, align 4
  %call3 = call i32 @_ZNKSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE8capacityEv(%"struct.std::__2::__split_buffer.130"* %this1)
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE10deallocateERS6_PS5_m(%"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %call, %"class.draco::IndexType"* %1, i32 %call3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"struct.std::__2::__split_buffer.130"*, %"struct.std::__2::__split_buffer.130"** %retval, align 4
  ret %"struct.std::__2::__split_buffer.130"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.70"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.70"*, align 4
  store %"class.std::__2::__compressed_pair.70"* %this, %"class.std::__2::__compressed_pair.70"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.70"*, %"class.std::__2::__compressed_pair.70"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.70"* %this1 to %"struct.std::__2::__compressed_pair_elem.71"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.71"* %0) #8
  ret %"class.draco::IndexType"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.71"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.71"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.71"* %this, %"struct.std::__2::__compressed_pair_elem.71"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.71"*, %"struct.std::__2::__compressed_pair_elem.71"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.71", %"struct.std::__2::__compressed_pair_elem.71"* %this1, i32 0, i32 0
  ret %"class.draco::IndexType"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionC2ERS7_m(%"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* returned %this, %"class.std::__2::vector.68"* nonnull align 4 dereferenceable(12) %__v, i32 %__n) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"*, align 4
  %__v.addr = alloca %"class.std::__2::vector.68"*, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"** %this.addr, align 4
  store %"class.std::__2::vector.68"* %__v, %"class.std::__2::vector.68"** %__v.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"*, %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"** %this.addr, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %__v.addr, align 4
  store %"class.std::__2::vector.68"* %0, %"class.std::__2::vector.68"** %__v_, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %1 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %__v.addr, align 4
  %2 = bitcast %"class.std::__2::vector.68"* %1 to %"class.std::__2::__vector_base.69"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %2, i32 0, i32 1
  %3 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__end_, align 4
  store %"class.draco::IndexType"* %3, %"class.draco::IndexType"** %__pos_, align 4
  %__new_end_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %this1, i32 0, i32 2
  %4 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %__v.addr, align 4
  %5 = bitcast %"class.std::__2::vector.68"* %4 to %"class.std::__2::__vector_base.69"*
  %__end_2 = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %5, i32 0, i32 1
  %6 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__end_2, align 4
  %7 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %6, i32 %7
  store %"class.draco::IndexType"* %add.ptr, %"class.draco::IndexType"** %__new_end_, align 4
  ret %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE9constructIS5_JRKS5_EEEvRS6_PT_DpOT0_(%"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType"* %__p, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %__args) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.73"*, align 4
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  %__args.addr = alloca %"class.draco::IndexType"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_construct.133", align 1
  store %"class.std::__2::allocator.73"* %__a, %"class.std::__2::allocator.73"** %__a.addr, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  store %"class.draco::IndexType"* %__args, %"class.draco::IndexType"** %__args.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_construct.133"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.73"*, %"class.std::__2::allocator.73"** %__a.addr, align 4
  %2 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  %3 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZNSt3__27forwardIRKN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %3) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE11__constructIS5_JRKS5_EEEvNS_17integral_constantIbLb1EEERS6_PT_DpOT0_(%"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %1, %"class.draco::IndexType"* %2, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType"* %__p) #0 comdat {
entry:
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  ret %"class.draco::IndexType"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"*, align 4
  store %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"** %this.addr, align 4
  %this1 = load %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"*, %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__pos_, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %__v_, align 4
  %2 = bitcast %"class.std::__2::vector.68"* %1 to %"class.std::__2::__vector_base.69"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %2, i32 0, i32 1
  store %"class.draco::IndexType"* %0, %"class.draco::IndexType"** %__end_, align 4
  ret %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE11__constructIS5_JRKS5_EEEvNS_17integral_constantIbLb1EEERS6_PT_DpOT0_(%"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType"* %__p, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %__args) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.73"*, align 4
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  %__args.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.std::__2::allocator.73"* %__a, %"class.std::__2::allocator.73"** %__a.addr, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  store %"class.draco::IndexType"* %__args, %"class.draco::IndexType"** %__args.addr, align 4
  %1 = load %"class.std::__2::allocator.73"*, %"class.std::__2::allocator.73"** %__a.addr, align 4
  %2 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  %3 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZNSt3__27forwardIRKN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %3) #8
  call void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE9constructIS4_JRKS4_EEEvPT_DpOT0_(%"class.std::__2::allocator.73"* %1, %"class.draco::IndexType"* %2, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZNSt3__27forwardIRKN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.draco::IndexType"* %__t, %"class.draco::IndexType"** %__t.addr, align 4
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__t.addr, align 4
  ret %"class.draco::IndexType"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE9constructIS4_JRKS4_EEEvPT_DpOT0_(%"class.std::__2::allocator.73"* %this, %"class.draco::IndexType"* %__p, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %__args) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.73"*, align 4
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  %__args.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.std::__2::allocator.73"* %this, %"class.std::__2::allocator.73"** %this.addr, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  store %"class.draco::IndexType"* %__args, %"class.draco::IndexType"** %__args.addr, align 4
  %this1 = load %"class.std::__2::allocator.73"*, %"class.std::__2::allocator.73"** %this.addr, align 4
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  %1 = bitcast %"class.draco::IndexType"* %0 to i8*
  %2 = bitcast i8* %1 to %"class.draco::IndexType"*
  %3 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZNSt3__27forwardIRKN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %3) #8
  %4 = bitcast %"class.draco::IndexType"* %2 to i8*
  %5 = bitcast %"class.draco::IndexType"* %call to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 4, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.70"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.70"*, align 4
  store %"class.std::__2::__compressed_pair.70"* %this, %"class.std::__2::__compressed_pair.70"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.70"*, %"class.std::__2::__compressed_pair.70"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.70"* %this1 to %"struct.std::__2::__compressed_pair_elem.72"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.72"* %0) #8
  ret %"class.std::__2::allocator.73"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.72"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.72"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.72"* %this, %"struct.std::__2::__compressed_pair_elem.72"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.72"*, %"struct.std::__2::__compressed_pair_elem.72"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.72"* %this1 to %"class.std::__2::allocator.73"*
  ret %"class.std::__2::allocator.73"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8max_sizeEv(%"class.std::__2::vector.68"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.68"*, align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp3 = alloca i32, align 4
  store %"class.std::__2::vector.68"* %this, %"class.std::__2::vector.68"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.68"* %this1 to %"class.std::__2::__vector_base.69"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.69"* %0) #8
  %call2 = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE8max_sizeERKS6_(%"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %call) #8
  store i32 %call2, i32* %ref.tmp, align 4
  %call4 = call i32 @_ZNSt3__214numeric_limitsIlE3maxEv() #8
  store i32 %call4, i32* %ref.tmp3, align 4
  %call5 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %1 = load i32, i32* %call5, align 4
  ret i32 %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.68"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.68"*, align 4
  store %"class.std::__2::vector.68"* %this, %"class.std::__2::vector.68"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.68"* %this1 to %"class.std::__2::__vector_base.69"*
  %call = call i32 @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::__vector_base.69"* %0) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE8max_sizeERKS6_(%"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.73"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_max_size.134", align 1
  store %"class.std::__2::allocator.73"* %__a, %"class.std::__2::allocator.73"** %__a.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_max_size.134"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.73"*, %"class.std::__2::allocator.73"** %__a.addr, align 4
  %call = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS6_(%"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %1) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.69"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.69"*, align 4
  store %"class.std::__2::__vector_base.69"* %this, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.69"*, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.70"* %__end_cap_) #8
  ret %"class.std::__2::allocator.73"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS6_(%"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.73"*, align 4
  store %"class.std::__2::allocator.73"* %__a, %"class.std::__2::allocator.73"** %__a.addr, align 4
  %1 = load %"class.std::__2::allocator.73"*, %"class.std::__2::allocator.73"** %__a.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE8max_sizeEv(%"class.std::__2::allocator.73"* %1) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE8max_sizeEv(%"class.std::__2::allocator.73"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.73"*, align 4
  store %"class.std::__2::allocator.73"* %this, %"class.std::__2::allocator.73"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.73"*, %"class.std::__2::allocator.73"** %this.addr, align 4
  ret i32 1073741823
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.70"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.70"*, align 4
  store %"class.std::__2::__compressed_pair.70"* %this, %"class.std::__2::__compressed_pair.70"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.70"*, %"class.std::__2::__compressed_pair.70"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.70"* %this1 to %"struct.std::__2::__compressed_pair_elem.72"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.72"* %0) #8
  ret %"class.std::__2::allocator.73"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.72"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.72"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.72"* %this, %"struct.std::__2::__compressed_pair_elem.72"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.72"*, %"struct.std::__2::__compressed_pair_elem.72"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.72"* %this1 to %"class.std::__2::allocator.73"*
  ret %"class.std::__2::allocator.73"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::__vector_base.69"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.69"*, align 4
  store %"class.std::__2::__vector_base.69"* %this, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.69"*, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base.69"* %this1) #8
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %this1, i32 0, i32 0
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.draco::IndexType"* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.draco::IndexType"* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base.69"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.69"*, align 4
  store %"class.std::__2::__vector_base.69"* %this, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.69"*, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.70"* %__end_cap_) #8
  ret %"class.draco::IndexType"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.70"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.70"*, align 4
  store %"class.std::__2::__compressed_pair.70"* %this, %"class.std::__2::__compressed_pair.70"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.70"*, %"class.std::__2::__compressed_pair.70"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.70"* %this1 to %"struct.std::__2::__compressed_pair_elem.71"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.71"* %0) #8
  ret %"class.draco::IndexType"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.71"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.71"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.71"* %this, %"struct.std::__2::__compressed_pair_elem.71"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.71"*, %"struct.std::__2::__compressed_pair_elem.71"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.71", %"struct.std::__2::__compressed_pair_elem.71"* %this1, i32 0, i32 0
  ret %"class.draco::IndexType"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.131"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEEC2IDnS8_EEOT_OT0_(%"class.std::__2::__compressed_pair.131"* returned %this, i8** nonnull align 4 dereferenceable(4) %__t1, %"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.131"*, align 4
  %__t1.addr = alloca i8**, align 4
  %__t2.addr = alloca %"class.std::__2::allocator.73"*, align 4
  store %"class.std::__2::__compressed_pair.131"* %this, %"class.std::__2::__compressed_pair.131"** %this.addr, align 4
  store i8** %__t1, i8*** %__t1.addr, align 4
  store %"class.std::__2::allocator.73"* %__t2, %"class.std::__2::allocator.73"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.131"*, %"class.std::__2::__compressed_pair.131"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.131"* %this1 to %"struct.std::__2::__compressed_pair_elem.71"*
  %1 = load i8**, i8*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %1) #8
  %call2 = call %"struct.std::__2::__compressed_pair_elem.71"* @_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.71"* %0, i8** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.131"* %this1 to i8*
  %3 = getelementptr inbounds i8, i8* %2, i32 4
  %4 = bitcast i8* %3 to %"struct.std::__2::__compressed_pair_elem.132"*
  %5 = load %"class.std::__2::allocator.73"*, %"class.std::__2::allocator.73"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNSt3__27forwardIRNS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEEEOT_RNS_16remove_referenceIS8_E4typeE(%"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %5) #8
  %call4 = call %"struct.std::__2::__compressed_pair_elem.132"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb0EEC2IS7_vEEOT_(%"struct.std::__2::__compressed_pair_elem.132"* %4, %"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::__compressed_pair.131"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType"* @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE8allocateERS6_m(%"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %__a, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.73"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.73"* %__a, %"class.std::__2::allocator.73"** %__a.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.73"*, %"class.std::__2::allocator.73"** %__a.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call %"class.draco::IndexType"* @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE8allocateEmPKv(%"class.std::__2::allocator.73"* %0, i32 %1, i8* null)
  ret %"class.draco::IndexType"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE7__allocEv(%"struct.std::__2::__split_buffer.130"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.130"*, align 4
  store %"struct.std::__2::__split_buffer.130"* %this, %"struct.std::__2::__split_buffer.130"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.130"*, %"struct.std::__2::__split_buffer.130"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer.130", %"struct.std::__2::__split_buffer.130"* %this1, i32 0, i32 3
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.131"* %__end_cap_) #8
  ret %"class.std::__2::allocator.73"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE9__end_capEv(%"struct.std::__2::__split_buffer.130"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.130"*, align 4
  store %"struct.std::__2::__split_buffer.130"* %this, %"struct.std::__2::__split_buffer.130"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.130"*, %"struct.std::__2::__split_buffer.130"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer.130", %"struct.std::__2::__split_buffer.130"* %this1, i32 0, i32 3
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.131"* %__end_cap_) #8
  ret %"class.draco::IndexType"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.71"* @_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.71"* returned %this, i8** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.71"*, align 4
  %__u.addr = alloca i8**, align 4
  store %"struct.std::__2::__compressed_pair_elem.71"* %this, %"struct.std::__2::__compressed_pair_elem.71"** %this.addr, align 4
  store i8** %__u, i8*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.71"*, %"struct.std::__2::__compressed_pair_elem.71"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.71", %"struct.std::__2::__compressed_pair_elem.71"* %this1, i32 0, i32 0
  %0 = load i8**, i8*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %0) #8
  store %"class.draco::IndexType"* null, %"class.draco::IndexType"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.71"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNSt3__27forwardIRNS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEEEOT_RNS_16remove_referenceIS8_E4typeE(%"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::allocator.73"*, align 4
  store %"class.std::__2::allocator.73"* %__t, %"class.std::__2::allocator.73"** %__t.addr, align 4
  %0 = load %"class.std::__2::allocator.73"*, %"class.std::__2::allocator.73"** %__t.addr, align 4
  ret %"class.std::__2::allocator.73"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.132"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb0EEC2IS7_vEEOT_(%"struct.std::__2::__compressed_pair_elem.132"* returned %this, %"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.132"*, align 4
  %__u.addr = alloca %"class.std::__2::allocator.73"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.132"* %this, %"struct.std::__2::__compressed_pair_elem.132"** %this.addr, align 4
  store %"class.std::__2::allocator.73"* %__u, %"class.std::__2::allocator.73"** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.132"*, %"struct.std::__2::__compressed_pair_elem.132"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.132", %"struct.std::__2::__compressed_pair_elem.132"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::allocator.73"*, %"class.std::__2::allocator.73"** %__u.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNSt3__27forwardIRNS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEEEOT_RNS_16remove_referenceIS8_E4typeE(%"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %0) #8
  store %"class.std::__2::allocator.73"* %call, %"class.std::__2::allocator.73"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.132"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType"* @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE8allocateEmPKv(%"class.std::__2::allocator.73"* %this, i32 %__n, i8* %0) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.73"*, align 4
  %__n.addr = alloca i32, align 4
  %.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.73"* %this, %"class.std::__2::allocator.73"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::allocator.73"*, %"class.std::__2::allocator.73"** %this.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE8max_sizeEv(%"class.std::__2::allocator.73"* %this1) #8
  %cmp = icmp ugt i32 %1, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__220__throw_length_errorEPKc(i8* getelementptr inbounds ([68 x i8], [68 x i8]* @.str, i32 0, i32 0)) #9
  unreachable

if.end:                                           ; preds = %entry
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  %call2 = call i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %mul, i32 4)
  %3 = bitcast i8* %call2 to %"class.draco::IndexType"*
  ret %"class.draco::IndexType"* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.131"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.131"*, align 4
  store %"class.std::__2::__compressed_pair.131"* %this, %"class.std::__2::__compressed_pair.131"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.131"*, %"class.std::__2::__compressed_pair.131"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.131"* %this1 to i8*
  %add.ptr = getelementptr inbounds i8, i8* %0, i32 4
  %1 = bitcast i8* %add.ptr to %"struct.std::__2::__compressed_pair_elem.132"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.132"* %1) #8
  ret %"class.std::__2::allocator.73"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.132"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.132"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.132"* %this, %"struct.std::__2::__compressed_pair_elem.132"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.132"*, %"struct.std::__2::__compressed_pair_elem.132"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.132", %"struct.std::__2::__compressed_pair_elem.132"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::allocator.73"*, %"class.std::__2::allocator.73"** %__value_, align 4
  ret %"class.std::__2::allocator.73"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.131"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.131"*, align 4
  store %"class.std::__2::__compressed_pair.131"* %this, %"class.std::__2::__compressed_pair.131"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.131"*, %"class.std::__2::__compressed_pair.131"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.131"* %this1 to %"struct.std::__2::__compressed_pair_elem.71"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.71"* %0) #8
  ret %"class.draco::IndexType"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE21_ConstructTransactionC2EPPS4_m(%"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"* returned %this, %"class.draco::IndexType"** %__p, i32 %__n) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"*, align 4
  %__p.addr = alloca %"class.draco::IndexType"**, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"* %this, %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"** %this.addr, align 4
  store %"class.draco::IndexType"** %__p, %"class.draco::IndexType"*** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"*, %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexType"**, %"class.draco::IndexType"*** %__p.addr, align 4
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %0, align 4
  store %"class.draco::IndexType"* %1, %"class.draco::IndexType"** %__pos_, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"* %this1, i32 0, i32 1
  %2 = load %"class.draco::IndexType"**, %"class.draco::IndexType"*** %__p.addr, align 4
  %3 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %2, align 4
  %4 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %3, i32 %4
  store %"class.draco::IndexType"* %add.ptr, %"class.draco::IndexType"** %__end_, align 4
  %__dest_ = getelementptr inbounds %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"* %this1, i32 0, i32 2
  %5 = load %"class.draco::IndexType"**, %"class.draco::IndexType"*** %__p.addr, align 4
  store %"class.draco::IndexType"** %5, %"class.draco::IndexType"*** %__dest_, align 4
  ret %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"*, align 4
  store %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"* %this, %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"*, %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__pos_, align 4
  %__dest_ = getelementptr inbounds %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"* %this1, i32 0, i32 2
  %1 = load %"class.draco::IndexType"**, %"class.draco::IndexType"*** %__dest_, align 4
  store %"class.draco::IndexType"* %0, %"class.draco::IndexType"** %1, align 4
  ret %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_deleteEv(%"class.std::__2::vector.68"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.68"*, align 4
  store %"class.std::__2::vector.68"* %this, %"class.std::__2::vector.68"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %this.addr, align 4
  %call = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.68"* %this1) #8
  %0 = bitcast %"class.draco::IndexType"* %call to i8*
  %call2 = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.68"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.68"* %this1) #8
  %add.ptr = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %call2, i32 %call3
  %1 = bitcast %"class.draco::IndexType"* %add.ptr to i8*
  %call4 = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.68"* %this1) #8
  %call5 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.68"* %this1) #8
  %add.ptr6 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %call4, i32 %call5
  %2 = bitcast %"class.draco::IndexType"* %add.ptr6 to i8*
  %call7 = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.68"* %this1) #8
  %call8 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.68"* %this1) #8
  %add.ptr9 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %call7, i32 %call8
  %3 = bitcast %"class.draco::IndexType"* %add.ptr9 to i8*
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_(%"class.std::__2::vector.68"* %this1, i8* %0, i8* %1, i8* %2, i8* %3) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE46__construct_backward_with_exception_guaranteesIS5_EENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS6_PT_SC_EE5valuesr31is_trivially_move_constructibleISC_EE5valueEvE4typeERS6_SD_SD_RSD_(%"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %0, %"class.draco::IndexType"* %__begin1, %"class.draco::IndexType"* %__end1, %"class.draco::IndexType"** nonnull align 4 dereferenceable(4) %__end2) #0 comdat {
entry:
  %.addr = alloca %"class.std::__2::allocator.73"*, align 4
  %__begin1.addr = alloca %"class.draco::IndexType"*, align 4
  %__end1.addr = alloca %"class.draco::IndexType"*, align 4
  %__end2.addr = alloca %"class.draco::IndexType"**, align 4
  %_Np = alloca i32, align 4
  store %"class.std::__2::allocator.73"* %0, %"class.std::__2::allocator.73"** %.addr, align 4
  store %"class.draco::IndexType"* %__begin1, %"class.draco::IndexType"** %__begin1.addr, align 4
  store %"class.draco::IndexType"* %__end1, %"class.draco::IndexType"** %__end1.addr, align 4
  store %"class.draco::IndexType"** %__end2, %"class.draco::IndexType"*** %__end2.addr, align 4
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__end1.addr, align 4
  %2 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin1.addr, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.draco::IndexType"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.draco::IndexType"* %2 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  store i32 %sub.ptr.div, i32* %_Np, align 4
  %3 = load i32, i32* %_Np, align 4
  %4 = load %"class.draco::IndexType"**, %"class.draco::IndexType"*** %__end2.addr, align 4
  %5 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %4, align 4
  %idx.neg = sub i32 0, %3
  %add.ptr = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %5, i32 %idx.neg
  store %"class.draco::IndexType"* %add.ptr, %"class.draco::IndexType"** %4, align 4
  %6 = load i32, i32* %_Np, align 4
  %cmp = icmp sgt i32 %6, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %7 = load %"class.draco::IndexType"**, %"class.draco::IndexType"*** %__end2.addr, align 4
  %8 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %7, align 4
  %9 = bitcast %"class.draco::IndexType"* %8 to i8*
  %10 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin1.addr, align 4
  %11 = bitcast %"class.draco::IndexType"* %10 to i8*
  %12 = load i32, i32* %_Np, align 4
  %mul = mul i32 %12, 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %11, i32 %mul, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__24swapIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS7_EE5valueEvE4typeERS7_SA_(%"class.draco::IndexType"** nonnull align 4 dereferenceable(4) %__x, %"class.draco::IndexType"** nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %__x.addr = alloca %"class.draco::IndexType"**, align 4
  %__y.addr = alloca %"class.draco::IndexType"**, align 4
  %__t = alloca %"class.draco::IndexType"*, align 4
  store %"class.draco::IndexType"** %__x, %"class.draco::IndexType"*** %__x.addr, align 4
  store %"class.draco::IndexType"** %__y, %"class.draco::IndexType"*** %__y.addr, align 4
  %0 = load %"class.draco::IndexType"**, %"class.draco::IndexType"*** %__x.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNSt3__24moveIRPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEONS_16remove_referenceIT_E4typeEOS8_(%"class.draco::IndexType"** nonnull align 4 dereferenceable(4) %0) #8
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %call, align 4
  store %"class.draco::IndexType"* %1, %"class.draco::IndexType"** %__t, align 4
  %2 = load %"class.draco::IndexType"**, %"class.draco::IndexType"*** %__y.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNSt3__24moveIRPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEONS_16remove_referenceIT_E4typeEOS8_(%"class.draco::IndexType"** nonnull align 4 dereferenceable(4) %2) #8
  %3 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %call1, align 4
  %4 = load %"class.draco::IndexType"**, %"class.draco::IndexType"*** %__x.addr, align 4
  store %"class.draco::IndexType"* %3, %"class.draco::IndexType"** %4, align 4
  %call2 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNSt3__24moveIRPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEONS_16remove_referenceIT_E4typeEOS8_(%"class.draco::IndexType"** nonnull align 4 dereferenceable(4) %__t) #8
  %5 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %call2, align 4
  %6 = load %"class.draco::IndexType"**, %"class.draco::IndexType"*** %__y.addr, align 4
  store %"class.draco::IndexType"* %5, %"class.draco::IndexType"** %6, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE14__annotate_newEm(%"class.std::__2::vector.68"* %this, i32 %__current_size) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.68"*, align 4
  %__current_size.addr = alloca i32, align 4
  store %"class.std::__2::vector.68"* %this, %"class.std::__2::vector.68"** %this.addr, align 4
  store i32 %__current_size, i32* %__current_size.addr, align 4
  %this1 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %this.addr, align 4
  %call = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.68"* %this1) #8
  %0 = bitcast %"class.draco::IndexType"* %call to i8*
  %call2 = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.68"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.68"* %this1) #8
  %add.ptr = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %call2, i32 %call3
  %1 = bitcast %"class.draco::IndexType"* %add.ptr to i8*
  %call4 = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.68"* %this1) #8
  %call5 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.68"* %this1) #8
  %add.ptr6 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %call4, i32 %call5
  %2 = bitcast %"class.draco::IndexType"* %add.ptr6 to i8*
  %call7 = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.68"* %this1) #8
  %3 = load i32, i32* %__current_size.addr, align 4
  %add.ptr8 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %call7, i32 %3
  %4 = bitcast %"class.draco::IndexType"* %add.ptr8 to i8*
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_(%"class.std::__2::vector.68"* %this1, i8* %0, i8* %1, i8* %2, i8* %4) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector.68"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.68"*, align 4
  store %"class.std::__2::vector.68"* %this, %"class.std::__2::vector.68"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_(%"class.std::__2::vector.68"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.68"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector.68"* %this, %"class.std::__2::vector.68"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.68"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.68"*, align 4
  store %"class.std::__2::vector.68"* %this, %"class.std::__2::vector.68"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.68"* %this1 to %"class.std::__2::__vector_base.69"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %0, i32 0, i32 0
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  %call = call %"class.draco::IndexType"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType"* %1) #8
  ret %"class.draco::IndexType"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNSt3__24moveIRPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEONS_16remove_referenceIT_E4typeEOS8_(%"class.draco::IndexType"** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.draco::IndexType"**, align 4
  store %"class.draco::IndexType"** %__t, %"class.draco::IndexType"*** %__t.addr, align 4
  %0 = load %"class.draco::IndexType"**, %"class.draco::IndexType"*** %__t.addr, align 4
  ret %"class.draco::IndexType"** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE5clearEv(%"struct.std::__2::__split_buffer.130"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.130"*, align 4
  store %"struct.std::__2::__split_buffer.130"* %this, %"struct.std::__2::__split_buffer.130"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.130"*, %"struct.std::__2::__split_buffer.130"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"struct.std::__2::__split_buffer.130", %"struct.std::__2::__split_buffer.130"* %this1, i32 0, i32 1
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  call void @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE17__destruct_at_endEPS4_(%"struct.std::__2::__split_buffer.130"* %this1, %"class.draco::IndexType"* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE10deallocateERS6_PS5_m(%"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType"* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.73"*, align 4
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.73"* %__a, %"class.std::__2::allocator.73"** %__a.addr, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.73"*, %"class.std::__2::allocator.73"** %__a.addr, align 4
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE10deallocateEPS4_m(%"class.std::__2::allocator.73"* %0, %"class.draco::IndexType"* %1, i32 %2) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE8capacityEv(%"struct.std::__2::__split_buffer.130"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.130"*, align 4
  store %"struct.std::__2::__split_buffer.130"* %this, %"struct.std::__2::__split_buffer.130"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.130"*, %"struct.std::__2::__split_buffer.130"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE9__end_capEv(%"struct.std::__2::__split_buffer.130"* %this1) #8
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %call, align 4
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer.130", %"struct.std::__2::__split_buffer.130"* %this1, i32 0, i32 0
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__first_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.draco::IndexType"* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.draco::IndexType"* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE17__destruct_at_endEPS4_(%"struct.std::__2::__split_buffer.130"* %this, %"class.draco::IndexType"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.130"*, align 4
  %__new_last.addr = alloca %"class.draco::IndexType"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant.129", align 1
  store %"struct.std::__2::__split_buffer.130"* %this, %"struct.std::__2::__split_buffer.130"** %this.addr, align 4
  store %"class.draco::IndexType"* %__new_last, %"class.draco::IndexType"** %__new_last.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.130"*, %"struct.std::__2::__split_buffer.130"** %this.addr, align 4
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__new_last.addr, align 4
  call void @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE17__destruct_at_endEPS4_NS_17integral_constantIbLb0EEE(%"struct.std::__2::__split_buffer.130"* %this1, %"class.draco::IndexType"* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE17__destruct_at_endEPS4_NS_17integral_constantIbLb0EEE(%"struct.std::__2::__split_buffer.130"* %this, %"class.draco::IndexType"* %__new_last) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant.129", align 1
  %this.addr = alloca %"struct.std::__2::__split_buffer.130"*, align 4
  %__new_last.addr = alloca %"class.draco::IndexType"*, align 4
  store %"struct.std::__2::__split_buffer.130"* %this, %"struct.std::__2::__split_buffer.130"** %this.addr, align 4
  store %"class.draco::IndexType"* %__new_last, %"class.draco::IndexType"** %__new_last.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.130"*, %"struct.std::__2::__split_buffer.130"** %this.addr, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__new_last.addr, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer.130", %"struct.std::__2::__split_buffer.130"* %this1, i32 0, i32 2
  %2 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__end_, align 4
  %cmp = icmp ne %"class.draco::IndexType"* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE7__allocEv(%"struct.std::__2::__split_buffer.130"* %this1) #8
  %__end_2 = getelementptr inbounds %"struct.std::__2::__split_buffer.130", %"struct.std::__2::__split_buffer.130"* %this1, i32 0, i32 2
  %3 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__end_2, align 4
  %incdec.ptr = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %3, i32 -1
  store %"class.draco::IndexType"* %incdec.ptr, %"class.draco::IndexType"** %__end_2, align 4
  %call3 = call %"class.draco::IndexType"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType"* %incdec.ptr) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE7destroyIS5_EEvRS6_PT_(%"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %call, %"class.draco::IndexType"* %call3)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE7destroyIS5_EEvRS6_PT_(%"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType"* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.73"*, align 4
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy.135", align 1
  store %"class.std::__2::allocator.73"* %__a, %"class.std::__2::allocator.73"** %__a.addr, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy.135"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.73"*, %"class.std::__2::allocator.73"** %__a.addr, align 4
  %2 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE9__destroyIS5_EEvNS_17integral_constantIbLb1EEERS6_PT_(%"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %1, %"class.draco::IndexType"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE9__destroyIS5_EEvNS_17integral_constantIbLb1EEERS6_PT_(%"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType"* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.73"*, align 4
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.std::__2::allocator.73"* %__a, %"class.std::__2::allocator.73"** %__a.addr, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.73"*, %"class.std::__2::allocator.73"** %__a.addr, align 4
  %2 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  call void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE7destroyEPS4_(%"class.std::__2::allocator.73"* %1, %"class.draco::IndexType"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE7destroyEPS4_(%"class.std::__2::allocator.73"* %this, %"class.draco::IndexType"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.73"*, align 4
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.std::__2::allocator.73"* %this, %"class.std::__2::allocator.73"** %this.addr, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.73"*, %"class.std::__2::allocator.73"** %this.addr, align 4
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE10deallocateEPS4_m(%"class.std::__2::allocator.73"* %this, %"class.draco::IndexType"* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.73"*, align 4
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.73"* %this, %"class.std::__2::allocator.73"** %this.addr, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.73"*, %"class.std::__2::allocator.73"** %this.addr, align 4
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  %1 = bitcast %"class.draco::IndexType"* %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE9__end_capEv(%"struct.std::__2::__split_buffer.130"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.130"*, align 4
  store %"struct.std::__2::__split_buffer.130"* %this, %"struct.std::__2::__split_buffer.130"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.130"*, %"struct.std::__2::__split_buffer.130"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer.130", %"struct.std::__2::__split_buffer.130"* %this1, i32 0, i32 3
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.131"* %__end_cap_) #8
  ret %"class.draco::IndexType"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.131"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.131"*, align 4
  store %"class.std::__2::__compressed_pair.131"* %this, %"class.std::__2::__compressed_pair.131"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.131"*, %"class.std::__2::__compressed_pair.131"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.131"* %this1 to %"struct.std::__2::__compressed_pair_elem.71"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.71"* %0) #8
  ret %"class.draco::IndexType"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE27__invalidate_iterators_pastEPS4_(%"class.std::__2::vector.68"* %this, %"class.draco::IndexType"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.68"*, align 4
  %__new_last.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.std::__2::vector.68"* %this, %"class.std::__2::vector.68"** %this.addr, align 4
  store %"class.draco::IndexType"* %__new_last, %"class.draco::IndexType"** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_(%"class.std::__2::__vector_base.69"* %this, %"class.draco::IndexType"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.69"*, align 4
  %__new_last.addr = alloca %"class.draco::IndexType"*, align 4
  %__soon_to_be_end = alloca %"class.draco::IndexType"*, align 4
  store %"class.std::__2::__vector_base.69"* %this, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  store %"class.draco::IndexType"* %__new_last, %"class.draco::IndexType"** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.69"*, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %this1, i32 0, i32 1
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__end_, align 4
  store %"class.draco::IndexType"* %0, %"class.draco::IndexType"** %__soon_to_be_end, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__new_last.addr, align 4
  %2 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__soon_to_be_end, align 4
  %cmp = icmp ne %"class.draco::IndexType"* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.69"* %this1) #8
  %3 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__soon_to_be_end, align 4
  %incdec.ptr = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %3, i32 -1
  store %"class.draco::IndexType"* %incdec.ptr, %"class.draco::IndexType"** %__soon_to_be_end, align 4
  %call2 = call %"class.draco::IndexType"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType"* %incdec.ptr) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE7destroyIS5_EEvRS6_PT_(%"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %call, %"class.draco::IndexType"* %call2)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %4 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__new_last.addr, align 4
  %__end_3 = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %this1, i32 0, i32 1
  store %"class.draco::IndexType"* %4, %"class.draco::IndexType"** %__end_3, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_shrinkEm(%"class.std::__2::vector.68"* %this, i32 %__old_size) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.68"*, align 4
  %__old_size.addr = alloca i32, align 4
  store %"class.std::__2::vector.68"* %this, %"class.std::__2::vector.68"** %this.addr, align 4
  store i32 %__old_size, i32* %__old_size.addr, align 4
  %this1 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %this.addr, align 4
  %call = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.68"* %this1) #8
  %0 = bitcast %"class.draco::IndexType"* %call to i8*
  %call2 = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.68"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.68"* %this1) #8
  %add.ptr = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %call2, i32 %call3
  %1 = bitcast %"class.draco::IndexType"* %add.ptr to i8*
  %call4 = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.68"* %this1) #8
  %2 = load i32, i32* %__old_size.addr, align 4
  %add.ptr5 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %call4, i32 %2
  %3 = bitcast %"class.draco::IndexType"* %add.ptr5 to i8*
  %call6 = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.68"* %this1) #8
  %call7 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.68"* %this1) #8
  %add.ptr8 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %call6, i32 %call7
  %4 = bitcast %"class.draco::IndexType"* %add.ptr8 to i8*
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_(%"class.std::__2::vector.68"* %this1, i8* %0, i8* %1, i8* %3, i8* %4) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEEEixERKS3_(%"class.draco::IndexTypeVector"* %this, %"class.draco::IndexType.104"* nonnull align 4 dereferenceable(4) %index) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector"*, align 4
  %index.addr = alloca %"class.draco::IndexType.104"*, align 4
  store %"class.draco::IndexTypeVector"* %this, %"class.draco::IndexTypeVector"** %this.addr, align 4
  store %"class.draco::IndexType.104"* %index, %"class.draco::IndexType.104"** %index.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector"*, %"class.draco::IndexTypeVector"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector", %"class.draco::IndexTypeVector"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexType.104"*, %"class.draco::IndexType.104"** %index.addr, align 4
  %call = call i32 @_ZNK5draco9IndexTypeIjNS_20PointIndex_tag_type_EE5valueEv(%"class.draco::IndexType.104"* %0)
  %call2 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEEixEm(%"class.std::__2::vector.68"* %vector_, i32 %call) #8
  ret %"class.draco::IndexType"* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEEixEm(%"class.std::__2::vector.68"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.68"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector.68"* %this, %"class.std::__2::vector.68"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.68"* %this1 to %"class.std::__2::__vector_base.69"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %0, i32 0, i32 0
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  %2 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %1, i32 %2
  ret %"class.draco::IndexType"* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector.68"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEED2Ev(%"class.std::__2::vector.68"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.68"*, align 4
  store %"class.std::__2::vector.68"* %this, %"class.std::__2::vector.68"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %this.addr, align 4
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_deleteEv(%"class.std::__2::vector.68"* %this1) #8
  %0 = bitcast %"class.std::__2::vector.68"* %this1 to %"class.std::__2::__vector_base.69"*
  %call = call %"class.std::__2::__vector_base.69"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEED2Ev(%"class.std::__2::__vector_base.69"* %0) #8
  ret %"class.std::__2::vector.68"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base.69"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEED2Ev(%"class.std::__2::__vector_base.69"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__vector_base.69"*, align 4
  %this.addr = alloca %"class.std::__2::__vector_base.69"*, align 4
  store %"class.std::__2::__vector_base.69"* %this, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.69"*, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  store %"class.std::__2::__vector_base.69"* %this1, %"class.std::__2::__vector_base.69"** %retval, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  %cmp = icmp ne %"class.draco::IndexType"* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5clearEv(%"class.std::__2::__vector_base.69"* %this1) #8
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.69"* %this1) #8
  %__begin_2 = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %this1, i32 0, i32 0
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_2, align 4
  %call3 = call i32 @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::__vector_base.69"* %this1) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE10deallocateERS6_PS5_m(%"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %call, %"class.draco::IndexType"* %1, i32 %call3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"class.std::__2::__vector_base.69"*, %"class.std::__2::__vector_base.69"** %retval, align 4
  ret %"class.std::__2::__vector_base.69"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5clearEv(%"class.std::__2::__vector_base.69"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.69"*, align 4
  store %"class.std::__2::__vector_base.69"* %this, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.69"*, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  call void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_(%"class.std::__2::__vector_base.69"* %this1, %"class.draco::IndexType"* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector.56"* @_ZNSt3__26vectorIhNS_9allocatorIhEEED2Ev(%"class.std::__2::vector.56"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.56"*, align 4
  store %"class.std::__2::vector.56"* %this, %"class.std::__2::vector.56"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.56"*, %"class.std::__2::vector.56"** %this.addr, align 4
  call void @_ZNKSt3__26vectorIhNS_9allocatorIhEEE17__annotate_deleteEv(%"class.std::__2::vector.56"* %this1) #8
  %0 = bitcast %"class.std::__2::vector.56"* %this1 to %"class.std::__2::__vector_base.57"*
  %call = call %"class.std::__2::__vector_base.57"* @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEED2Ev(%"class.std::__2::__vector_base.57"* %0) #8
  ret %"class.std::__2::vector.56"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIhNS_9allocatorIhEEE17__annotate_deleteEv(%"class.std::__2::vector.56"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.56"*, align 4
  store %"class.std::__2::vector.56"* %this, %"class.std::__2::vector.56"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.56"*, %"class.std::__2::vector.56"** %this.addr, align 4
  %call = call i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector.56"* %this1) #8
  %call2 = call i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector.56"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::vector.56"* %this1) #8
  %add.ptr = getelementptr inbounds i8, i8* %call2, i32 %call3
  %call4 = call i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector.56"* %this1) #8
  %call5 = call i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4sizeEv(%"class.std::__2::vector.56"* %this1) #8
  %add.ptr6 = getelementptr inbounds i8, i8* %call4, i32 %call5
  %call7 = call i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector.56"* %this1) #8
  %call8 = call i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::vector.56"* %this1) #8
  %add.ptr9 = getelementptr inbounds i8, i8* %call7, i32 %call8
  call void @_ZNKSt3__26vectorIhNS_9allocatorIhEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector.56"* %this1, i8* %call, i8* %add.ptr, i8* %add.ptr6, i8* %add.ptr9) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base.57"* @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEED2Ev(%"class.std::__2::__vector_base.57"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__vector_base.57"*, align 4
  %this.addr = alloca %"class.std::__2::__vector_base.57"*, align 4
  store %"class.std::__2::__vector_base.57"* %this, %"class.std::__2::__vector_base.57"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.57"*, %"class.std::__2::__vector_base.57"** %this.addr, align 4
  store %"class.std::__2::__vector_base.57"* %this1, %"class.std::__2::__vector_base.57"** %retval, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.57", %"class.std::__2::__vector_base.57"* %this1, i32 0, i32 0
  %0 = load i8*, i8** %__begin_, align 4
  %cmp = icmp ne i8* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE5clearEv(%"class.std::__2::__vector_base.57"* %this1) #8
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.61"* @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE7__allocEv(%"class.std::__2::__vector_base.57"* %this1) #8
  %__begin_2 = getelementptr inbounds %"class.std::__2::__vector_base.57", %"class.std::__2::__vector_base.57"* %this1, i32 0, i32 0
  %1 = load i8*, i8** %__begin_2, align 4
  %call3 = call i32 @_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::__vector_base.57"* %this1) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE10deallocateERS2_Phm(%"class.std::__2::allocator.61"* nonnull align 1 dereferenceable(1) %call, i8* %1, i32 %call3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"class.std::__2::__vector_base.57"*, %"class.std::__2::__vector_base.57"** %retval, align 4
  ret %"class.std::__2::__vector_base.57"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIhNS_9allocatorIhEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector.56"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.56"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector.56"* %this, %"class.std::__2::vector.56"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector.56"*, %"class.std::__2::vector.56"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector.56"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.56"*, align 4
  store %"class.std::__2::vector.56"* %this, %"class.std::__2::vector.56"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.56"*, %"class.std::__2::vector.56"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.56"* %this1 to %"class.std::__2::__vector_base.57"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.57", %"class.std::__2::__vector_base.57"* %0, i32 0, i32 0
  %1 = load i8*, i8** %__begin_, align 4
  %call = call i8* @_ZNSt3__212__to_addressIhEEPT_S2_(i8* %1) #8
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::vector.56"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.56"*, align 4
  store %"class.std::__2::vector.56"* %this, %"class.std::__2::vector.56"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.56"*, %"class.std::__2::vector.56"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.56"* %this1 to %"class.std::__2::__vector_base.57"*
  %call = call i32 @_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::__vector_base.57"* %0) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4sizeEv(%"class.std::__2::vector.56"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.56"*, align 4
  store %"class.std::__2::vector.56"* %this, %"class.std::__2::vector.56"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.56"*, %"class.std::__2::vector.56"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.56"* %this1 to %"class.std::__2::__vector_base.57"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.57", %"class.std::__2::__vector_base.57"* %0, i32 0, i32 1
  %1 = load i8*, i8** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector.56"* %this1 to %"class.std::__2::__vector_base.57"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.57", %"class.std::__2::__vector_base.57"* %2, i32 0, i32 0
  %3 = load i8*, i8** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint i8* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  ret i32 %sub.ptr.sub
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__212__to_addressIhEEPT_S2_(i8* %__p) #0 comdat {
entry:
  %__p.addr = alloca i8*, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %0 = load i8*, i8** %__p.addr, align 4
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::__vector_base.57"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.57"*, align 4
  store %"class.std::__2::__vector_base.57"* %this, %"class.std::__2::__vector_base.57"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.57"*, %"class.std::__2::__vector_base.57"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE9__end_capEv(%"class.std::__2::__vector_base.57"* %this1) #8
  %0 = load i8*, i8** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.57", %"class.std::__2::__vector_base.57"* %this1, i32 0, i32 0
  %1 = load i8*, i8** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint i8* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  ret i32 %sub.ptr.sub
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE9__end_capEv(%"class.std::__2::__vector_base.57"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.57"*, align 4
  store %"class.std::__2::__vector_base.57"* %this, %"class.std::__2::__vector_base.57"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.57"*, %"class.std::__2::__vector_base.57"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.57", %"class.std::__2::__vector_base.57"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__217__compressed_pairIPhNS_9allocatorIhEEE5firstEv(%"class.std::__2::__compressed_pair.58"* %__end_cap_) #8
  ret i8** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__217__compressed_pairIPhNS_9allocatorIhEEE5firstEv(%"class.std::__2::__compressed_pair.58"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.58"*, align 4
  store %"class.std::__2::__compressed_pair.58"* %this, %"class.std::__2::__compressed_pair.58"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.58"*, %"class.std::__2::__compressed_pair.58"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.58"* %this1 to %"struct.std::__2::__compressed_pair_elem.59"*
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__222__compressed_pair_elemIPhLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.59"* %0) #8
  ret i8** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__222__compressed_pair_elemIPhLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.59"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.59"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.59"* %this, %"struct.std::__2::__compressed_pair_elem.59"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.59"*, %"struct.std::__2::__compressed_pair_elem.59"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.59", %"struct.std::__2::__compressed_pair_elem.59"* %this1, i32 0, i32 0
  ret i8** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE5clearEv(%"class.std::__2::__vector_base.57"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.57"*, align 4
  store %"class.std::__2::__vector_base.57"* %this, %"class.std::__2::__vector_base.57"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.57"*, %"class.std::__2::__vector_base.57"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.57", %"class.std::__2::__vector_base.57"* %this1, i32 0, i32 0
  %0 = load i8*, i8** %__begin_, align 4
  call void @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE17__destruct_at_endEPh(%"class.std::__2::__vector_base.57"* %this1, i8* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE10deallocateERS2_Phm(%"class.std::__2::allocator.61"* nonnull align 1 dereferenceable(1) %__a, i8* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.61"*, align 4
  %__p.addr = alloca i8*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.61"* %__a, %"class.std::__2::allocator.61"** %__a.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.61"*, %"class.std::__2::allocator.61"** %__a.addr, align 4
  %1 = load i8*, i8** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorIhE10deallocateEPhm(%"class.std::__2::allocator.61"* %0, i8* %1, i32 %2) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.61"* @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE7__allocEv(%"class.std::__2::__vector_base.57"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.57"*, align 4
  store %"class.std::__2::__vector_base.57"* %this, %"class.std::__2::__vector_base.57"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.57"*, %"class.std::__2::__vector_base.57"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.57", %"class.std::__2::__vector_base.57"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.61"* @_ZNSt3__217__compressed_pairIPhNS_9allocatorIhEEE6secondEv(%"class.std::__2::__compressed_pair.58"* %__end_cap_) #8
  ret %"class.std::__2::allocator.61"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE17__destruct_at_endEPh(%"class.std::__2::__vector_base.57"* %this, i8* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.57"*, align 4
  %__new_last.addr = alloca i8*, align 4
  %__soon_to_be_end = alloca i8*, align 4
  store %"class.std::__2::__vector_base.57"* %this, %"class.std::__2::__vector_base.57"** %this.addr, align 4
  store i8* %__new_last, i8** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.57"*, %"class.std::__2::__vector_base.57"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.57", %"class.std::__2::__vector_base.57"* %this1, i32 0, i32 1
  %0 = load i8*, i8** %__end_, align 4
  store i8* %0, i8** %__soon_to_be_end, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load i8*, i8** %__new_last.addr, align 4
  %2 = load i8*, i8** %__soon_to_be_end, align 4
  %cmp = icmp ne i8* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.61"* @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE7__allocEv(%"class.std::__2::__vector_base.57"* %this1) #8
  %3 = load i8*, i8** %__soon_to_be_end, align 4
  %incdec.ptr = getelementptr inbounds i8, i8* %3, i32 -1
  store i8* %incdec.ptr, i8** %__soon_to_be_end, align 4
  %call2 = call i8* @_ZNSt3__212__to_addressIhEEPT_S2_(i8* %incdec.ptr) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE7destroyIhEEvRS2_PT_(%"class.std::__2::allocator.61"* nonnull align 1 dereferenceable(1) %call, i8* %call2)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %4 = load i8*, i8** %__new_last.addr, align 4
  %__end_3 = getelementptr inbounds %"class.std::__2::__vector_base.57", %"class.std::__2::__vector_base.57"* %this1, i32 0, i32 1
  store i8* %4, i8** %__end_3, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE7destroyIhEEvRS2_PT_(%"class.std::__2::allocator.61"* nonnull align 1 dereferenceable(1) %__a, i8* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.61"*, align 4
  %__p.addr = alloca i8*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy.136", align 1
  store %"class.std::__2::allocator.61"* %__a, %"class.std::__2::allocator.61"** %__a.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy.136"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.61"*, %"class.std::__2::allocator.61"** %__a.addr, align 4
  %2 = load i8*, i8** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE9__destroyIhEEvNS_17integral_constantIbLb1EEERS2_PT_(%"class.std::__2::allocator.61"* nonnull align 1 dereferenceable(1) %1, i8* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE9__destroyIhEEvNS_17integral_constantIbLb1EEERS2_PT_(%"class.std::__2::allocator.61"* nonnull align 1 dereferenceable(1) %__a, i8* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.61"*, align 4
  %__p.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.61"* %__a, %"class.std::__2::allocator.61"** %__a.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.61"*, %"class.std::__2::allocator.61"** %__a.addr, align 4
  %2 = load i8*, i8** %__p.addr, align 4
  call void @_ZNSt3__29allocatorIhE7destroyEPh(%"class.std::__2::allocator.61"* %1, i8* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIhE7destroyEPh(%"class.std::__2::allocator.61"* %this, i8* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.61"*, align 4
  %__p.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.61"* %this, %"class.std::__2::allocator.61"** %this.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.61"*, %"class.std::__2::allocator.61"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIhE10deallocateEPhm(%"class.std::__2::allocator.61"* %this, i8* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.61"*, align 4
  %__p.addr = alloca i8*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.61"* %this, %"class.std::__2::allocator.61"** %this.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.61"*, %"class.std::__2::allocator.61"** %this.addr, align 4
  %0 = load i8*, i8** %__p.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %1, 1
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %0, i32 %mul, i32 1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.61"* @_ZNSt3__217__compressed_pairIPhNS_9allocatorIhEEE6secondEv(%"class.std::__2::__compressed_pair.58"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.58"*, align 4
  store %"class.std::__2::__compressed_pair.58"* %this, %"class.std::__2::__compressed_pair.58"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.58"*, %"class.std::__2::__compressed_pair.58"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.58"* %this1 to %"struct.std::__2::__compressed_pair_elem.60"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.61"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIhEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.60"* %0) #8
  ret %"class.std::__2::allocator.61"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.61"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIhEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.60"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.60"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.60"* %this, %"struct.std::__2::__compressed_pair_elem.60"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.60"*, %"struct.std::__2::__compressed_pair_elem.60"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.60"* %this1 to %"class.std::__2::allocator.61"*
  ret %"class.std::__2::allocator.61"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector.118"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEED2Ev(%"class.std::__2::vector.118"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.118"*, align 4
  store %"class.std::__2::vector.118"* %this, %"class.std::__2::vector.118"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.118"*, %"class.std::__2::vector.118"** %this.addr, align 4
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_deleteEv(%"class.std::__2::vector.118"* %this1) #8
  %0 = bitcast %"class.std::__2::vector.118"* %this1 to %"class.std::__2::__vector_base.119"*
  %call = call %"class.std::__2::__vector_base.119"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEED2Ev(%"class.std::__2::__vector_base.119"* %0) #8
  ret %"class.std::__2::vector.118"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_deleteEv(%"class.std::__2::vector.118"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.118"*, align 4
  store %"class.std::__2::vector.118"* %this, %"class.std::__2::vector.118"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.118"*, %"class.std::__2::vector.118"** %this.addr, align 4
  %call = call %"class.draco::IndexType.104"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.118"* %this1) #8
  %0 = bitcast %"class.draco::IndexType.104"* %call to i8*
  %call2 = call %"class.draco::IndexType.104"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.118"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.118"* %this1) #8
  %add.ptr = getelementptr inbounds %"class.draco::IndexType.104", %"class.draco::IndexType.104"* %call2, i32 %call3
  %1 = bitcast %"class.draco::IndexType.104"* %add.ptr to i8*
  %call4 = call %"class.draco::IndexType.104"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.118"* %this1) #8
  %call5 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.118"* %this1) #8
  %add.ptr6 = getelementptr inbounds %"class.draco::IndexType.104", %"class.draco::IndexType.104"* %call4, i32 %call5
  %2 = bitcast %"class.draco::IndexType.104"* %add.ptr6 to i8*
  %call7 = call %"class.draco::IndexType.104"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.118"* %this1) #8
  %call8 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.118"* %this1) #8
  %add.ptr9 = getelementptr inbounds %"class.draco::IndexType.104", %"class.draco::IndexType.104"* %call7, i32 %call8
  %3 = bitcast %"class.draco::IndexType.104"* %add.ptr9 to i8*
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_(%"class.std::__2::vector.118"* %this1, i8* %0, i8* %1, i8* %2, i8* %3) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base.119"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEED2Ev(%"class.std::__2::__vector_base.119"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__vector_base.119"*, align 4
  %this.addr = alloca %"class.std::__2::__vector_base.119"*, align 4
  store %"class.std::__2::__vector_base.119"* %this, %"class.std::__2::__vector_base.119"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.119"*, %"class.std::__2::__vector_base.119"** %this.addr, align 4
  store %"class.std::__2::__vector_base.119"* %this1, %"class.std::__2::__vector_base.119"** %retval, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.119", %"class.std::__2::__vector_base.119"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexType.104"*, %"class.draco::IndexType.104"** %__begin_, align 4
  %cmp = icmp ne %"class.draco::IndexType.104"* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE5clearEv(%"class.std::__2::__vector_base.119"* %this1) #8
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.123"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.119"* %this1) #8
  %__begin_2 = getelementptr inbounds %"class.std::__2::__vector_base.119", %"class.std::__2::__vector_base.119"* %this1, i32 0, i32 0
  %1 = load %"class.draco::IndexType.104"*, %"class.draco::IndexType.104"** %__begin_2, align 4
  %call3 = call i32 @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::__vector_base.119"* %this1) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE10deallocateERS6_PS5_m(%"class.std::__2::allocator.123"* nonnull align 1 dereferenceable(1) %call, %"class.draco::IndexType.104"* %1, i32 %call3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"class.std::__2::__vector_base.119"*, %"class.std::__2::__vector_base.119"** %retval, align 4
  ret %"class.std::__2::__vector_base.119"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_(%"class.std::__2::vector.118"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.118"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector.118"* %this, %"class.std::__2::vector.118"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector.118"*, %"class.std::__2::vector.118"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType.104"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.118"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.118"*, align 4
  store %"class.std::__2::vector.118"* %this, %"class.std::__2::vector.118"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.118"*, %"class.std::__2::vector.118"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.118"* %this1 to %"class.std::__2::__vector_base.119"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.119", %"class.std::__2::__vector_base.119"* %0, i32 0, i32 0
  %1 = load %"class.draco::IndexType.104"*, %"class.draco::IndexType.104"** %__begin_, align 4
  %call = call %"class.draco::IndexType.104"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType.104"* %1) #8
  ret %"class.draco::IndexType.104"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.118"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.118"*, align 4
  store %"class.std::__2::vector.118"* %this, %"class.std::__2::vector.118"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.118"*, %"class.std::__2::vector.118"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.118"* %this1 to %"class.std::__2::__vector_base.119"*
  %call = call i32 @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::__vector_base.119"* %0) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.118"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.118"*, align 4
  store %"class.std::__2::vector.118"* %this, %"class.std::__2::vector.118"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.118"*, %"class.std::__2::vector.118"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.118"* %this1 to %"class.std::__2::__vector_base.119"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.119", %"class.std::__2::__vector_base.119"* %0, i32 0, i32 1
  %1 = load %"class.draco::IndexType.104"*, %"class.draco::IndexType.104"** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector.118"* %this1 to %"class.std::__2::__vector_base.119"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.119", %"class.std::__2::__vector_base.119"* %2, i32 0, i32 0
  %3 = load %"class.draco::IndexType.104"*, %"class.draco::IndexType.104"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.draco::IndexType.104"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.draco::IndexType.104"* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType.104"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType.104"* %__p) #0 comdat {
entry:
  %__p.addr = alloca %"class.draco::IndexType.104"*, align 4
  store %"class.draco::IndexType.104"* %__p, %"class.draco::IndexType.104"** %__p.addr, align 4
  %0 = load %"class.draco::IndexType.104"*, %"class.draco::IndexType.104"** %__p.addr, align 4
  ret %"class.draco::IndexType.104"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::__vector_base.119"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.119"*, align 4
  store %"class.std::__2::__vector_base.119"* %this, %"class.std::__2::__vector_base.119"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.119"*, %"class.std::__2::__vector_base.119"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.104"** @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base.119"* %this1) #8
  %0 = load %"class.draco::IndexType.104"*, %"class.draco::IndexType.104"** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.119", %"class.std::__2::__vector_base.119"* %this1, i32 0, i32 0
  %1 = load %"class.draco::IndexType.104"*, %"class.draco::IndexType.104"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.draco::IndexType.104"* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.draco::IndexType.104"* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.104"** @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base.119"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.119"*, align 4
  store %"class.std::__2::__vector_base.119"* %this, %"class.std::__2::__vector_base.119"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.119"*, %"class.std::__2::__vector_base.119"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.119", %"class.std::__2::__vector_base.119"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.104"** @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.120"* %__end_cap_) #8
  ret %"class.draco::IndexType.104"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.104"** @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.120"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.120"*, align 4
  store %"class.std::__2::__compressed_pair.120"* %this, %"class.std::__2::__compressed_pair.120"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.120"*, %"class.std::__2::__compressed_pair.120"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.120"* %this1 to %"struct.std::__2::__compressed_pair_elem.121"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.104"** @_ZNKSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.121"* %0) #8
  ret %"class.draco::IndexType.104"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.104"** @_ZNKSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.121"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.121"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.121"* %this, %"struct.std::__2::__compressed_pair_elem.121"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.121"*, %"struct.std::__2::__compressed_pair_elem.121"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.121", %"struct.std::__2::__compressed_pair_elem.121"* %this1, i32 0, i32 0
  ret %"class.draco::IndexType.104"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE5clearEv(%"class.std::__2::__vector_base.119"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.119"*, align 4
  store %"class.std::__2::__vector_base.119"* %this, %"class.std::__2::__vector_base.119"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.119"*, %"class.std::__2::__vector_base.119"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.119", %"class.std::__2::__vector_base.119"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexType.104"*, %"class.draco::IndexType.104"** %__begin_, align 4
  call void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_(%"class.std::__2::__vector_base.119"* %this1, %"class.draco::IndexType.104"* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE10deallocateERS6_PS5_m(%"class.std::__2::allocator.123"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType.104"* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.123"*, align 4
  %__p.addr = alloca %"class.draco::IndexType.104"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.123"* %__a, %"class.std::__2::allocator.123"** %__a.addr, align 4
  store %"class.draco::IndexType.104"* %__p, %"class.draco::IndexType.104"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.123"*, %"class.std::__2::allocator.123"** %__a.addr, align 4
  %1 = load %"class.draco::IndexType.104"*, %"class.draco::IndexType.104"** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEE10deallocateEPS4_m(%"class.std::__2::allocator.123"* %0, %"class.draco::IndexType.104"* %1, i32 %2) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.123"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.119"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.119"*, align 4
  store %"class.std::__2::__vector_base.119"* %this, %"class.std::__2::__vector_base.119"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.119"*, %"class.std::__2::__vector_base.119"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.119", %"class.std::__2::__vector_base.119"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.123"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.120"* %__end_cap_) #8
  ret %"class.std::__2::allocator.123"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_(%"class.std::__2::__vector_base.119"* %this, %"class.draco::IndexType.104"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.119"*, align 4
  %__new_last.addr = alloca %"class.draco::IndexType.104"*, align 4
  %__soon_to_be_end = alloca %"class.draco::IndexType.104"*, align 4
  store %"class.std::__2::__vector_base.119"* %this, %"class.std::__2::__vector_base.119"** %this.addr, align 4
  store %"class.draco::IndexType.104"* %__new_last, %"class.draco::IndexType.104"** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.119"*, %"class.std::__2::__vector_base.119"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.119", %"class.std::__2::__vector_base.119"* %this1, i32 0, i32 1
  %0 = load %"class.draco::IndexType.104"*, %"class.draco::IndexType.104"** %__end_, align 4
  store %"class.draco::IndexType.104"* %0, %"class.draco::IndexType.104"** %__soon_to_be_end, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load %"class.draco::IndexType.104"*, %"class.draco::IndexType.104"** %__new_last.addr, align 4
  %2 = load %"class.draco::IndexType.104"*, %"class.draco::IndexType.104"** %__soon_to_be_end, align 4
  %cmp = icmp ne %"class.draco::IndexType.104"* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.123"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.119"* %this1) #8
  %3 = load %"class.draco::IndexType.104"*, %"class.draco::IndexType.104"** %__soon_to_be_end, align 4
  %incdec.ptr = getelementptr inbounds %"class.draco::IndexType.104", %"class.draco::IndexType.104"* %3, i32 -1
  store %"class.draco::IndexType.104"* %incdec.ptr, %"class.draco::IndexType.104"** %__soon_to_be_end, align 4
  %call2 = call %"class.draco::IndexType.104"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType.104"* %incdec.ptr) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE7destroyIS5_EEvRS6_PT_(%"class.std::__2::allocator.123"* nonnull align 1 dereferenceable(1) %call, %"class.draco::IndexType.104"* %call2)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %4 = load %"class.draco::IndexType.104"*, %"class.draco::IndexType.104"** %__new_last.addr, align 4
  %__end_3 = getelementptr inbounds %"class.std::__2::__vector_base.119", %"class.std::__2::__vector_base.119"* %this1, i32 0, i32 1
  store %"class.draco::IndexType.104"* %4, %"class.draco::IndexType.104"** %__end_3, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE7destroyIS5_EEvRS6_PT_(%"class.std::__2::allocator.123"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType.104"* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.123"*, align 4
  %__p.addr = alloca %"class.draco::IndexType.104"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy.137", align 1
  store %"class.std::__2::allocator.123"* %__a, %"class.std::__2::allocator.123"** %__a.addr, align 4
  store %"class.draco::IndexType.104"* %__p, %"class.draco::IndexType.104"** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy.137"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.123"*, %"class.std::__2::allocator.123"** %__a.addr, align 4
  %2 = load %"class.draco::IndexType.104"*, %"class.draco::IndexType.104"** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE9__destroyIS5_EEvNS_17integral_constantIbLb1EEERS6_PT_(%"class.std::__2::allocator.123"* nonnull align 1 dereferenceable(1) %1, %"class.draco::IndexType.104"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE9__destroyIS5_EEvNS_17integral_constantIbLb1EEERS6_PT_(%"class.std::__2::allocator.123"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType.104"* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.123"*, align 4
  %__p.addr = alloca %"class.draco::IndexType.104"*, align 4
  store %"class.std::__2::allocator.123"* %__a, %"class.std::__2::allocator.123"** %__a.addr, align 4
  store %"class.draco::IndexType.104"* %__p, %"class.draco::IndexType.104"** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.123"*, %"class.std::__2::allocator.123"** %__a.addr, align 4
  %2 = load %"class.draco::IndexType.104"*, %"class.draco::IndexType.104"** %__p.addr, align 4
  call void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEE7destroyEPS4_(%"class.std::__2::allocator.123"* %1, %"class.draco::IndexType.104"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEE7destroyEPS4_(%"class.std::__2::allocator.123"* %this, %"class.draco::IndexType.104"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.123"*, align 4
  %__p.addr = alloca %"class.draco::IndexType.104"*, align 4
  store %"class.std::__2::allocator.123"* %this, %"class.std::__2::allocator.123"** %this.addr, align 4
  store %"class.draco::IndexType.104"* %__p, %"class.draco::IndexType.104"** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.123"*, %"class.std::__2::allocator.123"** %this.addr, align 4
  %0 = load %"class.draco::IndexType.104"*, %"class.draco::IndexType.104"** %__p.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEE10deallocateEPS4_m(%"class.std::__2::allocator.123"* %this, %"class.draco::IndexType.104"* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.123"*, align 4
  %__p.addr = alloca %"class.draco::IndexType.104"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.123"* %this, %"class.std::__2::allocator.123"** %this.addr, align 4
  store %"class.draco::IndexType.104"* %__p, %"class.draco::IndexType.104"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.123"*, %"class.std::__2::allocator.123"** %this.addr, align 4
  %0 = load %"class.draco::IndexType.104"*, %"class.draco::IndexType.104"** %__p.addr, align 4
  %1 = bitcast %"class.draco::IndexType.104"* %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.123"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.120"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.120"*, align 4
  store %"class.std::__2::__compressed_pair.120"* %this, %"class.std::__2::__compressed_pair.120"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.120"*, %"class.std::__2::__compressed_pair.120"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.120"* %this1 to %"struct.std::__2::__compressed_pair_elem.122"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.123"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.122"* %0) #8
  ret %"class.std::__2::allocator.123"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.123"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.122"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.122"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.122"* %this, %"struct.std::__2::__compressed_pair_elem.122"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.122"*, %"struct.std::__2::__compressed_pair_elem.122"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.122"* %this1 to %"class.std::__2::allocator.123"*
  ret %"class.std::__2::allocator.123"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base_common"* @_ZNSt3__220__vector_base_commonILb1EEC2Ev(%"class.std::__2::__vector_base_common"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base_common"*, align 4
  store %"class.std::__2::__vector_base_common"* %this, %"class.std::__2::__vector_base_common"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base_common"*, %"class.std::__2::__vector_base_common"** %this.addr, align 4
  ret %"class.std::__2::__vector_base_common"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.111"* @_ZNSt3__217__compressed_pairImNS_9allocatorImEEEC2IiNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.111"* returned %this, i32* nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.111"*, align 4
  %__t1.addr = alloca i32*, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.111"* %this, %"class.std::__2::__compressed_pair.111"** %this.addr, align 4
  store i32* %__t1, i32** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.111"*, %"class.std::__2::__compressed_pair.111"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.111"* %this1 to %"struct.std::__2::__compressed_pair_elem.5"*
  %1 = load i32*, i32** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__27forwardIiEEOT_RNS_16remove_referenceIS1_E4typeE(i32* nonnull align 4 dereferenceable(4) %1) #8
  %call2 = call %"struct.std::__2::__compressed_pair_elem.5"* @_ZNSt3__222__compressed_pair_elemImLi0ELb0EEC2IivEEOT_(%"struct.std::__2::__compressed_pair_elem.5"* %0, i32* nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.111"* %this1 to %"struct.std::__2::__compressed_pair_elem.112"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #8
  %call4 = call %"struct.std::__2::__compressed_pair_elem.112"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorImEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.112"* %2)
  ret %"class.std::__2::__compressed_pair.111"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__27forwardIiEEOT_RNS_16remove_referenceIS1_E4typeE(i32* nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca i32*, align 4
  store i32* %__t, i32** %__t.addr, align 4
  %0 = load i32*, i32** %__t.addr, align 4
  ret i32* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.5"* @_ZNSt3__222__compressed_pair_elemImLi0ELb0EEC2IivEEOT_(%"struct.std::__2::__compressed_pair_elem.5"* returned %this, i32* nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.5"*, align 4
  %__u.addr = alloca i32*, align 4
  store %"struct.std::__2::__compressed_pair_elem.5"* %this, %"struct.std::__2::__compressed_pair_elem.5"** %this.addr, align 4
  store i32* %__u, i32** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.5"*, %"struct.std::__2::__compressed_pair_elem.5"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.5", %"struct.std::__2::__compressed_pair_elem.5"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__27forwardIiEEOT_RNS_16remove_referenceIS1_E4typeE(i32* nonnull align 4 dereferenceable(4) %0) #8
  %1 = load i32, i32* %call, align 4
  store i32 %1, i32* %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.5"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  store %"struct.std::__2::__default_init_tag"* %__t, %"struct.std::__2::__default_init_tag"** %__t.addr, align 4
  %0 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t.addr, align 4
  ret %"struct.std::__2::__default_init_tag"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.112"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorImEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.112"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.112"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.112"* %this, %"struct.std::__2::__compressed_pair_elem.112"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.112"*, %"struct.std::__2::__compressed_pair_elem.112"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.112"* %this1 to %"class.std::__2::allocator.113"*
  %call = call %"class.std::__2::allocator.113"* @_ZNSt3__29allocatorImEC2Ev(%"class.std::__2::allocator.113"* %1) #8
  ret %"struct.std::__2::__compressed_pair_elem.112"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::allocator.113"* @_ZNSt3__29allocatorImEC2Ev(%"class.std::__2::allocator.113"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.113"*, align 4
  store %"class.std::__2::allocator.113"* %this, %"class.std::__2::allocator.113"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.113"*, %"class.std::__2::allocator.113"** %this.addr, align 4
  ret %"class.std::__2::allocator.113"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorImEEE10deallocateERS2_Pmm(%"class.std::__2::allocator.113"* nonnull align 1 dereferenceable(1) %__a, i32* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.113"*, align 4
  %__p.addr = alloca i32*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.113"* %__a, %"class.std::__2::allocator.113"** %__a.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.113"*, %"class.std::__2::allocator.113"** %__a.addr, align 4
  %1 = load i32*, i32** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorImE10deallocateEPmm(%"class.std::__2::allocator.113"* %0, i32* %1, i32 %2) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.113"* @_ZNSt3__26vectorIbNS_9allocatorIbEEE7__allocEv(%"class.std::__2::vector.110"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.110"*, align 4
  store %"class.std::__2::vector.110"* %this, %"class.std::__2::vector.110"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.110"*, %"class.std::__2::vector.110"** %this.addr, align 4
  %__cap_alloc_ = getelementptr inbounds %"class.std::__2::vector.110", %"class.std::__2::vector.110"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.113"* @_ZNSt3__217__compressed_pairImNS_9allocatorImEEE6secondEv(%"class.std::__2::__compressed_pair.111"* %__cap_alloc_) #8
  ret %"class.std::__2::allocator.113"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIbNS_9allocatorIbEEE5__capEv(%"class.std::__2::vector.110"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.110"*, align 4
  store %"class.std::__2::vector.110"* %this, %"class.std::__2::vector.110"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.110"*, %"class.std::__2::vector.110"** %this.addr, align 4
  %__cap_alloc_ = getelementptr inbounds %"class.std::__2::vector.110", %"class.std::__2::vector.110"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__217__compressed_pairImNS_9allocatorImEEE5firstEv(%"class.std::__2::__compressed_pair.111"* %__cap_alloc_) #8
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIbNS_9allocatorIbEEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector.110"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.110"*, align 4
  store %"class.std::__2::vector.110"* %this, %"class.std::__2::vector.110"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.110"*, %"class.std::__2::vector.110"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorImE10deallocateEPmm(%"class.std::__2::allocator.113"* %this, i32* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.113"*, align 4
  %__p.addr = alloca i32*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.113"* %this, %"class.std::__2::allocator.113"** %this.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.113"*, %"class.std::__2::allocator.113"** %this.addr, align 4
  %0 = load i32*, i32** %__p.addr, align 4
  %1 = bitcast i32* %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.113"* @_ZNSt3__217__compressed_pairImNS_9allocatorImEEE6secondEv(%"class.std::__2::__compressed_pair.111"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.111"*, align 4
  store %"class.std::__2::__compressed_pair.111"* %this, %"class.std::__2::__compressed_pair.111"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.111"*, %"class.std::__2::__compressed_pair.111"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.111"* %this1 to %"struct.std::__2::__compressed_pair_elem.112"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.113"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorImEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.112"* %0) #8
  ret %"class.std::__2::allocator.113"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.113"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorImEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.112"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.112"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.112"* %this, %"struct.std::__2::__compressed_pair_elem.112"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.112"*, %"struct.std::__2::__compressed_pair_elem.112"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.112"* %this1 to %"class.std::__2::allocator.113"*
  ret %"class.std::__2::allocator.113"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__217__compressed_pairImNS_9allocatorImEEE5firstEv(%"class.std::__2::__compressed_pair.111"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.111"*, align 4
  store %"class.std::__2::__compressed_pair.111"* %this, %"class.std::__2::__compressed_pair.111"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.111"*, %"class.std::__2::__compressed_pair.111"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.111"* %this1 to %"struct.std::__2::__compressed_pair_elem.5"*
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__222__compressed_pair_elemImLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.5"* %0) #8
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__222__compressed_pair_elemImLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.5"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.5"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.5"* %this, %"struct.std::__2::__compressed_pair_elem.5"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.5"*, %"struct.std::__2::__compressed_pair_elem.5"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.5", %"struct.std::__2::__compressed_pair_elem.5"* %this1, i32 0, i32 0
  ret i32* %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIbNS_9allocatorIbEEE4sizeEv(%"class.std::__2::vector.110"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.110"*, align 4
  store %"class.std::__2::vector.110"* %this, %"class.std::__2::vector.110"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.110"*, %"class.std::__2::vector.110"** %this.addr, align 4
  %__size_ = getelementptr inbounds %"class.std::__2::vector.110", %"class.std::__2::vector.110"* %this1, i32 0, i32 1
  %0 = load i32, i32* %__size_, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__bit_iterator"* @_ZNSt3__214__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb0ELm0EEC2Ev(%"class.std::__2::__bit_iterator"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__bit_iterator"*, align 4
  store %"class.std::__2::__bit_iterator"* %this, %"class.std::__2::__bit_iterator"** %this.addr, align 4
  %this1 = load %"class.std::__2::__bit_iterator"*, %"class.std::__2::__bit_iterator"** %this.addr, align 4
  %__seg_ = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %this1, i32 0, i32 0
  store i32* null, i32** %__seg_, align 4
  %__ctz_ = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %this1, i32 0, i32 1
  store i32 0, i32* %__ctz_, align 4
  ret %"class.std::__2::__bit_iterator"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIbNS_9allocatorIbEEE8capacityEv(%"class.std::__2::vector.110"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.110"*, align 4
  store %"class.std::__2::vector.110"* %this, %"class.std::__2::vector.110"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.110"*, %"class.std::__2::vector.110"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNKSt3__26vectorIbNS_9allocatorIbEEE5__capEv(%"class.std::__2::vector.110"* %this1) #8
  %0 = load i32, i32* %call, align 4
  %call2 = call i32 @_ZNSt3__26vectorIbNS_9allocatorIbEEE26__internal_cap_to_externalEm(i32 %0) #8
  ret i32 %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIbNS_9allocatorIbEEE3endEv(%"class.std::__2::__bit_iterator"* noalias sret align 4 %agg.result, %"class.std::__2::vector.110"* %this) #0 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %"class.std::__2::vector.110"*, align 4
  %0 = bitcast %"class.std::__2::__bit_iterator"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %"class.std::__2::vector.110"* %this, %"class.std::__2::vector.110"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.110"*, %"class.std::__2::vector.110"** %this.addr, align 4
  %__size_ = getelementptr inbounds %"class.std::__2::vector.110", %"class.std::__2::vector.110"* %this1, i32 0, i32 1
  %1 = load i32, i32* %__size_, align 4
  call void @_ZNSt3__26vectorIbNS_9allocatorIbEEE11__make_iterEm(%"class.std::__2::__bit_iterator"* sret align 4 %agg.result, %"class.std::__2::vector.110"* %this1, i32 %1) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::allocator.138"* @_ZNSt3__29allocatorIbEC2ImEERKNS0_IT_EE(%"class.std::__2::allocator.138"* returned %this, %"class.std::__2::allocator.113"* nonnull align 1 dereferenceable(1) %0) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.138"*, align 4
  %.addr = alloca %"class.std::__2::allocator.113"*, align 4
  store %"class.std::__2::allocator.138"* %this, %"class.std::__2::allocator.138"** %this.addr, align 4
  store %"class.std::__2::allocator.113"* %0, %"class.std::__2::allocator.113"** %.addr, align 4
  %this1 = load %"class.std::__2::allocator.138"*, %"class.std::__2::allocator.138"** %this.addr, align 4
  ret %"class.std::__2::allocator.138"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector.110"* @_ZNSt3__26vectorIbNS_9allocatorIbEEEC2ERKS2_(%"class.std::__2::vector.110"* returned %this, %"class.std::__2::allocator.138"* nonnull align 1 dereferenceable(1) %__a) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.110"*, align 4
  %__a.addr = alloca %"class.std::__2::allocator.138"*, align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp2 = alloca %"class.std::__2::allocator.113", align 1
  store %"class.std::__2::vector.110"* %this, %"class.std::__2::vector.110"** %this.addr, align 4
  store %"class.std::__2::allocator.138"* %__a, %"class.std::__2::allocator.138"** %__a.addr, align 4
  %this1 = load %"class.std::__2::vector.110"*, %"class.std::__2::vector.110"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.110"* %this1 to %"class.std::__2::__vector_base_common"*
  %call = call %"class.std::__2::__vector_base_common"* @_ZNSt3__220__vector_base_commonILb1EEC2Ev(%"class.std::__2::__vector_base_common"* %0)
  %__begin_ = getelementptr inbounds %"class.std::__2::vector.110", %"class.std::__2::vector.110"* %this1, i32 0, i32 0
  store i32* null, i32** %__begin_, align 4
  %__size_ = getelementptr inbounds %"class.std::__2::vector.110", %"class.std::__2::vector.110"* %this1, i32 0, i32 1
  store i32 0, i32* %__size_, align 4
  %__cap_alloc_ = getelementptr inbounds %"class.std::__2::vector.110", %"class.std::__2::vector.110"* %this1, i32 0, i32 2
  store i32 0, i32* %ref.tmp, align 4
  %1 = load %"class.std::__2::allocator.138"*, %"class.std::__2::allocator.138"** %__a.addr, align 4
  %call3 = call %"class.std::__2::allocator.113"* @_ZNSt3__29allocatorImEC2IbEERKNS0_IT_EE(%"class.std::__2::allocator.113"* %ref.tmp2, %"class.std::__2::allocator.138"* nonnull align 1 dereferenceable(1) %1) #8
  %call4 = call %"class.std::__2::__compressed_pair.111"* @_ZNSt3__217__compressed_pairImNS_9allocatorImEEEC2IiS2_EEOT_OT0_(%"class.std::__2::__compressed_pair.111"* %__cap_alloc_, i32* nonnull align 4 dereferenceable(4) %ref.tmp, %"class.std::__2::allocator.113"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  ret %"class.std::__2::vector.110"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIbNS_9allocatorIbEEE7reserveEm(%"class.std::__2::vector.110"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.110"*, align 4
  %__n.addr = alloca i32, align 4
  %__v = alloca %"class.std::__2::vector.110", align 4
  %ref.tmp = alloca %"class.std::__2::allocator.138", align 1
  %agg.tmp = alloca %"class.std::__2::__bit_iterator", align 4
  %agg.tmp5 = alloca %"class.std::__2::__bit_iterator", align 4
  store %"class.std::__2::vector.110"* %this, %"class.std::__2::vector.110"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.110"*, %"class.std::__2::vector.110"** %this.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIbNS_9allocatorIbEEE8capacityEv(%"class.std::__2::vector.110"* %this1) #8
  %cmp = icmp ugt i32 %0, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.113"* @_ZNSt3__26vectorIbNS_9allocatorIbEEE7__allocEv(%"class.std::__2::vector.110"* %this1) #8
  %call3 = call %"class.std::__2::allocator.138"* @_ZNSt3__29allocatorIbEC2ImEERKNS0_IT_EE(%"class.std::__2::allocator.138"* %ref.tmp, %"class.std::__2::allocator.113"* nonnull align 1 dereferenceable(1) %call2) #8
  %call4 = call %"class.std::__2::vector.110"* @_ZNSt3__26vectorIbNS_9allocatorIbEEEC2ERKS2_(%"class.std::__2::vector.110"* %__v, %"class.std::__2::allocator.138"* nonnull align 1 dereferenceable(1) %ref.tmp) #8
  %1 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__26vectorIbNS_9allocatorIbEEE11__vallocateEm(%"class.std::__2::vector.110"* %__v, i32 %1)
  call void @_ZNSt3__26vectorIbNS_9allocatorIbEEE5beginEv(%"class.std::__2::__bit_iterator"* sret align 4 %agg.tmp, %"class.std::__2::vector.110"* %this1) #8
  call void @_ZNSt3__26vectorIbNS_9allocatorIbEEE3endEv(%"class.std::__2::__bit_iterator"* sret align 4 %agg.tmp5, %"class.std::__2::vector.110"* %this1) #8
  call void @_ZNSt3__26vectorIbNS_9allocatorIbEEE18__construct_at_endINS_14__bit_iteratorIS3_Lb0ELm0EEEEENS_9enable_ifIXsr27__is_cpp17_forward_iteratorIT_EE5valueEvE4typeES8_S8_(%"class.std::__2::vector.110"* %__v, %"class.std::__2::__bit_iterator"* %agg.tmp, %"class.std::__2::__bit_iterator"* %agg.tmp5)
  call void @_ZNSt3__26vectorIbNS_9allocatorIbEEE4swapERS3_(%"class.std::__2::vector.110"* %this1, %"class.std::__2::vector.110"* nonnull align 4 dereferenceable(12) %__v) #8
  call void @_ZNSt3__26vectorIbNS_9allocatorIbEEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector.110"* %this1)
  %call6 = call %"class.std::__2::vector.110"* @_ZNSt3__26vectorIbNS_9allocatorIbEEED2Ev(%"class.std::__2::vector.110"* %__v) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIbNS_9allocatorIbEEE11__recommendEm(%"class.std::__2::vector.110"* %this, i32 %__new_size) #0 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %"class.std::__2::vector.110"*, align 4
  %__new_size.addr = alloca i32, align 4
  %__ms = alloca i32, align 4
  %__cap = alloca i32, align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp6 = alloca i32, align 4
  store %"class.std::__2::vector.110"* %this, %"class.std::__2::vector.110"** %this.addr, align 4
  store i32 %__new_size, i32* %__new_size.addr, align 4
  %this1 = load %"class.std::__2::vector.110"*, %"class.std::__2::vector.110"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIbNS_9allocatorIbEEE8max_sizeEv(%"class.std::__2::vector.110"* %this1) #8
  store i32 %call, i32* %__ms, align 4
  %0 = load i32, i32* %__new_size.addr, align 4
  %1 = load i32, i32* %__ms, align 4
  %cmp = icmp ugt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = bitcast %"class.std::__2::vector.110"* %this1 to %"class.std::__2::__vector_base_common"*
  call void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"* %2) #9
  unreachable

if.end:                                           ; preds = %entry
  %call2 = call i32 @_ZNKSt3__26vectorIbNS_9allocatorIbEEE8capacityEv(%"class.std::__2::vector.110"* %this1) #8
  store i32 %call2, i32* %__cap, align 4
  %3 = load i32, i32* %__cap, align 4
  %4 = load i32, i32* %__ms, align 4
  %div = udiv i32 %4, 2
  %cmp3 = icmp uge i32 %3, %div
  br i1 %cmp3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %if.end
  %5 = load i32, i32* %__ms, align 4
  store i32 %5, i32* %retval, align 4
  br label %return

if.end5:                                          ; preds = %if.end
  %6 = load i32, i32* %__cap, align 4
  %mul = mul i32 2, %6
  store i32 %mul, i32* %ref.tmp, align 4
  %7 = load i32, i32* %__new_size.addr, align 4
  %call7 = call i32 @_ZNSt3__26vectorIbNS_9allocatorIbEEE10__align_itEm(i32 %7) #8
  store i32 %call7, i32* %ref.tmp6, align 4
  %call8 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %ref.tmp6)
  %8 = load i32, i32* %call8, align 4
  store i32 %8, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end5, %if.then4
  %9 = load i32, i32* %retval, align 4
  ret i32 %9
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__24copyINS_6vectorIbNS_9allocatorIbEEEELb1EEENS_14__bit_iteratorIT_Lb0EXLi0EEEENS5_IS6_XT0_EXLi0EEEES8_S7_(%"class.std::__2::__bit_iterator"* noalias sret align 4 %agg.result, %"class.std::__2::__bit_iterator.140"* byval(%"class.std::__2::__bit_iterator.140") align 4 %__first, %"class.std::__2::__bit_iterator.140"* byval(%"class.std::__2::__bit_iterator.140") align 4 %__last, %"class.std::__2::__bit_iterator"* %__result) #0 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %agg.tmp = alloca %"class.std::__2::__bit_iterator.140", align 4
  %agg.tmp2 = alloca %"class.std::__2::__bit_iterator.140", align 4
  %agg.tmp3 = alloca %"class.std::__2::__bit_iterator", align 4
  %agg.tmp4 = alloca %"class.std::__2::__bit_iterator.140", align 4
  %agg.tmp5 = alloca %"class.std::__2::__bit_iterator.140", align 4
  %agg.tmp6 = alloca %"class.std::__2::__bit_iterator", align 4
  %0 = bitcast %"class.std::__2::__bit_iterator"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  %__ctz_ = getelementptr inbounds %"class.std::__2::__bit_iterator.140", %"class.std::__2::__bit_iterator.140"* %__first, i32 0, i32 1
  %1 = load i32, i32* %__ctz_, align 4
  %__ctz_1 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 1
  %2 = load i32, i32* %__ctz_1, align 4
  %cmp = icmp eq i32 %1, %2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %3 = bitcast %"class.std::__2::__bit_iterator.140"* %agg.tmp to i8*
  %4 = bitcast %"class.std::__2::__bit_iterator.140"* %__first to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 8, i1 false)
  %5 = bitcast %"class.std::__2::__bit_iterator.140"* %agg.tmp2 to i8*
  %6 = bitcast %"class.std::__2::__bit_iterator.140"* %__last to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %5, i8* align 4 %6, i32 8, i1 false)
  %call = call %"class.std::__2::__bit_iterator"* @_ZNSt3__214__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb0ELm0EEC2ERKS5_(%"class.std::__2::__bit_iterator"* %agg.tmp3, %"class.std::__2::__bit_iterator"* nonnull align 4 dereferenceable(8) %__result) #8
  call void @_ZNSt3__214__copy_alignedINS_6vectorIbNS_9allocatorIbEEEELb1EEENS_14__bit_iteratorIT_Lb0EXLi0EEEENS5_IS6_XT0_EXLi0EEEES8_S7_(%"class.std::__2::__bit_iterator"* sret align 4 %agg.result, %"class.std::__2::__bit_iterator.140"* byval(%"class.std::__2::__bit_iterator.140") align 4 %agg.tmp, %"class.std::__2::__bit_iterator.140"* byval(%"class.std::__2::__bit_iterator.140") align 4 %agg.tmp2, %"class.std::__2::__bit_iterator"* %agg.tmp3)
  br label %return

if.end:                                           ; preds = %entry
  %7 = bitcast %"class.std::__2::__bit_iterator.140"* %agg.tmp4 to i8*
  %8 = bitcast %"class.std::__2::__bit_iterator.140"* %__first to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 8, i1 false)
  %9 = bitcast %"class.std::__2::__bit_iterator.140"* %agg.tmp5 to i8*
  %10 = bitcast %"class.std::__2::__bit_iterator.140"* %__last to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 8, i1 false)
  %call7 = call %"class.std::__2::__bit_iterator"* @_ZNSt3__214__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb0ELm0EEC2ERKS5_(%"class.std::__2::__bit_iterator"* %agg.tmp6, %"class.std::__2::__bit_iterator"* nonnull align 4 dereferenceable(8) %__result) #8
  call void @_ZNSt3__216__copy_unalignedINS_6vectorIbNS_9allocatorIbEEEELb1EEENS_14__bit_iteratorIT_Lb0EXLi0EEEENS5_IS6_XT0_EXLi0EEEES8_S7_(%"class.std::__2::__bit_iterator"* sret align 4 %agg.result, %"class.std::__2::__bit_iterator.140"* byval(%"class.std::__2::__bit_iterator.140") align 4 %agg.tmp4, %"class.std::__2::__bit_iterator.140"* byval(%"class.std::__2::__bit_iterator.140") align 4 %agg.tmp5, %"class.std::__2::__bit_iterator"* %agg.tmp6)
  br label %return

return:                                           ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIbNS_9allocatorIbEEE6cbeginEv(%"class.std::__2::__bit_iterator.140"* noalias sret align 4 %agg.result, %"class.std::__2::vector.110"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.110"*, align 4
  store %"class.std::__2::vector.110"* %this, %"class.std::__2::vector.110"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.110"*, %"class.std::__2::vector.110"** %this.addr, align 4
  call void @_ZNKSt3__26vectorIbNS_9allocatorIbEEE11__make_iterEm(%"class.std::__2::__bit_iterator.140"* sret align 4 %agg.result, %"class.std::__2::vector.110"* %this1, i32 0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIbNS_9allocatorIbEEE4cendEv(%"class.std::__2::__bit_iterator.140"* noalias sret align 4 %agg.result, %"class.std::__2::vector.110"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.110"*, align 4
  store %"class.std::__2::vector.110"* %this, %"class.std::__2::vector.110"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.110"*, %"class.std::__2::vector.110"** %this.addr, align 4
  %__size_ = getelementptr inbounds %"class.std::__2::vector.110", %"class.std::__2::vector.110"* %this1, i32 0, i32 1
  %0 = load i32, i32* %__size_, align 4
  call void @_ZNKSt3__26vectorIbNS_9allocatorIbEEE11__make_iterEm(%"class.std::__2::__bit_iterator.140"* sret align 4 %agg.result, %"class.std::__2::vector.110"* %this1, i32 %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIbNS_9allocatorIbEEE5beginEv(%"class.std::__2::__bit_iterator"* noalias sret align 4 %agg.result, %"class.std::__2::vector.110"* %this) #0 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %"class.std::__2::vector.110"*, align 4
  %0 = bitcast %"class.std::__2::__bit_iterator"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %"class.std::__2::vector.110"* %this, %"class.std::__2::vector.110"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.110"*, %"class.std::__2::vector.110"** %this.addr, align 4
  call void @_ZNSt3__26vectorIbNS_9allocatorIbEEE11__make_iterEm(%"class.std::__2::__bit_iterator"* sret align 4 %agg.result, %"class.std::__2::vector.110"* %this1, i32 0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIbNS_9allocatorIbEEE4swapERS3_(%"class.std::__2::vector.110"* %this, %"class.std::__2::vector.110"* nonnull align 4 dereferenceable(12) %__x) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.110"*, align 4
  %__x.addr = alloca %"class.std::__2::vector.110"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant.129", align 1
  store %"class.std::__2::vector.110"* %this, %"class.std::__2::vector.110"** %this.addr, align 4
  store %"class.std::__2::vector.110"* %__x, %"class.std::__2::vector.110"** %__x.addr, align 4
  %this1 = load %"class.std::__2::vector.110"*, %"class.std::__2::vector.110"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::vector.110", %"class.std::__2::vector.110"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::vector.110"*, %"class.std::__2::vector.110"** %__x.addr, align 4
  %__begin_2 = getelementptr inbounds %"class.std::__2::vector.110", %"class.std::__2::vector.110"* %0, i32 0, i32 0
  call void @_ZNSt3__24swapIPmEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_(i32** nonnull align 4 dereferenceable(4) %__begin_, i32** nonnull align 4 dereferenceable(4) %__begin_2) #8
  %__size_ = getelementptr inbounds %"class.std::__2::vector.110", %"class.std::__2::vector.110"* %this1, i32 0, i32 1
  %1 = load %"class.std::__2::vector.110"*, %"class.std::__2::vector.110"** %__x.addr, align 4
  %__size_3 = getelementptr inbounds %"class.std::__2::vector.110", %"class.std::__2::vector.110"* %1, i32 0, i32 1
  call void @_ZNSt3__24swapImEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS2_EE5valueEvE4typeERS2_S5_(i32* nonnull align 4 dereferenceable(4) %__size_, i32* nonnull align 4 dereferenceable(4) %__size_3) #8
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIbNS_9allocatorIbEEE5__capEv(%"class.std::__2::vector.110"* %this1) #8
  %2 = load %"class.std::__2::vector.110"*, %"class.std::__2::vector.110"** %__x.addr, align 4
  %call4 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIbNS_9allocatorIbEEE5__capEv(%"class.std::__2::vector.110"* %2) #8
  call void @_ZNSt3__24swapImEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS2_EE5valueEvE4typeERS2_S5_(i32* nonnull align 4 dereferenceable(4) %call, i32* nonnull align 4 dereferenceable(4) %call4) #8
  %call5 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.113"* @_ZNSt3__26vectorIbNS_9allocatorIbEEE7__allocEv(%"class.std::__2::vector.110"* %this1) #8
  %3 = load %"class.std::__2::vector.110"*, %"class.std::__2::vector.110"** %__x.addr, align 4
  %call6 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.113"* @_ZNSt3__26vectorIbNS_9allocatorIbEEE7__allocEv(%"class.std::__2::vector.110"* %3) #8
  call void @_ZNSt3__216__swap_allocatorINS_9allocatorImEEEEvRT_S4_NS_17integral_constantIbLb0EEE(%"class.std::__2::allocator.113"* nonnull align 1 dereferenceable(1) %call5, %"class.std::__2::allocator.113"* nonnull align 1 dereferenceable(1) %call6) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26fill_nINS_6vectorIbNS_9allocatorIbEEEEEEvNS_14__bit_iteratorIT_Lb0EXLi0EEEENS6_9size_typeEb(%"class.std::__2::__bit_iterator"* %__first, i32 %__n, i1 zeroext %__value_) #0 comdat {
entry:
  %__n.addr = alloca i32, align 4
  %__value_.addr = alloca i8, align 1
  %agg.tmp = alloca %"class.std::__2::__bit_iterator", align 4
  %agg.tmp2 = alloca %"class.std::__2::__bit_iterator", align 4
  store i32 %__n, i32* %__n.addr, align 4
  %frombool = zext i1 %__value_ to i8
  store i8 %frombool, i8* %__value_.addr, align 1
  %0 = load i32, i32* %__n.addr, align 4
  %cmp = icmp ugt i32 %0, 0
  br i1 %cmp, label %if.then, label %if.end4

if.then:                                          ; preds = %entry
  %1 = load i8, i8* %__value_.addr, align 1
  %tobool = trunc i8 %1 to i1
  br i1 %tobool, label %if.then1, label %if.else

if.then1:                                         ; preds = %if.then
  %call = call %"class.std::__2::__bit_iterator"* @_ZNSt3__214__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb0ELm0EEC2ERKS5_(%"class.std::__2::__bit_iterator"* %agg.tmp, %"class.std::__2::__bit_iterator"* nonnull align 4 dereferenceable(8) %__first) #8
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__213__fill_n_trueINS_6vectorIbNS_9allocatorIbEEEEEEvNS_14__bit_iteratorIT_Lb0EXLi0EEEENS6_9size_typeE(%"class.std::__2::__bit_iterator"* %agg.tmp, i32 %2)
  br label %if.end

if.else:                                          ; preds = %if.then
  %call3 = call %"class.std::__2::__bit_iterator"* @_ZNSt3__214__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb0ELm0EEC2ERKS5_(%"class.std::__2::__bit_iterator"* %agg.tmp2, %"class.std::__2::__bit_iterator"* nonnull align 4 dereferenceable(8) %__first) #8
  %3 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__214__fill_n_falseINS_6vectorIbNS_9allocatorIbEEEEEEvNS_14__bit_iteratorIT_Lb0EXLi0EEEENS6_9size_typeE(%"class.std::__2::__bit_iterator"* %agg.tmp2, i32 %3)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then1
  br label %if.end4

if.end4:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__bit_iterator"* @_ZNSt3__214__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb0ELm0EEC2ERKS5_(%"class.std::__2::__bit_iterator"* returned %this, %"class.std::__2::__bit_iterator"* nonnull align 4 dereferenceable(8) %__it) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__bit_iterator"*, align 4
  %__it.addr = alloca %"class.std::__2::__bit_iterator"*, align 4
  store %"class.std::__2::__bit_iterator"* %this, %"class.std::__2::__bit_iterator"** %this.addr, align 4
  store %"class.std::__2::__bit_iterator"* %__it, %"class.std::__2::__bit_iterator"** %__it.addr, align 4
  %this1 = load %"class.std::__2::__bit_iterator"*, %"class.std::__2::__bit_iterator"** %this.addr, align 4
  %__seg_ = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::__bit_iterator"*, %"class.std::__2::__bit_iterator"** %__it.addr, align 4
  %__seg_2 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %0, i32 0, i32 0
  %1 = load i32*, i32** %__seg_2, align 4
  store i32* %1, i32** %__seg_, align 4
  %__ctz_ = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %this1, i32 0, i32 1
  %2 = load %"class.std::__2::__bit_iterator"*, %"class.std::__2::__bit_iterator"** %__it.addr, align 4
  %__ctz_3 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %2, i32 0, i32 1
  %3 = load i32, i32* %__ctz_3, align 4
  store i32 %3, i32* %__ctz_, align 4
  ret %"class.std::__2::__bit_iterator"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__26vectorIbNS_9allocatorIbEEE26__internal_cap_to_externalEm(i32 %__n) #0 comdat {
entry:
  %__n.addr = alloca i32, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %0, 32
  ret i32 %mul
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNKSt3__26vectorIbNS_9allocatorIbEEE5__capEv(%"class.std::__2::vector.110"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.110"*, align 4
  store %"class.std::__2::vector.110"* %this, %"class.std::__2::vector.110"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.110"*, %"class.std::__2::vector.110"** %this.addr, align 4
  %__cap_alloc_ = getelementptr inbounds %"class.std::__2::vector.110", %"class.std::__2::vector.110"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNKSt3__217__compressed_pairImNS_9allocatorImEEE5firstEv(%"class.std::__2::__compressed_pair.111"* %__cap_alloc_) #8
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNKSt3__217__compressed_pairImNS_9allocatorImEEE5firstEv(%"class.std::__2::__compressed_pair.111"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.111"*, align 4
  store %"class.std::__2::__compressed_pair.111"* %this, %"class.std::__2::__compressed_pair.111"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.111"*, %"class.std::__2::__compressed_pair.111"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.111"* %this1 to %"struct.std::__2::__compressed_pair_elem.5"*
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNKSt3__222__compressed_pair_elemImLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.5"* %0) #8
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNKSt3__222__compressed_pair_elemImLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.5"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.5"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.5"* %this, %"struct.std::__2::__compressed_pair_elem.5"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.5"*, %"struct.std::__2::__compressed_pair_elem.5"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.5", %"struct.std::__2::__compressed_pair_elem.5"* %this1, i32 0, i32 0
  ret i32* %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIbNS_9allocatorIbEEE11__make_iterEm(%"class.std::__2::__bit_iterator"* noalias sret align 4 %agg.result, %"class.std::__2::vector.110"* %this, i32 %__pos) #0 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %"class.std::__2::vector.110"*, align 4
  %__pos.addr = alloca i32, align 4
  %0 = bitcast %"class.std::__2::__bit_iterator"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %"class.std::__2::vector.110"* %this, %"class.std::__2::vector.110"** %this.addr, align 4
  store i32 %__pos, i32* %__pos.addr, align 4
  %this1 = load %"class.std::__2::vector.110"*, %"class.std::__2::vector.110"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::vector.110", %"class.std::__2::vector.110"* %this1, i32 0, i32 0
  %1 = load i32*, i32** %__begin_, align 4
  %2 = load i32, i32* %__pos.addr, align 4
  %div = udiv i32 %2, 32
  %add.ptr = getelementptr inbounds i32, i32* %1, i32 %div
  %3 = load i32, i32* %__pos.addr, align 4
  %rem = urem i32 %3, 32
  %call = call %"class.std::__2::__bit_iterator"* @_ZNSt3__214__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb0ELm0EEC2EPmj(%"class.std::__2::__bit_iterator"* %agg.result, i32* %add.ptr, i32 %rem) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__bit_iterator"* @_ZNSt3__214__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb0ELm0EEC2EPmj(%"class.std::__2::__bit_iterator"* returned %this, i32* %__s, i32 %__ctz) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__bit_iterator"*, align 4
  %__s.addr = alloca i32*, align 4
  %__ctz.addr = alloca i32, align 4
  store %"class.std::__2::__bit_iterator"* %this, %"class.std::__2::__bit_iterator"** %this.addr, align 4
  store i32* %__s, i32** %__s.addr, align 4
  store i32 %__ctz, i32* %__ctz.addr, align 4
  %this1 = load %"class.std::__2::__bit_iterator"*, %"class.std::__2::__bit_iterator"** %this.addr, align 4
  %__seg_ = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__s.addr, align 4
  store i32* %0, i32** %__seg_, align 4
  %__ctz_ = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %this1, i32 0, i32 1
  %1 = load i32, i32* %__ctz.addr, align 4
  store i32 %1, i32* %__ctz_, align 4
  ret %"class.std::__2::__bit_iterator"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::allocator.113"* @_ZNSt3__29allocatorImEC2IbEERKNS0_IT_EE(%"class.std::__2::allocator.113"* returned %this, %"class.std::__2::allocator.138"* nonnull align 1 dereferenceable(1) %0) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.113"*, align 4
  %.addr = alloca %"class.std::__2::allocator.138"*, align 4
  store %"class.std::__2::allocator.113"* %this, %"class.std::__2::allocator.113"** %this.addr, align 4
  store %"class.std::__2::allocator.138"* %0, %"class.std::__2::allocator.138"** %.addr, align 4
  %this1 = load %"class.std::__2::allocator.113"*, %"class.std::__2::allocator.113"** %this.addr, align 4
  ret %"class.std::__2::allocator.113"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.111"* @_ZNSt3__217__compressed_pairImNS_9allocatorImEEEC2IiS2_EEOT_OT0_(%"class.std::__2::__compressed_pair.111"* returned %this, i32* nonnull align 4 dereferenceable(4) %__t1, %"class.std::__2::allocator.113"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.111"*, align 4
  %__t1.addr = alloca i32*, align 4
  %__t2.addr = alloca %"class.std::__2::allocator.113"*, align 4
  store %"class.std::__2::__compressed_pair.111"* %this, %"class.std::__2::__compressed_pair.111"** %this.addr, align 4
  store i32* %__t1, i32** %__t1.addr, align 4
  store %"class.std::__2::allocator.113"* %__t2, %"class.std::__2::allocator.113"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.111"*, %"class.std::__2::__compressed_pair.111"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.111"* %this1 to %"struct.std::__2::__compressed_pair_elem.5"*
  %1 = load i32*, i32** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__27forwardIiEEOT_RNS_16remove_referenceIS1_E4typeE(i32* nonnull align 4 dereferenceable(4) %1) #8
  %call2 = call %"struct.std::__2::__compressed_pair_elem.5"* @_ZNSt3__222__compressed_pair_elemImLi0ELb0EEC2IivEEOT_(%"struct.std::__2::__compressed_pair_elem.5"* %0, i32* nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.111"* %this1 to %"struct.std::__2::__compressed_pair_elem.112"*
  %3 = load %"class.std::__2::allocator.113"*, %"class.std::__2::allocator.113"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.113"* @_ZNSt3__27forwardINS_9allocatorImEEEEOT_RNS_16remove_referenceIS3_E4typeE(%"class.std::__2::allocator.113"* nonnull align 1 dereferenceable(1) %3) #8
  %call4 = call %"struct.std::__2::__compressed_pair_elem.112"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorImEELi1ELb1EEC2IS2_vEEOT_(%"struct.std::__2::__compressed_pair_elem.112"* %2, %"class.std::__2::allocator.113"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::__compressed_pair.111"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.113"* @_ZNSt3__27forwardINS_9allocatorImEEEEOT_RNS_16remove_referenceIS3_E4typeE(%"class.std::__2::allocator.113"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::allocator.113"*, align 4
  store %"class.std::__2::allocator.113"* %__t, %"class.std::__2::allocator.113"** %__t.addr, align 4
  %0 = load %"class.std::__2::allocator.113"*, %"class.std::__2::allocator.113"** %__t.addr, align 4
  ret %"class.std::__2::allocator.113"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.112"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorImEELi1ELb1EEC2IS2_vEEOT_(%"struct.std::__2::__compressed_pair_elem.112"* returned %this, %"class.std::__2::allocator.113"* nonnull align 1 dereferenceable(1) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.112"*, align 4
  %__u.addr = alloca %"class.std::__2::allocator.113"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.112"* %this, %"struct.std::__2::__compressed_pair_elem.112"** %this.addr, align 4
  store %"class.std::__2::allocator.113"* %__u, %"class.std::__2::allocator.113"** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.112"*, %"struct.std::__2::__compressed_pair_elem.112"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.112"* %this1 to %"class.std::__2::allocator.113"*
  %1 = load %"class.std::__2::allocator.113"*, %"class.std::__2::allocator.113"** %__u.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.113"* @_ZNSt3__27forwardINS_9allocatorImEEEEOT_RNS_16remove_referenceIS3_E4typeE(%"class.std::__2::allocator.113"* nonnull align 1 dereferenceable(1) %1) #8
  ret %"struct.std::__2::__compressed_pair_elem.112"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIbNS_9allocatorIbEEE11__vallocateEm(%"class.std::__2::vector.110"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.110"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector.110"* %this, %"class.std::__2::vector.110"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.110"*, %"class.std::__2::vector.110"** %this.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIbNS_9allocatorIbEEE8max_sizeEv(%"class.std::__2::vector.110"* %this1) #8
  %cmp = icmp ugt i32 %0, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %"class.std::__2::vector.110"* %this1 to %"class.std::__2::__vector_base_common"*
  call void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"* %1) #9
  unreachable

if.end:                                           ; preds = %entry
  %2 = load i32, i32* %__n.addr, align 4
  %call2 = call i32 @_ZNSt3__26vectorIbNS_9allocatorIbEEE26__external_cap_to_internalEm(i32 %2) #8
  store i32 %call2, i32* %__n.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.113"* @_ZNSt3__26vectorIbNS_9allocatorIbEEE7__allocEv(%"class.std::__2::vector.110"* %this1) #8
  %3 = load i32, i32* %__n.addr, align 4
  %call4 = call i32* @_ZNSt3__216allocator_traitsINS_9allocatorImEEE8allocateERS2_m(%"class.std::__2::allocator.113"* nonnull align 1 dereferenceable(1) %call3, i32 %3)
  %__begin_ = getelementptr inbounds %"class.std::__2::vector.110", %"class.std::__2::vector.110"* %this1, i32 0, i32 0
  store i32* %call4, i32** %__begin_, align 4
  %__size_ = getelementptr inbounds %"class.std::__2::vector.110", %"class.std::__2::vector.110"* %this1, i32 0, i32 1
  store i32 0, i32* %__size_, align 4
  %4 = load i32, i32* %__n.addr, align 4
  %call5 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIbNS_9allocatorIbEEE5__capEv(%"class.std::__2::vector.110"* %this1) #8
  store i32 %4, i32* %call5, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIbNS_9allocatorIbEEE18__construct_at_endINS_14__bit_iteratorIS3_Lb0ELm0EEEEENS_9enable_ifIXsr27__is_cpp17_forward_iteratorIT_EE5valueEvE4typeES8_S8_(%"class.std::__2::vector.110"* %this, %"class.std::__2::__bit_iterator"* %__first, %"class.std::__2::__bit_iterator"* %__last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.110"*, align 4
  %__old_size = alloca i32, align 4
  %agg.tmp = alloca %"class.std::__2::__bit_iterator", align 4
  %agg.tmp2 = alloca %"class.std::__2::__bit_iterator", align 4
  %agg.tmp19 = alloca %"class.std::__2::__bit_iterator", align 4
  %agg.tmp21 = alloca %"class.std::__2::__bit_iterator", align 4
  %agg.tmp23 = alloca %"class.std::__2::__bit_iterator", align 4
  %tmp = alloca %"class.std::__2::__bit_iterator", align 4
  store %"class.std::__2::vector.110"* %this, %"class.std::__2::vector.110"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.110"*, %"class.std::__2::vector.110"** %this.addr, align 4
  %__size_ = getelementptr inbounds %"class.std::__2::vector.110", %"class.std::__2::vector.110"* %this1, i32 0, i32 1
  %0 = load i32, i32* %__size_, align 4
  store i32 %0, i32* %__old_size, align 4
  %call = call %"class.std::__2::__bit_iterator"* @_ZNSt3__214__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb0ELm0EEC2ERKS5_(%"class.std::__2::__bit_iterator"* %agg.tmp, %"class.std::__2::__bit_iterator"* nonnull align 4 dereferenceable(8) %__first) #8
  %call3 = call %"class.std::__2::__bit_iterator"* @_ZNSt3__214__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb0ELm0EEC2ERKS5_(%"class.std::__2::__bit_iterator"* %agg.tmp2, %"class.std::__2::__bit_iterator"* nonnull align 4 dereferenceable(8) %__last) #8
  %call4 = call i32 @_ZNSt3__28distanceINS_14__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb0ELm0EEEEENS_15iterator_traitsIT_E15difference_typeES8_S8_(%"class.std::__2::__bit_iterator"* %agg.tmp, %"class.std::__2::__bit_iterator"* %agg.tmp2)
  %__size_5 = getelementptr inbounds %"class.std::__2::vector.110", %"class.std::__2::vector.110"* %this1, i32 0, i32 1
  %1 = load i32, i32* %__size_5, align 4
  %add = add i32 %1, %call4
  store i32 %add, i32* %__size_5, align 4
  %2 = load i32, i32* %__old_size, align 4
  %cmp = icmp eq i32 %2, 0
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %3 = load i32, i32* %__old_size, align 4
  %sub = sub i32 %3, 1
  %div = udiv i32 %sub, 32
  %__size_6 = getelementptr inbounds %"class.std::__2::vector.110", %"class.std::__2::vector.110"* %this1, i32 0, i32 1
  %4 = load i32, i32* %__size_6, align 4
  %sub7 = sub i32 %4, 1
  %div8 = udiv i32 %sub7, 32
  %cmp9 = icmp ne i32 %div, %div8
  br i1 %cmp9, label %if.then, label %if.end18

if.then:                                          ; preds = %lor.lhs.false, %entry
  %__size_10 = getelementptr inbounds %"class.std::__2::vector.110", %"class.std::__2::vector.110"* %this1, i32 0, i32 1
  %5 = load i32, i32* %__size_10, align 4
  %cmp11 = icmp ule i32 %5, 32
  br i1 %cmp11, label %if.then12, label %if.else

if.then12:                                        ; preds = %if.then
  %__begin_ = getelementptr inbounds %"class.std::__2::vector.110", %"class.std::__2::vector.110"* %this1, i32 0, i32 0
  %6 = load i32*, i32** %__begin_, align 4
  %arrayidx = getelementptr inbounds i32, i32* %6, i32 0
  store i32 0, i32* %arrayidx, align 4
  br label %if.end

if.else:                                          ; preds = %if.then
  %__begin_13 = getelementptr inbounds %"class.std::__2::vector.110", %"class.std::__2::vector.110"* %this1, i32 0, i32 0
  %7 = load i32*, i32** %__begin_13, align 4
  %__size_14 = getelementptr inbounds %"class.std::__2::vector.110", %"class.std::__2::vector.110"* %this1, i32 0, i32 1
  %8 = load i32, i32* %__size_14, align 4
  %sub15 = sub i32 %8, 1
  %div16 = udiv i32 %sub15, 32
  %arrayidx17 = getelementptr inbounds i32, i32* %7, i32 %div16
  store i32 0, i32* %arrayidx17, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then12
  br label %if.end18

if.end18:                                         ; preds = %if.end, %lor.lhs.false
  %call20 = call %"class.std::__2::__bit_iterator"* @_ZNSt3__214__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb0ELm0EEC2ERKS5_(%"class.std::__2::__bit_iterator"* %agg.tmp19, %"class.std::__2::__bit_iterator"* nonnull align 4 dereferenceable(8) %__first) #8
  %call22 = call %"class.std::__2::__bit_iterator"* @_ZNSt3__214__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb0ELm0EEC2ERKS5_(%"class.std::__2::__bit_iterator"* %agg.tmp21, %"class.std::__2::__bit_iterator"* nonnull align 4 dereferenceable(8) %__last) #8
  %9 = load i32, i32* %__old_size, align 4
  call void @_ZNSt3__26vectorIbNS_9allocatorIbEEE11__make_iterEm(%"class.std::__2::__bit_iterator"* sret align 4 %agg.tmp23, %"class.std::__2::vector.110"* %this1, i32 %9) #8
  call void @_ZNSt3__24copyINS_6vectorIbNS_9allocatorIbEEEELb0EEENS_14__bit_iteratorIT_Lb0EXLi0EEEENS5_IS6_XT0_EXLi0EEEES8_S7_(%"class.std::__2::__bit_iterator"* sret align 4 %tmp, %"class.std::__2::__bit_iterator"* %agg.tmp19, %"class.std::__2::__bit_iterator"* %agg.tmp21, %"class.std::__2::__bit_iterator"* %agg.tmp23)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIbNS_9allocatorIbEEE8max_sizeEv(%"class.std::__2::vector.110"* %this) #0 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %"class.std::__2::vector.110"*, align 4
  %__amax = alloca i32, align 4
  %__nmax = alloca i32, align 4
  store %"class.std::__2::vector.110"* %this, %"class.std::__2::vector.110"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.110"*, %"class.std::__2::vector.110"** %this.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.113"* @_ZNKSt3__26vectorIbNS_9allocatorIbEEE7__allocEv(%"class.std::__2::vector.110"* %this1) #8
  %call2 = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorImEEE8max_sizeERKS2_(%"class.std::__2::allocator.113"* nonnull align 1 dereferenceable(1) %call) #8
  store i32 %call2, i32* %__amax, align 4
  %call3 = call i32 @_ZNSt3__214numeric_limitsImE3maxEv() #8
  %div = udiv i32 %call3, 2
  store i32 %div, i32* %__nmax, align 4
  %0 = load i32, i32* %__nmax, align 4
  %div4 = udiv i32 %0, 32
  %1 = load i32, i32* %__amax, align 4
  %cmp = icmp ule i32 %div4, %1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %__nmax, align 4
  store i32 %2, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %3 = load i32, i32* %__amax, align 4
  %call5 = call i32 @_ZNSt3__26vectorIbNS_9allocatorIbEEE26__internal_cap_to_externalEm(i32 %3) #8
  store i32 %call5, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %4 = load i32, i32* %retval, align 4
  ret i32 %4
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__26vectorIbNS_9allocatorIbEEE26__external_cap_to_internalEm(i32 %__n) #0 comdat {
entry:
  %__n.addr = alloca i32, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %sub = sub i32 %0, 1
  %div = udiv i32 %sub, 32
  %add = add i32 %div, 1
  ret i32 %add
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNSt3__216allocator_traitsINS_9allocatorImEEE8allocateERS2_m(%"class.std::__2::allocator.113"* nonnull align 1 dereferenceable(1) %__a, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.113"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.113"* %__a, %"class.std::__2::allocator.113"** %__a.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.113"*, %"class.std::__2::allocator.113"** %__a.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call i32* @_ZNSt3__29allocatorImE8allocateEmPKv(%"class.std::__2::allocator.113"* %0, i32 %1, i8* null)
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorImEEE8max_sizeERKS2_(%"class.std::__2::allocator.113"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.113"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_max_size.141", align 1
  store %"class.std::__2::allocator.113"* %__a, %"class.std::__2::allocator.113"** %__a.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_max_size.141"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.113"*, %"class.std::__2::allocator.113"** %__a.addr, align 4
  %call = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorImEEE10__max_sizeENS_17integral_constantIbLb1EEERKS2_(%"class.std::__2::allocator.113"* nonnull align 1 dereferenceable(1) %1) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.113"* @_ZNKSt3__26vectorIbNS_9allocatorIbEEE7__allocEv(%"class.std::__2::vector.110"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.110"*, align 4
  store %"class.std::__2::vector.110"* %this, %"class.std::__2::vector.110"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.110"*, %"class.std::__2::vector.110"** %this.addr, align 4
  %__cap_alloc_ = getelementptr inbounds %"class.std::__2::vector.110", %"class.std::__2::vector.110"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.113"* @_ZNKSt3__217__compressed_pairImNS_9allocatorImEEE6secondEv(%"class.std::__2::__compressed_pair.111"* %__cap_alloc_) #8
  ret %"class.std::__2::allocator.113"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__214numeric_limitsImE3maxEv() #0 comdat {
entry:
  %call = call i32 @_ZNSt3__223__libcpp_numeric_limitsImLb1EE3maxEv() #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorImEEE10__max_sizeENS_17integral_constantIbLb1EEERKS2_(%"class.std::__2::allocator.113"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.113"*, align 4
  store %"class.std::__2::allocator.113"* %__a, %"class.std::__2::allocator.113"** %__a.addr, align 4
  %1 = load %"class.std::__2::allocator.113"*, %"class.std::__2::allocator.113"** %__a.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorImE8max_sizeEv(%"class.std::__2::allocator.113"* %1) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__29allocatorImE8max_sizeEv(%"class.std::__2::allocator.113"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.113"*, align 4
  store %"class.std::__2::allocator.113"* %this, %"class.std::__2::allocator.113"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.113"*, %"class.std::__2::allocator.113"** %this.addr, align 4
  ret i32 1073741823
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.113"* @_ZNKSt3__217__compressed_pairImNS_9allocatorImEEE6secondEv(%"class.std::__2::__compressed_pair.111"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.111"*, align 4
  store %"class.std::__2::__compressed_pair.111"* %this, %"class.std::__2::__compressed_pair.111"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.111"*, %"class.std::__2::__compressed_pair.111"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.111"* %this1 to %"struct.std::__2::__compressed_pair_elem.112"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.113"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorImEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.112"* %0) #8
  ret %"class.std::__2::allocator.113"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.113"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorImEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.112"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.112"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.112"* %this, %"struct.std::__2::__compressed_pair_elem.112"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.112"*, %"struct.std::__2::__compressed_pair_elem.112"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.112"* %this1 to %"class.std::__2::allocator.113"*
  ret %"class.std::__2::allocator.113"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__223__libcpp_numeric_limitsImLb1EE3maxEv() #0 comdat {
entry:
  ret i32 -1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNSt3__29allocatorImE8allocateEmPKv(%"class.std::__2::allocator.113"* %this, i32 %__n, i8* %0) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.113"*, align 4
  %__n.addr = alloca i32, align 4
  %.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.113"* %this, %"class.std::__2::allocator.113"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::allocator.113"*, %"class.std::__2::allocator.113"** %this.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorImE8max_sizeEv(%"class.std::__2::allocator.113"* %this1) #8
  %cmp = icmp ugt i32 %1, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__220__throw_length_errorEPKc(i8* getelementptr inbounds ([68 x i8], [68 x i8]* @.str, i32 0, i32 0)) #9
  unreachable

if.end:                                           ; preds = %entry
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  %call2 = call i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %mul, i32 4)
  %3 = bitcast i8* %call2 to i32*
  ret i32* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__28distanceINS_14__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb0ELm0EEEEENS_15iterator_traitsIT_E15difference_typeES8_S8_(%"class.std::__2::__bit_iterator"* %__first, %"class.std::__2::__bit_iterator"* %__last) #0 comdat {
entry:
  %agg.tmp = alloca %"class.std::__2::__bit_iterator", align 4
  %agg.tmp1 = alloca %"class.std::__2::__bit_iterator", align 4
  %agg.tmp3 = alloca %"struct.std::__2::random_access_iterator_tag", align 1
  %call = call %"class.std::__2::__bit_iterator"* @_ZNSt3__214__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb0ELm0EEC2ERKS5_(%"class.std::__2::__bit_iterator"* %agg.tmp, %"class.std::__2::__bit_iterator"* nonnull align 4 dereferenceable(8) %__first) #8
  %call2 = call %"class.std::__2::__bit_iterator"* @_ZNSt3__214__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb0ELm0EEC2ERKS5_(%"class.std::__2::__bit_iterator"* %agg.tmp1, %"class.std::__2::__bit_iterator"* nonnull align 4 dereferenceable(8) %__last) #8
  %call4 = call i32 @_ZNSt3__210__distanceINS_14__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb0ELm0EEEEENS_15iterator_traitsIT_E15difference_typeES8_S8_NS_26random_access_iterator_tagE(%"class.std::__2::__bit_iterator"* %agg.tmp, %"class.std::__2::__bit_iterator"* %agg.tmp1)
  ret i32 %call4
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__24copyINS_6vectorIbNS_9allocatorIbEEEELb0EEENS_14__bit_iteratorIT_Lb0EXLi0EEEENS5_IS6_XT0_EXLi0EEEES8_S7_(%"class.std::__2::__bit_iterator"* noalias sret align 4 %agg.result, %"class.std::__2::__bit_iterator"* %__first, %"class.std::__2::__bit_iterator"* %__last, %"class.std::__2::__bit_iterator"* %__result) #0 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %agg.tmp = alloca %"class.std::__2::__bit_iterator", align 4
  %agg.tmp2 = alloca %"class.std::__2::__bit_iterator", align 4
  %agg.tmp4 = alloca %"class.std::__2::__bit_iterator", align 4
  %agg.tmp6 = alloca %"class.std::__2::__bit_iterator", align 4
  %agg.tmp8 = alloca %"class.std::__2::__bit_iterator", align 4
  %agg.tmp10 = alloca %"class.std::__2::__bit_iterator", align 4
  %0 = bitcast %"class.std::__2::__bit_iterator"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  %__ctz_ = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__first, i32 0, i32 1
  %1 = load i32, i32* %__ctz_, align 4
  %__ctz_1 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 1
  %2 = load i32, i32* %__ctz_1, align 4
  %cmp = icmp eq i32 %1, %2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call = call %"class.std::__2::__bit_iterator"* @_ZNSt3__214__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb0ELm0EEC2ERKS5_(%"class.std::__2::__bit_iterator"* %agg.tmp, %"class.std::__2::__bit_iterator"* nonnull align 4 dereferenceable(8) %__first) #8
  %call3 = call %"class.std::__2::__bit_iterator"* @_ZNSt3__214__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb0ELm0EEC2ERKS5_(%"class.std::__2::__bit_iterator"* %agg.tmp2, %"class.std::__2::__bit_iterator"* nonnull align 4 dereferenceable(8) %__last) #8
  %call5 = call %"class.std::__2::__bit_iterator"* @_ZNSt3__214__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb0ELm0EEC2ERKS5_(%"class.std::__2::__bit_iterator"* %agg.tmp4, %"class.std::__2::__bit_iterator"* nonnull align 4 dereferenceable(8) %__result) #8
  call void @_ZNSt3__214__copy_alignedINS_6vectorIbNS_9allocatorIbEEEELb0EEENS_14__bit_iteratorIT_Lb0EXLi0EEEENS5_IS6_XT0_EXLi0EEEES8_S7_(%"class.std::__2::__bit_iterator"* sret align 4 %agg.result, %"class.std::__2::__bit_iterator"* %agg.tmp, %"class.std::__2::__bit_iterator"* %agg.tmp2, %"class.std::__2::__bit_iterator"* %agg.tmp4)
  br label %return

if.end:                                           ; preds = %entry
  %call7 = call %"class.std::__2::__bit_iterator"* @_ZNSt3__214__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb0ELm0EEC2ERKS5_(%"class.std::__2::__bit_iterator"* %agg.tmp6, %"class.std::__2::__bit_iterator"* nonnull align 4 dereferenceable(8) %__first) #8
  %call9 = call %"class.std::__2::__bit_iterator"* @_ZNSt3__214__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb0ELm0EEC2ERKS5_(%"class.std::__2::__bit_iterator"* %agg.tmp8, %"class.std::__2::__bit_iterator"* nonnull align 4 dereferenceable(8) %__last) #8
  %call11 = call %"class.std::__2::__bit_iterator"* @_ZNSt3__214__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb0ELm0EEC2ERKS5_(%"class.std::__2::__bit_iterator"* %agg.tmp10, %"class.std::__2::__bit_iterator"* nonnull align 4 dereferenceable(8) %__result) #8
  call void @_ZNSt3__216__copy_unalignedINS_6vectorIbNS_9allocatorIbEEEELb0EEENS_14__bit_iteratorIT_Lb0EXLi0EEEENS5_IS6_XT0_EXLi0EEEES8_S7_(%"class.std::__2::__bit_iterator"* sret align 4 %agg.result, %"class.std::__2::__bit_iterator"* %agg.tmp6, %"class.std::__2::__bit_iterator"* %agg.tmp8, %"class.std::__2::__bit_iterator"* %agg.tmp10)
  br label %return

return:                                           ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__210__distanceINS_14__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb0ELm0EEEEENS_15iterator_traitsIT_E15difference_typeES8_S8_NS_26random_access_iterator_tagE(%"class.std::__2::__bit_iterator"* %__first, %"class.std::__2::__bit_iterator"* %__last) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::random_access_iterator_tag", align 1
  %call = call i32 @_ZNSt3__2miERKNS_14__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb0ELm0EEES7_(%"class.std::__2::__bit_iterator"* nonnull align 4 dereferenceable(8) %__last, %"class.std::__2::__bit_iterator"* nonnull align 4 dereferenceable(8) %__first)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__2miERKNS_14__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb0ELm0EEES7_(%"class.std::__2::__bit_iterator"* nonnull align 4 dereferenceable(8) %__x, %"class.std::__2::__bit_iterator"* nonnull align 4 dereferenceable(8) %__y) #0 comdat {
entry:
  %__x.addr = alloca %"class.std::__2::__bit_iterator"*, align 4
  %__y.addr = alloca %"class.std::__2::__bit_iterator"*, align 4
  store %"class.std::__2::__bit_iterator"* %__x, %"class.std::__2::__bit_iterator"** %__x.addr, align 4
  store %"class.std::__2::__bit_iterator"* %__y, %"class.std::__2::__bit_iterator"** %__y.addr, align 4
  %0 = load %"class.std::__2::__bit_iterator"*, %"class.std::__2::__bit_iterator"** %__x.addr, align 4
  %__seg_ = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %0, i32 0, i32 0
  %1 = load i32*, i32** %__seg_, align 4
  %2 = load %"class.std::__2::__bit_iterator"*, %"class.std::__2::__bit_iterator"** %__y.addr, align 4
  %__seg_1 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %2, i32 0, i32 0
  %3 = load i32*, i32** %__seg_1, align 4
  %sub.ptr.lhs.cast = ptrtoint i32* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint i32* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  %mul = mul i32 %sub.ptr.div, 32
  %4 = load %"class.std::__2::__bit_iterator"*, %"class.std::__2::__bit_iterator"** %__x.addr, align 4
  %__ctz_ = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %4, i32 0, i32 1
  %5 = load i32, i32* %__ctz_, align 4
  %add = add i32 %mul, %5
  %6 = load %"class.std::__2::__bit_iterator"*, %"class.std::__2::__bit_iterator"** %__y.addr, align 4
  %__ctz_2 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %6, i32 0, i32 1
  %7 = load i32, i32* %__ctz_2, align 4
  %sub = sub i32 %add, %7
  ret i32 %sub
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__copy_alignedINS_6vectorIbNS_9allocatorIbEEEELb0EEENS_14__bit_iteratorIT_Lb0EXLi0EEEENS5_IS6_XT0_EXLi0EEEES8_S7_(%"class.std::__2::__bit_iterator"* noalias sret align 4 %agg.result, %"class.std::__2::__bit_iterator"* %__first, %"class.std::__2::__bit_iterator"* %__last, %"class.std::__2::__bit_iterator"* %__result) #0 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %__bits_per_word = alloca i32, align 4
  %__n = alloca i32, align 4
  %__clz = alloca i32, align 4
  %__dn = alloca i32, align 4
  %ref.tmp = alloca i32, align 4
  %__m = alloca i32, align 4
  %__b = alloca i32, align 4
  %__nw = alloca i32, align 4
  %__m31 = alloca i32, align 4
  %__b34 = alloca i32, align 4
  %0 = bitcast %"class.std::__2::__bit_iterator"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store i32 32, i32* %__bits_per_word, align 4
  %call = call i32 @_ZNSt3__2miERKNS_14__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb0ELm0EEES7_(%"class.std::__2::__bit_iterator"* nonnull align 4 dereferenceable(8) %__last, %"class.std::__2::__bit_iterator"* nonnull align 4 dereferenceable(8) %__first)
  store i32 %call, i32* %__n, align 4
  %1 = load i32, i32* %__n, align 4
  %cmp = icmp sgt i32 %1, 0
  br i1 %cmp, label %if.then, label %if.end44

if.then:                                          ; preds = %entry
  %__ctz_ = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__first, i32 0, i32 1
  %2 = load i32, i32* %__ctz_, align 4
  %cmp1 = icmp ne i32 %2, 0
  br i1 %cmp1, label %if.then2, label %if.end

if.then2:                                         ; preds = %if.then
  %__ctz_3 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__first, i32 0, i32 1
  %3 = load i32, i32* %__ctz_3, align 4
  %sub = sub i32 32, %3
  store i32 %sub, i32* %__clz, align 4
  %4 = load i32, i32* %__clz, align 4
  store i32 %4, i32* %ref.tmp, align 4
  %call4 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minIlEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %__n)
  %5 = load i32, i32* %call4, align 4
  store i32 %5, i32* %__dn, align 4
  %6 = load i32, i32* %__dn, align 4
  %7 = load i32, i32* %__n, align 4
  %sub5 = sub nsw i32 %7, %6
  store i32 %sub5, i32* %__n, align 4
  %__ctz_6 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__first, i32 0, i32 1
  %8 = load i32, i32* %__ctz_6, align 4
  %shl = shl i32 -1, %8
  %9 = load i32, i32* %__clz, align 4
  %10 = load i32, i32* %__dn, align 4
  %sub7 = sub i32 %9, %10
  %shr = lshr i32 -1, %sub7
  %and = and i32 %shl, %shr
  store i32 %and, i32* %__m, align 4
  %__seg_ = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__first, i32 0, i32 0
  %11 = load i32*, i32** %__seg_, align 4
  %12 = load i32, i32* %11, align 4
  %13 = load i32, i32* %__m, align 4
  %and8 = and i32 %12, %13
  store i32 %and8, i32* %__b, align 4
  %14 = load i32, i32* %__m, align 4
  %neg = xor i32 %14, -1
  %__seg_9 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 0
  %15 = load i32*, i32** %__seg_9, align 4
  %16 = load i32, i32* %15, align 4
  %and10 = and i32 %16, %neg
  store i32 %and10, i32* %15, align 4
  %17 = load i32, i32* %__b, align 4
  %__seg_11 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 0
  %18 = load i32*, i32** %__seg_11, align 4
  %19 = load i32, i32* %18, align 4
  %or = or i32 %19, %17
  store i32 %or, i32* %18, align 4
  %20 = load i32, i32* %__dn, align 4
  %__ctz_12 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 1
  %21 = load i32, i32* %__ctz_12, align 4
  %add = add i32 %20, %21
  %div = udiv i32 %add, 32
  %__seg_13 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 0
  %22 = load i32*, i32** %__seg_13, align 4
  %add.ptr = getelementptr inbounds i32, i32* %22, i32 %div
  store i32* %add.ptr, i32** %__seg_13, align 4
  %23 = load i32, i32* %__dn, align 4
  %__ctz_14 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 1
  %24 = load i32, i32* %__ctz_14, align 4
  %add15 = add i32 %23, %24
  %rem = urem i32 %add15, 32
  %__ctz_16 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 1
  store i32 %rem, i32* %__ctz_16, align 4
  %__seg_17 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__first, i32 0, i32 0
  %25 = load i32*, i32** %__seg_17, align 4
  %incdec.ptr = getelementptr inbounds i32, i32* %25, i32 1
  store i32* %incdec.ptr, i32** %__seg_17, align 4
  br label %if.end

if.end:                                           ; preds = %if.then2, %if.then
  %26 = load i32, i32* %__n, align 4
  %div18 = sdiv i32 %26, 32
  store i32 %div18, i32* %__nw, align 4
  %__seg_19 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 0
  %27 = load i32*, i32** %__seg_19, align 4
  %call20 = call i32* @_ZNSt3__212__to_addressImEEPT_S2_(i32* %27) #8
  %28 = bitcast i32* %call20 to i8*
  %__seg_21 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__first, i32 0, i32 0
  %29 = load i32*, i32** %__seg_21, align 4
  %call22 = call i32* @_ZNSt3__212__to_addressImEEPT_S2_(i32* %29) #8
  %30 = bitcast i32* %call22 to i8*
  %31 = load i32, i32* %__nw, align 4
  %mul = mul i32 %31, 4
  call void @llvm.memmove.p0i8.p0i8.i32(i8* align 4 %28, i8* align 4 %30, i32 %mul, i1 false)
  %32 = load i32, i32* %__nw, align 4
  %mul23 = mul i32 %32, 32
  %33 = load i32, i32* %__n, align 4
  %sub24 = sub i32 %33, %mul23
  store i32 %sub24, i32* %__n, align 4
  %34 = load i32, i32* %__nw, align 4
  %__seg_25 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 0
  %35 = load i32*, i32** %__seg_25, align 4
  %add.ptr26 = getelementptr inbounds i32, i32* %35, i32 %34
  store i32* %add.ptr26, i32** %__seg_25, align 4
  %36 = load i32, i32* %__n, align 4
  %cmp27 = icmp sgt i32 %36, 0
  br i1 %cmp27, label %if.then28, label %if.end43

if.then28:                                        ; preds = %if.end
  %37 = load i32, i32* %__nw, align 4
  %__seg_29 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__first, i32 0, i32 0
  %38 = load i32*, i32** %__seg_29, align 4
  %add.ptr30 = getelementptr inbounds i32, i32* %38, i32 %37
  store i32* %add.ptr30, i32** %__seg_29, align 4
  %39 = load i32, i32* %__n, align 4
  %sub32 = sub nsw i32 32, %39
  %shr33 = lshr i32 -1, %sub32
  store i32 %shr33, i32* %__m31, align 4
  %__seg_35 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__first, i32 0, i32 0
  %40 = load i32*, i32** %__seg_35, align 4
  %41 = load i32, i32* %40, align 4
  %42 = load i32, i32* %__m31, align 4
  %and36 = and i32 %41, %42
  store i32 %and36, i32* %__b34, align 4
  %43 = load i32, i32* %__m31, align 4
  %neg37 = xor i32 %43, -1
  %__seg_38 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 0
  %44 = load i32*, i32** %__seg_38, align 4
  %45 = load i32, i32* %44, align 4
  %and39 = and i32 %45, %neg37
  store i32 %and39, i32* %44, align 4
  %46 = load i32, i32* %__b34, align 4
  %__seg_40 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 0
  %47 = load i32*, i32** %__seg_40, align 4
  %48 = load i32, i32* %47, align 4
  %or41 = or i32 %48, %46
  store i32 %or41, i32* %47, align 4
  %49 = load i32, i32* %__n, align 4
  %__ctz_42 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 1
  store i32 %49, i32* %__ctz_42, align 4
  br label %if.end43

if.end43:                                         ; preds = %if.then28, %if.end
  br label %if.end44

if.end44:                                         ; preds = %if.end43, %entry
  %call45 = call %"class.std::__2::__bit_iterator"* @_ZNSt3__214__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb0ELm0EEC2ERKS5_(%"class.std::__2::__bit_iterator"* %agg.result, %"class.std::__2::__bit_iterator"* nonnull align 4 dereferenceable(8) %__result) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216__copy_unalignedINS_6vectorIbNS_9allocatorIbEEEELb0EEENS_14__bit_iteratorIT_Lb0EXLi0EEEENS5_IS6_XT0_EXLi0EEEES8_S7_(%"class.std::__2::__bit_iterator"* noalias sret align 4 %agg.result, %"class.std::__2::__bit_iterator"* %__first, %"class.std::__2::__bit_iterator"* %__last, %"class.std::__2::__bit_iterator"* %__result) #0 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %__n = alloca i32, align 4
  %__clz_f = alloca i32, align 4
  %__dn = alloca i32, align 4
  %ref.tmp = alloca i32, align 4
  %__m = alloca i32, align 4
  %__b = alloca i32, align 4
  %__clz_r = alloca i32, align 4
  %__ddn = alloca i32, align 4
  %ref.tmp11 = alloca i32, align 4
  %ref.tmp12 = alloca i32, align 4
  %__clz_r58 = alloca i32, align 4
  %__m61 = alloca i32, align 4
  %__b65 = alloca i32, align 4
  %__b88 = alloca i32, align 4
  %__dn91 = alloca i32, align 4
  %ref.tmp92 = alloca i32, align 4
  %0 = bitcast %"class.std::__2::__bit_iterator"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  %call = call i32 @_ZNSt3__2miERKNS_14__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb0ELm0EEES7_(%"class.std::__2::__bit_iterator"* nonnull align 4 dereferenceable(8) %__last, %"class.std::__2::__bit_iterator"* nonnull align 4 dereferenceable(8) %__first)
  store i32 %call, i32* %__n, align 4
  %1 = load i32, i32* %__n, align 4
  %cmp = icmp sgt i32 %1, 0
  br i1 %cmp, label %if.then, label %if.end129

if.then:                                          ; preds = %entry
  %__ctz_ = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__first, i32 0, i32 1
  %2 = load i32, i32* %__ctz_, align 4
  %cmp1 = icmp ne i32 %2, 0
  br i1 %cmp1, label %if.then2, label %if.end57

if.then2:                                         ; preds = %if.then
  %__ctz_3 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__first, i32 0, i32 1
  %3 = load i32, i32* %__ctz_3, align 4
  %sub = sub i32 32, %3
  store i32 %sub, i32* %__clz_f, align 4
  %4 = load i32, i32* %__clz_f, align 4
  store i32 %4, i32* %ref.tmp, align 4
  %call4 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minIlEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %__n)
  %5 = load i32, i32* %call4, align 4
  store i32 %5, i32* %__dn, align 4
  %6 = load i32, i32* %__dn, align 4
  %7 = load i32, i32* %__n, align 4
  %sub5 = sub nsw i32 %7, %6
  store i32 %sub5, i32* %__n, align 4
  %__ctz_6 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__first, i32 0, i32 1
  %8 = load i32, i32* %__ctz_6, align 4
  %shl = shl i32 -1, %8
  %9 = load i32, i32* %__clz_f, align 4
  %10 = load i32, i32* %__dn, align 4
  %sub7 = sub i32 %9, %10
  %shr = lshr i32 -1, %sub7
  %and = and i32 %shl, %shr
  store i32 %and, i32* %__m, align 4
  %__seg_ = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__first, i32 0, i32 0
  %11 = load i32*, i32** %__seg_, align 4
  %12 = load i32, i32* %11, align 4
  %13 = load i32, i32* %__m, align 4
  %and8 = and i32 %12, %13
  store i32 %and8, i32* %__b, align 4
  %__ctz_9 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 1
  %14 = load i32, i32* %__ctz_9, align 4
  %sub10 = sub i32 32, %14
  store i32 %sub10, i32* %__clz_r, align 4
  %15 = load i32, i32* %__dn, align 4
  store i32 %15, i32* %ref.tmp11, align 4
  %16 = load i32, i32* %__clz_r, align 4
  store i32 %16, i32* %ref.tmp12, align 4
  %call13 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp11, i32* nonnull align 4 dereferenceable(4) %ref.tmp12)
  %17 = load i32, i32* %call13, align 4
  store i32 %17, i32* %__ddn, align 4
  %__ctz_14 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 1
  %18 = load i32, i32* %__ctz_14, align 4
  %shl15 = shl i32 -1, %18
  %19 = load i32, i32* %__clz_r, align 4
  %20 = load i32, i32* %__ddn, align 4
  %sub16 = sub i32 %19, %20
  %shr17 = lshr i32 -1, %sub16
  %and18 = and i32 %shl15, %shr17
  store i32 %and18, i32* %__m, align 4
  %21 = load i32, i32* %__m, align 4
  %neg = xor i32 %21, -1
  %__seg_19 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 0
  %22 = load i32*, i32** %__seg_19, align 4
  %23 = load i32, i32* %22, align 4
  %and20 = and i32 %23, %neg
  store i32 %and20, i32* %22, align 4
  %__ctz_21 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 1
  %24 = load i32, i32* %__ctz_21, align 4
  %__ctz_22 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__first, i32 0, i32 1
  %25 = load i32, i32* %__ctz_22, align 4
  %cmp23 = icmp ugt i32 %24, %25
  br i1 %cmp23, label %if.then24, label %if.else

if.then24:                                        ; preds = %if.then2
  %26 = load i32, i32* %__b, align 4
  %__ctz_25 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 1
  %27 = load i32, i32* %__ctz_25, align 4
  %__ctz_26 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__first, i32 0, i32 1
  %28 = load i32, i32* %__ctz_26, align 4
  %sub27 = sub i32 %27, %28
  %shl28 = shl i32 %26, %sub27
  %__seg_29 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 0
  %29 = load i32*, i32** %__seg_29, align 4
  %30 = load i32, i32* %29, align 4
  %or = or i32 %30, %shl28
  store i32 %or, i32* %29, align 4
  br label %if.end

if.else:                                          ; preds = %if.then2
  %31 = load i32, i32* %__b, align 4
  %__ctz_30 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__first, i32 0, i32 1
  %32 = load i32, i32* %__ctz_30, align 4
  %__ctz_31 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 1
  %33 = load i32, i32* %__ctz_31, align 4
  %sub32 = sub i32 %32, %33
  %shr33 = lshr i32 %31, %sub32
  %__seg_34 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 0
  %34 = load i32*, i32** %__seg_34, align 4
  %35 = load i32, i32* %34, align 4
  %or35 = or i32 %35, %shr33
  store i32 %or35, i32* %34, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then24
  %36 = load i32, i32* %__ddn, align 4
  %__ctz_36 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 1
  %37 = load i32, i32* %__ctz_36, align 4
  %add = add i32 %36, %37
  %div = udiv i32 %add, 32
  %__seg_37 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 0
  %38 = load i32*, i32** %__seg_37, align 4
  %add.ptr = getelementptr inbounds i32, i32* %38, i32 %div
  store i32* %add.ptr, i32** %__seg_37, align 4
  %39 = load i32, i32* %__ddn, align 4
  %__ctz_38 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 1
  %40 = load i32, i32* %__ctz_38, align 4
  %add39 = add i32 %39, %40
  %rem = urem i32 %add39, 32
  %__ctz_40 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 1
  store i32 %rem, i32* %__ctz_40, align 4
  %41 = load i32, i32* %__ddn, align 4
  %42 = load i32, i32* %__dn, align 4
  %sub41 = sub i32 %42, %41
  store i32 %sub41, i32* %__dn, align 4
  %43 = load i32, i32* %__dn, align 4
  %cmp42 = icmp sgt i32 %43, 0
  br i1 %cmp42, label %if.then43, label %if.end55

if.then43:                                        ; preds = %if.end
  %44 = load i32, i32* %__dn, align 4
  %sub44 = sub nsw i32 32, %44
  %shr45 = lshr i32 -1, %sub44
  store i32 %shr45, i32* %__m, align 4
  %45 = load i32, i32* %__m, align 4
  %neg46 = xor i32 %45, -1
  %__seg_47 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 0
  %46 = load i32*, i32** %__seg_47, align 4
  %47 = load i32, i32* %46, align 4
  %and48 = and i32 %47, %neg46
  store i32 %and48, i32* %46, align 4
  %48 = load i32, i32* %__b, align 4
  %__ctz_49 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__first, i32 0, i32 1
  %49 = load i32, i32* %__ctz_49, align 4
  %50 = load i32, i32* %__ddn, align 4
  %add50 = add i32 %49, %50
  %shr51 = lshr i32 %48, %add50
  %__seg_52 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 0
  %51 = load i32*, i32** %__seg_52, align 4
  %52 = load i32, i32* %51, align 4
  %or53 = or i32 %52, %shr51
  store i32 %or53, i32* %51, align 4
  %53 = load i32, i32* %__dn, align 4
  %__ctz_54 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 1
  store i32 %53, i32* %__ctz_54, align 4
  br label %if.end55

if.end55:                                         ; preds = %if.then43, %if.end
  %__seg_56 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__first, i32 0, i32 0
  %54 = load i32*, i32** %__seg_56, align 4
  %incdec.ptr = getelementptr inbounds i32, i32* %54, i32 1
  store i32* %incdec.ptr, i32** %__seg_56, align 4
  br label %if.end57

if.end57:                                         ; preds = %if.end55, %if.then
  %__ctz_59 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 1
  %55 = load i32, i32* %__ctz_59, align 4
  %sub60 = sub i32 32, %55
  store i32 %sub60, i32* %__clz_r58, align 4
  %__ctz_62 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 1
  %56 = load i32, i32* %__ctz_62, align 4
  %shl63 = shl i32 -1, %56
  store i32 %shl63, i32* %__m61, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end57
  %57 = load i32, i32* %__n, align 4
  %cmp64 = icmp sge i32 %57, 32
  br i1 %cmp64, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %__seg_66 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__first, i32 0, i32 0
  %58 = load i32*, i32** %__seg_66, align 4
  %59 = load i32, i32* %58, align 4
  store i32 %59, i32* %__b65, align 4
  %60 = load i32, i32* %__m61, align 4
  %neg67 = xor i32 %60, -1
  %__seg_68 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 0
  %61 = load i32*, i32** %__seg_68, align 4
  %62 = load i32, i32* %61, align 4
  %and69 = and i32 %62, %neg67
  store i32 %and69, i32* %61, align 4
  %63 = load i32, i32* %__b65, align 4
  %__ctz_70 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 1
  %64 = load i32, i32* %__ctz_70, align 4
  %shl71 = shl i32 %63, %64
  %__seg_72 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 0
  %65 = load i32*, i32** %__seg_72, align 4
  %66 = load i32, i32* %65, align 4
  %or73 = or i32 %66, %shl71
  store i32 %or73, i32* %65, align 4
  %__seg_74 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 0
  %67 = load i32*, i32** %__seg_74, align 4
  %incdec.ptr75 = getelementptr inbounds i32, i32* %67, i32 1
  store i32* %incdec.ptr75, i32** %__seg_74, align 4
  %68 = load i32, i32* %__m61, align 4
  %__seg_76 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 0
  %69 = load i32*, i32** %__seg_76, align 4
  %70 = load i32, i32* %69, align 4
  %and77 = and i32 %70, %68
  store i32 %and77, i32* %69, align 4
  %71 = load i32, i32* %__b65, align 4
  %72 = load i32, i32* %__clz_r58, align 4
  %shr78 = lshr i32 %71, %72
  %__seg_79 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 0
  %73 = load i32*, i32** %__seg_79, align 4
  %74 = load i32, i32* %73, align 4
  %or80 = or i32 %74, %shr78
  store i32 %or80, i32* %73, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %75 = load i32, i32* %__n, align 4
  %sub81 = sub nsw i32 %75, 32
  store i32 %sub81, i32* %__n, align 4
  %__seg_82 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__first, i32 0, i32 0
  %76 = load i32*, i32** %__seg_82, align 4
  %incdec.ptr83 = getelementptr inbounds i32, i32* %76, i32 1
  store i32* %incdec.ptr83, i32** %__seg_82, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %77 = load i32, i32* %__n, align 4
  %cmp84 = icmp sgt i32 %77, 0
  br i1 %cmp84, label %if.then85, label %if.end128

if.then85:                                        ; preds = %for.end
  %78 = load i32, i32* %__n, align 4
  %sub86 = sub nsw i32 32, %78
  %shr87 = lshr i32 -1, %sub86
  store i32 %shr87, i32* %__m61, align 4
  %__seg_89 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__first, i32 0, i32 0
  %79 = load i32*, i32** %__seg_89, align 4
  %80 = load i32, i32* %79, align 4
  %81 = load i32, i32* %__m61, align 4
  %and90 = and i32 %80, %81
  store i32 %and90, i32* %__b88, align 4
  %82 = load i32, i32* %__clz_r58, align 4
  store i32 %82, i32* %ref.tmp92, align 4
  %call93 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minIlEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %__n, i32* nonnull align 4 dereferenceable(4) %ref.tmp92)
  %83 = load i32, i32* %call93, align 4
  store i32 %83, i32* %__dn91, align 4
  %__ctz_94 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 1
  %84 = load i32, i32* %__ctz_94, align 4
  %shl95 = shl i32 -1, %84
  %85 = load i32, i32* %__clz_r58, align 4
  %86 = load i32, i32* %__dn91, align 4
  %sub96 = sub i32 %85, %86
  %shr97 = lshr i32 -1, %sub96
  %and98 = and i32 %shl95, %shr97
  store i32 %and98, i32* %__m61, align 4
  %87 = load i32, i32* %__m61, align 4
  %neg99 = xor i32 %87, -1
  %__seg_100 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 0
  %88 = load i32*, i32** %__seg_100, align 4
  %89 = load i32, i32* %88, align 4
  %and101 = and i32 %89, %neg99
  store i32 %and101, i32* %88, align 4
  %90 = load i32, i32* %__b88, align 4
  %__ctz_102 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 1
  %91 = load i32, i32* %__ctz_102, align 4
  %shl103 = shl i32 %90, %91
  %__seg_104 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 0
  %92 = load i32*, i32** %__seg_104, align 4
  %93 = load i32, i32* %92, align 4
  %or105 = or i32 %93, %shl103
  store i32 %or105, i32* %92, align 4
  %94 = load i32, i32* %__dn91, align 4
  %__ctz_106 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 1
  %95 = load i32, i32* %__ctz_106, align 4
  %add107 = add i32 %94, %95
  %div108 = udiv i32 %add107, 32
  %__seg_109 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 0
  %96 = load i32*, i32** %__seg_109, align 4
  %add.ptr110 = getelementptr inbounds i32, i32* %96, i32 %div108
  store i32* %add.ptr110, i32** %__seg_109, align 4
  %97 = load i32, i32* %__dn91, align 4
  %__ctz_111 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 1
  %98 = load i32, i32* %__ctz_111, align 4
  %add112 = add i32 %97, %98
  %rem113 = urem i32 %add112, 32
  %__ctz_114 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 1
  store i32 %rem113, i32* %__ctz_114, align 4
  %99 = load i32, i32* %__dn91, align 4
  %100 = load i32, i32* %__n, align 4
  %sub115 = sub i32 %100, %99
  store i32 %sub115, i32* %__n, align 4
  %101 = load i32, i32* %__n, align 4
  %cmp116 = icmp sgt i32 %101, 0
  br i1 %cmp116, label %if.then117, label %if.end127

if.then117:                                       ; preds = %if.then85
  %102 = load i32, i32* %__n, align 4
  %sub118 = sub nsw i32 32, %102
  %shr119 = lshr i32 -1, %sub118
  store i32 %shr119, i32* %__m61, align 4
  %103 = load i32, i32* %__m61, align 4
  %neg120 = xor i32 %103, -1
  %__seg_121 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 0
  %104 = load i32*, i32** %__seg_121, align 4
  %105 = load i32, i32* %104, align 4
  %and122 = and i32 %105, %neg120
  store i32 %and122, i32* %104, align 4
  %106 = load i32, i32* %__b88, align 4
  %107 = load i32, i32* %__dn91, align 4
  %shr123 = lshr i32 %106, %107
  %__seg_124 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 0
  %108 = load i32*, i32** %__seg_124, align 4
  %109 = load i32, i32* %108, align 4
  %or125 = or i32 %109, %shr123
  store i32 %or125, i32* %108, align 4
  %110 = load i32, i32* %__n, align 4
  %__ctz_126 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 1
  store i32 %110, i32* %__ctz_126, align 4
  br label %if.end127

if.end127:                                        ; preds = %if.then117, %if.then85
  br label %if.end128

if.end128:                                        ; preds = %if.end127, %for.end
  br label %if.end129

if.end129:                                        ; preds = %if.end128, %entry
  %call130 = call %"class.std::__2::__bit_iterator"* @_ZNSt3__214__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb0ELm0EEC2ERKS5_(%"class.std::__2::__bit_iterator"* %agg.result, %"class.std::__2::__bit_iterator"* nonnull align 4 dereferenceable(8) %__result) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minIlEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::__less.142", align 1
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__a.addr, align 4
  %1 = load i32*, i32** %__b.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minIlNS_6__lessIllEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNSt3__212__to_addressImEEPT_S2_(i32* %__p) #0 comdat {
entry:
  %__p.addr = alloca i32*, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %0 = load i32*, i32** %__p.addr, align 4
  ret i32* %0
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memmove.p0i8.p0i8.i32(i8* nocapture, i8* nocapture readonly, i32, i1 immarg) #2

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minIlNS_6__lessIllEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__comp = alloca %"struct.std::__2::__less.142", align 1
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__b.addr, align 4
  %1 = load i32*, i32** %__a.addr, align 4
  %call = call zeroext i1 @_ZNKSt3__26__lessIllEclERKlS3_(%"struct.std::__2::__less.142"* %__comp, i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  br i1 %call, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32*, i32** %__b.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %3 = load i32*, i32** %__a.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi i32* [ %2, %cond.true ], [ %3, %cond.false ]
  ret i32* %cond-lvalue
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__26__lessIllEclERKlS3_(%"struct.std::__2::__less.142"* %this, i32* nonnull align 4 dereferenceable(4) %__x, i32* nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__less.142"*, align 4
  %__x.addr = alloca i32*, align 4
  %__y.addr = alloca i32*, align 4
  store %"struct.std::__2::__less.142"* %this, %"struct.std::__2::__less.142"** %this.addr, align 4
  store i32* %__x, i32** %__x.addr, align 4
  store i32* %__y, i32** %__y.addr, align 4
  %this1 = load %"struct.std::__2::__less.142"*, %"struct.std::__2::__less.142"** %this.addr, align 4
  %0 = load i32*, i32** %__x.addr, align 4
  %1 = load i32, i32* %0, align 4
  %2 = load i32*, i32** %__y.addr, align 4
  %3 = load i32, i32* %2, align 4
  %cmp = icmp slt i32 %1, %3
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__26vectorIbNS_9allocatorIbEEE10__align_itEm(i32 %__new_size) #0 comdat {
entry:
  %__new_size.addr = alloca i32, align 4
  store i32 %__new_size, i32* %__new_size.addr, align 4
  %0 = load i32, i32* %__new_size.addr, align 4
  %add = add i32 %0, 31
  %and = and i32 %add, -32
  ret i32 %and
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__copy_alignedINS_6vectorIbNS_9allocatorIbEEEELb1EEENS_14__bit_iteratorIT_Lb0EXLi0EEEENS5_IS6_XT0_EXLi0EEEES8_S7_(%"class.std::__2::__bit_iterator"* noalias sret align 4 %agg.result, %"class.std::__2::__bit_iterator.140"* byval(%"class.std::__2::__bit_iterator.140") align 4 %__first, %"class.std::__2::__bit_iterator.140"* byval(%"class.std::__2::__bit_iterator.140") align 4 %__last, %"class.std::__2::__bit_iterator"* %__result) #0 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %__bits_per_word = alloca i32, align 4
  %__n = alloca i32, align 4
  %__clz = alloca i32, align 4
  %__dn = alloca i32, align 4
  %ref.tmp = alloca i32, align 4
  %__m = alloca i32, align 4
  %__b = alloca i32, align 4
  %__nw = alloca i32, align 4
  %__m31 = alloca i32, align 4
  %__b34 = alloca i32, align 4
  %0 = bitcast %"class.std::__2::__bit_iterator"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store i32 32, i32* %__bits_per_word, align 4
  %call = call i32 @_ZNSt3__2miERKNS_14__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb1ELm0EEES7_(%"class.std::__2::__bit_iterator.140"* nonnull align 4 dereferenceable(8) %__last, %"class.std::__2::__bit_iterator.140"* nonnull align 4 dereferenceable(8) %__first)
  store i32 %call, i32* %__n, align 4
  %1 = load i32, i32* %__n, align 4
  %cmp = icmp sgt i32 %1, 0
  br i1 %cmp, label %if.then, label %if.end44

if.then:                                          ; preds = %entry
  %__ctz_ = getelementptr inbounds %"class.std::__2::__bit_iterator.140", %"class.std::__2::__bit_iterator.140"* %__first, i32 0, i32 1
  %2 = load i32, i32* %__ctz_, align 4
  %cmp1 = icmp ne i32 %2, 0
  br i1 %cmp1, label %if.then2, label %if.end

if.then2:                                         ; preds = %if.then
  %__ctz_3 = getelementptr inbounds %"class.std::__2::__bit_iterator.140", %"class.std::__2::__bit_iterator.140"* %__first, i32 0, i32 1
  %3 = load i32, i32* %__ctz_3, align 4
  %sub = sub i32 32, %3
  store i32 %sub, i32* %__clz, align 4
  %4 = load i32, i32* %__clz, align 4
  store i32 %4, i32* %ref.tmp, align 4
  %call4 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minIlEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %__n)
  %5 = load i32, i32* %call4, align 4
  store i32 %5, i32* %__dn, align 4
  %6 = load i32, i32* %__dn, align 4
  %7 = load i32, i32* %__n, align 4
  %sub5 = sub nsw i32 %7, %6
  store i32 %sub5, i32* %__n, align 4
  %__ctz_6 = getelementptr inbounds %"class.std::__2::__bit_iterator.140", %"class.std::__2::__bit_iterator.140"* %__first, i32 0, i32 1
  %8 = load i32, i32* %__ctz_6, align 4
  %shl = shl i32 -1, %8
  %9 = load i32, i32* %__clz, align 4
  %10 = load i32, i32* %__dn, align 4
  %sub7 = sub i32 %9, %10
  %shr = lshr i32 -1, %sub7
  %and = and i32 %shl, %shr
  store i32 %and, i32* %__m, align 4
  %__seg_ = getelementptr inbounds %"class.std::__2::__bit_iterator.140", %"class.std::__2::__bit_iterator.140"* %__first, i32 0, i32 0
  %11 = load i32*, i32** %__seg_, align 4
  %12 = load i32, i32* %11, align 4
  %13 = load i32, i32* %__m, align 4
  %and8 = and i32 %12, %13
  store i32 %and8, i32* %__b, align 4
  %14 = load i32, i32* %__m, align 4
  %neg = xor i32 %14, -1
  %__seg_9 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 0
  %15 = load i32*, i32** %__seg_9, align 4
  %16 = load i32, i32* %15, align 4
  %and10 = and i32 %16, %neg
  store i32 %and10, i32* %15, align 4
  %17 = load i32, i32* %__b, align 4
  %__seg_11 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 0
  %18 = load i32*, i32** %__seg_11, align 4
  %19 = load i32, i32* %18, align 4
  %or = or i32 %19, %17
  store i32 %or, i32* %18, align 4
  %20 = load i32, i32* %__dn, align 4
  %__ctz_12 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 1
  %21 = load i32, i32* %__ctz_12, align 4
  %add = add i32 %20, %21
  %div = udiv i32 %add, 32
  %__seg_13 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 0
  %22 = load i32*, i32** %__seg_13, align 4
  %add.ptr = getelementptr inbounds i32, i32* %22, i32 %div
  store i32* %add.ptr, i32** %__seg_13, align 4
  %23 = load i32, i32* %__dn, align 4
  %__ctz_14 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 1
  %24 = load i32, i32* %__ctz_14, align 4
  %add15 = add i32 %23, %24
  %rem = urem i32 %add15, 32
  %__ctz_16 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 1
  store i32 %rem, i32* %__ctz_16, align 4
  %__seg_17 = getelementptr inbounds %"class.std::__2::__bit_iterator.140", %"class.std::__2::__bit_iterator.140"* %__first, i32 0, i32 0
  %25 = load i32*, i32** %__seg_17, align 4
  %incdec.ptr = getelementptr inbounds i32, i32* %25, i32 1
  store i32* %incdec.ptr, i32** %__seg_17, align 4
  br label %if.end

if.end:                                           ; preds = %if.then2, %if.then
  %26 = load i32, i32* %__n, align 4
  %div18 = sdiv i32 %26, 32
  store i32 %div18, i32* %__nw, align 4
  %__seg_19 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 0
  %27 = load i32*, i32** %__seg_19, align 4
  %call20 = call i32* @_ZNSt3__212__to_addressImEEPT_S2_(i32* %27) #8
  %28 = bitcast i32* %call20 to i8*
  %__seg_21 = getelementptr inbounds %"class.std::__2::__bit_iterator.140", %"class.std::__2::__bit_iterator.140"* %__first, i32 0, i32 0
  %29 = load i32*, i32** %__seg_21, align 4
  %call22 = call i32* @_ZNSt3__212__to_addressIKmEEPT_S3_(i32* %29) #8
  %30 = bitcast i32* %call22 to i8*
  %31 = load i32, i32* %__nw, align 4
  %mul = mul i32 %31, 4
  call void @llvm.memmove.p0i8.p0i8.i32(i8* align 4 %28, i8* align 4 %30, i32 %mul, i1 false)
  %32 = load i32, i32* %__nw, align 4
  %mul23 = mul i32 %32, 32
  %33 = load i32, i32* %__n, align 4
  %sub24 = sub i32 %33, %mul23
  store i32 %sub24, i32* %__n, align 4
  %34 = load i32, i32* %__nw, align 4
  %__seg_25 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 0
  %35 = load i32*, i32** %__seg_25, align 4
  %add.ptr26 = getelementptr inbounds i32, i32* %35, i32 %34
  store i32* %add.ptr26, i32** %__seg_25, align 4
  %36 = load i32, i32* %__n, align 4
  %cmp27 = icmp sgt i32 %36, 0
  br i1 %cmp27, label %if.then28, label %if.end43

if.then28:                                        ; preds = %if.end
  %37 = load i32, i32* %__nw, align 4
  %__seg_29 = getelementptr inbounds %"class.std::__2::__bit_iterator.140", %"class.std::__2::__bit_iterator.140"* %__first, i32 0, i32 0
  %38 = load i32*, i32** %__seg_29, align 4
  %add.ptr30 = getelementptr inbounds i32, i32* %38, i32 %37
  store i32* %add.ptr30, i32** %__seg_29, align 4
  %39 = load i32, i32* %__n, align 4
  %sub32 = sub nsw i32 32, %39
  %shr33 = lshr i32 -1, %sub32
  store i32 %shr33, i32* %__m31, align 4
  %__seg_35 = getelementptr inbounds %"class.std::__2::__bit_iterator.140", %"class.std::__2::__bit_iterator.140"* %__first, i32 0, i32 0
  %40 = load i32*, i32** %__seg_35, align 4
  %41 = load i32, i32* %40, align 4
  %42 = load i32, i32* %__m31, align 4
  %and36 = and i32 %41, %42
  store i32 %and36, i32* %__b34, align 4
  %43 = load i32, i32* %__m31, align 4
  %neg37 = xor i32 %43, -1
  %__seg_38 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 0
  %44 = load i32*, i32** %__seg_38, align 4
  %45 = load i32, i32* %44, align 4
  %and39 = and i32 %45, %neg37
  store i32 %and39, i32* %44, align 4
  %46 = load i32, i32* %__b34, align 4
  %__seg_40 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 0
  %47 = load i32*, i32** %__seg_40, align 4
  %48 = load i32, i32* %47, align 4
  %or41 = or i32 %48, %46
  store i32 %or41, i32* %47, align 4
  %49 = load i32, i32* %__n, align 4
  %__ctz_42 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 1
  store i32 %49, i32* %__ctz_42, align 4
  br label %if.end43

if.end43:                                         ; preds = %if.then28, %if.end
  br label %if.end44

if.end44:                                         ; preds = %if.end43, %entry
  %call45 = call %"class.std::__2::__bit_iterator"* @_ZNSt3__214__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb0ELm0EEC2ERKS5_(%"class.std::__2::__bit_iterator"* %agg.result, %"class.std::__2::__bit_iterator"* nonnull align 4 dereferenceable(8) %__result) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216__copy_unalignedINS_6vectorIbNS_9allocatorIbEEEELb1EEENS_14__bit_iteratorIT_Lb0EXLi0EEEENS5_IS6_XT0_EXLi0EEEES8_S7_(%"class.std::__2::__bit_iterator"* noalias sret align 4 %agg.result, %"class.std::__2::__bit_iterator.140"* byval(%"class.std::__2::__bit_iterator.140") align 4 %__first, %"class.std::__2::__bit_iterator.140"* byval(%"class.std::__2::__bit_iterator.140") align 4 %__last, %"class.std::__2::__bit_iterator"* %__result) #0 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %__n = alloca i32, align 4
  %__clz_f = alloca i32, align 4
  %__dn = alloca i32, align 4
  %ref.tmp = alloca i32, align 4
  %__m = alloca i32, align 4
  %__b = alloca i32, align 4
  %__clz_r = alloca i32, align 4
  %__ddn = alloca i32, align 4
  %ref.tmp11 = alloca i32, align 4
  %ref.tmp12 = alloca i32, align 4
  %__clz_r58 = alloca i32, align 4
  %__m61 = alloca i32, align 4
  %__b65 = alloca i32, align 4
  %__b88 = alloca i32, align 4
  %__dn91 = alloca i32, align 4
  %ref.tmp92 = alloca i32, align 4
  %0 = bitcast %"class.std::__2::__bit_iterator"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  %call = call i32 @_ZNSt3__2miERKNS_14__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb1ELm0EEES7_(%"class.std::__2::__bit_iterator.140"* nonnull align 4 dereferenceable(8) %__last, %"class.std::__2::__bit_iterator.140"* nonnull align 4 dereferenceable(8) %__first)
  store i32 %call, i32* %__n, align 4
  %1 = load i32, i32* %__n, align 4
  %cmp = icmp sgt i32 %1, 0
  br i1 %cmp, label %if.then, label %if.end129

if.then:                                          ; preds = %entry
  %__ctz_ = getelementptr inbounds %"class.std::__2::__bit_iterator.140", %"class.std::__2::__bit_iterator.140"* %__first, i32 0, i32 1
  %2 = load i32, i32* %__ctz_, align 4
  %cmp1 = icmp ne i32 %2, 0
  br i1 %cmp1, label %if.then2, label %if.end57

if.then2:                                         ; preds = %if.then
  %__ctz_3 = getelementptr inbounds %"class.std::__2::__bit_iterator.140", %"class.std::__2::__bit_iterator.140"* %__first, i32 0, i32 1
  %3 = load i32, i32* %__ctz_3, align 4
  %sub = sub i32 32, %3
  store i32 %sub, i32* %__clz_f, align 4
  %4 = load i32, i32* %__clz_f, align 4
  store i32 %4, i32* %ref.tmp, align 4
  %call4 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minIlEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %__n)
  %5 = load i32, i32* %call4, align 4
  store i32 %5, i32* %__dn, align 4
  %6 = load i32, i32* %__dn, align 4
  %7 = load i32, i32* %__n, align 4
  %sub5 = sub nsw i32 %7, %6
  store i32 %sub5, i32* %__n, align 4
  %__ctz_6 = getelementptr inbounds %"class.std::__2::__bit_iterator.140", %"class.std::__2::__bit_iterator.140"* %__first, i32 0, i32 1
  %8 = load i32, i32* %__ctz_6, align 4
  %shl = shl i32 -1, %8
  %9 = load i32, i32* %__clz_f, align 4
  %10 = load i32, i32* %__dn, align 4
  %sub7 = sub i32 %9, %10
  %shr = lshr i32 -1, %sub7
  %and = and i32 %shl, %shr
  store i32 %and, i32* %__m, align 4
  %__seg_ = getelementptr inbounds %"class.std::__2::__bit_iterator.140", %"class.std::__2::__bit_iterator.140"* %__first, i32 0, i32 0
  %11 = load i32*, i32** %__seg_, align 4
  %12 = load i32, i32* %11, align 4
  %13 = load i32, i32* %__m, align 4
  %and8 = and i32 %12, %13
  store i32 %and8, i32* %__b, align 4
  %__ctz_9 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 1
  %14 = load i32, i32* %__ctz_9, align 4
  %sub10 = sub i32 32, %14
  store i32 %sub10, i32* %__clz_r, align 4
  %15 = load i32, i32* %__dn, align 4
  store i32 %15, i32* %ref.tmp11, align 4
  %16 = load i32, i32* %__clz_r, align 4
  store i32 %16, i32* %ref.tmp12, align 4
  %call13 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp11, i32* nonnull align 4 dereferenceable(4) %ref.tmp12)
  %17 = load i32, i32* %call13, align 4
  store i32 %17, i32* %__ddn, align 4
  %__ctz_14 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 1
  %18 = load i32, i32* %__ctz_14, align 4
  %shl15 = shl i32 -1, %18
  %19 = load i32, i32* %__clz_r, align 4
  %20 = load i32, i32* %__ddn, align 4
  %sub16 = sub i32 %19, %20
  %shr17 = lshr i32 -1, %sub16
  %and18 = and i32 %shl15, %shr17
  store i32 %and18, i32* %__m, align 4
  %21 = load i32, i32* %__m, align 4
  %neg = xor i32 %21, -1
  %__seg_19 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 0
  %22 = load i32*, i32** %__seg_19, align 4
  %23 = load i32, i32* %22, align 4
  %and20 = and i32 %23, %neg
  store i32 %and20, i32* %22, align 4
  %__ctz_21 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 1
  %24 = load i32, i32* %__ctz_21, align 4
  %__ctz_22 = getelementptr inbounds %"class.std::__2::__bit_iterator.140", %"class.std::__2::__bit_iterator.140"* %__first, i32 0, i32 1
  %25 = load i32, i32* %__ctz_22, align 4
  %cmp23 = icmp ugt i32 %24, %25
  br i1 %cmp23, label %if.then24, label %if.else

if.then24:                                        ; preds = %if.then2
  %26 = load i32, i32* %__b, align 4
  %__ctz_25 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 1
  %27 = load i32, i32* %__ctz_25, align 4
  %__ctz_26 = getelementptr inbounds %"class.std::__2::__bit_iterator.140", %"class.std::__2::__bit_iterator.140"* %__first, i32 0, i32 1
  %28 = load i32, i32* %__ctz_26, align 4
  %sub27 = sub i32 %27, %28
  %shl28 = shl i32 %26, %sub27
  %__seg_29 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 0
  %29 = load i32*, i32** %__seg_29, align 4
  %30 = load i32, i32* %29, align 4
  %or = or i32 %30, %shl28
  store i32 %or, i32* %29, align 4
  br label %if.end

if.else:                                          ; preds = %if.then2
  %31 = load i32, i32* %__b, align 4
  %__ctz_30 = getelementptr inbounds %"class.std::__2::__bit_iterator.140", %"class.std::__2::__bit_iterator.140"* %__first, i32 0, i32 1
  %32 = load i32, i32* %__ctz_30, align 4
  %__ctz_31 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 1
  %33 = load i32, i32* %__ctz_31, align 4
  %sub32 = sub i32 %32, %33
  %shr33 = lshr i32 %31, %sub32
  %__seg_34 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 0
  %34 = load i32*, i32** %__seg_34, align 4
  %35 = load i32, i32* %34, align 4
  %or35 = or i32 %35, %shr33
  store i32 %or35, i32* %34, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then24
  %36 = load i32, i32* %__ddn, align 4
  %__ctz_36 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 1
  %37 = load i32, i32* %__ctz_36, align 4
  %add = add i32 %36, %37
  %div = udiv i32 %add, 32
  %__seg_37 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 0
  %38 = load i32*, i32** %__seg_37, align 4
  %add.ptr = getelementptr inbounds i32, i32* %38, i32 %div
  store i32* %add.ptr, i32** %__seg_37, align 4
  %39 = load i32, i32* %__ddn, align 4
  %__ctz_38 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 1
  %40 = load i32, i32* %__ctz_38, align 4
  %add39 = add i32 %39, %40
  %rem = urem i32 %add39, 32
  %__ctz_40 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 1
  store i32 %rem, i32* %__ctz_40, align 4
  %41 = load i32, i32* %__ddn, align 4
  %42 = load i32, i32* %__dn, align 4
  %sub41 = sub i32 %42, %41
  store i32 %sub41, i32* %__dn, align 4
  %43 = load i32, i32* %__dn, align 4
  %cmp42 = icmp sgt i32 %43, 0
  br i1 %cmp42, label %if.then43, label %if.end55

if.then43:                                        ; preds = %if.end
  %44 = load i32, i32* %__dn, align 4
  %sub44 = sub nsw i32 32, %44
  %shr45 = lshr i32 -1, %sub44
  store i32 %shr45, i32* %__m, align 4
  %45 = load i32, i32* %__m, align 4
  %neg46 = xor i32 %45, -1
  %__seg_47 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 0
  %46 = load i32*, i32** %__seg_47, align 4
  %47 = load i32, i32* %46, align 4
  %and48 = and i32 %47, %neg46
  store i32 %and48, i32* %46, align 4
  %48 = load i32, i32* %__b, align 4
  %__ctz_49 = getelementptr inbounds %"class.std::__2::__bit_iterator.140", %"class.std::__2::__bit_iterator.140"* %__first, i32 0, i32 1
  %49 = load i32, i32* %__ctz_49, align 4
  %50 = load i32, i32* %__ddn, align 4
  %add50 = add i32 %49, %50
  %shr51 = lshr i32 %48, %add50
  %__seg_52 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 0
  %51 = load i32*, i32** %__seg_52, align 4
  %52 = load i32, i32* %51, align 4
  %or53 = or i32 %52, %shr51
  store i32 %or53, i32* %51, align 4
  %53 = load i32, i32* %__dn, align 4
  %__ctz_54 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 1
  store i32 %53, i32* %__ctz_54, align 4
  br label %if.end55

if.end55:                                         ; preds = %if.then43, %if.end
  %__seg_56 = getelementptr inbounds %"class.std::__2::__bit_iterator.140", %"class.std::__2::__bit_iterator.140"* %__first, i32 0, i32 0
  %54 = load i32*, i32** %__seg_56, align 4
  %incdec.ptr = getelementptr inbounds i32, i32* %54, i32 1
  store i32* %incdec.ptr, i32** %__seg_56, align 4
  br label %if.end57

if.end57:                                         ; preds = %if.end55, %if.then
  %__ctz_59 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 1
  %55 = load i32, i32* %__ctz_59, align 4
  %sub60 = sub i32 32, %55
  store i32 %sub60, i32* %__clz_r58, align 4
  %__ctz_62 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 1
  %56 = load i32, i32* %__ctz_62, align 4
  %shl63 = shl i32 -1, %56
  store i32 %shl63, i32* %__m61, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end57
  %57 = load i32, i32* %__n, align 4
  %cmp64 = icmp sge i32 %57, 32
  br i1 %cmp64, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %__seg_66 = getelementptr inbounds %"class.std::__2::__bit_iterator.140", %"class.std::__2::__bit_iterator.140"* %__first, i32 0, i32 0
  %58 = load i32*, i32** %__seg_66, align 4
  %59 = load i32, i32* %58, align 4
  store i32 %59, i32* %__b65, align 4
  %60 = load i32, i32* %__m61, align 4
  %neg67 = xor i32 %60, -1
  %__seg_68 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 0
  %61 = load i32*, i32** %__seg_68, align 4
  %62 = load i32, i32* %61, align 4
  %and69 = and i32 %62, %neg67
  store i32 %and69, i32* %61, align 4
  %63 = load i32, i32* %__b65, align 4
  %__ctz_70 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 1
  %64 = load i32, i32* %__ctz_70, align 4
  %shl71 = shl i32 %63, %64
  %__seg_72 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 0
  %65 = load i32*, i32** %__seg_72, align 4
  %66 = load i32, i32* %65, align 4
  %or73 = or i32 %66, %shl71
  store i32 %or73, i32* %65, align 4
  %__seg_74 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 0
  %67 = load i32*, i32** %__seg_74, align 4
  %incdec.ptr75 = getelementptr inbounds i32, i32* %67, i32 1
  store i32* %incdec.ptr75, i32** %__seg_74, align 4
  %68 = load i32, i32* %__m61, align 4
  %__seg_76 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 0
  %69 = load i32*, i32** %__seg_76, align 4
  %70 = load i32, i32* %69, align 4
  %and77 = and i32 %70, %68
  store i32 %and77, i32* %69, align 4
  %71 = load i32, i32* %__b65, align 4
  %72 = load i32, i32* %__clz_r58, align 4
  %shr78 = lshr i32 %71, %72
  %__seg_79 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 0
  %73 = load i32*, i32** %__seg_79, align 4
  %74 = load i32, i32* %73, align 4
  %or80 = or i32 %74, %shr78
  store i32 %or80, i32* %73, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %75 = load i32, i32* %__n, align 4
  %sub81 = sub nsw i32 %75, 32
  store i32 %sub81, i32* %__n, align 4
  %__seg_82 = getelementptr inbounds %"class.std::__2::__bit_iterator.140", %"class.std::__2::__bit_iterator.140"* %__first, i32 0, i32 0
  %76 = load i32*, i32** %__seg_82, align 4
  %incdec.ptr83 = getelementptr inbounds i32, i32* %76, i32 1
  store i32* %incdec.ptr83, i32** %__seg_82, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %77 = load i32, i32* %__n, align 4
  %cmp84 = icmp sgt i32 %77, 0
  br i1 %cmp84, label %if.then85, label %if.end128

if.then85:                                        ; preds = %for.end
  %78 = load i32, i32* %__n, align 4
  %sub86 = sub nsw i32 32, %78
  %shr87 = lshr i32 -1, %sub86
  store i32 %shr87, i32* %__m61, align 4
  %__seg_89 = getelementptr inbounds %"class.std::__2::__bit_iterator.140", %"class.std::__2::__bit_iterator.140"* %__first, i32 0, i32 0
  %79 = load i32*, i32** %__seg_89, align 4
  %80 = load i32, i32* %79, align 4
  %81 = load i32, i32* %__m61, align 4
  %and90 = and i32 %80, %81
  store i32 %and90, i32* %__b88, align 4
  %82 = load i32, i32* %__clz_r58, align 4
  store i32 %82, i32* %ref.tmp92, align 4
  %call93 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minIlEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %__n, i32* nonnull align 4 dereferenceable(4) %ref.tmp92)
  %83 = load i32, i32* %call93, align 4
  store i32 %83, i32* %__dn91, align 4
  %__ctz_94 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 1
  %84 = load i32, i32* %__ctz_94, align 4
  %shl95 = shl i32 -1, %84
  %85 = load i32, i32* %__clz_r58, align 4
  %86 = load i32, i32* %__dn91, align 4
  %sub96 = sub i32 %85, %86
  %shr97 = lshr i32 -1, %sub96
  %and98 = and i32 %shl95, %shr97
  store i32 %and98, i32* %__m61, align 4
  %87 = load i32, i32* %__m61, align 4
  %neg99 = xor i32 %87, -1
  %__seg_100 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 0
  %88 = load i32*, i32** %__seg_100, align 4
  %89 = load i32, i32* %88, align 4
  %and101 = and i32 %89, %neg99
  store i32 %and101, i32* %88, align 4
  %90 = load i32, i32* %__b88, align 4
  %__ctz_102 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 1
  %91 = load i32, i32* %__ctz_102, align 4
  %shl103 = shl i32 %90, %91
  %__seg_104 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 0
  %92 = load i32*, i32** %__seg_104, align 4
  %93 = load i32, i32* %92, align 4
  %or105 = or i32 %93, %shl103
  store i32 %or105, i32* %92, align 4
  %94 = load i32, i32* %__dn91, align 4
  %__ctz_106 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 1
  %95 = load i32, i32* %__ctz_106, align 4
  %add107 = add i32 %94, %95
  %div108 = udiv i32 %add107, 32
  %__seg_109 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 0
  %96 = load i32*, i32** %__seg_109, align 4
  %add.ptr110 = getelementptr inbounds i32, i32* %96, i32 %div108
  store i32* %add.ptr110, i32** %__seg_109, align 4
  %97 = load i32, i32* %__dn91, align 4
  %__ctz_111 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 1
  %98 = load i32, i32* %__ctz_111, align 4
  %add112 = add i32 %97, %98
  %rem113 = urem i32 %add112, 32
  %__ctz_114 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 1
  store i32 %rem113, i32* %__ctz_114, align 4
  %99 = load i32, i32* %__dn91, align 4
  %100 = load i32, i32* %__n, align 4
  %sub115 = sub i32 %100, %99
  store i32 %sub115, i32* %__n, align 4
  %101 = load i32, i32* %__n, align 4
  %cmp116 = icmp sgt i32 %101, 0
  br i1 %cmp116, label %if.then117, label %if.end127

if.then117:                                       ; preds = %if.then85
  %102 = load i32, i32* %__n, align 4
  %sub118 = sub nsw i32 32, %102
  %shr119 = lshr i32 -1, %sub118
  store i32 %shr119, i32* %__m61, align 4
  %103 = load i32, i32* %__m61, align 4
  %neg120 = xor i32 %103, -1
  %__seg_121 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 0
  %104 = load i32*, i32** %__seg_121, align 4
  %105 = load i32, i32* %104, align 4
  %and122 = and i32 %105, %neg120
  store i32 %and122, i32* %104, align 4
  %106 = load i32, i32* %__b88, align 4
  %107 = load i32, i32* %__dn91, align 4
  %shr123 = lshr i32 %106, %107
  %__seg_124 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 0
  %108 = load i32*, i32** %__seg_124, align 4
  %109 = load i32, i32* %108, align 4
  %or125 = or i32 %109, %shr123
  store i32 %or125, i32* %108, align 4
  %110 = load i32, i32* %__n, align 4
  %__ctz_126 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 1
  store i32 %110, i32* %__ctz_126, align 4
  br label %if.end127

if.end127:                                        ; preds = %if.then117, %if.then85
  br label %if.end128

if.end128:                                        ; preds = %if.end127, %for.end
  br label %if.end129

if.end129:                                        ; preds = %if.end128, %entry
  %call130 = call %"class.std::__2::__bit_iterator"* @_ZNSt3__214__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb0ELm0EEC2ERKS5_(%"class.std::__2::__bit_iterator"* %agg.result, %"class.std::__2::__bit_iterator"* nonnull align 4 dereferenceable(8) %__result) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__2miERKNS_14__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb1ELm0EEES7_(%"class.std::__2::__bit_iterator.140"* nonnull align 4 dereferenceable(8) %__x, %"class.std::__2::__bit_iterator.140"* nonnull align 4 dereferenceable(8) %__y) #0 comdat {
entry:
  %__x.addr = alloca %"class.std::__2::__bit_iterator.140"*, align 4
  %__y.addr = alloca %"class.std::__2::__bit_iterator.140"*, align 4
  store %"class.std::__2::__bit_iterator.140"* %__x, %"class.std::__2::__bit_iterator.140"** %__x.addr, align 4
  store %"class.std::__2::__bit_iterator.140"* %__y, %"class.std::__2::__bit_iterator.140"** %__y.addr, align 4
  %0 = load %"class.std::__2::__bit_iterator.140"*, %"class.std::__2::__bit_iterator.140"** %__x.addr, align 4
  %__seg_ = getelementptr inbounds %"class.std::__2::__bit_iterator.140", %"class.std::__2::__bit_iterator.140"* %0, i32 0, i32 0
  %1 = load i32*, i32** %__seg_, align 4
  %2 = load %"class.std::__2::__bit_iterator.140"*, %"class.std::__2::__bit_iterator.140"** %__y.addr, align 4
  %__seg_1 = getelementptr inbounds %"class.std::__2::__bit_iterator.140", %"class.std::__2::__bit_iterator.140"* %2, i32 0, i32 0
  %3 = load i32*, i32** %__seg_1, align 4
  %sub.ptr.lhs.cast = ptrtoint i32* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint i32* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  %mul = mul i32 %sub.ptr.div, 32
  %4 = load %"class.std::__2::__bit_iterator.140"*, %"class.std::__2::__bit_iterator.140"** %__x.addr, align 4
  %__ctz_ = getelementptr inbounds %"class.std::__2::__bit_iterator.140", %"class.std::__2::__bit_iterator.140"* %4, i32 0, i32 1
  %5 = load i32, i32* %__ctz_, align 4
  %add = add i32 %mul, %5
  %6 = load %"class.std::__2::__bit_iterator.140"*, %"class.std::__2::__bit_iterator.140"** %__y.addr, align 4
  %__ctz_2 = getelementptr inbounds %"class.std::__2::__bit_iterator.140", %"class.std::__2::__bit_iterator.140"* %6, i32 0, i32 1
  %7 = load i32, i32* %__ctz_2, align 4
  %sub = sub i32 %add, %7
  ret i32 %sub
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNSt3__212__to_addressIKmEEPT_S3_(i32* %__p) #0 comdat {
entry:
  %__p.addr = alloca i32*, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %0 = load i32*, i32** %__p.addr, align 4
  ret i32* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIbNS_9allocatorIbEEE11__make_iterEm(%"class.std::__2::__bit_iterator.140"* noalias sret align 4 %agg.result, %"class.std::__2::vector.110"* %this, i32 %__pos) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.110"*, align 4
  %__pos.addr = alloca i32, align 4
  store %"class.std::__2::vector.110"* %this, %"class.std::__2::vector.110"** %this.addr, align 4
  store i32 %__pos, i32* %__pos.addr, align 4
  %this1 = load %"class.std::__2::vector.110"*, %"class.std::__2::vector.110"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::vector.110", %"class.std::__2::vector.110"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__begin_, align 4
  %1 = load i32, i32* %__pos.addr, align 4
  %div = udiv i32 %1, 32
  %add.ptr = getelementptr inbounds i32, i32* %0, i32 %div
  %2 = load i32, i32* %__pos.addr, align 4
  %rem = urem i32 %2, 32
  %call = call %"class.std::__2::__bit_iterator.140"* @_ZNSt3__214__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb1ELm0EEC2EPKmj(%"class.std::__2::__bit_iterator.140"* %agg.result, i32* %add.ptr, i32 %rem) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__bit_iterator.140"* @_ZNSt3__214__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb1ELm0EEC2EPKmj(%"class.std::__2::__bit_iterator.140"* returned %this, i32* %__s, i32 %__ctz) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__bit_iterator.140"*, align 4
  %__s.addr = alloca i32*, align 4
  %__ctz.addr = alloca i32, align 4
  store %"class.std::__2::__bit_iterator.140"* %this, %"class.std::__2::__bit_iterator.140"** %this.addr, align 4
  store i32* %__s, i32** %__s.addr, align 4
  store i32 %__ctz, i32* %__ctz.addr, align 4
  %this1 = load %"class.std::__2::__bit_iterator.140"*, %"class.std::__2::__bit_iterator.140"** %this.addr, align 4
  %__seg_ = getelementptr inbounds %"class.std::__2::__bit_iterator.140", %"class.std::__2::__bit_iterator.140"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__s.addr, align 4
  store i32* %0, i32** %__seg_, align 4
  %__ctz_ = getelementptr inbounds %"class.std::__2::__bit_iterator.140", %"class.std::__2::__bit_iterator.140"* %this1, i32 0, i32 1
  %1 = load i32, i32* %__ctz.addr, align 4
  store i32 %1, i32* %__ctz_, align 4
  ret %"class.std::__2::__bit_iterator.140"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__24swapIPmEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_(i32** nonnull align 4 dereferenceable(4) %__x, i32** nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %__x.addr = alloca i32**, align 4
  %__y.addr = alloca i32**, align 4
  %__t = alloca i32*, align 4
  store i32** %__x, i32*** %__x.addr, align 4
  store i32** %__y, i32*** %__y.addr, align 4
  %0 = load i32**, i32*** %__x.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__24moveIRPmEEONS_16remove_referenceIT_E4typeEOS4_(i32** nonnull align 4 dereferenceable(4) %0) #8
  %1 = load i32*, i32** %call, align 4
  store i32* %1, i32** %__t, align 4
  %2 = load i32**, i32*** %__y.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__24moveIRPmEEONS_16remove_referenceIT_E4typeEOS4_(i32** nonnull align 4 dereferenceable(4) %2) #8
  %3 = load i32*, i32** %call1, align 4
  %4 = load i32**, i32*** %__x.addr, align 4
  store i32* %3, i32** %4, align 4
  %call2 = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__24moveIRPmEEONS_16remove_referenceIT_E4typeEOS4_(i32** nonnull align 4 dereferenceable(4) %__t) #8
  %5 = load i32*, i32** %call2, align 4
  %6 = load i32**, i32*** %__y.addr, align 4
  store i32* %5, i32** %6, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__24swapImEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS2_EE5valueEvE4typeERS2_S5_(i32* nonnull align 4 dereferenceable(4) %__x, i32* nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %__x.addr = alloca i32*, align 4
  %__y.addr = alloca i32*, align 4
  %__t = alloca i32, align 4
  store i32* %__x, i32** %__x.addr, align 4
  store i32* %__y, i32** %__y.addr, align 4
  %0 = load i32*, i32** %__x.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__24moveIRmEEONS_16remove_referenceIT_E4typeEOS3_(i32* nonnull align 4 dereferenceable(4) %0) #8
  %1 = load i32, i32* %call, align 4
  store i32 %1, i32* %__t, align 4
  %2 = load i32*, i32** %__y.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__24moveIRmEEONS_16remove_referenceIT_E4typeEOS3_(i32* nonnull align 4 dereferenceable(4) %2) #8
  %3 = load i32, i32* %call1, align 4
  %4 = load i32*, i32** %__x.addr, align 4
  store i32 %3, i32* %4, align 4
  %call2 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__24moveIRmEEONS_16remove_referenceIT_E4typeEOS3_(i32* nonnull align 4 dereferenceable(4) %__t) #8
  %5 = load i32, i32* %call2, align 4
  %6 = load i32*, i32** %__y.addr, align 4
  store i32 %5, i32* %6, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216__swap_allocatorINS_9allocatorImEEEEvRT_S4_NS_17integral_constantIbLb0EEE(%"class.std::__2::allocator.113"* nonnull align 1 dereferenceable(1) %0, %"class.std::__2::allocator.113"* nonnull align 1 dereferenceable(1) %1) #0 comdat {
entry:
  %2 = alloca %"struct.std::__2::integral_constant.129", align 1
  %.addr = alloca %"class.std::__2::allocator.113"*, align 4
  %.addr1 = alloca %"class.std::__2::allocator.113"*, align 4
  store %"class.std::__2::allocator.113"* %0, %"class.std::__2::allocator.113"** %.addr, align 4
  store %"class.std::__2::allocator.113"* %1, %"class.std::__2::allocator.113"** %.addr1, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNSt3__24moveIRPmEEONS_16remove_referenceIT_E4typeEOS4_(i32** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca i32**, align 4
  store i32** %__t, i32*** %__t.addr, align 4
  %0 = load i32**, i32*** %__t.addr, align 4
  ret i32** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__24moveIRmEEONS_16remove_referenceIT_E4typeEOS3_(i32* nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca i32*, align 4
  store i32* %__t, i32** %__t.addr, align 4
  %0 = load i32*, i32** %__t.addr, align 4
  ret i32* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__fill_n_trueINS_6vectorIbNS_9allocatorIbEEEEEEvNS_14__bit_iteratorIT_Lb0EXLi0EEEENS6_9size_typeE(%"class.std::__2::__bit_iterator"* %__first, i32 %__n) #0 comdat {
entry:
  %__n.addr = alloca i32, align 4
  %__bits_per_word = alloca i32, align 4
  %__clz_f = alloca i32, align 4
  %__dn = alloca i32, align 4
  %__m = alloca i32, align 4
  %__nw = alloca i32, align 4
  %__m13 = alloca i32, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store i32 32, i32* %__bits_per_word, align 4
  %__ctz_ = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__first, i32 0, i32 1
  %0 = load i32, i32* %__ctz_, align 4
  %cmp = icmp ne i32 %0, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__ctz_1 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__first, i32 0, i32 1
  %1 = load i32, i32* %__ctz_1, align 4
  %sub = sub i32 32, %1
  store i32 %sub, i32* %__clz_f, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %__clz_f, i32* nonnull align 4 dereferenceable(4) %__n.addr)
  %2 = load i32, i32* %call, align 4
  store i32 %2, i32* %__dn, align 4
  %__ctz_2 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__first, i32 0, i32 1
  %3 = load i32, i32* %__ctz_2, align 4
  %shl = shl i32 -1, %3
  %4 = load i32, i32* %__clz_f, align 4
  %5 = load i32, i32* %__dn, align 4
  %sub3 = sub i32 %4, %5
  %shr = lshr i32 -1, %sub3
  %and = and i32 %shl, %shr
  store i32 %and, i32* %__m, align 4
  %6 = load i32, i32* %__m, align 4
  %__seg_ = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__first, i32 0, i32 0
  %7 = load i32*, i32** %__seg_, align 4
  %8 = load i32, i32* %7, align 4
  %or = or i32 %8, %6
  store i32 %or, i32* %7, align 4
  %9 = load i32, i32* %__dn, align 4
  %10 = load i32, i32* %__n.addr, align 4
  %sub4 = sub i32 %10, %9
  store i32 %sub4, i32* %__n.addr, align 4
  %__seg_5 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__first, i32 0, i32 0
  %11 = load i32*, i32** %__seg_5, align 4
  %incdec.ptr = getelementptr inbounds i32, i32* %11, i32 1
  store i32* %incdec.ptr, i32** %__seg_5, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %12 = load i32, i32* %__n.addr, align 4
  %div = udiv i32 %12, 32
  store i32 %div, i32* %__nw, align 4
  %__seg_6 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__first, i32 0, i32 0
  %13 = load i32*, i32** %__seg_6, align 4
  %call7 = call i32* @_ZNSt3__212__to_addressImEEPT_S2_(i32* %13) #8
  %14 = bitcast i32* %call7 to i8*
  %15 = load i32, i32* %__nw, align 4
  %mul = mul i32 %15, 4
  call void @llvm.memset.p0i8.i32(i8* align 4 %14, i8 -1, i32 %mul, i1 false)
  %16 = load i32, i32* %__nw, align 4
  %mul8 = mul i32 %16, 32
  %17 = load i32, i32* %__n.addr, align 4
  %sub9 = sub i32 %17, %mul8
  store i32 %sub9, i32* %__n.addr, align 4
  %18 = load i32, i32* %__n.addr, align 4
  %cmp10 = icmp ugt i32 %18, 0
  br i1 %cmp10, label %if.then11, label %if.end18

if.then11:                                        ; preds = %if.end
  %19 = load i32, i32* %__nw, align 4
  %__seg_12 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__first, i32 0, i32 0
  %20 = load i32*, i32** %__seg_12, align 4
  %add.ptr = getelementptr inbounds i32, i32* %20, i32 %19
  store i32* %add.ptr, i32** %__seg_12, align 4
  %21 = load i32, i32* %__n.addr, align 4
  %sub14 = sub i32 32, %21
  %shr15 = lshr i32 -1, %sub14
  store i32 %shr15, i32* %__m13, align 4
  %22 = load i32, i32* %__m13, align 4
  %__seg_16 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__first, i32 0, i32 0
  %23 = load i32*, i32** %__seg_16, align 4
  %24 = load i32, i32* %23, align 4
  %or17 = or i32 %24, %22
  store i32 %or17, i32* %23, align 4
  br label %if.end18

if.end18:                                         ; preds = %if.then11, %if.end
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__fill_n_falseINS_6vectorIbNS_9allocatorIbEEEEEEvNS_14__bit_iteratorIT_Lb0EXLi0EEEENS6_9size_typeE(%"class.std::__2::__bit_iterator"* %__first, i32 %__n) #0 comdat {
entry:
  %__n.addr = alloca i32, align 4
  %__bits_per_word = alloca i32, align 4
  %__clz_f = alloca i32, align 4
  %__dn = alloca i32, align 4
  %__m = alloca i32, align 4
  %__nw = alloca i32, align 4
  %__m14 = alloca i32, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store i32 32, i32* %__bits_per_word, align 4
  %__ctz_ = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__first, i32 0, i32 1
  %0 = load i32, i32* %__ctz_, align 4
  %cmp = icmp ne i32 %0, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__ctz_1 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__first, i32 0, i32 1
  %1 = load i32, i32* %__ctz_1, align 4
  %sub = sub i32 32, %1
  store i32 %sub, i32* %__clz_f, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %__clz_f, i32* nonnull align 4 dereferenceable(4) %__n.addr)
  %2 = load i32, i32* %call, align 4
  store i32 %2, i32* %__dn, align 4
  %__ctz_2 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__first, i32 0, i32 1
  %3 = load i32, i32* %__ctz_2, align 4
  %shl = shl i32 -1, %3
  %4 = load i32, i32* %__clz_f, align 4
  %5 = load i32, i32* %__dn, align 4
  %sub3 = sub i32 %4, %5
  %shr = lshr i32 -1, %sub3
  %and = and i32 %shl, %shr
  store i32 %and, i32* %__m, align 4
  %6 = load i32, i32* %__m, align 4
  %neg = xor i32 %6, -1
  %__seg_ = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__first, i32 0, i32 0
  %7 = load i32*, i32** %__seg_, align 4
  %8 = load i32, i32* %7, align 4
  %and4 = and i32 %8, %neg
  store i32 %and4, i32* %7, align 4
  %9 = load i32, i32* %__dn, align 4
  %10 = load i32, i32* %__n.addr, align 4
  %sub5 = sub i32 %10, %9
  store i32 %sub5, i32* %__n.addr, align 4
  %__seg_6 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__first, i32 0, i32 0
  %11 = load i32*, i32** %__seg_6, align 4
  %incdec.ptr = getelementptr inbounds i32, i32* %11, i32 1
  store i32* %incdec.ptr, i32** %__seg_6, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %12 = load i32, i32* %__n.addr, align 4
  %div = udiv i32 %12, 32
  store i32 %div, i32* %__nw, align 4
  %__seg_7 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__first, i32 0, i32 0
  %13 = load i32*, i32** %__seg_7, align 4
  %call8 = call i32* @_ZNSt3__212__to_addressImEEPT_S2_(i32* %13) #8
  %14 = bitcast i32* %call8 to i8*
  %15 = load i32, i32* %__nw, align 4
  %mul = mul i32 %15, 4
  call void @llvm.memset.p0i8.i32(i8* align 4 %14, i8 0, i32 %mul, i1 false)
  %16 = load i32, i32* %__nw, align 4
  %mul9 = mul i32 %16, 32
  %17 = load i32, i32* %__n.addr, align 4
  %sub10 = sub i32 %17, %mul9
  store i32 %sub10, i32* %__n.addr, align 4
  %18 = load i32, i32* %__n.addr, align 4
  %cmp11 = icmp ugt i32 %18, 0
  br i1 %cmp11, label %if.then12, label %if.end20

if.then12:                                        ; preds = %if.end
  %19 = load i32, i32* %__nw, align 4
  %__seg_13 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__first, i32 0, i32 0
  %20 = load i32*, i32** %__seg_13, align 4
  %add.ptr = getelementptr inbounds i32, i32* %20, i32 %19
  store i32* %add.ptr, i32** %__seg_13, align 4
  %21 = load i32, i32* %__n.addr, align 4
  %sub15 = sub i32 32, %21
  %shr16 = lshr i32 -1, %sub15
  store i32 %shr16, i32* %__m14, align 4
  %22 = load i32, i32* %__m14, align 4
  %neg17 = xor i32 %22, -1
  %__seg_18 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__first, i32 0, i32 0
  %23 = load i32*, i32** %__seg_18, align 4
  %24 = load i32, i32* %23, align 4
  %and19 = and i32 %24, %neg17
  store i32 %and19, i32* %23, align 4
  br label %if.end20

if.end20:                                         ; preds = %if.then12, %if.end
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIbNS_9allocatorIbEEE10__make_refEm(%"class.std::__2::__bit_reference"* noalias sret align 4 %agg.result, %"class.std::__2::vector.110"* %this, i32 %__pos) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.110"*, align 4
  %__pos.addr = alloca i32, align 4
  store %"class.std::__2::vector.110"* %this, %"class.std::__2::vector.110"** %this.addr, align 4
  store i32 %__pos, i32* %__pos.addr, align 4
  %this1 = load %"class.std::__2::vector.110"*, %"class.std::__2::vector.110"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::vector.110", %"class.std::__2::vector.110"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__begin_, align 4
  %1 = load i32, i32* %__pos.addr, align 4
  %div = udiv i32 %1, 32
  %add.ptr = getelementptr inbounds i32, i32* %0, i32 %div
  %2 = load i32, i32* %__pos.addr, align 4
  %rem = urem i32 %2, 32
  %shl = shl i32 1, %rem
  %call = call %"class.std::__2::__bit_reference"* @_ZNSt3__215__bit_referenceINS_6vectorIbNS_9allocatorIbEEEELb1EEC2EPmm(%"class.std::__2::__bit_reference"* %agg.result, i32* %add.ptr, i32 %shl) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__bit_reference"* @_ZNSt3__215__bit_referenceINS_6vectorIbNS_9allocatorIbEEEELb1EEC2EPmm(%"class.std::__2::__bit_reference"* returned %this, i32* %__s, i32 %__m) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__bit_reference"*, align 4
  %__s.addr = alloca i32*, align 4
  %__m.addr = alloca i32, align 4
  store %"class.std::__2::__bit_reference"* %this, %"class.std::__2::__bit_reference"** %this.addr, align 4
  store i32* %__s, i32** %__s.addr, align 4
  store i32 %__m, i32* %__m.addr, align 4
  %this1 = load %"class.std::__2::__bit_reference"*, %"class.std::__2::__bit_reference"** %this.addr, align 4
  %__seg_ = getelementptr inbounds %"class.std::__2::__bit_reference", %"class.std::__2::__bit_reference"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__s.addr, align 4
  store i32* %0, i32** %__seg_, align 4
  %__mask_ = getelementptr inbounds %"class.std::__2::__bit_reference", %"class.std::__2::__bit_reference"* %this1, i32 0, i32 1
  %1 = load i32, i32* %__m.addr, align 4
  store i32 %1, i32* %__mask_, align 4
  ret %"class.std::__2::__bit_reference"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector.118"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEEC2Em(%"class.std::__2::vector.118"* returned %this, i32 %__n) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"class.std::__2::vector.118"*, align 4
  %this.addr = alloca %"class.std::__2::vector.118"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector.118"* %this, %"class.std::__2::vector.118"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.118"*, %"class.std::__2::vector.118"** %this.addr, align 4
  store %"class.std::__2::vector.118"* %this1, %"class.std::__2::vector.118"** %retval, align 4
  %0 = bitcast %"class.std::__2::vector.118"* %this1 to %"class.std::__2::__vector_base.119"*
  %call = call %"class.std::__2::__vector_base.119"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEEC2Ev(%"class.std::__2::__vector_base.119"* %0) #8
  %1 = load i32, i32* %__n.addr, align 4
  %cmp = icmp ugt i32 %1, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE11__vallocateEm(%"class.std::__2::vector.118"* %this1, i32 %2)
  %3 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE18__construct_at_endEm(%"class.std::__2::vector.118"* %this1, i32 %3)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %4 = load %"class.std::__2::vector.118"*, %"class.std::__2::vector.118"** %retval, align 4
  ret %"class.std::__2::vector.118"* %4
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base.119"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEEC2Ev(%"class.std::__2::__vector_base.119"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.119"*, align 4
  %ref.tmp = alloca i8*, align 4
  %ref.tmp2 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__vector_base.119"* %this, %"class.std::__2::__vector_base.119"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.119"*, %"class.std::__2::__vector_base.119"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__vector_base.119"* %this1 to %"class.std::__2::__vector_base_common"*
  %call = call %"class.std::__2::__vector_base_common"* @_ZNSt3__220__vector_base_commonILb1EEC2Ev(%"class.std::__2::__vector_base_common"* %0)
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.119", %"class.std::__2::__vector_base.119"* %this1, i32 0, i32 0
  store %"class.draco::IndexType.104"* null, %"class.draco::IndexType.104"** %__begin_, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.119", %"class.std::__2::__vector_base.119"* %this1, i32 0, i32 1
  store %"class.draco::IndexType.104"* null, %"class.draco::IndexType.104"** %__end_, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.119", %"class.std::__2::__vector_base.119"* %this1, i32 0, i32 2
  store i8* null, i8** %ref.tmp, align 4
  %call3 = call %"class.std::__2::__compressed_pair.120"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEEC2IDnNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.120"* %__end_cap_, i8** nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  ret %"class.std::__2::__vector_base.119"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE11__vallocateEm(%"class.std::__2::vector.118"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.118"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector.118"* %this, %"class.std::__2::vector.118"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.118"*, %"class.std::__2::vector.118"** %this.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE8max_sizeEv(%"class.std::__2::vector.118"* %this1) #8
  %cmp = icmp ugt i32 %0, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %"class.std::__2::vector.118"* %this1 to %"class.std::__2::__vector_base_common"*
  call void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"* %1) #9
  unreachable

if.end:                                           ; preds = %entry
  %2 = bitcast %"class.std::__2::vector.118"* %this1 to %"class.std::__2::__vector_base.119"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.123"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.119"* %2) #8
  %3 = load i32, i32* %__n.addr, align 4
  %call3 = call %"class.draco::IndexType.104"* @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE8allocateERS6_m(%"class.std::__2::allocator.123"* nonnull align 1 dereferenceable(1) %call2, i32 %3)
  %4 = bitcast %"class.std::__2::vector.118"* %this1 to %"class.std::__2::__vector_base.119"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.119", %"class.std::__2::__vector_base.119"* %4, i32 0, i32 1
  store %"class.draco::IndexType.104"* %call3, %"class.draco::IndexType.104"** %__end_, align 4
  %5 = bitcast %"class.std::__2::vector.118"* %this1 to %"class.std::__2::__vector_base.119"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.119", %"class.std::__2::__vector_base.119"* %5, i32 0, i32 0
  store %"class.draco::IndexType.104"* %call3, %"class.draco::IndexType.104"** %__begin_, align 4
  %6 = bitcast %"class.std::__2::vector.118"* %this1 to %"class.std::__2::__vector_base.119"*
  %__begin_4 = getelementptr inbounds %"class.std::__2::__vector_base.119", %"class.std::__2::__vector_base.119"* %6, i32 0, i32 0
  %7 = load %"class.draco::IndexType.104"*, %"class.draco::IndexType.104"** %__begin_4, align 4
  %8 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds %"class.draco::IndexType.104", %"class.draco::IndexType.104"* %7, i32 %8
  %9 = bitcast %"class.std::__2::vector.118"* %this1 to %"class.std::__2::__vector_base.119"*
  %call5 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.104"** @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base.119"* %9) #8
  store %"class.draco::IndexType.104"* %add.ptr, %"class.draco::IndexType.104"** %call5, align 4
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE14__annotate_newEm(%"class.std::__2::vector.118"* %this1, i32 0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE18__construct_at_endEm(%"class.std::__2::vector.118"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.118"*, align 4
  %__n.addr = alloca i32, align 4
  %__tx = alloca %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction", align 4
  store %"class.std::__2::vector.118"* %this, %"class.std::__2::vector.118"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.118"*, %"class.std::__2::vector.118"** %this.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %call = call %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionC2ERS7_m(%"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"* %__tx, %"class.std::__2::vector.118"* nonnull align 4 dereferenceable(12) %this1, i32 %0)
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %1 = load %"class.draco::IndexType.104"*, %"class.draco::IndexType.104"** %__pos_, align 4
  %__new_end_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"* %__tx, i32 0, i32 2
  %2 = load %"class.draco::IndexType.104"*, %"class.draco::IndexType.104"** %__new_end_, align 4
  %cmp = icmp ne %"class.draco::IndexType.104"* %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = bitcast %"class.std::__2::vector.118"* %this1 to %"class.std::__2::__vector_base.119"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.123"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.119"* %3) #8
  %__pos_3 = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %4 = load %"class.draco::IndexType.104"*, %"class.draco::IndexType.104"** %__pos_3, align 4
  %call4 = call %"class.draco::IndexType.104"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType.104"* %4) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE9constructIS5_JEEEvRS6_PT_DpOT0_(%"class.std::__2::allocator.123"* nonnull align 1 dereferenceable(1) %call2, %"class.draco::IndexType.104"* %call4)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %__pos_5 = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %5 = load %"class.draco::IndexType.104"*, %"class.draco::IndexType.104"** %__pos_5, align 4
  %incdec.ptr = getelementptr inbounds %"class.draco::IndexType.104", %"class.draco::IndexType.104"* %5, i32 1
  store %"class.draco::IndexType.104"* %incdec.ptr, %"class.draco::IndexType.104"** %__pos_5, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call6 = call %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"* %__tx) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.120"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEEC2IDnNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.120"* returned %this, i8** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.120"*, align 4
  %__t1.addr = alloca i8**, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.120"* %this, %"class.std::__2::__compressed_pair.120"** %this.addr, align 4
  store i8** %__t1, i8*** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.120"*, %"class.std::__2::__compressed_pair.120"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.120"* %this1 to %"struct.std::__2::__compressed_pair_elem.121"*
  %1 = load i8**, i8*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %1) #8
  %call2 = call %"struct.std::__2::__compressed_pair_elem.121"* @_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.121"* %0, i8** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.120"* %this1 to %"struct.std::__2::__compressed_pair_elem.122"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #8
  %call4 = call %"struct.std::__2::__compressed_pair_elem.122"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.122"* %2)
  ret %"class.std::__2::__compressed_pair.120"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.121"* @_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.121"* returned %this, i8** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.121"*, align 4
  %__u.addr = alloca i8**, align 4
  store %"struct.std::__2::__compressed_pair_elem.121"* %this, %"struct.std::__2::__compressed_pair_elem.121"** %this.addr, align 4
  store i8** %__u, i8*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.121"*, %"struct.std::__2::__compressed_pair_elem.121"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.121", %"struct.std::__2::__compressed_pair_elem.121"* %this1, i32 0, i32 0
  %0 = load i8**, i8*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %0) #8
  store %"class.draco::IndexType.104"* null, %"class.draco::IndexType.104"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.121"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.122"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.122"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.122"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.122"* %this, %"struct.std::__2::__compressed_pair_elem.122"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.122"*, %"struct.std::__2::__compressed_pair_elem.122"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.122"* %this1 to %"class.std::__2::allocator.123"*
  %call = call %"class.std::__2::allocator.123"* @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEEC2Ev(%"class.std::__2::allocator.123"* %1) #8
  ret %"struct.std::__2::__compressed_pair_elem.122"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::allocator.123"* @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEEC2Ev(%"class.std::__2::allocator.123"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.123"*, align 4
  store %"class.std::__2::allocator.123"* %this, %"class.std::__2::allocator.123"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.123"*, %"class.std::__2::allocator.123"** %this.addr, align 4
  ret %"class.std::__2::allocator.123"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE8max_sizeEv(%"class.std::__2::vector.118"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.118"*, align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp3 = alloca i32, align 4
  store %"class.std::__2::vector.118"* %this, %"class.std::__2::vector.118"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.118"*, %"class.std::__2::vector.118"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.118"* %this1 to %"class.std::__2::__vector_base.119"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.123"* @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.119"* %0) #8
  %call2 = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE8max_sizeERKS6_(%"class.std::__2::allocator.123"* nonnull align 1 dereferenceable(1) %call) #8
  store i32 %call2, i32* %ref.tmp, align 4
  %call4 = call i32 @_ZNSt3__214numeric_limitsIlE3maxEv() #8
  store i32 %call4, i32* %ref.tmp3, align 4
  %call5 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %1 = load i32, i32* %call5, align 4
  ret i32 %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType.104"* @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE8allocateERS6_m(%"class.std::__2::allocator.123"* nonnull align 1 dereferenceable(1) %__a, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.123"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.123"* %__a, %"class.std::__2::allocator.123"** %__a.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.123"*, %"class.std::__2::allocator.123"** %__a.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call %"class.draco::IndexType.104"* @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEE8allocateEmPKv(%"class.std::__2::allocator.123"* %0, i32 %1, i8* null)
  ret %"class.draco::IndexType.104"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.104"** @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base.119"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.119"*, align 4
  store %"class.std::__2::__vector_base.119"* %this, %"class.std::__2::__vector_base.119"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.119"*, %"class.std::__2::__vector_base.119"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.119", %"class.std::__2::__vector_base.119"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.104"** @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.120"* %__end_cap_) #8
  ret %"class.draco::IndexType.104"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE14__annotate_newEm(%"class.std::__2::vector.118"* %this, i32 %__current_size) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.118"*, align 4
  %__current_size.addr = alloca i32, align 4
  store %"class.std::__2::vector.118"* %this, %"class.std::__2::vector.118"** %this.addr, align 4
  store i32 %__current_size, i32* %__current_size.addr, align 4
  %this1 = load %"class.std::__2::vector.118"*, %"class.std::__2::vector.118"** %this.addr, align 4
  %call = call %"class.draco::IndexType.104"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.118"* %this1) #8
  %0 = bitcast %"class.draco::IndexType.104"* %call to i8*
  %call2 = call %"class.draco::IndexType.104"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.118"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.118"* %this1) #8
  %add.ptr = getelementptr inbounds %"class.draco::IndexType.104", %"class.draco::IndexType.104"* %call2, i32 %call3
  %1 = bitcast %"class.draco::IndexType.104"* %add.ptr to i8*
  %call4 = call %"class.draco::IndexType.104"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.118"* %this1) #8
  %call5 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.118"* %this1) #8
  %add.ptr6 = getelementptr inbounds %"class.draco::IndexType.104", %"class.draco::IndexType.104"* %call4, i32 %call5
  %2 = bitcast %"class.draco::IndexType.104"* %add.ptr6 to i8*
  %call7 = call %"class.draco::IndexType.104"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.118"* %this1) #8
  %3 = load i32, i32* %__current_size.addr, align 4
  %add.ptr8 = getelementptr inbounds %"class.draco::IndexType.104", %"class.draco::IndexType.104"* %call7, i32 %3
  %4 = bitcast %"class.draco::IndexType.104"* %add.ptr8 to i8*
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_(%"class.std::__2::vector.118"* %this1, i8* %0, i8* %1, i8* %2, i8* %4) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE8max_sizeERKS6_(%"class.std::__2::allocator.123"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.123"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_max_size.143", align 1
  store %"class.std::__2::allocator.123"* %__a, %"class.std::__2::allocator.123"** %__a.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_max_size.143"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.123"*, %"class.std::__2::allocator.123"** %__a.addr, align 4
  %call = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS6_(%"class.std::__2::allocator.123"* nonnull align 1 dereferenceable(1) %1) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.123"* @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.119"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.119"*, align 4
  store %"class.std::__2::__vector_base.119"* %this, %"class.std::__2::__vector_base.119"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.119"*, %"class.std::__2::__vector_base.119"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.119", %"class.std::__2::__vector_base.119"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.123"* @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.120"* %__end_cap_) #8
  ret %"class.std::__2::allocator.123"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS6_(%"class.std::__2::allocator.123"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.123"*, align 4
  store %"class.std::__2::allocator.123"* %__a, %"class.std::__2::allocator.123"** %__a.addr, align 4
  %1 = load %"class.std::__2::allocator.123"*, %"class.std::__2::allocator.123"** %__a.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEE8max_sizeEv(%"class.std::__2::allocator.123"* %1) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__29allocatorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEE8max_sizeEv(%"class.std::__2::allocator.123"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.123"*, align 4
  store %"class.std::__2::allocator.123"* %this, %"class.std::__2::allocator.123"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.123"*, %"class.std::__2::allocator.123"** %this.addr, align 4
  ret i32 1073741823
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.123"* @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.120"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.120"*, align 4
  store %"class.std::__2::__compressed_pair.120"* %this, %"class.std::__2::__compressed_pair.120"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.120"*, %"class.std::__2::__compressed_pair.120"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.120"* %this1 to %"struct.std::__2::__compressed_pair_elem.122"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.123"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.122"* %0) #8
  ret %"class.std::__2::allocator.123"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.123"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.122"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.122"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.122"* %this, %"struct.std::__2::__compressed_pair_elem.122"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.122"*, %"struct.std::__2::__compressed_pair_elem.122"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.122"* %this1 to %"class.std::__2::allocator.123"*
  ret %"class.std::__2::allocator.123"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType.104"* @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEE8allocateEmPKv(%"class.std::__2::allocator.123"* %this, i32 %__n, i8* %0) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.123"*, align 4
  %__n.addr = alloca i32, align 4
  %.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.123"* %this, %"class.std::__2::allocator.123"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::allocator.123"*, %"class.std::__2::allocator.123"** %this.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEE8max_sizeEv(%"class.std::__2::allocator.123"* %this1) #8
  %cmp = icmp ugt i32 %1, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__220__throw_length_errorEPKc(i8* getelementptr inbounds ([68 x i8], [68 x i8]* @.str, i32 0, i32 0)) #9
  unreachable

if.end:                                           ; preds = %entry
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  %call2 = call i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %mul, i32 4)
  %3 = bitcast i8* %call2 to %"class.draco::IndexType.104"*
  ret %"class.draco::IndexType.104"* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.104"** @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.120"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.120"*, align 4
  store %"class.std::__2::__compressed_pair.120"* %this, %"class.std::__2::__compressed_pair.120"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.120"*, %"class.std::__2::__compressed_pair.120"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.120"* %this1 to %"struct.std::__2::__compressed_pair_elem.121"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.104"** @_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.121"* %0) #8
  ret %"class.draco::IndexType.104"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.104"** @_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.121"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.121"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.121"* %this, %"struct.std::__2::__compressed_pair_elem.121"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.121"*, %"struct.std::__2::__compressed_pair_elem.121"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.121", %"struct.std::__2::__compressed_pair_elem.121"* %this1, i32 0, i32 0
  ret %"class.draco::IndexType.104"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionC2ERS7_m(%"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"* returned %this, %"class.std::__2::vector.118"* nonnull align 4 dereferenceable(12) %__v, i32 %__n) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"*, align 4
  %__v.addr = alloca %"class.std::__2::vector.118"*, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"** %this.addr, align 4
  store %"class.std::__2::vector.118"* %__v, %"class.std::__2::vector.118"** %__v.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"*, %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"** %this.addr, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::vector.118"*, %"class.std::__2::vector.118"** %__v.addr, align 4
  store %"class.std::__2::vector.118"* %0, %"class.std::__2::vector.118"** %__v_, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %1 = load %"class.std::__2::vector.118"*, %"class.std::__2::vector.118"** %__v.addr, align 4
  %2 = bitcast %"class.std::__2::vector.118"* %1 to %"class.std::__2::__vector_base.119"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.119", %"class.std::__2::__vector_base.119"* %2, i32 0, i32 1
  %3 = load %"class.draco::IndexType.104"*, %"class.draco::IndexType.104"** %__end_, align 4
  store %"class.draco::IndexType.104"* %3, %"class.draco::IndexType.104"** %__pos_, align 4
  %__new_end_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"* %this1, i32 0, i32 2
  %4 = load %"class.std::__2::vector.118"*, %"class.std::__2::vector.118"** %__v.addr, align 4
  %5 = bitcast %"class.std::__2::vector.118"* %4 to %"class.std::__2::__vector_base.119"*
  %__end_2 = getelementptr inbounds %"class.std::__2::__vector_base.119", %"class.std::__2::__vector_base.119"* %5, i32 0, i32 1
  %6 = load %"class.draco::IndexType.104"*, %"class.draco::IndexType.104"** %__end_2, align 4
  %7 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds %"class.draco::IndexType.104", %"class.draco::IndexType.104"* %6, i32 %7
  store %"class.draco::IndexType.104"* %add.ptr, %"class.draco::IndexType.104"** %__new_end_, align 4
  ret %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE9constructIS5_JEEEvRS6_PT_DpOT0_(%"class.std::__2::allocator.123"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType.104"* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.123"*, align 4
  %__p.addr = alloca %"class.draco::IndexType.104"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_construct.144", align 1
  store %"class.std::__2::allocator.123"* %__a, %"class.std::__2::allocator.123"** %__a.addr, align 4
  store %"class.draco::IndexType.104"* %__p, %"class.draco::IndexType.104"** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_construct.144"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.123"*, %"class.std::__2::allocator.123"** %__a.addr, align 4
  %2 = load %"class.draco::IndexType.104"*, %"class.draco::IndexType.104"** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE11__constructIS5_JEEEvNS_17integral_constantIbLb1EEERS6_PT_DpOT0_(%"class.std::__2::allocator.123"* nonnull align 1 dereferenceable(1) %1, %"class.draco::IndexType.104"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"*, align 4
  store %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"** %this.addr, align 4
  %this1 = load %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"*, %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %0 = load %"class.draco::IndexType.104"*, %"class.draco::IndexType.104"** %__pos_, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::vector.118"*, %"class.std::__2::vector.118"** %__v_, align 4
  %2 = bitcast %"class.std::__2::vector.118"* %1 to %"class.std::__2::__vector_base.119"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.119", %"class.std::__2::__vector_base.119"* %2, i32 0, i32 1
  store %"class.draco::IndexType.104"* %0, %"class.draco::IndexType.104"** %__end_, align 4
  ret %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEEEEE11__constructIS5_JEEEvNS_17integral_constantIbLb1EEERS6_PT_DpOT0_(%"class.std::__2::allocator.123"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType.104"* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.123"*, align 4
  %__p.addr = alloca %"class.draco::IndexType.104"*, align 4
  store %"class.std::__2::allocator.123"* %__a, %"class.std::__2::allocator.123"** %__a.addr, align 4
  store %"class.draco::IndexType.104"* %__p, %"class.draco::IndexType.104"** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.123"*, %"class.std::__2::allocator.123"** %__a.addr, align 4
  %2 = load %"class.draco::IndexType.104"*, %"class.draco::IndexType.104"** %__p.addr, align 4
  call void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEE9constructIS4_JEEEvPT_DpOT0_(%"class.std::__2::allocator.123"* %1, %"class.draco::IndexType.104"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEEE9constructIS4_JEEEvPT_DpOT0_(%"class.std::__2::allocator.123"* %this, %"class.draco::IndexType.104"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.123"*, align 4
  %__p.addr = alloca %"class.draco::IndexType.104"*, align 4
  store %"class.std::__2::allocator.123"* %this, %"class.std::__2::allocator.123"** %this.addr, align 4
  store %"class.draco::IndexType.104"* %__p, %"class.draco::IndexType.104"** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.123"*, %"class.std::__2::allocator.123"** %this.addr, align 4
  %0 = load %"class.draco::IndexType.104"*, %"class.draco::IndexType.104"** %__p.addr, align 4
  %1 = bitcast %"class.draco::IndexType.104"* %0 to i8*
  %2 = bitcast i8* %1 to %"class.draco::IndexType.104"*
  %call = call %"class.draco::IndexType.104"* @_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEC2Ev(%"class.draco::IndexType.104"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.104"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEENS_9allocatorIS4_EEEixEm(%"class.std::__2::vector.118"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.118"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector.118"* %this, %"class.std::__2::vector.118"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.118"*, %"class.std::__2::vector.118"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.118"* %this1 to %"class.std::__2::__vector_base.119"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.119", %"class.std::__2::__vector_base.119"* %0, i32 0, i32 0
  %1 = load %"class.draco::IndexType.104"*, %"class.draco::IndexType.104"** %__begin_, align 4
  %2 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds %"class.draco::IndexType.104", %"class.draco::IndexType.104"* %1, i32 %2
  ret %"class.draco::IndexType.104"* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector.56"* @_ZNSt3__26vectorIhNS_9allocatorIhEEEC2Ev(%"class.std::__2::vector.56"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.56"*, align 4
  store %"class.std::__2::vector.56"* %this, %"class.std::__2::vector.56"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.56"*, %"class.std::__2::vector.56"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.56"* %this1 to %"class.std::__2::__vector_base.57"*
  %call = call %"class.std::__2::__vector_base.57"* @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEEC2Ev(%"class.std::__2::__vector_base.57"* %0) #8
  ret %"class.std::__2::vector.56"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base.57"* @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEEC2Ev(%"class.std::__2::__vector_base.57"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.57"*, align 4
  %ref.tmp = alloca i8*, align 4
  %ref.tmp2 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__vector_base.57"* %this, %"class.std::__2::__vector_base.57"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.57"*, %"class.std::__2::__vector_base.57"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__vector_base.57"* %this1 to %"class.std::__2::__vector_base_common"*
  %call = call %"class.std::__2::__vector_base_common"* @_ZNSt3__220__vector_base_commonILb1EEC2Ev(%"class.std::__2::__vector_base_common"* %0)
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.57", %"class.std::__2::__vector_base.57"* %this1, i32 0, i32 0
  store i8* null, i8** %__begin_, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.57", %"class.std::__2::__vector_base.57"* %this1, i32 0, i32 1
  store i8* null, i8** %__end_, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.57", %"class.std::__2::__vector_base.57"* %this1, i32 0, i32 2
  store i8* null, i8** %ref.tmp, align 4
  %call3 = call %"class.std::__2::__compressed_pair.58"* @_ZNSt3__217__compressed_pairIPhNS_9allocatorIhEEEC2IDnNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.58"* %__end_cap_, i8** nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  ret %"class.std::__2::__vector_base.57"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.58"* @_ZNSt3__217__compressed_pairIPhNS_9allocatorIhEEEC2IDnNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.58"* returned %this, i8** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.58"*, align 4
  %__t1.addr = alloca i8**, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.58"* %this, %"class.std::__2::__compressed_pair.58"** %this.addr, align 4
  store i8** %__t1, i8*** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.58"*, %"class.std::__2::__compressed_pair.58"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.58"* %this1 to %"struct.std::__2::__compressed_pair_elem.59"*
  %1 = load i8**, i8*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %1) #8
  %call2 = call %"struct.std::__2::__compressed_pair_elem.59"* @_ZNSt3__222__compressed_pair_elemIPhLi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.59"* %0, i8** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.58"* %this1 to %"struct.std::__2::__compressed_pair_elem.60"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #8
  %call4 = call %"struct.std::__2::__compressed_pair_elem.60"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIhEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.60"* %2)
  ret %"class.std::__2::__compressed_pair.58"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.59"* @_ZNSt3__222__compressed_pair_elemIPhLi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.59"* returned %this, i8** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.59"*, align 4
  %__u.addr = alloca i8**, align 4
  store %"struct.std::__2::__compressed_pair_elem.59"* %this, %"struct.std::__2::__compressed_pair_elem.59"** %this.addr, align 4
  store i8** %__u, i8*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.59"*, %"struct.std::__2::__compressed_pair_elem.59"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.59", %"struct.std::__2::__compressed_pair_elem.59"* %this1, i32 0, i32 0
  %0 = load i8**, i8*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %0) #8
  store i8* null, i8** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.59"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.60"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIhEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.60"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.60"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.60"* %this, %"struct.std::__2::__compressed_pair_elem.60"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.60"*, %"struct.std::__2::__compressed_pair_elem.60"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.60"* %this1 to %"class.std::__2::allocator.61"*
  %call = call %"class.std::__2::allocator.61"* @_ZNSt3__29allocatorIhEC2Ev(%"class.std::__2::allocator.61"* %1) #8
  ret %"struct.std::__2::__compressed_pair_elem.60"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::allocator.61"* @_ZNSt3__29allocatorIhEC2Ev(%"class.std::__2::allocator.61"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.61"*, align 4
  store %"class.std::__2::allocator.61"* %this, %"class.std::__2::allocator.61"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.61"*, %"class.std::__2::allocator.61"** %this.addr, align 4
  ret %"class.std::__2::allocator.61"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector.68"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEEC2Ev(%"class.std::__2::vector.68"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.68"*, align 4
  store %"class.std::__2::vector.68"* %this, %"class.std::__2::vector.68"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.68"* %this1 to %"class.std::__2::__vector_base.69"*
  %call = call %"class.std::__2::__vector_base.69"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEEC2Ev(%"class.std::__2::__vector_base.69"* %0) #8
  ret %"class.std::__2::vector.68"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base.69"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEEC2Ev(%"class.std::__2::__vector_base.69"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.69"*, align 4
  %ref.tmp = alloca i8*, align 4
  %ref.tmp2 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__vector_base.69"* %this, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.69"*, %"class.std::__2::__vector_base.69"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__vector_base.69"* %this1 to %"class.std::__2::__vector_base_common"*
  %call = call %"class.std::__2::__vector_base_common"* @_ZNSt3__220__vector_base_commonILb1EEC2Ev(%"class.std::__2::__vector_base_common"* %0)
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %this1, i32 0, i32 0
  store %"class.draco::IndexType"* null, %"class.draco::IndexType"** %__begin_, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %this1, i32 0, i32 1
  store %"class.draco::IndexType"* null, %"class.draco::IndexType"** %__end_, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %this1, i32 0, i32 2
  store i8* null, i8** %ref.tmp, align 4
  %call3 = call %"class.std::__2::__compressed_pair.70"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEEC2IDnNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.70"* %__end_cap_, i8** nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  ret %"class.std::__2::__vector_base.69"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.70"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEEC2IDnNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.70"* returned %this, i8** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.70"*, align 4
  %__t1.addr = alloca i8**, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.70"* %this, %"class.std::__2::__compressed_pair.70"** %this.addr, align 4
  store i8** %__t1, i8*** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.70"*, %"class.std::__2::__compressed_pair.70"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.70"* %this1 to %"struct.std::__2::__compressed_pair_elem.71"*
  %1 = load i8**, i8*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %1) #8
  %call2 = call %"struct.std::__2::__compressed_pair_elem.71"* @_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.71"* %0, i8** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.70"* %this1 to %"struct.std::__2::__compressed_pair_elem.72"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #8
  %call4 = call %"struct.std::__2::__compressed_pair_elem.72"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.72"* %2)
  ret %"class.std::__2::__compressed_pair.70"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.72"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.72"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.72"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.72"* %this, %"struct.std::__2::__compressed_pair_elem.72"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.72"*, %"struct.std::__2::__compressed_pair_elem.72"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.72"* %this1 to %"class.std::__2::allocator.73"*
  %call = call %"class.std::__2::allocator.73"* @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEC2Ev(%"class.std::__2::allocator.73"* %1) #8
  ret %"struct.std::__2::__compressed_pair_elem.72"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::allocator.73"* @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEC2Ev(%"class.std::__2::allocator.73"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.73"*, align 4
  store %"class.std::__2::allocator.73"* %this, %"class.std::__2::allocator.73"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.73"*, %"class.std::__2::allocator.73"** %this.addr, align 4
  ret %"class.std::__2::allocator.73"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIhNS_9allocatorIhEEE6assignEmRKh(%"class.std::__2::vector.56"* %this, i32 %__n, i8* nonnull align 1 dereferenceable(1) %__u) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.56"*, align 4
  %__n.addr = alloca i32, align 4
  %__u.addr = alloca i8*, align 4
  %__s = alloca i32, align 4
  store %"class.std::__2::vector.56"* %this, %"class.std::__2::vector.56"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store i8* %__u, i8** %__u.addr, align 4
  %this1 = load %"class.std::__2::vector.56"*, %"class.std::__2::vector.56"** %this.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::vector.56"* %this1) #8
  %cmp = icmp ule i32 %0, %call
  br i1 %cmp, label %if.then, label %if.else8

if.then:                                          ; preds = %entry
  %call2 = call i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4sizeEv(%"class.std::__2::vector.56"* %this1) #8
  store i32 %call2, i32* %__s, align 4
  %1 = bitcast %"class.std::__2::vector.56"* %this1 to %"class.std::__2::__vector_base.57"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.57", %"class.std::__2::__vector_base.57"* %1, i32 0, i32 0
  %2 = load i8*, i8** %__begin_, align 4
  %call3 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %__n.addr, i32* nonnull align 4 dereferenceable(4) %__s)
  %3 = load i32, i32* %call3, align 4
  %4 = load i8*, i8** %__u.addr, align 4
  %call4 = call i8* @_ZNSt3__26fill_nIPhmhEET_S2_T0_RKT1_(i8* %2, i32 %3, i8* nonnull align 1 dereferenceable(1) %4)
  %5 = load i32, i32* %__n.addr, align 4
  %6 = load i32, i32* %__s, align 4
  %cmp5 = icmp ugt i32 %5, %6
  br i1 %cmp5, label %if.then6, label %if.else

if.then6:                                         ; preds = %if.then
  %7 = load i32, i32* %__n.addr, align 4
  %8 = load i32, i32* %__s, align 4
  %sub = sub i32 %7, %8
  %9 = load i8*, i8** %__u.addr, align 4
  call void @_ZNSt3__26vectorIhNS_9allocatorIhEEE18__construct_at_endEmRKh(%"class.std::__2::vector.56"* %this1, i32 %sub, i8* nonnull align 1 dereferenceable(1) %9)
  br label %if.end

if.else:                                          ; preds = %if.then
  %10 = bitcast %"class.std::__2::vector.56"* %this1 to %"class.std::__2::__vector_base.57"*
  %__begin_7 = getelementptr inbounds %"class.std::__2::__vector_base.57", %"class.std::__2::__vector_base.57"* %10, i32 0, i32 0
  %11 = load i8*, i8** %__begin_7, align 4
  %12 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds i8, i8* %11, i32 %12
  call void @_ZNSt3__26vectorIhNS_9allocatorIhEEE17__destruct_at_endEPh(%"class.std::__2::vector.56"* %this1, i8* %add.ptr) #8
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then6
  br label %if.end10

if.else8:                                         ; preds = %entry
  call void @_ZNSt3__26vectorIhNS_9allocatorIhEEE13__vdeallocateEv(%"class.std::__2::vector.56"* %this1) #8
  %13 = load i32, i32* %__n.addr, align 4
  %call9 = call i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE11__recommendEm(%"class.std::__2::vector.56"* %this1, i32 %13)
  call void @_ZNSt3__26vectorIhNS_9allocatorIhEEE11__vallocateEm(%"class.std::__2::vector.56"* %this1, i32 %call9)
  %14 = load i32, i32* %__n.addr, align 4
  %15 = load i8*, i8** %__u.addr, align 4
  call void @_ZNSt3__26vectorIhNS_9allocatorIhEEE18__construct_at_endEmRKh(%"class.std::__2::vector.56"* %this1, i32 %14, i8* nonnull align 1 dereferenceable(1) %15)
  br label %if.end10

if.end10:                                         ; preds = %if.else8, %if.end
  call void @_ZNSt3__26vectorIhNS_9allocatorIhEEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector.56"* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__26fill_nIPhmhEET_S2_T0_RKT1_(i8* %__first, i32 %__n, i8* nonnull align 1 dereferenceable(1) %__value_) #0 comdat {
entry:
  %__first.addr = alloca i8*, align 4
  %__n.addr = alloca i32, align 4
  %__value_.addr = alloca i8*, align 4
  store i8* %__first, i8** %__first.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store i8* %__value_, i8** %__value_.addr, align 4
  %0 = load i8*, i8** %__first.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNSt3__221__convert_to_integralEm(i32 %1)
  %2 = load i8*, i8** %__value_.addr, align 4
  %call1 = call i8* @_ZNSt3__28__fill_nIPhmhEET_S2_T0_RKT1_(i8* %0, i32 %call, i8* nonnull align 1 dereferenceable(1) %2)
  ret i8* %call1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIhNS_9allocatorIhEEE18__construct_at_endEmRKh(%"class.std::__2::vector.56"* %this, i32 %__n, i8* nonnull align 1 dereferenceable(1) %__x) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.56"*, align 4
  %__n.addr = alloca i32, align 4
  %__x.addr = alloca i8*, align 4
  %__tx = alloca %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction", align 4
  store %"class.std::__2::vector.56"* %this, %"class.std::__2::vector.56"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store i8* %__x, i8** %__x.addr, align 4
  %this1 = load %"class.std::__2::vector.56"*, %"class.std::__2::vector.56"** %this.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %call = call %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"* @_ZNSt3__26vectorIhNS_9allocatorIhEEE21_ConstructTransactionC2ERS3_m(%"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"* %__tx, %"class.std::__2::vector.56"* nonnull align 4 dereferenceable(12) %this1, i32 %0)
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction", %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %1 = load i8*, i8** %__pos_, align 4
  %__new_end_ = getelementptr inbounds %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction", %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"* %__tx, i32 0, i32 2
  %2 = load i8*, i8** %__new_end_, align 4
  %cmp = icmp ne i8* %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = bitcast %"class.std::__2::vector.56"* %this1 to %"class.std::__2::__vector_base.57"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.61"* @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE7__allocEv(%"class.std::__2::__vector_base.57"* %3) #8
  %__pos_3 = getelementptr inbounds %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction", %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %4 = load i8*, i8** %__pos_3, align 4
  %call4 = call i8* @_ZNSt3__212__to_addressIhEEPT_S2_(i8* %4) #8
  %5 = load i8*, i8** %__x.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE9constructIhJRKhEEEvRS2_PT_DpOT0_(%"class.std::__2::allocator.61"* nonnull align 1 dereferenceable(1) %call2, i8* %call4, i8* nonnull align 1 dereferenceable(1) %5)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %__pos_5 = getelementptr inbounds %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction", %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %6 = load i8*, i8** %__pos_5, align 4
  %incdec.ptr = getelementptr inbounds i8, i8* %6, i32 1
  store i8* %incdec.ptr, i8** %__pos_5, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call6 = call %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"* @_ZNSt3__26vectorIhNS_9allocatorIhEEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"* %__tx) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIhNS_9allocatorIhEEE17__destruct_at_endEPh(%"class.std::__2::vector.56"* %this, i8* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.56"*, align 4
  %__new_last.addr = alloca i8*, align 4
  %__old_size = alloca i32, align 4
  store %"class.std::__2::vector.56"* %this, %"class.std::__2::vector.56"** %this.addr, align 4
  store i8* %__new_last, i8** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::vector.56"*, %"class.std::__2::vector.56"** %this.addr, align 4
  %0 = load i8*, i8** %__new_last.addr, align 4
  call void @_ZNSt3__26vectorIhNS_9allocatorIhEEE27__invalidate_iterators_pastEPh(%"class.std::__2::vector.56"* %this1, i8* %0)
  %call = call i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4sizeEv(%"class.std::__2::vector.56"* %this1) #8
  store i32 %call, i32* %__old_size, align 4
  %1 = bitcast %"class.std::__2::vector.56"* %this1 to %"class.std::__2::__vector_base.57"*
  %2 = load i8*, i8** %__new_last.addr, align 4
  call void @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE17__destruct_at_endEPh(%"class.std::__2::__vector_base.57"* %1, i8* %2) #8
  %3 = load i32, i32* %__old_size, align 4
  call void @_ZNKSt3__26vectorIhNS_9allocatorIhEEE17__annotate_shrinkEm(%"class.std::__2::vector.56"* %this1, i32 %3) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIhNS_9allocatorIhEEE13__vdeallocateEv(%"class.std::__2::vector.56"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.56"*, align 4
  store %"class.std::__2::vector.56"* %this, %"class.std::__2::vector.56"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.56"*, %"class.std::__2::vector.56"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.56"* %this1 to %"class.std::__2::__vector_base.57"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.57", %"class.std::__2::__vector_base.57"* %0, i32 0, i32 0
  %1 = load i8*, i8** %__begin_, align 4
  %cmp = icmp ne i8* %1, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__26vectorIhNS_9allocatorIhEEE5clearEv(%"class.std::__2::vector.56"* %this1) #8
  %2 = bitcast %"class.std::__2::vector.56"* %this1 to %"class.std::__2::__vector_base.57"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.61"* @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE7__allocEv(%"class.std::__2::__vector_base.57"* %2) #8
  %3 = bitcast %"class.std::__2::vector.56"* %this1 to %"class.std::__2::__vector_base.57"*
  %__begin_2 = getelementptr inbounds %"class.std::__2::__vector_base.57", %"class.std::__2::__vector_base.57"* %3, i32 0, i32 0
  %4 = load i8*, i8** %__begin_2, align 4
  %call3 = call i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::vector.56"* %this1) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE10deallocateERS2_Phm(%"class.std::__2::allocator.61"* nonnull align 1 dereferenceable(1) %call, i8* %4, i32 %call3) #8
  %5 = bitcast %"class.std::__2::vector.56"* %this1 to %"class.std::__2::__vector_base.57"*
  %call4 = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE9__end_capEv(%"class.std::__2::__vector_base.57"* %5) #8
  store i8* null, i8** %call4, align 4
  %6 = bitcast %"class.std::__2::vector.56"* %this1 to %"class.std::__2::__vector_base.57"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.57", %"class.std::__2::__vector_base.57"* %6, i32 0, i32 1
  store i8* null, i8** %__end_, align 4
  %7 = bitcast %"class.std::__2::vector.56"* %this1 to %"class.std::__2::__vector_base.57"*
  %__begin_5 = getelementptr inbounds %"class.std::__2::__vector_base.57", %"class.std::__2::__vector_base.57"* %7, i32 0, i32 0
  store i8* null, i8** %__begin_5, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIhNS_9allocatorIhEEE11__vallocateEm(%"class.std::__2::vector.56"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.56"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector.56"* %this, %"class.std::__2::vector.56"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.56"*, %"class.std::__2::vector.56"** %this.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE8max_sizeEv(%"class.std::__2::vector.56"* %this1) #8
  %cmp = icmp ugt i32 %0, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %"class.std::__2::vector.56"* %this1 to %"class.std::__2::__vector_base_common"*
  call void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"* %1) #9
  unreachable

if.end:                                           ; preds = %entry
  %2 = bitcast %"class.std::__2::vector.56"* %this1 to %"class.std::__2::__vector_base.57"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.61"* @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE7__allocEv(%"class.std::__2::__vector_base.57"* %2) #8
  %3 = load i32, i32* %__n.addr, align 4
  %call3 = call i8* @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE8allocateERS2_m(%"class.std::__2::allocator.61"* nonnull align 1 dereferenceable(1) %call2, i32 %3)
  %4 = bitcast %"class.std::__2::vector.56"* %this1 to %"class.std::__2::__vector_base.57"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.57", %"class.std::__2::__vector_base.57"* %4, i32 0, i32 1
  store i8* %call3, i8** %__end_, align 4
  %5 = bitcast %"class.std::__2::vector.56"* %this1 to %"class.std::__2::__vector_base.57"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.57", %"class.std::__2::__vector_base.57"* %5, i32 0, i32 0
  store i8* %call3, i8** %__begin_, align 4
  %6 = bitcast %"class.std::__2::vector.56"* %this1 to %"class.std::__2::__vector_base.57"*
  %__begin_4 = getelementptr inbounds %"class.std::__2::__vector_base.57", %"class.std::__2::__vector_base.57"* %6, i32 0, i32 0
  %7 = load i8*, i8** %__begin_4, align 4
  %8 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds i8, i8* %7, i32 %8
  %9 = bitcast %"class.std::__2::vector.56"* %this1 to %"class.std::__2::__vector_base.57"*
  %call5 = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE9__end_capEv(%"class.std::__2::__vector_base.57"* %9) #8
  store i8* %add.ptr, i8** %call5, align 4
  call void @_ZNKSt3__26vectorIhNS_9allocatorIhEEE14__annotate_newEm(%"class.std::__2::vector.56"* %this1, i32 0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE11__recommendEm(%"class.std::__2::vector.56"* %this, i32 %__new_size) #0 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %"class.std::__2::vector.56"*, align 4
  %__new_size.addr = alloca i32, align 4
  %__ms = alloca i32, align 4
  %__cap = alloca i32, align 4
  %ref.tmp = alloca i32, align 4
  store %"class.std::__2::vector.56"* %this, %"class.std::__2::vector.56"** %this.addr, align 4
  store i32 %__new_size, i32* %__new_size.addr, align 4
  %this1 = load %"class.std::__2::vector.56"*, %"class.std::__2::vector.56"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE8max_sizeEv(%"class.std::__2::vector.56"* %this1) #8
  store i32 %call, i32* %__ms, align 4
  %0 = load i32, i32* %__new_size.addr, align 4
  %1 = load i32, i32* %__ms, align 4
  %cmp = icmp ugt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = bitcast %"class.std::__2::vector.56"* %this1 to %"class.std::__2::__vector_base_common"*
  call void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"* %2) #9
  unreachable

if.end:                                           ; preds = %entry
  %call2 = call i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::vector.56"* %this1) #8
  store i32 %call2, i32* %__cap, align 4
  %3 = load i32, i32* %__cap, align 4
  %4 = load i32, i32* %__ms, align 4
  %div = udiv i32 %4, 2
  %cmp3 = icmp uge i32 %3, %div
  br i1 %cmp3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %if.end
  %5 = load i32, i32* %__ms, align 4
  store i32 %5, i32* %retval, align 4
  br label %return

if.end5:                                          ; preds = %if.end
  %6 = load i32, i32* %__cap, align 4
  %mul = mul i32 2, %6
  store i32 %mul, i32* %ref.tmp, align 4
  %call6 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %__new_size.addr)
  %7 = load i32, i32* %call6, align 4
  store i32 %7, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end5, %if.then4
  %8 = load i32, i32* %retval, align 4
  ret i32 %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIhNS_9allocatorIhEEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector.56"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.56"*, align 4
  store %"class.std::__2::vector.56"* %this, %"class.std::__2::vector.56"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.56"*, %"class.std::__2::vector.56"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__28__fill_nIPhmhEET_S2_T0_RKT1_(i8* %__first, i32 %__n, i8* nonnull align 1 dereferenceable(1) %__value_) #0 comdat {
entry:
  %__first.addr = alloca i8*, align 4
  %__n.addr = alloca i32, align 4
  %__value_.addr = alloca i8*, align 4
  store i8* %__first, i8** %__first.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store i8* %__value_, i8** %__value_.addr, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %__n.addr, align 4
  %cmp = icmp ugt i32 %0, 0
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %1 = load i8*, i8** %__value_.addr, align 4
  %2 = load i8, i8* %1, align 1
  %3 = load i8*, i8** %__first.addr, align 4
  store i8 %2, i8* %3, align 1
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %4 = load i8*, i8** %__first.addr, align 4
  %incdec.ptr = getelementptr inbounds i8, i8* %4, i32 1
  store i8* %incdec.ptr, i8** %__first.addr, align 4
  %5 = load i32, i32* %__n.addr, align 4
  %dec = add i32 %5, -1
  store i32 %dec, i32* %__n.addr, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %6 = load i8*, i8** %__first.addr, align 4
  ret i8* %6
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__221__convert_to_integralEm(i32 %__val) #0 comdat {
entry:
  %__val.addr = alloca i32, align 4
  store i32 %__val, i32* %__val.addr, align 4
  %0 = load i32, i32* %__val.addr, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"* @_ZNSt3__26vectorIhNS_9allocatorIhEEE21_ConstructTransactionC2ERS3_m(%"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"* returned %this, %"class.std::__2::vector.56"* nonnull align 4 dereferenceable(12) %__v, i32 %__n) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"*, align 4
  %__v.addr = alloca %"class.std::__2::vector.56"*, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"** %this.addr, align 4
  store %"class.std::__2::vector.56"* %__v, %"class.std::__2::vector.56"** %__v.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"*, %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"** %this.addr, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction", %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::vector.56"*, %"class.std::__2::vector.56"** %__v.addr, align 4
  store %"class.std::__2::vector.56"* %0, %"class.std::__2::vector.56"** %__v_, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction", %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %1 = load %"class.std::__2::vector.56"*, %"class.std::__2::vector.56"** %__v.addr, align 4
  %2 = bitcast %"class.std::__2::vector.56"* %1 to %"class.std::__2::__vector_base.57"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.57", %"class.std::__2::__vector_base.57"* %2, i32 0, i32 1
  %3 = load i8*, i8** %__end_, align 4
  store i8* %3, i8** %__pos_, align 4
  %__new_end_ = getelementptr inbounds %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction", %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"* %this1, i32 0, i32 2
  %4 = load %"class.std::__2::vector.56"*, %"class.std::__2::vector.56"** %__v.addr, align 4
  %5 = bitcast %"class.std::__2::vector.56"* %4 to %"class.std::__2::__vector_base.57"*
  %__end_2 = getelementptr inbounds %"class.std::__2::__vector_base.57", %"class.std::__2::__vector_base.57"* %5, i32 0, i32 1
  %6 = load i8*, i8** %__end_2, align 4
  %7 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds i8, i8* %6, i32 %7
  store i8* %add.ptr, i8** %__new_end_, align 4
  ret %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE9constructIhJRKhEEEvRS2_PT_DpOT0_(%"class.std::__2::allocator.61"* nonnull align 1 dereferenceable(1) %__a, i8* %__p, i8* nonnull align 1 dereferenceable(1) %__args) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.61"*, align 4
  %__p.addr = alloca i8*, align 4
  %__args.addr = alloca i8*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_construct.145", align 1
  store %"class.std::__2::allocator.61"* %__a, %"class.std::__2::allocator.61"** %__a.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  store i8* %__args, i8** %__args.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_construct.145"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.61"*, %"class.std::__2::allocator.61"** %__a.addr, align 4
  %2 = load i8*, i8** %__p.addr, align 4
  %3 = load i8*, i8** %__args.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) i8* @_ZNSt3__27forwardIRKhEEOT_RNS_16remove_referenceIS3_E4typeE(i8* nonnull align 1 dereferenceable(1) %3) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE11__constructIhJRKhEEEvNS_17integral_constantIbLb1EEERS2_PT_DpOT0_(%"class.std::__2::allocator.61"* nonnull align 1 dereferenceable(1) %1, i8* %2, i8* nonnull align 1 dereferenceable(1) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"* @_ZNSt3__26vectorIhNS_9allocatorIhEEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"*, align 4
  store %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"** %this.addr, align 4
  %this1 = load %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"*, %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction", %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %0 = load i8*, i8** %__pos_, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction", %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::vector.56"*, %"class.std::__2::vector.56"** %__v_, align 4
  %2 = bitcast %"class.std::__2::vector.56"* %1 to %"class.std::__2::__vector_base.57"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.57", %"class.std::__2::__vector_base.57"* %2, i32 0, i32 1
  store i8* %0, i8** %__end_, align 4
  ret %"struct.std::__2::vector<unsigned char, std::__2::allocator<unsigned char>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE11__constructIhJRKhEEEvNS_17integral_constantIbLb1EEERS2_PT_DpOT0_(%"class.std::__2::allocator.61"* nonnull align 1 dereferenceable(1) %__a, i8* %__p, i8* nonnull align 1 dereferenceable(1) %__args) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.61"*, align 4
  %__p.addr = alloca i8*, align 4
  %__args.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.61"* %__a, %"class.std::__2::allocator.61"** %__a.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  store i8* %__args, i8** %__args.addr, align 4
  %1 = load %"class.std::__2::allocator.61"*, %"class.std::__2::allocator.61"** %__a.addr, align 4
  %2 = load i8*, i8** %__p.addr, align 4
  %3 = load i8*, i8** %__args.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) i8* @_ZNSt3__27forwardIRKhEEOT_RNS_16remove_referenceIS3_E4typeE(i8* nonnull align 1 dereferenceable(1) %3) #8
  call void @_ZNSt3__29allocatorIhE9constructIhJRKhEEEvPT_DpOT0_(%"class.std::__2::allocator.61"* %1, i8* %2, i8* nonnull align 1 dereferenceable(1) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) i8* @_ZNSt3__27forwardIRKhEEOT_RNS_16remove_referenceIS3_E4typeE(i8* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca i8*, align 4
  store i8* %__t, i8** %__t.addr, align 4
  %0 = load i8*, i8** %__t.addr, align 4
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIhE9constructIhJRKhEEEvPT_DpOT0_(%"class.std::__2::allocator.61"* %this, i8* %__p, i8* nonnull align 1 dereferenceable(1) %__args) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.61"*, align 4
  %__p.addr = alloca i8*, align 4
  %__args.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.61"* %this, %"class.std::__2::allocator.61"** %this.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  store i8* %__args, i8** %__args.addr, align 4
  %this1 = load %"class.std::__2::allocator.61"*, %"class.std::__2::allocator.61"** %this.addr, align 4
  %0 = load i8*, i8** %__p.addr, align 4
  %1 = load i8*, i8** %__args.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) i8* @_ZNSt3__27forwardIRKhEEOT_RNS_16remove_referenceIS3_E4typeE(i8* nonnull align 1 dereferenceable(1) %1) #8
  %2 = load i8, i8* %call, align 1
  store i8 %2, i8* %0, align 1
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIhNS_9allocatorIhEEE27__invalidate_iterators_pastEPh(%"class.std::__2::vector.56"* %this, i8* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.56"*, align 4
  %__new_last.addr = alloca i8*, align 4
  store %"class.std::__2::vector.56"* %this, %"class.std::__2::vector.56"** %this.addr, align 4
  store i8* %__new_last, i8** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::vector.56"*, %"class.std::__2::vector.56"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIhNS_9allocatorIhEEE17__annotate_shrinkEm(%"class.std::__2::vector.56"* %this, i32 %__old_size) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.56"*, align 4
  %__old_size.addr = alloca i32, align 4
  store %"class.std::__2::vector.56"* %this, %"class.std::__2::vector.56"** %this.addr, align 4
  store i32 %__old_size, i32* %__old_size.addr, align 4
  %this1 = load %"class.std::__2::vector.56"*, %"class.std::__2::vector.56"** %this.addr, align 4
  %call = call i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector.56"* %this1) #8
  %call2 = call i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector.56"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::vector.56"* %this1) #8
  %add.ptr = getelementptr inbounds i8, i8* %call2, i32 %call3
  %call4 = call i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector.56"* %this1) #8
  %0 = load i32, i32* %__old_size.addr, align 4
  %add.ptr5 = getelementptr inbounds i8, i8* %call4, i32 %0
  %call6 = call i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector.56"* %this1) #8
  %call7 = call i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4sizeEv(%"class.std::__2::vector.56"* %this1) #8
  %add.ptr8 = getelementptr inbounds i8, i8* %call6, i32 %call7
  call void @_ZNKSt3__26vectorIhNS_9allocatorIhEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector.56"* %this1, i8* %call, i8* %add.ptr, i8* %add.ptr5, i8* %add.ptr8) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIhNS_9allocatorIhEEE5clearEv(%"class.std::__2::vector.56"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.56"*, align 4
  %__old_size = alloca i32, align 4
  store %"class.std::__2::vector.56"* %this, %"class.std::__2::vector.56"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.56"*, %"class.std::__2::vector.56"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4sizeEv(%"class.std::__2::vector.56"* %this1) #8
  store i32 %call, i32* %__old_size, align 4
  %0 = bitcast %"class.std::__2::vector.56"* %this1 to %"class.std::__2::__vector_base.57"*
  call void @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE5clearEv(%"class.std::__2::__vector_base.57"* %0) #8
  %1 = load i32, i32* %__old_size, align 4
  call void @_ZNKSt3__26vectorIhNS_9allocatorIhEEE17__annotate_shrinkEm(%"class.std::__2::vector.56"* %this1, i32 %1) #8
  call void @_ZNSt3__26vectorIhNS_9allocatorIhEEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector.56"* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE9__end_capEv(%"class.std::__2::__vector_base.57"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.57"*, align 4
  store %"class.std::__2::__vector_base.57"* %this, %"class.std::__2::__vector_base.57"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.57"*, %"class.std::__2::__vector_base.57"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.57", %"class.std::__2::__vector_base.57"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__217__compressed_pairIPhNS_9allocatorIhEEE5firstEv(%"class.std::__2::__compressed_pair.58"* %__end_cap_) #8
  ret i8** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNSt3__217__compressed_pairIPhNS_9allocatorIhEEE5firstEv(%"class.std::__2::__compressed_pair.58"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.58"*, align 4
  store %"class.std::__2::__compressed_pair.58"* %this, %"class.std::__2::__compressed_pair.58"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.58"*, %"class.std::__2::__compressed_pair.58"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.58"* %this1 to %"struct.std::__2::__compressed_pair_elem.59"*
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__222__compressed_pair_elemIPhLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.59"* %0) #8
  ret i8** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNSt3__222__compressed_pair_elemIPhLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.59"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.59"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.59"* %this, %"struct.std::__2::__compressed_pair_elem.59"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.59"*, %"struct.std::__2::__compressed_pair_elem.59"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.59", %"struct.std::__2::__compressed_pair_elem.59"* %this1, i32 0, i32 0
  ret i8** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE8max_sizeEv(%"class.std::__2::vector.56"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.56"*, align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp3 = alloca i32, align 4
  store %"class.std::__2::vector.56"* %this, %"class.std::__2::vector.56"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.56"*, %"class.std::__2::vector.56"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.56"* %this1 to %"class.std::__2::__vector_base.57"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.61"* @_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE7__allocEv(%"class.std::__2::__vector_base.57"* %0) #8
  %call2 = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE8max_sizeERKS2_(%"class.std::__2::allocator.61"* nonnull align 1 dereferenceable(1) %call) #8
  store i32 %call2, i32* %ref.tmp, align 4
  %call4 = call i32 @_ZNSt3__214numeric_limitsIlE3maxEv() #8
  store i32 %call4, i32* %ref.tmp3, align 4
  %call5 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %1 = load i32, i32* %call5, align 4
  ret i32 %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE8allocateERS2_m(%"class.std::__2::allocator.61"* nonnull align 1 dereferenceable(1) %__a, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.61"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.61"* %__a, %"class.std::__2::allocator.61"** %__a.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.61"*, %"class.std::__2::allocator.61"** %__a.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call i8* @_ZNSt3__29allocatorIhE8allocateEmPKv(%"class.std::__2::allocator.61"* %0, i32 %1, i8* null)
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIhNS_9allocatorIhEEE14__annotate_newEm(%"class.std::__2::vector.56"* %this, i32 %__current_size) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.56"*, align 4
  %__current_size.addr = alloca i32, align 4
  store %"class.std::__2::vector.56"* %this, %"class.std::__2::vector.56"** %this.addr, align 4
  store i32 %__current_size, i32* %__current_size.addr, align 4
  %this1 = load %"class.std::__2::vector.56"*, %"class.std::__2::vector.56"** %this.addr, align 4
  %call = call i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector.56"* %this1) #8
  %call2 = call i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector.56"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::vector.56"* %this1) #8
  %add.ptr = getelementptr inbounds i8, i8* %call2, i32 %call3
  %call4 = call i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector.56"* %this1) #8
  %call5 = call i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::vector.56"* %this1) #8
  %add.ptr6 = getelementptr inbounds i8, i8* %call4, i32 %call5
  %call7 = call i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector.56"* %this1) #8
  %0 = load i32, i32* %__current_size.addr, align 4
  %add.ptr8 = getelementptr inbounds i8, i8* %call7, i32 %0
  call void @_ZNKSt3__26vectorIhNS_9allocatorIhEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector.56"* %this1, i8* %call, i8* %add.ptr, i8* %add.ptr6, i8* %add.ptr8) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE8max_sizeERKS2_(%"class.std::__2::allocator.61"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.61"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_max_size.146", align 1
  store %"class.std::__2::allocator.61"* %__a, %"class.std::__2::allocator.61"** %__a.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_max_size.146"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.61"*, %"class.std::__2::allocator.61"** %__a.addr, align 4
  %call = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE10__max_sizeENS_17integral_constantIbLb1EEERKS2_(%"class.std::__2::allocator.61"* nonnull align 1 dereferenceable(1) %1) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.61"* @_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE7__allocEv(%"class.std::__2::__vector_base.57"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.57"*, align 4
  store %"class.std::__2::__vector_base.57"* %this, %"class.std::__2::__vector_base.57"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.57"*, %"class.std::__2::__vector_base.57"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.57", %"class.std::__2::__vector_base.57"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.61"* @_ZNKSt3__217__compressed_pairIPhNS_9allocatorIhEEE6secondEv(%"class.std::__2::__compressed_pair.58"* %__end_cap_) #8
  ret %"class.std::__2::allocator.61"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE10__max_sizeENS_17integral_constantIbLb1EEERKS2_(%"class.std::__2::allocator.61"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.61"*, align 4
  store %"class.std::__2::allocator.61"* %__a, %"class.std::__2::allocator.61"** %__a.addr, align 4
  %1 = load %"class.std::__2::allocator.61"*, %"class.std::__2::allocator.61"** %__a.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorIhE8max_sizeEv(%"class.std::__2::allocator.61"* %1) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__29allocatorIhE8max_sizeEv(%"class.std::__2::allocator.61"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.61"*, align 4
  store %"class.std::__2::allocator.61"* %this, %"class.std::__2::allocator.61"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.61"*, %"class.std::__2::allocator.61"** %this.addr, align 4
  ret i32 -1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.61"* @_ZNKSt3__217__compressed_pairIPhNS_9allocatorIhEEE6secondEv(%"class.std::__2::__compressed_pair.58"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.58"*, align 4
  store %"class.std::__2::__compressed_pair.58"* %this, %"class.std::__2::__compressed_pair.58"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.58"*, %"class.std::__2::__compressed_pair.58"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.58"* %this1 to %"struct.std::__2::__compressed_pair_elem.60"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.61"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorIhEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.60"* %0) #8
  ret %"class.std::__2::allocator.61"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.61"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorIhEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.60"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.60"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.60"* %this, %"struct.std::__2::__compressed_pair_elem.60"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.60"*, %"struct.std::__2::__compressed_pair_elem.60"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.60"* %this1 to %"class.std::__2::allocator.61"*
  ret %"class.std::__2::allocator.61"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__29allocatorIhE8allocateEmPKv(%"class.std::__2::allocator.61"* %this, i32 %__n, i8* %0) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.61"*, align 4
  %__n.addr = alloca i32, align 4
  %.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.61"* %this, %"class.std::__2::allocator.61"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::allocator.61"*, %"class.std::__2::allocator.61"** %this.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorIhE8max_sizeEv(%"class.std::__2::allocator.61"* %this1) #8
  %cmp = icmp ugt i32 %1, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__220__throw_length_errorEPKc(i8* getelementptr inbounds ([68 x i8], [68 x i8]* @.str, i32 0, i32 0)) #9
  unreachable

if.end:                                           ; preds = %entry
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 1
  %call2 = call i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %mul, i32 1)
  ret i8* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5clearEv(%"class.std::__2::vector.68"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.68"*, align 4
  %__old_size = alloca i32, align 4
  store %"class.std::__2::vector.68"* %this, %"class.std::__2::vector.68"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.68"* %this1) #8
  store i32 %call, i32* %__old_size, align 4
  %0 = bitcast %"class.std::__2::vector.68"* %this1 to %"class.std::__2::__vector_base.69"*
  call void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5clearEv(%"class.std::__2::__vector_base.69"* %0) #8
  %1 = load i32, i32* %__old_size, align 4
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_shrinkEm(%"class.std::__2::vector.68"* %this1, i32 %1) #8
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector.68"* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE6resizeEm(%"class.std::__2::vector.68"* %this, i32 %__sz) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.68"*, align 4
  %__sz.addr = alloca i32, align 4
  %__cs = alloca i32, align 4
  store %"class.std::__2::vector.68"* %this, %"class.std::__2::vector.68"** %this.addr, align 4
  store i32 %__sz, i32* %__sz.addr, align 4
  %this1 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.68"* %this1) #8
  store i32 %call, i32* %__cs, align 4
  %0 = load i32, i32* %__cs, align 4
  %1 = load i32, i32* %__sz.addr, align 4
  %cmp = icmp ult i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %__sz.addr, align 4
  %3 = load i32, i32* %__cs, align 4
  %sub = sub i32 %2, %3
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8__appendEm(%"class.std::__2::vector.68"* %this1, i32 %sub)
  br label %if.end4

if.else:                                          ; preds = %entry
  %4 = load i32, i32* %__cs, align 4
  %5 = load i32, i32* %__sz.addr, align 4
  %cmp2 = icmp ugt i32 %4, %5
  br i1 %cmp2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.else
  %6 = bitcast %"class.std::__2::vector.68"* %this1 to %"class.std::__2::__vector_base.69"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %6, i32 0, i32 0
  %7 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  %8 = load i32, i32* %__sz.addr, align 4
  %add.ptr = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %7, i32 %8
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_(%"class.std::__2::vector.68"* %this1, %"class.draco::IndexType"* %add.ptr) #8
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.else
  br label %if.end4

if.end4:                                          ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8__appendEm(%"class.std::__2::vector.68"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.68"*, align 4
  %__n.addr = alloca i32, align 4
  %__a = alloca %"class.std::__2::allocator.73"*, align 4
  %__v = alloca %"struct.std::__2::__split_buffer.130", align 4
  store %"class.std::__2::vector.68"* %this, %"class.std::__2::vector.68"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.68"* %this1 to %"class.std::__2::__vector_base.69"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base.69"* %0) #8
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %call, align 4
  %2 = bitcast %"class.std::__2::vector.68"* %this1 to %"class.std::__2::__vector_base.69"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.69", %"class.std::__2::__vector_base.69"* %2, i32 0, i32 1
  %3 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__end_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.draco::IndexType"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.draco::IndexType"* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  %4 = load i32, i32* %__n.addr, align 4
  %cmp = icmp uge i32 %sub.ptr.div, %4
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %5 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE18__construct_at_endEm(%"class.std::__2::vector.68"* %this1, i32 %5)
  br label %if.end

if.else:                                          ; preds = %entry
  %6 = bitcast %"class.std::__2::vector.68"* %this1 to %"class.std::__2::__vector_base.69"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.69"* %6) #8
  store %"class.std::__2::allocator.73"* %call2, %"class.std::__2::allocator.73"** %__a, align 4
  %call3 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.68"* %this1) #8
  %7 = load i32, i32* %__n.addr, align 4
  %add = add i32 %call3, %7
  %call4 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE11__recommendEm(%"class.std::__2::vector.68"* %this1, i32 %add)
  %call5 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.68"* %this1) #8
  %8 = load %"class.std::__2::allocator.73"*, %"class.std::__2::allocator.73"** %__a, align 4
  %call6 = call %"struct.std::__2::__split_buffer.130"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEEC2EmmS7_(%"struct.std::__2::__split_buffer.130"* %__v, i32 %call4, i32 %call5, %"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %8)
  %9 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE18__construct_at_endEm(%"struct.std::__2::__split_buffer.130"* %__v, i32 %9)
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS4_RS6_EE(%"class.std::__2::vector.68"* %this1, %"struct.std::__2::__split_buffer.130"* nonnull align 4 dereferenceable(20) %__v)
  %call7 = call %"struct.std::__2::__split_buffer.130"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEED2Ev(%"struct.std::__2::__split_buffer.130"* %__v) #8
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE18__construct_at_endEm(%"class.std::__2::vector.68"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.68"*, align 4
  %__n.addr = alloca i32, align 4
  %__tx = alloca %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction", align 4
  store %"class.std::__2::vector.68"* %this, %"class.std::__2::vector.68"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.68"*, %"class.std::__2::vector.68"** %this.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %call = call %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionC2ERS7_m(%"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %__tx, %"class.std::__2::vector.68"* nonnull align 4 dereferenceable(12) %this1, i32 %0)
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__pos_, align 4
  %__new_end_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %__tx, i32 0, i32 2
  %2 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__new_end_, align 4
  %cmp = icmp ne %"class.draco::IndexType"* %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = bitcast %"class.std::__2::vector.68"* %this1 to %"class.std::__2::__vector_base.69"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.69"* %3) #8
  %__pos_3 = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %4 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__pos_3, align 4
  %call4 = call %"class.draco::IndexType"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType"* %4) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE9constructIS5_JEEEvRS6_PT_DpOT0_(%"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %call2, %"class.draco::IndexType"* %call4)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %__pos_5 = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %5 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__pos_5, align 4
  %incdec.ptr = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %5, i32 1
  store %"class.draco::IndexType"* %incdec.ptr, %"class.draco::IndexType"** %__pos_5, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call6 = call %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %__tx) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE18__construct_at_endEm(%"struct.std::__2::__split_buffer.130"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.130"*, align 4
  %__n.addr = alloca i32, align 4
  %__tx = alloca %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction", align 4
  store %"struct.std::__2::__split_buffer.130"* %this, %"struct.std::__2::__split_buffer.130"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.130"*, %"struct.std::__2::__split_buffer.130"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer.130", %"struct.std::__2::__split_buffer.130"* %this1, i32 0, i32 2
  %0 = load i32, i32* %__n.addr, align 4
  %call = call %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE21_ConstructTransactionC2EPPS4_m(%"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"* %__tx, %"class.draco::IndexType"** %__end_, i32 %0) #8
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %__pos_ = getelementptr inbounds %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"* %__tx, i32 0, i32 0
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__pos_, align 4
  %__end_2 = getelementptr inbounds %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %2 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__end_2, align 4
  %cmp = icmp ne %"class.draco::IndexType"* %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %call3 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.73"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE7__allocEv(%"struct.std::__2::__split_buffer.130"* %this1) #8
  %__pos_4 = getelementptr inbounds %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"* %__tx, i32 0, i32 0
  %3 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__pos_4, align 4
  %call5 = call %"class.draco::IndexType"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType"* %3) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE9constructIS5_JEEEvRS6_PT_DpOT0_(%"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %call3, %"class.draco::IndexType"* %call5)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %__pos_6 = getelementptr inbounds %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"* %__tx, i32 0, i32 0
  %4 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__pos_6, align 4
  %incdec.ptr = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %4, i32 1
  store %"class.draco::IndexType"* %incdec.ptr, %"class.draco::IndexType"** %__pos_6, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call7 = call %"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::__split_buffer<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>> &>::_ConstructTransaction"* %__tx) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE9constructIS5_JEEEvRS6_PT_DpOT0_(%"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType"* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.73"*, align 4
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_construct.147", align 1
  store %"class.std::__2::allocator.73"* %__a, %"class.std::__2::allocator.73"** %__a.addr, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_construct.147"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.73"*, %"class.std::__2::allocator.73"** %__a.addr, align 4
  %2 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE11__constructIS5_JEEEvNS_17integral_constantIbLb1EEERS6_PT_DpOT0_(%"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %1, %"class.draco::IndexType"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE11__constructIS5_JEEEvNS_17integral_constantIbLb1EEERS6_PT_DpOT0_(%"class.std::__2::allocator.73"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType"* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.73"*, align 4
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.std::__2::allocator.73"* %__a, %"class.std::__2::allocator.73"** %__a.addr, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.73"*, %"class.std::__2::allocator.73"** %__a.addr, align 4
  %2 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  call void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE9constructIS4_JEEEvPT_DpOT0_(%"class.std::__2::allocator.73"* %1, %"class.draco::IndexType"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE9constructIS4_JEEEvPT_DpOT0_(%"class.std::__2::allocator.73"* %this, %"class.draco::IndexType"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.73"*, align 4
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.std::__2::allocator.73"* %this, %"class.std::__2::allocator.73"** %this.addr, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.73"*, %"class.std::__2::allocator.73"** %this.addr, align 4
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  %1 = bitcast %"class.draco::IndexType"* %0 to i8*
  %2 = bitcast i8* %1 to %"class.draco::IndexType"*
  %call = call %"class.draco::IndexType"* @_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEC2Ev(%"class.draco::IndexType"* %2)
  ret void
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn }
attributes #3 = { argmemonly nounwind willreturn writeonly }
attributes #4 = { noreturn "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { noinline noreturn nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nobuiltin allocsize(0) "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #8 = { nounwind }
attributes #9 = { noreturn }
attributes #10 = { builtin allocsize(0) }
attributes #11 = { builtin nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
