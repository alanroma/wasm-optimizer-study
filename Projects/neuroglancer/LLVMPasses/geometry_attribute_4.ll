; ModuleID = './draco/src/draco/attributes/geometry_attribute.cc'
source_filename = "./draco/src/draco/attributes/geometry_attribute.cc"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%"class.draco::GeometryAttribute" = type { %"class.draco::DataBuffer"*, %"struct.draco::DataBufferDescriptor", i8, i32, i8, i64, i64, i32, i32 }
%"class.draco::DataBuffer" = type { %"class.std::__2::vector", %"struct.draco::DataBufferDescriptor" }
%"class.std::__2::vector" = type { %"class.std::__2::__vector_base" }
%"class.std::__2::__vector_base" = type { i8*, i8*, %"class.std::__2::__compressed_pair" }
%"class.std::__2::__compressed_pair" = type { %"struct.std::__2::__compressed_pair_elem" }
%"struct.std::__2::__compressed_pair_elem" = type { i8* }
%"struct.draco::DataBufferDescriptor" = type { i64, i64 }

$_ZN5draco20DataBufferDescriptorC2Ev = comdat any

$_ZNK5draco10DataBuffer9buffer_idEv = comdat any

$_ZNK5draco10DataBuffer12update_countEv = comdat any

$_ZN5draco10DataBuffer4dataEv = comdat any

$_ZNK5draco10DataBuffer9data_sizeEv = comdat any

$_ZNSt3__26vectorIhNS_9allocatorIhEEEixEm = comdat any

$_ZNKSt3__26vectorIhNS_9allocatorIhEEE4sizeEv = comdat any

@_ZN5draco17GeometryAttributeC1Ev = hidden unnamed_addr alias %"class.draco::GeometryAttribute"* (%"class.draco::GeometryAttribute"*), %"class.draco::GeometryAttribute"* (%"class.draco::GeometryAttribute"*)* @_ZN5draco17GeometryAttributeC2Ev

; Function Attrs: noinline nounwind optnone
define hidden %"class.draco::GeometryAttribute"* @_ZN5draco17GeometryAttributeC2Ev(%"class.draco::GeometryAttribute"* returned %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %"class.draco::GeometryAttribute"*, align 4
  store %"class.draco::GeometryAttribute"* %this, %"class.draco::GeometryAttribute"** %this.addr, align 4
  %this1 = load %"class.draco::GeometryAttribute"*, %"class.draco::GeometryAttribute"** %this.addr, align 4
  %buffer_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 0
  store %"class.draco::DataBuffer"* null, %"class.draco::DataBuffer"** %buffer_, align 8
  %buffer_descriptor_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 1
  %call = call %"struct.draco::DataBufferDescriptor"* @_ZN5draco20DataBufferDescriptorC2Ev(%"struct.draco::DataBufferDescriptor"* %buffer_descriptor_)
  %num_components_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 2
  store i8 1, i8* %num_components_, align 8
  %data_type_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 3
  store i32 9, i32* %data_type_, align 4
  %byte_stride_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 5
  store i64 0, i64* %byte_stride_, align 8
  %byte_offset_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 6
  store i64 0, i64* %byte_offset_, align 8
  %attribute_type_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 7
  store i32 -1, i32* %attribute_type_, align 8
  %unique_id_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 8
  store i32 0, i32* %unique_id_, align 4
  ret %"class.draco::GeometryAttribute"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.draco::DataBufferDescriptor"* @_ZN5draco20DataBufferDescriptorC2Ev(%"struct.draco::DataBufferDescriptor"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.draco::DataBufferDescriptor"*, align 4
  store %"struct.draco::DataBufferDescriptor"* %this, %"struct.draco::DataBufferDescriptor"** %this.addr, align 4
  %this1 = load %"struct.draco::DataBufferDescriptor"*, %"struct.draco::DataBufferDescriptor"** %this.addr, align 4
  %buffer_id = getelementptr inbounds %"struct.draco::DataBufferDescriptor", %"struct.draco::DataBufferDescriptor"* %this1, i32 0, i32 0
  store i64 0, i64* %buffer_id, align 8
  %buffer_update_count = getelementptr inbounds %"struct.draco::DataBufferDescriptor", %"struct.draco::DataBufferDescriptor"* %this1, i32 0, i32 1
  store i64 0, i64* %buffer_update_count, align 8
  ret %"struct.draco::DataBufferDescriptor"* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN5draco17GeometryAttribute4InitENS0_4TypeEPNS_10DataBufferEaNS_8DataTypeEbxx(%"class.draco::GeometryAttribute"* %this, i32 %attribute_type, %"class.draco::DataBuffer"* %buffer, i8 signext %num_components, i32 %data_type, i1 zeroext %normalized, i64 %byte_stride, i64 %byte_offset) #0 {
entry:
  %this.addr = alloca %"class.draco::GeometryAttribute"*, align 4
  %attribute_type.addr = alloca i32, align 4
  %buffer.addr = alloca %"class.draco::DataBuffer"*, align 4
  %num_components.addr = alloca i8, align 1
  %data_type.addr = alloca i32, align 4
  %normalized.addr = alloca i8, align 1
  %byte_stride.addr = alloca i64, align 8
  %byte_offset.addr = alloca i64, align 8
  store %"class.draco::GeometryAttribute"* %this, %"class.draco::GeometryAttribute"** %this.addr, align 4
  store i32 %attribute_type, i32* %attribute_type.addr, align 4
  store %"class.draco::DataBuffer"* %buffer, %"class.draco::DataBuffer"** %buffer.addr, align 4
  store i8 %num_components, i8* %num_components.addr, align 1
  store i32 %data_type, i32* %data_type.addr, align 4
  %frombool = zext i1 %normalized to i8
  store i8 %frombool, i8* %normalized.addr, align 1
  store i64 %byte_stride, i64* %byte_stride.addr, align 8
  store i64 %byte_offset, i64* %byte_offset.addr, align 8
  %this1 = load %"class.draco::GeometryAttribute"*, %"class.draco::GeometryAttribute"** %this.addr, align 4
  %0 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %buffer.addr, align 4
  %buffer_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 0
  store %"class.draco::DataBuffer"* %0, %"class.draco::DataBuffer"** %buffer_, align 8
  %1 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %buffer.addr, align 4
  %tobool = icmp ne %"class.draco::DataBuffer"* %1, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %buffer.addr, align 4
  %call = call i64 @_ZNK5draco10DataBuffer9buffer_idEv(%"class.draco::DataBuffer"* %2)
  %buffer_descriptor_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 1
  %buffer_id = getelementptr inbounds %"struct.draco::DataBufferDescriptor", %"struct.draco::DataBufferDescriptor"* %buffer_descriptor_, i32 0, i32 0
  store i64 %call, i64* %buffer_id, align 8
  %3 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %buffer.addr, align 4
  %call2 = call i64 @_ZNK5draco10DataBuffer12update_countEv(%"class.draco::DataBuffer"* %3)
  %buffer_descriptor_3 = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 1
  %buffer_update_count = getelementptr inbounds %"struct.draco::DataBufferDescriptor", %"struct.draco::DataBufferDescriptor"* %buffer_descriptor_3, i32 0, i32 1
  store i64 %call2, i64* %buffer_update_count, align 8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %4 = load i8, i8* %num_components.addr, align 1
  %num_components_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 2
  store i8 %4, i8* %num_components_, align 8
  %5 = load i32, i32* %data_type.addr, align 4
  %data_type_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 3
  store i32 %5, i32* %data_type_, align 4
  %6 = load i8, i8* %normalized.addr, align 1
  %tobool4 = trunc i8 %6 to i1
  %normalized_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 4
  %frombool5 = zext i1 %tobool4 to i8
  store i8 %frombool5, i8* %normalized_, align 8
  %7 = load i64, i64* %byte_stride.addr, align 8
  %byte_stride_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 5
  store i64 %7, i64* %byte_stride_, align 8
  %8 = load i64, i64* %byte_offset.addr, align 8
  %byte_offset_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 6
  store i64 %8, i64* %byte_offset_, align 8
  %9 = load i32, i32* %attribute_type.addr, align 4
  %attribute_type_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 7
  store i32 %9, i32* %attribute_type_, align 8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i64 @_ZNK5draco10DataBuffer9buffer_idEv(%"class.draco::DataBuffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::DataBuffer"*, align 4
  store %"class.draco::DataBuffer"* %this, %"class.draco::DataBuffer"** %this.addr, align 4
  %this1 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %this.addr, align 4
  %descriptor_ = getelementptr inbounds %"class.draco::DataBuffer", %"class.draco::DataBuffer"* %this1, i32 0, i32 1
  %buffer_id = getelementptr inbounds %"struct.draco::DataBufferDescriptor", %"struct.draco::DataBufferDescriptor"* %descriptor_, i32 0, i32 0
  %0 = load i64, i64* %buffer_id, align 8
  ret i64 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i64 @_ZNK5draco10DataBuffer12update_countEv(%"class.draco::DataBuffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::DataBuffer"*, align 4
  store %"class.draco::DataBuffer"* %this, %"class.draco::DataBuffer"** %this.addr, align 4
  %this1 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %this.addr, align 4
  %descriptor_ = getelementptr inbounds %"class.draco::DataBuffer", %"class.draco::DataBuffer"* %this1, i32 0, i32 1
  %buffer_update_count = getelementptr inbounds %"struct.draco::DataBufferDescriptor", %"struct.draco::DataBufferDescriptor"* %descriptor_, i32 0, i32 1
  %0 = load i64, i64* %buffer_update_count, align 8
  ret i64 %0
}

; Function Attrs: noinline nounwind optnone
define hidden zeroext i1 @_ZN5draco17GeometryAttribute8CopyFromERKS0_(%"class.draco::GeometryAttribute"* %this, %"class.draco::GeometryAttribute"* nonnull align 8 dereferenceable(64) %src_att) #0 {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::GeometryAttribute"*, align 4
  %src_att.addr = alloca %"class.draco::GeometryAttribute"*, align 4
  store %"class.draco::GeometryAttribute"* %this, %"class.draco::GeometryAttribute"** %this.addr, align 4
  store %"class.draco::GeometryAttribute"* %src_att, %"class.draco::GeometryAttribute"** %src_att.addr, align 4
  %this1 = load %"class.draco::GeometryAttribute"*, %"class.draco::GeometryAttribute"** %this.addr, align 4
  %buffer_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 0
  %0 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %buffer_, align 8
  %cmp = icmp eq %"class.draco::DataBuffer"* %0, null
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %1 = load %"class.draco::GeometryAttribute"*, %"class.draco::GeometryAttribute"** %src_att.addr, align 4
  %buffer_2 = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %1, i32 0, i32 0
  %2 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %buffer_2, align 8
  %cmp3 = icmp eq %"class.draco::DataBuffer"* %2, null
  br i1 %cmp3, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  %buffer_4 = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 0
  %3 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %buffer_4, align 8
  %4 = load %"class.draco::GeometryAttribute"*, %"class.draco::GeometryAttribute"** %src_att.addr, align 4
  %buffer_5 = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %4, i32 0, i32 0
  %5 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %buffer_5, align 8
  %call = call i8* @_ZN5draco10DataBuffer4dataEv(%"class.draco::DataBuffer"* %5)
  %6 = load %"class.draco::GeometryAttribute"*, %"class.draco::GeometryAttribute"** %src_att.addr, align 4
  %buffer_6 = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %6, i32 0, i32 0
  %7 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %buffer_6, align 8
  %call7 = call i32 @_ZNK5draco10DataBuffer9data_sizeEv(%"class.draco::DataBuffer"* %7)
  %conv = zext i32 %call7 to i64
  %call8 = call zeroext i1 @_ZN5draco10DataBuffer6UpdateEPKvx(%"class.draco::DataBuffer"* %3, i8* %call, i64 %conv)
  %8 = load %"class.draco::GeometryAttribute"*, %"class.draco::GeometryAttribute"** %src_att.addr, align 4
  %num_components_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %8, i32 0, i32 2
  %9 = load i8, i8* %num_components_, align 8
  %num_components_9 = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 2
  store i8 %9, i8* %num_components_9, align 8
  %10 = load %"class.draco::GeometryAttribute"*, %"class.draco::GeometryAttribute"** %src_att.addr, align 4
  %data_type_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %10, i32 0, i32 3
  %11 = load i32, i32* %data_type_, align 4
  %data_type_10 = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 3
  store i32 %11, i32* %data_type_10, align 4
  %12 = load %"class.draco::GeometryAttribute"*, %"class.draco::GeometryAttribute"** %src_att.addr, align 4
  %normalized_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %12, i32 0, i32 4
  %13 = load i8, i8* %normalized_, align 8
  %tobool = trunc i8 %13 to i1
  %normalized_11 = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 4
  %frombool = zext i1 %tobool to i8
  store i8 %frombool, i8* %normalized_11, align 8
  %14 = load %"class.draco::GeometryAttribute"*, %"class.draco::GeometryAttribute"** %src_att.addr, align 4
  %byte_stride_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %14, i32 0, i32 5
  %15 = load i64, i64* %byte_stride_, align 8
  %byte_stride_12 = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 5
  store i64 %15, i64* %byte_stride_12, align 8
  %16 = load %"class.draco::GeometryAttribute"*, %"class.draco::GeometryAttribute"** %src_att.addr, align 4
  %byte_offset_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %16, i32 0, i32 6
  %17 = load i64, i64* %byte_offset_, align 8
  %byte_offset_13 = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 6
  store i64 %17, i64* %byte_offset_13, align 8
  %18 = load %"class.draco::GeometryAttribute"*, %"class.draco::GeometryAttribute"** %src_att.addr, align 4
  %attribute_type_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %18, i32 0, i32 7
  %19 = load i32, i32* %attribute_type_, align 8
  %attribute_type_14 = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 7
  store i32 %19, i32* %attribute_type_14, align 8
  %20 = load %"class.draco::GeometryAttribute"*, %"class.draco::GeometryAttribute"** %src_att.addr, align 4
  %buffer_descriptor_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %20, i32 0, i32 1
  %buffer_descriptor_15 = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 1
  %21 = bitcast %"struct.draco::DataBufferDescriptor"* %buffer_descriptor_15 to i8*
  %22 = bitcast %"struct.draco::DataBufferDescriptor"* %buffer_descriptor_ to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %21, i8* align 8 %22, i32 16, i1 false)
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %23 = load i1, i1* %retval, align 1
  ret i1 %23
}

declare zeroext i1 @_ZN5draco10DataBuffer6UpdateEPKvx(%"class.draco::DataBuffer"*, i8*, i64) #1

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN5draco10DataBuffer4dataEv(%"class.draco::DataBuffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::DataBuffer"*, align 4
  store %"class.draco::DataBuffer"* %this, %"class.draco::DataBuffer"** %this.addr, align 4
  %this1 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %this.addr, align 4
  %data_ = getelementptr inbounds %"class.draco::DataBuffer", %"class.draco::DataBuffer"* %this1, i32 0, i32 0
  %call = call nonnull align 1 dereferenceable(1) i8* @_ZNSt3__26vectorIhNS_9allocatorIhEEEixEm(%"class.std::__2::vector"* %data_, i32 0) #3
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco10DataBuffer9data_sizeEv(%"class.draco::DataBuffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::DataBuffer"*, align 4
  store %"class.draco::DataBuffer"* %this, %"class.draco::DataBuffer"** %this.addr, align 4
  %this1 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %this.addr, align 4
  %data_ = getelementptr inbounds %"class.draco::DataBuffer", %"class.draco::DataBuffer"* %this1, i32 0, i32 0
  %call = call i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4sizeEv(%"class.std::__2::vector"* %data_) #3
  ret i32 %call
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #2

; Function Attrs: noinline nounwind optnone
define hidden zeroext i1 @_ZNK5draco17GeometryAttributeeqERKS0_(%"class.draco::GeometryAttribute"* %this, %"class.draco::GeometryAttribute"* nonnull align 8 dereferenceable(64) %va) #0 {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::GeometryAttribute"*, align 4
  %va.addr = alloca %"class.draco::GeometryAttribute"*, align 4
  store %"class.draco::GeometryAttribute"* %this, %"class.draco::GeometryAttribute"** %this.addr, align 4
  store %"class.draco::GeometryAttribute"* %va, %"class.draco::GeometryAttribute"** %va.addr, align 4
  %this1 = load %"class.draco::GeometryAttribute"*, %"class.draco::GeometryAttribute"** %this.addr, align 4
  %attribute_type_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 7
  %0 = load i32, i32* %attribute_type_, align 8
  %1 = load %"class.draco::GeometryAttribute"*, %"class.draco::GeometryAttribute"** %va.addr, align 4
  %attribute_type_2 = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %1, i32 0, i32 7
  %2 = load i32, i32* %attribute_type_2, align 8
  %cmp = icmp ne i32 %0, %2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %buffer_descriptor_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 1
  %buffer_id = getelementptr inbounds %"struct.draco::DataBufferDescriptor", %"struct.draco::DataBufferDescriptor"* %buffer_descriptor_, i32 0, i32 0
  %3 = load i64, i64* %buffer_id, align 8
  %4 = load %"class.draco::GeometryAttribute"*, %"class.draco::GeometryAttribute"** %va.addr, align 4
  %buffer_descriptor_3 = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %4, i32 0, i32 1
  %buffer_id4 = getelementptr inbounds %"struct.draco::DataBufferDescriptor", %"struct.draco::DataBufferDescriptor"* %buffer_descriptor_3, i32 0, i32 0
  %5 = load i64, i64* %buffer_id4, align 8
  %cmp5 = icmp ne i64 %3, %5
  br i1 %cmp5, label %if.then6, label %if.end7

if.then6:                                         ; preds = %if.end
  store i1 false, i1* %retval, align 1
  br label %return

if.end7:                                          ; preds = %if.end
  %buffer_descriptor_8 = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 1
  %buffer_update_count = getelementptr inbounds %"struct.draco::DataBufferDescriptor", %"struct.draco::DataBufferDescriptor"* %buffer_descriptor_8, i32 0, i32 1
  %6 = load i64, i64* %buffer_update_count, align 8
  %7 = load %"class.draco::GeometryAttribute"*, %"class.draco::GeometryAttribute"** %va.addr, align 4
  %buffer_descriptor_9 = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %7, i32 0, i32 1
  %buffer_update_count10 = getelementptr inbounds %"struct.draco::DataBufferDescriptor", %"struct.draco::DataBufferDescriptor"* %buffer_descriptor_9, i32 0, i32 1
  %8 = load i64, i64* %buffer_update_count10, align 8
  %cmp11 = icmp ne i64 %6, %8
  br i1 %cmp11, label %if.then12, label %if.end13

if.then12:                                        ; preds = %if.end7
  store i1 false, i1* %retval, align 1
  br label %return

if.end13:                                         ; preds = %if.end7
  %num_components_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 2
  %9 = load i8, i8* %num_components_, align 8
  %conv = sext i8 %9 to i32
  %10 = load %"class.draco::GeometryAttribute"*, %"class.draco::GeometryAttribute"** %va.addr, align 4
  %num_components_14 = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %10, i32 0, i32 2
  %11 = load i8, i8* %num_components_14, align 8
  %conv15 = sext i8 %11 to i32
  %cmp16 = icmp ne i32 %conv, %conv15
  br i1 %cmp16, label %if.then17, label %if.end18

if.then17:                                        ; preds = %if.end13
  store i1 false, i1* %retval, align 1
  br label %return

if.end18:                                         ; preds = %if.end13
  %data_type_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 3
  %12 = load i32, i32* %data_type_, align 4
  %13 = load %"class.draco::GeometryAttribute"*, %"class.draco::GeometryAttribute"** %va.addr, align 4
  %data_type_19 = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %13, i32 0, i32 3
  %14 = load i32, i32* %data_type_19, align 4
  %cmp20 = icmp ne i32 %12, %14
  br i1 %cmp20, label %if.then21, label %if.end22

if.then21:                                        ; preds = %if.end18
  store i1 false, i1* %retval, align 1
  br label %return

if.end22:                                         ; preds = %if.end18
  %byte_stride_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 5
  %15 = load i64, i64* %byte_stride_, align 8
  %16 = load %"class.draco::GeometryAttribute"*, %"class.draco::GeometryAttribute"** %va.addr, align 4
  %byte_stride_23 = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %16, i32 0, i32 5
  %17 = load i64, i64* %byte_stride_23, align 8
  %cmp24 = icmp ne i64 %15, %17
  br i1 %cmp24, label %if.then25, label %if.end26

if.then25:                                        ; preds = %if.end22
  store i1 false, i1* %retval, align 1
  br label %return

if.end26:                                         ; preds = %if.end22
  %byte_offset_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 6
  %18 = load i64, i64* %byte_offset_, align 8
  %19 = load %"class.draco::GeometryAttribute"*, %"class.draco::GeometryAttribute"** %va.addr, align 4
  %byte_offset_27 = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %19, i32 0, i32 6
  %20 = load i64, i64* %byte_offset_27, align 8
  %cmp28 = icmp ne i64 %18, %20
  br i1 %cmp28, label %if.then29, label %if.end30

if.then29:                                        ; preds = %if.end26
  store i1 false, i1* %retval, align 1
  br label %return

if.end30:                                         ; preds = %if.end26
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end30, %if.then29, %if.then25, %if.then21, %if.then17, %if.then12, %if.then6, %if.then
  %21 = load i1, i1* %retval, align 1
  ret i1 %21
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN5draco17GeometryAttribute11ResetBufferEPNS_10DataBufferExx(%"class.draco::GeometryAttribute"* %this, %"class.draco::DataBuffer"* %buffer, i64 %byte_stride, i64 %byte_offset) #0 {
entry:
  %this.addr = alloca %"class.draco::GeometryAttribute"*, align 4
  %buffer.addr = alloca %"class.draco::DataBuffer"*, align 4
  %byte_stride.addr = alloca i64, align 8
  %byte_offset.addr = alloca i64, align 8
  store %"class.draco::GeometryAttribute"* %this, %"class.draco::GeometryAttribute"** %this.addr, align 4
  store %"class.draco::DataBuffer"* %buffer, %"class.draco::DataBuffer"** %buffer.addr, align 4
  store i64 %byte_stride, i64* %byte_stride.addr, align 8
  store i64 %byte_offset, i64* %byte_offset.addr, align 8
  %this1 = load %"class.draco::GeometryAttribute"*, %"class.draco::GeometryAttribute"** %this.addr, align 4
  %0 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %buffer.addr, align 4
  %buffer_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 0
  store %"class.draco::DataBuffer"* %0, %"class.draco::DataBuffer"** %buffer_, align 8
  %1 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %buffer.addr, align 4
  %call = call i64 @_ZNK5draco10DataBuffer9buffer_idEv(%"class.draco::DataBuffer"* %1)
  %buffer_descriptor_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 1
  %buffer_id = getelementptr inbounds %"struct.draco::DataBufferDescriptor", %"struct.draco::DataBufferDescriptor"* %buffer_descriptor_, i32 0, i32 0
  store i64 %call, i64* %buffer_id, align 8
  %2 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %buffer.addr, align 4
  %call2 = call i64 @_ZNK5draco10DataBuffer12update_countEv(%"class.draco::DataBuffer"* %2)
  %buffer_descriptor_3 = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 1
  %buffer_update_count = getelementptr inbounds %"struct.draco::DataBufferDescriptor", %"struct.draco::DataBufferDescriptor"* %buffer_descriptor_3, i32 0, i32 1
  store i64 %call2, i64* %buffer_update_count, align 8
  %3 = load i64, i64* %byte_stride.addr, align 8
  %byte_stride_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 5
  store i64 %3, i64* %byte_stride_, align 8
  %4 = load i64, i64* %byte_offset.addr, align 8
  %byte_offset_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 6
  store i64 %4, i64* %byte_offset_, align 8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) i8* @_ZNSt3__26vectorIhNS_9allocatorIhEEEixEm(%"class.std::__2::vector"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 0
  %1 = load i8*, i8** %__begin_, align 4
  %2 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds i8, i8* %1, i32 %2
  ret i8* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4sizeEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 1
  %1 = load i8*, i8** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %2, i32 0, i32 0
  %3 = load i8*, i8** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint i8* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  ret i32 %sub.ptr.sub
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn }
attributes #3 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
