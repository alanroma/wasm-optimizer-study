; ModuleID = './draco/src/draco/mesh/triangle_soup_mesh_builder.cc'
source_filename = "./draco/src/draco/mesh/triangle_soup_mesh_builder.cc"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%"class.draco::TriangleSoupMeshBuilder" = type { %"class.std::__2::vector", %"class.std::__2::unique_ptr" }
%"class.std::__2::vector" = type { %"class.std::__2::__vector_base" }
%"class.std::__2::__vector_base" = type { i8*, i8*, %"class.std::__2::__compressed_pair" }
%"class.std::__2::__compressed_pair" = type { %"struct.std::__2::__compressed_pair_elem" }
%"struct.std::__2::__compressed_pair_elem" = type { i8* }
%"class.std::__2::unique_ptr" = type { %"class.std::__2::__compressed_pair.1" }
%"class.std::__2::__compressed_pair.1" = type { %"struct.std::__2::__compressed_pair_elem.2" }
%"struct.std::__2::__compressed_pair_elem.2" = type { %"class.draco::Mesh"* }
%"class.draco::Mesh" = type { %"class.draco::PointCloud", %"class.std::__2::vector.104", %"class.draco::IndexTypeVector.111" }
%"class.draco::PointCloud" = type { i32 (...)**, %"class.std::__2::unique_ptr.3", %"class.std::__2::vector.61", [5 x %"class.std::__2::vector.97"], i32 }
%"class.std::__2::unique_ptr.3" = type { %"class.std::__2::__compressed_pair.4" }
%"class.std::__2::__compressed_pair.4" = type { %"struct.std::__2::__compressed_pair_elem.5" }
%"struct.std::__2::__compressed_pair_elem.5" = type { %"class.draco::GeometryMetadata"* }
%"class.draco::GeometryMetadata" = type { %"class.draco::Metadata", %"class.std::__2::vector.48" }
%"class.draco::Metadata" = type { %"class.std::__2::unordered_map", %"class.std::__2::unordered_map.25" }
%"class.std::__2::unordered_map" = type { %"class.std::__2::__hash_table" }
%"class.std::__2::__hash_table" = type { %"class.std::__2::unique_ptr.6", %"class.std::__2::__compressed_pair.15", %"class.std::__2::__compressed_pair.20", %"class.std::__2::__compressed_pair.22" }
%"class.std::__2::unique_ptr.6" = type { %"class.std::__2::__compressed_pair.7" }
%"class.std::__2::__compressed_pair.7" = type { %"struct.std::__2::__compressed_pair_elem.8", %"struct.std::__2::__compressed_pair_elem.9" }
%"struct.std::__2::__compressed_pair_elem.8" = type { %"struct.std::__2::__hash_node_base"** }
%"struct.std::__2::__hash_node_base" = type { %"struct.std::__2::__hash_node_base"* }
%"struct.std::__2::__compressed_pair_elem.9" = type { %"class.std::__2::__bucket_list_deallocator" }
%"class.std::__2::__bucket_list_deallocator" = type { %"class.std::__2::__compressed_pair.10" }
%"class.std::__2::__compressed_pair.10" = type { %"struct.std::__2::__compressed_pair_elem.11" }
%"struct.std::__2::__compressed_pair_elem.11" = type { i32 }
%"class.std::__2::__compressed_pair.15" = type { %"struct.std::__2::__compressed_pair_elem.16" }
%"struct.std::__2::__compressed_pair_elem.16" = type { %"struct.std::__2::__hash_node_base" }
%"class.std::__2::__compressed_pair.20" = type { %"struct.std::__2::__compressed_pair_elem.11" }
%"class.std::__2::__compressed_pair.22" = type { %"struct.std::__2::__compressed_pair_elem.23" }
%"struct.std::__2::__compressed_pair_elem.23" = type { float }
%"class.std::__2::unordered_map.25" = type { %"class.std::__2::__hash_table.26" }
%"class.std::__2::__hash_table.26" = type { %"class.std::__2::unique_ptr.27", %"class.std::__2::__compressed_pair.37", %"class.std::__2::__compressed_pair.42", %"class.std::__2::__compressed_pair.45" }
%"class.std::__2::unique_ptr.27" = type { %"class.std::__2::__compressed_pair.28" }
%"class.std::__2::__compressed_pair.28" = type { %"struct.std::__2::__compressed_pair_elem.29", %"struct.std::__2::__compressed_pair_elem.31" }
%"struct.std::__2::__compressed_pair_elem.29" = type { %"struct.std::__2::__hash_node_base.30"** }
%"struct.std::__2::__hash_node_base.30" = type { %"struct.std::__2::__hash_node_base.30"* }
%"struct.std::__2::__compressed_pair_elem.31" = type { %"class.std::__2::__bucket_list_deallocator.32" }
%"class.std::__2::__bucket_list_deallocator.32" = type { %"class.std::__2::__compressed_pair.33" }
%"class.std::__2::__compressed_pair.33" = type { %"struct.std::__2::__compressed_pair_elem.11" }
%"class.std::__2::__compressed_pair.37" = type { %"struct.std::__2::__compressed_pair_elem.38" }
%"struct.std::__2::__compressed_pair_elem.38" = type { %"struct.std::__2::__hash_node_base.30" }
%"class.std::__2::__compressed_pair.42" = type { %"struct.std::__2::__compressed_pair_elem.11" }
%"class.std::__2::__compressed_pair.45" = type { %"struct.std::__2::__compressed_pair_elem.23" }
%"class.std::__2::vector.48" = type { %"class.std::__2::__vector_base.49" }
%"class.std::__2::__vector_base.49" = type { %"class.std::__2::unique_ptr.50"*, %"class.std::__2::unique_ptr.50"*, %"class.std::__2::__compressed_pair.54" }
%"class.std::__2::unique_ptr.50" = type { %"class.std::__2::__compressed_pair.51" }
%"class.std::__2::__compressed_pair.51" = type { %"struct.std::__2::__compressed_pair_elem.52" }
%"struct.std::__2::__compressed_pair_elem.52" = type { %"class.draco::AttributeMetadata"* }
%"class.draco::AttributeMetadata" = type { %"class.draco::Metadata", i32 }
%"class.std::__2::__compressed_pair.54" = type { %"struct.std::__2::__compressed_pair_elem.55" }
%"struct.std::__2::__compressed_pair_elem.55" = type { %"class.std::__2::unique_ptr.50"* }
%"class.std::__2::vector.61" = type { %"class.std::__2::__vector_base.62" }
%"class.std::__2::__vector_base.62" = type { %"class.std::__2::unique_ptr.63"*, %"class.std::__2::unique_ptr.63"*, %"class.std::__2::__compressed_pair.92" }
%"class.std::__2::unique_ptr.63" = type { %"class.std::__2::__compressed_pair.64" }
%"class.std::__2::__compressed_pair.64" = type { %"struct.std::__2::__compressed_pair_elem.65" }
%"struct.std::__2::__compressed_pair_elem.65" = type { %"class.draco::PointAttribute"* }
%"class.draco::PointAttribute" = type <{ %"class.draco::GeometryAttribute", %"class.std::__2::unique_ptr.73", %"class.draco::IndexTypeVector", i32, i8, [3 x i8], %"class.std::__2::unique_ptr.85", [4 x i8] }>
%"class.draco::GeometryAttribute" = type { %"class.draco::DataBuffer"*, %"struct.draco::DataBufferDescriptor", i8, i32, i8, i64, i64, i32, i32 }
%"class.draco::DataBuffer" = type { %"class.std::__2::vector.66", %"struct.draco::DataBufferDescriptor" }
%"class.std::__2::vector.66" = type { %"class.std::__2::__vector_base.67" }
%"class.std::__2::__vector_base.67" = type { i8*, i8*, %"class.std::__2::__compressed_pair.68" }
%"class.std::__2::__compressed_pair.68" = type { %"struct.std::__2::__compressed_pair_elem.69" }
%"struct.std::__2::__compressed_pair_elem.69" = type { i8* }
%"struct.draco::DataBufferDescriptor" = type { i64, i64 }
%"class.std::__2::unique_ptr.73" = type { %"class.std::__2::__compressed_pair.74" }
%"class.std::__2::__compressed_pair.74" = type { %"struct.std::__2::__compressed_pair_elem.75" }
%"struct.std::__2::__compressed_pair_elem.75" = type { %"class.draco::DataBuffer"* }
%"class.draco::IndexTypeVector" = type { %"class.std::__2::vector.78" }
%"class.std::__2::vector.78" = type { %"class.std::__2::__vector_base.79" }
%"class.std::__2::__vector_base.79" = type { %"class.draco::IndexType"*, %"class.draco::IndexType"*, %"class.std::__2::__compressed_pair.80" }
%"class.draco::IndexType" = type { i32 }
%"class.std::__2::__compressed_pair.80" = type { %"struct.std::__2::__compressed_pair_elem.81" }
%"struct.std::__2::__compressed_pair_elem.81" = type { %"class.draco::IndexType"* }
%"class.std::__2::unique_ptr.85" = type { %"class.std::__2::__compressed_pair.86" }
%"class.std::__2::__compressed_pair.86" = type { %"struct.std::__2::__compressed_pair_elem.87" }
%"struct.std::__2::__compressed_pair_elem.87" = type { %"class.draco::AttributeTransformData"* }
%"class.draco::AttributeTransformData" = type { i32, %"class.draco::DataBuffer" }
%"class.std::__2::__compressed_pair.92" = type { %"struct.std::__2::__compressed_pair_elem.93" }
%"struct.std::__2::__compressed_pair_elem.93" = type { %"class.std::__2::unique_ptr.63"* }
%"class.std::__2::vector.97" = type { %"class.std::__2::__vector_base.98" }
%"class.std::__2::__vector_base.98" = type { i32*, i32*, %"class.std::__2::__compressed_pair.99" }
%"class.std::__2::__compressed_pair.99" = type { %"struct.std::__2::__compressed_pair_elem.100" }
%"struct.std::__2::__compressed_pair_elem.100" = type { i32* }
%"class.std::__2::vector.104" = type { %"class.std::__2::__vector_base.105" }
%"class.std::__2::__vector_base.105" = type { %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"*, %"class.std::__2::__compressed_pair.106" }
%"struct.draco::Mesh::AttributeData" = type { i32 }
%"class.std::__2::__compressed_pair.106" = type { %"struct.std::__2::__compressed_pair_elem.107" }
%"struct.std::__2::__compressed_pair_elem.107" = type { %"struct.draco::Mesh::AttributeData"* }
%"class.draco::IndexTypeVector.111" = type { %"class.std::__2::vector.112" }
%"class.std::__2::vector.112" = type { %"class.std::__2::__vector_base.113" }
%"class.std::__2::__vector_base.113" = type { %"struct.std::__2::array"*, %"struct.std::__2::array"*, %"class.std::__2::__compressed_pair.115" }
%"struct.std::__2::array" = type { [3 x %"class.draco::IndexType.114"] }
%"class.draco::IndexType.114" = type { i32 }
%"class.std::__2::__compressed_pair.115" = type { %"struct.std::__2::__compressed_pair_elem.116" }
%"struct.std::__2::__compressed_pair_elem.116" = type { %"struct.std::__2::array"* }
%"struct.std::__2::__default_init_tag" = type { i8 }
%"struct.std::__2::default_delete.121" = type { i8 }
%"class.draco::IndexType.122" = type { i32 }
%"class.std::__2::allocator.118" = type { i8 }
%"struct.std::__2::__split_buffer" = type { %"struct.std::__2::array"*, %"struct.std::__2::array"*, %"struct.std::__2::array"*, %"class.std::__2::__compressed_pair.123" }
%"class.std::__2::__compressed_pair.123" = type { %"struct.std::__2::__compressed_pair_elem.116", %"struct.std::__2::__compressed_pair_elem.124" }
%"struct.std::__2::__compressed_pair_elem.124" = type { %"class.std::__2::allocator.118"* }
%"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction" = type { %"class.std::__2::vector.112"*, %"struct.std::__2::array"*, %"struct.std::__2::array"* }
%"class.std::__2::__vector_base_common" = type { i8 }
%"class.std::__2::__split_buffer_common" = type { i8 }
%"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction" = type { %"struct.std::__2::array"*, %"struct.std::__2::array"*, %"struct.std::__2::array"** }
%"struct.std::__2::integral_constant" = type { i8 }
%"struct.std::__2::__has_construct" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.117" = type { i8 }
%"struct.std::__2::__less" = type { i8 }
%"struct.std::__2::__has_max_size" = type { i8 }
%"struct.std::__2::integral_constant.125" = type { i8 }
%"struct.std::__2::__has_destroy" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.120" = type { i8 }
%"class.std::__2::allocator" = type { i8 }
%"struct.std::__2::__has_destroy.126" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.0" = type { i8 }
%"struct.std::__2::vector<signed char, std::__2::allocator<signed char>>::_ConstructTransaction" = type { %"class.std::__2::vector"*, i8*, i8* }
%"struct.std::__2::__split_buffer.128" = type { i8*, i8*, i8*, %"class.std::__2::__compressed_pair.129" }
%"class.std::__2::__compressed_pair.129" = type { %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem.130" }
%"struct.std::__2::__compressed_pair_elem.130" = type { %"class.std::__2::allocator"* }
%"struct.std::__2::__has_construct.127" = type { i8 }
%"struct.std::__2::__has_max_size.131" = type { i8 }

$_ZNSt3__210unique_ptrIN5draco4MeshENS_14default_deleteIS2_EEEC2ILb1EvEEPS2_ = comdat any

$_ZNSt3__210unique_ptrIN5draco4MeshENS_14default_deleteIS2_EEEaSEOS5_ = comdat any

$_ZNSt3__210unique_ptrIN5draco4MeshENS_14default_deleteIS2_EEED2Ev = comdat any

$_ZNKSt3__210unique_ptrIN5draco4MeshENS_14default_deleteIS2_EEEptEv = comdat any

$_ZN5draco4Mesh11SetNumFacesEm = comdat any

$_ZN5draco10PointCloud14set_num_pointsEj = comdat any

$_ZNSt3__26vectorIaNS_9allocatorIaEEE5clearEv = comdat any

$_ZNSt3__26vectorIaNS_9allocatorIaEEE9push_backEOa = comdat any

$_ZNK5draco10PointCloud10num_pointsEv = comdat any

$_ZNK5draco9IndexTypeIjNS_19FaceIndex_tag_type_EE5valueEv = comdat any

$_ZN5draco10PointCloud9attributeEi = comdat any

$_ZN5draco14PointAttribute17SetAttributeValueENS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEEPKv = comdat any

$_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEC2Ej = comdat any

$_ZN5draco4Mesh7SetFaceENS_9IndexTypeIjNS_19FaceIndex_tag_type_EEERKNSt3__25arrayINS1_IjNS_20PointIndex_tag_type_EEELm3EEE = comdat any

$_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEC2Ej = comdat any

$_ZNSt3__26vectorIaNS_9allocatorIaEEEixEm = comdat any

$_ZNKSt3__26vectorIaNS_9allocatorIaEEE4sizeEv = comdat any

$_ZN5draco4Mesh23SetAttributeElementTypeEiNS_24MeshAttributeElementTypeE = comdat any

$_ZNSt3__24moveIRNS_10unique_ptrIN5draco4MeshENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOS9_ = comdat any

$_ZNSt3__210unique_ptrIN5draco4MeshENS_14default_deleteIS2_EEEC2EOS5_ = comdat any

$_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEENSt3__25arrayINS1_IjNS_20PointIndex_tag_type_EEELm3EEEE6resizeEmRKS8_ = comdat any

$_ZNSt3__25arrayIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELm3EEC2Ev = comdat any

$_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE6resizeEmRKS6_ = comdat any

$_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4sizeEv = comdat any

$_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE8__appendEmRKS6_ = comdat any

$_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE17__destruct_at_endEPS6_ = comdat any

$_ZNSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE9__end_capEv = comdat any

$_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE18__construct_at_endEmRKS6_ = comdat any

$_ZNSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE7__allocEv = comdat any

$_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE11__recommendEm = comdat any

$_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEEC2EmmS9_ = comdat any

$_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE18__construct_at_endEmRKS6_ = comdat any

$_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS6_RS8_EE = comdat any

$_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEED2Ev = comdat any

$_ZNSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE5firstEv = comdat any

$_ZNSt3__222__compressed_pair_elemIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE21_ConstructTransactionC2ERS9_m = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE9constructIS7_JRKS7_EEEvRS8_PT_DpOT0_ = comdat any

$_ZNSt3__212__to_addressINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEEEPT_S8_ = comdat any

$_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE21_ConstructTransactionD2Ev = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE11__constructIS7_JRKS7_EEEvNS_17integral_constantIbLb1EEERS8_PT_DpOT0_ = comdat any

$_ZNSt3__27forwardIRKNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEEEOT_RNS_16remove_referenceIS9_E4typeE = comdat any

$_ZNSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEE9constructIS6_JRKS6_EEEvPT_DpOT0_ = comdat any

$_ZNSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEELi1ELb1EE5__getEv = comdat any

$_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE8max_sizeEv = comdat any

$_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE8capacityEv = comdat any

$_ZNSt3__23maxImEERKT_S3_S3_ = comdat any

$_ZNSt3__23minImEERKT_S3_S3_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE8max_sizeERKS8_ = comdat any

$_ZNKSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE7__allocEv = comdat any

$_ZNSt3__214numeric_limitsIlE3maxEv = comdat any

$_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_ = comdat any

$_ZNKSt3__26__lessImmEclERKmS3_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS8_ = comdat any

$_ZNKSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEE8max_sizeEv = comdat any

$_ZNKSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE6secondEv = comdat any

$_ZNKSt3__222__compressed_pair_elemINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv = comdat any

$_ZNKSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__23maxImNS_6__lessImmEEEERKT_S5_S5_T0_ = comdat any

$_ZNSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEEC2IDnSA_EEOT_OT0_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE8allocateERS8_m = comdat any

$_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE7__allocEv = comdat any

$_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE9__end_capEv = comdat any

$_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEELi0ELb0EEC2IDnvEEOT_ = comdat any

$_ZNSt3__27forwardIRNS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEEEOT_RNS_16remove_referenceISA_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIRNS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEELi1ELb0EEC2IS9_vEEOT_ = comdat any

$_ZNSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEE8allocateEmPKv = comdat any

$_ZNSt3__220__throw_length_errorEPKc = comdat any

$_ZNSt3__217__libcpp_allocateEmm = comdat any

$_ZNSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemIRNS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEELi1ELb0EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE5firstEv = comdat any

$_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE21_ConstructTransactionC2EPPS6_m = comdat any

$_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE21_ConstructTransactionD2Ev = comdat any

$_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE17__annotate_deleteEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE46__construct_backward_with_exception_guaranteesIS7_EENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS8_PT_SE_EE5valuesr31is_trivially_move_constructibleISE_EE5valueEvE4typeERS8_SF_SF_RSF_ = comdat any

$_ZNSt3__24swapIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS9_EE5valueEvE4typeERS9_SC_ = comdat any

$_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE14__annotate_newEm = comdat any

$_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE26__invalidate_all_iteratorsEv = comdat any

$_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE31__annotate_contiguous_containerEPKvSB_SB_SB_ = comdat any

$_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4dataEv = comdat any

$_ZNSt3__24moveIRPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEEEONS_16remove_referenceIT_E4typeEOSA_ = comdat any

$_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE5clearEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE10deallocateERS8_PS7_m = comdat any

$_ZNKSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE8capacityEv = comdat any

$_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE17__destruct_at_endEPS6_ = comdat any

$_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE17__destruct_at_endEPS6_NS_17integral_constantIbLb0EEE = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE7destroyIS7_EEvRS8_PT_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE9__destroyIS7_EEvNS_17integral_constantIbLb1EEERS8_PT_ = comdat any

$_ZNSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEE7destroyEPS6_ = comdat any

$_ZNSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEE10deallocateEPS6_m = comdat any

$_ZNSt3__219__libcpp_deallocateEPvmm = comdat any

$_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm = comdat any

$_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm = comdat any

$_ZNSt3__217_DeallocateCaller9__do_callEPv = comdat any

$_ZNKSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE5firstEv = comdat any

$_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE27__invalidate_iterators_pastEPS6_ = comdat any

$_ZNSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE17__destruct_at_endEPS6_ = comdat any

$_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE17__annotate_shrinkEm = comdat any

$_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEC2Ev = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEixEm = comdat any

$_ZNKSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEE3getEv = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPN5draco14PointAttributeELi0ELb0EE5__getEv = comdat any

$_ZNK5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EE5valueEv = comdat any

$_ZNK5draco17GeometryAttribute11byte_strideEv = comdat any

$_ZNK5draco14PointAttribute6bufferEv = comdat any

$_ZN5draco10DataBuffer5WriteExPKvm = comdat any

$_ZNKSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEE3getEv = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPN5draco10DataBufferELi0ELb0EE5__getEv = comdat any

$_ZN5draco10DataBuffer4dataEv = comdat any

$_ZNSt3__26vectorIhNS_9allocatorIhEEEixEm = comdat any

$_ZNK5draco9IndexTypeIjNS_19FaceIndex_tag_type_EEgeERKj = comdat any

$_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEENSt3__25arrayINS1_IjNS_20PointIndex_tag_type_EEELm3EEEE4sizeEv = comdat any

$_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEENSt3__25arrayINS1_IjNS_20PointIndex_tag_type_EEELm3EEEEixERKS3_ = comdat any

$_ZNSt3__25arrayIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELm3EEaSERKS5_ = comdat any

$_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEEixEm = comdat any

$_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEaSERKS2_ = comdat any

$_ZNSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEEixEm = comdat any

$_ZNSt3__217__compressed_pairIPN5draco4MeshENS_14default_deleteIS2_EEEC2IRS3_NS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__27forwardIRPN5draco4MeshEEEOT_RNS_16remove_referenceIS5_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco4MeshELi0ELb0EEC2IRS3_vEEOT_ = comdat any

$_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco4MeshEEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__210unique_ptrIN5draco4MeshENS_14default_deleteIS2_EEE5resetEPS2_ = comdat any

$_ZNSt3__217__compressed_pairIPN5draco4MeshENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco4MeshENS_14default_deleteIS2_EEE6secondEv = comdat any

$_ZNKSt3__214default_deleteIN5draco4MeshEEclEPS2_ = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco4MeshELi0ELb0EE5__getEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco4MeshEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__210unique_ptrIN5draco4MeshENS_14default_deleteIS2_EEE7releaseEv = comdat any

$_ZNSt3__27forwardINS_14default_deleteIN5draco4MeshEEEEEOT_RNS_16remove_referenceIS5_E4typeE = comdat any

$_ZNSt3__210unique_ptrIN5draco4MeshENS_14default_deleteIS2_EEE11get_deleterEv = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco4MeshENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPN5draco4MeshELi0ELb0EE5__getEv = comdat any

$_ZNSt3__213__vector_baseIaNS_9allocatorIaEEE5clearEv = comdat any

$_ZNKSt3__26vectorIaNS_9allocatorIaEEE17__annotate_shrinkEm = comdat any

$_ZNSt3__26vectorIaNS_9allocatorIaEEE26__invalidate_all_iteratorsEv = comdat any

$_ZNSt3__213__vector_baseIaNS_9allocatorIaEEE17__destruct_at_endEPa = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIaEEE7destroyIaEEvRS2_PT_ = comdat any

$_ZNSt3__213__vector_baseIaNS_9allocatorIaEEE7__allocEv = comdat any

$_ZNSt3__212__to_addressIaEEPT_S2_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIaEEE9__destroyIaEEvNS_17integral_constantIbLb1EEERS2_PT_ = comdat any

$_ZNSt3__29allocatorIaE7destroyEPa = comdat any

$_ZNSt3__217__compressed_pairIPaNS_9allocatorIaEEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIaEELi1ELb1EE5__getEv = comdat any

$_ZNKSt3__26vectorIaNS_9allocatorIaEEE31__annotate_contiguous_containerEPKvS5_S5_S5_ = comdat any

$_ZNKSt3__26vectorIaNS_9allocatorIaEEE4dataEv = comdat any

$_ZNKSt3__26vectorIaNS_9allocatorIaEEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseIaNS_9allocatorIaEEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseIaNS_9allocatorIaEEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPaNS_9allocatorIaEEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPaLi0ELb0EE5__getEv = comdat any

$_ZNSt3__213__vector_baseIaNS_9allocatorIaEEE9__end_capEv = comdat any

$_ZNSt3__26vectorIaNS_9allocatorIaEEE22__construct_one_at_endIJaEEEvDpOT_ = comdat any

$_ZNSt3__24moveIRaEEONS_16remove_referenceIT_E4typeEOS3_ = comdat any

$_ZNSt3__26vectorIaNS_9allocatorIaEEE21__push_back_slow_pathIaEEvOT_ = comdat any

$_ZNSt3__217__compressed_pairIPaNS_9allocatorIaEEE5firstEv = comdat any

$_ZNSt3__222__compressed_pair_elemIPaLi0ELb0EE5__getEv = comdat any

$_ZNSt3__26vectorIaNS_9allocatorIaEEE21_ConstructTransactionC2ERS3_m = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIaEEE9constructIaJaEEEvRS2_PT_DpOT0_ = comdat any

$_ZNSt3__27forwardIaEEOT_RNS_16remove_referenceIS1_E4typeE = comdat any

$_ZNSt3__26vectorIaNS_9allocatorIaEEE21_ConstructTransactionD2Ev = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIaEEE11__constructIaJaEEEvNS_17integral_constantIbLb1EEERS2_PT_DpOT0_ = comdat any

$_ZNSt3__29allocatorIaE9constructIaJaEEEvPT_DpOT0_ = comdat any

$_ZNKSt3__26vectorIaNS_9allocatorIaEEE11__recommendEm = comdat any

$_ZNSt3__214__split_bufferIaRNS_9allocatorIaEEEC2EmmS3_ = comdat any

$_ZNSt3__26vectorIaNS_9allocatorIaEEE26__swap_out_circular_bufferERNS_14__split_bufferIaRS2_EE = comdat any

$_ZNSt3__214__split_bufferIaRNS_9allocatorIaEEED2Ev = comdat any

$_ZNKSt3__26vectorIaNS_9allocatorIaEEE8max_sizeEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIaEEE8max_sizeERKS2_ = comdat any

$_ZNKSt3__213__vector_baseIaNS_9allocatorIaEEE7__allocEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIaEEE10__max_sizeENS_17integral_constantIbLb1EEERKS2_ = comdat any

$_ZNKSt3__29allocatorIaE8max_sizeEv = comdat any

$_ZNKSt3__217__compressed_pairIPaNS_9allocatorIaEEE6secondEv = comdat any

$_ZNKSt3__222__compressed_pair_elemINS_9allocatorIaEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPaRNS_9allocatorIaEEEC2IDnS4_EEOT_OT0_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIaEEE8allocateERS2_m = comdat any

$_ZNSt3__214__split_bufferIaRNS_9allocatorIaEEE7__allocEv = comdat any

$_ZNSt3__214__split_bufferIaRNS_9allocatorIaEEE9__end_capEv = comdat any

$_ZNSt3__222__compressed_pair_elemIPaLi0ELb0EEC2IDnvEEOT_ = comdat any

$_ZNSt3__27forwardIRNS_9allocatorIaEEEEOT_RNS_16remove_referenceIS4_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIaEELi1ELb0EEC2IS3_vEEOT_ = comdat any

$_ZNSt3__29allocatorIaE8allocateEmPKv = comdat any

$_ZNSt3__217__compressed_pairIPaRNS_9allocatorIaEEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIaEELi1ELb0EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPaRNS_9allocatorIaEEE5firstEv = comdat any

$_ZNKSt3__26vectorIaNS_9allocatorIaEEE17__annotate_deleteEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIaEEE46__construct_backward_with_exception_guaranteesIaEENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS2_PT_S8_EE5valuesr31is_trivially_move_constructibleIS8_EE5valueEvE4typeERS2_S9_S9_RS9_ = comdat any

$_ZNSt3__24swapIPaEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_ = comdat any

$_ZNKSt3__26vectorIaNS_9allocatorIaEEE14__annotate_newEm = comdat any

$_ZNSt3__24moveIRPaEEONS_16remove_referenceIT_E4typeEOS4_ = comdat any

$_ZNSt3__214__split_bufferIaRNS_9allocatorIaEEE5clearEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIaEEE10deallocateERS2_Pam = comdat any

$_ZNKSt3__214__split_bufferIaRNS_9allocatorIaEEE8capacityEv = comdat any

$_ZNSt3__214__split_bufferIaRNS_9allocatorIaEEE17__destruct_at_endEPa = comdat any

$_ZNSt3__214__split_bufferIaRNS_9allocatorIaEEE17__destruct_at_endEPaNS_17integral_constantIbLb0EEE = comdat any

$_ZNSt3__29allocatorIaE10deallocateEPam = comdat any

$_ZNKSt3__214__split_bufferIaRNS_9allocatorIaEEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPaRNS_9allocatorIaEEE5firstEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco4MeshENS_14default_deleteIS2_EEEC2IS3_S5_EEOT_OT0_ = comdat any

$_ZNSt3__27forwardIPN5draco4MeshEEEOT_RNS_16remove_referenceIS4_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco4MeshELi0ELb0EEC2IS3_vEEOT_ = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco4MeshEEELi1ELb1EEC2IS4_vEEOT_ = comdat any

@.str = private unnamed_addr constant [68 x i8] c"allocator<T>::allocate(size_t n) 'n' exceeds maximum supported size\00", align 1

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN5draco23TriangleSoupMeshBuilder5StartEi(%"class.draco::TriangleSoupMeshBuilder"* %this, i32 %num_faces) #0 {
entry:
  %this.addr = alloca %"class.draco::TriangleSoupMeshBuilder"*, align 4
  %num_faces.addr = alloca i32, align 4
  %ref.tmp = alloca %"class.std::__2::unique_ptr", align 4
  store %"class.draco::TriangleSoupMeshBuilder"* %this, %"class.draco::TriangleSoupMeshBuilder"** %this.addr, align 4
  store i32 %num_faces, i32* %num_faces.addr, align 4
  %this1 = load %"class.draco::TriangleSoupMeshBuilder"*, %"class.draco::TriangleSoupMeshBuilder"** %this.addr, align 4
  %call = call noalias nonnull i8* @_Znwm(i32 108) #8
  %0 = bitcast i8* %call to %"class.draco::Mesh"*
  %call2 = call %"class.draco::Mesh"* @_ZN5draco4MeshC1Ev(%"class.draco::Mesh"* %0)
  %call3 = call %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrIN5draco4MeshENS_14default_deleteIS2_EEEC2ILb1EvEEPS2_(%"class.std::__2::unique_ptr"* %ref.tmp, %"class.draco::Mesh"* %0) #9
  %mesh_ = getelementptr inbounds %"class.draco::TriangleSoupMeshBuilder", %"class.draco::TriangleSoupMeshBuilder"* %this1, i32 0, i32 1
  %call4 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrIN5draco4MeshENS_14default_deleteIS2_EEEaSEOS5_(%"class.std::__2::unique_ptr"* %mesh_, %"class.std::__2::unique_ptr"* nonnull align 4 dereferenceable(4) %ref.tmp) #9
  %call5 = call %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrIN5draco4MeshENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr"* %ref.tmp) #9
  %mesh_6 = getelementptr inbounds %"class.draco::TriangleSoupMeshBuilder", %"class.draco::TriangleSoupMeshBuilder"* %this1, i32 0, i32 1
  %call7 = call %"class.draco::Mesh"* @_ZNKSt3__210unique_ptrIN5draco4MeshENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr"* %mesh_6) #9
  %1 = load i32, i32* %num_faces.addr, align 4
  call void @_ZN5draco4Mesh11SetNumFacesEm(%"class.draco::Mesh"* %call7, i32 %1)
  %mesh_8 = getelementptr inbounds %"class.draco::TriangleSoupMeshBuilder", %"class.draco::TriangleSoupMeshBuilder"* %this1, i32 0, i32 1
  %call9 = call %"class.draco::Mesh"* @_ZNKSt3__210unique_ptrIN5draco4MeshENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr"* %mesh_8) #9
  %2 = bitcast %"class.draco::Mesh"* %call9 to %"class.draco::PointCloud"*
  %3 = load i32, i32* %num_faces.addr, align 4
  %mul = mul nsw i32 %3, 3
  call void @_ZN5draco10PointCloud14set_num_pointsEj(%"class.draco::PointCloud"* %2, i32 %mul)
  %attribute_element_types_ = getelementptr inbounds %"class.draco::TriangleSoupMeshBuilder", %"class.draco::TriangleSoupMeshBuilder"* %this1, i32 0, i32 0
  call void @_ZNSt3__26vectorIaNS_9allocatorIaEEE5clearEv(%"class.std::__2::vector"* %attribute_element_types_) #9
  ret void
}

; Function Attrs: nobuiltin allocsize(0)
declare nonnull i8* @_Znwm(i32) #1

declare %"class.draco::Mesh"* @_ZN5draco4MeshC1Ev(%"class.draco::Mesh"* returned) unnamed_addr #2

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrIN5draco4MeshENS_14default_deleteIS2_EEEC2ILb1EvEEPS2_(%"class.std::__2::unique_ptr"* returned %this, %"class.draco::Mesh"* %__p) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  %__p.addr = alloca %"class.draco::Mesh"*, align 4
  %ref.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  store %"class.draco::Mesh"* %__p, %"class.draco::Mesh"** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::__compressed_pair.1"* @_ZNSt3__217__compressed_pairIPN5draco4MeshENS_14default_deleteIS2_EEEC2IRS3_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.1"* %__ptr_, %"class.draco::Mesh"** nonnull align 4 dereferenceable(4) %__p.addr, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp)
  ret %"class.std::__2::unique_ptr"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrIN5draco4MeshENS_14default_deleteIS2_EEEaSEOS5_(%"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"* nonnull align 4 dereferenceable(4) %__u) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  %__u.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  store %"class.std::__2::unique_ptr"* %__u, %"class.std::__2::unique_ptr"** %__u.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %0 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %__u.addr, align 4
  %call = call %"class.draco::Mesh"* @_ZNSt3__210unique_ptrIN5draco4MeshENS_14default_deleteIS2_EEE7releaseEv(%"class.std::__2::unique_ptr"* %0) #9
  call void @_ZNSt3__210unique_ptrIN5draco4MeshENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr"* %this1, %"class.draco::Mesh"* %call) #9
  %1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %__u.addr, align 4
  %call2 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.121"* @_ZNSt3__210unique_ptrIN5draco4MeshENS_14default_deleteIS2_EEE11get_deleterEv(%"class.std::__2::unique_ptr"* %1) #9
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.121"* @_ZNSt3__27forwardINS_14default_deleteIN5draco4MeshEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.121"* nonnull align 1 dereferenceable(1) %call2) #9
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call4 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.121"* @_ZNSt3__217__compressed_pairIPN5draco4MeshENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.1"* %__ptr_) #9
  ret %"class.std::__2::unique_ptr"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrIN5draco4MeshENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  call void @_ZNSt3__210unique_ptrIN5draco4MeshENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr"* %this1, %"class.draco::Mesh"* null) #9
  ret %"class.std::__2::unique_ptr"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::Mesh"* @_ZNKSt3__210unique_ptrIN5draco4MeshENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::Mesh"** @_ZNKSt3__217__compressed_pairIPN5draco4MeshENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.1"* %__ptr_) #9
  %0 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %call, align 4
  ret %"class.draco::Mesh"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco4Mesh11SetNumFacesEm(%"class.draco::Mesh"* %this, i32 %num_faces) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::Mesh"*, align 4
  %num_faces.addr = alloca i32, align 4
  %ref.tmp = alloca %"struct.std::__2::array", align 4
  store %"class.draco::Mesh"* %this, %"class.draco::Mesh"** %this.addr, align 4
  store i32 %num_faces, i32* %num_faces.addr, align 4
  %this1 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %this.addr, align 4
  %faces_ = getelementptr inbounds %"class.draco::Mesh", %"class.draco::Mesh"* %this1, i32 0, i32 2
  %0 = load i32, i32* %num_faces.addr, align 4
  %1 = bitcast %"struct.std::__2::array"* %ref.tmp to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %1, i8 0, i32 12, i1 false)
  %call = call %"struct.std::__2::array"* @_ZNSt3__25arrayIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELm3EEC2Ev(%"struct.std::__2::array"* %ref.tmp)
  call void @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEENSt3__25arrayINS1_IjNS_20PointIndex_tag_type_EEELm3EEEE6resizeEmRKS8_(%"class.draco::IndexTypeVector.111"* %faces_, i32 %0, %"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %ref.tmp)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco10PointCloud14set_num_pointsEj(%"class.draco::PointCloud"* %this, i32 %num) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointCloud"*, align 4
  %num.addr = alloca i32, align 4
  store %"class.draco::PointCloud"* %this, %"class.draco::PointCloud"** %this.addr, align 4
  store i32 %num, i32* %num.addr, align 4
  %this1 = load %"class.draco::PointCloud"*, %"class.draco::PointCloud"** %this.addr, align 4
  %0 = load i32, i32* %num.addr, align 4
  %num_points_ = getelementptr inbounds %"class.draco::PointCloud", %"class.draco::PointCloud"* %this1, i32 0, i32 4
  store i32 %0, i32* %num_points_, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIaNS_9allocatorIaEEE5clearEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__old_size = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIaNS_9allocatorIaEEE4sizeEv(%"class.std::__2::vector"* %this1) #9
  store i32 %call, i32* %__old_size, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  call void @_ZNSt3__213__vector_baseIaNS_9allocatorIaEEE5clearEv(%"class.std::__2::__vector_base"* %0) #9
  %1 = load i32, i32* %__old_size, align 4
  call void @_ZNKSt3__26vectorIaNS_9allocatorIaEEE17__annotate_shrinkEm(%"class.std::__2::vector"* %this1, i32 %1) #9
  call void @_ZNSt3__26vectorIaNS_9allocatorIaEEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector"* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @_ZN5draco23TriangleSoupMeshBuilder12AddAttributeENS_17GeometryAttribute4TypeEaNS_8DataTypeE(%"class.draco::TriangleSoupMeshBuilder"* %this, i32 %attribute_type, i8 signext %num_components, i32 %data_type) #0 {
entry:
  %this.addr = alloca %"class.draco::TriangleSoupMeshBuilder"*, align 4
  %attribute_type.addr = alloca i32, align 4
  %num_components.addr = alloca i8, align 1
  %data_type.addr = alloca i32, align 4
  %va = alloca %"class.draco::GeometryAttribute", align 8
  %ref.tmp = alloca i8, align 1
  store %"class.draco::TriangleSoupMeshBuilder"* %this, %"class.draco::TriangleSoupMeshBuilder"** %this.addr, align 4
  store i32 %attribute_type, i32* %attribute_type.addr, align 4
  store i8 %num_components, i8* %num_components.addr, align 1
  store i32 %data_type, i32* %data_type.addr, align 4
  %this1 = load %"class.draco::TriangleSoupMeshBuilder"*, %"class.draco::TriangleSoupMeshBuilder"** %this.addr, align 4
  %call = call %"class.draco::GeometryAttribute"* @_ZN5draco17GeometryAttributeC1Ev(%"class.draco::GeometryAttribute"* %va)
  %0 = load i32, i32* %attribute_type.addr, align 4
  %1 = load i8, i8* %num_components.addr, align 1
  %2 = load i32, i32* %data_type.addr, align 4
  %3 = load i32, i32* %data_type.addr, align 4
  %call2 = call i32 @_ZN5draco14DataTypeLengthENS_8DataTypeE(i32 %3)
  %4 = load i8, i8* %num_components.addr, align 1
  %conv = sext i8 %4 to i32
  %mul = mul nsw i32 %call2, %conv
  %conv3 = sext i32 %mul to i64
  call void @_ZN5draco17GeometryAttribute4InitENS0_4TypeEPNS_10DataBufferEaNS_8DataTypeEbxx(%"class.draco::GeometryAttribute"* %va, i32 %0, %"class.draco::DataBuffer"* null, i8 signext %1, i32 %2, i1 zeroext false, i64 %conv3, i64 0)
  %attribute_element_types_ = getelementptr inbounds %"class.draco::TriangleSoupMeshBuilder", %"class.draco::TriangleSoupMeshBuilder"* %this1, i32 0, i32 0
  store i8 -1, i8* %ref.tmp, align 1
  call void @_ZNSt3__26vectorIaNS_9allocatorIaEEE9push_backEOa(%"class.std::__2::vector"* %attribute_element_types_, i8* nonnull align 1 dereferenceable(1) %ref.tmp)
  %mesh_ = getelementptr inbounds %"class.draco::TriangleSoupMeshBuilder", %"class.draco::TriangleSoupMeshBuilder"* %this1, i32 0, i32 1
  %call4 = call %"class.draco::Mesh"* @_ZNKSt3__210unique_ptrIN5draco4MeshENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr"* %mesh_) #9
  %5 = bitcast %"class.draco::Mesh"* %call4 to %"class.draco::PointCloud"*
  %mesh_5 = getelementptr inbounds %"class.draco::TriangleSoupMeshBuilder", %"class.draco::TriangleSoupMeshBuilder"* %this1, i32 0, i32 1
  %call6 = call %"class.draco::Mesh"* @_ZNKSt3__210unique_ptrIN5draco4MeshENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr"* %mesh_5) #9
  %6 = bitcast %"class.draco::Mesh"* %call6 to %"class.draco::PointCloud"*
  %call7 = call i32 @_ZNK5draco10PointCloud10num_pointsEv(%"class.draco::PointCloud"* %6)
  %call8 = call i32 @_ZN5draco10PointCloud12AddAttributeERKNS_17GeometryAttributeEbj(%"class.draco::PointCloud"* %5, %"class.draco::GeometryAttribute"* nonnull align 8 dereferenceable(64) %va, i1 zeroext true, i32 %call7)
  ret i32 %call8
}

declare %"class.draco::GeometryAttribute"* @_ZN5draco17GeometryAttributeC1Ev(%"class.draco::GeometryAttribute"* returned) unnamed_addr #2

declare void @_ZN5draco17GeometryAttribute4InitENS0_4TypeEPNS_10DataBufferEaNS_8DataTypeEbxx(%"class.draco::GeometryAttribute"*, i32, %"class.draco::DataBuffer"*, i8 signext, i32, i1 zeroext, i64, i64) #2

declare i32 @_ZN5draco14DataTypeLengthENS_8DataTypeE(i32) #2

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIaNS_9allocatorIaEEE9push_backEOa(%"class.std::__2::vector"* %this, i8* nonnull align 1 dereferenceable(1) %__x) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__x.addr = alloca i8*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i8* %__x, i8** %__x.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 1
  %1 = load i8*, i8** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__213__vector_baseIaNS_9allocatorIaEEE9__end_capEv(%"class.std::__2::__vector_base"* %2) #9
  %3 = load i8*, i8** %call, align 4
  %cmp = icmp ult i8* %1, %3
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %4 = load i8*, i8** %__x.addr, align 4
  %call2 = call nonnull align 1 dereferenceable(1) i8* @_ZNSt3__24moveIRaEEONS_16remove_referenceIT_E4typeEOS3_(i8* nonnull align 1 dereferenceable(1) %4) #9
  call void @_ZNSt3__26vectorIaNS_9allocatorIaEEE22__construct_one_at_endIJaEEEvDpOT_(%"class.std::__2::vector"* %this1, i8* nonnull align 1 dereferenceable(1) %call2)
  br label %if.end

if.else:                                          ; preds = %entry
  %5 = load i8*, i8** %__x.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) i8* @_ZNSt3__24moveIRaEEONS_16remove_referenceIT_E4typeEOS3_(i8* nonnull align 1 dereferenceable(1) %5) #9
  call void @_ZNSt3__26vectorIaNS_9allocatorIaEEE21__push_back_slow_pathIaEEvOT_(%"class.std::__2::vector"* %this1, i8* nonnull align 1 dereferenceable(1) %call3)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

declare i32 @_ZN5draco10PointCloud12AddAttributeERKNS_17GeometryAttributeEbj(%"class.draco::PointCloud"*, %"class.draco::GeometryAttribute"* nonnull align 8 dereferenceable(64), i1 zeroext, i32) #2

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco10PointCloud10num_pointsEv(%"class.draco::PointCloud"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointCloud"*, align 4
  store %"class.draco::PointCloud"* %this, %"class.draco::PointCloud"** %this.addr, align 4
  %this1 = load %"class.draco::PointCloud"*, %"class.draco::PointCloud"** %this.addr, align 4
  %num_points_ = getelementptr inbounds %"class.draco::PointCloud", %"class.draco::PointCloud"* %this1, i32 0, i32 4
  %0 = load i32, i32* %num_points_, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN5draco23TriangleSoupMeshBuilder25SetAttributeValuesForFaceEiNS_9IndexTypeIjNS_19FaceIndex_tag_type_EEEPKvS5_S5_(%"class.draco::TriangleSoupMeshBuilder"* %this, i32 %att_id, i32 %face_id.coerce, i8* %corner_value_0, i8* %corner_value_1, i8* %corner_value_2) #0 {
entry:
  %face_id = alloca %"class.draco::IndexType.122", align 4
  %this.addr = alloca %"class.draco::TriangleSoupMeshBuilder"*, align 4
  %att_id.addr = alloca i32, align 4
  %corner_value_0.addr = alloca i8*, align 4
  %corner_value_1.addr = alloca i8*, align 4
  %corner_value_2.addr = alloca i8*, align 4
  %start_index = alloca i32, align 4
  %att = alloca %"class.draco::PointAttribute"*, align 4
  %agg.tmp = alloca %"class.draco::IndexType", align 4
  %agg.tmp6 = alloca %"class.draco::IndexType", align 4
  %agg.tmp9 = alloca %"class.draco::IndexType", align 4
  %agg.tmp15 = alloca %"class.draco::IndexType.122", align 4
  %ref.tmp = alloca %"struct.std::__2::array", align 4
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType.122", %"class.draco::IndexType.122"* %face_id, i32 0, i32 0
  store i32 %face_id.coerce, i32* %coerce.dive, align 4
  store %"class.draco::TriangleSoupMeshBuilder"* %this, %"class.draco::TriangleSoupMeshBuilder"** %this.addr, align 4
  store i32 %att_id, i32* %att_id.addr, align 4
  store i8* %corner_value_0, i8** %corner_value_0.addr, align 4
  store i8* %corner_value_1, i8** %corner_value_1.addr, align 4
  store i8* %corner_value_2, i8** %corner_value_2.addr, align 4
  %this1 = load %"class.draco::TriangleSoupMeshBuilder"*, %"class.draco::TriangleSoupMeshBuilder"** %this.addr, align 4
  %call = call i32 @_ZNK5draco9IndexTypeIjNS_19FaceIndex_tag_type_EE5valueEv(%"class.draco::IndexType.122"* %face_id)
  %mul = mul i32 3, %call
  store i32 %mul, i32* %start_index, align 4
  %mesh_ = getelementptr inbounds %"class.draco::TriangleSoupMeshBuilder", %"class.draco::TriangleSoupMeshBuilder"* %this1, i32 0, i32 1
  %call2 = call %"class.draco::Mesh"* @_ZNKSt3__210unique_ptrIN5draco4MeshENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr"* %mesh_) #9
  %0 = bitcast %"class.draco::Mesh"* %call2 to %"class.draco::PointCloud"*
  %1 = load i32, i32* %att_id.addr, align 4
  %call3 = call %"class.draco::PointAttribute"* @_ZN5draco10PointCloud9attributeEi(%"class.draco::PointCloud"* %0, i32 %1)
  store %"class.draco::PointAttribute"* %call3, %"class.draco::PointAttribute"** %att, align 4
  %2 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %att, align 4
  %3 = load i32, i32* %start_index, align 4
  %call4 = call %"class.draco::IndexType"* @_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEC2Ej(%"class.draco::IndexType"* %agg.tmp, i32 %3)
  %4 = load i8*, i8** %corner_value_0.addr, align 4
  %coerce.dive5 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %agg.tmp, i32 0, i32 0
  %5 = load i32, i32* %coerce.dive5, align 4
  call void @_ZN5draco14PointAttribute17SetAttributeValueENS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEEPKv(%"class.draco::PointAttribute"* %2, i32 %5, i8* %4)
  %6 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %att, align 4
  %7 = load i32, i32* %start_index, align 4
  %add = add nsw i32 %7, 1
  %call7 = call %"class.draco::IndexType"* @_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEC2Ej(%"class.draco::IndexType"* %agg.tmp6, i32 %add)
  %8 = load i8*, i8** %corner_value_1.addr, align 4
  %coerce.dive8 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %agg.tmp6, i32 0, i32 0
  %9 = load i32, i32* %coerce.dive8, align 4
  call void @_ZN5draco14PointAttribute17SetAttributeValueENS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEEPKv(%"class.draco::PointAttribute"* %6, i32 %9, i8* %8)
  %10 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %att, align 4
  %11 = load i32, i32* %start_index, align 4
  %add10 = add nsw i32 %11, 2
  %call11 = call %"class.draco::IndexType"* @_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEC2Ej(%"class.draco::IndexType"* %agg.tmp9, i32 %add10)
  %12 = load i8*, i8** %corner_value_2.addr, align 4
  %coerce.dive12 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %agg.tmp9, i32 0, i32 0
  %13 = load i32, i32* %coerce.dive12, align 4
  call void @_ZN5draco14PointAttribute17SetAttributeValueENS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEEPKv(%"class.draco::PointAttribute"* %10, i32 %13, i8* %12)
  %mesh_13 = getelementptr inbounds %"class.draco::TriangleSoupMeshBuilder", %"class.draco::TriangleSoupMeshBuilder"* %this1, i32 0, i32 1
  %call14 = call %"class.draco::Mesh"* @_ZNKSt3__210unique_ptrIN5draco4MeshENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr"* %mesh_13) #9
  %14 = bitcast %"class.draco::IndexType.122"* %agg.tmp15 to i8*
  %15 = bitcast %"class.draco::IndexType.122"* %face_id to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %14, i8* align 4 %15, i32 4, i1 false)
  %__elems_ = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %ref.tmp, i32 0, i32 0
  %arrayinit.begin = getelementptr inbounds [3 x %"class.draco::IndexType.114"], [3 x %"class.draco::IndexType.114"]* %__elems_, i32 0, i32 0
  %16 = load i32, i32* %start_index, align 4
  %call16 = call %"class.draco::IndexType.114"* @_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEC2Ej(%"class.draco::IndexType.114"* %arrayinit.begin, i32 %16)
  %arrayinit.element = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %arrayinit.begin, i32 1
  %17 = load i32, i32* %start_index, align 4
  %add17 = add nsw i32 %17, 1
  %call18 = call %"class.draco::IndexType.114"* @_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEC2Ej(%"class.draco::IndexType.114"* %arrayinit.element, i32 %add17)
  %arrayinit.element19 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %arrayinit.element, i32 1
  %18 = load i32, i32* %start_index, align 4
  %add20 = add nsw i32 %18, 2
  %call21 = call %"class.draco::IndexType.114"* @_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEC2Ej(%"class.draco::IndexType.114"* %arrayinit.element19, i32 %add20)
  %coerce.dive22 = getelementptr inbounds %"class.draco::IndexType.122", %"class.draco::IndexType.122"* %agg.tmp15, i32 0, i32 0
  %19 = load i32, i32* %coerce.dive22, align 4
  call void @_ZN5draco4Mesh7SetFaceENS_9IndexTypeIjNS_19FaceIndex_tag_type_EEERKNSt3__25arrayINS1_IjNS_20PointIndex_tag_type_EEELm3EEE(%"class.draco::Mesh"* %call14, i32 %19, %"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %ref.tmp)
  %attribute_element_types_ = getelementptr inbounds %"class.draco::TriangleSoupMeshBuilder", %"class.draco::TriangleSoupMeshBuilder"* %this1, i32 0, i32 0
  %20 = load i32, i32* %att_id.addr, align 4
  %call23 = call nonnull align 1 dereferenceable(1) i8* @_ZNSt3__26vectorIaNS_9allocatorIaEEEixEm(%"class.std::__2::vector"* %attribute_element_types_, i32 %20) #9
  store i8 1, i8* %call23, align 1
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco9IndexTypeIjNS_19FaceIndex_tag_type_EE5valueEv(%"class.draco::IndexType.122"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.122"*, align 4
  store %"class.draco::IndexType.122"* %this, %"class.draco::IndexType.122"** %this.addr, align 4
  %this1 = load %"class.draco::IndexType.122"*, %"class.draco::IndexType.122"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.122", %"class.draco::IndexType.122"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value_, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::PointAttribute"* @_ZN5draco10PointCloud9attributeEi(%"class.draco::PointCloud"* %this, i32 %att_id) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointCloud"*, align 4
  %att_id.addr = alloca i32, align 4
  store %"class.draco::PointCloud"* %this, %"class.draco::PointCloud"** %this.addr, align 4
  store i32 %att_id, i32* %att_id.addr, align 4
  %this1 = load %"class.draco::PointCloud"*, %"class.draco::PointCloud"** %this.addr, align 4
  %attributes_ = getelementptr inbounds %"class.draco::PointCloud", %"class.draco::PointCloud"* %this1, i32 0, i32 2
  %0 = load i32, i32* %att_id.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.63"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEixEm(%"class.std::__2::vector.61"* %attributes_, i32 %0) #9
  %call2 = call %"class.draco::PointAttribute"* @_ZNKSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEE3getEv(%"class.std::__2::unique_ptr.63"* %call) #9
  ret %"class.draco::PointAttribute"* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco14PointAttribute17SetAttributeValueENS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEEPKv(%"class.draco::PointAttribute"* %this, i32 %entry_index.coerce, i8* %value) #0 comdat {
entry:
  %entry_index = alloca %"class.draco::IndexType", align 4
  %this.addr = alloca %"class.draco::PointAttribute"*, align 4
  %value.addr = alloca i8*, align 4
  %byte_pos = alloca i64, align 8
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %entry_index, i32 0, i32 0
  store i32 %entry_index.coerce, i32* %coerce.dive, align 4
  store %"class.draco::PointAttribute"* %this, %"class.draco::PointAttribute"** %this.addr, align 4
  store i8* %value, i8** %value.addr, align 4
  %this1 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %this.addr, align 4
  %call = call i32 @_ZNK5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EE5valueEv(%"class.draco::IndexType"* %entry_index)
  %conv = zext i32 %call to i64
  %0 = bitcast %"class.draco::PointAttribute"* %this1 to %"class.draco::GeometryAttribute"*
  %call2 = call i64 @_ZNK5draco17GeometryAttribute11byte_strideEv(%"class.draco::GeometryAttribute"* %0)
  %mul = mul nsw i64 %conv, %call2
  store i64 %mul, i64* %byte_pos, align 8
  %call3 = call %"class.draco::DataBuffer"* @_ZNK5draco14PointAttribute6bufferEv(%"class.draco::PointAttribute"* %this1)
  %1 = load i64, i64* %byte_pos, align 8
  %2 = load i8*, i8** %value.addr, align 4
  %3 = bitcast %"class.draco::PointAttribute"* %this1 to %"class.draco::GeometryAttribute"*
  %call4 = call i64 @_ZNK5draco17GeometryAttribute11byte_strideEv(%"class.draco::GeometryAttribute"* %3)
  %conv5 = trunc i64 %call4 to i32
  call void @_ZN5draco10DataBuffer5WriteExPKvm(%"class.draco::DataBuffer"* %call3, i64 %1, i8* %2, i32 %conv5)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType"* @_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEC2Ej(%"class.draco::IndexType"* returned %this, i32 %value) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType"*, align 4
  %value.addr = alloca i32, align 4
  store %"class.draco::IndexType"* %this, %"class.draco::IndexType"** %this.addr, align 4
  store i32 %value, i32* %value.addr, align 4
  %this1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value.addr, align 4
  store i32 %0, i32* %value_, align 4
  ret %"class.draco::IndexType"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco4Mesh7SetFaceENS_9IndexTypeIjNS_19FaceIndex_tag_type_EEERKNSt3__25arrayINS1_IjNS_20PointIndex_tag_type_EEELm3EEE(%"class.draco::Mesh"* %this, i32 %face_id.coerce, %"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %face) #0 comdat {
entry:
  %face_id = alloca %"class.draco::IndexType.122", align 4
  %this.addr = alloca %"class.draco::Mesh"*, align 4
  %face.addr = alloca %"struct.std::__2::array"*, align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp5 = alloca %"struct.std::__2::array", align 4
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType.122", %"class.draco::IndexType.122"* %face_id, i32 0, i32 0
  store i32 %face_id.coerce, i32* %coerce.dive, align 4
  store %"class.draco::Mesh"* %this, %"class.draco::Mesh"** %this.addr, align 4
  store %"struct.std::__2::array"* %face, %"struct.std::__2::array"** %face.addr, align 4
  %this1 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %this.addr, align 4
  %faces_ = getelementptr inbounds %"class.draco::Mesh", %"class.draco::Mesh"* %this1, i32 0, i32 2
  %call = call i32 @_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEENSt3__25arrayINS1_IjNS_20PointIndex_tag_type_EEELm3EEEE4sizeEv(%"class.draco::IndexTypeVector.111"* %faces_)
  store i32 %call, i32* %ref.tmp, align 4
  %call2 = call zeroext i1 @_ZNK5draco9IndexTypeIjNS_19FaceIndex_tag_type_EEgeERKj(%"class.draco::IndexType.122"* %face_id, i32* nonnull align 4 dereferenceable(4) %ref.tmp)
  br i1 %call2, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %faces_3 = getelementptr inbounds %"class.draco::Mesh", %"class.draco::Mesh"* %this1, i32 0, i32 2
  %call4 = call i32 @_ZNK5draco9IndexTypeIjNS_19FaceIndex_tag_type_EE5valueEv(%"class.draco::IndexType.122"* %face_id)
  %add = add i32 %call4, 1
  %0 = bitcast %"struct.std::__2::array"* %ref.tmp5 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %0, i8 0, i32 12, i1 false)
  %call6 = call %"struct.std::__2::array"* @_ZNSt3__25arrayIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELm3EEC2Ev(%"struct.std::__2::array"* %ref.tmp5)
  call void @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEENSt3__25arrayINS1_IjNS_20PointIndex_tag_type_EEELm3EEEE6resizeEmRKS8_(%"class.draco::IndexTypeVector.111"* %faces_3, i32 %add, %"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %ref.tmp5)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %face.addr, align 4
  %faces_7 = getelementptr inbounds %"class.draco::Mesh", %"class.draco::Mesh"* %this1, i32 0, i32 2
  %call8 = call nonnull align 4 dereferenceable(12) %"struct.std::__2::array"* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEENSt3__25arrayINS1_IjNS_20PointIndex_tag_type_EEELm3EEEEixERKS3_(%"class.draco::IndexTypeVector.111"* %faces_7, %"class.draco::IndexType.122"* nonnull align 4 dereferenceable(4) %face_id)
  %call9 = call nonnull align 4 dereferenceable(12) %"struct.std::__2::array"* @_ZNSt3__25arrayIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELm3EEaSERKS5_(%"struct.std::__2::array"* %call8, %"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %1)
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType.114"* @_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEC2Ej(%"class.draco::IndexType.114"* returned %this, i32 %value) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.114"*, align 4
  %value.addr = alloca i32, align 4
  store %"class.draco::IndexType.114"* %this, %"class.draco::IndexType.114"** %this.addr, align 4
  store i32 %value, i32* %value.addr, align 4
  %this1 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value.addr, align 4
  store i32 %0, i32* %value_, align 4
  ret %"class.draco::IndexType.114"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) i8* @_ZNSt3__26vectorIaNS_9allocatorIaEEEixEm(%"class.std::__2::vector"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 0
  %1 = load i8*, i8** %__begin_, align 4
  %2 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds i8, i8* %1, i32 %2
  ret i8* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN5draco23TriangleSoupMeshBuilder31SetPerFaceAttributeValueForFaceEiNS_9IndexTypeIjNS_19FaceIndex_tag_type_EEEPKv(%"class.draco::TriangleSoupMeshBuilder"* %this, i32 %att_id, i32 %face_id.coerce, i8* %value) #0 {
entry:
  %face_id = alloca %"class.draco::IndexType.122", align 4
  %this.addr = alloca %"class.draco::TriangleSoupMeshBuilder"*, align 4
  %att_id.addr = alloca i32, align 4
  %value.addr = alloca i8*, align 4
  %start_index = alloca i32, align 4
  %att = alloca %"class.draco::PointAttribute"*, align 4
  %agg.tmp = alloca %"class.draco::IndexType", align 4
  %agg.tmp6 = alloca %"class.draco::IndexType", align 4
  %agg.tmp9 = alloca %"class.draco::IndexType", align 4
  %agg.tmp15 = alloca %"class.draco::IndexType.122", align 4
  %ref.tmp = alloca %"struct.std::__2::array", align 4
  %element_type = alloca i8*, align 4
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType.122", %"class.draco::IndexType.122"* %face_id, i32 0, i32 0
  store i32 %face_id.coerce, i32* %coerce.dive, align 4
  store %"class.draco::TriangleSoupMeshBuilder"* %this, %"class.draco::TriangleSoupMeshBuilder"** %this.addr, align 4
  store i32 %att_id, i32* %att_id.addr, align 4
  store i8* %value, i8** %value.addr, align 4
  %this1 = load %"class.draco::TriangleSoupMeshBuilder"*, %"class.draco::TriangleSoupMeshBuilder"** %this.addr, align 4
  %call = call i32 @_ZNK5draco9IndexTypeIjNS_19FaceIndex_tag_type_EE5valueEv(%"class.draco::IndexType.122"* %face_id)
  %mul = mul i32 3, %call
  store i32 %mul, i32* %start_index, align 4
  %mesh_ = getelementptr inbounds %"class.draco::TriangleSoupMeshBuilder", %"class.draco::TriangleSoupMeshBuilder"* %this1, i32 0, i32 1
  %call2 = call %"class.draco::Mesh"* @_ZNKSt3__210unique_ptrIN5draco4MeshENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr"* %mesh_) #9
  %0 = bitcast %"class.draco::Mesh"* %call2 to %"class.draco::PointCloud"*
  %1 = load i32, i32* %att_id.addr, align 4
  %call3 = call %"class.draco::PointAttribute"* @_ZN5draco10PointCloud9attributeEi(%"class.draco::PointCloud"* %0, i32 %1)
  store %"class.draco::PointAttribute"* %call3, %"class.draco::PointAttribute"** %att, align 4
  %2 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %att, align 4
  %3 = load i32, i32* %start_index, align 4
  %call4 = call %"class.draco::IndexType"* @_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEC2Ej(%"class.draco::IndexType"* %agg.tmp, i32 %3)
  %4 = load i8*, i8** %value.addr, align 4
  %coerce.dive5 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %agg.tmp, i32 0, i32 0
  %5 = load i32, i32* %coerce.dive5, align 4
  call void @_ZN5draco14PointAttribute17SetAttributeValueENS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEEPKv(%"class.draco::PointAttribute"* %2, i32 %5, i8* %4)
  %6 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %att, align 4
  %7 = load i32, i32* %start_index, align 4
  %add = add nsw i32 %7, 1
  %call7 = call %"class.draco::IndexType"* @_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEC2Ej(%"class.draco::IndexType"* %agg.tmp6, i32 %add)
  %8 = load i8*, i8** %value.addr, align 4
  %coerce.dive8 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %agg.tmp6, i32 0, i32 0
  %9 = load i32, i32* %coerce.dive8, align 4
  call void @_ZN5draco14PointAttribute17SetAttributeValueENS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEEPKv(%"class.draco::PointAttribute"* %6, i32 %9, i8* %8)
  %10 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %att, align 4
  %11 = load i32, i32* %start_index, align 4
  %add10 = add nsw i32 %11, 2
  %call11 = call %"class.draco::IndexType"* @_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEC2Ej(%"class.draco::IndexType"* %agg.tmp9, i32 %add10)
  %12 = load i8*, i8** %value.addr, align 4
  %coerce.dive12 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %agg.tmp9, i32 0, i32 0
  %13 = load i32, i32* %coerce.dive12, align 4
  call void @_ZN5draco14PointAttribute17SetAttributeValueENS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEEPKv(%"class.draco::PointAttribute"* %10, i32 %13, i8* %12)
  %mesh_13 = getelementptr inbounds %"class.draco::TriangleSoupMeshBuilder", %"class.draco::TriangleSoupMeshBuilder"* %this1, i32 0, i32 1
  %call14 = call %"class.draco::Mesh"* @_ZNKSt3__210unique_ptrIN5draco4MeshENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr"* %mesh_13) #9
  %14 = bitcast %"class.draco::IndexType.122"* %agg.tmp15 to i8*
  %15 = bitcast %"class.draco::IndexType.122"* %face_id to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %14, i8* align 4 %15, i32 4, i1 false)
  %__elems_ = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %ref.tmp, i32 0, i32 0
  %arrayinit.begin = getelementptr inbounds [3 x %"class.draco::IndexType.114"], [3 x %"class.draco::IndexType.114"]* %__elems_, i32 0, i32 0
  %16 = load i32, i32* %start_index, align 4
  %call16 = call %"class.draco::IndexType.114"* @_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEC2Ej(%"class.draco::IndexType.114"* %arrayinit.begin, i32 %16)
  %arrayinit.element = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %arrayinit.begin, i32 1
  %17 = load i32, i32* %start_index, align 4
  %add17 = add nsw i32 %17, 1
  %call18 = call %"class.draco::IndexType.114"* @_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEC2Ej(%"class.draco::IndexType.114"* %arrayinit.element, i32 %add17)
  %arrayinit.element19 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %arrayinit.element, i32 1
  %18 = load i32, i32* %start_index, align 4
  %add20 = add nsw i32 %18, 2
  %call21 = call %"class.draco::IndexType.114"* @_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEC2Ej(%"class.draco::IndexType.114"* %arrayinit.element19, i32 %add20)
  %coerce.dive22 = getelementptr inbounds %"class.draco::IndexType.122", %"class.draco::IndexType.122"* %agg.tmp15, i32 0, i32 0
  %19 = load i32, i32* %coerce.dive22, align 4
  call void @_ZN5draco4Mesh7SetFaceENS_9IndexTypeIjNS_19FaceIndex_tag_type_EEERKNSt3__25arrayINS1_IjNS_20PointIndex_tag_type_EEELm3EEE(%"class.draco::Mesh"* %call14, i32 %19, %"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %ref.tmp)
  %attribute_element_types_ = getelementptr inbounds %"class.draco::TriangleSoupMeshBuilder", %"class.draco::TriangleSoupMeshBuilder"* %this1, i32 0, i32 0
  %20 = load i32, i32* %att_id.addr, align 4
  %call23 = call nonnull align 1 dereferenceable(1) i8* @_ZNSt3__26vectorIaNS_9allocatorIaEEEixEm(%"class.std::__2::vector"* %attribute_element_types_, i32 %20) #9
  store i8* %call23, i8** %element_type, align 4
  %21 = load i8*, i8** %element_type, align 4
  %22 = load i8, i8* %21, align 1
  %conv = sext i8 %22 to i32
  %cmp = icmp slt i32 %conv, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %23 = load i8*, i8** %element_type, align 4
  store i8 2, i8* %23, align 1
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN5draco23TriangleSoupMeshBuilder8FinalizeEv(%"class.std::__2::unique_ptr"* noalias sret align 4 %agg.result, %"class.draco::TriangleSoupMeshBuilder"* %this) #0 {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %"class.draco::TriangleSoupMeshBuilder"*, align 4
  %i = alloca i32, align 4
  %0 = bitcast %"class.std::__2::unique_ptr"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %"class.draco::TriangleSoupMeshBuilder"* %this, %"class.draco::TriangleSoupMeshBuilder"** %this.addr, align 4
  %this1 = load %"class.draco::TriangleSoupMeshBuilder"*, %"class.draco::TriangleSoupMeshBuilder"** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %attribute_element_types_ = getelementptr inbounds %"class.draco::TriangleSoupMeshBuilder", %"class.draco::TriangleSoupMeshBuilder"* %this1, i32 0, i32 0
  %call = call i32 @_ZNKSt3__26vectorIaNS_9allocatorIaEEE4sizeEv(%"class.std::__2::vector"* %attribute_element_types_) #9
  %cmp = icmp ult i32 %1, %call
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %attribute_element_types_2 = getelementptr inbounds %"class.draco::TriangleSoupMeshBuilder", %"class.draco::TriangleSoupMeshBuilder"* %this1, i32 0, i32 0
  %2 = load i32, i32* %i, align 4
  %call3 = call nonnull align 1 dereferenceable(1) i8* @_ZNSt3__26vectorIaNS_9allocatorIaEEEixEm(%"class.std::__2::vector"* %attribute_element_types_2, i32 %2) #9
  %3 = load i8, i8* %call3, align 1
  %conv = sext i8 %3 to i32
  %cmp4 = icmp sge i32 %conv, 0
  br i1 %cmp4, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %mesh_ = getelementptr inbounds %"class.draco::TriangleSoupMeshBuilder", %"class.draco::TriangleSoupMeshBuilder"* %this1, i32 0, i32 1
  %call5 = call %"class.draco::Mesh"* @_ZNKSt3__210unique_ptrIN5draco4MeshENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr"* %mesh_) #9
  %4 = load i32, i32* %i, align 4
  %attribute_element_types_6 = getelementptr inbounds %"class.draco::TriangleSoupMeshBuilder", %"class.draco::TriangleSoupMeshBuilder"* %this1, i32 0, i32 0
  %5 = load i32, i32* %i, align 4
  %call7 = call nonnull align 1 dereferenceable(1) i8* @_ZNSt3__26vectorIaNS_9allocatorIaEEEixEm(%"class.std::__2::vector"* %attribute_element_types_6, i32 %5) #9
  %6 = load i8, i8* %call7, align 1
  %conv8 = sext i8 %6 to i32
  call void @_ZN5draco4Mesh23SetAttributeElementTypeEiNS_24MeshAttributeElementTypeE(%"class.draco::Mesh"* %call5, i32 %4, i32 %conv8)
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %7 = load i32, i32* %i, align 4
  %inc = add i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %mesh_9 = getelementptr inbounds %"class.draco::TriangleSoupMeshBuilder", %"class.draco::TriangleSoupMeshBuilder"* %this1, i32 0, i32 1
  %call10 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr"* @_ZNSt3__24moveIRNS_10unique_ptrIN5draco4MeshENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOS9_(%"class.std::__2::unique_ptr"* nonnull align 4 dereferenceable(4) %mesh_9) #9
  %call11 = call %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrIN5draco4MeshENS_14default_deleteIS2_EEEC2EOS5_(%"class.std::__2::unique_ptr"* %agg.result, %"class.std::__2::unique_ptr"* nonnull align 4 dereferenceable(4) %call10) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIaNS_9allocatorIaEEE4sizeEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 1
  %1 = load i8*, i8** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %2, i32 0, i32 0
  %3 = load i8*, i8** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint i8* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  ret i32 %sub.ptr.sub
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco4Mesh23SetAttributeElementTypeEiNS_24MeshAttributeElementTypeE(%"class.draco::Mesh"* %this, i32 %att_id, i32 %et) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::Mesh"*, align 4
  %att_id.addr = alloca i32, align 4
  %et.addr = alloca i32, align 4
  store %"class.draco::Mesh"* %this, %"class.draco::Mesh"** %this.addr, align 4
  store i32 %att_id, i32* %att_id.addr, align 4
  store i32 %et, i32* %et.addr, align 4
  %this1 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %this.addr, align 4
  %0 = load i32, i32* %et.addr, align 4
  %attribute_data_ = getelementptr inbounds %"class.draco::Mesh", %"class.draco::Mesh"* %this1, i32 0, i32 1
  %1 = load i32, i32* %att_id.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"struct.draco::Mesh::AttributeData"* @_ZNSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEEixEm(%"class.std::__2::vector.104"* %attribute_data_, i32 %1) #9
  %element_type = getelementptr inbounds %"struct.draco::Mesh::AttributeData", %"struct.draco::Mesh::AttributeData"* %call, i32 0, i32 0
  store i32 %0, i32* %element_type, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr"* @_ZNSt3__24moveIRNS_10unique_ptrIN5draco4MeshENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOS9_(%"class.std::__2::unique_ptr"* nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  store %"class.std::__2::unique_ptr"* %__t, %"class.std::__2::unique_ptr"** %__t.addr, align 4
  %0 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %__t.addr, align 4
  ret %"class.std::__2::unique_ptr"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrIN5draco4MeshENS_14default_deleteIS2_EEEC2EOS5_(%"class.std::__2::unique_ptr"* returned %this, %"class.std::__2::unique_ptr"* nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  %__u.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  %ref.tmp = alloca %"class.draco::Mesh"*, align 4
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  store %"class.std::__2::unique_ptr"* %__u, %"class.std::__2::unique_ptr"** %__u.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %__u.addr, align 4
  %call = call %"class.draco::Mesh"* @_ZNSt3__210unique_ptrIN5draco4MeshENS_14default_deleteIS2_EEE7releaseEv(%"class.std::__2::unique_ptr"* %0) #9
  store %"class.draco::Mesh"* %call, %"class.draco::Mesh"** %ref.tmp, align 4
  %1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %__u.addr, align 4
  %call2 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.121"* @_ZNSt3__210unique_ptrIN5draco4MeshENS_14default_deleteIS2_EEE11get_deleterEv(%"class.std::__2::unique_ptr"* %1) #9
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.121"* @_ZNSt3__27forwardINS_14default_deleteIN5draco4MeshEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.121"* nonnull align 1 dereferenceable(1) %call2) #9
  %call4 = call %"class.std::__2::__compressed_pair.1"* @_ZNSt3__217__compressed_pairIPN5draco4MeshENS_14default_deleteIS2_EEEC2IS3_S5_EEOT_OT0_(%"class.std::__2::__compressed_pair.1"* %__ptr_, %"class.draco::Mesh"** nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::default_delete.121"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::unique_ptr"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEENSt3__25arrayINS1_IjNS_20PointIndex_tag_type_EEELm3EEEE6resizeEmRKS8_(%"class.draco::IndexTypeVector.111"* %this, i32 %size, %"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %val) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector.111"*, align 4
  %size.addr = alloca i32, align 4
  %val.addr = alloca %"struct.std::__2::array"*, align 4
  store %"class.draco::IndexTypeVector.111"* %this, %"class.draco::IndexTypeVector.111"** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  store %"struct.std::__2::array"* %val, %"struct.std::__2::array"** %val.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector.111"*, %"class.draco::IndexTypeVector.111"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector.111", %"class.draco::IndexTypeVector.111"* %this1, i32 0, i32 0
  %0 = load i32, i32* %size.addr, align 4
  %1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %val.addr, align 4
  call void @_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE6resizeEmRKS6_(%"class.std::__2::vector.112"* %vector_, i32 %0, %"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %1)
  ret void
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::array"* @_ZNSt3__25arrayIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELm3EEC2Ev(%"struct.std::__2::array"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"struct.std::__2::array"*, align 4
  %this.addr = alloca %"struct.std::__2::array"*, align 4
  store %"struct.std::__2::array"* %this, %"struct.std::__2::array"** %this.addr, align 4
  %this1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %this.addr, align 4
  store %"struct.std::__2::array"* %this1, %"struct.std::__2::array"** %retval, align 4
  %__elems_ = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %"class.draco::IndexType.114"], [3 x %"class.draco::IndexType.114"]* %__elems_, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %"class.draco::IndexType.114"* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %"class.draco::IndexType.114"* @_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEC2Ev(%"class.draco::IndexType.114"* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %"class.draco::IndexType.114"* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %retval, align 4
  ret %"struct.std::__2::array"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE6resizeEmRKS6_(%"class.std::__2::vector.112"* %this, i32 %__sz, %"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %__x) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.112"*, align 4
  %__sz.addr = alloca i32, align 4
  %__x.addr = alloca %"struct.std::__2::array"*, align 4
  %__cs = alloca i32, align 4
  store %"class.std::__2::vector.112"* %this, %"class.std::__2::vector.112"** %this.addr, align 4
  store i32 %__sz, i32* %__sz.addr, align 4
  store %"struct.std::__2::array"* %__x, %"struct.std::__2::array"** %__x.addr, align 4
  %this1 = load %"class.std::__2::vector.112"*, %"class.std::__2::vector.112"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.112"* %this1) #9
  store i32 %call, i32* %__cs, align 4
  %0 = load i32, i32* %__cs, align 4
  %1 = load i32, i32* %__sz.addr, align 4
  %cmp = icmp ult i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %__sz.addr, align 4
  %3 = load i32, i32* %__cs, align 4
  %sub = sub i32 %2, %3
  %4 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__x.addr, align 4
  call void @_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE8__appendEmRKS6_(%"class.std::__2::vector.112"* %this1, i32 %sub, %"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %4)
  br label %if.end4

if.else:                                          ; preds = %entry
  %5 = load i32, i32* %__cs, align 4
  %6 = load i32, i32* %__sz.addr, align 4
  %cmp2 = icmp ugt i32 %5, %6
  br i1 %cmp2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.else
  %7 = bitcast %"class.std::__2::vector.112"* %this1 to %"class.std::__2::__vector_base.113"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.113", %"class.std::__2::__vector_base.113"* %7, i32 0, i32 0
  %8 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__begin_, align 4
  %9 = load i32, i32* %__sz.addr, align 4
  %add.ptr = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %8, i32 %9
  call void @_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE17__destruct_at_endEPS6_(%"class.std::__2::vector.112"* %this1, %"struct.std::__2::array"* %add.ptr) #9
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.else
  br label %if.end4

if.end4:                                          ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.112"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.112"*, align 4
  store %"class.std::__2::vector.112"* %this, %"class.std::__2::vector.112"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.112"*, %"class.std::__2::vector.112"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.112"* %this1 to %"class.std::__2::__vector_base.113"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.113", %"class.std::__2::__vector_base.113"* %0, i32 0, i32 1
  %1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector.112"* %this1 to %"class.std::__2::__vector_base.113"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.113", %"class.std::__2::__vector_base.113"* %2, i32 0, i32 0
  %3 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"struct.std::__2::array"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"struct.std::__2::array"* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 12
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE8__appendEmRKS6_(%"class.std::__2::vector.112"* %this, i32 %__n, %"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %__x) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.112"*, align 4
  %__n.addr = alloca i32, align 4
  %__x.addr = alloca %"struct.std::__2::array"*, align 4
  %__a = alloca %"class.std::__2::allocator.118"*, align 4
  %__v = alloca %"struct.std::__2::__split_buffer", align 4
  store %"class.std::__2::vector.112"* %this, %"class.std::__2::vector.112"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store %"struct.std::__2::array"* %__x, %"struct.std::__2::array"** %__x.addr, align 4
  %this1 = load %"class.std::__2::vector.112"*, %"class.std::__2::vector.112"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.112"* %this1 to %"class.std::__2::__vector_base.113"*
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE9__end_capEv(%"class.std::__2::__vector_base.113"* %0) #9
  %1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %call, align 4
  %2 = bitcast %"class.std::__2::vector.112"* %this1 to %"class.std::__2::__vector_base.113"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.113", %"class.std::__2::__vector_base.113"* %2, i32 0, i32 1
  %3 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__end_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"struct.std::__2::array"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"struct.std::__2::array"* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 12
  %4 = load i32, i32* %__n.addr, align 4
  %cmp = icmp uge i32 %sub.ptr.div, %4
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %5 = load i32, i32* %__n.addr, align 4
  %6 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__x.addr, align 4
  call void @_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE18__construct_at_endEmRKS6_(%"class.std::__2::vector.112"* %this1, i32 %5, %"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %6)
  br label %if.end

if.else:                                          ; preds = %entry
  %7 = bitcast %"class.std::__2::vector.112"* %this1 to %"class.std::__2::__vector_base.113"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.118"* @_ZNSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base.113"* %7) #9
  store %"class.std::__2::allocator.118"* %call2, %"class.std::__2::allocator.118"** %__a, align 4
  %call3 = call i32 @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.112"* %this1) #9
  %8 = load i32, i32* %__n.addr, align 4
  %add = add i32 %call3, %8
  %call4 = call i32 @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE11__recommendEm(%"class.std::__2::vector.112"* %this1, i32 %add)
  %call5 = call i32 @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.112"* %this1) #9
  %9 = load %"class.std::__2::allocator.118"*, %"class.std::__2::allocator.118"** %__a, align 4
  %call6 = call %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEEC2EmmS9_(%"struct.std::__2::__split_buffer"* %__v, i32 %call4, i32 %call5, %"class.std::__2::allocator.118"* nonnull align 1 dereferenceable(1) %9)
  %10 = load i32, i32* %__n.addr, align 4
  %11 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__x.addr, align 4
  call void @_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE18__construct_at_endEmRKS6_(%"struct.std::__2::__split_buffer"* %__v, i32 %10, %"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %11)
  call void @_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS6_RS8_EE(%"class.std::__2::vector.112"* %this1, %"struct.std::__2::__split_buffer"* nonnull align 4 dereferenceable(20) %__v)
  %call7 = call %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEED2Ev(%"struct.std::__2::__split_buffer"* %__v) #9
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE17__destruct_at_endEPS6_(%"class.std::__2::vector.112"* %this, %"struct.std::__2::array"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.112"*, align 4
  %__new_last.addr = alloca %"struct.std::__2::array"*, align 4
  %__old_size = alloca i32, align 4
  store %"class.std::__2::vector.112"* %this, %"class.std::__2::vector.112"** %this.addr, align 4
  store %"struct.std::__2::array"* %__new_last, %"struct.std::__2::array"** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::vector.112"*, %"class.std::__2::vector.112"** %this.addr, align 4
  %0 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__new_last.addr, align 4
  call void @_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE27__invalidate_iterators_pastEPS6_(%"class.std::__2::vector.112"* %this1, %"struct.std::__2::array"* %0)
  %call = call i32 @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.112"* %this1) #9
  store i32 %call, i32* %__old_size, align 4
  %1 = bitcast %"class.std::__2::vector.112"* %this1 to %"class.std::__2::__vector_base.113"*
  %2 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__new_last.addr, align 4
  call void @_ZNSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE17__destruct_at_endEPS6_(%"class.std::__2::__vector_base.113"* %1, %"struct.std::__2::array"* %2) #9
  %3 = load i32, i32* %__old_size, align 4
  call void @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE17__annotate_shrinkEm(%"class.std::__2::vector.112"* %this1, i32 %3) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE9__end_capEv(%"class.std::__2::__vector_base.113"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.113"*, align 4
  store %"class.std::__2::__vector_base.113"* %this, %"class.std::__2::__vector_base.113"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.113"*, %"class.std::__2::__vector_base.113"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.113", %"class.std::__2::__vector_base.113"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.115"* %__end_cap_) #9
  ret %"struct.std::__2::array"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE18__construct_at_endEmRKS6_(%"class.std::__2::vector.112"* %this, i32 %__n, %"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %__x) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.112"*, align 4
  %__n.addr = alloca i32, align 4
  %__x.addr = alloca %"struct.std::__2::array"*, align 4
  %__tx = alloca %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction", align 4
  store %"class.std::__2::vector.112"* %this, %"class.std::__2::vector.112"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store %"struct.std::__2::array"* %__x, %"struct.std::__2::array"** %__x.addr, align 4
  %this1 = load %"class.std::__2::vector.112"*, %"class.std::__2::vector.112"** %this.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %call = call %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"* @_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE21_ConstructTransactionC2ERS9_m(%"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"* %__tx, %"class.std::__2::vector.112"* nonnull align 4 dereferenceable(12) %this1, i32 %0)
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__pos_, align 4
  %__new_end_ = getelementptr inbounds %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"* %__tx, i32 0, i32 2
  %2 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__new_end_, align 4
  %cmp = icmp ne %"struct.std::__2::array"* %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = bitcast %"class.std::__2::vector.112"* %this1 to %"class.std::__2::__vector_base.113"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.118"* @_ZNSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base.113"* %3) #9
  %__pos_3 = getelementptr inbounds %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %4 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__pos_3, align 4
  %call4 = call %"struct.std::__2::array"* @_ZNSt3__212__to_addressINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEEEPT_S8_(%"struct.std::__2::array"* %4) #9
  %5 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__x.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE9constructIS7_JRKS7_EEEvRS8_PT_DpOT0_(%"class.std::__2::allocator.118"* nonnull align 1 dereferenceable(1) %call2, %"struct.std::__2::array"* %call4, %"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %5)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %__pos_5 = getelementptr inbounds %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %6 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__pos_5, align 4
  %incdec.ptr = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %6, i32 1
  store %"struct.std::__2::array"* %incdec.ptr, %"struct.std::__2::array"** %__pos_5, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call6 = call %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"* @_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"* %__tx) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.118"* @_ZNSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base.113"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.113"*, align 4
  store %"class.std::__2::__vector_base.113"* %this, %"class.std::__2::__vector_base.113"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.113"*, %"class.std::__2::__vector_base.113"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.113", %"class.std::__2::__vector_base.113"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.118"* @_ZNSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE6secondEv(%"class.std::__2::__compressed_pair.115"* %__end_cap_) #9
  ret %"class.std::__2::allocator.118"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE11__recommendEm(%"class.std::__2::vector.112"* %this, i32 %__new_size) #0 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %"class.std::__2::vector.112"*, align 4
  %__new_size.addr = alloca i32, align 4
  %__ms = alloca i32, align 4
  %__cap = alloca i32, align 4
  %ref.tmp = alloca i32, align 4
  store %"class.std::__2::vector.112"* %this, %"class.std::__2::vector.112"** %this.addr, align 4
  store i32 %__new_size, i32* %__new_size.addr, align 4
  %this1 = load %"class.std::__2::vector.112"*, %"class.std::__2::vector.112"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE8max_sizeEv(%"class.std::__2::vector.112"* %this1) #9
  store i32 %call, i32* %__ms, align 4
  %0 = load i32, i32* %__new_size.addr, align 4
  %1 = load i32, i32* %__ms, align 4
  %cmp = icmp ugt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = bitcast %"class.std::__2::vector.112"* %this1 to %"class.std::__2::__vector_base_common"*
  call void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"* %2) #10
  unreachable

if.end:                                           ; preds = %entry
  %call2 = call i32 @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector.112"* %this1) #9
  store i32 %call2, i32* %__cap, align 4
  %3 = load i32, i32* %__cap, align 4
  %4 = load i32, i32* %__ms, align 4
  %div = udiv i32 %4, 2
  %cmp3 = icmp uge i32 %3, %div
  br i1 %cmp3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %if.end
  %5 = load i32, i32* %__ms, align 4
  store i32 %5, i32* %retval, align 4
  br label %return

if.end5:                                          ; preds = %if.end
  %6 = load i32, i32* %__cap, align 4
  %mul = mul i32 2, %6
  store i32 %mul, i32* %ref.tmp, align 4
  %call6 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %__new_size.addr)
  %7 = load i32, i32* %call6, align 4
  store i32 %7, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end5, %if.then4
  %8 = load i32, i32* %retval, align 4
  ret i32 %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEEC2EmmS9_(%"struct.std::__2::__split_buffer"* returned %this, i32 %__cap, i32 %__start, %"class.std::__2::allocator.118"* nonnull align 1 dereferenceable(1) %__a) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"struct.std::__2::__split_buffer"*, align 4
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__cap.addr = alloca i32, align 4
  %__start.addr = alloca i32, align 4
  %__a.addr = alloca %"class.std::__2::allocator.118"*, align 4
  %ref.tmp = alloca i8*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store i32 %__cap, i32* %__cap.addr, align 4
  store i32 %__start, i32* %__start.addr, align 4
  store %"class.std::__2::allocator.118"* %__a, %"class.std::__2::allocator.118"** %__a.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer"* %this1, %"struct.std::__2::__split_buffer"** %retval, align 4
  %0 = bitcast %"struct.std::__2::__split_buffer"* %this1 to %"class.std::__2::__split_buffer_common"*
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  store i8* null, i8** %ref.tmp, align 4
  %1 = load %"class.std::__2::allocator.118"*, %"class.std::__2::allocator.118"** %__a.addr, align 4
  %call = call %"class.std::__2::__compressed_pair.123"* @_ZNSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEEC2IDnSA_EEOT_OT0_(%"class.std::__2::__compressed_pair.123"* %__end_cap_, i8** nonnull align 4 dereferenceable(4) %ref.tmp, %"class.std::__2::allocator.118"* nonnull align 1 dereferenceable(1) %1)
  %2 = load i32, i32* %__cap.addr, align 4
  %cmp = icmp ne i32 %2, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.118"* @_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE7__allocEv(%"struct.std::__2::__split_buffer"* %this1) #9
  %3 = load i32, i32* %__cap.addr, align 4
  %call3 = call %"struct.std::__2::array"* @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE8allocateERS8_m(%"class.std::__2::allocator.118"* nonnull align 1 dereferenceable(1) %call2, i32 %3)
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %"struct.std::__2::array"* [ %call3, %cond.true ], [ null, %cond.false ]
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  store %"struct.std::__2::array"* %cond, %"struct.std::__2::array"** %__first_, align 4
  %__first_4 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %4 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__first_4, align 4
  %5 = load i32, i32* %__start.addr, align 4
  %add.ptr = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %4, i32 %5
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 2
  store %"struct.std::__2::array"* %add.ptr, %"struct.std::__2::array"** %__end_, align 4
  %__begin_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 1
  store %"struct.std::__2::array"* %add.ptr, %"struct.std::__2::array"** %__begin_, align 4
  %__first_5 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %6 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__first_5, align 4
  %7 = load i32, i32* %__cap.addr, align 4
  %add.ptr6 = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %6, i32 %7
  %call7 = call nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this1) #9
  store %"struct.std::__2::array"* %add.ptr6, %"struct.std::__2::array"** %call7, align 4
  %8 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %retval, align 4
  ret %"struct.std::__2::__split_buffer"* %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE18__construct_at_endEmRKS6_(%"struct.std::__2::__split_buffer"* %this, i32 %__n, %"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %__x) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__n.addr = alloca i32, align 4
  %__x.addr = alloca %"struct.std::__2::array"*, align 4
  %__tx = alloca %"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction", align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store %"struct.std::__2::array"* %__x, %"struct.std::__2::array"** %__x.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 2
  %0 = load i32, i32* %__n.addr, align 4
  %call = call %"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE21_ConstructTransactionC2EPPS6_m(%"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction"* %__tx, %"struct.std::__2::array"** %__end_, i32 %0) #9
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %__pos_ = getelementptr inbounds %"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction"* %__tx, i32 0, i32 0
  %1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__pos_, align 4
  %__end_2 = getelementptr inbounds %"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %2 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__end_2, align 4
  %cmp = icmp ne %"struct.std::__2::array"* %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %call3 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.118"* @_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE7__allocEv(%"struct.std::__2::__split_buffer"* %this1) #9
  %__pos_4 = getelementptr inbounds %"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction"* %__tx, i32 0, i32 0
  %3 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__pos_4, align 4
  %call5 = call %"struct.std::__2::array"* @_ZNSt3__212__to_addressINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEEEPT_S8_(%"struct.std::__2::array"* %3) #9
  %4 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__x.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE9constructIS7_JRKS7_EEEvRS8_PT_DpOT0_(%"class.std::__2::allocator.118"* nonnull align 1 dereferenceable(1) %call3, %"struct.std::__2::array"* %call5, %"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %4)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %__pos_6 = getelementptr inbounds %"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction"* %__tx, i32 0, i32 0
  %5 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__pos_6, align 4
  %incdec.ptr = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %5, i32 1
  store %"struct.std::__2::array"* %incdec.ptr, %"struct.std::__2::array"** %__pos_6, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call7 = call %"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction"* %__tx) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS6_RS8_EE(%"class.std::__2::vector.112"* %this, %"struct.std::__2::__split_buffer"* nonnull align 4 dereferenceable(20) %__v) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.112"*, align 4
  %__v.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"class.std::__2::vector.112"* %this, %"class.std::__2::vector.112"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer"* %__v, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %this1 = load %"class.std::__2::vector.112"*, %"class.std::__2::vector.112"** %this.addr, align 4
  call void @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE17__annotate_deleteEv(%"class.std::__2::vector.112"* %this1) #9
  %0 = bitcast %"class.std::__2::vector.112"* %this1 to %"class.std::__2::__vector_base.113"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.118"* @_ZNSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base.113"* %0) #9
  %1 = bitcast %"class.std::__2::vector.112"* %this1 to %"class.std::__2::__vector_base.113"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.113", %"class.std::__2::__vector_base.113"* %1, i32 0, i32 0
  %2 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__begin_, align 4
  %3 = bitcast %"class.std::__2::vector.112"* %this1 to %"class.std::__2::__vector_base.113"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.113", %"class.std::__2::__vector_base.113"* %3, i32 0, i32 1
  %4 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__end_, align 4
  %5 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__begin_2 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %5, i32 0, i32 1
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE46__construct_backward_with_exception_guaranteesIS7_EENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS8_PT_SE_EE5valuesr31is_trivially_move_constructibleISE_EE5valueEvE4typeERS8_SF_SF_RSF_(%"class.std::__2::allocator.118"* nonnull align 1 dereferenceable(1) %call, %"struct.std::__2::array"* %2, %"struct.std::__2::array"* %4, %"struct.std::__2::array"** nonnull align 4 dereferenceable(4) %__begin_2)
  %6 = bitcast %"class.std::__2::vector.112"* %this1 to %"class.std::__2::__vector_base.113"*
  %__begin_3 = getelementptr inbounds %"class.std::__2::__vector_base.113", %"class.std::__2::__vector_base.113"* %6, i32 0, i32 0
  %7 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__begin_4 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %7, i32 0, i32 1
  call void @_ZNSt3__24swapIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS9_EE5valueEvE4typeERS9_SC_(%"struct.std::__2::array"** nonnull align 4 dereferenceable(4) %__begin_3, %"struct.std::__2::array"** nonnull align 4 dereferenceable(4) %__begin_4) #9
  %8 = bitcast %"class.std::__2::vector.112"* %this1 to %"class.std::__2::__vector_base.113"*
  %__end_5 = getelementptr inbounds %"class.std::__2::__vector_base.113", %"class.std::__2::__vector_base.113"* %8, i32 0, i32 1
  %9 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__end_6 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %9, i32 0, i32 2
  call void @_ZNSt3__24swapIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS9_EE5valueEvE4typeERS9_SC_(%"struct.std::__2::array"** nonnull align 4 dereferenceable(4) %__end_5, %"struct.std::__2::array"** nonnull align 4 dereferenceable(4) %__end_6) #9
  %10 = bitcast %"class.std::__2::vector.112"* %this1 to %"class.std::__2::__vector_base.113"*
  %call7 = call nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE9__end_capEv(%"class.std::__2::__vector_base.113"* %10) #9
  %11 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %call8 = call nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE9__end_capEv(%"struct.std::__2::__split_buffer"* %11) #9
  call void @_ZNSt3__24swapIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS9_EE5valueEvE4typeERS9_SC_(%"struct.std::__2::array"** nonnull align 4 dereferenceable(4) %call7, %"struct.std::__2::array"** nonnull align 4 dereferenceable(4) %call8) #9
  %12 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__begin_9 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %12, i32 0, i32 1
  %13 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__begin_9, align 4
  %14 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %14, i32 0, i32 0
  store %"struct.std::__2::array"* %13, %"struct.std::__2::array"** %__first_, align 4
  %call10 = call i32 @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.112"* %this1) #9
  call void @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE14__annotate_newEm(%"class.std::__2::vector.112"* %this1, i32 %call10) #9
  call void @_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector.112"* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEED2Ev(%"struct.std::__2::__split_buffer"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"struct.std::__2::__split_buffer"*, align 4
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer"* %this1, %"struct.std::__2::__split_buffer"** %retval, align 4
  call void @_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE5clearEv(%"struct.std::__2::__split_buffer"* %this1) #9
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %0 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__first_, align 4
  %tobool = icmp ne %"struct.std::__2::array"* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.118"* @_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE7__allocEv(%"struct.std::__2::__split_buffer"* %this1) #9
  %__first_2 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__first_2, align 4
  %call3 = call i32 @_ZNKSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE8capacityEv(%"struct.std::__2::__split_buffer"* %this1)
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE10deallocateERS8_PS7_m(%"class.std::__2::allocator.118"* nonnull align 1 dereferenceable(1) %call, %"struct.std::__2::array"* %1, i32 %call3) #9
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %retval, align 4
  ret %"struct.std::__2::__split_buffer"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.115"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.115"*, align 4
  store %"class.std::__2::__compressed_pair.115"* %this, %"class.std::__2::__compressed_pair.115"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.115"*, %"class.std::__2::__compressed_pair.115"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.115"* %this1 to %"struct.std::__2::__compressed_pair_elem.116"*
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNSt3__222__compressed_pair_elemIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.116"* %0) #9
  ret %"struct.std::__2::array"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNSt3__222__compressed_pair_elemIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.116"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.116"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.116"* %this, %"struct.std::__2::__compressed_pair_elem.116"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.116"*, %"struct.std::__2::__compressed_pair_elem.116"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.116", %"struct.std::__2::__compressed_pair_elem.116"* %this1, i32 0, i32 0
  ret %"struct.std::__2::array"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"* @_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE21_ConstructTransactionC2ERS9_m(%"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"* returned %this, %"class.std::__2::vector.112"* nonnull align 4 dereferenceable(12) %__v, i32 %__n) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"*, align 4
  %__v.addr = alloca %"class.std::__2::vector.112"*, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"** %this.addr, align 4
  store %"class.std::__2::vector.112"* %__v, %"class.std::__2::vector.112"** %__v.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"*, %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"** %this.addr, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::vector.112"*, %"class.std::__2::vector.112"** %__v.addr, align 4
  store %"class.std::__2::vector.112"* %0, %"class.std::__2::vector.112"** %__v_, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %1 = load %"class.std::__2::vector.112"*, %"class.std::__2::vector.112"** %__v.addr, align 4
  %2 = bitcast %"class.std::__2::vector.112"* %1 to %"class.std::__2::__vector_base.113"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.113", %"class.std::__2::__vector_base.113"* %2, i32 0, i32 1
  %3 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__end_, align 4
  store %"struct.std::__2::array"* %3, %"struct.std::__2::array"** %__pos_, align 4
  %__new_end_ = getelementptr inbounds %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"* %this1, i32 0, i32 2
  %4 = load %"class.std::__2::vector.112"*, %"class.std::__2::vector.112"** %__v.addr, align 4
  %5 = bitcast %"class.std::__2::vector.112"* %4 to %"class.std::__2::__vector_base.113"*
  %__end_2 = getelementptr inbounds %"class.std::__2::__vector_base.113", %"class.std::__2::__vector_base.113"* %5, i32 0, i32 1
  %6 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__end_2, align 4
  %7 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %6, i32 %7
  store %"struct.std::__2::array"* %add.ptr, %"struct.std::__2::array"** %__new_end_, align 4
  ret %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE9constructIS7_JRKS7_EEEvRS8_PT_DpOT0_(%"class.std::__2::allocator.118"* nonnull align 1 dereferenceable(1) %__a, %"struct.std::__2::array"* %__p, %"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %__args) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.118"*, align 4
  %__p.addr = alloca %"struct.std::__2::array"*, align 4
  %__args.addr = alloca %"struct.std::__2::array"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_construct", align 1
  store %"class.std::__2::allocator.118"* %__a, %"class.std::__2::allocator.118"** %__a.addr, align 4
  store %"struct.std::__2::array"* %__p, %"struct.std::__2::array"** %__p.addr, align 4
  store %"struct.std::__2::array"* %__args, %"struct.std::__2::array"** %__args.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_construct"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.118"*, %"class.std::__2::allocator.118"** %__a.addr, align 4
  %2 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__p.addr, align 4
  %3 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::array"* @_ZNSt3__27forwardIRKNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEEEOT_RNS_16remove_referenceIS9_E4typeE(%"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %3) #9
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE11__constructIS7_JRKS7_EEEvNS_17integral_constantIbLb1EEERS8_PT_DpOT0_(%"class.std::__2::allocator.118"* nonnull align 1 dereferenceable(1) %1, %"struct.std::__2::array"* %2, %"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::array"* @_ZNSt3__212__to_addressINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEEEPT_S8_(%"struct.std::__2::array"* %__p) #0 comdat {
entry:
  %__p.addr = alloca %"struct.std::__2::array"*, align 4
  store %"struct.std::__2::array"* %__p, %"struct.std::__2::array"** %__p.addr, align 4
  %0 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__p.addr, align 4
  ret %"struct.std::__2::array"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"* @_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"*, align 4
  store %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"** %this.addr, align 4
  %this1 = load %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"*, %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %0 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__pos_, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction", %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::vector.112"*, %"class.std::__2::vector.112"** %__v_, align 4
  %2 = bitcast %"class.std::__2::vector.112"* %1 to %"class.std::__2::__vector_base.113"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.113", %"class.std::__2::__vector_base.113"* %2, i32 0, i32 1
  store %"struct.std::__2::array"* %0, %"struct.std::__2::array"** %__end_, align 4
  ret %"struct.std::__2::vector<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE11__constructIS7_JRKS7_EEEvNS_17integral_constantIbLb1EEERS8_PT_DpOT0_(%"class.std::__2::allocator.118"* nonnull align 1 dereferenceable(1) %__a, %"struct.std::__2::array"* %__p, %"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %__args) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.118"*, align 4
  %__p.addr = alloca %"struct.std::__2::array"*, align 4
  %__args.addr = alloca %"struct.std::__2::array"*, align 4
  store %"class.std::__2::allocator.118"* %__a, %"class.std::__2::allocator.118"** %__a.addr, align 4
  store %"struct.std::__2::array"* %__p, %"struct.std::__2::array"** %__p.addr, align 4
  store %"struct.std::__2::array"* %__args, %"struct.std::__2::array"** %__args.addr, align 4
  %1 = load %"class.std::__2::allocator.118"*, %"class.std::__2::allocator.118"** %__a.addr, align 4
  %2 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__p.addr, align 4
  %3 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::array"* @_ZNSt3__27forwardIRKNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEEEOT_RNS_16remove_referenceIS9_E4typeE(%"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %3) #9
  call void @_ZNSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEE9constructIS6_JRKS6_EEEvPT_DpOT0_(%"class.std::__2::allocator.118"* %1, %"struct.std::__2::array"* %2, %"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"struct.std::__2::array"* @_ZNSt3__27forwardIRKNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEEEOT_RNS_16remove_referenceIS9_E4typeE(%"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"struct.std::__2::array"*, align 4
  store %"struct.std::__2::array"* %__t, %"struct.std::__2::array"** %__t.addr, align 4
  %0 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__t.addr, align 4
  ret %"struct.std::__2::array"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEE9constructIS6_JRKS6_EEEvPT_DpOT0_(%"class.std::__2::allocator.118"* %this, %"struct.std::__2::array"* %__p, %"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %__args) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.118"*, align 4
  %__p.addr = alloca %"struct.std::__2::array"*, align 4
  %__args.addr = alloca %"struct.std::__2::array"*, align 4
  store %"class.std::__2::allocator.118"* %this, %"class.std::__2::allocator.118"** %this.addr, align 4
  store %"struct.std::__2::array"* %__p, %"struct.std::__2::array"** %__p.addr, align 4
  store %"struct.std::__2::array"* %__args, %"struct.std::__2::array"** %__args.addr, align 4
  %this1 = load %"class.std::__2::allocator.118"*, %"class.std::__2::allocator.118"** %this.addr, align 4
  %0 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__p.addr, align 4
  %1 = bitcast %"struct.std::__2::array"* %0 to i8*
  %2 = bitcast i8* %1 to %"struct.std::__2::array"*
  %3 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::array"* @_ZNSt3__27forwardIRKNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEEEOT_RNS_16remove_referenceIS9_E4typeE(%"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %3) #9
  %4 = bitcast %"struct.std::__2::array"* %2 to i8*
  %5 = bitcast %"struct.std::__2::array"* %call to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 12, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.118"* @_ZNSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE6secondEv(%"class.std::__2::__compressed_pair.115"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.115"*, align 4
  store %"class.std::__2::__compressed_pair.115"* %this, %"class.std::__2::__compressed_pair.115"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.115"*, %"class.std::__2::__compressed_pair.115"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.115"* %this1 to %"struct.std::__2::__compressed_pair_elem.117"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.118"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.117"* %0) #9
  ret %"class.std::__2::allocator.118"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.118"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.117"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.117"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.117"* %this, %"struct.std::__2::__compressed_pair_elem.117"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.117"*, %"struct.std::__2::__compressed_pair_elem.117"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.117"* %this1 to %"class.std::__2::allocator.118"*
  ret %"class.std::__2::allocator.118"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE8max_sizeEv(%"class.std::__2::vector.112"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.112"*, align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp3 = alloca i32, align 4
  store %"class.std::__2::vector.112"* %this, %"class.std::__2::vector.112"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.112"*, %"class.std::__2::vector.112"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.112"* %this1 to %"class.std::__2::__vector_base.113"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.118"* @_ZNKSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base.113"* %0) #9
  %call2 = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE8max_sizeERKS8_(%"class.std::__2::allocator.118"* nonnull align 1 dereferenceable(1) %call) #9
  store i32 %call2, i32* %ref.tmp, align 4
  %call4 = call i32 @_ZNSt3__214numeric_limitsIlE3maxEv() #9
  store i32 %call4, i32* %ref.tmp3, align 4
  %call5 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %1 = load i32, i32* %call5, align 4
  ret i32 %1
}

; Function Attrs: noreturn
declare void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"*) #5

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector.112"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.112"*, align 4
  store %"class.std::__2::vector.112"* %this, %"class.std::__2::vector.112"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.112"*, %"class.std::__2::vector.112"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.112"* %this1 to %"class.std::__2::__vector_base.113"*
  %call = call i32 @_ZNKSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::__vector_base.113"* %0) #9
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::__less", align 1
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__a.addr, align 4
  %1 = load i32*, i32** %__b.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::__less", align 1
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__a.addr, align 4
  %1 = load i32*, i32** %__b.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE8max_sizeERKS8_(%"class.std::__2::allocator.118"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.118"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_max_size", align 1
  store %"class.std::__2::allocator.118"* %__a, %"class.std::__2::allocator.118"** %__a.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_max_size"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.118"*, %"class.std::__2::allocator.118"** %__a.addr, align 4
  %call = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS8_(%"class.std::__2::allocator.118"* nonnull align 1 dereferenceable(1) %1) #9
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.118"* @_ZNKSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base.113"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.113"*, align 4
  store %"class.std::__2::__vector_base.113"* %this, %"class.std::__2::__vector_base.113"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.113"*, %"class.std::__2::__vector_base.113"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.113", %"class.std::__2::__vector_base.113"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.118"* @_ZNKSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE6secondEv(%"class.std::__2::__compressed_pair.115"* %__end_cap_) #9
  ret %"class.std::__2::allocator.118"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__214numeric_limitsIlE3maxEv() #0 comdat {
entry:
  %call = call i32 @_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv() #9
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__comp = alloca %"struct.std::__2::__less", align 1
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__b.addr, align 4
  %1 = load i32*, i32** %__a.addr, align 4
  %call = call zeroext i1 @_ZNKSt3__26__lessImmEclERKmS3_(%"struct.std::__2::__less"* %__comp, i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  br i1 %call, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32*, i32** %__b.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %3 = load i32*, i32** %__a.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi i32* [ %2, %cond.true ], [ %3, %cond.false ]
  ret i32* %cond-lvalue
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__26__lessImmEclERKmS3_(%"struct.std::__2::__less"* %this, i32* nonnull align 4 dereferenceable(4) %__x, i32* nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__less"*, align 4
  %__x.addr = alloca i32*, align 4
  %__y.addr = alloca i32*, align 4
  store %"struct.std::__2::__less"* %this, %"struct.std::__2::__less"** %this.addr, align 4
  store i32* %__x, i32** %__x.addr, align 4
  store i32* %__y, i32** %__y.addr, align 4
  %this1 = load %"struct.std::__2::__less"*, %"struct.std::__2::__less"** %this.addr, align 4
  %0 = load i32*, i32** %__x.addr, align 4
  %1 = load i32, i32* %0, align 4
  %2 = load i32*, i32** %__y.addr, align 4
  %3 = load i32, i32* %2, align 4
  %cmp = icmp ult i32 %1, %3
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS8_(%"class.std::__2::allocator.118"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.118"*, align 4
  store %"class.std::__2::allocator.118"* %__a, %"class.std::__2::allocator.118"** %__a.addr, align 4
  %1 = load %"class.std::__2::allocator.118"*, %"class.std::__2::allocator.118"** %__a.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEE8max_sizeEv(%"class.std::__2::allocator.118"* %1) #9
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEE8max_sizeEv(%"class.std::__2::allocator.118"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.118"*, align 4
  store %"class.std::__2::allocator.118"* %this, %"class.std::__2::allocator.118"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.118"*, %"class.std::__2::allocator.118"** %this.addr, align 4
  ret i32 357913941
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.118"* @_ZNKSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE6secondEv(%"class.std::__2::__compressed_pair.115"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.115"*, align 4
  store %"class.std::__2::__compressed_pair.115"* %this, %"class.std::__2::__compressed_pair.115"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.115"*, %"class.std::__2::__compressed_pair.115"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.115"* %this1 to %"struct.std::__2::__compressed_pair_elem.117"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.118"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.117"* %0) #9
  ret %"class.std::__2::allocator.118"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.118"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.117"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.117"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.117"* %this, %"struct.std::__2::__compressed_pair_elem.117"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.117"*, %"struct.std::__2::__compressed_pair_elem.117"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.117"* %this1 to %"class.std::__2::allocator.118"*
  ret %"class.std::__2::allocator.118"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv() #0 comdat {
entry:
  ret i32 2147483647
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::__vector_base.113"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.113"*, align 4
  store %"class.std::__2::__vector_base.113"* %this, %"class.std::__2::__vector_base.113"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.113"*, %"class.std::__2::__vector_base.113"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNKSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE9__end_capEv(%"class.std::__2::__vector_base.113"* %this1) #9
  %0 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.113", %"class.std::__2::__vector_base.113"* %this1, i32 0, i32 0
  %1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"struct.std::__2::array"* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint %"struct.std::__2::array"* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 12
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNKSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE9__end_capEv(%"class.std::__2::__vector_base.113"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.113"*, align 4
  store %"class.std::__2::__vector_base.113"* %this, %"class.std::__2::__vector_base.113"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.113"*, %"class.std::__2::__vector_base.113"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.113", %"class.std::__2::__vector_base.113"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNKSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.115"* %__end_cap_) #9
  ret %"struct.std::__2::array"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNKSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.115"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.115"*, align 4
  store %"class.std::__2::__compressed_pair.115"* %this, %"class.std::__2::__compressed_pair.115"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.115"*, %"class.std::__2::__compressed_pair.115"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.115"* %this1 to %"struct.std::__2::__compressed_pair_elem.116"*
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNKSt3__222__compressed_pair_elemIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.116"* %0) #9
  ret %"struct.std::__2::array"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNKSt3__222__compressed_pair_elemIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.116"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.116"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.116"* %this, %"struct.std::__2::__compressed_pair_elem.116"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.116"*, %"struct.std::__2::__compressed_pair_elem.116"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.116", %"struct.std::__2::__compressed_pair_elem.116"* %this1, i32 0, i32 0
  ret %"struct.std::__2::array"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__comp = alloca %"struct.std::__2::__less", align 1
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__a.addr, align 4
  %1 = load i32*, i32** %__b.addr, align 4
  %call = call zeroext i1 @_ZNKSt3__26__lessImmEclERKmS3_(%"struct.std::__2::__less"* %__comp, i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  br i1 %call, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32*, i32** %__b.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %3 = load i32*, i32** %__a.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi i32* [ %2, %cond.true ], [ %3, %cond.false ]
  ret i32* %cond-lvalue
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.123"* @_ZNSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEEC2IDnSA_EEOT_OT0_(%"class.std::__2::__compressed_pair.123"* returned %this, i8** nonnull align 4 dereferenceable(4) %__t1, %"class.std::__2::allocator.118"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.123"*, align 4
  %__t1.addr = alloca i8**, align 4
  %__t2.addr = alloca %"class.std::__2::allocator.118"*, align 4
  store %"class.std::__2::__compressed_pair.123"* %this, %"class.std::__2::__compressed_pair.123"** %this.addr, align 4
  store i8** %__t1, i8*** %__t1.addr, align 4
  store %"class.std::__2::allocator.118"* %__t2, %"class.std::__2::allocator.118"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.123"*, %"class.std::__2::__compressed_pair.123"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.123"* %this1 to %"struct.std::__2::__compressed_pair_elem.116"*
  %1 = load i8**, i8*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %1) #9
  %call2 = call %"struct.std::__2::__compressed_pair_elem.116"* @_ZNSt3__222__compressed_pair_elemIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEELi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.116"* %0, i8** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.123"* %this1 to i8*
  %3 = getelementptr inbounds i8, i8* %2, i32 4
  %4 = bitcast i8* %3 to %"struct.std::__2::__compressed_pair_elem.124"*
  %5 = load %"class.std::__2::allocator.118"*, %"class.std::__2::allocator.118"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.118"* @_ZNSt3__27forwardIRNS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEEEOT_RNS_16remove_referenceISA_E4typeE(%"class.std::__2::allocator.118"* nonnull align 1 dereferenceable(1) %5) #9
  %call4 = call %"struct.std::__2::__compressed_pair_elem.124"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEELi1ELb0EEC2IS9_vEEOT_(%"struct.std::__2::__compressed_pair_elem.124"* %4, %"class.std::__2::allocator.118"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::__compressed_pair.123"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::array"* @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE8allocateERS8_m(%"class.std::__2::allocator.118"* nonnull align 1 dereferenceable(1) %__a, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.118"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.118"* %__a, %"class.std::__2::allocator.118"** %__a.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.118"*, %"class.std::__2::allocator.118"** %__a.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call %"struct.std::__2::array"* @_ZNSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEE8allocateEmPKv(%"class.std::__2::allocator.118"* %0, i32 %1, i8* null)
  ret %"struct.std::__2::array"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.118"* @_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE7__allocEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.118"* @_ZNSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE6secondEv(%"class.std::__2::__compressed_pair.123"* %__end_cap_) #9
  ret %"class.std::__2::allocator.118"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.123"* %__end_cap_) #9
  ret %"struct.std::__2::array"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca i8**, align 4
  store i8** %__t, i8*** %__t.addr, align 4
  %0 = load i8**, i8*** %__t.addr, align 4
  ret i8** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.116"* @_ZNSt3__222__compressed_pair_elemIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEELi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.116"* returned %this, i8** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.116"*, align 4
  %__u.addr = alloca i8**, align 4
  store %"struct.std::__2::__compressed_pair_elem.116"* %this, %"struct.std::__2::__compressed_pair_elem.116"** %this.addr, align 4
  store i8** %__u, i8*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.116"*, %"struct.std::__2::__compressed_pair_elem.116"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.116", %"struct.std::__2::__compressed_pair_elem.116"* %this1, i32 0, i32 0
  %0 = load i8**, i8*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %0) #9
  store %"struct.std::__2::array"* null, %"struct.std::__2::array"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.116"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.118"* @_ZNSt3__27forwardIRNS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEEEOT_RNS_16remove_referenceISA_E4typeE(%"class.std::__2::allocator.118"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::allocator.118"*, align 4
  store %"class.std::__2::allocator.118"* %__t, %"class.std::__2::allocator.118"** %__t.addr, align 4
  %0 = load %"class.std::__2::allocator.118"*, %"class.std::__2::allocator.118"** %__t.addr, align 4
  ret %"class.std::__2::allocator.118"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.124"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEELi1ELb0EEC2IS9_vEEOT_(%"struct.std::__2::__compressed_pair_elem.124"* returned %this, %"class.std::__2::allocator.118"* nonnull align 1 dereferenceable(1) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.124"*, align 4
  %__u.addr = alloca %"class.std::__2::allocator.118"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.124"* %this, %"struct.std::__2::__compressed_pair_elem.124"** %this.addr, align 4
  store %"class.std::__2::allocator.118"* %__u, %"class.std::__2::allocator.118"** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.124"*, %"struct.std::__2::__compressed_pair_elem.124"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.124", %"struct.std::__2::__compressed_pair_elem.124"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::allocator.118"*, %"class.std::__2::allocator.118"** %__u.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.118"* @_ZNSt3__27forwardIRNS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEEEOT_RNS_16remove_referenceISA_E4typeE(%"class.std::__2::allocator.118"* nonnull align 1 dereferenceable(1) %0) #9
  store %"class.std::__2::allocator.118"* %call, %"class.std::__2::allocator.118"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.124"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::array"* @_ZNSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEE8allocateEmPKv(%"class.std::__2::allocator.118"* %this, i32 %__n, i8* %0) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.118"*, align 4
  %__n.addr = alloca i32, align 4
  %.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.118"* %this, %"class.std::__2::allocator.118"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::allocator.118"*, %"class.std::__2::allocator.118"** %this.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEE8max_sizeEv(%"class.std::__2::allocator.118"* %this1) #9
  %cmp = icmp ugt i32 %1, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__220__throw_length_errorEPKc(i8* getelementptr inbounds ([68 x i8], [68 x i8]* @.str, i32 0, i32 0)) #10
  unreachable

if.end:                                           ; preds = %entry
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 12
  %call2 = call i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %mul, i32 4)
  %3 = bitcast i8* %call2 to %"struct.std::__2::array"*
  ret %"struct.std::__2::array"* %3
}

; Function Attrs: noinline noreturn nounwind optnone
define linkonce_odr hidden void @_ZNSt3__220__throw_length_errorEPKc(i8* %__msg) #6 comdat {
entry:
  %__msg.addr = alloca i8*, align 4
  store i8* %__msg, i8** %__msg.addr, align 4
  call void @abort() #10
  unreachable
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %__size, i32 %__align) #0 comdat {
entry:
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i32, i32* %__size.addr, align 4
  %call = call noalias nonnull i8* @_Znwm(i32 %0) #8
  ret i8* %call
}

; Function Attrs: noreturn
declare void @abort() #5

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.118"* @_ZNSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE6secondEv(%"class.std::__2::__compressed_pair.123"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.123"*, align 4
  store %"class.std::__2::__compressed_pair.123"* %this, %"class.std::__2::__compressed_pair.123"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.123"*, %"class.std::__2::__compressed_pair.123"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.123"* %this1 to i8*
  %add.ptr = getelementptr inbounds i8, i8* %0, i32 4
  %1 = bitcast i8* %add.ptr to %"struct.std::__2::__compressed_pair_elem.124"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.118"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.124"* %1) #9
  ret %"class.std::__2::allocator.118"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.118"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.124"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.124"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.124"* %this, %"struct.std::__2::__compressed_pair_elem.124"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.124"*, %"struct.std::__2::__compressed_pair_elem.124"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.124", %"struct.std::__2::__compressed_pair_elem.124"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::allocator.118"*, %"class.std::__2::allocator.118"** %__value_, align 4
  ret %"class.std::__2::allocator.118"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.123"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.123"*, align 4
  store %"class.std::__2::__compressed_pair.123"* %this, %"class.std::__2::__compressed_pair.123"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.123"*, %"class.std::__2::__compressed_pair.123"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.123"* %this1 to %"struct.std::__2::__compressed_pair_elem.116"*
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNSt3__222__compressed_pair_elemIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.116"* %0) #9
  ret %"struct.std::__2::array"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE21_ConstructTransactionC2EPPS6_m(%"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction"* returned %this, %"struct.std::__2::array"** %__p, i32 %__n) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction"*, align 4
  %__p.addr = alloca %"struct.std::__2::array"**, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction"* %this, %"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction"** %this.addr, align 4
  store %"struct.std::__2::array"** %__p, %"struct.std::__2::array"*** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction"*, %"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load %"struct.std::__2::array"**, %"struct.std::__2::array"*** %__p.addr, align 4
  %1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %0, align 4
  store %"struct.std::__2::array"* %1, %"struct.std::__2::array"** %__pos_, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction"* %this1, i32 0, i32 1
  %2 = load %"struct.std::__2::array"**, %"struct.std::__2::array"*** %__p.addr, align 4
  %3 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %2, align 4
  %4 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %3, i32 %4
  store %"struct.std::__2::array"* %add.ptr, %"struct.std::__2::array"** %__end_, align 4
  %__dest_ = getelementptr inbounds %"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction"* %this1, i32 0, i32 2
  %5 = load %"struct.std::__2::array"**, %"struct.std::__2::array"*** %__p.addr, align 4
  store %"struct.std::__2::array"** %5, %"struct.std::__2::array"*** %__dest_, align 4
  ret %"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction"*, align 4
  store %"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction"* %this, %"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction"*, %"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__pos_, align 4
  %__dest_ = getelementptr inbounds %"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction"* %this1, i32 0, i32 2
  %1 = load %"struct.std::__2::array"**, %"struct.std::__2::array"*** %__dest_, align 4
  store %"struct.std::__2::array"* %0, %"struct.std::__2::array"** %1, align 4
  ret %"struct.std::__2::__split_buffer<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>, std::__2::allocator<std::__2::array<draco::IndexType<unsigned int, draco::PointIndex_tag_type_>, 3>> &>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE17__annotate_deleteEv(%"class.std::__2::vector.112"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.112"*, align 4
  store %"class.std::__2::vector.112"* %this, %"class.std::__2::vector.112"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.112"*, %"class.std::__2::vector.112"** %this.addr, align 4
  %call = call %"struct.std::__2::array"* @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.112"* %this1) #9
  %0 = bitcast %"struct.std::__2::array"* %call to i8*
  %call2 = call %"struct.std::__2::array"* @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.112"* %this1) #9
  %call3 = call i32 @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector.112"* %this1) #9
  %add.ptr = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %call2, i32 %call3
  %1 = bitcast %"struct.std::__2::array"* %add.ptr to i8*
  %call4 = call %"struct.std::__2::array"* @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.112"* %this1) #9
  %call5 = call i32 @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.112"* %this1) #9
  %add.ptr6 = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %call4, i32 %call5
  %2 = bitcast %"struct.std::__2::array"* %add.ptr6 to i8*
  %call7 = call %"struct.std::__2::array"* @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.112"* %this1) #9
  %call8 = call i32 @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector.112"* %this1) #9
  %add.ptr9 = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %call7, i32 %call8
  %3 = bitcast %"struct.std::__2::array"* %add.ptr9 to i8*
  call void @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE31__annotate_contiguous_containerEPKvSB_SB_SB_(%"class.std::__2::vector.112"* %this1, i8* %0, i8* %1, i8* %2, i8* %3) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE46__construct_backward_with_exception_guaranteesIS7_EENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS8_PT_SE_EE5valuesr31is_trivially_move_constructibleISE_EE5valueEvE4typeERS8_SF_SF_RSF_(%"class.std::__2::allocator.118"* nonnull align 1 dereferenceable(1) %0, %"struct.std::__2::array"* %__begin1, %"struct.std::__2::array"* %__end1, %"struct.std::__2::array"** nonnull align 4 dereferenceable(4) %__end2) #0 comdat {
entry:
  %.addr = alloca %"class.std::__2::allocator.118"*, align 4
  %__begin1.addr = alloca %"struct.std::__2::array"*, align 4
  %__end1.addr = alloca %"struct.std::__2::array"*, align 4
  %__end2.addr = alloca %"struct.std::__2::array"**, align 4
  %_Np = alloca i32, align 4
  store %"class.std::__2::allocator.118"* %0, %"class.std::__2::allocator.118"** %.addr, align 4
  store %"struct.std::__2::array"* %__begin1, %"struct.std::__2::array"** %__begin1.addr, align 4
  store %"struct.std::__2::array"* %__end1, %"struct.std::__2::array"** %__end1.addr, align 4
  store %"struct.std::__2::array"** %__end2, %"struct.std::__2::array"*** %__end2.addr, align 4
  %1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__end1.addr, align 4
  %2 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__begin1.addr, align 4
  %sub.ptr.lhs.cast = ptrtoint %"struct.std::__2::array"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"struct.std::__2::array"* %2 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 12
  store i32 %sub.ptr.div, i32* %_Np, align 4
  %3 = load i32, i32* %_Np, align 4
  %4 = load %"struct.std::__2::array"**, %"struct.std::__2::array"*** %__end2.addr, align 4
  %5 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %4, align 4
  %idx.neg = sub i32 0, %3
  %add.ptr = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %5, i32 %idx.neg
  store %"struct.std::__2::array"* %add.ptr, %"struct.std::__2::array"** %4, align 4
  %6 = load i32, i32* %_Np, align 4
  %cmp = icmp sgt i32 %6, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %7 = load %"struct.std::__2::array"**, %"struct.std::__2::array"*** %__end2.addr, align 4
  %8 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %7, align 4
  %9 = bitcast %"struct.std::__2::array"* %8 to i8*
  %10 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__begin1.addr, align 4
  %11 = bitcast %"struct.std::__2::array"* %10 to i8*
  %12 = load i32, i32* %_Np, align 4
  %mul = mul i32 %12, 12
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %11, i32 %mul, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__24swapIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS9_EE5valueEvE4typeERS9_SC_(%"struct.std::__2::array"** nonnull align 4 dereferenceable(4) %__x, %"struct.std::__2::array"** nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %__x.addr = alloca %"struct.std::__2::array"**, align 4
  %__y.addr = alloca %"struct.std::__2::array"**, align 4
  %__t = alloca %"struct.std::__2::array"*, align 4
  store %"struct.std::__2::array"** %__x, %"struct.std::__2::array"*** %__x.addr, align 4
  store %"struct.std::__2::array"** %__y, %"struct.std::__2::array"*** %__y.addr, align 4
  %0 = load %"struct.std::__2::array"**, %"struct.std::__2::array"*** %__x.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNSt3__24moveIRPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEEEONS_16remove_referenceIT_E4typeEOSA_(%"struct.std::__2::array"** nonnull align 4 dereferenceable(4) %0) #9
  %1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %call, align 4
  store %"struct.std::__2::array"* %1, %"struct.std::__2::array"** %__t, align 4
  %2 = load %"struct.std::__2::array"**, %"struct.std::__2::array"*** %__y.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNSt3__24moveIRPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEEEONS_16remove_referenceIT_E4typeEOSA_(%"struct.std::__2::array"** nonnull align 4 dereferenceable(4) %2) #9
  %3 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %call1, align 4
  %4 = load %"struct.std::__2::array"**, %"struct.std::__2::array"*** %__x.addr, align 4
  store %"struct.std::__2::array"* %3, %"struct.std::__2::array"** %4, align 4
  %call2 = call nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNSt3__24moveIRPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEEEONS_16remove_referenceIT_E4typeEOSA_(%"struct.std::__2::array"** nonnull align 4 dereferenceable(4) %__t) #9
  %5 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %call2, align 4
  %6 = load %"struct.std::__2::array"**, %"struct.std::__2::array"*** %__y.addr, align 4
  store %"struct.std::__2::array"* %5, %"struct.std::__2::array"** %6, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE14__annotate_newEm(%"class.std::__2::vector.112"* %this, i32 %__current_size) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.112"*, align 4
  %__current_size.addr = alloca i32, align 4
  store %"class.std::__2::vector.112"* %this, %"class.std::__2::vector.112"** %this.addr, align 4
  store i32 %__current_size, i32* %__current_size.addr, align 4
  %this1 = load %"class.std::__2::vector.112"*, %"class.std::__2::vector.112"** %this.addr, align 4
  %call = call %"struct.std::__2::array"* @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.112"* %this1) #9
  %0 = bitcast %"struct.std::__2::array"* %call to i8*
  %call2 = call %"struct.std::__2::array"* @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.112"* %this1) #9
  %call3 = call i32 @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector.112"* %this1) #9
  %add.ptr = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %call2, i32 %call3
  %1 = bitcast %"struct.std::__2::array"* %add.ptr to i8*
  %call4 = call %"struct.std::__2::array"* @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.112"* %this1) #9
  %call5 = call i32 @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector.112"* %this1) #9
  %add.ptr6 = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %call4, i32 %call5
  %2 = bitcast %"struct.std::__2::array"* %add.ptr6 to i8*
  %call7 = call %"struct.std::__2::array"* @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.112"* %this1) #9
  %3 = load i32, i32* %__current_size.addr, align 4
  %add.ptr8 = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %call7, i32 %3
  %4 = bitcast %"struct.std::__2::array"* %add.ptr8 to i8*
  call void @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE31__annotate_contiguous_containerEPKvSB_SB_SB_(%"class.std::__2::vector.112"* %this1, i8* %0, i8* %1, i8* %2, i8* %4) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector.112"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.112"*, align 4
  store %"class.std::__2::vector.112"* %this, %"class.std::__2::vector.112"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.112"*, %"class.std::__2::vector.112"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE31__annotate_contiguous_containerEPKvSB_SB_SB_(%"class.std::__2::vector.112"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.112"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector.112"* %this, %"class.std::__2::vector.112"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector.112"*, %"class.std::__2::vector.112"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::array"* @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.112"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.112"*, align 4
  store %"class.std::__2::vector.112"* %this, %"class.std::__2::vector.112"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.112"*, %"class.std::__2::vector.112"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.112"* %this1 to %"class.std::__2::__vector_base.113"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.113", %"class.std::__2::__vector_base.113"* %0, i32 0, i32 0
  %1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__begin_, align 4
  %call = call %"struct.std::__2::array"* @_ZNSt3__212__to_addressINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEEEPT_S8_(%"struct.std::__2::array"* %1) #9
  ret %"struct.std::__2::array"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNSt3__24moveIRPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEEEONS_16remove_referenceIT_E4typeEOSA_(%"struct.std::__2::array"** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"struct.std::__2::array"**, align 4
  store %"struct.std::__2::array"** %__t, %"struct.std::__2::array"*** %__t.addr, align 4
  %0 = load %"struct.std::__2::array"**, %"struct.std::__2::array"*** %__t.addr, align 4
  ret %"struct.std::__2::array"** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE5clearEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 1
  %0 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__begin_, align 4
  call void @_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE17__destruct_at_endEPS6_(%"struct.std::__2::__split_buffer"* %this1, %"struct.std::__2::array"* %0) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE10deallocateERS8_PS7_m(%"class.std::__2::allocator.118"* nonnull align 1 dereferenceable(1) %__a, %"struct.std::__2::array"* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.118"*, align 4
  %__p.addr = alloca %"struct.std::__2::array"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.118"* %__a, %"class.std::__2::allocator.118"** %__a.addr, align 4
  store %"struct.std::__2::array"* %__p, %"struct.std::__2::array"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.118"*, %"class.std::__2::allocator.118"** %__a.addr, align 4
  %1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEE10deallocateEPS6_m(%"class.std::__2::allocator.118"* %0, %"struct.std::__2::array"* %1, i32 %2) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE8capacityEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNKSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this1) #9
  %0 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %call, align 4
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__first_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"struct.std::__2::array"* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint %"struct.std::__2::array"* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 12
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE17__destruct_at_endEPS6_(%"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::array"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__new_last.addr = alloca %"struct.std::__2::array"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant.125", align 1
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store %"struct.std::__2::array"* %__new_last, %"struct.std::__2::array"** %__new_last.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %0 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__new_last.addr, align 4
  call void @_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE17__destruct_at_endEPS6_NS_17integral_constantIbLb0EEE(%"struct.std::__2::__split_buffer"* %this1, %"struct.std::__2::array"* %0) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE17__destruct_at_endEPS6_NS_17integral_constantIbLb0EEE(%"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::array"* %__new_last) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant.125", align 1
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__new_last.addr = alloca %"struct.std::__2::array"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store %"struct.std::__2::array"* %__new_last, %"struct.std::__2::array"** %__new_last.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__new_last.addr, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 2
  %2 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__end_, align 4
  %cmp = icmp ne %"struct.std::__2::array"* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.118"* @_ZNSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE7__allocEv(%"struct.std::__2::__split_buffer"* %this1) #9
  %__end_2 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 2
  %3 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__end_2, align 4
  %incdec.ptr = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %3, i32 -1
  store %"struct.std::__2::array"* %incdec.ptr, %"struct.std::__2::array"** %__end_2, align 4
  %call3 = call %"struct.std::__2::array"* @_ZNSt3__212__to_addressINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEEEPT_S8_(%"struct.std::__2::array"* %incdec.ptr) #9
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE7destroyIS7_EEvRS8_PT_(%"class.std::__2::allocator.118"* nonnull align 1 dereferenceable(1) %call, %"struct.std::__2::array"* %call3)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE7destroyIS7_EEvRS8_PT_(%"class.std::__2::allocator.118"* nonnull align 1 dereferenceable(1) %__a, %"struct.std::__2::array"* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.118"*, align 4
  %__p.addr = alloca %"struct.std::__2::array"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy", align 1
  store %"class.std::__2::allocator.118"* %__a, %"class.std::__2::allocator.118"** %__a.addr, align 4
  store %"struct.std::__2::array"* %__p, %"struct.std::__2::array"** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.118"*, %"class.std::__2::allocator.118"** %__a.addr, align 4
  %2 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE9__destroyIS7_EEvNS_17integral_constantIbLb1EEERS8_PT_(%"class.std::__2::allocator.118"* nonnull align 1 dereferenceable(1) %1, %"struct.std::__2::array"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE9__destroyIS7_EEvNS_17integral_constantIbLb1EEERS8_PT_(%"class.std::__2::allocator.118"* nonnull align 1 dereferenceable(1) %__a, %"struct.std::__2::array"* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.118"*, align 4
  %__p.addr = alloca %"struct.std::__2::array"*, align 4
  store %"class.std::__2::allocator.118"* %__a, %"class.std::__2::allocator.118"** %__a.addr, align 4
  store %"struct.std::__2::array"* %__p, %"struct.std::__2::array"** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.118"*, %"class.std::__2::allocator.118"** %__a.addr, align 4
  %2 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__p.addr, align 4
  call void @_ZNSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEE7destroyEPS6_(%"class.std::__2::allocator.118"* %1, %"struct.std::__2::array"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEE7destroyEPS6_(%"class.std::__2::allocator.118"* %this, %"struct.std::__2::array"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.118"*, align 4
  %__p.addr = alloca %"struct.std::__2::array"*, align 4
  store %"class.std::__2::allocator.118"* %this, %"class.std::__2::allocator.118"** %this.addr, align 4
  store %"struct.std::__2::array"* %__p, %"struct.std::__2::array"** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.118"*, %"class.std::__2::allocator.118"** %this.addr, align 4
  %0 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__p.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEE10deallocateEPS6_m(%"class.std::__2::allocator.118"* %this, %"struct.std::__2::array"* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.118"*, align 4
  %__p.addr = alloca %"struct.std::__2::array"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.118"* %this, %"class.std::__2::allocator.118"** %this.addr, align 4
  store %"struct.std::__2::array"* %__p, %"struct.std::__2::array"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.118"*, %"class.std::__2::allocator.118"** %this.addr, align 4
  %0 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__p.addr, align 4
  %1 = bitcast %"struct.std::__2::array"* %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 12
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %__ptr, i32 %__size, i32 %__align) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  %1 = load i32, i32* %__size.addr, align 4
  %2 = load i32, i32* %__align.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm(i8* %0, i32 %1, i32 %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm(i8* %__ptr, i32 %__size, i32 %__align) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  %1 = load i32, i32* %__size.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm(i8* %0, i32 %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm(i8* %__ptr, i32 %__size) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller9__do_callEPv(i8* %0)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller9__do_callEPv(i8* %__ptr) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  call void @_ZdlPv(i8* %0) #11
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #7

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNKSt3__214__split_bufferINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNKSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.123"* %__end_cap_) #9
  ret %"struct.std::__2::array"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNKSt3__217__compressed_pairIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEERNS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.123"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.123"*, align 4
  store %"class.std::__2::__compressed_pair.123"* %this, %"class.std::__2::__compressed_pair.123"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.123"*, %"class.std::__2::__compressed_pair.123"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.123"* %this1 to %"struct.std::__2::__compressed_pair_elem.116"*
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::array"** @_ZNKSt3__222__compressed_pair_elemIPNS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.116"* %0) #9
  ret %"struct.std::__2::array"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE27__invalidate_iterators_pastEPS6_(%"class.std::__2::vector.112"* %this, %"struct.std::__2::array"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.112"*, align 4
  %__new_last.addr = alloca %"struct.std::__2::array"*, align 4
  store %"class.std::__2::vector.112"* %this, %"class.std::__2::vector.112"** %this.addr, align 4
  store %"struct.std::__2::array"* %__new_last, %"struct.std::__2::array"** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::vector.112"*, %"class.std::__2::vector.112"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE17__destruct_at_endEPS6_(%"class.std::__2::__vector_base.113"* %this, %"struct.std::__2::array"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.113"*, align 4
  %__new_last.addr = alloca %"struct.std::__2::array"*, align 4
  %__soon_to_be_end = alloca %"struct.std::__2::array"*, align 4
  store %"class.std::__2::__vector_base.113"* %this, %"class.std::__2::__vector_base.113"** %this.addr, align 4
  store %"struct.std::__2::array"* %__new_last, %"struct.std::__2::array"** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.113"*, %"class.std::__2::__vector_base.113"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.113", %"class.std::__2::__vector_base.113"* %this1, i32 0, i32 1
  %0 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__end_, align 4
  store %"struct.std::__2::array"* %0, %"struct.std::__2::array"** %__soon_to_be_end, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__new_last.addr, align 4
  %2 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__soon_to_be_end, align 4
  %cmp = icmp ne %"struct.std::__2::array"* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.118"* @_ZNSt3__213__vector_baseINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base.113"* %this1) #9
  %3 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__soon_to_be_end, align 4
  %incdec.ptr = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %3, i32 -1
  store %"struct.std::__2::array"* %incdec.ptr, %"struct.std::__2::array"** %__soon_to_be_end, align 4
  %call2 = call %"struct.std::__2::array"* @_ZNSt3__212__to_addressINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEEEEPT_S8_(%"struct.std::__2::array"* %incdec.ptr) #9
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_5arrayIN5draco9IndexTypeIjNS3_20PointIndex_tag_type_EEELm3EEEEEE7destroyIS7_EEvRS8_PT_(%"class.std::__2::allocator.118"* nonnull align 1 dereferenceable(1) %call, %"struct.std::__2::array"* %call2)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %4 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__new_last.addr, align 4
  %__end_3 = getelementptr inbounds %"class.std::__2::__vector_base.113", %"class.std::__2::__vector_base.113"* %this1, i32 0, i32 1
  store %"struct.std::__2::array"* %4, %"struct.std::__2::array"** %__end_3, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE17__annotate_shrinkEm(%"class.std::__2::vector.112"* %this, i32 %__old_size) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.112"*, align 4
  %__old_size.addr = alloca i32, align 4
  store %"class.std::__2::vector.112"* %this, %"class.std::__2::vector.112"** %this.addr, align 4
  store i32 %__old_size, i32* %__old_size.addr, align 4
  %this1 = load %"class.std::__2::vector.112"*, %"class.std::__2::vector.112"** %this.addr, align 4
  %call = call %"struct.std::__2::array"* @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.112"* %this1) #9
  %0 = bitcast %"struct.std::__2::array"* %call to i8*
  %call2 = call %"struct.std::__2::array"* @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.112"* %this1) #9
  %call3 = call i32 @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector.112"* %this1) #9
  %add.ptr = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %call2, i32 %call3
  %1 = bitcast %"struct.std::__2::array"* %add.ptr to i8*
  %call4 = call %"struct.std::__2::array"* @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.112"* %this1) #9
  %2 = load i32, i32* %__old_size.addr, align 4
  %add.ptr5 = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %call4, i32 %2
  %3 = bitcast %"struct.std::__2::array"* %add.ptr5 to i8*
  %call6 = call %"struct.std::__2::array"* @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.112"* %this1) #9
  %call7 = call i32 @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.112"* %this1) #9
  %add.ptr8 = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %call6, i32 %call7
  %4 = bitcast %"struct.std::__2::array"* %add.ptr8 to i8*
  call void @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE31__annotate_contiguous_containerEPKvSB_SB_SB_(%"class.std::__2::vector.112"* %this1, i8* %0, i8* %1, i8* %3, i8* %4) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType.114"* @_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEC2Ev(%"class.draco::IndexType.114"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.114"*, align 4
  store %"class.draco::IndexType.114"* %this, %"class.draco::IndexType.114"** %this.addr, align 4
  %this1 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %this1, i32 0, i32 0
  store i32 0, i32* %value_, align 4
  ret %"class.draco::IndexType.114"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.63"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEixEm(%"class.std::__2::vector.61"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.61"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector.61"* %this, %"class.std::__2::vector.61"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.61"*, %"class.std::__2::vector.61"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.61"* %this1 to %"class.std::__2::__vector_base.62"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.62", %"class.std::__2::__vector_base.62"* %0, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.63"*, %"class.std::__2::unique_ptr.63"** %__begin_, align 4
  %2 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds %"class.std::__2::unique_ptr.63", %"class.std::__2::unique_ptr.63"* %1, i32 %2
  ret %"class.std::__2::unique_ptr.63"* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::PointAttribute"* @_ZNKSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEE3getEv(%"class.std::__2::unique_ptr.63"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.63"*, align 4
  store %"class.std::__2::unique_ptr.63"* %this, %"class.std::__2::unique_ptr.63"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.63"*, %"class.std::__2::unique_ptr.63"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.63", %"class.std::__2::unique_ptr.63"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNKSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.64"* %__ptr_) #9
  %0 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %call, align 4
  ret %"class.draco::PointAttribute"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNKSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.64"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.64"*, align 4
  store %"class.std::__2::__compressed_pair.64"* %this, %"class.std::__2::__compressed_pair.64"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.64"*, %"class.std::__2::__compressed_pair.64"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.64"* %this1 to %"struct.std::__2::__compressed_pair_elem.65"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNKSt3__222__compressed_pair_elemIPN5draco14PointAttributeELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.65"* %0) #9
  ret %"class.draco::PointAttribute"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNKSt3__222__compressed_pair_elemIPN5draco14PointAttributeELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.65"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.65"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.65"* %this, %"struct.std::__2::__compressed_pair_elem.65"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.65"*, %"struct.std::__2::__compressed_pair_elem.65"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.65", %"struct.std::__2::__compressed_pair_elem.65"* %this1, i32 0, i32 0
  ret %"class.draco::PointAttribute"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EE5valueEv(%"class.draco::IndexType"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.draco::IndexType"* %this, %"class.draco::IndexType"** %this.addr, align 4
  %this1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value_, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i64 @_ZNK5draco17GeometryAttribute11byte_strideEv(%"class.draco::GeometryAttribute"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::GeometryAttribute"*, align 4
  store %"class.draco::GeometryAttribute"* %this, %"class.draco::GeometryAttribute"** %this.addr, align 4
  %this1 = load %"class.draco::GeometryAttribute"*, %"class.draco::GeometryAttribute"** %this.addr, align 4
  %byte_stride_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 5
  %0 = load i64, i64* %byte_stride_, align 8
  ret i64 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::DataBuffer"* @_ZNK5draco14PointAttribute6bufferEv(%"class.draco::PointAttribute"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointAttribute"*, align 4
  store %"class.draco::PointAttribute"* %this, %"class.draco::PointAttribute"** %this.addr, align 4
  %this1 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %this.addr, align 4
  %attribute_buffer_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 1
  %call = call %"class.draco::DataBuffer"* @_ZNKSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEE3getEv(%"class.std::__2::unique_ptr.73"* %attribute_buffer_) #9
  ret %"class.draco::DataBuffer"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco10DataBuffer5WriteExPKvm(%"class.draco::DataBuffer"* %this, i64 %byte_pos, i8* %in_data, i32 %data_size) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::DataBuffer"*, align 4
  %byte_pos.addr = alloca i64, align 8
  %in_data.addr = alloca i8*, align 4
  %data_size.addr = alloca i32, align 4
  store %"class.draco::DataBuffer"* %this, %"class.draco::DataBuffer"** %this.addr, align 4
  store i64 %byte_pos, i64* %byte_pos.addr, align 8
  store i8* %in_data, i8** %in_data.addr, align 4
  store i32 %data_size, i32* %data_size.addr, align 4
  %this1 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %this.addr, align 4
  %call = call i8* @_ZN5draco10DataBuffer4dataEv(%"class.draco::DataBuffer"* %this1)
  %0 = load i64, i64* %byte_pos.addr, align 8
  %idx.ext = trunc i64 %0 to i32
  %add.ptr = getelementptr inbounds i8, i8* %call, i32 %idx.ext
  %1 = load i8*, i8** %in_data.addr, align 4
  %2 = load i32, i32* %data_size.addr, align 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %add.ptr, i8* align 1 %1, i32 %2, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::DataBuffer"* @_ZNKSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEE3getEv(%"class.std::__2::unique_ptr.73"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.73"*, align 4
  store %"class.std::__2::unique_ptr.73"* %this, %"class.std::__2::unique_ptr.73"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.73"*, %"class.std::__2::unique_ptr.73"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.73", %"class.std::__2::unique_ptr.73"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::DataBuffer"** @_ZNKSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.74"* %__ptr_) #9
  %0 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %call, align 4
  ret %"class.draco::DataBuffer"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::DataBuffer"** @_ZNKSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.74"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.74"*, align 4
  store %"class.std::__2::__compressed_pair.74"* %this, %"class.std::__2::__compressed_pair.74"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.74"*, %"class.std::__2::__compressed_pair.74"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.74"* %this1 to %"struct.std::__2::__compressed_pair_elem.75"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::DataBuffer"** @_ZNKSt3__222__compressed_pair_elemIPN5draco10DataBufferELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.75"* %0) #9
  ret %"class.draco::DataBuffer"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::DataBuffer"** @_ZNKSt3__222__compressed_pair_elemIPN5draco10DataBufferELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.75"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.75"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.75"* %this, %"struct.std::__2::__compressed_pair_elem.75"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.75"*, %"struct.std::__2::__compressed_pair_elem.75"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.75", %"struct.std::__2::__compressed_pair_elem.75"* %this1, i32 0, i32 0
  ret %"class.draco::DataBuffer"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN5draco10DataBuffer4dataEv(%"class.draco::DataBuffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::DataBuffer"*, align 4
  store %"class.draco::DataBuffer"* %this, %"class.draco::DataBuffer"** %this.addr, align 4
  %this1 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %this.addr, align 4
  %data_ = getelementptr inbounds %"class.draco::DataBuffer", %"class.draco::DataBuffer"* %this1, i32 0, i32 0
  %call = call nonnull align 1 dereferenceable(1) i8* @_ZNSt3__26vectorIhNS_9allocatorIhEEEixEm(%"class.std::__2::vector.66"* %data_, i32 0) #9
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) i8* @_ZNSt3__26vectorIhNS_9allocatorIhEEEixEm(%"class.std::__2::vector.66"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.66"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector.66"* %this, %"class.std::__2::vector.66"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.66"*, %"class.std::__2::vector.66"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.66"* %this1 to %"class.std::__2::__vector_base.67"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.67", %"class.std::__2::__vector_base.67"* %0, i32 0, i32 0
  %1 = load i8*, i8** %__begin_, align 4
  %2 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds i8, i8* %1, i32 %2
  ret i8* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK5draco9IndexTypeIjNS_19FaceIndex_tag_type_EEgeERKj(%"class.draco::IndexType.122"* %this, i32* nonnull align 4 dereferenceable(4) %val) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.122"*, align 4
  %val.addr = alloca i32*, align 4
  store %"class.draco::IndexType.122"* %this, %"class.draco::IndexType.122"** %this.addr, align 4
  store i32* %val, i32** %val.addr, align 4
  %this1 = load %"class.draco::IndexType.122"*, %"class.draco::IndexType.122"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.122", %"class.draco::IndexType.122"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value_, align 4
  %1 = load i32*, i32** %val.addr, align 4
  %2 = load i32, i32* %1, align 4
  %cmp = icmp uge i32 %0, %2
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEENSt3__25arrayINS1_IjNS_20PointIndex_tag_type_EEELm3EEEE4sizeEv(%"class.draco::IndexTypeVector.111"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector.111"*, align 4
  store %"class.draco::IndexTypeVector.111"* %this, %"class.draco::IndexTypeVector.111"** %this.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector.111"*, %"class.draco::IndexTypeVector.111"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector.111", %"class.draco::IndexTypeVector.111"* %this1, i32 0, i32 0
  %call = call i32 @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.112"* %vector_) #9
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"struct.std::__2::array"* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEENSt3__25arrayINS1_IjNS_20PointIndex_tag_type_EEELm3EEEEixERKS3_(%"class.draco::IndexTypeVector.111"* %this, %"class.draco::IndexType.122"* nonnull align 4 dereferenceable(4) %index) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector.111"*, align 4
  %index.addr = alloca %"class.draco::IndexType.122"*, align 4
  store %"class.draco::IndexTypeVector.111"* %this, %"class.draco::IndexTypeVector.111"** %this.addr, align 4
  store %"class.draco::IndexType.122"* %index, %"class.draco::IndexType.122"** %index.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector.111"*, %"class.draco::IndexTypeVector.111"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector.111", %"class.draco::IndexTypeVector.111"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexType.122"*, %"class.draco::IndexType.122"** %index.addr, align 4
  %call = call i32 @_ZNK5draco9IndexTypeIjNS_19FaceIndex_tag_type_EE5valueEv(%"class.draco::IndexType.122"* %0)
  %call2 = call nonnull align 4 dereferenceable(12) %"struct.std::__2::array"* @_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEEixEm(%"class.std::__2::vector.112"* %vector_, i32 %call) #9
  ret %"struct.std::__2::array"* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"struct.std::__2::array"* @_ZNSt3__25arrayIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELm3EEaSERKS5_(%"struct.std::__2::array"* %this, %"struct.std::__2::array"* nonnull align 4 dereferenceable(12) %0) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::array"*, align 4
  %.addr = alloca %"struct.std::__2::array"*, align 4
  %__i0 = alloca i32, align 4
  store %"struct.std::__2::array"* %this, %"struct.std::__2::array"** %this.addr, align 4
  store %"struct.std::__2::array"* %0, %"struct.std::__2::array"** %.addr, align 4
  %this1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %this.addr, align 4
  store i32 0, i32* %__i0, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %__i0, align 4
  %cmp = icmp ne i32 %1, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %__elems_ = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %this1, i32 0, i32 0
  %2 = load i32, i32* %__i0, align 4
  %arrayidx = getelementptr inbounds [3 x %"class.draco::IndexType.114"], [3 x %"class.draco::IndexType.114"]* %__elems_, i32 0, i32 %2
  %3 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %.addr, align 4
  %__elems_2 = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %3, i32 0, i32 0
  %4 = load i32, i32* %__i0, align 4
  %arrayidx3 = getelementptr inbounds [3 x %"class.draco::IndexType.114"], [3 x %"class.draco::IndexType.114"]* %__elems_2, i32 0, i32 %4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.114"* @_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEaSERKS2_(%"class.draco::IndexType.114"* %arrayidx, %"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %arrayidx3)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %__i0, align 4
  %inc = add i32 %5, 1
  store i32 %inc, i32* %__i0, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret %"struct.std::__2::array"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"struct.std::__2::array"* @_ZNSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEEixEm(%"class.std::__2::vector.112"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.112"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector.112"* %this, %"class.std::__2::vector.112"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.112"*, %"class.std::__2::vector.112"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.112"* %this1 to %"class.std::__2::__vector_base.113"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.113", %"class.std::__2::__vector_base.113"* %0, i32 0, i32 0
  %1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__begin_, align 4
  %2 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %1, i32 %2
  ret %"struct.std::__2::array"* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.114"* @_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEaSERKS2_(%"class.draco::IndexType.114"* %this, %"class.draco::IndexType.114"* nonnull align 4 dereferenceable(4) %i) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.114"*, align 4
  %i.addr = alloca %"class.draco::IndexType.114"*, align 4
  store %"class.draco::IndexType.114"* %this, %"class.draco::IndexType.114"** %this.addr, align 4
  store %"class.draco::IndexType.114"* %i, %"class.draco::IndexType.114"** %i.addr, align 4
  %this1 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %this.addr, align 4
  %0 = load %"class.draco::IndexType.114"*, %"class.draco::IndexType.114"** %i.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %0, i32 0, i32 0
  %1 = load i32, i32* %value_, align 4
  %value_2 = getelementptr inbounds %"class.draco::IndexType.114", %"class.draco::IndexType.114"* %this1, i32 0, i32 0
  store i32 %1, i32* %value_2, align 4
  ret %"class.draco::IndexType.114"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.draco::Mesh::AttributeData"* @_ZNSt3__26vectorIN5draco4Mesh13AttributeDataENS_9allocatorIS3_EEEixEm(%"class.std::__2::vector.104"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.104"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector.104"* %this, %"class.std::__2::vector.104"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.104"*, %"class.std::__2::vector.104"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.104"* %this1 to %"class.std::__2::__vector_base.105"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.105", %"class.std::__2::__vector_base.105"* %0, i32 0, i32 0
  %1 = load %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"** %__begin_, align 4
  %2 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds %"struct.draco::Mesh::AttributeData", %"struct.draco::Mesh::AttributeData"* %1, i32 %2
  ret %"struct.draco::Mesh::AttributeData"* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.1"* @_ZNSt3__217__compressed_pairIPN5draco4MeshENS_14default_deleteIS2_EEEC2IRS3_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.1"* returned %this, %"class.draco::Mesh"** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.1"*, align 4
  %__t1.addr = alloca %"class.draco::Mesh"**, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.1"* %this, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  store %"class.draco::Mesh"** %__t1, %"class.draco::Mesh"*** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.1"*, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.1"* %this1 to %"struct.std::__2::__compressed_pair_elem.2"*
  %1 = load %"class.draco::Mesh"**, %"class.draco::Mesh"*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::Mesh"** @_ZNSt3__27forwardIRPN5draco4MeshEEEOT_RNS_16remove_referenceIS5_E4typeE(%"class.draco::Mesh"** nonnull align 4 dereferenceable(4) %1) #9
  %call2 = call %"struct.std::__2::__compressed_pair_elem.2"* @_ZNSt3__222__compressed_pair_elemIPN5draco4MeshELi0ELb0EEC2IRS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.2"* %0, %"class.draco::Mesh"** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.1"* %this1 to %"struct.std::__2::__compressed_pair_elem.120"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #9
  %call4 = call %"struct.std::__2::__compressed_pair_elem.120"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco4MeshEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.120"* %2)
  ret %"class.std::__2::__compressed_pair.1"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::Mesh"** @_ZNSt3__27forwardIRPN5draco4MeshEEEOT_RNS_16remove_referenceIS5_E4typeE(%"class.draco::Mesh"** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.draco::Mesh"**, align 4
  store %"class.draco::Mesh"** %__t, %"class.draco::Mesh"*** %__t.addr, align 4
  %0 = load %"class.draco::Mesh"**, %"class.draco::Mesh"*** %__t.addr, align 4
  ret %"class.draco::Mesh"** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.2"* @_ZNSt3__222__compressed_pair_elemIPN5draco4MeshELi0ELb0EEC2IRS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.2"* returned %this, %"class.draco::Mesh"** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.2"*, align 4
  %__u.addr = alloca %"class.draco::Mesh"**, align 4
  store %"struct.std::__2::__compressed_pair_elem.2"* %this, %"struct.std::__2::__compressed_pair_elem.2"** %this.addr, align 4
  store %"class.draco::Mesh"** %__u, %"class.draco::Mesh"*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.2"*, %"struct.std::__2::__compressed_pair_elem.2"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.2", %"struct.std::__2::__compressed_pair_elem.2"* %this1, i32 0, i32 0
  %0 = load %"class.draco::Mesh"**, %"class.draco::Mesh"*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::Mesh"** @_ZNSt3__27forwardIRPN5draco4MeshEEEOT_RNS_16remove_referenceIS5_E4typeE(%"class.draco::Mesh"** nonnull align 4 dereferenceable(4) %0) #9
  %1 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %call, align 4
  store %"class.draco::Mesh"* %1, %"class.draco::Mesh"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.2"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  store %"struct.std::__2::__default_init_tag"* %__t, %"struct.std::__2::__default_init_tag"** %__t.addr, align 4
  %0 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t.addr, align 4
  ret %"struct.std::__2::__default_init_tag"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.120"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco4MeshEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.120"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.120"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.120"* %this, %"struct.std::__2::__compressed_pair_elem.120"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.120"*, %"struct.std::__2::__compressed_pair_elem.120"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.120"* %this1 to %"struct.std::__2::default_delete.121"*
  ret %"struct.std::__2::__compressed_pair_elem.120"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__210unique_ptrIN5draco4MeshENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr"* %this, %"class.draco::Mesh"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  %__p.addr = alloca %"class.draco::Mesh"*, align 4
  %__tmp = alloca %"class.draco::Mesh"*, align 4
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  store %"class.draco::Mesh"* %__p, %"class.draco::Mesh"** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::Mesh"** @_ZNSt3__217__compressed_pairIPN5draco4MeshENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.1"* %__ptr_) #9
  %0 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %call, align 4
  store %"class.draco::Mesh"* %0, %"class.draco::Mesh"** %__tmp, align 4
  %1 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %__p.addr, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::Mesh"** @_ZNSt3__217__compressed_pairIPN5draco4MeshENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.1"* %__ptr_2) #9
  store %"class.draco::Mesh"* %1, %"class.draco::Mesh"** %call3, align 4
  %2 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %__tmp, align 4
  %tobool = icmp ne %"class.draco::Mesh"* %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__ptr_4 = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call5 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.121"* @_ZNSt3__217__compressed_pairIPN5draco4MeshENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.1"* %__ptr_4) #9
  %3 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %__tmp, align 4
  call void @_ZNKSt3__214default_deleteIN5draco4MeshEEclEPS2_(%"struct.std::__2::default_delete.121"* %call5, %"class.draco::Mesh"* %3) #9
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::Mesh"** @_ZNSt3__217__compressed_pairIPN5draco4MeshENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.1"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.1"*, align 4
  store %"class.std::__2::__compressed_pair.1"* %this, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.1"*, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.1"* %this1 to %"struct.std::__2::__compressed_pair_elem.2"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::Mesh"** @_ZNSt3__222__compressed_pair_elemIPN5draco4MeshELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.2"* %0) #9
  ret %"class.draco::Mesh"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.121"* @_ZNSt3__217__compressed_pairIPN5draco4MeshENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.1"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.1"*, align 4
  store %"class.std::__2::__compressed_pair.1"* %this, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.1"*, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.1"* %this1 to %"struct.std::__2::__compressed_pair_elem.120"*
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.121"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco4MeshEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.120"* %0) #9
  ret %"struct.std::__2::default_delete.121"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__214default_deleteIN5draco4MeshEEclEPS2_(%"struct.std::__2::default_delete.121"* %this, %"class.draco::Mesh"* %__ptr) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::default_delete.121"*, align 4
  %__ptr.addr = alloca %"class.draco::Mesh"*, align 4
  store %"struct.std::__2::default_delete.121"* %this, %"struct.std::__2::default_delete.121"** %this.addr, align 4
  store %"class.draco::Mesh"* %__ptr, %"class.draco::Mesh"** %__ptr.addr, align 4
  %this1 = load %"struct.std::__2::default_delete.121"*, %"struct.std::__2::default_delete.121"** %this.addr, align 4
  %0 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %__ptr.addr, align 4
  %isnull = icmp eq %"class.draco::Mesh"* %0, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %entry
  %1 = bitcast %"class.draco::Mesh"* %0 to void (%"class.draco::Mesh"*)***
  %vtable = load void (%"class.draco::Mesh"*)**, void (%"class.draco::Mesh"*)*** %1, align 4
  %vfn = getelementptr inbounds void (%"class.draco::Mesh"*)*, void (%"class.draco::Mesh"*)** %vtable, i64 1
  %2 = load void (%"class.draco::Mesh"*)*, void (%"class.draco::Mesh"*)** %vfn, align 4
  call void %2(%"class.draco::Mesh"* %0) #9
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::Mesh"** @_ZNSt3__222__compressed_pair_elemIPN5draco4MeshELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.2"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.2"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.2"* %this, %"struct.std::__2::__compressed_pair_elem.2"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.2"*, %"struct.std::__2::__compressed_pair_elem.2"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.2", %"struct.std::__2::__compressed_pair_elem.2"* %this1, i32 0, i32 0
  ret %"class.draco::Mesh"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.121"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco4MeshEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.120"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.120"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.120"* %this, %"struct.std::__2::__compressed_pair_elem.120"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.120"*, %"struct.std::__2::__compressed_pair_elem.120"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.120"* %this1 to %"struct.std::__2::default_delete.121"*
  ret %"struct.std::__2::default_delete.121"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::Mesh"* @_ZNSt3__210unique_ptrIN5draco4MeshENS_14default_deleteIS2_EEE7releaseEv(%"class.std::__2::unique_ptr"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  %__t = alloca %"class.draco::Mesh"*, align 4
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::Mesh"** @_ZNSt3__217__compressed_pairIPN5draco4MeshENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.1"* %__ptr_) #9
  %0 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %call, align 4
  store %"class.draco::Mesh"* %0, %"class.draco::Mesh"** %__t, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::Mesh"** @_ZNSt3__217__compressed_pairIPN5draco4MeshENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.1"* %__ptr_2) #9
  store %"class.draco::Mesh"* null, %"class.draco::Mesh"** %call3, align 4
  %1 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %__t, align 4
  ret %"class.draco::Mesh"* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.121"* @_ZNSt3__27forwardINS_14default_deleteIN5draco4MeshEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.121"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"struct.std::__2::default_delete.121"*, align 4
  store %"struct.std::__2::default_delete.121"* %__t, %"struct.std::__2::default_delete.121"** %__t.addr, align 4
  %0 = load %"struct.std::__2::default_delete.121"*, %"struct.std::__2::default_delete.121"** %__t.addr, align 4
  ret %"struct.std::__2::default_delete.121"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.121"* @_ZNSt3__210unique_ptrIN5draco4MeshENS_14default_deleteIS2_EEE11get_deleterEv(%"class.std::__2::unique_ptr"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.121"* @_ZNSt3__217__compressed_pairIPN5draco4MeshENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.1"* %__ptr_) #9
  ret %"struct.std::__2::default_delete.121"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::Mesh"** @_ZNKSt3__217__compressed_pairIPN5draco4MeshENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.1"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.1"*, align 4
  store %"class.std::__2::__compressed_pair.1"* %this, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.1"*, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.1"* %this1 to %"struct.std::__2::__compressed_pair_elem.2"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::Mesh"** @_ZNKSt3__222__compressed_pair_elemIPN5draco4MeshELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.2"* %0) #9
  ret %"class.draco::Mesh"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::Mesh"** @_ZNKSt3__222__compressed_pair_elemIPN5draco4MeshELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.2"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.2"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.2"* %this, %"struct.std::__2::__compressed_pair_elem.2"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.2"*, %"struct.std::__2::__compressed_pair_elem.2"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.2", %"struct.std::__2::__compressed_pair_elem.2"* %this1, i32 0, i32 0
  ret %"class.draco::Mesh"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIaNS_9allocatorIaEEE5clearEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  %0 = load i8*, i8** %__begin_, align 4
  call void @_ZNSt3__213__vector_baseIaNS_9allocatorIaEEE17__destruct_at_endEPa(%"class.std::__2::__vector_base"* %this1, i8* %0) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIaNS_9allocatorIaEEE17__annotate_shrinkEm(%"class.std::__2::vector"* %this, i32 %__old_size) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__old_size.addr = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__old_size, i32* %__old_size.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %call = call i8* @_ZNKSt3__26vectorIaNS_9allocatorIaEEE4dataEv(%"class.std::__2::vector"* %this1) #9
  %call2 = call i8* @_ZNKSt3__26vectorIaNS_9allocatorIaEEE4dataEv(%"class.std::__2::vector"* %this1) #9
  %call3 = call i32 @_ZNKSt3__26vectorIaNS_9allocatorIaEEE8capacityEv(%"class.std::__2::vector"* %this1) #9
  %add.ptr = getelementptr inbounds i8, i8* %call2, i32 %call3
  %call4 = call i8* @_ZNKSt3__26vectorIaNS_9allocatorIaEEE4dataEv(%"class.std::__2::vector"* %this1) #9
  %0 = load i32, i32* %__old_size.addr, align 4
  %add.ptr5 = getelementptr inbounds i8, i8* %call4, i32 %0
  %call6 = call i8* @_ZNKSt3__26vectorIaNS_9allocatorIaEEE4dataEv(%"class.std::__2::vector"* %this1) #9
  %call7 = call i32 @_ZNKSt3__26vectorIaNS_9allocatorIaEEE4sizeEv(%"class.std::__2::vector"* %this1) #9
  %add.ptr8 = getelementptr inbounds i8, i8* %call6, i32 %call7
  call void @_ZNKSt3__26vectorIaNS_9allocatorIaEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector"* %this1, i8* %call, i8* %add.ptr, i8* %add.ptr5, i8* %add.ptr8) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIaNS_9allocatorIaEEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIaNS_9allocatorIaEEE17__destruct_at_endEPa(%"class.std::__2::__vector_base"* %this, i8* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  %__new_last.addr = alloca i8*, align 4
  %__soon_to_be_end = alloca i8*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  store i8* %__new_last, i8** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 1
  %0 = load i8*, i8** %__end_, align 4
  store i8* %0, i8** %__soon_to_be_end, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load i8*, i8** %__new_last.addr, align 4
  %2 = load i8*, i8** %__soon_to_be_end, align 4
  %cmp = icmp ne i8* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__213__vector_baseIaNS_9allocatorIaEEE7__allocEv(%"class.std::__2::__vector_base"* %this1) #9
  %3 = load i8*, i8** %__soon_to_be_end, align 4
  %incdec.ptr = getelementptr inbounds i8, i8* %3, i32 -1
  store i8* %incdec.ptr, i8** %__soon_to_be_end, align 4
  %call2 = call i8* @_ZNSt3__212__to_addressIaEEPT_S2_(i8* %incdec.ptr) #9
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIaEEE7destroyIaEEvRS2_PT_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call, i8* %call2)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %4 = load i8*, i8** %__new_last.addr, align 4
  %__end_3 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 1
  store i8* %4, i8** %__end_3, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIaEEE7destroyIaEEvRS2_PT_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, i8* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i8*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy.126", align 1
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy.126"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %2 = load i8*, i8** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIaEEE9__destroyIaEEvNS_17integral_constantIbLb1EEERS2_PT_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %1, i8* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__213__vector_baseIaNS_9allocatorIaEEE7__allocEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__217__compressed_pairIPaNS_9allocatorIaEEE6secondEv(%"class.std::__2::__compressed_pair"* %__end_cap_) #9
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__212__to_addressIaEEPT_S2_(i8* %__p) #0 comdat {
entry:
  %__p.addr = alloca i8*, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %0 = load i8*, i8** %__p.addr, align 4
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIaEEE9__destroyIaEEvNS_17integral_constantIbLb1EEERS2_PT_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, i8* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i8*, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %2 = load i8*, i8** %__p.addr, align 4
  call void @_ZNSt3__29allocatorIaE7destroyEPa(%"class.std::__2::allocator"* %1, i8* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIaE7destroyEPa(%"class.std::__2::allocator"* %this, i8* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i8*, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__217__compressed_pairIPaNS_9allocatorIaEEE6secondEv(%"class.std::__2::__compressed_pair"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem.0"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIaEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.0"* %0) #9
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIaEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.0"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.0"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.0"* %this, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.0"*, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.0"* %this1 to %"class.std::__2::allocator"*
  ret %"class.std::__2::allocator"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIaNS_9allocatorIaEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__26vectorIaNS_9allocatorIaEEE4dataEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 0
  %1 = load i8*, i8** %__begin_, align 4
  %call = call i8* @_ZNSt3__212__to_addressIaEEPT_S2_(i8* %1) #9
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIaNS_9allocatorIaEEE8capacityEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call i32 @_ZNKSt3__213__vector_baseIaNS_9allocatorIaEEE8capacityEv(%"class.std::__2::__vector_base"* %0) #9
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseIaNS_9allocatorIaEEE8capacityEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__213__vector_baseIaNS_9allocatorIaEEE9__end_capEv(%"class.std::__2::__vector_base"* %this1) #9
  %0 = load i8*, i8** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  %1 = load i8*, i8** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint i8* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  ret i32 %sub.ptr.sub
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__213__vector_baseIaNS_9allocatorIaEEE9__end_capEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__217__compressed_pairIPaNS_9allocatorIaEEE5firstEv(%"class.std::__2::__compressed_pair"* %__end_cap_) #9
  ret i8** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__217__compressed_pairIPaNS_9allocatorIaEEE5firstEv(%"class.std::__2::__compressed_pair"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__222__compressed_pair_elemIPaLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %0) #9
  ret i8** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__222__compressed_pair_elemIPaLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem"*, align 4
  store %"struct.std::__2::__compressed_pair_elem"* %this, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem"*, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem"* %this1, i32 0, i32 0
  ret i8** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNSt3__213__vector_baseIaNS_9allocatorIaEEE9__end_capEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__217__compressed_pairIPaNS_9allocatorIaEEE5firstEv(%"class.std::__2::__compressed_pair"* %__end_cap_) #9
  ret i8** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIaNS_9allocatorIaEEE22__construct_one_at_endIJaEEEvDpOT_(%"class.std::__2::vector"* %this, i8* nonnull align 1 dereferenceable(1) %__args) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__args.addr = alloca i8*, align 4
  %__tx = alloca %"struct.std::__2::vector<signed char, std::__2::allocator<signed char>>::_ConstructTransaction", align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i8* %__args, i8** %__args.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %call = call %"struct.std::__2::vector<signed char, std::__2::allocator<signed char>>::_ConstructTransaction"* @_ZNSt3__26vectorIaNS_9allocatorIaEEE21_ConstructTransactionC2ERS3_m(%"struct.std::__2::vector<signed char, std::__2::allocator<signed char>>::_ConstructTransaction"* %__tx, %"class.std::__2::vector"* nonnull align 4 dereferenceable(12) %this1, i32 1)
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__213__vector_baseIaNS_9allocatorIaEEE7__allocEv(%"class.std::__2::__vector_base"* %0) #9
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<signed char, std::__2::allocator<signed char>>::_ConstructTransaction", %"struct.std::__2::vector<signed char, std::__2::allocator<signed char>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %1 = load i8*, i8** %__pos_, align 4
  %call3 = call i8* @_ZNSt3__212__to_addressIaEEPT_S2_(i8* %1) #9
  %2 = load i8*, i8** %__args.addr, align 4
  %call4 = call nonnull align 1 dereferenceable(1) i8* @_ZNSt3__27forwardIaEEOT_RNS_16remove_referenceIS1_E4typeE(i8* nonnull align 1 dereferenceable(1) %2) #9
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIaEEE9constructIaJaEEEvRS2_PT_DpOT0_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call2, i8* %call3, i8* nonnull align 1 dereferenceable(1) %call4)
  %__pos_5 = getelementptr inbounds %"struct.std::__2::vector<signed char, std::__2::allocator<signed char>>::_ConstructTransaction", %"struct.std::__2::vector<signed char, std::__2::allocator<signed char>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %3 = load i8*, i8** %__pos_5, align 4
  %incdec.ptr = getelementptr inbounds i8, i8* %3, i32 1
  store i8* %incdec.ptr, i8** %__pos_5, align 4
  %call6 = call %"struct.std::__2::vector<signed char, std::__2::allocator<signed char>>::_ConstructTransaction"* @_ZNSt3__26vectorIaNS_9allocatorIaEEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<signed char, std::__2::allocator<signed char>>::_ConstructTransaction"* %__tx) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) i8* @_ZNSt3__24moveIRaEEONS_16remove_referenceIT_E4typeEOS3_(i8* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca i8*, align 4
  store i8* %__t, i8** %__t.addr, align 4
  %0 = load i8*, i8** %__t.addr, align 4
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIaNS_9allocatorIaEEE21__push_back_slow_pathIaEEvOT_(%"class.std::__2::vector"* %this, i8* nonnull align 1 dereferenceable(1) %__x) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__x.addr = alloca i8*, align 4
  %__a = alloca %"class.std::__2::allocator"*, align 4
  %__v = alloca %"struct.std::__2::__split_buffer.128", align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i8* %__x, i8** %__x.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__213__vector_baseIaNS_9allocatorIaEEE7__allocEv(%"class.std::__2::__vector_base"* %0) #9
  store %"class.std::__2::allocator"* %call, %"class.std::__2::allocator"** %__a, align 4
  %call2 = call i32 @_ZNKSt3__26vectorIaNS_9allocatorIaEEE4sizeEv(%"class.std::__2::vector"* %this1) #9
  %add = add i32 %call2, 1
  %call3 = call i32 @_ZNKSt3__26vectorIaNS_9allocatorIaEEE11__recommendEm(%"class.std::__2::vector"* %this1, i32 %add)
  %call4 = call i32 @_ZNKSt3__26vectorIaNS_9allocatorIaEEE4sizeEv(%"class.std::__2::vector"* %this1) #9
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a, align 4
  %call5 = call %"struct.std::__2::__split_buffer.128"* @_ZNSt3__214__split_bufferIaRNS_9allocatorIaEEEC2EmmS3_(%"struct.std::__2::__split_buffer.128"* %__v, i32 %call3, i32 %call4, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %1)
  %2 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer.128", %"struct.std::__2::__split_buffer.128"* %__v, i32 0, i32 2
  %3 = load i8*, i8** %__end_, align 4
  %call6 = call i8* @_ZNSt3__212__to_addressIaEEPT_S2_(i8* %3) #9
  %4 = load i8*, i8** %__x.addr, align 4
  %call7 = call nonnull align 1 dereferenceable(1) i8* @_ZNSt3__27forwardIaEEOT_RNS_16remove_referenceIS1_E4typeE(i8* nonnull align 1 dereferenceable(1) %4) #9
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIaEEE9constructIaJaEEEvRS2_PT_DpOT0_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %2, i8* %call6, i8* nonnull align 1 dereferenceable(1) %call7)
  %__end_8 = getelementptr inbounds %"struct.std::__2::__split_buffer.128", %"struct.std::__2::__split_buffer.128"* %__v, i32 0, i32 2
  %5 = load i8*, i8** %__end_8, align 4
  %incdec.ptr = getelementptr inbounds i8, i8* %5, i32 1
  store i8* %incdec.ptr, i8** %__end_8, align 4
  call void @_ZNSt3__26vectorIaNS_9allocatorIaEEE26__swap_out_circular_bufferERNS_14__split_bufferIaRS2_EE(%"class.std::__2::vector"* %this1, %"struct.std::__2::__split_buffer.128"* nonnull align 4 dereferenceable(20) %__v)
  %call9 = call %"struct.std::__2::__split_buffer.128"* @_ZNSt3__214__split_bufferIaRNS_9allocatorIaEEED2Ev(%"struct.std::__2::__split_buffer.128"* %__v) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNSt3__217__compressed_pairIPaNS_9allocatorIaEEE5firstEv(%"class.std::__2::__compressed_pair"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__222__compressed_pair_elemIPaLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %0) #9
  ret i8** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNSt3__222__compressed_pair_elemIPaLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem"*, align 4
  store %"struct.std::__2::__compressed_pair_elem"* %this, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem"*, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem"* %this1, i32 0, i32 0
  ret i8** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<signed char, std::__2::allocator<signed char>>::_ConstructTransaction"* @_ZNSt3__26vectorIaNS_9allocatorIaEEE21_ConstructTransactionC2ERS3_m(%"struct.std::__2::vector<signed char, std::__2::allocator<signed char>>::_ConstructTransaction"* returned %this, %"class.std::__2::vector"* nonnull align 4 dereferenceable(12) %__v, i32 %__n) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<signed char, std::__2::allocator<signed char>>::_ConstructTransaction"*, align 4
  %__v.addr = alloca %"class.std::__2::vector"*, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::vector<signed char, std::__2::allocator<signed char>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<signed char, std::__2::allocator<signed char>>::_ConstructTransaction"** %this.addr, align 4
  store %"class.std::__2::vector"* %__v, %"class.std::__2::vector"** %__v.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::vector<signed char, std::__2::allocator<signed char>>::_ConstructTransaction"*, %"struct.std::__2::vector<signed char, std::__2::allocator<signed char>>::_ConstructTransaction"** %this.addr, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<signed char, std::__2::allocator<signed char>>::_ConstructTransaction", %"struct.std::__2::vector<signed char, std::__2::allocator<signed char>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__v.addr, align 4
  store %"class.std::__2::vector"* %0, %"class.std::__2::vector"** %__v_, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<signed char, std::__2::allocator<signed char>>::_ConstructTransaction", %"struct.std::__2::vector<signed char, std::__2::allocator<signed char>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__v.addr, align 4
  %2 = bitcast %"class.std::__2::vector"* %1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %2, i32 0, i32 1
  %3 = load i8*, i8** %__end_, align 4
  store i8* %3, i8** %__pos_, align 4
  %__new_end_ = getelementptr inbounds %"struct.std::__2::vector<signed char, std::__2::allocator<signed char>>::_ConstructTransaction", %"struct.std::__2::vector<signed char, std::__2::allocator<signed char>>::_ConstructTransaction"* %this1, i32 0, i32 2
  %4 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__v.addr, align 4
  %5 = bitcast %"class.std::__2::vector"* %4 to %"class.std::__2::__vector_base"*
  %__end_2 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %5, i32 0, i32 1
  %6 = load i8*, i8** %__end_2, align 4
  %7 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds i8, i8* %6, i32 %7
  store i8* %add.ptr, i8** %__new_end_, align 4
  ret %"struct.std::__2::vector<signed char, std::__2::allocator<signed char>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIaEEE9constructIaJaEEEvRS2_PT_DpOT0_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, i8* %__p, i8* nonnull align 1 dereferenceable(1) %__args) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i8*, align 4
  %__args.addr = alloca i8*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_construct.127", align 1
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  store i8* %__args, i8** %__args.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_construct.127"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %2 = load i8*, i8** %__p.addr, align 4
  %3 = load i8*, i8** %__args.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) i8* @_ZNSt3__27forwardIaEEOT_RNS_16remove_referenceIS1_E4typeE(i8* nonnull align 1 dereferenceable(1) %3) #9
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIaEEE11__constructIaJaEEEvNS_17integral_constantIbLb1EEERS2_PT_DpOT0_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %1, i8* %2, i8* nonnull align 1 dereferenceable(1) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) i8* @_ZNSt3__27forwardIaEEOT_RNS_16remove_referenceIS1_E4typeE(i8* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca i8*, align 4
  store i8* %__t, i8** %__t.addr, align 4
  %0 = load i8*, i8** %__t.addr, align 4
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<signed char, std::__2::allocator<signed char>>::_ConstructTransaction"* @_ZNSt3__26vectorIaNS_9allocatorIaEEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<signed char, std::__2::allocator<signed char>>::_ConstructTransaction"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<signed char, std::__2::allocator<signed char>>::_ConstructTransaction"*, align 4
  store %"struct.std::__2::vector<signed char, std::__2::allocator<signed char>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<signed char, std::__2::allocator<signed char>>::_ConstructTransaction"** %this.addr, align 4
  %this1 = load %"struct.std::__2::vector<signed char, std::__2::allocator<signed char>>::_ConstructTransaction"*, %"struct.std::__2::vector<signed char, std::__2::allocator<signed char>>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<signed char, std::__2::allocator<signed char>>::_ConstructTransaction", %"struct.std::__2::vector<signed char, std::__2::allocator<signed char>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %0 = load i8*, i8** %__pos_, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<signed char, std::__2::allocator<signed char>>::_ConstructTransaction", %"struct.std::__2::vector<signed char, std::__2::allocator<signed char>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__v_, align 4
  %2 = bitcast %"class.std::__2::vector"* %1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %2, i32 0, i32 1
  store i8* %0, i8** %__end_, align 4
  ret %"struct.std::__2::vector<signed char, std::__2::allocator<signed char>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIaEEE11__constructIaJaEEEvNS_17integral_constantIbLb1EEERS2_PT_DpOT0_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, i8* %__p, i8* nonnull align 1 dereferenceable(1) %__args) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i8*, align 4
  %__args.addr = alloca i8*, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  store i8* %__args, i8** %__args.addr, align 4
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %2 = load i8*, i8** %__p.addr, align 4
  %3 = load i8*, i8** %__args.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) i8* @_ZNSt3__27forwardIaEEOT_RNS_16remove_referenceIS1_E4typeE(i8* nonnull align 1 dereferenceable(1) %3) #9
  call void @_ZNSt3__29allocatorIaE9constructIaJaEEEvPT_DpOT0_(%"class.std::__2::allocator"* %1, i8* %2, i8* nonnull align 1 dereferenceable(1) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIaE9constructIaJaEEEvPT_DpOT0_(%"class.std::__2::allocator"* %this, i8* %__p, i8* nonnull align 1 dereferenceable(1) %__args) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i8*, align 4
  %__args.addr = alloca i8*, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  store i8* %__args, i8** %__args.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  %0 = load i8*, i8** %__p.addr, align 4
  %1 = load i8*, i8** %__args.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) i8* @_ZNSt3__27forwardIaEEOT_RNS_16remove_referenceIS1_E4typeE(i8* nonnull align 1 dereferenceable(1) %1) #9
  %2 = load i8, i8* %call, align 1
  store i8 %2, i8* %0, align 1
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIaNS_9allocatorIaEEE11__recommendEm(%"class.std::__2::vector"* %this, i32 %__new_size) #0 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__new_size.addr = alloca i32, align 4
  %__ms = alloca i32, align 4
  %__cap = alloca i32, align 4
  %ref.tmp = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__new_size, i32* %__new_size.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIaNS_9allocatorIaEEE8max_sizeEv(%"class.std::__2::vector"* %this1) #9
  store i32 %call, i32* %__ms, align 4
  %0 = load i32, i32* %__new_size.addr, align 4
  %1 = load i32, i32* %__ms, align 4
  %cmp = icmp ugt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base_common"*
  call void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"* %2) #10
  unreachable

if.end:                                           ; preds = %entry
  %call2 = call i32 @_ZNKSt3__26vectorIaNS_9allocatorIaEEE8capacityEv(%"class.std::__2::vector"* %this1) #9
  store i32 %call2, i32* %__cap, align 4
  %3 = load i32, i32* %__cap, align 4
  %4 = load i32, i32* %__ms, align 4
  %div = udiv i32 %4, 2
  %cmp3 = icmp uge i32 %3, %div
  br i1 %cmp3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %if.end
  %5 = load i32, i32* %__ms, align 4
  store i32 %5, i32* %retval, align 4
  br label %return

if.end5:                                          ; preds = %if.end
  %6 = load i32, i32* %__cap, align 4
  %mul = mul i32 2, %6
  store i32 %mul, i32* %ref.tmp, align 4
  %call6 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %__new_size.addr)
  %7 = load i32, i32* %call6, align 4
  store i32 %7, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end5, %if.then4
  %8 = load i32, i32* %retval, align 4
  ret i32 %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer.128"* @_ZNSt3__214__split_bufferIaRNS_9allocatorIaEEEC2EmmS3_(%"struct.std::__2::__split_buffer.128"* returned %this, i32 %__cap, i32 %__start, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"struct.std::__2::__split_buffer.128"*, align 4
  %this.addr = alloca %"struct.std::__2::__split_buffer.128"*, align 4
  %__cap.addr = alloca i32, align 4
  %__start.addr = alloca i32, align 4
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %ref.tmp = alloca i8*, align 4
  store %"struct.std::__2::__split_buffer.128"* %this, %"struct.std::__2::__split_buffer.128"** %this.addr, align 4
  store i32 %__cap, i32* %__cap.addr, align 4
  store i32 %__start, i32* %__start.addr, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.128"*, %"struct.std::__2::__split_buffer.128"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer.128"* %this1, %"struct.std::__2::__split_buffer.128"** %retval, align 4
  %0 = bitcast %"struct.std::__2::__split_buffer.128"* %this1 to %"class.std::__2::__split_buffer_common"*
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer.128", %"struct.std::__2::__split_buffer.128"* %this1, i32 0, i32 3
  store i8* null, i8** %ref.tmp, align 4
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %call = call %"class.std::__2::__compressed_pair.129"* @_ZNSt3__217__compressed_pairIPaRNS_9allocatorIaEEEC2IDnS4_EEOT_OT0_(%"class.std::__2::__compressed_pair.129"* %__end_cap_, i8** nonnull align 4 dereferenceable(4) %ref.tmp, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %1)
  %2 = load i32, i32* %__cap.addr, align 4
  %cmp = icmp ne i32 %2, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__214__split_bufferIaRNS_9allocatorIaEEE7__allocEv(%"struct.std::__2::__split_buffer.128"* %this1) #9
  %3 = load i32, i32* %__cap.addr, align 4
  %call3 = call i8* @_ZNSt3__216allocator_traitsINS_9allocatorIaEEE8allocateERS2_m(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call2, i32 %3)
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i8* [ %call3, %cond.true ], [ null, %cond.false ]
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer.128", %"struct.std::__2::__split_buffer.128"* %this1, i32 0, i32 0
  store i8* %cond, i8** %__first_, align 4
  %__first_4 = getelementptr inbounds %"struct.std::__2::__split_buffer.128", %"struct.std::__2::__split_buffer.128"* %this1, i32 0, i32 0
  %4 = load i8*, i8** %__first_4, align 4
  %5 = load i32, i32* %__start.addr, align 4
  %add.ptr = getelementptr inbounds i8, i8* %4, i32 %5
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer.128", %"struct.std::__2::__split_buffer.128"* %this1, i32 0, i32 2
  store i8* %add.ptr, i8** %__end_, align 4
  %__begin_ = getelementptr inbounds %"struct.std::__2::__split_buffer.128", %"struct.std::__2::__split_buffer.128"* %this1, i32 0, i32 1
  store i8* %add.ptr, i8** %__begin_, align 4
  %__first_5 = getelementptr inbounds %"struct.std::__2::__split_buffer.128", %"struct.std::__2::__split_buffer.128"* %this1, i32 0, i32 0
  %6 = load i8*, i8** %__first_5, align 4
  %7 = load i32, i32* %__cap.addr, align 4
  %add.ptr6 = getelementptr inbounds i8, i8* %6, i32 %7
  %call7 = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__214__split_bufferIaRNS_9allocatorIaEEE9__end_capEv(%"struct.std::__2::__split_buffer.128"* %this1) #9
  store i8* %add.ptr6, i8** %call7, align 4
  %8 = load %"struct.std::__2::__split_buffer.128"*, %"struct.std::__2::__split_buffer.128"** %retval, align 4
  ret %"struct.std::__2::__split_buffer.128"* %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIaNS_9allocatorIaEEE26__swap_out_circular_bufferERNS_14__split_bufferIaRS2_EE(%"class.std::__2::vector"* %this, %"struct.std::__2::__split_buffer.128"* nonnull align 4 dereferenceable(20) %__v) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__v.addr = alloca %"struct.std::__2::__split_buffer.128"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer.128"* %__v, %"struct.std::__2::__split_buffer.128"** %__v.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  call void @_ZNKSt3__26vectorIaNS_9allocatorIaEEE17__annotate_deleteEv(%"class.std::__2::vector"* %this1) #9
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__213__vector_baseIaNS_9allocatorIaEEE7__allocEv(%"class.std::__2::__vector_base"* %0) #9
  %1 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %1, i32 0, i32 0
  %2 = load i8*, i8** %__begin_, align 4
  %3 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %3, i32 0, i32 1
  %4 = load i8*, i8** %__end_, align 4
  %5 = load %"struct.std::__2::__split_buffer.128"*, %"struct.std::__2::__split_buffer.128"** %__v.addr, align 4
  %__begin_2 = getelementptr inbounds %"struct.std::__2::__split_buffer.128", %"struct.std::__2::__split_buffer.128"* %5, i32 0, i32 1
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIaEEE46__construct_backward_with_exception_guaranteesIaEENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS2_PT_S8_EE5valuesr31is_trivially_move_constructibleIS8_EE5valueEvE4typeERS2_S9_S9_RS9_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call, i8* %2, i8* %4, i8** nonnull align 4 dereferenceable(4) %__begin_2)
  %6 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_3 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %6, i32 0, i32 0
  %7 = load %"struct.std::__2::__split_buffer.128"*, %"struct.std::__2::__split_buffer.128"** %__v.addr, align 4
  %__begin_4 = getelementptr inbounds %"struct.std::__2::__split_buffer.128", %"struct.std::__2::__split_buffer.128"* %7, i32 0, i32 1
  call void @_ZNSt3__24swapIPaEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_(i8** nonnull align 4 dereferenceable(4) %__begin_3, i8** nonnull align 4 dereferenceable(4) %__begin_4) #9
  %8 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__end_5 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %8, i32 0, i32 1
  %9 = load %"struct.std::__2::__split_buffer.128"*, %"struct.std::__2::__split_buffer.128"** %__v.addr, align 4
  %__end_6 = getelementptr inbounds %"struct.std::__2::__split_buffer.128", %"struct.std::__2::__split_buffer.128"* %9, i32 0, i32 2
  call void @_ZNSt3__24swapIPaEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_(i8** nonnull align 4 dereferenceable(4) %__end_5, i8** nonnull align 4 dereferenceable(4) %__end_6) #9
  %10 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call7 = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__213__vector_baseIaNS_9allocatorIaEEE9__end_capEv(%"class.std::__2::__vector_base"* %10) #9
  %11 = load %"struct.std::__2::__split_buffer.128"*, %"struct.std::__2::__split_buffer.128"** %__v.addr, align 4
  %call8 = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__214__split_bufferIaRNS_9allocatorIaEEE9__end_capEv(%"struct.std::__2::__split_buffer.128"* %11) #9
  call void @_ZNSt3__24swapIPaEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_(i8** nonnull align 4 dereferenceable(4) %call7, i8** nonnull align 4 dereferenceable(4) %call8) #9
  %12 = load %"struct.std::__2::__split_buffer.128"*, %"struct.std::__2::__split_buffer.128"** %__v.addr, align 4
  %__begin_9 = getelementptr inbounds %"struct.std::__2::__split_buffer.128", %"struct.std::__2::__split_buffer.128"* %12, i32 0, i32 1
  %13 = load i8*, i8** %__begin_9, align 4
  %14 = load %"struct.std::__2::__split_buffer.128"*, %"struct.std::__2::__split_buffer.128"** %__v.addr, align 4
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer.128", %"struct.std::__2::__split_buffer.128"* %14, i32 0, i32 0
  store i8* %13, i8** %__first_, align 4
  %call10 = call i32 @_ZNKSt3__26vectorIaNS_9allocatorIaEEE4sizeEv(%"class.std::__2::vector"* %this1) #9
  call void @_ZNKSt3__26vectorIaNS_9allocatorIaEEE14__annotate_newEm(%"class.std::__2::vector"* %this1, i32 %call10) #9
  call void @_ZNSt3__26vectorIaNS_9allocatorIaEEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector"* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer.128"* @_ZNSt3__214__split_bufferIaRNS_9allocatorIaEEED2Ev(%"struct.std::__2::__split_buffer.128"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"struct.std::__2::__split_buffer.128"*, align 4
  %this.addr = alloca %"struct.std::__2::__split_buffer.128"*, align 4
  store %"struct.std::__2::__split_buffer.128"* %this, %"struct.std::__2::__split_buffer.128"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.128"*, %"struct.std::__2::__split_buffer.128"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer.128"* %this1, %"struct.std::__2::__split_buffer.128"** %retval, align 4
  call void @_ZNSt3__214__split_bufferIaRNS_9allocatorIaEEE5clearEv(%"struct.std::__2::__split_buffer.128"* %this1) #9
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer.128", %"struct.std::__2::__split_buffer.128"* %this1, i32 0, i32 0
  %0 = load i8*, i8** %__first_, align 4
  %tobool = icmp ne i8* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__214__split_bufferIaRNS_9allocatorIaEEE7__allocEv(%"struct.std::__2::__split_buffer.128"* %this1) #9
  %__first_2 = getelementptr inbounds %"struct.std::__2::__split_buffer.128", %"struct.std::__2::__split_buffer.128"* %this1, i32 0, i32 0
  %1 = load i8*, i8** %__first_2, align 4
  %call3 = call i32 @_ZNKSt3__214__split_bufferIaRNS_9allocatorIaEEE8capacityEv(%"struct.std::__2::__split_buffer.128"* %this1)
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIaEEE10deallocateERS2_Pam(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call, i8* %1, i32 %call3) #9
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"struct.std::__2::__split_buffer.128"*, %"struct.std::__2::__split_buffer.128"** %retval, align 4
  ret %"struct.std::__2::__split_buffer.128"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIaNS_9allocatorIaEEE8max_sizeEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp3 = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNKSt3__213__vector_baseIaNS_9allocatorIaEEE7__allocEv(%"class.std::__2::__vector_base"* %0) #9
  %call2 = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorIaEEE8max_sizeERKS2_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call) #9
  store i32 %call2, i32* %ref.tmp, align 4
  %call4 = call i32 @_ZNSt3__214numeric_limitsIlE3maxEv() #9
  store i32 %call4, i32* %ref.tmp3, align 4
  %call5 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %1 = load i32, i32* %call5, align 4
  ret i32 %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorIaEEE8max_sizeERKS2_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_max_size.131", align 1
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_max_size.131"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %call = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorIaEEE10__max_sizeENS_17integral_constantIbLb1EEERKS2_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %1) #9
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNKSt3__213__vector_baseIaNS_9allocatorIaEEE7__allocEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNKSt3__217__compressed_pairIPaNS_9allocatorIaEEE6secondEv(%"class.std::__2::__compressed_pair"* %__end_cap_) #9
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorIaEEE10__max_sizeENS_17integral_constantIbLb1EEERKS2_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorIaE8max_sizeEv(%"class.std::__2::allocator"* %1) #9
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__29allocatorIaE8max_sizeEv(%"class.std::__2::allocator"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  ret i32 -1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNKSt3__217__compressed_pairIPaNS_9allocatorIaEEE6secondEv(%"class.std::__2::__compressed_pair"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem.0"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorIaEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.0"* %0) #9
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorIaEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.0"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.0"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.0"* %this, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.0"*, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.0"* %this1 to %"class.std::__2::allocator"*
  ret %"class.std::__2::allocator"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.129"* @_ZNSt3__217__compressed_pairIPaRNS_9allocatorIaEEEC2IDnS4_EEOT_OT0_(%"class.std::__2::__compressed_pair.129"* returned %this, i8** nonnull align 4 dereferenceable(4) %__t1, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.129"*, align 4
  %__t1.addr = alloca i8**, align 4
  %__t2.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"class.std::__2::__compressed_pair.129"* %this, %"class.std::__2::__compressed_pair.129"** %this.addr, align 4
  store i8** %__t1, i8*** %__t1.addr, align 4
  store %"class.std::__2::allocator"* %__t2, %"class.std::__2::allocator"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.129"*, %"class.std::__2::__compressed_pair.129"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.129"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %1 = load i8**, i8*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %1) #9
  %call2 = call %"struct.std::__2::__compressed_pair_elem"* @_ZNSt3__222__compressed_pair_elemIPaLi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem"* %0, i8** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.129"* %this1 to i8*
  %3 = getelementptr inbounds i8, i8* %2, i32 4
  %4 = bitcast i8* %3 to %"struct.std::__2::__compressed_pair_elem.130"*
  %5 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__27forwardIRNS_9allocatorIaEEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %5) #9
  %call4 = call %"struct.std::__2::__compressed_pair_elem.130"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIaEELi1ELb0EEC2IS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.130"* %4, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::__compressed_pair.129"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__216allocator_traitsINS_9allocatorIaEEE8allocateERS2_m(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call i8* @_ZNSt3__29allocatorIaE8allocateEmPKv(%"class.std::__2::allocator"* %0, i32 %1, i8* null)
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__214__split_bufferIaRNS_9allocatorIaEEE7__allocEv(%"struct.std::__2::__split_buffer.128"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.128"*, align 4
  store %"struct.std::__2::__split_buffer.128"* %this, %"struct.std::__2::__split_buffer.128"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.128"*, %"struct.std::__2::__split_buffer.128"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer.128", %"struct.std::__2::__split_buffer.128"* %this1, i32 0, i32 3
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__217__compressed_pairIPaRNS_9allocatorIaEEE6secondEv(%"class.std::__2::__compressed_pair.129"* %__end_cap_) #9
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNSt3__214__split_bufferIaRNS_9allocatorIaEEE9__end_capEv(%"struct.std::__2::__split_buffer.128"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.128"*, align 4
  store %"struct.std::__2::__split_buffer.128"* %this, %"struct.std::__2::__split_buffer.128"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.128"*, %"struct.std::__2::__split_buffer.128"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer.128", %"struct.std::__2::__split_buffer.128"* %this1, i32 0, i32 3
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__217__compressed_pairIPaRNS_9allocatorIaEEE5firstEv(%"class.std::__2::__compressed_pair.129"* %__end_cap_) #9
  ret i8** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem"* @_ZNSt3__222__compressed_pair_elemIPaLi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem"* returned %this, i8** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem"*, align 4
  %__u.addr = alloca i8**, align 4
  store %"struct.std::__2::__compressed_pair_elem"* %this, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  store i8** %__u, i8*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem"*, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem"* %this1, i32 0, i32 0
  %0 = load i8**, i8*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %0) #9
  store i8* null, i8** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__27forwardIRNS_9allocatorIaEEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"class.std::__2::allocator"* %__t, %"class.std::__2::allocator"** %__t.addr, align 4
  %0 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__t.addr, align 4
  ret %"class.std::__2::allocator"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.130"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIaEELi1ELb0EEC2IS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.130"* returned %this, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.130"*, align 4
  %__u.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.130"* %this, %"struct.std::__2::__compressed_pair_elem.130"** %this.addr, align 4
  store %"class.std::__2::allocator"* %__u, %"class.std::__2::allocator"** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.130"*, %"struct.std::__2::__compressed_pair_elem.130"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.130", %"struct.std::__2::__compressed_pair_elem.130"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__u.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__27forwardIRNS_9allocatorIaEEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %0) #9
  store %"class.std::__2::allocator"* %call, %"class.std::__2::allocator"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.130"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__29allocatorIaE8allocateEmPKv(%"class.std::__2::allocator"* %this, i32 %__n, i8* %0) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  %__n.addr = alloca i32, align 4
  %.addr = alloca i8*, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorIaE8max_sizeEv(%"class.std::__2::allocator"* %this1) #9
  %cmp = icmp ugt i32 %1, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__220__throw_length_errorEPKc(i8* getelementptr inbounds ([68 x i8], [68 x i8]* @.str, i32 0, i32 0)) #10
  unreachable

if.end:                                           ; preds = %entry
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 1
  %call2 = call i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %mul, i32 1)
  ret i8* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__217__compressed_pairIPaRNS_9allocatorIaEEE6secondEv(%"class.std::__2::__compressed_pair.129"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.129"*, align 4
  store %"class.std::__2::__compressed_pair.129"* %this, %"class.std::__2::__compressed_pair.129"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.129"*, %"class.std::__2::__compressed_pair.129"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.129"* %this1 to i8*
  %add.ptr = getelementptr inbounds i8, i8* %0, i32 4
  %1 = bitcast i8* %add.ptr to %"struct.std::__2::__compressed_pair_elem.130"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIaEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.130"* %1) #9
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIaEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.130"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.130"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.130"* %this, %"struct.std::__2::__compressed_pair_elem.130"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.130"*, %"struct.std::__2::__compressed_pair_elem.130"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.130", %"struct.std::__2::__compressed_pair_elem.130"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__value_, align 4
  ret %"class.std::__2::allocator"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNSt3__217__compressed_pairIPaRNS_9allocatorIaEEE5firstEv(%"class.std::__2::__compressed_pair.129"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.129"*, align 4
  store %"class.std::__2::__compressed_pair.129"* %this, %"class.std::__2::__compressed_pair.129"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.129"*, %"class.std::__2::__compressed_pair.129"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.129"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__222__compressed_pair_elemIPaLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %0) #9
  ret i8** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIaNS_9allocatorIaEEE17__annotate_deleteEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %call = call i8* @_ZNKSt3__26vectorIaNS_9allocatorIaEEE4dataEv(%"class.std::__2::vector"* %this1) #9
  %call2 = call i8* @_ZNKSt3__26vectorIaNS_9allocatorIaEEE4dataEv(%"class.std::__2::vector"* %this1) #9
  %call3 = call i32 @_ZNKSt3__26vectorIaNS_9allocatorIaEEE8capacityEv(%"class.std::__2::vector"* %this1) #9
  %add.ptr = getelementptr inbounds i8, i8* %call2, i32 %call3
  %call4 = call i8* @_ZNKSt3__26vectorIaNS_9allocatorIaEEE4dataEv(%"class.std::__2::vector"* %this1) #9
  %call5 = call i32 @_ZNKSt3__26vectorIaNS_9allocatorIaEEE4sizeEv(%"class.std::__2::vector"* %this1) #9
  %add.ptr6 = getelementptr inbounds i8, i8* %call4, i32 %call5
  %call7 = call i8* @_ZNKSt3__26vectorIaNS_9allocatorIaEEE4dataEv(%"class.std::__2::vector"* %this1) #9
  %call8 = call i32 @_ZNKSt3__26vectorIaNS_9allocatorIaEEE8capacityEv(%"class.std::__2::vector"* %this1) #9
  %add.ptr9 = getelementptr inbounds i8, i8* %call7, i32 %call8
  call void @_ZNKSt3__26vectorIaNS_9allocatorIaEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector"* %this1, i8* %call, i8* %add.ptr, i8* %add.ptr6, i8* %add.ptr9) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIaEEE46__construct_backward_with_exception_guaranteesIaEENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS2_PT_S8_EE5valuesr31is_trivially_move_constructibleIS8_EE5valueEvE4typeERS2_S9_S9_RS9_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %0, i8* %__begin1, i8* %__end1, i8** nonnull align 4 dereferenceable(4) %__end2) #0 comdat {
entry:
  %.addr = alloca %"class.std::__2::allocator"*, align 4
  %__begin1.addr = alloca i8*, align 4
  %__end1.addr = alloca i8*, align 4
  %__end2.addr = alloca i8**, align 4
  %_Np = alloca i32, align 4
  store %"class.std::__2::allocator"* %0, %"class.std::__2::allocator"** %.addr, align 4
  store i8* %__begin1, i8** %__begin1.addr, align 4
  store i8* %__end1, i8** %__end1.addr, align 4
  store i8** %__end2, i8*** %__end2.addr, align 4
  %1 = load i8*, i8** %__end1.addr, align 4
  %2 = load i8*, i8** %__begin1.addr, align 4
  %sub.ptr.lhs.cast = ptrtoint i8* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %2 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  store i32 %sub.ptr.sub, i32* %_Np, align 4
  %3 = load i32, i32* %_Np, align 4
  %4 = load i8**, i8*** %__end2.addr, align 4
  %5 = load i8*, i8** %4, align 4
  %idx.neg = sub i32 0, %3
  %add.ptr = getelementptr inbounds i8, i8* %5, i32 %idx.neg
  store i8* %add.ptr, i8** %4, align 4
  %6 = load i32, i32* %_Np, align 4
  %cmp = icmp sgt i32 %6, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %7 = load i8**, i8*** %__end2.addr, align 4
  %8 = load i8*, i8** %7, align 4
  %9 = load i8*, i8** %__begin1.addr, align 4
  %10 = load i32, i32* %_Np, align 4
  %mul = mul i32 %10, 1
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %8, i8* align 1 %9, i32 %mul, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__24swapIPaEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_(i8** nonnull align 4 dereferenceable(4) %__x, i8** nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %__x.addr = alloca i8**, align 4
  %__y.addr = alloca i8**, align 4
  %__t = alloca i8*, align 4
  store i8** %__x, i8*** %__x.addr, align 4
  store i8** %__y, i8*** %__y.addr, align 4
  %0 = load i8**, i8*** %__x.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__24moveIRPaEEONS_16remove_referenceIT_E4typeEOS4_(i8** nonnull align 4 dereferenceable(4) %0) #9
  %1 = load i8*, i8** %call, align 4
  store i8* %1, i8** %__t, align 4
  %2 = load i8**, i8*** %__y.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__24moveIRPaEEONS_16remove_referenceIT_E4typeEOS4_(i8** nonnull align 4 dereferenceable(4) %2) #9
  %3 = load i8*, i8** %call1, align 4
  %4 = load i8**, i8*** %__x.addr, align 4
  store i8* %3, i8** %4, align 4
  %call2 = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__24moveIRPaEEONS_16remove_referenceIT_E4typeEOS4_(i8** nonnull align 4 dereferenceable(4) %__t) #9
  %5 = load i8*, i8** %call2, align 4
  %6 = load i8**, i8*** %__y.addr, align 4
  store i8* %5, i8** %6, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIaNS_9allocatorIaEEE14__annotate_newEm(%"class.std::__2::vector"* %this, i32 %__current_size) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__current_size.addr = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__current_size, i32* %__current_size.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %call = call i8* @_ZNKSt3__26vectorIaNS_9allocatorIaEEE4dataEv(%"class.std::__2::vector"* %this1) #9
  %call2 = call i8* @_ZNKSt3__26vectorIaNS_9allocatorIaEEE4dataEv(%"class.std::__2::vector"* %this1) #9
  %call3 = call i32 @_ZNKSt3__26vectorIaNS_9allocatorIaEEE8capacityEv(%"class.std::__2::vector"* %this1) #9
  %add.ptr = getelementptr inbounds i8, i8* %call2, i32 %call3
  %call4 = call i8* @_ZNKSt3__26vectorIaNS_9allocatorIaEEE4dataEv(%"class.std::__2::vector"* %this1) #9
  %call5 = call i32 @_ZNKSt3__26vectorIaNS_9allocatorIaEEE8capacityEv(%"class.std::__2::vector"* %this1) #9
  %add.ptr6 = getelementptr inbounds i8, i8* %call4, i32 %call5
  %call7 = call i8* @_ZNKSt3__26vectorIaNS_9allocatorIaEEE4dataEv(%"class.std::__2::vector"* %this1) #9
  %0 = load i32, i32* %__current_size.addr, align 4
  %add.ptr8 = getelementptr inbounds i8, i8* %call7, i32 %0
  call void @_ZNKSt3__26vectorIaNS_9allocatorIaEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector"* %this1, i8* %call, i8* %add.ptr, i8* %add.ptr6, i8* %add.ptr8) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNSt3__24moveIRPaEEONS_16remove_referenceIT_E4typeEOS4_(i8** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca i8**, align 4
  store i8** %__t, i8*** %__t.addr, align 4
  %0 = load i8**, i8*** %__t.addr, align 4
  ret i8** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIaRNS_9allocatorIaEEE5clearEv(%"struct.std::__2::__split_buffer.128"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.128"*, align 4
  store %"struct.std::__2::__split_buffer.128"* %this, %"struct.std::__2::__split_buffer.128"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.128"*, %"struct.std::__2::__split_buffer.128"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"struct.std::__2::__split_buffer.128", %"struct.std::__2::__split_buffer.128"* %this1, i32 0, i32 1
  %0 = load i8*, i8** %__begin_, align 4
  call void @_ZNSt3__214__split_bufferIaRNS_9allocatorIaEEE17__destruct_at_endEPa(%"struct.std::__2::__split_buffer.128"* %this1, i8* %0) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIaEEE10deallocateERS2_Pam(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, i8* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i8*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %1 = load i8*, i8** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorIaE10deallocateEPam(%"class.std::__2::allocator"* %0, i8* %1, i32 %2) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__214__split_bufferIaRNS_9allocatorIaEEE8capacityEv(%"struct.std::__2::__split_buffer.128"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.128"*, align 4
  store %"struct.std::__2::__split_buffer.128"* %this, %"struct.std::__2::__split_buffer.128"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.128"*, %"struct.std::__2::__split_buffer.128"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__214__split_bufferIaRNS_9allocatorIaEEE9__end_capEv(%"struct.std::__2::__split_buffer.128"* %this1) #9
  %0 = load i8*, i8** %call, align 4
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer.128", %"struct.std::__2::__split_buffer.128"* %this1, i32 0, i32 0
  %1 = load i8*, i8** %__first_, align 4
  %sub.ptr.lhs.cast = ptrtoint i8* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  ret i32 %sub.ptr.sub
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIaRNS_9allocatorIaEEE17__destruct_at_endEPa(%"struct.std::__2::__split_buffer.128"* %this, i8* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.128"*, align 4
  %__new_last.addr = alloca i8*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant.125", align 1
  store %"struct.std::__2::__split_buffer.128"* %this, %"struct.std::__2::__split_buffer.128"** %this.addr, align 4
  store i8* %__new_last, i8** %__new_last.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.128"*, %"struct.std::__2::__split_buffer.128"** %this.addr, align 4
  %0 = load i8*, i8** %__new_last.addr, align 4
  call void @_ZNSt3__214__split_bufferIaRNS_9allocatorIaEEE17__destruct_at_endEPaNS_17integral_constantIbLb0EEE(%"struct.std::__2::__split_buffer.128"* %this1, i8* %0) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIaRNS_9allocatorIaEEE17__destruct_at_endEPaNS_17integral_constantIbLb0EEE(%"struct.std::__2::__split_buffer.128"* %this, i8* %__new_last) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant.125", align 1
  %this.addr = alloca %"struct.std::__2::__split_buffer.128"*, align 4
  %__new_last.addr = alloca i8*, align 4
  store %"struct.std::__2::__split_buffer.128"* %this, %"struct.std::__2::__split_buffer.128"** %this.addr, align 4
  store i8* %__new_last, i8** %__new_last.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.128"*, %"struct.std::__2::__split_buffer.128"** %this.addr, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load i8*, i8** %__new_last.addr, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer.128", %"struct.std::__2::__split_buffer.128"* %this1, i32 0, i32 2
  %2 = load i8*, i8** %__end_, align 4
  %cmp = icmp ne i8* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__214__split_bufferIaRNS_9allocatorIaEEE7__allocEv(%"struct.std::__2::__split_buffer.128"* %this1) #9
  %__end_2 = getelementptr inbounds %"struct.std::__2::__split_buffer.128", %"struct.std::__2::__split_buffer.128"* %this1, i32 0, i32 2
  %3 = load i8*, i8** %__end_2, align 4
  %incdec.ptr = getelementptr inbounds i8, i8* %3, i32 -1
  store i8* %incdec.ptr, i8** %__end_2, align 4
  %call3 = call i8* @_ZNSt3__212__to_addressIaEEPT_S2_(i8* %incdec.ptr) #9
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIaEEE7destroyIaEEvRS2_PT_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call, i8* %call3)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIaE10deallocateEPam(%"class.std::__2::allocator"* %this, i8* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i8*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  %0 = load i8*, i8** %__p.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %1, 1
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %0, i32 %mul, i32 1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__214__split_bufferIaRNS_9allocatorIaEEE9__end_capEv(%"struct.std::__2::__split_buffer.128"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.128"*, align 4
  store %"struct.std::__2::__split_buffer.128"* %this, %"struct.std::__2::__split_buffer.128"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.128"*, %"struct.std::__2::__split_buffer.128"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer.128", %"struct.std::__2::__split_buffer.128"* %this1, i32 0, i32 3
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__217__compressed_pairIPaRNS_9allocatorIaEEE5firstEv(%"class.std::__2::__compressed_pair.129"* %__end_cap_) #9
  ret i8** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__217__compressed_pairIPaRNS_9allocatorIaEEE5firstEv(%"class.std::__2::__compressed_pair.129"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.129"*, align 4
  store %"class.std::__2::__compressed_pair.129"* %this, %"class.std::__2::__compressed_pair.129"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.129"*, %"class.std::__2::__compressed_pair.129"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.129"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__222__compressed_pair_elemIPaLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %0) #9
  ret i8** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.1"* @_ZNSt3__217__compressed_pairIPN5draco4MeshENS_14default_deleteIS2_EEEC2IS3_S5_EEOT_OT0_(%"class.std::__2::__compressed_pair.1"* returned %this, %"class.draco::Mesh"** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::default_delete.121"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.1"*, align 4
  %__t1.addr = alloca %"class.draco::Mesh"**, align 4
  %__t2.addr = alloca %"struct.std::__2::default_delete.121"*, align 4
  store %"class.std::__2::__compressed_pair.1"* %this, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  store %"class.draco::Mesh"** %__t1, %"class.draco::Mesh"*** %__t1.addr, align 4
  store %"struct.std::__2::default_delete.121"* %__t2, %"struct.std::__2::default_delete.121"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.1"*, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.1"* %this1 to %"struct.std::__2::__compressed_pair_elem.2"*
  %1 = load %"class.draco::Mesh"**, %"class.draco::Mesh"*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::Mesh"** @_ZNSt3__27forwardIPN5draco4MeshEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::Mesh"** nonnull align 4 dereferenceable(4) %1) #9
  %call2 = call %"struct.std::__2::__compressed_pair_elem.2"* @_ZNSt3__222__compressed_pair_elemIPN5draco4MeshELi0ELb0EEC2IS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.2"* %0, %"class.draco::Mesh"** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.1"* %this1 to %"struct.std::__2::__compressed_pair_elem.120"*
  %3 = load %"struct.std::__2::default_delete.121"*, %"struct.std::__2::default_delete.121"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.121"* @_ZNSt3__27forwardINS_14default_deleteIN5draco4MeshEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.121"* nonnull align 1 dereferenceable(1) %3) #9
  %call4 = call %"struct.std::__2::__compressed_pair_elem.120"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco4MeshEEELi1ELb1EEC2IS4_vEEOT_(%"struct.std::__2::__compressed_pair_elem.120"* %2, %"struct.std::__2::default_delete.121"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::__compressed_pair.1"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::Mesh"** @_ZNSt3__27forwardIPN5draco4MeshEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::Mesh"** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.draco::Mesh"**, align 4
  store %"class.draco::Mesh"** %__t, %"class.draco::Mesh"*** %__t.addr, align 4
  %0 = load %"class.draco::Mesh"**, %"class.draco::Mesh"*** %__t.addr, align 4
  ret %"class.draco::Mesh"** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.2"* @_ZNSt3__222__compressed_pair_elemIPN5draco4MeshELi0ELb0EEC2IS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.2"* returned %this, %"class.draco::Mesh"** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.2"*, align 4
  %__u.addr = alloca %"class.draco::Mesh"**, align 4
  store %"struct.std::__2::__compressed_pair_elem.2"* %this, %"struct.std::__2::__compressed_pair_elem.2"** %this.addr, align 4
  store %"class.draco::Mesh"** %__u, %"class.draco::Mesh"*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.2"*, %"struct.std::__2::__compressed_pair_elem.2"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.2", %"struct.std::__2::__compressed_pair_elem.2"* %this1, i32 0, i32 0
  %0 = load %"class.draco::Mesh"**, %"class.draco::Mesh"*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::Mesh"** @_ZNSt3__27forwardIPN5draco4MeshEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::Mesh"** nonnull align 4 dereferenceable(4) %0) #9
  %1 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %call, align 4
  store %"class.draco::Mesh"* %1, %"class.draco::Mesh"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.2"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.120"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco4MeshEEELi1ELb1EEC2IS4_vEEOT_(%"struct.std::__2::__compressed_pair_elem.120"* returned %this, %"struct.std::__2::default_delete.121"* nonnull align 1 dereferenceable(1) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.120"*, align 4
  %__u.addr = alloca %"struct.std::__2::default_delete.121"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.120"* %this, %"struct.std::__2::__compressed_pair_elem.120"** %this.addr, align 4
  store %"struct.std::__2::default_delete.121"* %__u, %"struct.std::__2::default_delete.121"** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.120"*, %"struct.std::__2::__compressed_pair_elem.120"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.120"* %this1 to %"struct.std::__2::default_delete.121"*
  %1 = load %"struct.std::__2::default_delete.121"*, %"struct.std::__2::default_delete.121"** %__u.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.121"* @_ZNSt3__27forwardINS_14default_deleteIN5draco4MeshEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.121"* nonnull align 1 dereferenceable(1) %1) #9
  ret %"struct.std::__2::__compressed_pair_elem.120"* %this1
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nobuiltin allocsize(0) "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind willreturn }
attributes #4 = { argmemonly nounwind willreturn writeonly }
attributes #5 = { noreturn "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { noinline noreturn nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #8 = { builtin allocsize(0) }
attributes #9 = { nounwind }
attributes #10 = { noreturn }
attributes #11 = { builtin nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
