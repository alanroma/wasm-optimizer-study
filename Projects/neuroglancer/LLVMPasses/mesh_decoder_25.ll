; ModuleID = './draco/src/draco/compression/mesh/mesh_decoder.cc'
source_filename = "./draco/src/draco/compression/mesh/mesh_decoder.cc"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%"class.draco::MeshDecoder" = type { %"class.draco::PointCloudDecoder", %"class.draco::Mesh"* }
%"class.draco::PointCloudDecoder" = type { i32 (...)**, %"class.draco::PointCloud"*, %"class.std::__2::vector.94", %"class.std::__2::vector.87", %"class.draco::DecoderBuffer"*, i8, i8, %"class.draco::DracoOptions"* }
%"class.draco::PointCloud" = type { i32 (...)**, %"class.std::__2::unique_ptr", %"class.std::__2::vector.51", [5 x %"class.std::__2::vector.87"], i32 }
%"class.std::__2::unique_ptr" = type { %"class.std::__2::__compressed_pair" }
%"class.std::__2::__compressed_pair" = type { %"struct.std::__2::__compressed_pair_elem" }
%"struct.std::__2::__compressed_pair_elem" = type { %"class.draco::GeometryMetadata"* }
%"class.draco::GeometryMetadata" = type { %"class.draco::Metadata", %"class.std::__2::vector" }
%"class.draco::Metadata" = type { %"class.std::__2::unordered_map", %"class.std::__2::unordered_map.17" }
%"class.std::__2::unordered_map" = type { %"class.std::__2::__hash_table" }
%"class.std::__2::__hash_table" = type { %"class.std::__2::unique_ptr.0", %"class.std::__2::__compressed_pair.7", %"class.std::__2::__compressed_pair.12", %"class.std::__2::__compressed_pair.14" }
%"class.std::__2::unique_ptr.0" = type { %"class.std::__2::__compressed_pair.1" }
%"class.std::__2::__compressed_pair.1" = type { %"struct.std::__2::__compressed_pair_elem.2", %"struct.std::__2::__compressed_pair_elem.3" }
%"struct.std::__2::__compressed_pair_elem.2" = type { %"struct.std::__2::__hash_node_base"** }
%"struct.std::__2::__hash_node_base" = type { %"struct.std::__2::__hash_node_base"* }
%"struct.std::__2::__compressed_pair_elem.3" = type { %"class.std::__2::__bucket_list_deallocator" }
%"class.std::__2::__bucket_list_deallocator" = type { %"class.std::__2::__compressed_pair.4" }
%"class.std::__2::__compressed_pair.4" = type { %"struct.std::__2::__compressed_pair_elem.5" }
%"struct.std::__2::__compressed_pair_elem.5" = type { i32 }
%"class.std::__2::__compressed_pair.7" = type { %"struct.std::__2::__compressed_pair_elem.8" }
%"struct.std::__2::__compressed_pair_elem.8" = type { %"struct.std::__2::__hash_node_base" }
%"class.std::__2::__compressed_pair.12" = type { %"struct.std::__2::__compressed_pair_elem.5" }
%"class.std::__2::__compressed_pair.14" = type { %"struct.std::__2::__compressed_pair_elem.15" }
%"struct.std::__2::__compressed_pair_elem.15" = type { float }
%"class.std::__2::unordered_map.17" = type { %"class.std::__2::__hash_table.18" }
%"class.std::__2::__hash_table.18" = type { %"class.std::__2::unique_ptr.19", %"class.std::__2::__compressed_pair.29", %"class.std::__2::__compressed_pair.34", %"class.std::__2::__compressed_pair.37" }
%"class.std::__2::unique_ptr.19" = type { %"class.std::__2::__compressed_pair.20" }
%"class.std::__2::__compressed_pair.20" = type { %"struct.std::__2::__compressed_pair_elem.21", %"struct.std::__2::__compressed_pair_elem.23" }
%"struct.std::__2::__compressed_pair_elem.21" = type { %"struct.std::__2::__hash_node_base.22"** }
%"struct.std::__2::__hash_node_base.22" = type { %"struct.std::__2::__hash_node_base.22"* }
%"struct.std::__2::__compressed_pair_elem.23" = type { %"class.std::__2::__bucket_list_deallocator.24" }
%"class.std::__2::__bucket_list_deallocator.24" = type { %"class.std::__2::__compressed_pair.25" }
%"class.std::__2::__compressed_pair.25" = type { %"struct.std::__2::__compressed_pair_elem.5" }
%"class.std::__2::__compressed_pair.29" = type { %"struct.std::__2::__compressed_pair_elem.30" }
%"struct.std::__2::__compressed_pair_elem.30" = type { %"struct.std::__2::__hash_node_base.22" }
%"class.std::__2::__compressed_pair.34" = type { %"struct.std::__2::__compressed_pair_elem.5" }
%"class.std::__2::__compressed_pair.37" = type { %"struct.std::__2::__compressed_pair_elem.15" }
%"class.std::__2::vector" = type { %"class.std::__2::__vector_base" }
%"class.std::__2::__vector_base" = type { %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"*, %"class.std::__2::__compressed_pair.44" }
%"class.std::__2::unique_ptr.40" = type { %"class.std::__2::__compressed_pair.41" }
%"class.std::__2::__compressed_pair.41" = type { %"struct.std::__2::__compressed_pair_elem.42" }
%"struct.std::__2::__compressed_pair_elem.42" = type { %"class.draco::AttributeMetadata"* }
%"class.draco::AttributeMetadata" = type { %"class.draco::Metadata", i32 }
%"class.std::__2::__compressed_pair.44" = type { %"struct.std::__2::__compressed_pair_elem.45" }
%"struct.std::__2::__compressed_pair_elem.45" = type { %"class.std::__2::unique_ptr.40"* }
%"class.std::__2::vector.51" = type { %"class.std::__2::__vector_base.52" }
%"class.std::__2::__vector_base.52" = type { %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"*, %"class.std::__2::__compressed_pair.82" }
%"class.std::__2::unique_ptr.53" = type { %"class.std::__2::__compressed_pair.54" }
%"class.std::__2::__compressed_pair.54" = type { %"struct.std::__2::__compressed_pair_elem.55" }
%"struct.std::__2::__compressed_pair_elem.55" = type { %"class.draco::PointAttribute"* }
%"class.draco::PointAttribute" = type <{ %"class.draco::GeometryAttribute", %"class.std::__2::unique_ptr.63", %"class.draco::IndexTypeVector", i32, i8, [3 x i8], %"class.std::__2::unique_ptr.75", [4 x i8] }>
%"class.draco::GeometryAttribute" = type { %"class.draco::DataBuffer"*, %"struct.draco::DataBufferDescriptor", i8, i32, i8, i64, i64, i32, i32 }
%"class.draco::DataBuffer" = type { %"class.std::__2::vector.56", %"struct.draco::DataBufferDescriptor" }
%"class.std::__2::vector.56" = type { %"class.std::__2::__vector_base.57" }
%"class.std::__2::__vector_base.57" = type { i8*, i8*, %"class.std::__2::__compressed_pair.58" }
%"class.std::__2::__compressed_pair.58" = type { %"struct.std::__2::__compressed_pair_elem.59" }
%"struct.std::__2::__compressed_pair_elem.59" = type { i8* }
%"struct.draco::DataBufferDescriptor" = type { i64, i64 }
%"class.std::__2::unique_ptr.63" = type { %"class.std::__2::__compressed_pair.64" }
%"class.std::__2::__compressed_pair.64" = type { %"struct.std::__2::__compressed_pair_elem.65" }
%"struct.std::__2::__compressed_pair_elem.65" = type { %"class.draco::DataBuffer"* }
%"class.draco::IndexTypeVector" = type { %"class.std::__2::vector.68" }
%"class.std::__2::vector.68" = type { %"class.std::__2::__vector_base.69" }
%"class.std::__2::__vector_base.69" = type { %"class.draco::IndexType"*, %"class.draco::IndexType"*, %"class.std::__2::__compressed_pair.70" }
%"class.draco::IndexType" = type { i32 }
%"class.std::__2::__compressed_pair.70" = type { %"struct.std::__2::__compressed_pair_elem.71" }
%"struct.std::__2::__compressed_pair_elem.71" = type { %"class.draco::IndexType"* }
%"class.std::__2::unique_ptr.75" = type { %"class.std::__2::__compressed_pair.76" }
%"class.std::__2::__compressed_pair.76" = type { %"struct.std::__2::__compressed_pair_elem.77" }
%"struct.std::__2::__compressed_pair_elem.77" = type { %"class.draco::AttributeTransformData"* }
%"class.draco::AttributeTransformData" = type { i32, %"class.draco::DataBuffer" }
%"class.std::__2::__compressed_pair.82" = type { %"struct.std::__2::__compressed_pair_elem.83" }
%"struct.std::__2::__compressed_pair_elem.83" = type { %"class.std::__2::unique_ptr.53"* }
%"class.std::__2::vector.94" = type { %"class.std::__2::__vector_base.95" }
%"class.std::__2::__vector_base.95" = type { %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"*, %"class.std::__2::__compressed_pair.101" }
%"class.std::__2::unique_ptr.96" = type { %"class.std::__2::__compressed_pair.97" }
%"class.std::__2::__compressed_pair.97" = type { %"struct.std::__2::__compressed_pair_elem.98" }
%"struct.std::__2::__compressed_pair_elem.98" = type { %"class.draco::AttributesDecoderInterface"* }
%"class.draco::AttributesDecoderInterface" = type { i32 (...)** }
%"class.std::__2::__compressed_pair.101" = type { %"struct.std::__2::__compressed_pair_elem.102" }
%"struct.std::__2::__compressed_pair_elem.102" = type { %"class.std::__2::unique_ptr.96"* }
%"class.std::__2::vector.87" = type { %"class.std::__2::__vector_base.88" }
%"class.std::__2::__vector_base.88" = type { i32*, i32*, %"class.std::__2::__compressed_pair.89" }
%"class.std::__2::__compressed_pair.89" = type { %"struct.std::__2::__compressed_pair_elem.90" }
%"struct.std::__2::__compressed_pair_elem.90" = type { i32* }
%"class.draco::DecoderBuffer" = type { i8*, i64, i64, %"class.draco::DecoderBuffer::BitDecoder", i8, i16 }
%"class.draco::DecoderBuffer::BitDecoder" = type { i8*, i8*, i32 }
%"class.draco::DracoOptions" = type opaque
%"class.draco::Mesh" = type { %"class.draco::PointCloud", %"class.std::__2::vector.106", %"class.draco::IndexTypeVector.113" }
%"class.std::__2::vector.106" = type { %"class.std::__2::__vector_base.107" }
%"class.std::__2::__vector_base.107" = type { %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"*, %"class.std::__2::__compressed_pair.108" }
%"struct.draco::Mesh::AttributeData" = type { i32 }
%"class.std::__2::__compressed_pair.108" = type { %"struct.std::__2::__compressed_pair_elem.109" }
%"struct.std::__2::__compressed_pair_elem.109" = type { %"struct.draco::Mesh::AttributeData"* }
%"class.draco::IndexTypeVector.113" = type { %"class.std::__2::vector.114" }
%"class.std::__2::vector.114" = type { %"class.std::__2::__vector_base.115" }
%"class.std::__2::__vector_base.115" = type { %"struct.std::__2::array"*, %"struct.std::__2::array"*, %"class.std::__2::__compressed_pair.117" }
%"struct.std::__2::array" = type { [3 x %"class.draco::IndexType.116"] }
%"class.draco::IndexType.116" = type { i32 }
%"class.std::__2::__compressed_pair.117" = type { %"struct.std::__2::__compressed_pair_elem.118" }
%"struct.std::__2::__compressed_pair_elem.118" = type { %"struct.std::__2::array"* }
%"class.draco::Status" = type { i32, %"class.std::__2::basic_string" }
%"class.std::__2::basic_string" = type { %"class.std::__2::__compressed_pair.122" }
%"class.std::__2::__compressed_pair.122" = type { %"struct.std::__2::__compressed_pair_elem.123" }
%"struct.std::__2::__compressed_pair_elem.123" = type { %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep" }
%"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep" = type { %union.anon }
%union.anon = type { %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long" }
%"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long" = type { i8*, i32, i32 }
%"class.draco::CornerTable" = type { %"class.draco::IndexTypeVector.127", %"class.draco::IndexTypeVector.136", %"class.draco::IndexTypeVector.145", i32, i32, i32, %"class.draco::IndexTypeVector.146", %"class.draco::ValenceCache" }
%"class.draco::IndexTypeVector.127" = type { %"class.std::__2::vector.128" }
%"class.std::__2::vector.128" = type { %"class.std::__2::__vector_base.129" }
%"class.std::__2::__vector_base.129" = type { %"class.draco::IndexType.130"*, %"class.draco::IndexType.130"*, %"class.std::__2::__compressed_pair.131" }
%"class.draco::IndexType.130" = type { i32 }
%"class.std::__2::__compressed_pair.131" = type { %"struct.std::__2::__compressed_pair_elem.132" }
%"struct.std::__2::__compressed_pair_elem.132" = type { %"class.draco::IndexType.130"* }
%"class.draco::IndexTypeVector.136" = type { %"class.std::__2::vector.137" }
%"class.std::__2::vector.137" = type { %"class.std::__2::__vector_base.138" }
%"class.std::__2::__vector_base.138" = type { %"class.draco::IndexType.139"*, %"class.draco::IndexType.139"*, %"class.std::__2::__compressed_pair.140" }
%"class.draco::IndexType.139" = type { i32 }
%"class.std::__2::__compressed_pair.140" = type { %"struct.std::__2::__compressed_pair_elem.141" }
%"struct.std::__2::__compressed_pair_elem.141" = type { %"class.draco::IndexType.139"* }
%"class.draco::IndexTypeVector.145" = type { %"class.std::__2::vector.137" }
%"class.draco::IndexTypeVector.146" = type { %"class.std::__2::vector.128" }
%"class.draco::ValenceCache" = type { %"class.draco::CornerTable"*, %"class.draco::IndexTypeVector.147", %"class.draco::IndexTypeVector.155" }
%"class.draco::IndexTypeVector.147" = type { %"class.std::__2::vector.148" }
%"class.std::__2::vector.148" = type { %"class.std::__2::__vector_base.149" }
%"class.std::__2::__vector_base.149" = type { i8*, i8*, %"class.std::__2::__compressed_pair.150" }
%"class.std::__2::__compressed_pair.150" = type { %"struct.std::__2::__compressed_pair_elem.151" }
%"struct.std::__2::__compressed_pair_elem.151" = type { i8* }
%"class.draco::IndexTypeVector.155" = type { %"class.std::__2::vector.87" }
%"class.draco::MeshAttributeCornerTable" = type { %"class.std::__2::vector.156", %"class.std::__2::vector.156", i8, %"class.std::__2::vector.128", %"class.std::__2::vector.137", %"class.std::__2::vector.68", %"class.draco::CornerTable"*, %"class.draco::ValenceCache.161" }
%"class.std::__2::vector.156" = type { i32*, i32, %"class.std::__2::__compressed_pair.157" }
%"class.std::__2::__compressed_pair.157" = type { %"struct.std::__2::__compressed_pair_elem.5" }
%"class.draco::ValenceCache.161" = type { %"class.draco::MeshAttributeCornerTable"*, %"class.draco::IndexTypeVector.147", %"class.draco::IndexTypeVector.155" }
%"struct.draco::MeshAttributeIndicesEncodingData" = type { %"class.std::__2::vector.137", %"class.std::__2::vector.87", i32 }
%"class.std::__2::allocator.92" = type { i8 }
%"struct.std::__2::integral_constant" = type { i8 }
%"struct.std::__2::__has_destroy" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.91" = type { i8 }
%"class.std::__2::allocator.104" = type { i8 }
%"struct.std::__2::__has_destroy.162" = type { i8 }
%"struct.std::__2::default_delete.100" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.99" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.103" = type { i8 }

$_ZN5draco17PointCloudDecoder18DecodeGeometryDataEv = comdat any

$_ZN5draco11MeshDecoderD2Ev = comdat any

$_ZN5draco11MeshDecoderD0Ev = comdat any

$_ZNK5draco11MeshDecoder15GetGeometryTypeEv = comdat any

$_ZN5draco17PointCloudDecoder17InitializeDecoderEv = comdat any

$_ZN5draco17PointCloudDecoder19OnAttributesDecodedEv = comdat any

$_ZNK5draco11MeshDecoder14GetCornerTableEv = comdat any

$_ZNK5draco11MeshDecoder23GetAttributeCornerTableEi = comdat any

$_ZNK5draco11MeshDecoder24GetAttributeEncodingDataEi = comdat any

$_ZN5draco17PointCloudDecoderD2Ev = comdat any

$_ZNSt3__26vectorIiNS_9allocatorIiEEED2Ev = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEED2Ev = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEE17__annotate_deleteEv = comdat any

$_ZNSt3__213__vector_baseIiNS_9allocatorIiEEED2Ev = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEE31__annotate_contiguous_containerEPKvS5_S5_S5_ = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEE8capacityEv = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEE4sizeEv = comdat any

$_ZNSt3__212__to_addressIiEEPT_S2_ = comdat any

$_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPiNS_9allocatorIiEEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPiLi0ELb0EE5__getEv = comdat any

$_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE5clearEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIiEEE10deallocateERS2_Pim = comdat any

$_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE7__allocEv = comdat any

$_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE17__destruct_at_endEPi = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIiEEE7destroyIiEEvRS2_PT_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIiEEE9__destroyIiEEvNS_17integral_constantIbLb1EEERS2_PT_ = comdat any

$_ZNSt3__29allocatorIiE7destroyEPi = comdat any

$_ZNSt3__29allocatorIiE10deallocateEPim = comdat any

$_ZNSt3__219__libcpp_deallocateEPvmm = comdat any

$_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm = comdat any

$_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm = comdat any

$_ZNSt3__217_DeallocateCaller9__do_callEPv = comdat any

$_ZNSt3__217__compressed_pairIPiNS_9allocatorIiEEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIiEELi1ELb1EE5__getEv = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__annotate_deleteEv = comdat any

$_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEED2Ev = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE31__annotate_contiguous_containerEPKvSB_SB_SB_ = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv = comdat any

$_ZNSt3__212__to_addressINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEEPT_S8_ = comdat any

$_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5clearEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE10deallocateERS8_PS7_m = comdat any

$_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv = comdat any

$_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__destruct_at_endEPS6_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE7destroyIS7_EEvRS8_PT_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE9__destroyIS7_EEvNS_17integral_constantIbLb1EEERS8_PT_ = comdat any

$_ZNSt3__29allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEE7destroyEPS6_ = comdat any

$_ZNSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEED2Ev = comdat any

$_ZNSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE5resetEPS2_ = comdat any

$_ZNSt3__217__compressed_pairIPN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE6secondEv = comdat any

$_ZNKSt3__214default_deleteIN5draco26AttributesDecoderInterfaceEEclEPS2_ = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco26AttributesDecoderInterfaceELi0ELb0EE5__getEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco26AttributesDecoderInterfaceEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__29allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEE10deallocateEPS6_m = comdat any

$_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEELi1ELb1EE5__getEv = comdat any

@_ZTVN5draco11MeshDecoderE = hidden unnamed_addr constant { [15 x i8*] } { [15 x i8*] [i8* null, i8* null, i8* bitcast (%"class.draco::MeshDecoder"* (%"class.draco::MeshDecoder"*)* @_ZN5draco11MeshDecoderD2Ev to i8*), i8* bitcast (void (%"class.draco::MeshDecoder"*)* @_ZN5draco11MeshDecoderD0Ev to i8*), i8* bitcast (i32 (%"class.draco::MeshDecoder"*)* @_ZNK5draco11MeshDecoder15GetGeometryTypeEv to i8*), i8* bitcast (i1 (%"class.draco::PointCloudDecoder"*)* @_ZN5draco17PointCloudDecoder17InitializeDecoderEv to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (i1 (%"class.draco::MeshDecoder"*)* @_ZN5draco11MeshDecoder18DecodeGeometryDataEv to i8*), i8* bitcast (i1 (%"class.draco::PointCloudDecoder"*)* @_ZN5draco17PointCloudDecoder21DecodePointAttributesEv to i8*), i8* bitcast (i1 (%"class.draco::PointCloudDecoder"*)* @_ZN5draco17PointCloudDecoder19DecodeAllAttributesEv to i8*), i8* bitcast (i1 (%"class.draco::PointCloudDecoder"*)* @_ZN5draco17PointCloudDecoder19OnAttributesDecodedEv to i8*), i8* bitcast (%"class.draco::CornerTable"* (%"class.draco::MeshDecoder"*)* @_ZNK5draco11MeshDecoder14GetCornerTableEv to i8*), i8* bitcast (%"class.draco::MeshAttributeCornerTable"* (%"class.draco::MeshDecoder"*, i32)* @_ZNK5draco11MeshDecoder23GetAttributeCornerTableEi to i8*), i8* bitcast (%"struct.draco::MeshAttributeIndicesEncodingData"* (%"class.draco::MeshDecoder"*, i32)* @_ZNK5draco11MeshDecoder24GetAttributeEncodingDataEi to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, align 4
@_ZTVN5draco17PointCloudDecoderE = external unnamed_addr constant { [11 x i8*] }, align 4

; Function Attrs: noinline nounwind optnone
define hidden %"class.draco::MeshDecoder"* @_ZN5draco11MeshDecoderC2Ev(%"class.draco::MeshDecoder"* returned %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %"class.draco::MeshDecoder"*, align 4
  store %"class.draco::MeshDecoder"* %this, %"class.draco::MeshDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::MeshDecoder"*, %"class.draco::MeshDecoder"** %this.addr, align 4
  %0 = bitcast %"class.draco::MeshDecoder"* %this1 to %"class.draco::PointCloudDecoder"*
  %call = call %"class.draco::PointCloudDecoder"* @_ZN5draco17PointCloudDecoderC2Ev(%"class.draco::PointCloudDecoder"* %0)
  %1 = bitcast %"class.draco::MeshDecoder"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [15 x i8*] }, { [15 x i8*] }* @_ZTVN5draco11MeshDecoderE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %mesh_ = getelementptr inbounds %"class.draco::MeshDecoder", %"class.draco::MeshDecoder"* %this1, i32 0, i32 1
  store %"class.draco::Mesh"* null, %"class.draco::Mesh"** %mesh_, align 4
  ret %"class.draco::MeshDecoder"* %this1
}

declare %"class.draco::PointCloudDecoder"* @_ZN5draco17PointCloudDecoderC2Ev(%"class.draco::PointCloudDecoder"* returned) unnamed_addr #1

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN5draco11MeshDecoder6DecodeERKNS_12DracoOptionsINS_17GeometryAttribute4TypeEEEPNS_13DecoderBufferEPNS_4MeshE(%"class.draco::Status"* noalias sret align 4 %agg.result, %"class.draco::MeshDecoder"* %this, %"class.draco::DracoOptions"* nonnull align 1 %options, %"class.draco::DecoderBuffer"* %in_buffer, %"class.draco::Mesh"* %out_mesh) #0 {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %"class.draco::MeshDecoder"*, align 4
  %options.addr = alloca %"class.draco::DracoOptions"*, align 4
  %in_buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %out_mesh.addr = alloca %"class.draco::Mesh"*, align 4
  %0 = bitcast %"class.draco::Status"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %"class.draco::MeshDecoder"* %this, %"class.draco::MeshDecoder"** %this.addr, align 4
  store %"class.draco::DracoOptions"* %options, %"class.draco::DracoOptions"** %options.addr, align 4
  store %"class.draco::DecoderBuffer"* %in_buffer, %"class.draco::DecoderBuffer"** %in_buffer.addr, align 4
  store %"class.draco::Mesh"* %out_mesh, %"class.draco::Mesh"** %out_mesh.addr, align 4
  %this1 = load %"class.draco::MeshDecoder"*, %"class.draco::MeshDecoder"** %this.addr, align 4
  %1 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %out_mesh.addr, align 4
  %mesh_ = getelementptr inbounds %"class.draco::MeshDecoder", %"class.draco::MeshDecoder"* %this1, i32 0, i32 1
  store %"class.draco::Mesh"* %1, %"class.draco::Mesh"** %mesh_, align 4
  %2 = bitcast %"class.draco::MeshDecoder"* %this1 to %"class.draco::PointCloudDecoder"*
  %3 = load %"class.draco::DracoOptions"*, %"class.draco::DracoOptions"** %options.addr, align 4
  %4 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %in_buffer.addr, align 4
  %5 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %out_mesh.addr, align 4
  %6 = bitcast %"class.draco::Mesh"* %5 to %"class.draco::PointCloud"*
  call void @_ZN5draco17PointCloudDecoder6DecodeERKNS_12DracoOptionsINS_17GeometryAttribute4TypeEEEPNS_13DecoderBufferEPNS_10PointCloudE(%"class.draco::Status"* sret align 4 %agg.result, %"class.draco::PointCloudDecoder"* %2, %"class.draco::DracoOptions"* nonnull align 1 %3, %"class.draco::DecoderBuffer"* %4, %"class.draco::PointCloud"* %6)
  ret void
}

declare void @_ZN5draco17PointCloudDecoder6DecodeERKNS_12DracoOptionsINS_17GeometryAttribute4TypeEEEPNS_13DecoderBufferEPNS_10PointCloudE(%"class.draco::Status"* sret align 4, %"class.draco::PointCloudDecoder"*, %"class.draco::DracoOptions"* nonnull align 1, %"class.draco::DecoderBuffer"*, %"class.draco::PointCloud"*) #1

; Function Attrs: noinline nounwind optnone
define hidden zeroext i1 @_ZN5draco11MeshDecoder18DecodeGeometryDataEv(%"class.draco::MeshDecoder"* %this) unnamed_addr #0 {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::MeshDecoder"*, align 4
  store %"class.draco::MeshDecoder"* %this, %"class.draco::MeshDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::MeshDecoder"*, %"class.draco::MeshDecoder"** %this.addr, align 4
  %mesh_ = getelementptr inbounds %"class.draco::MeshDecoder", %"class.draco::MeshDecoder"* %this1, i32 0, i32 1
  %0 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %mesh_, align 4
  %cmp = icmp eq %"class.draco::Mesh"* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %1 = bitcast %"class.draco::MeshDecoder"* %this1 to i1 (%"class.draco::MeshDecoder"*)***
  %vtable = load i1 (%"class.draco::MeshDecoder"*)**, i1 (%"class.draco::MeshDecoder"*)*** %1, align 4
  %vfn = getelementptr inbounds i1 (%"class.draco::MeshDecoder"*)*, i1 (%"class.draco::MeshDecoder"*)** %vtable, i64 12
  %2 = load i1 (%"class.draco::MeshDecoder"*)*, i1 (%"class.draco::MeshDecoder"*)** %vfn, align 4
  %call = call zeroext i1 %2(%"class.draco::MeshDecoder"* %this1)
  br i1 %call, label %if.end3, label %if.then2

if.then2:                                         ; preds = %if.end
  store i1 false, i1* %retval, align 1
  br label %return

if.end3:                                          ; preds = %if.end
  %3 = bitcast %"class.draco::MeshDecoder"* %this1 to %"class.draco::PointCloudDecoder"*
  %call4 = call zeroext i1 @_ZN5draco17PointCloudDecoder18DecodeGeometryDataEv(%"class.draco::PointCloudDecoder"* %3)
  store i1 %call4, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end3, %if.then2, %if.then
  %4 = load i1, i1* %retval, align 1
  ret i1 %4
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco17PointCloudDecoder18DecodeGeometryDataEv(%"class.draco::PointCloudDecoder"* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointCloudDecoder"*, align 4
  store %"class.draco::PointCloudDecoder"* %this, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::PointCloudDecoder"*, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  ret i1 true
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::MeshDecoder"* @_ZN5draco11MeshDecoderD2Ev(%"class.draco::MeshDecoder"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::MeshDecoder"*, align 4
  store %"class.draco::MeshDecoder"* %this, %"class.draco::MeshDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::MeshDecoder"*, %"class.draco::MeshDecoder"** %this.addr, align 4
  %0 = bitcast %"class.draco::MeshDecoder"* %this1 to %"class.draco::PointCloudDecoder"*
  %call = call %"class.draco::PointCloudDecoder"* @_ZN5draco17PointCloudDecoderD2Ev(%"class.draco::PointCloudDecoder"* %0) #4
  ret %"class.draco::MeshDecoder"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco11MeshDecoderD0Ev(%"class.draco::MeshDecoder"* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::MeshDecoder"*, align 4
  store %"class.draco::MeshDecoder"* %this, %"class.draco::MeshDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::MeshDecoder"*, %"class.draco::MeshDecoder"** %this.addr, align 4
  call void @llvm.trap() #5
  unreachable
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco11MeshDecoder15GetGeometryTypeEv(%"class.draco::MeshDecoder"* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::MeshDecoder"*, align 4
  store %"class.draco::MeshDecoder"* %this, %"class.draco::MeshDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::MeshDecoder"*, %"class.draco::MeshDecoder"** %this.addr, align 4
  ret i32 1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco17PointCloudDecoder17InitializeDecoderEv(%"class.draco::PointCloudDecoder"* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointCloudDecoder"*, align 4
  store %"class.draco::PointCloudDecoder"* %this, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::PointCloudDecoder"*, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  ret i1 true
}

declare void @__cxa_pure_virtual() unnamed_addr

declare zeroext i1 @_ZN5draco17PointCloudDecoder21DecodePointAttributesEv(%"class.draco::PointCloudDecoder"*) unnamed_addr #1

declare zeroext i1 @_ZN5draco17PointCloudDecoder19DecodeAllAttributesEv(%"class.draco::PointCloudDecoder"*) unnamed_addr #1

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco17PointCloudDecoder19OnAttributesDecodedEv(%"class.draco::PointCloudDecoder"* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointCloudDecoder"*, align 4
  store %"class.draco::PointCloudDecoder"* %this, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::PointCloudDecoder"*, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  ret i1 true
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::CornerTable"* @_ZNK5draco11MeshDecoder14GetCornerTableEv(%"class.draco::MeshDecoder"* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::MeshDecoder"*, align 4
  store %"class.draco::MeshDecoder"* %this, %"class.draco::MeshDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::MeshDecoder"*, %"class.draco::MeshDecoder"** %this.addr, align 4
  ret %"class.draco::CornerTable"* null
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::MeshAttributeCornerTable"* @_ZNK5draco11MeshDecoder23GetAttributeCornerTableEi(%"class.draco::MeshDecoder"* %this, i32 %0) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::MeshDecoder"*, align 4
  %.addr = alloca i32, align 4
  store %"class.draco::MeshDecoder"* %this, %"class.draco::MeshDecoder"** %this.addr, align 4
  store i32 %0, i32* %.addr, align 4
  %this1 = load %"class.draco::MeshDecoder"*, %"class.draco::MeshDecoder"** %this.addr, align 4
  ret %"class.draco::MeshAttributeCornerTable"* null
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.draco::MeshAttributeIndicesEncodingData"* @_ZNK5draco11MeshDecoder24GetAttributeEncodingDataEi(%"class.draco::MeshDecoder"* %this, i32 %0) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::MeshDecoder"*, align 4
  %.addr = alloca i32, align 4
  store %"class.draco::MeshDecoder"* %this, %"class.draco::MeshDecoder"** %this.addr, align 4
  store i32 %0, i32* %.addr, align 4
  %this1 = load %"class.draco::MeshDecoder"*, %"class.draco::MeshDecoder"** %this.addr, align 4
  ret %"struct.draco::MeshAttributeIndicesEncodingData"* null
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::PointCloudDecoder"* @_ZN5draco17PointCloudDecoderD2Ev(%"class.draco::PointCloudDecoder"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointCloudDecoder"*, align 4
  store %"class.draco::PointCloudDecoder"* %this, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::PointCloudDecoder"*, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  %0 = bitcast %"class.draco::PointCloudDecoder"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [11 x i8*] }, { [11 x i8*] }* @_ZTVN5draco17PointCloudDecoderE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %attribute_to_decoder_map_ = getelementptr inbounds %"class.draco::PointCloudDecoder", %"class.draco::PointCloudDecoder"* %this1, i32 0, i32 3
  %call = call %"class.std::__2::vector.87"* @_ZNSt3__26vectorIiNS_9allocatorIiEEED2Ev(%"class.std::__2::vector.87"* %attribute_to_decoder_map_) #4
  %attributes_decoders_ = getelementptr inbounds %"class.draco::PointCloudDecoder", %"class.draco::PointCloudDecoder"* %this1, i32 0, i32 2
  %call2 = call %"class.std::__2::vector.94"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEED2Ev(%"class.std::__2::vector.94"* %attributes_decoders_) #4
  ret %"class.draco::PointCloudDecoder"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector.87"* @_ZNSt3__26vectorIiNS_9allocatorIiEEED2Ev(%"class.std::__2::vector.87"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  call void @_ZNKSt3__26vectorIiNS_9allocatorIiEEE17__annotate_deleteEv(%"class.std::__2::vector.87"* %this1) #4
  %0 = bitcast %"class.std::__2::vector.87"* %this1 to %"class.std::__2::__vector_base.88"*
  %call = call %"class.std::__2::__vector_base.88"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEED2Ev(%"class.std::__2::__vector_base.88"* %0) #4
  ret %"class.std::__2::vector.87"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector.94"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEED2Ev(%"class.std::__2::vector.94"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  call void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__annotate_deleteEv(%"class.std::__2::vector.94"* %this1) #4
  %0 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %call = call %"class.std::__2::__vector_base.95"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEED2Ev(%"class.std::__2::__vector_base.95"* %0) #4
  ret %"class.std::__2::vector.94"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIiNS_9allocatorIiEEE17__annotate_deleteEv(%"class.std::__2::vector.87"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  %call = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector.87"* %this1) #4
  %0 = bitcast i32* %call to i8*
  %call2 = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector.87"* %this1) #4
  %call3 = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::vector.87"* %this1) #4
  %add.ptr = getelementptr inbounds i32, i32* %call2, i32 %call3
  %1 = bitcast i32* %add.ptr to i8*
  %call4 = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector.87"* %this1) #4
  %call5 = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4sizeEv(%"class.std::__2::vector.87"* %this1) #4
  %add.ptr6 = getelementptr inbounds i32, i32* %call4, i32 %call5
  %2 = bitcast i32* %add.ptr6 to i8*
  %call7 = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector.87"* %this1) #4
  %call8 = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::vector.87"* %this1) #4
  %add.ptr9 = getelementptr inbounds i32, i32* %call7, i32 %call8
  %3 = bitcast i32* %add.ptr9 to i8*
  call void @_ZNKSt3__26vectorIiNS_9allocatorIiEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector.87"* %this1, i8* %0, i8* %1, i8* %2, i8* %3) #4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base.88"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEED2Ev(%"class.std::__2::__vector_base.88"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__vector_base.88"*, align 4
  %this.addr = alloca %"class.std::__2::__vector_base.88"*, align 4
  store %"class.std::__2::__vector_base.88"* %this, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.88"*, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  store %"class.std::__2::__vector_base.88"* %this1, %"class.std::__2::__vector_base.88"** %retval, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__begin_, align 4
  %cmp = icmp ne i32* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE5clearEv(%"class.std::__2::__vector_base.88"* %this1) #4
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE7__allocEv(%"class.std::__2::__vector_base.88"* %this1) #4
  %__begin_2 = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %this1, i32 0, i32 0
  %1 = load i32*, i32** %__begin_2, align 4
  %call3 = call i32 @_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::__vector_base.88"* %this1) #4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE10deallocateERS2_Pim(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %call, i32* %1, i32 %call3) #4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"class.std::__2::__vector_base.88"*, %"class.std::__2::__vector_base.88"** %retval, align 4
  ret %"class.std::__2::__vector_base.88"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIiNS_9allocatorIiEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector.87"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector.87"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.87"* %this1 to %"class.std::__2::__vector_base.88"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %0, i32 0, i32 0
  %1 = load i32*, i32** %__begin_, align 4
  %call = call i32* @_ZNSt3__212__to_addressIiEEPT_S2_(i32* %1) #4
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::vector.87"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.87"* %this1 to %"class.std::__2::__vector_base.88"*
  %call = call i32 @_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::__vector_base.88"* %0) #4
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4sizeEv(%"class.std::__2::vector.87"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.87"* %this1 to %"class.std::__2::__vector_base.88"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %0, i32 0, i32 1
  %1 = load i32*, i32** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector.87"* %this1 to %"class.std::__2::__vector_base.88"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %2, i32 0, i32 0
  %3 = load i32*, i32** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint i32* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint i32* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNSt3__212__to_addressIiEEPT_S2_(i32* %__p) #0 comdat {
entry:
  %__p.addr = alloca i32*, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %0 = load i32*, i32** %__p.addr, align 4
  ret i32* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::__vector_base.88"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.88"*, align 4
  store %"class.std::__2::__vector_base.88"* %this, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.88"*, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE9__end_capEv(%"class.std::__2::__vector_base.88"* %this1) #4
  %0 = load i32*, i32** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %this1, i32 0, i32 0
  %1 = load i32*, i32** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint i32* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint i32* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE9__end_capEv(%"class.std::__2::__vector_base.88"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.88"*, align 4
  store %"class.std::__2::__vector_base.88"* %this, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.88"*, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__217__compressed_pairIPiNS_9allocatorIiEEE5firstEv(%"class.std::__2::__compressed_pair.89"* %__end_cap_) #4
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__217__compressed_pairIPiNS_9allocatorIiEEE5firstEv(%"class.std::__2::__compressed_pair.89"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.89"*, align 4
  store %"class.std::__2::__compressed_pair.89"* %this, %"class.std::__2::__compressed_pair.89"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.89"*, %"class.std::__2::__compressed_pair.89"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.89"* %this1 to %"struct.std::__2::__compressed_pair_elem.90"*
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__222__compressed_pair_elemIPiLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.90"* %0) #4
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__222__compressed_pair_elemIPiLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.90"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.90"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.90"* %this, %"struct.std::__2::__compressed_pair_elem.90"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.90"*, %"struct.std::__2::__compressed_pair_elem.90"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.90", %"struct.std::__2::__compressed_pair_elem.90"* %this1, i32 0, i32 0
  ret i32** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE5clearEv(%"class.std::__2::__vector_base.88"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.88"*, align 4
  store %"class.std::__2::__vector_base.88"* %this, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.88"*, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__begin_, align 4
  call void @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE17__destruct_at_endEPi(%"class.std::__2::__vector_base.88"* %this1, i32* %0) #4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE10deallocateERS2_Pim(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %__a, i32* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.92"*, align 4
  %__p.addr = alloca i32*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.92"* %__a, %"class.std::__2::allocator.92"** %__a.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.92"*, %"class.std::__2::allocator.92"** %__a.addr, align 4
  %1 = load i32*, i32** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorIiE10deallocateEPim(%"class.std::__2::allocator.92"* %0, i32* %1, i32 %2) #4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE7__allocEv(%"class.std::__2::__vector_base.88"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.88"*, align 4
  store %"class.std::__2::__vector_base.88"* %this, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.88"*, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__217__compressed_pairIPiNS_9allocatorIiEEE6secondEv(%"class.std::__2::__compressed_pair.89"* %__end_cap_) #4
  ret %"class.std::__2::allocator.92"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE17__destruct_at_endEPi(%"class.std::__2::__vector_base.88"* %this, i32* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.88"*, align 4
  %__new_last.addr = alloca i32*, align 4
  %__soon_to_be_end = alloca i32*, align 4
  store %"class.std::__2::__vector_base.88"* %this, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  store i32* %__new_last, i32** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.88"*, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %this1, i32 0, i32 1
  %0 = load i32*, i32** %__end_, align 4
  store i32* %0, i32** %__soon_to_be_end, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load i32*, i32** %__new_last.addr, align 4
  %2 = load i32*, i32** %__soon_to_be_end, align 4
  %cmp = icmp ne i32* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE7__allocEv(%"class.std::__2::__vector_base.88"* %this1) #4
  %3 = load i32*, i32** %__soon_to_be_end, align 4
  %incdec.ptr = getelementptr inbounds i32, i32* %3, i32 -1
  store i32* %incdec.ptr, i32** %__soon_to_be_end, align 4
  %call2 = call i32* @_ZNSt3__212__to_addressIiEEPT_S2_(i32* %incdec.ptr) #4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE7destroyIiEEvRS2_PT_(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %call, i32* %call2)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %4 = load i32*, i32** %__new_last.addr, align 4
  %__end_3 = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %this1, i32 0, i32 1
  store i32* %4, i32** %__end_3, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE7destroyIiEEvRS2_PT_(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %__a, i32* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.92"*, align 4
  %__p.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy", align 1
  store %"class.std::__2::allocator.92"* %__a, %"class.std::__2::allocator.92"** %__a.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.92"*, %"class.std::__2::allocator.92"** %__a.addr, align 4
  %2 = load i32*, i32** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE9__destroyIiEEvNS_17integral_constantIbLb1EEERS2_PT_(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %1, i32* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE9__destroyIiEEvNS_17integral_constantIbLb1EEERS2_PT_(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %__a, i32* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.92"*, align 4
  %__p.addr = alloca i32*, align 4
  store %"class.std::__2::allocator.92"* %__a, %"class.std::__2::allocator.92"** %__a.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.92"*, %"class.std::__2::allocator.92"** %__a.addr, align 4
  %2 = load i32*, i32** %__p.addr, align 4
  call void @_ZNSt3__29allocatorIiE7destroyEPi(%"class.std::__2::allocator.92"* %1, i32* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIiE7destroyEPi(%"class.std::__2::allocator.92"* %this, i32* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.92"*, align 4
  %__p.addr = alloca i32*, align 4
  store %"class.std::__2::allocator.92"* %this, %"class.std::__2::allocator.92"** %this.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.92"*, %"class.std::__2::allocator.92"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIiE10deallocateEPim(%"class.std::__2::allocator.92"* %this, i32* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.92"*, align 4
  %__p.addr = alloca i32*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.92"* %this, %"class.std::__2::allocator.92"** %this.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.92"*, %"class.std::__2::allocator.92"** %this.addr, align 4
  %0 = load i32*, i32** %__p.addr, align 4
  %1 = bitcast i32* %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %__ptr, i32 %__size, i32 %__align) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  %1 = load i32, i32* %__size.addr, align 4
  %2 = load i32, i32* %__align.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm(i8* %0, i32 %1, i32 %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm(i8* %__ptr, i32 %__size, i32 %__align) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  %1 = load i32, i32* %__size.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm(i8* %0, i32 %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm(i8* %__ptr, i32 %__size) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller9__do_callEPv(i8* %0)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller9__do_callEPv(i8* %__ptr) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  call void @_ZdlPv(i8* %0) #6
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #2

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__217__compressed_pairIPiNS_9allocatorIiEEE6secondEv(%"class.std::__2::__compressed_pair.89"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.89"*, align 4
  store %"class.std::__2::__compressed_pair.89"* %this, %"class.std::__2::__compressed_pair.89"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.89"*, %"class.std::__2::__compressed_pair.89"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.89"* %this1 to %"struct.std::__2::__compressed_pair_elem.91"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIiEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.91"* %0) #4
  ret %"class.std::__2::allocator.92"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIiEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.91"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.91"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.91"* %this, %"struct.std::__2::__compressed_pair_elem.91"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.91"*, %"struct.std::__2::__compressed_pair_elem.91"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.91"* %this1 to %"class.std::__2::allocator.92"*
  ret %"class.std::__2::allocator.92"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__annotate_deleteEv(%"class.std::__2::vector.94"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  %call = call %"class.std::__2::unique_ptr.96"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.94"* %this1) #4
  %0 = bitcast %"class.std::__2::unique_ptr.96"* %call to i8*
  %call2 = call %"class.std::__2::unique_ptr.96"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.94"* %this1) #4
  %call3 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector.94"* %this1) #4
  %add.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %call2, i32 %call3
  %1 = bitcast %"class.std::__2::unique_ptr.96"* %add.ptr to i8*
  %call4 = call %"class.std::__2::unique_ptr.96"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.94"* %this1) #4
  %call5 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.94"* %this1) #4
  %add.ptr6 = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %call4, i32 %call5
  %2 = bitcast %"class.std::__2::unique_ptr.96"* %add.ptr6 to i8*
  %call7 = call %"class.std::__2::unique_ptr.96"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.94"* %this1) #4
  %call8 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector.94"* %this1) #4
  %add.ptr9 = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %call7, i32 %call8
  %3 = bitcast %"class.std::__2::unique_ptr.96"* %add.ptr9 to i8*
  call void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE31__annotate_contiguous_containerEPKvSB_SB_SB_(%"class.std::__2::vector.94"* %this1, i8* %0, i8* %1, i8* %2, i8* %3) #4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base.95"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEED2Ev(%"class.std::__2::__vector_base.95"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__vector_base.95"*, align 4
  %this.addr = alloca %"class.std::__2::__vector_base.95"*, align 4
  store %"class.std::__2::__vector_base.95"* %this, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.95"*, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  store %"class.std::__2::__vector_base.95"* %this1, %"class.std::__2::__vector_base.95"** %retval, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__begin_, align 4
  %cmp = icmp ne %"class.std::__2::unique_ptr.96"* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5clearEv(%"class.std::__2::__vector_base.95"* %this1) #4
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base.95"* %this1) #4
  %__begin_2 = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__begin_2, align 4
  %call3 = call i32 @_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::__vector_base.95"* %this1) #4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE10deallocateERS8_PS7_m(%"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %call, %"class.std::__2::unique_ptr.96"* %1, i32 %call3) #4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"class.std::__2::__vector_base.95"*, %"class.std::__2::__vector_base.95"** %retval, align 4
  ret %"class.std::__2::__vector_base.95"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE31__annotate_contiguous_containerEPKvSB_SB_SB_(%"class.std::__2::vector.94"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.96"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.94"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %0, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__begin_, align 4
  %call = call %"class.std::__2::unique_ptr.96"* @_ZNSt3__212__to_addressINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEEPT_S8_(%"class.std::__2::unique_ptr.96"* %1) #4
  ret %"class.std::__2::unique_ptr.96"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector.94"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %call = call i32 @_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::__vector_base.95"* %0) #4
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.94"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %0, i32 0, i32 1
  %1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %2, i32 0, i32 0
  %3 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.std::__2::unique_ptr.96"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.std::__2::unique_ptr.96"* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.96"* @_ZNSt3__212__to_addressINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEEPT_S8_(%"class.std::__2::unique_ptr.96"* %__p) #0 comdat {
entry:
  %__p.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  store %"class.std::__2::unique_ptr.96"* %__p, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  ret %"class.std::__2::unique_ptr.96"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::__vector_base.95"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.95"*, align 4
  store %"class.std::__2::__vector_base.95"* %this, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.95"*, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"** @_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE9__end_capEv(%"class.std::__2::__vector_base.95"* %this1) #4
  %0 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.std::__2::unique_ptr.96"* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.std::__2::unique_ptr.96"* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"** @_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE9__end_capEv(%"class.std::__2::__vector_base.95"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.95"*, align 4
  store %"class.std::__2::__vector_base.95"* %this, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.95"*, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"** @_ZNKSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.101"* %__end_cap_) #4
  ret %"class.std::__2::unique_ptr.96"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"** @_ZNKSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.101"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.101"*, align 4
  store %"class.std::__2::__compressed_pair.101"* %this, %"class.std::__2::__compressed_pair.101"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.101"*, %"class.std::__2::__compressed_pair.101"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.101"* %this1 to %"struct.std::__2::__compressed_pair_elem.102"*
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"** @_ZNKSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.102"* %0) #4
  ret %"class.std::__2::unique_ptr.96"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"** @_ZNKSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.102"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.102"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.102"* %this, %"struct.std::__2::__compressed_pair_elem.102"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.102"*, %"struct.std::__2::__compressed_pair_elem.102"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.102", %"struct.std::__2::__compressed_pair_elem.102"* %this1, i32 0, i32 0
  ret %"class.std::__2::unique_ptr.96"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5clearEv(%"class.std::__2::__vector_base.95"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.95"*, align 4
  store %"class.std::__2::__vector_base.95"* %this, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.95"*, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__begin_, align 4
  call void @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__destruct_at_endEPS6_(%"class.std::__2::__vector_base.95"* %this1, %"class.std::__2::unique_ptr.96"* %0) #4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE10deallocateERS8_PS7_m(%"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %__a, %"class.std::__2::unique_ptr.96"* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.104"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.104"* %__a, %"class.std::__2::allocator.104"** %__a.addr, align 4
  store %"class.std::__2::unique_ptr.96"* %__p, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.104"*, %"class.std::__2::allocator.104"** %__a.addr, align 4
  %1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEE10deallocateEPS6_m(%"class.std::__2::allocator.104"* %0, %"class.std::__2::unique_ptr.96"* %1, i32 %2) #4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base.95"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.95"*, align 4
  store %"class.std::__2::__vector_base.95"* %this, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.95"*, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE6secondEv(%"class.std::__2::__compressed_pair.101"* %__end_cap_) #4
  ret %"class.std::__2::allocator.104"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__destruct_at_endEPS6_(%"class.std::__2::__vector_base.95"* %this, %"class.std::__2::unique_ptr.96"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.95"*, align 4
  %__new_last.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  %__soon_to_be_end = alloca %"class.std::__2::unique_ptr.96"*, align 4
  store %"class.std::__2::__vector_base.95"* %this, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.96"* %__new_last, %"class.std::__2::unique_ptr.96"** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.95"*, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %this1, i32 0, i32 1
  %0 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__end_, align 4
  store %"class.std::__2::unique_ptr.96"* %0, %"class.std::__2::unique_ptr.96"** %__soon_to_be_end, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__new_last.addr, align 4
  %2 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__soon_to_be_end, align 4
  %cmp = icmp ne %"class.std::__2::unique_ptr.96"* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base.95"* %this1) #4
  %3 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__soon_to_be_end, align 4
  %incdec.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %3, i32 -1
  store %"class.std::__2::unique_ptr.96"* %incdec.ptr, %"class.std::__2::unique_ptr.96"** %__soon_to_be_end, align 4
  %call2 = call %"class.std::__2::unique_ptr.96"* @_ZNSt3__212__to_addressINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEEPT_S8_(%"class.std::__2::unique_ptr.96"* %incdec.ptr) #4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE7destroyIS7_EEvRS8_PT_(%"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %call, %"class.std::__2::unique_ptr.96"* %call2)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %4 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__new_last.addr, align 4
  %__end_3 = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %this1, i32 0, i32 1
  store %"class.std::__2::unique_ptr.96"* %4, %"class.std::__2::unique_ptr.96"** %__end_3, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE7destroyIS7_EEvRS8_PT_(%"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %__a, %"class.std::__2::unique_ptr.96"* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.104"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy.162", align 1
  store %"class.std::__2::allocator.104"* %__a, %"class.std::__2::allocator.104"** %__a.addr, align 4
  store %"class.std::__2::unique_ptr.96"* %__p, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy.162"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.104"*, %"class.std::__2::allocator.104"** %__a.addr, align 4
  %2 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE9__destroyIS7_EEvNS_17integral_constantIbLb1EEERS8_PT_(%"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %1, %"class.std::__2::unique_ptr.96"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE9__destroyIS7_EEvNS_17integral_constantIbLb1EEERS8_PT_(%"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %__a, %"class.std::__2::unique_ptr.96"* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.104"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  store %"class.std::__2::allocator.104"* %__a, %"class.std::__2::allocator.104"** %__a.addr, align 4
  store %"class.std::__2::unique_ptr.96"* %__p, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.104"*, %"class.std::__2::allocator.104"** %__a.addr, align 4
  %2 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  call void @_ZNSt3__29allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEE7destroyEPS6_(%"class.std::__2::allocator.104"* %1, %"class.std::__2::unique_ptr.96"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEE7destroyEPS6_(%"class.std::__2::allocator.104"* %this, %"class.std::__2::unique_ptr.96"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.104"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  store %"class.std::__2::allocator.104"* %this, %"class.std::__2::allocator.104"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.96"* %__p, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.104"*, %"class.std::__2::allocator.104"** %this.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  %call = call %"class.std::__2::unique_ptr.96"* @_ZNSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.96"* %0) #4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.96"* @_ZNSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.96"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  store %"class.std::__2::unique_ptr.96"* %this, %"class.std::__2::unique_ptr.96"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %this.addr, align 4
  call void @_ZNSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.96"* %this1, %"class.draco::AttributesDecoderInterface"* null) #4
  ret %"class.std::__2::unique_ptr.96"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.96"* %this, %"class.draco::AttributesDecoderInterface"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  %__p.addr = alloca %"class.draco::AttributesDecoderInterface"*, align 4
  %__tmp = alloca %"class.draco::AttributesDecoderInterface"*, align 4
  store %"class.std::__2::unique_ptr.96"* %this, %"class.std::__2::unique_ptr.96"** %this.addr, align 4
  store %"class.draco::AttributesDecoderInterface"* %__p, %"class.draco::AttributesDecoderInterface"** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributesDecoderInterface"** @_ZNSt3__217__compressed_pairIPN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.97"* %__ptr_) #4
  %0 = load %"class.draco::AttributesDecoderInterface"*, %"class.draco::AttributesDecoderInterface"** %call, align 4
  store %"class.draco::AttributesDecoderInterface"* %0, %"class.draco::AttributesDecoderInterface"** %__tmp, align 4
  %1 = load %"class.draco::AttributesDecoderInterface"*, %"class.draco::AttributesDecoderInterface"** %__p.addr, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::AttributesDecoderInterface"** @_ZNSt3__217__compressed_pairIPN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.97"* %__ptr_2) #4
  store %"class.draco::AttributesDecoderInterface"* %1, %"class.draco::AttributesDecoderInterface"** %call3, align 4
  %2 = load %"class.draco::AttributesDecoderInterface"*, %"class.draco::AttributesDecoderInterface"** %__tmp, align 4
  %tobool = icmp ne %"class.draco::AttributesDecoderInterface"* %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__ptr_4 = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %this1, i32 0, i32 0
  %call5 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.100"* @_ZNSt3__217__compressed_pairIPN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.97"* %__ptr_4) #4
  %3 = load %"class.draco::AttributesDecoderInterface"*, %"class.draco::AttributesDecoderInterface"** %__tmp, align 4
  call void @_ZNKSt3__214default_deleteIN5draco26AttributesDecoderInterfaceEEclEPS2_(%"struct.std::__2::default_delete.100"* %call5, %"class.draco::AttributesDecoderInterface"* %3) #4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::AttributesDecoderInterface"** @_ZNSt3__217__compressed_pairIPN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.97"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.97"*, align 4
  store %"class.std::__2::__compressed_pair.97"* %this, %"class.std::__2::__compressed_pair.97"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.97"*, %"class.std::__2::__compressed_pair.97"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.97"* %this1 to %"struct.std::__2::__compressed_pair_elem.98"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributesDecoderInterface"** @_ZNSt3__222__compressed_pair_elemIPN5draco26AttributesDecoderInterfaceELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.98"* %0) #4
  ret %"class.draco::AttributesDecoderInterface"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.100"* @_ZNSt3__217__compressed_pairIPN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.97"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.97"*, align 4
  store %"class.std::__2::__compressed_pair.97"* %this, %"class.std::__2::__compressed_pair.97"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.97"*, %"class.std::__2::__compressed_pair.97"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.97"* %this1 to %"struct.std::__2::__compressed_pair_elem.99"*
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.100"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco26AttributesDecoderInterfaceEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.99"* %0) #4
  ret %"struct.std::__2::default_delete.100"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__214default_deleteIN5draco26AttributesDecoderInterfaceEEclEPS2_(%"struct.std::__2::default_delete.100"* %this, %"class.draco::AttributesDecoderInterface"* %__ptr) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::default_delete.100"*, align 4
  %__ptr.addr = alloca %"class.draco::AttributesDecoderInterface"*, align 4
  store %"struct.std::__2::default_delete.100"* %this, %"struct.std::__2::default_delete.100"** %this.addr, align 4
  store %"class.draco::AttributesDecoderInterface"* %__ptr, %"class.draco::AttributesDecoderInterface"** %__ptr.addr, align 4
  %this1 = load %"struct.std::__2::default_delete.100"*, %"struct.std::__2::default_delete.100"** %this.addr, align 4
  %0 = load %"class.draco::AttributesDecoderInterface"*, %"class.draco::AttributesDecoderInterface"** %__ptr.addr, align 4
  %isnull = icmp eq %"class.draco::AttributesDecoderInterface"* %0, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %entry
  %1 = bitcast %"class.draco::AttributesDecoderInterface"* %0 to void (%"class.draco::AttributesDecoderInterface"*)***
  %vtable = load void (%"class.draco::AttributesDecoderInterface"*)**, void (%"class.draco::AttributesDecoderInterface"*)*** %1, align 4
  %vfn = getelementptr inbounds void (%"class.draco::AttributesDecoderInterface"*)*, void (%"class.draco::AttributesDecoderInterface"*)** %vtable, i64 1
  %2 = load void (%"class.draco::AttributesDecoderInterface"*)*, void (%"class.draco::AttributesDecoderInterface"*)** %vfn, align 4
  call void %2(%"class.draco::AttributesDecoderInterface"* %0) #4
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::AttributesDecoderInterface"** @_ZNSt3__222__compressed_pair_elemIPN5draco26AttributesDecoderInterfaceELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.98"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.98"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.98"* %this, %"struct.std::__2::__compressed_pair_elem.98"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.98"*, %"struct.std::__2::__compressed_pair_elem.98"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.98", %"struct.std::__2::__compressed_pair_elem.98"* %this1, i32 0, i32 0
  ret %"class.draco::AttributesDecoderInterface"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.100"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco26AttributesDecoderInterfaceEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.99"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.99"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.99"* %this, %"struct.std::__2::__compressed_pair_elem.99"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.99"*, %"struct.std::__2::__compressed_pair_elem.99"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.99"* %this1 to %"struct.std::__2::default_delete.100"*
  ret %"struct.std::__2::default_delete.100"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEE10deallocateEPS6_m(%"class.std::__2::allocator.104"* %this, %"class.std::__2::unique_ptr.96"* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.104"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.104"* %this, %"class.std::__2::allocator.104"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.96"* %__p, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.104"*, %"class.std::__2::allocator.104"** %this.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  %1 = bitcast %"class.std::__2::unique_ptr.96"* %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE6secondEv(%"class.std::__2::__compressed_pair.101"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.101"*, align 4
  store %"class.std::__2::__compressed_pair.101"* %this, %"class.std::__2::__compressed_pair.101"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.101"*, %"class.std::__2::__compressed_pair.101"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.101"* %this1 to %"struct.std::__2::__compressed_pair_elem.103"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.103"* %0) #4
  ret %"class.std::__2::allocator.104"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.103"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.103"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.103"* %this, %"struct.std::__2::__compressed_pair_elem.103"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.103"*, %"struct.std::__2::__compressed_pair_elem.103"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.103"* %this1 to %"class.std::__2::allocator.104"*
  ret %"class.std::__2::allocator.104"* %0
}

; Function Attrs: cold noreturn nounwind
declare void @llvm.trap() #3

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { cold noreturn nounwind }
attributes #4 = { nounwind }
attributes #5 = { noreturn nounwind }
attributes #6 = { builtin nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
