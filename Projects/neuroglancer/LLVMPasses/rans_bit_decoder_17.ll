; ModuleID = './draco/src/draco/compression/bit_coders/rans_bit_decoder.cc'
source_filename = "./draco/src/draco/compression/bit_coders/rans_bit_decoder.cc"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%"class.draco::RAnsBitDecoder" = type <{ %"struct.draco::AnsDecoder", i8, [3 x i8] }>
%"struct.draco::AnsDecoder" = type { i8*, i32, i32 }
%"class.draco::DecoderBuffer" = type { i8*, i64, i64, %"class.draco::DecoderBuffer::BitDecoder", i8, i16 }
%"class.draco::DecoderBuffer::BitDecoder" = type { i8*, i8*, i32 }

$_ZN5draco10AnsDecoderC2Ev = comdat any

$_ZN5draco13DecoderBuffer6DecodeIhEEbPT_ = comdat any

$_ZNK5draco13DecoderBuffer17bitstream_versionEv = comdat any

$_ZN5draco13DecoderBuffer6DecodeIjEEbPT_ = comdat any

$_ZN5draco12DecodeVarintIjEEbPT_PNS_13DecoderBufferE = comdat any

$_ZNK5draco13DecoderBuffer14remaining_sizeEv = comdat any

$_ZNK5draco13DecoderBuffer9data_headEv = comdat any

$_ZN5draco13DecoderBuffer7AdvanceEx = comdat any

$_ZN5draco13DecoderBuffer4PeekIhEEbPT_ = comdat any

$_ZN5draco13DecoderBuffer4PeekIjEEbPT_ = comdat any

@_ZN5draco14RAnsBitDecoderC1Ev = hidden unnamed_addr alias %"class.draco::RAnsBitDecoder"* (%"class.draco::RAnsBitDecoder"*), %"class.draco::RAnsBitDecoder"* (%"class.draco::RAnsBitDecoder"*)* @_ZN5draco14RAnsBitDecoderC2Ev
@_ZN5draco14RAnsBitDecoderD1Ev = hidden unnamed_addr alias %"class.draco::RAnsBitDecoder"* (%"class.draco::RAnsBitDecoder"*), %"class.draco::RAnsBitDecoder"* (%"class.draco::RAnsBitDecoder"*)* @_ZN5draco14RAnsBitDecoderD2Ev

; Function Attrs: noinline nounwind optnone
define hidden %"class.draco::RAnsBitDecoder"* @_ZN5draco14RAnsBitDecoderC2Ev(%"class.draco::RAnsBitDecoder"* returned %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %"class.draco::RAnsBitDecoder"*, align 4
  store %"class.draco::RAnsBitDecoder"* %this, %"class.draco::RAnsBitDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsBitDecoder"*, %"class.draco::RAnsBitDecoder"** %this.addr, align 4
  %ans_decoder_ = getelementptr inbounds %"class.draco::RAnsBitDecoder", %"class.draco::RAnsBitDecoder"* %this1, i32 0, i32 0
  %call = call %"struct.draco::AnsDecoder"* @_ZN5draco10AnsDecoderC2Ev(%"struct.draco::AnsDecoder"* %ans_decoder_)
  %prob_zero_ = getelementptr inbounds %"class.draco::RAnsBitDecoder", %"class.draco::RAnsBitDecoder"* %this1, i32 0, i32 1
  store i8 0, i8* %prob_zero_, align 4
  ret %"class.draco::RAnsBitDecoder"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.draco::AnsDecoder"* @_ZN5draco10AnsDecoderC2Ev(%"struct.draco::AnsDecoder"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.draco::AnsDecoder"*, align 4
  store %"struct.draco::AnsDecoder"* %this, %"struct.draco::AnsDecoder"** %this.addr, align 4
  %this1 = load %"struct.draco::AnsDecoder"*, %"struct.draco::AnsDecoder"** %this.addr, align 4
  %buf = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %this1, i32 0, i32 0
  store i8* null, i8** %buf, align 4
  %buf_offset = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %this1, i32 0, i32 1
  store i32 0, i32* %buf_offset, align 4
  %state = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %this1, i32 0, i32 2
  store i32 0, i32* %state, align 4
  ret %"struct.draco::AnsDecoder"* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden %"class.draco::RAnsBitDecoder"* @_ZN5draco14RAnsBitDecoderD2Ev(%"class.draco::RAnsBitDecoder"* returned %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %"class.draco::RAnsBitDecoder"*, align 4
  store %"class.draco::RAnsBitDecoder"* %this, %"class.draco::RAnsBitDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsBitDecoder"*, %"class.draco::RAnsBitDecoder"** %this.addr, align 4
  call void @_ZN5draco14RAnsBitDecoder5ClearEv(%"class.draco::RAnsBitDecoder"* %this1)
  ret %"class.draco::RAnsBitDecoder"* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN5draco14RAnsBitDecoder5ClearEv(%"class.draco::RAnsBitDecoder"* %this) #0 {
entry:
  %this.addr = alloca %"class.draco::RAnsBitDecoder"*, align 4
  store %"class.draco::RAnsBitDecoder"* %this, %"class.draco::RAnsBitDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsBitDecoder"*, %"class.draco::RAnsBitDecoder"** %this.addr, align 4
  %ans_decoder_ = getelementptr inbounds %"class.draco::RAnsBitDecoder", %"class.draco::RAnsBitDecoder"* %this1, i32 0, i32 0
  %call = call i32 @_ZN5dracoL12ans_read_endEPNS_10AnsDecoderE(%"struct.draco::AnsDecoder"* %ans_decoder_)
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden zeroext i1 @_ZN5draco14RAnsBitDecoder13StartDecodingEPNS_13DecoderBufferE(%"class.draco::RAnsBitDecoder"* %this, %"class.draco::DecoderBuffer"* %source_buffer) #0 {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::RAnsBitDecoder"*, align 4
  %source_buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %size_in_bytes = alloca i32, align 4
  store %"class.draco::RAnsBitDecoder"* %this, %"class.draco::RAnsBitDecoder"** %this.addr, align 4
  store %"class.draco::DecoderBuffer"* %source_buffer, %"class.draco::DecoderBuffer"** %source_buffer.addr, align 4
  %this1 = load %"class.draco::RAnsBitDecoder"*, %"class.draco::RAnsBitDecoder"** %this.addr, align 4
  call void @_ZN5draco14RAnsBitDecoder5ClearEv(%"class.draco::RAnsBitDecoder"* %this1)
  %0 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %source_buffer.addr, align 4
  %prob_zero_ = getelementptr inbounds %"class.draco::RAnsBitDecoder", %"class.draco::RAnsBitDecoder"* %this1, i32 0, i32 1
  %call = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIhEEbPT_(%"class.draco::DecoderBuffer"* %0, i8* %prob_zero_)
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %source_buffer.addr, align 4
  %call2 = call zeroext i16 @_ZNK5draco13DecoderBuffer17bitstream_versionEv(%"class.draco::DecoderBuffer"* %1)
  %conv = zext i16 %call2 to i32
  %cmp = icmp slt i32 %conv, 514
  br i1 %cmp, label %if.then3, label %if.else

if.then3:                                         ; preds = %if.end
  %2 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %source_buffer.addr, align 4
  %call4 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIjEEbPT_(%"class.draco::DecoderBuffer"* %2, i32* %size_in_bytes)
  br i1 %call4, label %if.end6, label %if.then5

if.then5:                                         ; preds = %if.then3
  store i1 false, i1* %retval, align 1
  br label %return

if.end6:                                          ; preds = %if.then3
  br label %if.end10

if.else:                                          ; preds = %if.end
  %3 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %source_buffer.addr, align 4
  %call7 = call zeroext i1 @_ZN5draco12DecodeVarintIjEEbPT_PNS_13DecoderBufferE(i32* %size_in_bytes, %"class.draco::DecoderBuffer"* %3)
  br i1 %call7, label %if.end9, label %if.then8

if.then8:                                         ; preds = %if.else
  store i1 false, i1* %retval, align 1
  br label %return

if.end9:                                          ; preds = %if.else
  br label %if.end10

if.end10:                                         ; preds = %if.end9, %if.end6
  %4 = load i32, i32* %size_in_bytes, align 4
  %conv11 = zext i32 %4 to i64
  %5 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %source_buffer.addr, align 4
  %call12 = call i64 @_ZNK5draco13DecoderBuffer14remaining_sizeEv(%"class.draco::DecoderBuffer"* %5)
  %cmp13 = icmp sgt i64 %conv11, %call12
  br i1 %cmp13, label %if.then14, label %if.end15

if.then14:                                        ; preds = %if.end10
  store i1 false, i1* %retval, align 1
  br label %return

if.end15:                                         ; preds = %if.end10
  %ans_decoder_ = getelementptr inbounds %"class.draco::RAnsBitDecoder", %"class.draco::RAnsBitDecoder"* %this1, i32 0, i32 0
  %6 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %source_buffer.addr, align 4
  %call16 = call i8* @_ZNK5draco13DecoderBuffer9data_headEv(%"class.draco::DecoderBuffer"* %6)
  %7 = load i32, i32* %size_in_bytes, align 4
  %call17 = call i32 @_ZN5dracoL13ans_read_initEPNS_10AnsDecoderEPKhi(%"struct.draco::AnsDecoder"* %ans_decoder_, i8* %call16, i32 %7)
  %cmp18 = icmp ne i32 %call17, 0
  br i1 %cmp18, label %if.then19, label %if.end20

if.then19:                                        ; preds = %if.end15
  store i1 false, i1* %retval, align 1
  br label %return

if.end20:                                         ; preds = %if.end15
  %8 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %source_buffer.addr, align 4
  %9 = load i32, i32* %size_in_bytes, align 4
  %conv21 = zext i32 %9 to i64
  call void @_ZN5draco13DecoderBuffer7AdvanceEx(%"class.draco::DecoderBuffer"* %8, i64 %conv21)
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end20, %if.then19, %if.then14, %if.then8, %if.then5, %if.then
  %10 = load i1, i1* %retval, align 1
  ret i1 %10
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIhEEbPT_(%"class.draco::DecoderBuffer"* %this, i8* %out_val) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %out_val.addr = alloca i8*, align 4
  store %"class.draco::DecoderBuffer"* %this, %"class.draco::DecoderBuffer"** %this.addr, align 4
  store i8* %out_val, i8** %out_val.addr, align 4
  %this1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %this.addr, align 4
  %0 = load i8*, i8** %out_val.addr, align 4
  %call = call zeroext i1 @_ZN5draco13DecoderBuffer4PeekIhEEbPT_(%"class.draco::DecoderBuffer"* %this1, i8* %0)
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %pos_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %1 = load i64, i64* %pos_, align 8
  %add = add nsw i64 %1, 1
  store i64 %add, i64* %pos_, align 8
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %2 = load i1, i1* %retval, align 1
  ret i1 %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i16 @_ZNK5draco13DecoderBuffer17bitstream_versionEv(%"class.draco::DecoderBuffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  store %"class.draco::DecoderBuffer"* %this, %"class.draco::DecoderBuffer"** %this.addr, align 4
  %this1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %this.addr, align 4
  %bitstream_version_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 5
  %0 = load i16, i16* %bitstream_version_, align 2
  ret i16 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIjEEbPT_(%"class.draco::DecoderBuffer"* %this, i32* %out_val) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %out_val.addr = alloca i32*, align 4
  store %"class.draco::DecoderBuffer"* %this, %"class.draco::DecoderBuffer"** %this.addr, align 4
  store i32* %out_val, i32** %out_val.addr, align 4
  %this1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %this.addr, align 4
  %0 = load i32*, i32** %out_val.addr, align 4
  %call = call zeroext i1 @_ZN5draco13DecoderBuffer4PeekIjEEbPT_(%"class.draco::DecoderBuffer"* %this1, i32* %0)
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %pos_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %1 = load i64, i64* %pos_, align 8
  %add = add nsw i64 %1, 4
  store i64 %add, i64* %pos_, align 8
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %2 = load i1, i1* %retval, align 1
  ret i1 %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco12DecodeVarintIjEEbPT_PNS_13DecoderBufferE(i32* %out_val, %"class.draco::DecoderBuffer"* %buffer) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %out_val.addr = alloca i32*, align 4
  %buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %in = alloca i8, align 1
  store i32* %out_val, i32** %out_val.addr, align 4
  store %"class.draco::DecoderBuffer"* %buffer, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %0 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIhEEbPT_(%"class.draco::DecoderBuffer"* %0, i8* %in)
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %1 = load i8, i8* %in, align 1
  %conv = zext i8 %1 to i32
  %and = and i32 %conv, 128
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then1, label %if.else

if.then1:                                         ; preds = %if.end
  %2 = load i32*, i32** %out_val.addr, align 4
  %3 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %call2 = call zeroext i1 @_ZN5draco12DecodeVarintIjEEbPT_PNS_13DecoderBufferE(i32* %2, %"class.draco::DecoderBuffer"* %3)
  br i1 %call2, label %if.end4, label %if.then3

if.then3:                                         ; preds = %if.then1
  store i1 false, i1* %retval, align 1
  br label %return

if.end4:                                          ; preds = %if.then1
  %4 = load i32*, i32** %out_val.addr, align 4
  %5 = load i32, i32* %4, align 4
  %shl = shl i32 %5, 7
  store i32 %shl, i32* %4, align 4
  %6 = load i8, i8* %in, align 1
  %conv5 = zext i8 %6 to i32
  %and6 = and i32 %conv5, 127
  %7 = load i32*, i32** %out_val.addr, align 4
  %8 = load i32, i32* %7, align 4
  %or = or i32 %8, %and6
  store i32 %or, i32* %7, align 4
  br label %if.end8

if.else:                                          ; preds = %if.end
  %9 = load i8, i8* %in, align 1
  %conv7 = zext i8 %9 to i32
  %10 = load i32*, i32** %out_val.addr, align 4
  store i32 %conv7, i32* %10, align 4
  br label %if.end8

if.end8:                                          ; preds = %if.else, %if.end4
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end8, %if.then3, %if.then
  %11 = load i1, i1* %retval, align 1
  ret i1 %11
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i64 @_ZNK5draco13DecoderBuffer14remaining_sizeEv(%"class.draco::DecoderBuffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  store %"class.draco::DecoderBuffer"* %this, %"class.draco::DecoderBuffer"** %this.addr, align 4
  %this1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %this.addr, align 4
  %data_size_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 1
  %0 = load i64, i64* %data_size_, align 8
  %pos_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %1 = load i64, i64* %pos_, align 8
  %sub = sub nsw i64 %0, %1
  ret i64 %sub
}

; Function Attrs: noinline nounwind optnone
define internal i32 @_ZN5dracoL13ans_read_initEPNS_10AnsDecoderEPKhi(%"struct.draco::AnsDecoder"* %ans, i8* %buf, i32 %offset) #0 {
entry:
  %retval = alloca i32, align 4
  %ans.addr = alloca %"struct.draco::AnsDecoder"*, align 4
  %buf.addr = alloca i8*, align 4
  %offset.addr = alloca i32, align 4
  %x = alloca i32, align 4
  store %"struct.draco::AnsDecoder"* %ans, %"struct.draco::AnsDecoder"** %ans.addr, align 4
  store i8* %buf, i8** %buf.addr, align 4
  store i32 %offset, i32* %offset.addr, align 4
  %0 = load i32, i32* %offset.addr, align 4
  %cmp = icmp slt i32 %0, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load i8*, i8** %buf.addr, align 4
  %2 = load %"struct.draco::AnsDecoder"*, %"struct.draco::AnsDecoder"** %ans.addr, align 4
  %buf1 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %2, i32 0, i32 0
  store i8* %1, i8** %buf1, align 4
  %3 = load i8*, i8** %buf.addr, align 4
  %4 = load i32, i32* %offset.addr, align 4
  %sub = sub nsw i32 %4, 1
  %arrayidx = getelementptr inbounds i8, i8* %3, i32 %sub
  %5 = load i8, i8* %arrayidx, align 1
  %conv = zext i8 %5 to i32
  %shr = ashr i32 %conv, 6
  store i32 %shr, i32* %x, align 4
  %6 = load i32, i32* %x, align 4
  %cmp2 = icmp eq i32 %6, 0
  br i1 %cmp2, label %if.then3, label %if.else

if.then3:                                         ; preds = %if.end
  %7 = load i32, i32* %offset.addr, align 4
  %sub4 = sub nsw i32 %7, 1
  %8 = load %"struct.draco::AnsDecoder"*, %"struct.draco::AnsDecoder"** %ans.addr, align 4
  %buf_offset = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %8, i32 0, i32 1
  store i32 %sub4, i32* %buf_offset, align 4
  %9 = load i8*, i8** %buf.addr, align 4
  %10 = load i32, i32* %offset.addr, align 4
  %sub5 = sub nsw i32 %10, 1
  %arrayidx6 = getelementptr inbounds i8, i8* %9, i32 %sub5
  %11 = load i8, i8* %arrayidx6, align 1
  %conv7 = zext i8 %11 to i32
  %and = and i32 %conv7, 63
  %12 = load %"struct.draco::AnsDecoder"*, %"struct.draco::AnsDecoder"** %ans.addr, align 4
  %state = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %12, i32 0, i32 2
  store i32 %and, i32* %state, align 4
  br label %if.end34

if.else:                                          ; preds = %if.end
  %13 = load i32, i32* %x, align 4
  %cmp8 = icmp eq i32 %13, 1
  br i1 %cmp8, label %if.then9, label %if.else18

if.then9:                                         ; preds = %if.else
  %14 = load i32, i32* %offset.addr, align 4
  %cmp10 = icmp slt i32 %14, 2
  br i1 %cmp10, label %if.then11, label %if.end12

if.then11:                                        ; preds = %if.then9
  store i32 1, i32* %retval, align 4
  br label %return

if.end12:                                         ; preds = %if.then9
  %15 = load i32, i32* %offset.addr, align 4
  %sub13 = sub nsw i32 %15, 2
  %16 = load %"struct.draco::AnsDecoder"*, %"struct.draco::AnsDecoder"** %ans.addr, align 4
  %buf_offset14 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %16, i32 0, i32 1
  store i32 %sub13, i32* %buf_offset14, align 4
  %17 = load i8*, i8** %buf.addr, align 4
  %18 = load i32, i32* %offset.addr, align 4
  %add.ptr = getelementptr inbounds i8, i8* %17, i32 %18
  %add.ptr15 = getelementptr inbounds i8, i8* %add.ptr, i32 -2
  %call = call i32 @_ZN5dracoL12mem_get_le16EPKv(i8* %add.ptr15)
  %and16 = and i32 %call, 16383
  %19 = load %"struct.draco::AnsDecoder"*, %"struct.draco::AnsDecoder"** %ans.addr, align 4
  %state17 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %19, i32 0, i32 2
  store i32 %and16, i32* %state17, align 4
  br label %if.end33

if.else18:                                        ; preds = %if.else
  %20 = load i32, i32* %x, align 4
  %cmp19 = icmp eq i32 %20, 2
  br i1 %cmp19, label %if.then20, label %if.else31

if.then20:                                        ; preds = %if.else18
  %21 = load i32, i32* %offset.addr, align 4
  %cmp21 = icmp slt i32 %21, 3
  br i1 %cmp21, label %if.then22, label %if.end23

if.then22:                                        ; preds = %if.then20
  store i32 1, i32* %retval, align 4
  br label %return

if.end23:                                         ; preds = %if.then20
  %22 = load i32, i32* %offset.addr, align 4
  %sub24 = sub nsw i32 %22, 3
  %23 = load %"struct.draco::AnsDecoder"*, %"struct.draco::AnsDecoder"** %ans.addr, align 4
  %buf_offset25 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %23, i32 0, i32 1
  store i32 %sub24, i32* %buf_offset25, align 4
  %24 = load i8*, i8** %buf.addr, align 4
  %25 = load i32, i32* %offset.addr, align 4
  %add.ptr26 = getelementptr inbounds i8, i8* %24, i32 %25
  %add.ptr27 = getelementptr inbounds i8, i8* %add.ptr26, i32 -3
  %call28 = call i32 @_ZN5dracoL12mem_get_le24EPKv(i8* %add.ptr27)
  %and29 = and i32 %call28, 4194303
  %26 = load %"struct.draco::AnsDecoder"*, %"struct.draco::AnsDecoder"** %ans.addr, align 4
  %state30 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %26, i32 0, i32 2
  store i32 %and29, i32* %state30, align 4
  br label %if.end32

if.else31:                                        ; preds = %if.else18
  store i32 1, i32* %retval, align 4
  br label %return

if.end32:                                         ; preds = %if.end23
  br label %if.end33

if.end33:                                         ; preds = %if.end32, %if.end12
  br label %if.end34

if.end34:                                         ; preds = %if.end33, %if.then3
  %27 = load %"struct.draco::AnsDecoder"*, %"struct.draco::AnsDecoder"** %ans.addr, align 4
  %state35 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %27, i32 0, i32 2
  %28 = load i32, i32* %state35, align 4
  %add = add i32 %28, 4096
  store i32 %add, i32* %state35, align 4
  %29 = load %"struct.draco::AnsDecoder"*, %"struct.draco::AnsDecoder"** %ans.addr, align 4
  %state36 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %29, i32 0, i32 2
  %30 = load i32, i32* %state36, align 4
  %cmp37 = icmp uge i32 %30, 1048576
  br i1 %cmp37, label %if.then38, label %if.end39

if.then38:                                        ; preds = %if.end34
  store i32 1, i32* %retval, align 4
  br label %return

if.end39:                                         ; preds = %if.end34
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end39, %if.then38, %if.else31, %if.then22, %if.then11, %if.then
  %31 = load i32, i32* %retval, align 4
  ret i32 %31
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNK5draco13DecoderBuffer9data_headEv(%"class.draco::DecoderBuffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  store %"class.draco::DecoderBuffer"* %this, %"class.draco::DecoderBuffer"** %this.addr, align 4
  %this1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %this.addr, align 4
  %data_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 0
  %0 = load i8*, i8** %data_, align 8
  %pos_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %1 = load i64, i64* %pos_, align 8
  %idx.ext = trunc i64 %1 to i32
  %add.ptr = getelementptr inbounds i8, i8* %0, i32 %idx.ext
  ret i8* %add.ptr
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco13DecoderBuffer7AdvanceEx(%"class.draco::DecoderBuffer"* %this, i64 %bytes) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %bytes.addr = alloca i64, align 8
  store %"class.draco::DecoderBuffer"* %this, %"class.draco::DecoderBuffer"** %this.addr, align 4
  store i64 %bytes, i64* %bytes.addr, align 8
  %this1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %this.addr, align 4
  %0 = load i64, i64* %bytes.addr, align 8
  %pos_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %1 = load i64, i64* %pos_, align 8
  %add = add nsw i64 %1, %0
  store i64 %add, i64* %pos_, align 8
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden zeroext i1 @_ZN5draco14RAnsBitDecoder13DecodeNextBitEv(%"class.draco::RAnsBitDecoder"* %this) #0 {
entry:
  %this.addr = alloca %"class.draco::RAnsBitDecoder"*, align 4
  %bit = alloca i8, align 1
  store %"class.draco::RAnsBitDecoder"* %this, %"class.draco::RAnsBitDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::RAnsBitDecoder"*, %"class.draco::RAnsBitDecoder"** %this.addr, align 4
  %ans_decoder_ = getelementptr inbounds %"class.draco::RAnsBitDecoder", %"class.draco::RAnsBitDecoder"* %this1, i32 0, i32 0
  %prob_zero_ = getelementptr inbounds %"class.draco::RAnsBitDecoder", %"class.draco::RAnsBitDecoder"* %this1, i32 0, i32 1
  %0 = load i8, i8* %prob_zero_, align 4
  %call = call i32 @_ZN5dracoL14rabs_desc_readEPNS_10AnsDecoderEh(%"struct.draco::AnsDecoder"* %ans_decoder_, i8 zeroext %0)
  %conv = trunc i32 %call to i8
  store i8 %conv, i8* %bit, align 1
  %1 = load i8, i8* %bit, align 1
  %conv2 = zext i8 %1 to i32
  %cmp = icmp sgt i32 %conv2, 0
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define internal i32 @_ZN5dracoL14rabs_desc_readEPNS_10AnsDecoderEh(%"struct.draco::AnsDecoder"* %ans, i8 zeroext %p0) #0 {
entry:
  %ans.addr = alloca %"struct.draco::AnsDecoder"*, align 4
  %p0.addr = alloca i8, align 1
  %val = alloca i32, align 4
  %quot = alloca i32, align 4
  %rem = alloca i32, align 4
  %x = alloca i32, align 4
  %xn = alloca i32, align 4
  %p = alloca i8, align 1
  store %"struct.draco::AnsDecoder"* %ans, %"struct.draco::AnsDecoder"** %ans.addr, align 4
  store i8 %p0, i8* %p0.addr, align 1
  %0 = load i8, i8* %p0.addr, align 1
  %conv = zext i8 %0 to i32
  %sub = sub i32 256, %conv
  %conv1 = trunc i32 %sub to i8
  store i8 %conv1, i8* %p, align 1
  %1 = load %"struct.draco::AnsDecoder"*, %"struct.draco::AnsDecoder"** %ans.addr, align 4
  %state = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %1, i32 0, i32 2
  %2 = load i32, i32* %state, align 4
  %cmp = icmp ult i32 %2, 4096
  br i1 %cmp, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %3 = load %"struct.draco::AnsDecoder"*, %"struct.draco::AnsDecoder"** %ans.addr, align 4
  %buf_offset = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %3, i32 0, i32 1
  %4 = load i32, i32* %buf_offset, align 4
  %cmp2 = icmp sgt i32 %4, 0
  br i1 %cmp2, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %5 = load %"struct.draco::AnsDecoder"*, %"struct.draco::AnsDecoder"** %ans.addr, align 4
  %state3 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %5, i32 0, i32 2
  %6 = load i32, i32* %state3, align 4
  %mul = mul i32 %6, 256
  %7 = load %"struct.draco::AnsDecoder"*, %"struct.draco::AnsDecoder"** %ans.addr, align 4
  %buf = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %7, i32 0, i32 0
  %8 = load i8*, i8** %buf, align 4
  %9 = load %"struct.draco::AnsDecoder"*, %"struct.draco::AnsDecoder"** %ans.addr, align 4
  %buf_offset4 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %9, i32 0, i32 1
  %10 = load i32, i32* %buf_offset4, align 4
  %dec = add nsw i32 %10, -1
  store i32 %dec, i32* %buf_offset4, align 4
  %arrayidx = getelementptr inbounds i8, i8* %8, i32 %dec
  %11 = load i8, i8* %arrayidx, align 1
  %conv5 = zext i8 %11 to i32
  %add = add i32 %mul, %conv5
  %12 = load %"struct.draco::AnsDecoder"*, %"struct.draco::AnsDecoder"** %ans.addr, align 4
  %state6 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %12, i32 0, i32 2
  store i32 %add, i32* %state6, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true, %entry
  %13 = load %"struct.draco::AnsDecoder"*, %"struct.draco::AnsDecoder"** %ans.addr, align 4
  %state7 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %13, i32 0, i32 2
  %14 = load i32, i32* %state7, align 4
  store i32 %14, i32* %x, align 4
  %15 = load i32, i32* %x, align 4
  %div = udiv i32 %15, 256
  store i32 %div, i32* %quot, align 4
  %16 = load i32, i32* %x, align 4
  %rem8 = urem i32 %16, 256
  store i32 %rem8, i32* %rem, align 4
  %17 = load i32, i32* %quot, align 4
  %18 = load i8, i8* %p, align 1
  %conv9 = zext i8 %18 to i32
  %mul10 = mul i32 %17, %conv9
  store i32 %mul10, i32* %xn, align 4
  %19 = load i32, i32* %rem, align 4
  %20 = load i8, i8* %p, align 1
  %conv11 = zext i8 %20 to i32
  %cmp12 = icmp ult i32 %19, %conv11
  %conv13 = zext i1 %cmp12 to i32
  store i32 %conv13, i32* %val, align 4
  %21 = load i32, i32* %val, align 4
  %tobool = icmp ne i32 %21, 0
  br i1 %tobool, label %if.then14, label %if.else

if.then14:                                        ; preds = %if.end
  %22 = load i32, i32* %xn, align 4
  %23 = load i32, i32* %rem, align 4
  %add15 = add i32 %22, %23
  %24 = load %"struct.draco::AnsDecoder"*, %"struct.draco::AnsDecoder"** %ans.addr, align 4
  %state16 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %24, i32 0, i32 2
  store i32 %add15, i32* %state16, align 4
  br label %if.end21

if.else:                                          ; preds = %if.end
  %25 = load i32, i32* %x, align 4
  %26 = load i32, i32* %xn, align 4
  %sub17 = sub i32 %25, %26
  %27 = load i8, i8* %p, align 1
  %conv18 = zext i8 %27 to i32
  %sub19 = sub i32 %sub17, %conv18
  %28 = load %"struct.draco::AnsDecoder"*, %"struct.draco::AnsDecoder"** %ans.addr, align 4
  %state20 = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %28, i32 0, i32 2
  store i32 %sub19, i32* %state20, align 4
  br label %if.end21

if.end21:                                         ; preds = %if.else, %if.then14
  %29 = load i32, i32* %val, align 4
  ret i32 %29
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN5draco14RAnsBitDecoder28DecodeLeastSignificantBits32EiPj(%"class.draco::RAnsBitDecoder"* %this, i32 %nbits, i32* %value) #0 {
entry:
  %this.addr = alloca %"class.draco::RAnsBitDecoder"*, align 4
  %nbits.addr = alloca i32, align 4
  %value.addr = alloca i32*, align 4
  %result = alloca i32, align 4
  store %"class.draco::RAnsBitDecoder"* %this, %"class.draco::RAnsBitDecoder"** %this.addr, align 4
  store i32 %nbits, i32* %nbits.addr, align 4
  store i32* %value, i32** %value.addr, align 4
  %this1 = load %"class.draco::RAnsBitDecoder"*, %"class.draco::RAnsBitDecoder"** %this.addr, align 4
  store i32 0, i32* %result, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %0 = load i32, i32* %nbits.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %1 = load i32, i32* %result, align 4
  %shl = shl i32 %1, 1
  %call = call zeroext i1 @_ZN5draco14RAnsBitDecoder13DecodeNextBitEv(%"class.draco::RAnsBitDecoder"* %this1)
  %conv = zext i1 %call to i32
  %add = add i32 %shl, %conv
  store i32 %add, i32* %result, align 4
  %2 = load i32, i32* %nbits.addr, align 4
  %dec = add nsw i32 %2, -1
  store i32 %dec, i32* %nbits.addr, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %3 = load i32, i32* %result, align 4
  %4 = load i32*, i32** %value.addr, align 4
  store i32 %3, i32* %4, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal i32 @_ZN5dracoL12ans_read_endEPNS_10AnsDecoderE(%"struct.draco::AnsDecoder"* %ans) #0 {
entry:
  %ans.addr = alloca %"struct.draco::AnsDecoder"*, align 4
  store %"struct.draco::AnsDecoder"* %ans, %"struct.draco::AnsDecoder"** %ans.addr, align 4
  %0 = load %"struct.draco::AnsDecoder"*, %"struct.draco::AnsDecoder"** %ans.addr, align 4
  %state = getelementptr inbounds %"struct.draco::AnsDecoder", %"struct.draco::AnsDecoder"* %0, i32 0, i32 2
  %1 = load i32, i32* %state, align 4
  %cmp = icmp eq i32 %1, 4096
  %conv = zext i1 %cmp to i32
  ret i32 %conv
}

; Function Attrs: noinline nounwind optnone
define internal i32 @_ZN5dracoL12mem_get_le16EPKv(i8* %vmem) #0 {
entry:
  %vmem.addr = alloca i8*, align 4
  %val = alloca i32, align 4
  %mem = alloca i8*, align 4
  store i8* %vmem, i8** %vmem.addr, align 4
  %0 = load i8*, i8** %vmem.addr, align 4
  store i8* %0, i8** %mem, align 4
  %1 = load i8*, i8** %mem, align 4
  %arrayidx = getelementptr inbounds i8, i8* %1, i32 1
  %2 = load i8, i8* %arrayidx, align 1
  %conv = zext i8 %2 to i32
  %shl = shl i32 %conv, 8
  store i32 %shl, i32* %val, align 4
  %3 = load i8*, i8** %mem, align 4
  %arrayidx1 = getelementptr inbounds i8, i8* %3, i32 0
  %4 = load i8, i8* %arrayidx1, align 1
  %conv2 = zext i8 %4 to i32
  %5 = load i32, i32* %val, align 4
  %or = or i32 %5, %conv2
  store i32 %or, i32* %val, align 4
  %6 = load i32, i32* %val, align 4
  ret i32 %6
}

; Function Attrs: noinline nounwind optnone
define internal i32 @_ZN5dracoL12mem_get_le24EPKv(i8* %vmem) #0 {
entry:
  %vmem.addr = alloca i8*, align 4
  %val = alloca i32, align 4
  %mem = alloca i8*, align 4
  store i8* %vmem, i8** %vmem.addr, align 4
  %0 = load i8*, i8** %vmem.addr, align 4
  store i8* %0, i8** %mem, align 4
  %1 = load i8*, i8** %mem, align 4
  %arrayidx = getelementptr inbounds i8, i8* %1, i32 2
  %2 = load i8, i8* %arrayidx, align 1
  %conv = zext i8 %2 to i32
  %shl = shl i32 %conv, 16
  store i32 %shl, i32* %val, align 4
  %3 = load i8*, i8** %mem, align 4
  %arrayidx1 = getelementptr inbounds i8, i8* %3, i32 1
  %4 = load i8, i8* %arrayidx1, align 1
  %conv2 = zext i8 %4 to i32
  %shl3 = shl i32 %conv2, 8
  %5 = load i32, i32* %val, align 4
  %or = or i32 %5, %shl3
  store i32 %or, i32* %val, align 4
  %6 = load i8*, i8** %mem, align 4
  %arrayidx4 = getelementptr inbounds i8, i8* %6, i32 0
  %7 = load i8, i8* %arrayidx4, align 1
  %conv5 = zext i8 %7 to i32
  %8 = load i32, i32* %val, align 4
  %or6 = or i32 %8, %conv5
  store i32 %or6, i32* %val, align 4
  %9 = load i32, i32* %val, align 4
  ret i32 %9
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco13DecoderBuffer4PeekIhEEbPT_(%"class.draco::DecoderBuffer"* %this, i8* %out_val) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %out_val.addr = alloca i8*, align 4
  %size_to_decode = alloca i32, align 4
  store %"class.draco::DecoderBuffer"* %this, %"class.draco::DecoderBuffer"** %this.addr, align 4
  store i8* %out_val, i8** %out_val.addr, align 4
  %this1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %this.addr, align 4
  store i32 1, i32* %size_to_decode, align 4
  %data_size_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 1
  %0 = load i64, i64* %data_size_, align 8
  %pos_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %1 = load i64, i64* %pos_, align 8
  %add = add nsw i64 %1, 1
  %cmp = icmp slt i64 %0, %add
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %2 = load i8*, i8** %out_val.addr, align 4
  %data_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 0
  %3 = load i8*, i8** %data_, align 8
  %pos_2 = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %4 = load i64, i64* %pos_2, align 8
  %idx.ext = trunc i64 %4 to i32
  %add.ptr = getelementptr inbounds i8, i8* %3, i32 %idx.ext
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %2, i8* align 1 %add.ptr, i32 1, i1 false)
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %5 = load i1, i1* %retval, align 1
  ret i1 %5
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco13DecoderBuffer4PeekIjEEbPT_(%"class.draco::DecoderBuffer"* %this, i32* %out_val) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %out_val.addr = alloca i32*, align 4
  %size_to_decode = alloca i32, align 4
  store %"class.draco::DecoderBuffer"* %this, %"class.draco::DecoderBuffer"** %this.addr, align 4
  store i32* %out_val, i32** %out_val.addr, align 4
  %this1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %this.addr, align 4
  store i32 4, i32* %size_to_decode, align 4
  %data_size_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 1
  %0 = load i64, i64* %data_size_, align 8
  %pos_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %1 = load i64, i64* %pos_, align 8
  %add = add nsw i64 %1, 4
  %cmp = icmp slt i64 %0, %add
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %2 = load i32*, i32** %out_val.addr, align 4
  %3 = bitcast i32* %2 to i8*
  %data_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 0
  %4 = load i8*, i8** %data_, align 8
  %pos_2 = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %5 = load i64, i64* %pos_2, align 8
  %idx.ext = trunc i64 %5 to i32
  %add.ptr = getelementptr inbounds i8, i8* %4, i32 %idx.ext
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 1 %add.ptr, i32 4, i1 false)
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %6 = load i1, i1* %retval, align 1
  ret i1 %6
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
