; ModuleID = './draco/src/draco/point_cloud/point_cloud_builder.cc'
source_filename = "./draco/src/draco/point_cloud/point_cloud_builder.cc"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%"class.draco::PointCloudBuilder" = type { %"class.std::__2::unique_ptr" }
%"class.std::__2::unique_ptr" = type { %"class.std::__2::__compressed_pair" }
%"class.std::__2::__compressed_pair" = type { %"struct.std::__2::__compressed_pair_elem" }
%"struct.std::__2::__compressed_pair_elem" = type { %"class.draco::PointCloud"* }
%"class.draco::PointCloud" = type { i32 (...)**, %"class.std::__2::unique_ptr.0", %"class.std::__2::vector.54", [5 x %"class.std::__2::vector.90"], i32 }
%"class.std::__2::unique_ptr.0" = type { %"class.std::__2::__compressed_pair.1" }
%"class.std::__2::__compressed_pair.1" = type { %"struct.std::__2::__compressed_pair_elem.2" }
%"struct.std::__2::__compressed_pair_elem.2" = type { %"class.draco::GeometryMetadata"* }
%"class.draco::GeometryMetadata" = type { %"class.draco::Metadata", %"class.std::__2::vector" }
%"class.draco::Metadata" = type { %"class.std::__2::unordered_map", %"class.std::__2::unordered_map.20" }
%"class.std::__2::unordered_map" = type { %"class.std::__2::__hash_table" }
%"class.std::__2::__hash_table" = type { %"class.std::__2::unique_ptr.3", %"class.std::__2::__compressed_pair.10", %"class.std::__2::__compressed_pair.15", %"class.std::__2::__compressed_pair.17" }
%"class.std::__2::unique_ptr.3" = type { %"class.std::__2::__compressed_pair.4" }
%"class.std::__2::__compressed_pair.4" = type { %"struct.std::__2::__compressed_pair_elem.5", %"struct.std::__2::__compressed_pair_elem.6" }
%"struct.std::__2::__compressed_pair_elem.5" = type { %"struct.std::__2::__hash_node_base"** }
%"struct.std::__2::__hash_node_base" = type { %"struct.std::__2::__hash_node_base"* }
%"struct.std::__2::__compressed_pair_elem.6" = type { %"class.std::__2::__bucket_list_deallocator" }
%"class.std::__2::__bucket_list_deallocator" = type { %"class.std::__2::__compressed_pair.7" }
%"class.std::__2::__compressed_pair.7" = type { %"struct.std::__2::__compressed_pair_elem.8" }
%"struct.std::__2::__compressed_pair_elem.8" = type { i32 }
%"class.std::__2::__compressed_pair.10" = type { %"struct.std::__2::__compressed_pair_elem.11" }
%"struct.std::__2::__compressed_pair_elem.11" = type { %"struct.std::__2::__hash_node_base" }
%"class.std::__2::__compressed_pair.15" = type { %"struct.std::__2::__compressed_pair_elem.8" }
%"class.std::__2::__compressed_pair.17" = type { %"struct.std::__2::__compressed_pair_elem.18" }
%"struct.std::__2::__compressed_pair_elem.18" = type { float }
%"class.std::__2::unordered_map.20" = type { %"class.std::__2::__hash_table.21" }
%"class.std::__2::__hash_table.21" = type { %"class.std::__2::unique_ptr.22", %"class.std::__2::__compressed_pair.32", %"class.std::__2::__compressed_pair.37", %"class.std::__2::__compressed_pair.40" }
%"class.std::__2::unique_ptr.22" = type { %"class.std::__2::__compressed_pair.23" }
%"class.std::__2::__compressed_pair.23" = type { %"struct.std::__2::__compressed_pair_elem.24", %"struct.std::__2::__compressed_pair_elem.26" }
%"struct.std::__2::__compressed_pair_elem.24" = type { %"struct.std::__2::__hash_node_base.25"** }
%"struct.std::__2::__hash_node_base.25" = type { %"struct.std::__2::__hash_node_base.25"* }
%"struct.std::__2::__compressed_pair_elem.26" = type { %"class.std::__2::__bucket_list_deallocator.27" }
%"class.std::__2::__bucket_list_deallocator.27" = type { %"class.std::__2::__compressed_pair.28" }
%"class.std::__2::__compressed_pair.28" = type { %"struct.std::__2::__compressed_pair_elem.8" }
%"class.std::__2::__compressed_pair.32" = type { %"struct.std::__2::__compressed_pair_elem.33" }
%"struct.std::__2::__compressed_pair_elem.33" = type { %"struct.std::__2::__hash_node_base.25" }
%"class.std::__2::__compressed_pair.37" = type { %"struct.std::__2::__compressed_pair_elem.8" }
%"class.std::__2::__compressed_pair.40" = type { %"struct.std::__2::__compressed_pair_elem.18" }
%"class.std::__2::vector" = type { %"class.std::__2::__vector_base" }
%"class.std::__2::__vector_base" = type { %"class.std::__2::unique_ptr.43"*, %"class.std::__2::unique_ptr.43"*, %"class.std::__2::__compressed_pair.47" }
%"class.std::__2::unique_ptr.43" = type { %"class.std::__2::__compressed_pair.44" }
%"class.std::__2::__compressed_pair.44" = type { %"struct.std::__2::__compressed_pair_elem.45" }
%"struct.std::__2::__compressed_pair_elem.45" = type { %"class.draco::AttributeMetadata"* }
%"class.draco::AttributeMetadata" = type { %"class.draco::Metadata", i32 }
%"class.std::__2::__compressed_pair.47" = type { %"struct.std::__2::__compressed_pair_elem.48" }
%"struct.std::__2::__compressed_pair_elem.48" = type { %"class.std::__2::unique_ptr.43"* }
%"class.std::__2::vector.54" = type { %"class.std::__2::__vector_base.55" }
%"class.std::__2::__vector_base.55" = type { %"class.std::__2::unique_ptr.56"*, %"class.std::__2::unique_ptr.56"*, %"class.std::__2::__compressed_pair.85" }
%"class.std::__2::unique_ptr.56" = type { %"class.std::__2::__compressed_pair.57" }
%"class.std::__2::__compressed_pair.57" = type { %"struct.std::__2::__compressed_pair_elem.58" }
%"struct.std::__2::__compressed_pair_elem.58" = type { %"class.draco::PointAttribute"* }
%"class.draco::PointAttribute" = type <{ %"class.draco::GeometryAttribute", %"class.std::__2::unique_ptr.66", %"class.draco::IndexTypeVector", i32, i8, [3 x i8], %"class.std::__2::unique_ptr.78", [4 x i8] }>
%"class.draco::GeometryAttribute" = type { %"class.draco::DataBuffer"*, %"struct.draco::DataBufferDescriptor", i8, i32, i8, i64, i64, i32, i32 }
%"class.draco::DataBuffer" = type { %"class.std::__2::vector.59", %"struct.draco::DataBufferDescriptor" }
%"class.std::__2::vector.59" = type { %"class.std::__2::__vector_base.60" }
%"class.std::__2::__vector_base.60" = type { i8*, i8*, %"class.std::__2::__compressed_pair.61" }
%"class.std::__2::__compressed_pair.61" = type { %"struct.std::__2::__compressed_pair_elem.62" }
%"struct.std::__2::__compressed_pair_elem.62" = type { i8* }
%"struct.draco::DataBufferDescriptor" = type { i64, i64 }
%"class.std::__2::unique_ptr.66" = type { %"class.std::__2::__compressed_pair.67" }
%"class.std::__2::__compressed_pair.67" = type { %"struct.std::__2::__compressed_pair_elem.68" }
%"struct.std::__2::__compressed_pair_elem.68" = type { %"class.draco::DataBuffer"* }
%"class.draco::IndexTypeVector" = type { %"class.std::__2::vector.71" }
%"class.std::__2::vector.71" = type { %"class.std::__2::__vector_base.72" }
%"class.std::__2::__vector_base.72" = type { %"class.draco::IndexType"*, %"class.draco::IndexType"*, %"class.std::__2::__compressed_pair.73" }
%"class.draco::IndexType" = type { i32 }
%"class.std::__2::__compressed_pair.73" = type { %"struct.std::__2::__compressed_pair_elem.74" }
%"struct.std::__2::__compressed_pair_elem.74" = type { %"class.draco::IndexType"* }
%"class.std::__2::unique_ptr.78" = type { %"class.std::__2::__compressed_pair.79" }
%"class.std::__2::__compressed_pair.79" = type { %"struct.std::__2::__compressed_pair_elem.80" }
%"struct.std::__2::__compressed_pair_elem.80" = type { %"class.draco::AttributeTransformData"* }
%"class.draco::AttributeTransformData" = type { i32, %"class.draco::DataBuffer" }
%"class.std::__2::__compressed_pair.85" = type { %"struct.std::__2::__compressed_pair_elem.86" }
%"struct.std::__2::__compressed_pair_elem.86" = type { %"class.std::__2::unique_ptr.56"* }
%"class.std::__2::vector.90" = type { %"class.std::__2::__vector_base.91" }
%"class.std::__2::__vector_base.91" = type { i32*, i32*, %"class.std::__2::__compressed_pair.92" }
%"class.std::__2::__compressed_pair.92" = type { %"struct.std::__2::__compressed_pair_elem.93" }
%"struct.std::__2::__compressed_pair_elem.93" = type { i32* }
%"struct.std::__2::__default_init_tag" = type { i8 }
%"struct.std::__2::default_delete.98" = type { i8 }
%"class.draco::IndexType.99" = type { i32 }
%"struct.std::__2::__compressed_pair_elem.97" = type { i8 }

$_ZNSt3__210unique_ptrIN5draco10PointCloudENS_14default_deleteIS2_EEEC2ILb1EvEEv = comdat any

$_ZNSt3__210unique_ptrIN5draco10PointCloudENS_14default_deleteIS2_EEEC2ILb1EvEEPS2_ = comdat any

$_ZNSt3__210unique_ptrIN5draco10PointCloudENS_14default_deleteIS2_EEEaSEOS5_ = comdat any

$_ZNSt3__210unique_ptrIN5draco10PointCloudENS_14default_deleteIS2_EEED2Ev = comdat any

$_ZNKSt3__210unique_ptrIN5draco10PointCloudENS_14default_deleteIS2_EEEptEv = comdat any

$_ZN5draco10PointCloud14set_num_pointsEj = comdat any

$_ZNK5draco10PointCloud10num_pointsEv = comdat any

$_ZN5draco10PointCloud9attributeEi = comdat any

$_ZN5draco14PointAttribute17SetAttributeValueENS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEEPKv = comdat any

$_ZNK5draco14PointAttribute12mapped_indexENS_9IndexTypeIjNS_20PointIndex_tag_type_EEE = comdat any

$_ZNK5draco17GeometryAttribute9data_typeEv = comdat any

$_ZNK5draco17GeometryAttribute14num_componentsEv = comdat any

$_ZNK5draco14PointAttribute6bufferEv = comdat any

$_ZN5draco10DataBuffer5WriteExPKvm = comdat any

$_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEC2Ej = comdat any

$_ZNK5draco9IndexTypeIjNS_20PointIndex_tag_type_EEltERKj = comdat any

$_ZNK5draco9IndexTypeIjNS_20PointIndex_tag_type_EE5valueEv = comdat any

$_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEppEv = comdat any

$_ZNSt3__24moveIRNS_10unique_ptrIN5draco10PointCloudENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOS9_ = comdat any

$_ZNSt3__210unique_ptrIN5draco10PointCloudENS_14default_deleteIS2_EEEC2EOS5_ = comdat any

$_ZNSt3__217__compressed_pairIPN5draco10PointCloudENS_14default_deleteIS2_EEEC2IS3_NS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__27forwardIPN5draco10PointCloudEEEOT_RNS_16remove_referenceIS4_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco10PointCloudELi0ELb0EEC2IS3_vEEOT_ = comdat any

$_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco10PointCloudEEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEixEm = comdat any

$_ZNKSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEE3getEv = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPN5draco14PointAttributeELi0ELb0EE5__getEv = comdat any

$_ZNK5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EE5valueEv = comdat any

$_ZNK5draco17GeometryAttribute11byte_strideEv = comdat any

$_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEC2Ej = comdat any

$_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEEEixERKS3_ = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEEixEm = comdat any

$_ZNKSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEE3getEv = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPN5draco10DataBufferELi0ELb0EE5__getEv = comdat any

$_ZN5draco10DataBuffer4dataEv = comdat any

$_ZNSt3__26vectorIhNS_9allocatorIhEEEixEm = comdat any

$_ZNSt3__210unique_ptrIN5draco10PointCloudENS_14default_deleteIS2_EEE5resetEPS2_ = comdat any

$_ZNSt3__217__compressed_pairIPN5draco10PointCloudENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco10PointCloudENS_14default_deleteIS2_EEE6secondEv = comdat any

$_ZNKSt3__214default_deleteIN5draco10PointCloudEEclEPS2_ = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco10PointCloudELi0ELb0EE5__getEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco10PointCloudEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco10PointCloudENS_14default_deleteIS2_EEEC2IRS3_NS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__27forwardIRPN5draco10PointCloudEEEOT_RNS_16remove_referenceIS5_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco10PointCloudELi0ELb0EEC2IRS3_vEEOT_ = comdat any

$_ZNSt3__210unique_ptrIN5draco10PointCloudENS_14default_deleteIS2_EEE7releaseEv = comdat any

$_ZNSt3__27forwardINS_14default_deleteIN5draco10PointCloudEEEEEOT_RNS_16remove_referenceIS5_E4typeE = comdat any

$_ZNSt3__210unique_ptrIN5draco10PointCloudENS_14default_deleteIS2_EEE11get_deleterEv = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco10PointCloudENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPN5draco10PointCloudELi0ELb0EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco10PointCloudENS_14default_deleteIS2_EEEC2IS3_S5_EEOT_OT0_ = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco10PointCloudEEELi1ELb1EEC2IS4_vEEOT_ = comdat any

@_ZN5draco17PointCloudBuilderC1Ev = hidden unnamed_addr alias %"class.draco::PointCloudBuilder"* (%"class.draco::PointCloudBuilder"*), %"class.draco::PointCloudBuilder"* (%"class.draco::PointCloudBuilder"*)* @_ZN5draco17PointCloudBuilderC2Ev

; Function Attrs: noinline nounwind optnone
define hidden %"class.draco::PointCloudBuilder"* @_ZN5draco17PointCloudBuilderC2Ev(%"class.draco::PointCloudBuilder"* returned %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %"class.draco::PointCloudBuilder"*, align 4
  store %"class.draco::PointCloudBuilder"* %this, %"class.draco::PointCloudBuilder"** %this.addr, align 4
  %this1 = load %"class.draco::PointCloudBuilder"*, %"class.draco::PointCloudBuilder"** %this.addr, align 4
  %point_cloud_ = getelementptr inbounds %"class.draco::PointCloudBuilder", %"class.draco::PointCloudBuilder"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrIN5draco10PointCloudENS_14default_deleteIS2_EEEC2ILb1EvEEv(%"class.std::__2::unique_ptr"* %point_cloud_) #4
  ret %"class.draco::PointCloudBuilder"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrIN5draco10PointCloudENS_14default_deleteIS2_EEEC2ILb1EvEEv(%"class.std::__2::unique_ptr"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  %ref.tmp = alloca %"class.draco::PointCloud"*, align 4
  %ref.tmp2 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  store %"class.draco::PointCloud"* null, %"class.draco::PointCloud"** %ref.tmp, align 4
  %call = call %"class.std::__2::__compressed_pair"* @_ZNSt3__217__compressed_pairIPN5draco10PointCloudENS_14default_deleteIS2_EEEC2IS3_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair"* %__ptr_, %"class.draco::PointCloud"** nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  ret %"class.std::__2::unique_ptr"* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN5draco17PointCloudBuilder5StartEj(%"class.draco::PointCloudBuilder"* %this, i32 %num_points) #0 {
entry:
  %this.addr = alloca %"class.draco::PointCloudBuilder"*, align 4
  %num_points.addr = alloca i32, align 4
  %ref.tmp = alloca %"class.std::__2::unique_ptr", align 4
  store %"class.draco::PointCloudBuilder"* %this, %"class.draco::PointCloudBuilder"** %this.addr, align 4
  store i32 %num_points, i32* %num_points.addr, align 4
  %this1 = load %"class.draco::PointCloudBuilder"*, %"class.draco::PointCloudBuilder"** %this.addr, align 4
  %call = call noalias nonnull i8* @_Znwm(i32 84) #5
  %0 = bitcast i8* %call to %"class.draco::PointCloud"*
  %call2 = call %"class.draco::PointCloud"* @_ZN5draco10PointCloudC1Ev(%"class.draco::PointCloud"* %0)
  %call3 = call %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrIN5draco10PointCloudENS_14default_deleteIS2_EEEC2ILb1EvEEPS2_(%"class.std::__2::unique_ptr"* %ref.tmp, %"class.draco::PointCloud"* %0) #4
  %point_cloud_ = getelementptr inbounds %"class.draco::PointCloudBuilder", %"class.draco::PointCloudBuilder"* %this1, i32 0, i32 0
  %call4 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrIN5draco10PointCloudENS_14default_deleteIS2_EEEaSEOS5_(%"class.std::__2::unique_ptr"* %point_cloud_, %"class.std::__2::unique_ptr"* nonnull align 4 dereferenceable(4) %ref.tmp) #4
  %call5 = call %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrIN5draco10PointCloudENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr"* %ref.tmp) #4
  %point_cloud_6 = getelementptr inbounds %"class.draco::PointCloudBuilder", %"class.draco::PointCloudBuilder"* %this1, i32 0, i32 0
  %call7 = call %"class.draco::PointCloud"* @_ZNKSt3__210unique_ptrIN5draco10PointCloudENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr"* %point_cloud_6) #4
  %1 = load i32, i32* %num_points.addr, align 4
  call void @_ZN5draco10PointCloud14set_num_pointsEj(%"class.draco::PointCloud"* %call7, i32 %1)
  ret void
}

; Function Attrs: nobuiltin allocsize(0)
declare nonnull i8* @_Znwm(i32) #1

declare %"class.draco::PointCloud"* @_ZN5draco10PointCloudC1Ev(%"class.draco::PointCloud"* returned) unnamed_addr #2

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrIN5draco10PointCloudENS_14default_deleteIS2_EEEC2ILb1EvEEPS2_(%"class.std::__2::unique_ptr"* returned %this, %"class.draco::PointCloud"* %__p) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  %__p.addr = alloca %"class.draco::PointCloud"*, align 4
  %ref.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  store %"class.draco::PointCloud"* %__p, %"class.draco::PointCloud"** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::__compressed_pair"* @_ZNSt3__217__compressed_pairIPN5draco10PointCloudENS_14default_deleteIS2_EEEC2IRS3_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair"* %__ptr_, %"class.draco::PointCloud"** nonnull align 4 dereferenceable(4) %__p.addr, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp)
  ret %"class.std::__2::unique_ptr"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrIN5draco10PointCloudENS_14default_deleteIS2_EEEaSEOS5_(%"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"* nonnull align 4 dereferenceable(4) %__u) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  %__u.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  store %"class.std::__2::unique_ptr"* %__u, %"class.std::__2::unique_ptr"** %__u.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %0 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %__u.addr, align 4
  %call = call %"class.draco::PointCloud"* @_ZNSt3__210unique_ptrIN5draco10PointCloudENS_14default_deleteIS2_EEE7releaseEv(%"class.std::__2::unique_ptr"* %0) #4
  call void @_ZNSt3__210unique_ptrIN5draco10PointCloudENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr"* %this1, %"class.draco::PointCloud"* %call) #4
  %1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %__u.addr, align 4
  %call2 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.98"* @_ZNSt3__210unique_ptrIN5draco10PointCloudENS_14default_deleteIS2_EEE11get_deleterEv(%"class.std::__2::unique_ptr"* %1) #4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.98"* @_ZNSt3__27forwardINS_14default_deleteIN5draco10PointCloudEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.98"* nonnull align 1 dereferenceable(1) %call2) #4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call4 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.98"* @_ZNSt3__217__compressed_pairIPN5draco10PointCloudENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair"* %__ptr_) #4
  ret %"class.std::__2::unique_ptr"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrIN5draco10PointCloudENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  call void @_ZNSt3__210unique_ptrIN5draco10PointCloudENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr"* %this1, %"class.draco::PointCloud"* null) #4
  ret %"class.std::__2::unique_ptr"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::PointCloud"* @_ZNKSt3__210unique_ptrIN5draco10PointCloudENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointCloud"** @_ZNKSt3__217__compressed_pairIPN5draco10PointCloudENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair"* %__ptr_) #4
  %0 = load %"class.draco::PointCloud"*, %"class.draco::PointCloud"** %call, align 4
  ret %"class.draco::PointCloud"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco10PointCloud14set_num_pointsEj(%"class.draco::PointCloud"* %this, i32 %num) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointCloud"*, align 4
  %num.addr = alloca i32, align 4
  store %"class.draco::PointCloud"* %this, %"class.draco::PointCloud"** %this.addr, align 4
  store i32 %num, i32* %num.addr, align 4
  %this1 = load %"class.draco::PointCloud"*, %"class.draco::PointCloud"** %this.addr, align 4
  %0 = load i32, i32* %num.addr, align 4
  %num_points_ = getelementptr inbounds %"class.draco::PointCloud", %"class.draco::PointCloud"* %this1, i32 0, i32 4
  store i32 %0, i32* %num_points_, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @_ZN5draco17PointCloudBuilder12AddAttributeENS_17GeometryAttribute4TypeEaNS_8DataTypeE(%"class.draco::PointCloudBuilder"* %this, i32 %attribute_type, i8 signext %num_components, i32 %data_type) #0 {
entry:
  %this.addr = alloca %"class.draco::PointCloudBuilder"*, align 4
  %attribute_type.addr = alloca i32, align 4
  %num_components.addr = alloca i8, align 1
  %data_type.addr = alloca i32, align 4
  %ga = alloca %"class.draco::GeometryAttribute", align 8
  store %"class.draco::PointCloudBuilder"* %this, %"class.draco::PointCloudBuilder"** %this.addr, align 4
  store i32 %attribute_type, i32* %attribute_type.addr, align 4
  store i8 %num_components, i8* %num_components.addr, align 1
  store i32 %data_type, i32* %data_type.addr, align 4
  %this1 = load %"class.draco::PointCloudBuilder"*, %"class.draco::PointCloudBuilder"** %this.addr, align 4
  %call = call %"class.draco::GeometryAttribute"* @_ZN5draco17GeometryAttributeC1Ev(%"class.draco::GeometryAttribute"* %ga)
  %0 = load i32, i32* %attribute_type.addr, align 4
  %1 = load i8, i8* %num_components.addr, align 1
  %2 = load i32, i32* %data_type.addr, align 4
  %3 = load i32, i32* %data_type.addr, align 4
  %call2 = call i32 @_ZN5draco14DataTypeLengthENS_8DataTypeE(i32 %3)
  %4 = load i8, i8* %num_components.addr, align 1
  %conv = sext i8 %4 to i32
  %mul = mul nsw i32 %call2, %conv
  %conv3 = sext i32 %mul to i64
  call void @_ZN5draco17GeometryAttribute4InitENS0_4TypeEPNS_10DataBufferEaNS_8DataTypeEbxx(%"class.draco::GeometryAttribute"* %ga, i32 %0, %"class.draco::DataBuffer"* null, i8 signext %1, i32 %2, i1 zeroext false, i64 %conv3, i64 0)
  %point_cloud_ = getelementptr inbounds %"class.draco::PointCloudBuilder", %"class.draco::PointCloudBuilder"* %this1, i32 0, i32 0
  %call4 = call %"class.draco::PointCloud"* @_ZNKSt3__210unique_ptrIN5draco10PointCloudENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr"* %point_cloud_) #4
  %point_cloud_5 = getelementptr inbounds %"class.draco::PointCloudBuilder", %"class.draco::PointCloudBuilder"* %this1, i32 0, i32 0
  %call6 = call %"class.draco::PointCloud"* @_ZNKSt3__210unique_ptrIN5draco10PointCloudENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr"* %point_cloud_5) #4
  %call7 = call i32 @_ZNK5draco10PointCloud10num_pointsEv(%"class.draco::PointCloud"* %call6)
  %call8 = call i32 @_ZN5draco10PointCloud12AddAttributeERKNS_17GeometryAttributeEbj(%"class.draco::PointCloud"* %call4, %"class.draco::GeometryAttribute"* nonnull align 8 dereferenceable(64) %ga, i1 zeroext true, i32 %call7)
  ret i32 %call8
}

declare %"class.draco::GeometryAttribute"* @_ZN5draco17GeometryAttributeC1Ev(%"class.draco::GeometryAttribute"* returned) unnamed_addr #2

declare void @_ZN5draco17GeometryAttribute4InitENS0_4TypeEPNS_10DataBufferEaNS_8DataTypeEbxx(%"class.draco::GeometryAttribute"*, i32, %"class.draco::DataBuffer"*, i8 signext, i32, i1 zeroext, i64, i64) #2

declare i32 @_ZN5draco14DataTypeLengthENS_8DataTypeE(i32) #2

declare i32 @_ZN5draco10PointCloud12AddAttributeERKNS_17GeometryAttributeEbj(%"class.draco::PointCloud"*, %"class.draco::GeometryAttribute"* nonnull align 8 dereferenceable(64), i1 zeroext, i32) #2

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco10PointCloud10num_pointsEv(%"class.draco::PointCloud"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointCloud"*, align 4
  store %"class.draco::PointCloud"* %this, %"class.draco::PointCloud"** %this.addr, align 4
  %this1 = load %"class.draco::PointCloud"*, %"class.draco::PointCloud"** %this.addr, align 4
  %num_points_ = getelementptr inbounds %"class.draco::PointCloud", %"class.draco::PointCloud"* %this1, i32 0, i32 4
  %0 = load i32, i32* %num_points_, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN5draco17PointCloudBuilder25SetAttributeValueForPointEiNS_9IndexTypeIjNS_20PointIndex_tag_type_EEEPKv(%"class.draco::PointCloudBuilder"* %this, i32 %att_id, i32 %point_index.coerce, i8* %attribute_value) #0 {
entry:
  %point_index = alloca %"class.draco::IndexType.99", align 4
  %this.addr = alloca %"class.draco::PointCloudBuilder"*, align 4
  %att_id.addr = alloca i32, align 4
  %attribute_value.addr = alloca i8*, align 4
  %att = alloca %"class.draco::PointAttribute"*, align 4
  %agg.tmp = alloca %"class.draco::IndexType", align 4
  %agg.tmp3 = alloca %"class.draco::IndexType.99", align 4
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType.99", %"class.draco::IndexType.99"* %point_index, i32 0, i32 0
  store i32 %point_index.coerce, i32* %coerce.dive, align 4
  store %"class.draco::PointCloudBuilder"* %this, %"class.draco::PointCloudBuilder"** %this.addr, align 4
  store i32 %att_id, i32* %att_id.addr, align 4
  store i8* %attribute_value, i8** %attribute_value.addr, align 4
  %this1 = load %"class.draco::PointCloudBuilder"*, %"class.draco::PointCloudBuilder"** %this.addr, align 4
  %point_cloud_ = getelementptr inbounds %"class.draco::PointCloudBuilder", %"class.draco::PointCloudBuilder"* %this1, i32 0, i32 0
  %call = call %"class.draco::PointCloud"* @_ZNKSt3__210unique_ptrIN5draco10PointCloudENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr"* %point_cloud_) #4
  %0 = load i32, i32* %att_id.addr, align 4
  %call2 = call %"class.draco::PointAttribute"* @_ZN5draco10PointCloud9attributeEi(%"class.draco::PointCloud"* %call, i32 %0)
  store %"class.draco::PointAttribute"* %call2, %"class.draco::PointAttribute"** %att, align 4
  %1 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %att, align 4
  %2 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %att, align 4
  %3 = bitcast %"class.draco::IndexType.99"* %agg.tmp3 to i8*
  %4 = bitcast %"class.draco::IndexType.99"* %point_index to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 4, i1 false)
  %coerce.dive4 = getelementptr inbounds %"class.draco::IndexType.99", %"class.draco::IndexType.99"* %agg.tmp3, i32 0, i32 0
  %5 = load i32, i32* %coerce.dive4, align 4
  %call5 = call i32 @_ZNK5draco14PointAttribute12mapped_indexENS_9IndexTypeIjNS_20PointIndex_tag_type_EEE(%"class.draco::PointAttribute"* %2, i32 %5)
  %coerce.dive6 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %agg.tmp, i32 0, i32 0
  store i32 %call5, i32* %coerce.dive6, align 4
  %6 = load i8*, i8** %attribute_value.addr, align 4
  %coerce.dive7 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %agg.tmp, i32 0, i32 0
  %7 = load i32, i32* %coerce.dive7, align 4
  call void @_ZN5draco14PointAttribute17SetAttributeValueENS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEEPKv(%"class.draco::PointAttribute"* %1, i32 %7, i8* %6)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::PointAttribute"* @_ZN5draco10PointCloud9attributeEi(%"class.draco::PointCloud"* %this, i32 %att_id) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointCloud"*, align 4
  %att_id.addr = alloca i32, align 4
  store %"class.draco::PointCloud"* %this, %"class.draco::PointCloud"** %this.addr, align 4
  store i32 %att_id, i32* %att_id.addr, align 4
  %this1 = load %"class.draco::PointCloud"*, %"class.draco::PointCloud"** %this.addr, align 4
  %attributes_ = getelementptr inbounds %"class.draco::PointCloud", %"class.draco::PointCloud"* %this1, i32 0, i32 2
  %0 = load i32, i32* %att_id.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.56"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEixEm(%"class.std::__2::vector.54"* %attributes_, i32 %0) #4
  %call2 = call %"class.draco::PointAttribute"* @_ZNKSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEE3getEv(%"class.std::__2::unique_ptr.56"* %call) #4
  ret %"class.draco::PointAttribute"* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco14PointAttribute17SetAttributeValueENS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEEPKv(%"class.draco::PointAttribute"* %this, i32 %entry_index.coerce, i8* %value) #0 comdat {
entry:
  %entry_index = alloca %"class.draco::IndexType", align 4
  %this.addr = alloca %"class.draco::PointAttribute"*, align 4
  %value.addr = alloca i8*, align 4
  %byte_pos = alloca i64, align 8
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %entry_index, i32 0, i32 0
  store i32 %entry_index.coerce, i32* %coerce.dive, align 4
  store %"class.draco::PointAttribute"* %this, %"class.draco::PointAttribute"** %this.addr, align 4
  store i8* %value, i8** %value.addr, align 4
  %this1 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %this.addr, align 4
  %call = call i32 @_ZNK5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EE5valueEv(%"class.draco::IndexType"* %entry_index)
  %conv = zext i32 %call to i64
  %0 = bitcast %"class.draco::PointAttribute"* %this1 to %"class.draco::GeometryAttribute"*
  %call2 = call i64 @_ZNK5draco17GeometryAttribute11byte_strideEv(%"class.draco::GeometryAttribute"* %0)
  %mul = mul nsw i64 %conv, %call2
  store i64 %mul, i64* %byte_pos, align 8
  %call3 = call %"class.draco::DataBuffer"* @_ZNK5draco14PointAttribute6bufferEv(%"class.draco::PointAttribute"* %this1)
  %1 = load i64, i64* %byte_pos, align 8
  %2 = load i8*, i8** %value.addr, align 4
  %3 = bitcast %"class.draco::PointAttribute"* %this1 to %"class.draco::GeometryAttribute"*
  %call4 = call i64 @_ZNK5draco17GeometryAttribute11byte_strideEv(%"class.draco::GeometryAttribute"* %3)
  %conv5 = trunc i64 %call4 to i32
  call void @_ZN5draco10DataBuffer5WriteExPKvm(%"class.draco::DataBuffer"* %call3, i64 %1, i8* %2, i32 %conv5)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco14PointAttribute12mapped_indexENS_9IndexTypeIjNS_20PointIndex_tag_type_EEE(%"class.draco::PointAttribute"* %this, i32 %point_index.coerce) #0 comdat {
entry:
  %retval = alloca %"class.draco::IndexType", align 4
  %point_index = alloca %"class.draco::IndexType.99", align 4
  %this.addr = alloca %"class.draco::PointAttribute"*, align 4
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType.99", %"class.draco::IndexType.99"* %point_index, i32 0, i32 0
  store i32 %point_index.coerce, i32* %coerce.dive, align 4
  store %"class.draco::PointAttribute"* %this, %"class.draco::PointAttribute"** %this.addr, align 4
  %this1 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %this.addr, align 4
  %identity_mapping_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 4
  %0 = load i8, i8* %identity_mapping_, align 4
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call = call i32 @_ZNK5draco9IndexTypeIjNS_20PointIndex_tag_type_EE5valueEv(%"class.draco::IndexType.99"* %point_index)
  %call2 = call %"class.draco::IndexType"* @_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEC2Ej(%"class.draco::IndexType"* %retval, i32 %call)
  br label %return

if.end:                                           ; preds = %entry
  %indices_map_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 2
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEEEixERKS3_(%"class.draco::IndexTypeVector"* %indices_map_, %"class.draco::IndexType.99"* nonnull align 4 dereferenceable(4) %point_index)
  %1 = bitcast %"class.draco::IndexType"* %retval to i8*
  %2 = bitcast %"class.draco::IndexType"* %call3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 4, i1 false)
  br label %return

return:                                           ; preds = %if.end, %if.then
  %coerce.dive4 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %retval, i32 0, i32 0
  %3 = load i32, i32* %coerce.dive4, align 4
  ret i32 %3
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #3

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN5draco17PointCloudBuilder30SetAttributeValuesForAllPointsEiPKvi(%"class.draco::PointCloudBuilder"* %this, i32 %att_id, i8* %attribute_values, i32 %stride) #0 {
entry:
  %this.addr = alloca %"class.draco::PointCloudBuilder"*, align 4
  %att_id.addr = alloca i32, align 4
  %attribute_values.addr = alloca i8*, align 4
  %stride.addr = alloca i32, align 4
  %att = alloca %"class.draco::PointAttribute"*, align 4
  %data_stride = alloca i32, align 4
  %i = alloca %"class.draco::IndexType.99", align 4
  %ref.tmp = alloca i32, align 4
  %agg.tmp = alloca %"class.draco::IndexType", align 4
  %agg.tmp18 = alloca %"class.draco::IndexType.99", align 4
  store %"class.draco::PointCloudBuilder"* %this, %"class.draco::PointCloudBuilder"** %this.addr, align 4
  store i32 %att_id, i32* %att_id.addr, align 4
  store i8* %attribute_values, i8** %attribute_values.addr, align 4
  store i32 %stride, i32* %stride.addr, align 4
  %this1 = load %"class.draco::PointCloudBuilder"*, %"class.draco::PointCloudBuilder"** %this.addr, align 4
  %point_cloud_ = getelementptr inbounds %"class.draco::PointCloudBuilder", %"class.draco::PointCloudBuilder"* %this1, i32 0, i32 0
  %call = call %"class.draco::PointCloud"* @_ZNKSt3__210unique_ptrIN5draco10PointCloudENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr"* %point_cloud_) #4
  %0 = load i32, i32* %att_id.addr, align 4
  %call2 = call %"class.draco::PointAttribute"* @_ZN5draco10PointCloud9attributeEi(%"class.draco::PointCloud"* %call, i32 %0)
  store %"class.draco::PointAttribute"* %call2, %"class.draco::PointAttribute"** %att, align 4
  %1 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %att, align 4
  %2 = bitcast %"class.draco::PointAttribute"* %1 to %"class.draco::GeometryAttribute"*
  %call3 = call i32 @_ZNK5draco17GeometryAttribute9data_typeEv(%"class.draco::GeometryAttribute"* %2)
  %call4 = call i32 @_ZN5draco14DataTypeLengthENS_8DataTypeE(i32 %call3)
  %3 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %att, align 4
  %4 = bitcast %"class.draco::PointAttribute"* %3 to %"class.draco::GeometryAttribute"*
  %call5 = call signext i8 @_ZNK5draco17GeometryAttribute14num_componentsEv(%"class.draco::GeometryAttribute"* %4)
  %conv = sext i8 %call5 to i32
  %mul = mul nsw i32 %call4, %conv
  store i32 %mul, i32* %data_stride, align 4
  %5 = load i32, i32* %stride.addr, align 4
  %cmp = icmp eq i32 %5, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %6 = load i32, i32* %data_stride, align 4
  store i32 %6, i32* %stride.addr, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %7 = load i32, i32* %stride.addr, align 4
  %8 = load i32, i32* %data_stride, align 4
  %cmp6 = icmp eq i32 %7, %8
  br i1 %cmp6, label %if.then7, label %if.else

if.then7:                                         ; preds = %if.end
  %9 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %att, align 4
  %call8 = call %"class.draco::DataBuffer"* @_ZNK5draco14PointAttribute6bufferEv(%"class.draco::PointAttribute"* %9)
  %10 = load i8*, i8** %attribute_values.addr, align 4
  %point_cloud_9 = getelementptr inbounds %"class.draco::PointCloudBuilder", %"class.draco::PointCloudBuilder"* %this1, i32 0, i32 0
  %call10 = call %"class.draco::PointCloud"* @_ZNKSt3__210unique_ptrIN5draco10PointCloudENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr"* %point_cloud_9) #4
  %call11 = call i32 @_ZNK5draco10PointCloud10num_pointsEv(%"class.draco::PointCloud"* %call10)
  %11 = load i32, i32* %data_stride, align 4
  %mul12 = mul i32 %call11, %11
  call void @_ZN5draco10DataBuffer5WriteExPKvm(%"class.draco::DataBuffer"* %call8, i64 0, i8* %10, i32 %mul12)
  br label %if.end25

if.else:                                          ; preds = %if.end
  %call13 = call %"class.draco::IndexType.99"* @_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEC2Ej(%"class.draco::IndexType.99"* %i, i32 0)
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.else
  %point_cloud_14 = getelementptr inbounds %"class.draco::PointCloudBuilder", %"class.draco::PointCloudBuilder"* %this1, i32 0, i32 0
  %call15 = call %"class.draco::PointCloud"* @_ZNKSt3__210unique_ptrIN5draco10PointCloudENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr"* %point_cloud_14) #4
  %call16 = call i32 @_ZNK5draco10PointCloud10num_pointsEv(%"class.draco::PointCloud"* %call15)
  store i32 %call16, i32* %ref.tmp, align 4
  %call17 = call zeroext i1 @_ZNK5draco9IndexTypeIjNS_20PointIndex_tag_type_EEltERKj(%"class.draco::IndexType.99"* %i, i32* nonnull align 4 dereferenceable(4) %ref.tmp)
  br i1 %call17, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %12 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %att, align 4
  %13 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %att, align 4
  %14 = bitcast %"class.draco::IndexType.99"* %agg.tmp18 to i8*
  %15 = bitcast %"class.draco::IndexType.99"* %i to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %14, i8* align 4 %15, i32 4, i1 false)
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType.99", %"class.draco::IndexType.99"* %agg.tmp18, i32 0, i32 0
  %16 = load i32, i32* %coerce.dive, align 4
  %call19 = call i32 @_ZNK5draco14PointAttribute12mapped_indexENS_9IndexTypeIjNS_20PointIndex_tag_type_EEE(%"class.draco::PointAttribute"* %13, i32 %16)
  %coerce.dive20 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %agg.tmp, i32 0, i32 0
  store i32 %call19, i32* %coerce.dive20, align 4
  %17 = load i8*, i8** %attribute_values.addr, align 4
  %18 = load i32, i32* %stride.addr, align 4
  %call21 = call i32 @_ZNK5draco9IndexTypeIjNS_20PointIndex_tag_type_EE5valueEv(%"class.draco::IndexType.99"* %i)
  %mul22 = mul i32 %18, %call21
  %add.ptr = getelementptr inbounds i8, i8* %17, i32 %mul22
  %coerce.dive23 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %agg.tmp, i32 0, i32 0
  %19 = load i32, i32* %coerce.dive23, align 4
  call void @_ZN5draco14PointAttribute17SetAttributeValueENS_9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEEPKv(%"class.draco::PointAttribute"* %12, i32 %19, i8* %add.ptr)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %call24 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.99"* @_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEppEv(%"class.draco::IndexType.99"* %i)
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end25

if.end25:                                         ; preds = %for.end, %if.then7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco17GeometryAttribute9data_typeEv(%"class.draco::GeometryAttribute"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::GeometryAttribute"*, align 4
  store %"class.draco::GeometryAttribute"* %this, %"class.draco::GeometryAttribute"** %this.addr, align 4
  %this1 = load %"class.draco::GeometryAttribute"*, %"class.draco::GeometryAttribute"** %this.addr, align 4
  %data_type_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 3
  %0 = load i32, i32* %data_type_, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden signext i8 @_ZNK5draco17GeometryAttribute14num_componentsEv(%"class.draco::GeometryAttribute"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::GeometryAttribute"*, align 4
  store %"class.draco::GeometryAttribute"* %this, %"class.draco::GeometryAttribute"** %this.addr, align 4
  %this1 = load %"class.draco::GeometryAttribute"*, %"class.draco::GeometryAttribute"** %this.addr, align 4
  %num_components_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 2
  %0 = load i8, i8* %num_components_, align 8
  ret i8 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::DataBuffer"* @_ZNK5draco14PointAttribute6bufferEv(%"class.draco::PointAttribute"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointAttribute"*, align 4
  store %"class.draco::PointAttribute"* %this, %"class.draco::PointAttribute"** %this.addr, align 4
  %this1 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %this.addr, align 4
  %attribute_buffer_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 1
  %call = call %"class.draco::DataBuffer"* @_ZNKSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEE3getEv(%"class.std::__2::unique_ptr.66"* %attribute_buffer_) #4
  ret %"class.draco::DataBuffer"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco10DataBuffer5WriteExPKvm(%"class.draco::DataBuffer"* %this, i64 %byte_pos, i8* %in_data, i32 %data_size) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::DataBuffer"*, align 4
  %byte_pos.addr = alloca i64, align 8
  %in_data.addr = alloca i8*, align 4
  %data_size.addr = alloca i32, align 4
  store %"class.draco::DataBuffer"* %this, %"class.draco::DataBuffer"** %this.addr, align 4
  store i64 %byte_pos, i64* %byte_pos.addr, align 8
  store i8* %in_data, i8** %in_data.addr, align 4
  store i32 %data_size, i32* %data_size.addr, align 4
  %this1 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %this.addr, align 4
  %call = call i8* @_ZN5draco10DataBuffer4dataEv(%"class.draco::DataBuffer"* %this1)
  %0 = load i64, i64* %byte_pos.addr, align 8
  %idx.ext = trunc i64 %0 to i32
  %add.ptr = getelementptr inbounds i8, i8* %call, i32 %idx.ext
  %1 = load i8*, i8** %in_data.addr, align 4
  %2 = load i32, i32* %data_size.addr, align 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %add.ptr, i8* align 1 %1, i32 %2, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType.99"* @_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEC2Ej(%"class.draco::IndexType.99"* returned %this, i32 %value) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.99"*, align 4
  %value.addr = alloca i32, align 4
  store %"class.draco::IndexType.99"* %this, %"class.draco::IndexType.99"** %this.addr, align 4
  store i32 %value, i32* %value.addr, align 4
  %this1 = load %"class.draco::IndexType.99"*, %"class.draco::IndexType.99"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.99", %"class.draco::IndexType.99"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value.addr, align 4
  store i32 %0, i32* %value_, align 4
  ret %"class.draco::IndexType.99"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK5draco9IndexTypeIjNS_20PointIndex_tag_type_EEltERKj(%"class.draco::IndexType.99"* %this, i32* nonnull align 4 dereferenceable(4) %val) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.99"*, align 4
  %val.addr = alloca i32*, align 4
  store %"class.draco::IndexType.99"* %this, %"class.draco::IndexType.99"** %this.addr, align 4
  store i32* %val, i32** %val.addr, align 4
  %this1 = load %"class.draco::IndexType.99"*, %"class.draco::IndexType.99"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.99", %"class.draco::IndexType.99"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value_, align 4
  %1 = load i32*, i32** %val.addr, align 4
  %2 = load i32, i32* %1, align 4
  %cmp = icmp ult i32 %0, %2
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco9IndexTypeIjNS_20PointIndex_tag_type_EE5valueEv(%"class.draco::IndexType.99"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.99"*, align 4
  store %"class.draco::IndexType.99"* %this, %"class.draco::IndexType.99"** %this.addr, align 4
  %this1 = load %"class.draco::IndexType.99"*, %"class.draco::IndexType.99"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.99", %"class.draco::IndexType.99"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value_, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.99"* @_ZN5draco9IndexTypeIjNS_20PointIndex_tag_type_EEppEv(%"class.draco::IndexType.99"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.99"*, align 4
  store %"class.draco::IndexType.99"* %this, %"class.draco::IndexType.99"** %this.addr, align 4
  %this1 = load %"class.draco::IndexType.99"*, %"class.draco::IndexType.99"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.99", %"class.draco::IndexType.99"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value_, align 4
  %inc = add i32 %0, 1
  store i32 %inc, i32* %value_, align 4
  ret %"class.draco::IndexType.99"* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN5draco17PointCloudBuilder8FinalizeEb(%"class.std::__2::unique_ptr"* noalias sret align 4 %agg.result, %"class.draco::PointCloudBuilder"* %this, i1 zeroext %deduplicate_points) #0 {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %"class.draco::PointCloudBuilder"*, align 4
  %deduplicate_points.addr = alloca i8, align 1
  %0 = bitcast %"class.std::__2::unique_ptr"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %"class.draco::PointCloudBuilder"* %this, %"class.draco::PointCloudBuilder"** %this.addr, align 4
  %frombool = zext i1 %deduplicate_points to i8
  store i8 %frombool, i8* %deduplicate_points.addr, align 1
  %this1 = load %"class.draco::PointCloudBuilder"*, %"class.draco::PointCloudBuilder"** %this.addr, align 4
  %1 = load i8, i8* %deduplicate_points.addr, align 1
  %tobool = trunc i8 %1 to i1
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %point_cloud_ = getelementptr inbounds %"class.draco::PointCloudBuilder", %"class.draco::PointCloudBuilder"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr"* @_ZNSt3__24moveIRNS_10unique_ptrIN5draco10PointCloudENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOS9_(%"class.std::__2::unique_ptr"* nonnull align 4 dereferenceable(4) %point_cloud_) #4
  %call2 = call %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrIN5draco10PointCloudENS_14default_deleteIS2_EEEC2EOS5_(%"class.std::__2::unique_ptr"* %agg.result, %"class.std::__2::unique_ptr"* nonnull align 4 dereferenceable(4) %call) #4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr"* @_ZNSt3__24moveIRNS_10unique_ptrIN5draco10PointCloudENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOS9_(%"class.std::__2::unique_ptr"* nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  store %"class.std::__2::unique_ptr"* %__t, %"class.std::__2::unique_ptr"** %__t.addr, align 4
  %0 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %__t.addr, align 4
  ret %"class.std::__2::unique_ptr"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrIN5draco10PointCloudENS_14default_deleteIS2_EEEC2EOS5_(%"class.std::__2::unique_ptr"* returned %this, %"class.std::__2::unique_ptr"* nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  %__u.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  %ref.tmp = alloca %"class.draco::PointCloud"*, align 4
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  store %"class.std::__2::unique_ptr"* %__u, %"class.std::__2::unique_ptr"** %__u.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %__u.addr, align 4
  %call = call %"class.draco::PointCloud"* @_ZNSt3__210unique_ptrIN5draco10PointCloudENS_14default_deleteIS2_EEE7releaseEv(%"class.std::__2::unique_ptr"* %0) #4
  store %"class.draco::PointCloud"* %call, %"class.draco::PointCloud"** %ref.tmp, align 4
  %1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %__u.addr, align 4
  %call2 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.98"* @_ZNSt3__210unique_ptrIN5draco10PointCloudENS_14default_deleteIS2_EEE11get_deleterEv(%"class.std::__2::unique_ptr"* %1) #4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.98"* @_ZNSt3__27forwardINS_14default_deleteIN5draco10PointCloudEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.98"* nonnull align 1 dereferenceable(1) %call2) #4
  %call4 = call %"class.std::__2::__compressed_pair"* @_ZNSt3__217__compressed_pairIPN5draco10PointCloudENS_14default_deleteIS2_EEEC2IS3_S5_EEOT_OT0_(%"class.std::__2::__compressed_pair"* %__ptr_, %"class.draco::PointCloud"** nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::default_delete.98"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::unique_ptr"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair"* @_ZNSt3__217__compressed_pairIPN5draco10PointCloudENS_14default_deleteIS2_EEEC2IS3_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair"* returned %this, %"class.draco::PointCloud"** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  %__t1.addr = alloca %"class.draco::PointCloud"**, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  store %"class.draco::PointCloud"** %__t1, %"class.draco::PointCloud"*** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %1 = load %"class.draco::PointCloud"**, %"class.draco::PointCloud"*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointCloud"** @_ZNSt3__27forwardIPN5draco10PointCloudEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::PointCloud"** nonnull align 4 dereferenceable(4) %1) #4
  %call2 = call %"struct.std::__2::__compressed_pair_elem"* @_ZNSt3__222__compressed_pair_elemIPN5draco10PointCloudELi0ELb0EEC2IS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem"* %0, %"class.draco::PointCloud"** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem.97"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #4
  %call4 = call %"struct.std::__2::__compressed_pair_elem.97"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco10PointCloudEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.97"* %2)
  ret %"class.std::__2::__compressed_pair"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::PointCloud"** @_ZNSt3__27forwardIPN5draco10PointCloudEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::PointCloud"** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.draco::PointCloud"**, align 4
  store %"class.draco::PointCloud"** %__t, %"class.draco::PointCloud"*** %__t.addr, align 4
  %0 = load %"class.draco::PointCloud"**, %"class.draco::PointCloud"*** %__t.addr, align 4
  ret %"class.draco::PointCloud"** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem"* @_ZNSt3__222__compressed_pair_elemIPN5draco10PointCloudELi0ELb0EEC2IS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem"* returned %this, %"class.draco::PointCloud"** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem"*, align 4
  %__u.addr = alloca %"class.draco::PointCloud"**, align 4
  store %"struct.std::__2::__compressed_pair_elem"* %this, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  store %"class.draco::PointCloud"** %__u, %"class.draco::PointCloud"*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem"*, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem"* %this1, i32 0, i32 0
  %0 = load %"class.draco::PointCloud"**, %"class.draco::PointCloud"*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointCloud"** @_ZNSt3__27forwardIPN5draco10PointCloudEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::PointCloud"** nonnull align 4 dereferenceable(4) %0) #4
  %1 = load %"class.draco::PointCloud"*, %"class.draco::PointCloud"** %call, align 4
  store %"class.draco::PointCloud"* %1, %"class.draco::PointCloud"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  store %"struct.std::__2::__default_init_tag"* %__t, %"struct.std::__2::__default_init_tag"** %__t.addr, align 4
  %0 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t.addr, align 4
  ret %"struct.std::__2::__default_init_tag"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.97"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco10PointCloudEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.97"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.97"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.97"* %this, %"struct.std::__2::__compressed_pair_elem.97"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.97"*, %"struct.std::__2::__compressed_pair_elem.97"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.97"* %this1 to %"struct.std::__2::default_delete.98"*
  ret %"struct.std::__2::__compressed_pair_elem.97"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.56"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEixEm(%"class.std::__2::vector.54"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.54"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector.54"* %this, %"class.std::__2::vector.54"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.54"*, %"class.std::__2::vector.54"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.54"* %this1 to %"class.std::__2::__vector_base.55"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.55", %"class.std::__2::__vector_base.55"* %0, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.56"*, %"class.std::__2::unique_ptr.56"** %__begin_, align 4
  %2 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds %"class.std::__2::unique_ptr.56", %"class.std::__2::unique_ptr.56"* %1, i32 %2
  ret %"class.std::__2::unique_ptr.56"* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::PointAttribute"* @_ZNKSt3__210unique_ptrIN5draco14PointAttributeENS_14default_deleteIS2_EEE3getEv(%"class.std::__2::unique_ptr.56"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.56"*, align 4
  store %"class.std::__2::unique_ptr.56"* %this, %"class.std::__2::unique_ptr.56"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.56"*, %"class.std::__2::unique_ptr.56"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.56", %"class.std::__2::unique_ptr.56"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNKSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.57"* %__ptr_) #4
  %0 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %call, align 4
  ret %"class.draco::PointAttribute"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNKSt3__217__compressed_pairIPN5draco14PointAttributeENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.57"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.57"*, align 4
  store %"class.std::__2::__compressed_pair.57"* %this, %"class.std::__2::__compressed_pair.57"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.57"*, %"class.std::__2::__compressed_pair.57"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.57"* %this1 to %"struct.std::__2::__compressed_pair_elem.58"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNKSt3__222__compressed_pair_elemIPN5draco14PointAttributeELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.58"* %0) #4
  ret %"class.draco::PointAttribute"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::PointAttribute"** @_ZNKSt3__222__compressed_pair_elemIPN5draco14PointAttributeELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.58"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.58"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.58"* %this, %"struct.std::__2::__compressed_pair_elem.58"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.58"*, %"struct.std::__2::__compressed_pair_elem.58"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.58", %"struct.std::__2::__compressed_pair_elem.58"* %this1, i32 0, i32 0
  ret %"class.draco::PointAttribute"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EE5valueEv(%"class.draco::IndexType"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.draco::IndexType"* %this, %"class.draco::IndexType"** %this.addr, align 4
  %this1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value_, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i64 @_ZNK5draco17GeometryAttribute11byte_strideEv(%"class.draco::GeometryAttribute"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::GeometryAttribute"*, align 4
  store %"class.draco::GeometryAttribute"* %this, %"class.draco::GeometryAttribute"** %this.addr, align 4
  %this1 = load %"class.draco::GeometryAttribute"*, %"class.draco::GeometryAttribute"** %this.addr, align 4
  %byte_stride_ = getelementptr inbounds %"class.draco::GeometryAttribute", %"class.draco::GeometryAttribute"* %this1, i32 0, i32 5
  %0 = load i64, i64* %byte_stride_, align 8
  ret i64 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType"* @_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEC2Ej(%"class.draco::IndexType"* returned %this, i32 %value) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType"*, align 4
  %value.addr = alloca i32, align 4
  store %"class.draco::IndexType"* %this, %"class.draco::IndexType"** %this.addr, align 4
  store i32 %value, i32* %value.addr, align 4
  %this1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value.addr, align 4
  store i32 %0, i32* %value_, align 4
  ret %"class.draco::IndexType"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEEEixERKS3_(%"class.draco::IndexTypeVector"* %this, %"class.draco::IndexType.99"* nonnull align 4 dereferenceable(4) %index) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector"*, align 4
  %index.addr = alloca %"class.draco::IndexType.99"*, align 4
  store %"class.draco::IndexTypeVector"* %this, %"class.draco::IndexTypeVector"** %this.addr, align 4
  store %"class.draco::IndexType.99"* %index, %"class.draco::IndexType.99"** %index.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector"*, %"class.draco::IndexTypeVector"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector", %"class.draco::IndexTypeVector"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexType.99"*, %"class.draco::IndexType.99"** %index.addr, align 4
  %call = call i32 @_ZNK5draco9IndexTypeIjNS_20PointIndex_tag_type_EE5valueEv(%"class.draco::IndexType.99"* %0)
  %call2 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEEixEm(%"class.std::__2::vector.71"* %vector_, i32 %call) #4
  ret %"class.draco::IndexType"* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEEixEm(%"class.std::__2::vector.71"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.71"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector.71"* %this, %"class.std::__2::vector.71"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.71"*, %"class.std::__2::vector.71"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.71"* %this1 to %"class.std::__2::__vector_base.72"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.72", %"class.std::__2::__vector_base.72"* %0, i32 0, i32 0
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  %2 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %1, i32 %2
  ret %"class.draco::IndexType"* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::DataBuffer"* @_ZNKSt3__210unique_ptrIN5draco10DataBufferENS_14default_deleteIS2_EEE3getEv(%"class.std::__2::unique_ptr.66"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.66"*, align 4
  store %"class.std::__2::unique_ptr.66"* %this, %"class.std::__2::unique_ptr.66"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.66"*, %"class.std::__2::unique_ptr.66"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.66", %"class.std::__2::unique_ptr.66"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::DataBuffer"** @_ZNKSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.67"* %__ptr_) #4
  %0 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %call, align 4
  ret %"class.draco::DataBuffer"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::DataBuffer"** @_ZNKSt3__217__compressed_pairIPN5draco10DataBufferENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.67"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.67"*, align 4
  store %"class.std::__2::__compressed_pair.67"* %this, %"class.std::__2::__compressed_pair.67"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.67"*, %"class.std::__2::__compressed_pair.67"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.67"* %this1 to %"struct.std::__2::__compressed_pair_elem.68"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::DataBuffer"** @_ZNKSt3__222__compressed_pair_elemIPN5draco10DataBufferELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.68"* %0) #4
  ret %"class.draco::DataBuffer"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::DataBuffer"** @_ZNKSt3__222__compressed_pair_elemIPN5draco10DataBufferELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.68"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.68"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.68"* %this, %"struct.std::__2::__compressed_pair_elem.68"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.68"*, %"struct.std::__2::__compressed_pair_elem.68"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.68", %"struct.std::__2::__compressed_pair_elem.68"* %this1, i32 0, i32 0
  ret %"class.draco::DataBuffer"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN5draco10DataBuffer4dataEv(%"class.draco::DataBuffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::DataBuffer"*, align 4
  store %"class.draco::DataBuffer"* %this, %"class.draco::DataBuffer"** %this.addr, align 4
  %this1 = load %"class.draco::DataBuffer"*, %"class.draco::DataBuffer"** %this.addr, align 4
  %data_ = getelementptr inbounds %"class.draco::DataBuffer", %"class.draco::DataBuffer"* %this1, i32 0, i32 0
  %call = call nonnull align 1 dereferenceable(1) i8* @_ZNSt3__26vectorIhNS_9allocatorIhEEEixEm(%"class.std::__2::vector.59"* %data_, i32 0) #4
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) i8* @_ZNSt3__26vectorIhNS_9allocatorIhEEEixEm(%"class.std::__2::vector.59"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.59"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector.59"* %this, %"class.std::__2::vector.59"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.59"*, %"class.std::__2::vector.59"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.59"* %this1 to %"class.std::__2::__vector_base.60"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.60", %"class.std::__2::__vector_base.60"* %0, i32 0, i32 0
  %1 = load i8*, i8** %__begin_, align 4
  %2 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds i8, i8* %1, i32 %2
  ret i8* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__210unique_ptrIN5draco10PointCloudENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr"* %this, %"class.draco::PointCloud"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  %__p.addr = alloca %"class.draco::PointCloud"*, align 4
  %__tmp = alloca %"class.draco::PointCloud"*, align 4
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  store %"class.draco::PointCloud"* %__p, %"class.draco::PointCloud"** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointCloud"** @_ZNSt3__217__compressed_pairIPN5draco10PointCloudENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair"* %__ptr_) #4
  %0 = load %"class.draco::PointCloud"*, %"class.draco::PointCloud"** %call, align 4
  store %"class.draco::PointCloud"* %0, %"class.draco::PointCloud"** %__tmp, align 4
  %1 = load %"class.draco::PointCloud"*, %"class.draco::PointCloud"** %__p.addr, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::PointCloud"** @_ZNSt3__217__compressed_pairIPN5draco10PointCloudENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair"* %__ptr_2) #4
  store %"class.draco::PointCloud"* %1, %"class.draco::PointCloud"** %call3, align 4
  %2 = load %"class.draco::PointCloud"*, %"class.draco::PointCloud"** %__tmp, align 4
  %tobool = icmp ne %"class.draco::PointCloud"* %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__ptr_4 = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call5 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.98"* @_ZNSt3__217__compressed_pairIPN5draco10PointCloudENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair"* %__ptr_4) #4
  %3 = load %"class.draco::PointCloud"*, %"class.draco::PointCloud"** %__tmp, align 4
  call void @_ZNKSt3__214default_deleteIN5draco10PointCloudEEclEPS2_(%"struct.std::__2::default_delete.98"* %call5, %"class.draco::PointCloud"* %3) #4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::PointCloud"** @_ZNSt3__217__compressed_pairIPN5draco10PointCloudENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointCloud"** @_ZNSt3__222__compressed_pair_elemIPN5draco10PointCloudELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %0) #4
  ret %"class.draco::PointCloud"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.98"* @_ZNSt3__217__compressed_pairIPN5draco10PointCloudENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem.97"*
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.98"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco10PointCloudEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.97"* %0) #4
  ret %"struct.std::__2::default_delete.98"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__214default_deleteIN5draco10PointCloudEEclEPS2_(%"struct.std::__2::default_delete.98"* %this, %"class.draco::PointCloud"* %__ptr) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::default_delete.98"*, align 4
  %__ptr.addr = alloca %"class.draco::PointCloud"*, align 4
  store %"struct.std::__2::default_delete.98"* %this, %"struct.std::__2::default_delete.98"** %this.addr, align 4
  store %"class.draco::PointCloud"* %__ptr, %"class.draco::PointCloud"** %__ptr.addr, align 4
  %this1 = load %"struct.std::__2::default_delete.98"*, %"struct.std::__2::default_delete.98"** %this.addr, align 4
  %0 = load %"class.draco::PointCloud"*, %"class.draco::PointCloud"** %__ptr.addr, align 4
  %isnull = icmp eq %"class.draco::PointCloud"* %0, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %entry
  %1 = bitcast %"class.draco::PointCloud"* %0 to void (%"class.draco::PointCloud"*)***
  %vtable = load void (%"class.draco::PointCloud"*)**, void (%"class.draco::PointCloud"*)*** %1, align 4
  %vfn = getelementptr inbounds void (%"class.draco::PointCloud"*)*, void (%"class.draco::PointCloud"*)** %vtable, i64 1
  %2 = load void (%"class.draco::PointCloud"*)*, void (%"class.draco::PointCloud"*)** %vfn, align 4
  call void %2(%"class.draco::PointCloud"* %0) #4
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::PointCloud"** @_ZNSt3__222__compressed_pair_elemIPN5draco10PointCloudELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem"*, align 4
  store %"struct.std::__2::__compressed_pair_elem"* %this, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem"*, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem"* %this1, i32 0, i32 0
  ret %"class.draco::PointCloud"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.98"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco10PointCloudEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.97"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.97"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.97"* %this, %"struct.std::__2::__compressed_pair_elem.97"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.97"*, %"struct.std::__2::__compressed_pair_elem.97"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.97"* %this1 to %"struct.std::__2::default_delete.98"*
  ret %"struct.std::__2::default_delete.98"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair"* @_ZNSt3__217__compressed_pairIPN5draco10PointCloudENS_14default_deleteIS2_EEEC2IRS3_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair"* returned %this, %"class.draco::PointCloud"** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  %__t1.addr = alloca %"class.draco::PointCloud"**, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  store %"class.draco::PointCloud"** %__t1, %"class.draco::PointCloud"*** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %1 = load %"class.draco::PointCloud"**, %"class.draco::PointCloud"*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointCloud"** @_ZNSt3__27forwardIRPN5draco10PointCloudEEEOT_RNS_16remove_referenceIS5_E4typeE(%"class.draco::PointCloud"** nonnull align 4 dereferenceable(4) %1) #4
  %call2 = call %"struct.std::__2::__compressed_pair_elem"* @_ZNSt3__222__compressed_pair_elemIPN5draco10PointCloudELi0ELb0EEC2IRS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem"* %0, %"class.draco::PointCloud"** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem.97"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #4
  %call4 = call %"struct.std::__2::__compressed_pair_elem.97"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco10PointCloudEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.97"* %2)
  ret %"class.std::__2::__compressed_pair"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::PointCloud"** @_ZNSt3__27forwardIRPN5draco10PointCloudEEEOT_RNS_16remove_referenceIS5_E4typeE(%"class.draco::PointCloud"** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.draco::PointCloud"**, align 4
  store %"class.draco::PointCloud"** %__t, %"class.draco::PointCloud"*** %__t.addr, align 4
  %0 = load %"class.draco::PointCloud"**, %"class.draco::PointCloud"*** %__t.addr, align 4
  ret %"class.draco::PointCloud"** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem"* @_ZNSt3__222__compressed_pair_elemIPN5draco10PointCloudELi0ELb0EEC2IRS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem"* returned %this, %"class.draco::PointCloud"** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem"*, align 4
  %__u.addr = alloca %"class.draco::PointCloud"**, align 4
  store %"struct.std::__2::__compressed_pair_elem"* %this, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  store %"class.draco::PointCloud"** %__u, %"class.draco::PointCloud"*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem"*, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem"* %this1, i32 0, i32 0
  %0 = load %"class.draco::PointCloud"**, %"class.draco::PointCloud"*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointCloud"** @_ZNSt3__27forwardIRPN5draco10PointCloudEEEOT_RNS_16remove_referenceIS5_E4typeE(%"class.draco::PointCloud"** nonnull align 4 dereferenceable(4) %0) #4
  %1 = load %"class.draco::PointCloud"*, %"class.draco::PointCloud"** %call, align 4
  store %"class.draco::PointCloud"* %1, %"class.draco::PointCloud"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::PointCloud"* @_ZNSt3__210unique_ptrIN5draco10PointCloudENS_14default_deleteIS2_EEE7releaseEv(%"class.std::__2::unique_ptr"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  %__t = alloca %"class.draco::PointCloud"*, align 4
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointCloud"** @_ZNSt3__217__compressed_pairIPN5draco10PointCloudENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair"* %__ptr_) #4
  %0 = load %"class.draco::PointCloud"*, %"class.draco::PointCloud"** %call, align 4
  store %"class.draco::PointCloud"* %0, %"class.draco::PointCloud"** %__t, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::PointCloud"** @_ZNSt3__217__compressed_pairIPN5draco10PointCloudENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair"* %__ptr_2) #4
  store %"class.draco::PointCloud"* null, %"class.draco::PointCloud"** %call3, align 4
  %1 = load %"class.draco::PointCloud"*, %"class.draco::PointCloud"** %__t, align 4
  ret %"class.draco::PointCloud"* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.98"* @_ZNSt3__27forwardINS_14default_deleteIN5draco10PointCloudEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.98"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"struct.std::__2::default_delete.98"*, align 4
  store %"struct.std::__2::default_delete.98"* %__t, %"struct.std::__2::default_delete.98"** %__t.addr, align 4
  %0 = load %"struct.std::__2::default_delete.98"*, %"struct.std::__2::default_delete.98"** %__t.addr, align 4
  ret %"struct.std::__2::default_delete.98"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.98"* @_ZNSt3__210unique_ptrIN5draco10PointCloudENS_14default_deleteIS2_EEE11get_deleterEv(%"class.std::__2::unique_ptr"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.98"* @_ZNSt3__217__compressed_pairIPN5draco10PointCloudENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair"* %__ptr_) #4
  ret %"struct.std::__2::default_delete.98"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::PointCloud"** @_ZNKSt3__217__compressed_pairIPN5draco10PointCloudENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointCloud"** @_ZNKSt3__222__compressed_pair_elemIPN5draco10PointCloudELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %0) #4
  ret %"class.draco::PointCloud"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::PointCloud"** @_ZNKSt3__222__compressed_pair_elemIPN5draco10PointCloudELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem"*, align 4
  store %"struct.std::__2::__compressed_pair_elem"* %this, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem"*, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem"* %this1, i32 0, i32 0
  ret %"class.draco::PointCloud"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair"* @_ZNSt3__217__compressed_pairIPN5draco10PointCloudENS_14default_deleteIS2_EEEC2IS3_S5_EEOT_OT0_(%"class.std::__2::__compressed_pair"* returned %this, %"class.draco::PointCloud"** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::default_delete.98"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  %__t1.addr = alloca %"class.draco::PointCloud"**, align 4
  %__t2.addr = alloca %"struct.std::__2::default_delete.98"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  store %"class.draco::PointCloud"** %__t1, %"class.draco::PointCloud"*** %__t1.addr, align 4
  store %"struct.std::__2::default_delete.98"* %__t2, %"struct.std::__2::default_delete.98"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %1 = load %"class.draco::PointCloud"**, %"class.draco::PointCloud"*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::PointCloud"** @_ZNSt3__27forwardIPN5draco10PointCloudEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::PointCloud"** nonnull align 4 dereferenceable(4) %1) #4
  %call2 = call %"struct.std::__2::__compressed_pair_elem"* @_ZNSt3__222__compressed_pair_elemIPN5draco10PointCloudELi0ELb0EEC2IS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem"* %0, %"class.draco::PointCloud"** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem.97"*
  %3 = load %"struct.std::__2::default_delete.98"*, %"struct.std::__2::default_delete.98"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.98"* @_ZNSt3__27forwardINS_14default_deleteIN5draco10PointCloudEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.98"* nonnull align 1 dereferenceable(1) %3) #4
  %call4 = call %"struct.std::__2::__compressed_pair_elem.97"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco10PointCloudEEELi1ELb1EEC2IS4_vEEOT_(%"struct.std::__2::__compressed_pair_elem.97"* %2, %"struct.std::__2::default_delete.98"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::__compressed_pair"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.97"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco10PointCloudEEELi1ELb1EEC2IS4_vEEOT_(%"struct.std::__2::__compressed_pair_elem.97"* returned %this, %"struct.std::__2::default_delete.98"* nonnull align 1 dereferenceable(1) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.97"*, align 4
  %__u.addr = alloca %"struct.std::__2::default_delete.98"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.97"* %this, %"struct.std::__2::__compressed_pair_elem.97"** %this.addr, align 4
  store %"struct.std::__2::default_delete.98"* %__u, %"struct.std::__2::default_delete.98"** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.97"*, %"struct.std::__2::__compressed_pair_elem.97"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.97"* %this1 to %"struct.std::__2::default_delete.98"*
  %1 = load %"struct.std::__2::default_delete.98"*, %"struct.std::__2::default_delete.98"** %__u.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.98"* @_ZNSt3__27forwardINS_14default_deleteIN5draco10PointCloudEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.98"* nonnull align 1 dereferenceable(1) %1) #4
  ret %"struct.std::__2::__compressed_pair_elem.97"* %this1
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nobuiltin allocsize(0) "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind willreturn }
attributes #4 = { nounwind }
attributes #5 = { builtin allocsize(0) }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
