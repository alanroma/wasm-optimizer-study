; ModuleID = './draco/src/draco/mesh/mesh_attribute_corner_table.cc'
source_filename = "./draco/src/draco/mesh/mesh_attribute_corner_table.cc"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%"class.draco::IndexType" = type { i32 }
%"class.draco::IndexType.9" = type { i32 }
%"class.draco::IndexType.145" = type { i32 }
%"class.draco::IndexType.139" = type { i32 }
%"class.draco::MeshAttributeCornerTable" = type { %"class.std::__2::vector", %"class.std::__2::vector", i8, %"class.std::__2::vector.1", %"class.std::__2::vector.7", %"class.std::__2::vector.15", %"class.draco::CornerTable"*, %"class.draco::ValenceCache.42" }
%"class.std::__2::vector" = type { i32*, i32, %"class.std::__2::__compressed_pair" }
%"class.std::__2::__compressed_pair" = type { %"struct.std::__2::__compressed_pair_elem" }
%"struct.std::__2::__compressed_pair_elem" = type { i32 }
%"class.std::__2::vector.1" = type { %"class.std::__2::__vector_base" }
%"class.std::__2::__vector_base" = type { %"class.draco::IndexType"*, %"class.draco::IndexType"*, %"class.std::__2::__compressed_pair.2" }
%"class.std::__2::__compressed_pair.2" = type { %"struct.std::__2::__compressed_pair_elem.3" }
%"struct.std::__2::__compressed_pair_elem.3" = type { %"class.draco::IndexType"* }
%"class.std::__2::vector.7" = type { %"class.std::__2::__vector_base.8" }
%"class.std::__2::__vector_base.8" = type { %"class.draco::IndexType.9"*, %"class.draco::IndexType.9"*, %"class.std::__2::__compressed_pair.10" }
%"class.std::__2::__compressed_pair.10" = type { %"struct.std::__2::__compressed_pair_elem.11" }
%"struct.std::__2::__compressed_pair_elem.11" = type { %"class.draco::IndexType.9"* }
%"class.std::__2::vector.15" = type { %"class.std::__2::__vector_base.16" }
%"class.std::__2::__vector_base.16" = type { %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"*, %"class.std::__2::__compressed_pair.18" }
%"class.draco::IndexType.17" = type { i32 }
%"class.std::__2::__compressed_pair.18" = type { %"struct.std::__2::__compressed_pair_elem.19" }
%"struct.std::__2::__compressed_pair_elem.19" = type { %"class.draco::IndexType.17"* }
%"class.draco::CornerTable" = type { %"class.draco::IndexTypeVector", %"class.draco::IndexTypeVector.23", %"class.draco::IndexTypeVector.24", i32, i32, i32, %"class.draco::IndexTypeVector.25", %"class.draco::ValenceCache" }
%"class.draco::IndexTypeVector" = type { %"class.std::__2::vector.1" }
%"class.draco::IndexTypeVector.23" = type { %"class.std::__2::vector.7" }
%"class.draco::IndexTypeVector.24" = type { %"class.std::__2::vector.7" }
%"class.draco::IndexTypeVector.25" = type { %"class.std::__2::vector.1" }
%"class.draco::ValenceCache" = type { %"class.draco::CornerTable"*, %"class.draco::IndexTypeVector.26", %"class.draco::IndexTypeVector.34" }
%"class.draco::IndexTypeVector.26" = type { %"class.std::__2::vector.27" }
%"class.std::__2::vector.27" = type { %"class.std::__2::__vector_base.28" }
%"class.std::__2::__vector_base.28" = type { i8*, i8*, %"class.std::__2::__compressed_pair.29" }
%"class.std::__2::__compressed_pair.29" = type { %"struct.std::__2::__compressed_pair_elem.30" }
%"struct.std::__2::__compressed_pair_elem.30" = type { i8* }
%"class.draco::IndexTypeVector.34" = type { %"class.std::__2::vector.35" }
%"class.std::__2::vector.35" = type { %"class.std::__2::__vector_base.36" }
%"class.std::__2::__vector_base.36" = type { i32*, i32*, %"class.std::__2::__compressed_pair.37" }
%"class.std::__2::__compressed_pair.37" = type { %"struct.std::__2::__compressed_pair_elem.38" }
%"struct.std::__2::__compressed_pair_elem.38" = type { i32* }
%"class.draco::ValenceCache.42" = type { %"class.draco::MeshAttributeCornerTable"*, %"class.draco::IndexTypeVector.26", %"class.draco::IndexTypeVector.34" }
%"struct.std::__2::__default_init_tag" = type { i8 }
%"class.std::__2::__vector_base_common" = type { i8 }
%"class.std::__2::allocator.148" = type { i8 }
%"class.std::__2::__bit_iterator" = type { i32*, i32 }
%"class.std::__2::allocator" = type { i8 }
%"class.std::__2::allocator.21" = type { i8 }
%"struct.std::__2::__split_buffer" = type { %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"*, %"class.std::__2::__compressed_pair.153" }
%"class.std::__2::__compressed_pair.153" = type { %"struct.std::__2::__compressed_pair_elem.19", %"struct.std::__2::__compressed_pair_elem.154" }
%"struct.std::__2::__compressed_pair_elem.154" = type { %"class.std::__2::allocator.21"* }
%"class.std::__2::allocator.13" = type { i8 }
%"struct.std::__2::__split_buffer.156" = type { %"class.draco::IndexType.9"*, %"class.draco::IndexType.9"*, %"class.draco::IndexType.9"*, %"class.std::__2::__compressed_pair.157" }
%"class.std::__2::__compressed_pair.157" = type { %"struct.std::__2::__compressed_pair_elem.11", %"struct.std::__2::__compressed_pair_elem.158" }
%"struct.std::__2::__compressed_pair_elem.158" = type { %"class.std::__2::allocator.13"* }
%"class.draco::Mesh" = type { %"class.draco::PointCloud", %"class.std::__2::vector.129", %"class.draco::IndexTypeVector.136" }
%"class.draco::PointCloud" = type { i32 (...)**, %"class.std::__2::unique_ptr", %"class.std::__2::vector.99", [5 x %"class.std::__2::vector.35"], i32 }
%"class.std::__2::unique_ptr" = type { %"class.std::__2::__compressed_pair.43" }
%"class.std::__2::__compressed_pair.43" = type { %"struct.std::__2::__compressed_pair_elem.44" }
%"struct.std::__2::__compressed_pair_elem.44" = type { %"class.draco::GeometryMetadata"* }
%"class.draco::GeometryMetadata" = type { %"class.draco::Metadata", %"class.std::__2::vector.86" }
%"class.draco::Metadata" = type { %"class.std::__2::unordered_map", %"class.std::__2::unordered_map.63" }
%"class.std::__2::unordered_map" = type { %"class.std::__2::__hash_table" }
%"class.std::__2::__hash_table" = type { %"class.std::__2::unique_ptr.45", %"class.std::__2::__compressed_pair.53", %"class.std::__2::__compressed_pair.58", %"class.std::__2::__compressed_pair.60" }
%"class.std::__2::unique_ptr.45" = type { %"class.std::__2::__compressed_pair.46" }
%"class.std::__2::__compressed_pair.46" = type { %"struct.std::__2::__compressed_pair_elem.47", %"struct.std::__2::__compressed_pair_elem.48" }
%"struct.std::__2::__compressed_pair_elem.47" = type { %"struct.std::__2::__hash_node_base"** }
%"struct.std::__2::__hash_node_base" = type { %"struct.std::__2::__hash_node_base"* }
%"struct.std::__2::__compressed_pair_elem.48" = type { %"class.std::__2::__bucket_list_deallocator" }
%"class.std::__2::__bucket_list_deallocator" = type { %"class.std::__2::__compressed_pair.49" }
%"class.std::__2::__compressed_pair.49" = type { %"struct.std::__2::__compressed_pair_elem" }
%"class.std::__2::__compressed_pair.53" = type { %"struct.std::__2::__compressed_pair_elem.54" }
%"struct.std::__2::__compressed_pair_elem.54" = type { %"struct.std::__2::__hash_node_base" }
%"class.std::__2::__compressed_pair.58" = type { %"struct.std::__2::__compressed_pair_elem" }
%"class.std::__2::__compressed_pair.60" = type { %"struct.std::__2::__compressed_pair_elem.61" }
%"struct.std::__2::__compressed_pair_elem.61" = type { float }
%"class.std::__2::unordered_map.63" = type { %"class.std::__2::__hash_table.64" }
%"class.std::__2::__hash_table.64" = type { %"class.std::__2::unique_ptr.65", %"class.std::__2::__compressed_pair.75", %"class.std::__2::__compressed_pair.80", %"class.std::__2::__compressed_pair.83" }
%"class.std::__2::unique_ptr.65" = type { %"class.std::__2::__compressed_pair.66" }
%"class.std::__2::__compressed_pair.66" = type { %"struct.std::__2::__compressed_pair_elem.67", %"struct.std::__2::__compressed_pair_elem.69" }
%"struct.std::__2::__compressed_pair_elem.67" = type { %"struct.std::__2::__hash_node_base.68"** }
%"struct.std::__2::__hash_node_base.68" = type { %"struct.std::__2::__hash_node_base.68"* }
%"struct.std::__2::__compressed_pair_elem.69" = type { %"class.std::__2::__bucket_list_deallocator.70" }
%"class.std::__2::__bucket_list_deallocator.70" = type { %"class.std::__2::__compressed_pair.71" }
%"class.std::__2::__compressed_pair.71" = type { %"struct.std::__2::__compressed_pair_elem" }
%"class.std::__2::__compressed_pair.75" = type { %"struct.std::__2::__compressed_pair_elem.76" }
%"struct.std::__2::__compressed_pair_elem.76" = type { %"struct.std::__2::__hash_node_base.68" }
%"class.std::__2::__compressed_pair.80" = type { %"struct.std::__2::__compressed_pair_elem" }
%"class.std::__2::__compressed_pair.83" = type { %"struct.std::__2::__compressed_pair_elem.61" }
%"class.std::__2::vector.86" = type { %"class.std::__2::__vector_base.87" }
%"class.std::__2::__vector_base.87" = type { %"class.std::__2::unique_ptr.88"*, %"class.std::__2::unique_ptr.88"*, %"class.std::__2::__compressed_pair.92" }
%"class.std::__2::unique_ptr.88" = type { %"class.std::__2::__compressed_pair.89" }
%"class.std::__2::__compressed_pair.89" = type { %"struct.std::__2::__compressed_pair_elem.90" }
%"struct.std::__2::__compressed_pair_elem.90" = type { %"class.draco::AttributeMetadata"* }
%"class.draco::AttributeMetadata" = type { %"class.draco::Metadata", i32 }
%"class.std::__2::__compressed_pair.92" = type { %"struct.std::__2::__compressed_pair_elem.93" }
%"struct.std::__2::__compressed_pair_elem.93" = type { %"class.std::__2::unique_ptr.88"* }
%"class.std::__2::vector.99" = type { %"class.std::__2::__vector_base.100" }
%"class.std::__2::__vector_base.100" = type { %"class.std::__2::unique_ptr.101"*, %"class.std::__2::unique_ptr.101"*, %"class.std::__2::__compressed_pair.124" }
%"class.std::__2::unique_ptr.101" = type { %"class.std::__2::__compressed_pair.102" }
%"class.std::__2::__compressed_pair.102" = type { %"struct.std::__2::__compressed_pair_elem.103" }
%"struct.std::__2::__compressed_pair_elem.103" = type { %"class.draco::PointAttribute"* }
%"class.std::__2::__compressed_pair.124" = type { %"struct.std::__2::__compressed_pair_elem.125" }
%"struct.std::__2::__compressed_pair_elem.125" = type { %"class.std::__2::unique_ptr.101"* }
%"class.std::__2::vector.129" = type { %"class.std::__2::__vector_base.130" }
%"class.std::__2::__vector_base.130" = type { %"struct.draco::Mesh::AttributeData"*, %"struct.draco::Mesh::AttributeData"*, %"class.std::__2::__compressed_pair.131" }
%"struct.draco::Mesh::AttributeData" = type { i32 }
%"class.std::__2::__compressed_pair.131" = type { %"struct.std::__2::__compressed_pair_elem.132" }
%"struct.std::__2::__compressed_pair_elem.132" = type { %"struct.draco::Mesh::AttributeData"* }
%"class.draco::IndexTypeVector.136" = type { %"class.std::__2::vector.137" }
%"class.std::__2::vector.137" = type { %"class.std::__2::__vector_base.138" }
%"class.std::__2::__vector_base.138" = type { %"struct.std::__2::array"*, %"struct.std::__2::array"*, %"class.std::__2::__compressed_pair.140" }
%"struct.std::__2::array" = type { [3 x %"class.draco::IndexType.139"] }
%"class.std::__2::__compressed_pair.140" = type { %"struct.std::__2::__compressed_pair_elem.141" }
%"struct.std::__2::__compressed_pair_elem.141" = type { %"struct.std::__2::array"* }
%"class.draco::PointAttribute" = type <{ %"class.draco::GeometryAttribute", %"class.std::__2::unique_ptr.111", %"class.draco::IndexTypeVector.116", i32, i8, [3 x i8], %"class.std::__2::unique_ptr.117", [4 x i8] }>
%"class.draco::GeometryAttribute" = type { %"class.draco::DataBuffer"*, %"struct.draco::DataBufferDescriptor", i8, i32, i8, i64, i64, i32, i32 }
%"class.draco::DataBuffer" = type { %"class.std::__2::vector.104", %"struct.draco::DataBufferDescriptor" }
%"class.std::__2::vector.104" = type { %"class.std::__2::__vector_base.105" }
%"class.std::__2::__vector_base.105" = type { i8*, i8*, %"class.std::__2::__compressed_pair.106" }
%"class.std::__2::__compressed_pair.106" = type { %"struct.std::__2::__compressed_pair_elem.107" }
%"struct.std::__2::__compressed_pair_elem.107" = type { i8* }
%"struct.draco::DataBufferDescriptor" = type { i64, i64 }
%"class.std::__2::unique_ptr.111" = type { %"class.std::__2::__compressed_pair.112" }
%"class.std::__2::__compressed_pair.112" = type { %"struct.std::__2::__compressed_pair_elem.113" }
%"struct.std::__2::__compressed_pair_elem.113" = type { %"class.draco::DataBuffer"* }
%"class.draco::IndexTypeVector.116" = type { %"class.std::__2::vector.15" }
%"class.std::__2::unique_ptr.117" = type { %"class.std::__2::__compressed_pair.118" }
%"class.std::__2::__compressed_pair.118" = type { %"struct.std::__2::__compressed_pair_elem.119" }
%"struct.std::__2::__compressed_pair_elem.119" = type { %"class.draco::AttributeTransformData"* }
%"class.draco::AttributeTransformData" = type { i32, %"class.draco::DataBuffer" }
%"class.std::__2::__bit_reference" = type { i32*, i32 }
%"class.draco::VertexRingIterator" = type <{ %"class.draco::MeshAttributeCornerTable"*, %"class.draco::IndexType.9", %"class.draco::IndexType.9", i8, [3 x i8] }>
%"struct.std::__2::iterator" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.31" = type { i8 }
%"class.std::__2::allocator.32" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.39" = type { i8 }
%"class.std::__2::allocator.40" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.0" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.4" = type { i8 }
%"class.std::__2::allocator.5" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.12" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.20" = type { i8 }
%"struct.std::__2::integral_constant" = type { i8 }
%"struct.std::__2::__has_destroy" = type { i8 }
%"struct.std::__2::integral_constant.146" = type { i8 }
%"struct.std::__2::__has_destroy.147" = type { i8 }
%"struct.std::__2::__has_max_size" = type { i8 }
%"struct.std::__2::random_access_iterator_tag" = type { i8 }
%"struct.std::__2::__less" = type { i8 }
%"struct.std::__2::__less.150" = type { i8 }
%"struct.std::__2::vector<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>>>::_ConstructTransaction" = type { %"class.std::__2::vector.1"*, %"class.draco::IndexType"*, %"class.draco::IndexType"* }
%"struct.std::__2::__has_construct" = type { i8 }
%"struct.std::__2::__has_destroy.151" = type { i8 }
%"struct.std::__2::__has_max_size.152" = type { i8 }
%"class.std::__2::__split_buffer_common" = type { i8 }
%"struct.std::__2::__has_destroy.155" = type { i8 }
%"struct.std::__2::__has_destroy.159" = type { i8 }
%"class.std::__2::__bit_const_reference" = type { i32*, i32 }
%"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction" = type { %"class.std::__2::vector.15"*, %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"* }
%"struct.std::__2::__has_construct.160" = type { i8 }
%"struct.std::__2::__has_max_size.161" = type { i8 }
%"struct.std::__2::vector<draco::IndexType<unsigned int, draco::CornerIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::CornerIndex_tag_type_>>>::_ConstructTransaction" = type { %"class.std::__2::vector.7"*, %"class.draco::IndexType.9"*, %"class.draco::IndexType.9"* }
%"struct.std::__2::__has_construct.162" = type { i8 }
%"struct.std::__2::__has_max_size.163" = type { i8 }
%"struct.std::__2::__has_construct.164" = type { i8 }

$_ZNSt3__26vectorIbNS_9allocatorIbEEEC2Ev = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEEC2Ev = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEEC2Ev = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEEC2Ev = comdat any

$_ZN5draco12ValenceCacheINS_24MeshAttributeCornerTableEEC2ERKS1_ = comdat any

$_ZNK5draco12ValenceCacheINS_24MeshAttributeCornerTableEE17ClearValenceCacheEv = comdat any

$_ZNK5draco12ValenceCacheINS_24MeshAttributeCornerTableEE27ClearValenceCacheInaccurateEv = comdat any

$_ZNSt3__26vectorIbNS_9allocatorIbEEE6assignEmRKb = comdat any

$_ZNK5draco11CornerTable11num_cornersEv = comdat any

$_ZNK5draco11CornerTable12num_verticesEv = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE6assignEmRKS4_ = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7reserveEm = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE7reserveEm = comdat any

$_ZN5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEC2Ej = comdat any

$_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEltERKj = comdat any

$_ZNK5draco11CornerTable4FaceENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE = comdat any

$_ZNK5draco11CornerTable8OppositeENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE = comdat any

$_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEeqERKS2_ = comdat any

$_ZNSt3__26vectorIbNS_9allocatorIbEEEixEm = comdat any

$_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EE5valueEv = comdat any

$_ZNSt3__215__bit_referenceINS_6vectorIbNS_9allocatorIbEEEELb1EEaSEb = comdat any

$_ZN5draco9IndexTypeIjNS_21VertexIndex_tag_type_EEC2Ev = comdat any

$_ZNK5draco11CornerTable6VertexENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE = comdat any

$_ZNK5draco11CornerTable4NextENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE = comdat any

$_ZN5draco9IndexTypeIjNS_21VertexIndex_tag_type_EEaSERKS2_ = comdat any

$_ZNK5draco9IndexTypeIjNS_21VertexIndex_tag_type_EE5valueEv = comdat any

$_ZNK5draco11CornerTable8PreviousENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE = comdat any

$_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEltERKS2_ = comdat any

$_ZN5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEaSERKS2_ = comdat any

$_ZNK5draco4Mesh15CornerToPointIdEi = comdat any

$_ZNK5draco14PointAttribute12mapped_indexENS_9IndexTypeIjNS_20PointIndex_tag_type_EEE = comdat any

$_ZNK5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEneERKS2_ = comdat any

$_ZN5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEppEv = comdat any

$_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEneERKS2_ = comdat any

$_ZN5draco24MeshAttributeCornerTable25RecomputeVerticesInternalILb1EEEvPKNS_4MeshEPKNS_14PointAttributeE = comdat any

$_ZN5draco24MeshAttributeCornerTable25RecomputeVerticesInternalILb0EEEvPKNS_4MeshEPKNS_14PointAttributeE = comdat any

$_ZNK5draco9IndexTypeIjNS_21VertexIndex_tag_type_EEeqERKS2_ = comdat any

$_ZN5draco18VertexRingIteratorINS_24MeshAttributeCornerTableEEC2EPKS1_NS_9IndexTypeIjNS_21VertexIndex_tag_type_EEE = comdat any

$_ZNK5draco18VertexRingIteratorINS_24MeshAttributeCornerTableEE3EndEv = comdat any

$_ZN5draco18VertexRingIteratorINS_24MeshAttributeCornerTableEE4NextEv = comdat any

$_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_21CornerIndex_tag_type_EEENS1_IjNS_21VertexIndex_tag_type_EEEE4sizeEv = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv = comdat any

$_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_21VertexIndex_tag_type_EEENS1_IjNS_21CornerIndex_tag_type_EEEE4sizeEv = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv = comdat any

$_ZN5draco9IndexTypeIjNS_19FaceIndex_tag_type_EEC2Ej = comdat any

$_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_21CornerIndex_tag_type_EEES3_EixERKS3_ = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEEixEm = comdat any

$_ZNK5draco11CornerTable15ConfidentVertexENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE = comdat any

$_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_21CornerIndex_tag_type_EEENS1_IjNS_21VertexIndex_tag_type_EEEEixERKS3_ = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEEixEm = comdat any

$_ZNK5draco11CornerTable10LocalIndexENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE = comdat any

$_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEmiERKj = comdat any

$_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEplERKj = comdat any

$_ZNK5draco4Mesh4faceENS_9IndexTypeIjNS_19FaceIndex_tag_type_EEE = comdat any

$_ZNKSt3__25arrayIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELm3EEixEm = comdat any

$_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEENSt3__25arrayINS1_IjNS_20PointIndex_tag_type_EEELm3EEEEixERKS3_ = comdat any

$_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEEixEm = comdat any

$_ZNK5draco9IndexTypeIjNS_19FaceIndex_tag_type_EE5valueEv = comdat any

$_ZNK5draco9IndexTypeIjNS_20PointIndex_tag_type_EE5valueEv = comdat any

$_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEC2Ej = comdat any

$_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEEEixERKS3_ = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEEixEm = comdat any

$_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_21VertexIndex_tag_type_EEEaEC2Ev = comdat any

$_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_21VertexIndex_tag_type_EEEiEC2Ev = comdat any

$_ZNSt3__26vectorIaNS_9allocatorIaEEEC2Ev = comdat any

$_ZNSt3__213__vector_baseIaNS_9allocatorIaEEEC2Ev = comdat any

$_ZNSt3__220__vector_base_commonILb1EEC2Ev = comdat any

$_ZNSt3__217__compressed_pairIPaNS_9allocatorIaEEEC2IDnNS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPaLi0ELb0EEC2IDnvEEOT_ = comdat any

$_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIaEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__29allocatorIaEC2Ev = comdat any

$_ZNSt3__26vectorIiNS_9allocatorIiEEEC2Ev = comdat any

$_ZNSt3__213__vector_baseIiNS_9allocatorIiEEEC2Ev = comdat any

$_ZNSt3__217__compressed_pairIPiNS_9allocatorIiEEEC2IDnNS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__222__compressed_pair_elemIPiLi0ELb0EEC2IDnvEEOT_ = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIiEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__29allocatorIiEC2Ev = comdat any

$_ZNSt3__217__compressed_pairImNS_9allocatorImEEEC2IiNS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__27forwardIiEEOT_RNS_16remove_referenceIS1_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemImLi0ELb0EEC2IivEEOT_ = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorImEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__29allocatorImEC2Ev = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEEC2Ev = comdat any

$_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEEC2IDnNS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEELi0ELb0EEC2IDnvEEOT_ = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEEEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEEEC2Ev = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEEC2Ev = comdat any

$_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEEC2IDnNS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEELi0ELb0EEC2IDnvEEOT_ = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_21CornerIndex_tag_type_EEEEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEEEC2Ev = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEEC2Ev = comdat any

$_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEEC2IDnNS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELi0ELb0EEC2IDnvEEOT_ = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEC2Ev = comdat any

$_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_21VertexIndex_tag_type_EEEiE5clearEv = comdat any

$_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_21VertexIndex_tag_type_EEEiE4swapERS4_ = comdat any

$_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_21VertexIndex_tag_type_EEEiED2Ev = comdat any

$_ZNSt3__26vectorIiNS_9allocatorIiEEE5clearEv = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEE4sizeEv = comdat any

$_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE5clearEv = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEE17__annotate_shrinkEm = comdat any

$_ZNSt3__26vectorIiNS_9allocatorIiEEE26__invalidate_all_iteratorsEv = comdat any

$_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE17__destruct_at_endEPi = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIiEEE7destroyIiEEvRS2_PT_ = comdat any

$_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE7__allocEv = comdat any

$_ZNSt3__212__to_addressIiEEPT_S2_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIiEEE9__destroyIiEEvNS_17integral_constantIbLb1EEERS2_PT_ = comdat any

$_ZNSt3__29allocatorIiE7destroyEPi = comdat any

$_ZNSt3__217__compressed_pairIPiNS_9allocatorIiEEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIiEELi1ELb1EE5__getEv = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEE31__annotate_contiguous_containerEPKvS5_S5_S5_ = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPiNS_9allocatorIiEEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPiLi0ELb0EE5__getEv = comdat any

$_ZNSt3__26vectorIiNS_9allocatorIiEEE4swapERS3_ = comdat any

$_ZNSt3__24swapIPiEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_ = comdat any

$_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE9__end_capEv = comdat any

$_ZNSt3__216__swap_allocatorINS_9allocatorIiEEEEvRT_S4_NS_17integral_constantIbLb0EEE = comdat any

$_ZNSt3__24moveIRPiEEONS_16remove_referenceIT_E4typeEOS4_ = comdat any

$_ZNSt3__217__compressed_pairIPiNS_9allocatorIiEEE5firstEv = comdat any

$_ZNSt3__222__compressed_pair_elemIPiLi0ELb0EE5__getEv = comdat any

$_ZNSt3__26vectorIiNS_9allocatorIiEEED2Ev = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEE17__annotate_deleteEv = comdat any

$_ZNSt3__213__vector_baseIiNS_9allocatorIiEEED2Ev = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIiEEE10deallocateERS2_Pim = comdat any

$_ZNSt3__29allocatorIiE10deallocateEPim = comdat any

$_ZNSt3__219__libcpp_deallocateEPvmm = comdat any

$_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm = comdat any

$_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm = comdat any

$_ZNSt3__217_DeallocateCaller9__do_callEPv = comdat any

$_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_21VertexIndex_tag_type_EEEaE5clearEv = comdat any

$_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_21VertexIndex_tag_type_EEEaE4swapERS4_ = comdat any

$_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_21VertexIndex_tag_type_EEEaED2Ev = comdat any

$_ZNSt3__26vectorIaNS_9allocatorIaEEE5clearEv = comdat any

$_ZNKSt3__26vectorIaNS_9allocatorIaEEE4sizeEv = comdat any

$_ZNSt3__213__vector_baseIaNS_9allocatorIaEEE5clearEv = comdat any

$_ZNKSt3__26vectorIaNS_9allocatorIaEEE17__annotate_shrinkEm = comdat any

$_ZNSt3__26vectorIaNS_9allocatorIaEEE26__invalidate_all_iteratorsEv = comdat any

$_ZNSt3__213__vector_baseIaNS_9allocatorIaEEE17__destruct_at_endEPa = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIaEEE7destroyIaEEvRS2_PT_ = comdat any

$_ZNSt3__213__vector_baseIaNS_9allocatorIaEEE7__allocEv = comdat any

$_ZNSt3__212__to_addressIaEEPT_S2_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIaEEE9__destroyIaEEvNS_17integral_constantIbLb1EEERS2_PT_ = comdat any

$_ZNSt3__29allocatorIaE7destroyEPa = comdat any

$_ZNSt3__217__compressed_pairIPaNS_9allocatorIaEEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIaEELi1ELb1EE5__getEv = comdat any

$_ZNKSt3__26vectorIaNS_9allocatorIaEEE31__annotate_contiguous_containerEPKvS5_S5_S5_ = comdat any

$_ZNKSt3__26vectorIaNS_9allocatorIaEEE4dataEv = comdat any

$_ZNKSt3__26vectorIaNS_9allocatorIaEEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseIaNS_9allocatorIaEEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseIaNS_9allocatorIaEEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPaNS_9allocatorIaEEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPaLi0ELb0EE5__getEv = comdat any

$_ZNSt3__26vectorIaNS_9allocatorIaEEE4swapERS3_ = comdat any

$_ZNSt3__24swapIPaEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_ = comdat any

$_ZNSt3__213__vector_baseIaNS_9allocatorIaEEE9__end_capEv = comdat any

$_ZNSt3__216__swap_allocatorINS_9allocatorIaEEEEvRT_S4_NS_17integral_constantIbLb0EEE = comdat any

$_ZNSt3__24moveIRPaEEONS_16remove_referenceIT_E4typeEOS4_ = comdat any

$_ZNSt3__217__compressed_pairIPaNS_9allocatorIaEEE5firstEv = comdat any

$_ZNSt3__222__compressed_pair_elemIPaLi0ELb0EE5__getEv = comdat any

$_ZNSt3__26vectorIaNS_9allocatorIaEEED2Ev = comdat any

$_ZNKSt3__26vectorIaNS_9allocatorIaEEE17__annotate_deleteEv = comdat any

$_ZNSt3__213__vector_baseIaNS_9allocatorIaEEED2Ev = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIaEEE10deallocateERS2_Pam = comdat any

$_ZNSt3__29allocatorIaE10deallocateEPam = comdat any

$_ZNKSt3__26vectorIbNS_9allocatorIbEEE8capacityEv = comdat any

$_ZNSt3__26vectorIbNS_9allocatorIbEEE7__allocEv = comdat any

$_ZNSt3__29allocatorIbEC2ImEERKNS0_IT_EE = comdat any

$_ZNSt3__26vectorIbNS_9allocatorIbEEEC2ERKS2_ = comdat any

$_ZNSt3__26vectorIbNS_9allocatorIbEEE7reserveEm = comdat any

$_ZNKSt3__26vectorIbNS_9allocatorIbEEE11__recommendEm = comdat any

$_ZNSt3__26vectorIbNS_9allocatorIbEEE4swapERS3_ = comdat any

$_ZNSt3__26vectorIbNS_9allocatorIbEEED2Ev = comdat any

$_ZNSt3__26fill_nINS_6vectorIbNS_9allocatorIbEEEEEEvNS_14__bit_iteratorIT_Lb0EXLi0EEEENS6_9size_typeEb = comdat any

$_ZNSt3__26vectorIbNS_9allocatorIbEEE5beginEv = comdat any

$_ZNSt3__26vectorIbNS_9allocatorIbEEE26__invalidate_all_iteratorsEv = comdat any

$_ZNSt3__26vectorIbNS_9allocatorIbEEE26__internal_cap_to_externalEm = comdat any

$_ZNKSt3__26vectorIbNS_9allocatorIbEEE5__capEv = comdat any

$_ZNKSt3__217__compressed_pairImNS_9allocatorImEEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemImLi0ELb0EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairImNS_9allocatorImEEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorImEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__29allocatorImEC2IbEERKNS0_IT_EE = comdat any

$_ZNSt3__217__compressed_pairImNS_9allocatorImEEEC2IiS2_EEOT_OT0_ = comdat any

$_ZNSt3__27forwardINS_9allocatorImEEEEOT_RNS_16remove_referenceIS3_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorImEELi1ELb1EEC2IS2_vEEOT_ = comdat any

$_ZNSt3__26vectorIbNS_9allocatorIbEEE11__vallocateEm = comdat any

$_ZNSt3__26vectorIbNS_9allocatorIbEEE18__construct_at_endINS_14__bit_iteratorIS3_Lb0ELm0EEEEENS_9enable_ifIXsr27__is_cpp17_forward_iteratorIT_EE5valueEvE4typeES8_S8_ = comdat any

$_ZNSt3__26vectorIbNS_9allocatorIbEEE3endEv = comdat any

$_ZNKSt3__26vectorIbNS_9allocatorIbEEE8max_sizeEv = comdat any

$_ZNSt3__26vectorIbNS_9allocatorIbEEE26__external_cap_to_internalEm = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorImEEE8allocateERS2_m = comdat any

$_ZNSt3__26vectorIbNS_9allocatorIbEEE5__capEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorImEEE8max_sizeERKS2_ = comdat any

$_ZNKSt3__26vectorIbNS_9allocatorIbEEE7__allocEv = comdat any

$_ZNSt3__214numeric_limitsImE3maxEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorImEEE10__max_sizeENS_17integral_constantIbLb1EEERKS2_ = comdat any

$_ZNKSt3__29allocatorImE8max_sizeEv = comdat any

$_ZNKSt3__217__compressed_pairImNS_9allocatorImEEE6secondEv = comdat any

$_ZNKSt3__222__compressed_pair_elemINS_9allocatorImEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__223__libcpp_numeric_limitsImLb1EE3maxEv = comdat any

$_ZNSt3__29allocatorImE8allocateEmPKv = comdat any

$_ZNSt3__220__throw_length_errorEPKc = comdat any

$_ZNSt3__217__libcpp_allocateEmm = comdat any

$_ZNSt3__217__compressed_pairImNS_9allocatorImEEE5firstEv = comdat any

$_ZNSt3__222__compressed_pair_elemImLi0ELb0EE5__getEv = comdat any

$_ZNSt3__28distanceINS_14__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb0ELm0EEEEENS_15iterator_traitsIT_E15difference_typeES8_S8_ = comdat any

$_ZNSt3__214__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb0ELm0EEC2ERKS5_ = comdat any

$_ZNSt3__24copyINS_6vectorIbNS_9allocatorIbEEEELb0EEENS_14__bit_iteratorIT_Lb0EXLi0EEEENS5_IS6_XT0_EXLi0EEEES8_S7_ = comdat any

$_ZNSt3__26vectorIbNS_9allocatorIbEEE11__make_iterEm = comdat any

$_ZNSt3__210__distanceINS_14__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb0ELm0EEEEENS_15iterator_traitsIT_E15difference_typeES8_S8_NS_26random_access_iterator_tagE = comdat any

$_ZNSt3__2miERKNS_14__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb0ELm0EEES7_ = comdat any

$_ZNSt3__214__copy_alignedINS_6vectorIbNS_9allocatorIbEEEELb0EEENS_14__bit_iteratorIT_Lb0EXLi0EEEENS5_IS6_XT0_EXLi0EEEES8_S7_ = comdat any

$_ZNSt3__216__copy_unalignedINS_6vectorIbNS_9allocatorIbEEEELb0EEENS_14__bit_iteratorIT_Lb0EXLi0EEEENS5_IS6_XT0_EXLi0EEEES8_S7_ = comdat any

$_ZNSt3__23minIlEERKT_S3_S3_ = comdat any

$_ZNSt3__212__to_addressImEEPT_S2_ = comdat any

$_ZNSt3__23minIlNS_6__lessIllEEEERKT_S5_S5_T0_ = comdat any

$_ZNKSt3__26__lessIllEclERKlS3_ = comdat any

$_ZNSt3__23minImEERKT_S3_S3_ = comdat any

$_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_ = comdat any

$_ZNKSt3__26__lessImmEclERKmS3_ = comdat any

$_ZNSt3__214__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb0ELm0EEC2EPmj = comdat any

$_ZNSt3__23maxImEERKT_S3_S3_ = comdat any

$_ZNSt3__26vectorIbNS_9allocatorIbEEE10__align_itEm = comdat any

$_ZNSt3__23maxImNS_6__lessImmEEEERKT_S5_S5_T0_ = comdat any

$_ZNSt3__24swapIPmEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_ = comdat any

$_ZNSt3__24swapImEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS2_EE5valueEvE4typeERS2_S5_ = comdat any

$_ZNSt3__216__swap_allocatorINS_9allocatorImEEEEvRT_S4_NS_17integral_constantIbLb0EEE = comdat any

$_ZNSt3__24moveIRPmEEONS_16remove_referenceIT_E4typeEOS4_ = comdat any

$_ZNSt3__24moveIRmEEONS_16remove_referenceIT_E4typeEOS3_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorImEEE10deallocateERS2_Pmm = comdat any

$_ZNSt3__29allocatorImE10deallocateEPmm = comdat any

$_ZNSt3__213__fill_n_trueINS_6vectorIbNS_9allocatorIbEEEEEEvNS_14__bit_iteratorIT_Lb0EXLi0EEEENS6_9size_typeE = comdat any

$_ZNSt3__214__fill_n_falseINS_6vectorIbNS_9allocatorIbEEEEEEvNS_14__bit_iteratorIT_Lb0EXLi0EEEENS6_9size_typeE = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv = comdat any

$_ZNSt3__26fill_nIPN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEEmS4_EET_S6_T0_RKT1_ = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE18__construct_at_endEmRKS4_ = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_ = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE13__vdeallocateEv = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE11__vallocateEm = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE11__recommendEm = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE26__invalidate_all_iteratorsEv = comdat any

$_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__28__fill_nIPN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEEmS4_EET_S6_T0_RKT1_ = comdat any

$_ZNSt3__221__convert_to_integralEm = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionC2ERS7_m = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEEEEE9constructIS5_JRKS5_EEEvRS6_PT_DpOT0_ = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv = comdat any

$_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEEEEPT_S6_ = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionD2Ev = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEEEEE11__constructIS5_JRKS5_EEEvNS_17integral_constantIbLb1EEERS6_PT_DpOT0_ = comdat any

$_ZNSt3__27forwardIRKN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEEEEOT_RNS_16remove_referenceIS7_E4typeE = comdat any

$_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEEE9constructIS4_JRKS4_EEEvPT_DpOT0_ = comdat any

$_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE27__invalidate_iterators_pastEPS4_ = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_ = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_shrinkEm = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEEEEE7destroyIS5_EEvRS6_PT_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEEEEE9__destroyIS5_EEvNS_17integral_constantIbLb1EEERS6_PT_ = comdat any

$_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEEE7destroyEPS4_ = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_ = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE5clearEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEEEEE10deallocateERS6_PS5_m = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE5clearEv = comdat any

$_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEEE10deallocateEPS4_m = comdat any

$_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEELi0ELb0EE5__getEv = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE8max_sizeEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEEEEE8allocateERS6_m = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE14__annotate_newEm = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEEEEE8max_sizeERKS6_ = comdat any

$_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv = comdat any

$_ZNSt3__214numeric_limitsIlE3maxEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS6_ = comdat any

$_ZNKSt3__29allocatorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEEE8max_sizeEv = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv = comdat any

$_ZNKSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv = comdat any

$_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEEE8allocateEmPKv = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEEC2EmmS7_ = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS4_RS6_EE = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEED2Ev = comdat any

$_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEEC2IDnS8_EEOT_OT0_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE8allocateERS6_m = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE7__allocEv = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE9__end_capEv = comdat any

$_ZNSt3__27forwardIRNS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEEEOT_RNS_16remove_referenceIS8_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb0EEC2IS7_vEEOT_ = comdat any

$_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE8allocateEmPKv = comdat any

$_ZNKSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE8max_sizeEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb0EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE5firstEv = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELi0ELb0EE5__getEv = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_deleteEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE46__construct_backward_with_exception_guaranteesIS5_EENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS6_PT_SC_EE5valuesr31is_trivially_move_constructibleISC_EE5valueEvE4typeERS6_SD_SD_RSD_ = comdat any

$_ZNSt3__24swapIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS7_EE5valueEvE4typeERS7_SA_ = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE14__annotate_newEm = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE26__invalidate_all_iteratorsEv = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_ = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv = comdat any

$_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEPT_S6_ = comdat any

$_ZNSt3__24moveIRPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEONS_16remove_referenceIT_E4typeEOS8_ = comdat any

$_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE5clearEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE10deallocateERS6_PS5_m = comdat any

$_ZNKSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE8capacityEv = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE17__destruct_at_endEPS4_ = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE17__destruct_at_endEPS4_NS_17integral_constantIbLb0EEE = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE7destroyIS5_EEvRS6_PT_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE9__destroyIS5_EEvNS_17integral_constantIbLb1EEERS6_PT_ = comdat any

$_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE7destroyEPS4_ = comdat any

$_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE10deallocateEPS4_m = comdat any

$_ZNKSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE5firstEv = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEERNS_9allocatorIS4_EEEC2EmmS7_ = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS4_RS6_EE = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEERNS_9allocatorIS4_EEED2Ev = comdat any

$_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_21CornerIndex_tag_type_EEEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEERNS_9allocatorIS4_EEEC2IDnS8_EEOT_OT0_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_21CornerIndex_tag_type_EEEEEE8allocateERS6_m = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEERNS_9allocatorIS4_EEE7__allocEv = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEERNS_9allocatorIS4_EEE9__end_capEv = comdat any

$_ZNSt3__27forwardIRNS_9allocatorIN5draco9IndexTypeIjNS2_21CornerIndex_tag_type_EEEEEEEOT_RNS_16remove_referenceIS8_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIN5draco9IndexTypeIjNS2_21CornerIndex_tag_type_EEEEELi1ELb0EEC2IS7_vEEOT_ = comdat any

$_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEEE8allocateEmPKv = comdat any

$_ZNKSt3__29allocatorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEEE8max_sizeEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEERNS_9allocatorIS4_EEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIN5draco9IndexTypeIjNS2_21CornerIndex_tag_type_EEEEELi1ELb0EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEERNS_9allocatorIS4_EEE5firstEv = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEELi0ELb0EE5__getEv = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_deleteEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_21CornerIndex_tag_type_EEEEEE46__construct_backward_with_exception_guaranteesIS5_EENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS6_PT_SC_EE5valuesr31is_trivially_move_constructibleISC_EE5valueEvE4typeERS6_SD_SD_RSD_ = comdat any

$_ZNSt3__24swapIPN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS7_EE5valueEvE4typeERS7_SA_ = comdat any

$_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE14__annotate_newEm = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE26__invalidate_all_iteratorsEv = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_ = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv = comdat any

$_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEEEEPT_S6_ = comdat any

$_ZNSt3__24moveIRPN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEEEEONS_16remove_referenceIT_E4typeEOS8_ = comdat any

$_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEERNS_9allocatorIS4_EEE5clearEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_21CornerIndex_tag_type_EEEEEE10deallocateERS6_PS5_m = comdat any

$_ZNKSt3__214__split_bufferIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEERNS_9allocatorIS4_EEE8capacityEv = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEERNS_9allocatorIS4_EEE17__destruct_at_endEPS4_ = comdat any

$_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEERNS_9allocatorIS4_EEE17__destruct_at_endEPS4_NS_17integral_constantIbLb0EEE = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_21CornerIndex_tag_type_EEEEEE7destroyIS5_EEvRS6_PT_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_21CornerIndex_tag_type_EEEEEE9__destroyIS5_EEvNS_17integral_constantIbLb1EEERS6_PT_ = comdat any

$_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEEE7destroyEPS4_ = comdat any

$_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEEE10deallocateEPS4_m = comdat any

$_ZNKSt3__214__split_bufferIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEERNS_9allocatorIS4_EEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEERNS_9allocatorIS4_EEE5firstEv = comdat any

$_ZNSt3__26vectorIbNS_9allocatorIbEEE10__make_refEm = comdat any

$_ZNSt3__215__bit_referenceINS_6vectorIbNS_9allocatorIbEEEELb1EEC2EPmm = comdat any

$_ZN5draco9IndexTypeIjNS_21VertexIndex_tag_type_EEC2Ej = comdat any

$_ZNK5draco9IndexTypeIjNS_21VertexIndex_tag_type_EEltERKj = comdat any

$_ZNK5draco11CornerTable14LeftMostCornerENS_9IndexTypeIjNS_21VertexIndex_tag_type_EEE = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE9push_backEOS4_ = comdat any

$_ZN5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEC2Ev = comdat any

$_ZNKSt3__215__bit_referenceINS_6vectorIbNS_9allocatorIbEEEELb1EEcvbEv = comdat any

$_ZNK5draco24MeshAttributeCornerTable9SwingLeftENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE = comdat any

$_ZNK5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EE5valueEv = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEEixEm = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE9push_backERKS4_ = comdat any

$_ZNK5draco11CornerTable10SwingRightENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE = comdat any

$_ZNK5draco24MeshAttributeCornerTable26IsCornerOppositeToSeamEdgeENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE = comdat any

$_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEaSERKS2_ = comdat any

$_ZN5draco9IndexTypeIjNS_21VertexIndex_tag_type_EEppEv = comdat any

$_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_21VertexIndex_tag_type_EEENS1_IjNS_21CornerIndex_tag_type_EEEEixERKS3_ = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE22__construct_one_at_endIJS4_EEEvDpOT_ = comdat any

$_ZNSt3__24moveIRN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEONS_16remove_referenceIT_E4typeEOS7_ = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE21__push_back_slow_pathIS4_EEvOT_ = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionC2ERS7_m = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE9constructIS5_JS5_EEEvRS6_PT_DpOT0_ = comdat any

$_ZNSt3__27forwardIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEOT_RNS_16remove_referenceIS5_E4typeE = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionD2Ev = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE11__constructIS5_JS5_EEEvNS_17integral_constantIbLb1EEERS6_PT_DpOT0_ = comdat any

$_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE9constructIS4_JS4_EEEvPT_DpOT0_ = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE11__recommendEm = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8max_sizeEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE8max_sizeERKS6_ = comdat any

$_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS6_ = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv = comdat any

$_ZNKSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb1EE5__getEv = comdat any

$_ZNK5draco24MeshAttributeCornerTable4NextENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE = comdat any

$_ZNK5draco24MeshAttributeCornerTable8OppositeENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE22__construct_one_at_endIJRKS4_EEEvDpOT_ = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE21__push_back_slow_pathIRKS4_EEvOT_ = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionC2ERS7_m = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_21CornerIndex_tag_type_EEEEEE9constructIS5_JRKS5_EEEvRS6_PT_DpOT0_ = comdat any

$_ZNSt3__27forwardIRKN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEEEEOT_RNS_16remove_referenceIS7_E4typeE = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionD2Ev = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_21CornerIndex_tag_type_EEEEEE11__constructIS5_JRKS5_EEEvNS_17integral_constantIbLb1EEERS6_PT_DpOT0_ = comdat any

$_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEEE9constructIS4_JRKS4_EEEvPT_DpOT0_ = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE11__recommendEm = comdat any

$_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE8max_sizeEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_21CornerIndex_tag_type_EEEEEE8max_sizeERKS6_ = comdat any

$_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_21CornerIndex_tag_type_EEEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS6_ = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv = comdat any

$_ZNKSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_21CornerIndex_tag_type_EEEEELi1ELb1EE5__getEv = comdat any

$_ZNKSt3__26vectorIbNS_9allocatorIbEEEixEm = comdat any

$_ZNKSt3__221__bit_const_referenceINS_6vectorIbNS_9allocatorIbEEEEEcvbEv = comdat any

$_ZNKSt3__26vectorIbNS_9allocatorIbEEE10__make_refEm = comdat any

$_ZNSt3__221__bit_const_referenceINS_6vectorIbNS_9allocatorIbEEEEEC2EPKmm = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE9push_backERKS4_ = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE22__construct_one_at_endIJRKS4_EEEvDpOT_ = comdat any

$_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE21__push_back_slow_pathIRKS4_EEvOT_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE9constructIS5_JRKS5_EEEvRS6_PT_DpOT0_ = comdat any

$_ZNSt3__27forwardIRKN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEOT_RNS_16remove_referenceIS7_E4typeE = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE11__constructIS5_JRKS5_EEEvNS_17integral_constantIbLb1EEERS6_PT_DpOT0_ = comdat any

$_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE9constructIS4_JRKS4_EEEvPT_DpOT0_ = comdat any

$_ZNK5draco24MeshAttributeCornerTable14LeftMostCornerENS_9IndexTypeIjNS_21VertexIndex_tag_type_EEE = comdat any

$_ZNK5draco24MeshAttributeCornerTable10SwingRightENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE = comdat any

$_ZNK5draco24MeshAttributeCornerTable8PreviousENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE = comdat any

$_ZZNSt3__216__copy_unalignedINS_6vectorIbNS_9allocatorIbEEEELb0EEENS_14__bit_iteratorIT_Lb0EXLi0EEEENS5_IS6_XT0_EXLi0EEEES8_S7_E15__bits_per_word = comdat any

@_ZN5dracoL19kInvalidVertexIndexE = internal constant %"class.draco::IndexType" { i32 -1 }, align 4
@_ZN5dracoL19kInvalidCornerIndexE = internal constant %"class.draco::IndexType.9" { i32 -1 }, align 4
@_ZN5dracoL17kInvalidFaceIndexE = internal constant %"class.draco::IndexType.145" { i32 -1 }, align 4
@_ZN5dracoL18kInvalidPointIndexE = internal constant %"class.draco::IndexType.139" { i32 -1 }, align 4
@.str = private unnamed_addr constant [68 x i8] c"allocator<T>::allocate(size_t n) 'n' exceeds maximum supported size\00", align 1
@_ZZNSt3__216__copy_unalignedINS_6vectorIbNS_9allocatorIbEEEELb0EEENS_14__bit_iteratorIT_Lb0EXLi0EEEENS5_IS6_XT0_EXLi0EEEES8_S7_E15__bits_per_word = linkonce_odr hidden constant i32 32, comdat, align 4

@_ZN5draco24MeshAttributeCornerTableC1Ev = hidden unnamed_addr alias %"class.draco::MeshAttributeCornerTable"* (%"class.draco::MeshAttributeCornerTable"*), %"class.draco::MeshAttributeCornerTable"* (%"class.draco::MeshAttributeCornerTable"*)* @_ZN5draco24MeshAttributeCornerTableC2Ev

; Function Attrs: noinline nounwind optnone
define hidden %"class.draco::MeshAttributeCornerTable"* @_ZN5draco24MeshAttributeCornerTableC2Ev(%"class.draco::MeshAttributeCornerTable"* returned %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %"class.draco::MeshAttributeCornerTable"*, align 4
  store %"class.draco::MeshAttributeCornerTable"* %this, %"class.draco::MeshAttributeCornerTable"** %this.addr, align 4
  %this1 = load %"class.draco::MeshAttributeCornerTable"*, %"class.draco::MeshAttributeCornerTable"** %this.addr, align 4
  %is_edge_on_seam_ = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::vector"* @_ZNSt3__26vectorIbNS_9allocatorIbEEEC2Ev(%"class.std::__2::vector"* %is_edge_on_seam_) #8
  %is_vertex_on_seam_ = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 1
  %call2 = call %"class.std::__2::vector"* @_ZNSt3__26vectorIbNS_9allocatorIbEEEC2Ev(%"class.std::__2::vector"* %is_vertex_on_seam_) #8
  %no_interior_seams_ = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 2
  store i8 1, i8* %no_interior_seams_, align 4
  %corner_to_vertex_map_ = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 3
  %call3 = call %"class.std::__2::vector.1"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEEC2Ev(%"class.std::__2::vector.1"* %corner_to_vertex_map_) #8
  %vertex_to_left_most_corner_map_ = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 4
  %call4 = call %"class.std::__2::vector.7"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEEC2Ev(%"class.std::__2::vector.7"* %vertex_to_left_most_corner_map_) #8
  %vertex_to_attribute_entry_id_map_ = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 5
  %call5 = call %"class.std::__2::vector.15"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEEC2Ev(%"class.std::__2::vector.15"* %vertex_to_attribute_entry_id_map_) #8
  %corner_table_ = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 6
  store %"class.draco::CornerTable"* null, %"class.draco::CornerTable"** %corner_table_, align 4
  %valence_cache_ = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 7
  %call6 = call %"class.draco::ValenceCache.42"* @_ZN5draco12ValenceCacheINS_24MeshAttributeCornerTableEEC2ERKS1_(%"class.draco::ValenceCache.42"* %valence_cache_, %"class.draco::MeshAttributeCornerTable"* nonnull align 4 dereferenceable(96) %this1)
  ret %"class.draco::MeshAttributeCornerTable"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector"* @_ZNSt3__26vectorIbNS_9allocatorIbEEEC2Ev(%"class.std::__2::vector"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp2 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base_common"*
  %call = call %"class.std::__2::__vector_base_common"* @_ZNSt3__220__vector_base_commonILb1EEC2Ev(%"class.std::__2::__vector_base_common"* %0)
  %__begin_ = getelementptr inbounds %"class.std::__2::vector", %"class.std::__2::vector"* %this1, i32 0, i32 0
  store i32* null, i32** %__begin_, align 4
  %__size_ = getelementptr inbounds %"class.std::__2::vector", %"class.std::__2::vector"* %this1, i32 0, i32 1
  store i32 0, i32* %__size_, align 4
  %__cap_alloc_ = getelementptr inbounds %"class.std::__2::vector", %"class.std::__2::vector"* %this1, i32 0, i32 2
  store i32 0, i32* %ref.tmp, align 4
  %call3 = call %"class.std::__2::__compressed_pair"* @_ZNSt3__217__compressed_pairImNS_9allocatorImEEEC2IiNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair"* %__cap_alloc_, i32* nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  ret %"class.std::__2::vector"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector.1"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEEC2Ev(%"class.std::__2::vector.1"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.1"*, align 4
  store %"class.std::__2::vector.1"* %this, %"class.std::__2::vector.1"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base"*
  %call = call %"class.std::__2::__vector_base"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEEC2Ev(%"class.std::__2::__vector_base"* %0) #8
  ret %"class.std::__2::vector.1"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector.7"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEEC2Ev(%"class.std::__2::vector.7"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.7"*, align 4
  store %"class.std::__2::vector.7"* %this, %"class.std::__2::vector.7"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.7"*, %"class.std::__2::vector.7"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.7"* %this1 to %"class.std::__2::__vector_base.8"*
  %call = call %"class.std::__2::__vector_base.8"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEEC2Ev(%"class.std::__2::__vector_base.8"* %0) #8
  ret %"class.std::__2::vector.7"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector.15"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEEC2Ev(%"class.std::__2::vector.15"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.15"*, align 4
  store %"class.std::__2::vector.15"* %this, %"class.std::__2::vector.15"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.15"*, %"class.std::__2::vector.15"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.15"* %this1 to %"class.std::__2::__vector_base.16"*
  %call = call %"class.std::__2::__vector_base.16"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEEC2Ev(%"class.std::__2::__vector_base.16"* %0) #8
  ret %"class.std::__2::vector.15"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::ValenceCache.42"* @_ZN5draco12ValenceCacheINS_24MeshAttributeCornerTableEEC2ERKS1_(%"class.draco::ValenceCache.42"* returned %this, %"class.draco::MeshAttributeCornerTable"* nonnull align 4 dereferenceable(96) %table) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::ValenceCache.42"*, align 4
  %table.addr = alloca %"class.draco::MeshAttributeCornerTable"*, align 4
  store %"class.draco::ValenceCache.42"* %this, %"class.draco::ValenceCache.42"** %this.addr, align 4
  store %"class.draco::MeshAttributeCornerTable"* %table, %"class.draco::MeshAttributeCornerTable"** %table.addr, align 4
  %this1 = load %"class.draco::ValenceCache.42"*, %"class.draco::ValenceCache.42"** %this.addr, align 4
  %table_ = getelementptr inbounds %"class.draco::ValenceCache.42", %"class.draco::ValenceCache.42"* %this1, i32 0, i32 0
  %0 = load %"class.draco::MeshAttributeCornerTable"*, %"class.draco::MeshAttributeCornerTable"** %table.addr, align 4
  store %"class.draco::MeshAttributeCornerTable"* %0, %"class.draco::MeshAttributeCornerTable"** %table_, align 4
  %vertex_valence_cache_8_bit_ = getelementptr inbounds %"class.draco::ValenceCache.42", %"class.draco::ValenceCache.42"* %this1, i32 0, i32 1
  %call = call %"class.draco::IndexTypeVector.26"* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_21VertexIndex_tag_type_EEEaEC2Ev(%"class.draco::IndexTypeVector.26"* %vertex_valence_cache_8_bit_)
  %vertex_valence_cache_32_bit_ = getelementptr inbounds %"class.draco::ValenceCache.42", %"class.draco::ValenceCache.42"* %this1, i32 0, i32 2
  %call2 = call %"class.draco::IndexTypeVector.34"* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_21VertexIndex_tag_type_EEEiEC2Ev(%"class.draco::IndexTypeVector.34"* %vertex_valence_cache_32_bit_)
  ret %"class.draco::ValenceCache.42"* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden zeroext i1 @_ZN5draco24MeshAttributeCornerTable9InitEmptyEPKNS_11CornerTableE(%"class.draco::MeshAttributeCornerTable"* %this, %"class.draco::CornerTable"* %table) #0 {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::MeshAttributeCornerTable"*, align 4
  %table.addr = alloca %"class.draco::CornerTable"*, align 4
  %ref.tmp = alloca i8, align 1
  %ref.tmp4 = alloca i8, align 1
  store %"class.draco::MeshAttributeCornerTable"* %this, %"class.draco::MeshAttributeCornerTable"** %this.addr, align 4
  store %"class.draco::CornerTable"* %table, %"class.draco::CornerTable"** %table.addr, align 4
  %this1 = load %"class.draco::MeshAttributeCornerTable"*, %"class.draco::MeshAttributeCornerTable"** %this.addr, align 4
  %0 = load %"class.draco::CornerTable"*, %"class.draco::CornerTable"** %table.addr, align 4
  %cmp = icmp eq %"class.draco::CornerTable"* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %valence_cache_ = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 7
  call void @_ZNK5draco12ValenceCacheINS_24MeshAttributeCornerTableEE17ClearValenceCacheEv(%"class.draco::ValenceCache.42"* %valence_cache_)
  %valence_cache_2 = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 7
  call void @_ZNK5draco12ValenceCacheINS_24MeshAttributeCornerTableEE27ClearValenceCacheInaccurateEv(%"class.draco::ValenceCache.42"* %valence_cache_2)
  %is_edge_on_seam_ = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 0
  %1 = load %"class.draco::CornerTable"*, %"class.draco::CornerTable"** %table.addr, align 4
  %call = call i32 @_ZNK5draco11CornerTable11num_cornersEv(%"class.draco::CornerTable"* %1)
  store i8 0, i8* %ref.tmp, align 1
  call void @_ZNSt3__26vectorIbNS_9allocatorIbEEE6assignEmRKb(%"class.std::__2::vector"* %is_edge_on_seam_, i32 %call, i8* nonnull align 1 dereferenceable(1) %ref.tmp)
  %is_vertex_on_seam_ = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 1
  %2 = load %"class.draco::CornerTable"*, %"class.draco::CornerTable"** %table.addr, align 4
  %call3 = call i32 @_ZNK5draco11CornerTable12num_verticesEv(%"class.draco::CornerTable"* %2)
  store i8 0, i8* %ref.tmp4, align 1
  call void @_ZNSt3__26vectorIbNS_9allocatorIbEEE6assignEmRKb(%"class.std::__2::vector"* %is_vertex_on_seam_, i32 %call3, i8* nonnull align 1 dereferenceable(1) %ref.tmp4)
  %corner_to_vertex_map_ = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 3
  %3 = load %"class.draco::CornerTable"*, %"class.draco::CornerTable"** %table.addr, align 4
  %call5 = call i32 @_ZNK5draco11CornerTable11num_cornersEv(%"class.draco::CornerTable"* %3)
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE6assignEmRKS4_(%"class.std::__2::vector.1"* %corner_to_vertex_map_, i32 %call5, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) @_ZN5dracoL19kInvalidVertexIndexE)
  %vertex_to_attribute_entry_id_map_ = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 5
  %4 = load %"class.draco::CornerTable"*, %"class.draco::CornerTable"** %table.addr, align 4
  %call6 = call i32 @_ZNK5draco11CornerTable12num_verticesEv(%"class.draco::CornerTable"* %4)
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7reserveEm(%"class.std::__2::vector.15"* %vertex_to_attribute_entry_id_map_, i32 %call6)
  %vertex_to_left_most_corner_map_ = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 4
  %5 = load %"class.draco::CornerTable"*, %"class.draco::CornerTable"** %table.addr, align 4
  %call7 = call i32 @_ZNK5draco11CornerTable12num_verticesEv(%"class.draco::CornerTable"* %5)
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE7reserveEm(%"class.std::__2::vector.7"* %vertex_to_left_most_corner_map_, i32 %call7)
  %6 = load %"class.draco::CornerTable"*, %"class.draco::CornerTable"** %table.addr, align 4
  %corner_table_ = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 6
  store %"class.draco::CornerTable"* %6, %"class.draco::CornerTable"** %corner_table_, align 4
  %no_interior_seams_ = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 2
  store i8 1, i8* %no_interior_seams_, align 4
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %7 = load i1, i1* %retval, align 1
  ret i1 %7
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK5draco12ValenceCacheINS_24MeshAttributeCornerTableEE17ClearValenceCacheEv(%"class.draco::ValenceCache.42"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::ValenceCache.42"*, align 4
  %ref.tmp = alloca %"class.draco::IndexTypeVector.34", align 4
  store %"class.draco::ValenceCache.42"* %this, %"class.draco::ValenceCache.42"** %this.addr, align 4
  %this1 = load %"class.draco::ValenceCache.42"*, %"class.draco::ValenceCache.42"** %this.addr, align 4
  %vertex_valence_cache_32_bit_ = getelementptr inbounds %"class.draco::ValenceCache.42", %"class.draco::ValenceCache.42"* %this1, i32 0, i32 2
  call void @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_21VertexIndex_tag_type_EEEiE5clearEv(%"class.draco::IndexTypeVector.34"* %vertex_valence_cache_32_bit_)
  %call = call %"class.draco::IndexTypeVector.34"* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_21VertexIndex_tag_type_EEEiEC2Ev(%"class.draco::IndexTypeVector.34"* %ref.tmp)
  %vertex_valence_cache_32_bit_2 = getelementptr inbounds %"class.draco::ValenceCache.42", %"class.draco::ValenceCache.42"* %this1, i32 0, i32 2
  call void @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_21VertexIndex_tag_type_EEEiE4swapERS4_(%"class.draco::IndexTypeVector.34"* %ref.tmp, %"class.draco::IndexTypeVector.34"* nonnull align 4 dereferenceable(12) %vertex_valence_cache_32_bit_2)
  %call3 = call %"class.draco::IndexTypeVector.34"* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_21VertexIndex_tag_type_EEEiED2Ev(%"class.draco::IndexTypeVector.34"* %ref.tmp) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK5draco12ValenceCacheINS_24MeshAttributeCornerTableEE27ClearValenceCacheInaccurateEv(%"class.draco::ValenceCache.42"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::ValenceCache.42"*, align 4
  %ref.tmp = alloca %"class.draco::IndexTypeVector.26", align 4
  store %"class.draco::ValenceCache.42"* %this, %"class.draco::ValenceCache.42"** %this.addr, align 4
  %this1 = load %"class.draco::ValenceCache.42"*, %"class.draco::ValenceCache.42"** %this.addr, align 4
  %vertex_valence_cache_8_bit_ = getelementptr inbounds %"class.draco::ValenceCache.42", %"class.draco::ValenceCache.42"* %this1, i32 0, i32 1
  call void @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_21VertexIndex_tag_type_EEEaE5clearEv(%"class.draco::IndexTypeVector.26"* %vertex_valence_cache_8_bit_)
  %call = call %"class.draco::IndexTypeVector.26"* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_21VertexIndex_tag_type_EEEaEC2Ev(%"class.draco::IndexTypeVector.26"* %ref.tmp)
  %vertex_valence_cache_8_bit_2 = getelementptr inbounds %"class.draco::ValenceCache.42", %"class.draco::ValenceCache.42"* %this1, i32 0, i32 1
  call void @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_21VertexIndex_tag_type_EEEaE4swapERS4_(%"class.draco::IndexTypeVector.26"* %ref.tmp, %"class.draco::IndexTypeVector.26"* nonnull align 4 dereferenceable(12) %vertex_valence_cache_8_bit_2)
  %call3 = call %"class.draco::IndexTypeVector.26"* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_21VertexIndex_tag_type_EEEaED2Ev(%"class.draco::IndexTypeVector.26"* %ref.tmp) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIbNS_9allocatorIbEEE6assignEmRKb(%"class.std::__2::vector"* %this, i32 %__n, i8* nonnull align 1 dereferenceable(1) %__x) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__n.addr = alloca i32, align 4
  %__x.addr = alloca i8*, align 4
  %__c = alloca i32, align 4
  %__v = alloca %"class.std::__2::vector", align 4
  %ref.tmp = alloca %"class.std::__2::allocator.148", align 1
  %agg.tmp = alloca %"class.std::__2::__bit_iterator", align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store i8* %__x, i8** %__x.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %__size_ = getelementptr inbounds %"class.std::__2::vector", %"class.std::__2::vector"* %this1, i32 0, i32 1
  store i32 0, i32* %__size_, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %cmp = icmp ugt i32 %0, 0
  br i1 %cmp, label %if.then, label %if.end11

if.then:                                          ; preds = %entry
  %call = call i32 @_ZNKSt3__26vectorIbNS_9allocatorIbEEE8capacityEv(%"class.std::__2::vector"* %this1) #8
  store i32 %call, i32* %__c, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %2 = load i32, i32* %__c, align 4
  %cmp2 = icmp ule i32 %1, %2
  br i1 %cmp2, label %if.then3, label %if.else

if.then3:                                         ; preds = %if.then
  %3 = load i32, i32* %__n.addr, align 4
  %__size_4 = getelementptr inbounds %"class.std::__2::vector", %"class.std::__2::vector"* %this1, i32 0, i32 1
  store i32 %3, i32* %__size_4, align 4
  br label %if.end

if.else:                                          ; preds = %if.then
  %call5 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__26vectorIbNS_9allocatorIbEEE7__allocEv(%"class.std::__2::vector"* %this1) #8
  %call6 = call %"class.std::__2::allocator.148"* @_ZNSt3__29allocatorIbEC2ImEERKNS0_IT_EE(%"class.std::__2::allocator.148"* %ref.tmp, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call5) #8
  %call7 = call %"class.std::__2::vector"* @_ZNSt3__26vectorIbNS_9allocatorIbEEEC2ERKS2_(%"class.std::__2::vector"* %__v, %"class.std::__2::allocator.148"* nonnull align 1 dereferenceable(1) %ref.tmp) #8
  %4 = load i32, i32* %__n.addr, align 4
  %call8 = call i32 @_ZNKSt3__26vectorIbNS_9allocatorIbEEE11__recommendEm(%"class.std::__2::vector"* %this1, i32 %4)
  call void @_ZNSt3__26vectorIbNS_9allocatorIbEEE7reserveEm(%"class.std::__2::vector"* %__v, i32 %call8)
  %5 = load i32, i32* %__n.addr, align 4
  %__size_9 = getelementptr inbounds %"class.std::__2::vector", %"class.std::__2::vector"* %__v, i32 0, i32 1
  store i32 %5, i32* %__size_9, align 4
  call void @_ZNSt3__26vectorIbNS_9allocatorIbEEE4swapERS3_(%"class.std::__2::vector"* %this1, %"class.std::__2::vector"* nonnull align 4 dereferenceable(12) %__v) #8
  %call10 = call %"class.std::__2::vector"* @_ZNSt3__26vectorIbNS_9allocatorIbEEED2Ev(%"class.std::__2::vector"* %__v) #8
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then3
  call void @_ZNSt3__26vectorIbNS_9allocatorIbEEE5beginEv(%"class.std::__2::__bit_iterator"* sret align 4 %agg.tmp, %"class.std::__2::vector"* %this1) #8
  %6 = load i32, i32* %__n.addr, align 4
  %7 = load i8*, i8** %__x.addr, align 4
  %8 = load i8, i8* %7, align 1
  %tobool = trunc i8 %8 to i1
  call void @_ZNSt3__26fill_nINS_6vectorIbNS_9allocatorIbEEEEEEvNS_14__bit_iteratorIT_Lb0EXLi0EEEENS6_9size_typeEb(%"class.std::__2::__bit_iterator"* %agg.tmp, i32 %6, i1 zeroext %tobool)
  br label %if.end11

if.end11:                                         ; preds = %if.end, %entry
  call void @_ZNSt3__26vectorIbNS_9allocatorIbEEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector"* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco11CornerTable11num_cornersEv(%"class.draco::CornerTable"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::CornerTable"*, align 4
  store %"class.draco::CornerTable"* %this, %"class.draco::CornerTable"** %this.addr, align 4
  %this1 = load %"class.draco::CornerTable"*, %"class.draco::CornerTable"** %this.addr, align 4
  %corner_to_vertex_map_ = getelementptr inbounds %"class.draco::CornerTable", %"class.draco::CornerTable"* %this1, i32 0, i32 0
  %call = call i32 @_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_21CornerIndex_tag_type_EEENS1_IjNS_21VertexIndex_tag_type_EEEE4sizeEv(%"class.draco::IndexTypeVector"* %corner_to_vertex_map_)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco11CornerTable12num_verticesEv(%"class.draco::CornerTable"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::CornerTable"*, align 4
  store %"class.draco::CornerTable"* %this, %"class.draco::CornerTable"** %this.addr, align 4
  %this1 = load %"class.draco::CornerTable"*, %"class.draco::CornerTable"** %this.addr, align 4
  %vertex_corners_ = getelementptr inbounds %"class.draco::CornerTable", %"class.draco::CornerTable"* %this1, i32 0, i32 2
  %call = call i32 @_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_21VertexIndex_tag_type_EEENS1_IjNS_21CornerIndex_tag_type_EEEE4sizeEv(%"class.draco::IndexTypeVector.24"* %vertex_corners_)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE6assignEmRKS4_(%"class.std::__2::vector.1"* %this, i32 %__n, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %__u) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.1"*, align 4
  %__n.addr = alloca i32, align 4
  %__u.addr = alloca %"class.draco::IndexType"*, align 4
  %__s = alloca i32, align 4
  store %"class.std::__2::vector.1"* %this, %"class.std::__2::vector.1"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store %"class.draco::IndexType"* %__u, %"class.draco::IndexType"** %__u.addr, align 4
  %this1 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %this.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.1"* %this1) #8
  %cmp = icmp ule i32 %0, %call
  br i1 %cmp, label %if.then, label %if.else8

if.then:                                          ; preds = %entry
  %call2 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.1"* %this1) #8
  store i32 %call2, i32* %__s, align 4
  %1 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %1, i32 0, i32 0
  %2 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  %call3 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %__n.addr, i32* nonnull align 4 dereferenceable(4) %__s)
  %3 = load i32, i32* %call3, align 4
  %4 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__u.addr, align 4
  %call4 = call %"class.draco::IndexType"* @_ZNSt3__26fill_nIPN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEEmS4_EET_S6_T0_RKT1_(%"class.draco::IndexType"* %2, i32 %3, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %4)
  %5 = load i32, i32* %__n.addr, align 4
  %6 = load i32, i32* %__s, align 4
  %cmp5 = icmp ugt i32 %5, %6
  br i1 %cmp5, label %if.then6, label %if.else

if.then6:                                         ; preds = %if.then
  %7 = load i32, i32* %__n.addr, align 4
  %8 = load i32, i32* %__s, align 4
  %sub = sub i32 %7, %8
  %9 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__u.addr, align 4
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE18__construct_at_endEmRKS4_(%"class.std::__2::vector.1"* %this1, i32 %sub, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %9)
  br label %if.end

if.else:                                          ; preds = %if.then
  %10 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_7 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %10, i32 0, i32 0
  %11 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_7, align 4
  %12 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %11, i32 %12
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_(%"class.std::__2::vector.1"* %this1, %"class.draco::IndexType"* %add.ptr) #8
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then6
  br label %if.end10

if.else8:                                         ; preds = %entry
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE13__vdeallocateEv(%"class.std::__2::vector.1"* %this1) #8
  %13 = load i32, i32* %__n.addr, align 4
  %call9 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE11__recommendEm(%"class.std::__2::vector.1"* %this1, i32 %13)
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE11__vallocateEm(%"class.std::__2::vector.1"* %this1, i32 %call9)
  %14 = load i32, i32* %__n.addr, align 4
  %15 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__u.addr, align 4
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE18__construct_at_endEmRKS4_(%"class.std::__2::vector.1"* %this1, i32 %14, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %15)
  br label %if.end10

if.end10:                                         ; preds = %if.else8, %if.end
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector.1"* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7reserveEm(%"class.std::__2::vector.15"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.15"*, align 4
  %__n.addr = alloca i32, align 4
  %__a = alloca %"class.std::__2::allocator.21"*, align 4
  %__v = alloca %"struct.std::__2::__split_buffer", align 4
  store %"class.std::__2::vector.15"* %this, %"class.std::__2::vector.15"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.15"*, %"class.std::__2::vector.15"** %this.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.15"* %this1) #8
  %cmp = icmp ugt i32 %0, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %"class.std::__2::vector.15"* %this1 to %"class.std::__2::__vector_base.16"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.21"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.16"* %1) #8
  store %"class.std::__2::allocator.21"* %call2, %"class.std::__2::allocator.21"** %__a, align 4
  %2 = load i32, i32* %__n.addr, align 4
  %call3 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.15"* %this1) #8
  %3 = load %"class.std::__2::allocator.21"*, %"class.std::__2::allocator.21"** %__a, align 4
  %call4 = call %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEEC2EmmS7_(%"struct.std::__2::__split_buffer"* %__v, i32 %2, i32 %call3, %"class.std::__2::allocator.21"* nonnull align 1 dereferenceable(1) %3)
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS4_RS6_EE(%"class.std::__2::vector.15"* %this1, %"struct.std::__2::__split_buffer"* nonnull align 4 dereferenceable(20) %__v)
  %call5 = call %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEED2Ev(%"struct.std::__2::__split_buffer"* %__v) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE7reserveEm(%"class.std::__2::vector.7"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.7"*, align 4
  %__n.addr = alloca i32, align 4
  %__a = alloca %"class.std::__2::allocator.13"*, align 4
  %__v = alloca %"struct.std::__2::__split_buffer.156", align 4
  store %"class.std::__2::vector.7"* %this, %"class.std::__2::vector.7"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.7"*, %"class.std::__2::vector.7"** %this.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.7"* %this1) #8
  %cmp = icmp ugt i32 %0, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %"class.std::__2::vector.7"* %this1 to %"class.std::__2::__vector_base.8"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.13"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.8"* %1) #8
  store %"class.std::__2::allocator.13"* %call2, %"class.std::__2::allocator.13"** %__a, align 4
  %2 = load i32, i32* %__n.addr, align 4
  %call3 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.7"* %this1) #8
  %3 = load %"class.std::__2::allocator.13"*, %"class.std::__2::allocator.13"** %__a, align 4
  %call4 = call %"struct.std::__2::__split_buffer.156"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEERNS_9allocatorIS4_EEEC2EmmS7_(%"struct.std::__2::__split_buffer.156"* %__v, i32 %2, i32 %call3, %"class.std::__2::allocator.13"* nonnull align 1 dereferenceable(1) %3)
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS4_RS6_EE(%"class.std::__2::vector.7"* %this1, %"struct.std::__2::__split_buffer.156"* nonnull align 4 dereferenceable(20) %__v)
  %call5 = call %"struct.std::__2::__split_buffer.156"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEERNS_9allocatorIS4_EEED2Ev(%"struct.std::__2::__split_buffer.156"* %__v) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden zeroext i1 @_ZN5draco24MeshAttributeCornerTable17InitFromAttributeEPKNS_4MeshEPKNS_11CornerTableEPKNS_14PointAttributeE(%"class.draco::MeshAttributeCornerTable"* %this, %"class.draco::Mesh"* %mesh, %"class.draco::CornerTable"* %table, %"class.draco::PointAttribute"* %att) #0 {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::MeshAttributeCornerTable"*, align 4
  %mesh.addr = alloca %"class.draco::Mesh"*, align 4
  %table.addr = alloca %"class.draco::CornerTable"*, align 4
  %att.addr = alloca %"class.draco::PointAttribute"*, align 4
  %c = alloca %"class.draco::IndexType.9", align 4
  %ref.tmp = alloca i32, align 4
  %f = alloca %"class.draco::IndexType.145", align 4
  %agg.tmp = alloca %"class.draco::IndexType.9", align 4
  %agg.tmp10 = alloca %"class.draco::IndexType.145", align 4
  %opp_corner = alloca %"class.draco::IndexType.9", align 4
  %agg.tmp16 = alloca %"class.draco::IndexType.9", align 4
  %ref.tmp22 = alloca %"class.std::__2::__bit_reference", align 4
  %v = alloca %"class.draco::IndexType", align 4
  %ref.tmp26 = alloca %"class.draco::IndexType", align 4
  %agg.tmp28 = alloca %"class.draco::IndexType.9", align 4
  %agg.tmp30 = alloca %"class.draco::IndexType.9", align 4
  %ref.tmp38 = alloca %"class.std::__2::__bit_reference", align 4
  %ref.tmp41 = alloca %"class.draco::IndexType", align 4
  %agg.tmp43 = alloca %"class.draco::IndexType.9", align 4
  %agg.tmp45 = alloca %"class.draco::IndexType.9", align 4
  %ref.tmp53 = alloca %"class.std::__2::__bit_reference", align 4
  %act_c = alloca %"class.draco::IndexType.9", align 4
  %act_sibling_c = alloca %"class.draco::IndexType.9", align 4
  %i = alloca i32, align 4
  %ref.tmp63 = alloca %"class.draco::IndexType.9", align 4
  %agg.tmp65 = alloca %"class.draco::IndexType.9", align 4
  %ref.tmp70 = alloca %"class.draco::IndexType.9", align 4
  %agg.tmp72 = alloca %"class.draco::IndexType.9", align 4
  %point_id = alloca %"class.draco::IndexType.139", align 4
  %sibling_point_id = alloca %"class.draco::IndexType.139", align 4
  %ref.tmp83 = alloca %"class.draco::IndexType.17", align 4
  %agg.tmp84 = alloca %"class.draco::IndexType.139", align 4
  %ref.tmp88 = alloca %"class.draco::IndexType.17", align 4
  %agg.tmp89 = alloca %"class.draco::IndexType.139", align 4
  %ref.tmp95 = alloca %"class.std::__2::__bit_reference", align 4
  %ref.tmp99 = alloca %"class.std::__2::__bit_reference", align 4
  %ref.tmp103 = alloca %"class.std::__2::__bit_reference", align 4
  %ref.tmp105 = alloca %"class.draco::IndexType", align 4
  %agg.tmp107 = alloca %"class.draco::IndexType.9", align 4
  %agg.tmp109 = alloca %"class.draco::IndexType.9", align 4
  %ref.tmp118 = alloca %"class.std::__2::__bit_reference", align 4
  %ref.tmp120 = alloca %"class.draco::IndexType", align 4
  %agg.tmp122 = alloca %"class.draco::IndexType.9", align 4
  %agg.tmp124 = alloca %"class.draco::IndexType.9", align 4
  %ref.tmp133 = alloca %"class.std::__2::__bit_reference", align 4
  %ref.tmp135 = alloca %"class.draco::IndexType", align 4
  %agg.tmp137 = alloca %"class.draco::IndexType.9", align 4
  %agg.tmp139 = alloca %"class.draco::IndexType.9", align 4
  %ref.tmp148 = alloca %"class.std::__2::__bit_reference", align 4
  %ref.tmp150 = alloca %"class.draco::IndexType", align 4
  %agg.tmp152 = alloca %"class.draco::IndexType.9", align 4
  %agg.tmp154 = alloca %"class.draco::IndexType.9", align 4
  store %"class.draco::MeshAttributeCornerTable"* %this, %"class.draco::MeshAttributeCornerTable"** %this.addr, align 4
  store %"class.draco::Mesh"* %mesh, %"class.draco::Mesh"** %mesh.addr, align 4
  store %"class.draco::CornerTable"* %table, %"class.draco::CornerTable"** %table.addr, align 4
  store %"class.draco::PointAttribute"* %att, %"class.draco::PointAttribute"** %att.addr, align 4
  %this1 = load %"class.draco::MeshAttributeCornerTable"*, %"class.draco::MeshAttributeCornerTable"** %this.addr, align 4
  %0 = load %"class.draco::CornerTable"*, %"class.draco::CornerTable"** %table.addr, align 4
  %call = call zeroext i1 @_ZN5draco24MeshAttributeCornerTable9InitEmptyEPKNS_11CornerTableE(%"class.draco::MeshAttributeCornerTable"* %this1, %"class.draco::CornerTable"* %0)
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %valence_cache_ = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 7
  call void @_ZNK5draco12ValenceCacheINS_24MeshAttributeCornerTableEE17ClearValenceCacheEv(%"class.draco::ValenceCache.42"* %valence_cache_)
  %valence_cache_2 = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 7
  call void @_ZNK5draco12ValenceCacheINS_24MeshAttributeCornerTableEE27ClearValenceCacheInaccurateEv(%"class.draco::ValenceCache.42"* %valence_cache_2)
  %call3 = call %"class.draco::IndexType.9"* @_ZN5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEC2Ej(%"class.draco::IndexType.9"* %c, i32 0)
  br label %for.cond

for.cond:                                         ; preds = %for.inc164, %if.end
  %corner_table_ = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 6
  %1 = load %"class.draco::CornerTable"*, %"class.draco::CornerTable"** %corner_table_, align 4
  %call4 = call i32 @_ZNK5draco11CornerTable11num_cornersEv(%"class.draco::CornerTable"* %1)
  store i32 %call4, i32* %ref.tmp, align 4
  %call5 = call zeroext i1 @_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEltERKj(%"class.draco::IndexType.9"* %c, i32* nonnull align 4 dereferenceable(4) %ref.tmp)
  br i1 %call5, label %for.body, label %for.end166

for.body:                                         ; preds = %for.cond
  %corner_table_6 = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 6
  %2 = load %"class.draco::CornerTable"*, %"class.draco::CornerTable"** %corner_table_6, align 4
  %3 = bitcast %"class.draco::IndexType.9"* %agg.tmp to i8*
  %4 = bitcast %"class.draco::IndexType.9"* %c to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 4, i1 false)
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %agg.tmp, i32 0, i32 0
  %5 = load i32, i32* %coerce.dive, align 4
  %call7 = call i32 @_ZNK5draco11CornerTable4FaceENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::CornerTable"* %2, i32 %5)
  %coerce.dive8 = getelementptr inbounds %"class.draco::IndexType.145", %"class.draco::IndexType.145"* %f, i32 0, i32 0
  store i32 %call7, i32* %coerce.dive8, align 4
  %corner_table_9 = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 6
  %6 = load %"class.draco::CornerTable"*, %"class.draco::CornerTable"** %corner_table_9, align 4
  %7 = bitcast %"class.draco::IndexType.145"* %agg.tmp10 to i8*
  %8 = bitcast %"class.draco::IndexType.145"* %f to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 4, i1 false)
  %coerce.dive11 = getelementptr inbounds %"class.draco::IndexType.145", %"class.draco::IndexType.145"* %agg.tmp10, i32 0, i32 0
  %9 = load i32, i32* %coerce.dive11, align 4
  %call12 = call zeroext i1 @_ZNK5draco11CornerTable13IsDegeneratedENS_9IndexTypeIjNS_19FaceIndex_tag_type_EEE(%"class.draco::CornerTable"* %6, i32 %9)
  br i1 %call12, label %if.then13, label %if.end14

if.then13:                                        ; preds = %for.body
  br label %for.inc164

if.end14:                                         ; preds = %for.body
  %corner_table_15 = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 6
  %10 = load %"class.draco::CornerTable"*, %"class.draco::CornerTable"** %corner_table_15, align 4
  %11 = bitcast %"class.draco::IndexType.9"* %agg.tmp16 to i8*
  %12 = bitcast %"class.draco::IndexType.9"* %c to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %11, i8* align 4 %12, i32 4, i1 false)
  %coerce.dive17 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %agg.tmp16, i32 0, i32 0
  %13 = load i32, i32* %coerce.dive17, align 4
  %call18 = call i32 @_ZNK5draco11CornerTable8OppositeENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::CornerTable"* %10, i32 %13)
  %coerce.dive19 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %opp_corner, i32 0, i32 0
  store i32 %call18, i32* %coerce.dive19, align 4
  %call20 = call zeroext i1 @_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEeqERKS2_(%"class.draco::IndexType.9"* %opp_corner, %"class.draco::IndexType.9"* nonnull align 4 dereferenceable(4) @_ZN5dracoL19kInvalidCornerIndexE)
  br i1 %call20, label %if.then21, label %if.end57

if.then21:                                        ; preds = %if.end14
  %is_edge_on_seam_ = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 0
  %call23 = call i32 @_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EE5valueEv(%"class.draco::IndexType.9"* %c)
  call void @_ZNSt3__26vectorIbNS_9allocatorIbEEEixEm(%"class.std::__2::__bit_reference"* sret align 4 %ref.tmp22, %"class.std::__2::vector"* %is_edge_on_seam_, i32 %call23)
  %call24 = call nonnull align 4 dereferenceable(8) %"class.std::__2::__bit_reference"* @_ZNSt3__215__bit_referenceINS_6vectorIbNS_9allocatorIbEEEELb1EEaSEb(%"class.std::__2::__bit_reference"* %ref.tmp22, i1 zeroext true) #8
  %call25 = call %"class.draco::IndexType"* @_ZN5draco9IndexTypeIjNS_21VertexIndex_tag_type_EEC2Ev(%"class.draco::IndexType"* %v)
  %corner_table_27 = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 6
  %14 = load %"class.draco::CornerTable"*, %"class.draco::CornerTable"** %corner_table_27, align 4
  %corner_table_29 = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 6
  %15 = load %"class.draco::CornerTable"*, %"class.draco::CornerTable"** %corner_table_29, align 4
  %16 = bitcast %"class.draco::IndexType.9"* %agg.tmp30 to i8*
  %17 = bitcast %"class.draco::IndexType.9"* %c to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %16, i8* align 4 %17, i32 4, i1 false)
  %coerce.dive31 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %agg.tmp30, i32 0, i32 0
  %18 = load i32, i32* %coerce.dive31, align 4
  %call32 = call i32 @_ZNK5draco11CornerTable4NextENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::CornerTable"* %15, i32 %18)
  %coerce.dive33 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %agg.tmp28, i32 0, i32 0
  store i32 %call32, i32* %coerce.dive33, align 4
  %coerce.dive34 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %agg.tmp28, i32 0, i32 0
  %19 = load i32, i32* %coerce.dive34, align 4
  %call35 = call i32 @_ZNK5draco11CornerTable6VertexENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::CornerTable"* %14, i32 %19)
  %coerce.dive36 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %ref.tmp26, i32 0, i32 0
  store i32 %call35, i32* %coerce.dive36, align 4
  %call37 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZN5draco9IndexTypeIjNS_21VertexIndex_tag_type_EEaSERKS2_(%"class.draco::IndexType"* %v, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %ref.tmp26)
  %is_vertex_on_seam_ = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 1
  %call39 = call i32 @_ZNK5draco9IndexTypeIjNS_21VertexIndex_tag_type_EE5valueEv(%"class.draco::IndexType"* %v)
  call void @_ZNSt3__26vectorIbNS_9allocatorIbEEEixEm(%"class.std::__2::__bit_reference"* sret align 4 %ref.tmp38, %"class.std::__2::vector"* %is_vertex_on_seam_, i32 %call39)
  %call40 = call nonnull align 4 dereferenceable(8) %"class.std::__2::__bit_reference"* @_ZNSt3__215__bit_referenceINS_6vectorIbNS_9allocatorIbEEEELb1EEaSEb(%"class.std::__2::__bit_reference"* %ref.tmp38, i1 zeroext true) #8
  %corner_table_42 = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 6
  %20 = load %"class.draco::CornerTable"*, %"class.draco::CornerTable"** %corner_table_42, align 4
  %corner_table_44 = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 6
  %21 = load %"class.draco::CornerTable"*, %"class.draco::CornerTable"** %corner_table_44, align 4
  %22 = bitcast %"class.draco::IndexType.9"* %agg.tmp45 to i8*
  %23 = bitcast %"class.draco::IndexType.9"* %c to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %22, i8* align 4 %23, i32 4, i1 false)
  %coerce.dive46 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %agg.tmp45, i32 0, i32 0
  %24 = load i32, i32* %coerce.dive46, align 4
  %call47 = call i32 @_ZNK5draco11CornerTable8PreviousENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::CornerTable"* %21, i32 %24)
  %coerce.dive48 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %agg.tmp43, i32 0, i32 0
  store i32 %call47, i32* %coerce.dive48, align 4
  %coerce.dive49 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %agg.tmp43, i32 0, i32 0
  %25 = load i32, i32* %coerce.dive49, align 4
  %call50 = call i32 @_ZNK5draco11CornerTable6VertexENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::CornerTable"* %20, i32 %25)
  %coerce.dive51 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %ref.tmp41, i32 0, i32 0
  store i32 %call50, i32* %coerce.dive51, align 4
  %call52 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZN5draco9IndexTypeIjNS_21VertexIndex_tag_type_EEaSERKS2_(%"class.draco::IndexType"* %v, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %ref.tmp41)
  %is_vertex_on_seam_54 = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 1
  %call55 = call i32 @_ZNK5draco9IndexTypeIjNS_21VertexIndex_tag_type_EE5valueEv(%"class.draco::IndexType"* %v)
  call void @_ZNSt3__26vectorIbNS_9allocatorIbEEEixEm(%"class.std::__2::__bit_reference"* sret align 4 %ref.tmp53, %"class.std::__2::vector"* %is_vertex_on_seam_54, i32 %call55)
  %call56 = call nonnull align 4 dereferenceable(8) %"class.std::__2::__bit_reference"* @_ZNSt3__215__bit_referenceINS_6vectorIbNS_9allocatorIbEEEELb1EEaSEb(%"class.std::__2::__bit_reference"* %ref.tmp53, i1 zeroext true) #8
  br label %for.inc164

if.end57:                                         ; preds = %if.end14
  %call58 = call zeroext i1 @_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEltERKS2_(%"class.draco::IndexType.9"* %opp_corner, %"class.draco::IndexType.9"* nonnull align 4 dereferenceable(4) %c)
  br i1 %call58, label %if.then59, label %if.end60

if.then59:                                        ; preds = %if.end57
  br label %for.inc164

if.end60:                                         ; preds = %if.end57
  %26 = bitcast %"class.draco::IndexType.9"* %act_c to i8*
  %27 = bitcast %"class.draco::IndexType.9"* %c to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %26, i8* align 4 %27, i32 4, i1 false)
  %28 = bitcast %"class.draco::IndexType.9"* %act_sibling_c to i8*
  %29 = bitcast %"class.draco::IndexType.9"* %opp_corner to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %28, i8* align 4 %29, i32 4, i1 false)
  store i32 0, i32* %i, align 4
  br label %for.cond61

for.cond61:                                       ; preds = %for.inc, %if.end60
  %30 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %30, 2
  br i1 %cmp, label %for.body62, label %for.end

for.body62:                                       ; preds = %for.cond61
  %corner_table_64 = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 6
  %31 = load %"class.draco::CornerTable"*, %"class.draco::CornerTable"** %corner_table_64, align 4
  %32 = bitcast %"class.draco::IndexType.9"* %agg.tmp65 to i8*
  %33 = bitcast %"class.draco::IndexType.9"* %act_c to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %32, i8* align 4 %33, i32 4, i1 false)
  %coerce.dive66 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %agg.tmp65, i32 0, i32 0
  %34 = load i32, i32* %coerce.dive66, align 4
  %call67 = call i32 @_ZNK5draco11CornerTable4NextENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::CornerTable"* %31, i32 %34)
  %coerce.dive68 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %ref.tmp63, i32 0, i32 0
  store i32 %call67, i32* %coerce.dive68, align 4
  %call69 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.9"* @_ZN5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEaSERKS2_(%"class.draco::IndexType.9"* %act_c, %"class.draco::IndexType.9"* nonnull align 4 dereferenceable(4) %ref.tmp63)
  %corner_table_71 = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 6
  %35 = load %"class.draco::CornerTable"*, %"class.draco::CornerTable"** %corner_table_71, align 4
  %36 = bitcast %"class.draco::IndexType.9"* %agg.tmp72 to i8*
  %37 = bitcast %"class.draco::IndexType.9"* %act_sibling_c to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %36, i8* align 4 %37, i32 4, i1 false)
  %coerce.dive73 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %agg.tmp72, i32 0, i32 0
  %38 = load i32, i32* %coerce.dive73, align 4
  %call74 = call i32 @_ZNK5draco11CornerTable8PreviousENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::CornerTable"* %35, i32 %38)
  %coerce.dive75 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %ref.tmp70, i32 0, i32 0
  store i32 %call74, i32* %coerce.dive75, align 4
  %call76 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.9"* @_ZN5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEaSERKS2_(%"class.draco::IndexType.9"* %act_sibling_c, %"class.draco::IndexType.9"* nonnull align 4 dereferenceable(4) %ref.tmp70)
  %39 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %mesh.addr, align 4
  %call77 = call i32 @_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EE5valueEv(%"class.draco::IndexType.9"* %act_c)
  %call78 = call i32 @_ZNK5draco4Mesh15CornerToPointIdEi(%"class.draco::Mesh"* %39, i32 %call77)
  %coerce.dive79 = getelementptr inbounds %"class.draco::IndexType.139", %"class.draco::IndexType.139"* %point_id, i32 0, i32 0
  store i32 %call78, i32* %coerce.dive79, align 4
  %40 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %mesh.addr, align 4
  %call80 = call i32 @_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EE5valueEv(%"class.draco::IndexType.9"* %act_sibling_c)
  %call81 = call i32 @_ZNK5draco4Mesh15CornerToPointIdEi(%"class.draco::Mesh"* %40, i32 %call80)
  %coerce.dive82 = getelementptr inbounds %"class.draco::IndexType.139", %"class.draco::IndexType.139"* %sibling_point_id, i32 0, i32 0
  store i32 %call81, i32* %coerce.dive82, align 4
  %41 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %att.addr, align 4
  %42 = bitcast %"class.draco::IndexType.139"* %agg.tmp84 to i8*
  %43 = bitcast %"class.draco::IndexType.139"* %point_id to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %42, i8* align 4 %43, i32 4, i1 false)
  %coerce.dive85 = getelementptr inbounds %"class.draco::IndexType.139", %"class.draco::IndexType.139"* %agg.tmp84, i32 0, i32 0
  %44 = load i32, i32* %coerce.dive85, align 4
  %call86 = call i32 @_ZNK5draco14PointAttribute12mapped_indexENS_9IndexTypeIjNS_20PointIndex_tag_type_EEE(%"class.draco::PointAttribute"* %41, i32 %44)
  %coerce.dive87 = getelementptr inbounds %"class.draco::IndexType.17", %"class.draco::IndexType.17"* %ref.tmp83, i32 0, i32 0
  store i32 %call86, i32* %coerce.dive87, align 4
  %45 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %att.addr, align 4
  %46 = bitcast %"class.draco::IndexType.139"* %agg.tmp89 to i8*
  %47 = bitcast %"class.draco::IndexType.139"* %sibling_point_id to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %46, i8* align 4 %47, i32 4, i1 false)
  %coerce.dive90 = getelementptr inbounds %"class.draco::IndexType.139", %"class.draco::IndexType.139"* %agg.tmp89, i32 0, i32 0
  %48 = load i32, i32* %coerce.dive90, align 4
  %call91 = call i32 @_ZNK5draco14PointAttribute12mapped_indexENS_9IndexTypeIjNS_20PointIndex_tag_type_EEE(%"class.draco::PointAttribute"* %45, i32 %48)
  %coerce.dive92 = getelementptr inbounds %"class.draco::IndexType.17", %"class.draco::IndexType.17"* %ref.tmp88, i32 0, i32 0
  store i32 %call91, i32* %coerce.dive92, align 4
  %call93 = call zeroext i1 @_ZNK5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEneERKS2_(%"class.draco::IndexType.17"* %ref.tmp83, %"class.draco::IndexType.17"* nonnull align 4 dereferenceable(4) %ref.tmp88)
  br i1 %call93, label %if.then94, label %if.end163

if.then94:                                        ; preds = %for.body62
  %no_interior_seams_ = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 2
  store i8 0, i8* %no_interior_seams_, align 4
  %is_edge_on_seam_96 = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 0
  %call97 = call i32 @_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EE5valueEv(%"class.draco::IndexType.9"* %c)
  call void @_ZNSt3__26vectorIbNS_9allocatorIbEEEixEm(%"class.std::__2::__bit_reference"* sret align 4 %ref.tmp95, %"class.std::__2::vector"* %is_edge_on_seam_96, i32 %call97)
  %call98 = call nonnull align 4 dereferenceable(8) %"class.std::__2::__bit_reference"* @_ZNSt3__215__bit_referenceINS_6vectorIbNS_9allocatorIbEEEELb1EEaSEb(%"class.std::__2::__bit_reference"* %ref.tmp95, i1 zeroext true) #8
  %is_edge_on_seam_100 = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 0
  %call101 = call i32 @_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EE5valueEv(%"class.draco::IndexType.9"* %opp_corner)
  call void @_ZNSt3__26vectorIbNS_9allocatorIbEEEixEm(%"class.std::__2::__bit_reference"* sret align 4 %ref.tmp99, %"class.std::__2::vector"* %is_edge_on_seam_100, i32 %call101)
  %call102 = call nonnull align 4 dereferenceable(8) %"class.std::__2::__bit_reference"* @_ZNSt3__215__bit_referenceINS_6vectorIbNS_9allocatorIbEEEELb1EEaSEb(%"class.std::__2::__bit_reference"* %ref.tmp99, i1 zeroext true) #8
  %is_vertex_on_seam_104 = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 1
  %corner_table_106 = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 6
  %49 = load %"class.draco::CornerTable"*, %"class.draco::CornerTable"** %corner_table_106, align 4
  %corner_table_108 = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 6
  %50 = load %"class.draco::CornerTable"*, %"class.draco::CornerTable"** %corner_table_108, align 4
  %51 = bitcast %"class.draco::IndexType.9"* %agg.tmp109 to i8*
  %52 = bitcast %"class.draco::IndexType.9"* %c to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %51, i8* align 4 %52, i32 4, i1 false)
  %coerce.dive110 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %agg.tmp109, i32 0, i32 0
  %53 = load i32, i32* %coerce.dive110, align 4
  %call111 = call i32 @_ZNK5draco11CornerTable4NextENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::CornerTable"* %50, i32 %53)
  %coerce.dive112 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %agg.tmp107, i32 0, i32 0
  store i32 %call111, i32* %coerce.dive112, align 4
  %coerce.dive113 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %agg.tmp107, i32 0, i32 0
  %54 = load i32, i32* %coerce.dive113, align 4
  %call114 = call i32 @_ZNK5draco11CornerTable6VertexENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::CornerTable"* %49, i32 %54)
  %coerce.dive115 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %ref.tmp105, i32 0, i32 0
  store i32 %call114, i32* %coerce.dive115, align 4
  %call116 = call i32 @_ZNK5draco9IndexTypeIjNS_21VertexIndex_tag_type_EE5valueEv(%"class.draco::IndexType"* %ref.tmp105)
  call void @_ZNSt3__26vectorIbNS_9allocatorIbEEEixEm(%"class.std::__2::__bit_reference"* sret align 4 %ref.tmp103, %"class.std::__2::vector"* %is_vertex_on_seam_104, i32 %call116)
  %call117 = call nonnull align 4 dereferenceable(8) %"class.std::__2::__bit_reference"* @_ZNSt3__215__bit_referenceINS_6vectorIbNS_9allocatorIbEEEELb1EEaSEb(%"class.std::__2::__bit_reference"* %ref.tmp103, i1 zeroext true) #8
  %is_vertex_on_seam_119 = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 1
  %corner_table_121 = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 6
  %55 = load %"class.draco::CornerTable"*, %"class.draco::CornerTable"** %corner_table_121, align 4
  %corner_table_123 = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 6
  %56 = load %"class.draco::CornerTable"*, %"class.draco::CornerTable"** %corner_table_123, align 4
  %57 = bitcast %"class.draco::IndexType.9"* %agg.tmp124 to i8*
  %58 = bitcast %"class.draco::IndexType.9"* %c to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %57, i8* align 4 %58, i32 4, i1 false)
  %coerce.dive125 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %agg.tmp124, i32 0, i32 0
  %59 = load i32, i32* %coerce.dive125, align 4
  %call126 = call i32 @_ZNK5draco11CornerTable8PreviousENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::CornerTable"* %56, i32 %59)
  %coerce.dive127 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %agg.tmp122, i32 0, i32 0
  store i32 %call126, i32* %coerce.dive127, align 4
  %coerce.dive128 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %agg.tmp122, i32 0, i32 0
  %60 = load i32, i32* %coerce.dive128, align 4
  %call129 = call i32 @_ZNK5draco11CornerTable6VertexENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::CornerTable"* %55, i32 %60)
  %coerce.dive130 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %ref.tmp120, i32 0, i32 0
  store i32 %call129, i32* %coerce.dive130, align 4
  %call131 = call i32 @_ZNK5draco9IndexTypeIjNS_21VertexIndex_tag_type_EE5valueEv(%"class.draco::IndexType"* %ref.tmp120)
  call void @_ZNSt3__26vectorIbNS_9allocatorIbEEEixEm(%"class.std::__2::__bit_reference"* sret align 4 %ref.tmp118, %"class.std::__2::vector"* %is_vertex_on_seam_119, i32 %call131)
  %call132 = call nonnull align 4 dereferenceable(8) %"class.std::__2::__bit_reference"* @_ZNSt3__215__bit_referenceINS_6vectorIbNS_9allocatorIbEEEELb1EEaSEb(%"class.std::__2::__bit_reference"* %ref.tmp118, i1 zeroext true) #8
  %is_vertex_on_seam_134 = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 1
  %corner_table_136 = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 6
  %61 = load %"class.draco::CornerTable"*, %"class.draco::CornerTable"** %corner_table_136, align 4
  %corner_table_138 = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 6
  %62 = load %"class.draco::CornerTable"*, %"class.draco::CornerTable"** %corner_table_138, align 4
  %63 = bitcast %"class.draco::IndexType.9"* %agg.tmp139 to i8*
  %64 = bitcast %"class.draco::IndexType.9"* %opp_corner to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %63, i8* align 4 %64, i32 4, i1 false)
  %coerce.dive140 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %agg.tmp139, i32 0, i32 0
  %65 = load i32, i32* %coerce.dive140, align 4
  %call141 = call i32 @_ZNK5draco11CornerTable4NextENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::CornerTable"* %62, i32 %65)
  %coerce.dive142 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %agg.tmp137, i32 0, i32 0
  store i32 %call141, i32* %coerce.dive142, align 4
  %coerce.dive143 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %agg.tmp137, i32 0, i32 0
  %66 = load i32, i32* %coerce.dive143, align 4
  %call144 = call i32 @_ZNK5draco11CornerTable6VertexENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::CornerTable"* %61, i32 %66)
  %coerce.dive145 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %ref.tmp135, i32 0, i32 0
  store i32 %call144, i32* %coerce.dive145, align 4
  %call146 = call i32 @_ZNK5draco9IndexTypeIjNS_21VertexIndex_tag_type_EE5valueEv(%"class.draco::IndexType"* %ref.tmp135)
  call void @_ZNSt3__26vectorIbNS_9allocatorIbEEEixEm(%"class.std::__2::__bit_reference"* sret align 4 %ref.tmp133, %"class.std::__2::vector"* %is_vertex_on_seam_134, i32 %call146)
  %call147 = call nonnull align 4 dereferenceable(8) %"class.std::__2::__bit_reference"* @_ZNSt3__215__bit_referenceINS_6vectorIbNS_9allocatorIbEEEELb1EEaSEb(%"class.std::__2::__bit_reference"* %ref.tmp133, i1 zeroext true) #8
  %is_vertex_on_seam_149 = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 1
  %corner_table_151 = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 6
  %67 = load %"class.draco::CornerTable"*, %"class.draco::CornerTable"** %corner_table_151, align 4
  %corner_table_153 = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 6
  %68 = load %"class.draco::CornerTable"*, %"class.draco::CornerTable"** %corner_table_153, align 4
  %69 = bitcast %"class.draco::IndexType.9"* %agg.tmp154 to i8*
  %70 = bitcast %"class.draco::IndexType.9"* %opp_corner to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %69, i8* align 4 %70, i32 4, i1 false)
  %coerce.dive155 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %agg.tmp154, i32 0, i32 0
  %71 = load i32, i32* %coerce.dive155, align 4
  %call156 = call i32 @_ZNK5draco11CornerTable8PreviousENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::CornerTable"* %68, i32 %71)
  %coerce.dive157 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %agg.tmp152, i32 0, i32 0
  store i32 %call156, i32* %coerce.dive157, align 4
  %coerce.dive158 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %agg.tmp152, i32 0, i32 0
  %72 = load i32, i32* %coerce.dive158, align 4
  %call159 = call i32 @_ZNK5draco11CornerTable6VertexENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::CornerTable"* %67, i32 %72)
  %coerce.dive160 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %ref.tmp150, i32 0, i32 0
  store i32 %call159, i32* %coerce.dive160, align 4
  %call161 = call i32 @_ZNK5draco9IndexTypeIjNS_21VertexIndex_tag_type_EE5valueEv(%"class.draco::IndexType"* %ref.tmp150)
  call void @_ZNSt3__26vectorIbNS_9allocatorIbEEEixEm(%"class.std::__2::__bit_reference"* sret align 4 %ref.tmp148, %"class.std::__2::vector"* %is_vertex_on_seam_149, i32 %call161)
  %call162 = call nonnull align 4 dereferenceable(8) %"class.std::__2::__bit_reference"* @_ZNSt3__215__bit_referenceINS_6vectorIbNS_9allocatorIbEEEELb1EEaSEb(%"class.std::__2::__bit_reference"* %ref.tmp148, i1 zeroext true) #8
  br label %for.end

if.end163:                                        ; preds = %for.body62
  br label %for.inc

for.inc:                                          ; preds = %if.end163
  %73 = load i32, i32* %i, align 4
  %inc = add nsw i32 %73, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond61

for.end:                                          ; preds = %if.then94, %for.cond61
  br label %for.inc164

for.inc164:                                       ; preds = %for.end, %if.then59, %if.then21, %if.then13
  %call165 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.9"* @_ZN5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEppEv(%"class.draco::IndexType.9"* %c)
  br label %for.cond

for.end166:                                       ; preds = %for.cond
  %74 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %mesh.addr, align 4
  %75 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %att.addr, align 4
  call void @_ZN5draco24MeshAttributeCornerTable17RecomputeVerticesEPKNS_4MeshEPKNS_14PointAttributeE(%"class.draco::MeshAttributeCornerTable"* %this1, %"class.draco::Mesh"* %74, %"class.draco::PointAttribute"* %75)
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %for.end166, %if.then
  %76 = load i1, i1* %retval, align 1
  ret i1 %76
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType.9"* @_ZN5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEC2Ej(%"class.draco::IndexType.9"* returned %this, i32 %value) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.9"*, align 4
  %value.addr = alloca i32, align 4
  store %"class.draco::IndexType.9"* %this, %"class.draco::IndexType.9"** %this.addr, align 4
  store i32 %value, i32* %value.addr, align 4
  %this1 = load %"class.draco::IndexType.9"*, %"class.draco::IndexType.9"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value.addr, align 4
  store i32 %0, i32* %value_, align 4
  ret %"class.draco::IndexType.9"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEltERKj(%"class.draco::IndexType.9"* %this, i32* nonnull align 4 dereferenceable(4) %val) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.9"*, align 4
  %val.addr = alloca i32*, align 4
  store %"class.draco::IndexType.9"* %this, %"class.draco::IndexType.9"** %this.addr, align 4
  store i32* %val, i32** %val.addr, align 4
  %this1 = load %"class.draco::IndexType.9"*, %"class.draco::IndexType.9"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value_, align 4
  %1 = load i32*, i32** %val.addr, align 4
  %2 = load i32, i32* %1, align 4
  %cmp = icmp ult i32 %0, %2
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco11CornerTable4FaceENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::CornerTable"* %this, i32 %corner.coerce) #0 comdat {
entry:
  %retval = alloca %"class.draco::IndexType.145", align 4
  %corner = alloca %"class.draco::IndexType.9", align 4
  %this.addr = alloca %"class.draco::CornerTable"*, align 4
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %corner, i32 0, i32 0
  store i32 %corner.coerce, i32* %coerce.dive, align 4
  store %"class.draco::CornerTable"* %this, %"class.draco::CornerTable"** %this.addr, align 4
  %this1 = load %"class.draco::CornerTable"*, %"class.draco::CornerTable"** %this.addr, align 4
  %call = call zeroext i1 @_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEeqERKS2_(%"class.draco::IndexType.9"* %corner, %"class.draco::IndexType.9"* nonnull align 4 dereferenceable(4) @_ZN5dracoL19kInvalidCornerIndexE)
  br i1 %call, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %0 = bitcast %"class.draco::IndexType.145"* %retval to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %0, i8* align 4 bitcast (%"class.draco::IndexType.145"* @_ZN5dracoL17kInvalidFaceIndexE to i8*), i32 4, i1 false)
  br label %return

if.end:                                           ; preds = %entry
  %call2 = call i32 @_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EE5valueEv(%"class.draco::IndexType.9"* %corner)
  %div = udiv i32 %call2, 3
  %call3 = call %"class.draco::IndexType.145"* @_ZN5draco9IndexTypeIjNS_19FaceIndex_tag_type_EEC2Ej(%"class.draco::IndexType.145"* %retval, i32 %div)
  br label %return

return:                                           ; preds = %if.end, %if.then
  %coerce.dive4 = getelementptr inbounds %"class.draco::IndexType.145", %"class.draco::IndexType.145"* %retval, i32 0, i32 0
  %1 = load i32, i32* %coerce.dive4, align 4
  ret i32 %1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

declare zeroext i1 @_ZNK5draco11CornerTable13IsDegeneratedENS_9IndexTypeIjNS_19FaceIndex_tag_type_EEE(%"class.draco::CornerTable"*, i32) #2

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco11CornerTable8OppositeENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::CornerTable"* %this, i32 %corner.coerce) #0 comdat {
entry:
  %retval = alloca %"class.draco::IndexType.9", align 4
  %corner = alloca %"class.draco::IndexType.9", align 4
  %this.addr = alloca %"class.draco::CornerTable"*, align 4
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %corner, i32 0, i32 0
  store i32 %corner.coerce, i32* %coerce.dive, align 4
  store %"class.draco::CornerTable"* %this, %"class.draco::CornerTable"** %this.addr, align 4
  %this1 = load %"class.draco::CornerTable"*, %"class.draco::CornerTable"** %this.addr, align 4
  %call = call zeroext i1 @_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEeqERKS2_(%"class.draco::IndexType.9"* %corner, %"class.draco::IndexType.9"* nonnull align 4 dereferenceable(4) @_ZN5dracoL19kInvalidCornerIndexE)
  br i1 %call, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %0 = bitcast %"class.draco::IndexType.9"* %retval to i8*
  %1 = bitcast %"class.draco::IndexType.9"* %corner to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %0, i8* align 4 %1, i32 4, i1 false)
  br label %return

if.end:                                           ; preds = %entry
  %opposite_corners_ = getelementptr inbounds %"class.draco::CornerTable", %"class.draco::CornerTable"* %this1, i32 0, i32 1
  %call2 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.9"* @_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_21CornerIndex_tag_type_EEES3_EixERKS3_(%"class.draco::IndexTypeVector.23"* %opposite_corners_, %"class.draco::IndexType.9"* nonnull align 4 dereferenceable(4) %corner)
  %2 = bitcast %"class.draco::IndexType.9"* %retval to i8*
  %3 = bitcast %"class.draco::IndexType.9"* %call2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 4, i1 false)
  br label %return

return:                                           ; preds = %if.end, %if.then
  %coerce.dive3 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %retval, i32 0, i32 0
  %4 = load i32, i32* %coerce.dive3, align 4
  ret i32 %4
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEeqERKS2_(%"class.draco::IndexType.9"* %this, %"class.draco::IndexType.9"* nonnull align 4 dereferenceable(4) %i) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.9"*, align 4
  %i.addr = alloca %"class.draco::IndexType.9"*, align 4
  store %"class.draco::IndexType.9"* %this, %"class.draco::IndexType.9"** %this.addr, align 4
  store %"class.draco::IndexType.9"* %i, %"class.draco::IndexType.9"** %i.addr, align 4
  %this1 = load %"class.draco::IndexType.9"*, %"class.draco::IndexType.9"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value_, align 4
  %1 = load %"class.draco::IndexType.9"*, %"class.draco::IndexType.9"** %i.addr, align 4
  %value_2 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %1, i32 0, i32 0
  %2 = load i32, i32* %value_2, align 4
  %cmp = icmp eq i32 %0, %2
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIbNS_9allocatorIbEEEixEm(%"class.std::__2::__bit_reference"* noalias sret align 4 %agg.result, %"class.std::__2::vector"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__26vectorIbNS_9allocatorIbEEE10__make_refEm(%"class.std::__2::__bit_reference"* sret align 4 %agg.result, %"class.std::__2::vector"* %this1, i32 %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EE5valueEv(%"class.draco::IndexType.9"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.9"*, align 4
  store %"class.draco::IndexType.9"* %this, %"class.draco::IndexType.9"** %this.addr, align 4
  %this1 = load %"class.draco::IndexType.9"*, %"class.draco::IndexType.9"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value_, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(8) %"class.std::__2::__bit_reference"* @_ZNSt3__215__bit_referenceINS_6vectorIbNS_9allocatorIbEEEELb1EEaSEb(%"class.std::__2::__bit_reference"* %this, i1 zeroext %__x) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__bit_reference"*, align 4
  %__x.addr = alloca i8, align 1
  store %"class.std::__2::__bit_reference"* %this, %"class.std::__2::__bit_reference"** %this.addr, align 4
  %frombool = zext i1 %__x to i8
  store i8 %frombool, i8* %__x.addr, align 1
  %this1 = load %"class.std::__2::__bit_reference"*, %"class.std::__2::__bit_reference"** %this.addr, align 4
  %0 = load i8, i8* %__x.addr, align 1
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %__mask_ = getelementptr inbounds %"class.std::__2::__bit_reference", %"class.std::__2::__bit_reference"* %this1, i32 0, i32 1
  %1 = load i32, i32* %__mask_, align 4
  %__seg_ = getelementptr inbounds %"class.std::__2::__bit_reference", %"class.std::__2::__bit_reference"* %this1, i32 0, i32 0
  %2 = load i32*, i32** %__seg_, align 4
  %3 = load i32, i32* %2, align 4
  %or = or i32 %3, %1
  store i32 %or, i32* %2, align 4
  br label %if.end

if.else:                                          ; preds = %entry
  %__mask_2 = getelementptr inbounds %"class.std::__2::__bit_reference", %"class.std::__2::__bit_reference"* %this1, i32 0, i32 1
  %4 = load i32, i32* %__mask_2, align 4
  %neg = xor i32 %4, -1
  %__seg_3 = getelementptr inbounds %"class.std::__2::__bit_reference", %"class.std::__2::__bit_reference"* %this1, i32 0, i32 0
  %5 = load i32*, i32** %__seg_3, align 4
  %6 = load i32, i32* %5, align 4
  %and = and i32 %6, %neg
  store i32 %and, i32* %5, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret %"class.std::__2::__bit_reference"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType"* @_ZN5draco9IndexTypeIjNS_21VertexIndex_tag_type_EEC2Ev(%"class.draco::IndexType"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.draco::IndexType"* %this, %"class.draco::IndexType"** %this.addr, align 4
  %this1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %this1, i32 0, i32 0
  store i32 0, i32* %value_, align 4
  ret %"class.draco::IndexType"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco11CornerTable6VertexENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::CornerTable"* %this, i32 %corner.coerce) #0 comdat {
entry:
  %retval = alloca %"class.draco::IndexType", align 4
  %corner = alloca %"class.draco::IndexType.9", align 4
  %this.addr = alloca %"class.draco::CornerTable"*, align 4
  %agg.tmp = alloca %"class.draco::IndexType.9", align 4
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %corner, i32 0, i32 0
  store i32 %corner.coerce, i32* %coerce.dive, align 4
  store %"class.draco::CornerTable"* %this, %"class.draco::CornerTable"** %this.addr, align 4
  %this1 = load %"class.draco::CornerTable"*, %"class.draco::CornerTable"** %this.addr, align 4
  %call = call zeroext i1 @_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEeqERKS2_(%"class.draco::IndexType.9"* %corner, %"class.draco::IndexType.9"* nonnull align 4 dereferenceable(4) @_ZN5dracoL19kInvalidCornerIndexE)
  br i1 %call, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %0 = bitcast %"class.draco::IndexType"* %retval to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %0, i8* align 4 bitcast (%"class.draco::IndexType"* @_ZN5dracoL19kInvalidVertexIndexE to i8*), i32 4, i1 false)
  br label %return

if.end:                                           ; preds = %entry
  %1 = bitcast %"class.draco::IndexType.9"* %agg.tmp to i8*
  %2 = bitcast %"class.draco::IndexType.9"* %corner to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 4, i1 false)
  %coerce.dive2 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %agg.tmp, i32 0, i32 0
  %3 = load i32, i32* %coerce.dive2, align 4
  %call3 = call i32 @_ZNK5draco11CornerTable15ConfidentVertexENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::CornerTable"* %this1, i32 %3)
  %coerce.dive4 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %retval, i32 0, i32 0
  store i32 %call3, i32* %coerce.dive4, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %coerce.dive5 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %retval, i32 0, i32 0
  %4 = load i32, i32* %coerce.dive5, align 4
  ret i32 %4
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco11CornerTable4NextENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::CornerTable"* %this, i32 %corner.coerce) #0 comdat {
entry:
  %retval = alloca %"class.draco::IndexType.9", align 4
  %corner = alloca %"class.draco::IndexType.9", align 4
  %this.addr = alloca %"class.draco::CornerTable"*, align 4
  %agg.tmp = alloca %"class.draco::IndexType.9", align 4
  %ref.tmp = alloca i32, align 4
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %corner, i32 0, i32 0
  store i32 %corner.coerce, i32* %coerce.dive, align 4
  store %"class.draco::CornerTable"* %this, %"class.draco::CornerTable"** %this.addr, align 4
  %this1 = load %"class.draco::CornerTable"*, %"class.draco::CornerTable"** %this.addr, align 4
  %call = call zeroext i1 @_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEeqERKS2_(%"class.draco::IndexType.9"* %corner, %"class.draco::IndexType.9"* nonnull align 4 dereferenceable(4) @_ZN5dracoL19kInvalidCornerIndexE)
  br i1 %call, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %0 = bitcast %"class.draco::IndexType.9"* %retval to i8*
  %1 = bitcast %"class.draco::IndexType.9"* %corner to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %0, i8* align 4 %1, i32 4, i1 false)
  br label %return

if.end:                                           ; preds = %entry
  %call2 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.9"* @_ZN5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEppEv(%"class.draco::IndexType.9"* %corner)
  %2 = bitcast %"class.draco::IndexType.9"* %agg.tmp to i8*
  %3 = bitcast %"class.draco::IndexType.9"* %call2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 4, i1 false)
  %coerce.dive3 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %agg.tmp, i32 0, i32 0
  %4 = load i32, i32* %coerce.dive3, align 4
  %call4 = call i32 @_ZNK5draco11CornerTable10LocalIndexENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::CornerTable"* %this1, i32 %4)
  %tobool = icmp ne i32 %call4, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end
  %5 = bitcast %"class.draco::IndexType.9"* %retval to i8*
  %6 = bitcast %"class.draco::IndexType.9"* %corner to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %5, i8* align 4 %6, i32 4, i1 false)
  br label %cond.end

cond.false:                                       ; preds = %if.end
  store i32 3, i32* %ref.tmp, align 4
  %call5 = call i32 @_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEmiERKj(%"class.draco::IndexType.9"* %corner, i32* nonnull align 4 dereferenceable(4) %ref.tmp)
  %coerce.dive6 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %retval, i32 0, i32 0
  store i32 %call5, i32* %coerce.dive6, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  br label %return

return:                                           ; preds = %cond.end, %if.then
  %coerce.dive7 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %retval, i32 0, i32 0
  %7 = load i32, i32* %coerce.dive7, align 4
  ret i32 %7
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZN5draco9IndexTypeIjNS_21VertexIndex_tag_type_EEaSERKS2_(%"class.draco::IndexType"* %this, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %i) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType"*, align 4
  %i.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.draco::IndexType"* %this, %"class.draco::IndexType"** %this.addr, align 4
  store %"class.draco::IndexType"* %i, %"class.draco::IndexType"** %i.addr, align 4
  %this1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %this.addr, align 4
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %i.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %0, i32 0, i32 0
  %1 = load i32, i32* %value_, align 4
  %value_2 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %this1, i32 0, i32 0
  store i32 %1, i32* %value_2, align 4
  ret %"class.draco::IndexType"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco9IndexTypeIjNS_21VertexIndex_tag_type_EE5valueEv(%"class.draco::IndexType"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.draco::IndexType"* %this, %"class.draco::IndexType"** %this.addr, align 4
  %this1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value_, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco11CornerTable8PreviousENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::CornerTable"* %this, i32 %corner.coerce) #0 comdat {
entry:
  %retval = alloca %"class.draco::IndexType.9", align 4
  %corner = alloca %"class.draco::IndexType.9", align 4
  %this.addr = alloca %"class.draco::CornerTable"*, align 4
  %agg.tmp = alloca %"class.draco::IndexType.9", align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp6 = alloca i32, align 4
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %corner, i32 0, i32 0
  store i32 %corner.coerce, i32* %coerce.dive, align 4
  store %"class.draco::CornerTable"* %this, %"class.draco::CornerTable"** %this.addr, align 4
  %this1 = load %"class.draco::CornerTable"*, %"class.draco::CornerTable"** %this.addr, align 4
  %call = call zeroext i1 @_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEeqERKS2_(%"class.draco::IndexType.9"* %corner, %"class.draco::IndexType.9"* nonnull align 4 dereferenceable(4) @_ZN5dracoL19kInvalidCornerIndexE)
  br i1 %call, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %0 = bitcast %"class.draco::IndexType.9"* %retval to i8*
  %1 = bitcast %"class.draco::IndexType.9"* %corner to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %0, i8* align 4 %1, i32 4, i1 false)
  br label %return

if.end:                                           ; preds = %entry
  %2 = bitcast %"class.draco::IndexType.9"* %agg.tmp to i8*
  %3 = bitcast %"class.draco::IndexType.9"* %corner to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 4, i1 false)
  %coerce.dive2 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %agg.tmp, i32 0, i32 0
  %4 = load i32, i32* %coerce.dive2, align 4
  %call3 = call i32 @_ZNK5draco11CornerTable10LocalIndexENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::CornerTable"* %this1, i32 %4)
  %tobool = icmp ne i32 %call3, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end
  store i32 1, i32* %ref.tmp, align 4
  %call4 = call i32 @_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEmiERKj(%"class.draco::IndexType.9"* %corner, i32* nonnull align 4 dereferenceable(4) %ref.tmp)
  %coerce.dive5 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %retval, i32 0, i32 0
  store i32 %call4, i32* %coerce.dive5, align 4
  br label %cond.end

cond.false:                                       ; preds = %if.end
  store i32 2, i32* %ref.tmp6, align 4
  %call7 = call i32 @_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEplERKj(%"class.draco::IndexType.9"* %corner, i32* nonnull align 4 dereferenceable(4) %ref.tmp6)
  %coerce.dive8 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %retval, i32 0, i32 0
  store i32 %call7, i32* %coerce.dive8, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  br label %return

return:                                           ; preds = %cond.end, %if.then
  %coerce.dive9 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %retval, i32 0, i32 0
  %5 = load i32, i32* %coerce.dive9, align 4
  ret i32 %5
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEltERKS2_(%"class.draco::IndexType.9"* %this, %"class.draco::IndexType.9"* nonnull align 4 dereferenceable(4) %i) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.9"*, align 4
  %i.addr = alloca %"class.draco::IndexType.9"*, align 4
  store %"class.draco::IndexType.9"* %this, %"class.draco::IndexType.9"** %this.addr, align 4
  store %"class.draco::IndexType.9"* %i, %"class.draco::IndexType.9"** %i.addr, align 4
  %this1 = load %"class.draco::IndexType.9"*, %"class.draco::IndexType.9"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value_, align 4
  %1 = load %"class.draco::IndexType.9"*, %"class.draco::IndexType.9"** %i.addr, align 4
  %value_2 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %1, i32 0, i32 0
  %2 = load i32, i32* %value_2, align 4
  %cmp = icmp ult i32 %0, %2
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.9"* @_ZN5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEaSERKS2_(%"class.draco::IndexType.9"* %this, %"class.draco::IndexType.9"* nonnull align 4 dereferenceable(4) %i) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.9"*, align 4
  %i.addr = alloca %"class.draco::IndexType.9"*, align 4
  store %"class.draco::IndexType.9"* %this, %"class.draco::IndexType.9"** %this.addr, align 4
  store %"class.draco::IndexType.9"* %i, %"class.draco::IndexType.9"** %i.addr, align 4
  %this1 = load %"class.draco::IndexType.9"*, %"class.draco::IndexType.9"** %this.addr, align 4
  %0 = load %"class.draco::IndexType.9"*, %"class.draco::IndexType.9"** %i.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %0, i32 0, i32 0
  %1 = load i32, i32* %value_, align 4
  %value_2 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %this1, i32 0, i32 0
  store i32 %1, i32* %value_2, align 4
  ret %"class.draco::IndexType.9"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco4Mesh15CornerToPointIdEi(%"class.draco::Mesh"* %this, i32 %ci) #0 comdat {
entry:
  %retval = alloca %"class.draco::IndexType.139", align 4
  %this.addr = alloca %"class.draco::Mesh"*, align 4
  %ci.addr = alloca i32, align 4
  %agg.tmp = alloca %"class.draco::IndexType.145", align 4
  store %"class.draco::Mesh"* %this, %"class.draco::Mesh"** %this.addr, align 4
  store i32 %ci, i32* %ci.addr, align 4
  %this1 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %this.addr, align 4
  %0 = load i32, i32* %ci.addr, align 4
  %call = call i32 @_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EE5valueEv(%"class.draco::IndexType.9"* @_ZN5dracoL19kInvalidCornerIndexE)
  %cmp = icmp eq i32 %0, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %"class.draco::IndexType.139"* %retval to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 bitcast (%"class.draco::IndexType.139"* @_ZN5dracoL18kInvalidPointIndexE to i8*), i32 4, i1 false)
  br label %return

if.end:                                           ; preds = %entry
  %2 = load i32, i32* %ci.addr, align 4
  %div = sdiv i32 %2, 3
  %call2 = call %"class.draco::IndexType.145"* @_ZN5draco9IndexTypeIjNS_19FaceIndex_tag_type_EEC2Ej(%"class.draco::IndexType.145"* %agg.tmp, i32 %div)
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType.145", %"class.draco::IndexType.145"* %agg.tmp, i32 0, i32 0
  %3 = load i32, i32* %coerce.dive, align 4
  %call3 = call nonnull align 4 dereferenceable(12) %"struct.std::__2::array"* @_ZNK5draco4Mesh4faceENS_9IndexTypeIjNS_19FaceIndex_tag_type_EEE(%"class.draco::Mesh"* %this1, i32 %3)
  %4 = load i32, i32* %ci.addr, align 4
  %rem = srem i32 %4, 3
  %call4 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.139"* @_ZNKSt3__25arrayIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELm3EEixEm(%"struct.std::__2::array"* %call3, i32 %rem) #8
  %5 = bitcast %"class.draco::IndexType.139"* %retval to i8*
  %6 = bitcast %"class.draco::IndexType.139"* %call4 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %5, i8* align 4 %6, i32 4, i1 false)
  br label %return

return:                                           ; preds = %if.end, %if.then
  %coerce.dive5 = getelementptr inbounds %"class.draco::IndexType.139", %"class.draco::IndexType.139"* %retval, i32 0, i32 0
  %7 = load i32, i32* %coerce.dive5, align 4
  ret i32 %7
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco14PointAttribute12mapped_indexENS_9IndexTypeIjNS_20PointIndex_tag_type_EEE(%"class.draco::PointAttribute"* %this, i32 %point_index.coerce) #0 comdat {
entry:
  %retval = alloca %"class.draco::IndexType.17", align 4
  %point_index = alloca %"class.draco::IndexType.139", align 4
  %this.addr = alloca %"class.draco::PointAttribute"*, align 4
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType.139", %"class.draco::IndexType.139"* %point_index, i32 0, i32 0
  store i32 %point_index.coerce, i32* %coerce.dive, align 4
  store %"class.draco::PointAttribute"* %this, %"class.draco::PointAttribute"** %this.addr, align 4
  %this1 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %this.addr, align 4
  %identity_mapping_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 4
  %0 = load i8, i8* %identity_mapping_, align 4
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call = call i32 @_ZNK5draco9IndexTypeIjNS_20PointIndex_tag_type_EE5valueEv(%"class.draco::IndexType.139"* %point_index)
  %call2 = call %"class.draco::IndexType.17"* @_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEC2Ej(%"class.draco::IndexType.17"* %retval, i32 %call)
  br label %return

if.end:                                           ; preds = %entry
  %indices_map_ = getelementptr inbounds %"class.draco::PointAttribute", %"class.draco::PointAttribute"* %this1, i32 0, i32 2
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.17"* @_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEEEixERKS3_(%"class.draco::IndexTypeVector.116"* %indices_map_, %"class.draco::IndexType.139"* nonnull align 4 dereferenceable(4) %point_index)
  %1 = bitcast %"class.draco::IndexType.17"* %retval to i8*
  %2 = bitcast %"class.draco::IndexType.17"* %call3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 4, i1 false)
  br label %return

return:                                           ; preds = %if.end, %if.then
  %coerce.dive4 = getelementptr inbounds %"class.draco::IndexType.17", %"class.draco::IndexType.17"* %retval, i32 0, i32 0
  %3 = load i32, i32* %coerce.dive4, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEneERKS2_(%"class.draco::IndexType.17"* %this, %"class.draco::IndexType.17"* nonnull align 4 dereferenceable(4) %i) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.17"*, align 4
  %i.addr = alloca %"class.draco::IndexType.17"*, align 4
  store %"class.draco::IndexType.17"* %this, %"class.draco::IndexType.17"** %this.addr, align 4
  store %"class.draco::IndexType.17"* %i, %"class.draco::IndexType.17"** %i.addr, align 4
  %this1 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.17", %"class.draco::IndexType.17"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value_, align 4
  %1 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %i.addr, align 4
  %value_2 = getelementptr inbounds %"class.draco::IndexType.17", %"class.draco::IndexType.17"* %1, i32 0, i32 0
  %2 = load i32, i32* %value_2, align 4
  %cmp = icmp ne i32 %0, %2
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.9"* @_ZN5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEppEv(%"class.draco::IndexType.9"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.9"*, align 4
  store %"class.draco::IndexType.9"* %this, %"class.draco::IndexType.9"** %this.addr, align 4
  %this1 = load %"class.draco::IndexType.9"*, %"class.draco::IndexType.9"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value_, align 4
  %inc = add i32 %0, 1
  store i32 %inc, i32* %value_, align 4
  ret %"class.draco::IndexType.9"* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN5draco24MeshAttributeCornerTable17RecomputeVerticesEPKNS_4MeshEPKNS_14PointAttributeE(%"class.draco::MeshAttributeCornerTable"* %this, %"class.draco::Mesh"* %mesh, %"class.draco::PointAttribute"* %att) #0 {
entry:
  %this.addr = alloca %"class.draco::MeshAttributeCornerTable"*, align 4
  %mesh.addr = alloca %"class.draco::Mesh"*, align 4
  %att.addr = alloca %"class.draco::PointAttribute"*, align 4
  store %"class.draco::MeshAttributeCornerTable"* %this, %"class.draco::MeshAttributeCornerTable"** %this.addr, align 4
  store %"class.draco::Mesh"* %mesh, %"class.draco::Mesh"** %mesh.addr, align 4
  store %"class.draco::PointAttribute"* %att, %"class.draco::PointAttribute"** %att.addr, align 4
  %this1 = load %"class.draco::MeshAttributeCornerTable"*, %"class.draco::MeshAttributeCornerTable"** %this.addr, align 4
  %0 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %mesh.addr, align 4
  %cmp = icmp ne %"class.draco::Mesh"* %0, null
  br i1 %cmp, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %entry
  %1 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %att.addr, align 4
  %cmp2 = icmp ne %"class.draco::PointAttribute"* %1, null
  br i1 %cmp2, label %if.then, label %if.else

if.then:                                          ; preds = %land.lhs.true
  %2 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %mesh.addr, align 4
  %3 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %att.addr, align 4
  call void @_ZN5draco24MeshAttributeCornerTable25RecomputeVerticesInternalILb1EEEvPKNS_4MeshEPKNS_14PointAttributeE(%"class.draco::MeshAttributeCornerTable"* %this1, %"class.draco::Mesh"* %2, %"class.draco::PointAttribute"* %3)
  br label %if.end

if.else:                                          ; preds = %land.lhs.true, %entry
  call void @_ZN5draco24MeshAttributeCornerTable25RecomputeVerticesInternalILb0EEEvPKNS_4MeshEPKNS_14PointAttributeE(%"class.draco::MeshAttributeCornerTable"* %this1, %"class.draco::Mesh"* null, %"class.draco::PointAttribute"* null)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN5draco24MeshAttributeCornerTable11AddSeamEdgeENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::MeshAttributeCornerTable"* %this, i32 %c.coerce) #0 {
entry:
  %c = alloca %"class.draco::IndexType.9", align 4
  %this.addr = alloca %"class.draco::MeshAttributeCornerTable"*, align 4
  %ref.tmp = alloca %"class.std::__2::__bit_reference", align 4
  %ref.tmp3 = alloca %"class.std::__2::__bit_reference", align 4
  %ref.tmp4 = alloca %"class.draco::IndexType", align 4
  %agg.tmp = alloca %"class.draco::IndexType.9", align 4
  %agg.tmp6 = alloca %"class.draco::IndexType.9", align 4
  %ref.tmp15 = alloca %"class.std::__2::__bit_reference", align 4
  %ref.tmp17 = alloca %"class.draco::IndexType", align 4
  %agg.tmp19 = alloca %"class.draco::IndexType.9", align 4
  %agg.tmp21 = alloca %"class.draco::IndexType.9", align 4
  %opp_corner = alloca %"class.draco::IndexType.9", align 4
  %agg.tmp31 = alloca %"class.draco::IndexType.9", align 4
  %ref.tmp36 = alloca %"class.std::__2::__bit_reference", align 4
  %ref.tmp40 = alloca %"class.std::__2::__bit_reference", align 4
  %ref.tmp42 = alloca %"class.draco::IndexType", align 4
  %agg.tmp44 = alloca %"class.draco::IndexType.9", align 4
  %agg.tmp46 = alloca %"class.draco::IndexType.9", align 4
  %ref.tmp55 = alloca %"class.std::__2::__bit_reference", align 4
  %ref.tmp57 = alloca %"class.draco::IndexType", align 4
  %agg.tmp59 = alloca %"class.draco::IndexType.9", align 4
  %agg.tmp61 = alloca %"class.draco::IndexType.9", align 4
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %c, i32 0, i32 0
  store i32 %c.coerce, i32* %coerce.dive, align 4
  store %"class.draco::MeshAttributeCornerTable"* %this, %"class.draco::MeshAttributeCornerTable"** %this.addr, align 4
  %this1 = load %"class.draco::MeshAttributeCornerTable"*, %"class.draco::MeshAttributeCornerTable"** %this.addr, align 4
  %is_edge_on_seam_ = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 0
  %call = call i32 @_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EE5valueEv(%"class.draco::IndexType.9"* %c)
  call void @_ZNSt3__26vectorIbNS_9allocatorIbEEEixEm(%"class.std::__2::__bit_reference"* sret align 4 %ref.tmp, %"class.std::__2::vector"* %is_edge_on_seam_, i32 %call)
  %call2 = call nonnull align 4 dereferenceable(8) %"class.std::__2::__bit_reference"* @_ZNSt3__215__bit_referenceINS_6vectorIbNS_9allocatorIbEEEELb1EEaSEb(%"class.std::__2::__bit_reference"* %ref.tmp, i1 zeroext true) #8
  %is_vertex_on_seam_ = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 1
  %corner_table_ = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 6
  %0 = load %"class.draco::CornerTable"*, %"class.draco::CornerTable"** %corner_table_, align 4
  %corner_table_5 = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 6
  %1 = load %"class.draco::CornerTable"*, %"class.draco::CornerTable"** %corner_table_5, align 4
  %2 = bitcast %"class.draco::IndexType.9"* %agg.tmp6 to i8*
  %3 = bitcast %"class.draco::IndexType.9"* %c to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 4, i1 false)
  %coerce.dive7 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %agg.tmp6, i32 0, i32 0
  %4 = load i32, i32* %coerce.dive7, align 4
  %call8 = call i32 @_ZNK5draco11CornerTable4NextENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::CornerTable"* %1, i32 %4)
  %coerce.dive9 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %agg.tmp, i32 0, i32 0
  store i32 %call8, i32* %coerce.dive9, align 4
  %coerce.dive10 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %agg.tmp, i32 0, i32 0
  %5 = load i32, i32* %coerce.dive10, align 4
  %call11 = call i32 @_ZNK5draco11CornerTable6VertexENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::CornerTable"* %0, i32 %5)
  %coerce.dive12 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %ref.tmp4, i32 0, i32 0
  store i32 %call11, i32* %coerce.dive12, align 4
  %call13 = call i32 @_ZNK5draco9IndexTypeIjNS_21VertexIndex_tag_type_EE5valueEv(%"class.draco::IndexType"* %ref.tmp4)
  call void @_ZNSt3__26vectorIbNS_9allocatorIbEEEixEm(%"class.std::__2::__bit_reference"* sret align 4 %ref.tmp3, %"class.std::__2::vector"* %is_vertex_on_seam_, i32 %call13)
  %call14 = call nonnull align 4 dereferenceable(8) %"class.std::__2::__bit_reference"* @_ZNSt3__215__bit_referenceINS_6vectorIbNS_9allocatorIbEEEELb1EEaSEb(%"class.std::__2::__bit_reference"* %ref.tmp3, i1 zeroext true) #8
  %is_vertex_on_seam_16 = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 1
  %corner_table_18 = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 6
  %6 = load %"class.draco::CornerTable"*, %"class.draco::CornerTable"** %corner_table_18, align 4
  %corner_table_20 = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 6
  %7 = load %"class.draco::CornerTable"*, %"class.draco::CornerTable"** %corner_table_20, align 4
  %8 = bitcast %"class.draco::IndexType.9"* %agg.tmp21 to i8*
  %9 = bitcast %"class.draco::IndexType.9"* %c to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %8, i8* align 4 %9, i32 4, i1 false)
  %coerce.dive22 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %agg.tmp21, i32 0, i32 0
  %10 = load i32, i32* %coerce.dive22, align 4
  %call23 = call i32 @_ZNK5draco11CornerTable8PreviousENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::CornerTable"* %7, i32 %10)
  %coerce.dive24 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %agg.tmp19, i32 0, i32 0
  store i32 %call23, i32* %coerce.dive24, align 4
  %coerce.dive25 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %agg.tmp19, i32 0, i32 0
  %11 = load i32, i32* %coerce.dive25, align 4
  %call26 = call i32 @_ZNK5draco11CornerTable6VertexENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::CornerTable"* %6, i32 %11)
  %coerce.dive27 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %ref.tmp17, i32 0, i32 0
  store i32 %call26, i32* %coerce.dive27, align 4
  %call28 = call i32 @_ZNK5draco9IndexTypeIjNS_21VertexIndex_tag_type_EE5valueEv(%"class.draco::IndexType"* %ref.tmp17)
  call void @_ZNSt3__26vectorIbNS_9allocatorIbEEEixEm(%"class.std::__2::__bit_reference"* sret align 4 %ref.tmp15, %"class.std::__2::vector"* %is_vertex_on_seam_16, i32 %call28)
  %call29 = call nonnull align 4 dereferenceable(8) %"class.std::__2::__bit_reference"* @_ZNSt3__215__bit_referenceINS_6vectorIbNS_9allocatorIbEEEELb1EEaSEb(%"class.std::__2::__bit_reference"* %ref.tmp15, i1 zeroext true) #8
  %corner_table_30 = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 6
  %12 = load %"class.draco::CornerTable"*, %"class.draco::CornerTable"** %corner_table_30, align 4
  %13 = bitcast %"class.draco::IndexType.9"* %agg.tmp31 to i8*
  %14 = bitcast %"class.draco::IndexType.9"* %c to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %13, i8* align 4 %14, i32 4, i1 false)
  %coerce.dive32 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %agg.tmp31, i32 0, i32 0
  %15 = load i32, i32* %coerce.dive32, align 4
  %call33 = call i32 @_ZNK5draco11CornerTable8OppositeENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::CornerTable"* %12, i32 %15)
  %coerce.dive34 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %opp_corner, i32 0, i32 0
  store i32 %call33, i32* %coerce.dive34, align 4
  %call35 = call zeroext i1 @_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEneERKS2_(%"class.draco::IndexType.9"* %opp_corner, %"class.draco::IndexType.9"* nonnull align 4 dereferenceable(4) @_ZN5dracoL19kInvalidCornerIndexE)
  br i1 %call35, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %no_interior_seams_ = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 2
  store i8 0, i8* %no_interior_seams_, align 4
  %is_edge_on_seam_37 = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 0
  %call38 = call i32 @_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EE5valueEv(%"class.draco::IndexType.9"* %opp_corner)
  call void @_ZNSt3__26vectorIbNS_9allocatorIbEEEixEm(%"class.std::__2::__bit_reference"* sret align 4 %ref.tmp36, %"class.std::__2::vector"* %is_edge_on_seam_37, i32 %call38)
  %call39 = call nonnull align 4 dereferenceable(8) %"class.std::__2::__bit_reference"* @_ZNSt3__215__bit_referenceINS_6vectorIbNS_9allocatorIbEEEELb1EEaSEb(%"class.std::__2::__bit_reference"* %ref.tmp36, i1 zeroext true) #8
  %is_vertex_on_seam_41 = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 1
  %corner_table_43 = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 6
  %16 = load %"class.draco::CornerTable"*, %"class.draco::CornerTable"** %corner_table_43, align 4
  %corner_table_45 = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 6
  %17 = load %"class.draco::CornerTable"*, %"class.draco::CornerTable"** %corner_table_45, align 4
  %18 = bitcast %"class.draco::IndexType.9"* %agg.tmp46 to i8*
  %19 = bitcast %"class.draco::IndexType.9"* %opp_corner to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %18, i8* align 4 %19, i32 4, i1 false)
  %coerce.dive47 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %agg.tmp46, i32 0, i32 0
  %20 = load i32, i32* %coerce.dive47, align 4
  %call48 = call i32 @_ZNK5draco11CornerTable4NextENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::CornerTable"* %17, i32 %20)
  %coerce.dive49 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %agg.tmp44, i32 0, i32 0
  store i32 %call48, i32* %coerce.dive49, align 4
  %coerce.dive50 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %agg.tmp44, i32 0, i32 0
  %21 = load i32, i32* %coerce.dive50, align 4
  %call51 = call i32 @_ZNK5draco11CornerTable6VertexENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::CornerTable"* %16, i32 %21)
  %coerce.dive52 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %ref.tmp42, i32 0, i32 0
  store i32 %call51, i32* %coerce.dive52, align 4
  %call53 = call i32 @_ZNK5draco9IndexTypeIjNS_21VertexIndex_tag_type_EE5valueEv(%"class.draco::IndexType"* %ref.tmp42)
  call void @_ZNSt3__26vectorIbNS_9allocatorIbEEEixEm(%"class.std::__2::__bit_reference"* sret align 4 %ref.tmp40, %"class.std::__2::vector"* %is_vertex_on_seam_41, i32 %call53)
  %call54 = call nonnull align 4 dereferenceable(8) %"class.std::__2::__bit_reference"* @_ZNSt3__215__bit_referenceINS_6vectorIbNS_9allocatorIbEEEELb1EEaSEb(%"class.std::__2::__bit_reference"* %ref.tmp40, i1 zeroext true) #8
  %is_vertex_on_seam_56 = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 1
  %corner_table_58 = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 6
  %22 = load %"class.draco::CornerTable"*, %"class.draco::CornerTable"** %corner_table_58, align 4
  %corner_table_60 = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 6
  %23 = load %"class.draco::CornerTable"*, %"class.draco::CornerTable"** %corner_table_60, align 4
  %24 = bitcast %"class.draco::IndexType.9"* %agg.tmp61 to i8*
  %25 = bitcast %"class.draco::IndexType.9"* %opp_corner to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %24, i8* align 4 %25, i32 4, i1 false)
  %coerce.dive62 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %agg.tmp61, i32 0, i32 0
  %26 = load i32, i32* %coerce.dive62, align 4
  %call63 = call i32 @_ZNK5draco11CornerTable8PreviousENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::CornerTable"* %23, i32 %26)
  %coerce.dive64 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %agg.tmp59, i32 0, i32 0
  store i32 %call63, i32* %coerce.dive64, align 4
  %coerce.dive65 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %agg.tmp59, i32 0, i32 0
  %27 = load i32, i32* %coerce.dive65, align 4
  %call66 = call i32 @_ZNK5draco11CornerTable6VertexENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::CornerTable"* %22, i32 %27)
  %coerce.dive67 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %ref.tmp57, i32 0, i32 0
  store i32 %call66, i32* %coerce.dive67, align 4
  %call68 = call i32 @_ZNK5draco9IndexTypeIjNS_21VertexIndex_tag_type_EE5valueEv(%"class.draco::IndexType"* %ref.tmp57)
  call void @_ZNSt3__26vectorIbNS_9allocatorIbEEEixEm(%"class.std::__2::__bit_reference"* sret align 4 %ref.tmp55, %"class.std::__2::vector"* %is_vertex_on_seam_56, i32 %call68)
  %call69 = call nonnull align 4 dereferenceable(8) %"class.std::__2::__bit_reference"* @_ZNSt3__215__bit_referenceINS_6vectorIbNS_9allocatorIbEEEELb1EEaSEb(%"class.std::__2::__bit_reference"* %ref.tmp55, i1 zeroext true) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEneERKS2_(%"class.draco::IndexType.9"* %this, %"class.draco::IndexType.9"* nonnull align 4 dereferenceable(4) %i) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.9"*, align 4
  %i.addr = alloca %"class.draco::IndexType.9"*, align 4
  store %"class.draco::IndexType.9"* %this, %"class.draco::IndexType.9"** %this.addr, align 4
  store %"class.draco::IndexType.9"* %i, %"class.draco::IndexType.9"** %i.addr, align 4
  %this1 = load %"class.draco::IndexType.9"*, %"class.draco::IndexType.9"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value_, align 4
  %1 = load %"class.draco::IndexType.9"*, %"class.draco::IndexType.9"** %i.addr, align 4
  %value_2 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %1, i32 0, i32 0
  %2 = load i32, i32* %value_2, align 4
  %cmp = icmp ne i32 %0, %2
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco24MeshAttributeCornerTable25RecomputeVerticesInternalILb1EEEvPKNS_4MeshEPKNS_14PointAttributeE(%"class.draco::MeshAttributeCornerTable"* %this, %"class.draco::Mesh"* %mesh, %"class.draco::PointAttribute"* %att) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::MeshAttributeCornerTable"*, align 4
  %mesh.addr = alloca %"class.draco::Mesh"*, align 4
  %att.addr = alloca %"class.draco::PointAttribute"*, align 4
  %num_new_vertices = alloca i32, align 4
  %v = alloca %"class.draco::IndexType", align 4
  %ref.tmp = alloca i32, align 4
  %c = alloca %"class.draco::IndexType.9", align 4
  %agg.tmp = alloca %"class.draco::IndexType", align 4
  %first_vert_id = alloca %"class.draco::IndexType.17", align 4
  %point_id = alloca %"class.draco::IndexType.139", align 4
  %ref.tmp12 = alloca %"class.draco::IndexType.17", align 4
  %agg.tmp13 = alloca %"class.draco::IndexType.139", align 4
  %first_c = alloca %"class.draco::IndexType.9", align 4
  %act_c = alloca %"class.draco::IndexType.9", align 4
  %ref.tmp18 = alloca %"class.std::__2::__bit_reference", align 4
  %ref.tmp22 = alloca %"class.draco::IndexType.9", align 4
  %agg.tmp23 = alloca %"class.draco::IndexType.9", align 4
  %ref.tmp30 = alloca %"class.draco::IndexType.9", align 4
  %agg.tmp31 = alloca %"class.draco::IndexType.9", align 4
  %ref.tmp37 = alloca %"class.draco::IndexType", align 4
  %ref.tmp43 = alloca %"class.draco::IndexType.9", align 4
  %agg.tmp45 = alloca %"class.draco::IndexType.9", align 4
  %agg.tmp54 = alloca %"class.draco::IndexType.9", align 4
  %agg.tmp56 = alloca %"class.draco::IndexType.9", align 4
  %ref.tmp63 = alloca %"class.draco::IndexType.17", align 4
  %point_id67 = alloca %"class.draco::IndexType.139", align 4
  %ref.tmp72 = alloca %"class.draco::IndexType.17", align 4
  %agg.tmp73 = alloca %"class.draco::IndexType.139", align 4
  %ref.tmp79 = alloca %"class.draco::IndexType", align 4
  %ref.tmp86 = alloca %"class.draco::IndexType.9", align 4
  %agg.tmp88 = alloca %"class.draco::IndexType.9", align 4
  store %"class.draco::MeshAttributeCornerTable"* %this, %"class.draco::MeshAttributeCornerTable"** %this.addr, align 4
  store %"class.draco::Mesh"* %mesh, %"class.draco::Mesh"** %mesh.addr, align 4
  store %"class.draco::PointAttribute"* %att, %"class.draco::PointAttribute"** %att.addr, align 4
  %this1 = load %"class.draco::MeshAttributeCornerTable"*, %"class.draco::MeshAttributeCornerTable"** %this.addr, align 4
  store i32 0, i32* %num_new_vertices, align 4
  %call = call %"class.draco::IndexType"* @_ZN5draco9IndexTypeIjNS_21VertexIndex_tag_type_EEC2Ej(%"class.draco::IndexType"* %v, i32 0)
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %corner_table_ = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 6
  %0 = load %"class.draco::CornerTable"*, %"class.draco::CornerTable"** %corner_table_, align 4
  %call2 = call i32 @_ZNK5draco11CornerTable12num_verticesEv(%"class.draco::CornerTable"* %0)
  store i32 %call2, i32* %ref.tmp, align 4
  %call3 = call zeroext i1 @_ZNK5draco9IndexTypeIjNS_21VertexIndex_tag_type_EEltERKj(%"class.draco::IndexType"* %v, i32* nonnull align 4 dereferenceable(4) %ref.tmp)
  br i1 %call3, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %corner_table_4 = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 6
  %1 = load %"class.draco::CornerTable"*, %"class.draco::CornerTable"** %corner_table_4, align 4
  %2 = bitcast %"class.draco::IndexType"* %agg.tmp to i8*
  %3 = bitcast %"class.draco::IndexType"* %v to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 4, i1 false)
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %agg.tmp, i32 0, i32 0
  %4 = load i32, i32* %coerce.dive, align 4
  %call5 = call i32 @_ZNK5draco11CornerTable14LeftMostCornerENS_9IndexTypeIjNS_21VertexIndex_tag_type_EEE(%"class.draco::CornerTable"* %1, i32 %4)
  %coerce.dive6 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %c, i32 0, i32 0
  store i32 %call5, i32* %coerce.dive6, align 4
  %call7 = call zeroext i1 @_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEeqERKS2_(%"class.draco::IndexType.9"* %c, %"class.draco::IndexType.9"* nonnull align 4 dereferenceable(4) @_ZN5dracoL19kInvalidCornerIndexE)
  br i1 %call7, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  br label %for.inc

if.end:                                           ; preds = %for.body
  %5 = load i32, i32* %num_new_vertices, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %num_new_vertices, align 4
  %call8 = call %"class.draco::IndexType.17"* @_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEC2Ej(%"class.draco::IndexType.17"* %first_vert_id, i32 %5)
  %6 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %mesh.addr, align 4
  %call9 = call i32 @_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EE5valueEv(%"class.draco::IndexType.9"* %c)
  %call10 = call i32 @_ZNK5draco4Mesh15CornerToPointIdEi(%"class.draco::Mesh"* %6, i32 %call9)
  %coerce.dive11 = getelementptr inbounds %"class.draco::IndexType.139", %"class.draco::IndexType.139"* %point_id, i32 0, i32 0
  store i32 %call10, i32* %coerce.dive11, align 4
  %vertex_to_attribute_entry_id_map_ = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 5
  %7 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %att.addr, align 4
  %8 = bitcast %"class.draco::IndexType.139"* %agg.tmp13 to i8*
  %9 = bitcast %"class.draco::IndexType.139"* %point_id to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %8, i8* align 4 %9, i32 4, i1 false)
  %coerce.dive14 = getelementptr inbounds %"class.draco::IndexType.139", %"class.draco::IndexType.139"* %agg.tmp13, i32 0, i32 0
  %10 = load i32, i32* %coerce.dive14, align 4
  %call15 = call i32 @_ZNK5draco14PointAttribute12mapped_indexENS_9IndexTypeIjNS_20PointIndex_tag_type_EEE(%"class.draco::PointAttribute"* %7, i32 %10)
  %coerce.dive16 = getelementptr inbounds %"class.draco::IndexType.17", %"class.draco::IndexType.17"* %ref.tmp12, i32 0, i32 0
  store i32 %call15, i32* %coerce.dive16, align 4
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE9push_backEOS4_(%"class.std::__2::vector.15"* %vertex_to_attribute_entry_id_map_, %"class.draco::IndexType.17"* nonnull align 4 dereferenceable(4) %ref.tmp12)
  %11 = bitcast %"class.draco::IndexType.9"* %first_c to i8*
  %12 = bitcast %"class.draco::IndexType.9"* %c to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %11, i8* align 4 %12, i32 4, i1 false)
  %call17 = call %"class.draco::IndexType.9"* @_ZN5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEC2Ev(%"class.draco::IndexType.9"* %act_c)
  %is_vertex_on_seam_ = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 1
  %call19 = call i32 @_ZNK5draco9IndexTypeIjNS_21VertexIndex_tag_type_EE5valueEv(%"class.draco::IndexType"* %v)
  call void @_ZNSt3__26vectorIbNS_9allocatorIbEEEixEm(%"class.std::__2::__bit_reference"* sret align 4 %ref.tmp18, %"class.std::__2::vector"* %is_vertex_on_seam_, i32 %call19)
  %call20 = call zeroext i1 @_ZNKSt3__215__bit_referenceINS_6vectorIbNS_9allocatorIbEEEELb1EEcvbEv(%"class.std::__2::__bit_reference"* %ref.tmp18) #8
  br i1 %call20, label %if.then21, label %if.end36

if.then21:                                        ; preds = %if.end
  %13 = bitcast %"class.draco::IndexType.9"* %agg.tmp23 to i8*
  %14 = bitcast %"class.draco::IndexType.9"* %first_c to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %13, i8* align 4 %14, i32 4, i1 false)
  %coerce.dive24 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %agg.tmp23, i32 0, i32 0
  %15 = load i32, i32* %coerce.dive24, align 4
  %call25 = call i32 @_ZNK5draco24MeshAttributeCornerTable9SwingLeftENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::MeshAttributeCornerTable"* %this1, i32 %15)
  %coerce.dive26 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %ref.tmp22, i32 0, i32 0
  store i32 %call25, i32* %coerce.dive26, align 4
  %call27 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.9"* @_ZN5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEaSERKS2_(%"class.draco::IndexType.9"* %act_c, %"class.draco::IndexType.9"* nonnull align 4 dereferenceable(4) %ref.tmp22)
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.then21
  %call28 = call zeroext i1 @_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEneERKS2_(%"class.draco::IndexType.9"* %act_c, %"class.draco::IndexType.9"* nonnull align 4 dereferenceable(4) @_ZN5dracoL19kInvalidCornerIndexE)
  br i1 %call28, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call29 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.9"* @_ZN5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEaSERKS2_(%"class.draco::IndexType.9"* %first_c, %"class.draco::IndexType.9"* nonnull align 4 dereferenceable(4) %act_c)
  %16 = bitcast %"class.draco::IndexType.9"* %agg.tmp31 to i8*
  %17 = bitcast %"class.draco::IndexType.9"* %act_c to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %16, i8* align 4 %17, i32 4, i1 false)
  %coerce.dive32 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %agg.tmp31, i32 0, i32 0
  %18 = load i32, i32* %coerce.dive32, align 4
  %call33 = call i32 @_ZNK5draco24MeshAttributeCornerTable9SwingLeftENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::MeshAttributeCornerTable"* %this1, i32 %18)
  %coerce.dive34 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %ref.tmp30, i32 0, i32 0
  store i32 %call33, i32* %coerce.dive34, align 4
  %call35 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.9"* @_ZN5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEaSERKS2_(%"class.draco::IndexType.9"* %act_c, %"class.draco::IndexType.9"* nonnull align 4 dereferenceable(4) %ref.tmp30)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  br label %if.end36

if.end36:                                         ; preds = %while.end, %if.end
  %call38 = call i32 @_ZNK5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EE5valueEv(%"class.draco::IndexType.17"* %first_vert_id)
  %call39 = call %"class.draco::IndexType"* @_ZN5draco9IndexTypeIjNS_21VertexIndex_tag_type_EEC2Ej(%"class.draco::IndexType"* %ref.tmp37, i32 %call38)
  %corner_to_vertex_map_ = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 3
  %call40 = call i32 @_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EE5valueEv(%"class.draco::IndexType.9"* %first_c)
  %call41 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEEixEm(%"class.std::__2::vector.1"* %corner_to_vertex_map_, i32 %call40) #8
  %call42 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZN5draco9IndexTypeIjNS_21VertexIndex_tag_type_EEaSERKS2_(%"class.draco::IndexType"* %call41, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %ref.tmp37)
  %vertex_to_left_most_corner_map_ = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 4
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE9push_backERKS4_(%"class.std::__2::vector.7"* %vertex_to_left_most_corner_map_, %"class.draco::IndexType.9"* nonnull align 4 dereferenceable(4) %first_c)
  %corner_table_44 = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 6
  %19 = load %"class.draco::CornerTable"*, %"class.draco::CornerTable"** %corner_table_44, align 4
  %20 = bitcast %"class.draco::IndexType.9"* %agg.tmp45 to i8*
  %21 = bitcast %"class.draco::IndexType.9"* %first_c to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %20, i8* align 4 %21, i32 4, i1 false)
  %coerce.dive46 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %agg.tmp45, i32 0, i32 0
  %22 = load i32, i32* %coerce.dive46, align 4
  %call47 = call i32 @_ZNK5draco11CornerTable10SwingRightENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::CornerTable"* %19, i32 %22)
  %coerce.dive48 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %ref.tmp43, i32 0, i32 0
  store i32 %call47, i32* %coerce.dive48, align 4
  %call49 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.9"* @_ZN5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEaSERKS2_(%"class.draco::IndexType.9"* %act_c, %"class.draco::IndexType.9"* nonnull align 4 dereferenceable(4) %ref.tmp43)
  br label %while.cond50

while.cond50:                                     ; preds = %if.end78, %if.end36
  %call51 = call zeroext i1 @_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEneERKS2_(%"class.draco::IndexType.9"* %act_c, %"class.draco::IndexType.9"* nonnull align 4 dereferenceable(4) @_ZN5dracoL19kInvalidCornerIndexE)
  br i1 %call51, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %while.cond50
  %call52 = call zeroext i1 @_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEneERKS2_(%"class.draco::IndexType.9"* %act_c, %"class.draco::IndexType.9"* nonnull align 4 dereferenceable(4) %first_c)
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.cond50
  %23 = phi i1 [ false, %while.cond50 ], [ %call52, %land.rhs ]
  br i1 %23, label %while.body53, label %while.end93

while.body53:                                     ; preds = %land.end
  %corner_table_55 = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 6
  %24 = load %"class.draco::CornerTable"*, %"class.draco::CornerTable"** %corner_table_55, align 4
  %25 = bitcast %"class.draco::IndexType.9"* %agg.tmp56 to i8*
  %26 = bitcast %"class.draco::IndexType.9"* %act_c to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %25, i8* align 4 %26, i32 4, i1 false)
  %coerce.dive57 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %agg.tmp56, i32 0, i32 0
  %27 = load i32, i32* %coerce.dive57, align 4
  %call58 = call i32 @_ZNK5draco11CornerTable4NextENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::CornerTable"* %24, i32 %27)
  %coerce.dive59 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %agg.tmp54, i32 0, i32 0
  store i32 %call58, i32* %coerce.dive59, align 4
  %coerce.dive60 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %agg.tmp54, i32 0, i32 0
  %28 = load i32, i32* %coerce.dive60, align 4
  %call61 = call zeroext i1 @_ZNK5draco24MeshAttributeCornerTable26IsCornerOppositeToSeamEdgeENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::MeshAttributeCornerTable"* %this1, i32 %28)
  br i1 %call61, label %if.then62, label %if.end78

if.then62:                                        ; preds = %while.body53
  %29 = load i32, i32* %num_new_vertices, align 4
  %inc64 = add nsw i32 %29, 1
  store i32 %inc64, i32* %num_new_vertices, align 4
  %call65 = call %"class.draco::IndexType.17"* @_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEC2Ej(%"class.draco::IndexType.17"* %ref.tmp63, i32 %29)
  %call66 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.17"* @_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEaSERKS2_(%"class.draco::IndexType.17"* %first_vert_id, %"class.draco::IndexType.17"* nonnull align 4 dereferenceable(4) %ref.tmp63)
  %30 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %mesh.addr, align 4
  %call68 = call i32 @_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EE5valueEv(%"class.draco::IndexType.9"* %act_c)
  %call69 = call i32 @_ZNK5draco4Mesh15CornerToPointIdEi(%"class.draco::Mesh"* %30, i32 %call68)
  %coerce.dive70 = getelementptr inbounds %"class.draco::IndexType.139", %"class.draco::IndexType.139"* %point_id67, i32 0, i32 0
  store i32 %call69, i32* %coerce.dive70, align 4
  %vertex_to_attribute_entry_id_map_71 = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 5
  %31 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %att.addr, align 4
  %32 = bitcast %"class.draco::IndexType.139"* %agg.tmp73 to i8*
  %33 = bitcast %"class.draco::IndexType.139"* %point_id67 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %32, i8* align 4 %33, i32 4, i1 false)
  %coerce.dive74 = getelementptr inbounds %"class.draco::IndexType.139", %"class.draco::IndexType.139"* %agg.tmp73, i32 0, i32 0
  %34 = load i32, i32* %coerce.dive74, align 4
  %call75 = call i32 @_ZNK5draco14PointAttribute12mapped_indexENS_9IndexTypeIjNS_20PointIndex_tag_type_EEE(%"class.draco::PointAttribute"* %31, i32 %34)
  %coerce.dive76 = getelementptr inbounds %"class.draco::IndexType.17", %"class.draco::IndexType.17"* %ref.tmp72, i32 0, i32 0
  store i32 %call75, i32* %coerce.dive76, align 4
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE9push_backEOS4_(%"class.std::__2::vector.15"* %vertex_to_attribute_entry_id_map_71, %"class.draco::IndexType.17"* nonnull align 4 dereferenceable(4) %ref.tmp72)
  %vertex_to_left_most_corner_map_77 = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 4
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE9push_backERKS4_(%"class.std::__2::vector.7"* %vertex_to_left_most_corner_map_77, %"class.draco::IndexType.9"* nonnull align 4 dereferenceable(4) %act_c)
  br label %if.end78

if.end78:                                         ; preds = %if.then62, %while.body53
  %call80 = call i32 @_ZNK5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EE5valueEv(%"class.draco::IndexType.17"* %first_vert_id)
  %call81 = call %"class.draco::IndexType"* @_ZN5draco9IndexTypeIjNS_21VertexIndex_tag_type_EEC2Ej(%"class.draco::IndexType"* %ref.tmp79, i32 %call80)
  %corner_to_vertex_map_82 = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 3
  %call83 = call i32 @_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EE5valueEv(%"class.draco::IndexType.9"* %act_c)
  %call84 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEEixEm(%"class.std::__2::vector.1"* %corner_to_vertex_map_82, i32 %call83) #8
  %call85 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZN5draco9IndexTypeIjNS_21VertexIndex_tag_type_EEaSERKS2_(%"class.draco::IndexType"* %call84, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %ref.tmp79)
  %corner_table_87 = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 6
  %35 = load %"class.draco::CornerTable"*, %"class.draco::CornerTable"** %corner_table_87, align 4
  %36 = bitcast %"class.draco::IndexType.9"* %agg.tmp88 to i8*
  %37 = bitcast %"class.draco::IndexType.9"* %act_c to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %36, i8* align 4 %37, i32 4, i1 false)
  %coerce.dive89 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %agg.tmp88, i32 0, i32 0
  %38 = load i32, i32* %coerce.dive89, align 4
  %call90 = call i32 @_ZNK5draco11CornerTable10SwingRightENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::CornerTable"* %35, i32 %38)
  %coerce.dive91 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %ref.tmp86, i32 0, i32 0
  store i32 %call90, i32* %coerce.dive91, align 4
  %call92 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.9"* @_ZN5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEaSERKS2_(%"class.draco::IndexType.9"* %act_c, %"class.draco::IndexType.9"* nonnull align 4 dereferenceable(4) %ref.tmp86)
  br label %while.cond50

while.end93:                                      ; preds = %land.end
  br label %for.inc

for.inc:                                          ; preds = %while.end93, %if.then
  %call94 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZN5draco9IndexTypeIjNS_21VertexIndex_tag_type_EEppEv(%"class.draco::IndexType"* %v)
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco24MeshAttributeCornerTable25RecomputeVerticesInternalILb0EEEvPKNS_4MeshEPKNS_14PointAttributeE(%"class.draco::MeshAttributeCornerTable"* %this, %"class.draco::Mesh"* %mesh, %"class.draco::PointAttribute"* %att) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::MeshAttributeCornerTable"*, align 4
  %mesh.addr = alloca %"class.draco::Mesh"*, align 4
  %att.addr = alloca %"class.draco::PointAttribute"*, align 4
  %num_new_vertices = alloca i32, align 4
  %v = alloca %"class.draco::IndexType", align 4
  %ref.tmp = alloca i32, align 4
  %c = alloca %"class.draco::IndexType.9", align 4
  %agg.tmp = alloca %"class.draco::IndexType", align 4
  %first_vert_id = alloca %"class.draco::IndexType.17", align 4
  %first_c = alloca %"class.draco::IndexType.9", align 4
  %act_c = alloca %"class.draco::IndexType.9", align 4
  %ref.tmp10 = alloca %"class.std::__2::__bit_reference", align 4
  %ref.tmp14 = alloca %"class.draco::IndexType.9", align 4
  %agg.tmp15 = alloca %"class.draco::IndexType.9", align 4
  %ref.tmp22 = alloca %"class.draco::IndexType.9", align 4
  %agg.tmp23 = alloca %"class.draco::IndexType.9", align 4
  %ref.tmp29 = alloca %"class.draco::IndexType", align 4
  %ref.tmp35 = alloca %"class.draco::IndexType.9", align 4
  %agg.tmp37 = alloca %"class.draco::IndexType.9", align 4
  %agg.tmp46 = alloca %"class.draco::IndexType.9", align 4
  %agg.tmp48 = alloca %"class.draco::IndexType.9", align 4
  %ref.tmp55 = alloca %"class.draco::IndexType.17", align 4
  %ref.tmp62 = alloca %"class.draco::IndexType", align 4
  %ref.tmp69 = alloca %"class.draco::IndexType.9", align 4
  %agg.tmp71 = alloca %"class.draco::IndexType.9", align 4
  store %"class.draco::MeshAttributeCornerTable"* %this, %"class.draco::MeshAttributeCornerTable"** %this.addr, align 4
  store %"class.draco::Mesh"* %mesh, %"class.draco::Mesh"** %mesh.addr, align 4
  store %"class.draco::PointAttribute"* %att, %"class.draco::PointAttribute"** %att.addr, align 4
  %this1 = load %"class.draco::MeshAttributeCornerTable"*, %"class.draco::MeshAttributeCornerTable"** %this.addr, align 4
  store i32 0, i32* %num_new_vertices, align 4
  %call = call %"class.draco::IndexType"* @_ZN5draco9IndexTypeIjNS_21VertexIndex_tag_type_EEC2Ej(%"class.draco::IndexType"* %v, i32 0)
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %corner_table_ = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 6
  %0 = load %"class.draco::CornerTable"*, %"class.draco::CornerTable"** %corner_table_, align 4
  %call2 = call i32 @_ZNK5draco11CornerTable12num_verticesEv(%"class.draco::CornerTable"* %0)
  store i32 %call2, i32* %ref.tmp, align 4
  %call3 = call zeroext i1 @_ZNK5draco9IndexTypeIjNS_21VertexIndex_tag_type_EEltERKj(%"class.draco::IndexType"* %v, i32* nonnull align 4 dereferenceable(4) %ref.tmp)
  br i1 %call3, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %corner_table_4 = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 6
  %1 = load %"class.draco::CornerTable"*, %"class.draco::CornerTable"** %corner_table_4, align 4
  %2 = bitcast %"class.draco::IndexType"* %agg.tmp to i8*
  %3 = bitcast %"class.draco::IndexType"* %v to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 4, i1 false)
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %agg.tmp, i32 0, i32 0
  %4 = load i32, i32* %coerce.dive, align 4
  %call5 = call i32 @_ZNK5draco11CornerTable14LeftMostCornerENS_9IndexTypeIjNS_21VertexIndex_tag_type_EEE(%"class.draco::CornerTable"* %1, i32 %4)
  %coerce.dive6 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %c, i32 0, i32 0
  store i32 %call5, i32* %coerce.dive6, align 4
  %call7 = call zeroext i1 @_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEeqERKS2_(%"class.draco::IndexType.9"* %c, %"class.draco::IndexType.9"* nonnull align 4 dereferenceable(4) @_ZN5dracoL19kInvalidCornerIndexE)
  br i1 %call7, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  br label %for.inc

if.end:                                           ; preds = %for.body
  %5 = load i32, i32* %num_new_vertices, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %num_new_vertices, align 4
  %call8 = call %"class.draco::IndexType.17"* @_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEC2Ej(%"class.draco::IndexType.17"* %first_vert_id, i32 %5)
  %vertex_to_attribute_entry_id_map_ = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 5
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE9push_backERKS4_(%"class.std::__2::vector.15"* %vertex_to_attribute_entry_id_map_, %"class.draco::IndexType.17"* nonnull align 4 dereferenceable(4) %first_vert_id)
  %6 = bitcast %"class.draco::IndexType.9"* %first_c to i8*
  %7 = bitcast %"class.draco::IndexType.9"* %c to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 4, i1 false)
  %call9 = call %"class.draco::IndexType.9"* @_ZN5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEC2Ev(%"class.draco::IndexType.9"* %act_c)
  %is_vertex_on_seam_ = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 1
  %call11 = call i32 @_ZNK5draco9IndexTypeIjNS_21VertexIndex_tag_type_EE5valueEv(%"class.draco::IndexType"* %v)
  call void @_ZNSt3__26vectorIbNS_9allocatorIbEEEixEm(%"class.std::__2::__bit_reference"* sret align 4 %ref.tmp10, %"class.std::__2::vector"* %is_vertex_on_seam_, i32 %call11)
  %call12 = call zeroext i1 @_ZNKSt3__215__bit_referenceINS_6vectorIbNS_9allocatorIbEEEELb1EEcvbEv(%"class.std::__2::__bit_reference"* %ref.tmp10) #8
  br i1 %call12, label %if.then13, label %if.end28

if.then13:                                        ; preds = %if.end
  %8 = bitcast %"class.draco::IndexType.9"* %agg.tmp15 to i8*
  %9 = bitcast %"class.draco::IndexType.9"* %first_c to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %8, i8* align 4 %9, i32 4, i1 false)
  %coerce.dive16 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %agg.tmp15, i32 0, i32 0
  %10 = load i32, i32* %coerce.dive16, align 4
  %call17 = call i32 @_ZNK5draco24MeshAttributeCornerTable9SwingLeftENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::MeshAttributeCornerTable"* %this1, i32 %10)
  %coerce.dive18 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %ref.tmp14, i32 0, i32 0
  store i32 %call17, i32* %coerce.dive18, align 4
  %call19 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.9"* @_ZN5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEaSERKS2_(%"class.draco::IndexType.9"* %act_c, %"class.draco::IndexType.9"* nonnull align 4 dereferenceable(4) %ref.tmp14)
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.then13
  %call20 = call zeroext i1 @_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEneERKS2_(%"class.draco::IndexType.9"* %act_c, %"class.draco::IndexType.9"* nonnull align 4 dereferenceable(4) @_ZN5dracoL19kInvalidCornerIndexE)
  br i1 %call20, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call21 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.9"* @_ZN5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEaSERKS2_(%"class.draco::IndexType.9"* %first_c, %"class.draco::IndexType.9"* nonnull align 4 dereferenceable(4) %act_c)
  %11 = bitcast %"class.draco::IndexType.9"* %agg.tmp23 to i8*
  %12 = bitcast %"class.draco::IndexType.9"* %act_c to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %11, i8* align 4 %12, i32 4, i1 false)
  %coerce.dive24 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %agg.tmp23, i32 0, i32 0
  %13 = load i32, i32* %coerce.dive24, align 4
  %call25 = call i32 @_ZNK5draco24MeshAttributeCornerTable9SwingLeftENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::MeshAttributeCornerTable"* %this1, i32 %13)
  %coerce.dive26 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %ref.tmp22, i32 0, i32 0
  store i32 %call25, i32* %coerce.dive26, align 4
  %call27 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.9"* @_ZN5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEaSERKS2_(%"class.draco::IndexType.9"* %act_c, %"class.draco::IndexType.9"* nonnull align 4 dereferenceable(4) %ref.tmp22)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  br label %if.end28

if.end28:                                         ; preds = %while.end, %if.end
  %call30 = call i32 @_ZNK5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EE5valueEv(%"class.draco::IndexType.17"* %first_vert_id)
  %call31 = call %"class.draco::IndexType"* @_ZN5draco9IndexTypeIjNS_21VertexIndex_tag_type_EEC2Ej(%"class.draco::IndexType"* %ref.tmp29, i32 %call30)
  %corner_to_vertex_map_ = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 3
  %call32 = call i32 @_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EE5valueEv(%"class.draco::IndexType.9"* %first_c)
  %call33 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEEixEm(%"class.std::__2::vector.1"* %corner_to_vertex_map_, i32 %call32) #8
  %call34 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZN5draco9IndexTypeIjNS_21VertexIndex_tag_type_EEaSERKS2_(%"class.draco::IndexType"* %call33, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %ref.tmp29)
  %vertex_to_left_most_corner_map_ = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 4
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE9push_backERKS4_(%"class.std::__2::vector.7"* %vertex_to_left_most_corner_map_, %"class.draco::IndexType.9"* nonnull align 4 dereferenceable(4) %first_c)
  %corner_table_36 = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 6
  %14 = load %"class.draco::CornerTable"*, %"class.draco::CornerTable"** %corner_table_36, align 4
  %15 = bitcast %"class.draco::IndexType.9"* %agg.tmp37 to i8*
  %16 = bitcast %"class.draco::IndexType.9"* %first_c to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %15, i8* align 4 %16, i32 4, i1 false)
  %coerce.dive38 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %agg.tmp37, i32 0, i32 0
  %17 = load i32, i32* %coerce.dive38, align 4
  %call39 = call i32 @_ZNK5draco11CornerTable10SwingRightENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::CornerTable"* %14, i32 %17)
  %coerce.dive40 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %ref.tmp35, i32 0, i32 0
  store i32 %call39, i32* %coerce.dive40, align 4
  %call41 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.9"* @_ZN5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEaSERKS2_(%"class.draco::IndexType.9"* %act_c, %"class.draco::IndexType.9"* nonnull align 4 dereferenceable(4) %ref.tmp35)
  br label %while.cond42

while.cond42:                                     ; preds = %if.end61, %if.end28
  %call43 = call zeroext i1 @_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEneERKS2_(%"class.draco::IndexType.9"* %act_c, %"class.draco::IndexType.9"* nonnull align 4 dereferenceable(4) @_ZN5dracoL19kInvalidCornerIndexE)
  br i1 %call43, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %while.cond42
  %call44 = call zeroext i1 @_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEneERKS2_(%"class.draco::IndexType.9"* %act_c, %"class.draco::IndexType.9"* nonnull align 4 dereferenceable(4) %first_c)
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.cond42
  %18 = phi i1 [ false, %while.cond42 ], [ %call44, %land.rhs ]
  br i1 %18, label %while.body45, label %while.end76

while.body45:                                     ; preds = %land.end
  %corner_table_47 = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 6
  %19 = load %"class.draco::CornerTable"*, %"class.draco::CornerTable"** %corner_table_47, align 4
  %20 = bitcast %"class.draco::IndexType.9"* %agg.tmp48 to i8*
  %21 = bitcast %"class.draco::IndexType.9"* %act_c to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %20, i8* align 4 %21, i32 4, i1 false)
  %coerce.dive49 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %agg.tmp48, i32 0, i32 0
  %22 = load i32, i32* %coerce.dive49, align 4
  %call50 = call i32 @_ZNK5draco11CornerTable4NextENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::CornerTable"* %19, i32 %22)
  %coerce.dive51 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %agg.tmp46, i32 0, i32 0
  store i32 %call50, i32* %coerce.dive51, align 4
  %coerce.dive52 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %agg.tmp46, i32 0, i32 0
  %23 = load i32, i32* %coerce.dive52, align 4
  %call53 = call zeroext i1 @_ZNK5draco24MeshAttributeCornerTable26IsCornerOppositeToSeamEdgeENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::MeshAttributeCornerTable"* %this1, i32 %23)
  br i1 %call53, label %if.then54, label %if.end61

if.then54:                                        ; preds = %while.body45
  %24 = load i32, i32* %num_new_vertices, align 4
  %inc56 = add nsw i32 %24, 1
  store i32 %inc56, i32* %num_new_vertices, align 4
  %call57 = call %"class.draco::IndexType.17"* @_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEC2Ej(%"class.draco::IndexType.17"* %ref.tmp55, i32 %24)
  %call58 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.17"* @_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEaSERKS2_(%"class.draco::IndexType.17"* %first_vert_id, %"class.draco::IndexType.17"* nonnull align 4 dereferenceable(4) %ref.tmp55)
  %vertex_to_attribute_entry_id_map_59 = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 5
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE9push_backERKS4_(%"class.std::__2::vector.15"* %vertex_to_attribute_entry_id_map_59, %"class.draco::IndexType.17"* nonnull align 4 dereferenceable(4) %first_vert_id)
  %vertex_to_left_most_corner_map_60 = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 4
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE9push_backERKS4_(%"class.std::__2::vector.7"* %vertex_to_left_most_corner_map_60, %"class.draco::IndexType.9"* nonnull align 4 dereferenceable(4) %act_c)
  br label %if.end61

if.end61:                                         ; preds = %if.then54, %while.body45
  %call63 = call i32 @_ZNK5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EE5valueEv(%"class.draco::IndexType.17"* %first_vert_id)
  %call64 = call %"class.draco::IndexType"* @_ZN5draco9IndexTypeIjNS_21VertexIndex_tag_type_EEC2Ej(%"class.draco::IndexType"* %ref.tmp62, i32 %call63)
  %corner_to_vertex_map_65 = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 3
  %call66 = call i32 @_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EE5valueEv(%"class.draco::IndexType.9"* %act_c)
  %call67 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEEixEm(%"class.std::__2::vector.1"* %corner_to_vertex_map_65, i32 %call66) #8
  %call68 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZN5draco9IndexTypeIjNS_21VertexIndex_tag_type_EEaSERKS2_(%"class.draco::IndexType"* %call67, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %ref.tmp62)
  %corner_table_70 = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 6
  %25 = load %"class.draco::CornerTable"*, %"class.draco::CornerTable"** %corner_table_70, align 4
  %26 = bitcast %"class.draco::IndexType.9"* %agg.tmp71 to i8*
  %27 = bitcast %"class.draco::IndexType.9"* %act_c to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %26, i8* align 4 %27, i32 4, i1 false)
  %coerce.dive72 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %agg.tmp71, i32 0, i32 0
  %28 = load i32, i32* %coerce.dive72, align 4
  %call73 = call i32 @_ZNK5draco11CornerTable10SwingRightENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::CornerTable"* %25, i32 %28)
  %coerce.dive74 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %ref.tmp69, i32 0, i32 0
  store i32 %call73, i32* %coerce.dive74, align 4
  %call75 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.9"* @_ZN5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEaSERKS2_(%"class.draco::IndexType.9"* %act_c, %"class.draco::IndexType.9"* nonnull align 4 dereferenceable(4) %ref.tmp69)
  br label %while.cond42

while.end76:                                      ; preds = %land.end
  br label %for.inc

for.inc:                                          ; preds = %while.end76, %if.then
  %call77 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZN5draco9IndexTypeIjNS_21VertexIndex_tag_type_EEppEv(%"class.draco::IndexType"* %v)
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @_ZNK5draco24MeshAttributeCornerTable7ValenceENS_9IndexTypeIjNS_21VertexIndex_tag_type_EEE(%"class.draco::MeshAttributeCornerTable"* %this, i32 %v.coerce) #0 {
entry:
  %retval = alloca i32, align 4
  %v = alloca %"class.draco::IndexType", align 4
  %this.addr = alloca %"class.draco::MeshAttributeCornerTable"*, align 4
  %agg.tmp = alloca %"class.draco::IndexType", align 4
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %v, i32 0, i32 0
  store i32 %v.coerce, i32* %coerce.dive, align 4
  store %"class.draco::MeshAttributeCornerTable"* %this, %"class.draco::MeshAttributeCornerTable"** %this.addr, align 4
  %this1 = load %"class.draco::MeshAttributeCornerTable"*, %"class.draco::MeshAttributeCornerTable"** %this.addr, align 4
  %call = call zeroext i1 @_ZNK5draco9IndexTypeIjNS_21VertexIndex_tag_type_EEeqERKS2_(%"class.draco::IndexType"* %v, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) @_ZN5dracoL19kInvalidVertexIndexE)
  br i1 %call, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %0 = bitcast %"class.draco::IndexType"* %agg.tmp to i8*
  %1 = bitcast %"class.draco::IndexType"* %v to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %0, i8* align 4 %1, i32 4, i1 false)
  %coerce.dive2 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %agg.tmp, i32 0, i32 0
  %2 = load i32, i32* %coerce.dive2, align 4
  %call3 = call i32 @_ZNK5draco24MeshAttributeCornerTable16ConfidentValenceENS_9IndexTypeIjNS_21VertexIndex_tag_type_EEE(%"class.draco::MeshAttributeCornerTable"* %this1, i32 %2)
  store i32 %call3, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK5draco9IndexTypeIjNS_21VertexIndex_tag_type_EEeqERKS2_(%"class.draco::IndexType"* %this, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %i) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType"*, align 4
  %i.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.draco::IndexType"* %this, %"class.draco::IndexType"** %this.addr, align 4
  store %"class.draco::IndexType"* %i, %"class.draco::IndexType"** %i.addr, align 4
  %this1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value_, align 4
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %i.addr, align 4
  %value_2 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %1, i32 0, i32 0
  %2 = load i32, i32* %value_2, align 4
  %cmp = icmp eq i32 %0, %2
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @_ZNK5draco24MeshAttributeCornerTable16ConfidentValenceENS_9IndexTypeIjNS_21VertexIndex_tag_type_EEE(%"class.draco::MeshAttributeCornerTable"* %this, i32 %v.coerce) #0 {
entry:
  %v = alloca %"class.draco::IndexType", align 4
  %this.addr = alloca %"class.draco::MeshAttributeCornerTable"*, align 4
  %vi = alloca %"class.draco::VertexRingIterator", align 4
  %agg.tmp = alloca %"class.draco::IndexType", align 4
  %valence = alloca i32, align 4
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %v, i32 0, i32 0
  store i32 %v.coerce, i32* %coerce.dive, align 4
  store %"class.draco::MeshAttributeCornerTable"* %this, %"class.draco::MeshAttributeCornerTable"** %this.addr, align 4
  %this1 = load %"class.draco::MeshAttributeCornerTable"*, %"class.draco::MeshAttributeCornerTable"** %this.addr, align 4
  %0 = bitcast %"class.draco::IndexType"* %agg.tmp to i8*
  %1 = bitcast %"class.draco::IndexType"* %v to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %0, i8* align 4 %1, i32 4, i1 false)
  %coerce.dive2 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %agg.tmp, i32 0, i32 0
  %2 = load i32, i32* %coerce.dive2, align 4
  %call = call %"class.draco::VertexRingIterator"* @_ZN5draco18VertexRingIteratorINS_24MeshAttributeCornerTableEEC2EPKS1_NS_9IndexTypeIjNS_21VertexIndex_tag_type_EEE(%"class.draco::VertexRingIterator"* %vi, %"class.draco::MeshAttributeCornerTable"* %this1, i32 %2)
  store i32 0, i32* %valence, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %call3 = call zeroext i1 @_ZNK5draco18VertexRingIteratorINS_24MeshAttributeCornerTableEE3EndEv(%"class.draco::VertexRingIterator"* %vi)
  %lnot = xor i1 %call3, true
  br i1 %lnot, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load i32, i32* %valence, align 4
  %inc = add nsw i32 %3, 1
  store i32 %inc, i32* %valence, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  call void @_ZN5draco18VertexRingIteratorINS_24MeshAttributeCornerTableEE4NextEv(%"class.draco::VertexRingIterator"* %vi)
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %4 = load i32, i32* %valence, align 4
  ret i32 %4
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::VertexRingIterator"* @_ZN5draco18VertexRingIteratorINS_24MeshAttributeCornerTableEEC2EPKS1_NS_9IndexTypeIjNS_21VertexIndex_tag_type_EEE(%"class.draco::VertexRingIterator"* returned %this, %"class.draco::MeshAttributeCornerTable"* %table, i32 %vert_id.coerce) unnamed_addr #0 comdat {
entry:
  %vert_id = alloca %"class.draco::IndexType", align 4
  %this.addr = alloca %"class.draco::VertexRingIterator"*, align 4
  %table.addr = alloca %"class.draco::MeshAttributeCornerTable"*, align 4
  %agg.tmp = alloca %"class.draco::IndexType", align 4
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %vert_id, i32 0, i32 0
  store i32 %vert_id.coerce, i32* %coerce.dive, align 4
  store %"class.draco::VertexRingIterator"* %this, %"class.draco::VertexRingIterator"** %this.addr, align 4
  store %"class.draco::MeshAttributeCornerTable"* %table, %"class.draco::MeshAttributeCornerTable"** %table.addr, align 4
  %this1 = load %"class.draco::VertexRingIterator"*, %"class.draco::VertexRingIterator"** %this.addr, align 4
  %0 = bitcast %"class.draco::VertexRingIterator"* %this1 to %"struct.std::__2::iterator"*
  %corner_table_ = getelementptr inbounds %"class.draco::VertexRingIterator", %"class.draco::VertexRingIterator"* %this1, i32 0, i32 0
  %1 = load %"class.draco::MeshAttributeCornerTable"*, %"class.draco::MeshAttributeCornerTable"** %table.addr, align 4
  store %"class.draco::MeshAttributeCornerTable"* %1, %"class.draco::MeshAttributeCornerTable"** %corner_table_, align 4
  %start_corner_ = getelementptr inbounds %"class.draco::VertexRingIterator", %"class.draco::VertexRingIterator"* %this1, i32 0, i32 1
  %2 = load %"class.draco::MeshAttributeCornerTable"*, %"class.draco::MeshAttributeCornerTable"** %table.addr, align 4
  %3 = bitcast %"class.draco::IndexType"* %agg.tmp to i8*
  %4 = bitcast %"class.draco::IndexType"* %vert_id to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 4, i1 false)
  %coerce.dive2 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %agg.tmp, i32 0, i32 0
  %5 = load i32, i32* %coerce.dive2, align 4
  %call = call i32 @_ZNK5draco24MeshAttributeCornerTable14LeftMostCornerENS_9IndexTypeIjNS_21VertexIndex_tag_type_EEE(%"class.draco::MeshAttributeCornerTable"* %2, i32 %5)
  %coerce.dive3 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %start_corner_, i32 0, i32 0
  store i32 %call, i32* %coerce.dive3, align 4
  %corner_ = getelementptr inbounds %"class.draco::VertexRingIterator", %"class.draco::VertexRingIterator"* %this1, i32 0, i32 2
  %start_corner_4 = getelementptr inbounds %"class.draco::VertexRingIterator", %"class.draco::VertexRingIterator"* %this1, i32 0, i32 1
  %6 = bitcast %"class.draco::IndexType.9"* %corner_ to i8*
  %7 = bitcast %"class.draco::IndexType.9"* %start_corner_4 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 4, i1 false)
  %left_traversal_ = getelementptr inbounds %"class.draco::VertexRingIterator", %"class.draco::VertexRingIterator"* %this1, i32 0, i32 3
  store i8 1, i8* %left_traversal_, align 4
  ret %"class.draco::VertexRingIterator"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK5draco18VertexRingIteratorINS_24MeshAttributeCornerTableEE3EndEv(%"class.draco::VertexRingIterator"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::VertexRingIterator"*, align 4
  store %"class.draco::VertexRingIterator"* %this, %"class.draco::VertexRingIterator"** %this.addr, align 4
  %this1 = load %"class.draco::VertexRingIterator"*, %"class.draco::VertexRingIterator"** %this.addr, align 4
  %corner_ = getelementptr inbounds %"class.draco::VertexRingIterator", %"class.draco::VertexRingIterator"* %this1, i32 0, i32 2
  %call = call zeroext i1 @_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEeqERKS2_(%"class.draco::IndexType.9"* %corner_, %"class.draco::IndexType.9"* nonnull align 4 dereferenceable(4) @_ZN5dracoL19kInvalidCornerIndexE)
  ret i1 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco18VertexRingIteratorINS_24MeshAttributeCornerTableEE4NextEv(%"class.draco::VertexRingIterator"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::VertexRingIterator"*, align 4
  %ref.tmp = alloca %"class.draco::IndexType.9", align 4
  %agg.tmp = alloca %"class.draco::IndexType.9", align 4
  %ref.tmp19 = alloca %"class.draco::IndexType.9", align 4
  %agg.tmp21 = alloca %"class.draco::IndexType.9", align 4
  store %"class.draco::VertexRingIterator"* %this, %"class.draco::VertexRingIterator"** %this.addr, align 4
  %this1 = load %"class.draco::VertexRingIterator"*, %"class.draco::VertexRingIterator"** %this.addr, align 4
  %left_traversal_ = getelementptr inbounds %"class.draco::VertexRingIterator", %"class.draco::VertexRingIterator"* %this1, i32 0, i32 3
  %0 = load i8, i8* %left_traversal_, align 4
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.else18

if.then:                                          ; preds = %entry
  %corner_table_ = getelementptr inbounds %"class.draco::VertexRingIterator", %"class.draco::VertexRingIterator"* %this1, i32 0, i32 0
  %1 = load %"class.draco::MeshAttributeCornerTable"*, %"class.draco::MeshAttributeCornerTable"** %corner_table_, align 4
  %corner_ = getelementptr inbounds %"class.draco::VertexRingIterator", %"class.draco::VertexRingIterator"* %this1, i32 0, i32 2
  %2 = bitcast %"class.draco::IndexType.9"* %agg.tmp to i8*
  %3 = bitcast %"class.draco::IndexType.9"* %corner_ to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 4, i1 false)
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %agg.tmp, i32 0, i32 0
  %4 = load i32, i32* %coerce.dive, align 4
  %call = call i32 @_ZNK5draco24MeshAttributeCornerTable9SwingLeftENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::MeshAttributeCornerTable"* %1, i32 %4)
  %coerce.dive2 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %ref.tmp, i32 0, i32 0
  store i32 %call, i32* %coerce.dive2, align 4
  %corner_3 = getelementptr inbounds %"class.draco::VertexRingIterator", %"class.draco::VertexRingIterator"* %this1, i32 0, i32 2
  %call4 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.9"* @_ZN5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEaSERKS2_(%"class.draco::IndexType.9"* %corner_3, %"class.draco::IndexType.9"* nonnull align 4 dereferenceable(4) %ref.tmp)
  %corner_5 = getelementptr inbounds %"class.draco::VertexRingIterator", %"class.draco::VertexRingIterator"* %this1, i32 0, i32 2
  %call6 = call zeroext i1 @_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEeqERKS2_(%"class.draco::IndexType.9"* %corner_5, %"class.draco::IndexType.9"* nonnull align 4 dereferenceable(4) @_ZN5dracoL19kInvalidCornerIndexE)
  br i1 %call6, label %if.then7, label %if.else

if.then7:                                         ; preds = %if.then
  %start_corner_ = getelementptr inbounds %"class.draco::VertexRingIterator", %"class.draco::VertexRingIterator"* %this1, i32 0, i32 1
  %corner_8 = getelementptr inbounds %"class.draco::VertexRingIterator", %"class.draco::VertexRingIterator"* %this1, i32 0, i32 2
  %call9 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.9"* @_ZN5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEaSERKS2_(%"class.draco::IndexType.9"* %corner_8, %"class.draco::IndexType.9"* nonnull align 4 dereferenceable(4) %start_corner_)
  %left_traversal_10 = getelementptr inbounds %"class.draco::VertexRingIterator", %"class.draco::VertexRingIterator"* %this1, i32 0, i32 3
  store i8 0, i8* %left_traversal_10, align 4
  br label %if.end17

if.else:                                          ; preds = %if.then
  %corner_11 = getelementptr inbounds %"class.draco::VertexRingIterator", %"class.draco::VertexRingIterator"* %this1, i32 0, i32 2
  %start_corner_12 = getelementptr inbounds %"class.draco::VertexRingIterator", %"class.draco::VertexRingIterator"* %this1, i32 0, i32 1
  %call13 = call zeroext i1 @_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEeqERKS2_(%"class.draco::IndexType.9"* %corner_11, %"class.draco::IndexType.9"* nonnull align 4 dereferenceable(4) %start_corner_12)
  br i1 %call13, label %if.then14, label %if.end

if.then14:                                        ; preds = %if.else
  %corner_15 = getelementptr inbounds %"class.draco::VertexRingIterator", %"class.draco::VertexRingIterator"* %this1, i32 0, i32 2
  %call16 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.9"* @_ZN5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEaSERKS2_(%"class.draco::IndexType.9"* %corner_15, %"class.draco::IndexType.9"* nonnull align 4 dereferenceable(4) @_ZN5dracoL19kInvalidCornerIndexE)
  br label %if.end

if.end:                                           ; preds = %if.then14, %if.else
  br label %if.end17

if.end17:                                         ; preds = %if.end, %if.then7
  br label %if.end28

if.else18:                                        ; preds = %entry
  %corner_table_20 = getelementptr inbounds %"class.draco::VertexRingIterator", %"class.draco::VertexRingIterator"* %this1, i32 0, i32 0
  %5 = load %"class.draco::MeshAttributeCornerTable"*, %"class.draco::MeshAttributeCornerTable"** %corner_table_20, align 4
  %corner_22 = getelementptr inbounds %"class.draco::VertexRingIterator", %"class.draco::VertexRingIterator"* %this1, i32 0, i32 2
  %6 = bitcast %"class.draco::IndexType.9"* %agg.tmp21 to i8*
  %7 = bitcast %"class.draco::IndexType.9"* %corner_22 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 4, i1 false)
  %coerce.dive23 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %agg.tmp21, i32 0, i32 0
  %8 = load i32, i32* %coerce.dive23, align 4
  %call24 = call i32 @_ZNK5draco24MeshAttributeCornerTable10SwingRightENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::MeshAttributeCornerTable"* %5, i32 %8)
  %coerce.dive25 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %ref.tmp19, i32 0, i32 0
  store i32 %call24, i32* %coerce.dive25, align 4
  %corner_26 = getelementptr inbounds %"class.draco::VertexRingIterator", %"class.draco::VertexRingIterator"* %this1, i32 0, i32 2
  %call27 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.9"* @_ZN5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEaSERKS2_(%"class.draco::IndexType.9"* %corner_26, %"class.draco::IndexType.9"* nonnull align 4 dereferenceable(4) %ref.tmp19)
  br label %if.end28

if.end28:                                         ; preds = %if.else18, %if.end17
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_21CornerIndex_tag_type_EEENS1_IjNS_21VertexIndex_tag_type_EEEE4sizeEv(%"class.draco::IndexTypeVector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector"*, align 4
  store %"class.draco::IndexTypeVector"* %this, %"class.draco::IndexTypeVector"** %this.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector"*, %"class.draco::IndexTypeVector"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector", %"class.draco::IndexTypeVector"* %this1, i32 0, i32 0
  %call = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.1"* %vector_) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.1"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.1"*, align 4
  store %"class.std::__2::vector.1"* %this, %"class.std::__2::vector.1"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 1
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %2, i32 0, i32 0
  %3 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.draco::IndexType"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.draco::IndexType"* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_21VertexIndex_tag_type_EEENS1_IjNS_21CornerIndex_tag_type_EEEE4sizeEv(%"class.draco::IndexTypeVector.24"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector.24"*, align 4
  store %"class.draco::IndexTypeVector.24"* %this, %"class.draco::IndexTypeVector.24"** %this.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector.24"*, %"class.draco::IndexTypeVector.24"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector.24", %"class.draco::IndexTypeVector.24"* %this1, i32 0, i32 0
  %call = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.7"* %vector_) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.7"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.7"*, align 4
  store %"class.std::__2::vector.7"* %this, %"class.std::__2::vector.7"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.7"*, %"class.std::__2::vector.7"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.7"* %this1 to %"class.std::__2::__vector_base.8"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.8", %"class.std::__2::__vector_base.8"* %0, i32 0, i32 1
  %1 = load %"class.draco::IndexType.9"*, %"class.draco::IndexType.9"** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector.7"* %this1 to %"class.std::__2::__vector_base.8"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.8", %"class.std::__2::__vector_base.8"* %2, i32 0, i32 0
  %3 = load %"class.draco::IndexType.9"*, %"class.draco::IndexType.9"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.draco::IndexType.9"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.draco::IndexType.9"* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType.145"* @_ZN5draco9IndexTypeIjNS_19FaceIndex_tag_type_EEC2Ej(%"class.draco::IndexType.145"* returned %this, i32 %value) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.145"*, align 4
  %value.addr = alloca i32, align 4
  store %"class.draco::IndexType.145"* %this, %"class.draco::IndexType.145"** %this.addr, align 4
  store i32 %value, i32* %value.addr, align 4
  %this1 = load %"class.draco::IndexType.145"*, %"class.draco::IndexType.145"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.145", %"class.draco::IndexType.145"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value.addr, align 4
  store i32 %0, i32* %value_, align 4
  ret %"class.draco::IndexType.145"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.9"* @_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_21CornerIndex_tag_type_EEES3_EixERKS3_(%"class.draco::IndexTypeVector.23"* %this, %"class.draco::IndexType.9"* nonnull align 4 dereferenceable(4) %index) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector.23"*, align 4
  %index.addr = alloca %"class.draco::IndexType.9"*, align 4
  store %"class.draco::IndexTypeVector.23"* %this, %"class.draco::IndexTypeVector.23"** %this.addr, align 4
  store %"class.draco::IndexType.9"* %index, %"class.draco::IndexType.9"** %index.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector.23"*, %"class.draco::IndexTypeVector.23"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector.23", %"class.draco::IndexTypeVector.23"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexType.9"*, %"class.draco::IndexType.9"** %index.addr, align 4
  %call = call i32 @_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EE5valueEv(%"class.draco::IndexType.9"* %0)
  %call2 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.9"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEEixEm(%"class.std::__2::vector.7"* %vector_, i32 %call) #8
  ret %"class.draco::IndexType.9"* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.9"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEEixEm(%"class.std::__2::vector.7"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.7"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector.7"* %this, %"class.std::__2::vector.7"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.7"*, %"class.std::__2::vector.7"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.7"* %this1 to %"class.std::__2::__vector_base.8"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.8", %"class.std::__2::__vector_base.8"* %0, i32 0, i32 0
  %1 = load %"class.draco::IndexType.9"*, %"class.draco::IndexType.9"** %__begin_, align 4
  %2 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %1, i32 %2
  ret %"class.draco::IndexType.9"* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco11CornerTable15ConfidentVertexENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::CornerTable"* %this, i32 %corner.coerce) #0 comdat {
entry:
  %retval = alloca %"class.draco::IndexType", align 4
  %corner = alloca %"class.draco::IndexType.9", align 4
  %this.addr = alloca %"class.draco::CornerTable"*, align 4
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %corner, i32 0, i32 0
  store i32 %corner.coerce, i32* %coerce.dive, align 4
  store %"class.draco::CornerTable"* %this, %"class.draco::CornerTable"** %this.addr, align 4
  %this1 = load %"class.draco::CornerTable"*, %"class.draco::CornerTable"** %this.addr, align 4
  %corner_to_vertex_map_ = getelementptr inbounds %"class.draco::CornerTable", %"class.draco::CornerTable"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_21CornerIndex_tag_type_EEENS1_IjNS_21VertexIndex_tag_type_EEEEixERKS3_(%"class.draco::IndexTypeVector"* %corner_to_vertex_map_, %"class.draco::IndexType.9"* nonnull align 4 dereferenceable(4) %corner)
  %0 = bitcast %"class.draco::IndexType"* %retval to i8*
  %1 = bitcast %"class.draco::IndexType"* %call to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %0, i8* align 4 %1, i32 4, i1 false)
  %coerce.dive2 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %retval, i32 0, i32 0
  %2 = load i32, i32* %coerce.dive2, align 4
  ret i32 %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_21CornerIndex_tag_type_EEENS1_IjNS_21VertexIndex_tag_type_EEEEixERKS3_(%"class.draco::IndexTypeVector"* %this, %"class.draco::IndexType.9"* nonnull align 4 dereferenceable(4) %index) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector"*, align 4
  %index.addr = alloca %"class.draco::IndexType.9"*, align 4
  store %"class.draco::IndexTypeVector"* %this, %"class.draco::IndexTypeVector"** %this.addr, align 4
  store %"class.draco::IndexType.9"* %index, %"class.draco::IndexType.9"** %index.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector"*, %"class.draco::IndexTypeVector"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector", %"class.draco::IndexTypeVector"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexType.9"*, %"class.draco::IndexType.9"** %index.addr, align 4
  %call = call i32 @_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EE5valueEv(%"class.draco::IndexType.9"* %0)
  %call2 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEEixEm(%"class.std::__2::vector.1"* %vector_, i32 %call) #8
  ret %"class.draco::IndexType"* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEEixEm(%"class.std::__2::vector.1"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.1"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector.1"* %this, %"class.std::__2::vector.1"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 0
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  %2 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %1, i32 %2
  ret %"class.draco::IndexType"* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco11CornerTable10LocalIndexENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::CornerTable"* %this, i32 %corner.coerce) #0 comdat {
entry:
  %corner = alloca %"class.draco::IndexType.9", align 4
  %this.addr = alloca %"class.draco::CornerTable"*, align 4
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %corner, i32 0, i32 0
  store i32 %corner.coerce, i32* %coerce.dive, align 4
  store %"class.draco::CornerTable"* %this, %"class.draco::CornerTable"** %this.addr, align 4
  %this1 = load %"class.draco::CornerTable"*, %"class.draco::CornerTable"** %this.addr, align 4
  %call = call i32 @_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EE5valueEv(%"class.draco::IndexType.9"* %corner)
  %rem = urem i32 %call, 3
  ret i32 %rem
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEmiERKj(%"class.draco::IndexType.9"* %this, i32* nonnull align 4 dereferenceable(4) %val) #0 comdat {
entry:
  %retval = alloca %"class.draco::IndexType.9", align 4
  %this.addr = alloca %"class.draco::IndexType.9"*, align 4
  %val.addr = alloca i32*, align 4
  store %"class.draco::IndexType.9"* %this, %"class.draco::IndexType.9"** %this.addr, align 4
  store i32* %val, i32** %val.addr, align 4
  %this1 = load %"class.draco::IndexType.9"*, %"class.draco::IndexType.9"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value_, align 4
  %1 = load i32*, i32** %val.addr, align 4
  %2 = load i32, i32* %1, align 4
  %sub = sub i32 %0, %2
  %call = call %"class.draco::IndexType.9"* @_ZN5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEC2Ej(%"class.draco::IndexType.9"* %retval, i32 %sub)
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %retval, i32 0, i32 0
  %3 = load i32, i32* %coerce.dive, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEplERKj(%"class.draco::IndexType.9"* %this, i32* nonnull align 4 dereferenceable(4) %val) #0 comdat {
entry:
  %retval = alloca %"class.draco::IndexType.9", align 4
  %this.addr = alloca %"class.draco::IndexType.9"*, align 4
  %val.addr = alloca i32*, align 4
  store %"class.draco::IndexType.9"* %this, %"class.draco::IndexType.9"** %this.addr, align 4
  store i32* %val, i32** %val.addr, align 4
  %this1 = load %"class.draco::IndexType.9"*, %"class.draco::IndexType.9"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value_, align 4
  %1 = load i32*, i32** %val.addr, align 4
  %2 = load i32, i32* %1, align 4
  %add = add i32 %0, %2
  %call = call %"class.draco::IndexType.9"* @_ZN5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEC2Ej(%"class.draco::IndexType.9"* %retval, i32 %add)
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %retval, i32 0, i32 0
  %3 = load i32, i32* %coerce.dive, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"struct.std::__2::array"* @_ZNK5draco4Mesh4faceENS_9IndexTypeIjNS_19FaceIndex_tag_type_EEE(%"class.draco::Mesh"* %this, i32 %face_id.coerce) #0 comdat {
entry:
  %face_id = alloca %"class.draco::IndexType.145", align 4
  %this.addr = alloca %"class.draco::Mesh"*, align 4
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType.145", %"class.draco::IndexType.145"* %face_id, i32 0, i32 0
  store i32 %face_id.coerce, i32* %coerce.dive, align 4
  store %"class.draco::Mesh"* %this, %"class.draco::Mesh"** %this.addr, align 4
  %this1 = load %"class.draco::Mesh"*, %"class.draco::Mesh"** %this.addr, align 4
  %faces_ = getelementptr inbounds %"class.draco::Mesh", %"class.draco::Mesh"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::array"* @_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEENSt3__25arrayINS1_IjNS_20PointIndex_tag_type_EEELm3EEEEixERKS3_(%"class.draco::IndexTypeVector.136"* %faces_, %"class.draco::IndexType.145"* nonnull align 4 dereferenceable(4) %face_id)
  ret %"struct.std::__2::array"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.139"* @_ZNKSt3__25arrayIN5draco9IndexTypeIjNS1_20PointIndex_tag_type_EEELm3EEixEm(%"struct.std::__2::array"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::array"*, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::array"* %this, %"struct.std::__2::array"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %this.addr, align 4
  %__elems_ = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %this1, i32 0, i32 0
  %0 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %"class.draco::IndexType.139"], [3 x %"class.draco::IndexType.139"]* %__elems_, i32 0, i32 %0
  ret %"class.draco::IndexType.139"* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"struct.std::__2::array"* @_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_19FaceIndex_tag_type_EEENSt3__25arrayINS1_IjNS_20PointIndex_tag_type_EEELm3EEEEixERKS3_(%"class.draco::IndexTypeVector.136"* %this, %"class.draco::IndexType.145"* nonnull align 4 dereferenceable(4) %index) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector.136"*, align 4
  %index.addr = alloca %"class.draco::IndexType.145"*, align 4
  store %"class.draco::IndexTypeVector.136"* %this, %"class.draco::IndexTypeVector.136"** %this.addr, align 4
  store %"class.draco::IndexType.145"* %index, %"class.draco::IndexType.145"** %index.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector.136"*, %"class.draco::IndexTypeVector.136"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector.136", %"class.draco::IndexTypeVector.136"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexType.145"*, %"class.draco::IndexType.145"** %index.addr, align 4
  %call = call i32 @_ZNK5draco9IndexTypeIjNS_19FaceIndex_tag_type_EE5valueEv(%"class.draco::IndexType.145"* %0)
  %call2 = call nonnull align 4 dereferenceable(12) %"struct.std::__2::array"* @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEEixEm(%"class.std::__2::vector.137"* %vector_, i32 %call) #8
  ret %"struct.std::__2::array"* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"struct.std::__2::array"* @_ZNKSt3__26vectorINS_5arrayIN5draco9IndexTypeIjNS2_20PointIndex_tag_type_EEELm3EEENS_9allocatorIS6_EEEixEm(%"class.std::__2::vector.137"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.137"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector.137"* %this, %"class.std::__2::vector.137"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.137"*, %"class.std::__2::vector.137"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.137"* %this1 to %"class.std::__2::__vector_base.138"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.138", %"class.std::__2::__vector_base.138"* %0, i32 0, i32 0
  %1 = load %"struct.std::__2::array"*, %"struct.std::__2::array"** %__begin_, align 4
  %2 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds %"struct.std::__2::array", %"struct.std::__2::array"* %1, i32 %2
  ret %"struct.std::__2::array"* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco9IndexTypeIjNS_19FaceIndex_tag_type_EE5valueEv(%"class.draco::IndexType.145"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.145"*, align 4
  store %"class.draco::IndexType.145"* %this, %"class.draco::IndexType.145"** %this.addr, align 4
  %this1 = load %"class.draco::IndexType.145"*, %"class.draco::IndexType.145"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.145", %"class.draco::IndexType.145"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value_, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco9IndexTypeIjNS_20PointIndex_tag_type_EE5valueEv(%"class.draco::IndexType.139"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.139"*, align 4
  store %"class.draco::IndexType.139"* %this, %"class.draco::IndexType.139"** %this.addr, align 4
  %this1 = load %"class.draco::IndexType.139"*, %"class.draco::IndexType.139"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.139", %"class.draco::IndexType.139"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value_, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType.17"* @_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEC2Ej(%"class.draco::IndexType.17"* returned %this, i32 %value) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.17"*, align 4
  %value.addr = alloca i32, align 4
  store %"class.draco::IndexType.17"* %this, %"class.draco::IndexType.17"** %this.addr, align 4
  store i32 %value, i32* %value.addr, align 4
  %this1 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.17", %"class.draco::IndexType.17"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value.addr, align 4
  store i32 %0, i32* %value_, align 4
  ret %"class.draco::IndexType.17"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.17"* @_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_20PointIndex_tag_type_EEENS1_IjNS_29AttributeValueIndex_tag_type_EEEEixERKS3_(%"class.draco::IndexTypeVector.116"* %this, %"class.draco::IndexType.139"* nonnull align 4 dereferenceable(4) %index) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector.116"*, align 4
  %index.addr = alloca %"class.draco::IndexType.139"*, align 4
  store %"class.draco::IndexTypeVector.116"* %this, %"class.draco::IndexTypeVector.116"** %this.addr, align 4
  store %"class.draco::IndexType.139"* %index, %"class.draco::IndexType.139"** %index.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector.116"*, %"class.draco::IndexTypeVector.116"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector.116", %"class.draco::IndexTypeVector.116"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexType.139"*, %"class.draco::IndexType.139"** %index.addr, align 4
  %call = call i32 @_ZNK5draco9IndexTypeIjNS_20PointIndex_tag_type_EE5valueEv(%"class.draco::IndexType.139"* %0)
  %call2 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.17"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEEixEm(%"class.std::__2::vector.15"* %vector_, i32 %call) #8
  ret %"class.draco::IndexType.17"* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.17"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEEixEm(%"class.std::__2::vector.15"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.15"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector.15"* %this, %"class.std::__2::vector.15"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.15"*, %"class.std::__2::vector.15"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.15"* %this1 to %"class.std::__2::__vector_base.16"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.16", %"class.std::__2::__vector_base.16"* %0, i32 0, i32 0
  %1 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %__begin_, align 4
  %2 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds %"class.draco::IndexType.17", %"class.draco::IndexType.17"* %1, i32 %2
  ret %"class.draco::IndexType.17"* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexTypeVector.26"* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_21VertexIndex_tag_type_EEEaEC2Ev(%"class.draco::IndexTypeVector.26"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector.26"*, align 4
  store %"class.draco::IndexTypeVector.26"* %this, %"class.draco::IndexTypeVector.26"** %this.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector.26"*, %"class.draco::IndexTypeVector.26"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector.26", %"class.draco::IndexTypeVector.26"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::vector.27"* @_ZNSt3__26vectorIaNS_9allocatorIaEEEC2Ev(%"class.std::__2::vector.27"* %vector_) #8
  ret %"class.draco::IndexTypeVector.26"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexTypeVector.34"* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_21VertexIndex_tag_type_EEEiEC2Ev(%"class.draco::IndexTypeVector.34"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector.34"*, align 4
  store %"class.draco::IndexTypeVector.34"* %this, %"class.draco::IndexTypeVector.34"** %this.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector.34"*, %"class.draco::IndexTypeVector.34"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector.34", %"class.draco::IndexTypeVector.34"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::vector.35"* @_ZNSt3__26vectorIiNS_9allocatorIiEEEC2Ev(%"class.std::__2::vector.35"* %vector_) #8
  ret %"class.draco::IndexTypeVector.34"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector.27"* @_ZNSt3__26vectorIaNS_9allocatorIaEEEC2Ev(%"class.std::__2::vector.27"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.27"*, align 4
  store %"class.std::__2::vector.27"* %this, %"class.std::__2::vector.27"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.27"*, %"class.std::__2::vector.27"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.27"* %this1 to %"class.std::__2::__vector_base.28"*
  %call = call %"class.std::__2::__vector_base.28"* @_ZNSt3__213__vector_baseIaNS_9allocatorIaEEEC2Ev(%"class.std::__2::__vector_base.28"* %0) #8
  ret %"class.std::__2::vector.27"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base.28"* @_ZNSt3__213__vector_baseIaNS_9allocatorIaEEEC2Ev(%"class.std::__2::__vector_base.28"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.28"*, align 4
  %ref.tmp = alloca i8*, align 4
  %ref.tmp2 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__vector_base.28"* %this, %"class.std::__2::__vector_base.28"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.28"*, %"class.std::__2::__vector_base.28"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__vector_base.28"* %this1 to %"class.std::__2::__vector_base_common"*
  %call = call %"class.std::__2::__vector_base_common"* @_ZNSt3__220__vector_base_commonILb1EEC2Ev(%"class.std::__2::__vector_base_common"* %0)
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.28", %"class.std::__2::__vector_base.28"* %this1, i32 0, i32 0
  store i8* null, i8** %__begin_, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.28", %"class.std::__2::__vector_base.28"* %this1, i32 0, i32 1
  store i8* null, i8** %__end_, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.28", %"class.std::__2::__vector_base.28"* %this1, i32 0, i32 2
  store i8* null, i8** %ref.tmp, align 4
  %call3 = call %"class.std::__2::__compressed_pair.29"* @_ZNSt3__217__compressed_pairIPaNS_9allocatorIaEEEC2IDnNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.29"* %__end_cap_, i8** nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  ret %"class.std::__2::__vector_base.28"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base_common"* @_ZNSt3__220__vector_base_commonILb1EEC2Ev(%"class.std::__2::__vector_base_common"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base_common"*, align 4
  store %"class.std::__2::__vector_base_common"* %this, %"class.std::__2::__vector_base_common"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base_common"*, %"class.std::__2::__vector_base_common"** %this.addr, align 4
  ret %"class.std::__2::__vector_base_common"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.29"* @_ZNSt3__217__compressed_pairIPaNS_9allocatorIaEEEC2IDnNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.29"* returned %this, i8** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.29"*, align 4
  %__t1.addr = alloca i8**, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.29"* %this, %"class.std::__2::__compressed_pair.29"** %this.addr, align 4
  store i8** %__t1, i8*** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.29"*, %"class.std::__2::__compressed_pair.29"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.29"* %this1 to %"struct.std::__2::__compressed_pair_elem.30"*
  %1 = load i8**, i8*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %1) #8
  %call2 = call %"struct.std::__2::__compressed_pair_elem.30"* @_ZNSt3__222__compressed_pair_elemIPaLi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.30"* %0, i8** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.29"* %this1 to %"struct.std::__2::__compressed_pair_elem.31"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #8
  %call4 = call %"struct.std::__2::__compressed_pair_elem.31"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIaEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.31"* %2)
  ret %"class.std::__2::__compressed_pair.29"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca i8**, align 4
  store i8** %__t, i8*** %__t.addr, align 4
  %0 = load i8**, i8*** %__t.addr, align 4
  ret i8** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.30"* @_ZNSt3__222__compressed_pair_elemIPaLi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.30"* returned %this, i8** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.30"*, align 4
  %__u.addr = alloca i8**, align 4
  store %"struct.std::__2::__compressed_pair_elem.30"* %this, %"struct.std::__2::__compressed_pair_elem.30"** %this.addr, align 4
  store i8** %__u, i8*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.30"*, %"struct.std::__2::__compressed_pair_elem.30"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.30", %"struct.std::__2::__compressed_pair_elem.30"* %this1, i32 0, i32 0
  %0 = load i8**, i8*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %0) #8
  store i8* null, i8** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.30"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  store %"struct.std::__2::__default_init_tag"* %__t, %"struct.std::__2::__default_init_tag"** %__t.addr, align 4
  %0 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t.addr, align 4
  ret %"struct.std::__2::__default_init_tag"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.31"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIaEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.31"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.31"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.31"* %this, %"struct.std::__2::__compressed_pair_elem.31"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.31"*, %"struct.std::__2::__compressed_pair_elem.31"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.31"* %this1 to %"class.std::__2::allocator.32"*
  %call = call %"class.std::__2::allocator.32"* @_ZNSt3__29allocatorIaEC2Ev(%"class.std::__2::allocator.32"* %1) #8
  ret %"struct.std::__2::__compressed_pair_elem.31"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::allocator.32"* @_ZNSt3__29allocatorIaEC2Ev(%"class.std::__2::allocator.32"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.32"*, align 4
  store %"class.std::__2::allocator.32"* %this, %"class.std::__2::allocator.32"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.32"*, %"class.std::__2::allocator.32"** %this.addr, align 4
  ret %"class.std::__2::allocator.32"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector.35"* @_ZNSt3__26vectorIiNS_9allocatorIiEEEC2Ev(%"class.std::__2::vector.35"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.35"*, align 4
  store %"class.std::__2::vector.35"* %this, %"class.std::__2::vector.35"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.35"*, %"class.std::__2::vector.35"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.35"* %this1 to %"class.std::__2::__vector_base.36"*
  %call = call %"class.std::__2::__vector_base.36"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEEC2Ev(%"class.std::__2::__vector_base.36"* %0) #8
  ret %"class.std::__2::vector.35"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base.36"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEEC2Ev(%"class.std::__2::__vector_base.36"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.36"*, align 4
  %ref.tmp = alloca i8*, align 4
  %ref.tmp2 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__vector_base.36"* %this, %"class.std::__2::__vector_base.36"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.36"*, %"class.std::__2::__vector_base.36"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__vector_base.36"* %this1 to %"class.std::__2::__vector_base_common"*
  %call = call %"class.std::__2::__vector_base_common"* @_ZNSt3__220__vector_base_commonILb1EEC2Ev(%"class.std::__2::__vector_base_common"* %0)
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.36", %"class.std::__2::__vector_base.36"* %this1, i32 0, i32 0
  store i32* null, i32** %__begin_, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.36", %"class.std::__2::__vector_base.36"* %this1, i32 0, i32 1
  store i32* null, i32** %__end_, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.36", %"class.std::__2::__vector_base.36"* %this1, i32 0, i32 2
  store i8* null, i8** %ref.tmp, align 4
  %call3 = call %"class.std::__2::__compressed_pair.37"* @_ZNSt3__217__compressed_pairIPiNS_9allocatorIiEEEC2IDnNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.37"* %__end_cap_, i8** nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  ret %"class.std::__2::__vector_base.36"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.37"* @_ZNSt3__217__compressed_pairIPiNS_9allocatorIiEEEC2IDnNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.37"* returned %this, i8** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.37"*, align 4
  %__t1.addr = alloca i8**, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.37"* %this, %"class.std::__2::__compressed_pair.37"** %this.addr, align 4
  store i8** %__t1, i8*** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.37"*, %"class.std::__2::__compressed_pair.37"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.37"* %this1 to %"struct.std::__2::__compressed_pair_elem.38"*
  %1 = load i8**, i8*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %1) #8
  %call2 = call %"struct.std::__2::__compressed_pair_elem.38"* @_ZNSt3__222__compressed_pair_elemIPiLi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.38"* %0, i8** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.37"* %this1 to %"struct.std::__2::__compressed_pair_elem.39"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #8
  %call4 = call %"struct.std::__2::__compressed_pair_elem.39"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIiEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.39"* %2)
  ret %"class.std::__2::__compressed_pair.37"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.38"* @_ZNSt3__222__compressed_pair_elemIPiLi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.38"* returned %this, i8** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.38"*, align 4
  %__u.addr = alloca i8**, align 4
  store %"struct.std::__2::__compressed_pair_elem.38"* %this, %"struct.std::__2::__compressed_pair_elem.38"** %this.addr, align 4
  store i8** %__u, i8*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.38"*, %"struct.std::__2::__compressed_pair_elem.38"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.38", %"struct.std::__2::__compressed_pair_elem.38"* %this1, i32 0, i32 0
  %0 = load i8**, i8*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %0) #8
  store i32* null, i32** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.38"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.39"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIiEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.39"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.39"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.39"* %this, %"struct.std::__2::__compressed_pair_elem.39"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.39"*, %"struct.std::__2::__compressed_pair_elem.39"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.39"* %this1 to %"class.std::__2::allocator.40"*
  %call = call %"class.std::__2::allocator.40"* @_ZNSt3__29allocatorIiEC2Ev(%"class.std::__2::allocator.40"* %1) #8
  ret %"struct.std::__2::__compressed_pair_elem.39"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::allocator.40"* @_ZNSt3__29allocatorIiEC2Ev(%"class.std::__2::allocator.40"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.40"*, align 4
  store %"class.std::__2::allocator.40"* %this, %"class.std::__2::allocator.40"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.40"*, %"class.std::__2::allocator.40"** %this.addr, align 4
  ret %"class.std::__2::allocator.40"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair"* @_ZNSt3__217__compressed_pairImNS_9allocatorImEEEC2IiNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair"* returned %this, i32* nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  %__t1.addr = alloca i32*, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  store i32* %__t1, i32** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %1 = load i32*, i32** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__27forwardIiEEOT_RNS_16remove_referenceIS1_E4typeE(i32* nonnull align 4 dereferenceable(4) %1) #8
  %call2 = call %"struct.std::__2::__compressed_pair_elem"* @_ZNSt3__222__compressed_pair_elemImLi0ELb0EEC2IivEEOT_(%"struct.std::__2::__compressed_pair_elem"* %0, i32* nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem.0"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #8
  %call4 = call %"struct.std::__2::__compressed_pair_elem.0"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorImEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.0"* %2)
  ret %"class.std::__2::__compressed_pair"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__27forwardIiEEOT_RNS_16remove_referenceIS1_E4typeE(i32* nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca i32*, align 4
  store i32* %__t, i32** %__t.addr, align 4
  %0 = load i32*, i32** %__t.addr, align 4
  ret i32* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem"* @_ZNSt3__222__compressed_pair_elemImLi0ELb0EEC2IivEEOT_(%"struct.std::__2::__compressed_pair_elem"* returned %this, i32* nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem"*, align 4
  %__u.addr = alloca i32*, align 4
  store %"struct.std::__2::__compressed_pair_elem"* %this, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  store i32* %__u, i32** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem"*, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__27forwardIiEEOT_RNS_16remove_referenceIS1_E4typeE(i32* nonnull align 4 dereferenceable(4) %0) #8
  %1 = load i32, i32* %call, align 4
  store i32 %1, i32* %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.0"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorImEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.0"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.0"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.0"* %this, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.0"*, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.0"* %this1 to %"class.std::__2::allocator"*
  %call = call %"class.std::__2::allocator"* @_ZNSt3__29allocatorImEC2Ev(%"class.std::__2::allocator"* %1) #8
  ret %"struct.std::__2::__compressed_pair_elem.0"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::allocator"* @_ZNSt3__29allocatorImEC2Ev(%"class.std::__2::allocator"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  ret %"class.std::__2::allocator"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEEC2Ev(%"class.std::__2::__vector_base"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  %ref.tmp = alloca i8*, align 4
  %ref.tmp2 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__vector_base"* %this1 to %"class.std::__2::__vector_base_common"*
  %call = call %"class.std::__2::__vector_base_common"* @_ZNSt3__220__vector_base_commonILb1EEC2Ev(%"class.std::__2::__vector_base_common"* %0)
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  store %"class.draco::IndexType"* null, %"class.draco::IndexType"** %__begin_, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 1
  store %"class.draco::IndexType"* null, %"class.draco::IndexType"** %__end_, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  store i8* null, i8** %ref.tmp, align 4
  %call3 = call %"class.std::__2::__compressed_pair.2"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEEC2IDnNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.2"* %__end_cap_, i8** nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  ret %"class.std::__2::__vector_base"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.2"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEEC2IDnNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.2"* returned %this, i8** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.2"*, align 4
  %__t1.addr = alloca i8**, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.2"* %this, %"class.std::__2::__compressed_pair.2"** %this.addr, align 4
  store i8** %__t1, i8*** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.2"*, %"class.std::__2::__compressed_pair.2"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.2"* %this1 to %"struct.std::__2::__compressed_pair_elem.3"*
  %1 = load i8**, i8*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %1) #8
  %call2 = call %"struct.std::__2::__compressed_pair_elem.3"* @_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEELi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.3"* %0, i8** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.2"* %this1 to %"struct.std::__2::__compressed_pair_elem.4"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #8
  %call4 = call %"struct.std::__2::__compressed_pair_elem.4"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.4"* %2)
  ret %"class.std::__2::__compressed_pair.2"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.3"* @_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEELi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.3"* returned %this, i8** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.3"*, align 4
  %__u.addr = alloca i8**, align 4
  store %"struct.std::__2::__compressed_pair_elem.3"* %this, %"struct.std::__2::__compressed_pair_elem.3"** %this.addr, align 4
  store i8** %__u, i8*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.3"*, %"struct.std::__2::__compressed_pair_elem.3"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.3", %"struct.std::__2::__compressed_pair_elem.3"* %this1, i32 0, i32 0
  %0 = load i8**, i8*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %0) #8
  store %"class.draco::IndexType"* null, %"class.draco::IndexType"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.3"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.4"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.4"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.4"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.4"* %this, %"struct.std::__2::__compressed_pair_elem.4"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.4"*, %"struct.std::__2::__compressed_pair_elem.4"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.4"* %this1 to %"class.std::__2::allocator.5"*
  %call = call %"class.std::__2::allocator.5"* @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEEEC2Ev(%"class.std::__2::allocator.5"* %1) #8
  ret %"struct.std::__2::__compressed_pair_elem.4"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::allocator.5"* @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEEEC2Ev(%"class.std::__2::allocator.5"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.5"*, align 4
  store %"class.std::__2::allocator.5"* %this, %"class.std::__2::allocator.5"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.5"*, %"class.std::__2::allocator.5"** %this.addr, align 4
  ret %"class.std::__2::allocator.5"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base.8"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEEC2Ev(%"class.std::__2::__vector_base.8"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.8"*, align 4
  %ref.tmp = alloca i8*, align 4
  %ref.tmp2 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__vector_base.8"* %this, %"class.std::__2::__vector_base.8"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.8"*, %"class.std::__2::__vector_base.8"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__vector_base.8"* %this1 to %"class.std::__2::__vector_base_common"*
  %call = call %"class.std::__2::__vector_base_common"* @_ZNSt3__220__vector_base_commonILb1EEC2Ev(%"class.std::__2::__vector_base_common"* %0)
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.8", %"class.std::__2::__vector_base.8"* %this1, i32 0, i32 0
  store %"class.draco::IndexType.9"* null, %"class.draco::IndexType.9"** %__begin_, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.8", %"class.std::__2::__vector_base.8"* %this1, i32 0, i32 1
  store %"class.draco::IndexType.9"* null, %"class.draco::IndexType.9"** %__end_, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.8", %"class.std::__2::__vector_base.8"* %this1, i32 0, i32 2
  store i8* null, i8** %ref.tmp, align 4
  %call3 = call %"class.std::__2::__compressed_pair.10"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEEC2IDnNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.10"* %__end_cap_, i8** nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  ret %"class.std::__2::__vector_base.8"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.10"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEEC2IDnNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.10"* returned %this, i8** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.10"*, align 4
  %__t1.addr = alloca i8**, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.10"* %this, %"class.std::__2::__compressed_pair.10"** %this.addr, align 4
  store i8** %__t1, i8*** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.10"*, %"class.std::__2::__compressed_pair.10"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.10"* %this1 to %"struct.std::__2::__compressed_pair_elem.11"*
  %1 = load i8**, i8*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %1) #8
  %call2 = call %"struct.std::__2::__compressed_pair_elem.11"* @_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEELi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.11"* %0, i8** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.10"* %this1 to %"struct.std::__2::__compressed_pair_elem.12"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #8
  %call4 = call %"struct.std::__2::__compressed_pair_elem.12"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_21CornerIndex_tag_type_EEEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.12"* %2)
  ret %"class.std::__2::__compressed_pair.10"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.11"* @_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEELi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.11"* returned %this, i8** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.11"*, align 4
  %__u.addr = alloca i8**, align 4
  store %"struct.std::__2::__compressed_pair_elem.11"* %this, %"struct.std::__2::__compressed_pair_elem.11"** %this.addr, align 4
  store i8** %__u, i8*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.11"*, %"struct.std::__2::__compressed_pair_elem.11"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.11", %"struct.std::__2::__compressed_pair_elem.11"* %this1, i32 0, i32 0
  %0 = load i8**, i8*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %0) #8
  store %"class.draco::IndexType.9"* null, %"class.draco::IndexType.9"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.11"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.12"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_21CornerIndex_tag_type_EEEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.12"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.12"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.12"* %this, %"struct.std::__2::__compressed_pair_elem.12"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.12"*, %"struct.std::__2::__compressed_pair_elem.12"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.12"* %this1 to %"class.std::__2::allocator.13"*
  %call = call %"class.std::__2::allocator.13"* @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEEEC2Ev(%"class.std::__2::allocator.13"* %1) #8
  ret %"struct.std::__2::__compressed_pair_elem.12"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::allocator.13"* @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEEEC2Ev(%"class.std::__2::allocator.13"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.13"*, align 4
  store %"class.std::__2::allocator.13"* %this, %"class.std::__2::allocator.13"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.13"*, %"class.std::__2::allocator.13"** %this.addr, align 4
  ret %"class.std::__2::allocator.13"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base.16"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEEC2Ev(%"class.std::__2::__vector_base.16"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.16"*, align 4
  %ref.tmp = alloca i8*, align 4
  %ref.tmp2 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__vector_base.16"* %this, %"class.std::__2::__vector_base.16"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.16"*, %"class.std::__2::__vector_base.16"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__vector_base.16"* %this1 to %"class.std::__2::__vector_base_common"*
  %call = call %"class.std::__2::__vector_base_common"* @_ZNSt3__220__vector_base_commonILb1EEC2Ev(%"class.std::__2::__vector_base_common"* %0)
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.16", %"class.std::__2::__vector_base.16"* %this1, i32 0, i32 0
  store %"class.draco::IndexType.17"* null, %"class.draco::IndexType.17"** %__begin_, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.16", %"class.std::__2::__vector_base.16"* %this1, i32 0, i32 1
  store %"class.draco::IndexType.17"* null, %"class.draco::IndexType.17"** %__end_, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.16", %"class.std::__2::__vector_base.16"* %this1, i32 0, i32 2
  store i8* null, i8** %ref.tmp, align 4
  %call3 = call %"class.std::__2::__compressed_pair.18"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEEC2IDnNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.18"* %__end_cap_, i8** nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  ret %"class.std::__2::__vector_base.16"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.18"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEEC2IDnNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.18"* returned %this, i8** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.18"*, align 4
  %__t1.addr = alloca i8**, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.18"* %this, %"class.std::__2::__compressed_pair.18"** %this.addr, align 4
  store i8** %__t1, i8*** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.18"*, %"class.std::__2::__compressed_pair.18"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.18"* %this1 to %"struct.std::__2::__compressed_pair_elem.19"*
  %1 = load i8**, i8*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %1) #8
  %call2 = call %"struct.std::__2::__compressed_pair_elem.19"* @_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.19"* %0, i8** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.18"* %this1 to %"struct.std::__2::__compressed_pair_elem.20"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #8
  %call4 = call %"struct.std::__2::__compressed_pair_elem.20"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.20"* %2)
  ret %"class.std::__2::__compressed_pair.18"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.19"* @_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.19"* returned %this, i8** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.19"*, align 4
  %__u.addr = alloca i8**, align 4
  store %"struct.std::__2::__compressed_pair_elem.19"* %this, %"struct.std::__2::__compressed_pair_elem.19"** %this.addr, align 4
  store i8** %__u, i8*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.19"*, %"struct.std::__2::__compressed_pair_elem.19"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.19", %"struct.std::__2::__compressed_pair_elem.19"* %this1, i32 0, i32 0
  %0 = load i8**, i8*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %0) #8
  store %"class.draco::IndexType.17"* null, %"class.draco::IndexType.17"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.19"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.20"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.20"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.20"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.20"* %this, %"struct.std::__2::__compressed_pair_elem.20"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.20"*, %"struct.std::__2::__compressed_pair_elem.20"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.20"* %this1 to %"class.std::__2::allocator.21"*
  %call = call %"class.std::__2::allocator.21"* @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEC2Ev(%"class.std::__2::allocator.21"* %1) #8
  ret %"struct.std::__2::__compressed_pair_elem.20"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::allocator.21"* @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEC2Ev(%"class.std::__2::allocator.21"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.21"*, align 4
  store %"class.std::__2::allocator.21"* %this, %"class.std::__2::allocator.21"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.21"*, %"class.std::__2::allocator.21"** %this.addr, align 4
  ret %"class.std::__2::allocator.21"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_21VertexIndex_tag_type_EEEiE5clearEv(%"class.draco::IndexTypeVector.34"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector.34"*, align 4
  store %"class.draco::IndexTypeVector.34"* %this, %"class.draco::IndexTypeVector.34"** %this.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector.34"*, %"class.draco::IndexTypeVector.34"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector.34", %"class.draco::IndexTypeVector.34"* %this1, i32 0, i32 0
  call void @_ZNSt3__26vectorIiNS_9allocatorIiEEE5clearEv(%"class.std::__2::vector.35"* %vector_) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_21VertexIndex_tag_type_EEEiE4swapERS4_(%"class.draco::IndexTypeVector.34"* %this, %"class.draco::IndexTypeVector.34"* nonnull align 4 dereferenceable(12) %arg) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector.34"*, align 4
  %arg.addr = alloca %"class.draco::IndexTypeVector.34"*, align 4
  store %"class.draco::IndexTypeVector.34"* %this, %"class.draco::IndexTypeVector.34"** %this.addr, align 4
  store %"class.draco::IndexTypeVector.34"* %arg, %"class.draco::IndexTypeVector.34"** %arg.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector.34"*, %"class.draco::IndexTypeVector.34"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector.34", %"class.draco::IndexTypeVector.34"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexTypeVector.34"*, %"class.draco::IndexTypeVector.34"** %arg.addr, align 4
  %vector_2 = getelementptr inbounds %"class.draco::IndexTypeVector.34", %"class.draco::IndexTypeVector.34"* %0, i32 0, i32 0
  call void @_ZNSt3__26vectorIiNS_9allocatorIiEEE4swapERS3_(%"class.std::__2::vector.35"* %vector_, %"class.std::__2::vector.35"* nonnull align 4 dereferenceable(12) %vector_2) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexTypeVector.34"* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_21VertexIndex_tag_type_EEEiED2Ev(%"class.draco::IndexTypeVector.34"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector.34"*, align 4
  store %"class.draco::IndexTypeVector.34"* %this, %"class.draco::IndexTypeVector.34"** %this.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector.34"*, %"class.draco::IndexTypeVector.34"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector.34", %"class.draco::IndexTypeVector.34"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::vector.35"* @_ZNSt3__26vectorIiNS_9allocatorIiEEED2Ev(%"class.std::__2::vector.35"* %vector_) #8
  ret %"class.draco::IndexTypeVector.34"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIiNS_9allocatorIiEEE5clearEv(%"class.std::__2::vector.35"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.35"*, align 4
  %__old_size = alloca i32, align 4
  store %"class.std::__2::vector.35"* %this, %"class.std::__2::vector.35"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.35"*, %"class.std::__2::vector.35"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4sizeEv(%"class.std::__2::vector.35"* %this1) #8
  store i32 %call, i32* %__old_size, align 4
  %0 = bitcast %"class.std::__2::vector.35"* %this1 to %"class.std::__2::__vector_base.36"*
  call void @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE5clearEv(%"class.std::__2::__vector_base.36"* %0) #8
  %1 = load i32, i32* %__old_size, align 4
  call void @_ZNKSt3__26vectorIiNS_9allocatorIiEEE17__annotate_shrinkEm(%"class.std::__2::vector.35"* %this1, i32 %1) #8
  call void @_ZNSt3__26vectorIiNS_9allocatorIiEEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector.35"* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4sizeEv(%"class.std::__2::vector.35"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.35"*, align 4
  store %"class.std::__2::vector.35"* %this, %"class.std::__2::vector.35"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.35"*, %"class.std::__2::vector.35"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.35"* %this1 to %"class.std::__2::__vector_base.36"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.36", %"class.std::__2::__vector_base.36"* %0, i32 0, i32 1
  %1 = load i32*, i32** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector.35"* %this1 to %"class.std::__2::__vector_base.36"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.36", %"class.std::__2::__vector_base.36"* %2, i32 0, i32 0
  %3 = load i32*, i32** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint i32* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint i32* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE5clearEv(%"class.std::__2::__vector_base.36"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.36"*, align 4
  store %"class.std::__2::__vector_base.36"* %this, %"class.std::__2::__vector_base.36"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.36"*, %"class.std::__2::__vector_base.36"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.36", %"class.std::__2::__vector_base.36"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__begin_, align 4
  call void @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE17__destruct_at_endEPi(%"class.std::__2::__vector_base.36"* %this1, i32* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIiNS_9allocatorIiEEE17__annotate_shrinkEm(%"class.std::__2::vector.35"* %this, i32 %__old_size) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.35"*, align 4
  %__old_size.addr = alloca i32, align 4
  store %"class.std::__2::vector.35"* %this, %"class.std::__2::vector.35"** %this.addr, align 4
  store i32 %__old_size, i32* %__old_size.addr, align 4
  %this1 = load %"class.std::__2::vector.35"*, %"class.std::__2::vector.35"** %this.addr, align 4
  %call = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector.35"* %this1) #8
  %0 = bitcast i32* %call to i8*
  %call2 = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector.35"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::vector.35"* %this1) #8
  %add.ptr = getelementptr inbounds i32, i32* %call2, i32 %call3
  %1 = bitcast i32* %add.ptr to i8*
  %call4 = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector.35"* %this1) #8
  %2 = load i32, i32* %__old_size.addr, align 4
  %add.ptr5 = getelementptr inbounds i32, i32* %call4, i32 %2
  %3 = bitcast i32* %add.ptr5 to i8*
  %call6 = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector.35"* %this1) #8
  %call7 = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4sizeEv(%"class.std::__2::vector.35"* %this1) #8
  %add.ptr8 = getelementptr inbounds i32, i32* %call6, i32 %call7
  %4 = bitcast i32* %add.ptr8 to i8*
  call void @_ZNKSt3__26vectorIiNS_9allocatorIiEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector.35"* %this1, i8* %0, i8* %1, i8* %3, i8* %4) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIiNS_9allocatorIiEEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector.35"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.35"*, align 4
  store %"class.std::__2::vector.35"* %this, %"class.std::__2::vector.35"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.35"*, %"class.std::__2::vector.35"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE17__destruct_at_endEPi(%"class.std::__2::__vector_base.36"* %this, i32* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.36"*, align 4
  %__new_last.addr = alloca i32*, align 4
  %__soon_to_be_end = alloca i32*, align 4
  store %"class.std::__2::__vector_base.36"* %this, %"class.std::__2::__vector_base.36"** %this.addr, align 4
  store i32* %__new_last, i32** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.36"*, %"class.std::__2::__vector_base.36"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.36", %"class.std::__2::__vector_base.36"* %this1, i32 0, i32 1
  %0 = load i32*, i32** %__end_, align 4
  store i32* %0, i32** %__soon_to_be_end, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load i32*, i32** %__new_last.addr, align 4
  %2 = load i32*, i32** %__soon_to_be_end, align 4
  %cmp = icmp ne i32* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.40"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE7__allocEv(%"class.std::__2::__vector_base.36"* %this1) #8
  %3 = load i32*, i32** %__soon_to_be_end, align 4
  %incdec.ptr = getelementptr inbounds i32, i32* %3, i32 -1
  store i32* %incdec.ptr, i32** %__soon_to_be_end, align 4
  %call2 = call i32* @_ZNSt3__212__to_addressIiEEPT_S2_(i32* %incdec.ptr) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE7destroyIiEEvRS2_PT_(%"class.std::__2::allocator.40"* nonnull align 1 dereferenceable(1) %call, i32* %call2)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %4 = load i32*, i32** %__new_last.addr, align 4
  %__end_3 = getelementptr inbounds %"class.std::__2::__vector_base.36", %"class.std::__2::__vector_base.36"* %this1, i32 0, i32 1
  store i32* %4, i32** %__end_3, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE7destroyIiEEvRS2_PT_(%"class.std::__2::allocator.40"* nonnull align 1 dereferenceable(1) %__a, i32* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.40"*, align 4
  %__p.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy", align 1
  store %"class.std::__2::allocator.40"* %__a, %"class.std::__2::allocator.40"** %__a.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.40"*, %"class.std::__2::allocator.40"** %__a.addr, align 4
  %2 = load i32*, i32** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE9__destroyIiEEvNS_17integral_constantIbLb1EEERS2_PT_(%"class.std::__2::allocator.40"* nonnull align 1 dereferenceable(1) %1, i32* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.40"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE7__allocEv(%"class.std::__2::__vector_base.36"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.36"*, align 4
  store %"class.std::__2::__vector_base.36"* %this, %"class.std::__2::__vector_base.36"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.36"*, %"class.std::__2::__vector_base.36"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.36", %"class.std::__2::__vector_base.36"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.40"* @_ZNSt3__217__compressed_pairIPiNS_9allocatorIiEEE6secondEv(%"class.std::__2::__compressed_pair.37"* %__end_cap_) #8
  ret %"class.std::__2::allocator.40"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNSt3__212__to_addressIiEEPT_S2_(i32* %__p) #0 comdat {
entry:
  %__p.addr = alloca i32*, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %0 = load i32*, i32** %__p.addr, align 4
  ret i32* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE9__destroyIiEEvNS_17integral_constantIbLb1EEERS2_PT_(%"class.std::__2::allocator.40"* nonnull align 1 dereferenceable(1) %__a, i32* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.40"*, align 4
  %__p.addr = alloca i32*, align 4
  store %"class.std::__2::allocator.40"* %__a, %"class.std::__2::allocator.40"** %__a.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.40"*, %"class.std::__2::allocator.40"** %__a.addr, align 4
  %2 = load i32*, i32** %__p.addr, align 4
  call void @_ZNSt3__29allocatorIiE7destroyEPi(%"class.std::__2::allocator.40"* %1, i32* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIiE7destroyEPi(%"class.std::__2::allocator.40"* %this, i32* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.40"*, align 4
  %__p.addr = alloca i32*, align 4
  store %"class.std::__2::allocator.40"* %this, %"class.std::__2::allocator.40"** %this.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.40"*, %"class.std::__2::allocator.40"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.40"* @_ZNSt3__217__compressed_pairIPiNS_9allocatorIiEEE6secondEv(%"class.std::__2::__compressed_pair.37"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.37"*, align 4
  store %"class.std::__2::__compressed_pair.37"* %this, %"class.std::__2::__compressed_pair.37"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.37"*, %"class.std::__2::__compressed_pair.37"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.37"* %this1 to %"struct.std::__2::__compressed_pair_elem.39"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.40"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIiEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.39"* %0) #8
  ret %"class.std::__2::allocator.40"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.40"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIiEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.39"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.39"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.39"* %this, %"struct.std::__2::__compressed_pair_elem.39"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.39"*, %"struct.std::__2::__compressed_pair_elem.39"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.39"* %this1 to %"class.std::__2::allocator.40"*
  ret %"class.std::__2::allocator.40"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIiNS_9allocatorIiEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector.35"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.35"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector.35"* %this, %"class.std::__2::vector.35"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector.35"*, %"class.std::__2::vector.35"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector.35"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.35"*, align 4
  store %"class.std::__2::vector.35"* %this, %"class.std::__2::vector.35"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.35"*, %"class.std::__2::vector.35"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.35"* %this1 to %"class.std::__2::__vector_base.36"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.36", %"class.std::__2::__vector_base.36"* %0, i32 0, i32 0
  %1 = load i32*, i32** %__begin_, align 4
  %call = call i32* @_ZNSt3__212__to_addressIiEEPT_S2_(i32* %1) #8
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::vector.35"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.35"*, align 4
  store %"class.std::__2::vector.35"* %this, %"class.std::__2::vector.35"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.35"*, %"class.std::__2::vector.35"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.35"* %this1 to %"class.std::__2::__vector_base.36"*
  %call = call i32 @_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::__vector_base.36"* %0) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::__vector_base.36"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.36"*, align 4
  store %"class.std::__2::__vector_base.36"* %this, %"class.std::__2::__vector_base.36"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.36"*, %"class.std::__2::__vector_base.36"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE9__end_capEv(%"class.std::__2::__vector_base.36"* %this1) #8
  %0 = load i32*, i32** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.36", %"class.std::__2::__vector_base.36"* %this1, i32 0, i32 0
  %1 = load i32*, i32** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint i32* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint i32* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE9__end_capEv(%"class.std::__2::__vector_base.36"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.36"*, align 4
  store %"class.std::__2::__vector_base.36"* %this, %"class.std::__2::__vector_base.36"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.36"*, %"class.std::__2::__vector_base.36"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.36", %"class.std::__2::__vector_base.36"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__217__compressed_pairIPiNS_9allocatorIiEEE5firstEv(%"class.std::__2::__compressed_pair.37"* %__end_cap_) #8
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__217__compressed_pairIPiNS_9allocatorIiEEE5firstEv(%"class.std::__2::__compressed_pair.37"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.37"*, align 4
  store %"class.std::__2::__compressed_pair.37"* %this, %"class.std::__2::__compressed_pair.37"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.37"*, %"class.std::__2::__compressed_pair.37"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.37"* %this1 to %"struct.std::__2::__compressed_pair_elem.38"*
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__222__compressed_pair_elemIPiLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.38"* %0) #8
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__222__compressed_pair_elemIPiLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.38"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.38"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.38"* %this, %"struct.std::__2::__compressed_pair_elem.38"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.38"*, %"struct.std::__2::__compressed_pair_elem.38"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.38", %"struct.std::__2::__compressed_pair_elem.38"* %this1, i32 0, i32 0
  ret i32** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIiNS_9allocatorIiEEE4swapERS3_(%"class.std::__2::vector.35"* %this, %"class.std::__2::vector.35"* nonnull align 4 dereferenceable(12) %__x) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.35"*, align 4
  %__x.addr = alloca %"class.std::__2::vector.35"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant.146", align 1
  store %"class.std::__2::vector.35"* %this, %"class.std::__2::vector.35"** %this.addr, align 4
  store %"class.std::__2::vector.35"* %__x, %"class.std::__2::vector.35"** %__x.addr, align 4
  %this1 = load %"class.std::__2::vector.35"*, %"class.std::__2::vector.35"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.35"* %this1 to %"class.std::__2::__vector_base.36"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.36", %"class.std::__2::__vector_base.36"* %0, i32 0, i32 0
  %1 = load %"class.std::__2::vector.35"*, %"class.std::__2::vector.35"** %__x.addr, align 4
  %2 = bitcast %"class.std::__2::vector.35"* %1 to %"class.std::__2::__vector_base.36"*
  %__begin_2 = getelementptr inbounds %"class.std::__2::__vector_base.36", %"class.std::__2::__vector_base.36"* %2, i32 0, i32 0
  call void @_ZNSt3__24swapIPiEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_(i32** nonnull align 4 dereferenceable(4) %__begin_, i32** nonnull align 4 dereferenceable(4) %__begin_2) #8
  %3 = bitcast %"class.std::__2::vector.35"* %this1 to %"class.std::__2::__vector_base.36"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.36", %"class.std::__2::__vector_base.36"* %3, i32 0, i32 1
  %4 = load %"class.std::__2::vector.35"*, %"class.std::__2::vector.35"** %__x.addr, align 4
  %5 = bitcast %"class.std::__2::vector.35"* %4 to %"class.std::__2::__vector_base.36"*
  %__end_3 = getelementptr inbounds %"class.std::__2::__vector_base.36", %"class.std::__2::__vector_base.36"* %5, i32 0, i32 1
  call void @_ZNSt3__24swapIPiEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_(i32** nonnull align 4 dereferenceable(4) %__end_, i32** nonnull align 4 dereferenceable(4) %__end_3) #8
  %6 = bitcast %"class.std::__2::vector.35"* %this1 to %"class.std::__2::__vector_base.36"*
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE9__end_capEv(%"class.std::__2::__vector_base.36"* %6) #8
  %7 = load %"class.std::__2::vector.35"*, %"class.std::__2::vector.35"** %__x.addr, align 4
  %8 = bitcast %"class.std::__2::vector.35"* %7 to %"class.std::__2::__vector_base.36"*
  %call4 = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE9__end_capEv(%"class.std::__2::__vector_base.36"* %8) #8
  call void @_ZNSt3__24swapIPiEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_(i32** nonnull align 4 dereferenceable(4) %call, i32** nonnull align 4 dereferenceable(4) %call4) #8
  %9 = bitcast %"class.std::__2::vector.35"* %this1 to %"class.std::__2::__vector_base.36"*
  %call5 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.40"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE7__allocEv(%"class.std::__2::__vector_base.36"* %9) #8
  %10 = load %"class.std::__2::vector.35"*, %"class.std::__2::vector.35"** %__x.addr, align 4
  %11 = bitcast %"class.std::__2::vector.35"* %10 to %"class.std::__2::__vector_base.36"*
  %call6 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.40"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE7__allocEv(%"class.std::__2::__vector_base.36"* %11) #8
  call void @_ZNSt3__216__swap_allocatorINS_9allocatorIiEEEEvRT_S4_NS_17integral_constantIbLb0EEE(%"class.std::__2::allocator.40"* nonnull align 1 dereferenceable(1) %call5, %"class.std::__2::allocator.40"* nonnull align 1 dereferenceable(1) %call6) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__24swapIPiEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_(i32** nonnull align 4 dereferenceable(4) %__x, i32** nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %__x.addr = alloca i32**, align 4
  %__y.addr = alloca i32**, align 4
  %__t = alloca i32*, align 4
  store i32** %__x, i32*** %__x.addr, align 4
  store i32** %__y, i32*** %__y.addr, align 4
  %0 = load i32**, i32*** %__x.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__24moveIRPiEEONS_16remove_referenceIT_E4typeEOS4_(i32** nonnull align 4 dereferenceable(4) %0) #8
  %1 = load i32*, i32** %call, align 4
  store i32* %1, i32** %__t, align 4
  %2 = load i32**, i32*** %__y.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__24moveIRPiEEONS_16remove_referenceIT_E4typeEOS4_(i32** nonnull align 4 dereferenceable(4) %2) #8
  %3 = load i32*, i32** %call1, align 4
  %4 = load i32**, i32*** %__x.addr, align 4
  store i32* %3, i32** %4, align 4
  %call2 = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__24moveIRPiEEONS_16remove_referenceIT_E4typeEOS4_(i32** nonnull align 4 dereferenceable(4) %__t) #8
  %5 = load i32*, i32** %call2, align 4
  %6 = load i32**, i32*** %__y.addr, align 4
  store i32* %5, i32** %6, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE9__end_capEv(%"class.std::__2::__vector_base.36"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.36"*, align 4
  store %"class.std::__2::__vector_base.36"* %this, %"class.std::__2::__vector_base.36"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.36"*, %"class.std::__2::__vector_base.36"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.36", %"class.std::__2::__vector_base.36"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__217__compressed_pairIPiNS_9allocatorIiEEE5firstEv(%"class.std::__2::__compressed_pair.37"* %__end_cap_) #8
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216__swap_allocatorINS_9allocatorIiEEEEvRT_S4_NS_17integral_constantIbLb0EEE(%"class.std::__2::allocator.40"* nonnull align 1 dereferenceable(1) %0, %"class.std::__2::allocator.40"* nonnull align 1 dereferenceable(1) %1) #0 comdat {
entry:
  %2 = alloca %"struct.std::__2::integral_constant.146", align 1
  %.addr = alloca %"class.std::__2::allocator.40"*, align 4
  %.addr1 = alloca %"class.std::__2::allocator.40"*, align 4
  store %"class.std::__2::allocator.40"* %0, %"class.std::__2::allocator.40"** %.addr, align 4
  store %"class.std::__2::allocator.40"* %1, %"class.std::__2::allocator.40"** %.addr1, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNSt3__24moveIRPiEEONS_16remove_referenceIT_E4typeEOS4_(i32** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca i32**, align 4
  store i32** %__t, i32*** %__t.addr, align 4
  %0 = load i32**, i32*** %__t.addr, align 4
  ret i32** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNSt3__217__compressed_pairIPiNS_9allocatorIiEEE5firstEv(%"class.std::__2::__compressed_pair.37"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.37"*, align 4
  store %"class.std::__2::__compressed_pair.37"* %this, %"class.std::__2::__compressed_pair.37"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.37"*, %"class.std::__2::__compressed_pair.37"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.37"* %this1 to %"struct.std::__2::__compressed_pair_elem.38"*
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__222__compressed_pair_elemIPiLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.38"* %0) #8
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNSt3__222__compressed_pair_elemIPiLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.38"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.38"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.38"* %this, %"struct.std::__2::__compressed_pair_elem.38"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.38"*, %"struct.std::__2::__compressed_pair_elem.38"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.38", %"struct.std::__2::__compressed_pair_elem.38"* %this1, i32 0, i32 0
  ret i32** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector.35"* @_ZNSt3__26vectorIiNS_9allocatorIiEEED2Ev(%"class.std::__2::vector.35"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.35"*, align 4
  store %"class.std::__2::vector.35"* %this, %"class.std::__2::vector.35"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.35"*, %"class.std::__2::vector.35"** %this.addr, align 4
  call void @_ZNKSt3__26vectorIiNS_9allocatorIiEEE17__annotate_deleteEv(%"class.std::__2::vector.35"* %this1) #8
  %0 = bitcast %"class.std::__2::vector.35"* %this1 to %"class.std::__2::__vector_base.36"*
  %call = call %"class.std::__2::__vector_base.36"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEED2Ev(%"class.std::__2::__vector_base.36"* %0) #8
  ret %"class.std::__2::vector.35"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIiNS_9allocatorIiEEE17__annotate_deleteEv(%"class.std::__2::vector.35"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.35"*, align 4
  store %"class.std::__2::vector.35"* %this, %"class.std::__2::vector.35"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.35"*, %"class.std::__2::vector.35"** %this.addr, align 4
  %call = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector.35"* %this1) #8
  %0 = bitcast i32* %call to i8*
  %call2 = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector.35"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::vector.35"* %this1) #8
  %add.ptr = getelementptr inbounds i32, i32* %call2, i32 %call3
  %1 = bitcast i32* %add.ptr to i8*
  %call4 = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector.35"* %this1) #8
  %call5 = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4sizeEv(%"class.std::__2::vector.35"* %this1) #8
  %add.ptr6 = getelementptr inbounds i32, i32* %call4, i32 %call5
  %2 = bitcast i32* %add.ptr6 to i8*
  %call7 = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector.35"* %this1) #8
  %call8 = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::vector.35"* %this1) #8
  %add.ptr9 = getelementptr inbounds i32, i32* %call7, i32 %call8
  %3 = bitcast i32* %add.ptr9 to i8*
  call void @_ZNKSt3__26vectorIiNS_9allocatorIiEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector.35"* %this1, i8* %0, i8* %1, i8* %2, i8* %3) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base.36"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEED2Ev(%"class.std::__2::__vector_base.36"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__vector_base.36"*, align 4
  %this.addr = alloca %"class.std::__2::__vector_base.36"*, align 4
  store %"class.std::__2::__vector_base.36"* %this, %"class.std::__2::__vector_base.36"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.36"*, %"class.std::__2::__vector_base.36"** %this.addr, align 4
  store %"class.std::__2::__vector_base.36"* %this1, %"class.std::__2::__vector_base.36"** %retval, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.36", %"class.std::__2::__vector_base.36"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__begin_, align 4
  %cmp = icmp ne i32* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE5clearEv(%"class.std::__2::__vector_base.36"* %this1) #8
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.40"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE7__allocEv(%"class.std::__2::__vector_base.36"* %this1) #8
  %__begin_2 = getelementptr inbounds %"class.std::__2::__vector_base.36", %"class.std::__2::__vector_base.36"* %this1, i32 0, i32 0
  %1 = load i32*, i32** %__begin_2, align 4
  %call3 = call i32 @_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::__vector_base.36"* %this1) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE10deallocateERS2_Pim(%"class.std::__2::allocator.40"* nonnull align 1 dereferenceable(1) %call, i32* %1, i32 %call3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"class.std::__2::__vector_base.36"*, %"class.std::__2::__vector_base.36"** %retval, align 4
  ret %"class.std::__2::__vector_base.36"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE10deallocateERS2_Pim(%"class.std::__2::allocator.40"* nonnull align 1 dereferenceable(1) %__a, i32* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.40"*, align 4
  %__p.addr = alloca i32*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.40"* %__a, %"class.std::__2::allocator.40"** %__a.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.40"*, %"class.std::__2::allocator.40"** %__a.addr, align 4
  %1 = load i32*, i32** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorIiE10deallocateEPim(%"class.std::__2::allocator.40"* %0, i32* %1, i32 %2) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIiE10deallocateEPim(%"class.std::__2::allocator.40"* %this, i32* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.40"*, align 4
  %__p.addr = alloca i32*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.40"* %this, %"class.std::__2::allocator.40"** %this.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.40"*, %"class.std::__2::allocator.40"** %this.addr, align 4
  %0 = load i32*, i32** %__p.addr, align 4
  %1 = bitcast i32* %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %__ptr, i32 %__size, i32 %__align) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  %1 = load i32, i32* %__size.addr, align 4
  %2 = load i32, i32* %__align.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm(i8* %0, i32 %1, i32 %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm(i8* %__ptr, i32 %__size, i32 %__align) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  %1 = load i32, i32* %__size.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm(i8* %0, i32 %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm(i8* %__ptr, i32 %__size) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller9__do_callEPv(i8* %0)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller9__do_callEPv(i8* %__ptr) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  call void @_ZdlPv(i8* %0) #9
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_21VertexIndex_tag_type_EEEaE5clearEv(%"class.draco::IndexTypeVector.26"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector.26"*, align 4
  store %"class.draco::IndexTypeVector.26"* %this, %"class.draco::IndexTypeVector.26"** %this.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector.26"*, %"class.draco::IndexTypeVector.26"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector.26", %"class.draco::IndexTypeVector.26"* %this1, i32 0, i32 0
  call void @_ZNSt3__26vectorIaNS_9allocatorIaEEE5clearEv(%"class.std::__2::vector.27"* %vector_) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_21VertexIndex_tag_type_EEEaE4swapERS4_(%"class.draco::IndexTypeVector.26"* %this, %"class.draco::IndexTypeVector.26"* nonnull align 4 dereferenceable(12) %arg) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector.26"*, align 4
  %arg.addr = alloca %"class.draco::IndexTypeVector.26"*, align 4
  store %"class.draco::IndexTypeVector.26"* %this, %"class.draco::IndexTypeVector.26"** %this.addr, align 4
  store %"class.draco::IndexTypeVector.26"* %arg, %"class.draco::IndexTypeVector.26"** %arg.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector.26"*, %"class.draco::IndexTypeVector.26"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector.26", %"class.draco::IndexTypeVector.26"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexTypeVector.26"*, %"class.draco::IndexTypeVector.26"** %arg.addr, align 4
  %vector_2 = getelementptr inbounds %"class.draco::IndexTypeVector.26", %"class.draco::IndexTypeVector.26"* %0, i32 0, i32 0
  call void @_ZNSt3__26vectorIaNS_9allocatorIaEEE4swapERS3_(%"class.std::__2::vector.27"* %vector_, %"class.std::__2::vector.27"* nonnull align 4 dereferenceable(12) %vector_2) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexTypeVector.26"* @_ZN5draco15IndexTypeVectorINS_9IndexTypeIjNS_21VertexIndex_tag_type_EEEaED2Ev(%"class.draco::IndexTypeVector.26"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector.26"*, align 4
  store %"class.draco::IndexTypeVector.26"* %this, %"class.draco::IndexTypeVector.26"** %this.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector.26"*, %"class.draco::IndexTypeVector.26"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector.26", %"class.draco::IndexTypeVector.26"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::vector.27"* @_ZNSt3__26vectorIaNS_9allocatorIaEEED2Ev(%"class.std::__2::vector.27"* %vector_) #8
  ret %"class.draco::IndexTypeVector.26"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIaNS_9allocatorIaEEE5clearEv(%"class.std::__2::vector.27"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.27"*, align 4
  %__old_size = alloca i32, align 4
  store %"class.std::__2::vector.27"* %this, %"class.std::__2::vector.27"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.27"*, %"class.std::__2::vector.27"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIaNS_9allocatorIaEEE4sizeEv(%"class.std::__2::vector.27"* %this1) #8
  store i32 %call, i32* %__old_size, align 4
  %0 = bitcast %"class.std::__2::vector.27"* %this1 to %"class.std::__2::__vector_base.28"*
  call void @_ZNSt3__213__vector_baseIaNS_9allocatorIaEEE5clearEv(%"class.std::__2::__vector_base.28"* %0) #8
  %1 = load i32, i32* %__old_size, align 4
  call void @_ZNKSt3__26vectorIaNS_9allocatorIaEEE17__annotate_shrinkEm(%"class.std::__2::vector.27"* %this1, i32 %1) #8
  call void @_ZNSt3__26vectorIaNS_9allocatorIaEEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector.27"* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIaNS_9allocatorIaEEE4sizeEv(%"class.std::__2::vector.27"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.27"*, align 4
  store %"class.std::__2::vector.27"* %this, %"class.std::__2::vector.27"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.27"*, %"class.std::__2::vector.27"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.27"* %this1 to %"class.std::__2::__vector_base.28"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.28", %"class.std::__2::__vector_base.28"* %0, i32 0, i32 1
  %1 = load i8*, i8** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector.27"* %this1 to %"class.std::__2::__vector_base.28"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.28", %"class.std::__2::__vector_base.28"* %2, i32 0, i32 0
  %3 = load i8*, i8** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint i8* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  ret i32 %sub.ptr.sub
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIaNS_9allocatorIaEEE5clearEv(%"class.std::__2::__vector_base.28"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.28"*, align 4
  store %"class.std::__2::__vector_base.28"* %this, %"class.std::__2::__vector_base.28"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.28"*, %"class.std::__2::__vector_base.28"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.28", %"class.std::__2::__vector_base.28"* %this1, i32 0, i32 0
  %0 = load i8*, i8** %__begin_, align 4
  call void @_ZNSt3__213__vector_baseIaNS_9allocatorIaEEE17__destruct_at_endEPa(%"class.std::__2::__vector_base.28"* %this1, i8* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIaNS_9allocatorIaEEE17__annotate_shrinkEm(%"class.std::__2::vector.27"* %this, i32 %__old_size) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.27"*, align 4
  %__old_size.addr = alloca i32, align 4
  store %"class.std::__2::vector.27"* %this, %"class.std::__2::vector.27"** %this.addr, align 4
  store i32 %__old_size, i32* %__old_size.addr, align 4
  %this1 = load %"class.std::__2::vector.27"*, %"class.std::__2::vector.27"** %this.addr, align 4
  %call = call i8* @_ZNKSt3__26vectorIaNS_9allocatorIaEEE4dataEv(%"class.std::__2::vector.27"* %this1) #8
  %call2 = call i8* @_ZNKSt3__26vectorIaNS_9allocatorIaEEE4dataEv(%"class.std::__2::vector.27"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorIaNS_9allocatorIaEEE8capacityEv(%"class.std::__2::vector.27"* %this1) #8
  %add.ptr = getelementptr inbounds i8, i8* %call2, i32 %call3
  %call4 = call i8* @_ZNKSt3__26vectorIaNS_9allocatorIaEEE4dataEv(%"class.std::__2::vector.27"* %this1) #8
  %0 = load i32, i32* %__old_size.addr, align 4
  %add.ptr5 = getelementptr inbounds i8, i8* %call4, i32 %0
  %call6 = call i8* @_ZNKSt3__26vectorIaNS_9allocatorIaEEE4dataEv(%"class.std::__2::vector.27"* %this1) #8
  %call7 = call i32 @_ZNKSt3__26vectorIaNS_9allocatorIaEEE4sizeEv(%"class.std::__2::vector.27"* %this1) #8
  %add.ptr8 = getelementptr inbounds i8, i8* %call6, i32 %call7
  call void @_ZNKSt3__26vectorIaNS_9allocatorIaEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector.27"* %this1, i8* %call, i8* %add.ptr, i8* %add.ptr5, i8* %add.ptr8) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIaNS_9allocatorIaEEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector.27"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.27"*, align 4
  store %"class.std::__2::vector.27"* %this, %"class.std::__2::vector.27"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.27"*, %"class.std::__2::vector.27"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIaNS_9allocatorIaEEE17__destruct_at_endEPa(%"class.std::__2::__vector_base.28"* %this, i8* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.28"*, align 4
  %__new_last.addr = alloca i8*, align 4
  %__soon_to_be_end = alloca i8*, align 4
  store %"class.std::__2::__vector_base.28"* %this, %"class.std::__2::__vector_base.28"** %this.addr, align 4
  store i8* %__new_last, i8** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.28"*, %"class.std::__2::__vector_base.28"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.28", %"class.std::__2::__vector_base.28"* %this1, i32 0, i32 1
  %0 = load i8*, i8** %__end_, align 4
  store i8* %0, i8** %__soon_to_be_end, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load i8*, i8** %__new_last.addr, align 4
  %2 = load i8*, i8** %__soon_to_be_end, align 4
  %cmp = icmp ne i8* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.32"* @_ZNSt3__213__vector_baseIaNS_9allocatorIaEEE7__allocEv(%"class.std::__2::__vector_base.28"* %this1) #8
  %3 = load i8*, i8** %__soon_to_be_end, align 4
  %incdec.ptr = getelementptr inbounds i8, i8* %3, i32 -1
  store i8* %incdec.ptr, i8** %__soon_to_be_end, align 4
  %call2 = call i8* @_ZNSt3__212__to_addressIaEEPT_S2_(i8* %incdec.ptr) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIaEEE7destroyIaEEvRS2_PT_(%"class.std::__2::allocator.32"* nonnull align 1 dereferenceable(1) %call, i8* %call2)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %4 = load i8*, i8** %__new_last.addr, align 4
  %__end_3 = getelementptr inbounds %"class.std::__2::__vector_base.28", %"class.std::__2::__vector_base.28"* %this1, i32 0, i32 1
  store i8* %4, i8** %__end_3, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIaEEE7destroyIaEEvRS2_PT_(%"class.std::__2::allocator.32"* nonnull align 1 dereferenceable(1) %__a, i8* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.32"*, align 4
  %__p.addr = alloca i8*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy.147", align 1
  store %"class.std::__2::allocator.32"* %__a, %"class.std::__2::allocator.32"** %__a.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy.147"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.32"*, %"class.std::__2::allocator.32"** %__a.addr, align 4
  %2 = load i8*, i8** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIaEEE9__destroyIaEEvNS_17integral_constantIbLb1EEERS2_PT_(%"class.std::__2::allocator.32"* nonnull align 1 dereferenceable(1) %1, i8* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.32"* @_ZNSt3__213__vector_baseIaNS_9allocatorIaEEE7__allocEv(%"class.std::__2::__vector_base.28"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.28"*, align 4
  store %"class.std::__2::__vector_base.28"* %this, %"class.std::__2::__vector_base.28"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.28"*, %"class.std::__2::__vector_base.28"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.28", %"class.std::__2::__vector_base.28"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.32"* @_ZNSt3__217__compressed_pairIPaNS_9allocatorIaEEE6secondEv(%"class.std::__2::__compressed_pair.29"* %__end_cap_) #8
  ret %"class.std::__2::allocator.32"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__212__to_addressIaEEPT_S2_(i8* %__p) #0 comdat {
entry:
  %__p.addr = alloca i8*, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %0 = load i8*, i8** %__p.addr, align 4
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIaEEE9__destroyIaEEvNS_17integral_constantIbLb1EEERS2_PT_(%"class.std::__2::allocator.32"* nonnull align 1 dereferenceable(1) %__a, i8* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.32"*, align 4
  %__p.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.32"* %__a, %"class.std::__2::allocator.32"** %__a.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.32"*, %"class.std::__2::allocator.32"** %__a.addr, align 4
  %2 = load i8*, i8** %__p.addr, align 4
  call void @_ZNSt3__29allocatorIaE7destroyEPa(%"class.std::__2::allocator.32"* %1, i8* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIaE7destroyEPa(%"class.std::__2::allocator.32"* %this, i8* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.32"*, align 4
  %__p.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.32"* %this, %"class.std::__2::allocator.32"** %this.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.32"*, %"class.std::__2::allocator.32"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.32"* @_ZNSt3__217__compressed_pairIPaNS_9allocatorIaEEE6secondEv(%"class.std::__2::__compressed_pair.29"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.29"*, align 4
  store %"class.std::__2::__compressed_pair.29"* %this, %"class.std::__2::__compressed_pair.29"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.29"*, %"class.std::__2::__compressed_pair.29"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.29"* %this1 to %"struct.std::__2::__compressed_pair_elem.31"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.32"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIaEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.31"* %0) #8
  ret %"class.std::__2::allocator.32"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.32"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIaEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.31"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.31"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.31"* %this, %"struct.std::__2::__compressed_pair_elem.31"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.31"*, %"struct.std::__2::__compressed_pair_elem.31"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.31"* %this1 to %"class.std::__2::allocator.32"*
  ret %"class.std::__2::allocator.32"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIaNS_9allocatorIaEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector.27"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.27"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector.27"* %this, %"class.std::__2::vector.27"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector.27"*, %"class.std::__2::vector.27"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__26vectorIaNS_9allocatorIaEEE4dataEv(%"class.std::__2::vector.27"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.27"*, align 4
  store %"class.std::__2::vector.27"* %this, %"class.std::__2::vector.27"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.27"*, %"class.std::__2::vector.27"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.27"* %this1 to %"class.std::__2::__vector_base.28"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.28", %"class.std::__2::__vector_base.28"* %0, i32 0, i32 0
  %1 = load i8*, i8** %__begin_, align 4
  %call = call i8* @_ZNSt3__212__to_addressIaEEPT_S2_(i8* %1) #8
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIaNS_9allocatorIaEEE8capacityEv(%"class.std::__2::vector.27"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.27"*, align 4
  store %"class.std::__2::vector.27"* %this, %"class.std::__2::vector.27"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.27"*, %"class.std::__2::vector.27"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.27"* %this1 to %"class.std::__2::__vector_base.28"*
  %call = call i32 @_ZNKSt3__213__vector_baseIaNS_9allocatorIaEEE8capacityEv(%"class.std::__2::__vector_base.28"* %0) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseIaNS_9allocatorIaEEE8capacityEv(%"class.std::__2::__vector_base.28"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.28"*, align 4
  store %"class.std::__2::__vector_base.28"* %this, %"class.std::__2::__vector_base.28"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.28"*, %"class.std::__2::__vector_base.28"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__213__vector_baseIaNS_9allocatorIaEEE9__end_capEv(%"class.std::__2::__vector_base.28"* %this1) #8
  %0 = load i8*, i8** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.28", %"class.std::__2::__vector_base.28"* %this1, i32 0, i32 0
  %1 = load i8*, i8** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint i8* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  ret i32 %sub.ptr.sub
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__213__vector_baseIaNS_9allocatorIaEEE9__end_capEv(%"class.std::__2::__vector_base.28"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.28"*, align 4
  store %"class.std::__2::__vector_base.28"* %this, %"class.std::__2::__vector_base.28"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.28"*, %"class.std::__2::__vector_base.28"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.28", %"class.std::__2::__vector_base.28"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__217__compressed_pairIPaNS_9allocatorIaEEE5firstEv(%"class.std::__2::__compressed_pair.29"* %__end_cap_) #8
  ret i8** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__217__compressed_pairIPaNS_9allocatorIaEEE5firstEv(%"class.std::__2::__compressed_pair.29"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.29"*, align 4
  store %"class.std::__2::__compressed_pair.29"* %this, %"class.std::__2::__compressed_pair.29"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.29"*, %"class.std::__2::__compressed_pair.29"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.29"* %this1 to %"struct.std::__2::__compressed_pair_elem.30"*
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__222__compressed_pair_elemIPaLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.30"* %0) #8
  ret i8** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__222__compressed_pair_elemIPaLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.30"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.30"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.30"* %this, %"struct.std::__2::__compressed_pair_elem.30"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.30"*, %"struct.std::__2::__compressed_pair_elem.30"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.30", %"struct.std::__2::__compressed_pair_elem.30"* %this1, i32 0, i32 0
  ret i8** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIaNS_9allocatorIaEEE4swapERS3_(%"class.std::__2::vector.27"* %this, %"class.std::__2::vector.27"* nonnull align 4 dereferenceable(12) %__x) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.27"*, align 4
  %__x.addr = alloca %"class.std::__2::vector.27"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant.146", align 1
  store %"class.std::__2::vector.27"* %this, %"class.std::__2::vector.27"** %this.addr, align 4
  store %"class.std::__2::vector.27"* %__x, %"class.std::__2::vector.27"** %__x.addr, align 4
  %this1 = load %"class.std::__2::vector.27"*, %"class.std::__2::vector.27"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.27"* %this1 to %"class.std::__2::__vector_base.28"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.28", %"class.std::__2::__vector_base.28"* %0, i32 0, i32 0
  %1 = load %"class.std::__2::vector.27"*, %"class.std::__2::vector.27"** %__x.addr, align 4
  %2 = bitcast %"class.std::__2::vector.27"* %1 to %"class.std::__2::__vector_base.28"*
  %__begin_2 = getelementptr inbounds %"class.std::__2::__vector_base.28", %"class.std::__2::__vector_base.28"* %2, i32 0, i32 0
  call void @_ZNSt3__24swapIPaEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_(i8** nonnull align 4 dereferenceable(4) %__begin_, i8** nonnull align 4 dereferenceable(4) %__begin_2) #8
  %3 = bitcast %"class.std::__2::vector.27"* %this1 to %"class.std::__2::__vector_base.28"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.28", %"class.std::__2::__vector_base.28"* %3, i32 0, i32 1
  %4 = load %"class.std::__2::vector.27"*, %"class.std::__2::vector.27"** %__x.addr, align 4
  %5 = bitcast %"class.std::__2::vector.27"* %4 to %"class.std::__2::__vector_base.28"*
  %__end_3 = getelementptr inbounds %"class.std::__2::__vector_base.28", %"class.std::__2::__vector_base.28"* %5, i32 0, i32 1
  call void @_ZNSt3__24swapIPaEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_(i8** nonnull align 4 dereferenceable(4) %__end_, i8** nonnull align 4 dereferenceable(4) %__end_3) #8
  %6 = bitcast %"class.std::__2::vector.27"* %this1 to %"class.std::__2::__vector_base.28"*
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__213__vector_baseIaNS_9allocatorIaEEE9__end_capEv(%"class.std::__2::__vector_base.28"* %6) #8
  %7 = load %"class.std::__2::vector.27"*, %"class.std::__2::vector.27"** %__x.addr, align 4
  %8 = bitcast %"class.std::__2::vector.27"* %7 to %"class.std::__2::__vector_base.28"*
  %call4 = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__213__vector_baseIaNS_9allocatorIaEEE9__end_capEv(%"class.std::__2::__vector_base.28"* %8) #8
  call void @_ZNSt3__24swapIPaEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_(i8** nonnull align 4 dereferenceable(4) %call, i8** nonnull align 4 dereferenceable(4) %call4) #8
  %9 = bitcast %"class.std::__2::vector.27"* %this1 to %"class.std::__2::__vector_base.28"*
  %call5 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.32"* @_ZNSt3__213__vector_baseIaNS_9allocatorIaEEE7__allocEv(%"class.std::__2::__vector_base.28"* %9) #8
  %10 = load %"class.std::__2::vector.27"*, %"class.std::__2::vector.27"** %__x.addr, align 4
  %11 = bitcast %"class.std::__2::vector.27"* %10 to %"class.std::__2::__vector_base.28"*
  %call6 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.32"* @_ZNSt3__213__vector_baseIaNS_9allocatorIaEEE7__allocEv(%"class.std::__2::__vector_base.28"* %11) #8
  call void @_ZNSt3__216__swap_allocatorINS_9allocatorIaEEEEvRT_S4_NS_17integral_constantIbLb0EEE(%"class.std::__2::allocator.32"* nonnull align 1 dereferenceable(1) %call5, %"class.std::__2::allocator.32"* nonnull align 1 dereferenceable(1) %call6) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__24swapIPaEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_(i8** nonnull align 4 dereferenceable(4) %__x, i8** nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %__x.addr = alloca i8**, align 4
  %__y.addr = alloca i8**, align 4
  %__t = alloca i8*, align 4
  store i8** %__x, i8*** %__x.addr, align 4
  store i8** %__y, i8*** %__y.addr, align 4
  %0 = load i8**, i8*** %__x.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__24moveIRPaEEONS_16remove_referenceIT_E4typeEOS4_(i8** nonnull align 4 dereferenceable(4) %0) #8
  %1 = load i8*, i8** %call, align 4
  store i8* %1, i8** %__t, align 4
  %2 = load i8**, i8*** %__y.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__24moveIRPaEEONS_16remove_referenceIT_E4typeEOS4_(i8** nonnull align 4 dereferenceable(4) %2) #8
  %3 = load i8*, i8** %call1, align 4
  %4 = load i8**, i8*** %__x.addr, align 4
  store i8* %3, i8** %4, align 4
  %call2 = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__24moveIRPaEEONS_16remove_referenceIT_E4typeEOS4_(i8** nonnull align 4 dereferenceable(4) %__t) #8
  %5 = load i8*, i8** %call2, align 4
  %6 = load i8**, i8*** %__y.addr, align 4
  store i8* %5, i8** %6, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNSt3__213__vector_baseIaNS_9allocatorIaEEE9__end_capEv(%"class.std::__2::__vector_base.28"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.28"*, align 4
  store %"class.std::__2::__vector_base.28"* %this, %"class.std::__2::__vector_base.28"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.28"*, %"class.std::__2::__vector_base.28"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.28", %"class.std::__2::__vector_base.28"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__217__compressed_pairIPaNS_9allocatorIaEEE5firstEv(%"class.std::__2::__compressed_pair.29"* %__end_cap_) #8
  ret i8** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216__swap_allocatorINS_9allocatorIaEEEEvRT_S4_NS_17integral_constantIbLb0EEE(%"class.std::__2::allocator.32"* nonnull align 1 dereferenceable(1) %0, %"class.std::__2::allocator.32"* nonnull align 1 dereferenceable(1) %1) #0 comdat {
entry:
  %2 = alloca %"struct.std::__2::integral_constant.146", align 1
  %.addr = alloca %"class.std::__2::allocator.32"*, align 4
  %.addr1 = alloca %"class.std::__2::allocator.32"*, align 4
  store %"class.std::__2::allocator.32"* %0, %"class.std::__2::allocator.32"** %.addr, align 4
  store %"class.std::__2::allocator.32"* %1, %"class.std::__2::allocator.32"** %.addr1, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNSt3__24moveIRPaEEONS_16remove_referenceIT_E4typeEOS4_(i8** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca i8**, align 4
  store i8** %__t, i8*** %__t.addr, align 4
  %0 = load i8**, i8*** %__t.addr, align 4
  ret i8** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNSt3__217__compressed_pairIPaNS_9allocatorIaEEE5firstEv(%"class.std::__2::__compressed_pair.29"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.29"*, align 4
  store %"class.std::__2::__compressed_pair.29"* %this, %"class.std::__2::__compressed_pair.29"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.29"*, %"class.std::__2::__compressed_pair.29"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.29"* %this1 to %"struct.std::__2::__compressed_pair_elem.30"*
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__222__compressed_pair_elemIPaLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.30"* %0) #8
  ret i8** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNSt3__222__compressed_pair_elemIPaLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.30"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.30"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.30"* %this, %"struct.std::__2::__compressed_pair_elem.30"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.30"*, %"struct.std::__2::__compressed_pair_elem.30"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.30", %"struct.std::__2::__compressed_pair_elem.30"* %this1, i32 0, i32 0
  ret i8** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector.27"* @_ZNSt3__26vectorIaNS_9allocatorIaEEED2Ev(%"class.std::__2::vector.27"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.27"*, align 4
  store %"class.std::__2::vector.27"* %this, %"class.std::__2::vector.27"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.27"*, %"class.std::__2::vector.27"** %this.addr, align 4
  call void @_ZNKSt3__26vectorIaNS_9allocatorIaEEE17__annotate_deleteEv(%"class.std::__2::vector.27"* %this1) #8
  %0 = bitcast %"class.std::__2::vector.27"* %this1 to %"class.std::__2::__vector_base.28"*
  %call = call %"class.std::__2::__vector_base.28"* @_ZNSt3__213__vector_baseIaNS_9allocatorIaEEED2Ev(%"class.std::__2::__vector_base.28"* %0) #8
  ret %"class.std::__2::vector.27"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIaNS_9allocatorIaEEE17__annotate_deleteEv(%"class.std::__2::vector.27"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.27"*, align 4
  store %"class.std::__2::vector.27"* %this, %"class.std::__2::vector.27"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.27"*, %"class.std::__2::vector.27"** %this.addr, align 4
  %call = call i8* @_ZNKSt3__26vectorIaNS_9allocatorIaEEE4dataEv(%"class.std::__2::vector.27"* %this1) #8
  %call2 = call i8* @_ZNKSt3__26vectorIaNS_9allocatorIaEEE4dataEv(%"class.std::__2::vector.27"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorIaNS_9allocatorIaEEE8capacityEv(%"class.std::__2::vector.27"* %this1) #8
  %add.ptr = getelementptr inbounds i8, i8* %call2, i32 %call3
  %call4 = call i8* @_ZNKSt3__26vectorIaNS_9allocatorIaEEE4dataEv(%"class.std::__2::vector.27"* %this1) #8
  %call5 = call i32 @_ZNKSt3__26vectorIaNS_9allocatorIaEEE4sizeEv(%"class.std::__2::vector.27"* %this1) #8
  %add.ptr6 = getelementptr inbounds i8, i8* %call4, i32 %call5
  %call7 = call i8* @_ZNKSt3__26vectorIaNS_9allocatorIaEEE4dataEv(%"class.std::__2::vector.27"* %this1) #8
  %call8 = call i32 @_ZNKSt3__26vectorIaNS_9allocatorIaEEE8capacityEv(%"class.std::__2::vector.27"* %this1) #8
  %add.ptr9 = getelementptr inbounds i8, i8* %call7, i32 %call8
  call void @_ZNKSt3__26vectorIaNS_9allocatorIaEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector.27"* %this1, i8* %call, i8* %add.ptr, i8* %add.ptr6, i8* %add.ptr9) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base.28"* @_ZNSt3__213__vector_baseIaNS_9allocatorIaEEED2Ev(%"class.std::__2::__vector_base.28"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__vector_base.28"*, align 4
  %this.addr = alloca %"class.std::__2::__vector_base.28"*, align 4
  store %"class.std::__2::__vector_base.28"* %this, %"class.std::__2::__vector_base.28"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.28"*, %"class.std::__2::__vector_base.28"** %this.addr, align 4
  store %"class.std::__2::__vector_base.28"* %this1, %"class.std::__2::__vector_base.28"** %retval, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.28", %"class.std::__2::__vector_base.28"* %this1, i32 0, i32 0
  %0 = load i8*, i8** %__begin_, align 4
  %cmp = icmp ne i8* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__213__vector_baseIaNS_9allocatorIaEEE5clearEv(%"class.std::__2::__vector_base.28"* %this1) #8
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.32"* @_ZNSt3__213__vector_baseIaNS_9allocatorIaEEE7__allocEv(%"class.std::__2::__vector_base.28"* %this1) #8
  %__begin_2 = getelementptr inbounds %"class.std::__2::__vector_base.28", %"class.std::__2::__vector_base.28"* %this1, i32 0, i32 0
  %1 = load i8*, i8** %__begin_2, align 4
  %call3 = call i32 @_ZNKSt3__213__vector_baseIaNS_9allocatorIaEEE8capacityEv(%"class.std::__2::__vector_base.28"* %this1) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIaEEE10deallocateERS2_Pam(%"class.std::__2::allocator.32"* nonnull align 1 dereferenceable(1) %call, i8* %1, i32 %call3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"class.std::__2::__vector_base.28"*, %"class.std::__2::__vector_base.28"** %retval, align 4
  ret %"class.std::__2::__vector_base.28"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIaEEE10deallocateERS2_Pam(%"class.std::__2::allocator.32"* nonnull align 1 dereferenceable(1) %__a, i8* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.32"*, align 4
  %__p.addr = alloca i8*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.32"* %__a, %"class.std::__2::allocator.32"** %__a.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.32"*, %"class.std::__2::allocator.32"** %__a.addr, align 4
  %1 = load i8*, i8** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorIaE10deallocateEPam(%"class.std::__2::allocator.32"* %0, i8* %1, i32 %2) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIaE10deallocateEPam(%"class.std::__2::allocator.32"* %this, i8* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.32"*, align 4
  %__p.addr = alloca i8*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.32"* %this, %"class.std::__2::allocator.32"** %this.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.32"*, %"class.std::__2::allocator.32"** %this.addr, align 4
  %0 = load i8*, i8** %__p.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %1, 1
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %0, i32 %mul, i32 1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIbNS_9allocatorIbEEE8capacityEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNKSt3__26vectorIbNS_9allocatorIbEEE5__capEv(%"class.std::__2::vector"* %this1) #8
  %0 = load i32, i32* %call, align 4
  %call2 = call i32 @_ZNSt3__26vectorIbNS_9allocatorIbEEE26__internal_cap_to_externalEm(i32 %0) #8
  ret i32 %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__26vectorIbNS_9allocatorIbEEE7__allocEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %__cap_alloc_ = getelementptr inbounds %"class.std::__2::vector", %"class.std::__2::vector"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__217__compressed_pairImNS_9allocatorImEEE6secondEv(%"class.std::__2::__compressed_pair"* %__cap_alloc_) #8
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::allocator.148"* @_ZNSt3__29allocatorIbEC2ImEERKNS0_IT_EE(%"class.std::__2::allocator.148"* returned %this, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %0) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.148"*, align 4
  %.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"class.std::__2::allocator.148"* %this, %"class.std::__2::allocator.148"** %this.addr, align 4
  store %"class.std::__2::allocator"* %0, %"class.std::__2::allocator"** %.addr, align 4
  %this1 = load %"class.std::__2::allocator.148"*, %"class.std::__2::allocator.148"** %this.addr, align 4
  ret %"class.std::__2::allocator.148"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector"* @_ZNSt3__26vectorIbNS_9allocatorIbEEEC2ERKS2_(%"class.std::__2::vector"* returned %this, %"class.std::__2::allocator.148"* nonnull align 1 dereferenceable(1) %__a) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__a.addr = alloca %"class.std::__2::allocator.148"*, align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp2 = alloca %"class.std::__2::allocator", align 1
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store %"class.std::__2::allocator.148"* %__a, %"class.std::__2::allocator.148"** %__a.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base_common"*
  %call = call %"class.std::__2::__vector_base_common"* @_ZNSt3__220__vector_base_commonILb1EEC2Ev(%"class.std::__2::__vector_base_common"* %0)
  %__begin_ = getelementptr inbounds %"class.std::__2::vector", %"class.std::__2::vector"* %this1, i32 0, i32 0
  store i32* null, i32** %__begin_, align 4
  %__size_ = getelementptr inbounds %"class.std::__2::vector", %"class.std::__2::vector"* %this1, i32 0, i32 1
  store i32 0, i32* %__size_, align 4
  %__cap_alloc_ = getelementptr inbounds %"class.std::__2::vector", %"class.std::__2::vector"* %this1, i32 0, i32 2
  store i32 0, i32* %ref.tmp, align 4
  %1 = load %"class.std::__2::allocator.148"*, %"class.std::__2::allocator.148"** %__a.addr, align 4
  %call3 = call %"class.std::__2::allocator"* @_ZNSt3__29allocatorImEC2IbEERKNS0_IT_EE(%"class.std::__2::allocator"* %ref.tmp2, %"class.std::__2::allocator.148"* nonnull align 1 dereferenceable(1) %1) #8
  %call4 = call %"class.std::__2::__compressed_pair"* @_ZNSt3__217__compressed_pairImNS_9allocatorImEEEC2IiS2_EEOT_OT0_(%"class.std::__2::__compressed_pair"* %__cap_alloc_, i32* nonnull align 4 dereferenceable(4) %ref.tmp, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  ret %"class.std::__2::vector"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIbNS_9allocatorIbEEE7reserveEm(%"class.std::__2::vector"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__n.addr = alloca i32, align 4
  %__v = alloca %"class.std::__2::vector", align 4
  %ref.tmp = alloca %"class.std::__2::allocator.148", align 1
  %agg.tmp = alloca %"class.std::__2::__bit_iterator", align 4
  %agg.tmp5 = alloca %"class.std::__2::__bit_iterator", align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIbNS_9allocatorIbEEE8capacityEv(%"class.std::__2::vector"* %this1) #8
  %cmp = icmp ugt i32 %0, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__26vectorIbNS_9allocatorIbEEE7__allocEv(%"class.std::__2::vector"* %this1) #8
  %call3 = call %"class.std::__2::allocator.148"* @_ZNSt3__29allocatorIbEC2ImEERKNS0_IT_EE(%"class.std::__2::allocator.148"* %ref.tmp, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call2) #8
  %call4 = call %"class.std::__2::vector"* @_ZNSt3__26vectorIbNS_9allocatorIbEEEC2ERKS2_(%"class.std::__2::vector"* %__v, %"class.std::__2::allocator.148"* nonnull align 1 dereferenceable(1) %ref.tmp) #8
  %1 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__26vectorIbNS_9allocatorIbEEE11__vallocateEm(%"class.std::__2::vector"* %__v, i32 %1)
  call void @_ZNSt3__26vectorIbNS_9allocatorIbEEE5beginEv(%"class.std::__2::__bit_iterator"* sret align 4 %agg.tmp, %"class.std::__2::vector"* %this1) #8
  call void @_ZNSt3__26vectorIbNS_9allocatorIbEEE3endEv(%"class.std::__2::__bit_iterator"* sret align 4 %agg.tmp5, %"class.std::__2::vector"* %this1) #8
  call void @_ZNSt3__26vectorIbNS_9allocatorIbEEE18__construct_at_endINS_14__bit_iteratorIS3_Lb0ELm0EEEEENS_9enable_ifIXsr27__is_cpp17_forward_iteratorIT_EE5valueEvE4typeES8_S8_(%"class.std::__2::vector"* %__v, %"class.std::__2::__bit_iterator"* %agg.tmp, %"class.std::__2::__bit_iterator"* %agg.tmp5)
  call void @_ZNSt3__26vectorIbNS_9allocatorIbEEE4swapERS3_(%"class.std::__2::vector"* %this1, %"class.std::__2::vector"* nonnull align 4 dereferenceable(12) %__v) #8
  call void @_ZNSt3__26vectorIbNS_9allocatorIbEEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector"* %this1)
  %call6 = call %"class.std::__2::vector"* @_ZNSt3__26vectorIbNS_9allocatorIbEEED2Ev(%"class.std::__2::vector"* %__v) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIbNS_9allocatorIbEEE11__recommendEm(%"class.std::__2::vector"* %this, i32 %__new_size) #0 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__new_size.addr = alloca i32, align 4
  %__ms = alloca i32, align 4
  %__cap = alloca i32, align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp6 = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__new_size, i32* %__new_size.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIbNS_9allocatorIbEEE8max_sizeEv(%"class.std::__2::vector"* %this1) #8
  store i32 %call, i32* %__ms, align 4
  %0 = load i32, i32* %__new_size.addr, align 4
  %1 = load i32, i32* %__ms, align 4
  %cmp = icmp ugt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base_common"*
  call void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"* %2) #10
  unreachable

if.end:                                           ; preds = %entry
  %call2 = call i32 @_ZNKSt3__26vectorIbNS_9allocatorIbEEE8capacityEv(%"class.std::__2::vector"* %this1) #8
  store i32 %call2, i32* %__cap, align 4
  %3 = load i32, i32* %__cap, align 4
  %4 = load i32, i32* %__ms, align 4
  %div = udiv i32 %4, 2
  %cmp3 = icmp uge i32 %3, %div
  br i1 %cmp3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %if.end
  %5 = load i32, i32* %__ms, align 4
  store i32 %5, i32* %retval, align 4
  br label %return

if.end5:                                          ; preds = %if.end
  %6 = load i32, i32* %__cap, align 4
  %mul = mul i32 2, %6
  store i32 %mul, i32* %ref.tmp, align 4
  %7 = load i32, i32* %__new_size.addr, align 4
  %call7 = call i32 @_ZNSt3__26vectorIbNS_9allocatorIbEEE10__align_itEm(i32 %7) #8
  store i32 %call7, i32* %ref.tmp6, align 4
  %call8 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %ref.tmp6)
  %8 = load i32, i32* %call8, align 4
  store i32 %8, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end5, %if.then4
  %9 = load i32, i32* %retval, align 4
  ret i32 %9
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIbNS_9allocatorIbEEE4swapERS3_(%"class.std::__2::vector"* %this, %"class.std::__2::vector"* nonnull align 4 dereferenceable(12) %__x) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__x.addr = alloca %"class.std::__2::vector"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant.146", align 1
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store %"class.std::__2::vector"* %__x, %"class.std::__2::vector"** %__x.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::vector", %"class.std::__2::vector"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__x.addr, align 4
  %__begin_2 = getelementptr inbounds %"class.std::__2::vector", %"class.std::__2::vector"* %0, i32 0, i32 0
  call void @_ZNSt3__24swapIPmEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_(i32** nonnull align 4 dereferenceable(4) %__begin_, i32** nonnull align 4 dereferenceable(4) %__begin_2) #8
  %__size_ = getelementptr inbounds %"class.std::__2::vector", %"class.std::__2::vector"* %this1, i32 0, i32 1
  %1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__x.addr, align 4
  %__size_3 = getelementptr inbounds %"class.std::__2::vector", %"class.std::__2::vector"* %1, i32 0, i32 1
  call void @_ZNSt3__24swapImEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS2_EE5valueEvE4typeERS2_S5_(i32* nonnull align 4 dereferenceable(4) %__size_, i32* nonnull align 4 dereferenceable(4) %__size_3) #8
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIbNS_9allocatorIbEEE5__capEv(%"class.std::__2::vector"* %this1) #8
  %2 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__x.addr, align 4
  %call4 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIbNS_9allocatorIbEEE5__capEv(%"class.std::__2::vector"* %2) #8
  call void @_ZNSt3__24swapImEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS2_EE5valueEvE4typeERS2_S5_(i32* nonnull align 4 dereferenceable(4) %call, i32* nonnull align 4 dereferenceable(4) %call4) #8
  %call5 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__26vectorIbNS_9allocatorIbEEE7__allocEv(%"class.std::__2::vector"* %this1) #8
  %3 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__x.addr, align 4
  %call6 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__26vectorIbNS_9allocatorIbEEE7__allocEv(%"class.std::__2::vector"* %3) #8
  call void @_ZNSt3__216__swap_allocatorINS_9allocatorImEEEEvRT_S4_NS_17integral_constantIbLb0EEE(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call5, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call6) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector"* @_ZNSt3__26vectorIbNS_9allocatorIbEEED2Ev(%"class.std::__2::vector"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"class.std::__2::vector"*, align 4
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  store %"class.std::__2::vector"* %this1, %"class.std::__2::vector"** %retval, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::vector", %"class.std::__2::vector"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__begin_, align 4
  %cmp = icmp ne i32* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__26vectorIbNS_9allocatorIbEEE7__allocEv(%"class.std::__2::vector"* %this1) #8
  %__begin_2 = getelementptr inbounds %"class.std::__2::vector", %"class.std::__2::vector"* %this1, i32 0, i32 0
  %1 = load i32*, i32** %__begin_2, align 4
  %call3 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIbNS_9allocatorIbEEE5__capEv(%"class.std::__2::vector"* %this1) #8
  %2 = load i32, i32* %call3, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorImEEE10deallocateERS2_Pmm(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call, i32* %1, i32 %2) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  call void @_ZNSt3__26vectorIbNS_9allocatorIbEEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector"* %this1)
  %3 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %retval, align 4
  ret %"class.std::__2::vector"* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26fill_nINS_6vectorIbNS_9allocatorIbEEEEEEvNS_14__bit_iteratorIT_Lb0EXLi0EEEENS6_9size_typeEb(%"class.std::__2::__bit_iterator"* %__first, i32 %__n, i1 zeroext %__value_) #0 comdat {
entry:
  %__n.addr = alloca i32, align 4
  %__value_.addr = alloca i8, align 1
  %agg.tmp = alloca %"class.std::__2::__bit_iterator", align 4
  %agg.tmp2 = alloca %"class.std::__2::__bit_iterator", align 4
  store i32 %__n, i32* %__n.addr, align 4
  %frombool = zext i1 %__value_ to i8
  store i8 %frombool, i8* %__value_.addr, align 1
  %0 = load i32, i32* %__n.addr, align 4
  %cmp = icmp ugt i32 %0, 0
  br i1 %cmp, label %if.then, label %if.end4

if.then:                                          ; preds = %entry
  %1 = load i8, i8* %__value_.addr, align 1
  %tobool = trunc i8 %1 to i1
  br i1 %tobool, label %if.then1, label %if.else

if.then1:                                         ; preds = %if.then
  %call = call %"class.std::__2::__bit_iterator"* @_ZNSt3__214__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb0ELm0EEC2ERKS5_(%"class.std::__2::__bit_iterator"* %agg.tmp, %"class.std::__2::__bit_iterator"* nonnull align 4 dereferenceable(8) %__first) #8
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__213__fill_n_trueINS_6vectorIbNS_9allocatorIbEEEEEEvNS_14__bit_iteratorIT_Lb0EXLi0EEEENS6_9size_typeE(%"class.std::__2::__bit_iterator"* %agg.tmp, i32 %2)
  br label %if.end

if.else:                                          ; preds = %if.then
  %call3 = call %"class.std::__2::__bit_iterator"* @_ZNSt3__214__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb0ELm0EEC2ERKS5_(%"class.std::__2::__bit_iterator"* %agg.tmp2, %"class.std::__2::__bit_iterator"* nonnull align 4 dereferenceable(8) %__first) #8
  %3 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__214__fill_n_falseINS_6vectorIbNS_9allocatorIbEEEEEEvNS_14__bit_iteratorIT_Lb0EXLi0EEEENS6_9size_typeE(%"class.std::__2::__bit_iterator"* %agg.tmp2, i32 %3)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then1
  br label %if.end4

if.end4:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIbNS_9allocatorIbEEE5beginEv(%"class.std::__2::__bit_iterator"* noalias sret align 4 %agg.result, %"class.std::__2::vector"* %this) #0 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %0 = bitcast %"class.std::__2::__bit_iterator"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  call void @_ZNSt3__26vectorIbNS_9allocatorIbEEE11__make_iterEm(%"class.std::__2::__bit_iterator"* sret align 4 %agg.result, %"class.std::__2::vector"* %this1, i32 0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIbNS_9allocatorIbEEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__26vectorIbNS_9allocatorIbEEE26__internal_cap_to_externalEm(i32 %__n) #0 comdat {
entry:
  %__n.addr = alloca i32, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %0, 32
  ret i32 %mul
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNKSt3__26vectorIbNS_9allocatorIbEEE5__capEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %__cap_alloc_ = getelementptr inbounds %"class.std::__2::vector", %"class.std::__2::vector"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNKSt3__217__compressed_pairImNS_9allocatorImEEE5firstEv(%"class.std::__2::__compressed_pair"* %__cap_alloc_) #8
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNKSt3__217__compressed_pairImNS_9allocatorImEEE5firstEv(%"class.std::__2::__compressed_pair"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNKSt3__222__compressed_pair_elemImLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %0) #8
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNKSt3__222__compressed_pair_elemImLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem"*, align 4
  store %"struct.std::__2::__compressed_pair_elem"* %this, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem"*, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem"* %this1, i32 0, i32 0
  ret i32* %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__217__compressed_pairImNS_9allocatorImEEE6secondEv(%"class.std::__2::__compressed_pair"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem.0"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorImEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.0"* %0) #8
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorImEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.0"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.0"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.0"* %this, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.0"*, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.0"* %this1 to %"class.std::__2::allocator"*
  ret %"class.std::__2::allocator"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::allocator"* @_ZNSt3__29allocatorImEC2IbEERKNS0_IT_EE(%"class.std::__2::allocator"* returned %this, %"class.std::__2::allocator.148"* nonnull align 1 dereferenceable(1) %0) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  %.addr = alloca %"class.std::__2::allocator.148"*, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  store %"class.std::__2::allocator.148"* %0, %"class.std::__2::allocator.148"** %.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  ret %"class.std::__2::allocator"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair"* @_ZNSt3__217__compressed_pairImNS_9allocatorImEEEC2IiS2_EEOT_OT0_(%"class.std::__2::__compressed_pair"* returned %this, i32* nonnull align 4 dereferenceable(4) %__t1, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  %__t1.addr = alloca i32*, align 4
  %__t2.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  store i32* %__t1, i32** %__t1.addr, align 4
  store %"class.std::__2::allocator"* %__t2, %"class.std::__2::allocator"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %1 = load i32*, i32** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__27forwardIiEEOT_RNS_16remove_referenceIS1_E4typeE(i32* nonnull align 4 dereferenceable(4) %1) #8
  %call2 = call %"struct.std::__2::__compressed_pair_elem"* @_ZNSt3__222__compressed_pair_elemImLi0ELb0EEC2IivEEOT_(%"struct.std::__2::__compressed_pair_elem"* %0, i32* nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem.0"*
  %3 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__27forwardINS_9allocatorImEEEEOT_RNS_16remove_referenceIS3_E4typeE(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %3) #8
  %call4 = call %"struct.std::__2::__compressed_pair_elem.0"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorImEELi1ELb1EEC2IS2_vEEOT_(%"struct.std::__2::__compressed_pair_elem.0"* %2, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::__compressed_pair"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__27forwardINS_9allocatorImEEEEOT_RNS_16remove_referenceIS3_E4typeE(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"class.std::__2::allocator"* %__t, %"class.std::__2::allocator"** %__t.addr, align 4
  %0 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__t.addr, align 4
  ret %"class.std::__2::allocator"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.0"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorImEELi1ELb1EEC2IS2_vEEOT_(%"struct.std::__2::__compressed_pair_elem.0"* returned %this, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.0"*, align 4
  %__u.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.0"* %this, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  store %"class.std::__2::allocator"* %__u, %"class.std::__2::allocator"** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.0"*, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.0"* %this1 to %"class.std::__2::allocator"*
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__u.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__27forwardINS_9allocatorImEEEEOT_RNS_16remove_referenceIS3_E4typeE(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %1) #8
  ret %"struct.std::__2::__compressed_pair_elem.0"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIbNS_9allocatorIbEEE11__vallocateEm(%"class.std::__2::vector"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIbNS_9allocatorIbEEE8max_sizeEv(%"class.std::__2::vector"* %this1) #8
  %cmp = icmp ugt i32 %0, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base_common"*
  call void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"* %1) #10
  unreachable

if.end:                                           ; preds = %entry
  %2 = load i32, i32* %__n.addr, align 4
  %call2 = call i32 @_ZNSt3__26vectorIbNS_9allocatorIbEEE26__external_cap_to_internalEm(i32 %2) #8
  store i32 %call2, i32* %__n.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__26vectorIbNS_9allocatorIbEEE7__allocEv(%"class.std::__2::vector"* %this1) #8
  %3 = load i32, i32* %__n.addr, align 4
  %call4 = call i32* @_ZNSt3__216allocator_traitsINS_9allocatorImEEE8allocateERS2_m(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call3, i32 %3)
  %__begin_ = getelementptr inbounds %"class.std::__2::vector", %"class.std::__2::vector"* %this1, i32 0, i32 0
  store i32* %call4, i32** %__begin_, align 4
  %__size_ = getelementptr inbounds %"class.std::__2::vector", %"class.std::__2::vector"* %this1, i32 0, i32 1
  store i32 0, i32* %__size_, align 4
  %4 = load i32, i32* %__n.addr, align 4
  %call5 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIbNS_9allocatorIbEEE5__capEv(%"class.std::__2::vector"* %this1) #8
  store i32 %4, i32* %call5, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIbNS_9allocatorIbEEE18__construct_at_endINS_14__bit_iteratorIS3_Lb0ELm0EEEEENS_9enable_ifIXsr27__is_cpp17_forward_iteratorIT_EE5valueEvE4typeES8_S8_(%"class.std::__2::vector"* %this, %"class.std::__2::__bit_iterator"* %__first, %"class.std::__2::__bit_iterator"* %__last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__old_size = alloca i32, align 4
  %agg.tmp = alloca %"class.std::__2::__bit_iterator", align 4
  %agg.tmp2 = alloca %"class.std::__2::__bit_iterator", align 4
  %agg.tmp19 = alloca %"class.std::__2::__bit_iterator", align 4
  %agg.tmp21 = alloca %"class.std::__2::__bit_iterator", align 4
  %agg.tmp23 = alloca %"class.std::__2::__bit_iterator", align 4
  %tmp = alloca %"class.std::__2::__bit_iterator", align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %__size_ = getelementptr inbounds %"class.std::__2::vector", %"class.std::__2::vector"* %this1, i32 0, i32 1
  %0 = load i32, i32* %__size_, align 4
  store i32 %0, i32* %__old_size, align 4
  %call = call %"class.std::__2::__bit_iterator"* @_ZNSt3__214__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb0ELm0EEC2ERKS5_(%"class.std::__2::__bit_iterator"* %agg.tmp, %"class.std::__2::__bit_iterator"* nonnull align 4 dereferenceable(8) %__first) #8
  %call3 = call %"class.std::__2::__bit_iterator"* @_ZNSt3__214__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb0ELm0EEC2ERKS5_(%"class.std::__2::__bit_iterator"* %agg.tmp2, %"class.std::__2::__bit_iterator"* nonnull align 4 dereferenceable(8) %__last) #8
  %call4 = call i32 @_ZNSt3__28distanceINS_14__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb0ELm0EEEEENS_15iterator_traitsIT_E15difference_typeES8_S8_(%"class.std::__2::__bit_iterator"* %agg.tmp, %"class.std::__2::__bit_iterator"* %agg.tmp2)
  %__size_5 = getelementptr inbounds %"class.std::__2::vector", %"class.std::__2::vector"* %this1, i32 0, i32 1
  %1 = load i32, i32* %__size_5, align 4
  %add = add i32 %1, %call4
  store i32 %add, i32* %__size_5, align 4
  %2 = load i32, i32* %__old_size, align 4
  %cmp = icmp eq i32 %2, 0
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %3 = load i32, i32* %__old_size, align 4
  %sub = sub i32 %3, 1
  %div = udiv i32 %sub, 32
  %__size_6 = getelementptr inbounds %"class.std::__2::vector", %"class.std::__2::vector"* %this1, i32 0, i32 1
  %4 = load i32, i32* %__size_6, align 4
  %sub7 = sub i32 %4, 1
  %div8 = udiv i32 %sub7, 32
  %cmp9 = icmp ne i32 %div, %div8
  br i1 %cmp9, label %if.then, label %if.end18

if.then:                                          ; preds = %lor.lhs.false, %entry
  %__size_10 = getelementptr inbounds %"class.std::__2::vector", %"class.std::__2::vector"* %this1, i32 0, i32 1
  %5 = load i32, i32* %__size_10, align 4
  %cmp11 = icmp ule i32 %5, 32
  br i1 %cmp11, label %if.then12, label %if.else

if.then12:                                        ; preds = %if.then
  %__begin_ = getelementptr inbounds %"class.std::__2::vector", %"class.std::__2::vector"* %this1, i32 0, i32 0
  %6 = load i32*, i32** %__begin_, align 4
  %arrayidx = getelementptr inbounds i32, i32* %6, i32 0
  store i32 0, i32* %arrayidx, align 4
  br label %if.end

if.else:                                          ; preds = %if.then
  %__begin_13 = getelementptr inbounds %"class.std::__2::vector", %"class.std::__2::vector"* %this1, i32 0, i32 0
  %7 = load i32*, i32** %__begin_13, align 4
  %__size_14 = getelementptr inbounds %"class.std::__2::vector", %"class.std::__2::vector"* %this1, i32 0, i32 1
  %8 = load i32, i32* %__size_14, align 4
  %sub15 = sub i32 %8, 1
  %div16 = udiv i32 %sub15, 32
  %arrayidx17 = getelementptr inbounds i32, i32* %7, i32 %div16
  store i32 0, i32* %arrayidx17, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then12
  br label %if.end18

if.end18:                                         ; preds = %if.end, %lor.lhs.false
  %call20 = call %"class.std::__2::__bit_iterator"* @_ZNSt3__214__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb0ELm0EEC2ERKS5_(%"class.std::__2::__bit_iterator"* %agg.tmp19, %"class.std::__2::__bit_iterator"* nonnull align 4 dereferenceable(8) %__first) #8
  %call22 = call %"class.std::__2::__bit_iterator"* @_ZNSt3__214__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb0ELm0EEC2ERKS5_(%"class.std::__2::__bit_iterator"* %agg.tmp21, %"class.std::__2::__bit_iterator"* nonnull align 4 dereferenceable(8) %__last) #8
  %9 = load i32, i32* %__old_size, align 4
  call void @_ZNSt3__26vectorIbNS_9allocatorIbEEE11__make_iterEm(%"class.std::__2::__bit_iterator"* sret align 4 %agg.tmp23, %"class.std::__2::vector"* %this1, i32 %9) #8
  call void @_ZNSt3__24copyINS_6vectorIbNS_9allocatorIbEEEELb0EEENS_14__bit_iteratorIT_Lb0EXLi0EEEENS5_IS6_XT0_EXLi0EEEES8_S7_(%"class.std::__2::__bit_iterator"* sret align 4 %tmp, %"class.std::__2::__bit_iterator"* %agg.tmp19, %"class.std::__2::__bit_iterator"* %agg.tmp21, %"class.std::__2::__bit_iterator"* %agg.tmp23)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIbNS_9allocatorIbEEE3endEv(%"class.std::__2::__bit_iterator"* noalias sret align 4 %agg.result, %"class.std::__2::vector"* %this) #0 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %0 = bitcast %"class.std::__2::__bit_iterator"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %__size_ = getelementptr inbounds %"class.std::__2::vector", %"class.std::__2::vector"* %this1, i32 0, i32 1
  %1 = load i32, i32* %__size_, align 4
  call void @_ZNSt3__26vectorIbNS_9allocatorIbEEE11__make_iterEm(%"class.std::__2::__bit_iterator"* sret align 4 %agg.result, %"class.std::__2::vector"* %this1, i32 %1) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIbNS_9allocatorIbEEE8max_sizeEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__amax = alloca i32, align 4
  %__nmax = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNKSt3__26vectorIbNS_9allocatorIbEEE7__allocEv(%"class.std::__2::vector"* %this1) #8
  %call2 = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorImEEE8max_sizeERKS2_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call) #8
  store i32 %call2, i32* %__amax, align 4
  %call3 = call i32 @_ZNSt3__214numeric_limitsImE3maxEv() #8
  %div = udiv i32 %call3, 2
  store i32 %div, i32* %__nmax, align 4
  %0 = load i32, i32* %__nmax, align 4
  %div4 = udiv i32 %0, 32
  %1 = load i32, i32* %__amax, align 4
  %cmp = icmp ule i32 %div4, %1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %__nmax, align 4
  store i32 %2, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %3 = load i32, i32* %__amax, align 4
  %call5 = call i32 @_ZNSt3__26vectorIbNS_9allocatorIbEEE26__internal_cap_to_externalEm(i32 %3) #8
  store i32 %call5, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %4 = load i32, i32* %retval, align 4
  ret i32 %4
}

; Function Attrs: noreturn
declare void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"*) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__26vectorIbNS_9allocatorIbEEE26__external_cap_to_internalEm(i32 %__n) #0 comdat {
entry:
  %__n.addr = alloca i32, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %sub = sub i32 %0, 1
  %div = udiv i32 %sub, 32
  %add = add i32 %div, 1
  ret i32 %add
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNSt3__216allocator_traitsINS_9allocatorImEEE8allocateERS2_m(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call i32* @_ZNSt3__29allocatorImE8allocateEmPKv(%"class.std::__2::allocator"* %0, i32 %1, i8* null)
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIbNS_9allocatorIbEEE5__capEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %__cap_alloc_ = getelementptr inbounds %"class.std::__2::vector", %"class.std::__2::vector"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__217__compressed_pairImNS_9allocatorImEEE5firstEv(%"class.std::__2::__compressed_pair"* %__cap_alloc_) #8
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorImEEE8max_sizeERKS2_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_max_size", align 1
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_max_size"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %call = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorImEEE10__max_sizeENS_17integral_constantIbLb1EEERKS2_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %1) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNKSt3__26vectorIbNS_9allocatorIbEEE7__allocEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %__cap_alloc_ = getelementptr inbounds %"class.std::__2::vector", %"class.std::__2::vector"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNKSt3__217__compressed_pairImNS_9allocatorImEEE6secondEv(%"class.std::__2::__compressed_pair"* %__cap_alloc_) #8
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__214numeric_limitsImE3maxEv() #0 comdat {
entry:
  %call = call i32 @_ZNSt3__223__libcpp_numeric_limitsImLb1EE3maxEv() #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorImEEE10__max_sizeENS_17integral_constantIbLb1EEERKS2_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorImE8max_sizeEv(%"class.std::__2::allocator"* %1) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__29allocatorImE8max_sizeEv(%"class.std::__2::allocator"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  ret i32 1073741823
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNKSt3__217__compressed_pairImNS_9allocatorImEEE6secondEv(%"class.std::__2::__compressed_pair"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem.0"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorImEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.0"* %0) #8
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorImEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.0"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.0"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.0"* %this, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.0"*, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.0"* %this1 to %"class.std::__2::allocator"*
  ret %"class.std::__2::allocator"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__223__libcpp_numeric_limitsImLb1EE3maxEv() #0 comdat {
entry:
  ret i32 -1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNSt3__29allocatorImE8allocateEmPKv(%"class.std::__2::allocator"* %this, i32 %__n, i8* %0) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  %__n.addr = alloca i32, align 4
  %.addr = alloca i8*, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorImE8max_sizeEv(%"class.std::__2::allocator"* %this1) #8
  %cmp = icmp ugt i32 %1, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__220__throw_length_errorEPKc(i8* getelementptr inbounds ([68 x i8], [68 x i8]* @.str, i32 0, i32 0)) #10
  unreachable

if.end:                                           ; preds = %entry
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  %call2 = call i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %mul, i32 4)
  %3 = bitcast i8* %call2 to i32*
  ret i32* %3
}

; Function Attrs: noinline noreturn nounwind optnone
define linkonce_odr hidden void @_ZNSt3__220__throw_length_errorEPKc(i8* %__msg) #5 comdat {
entry:
  %__msg.addr = alloca i8*, align 4
  store i8* %__msg, i8** %__msg.addr, align 4
  call void @abort() #10
  unreachable
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %__size, i32 %__align) #0 comdat {
entry:
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i32, i32* %__size.addr, align 4
  %call = call noalias nonnull i8* @_Znwm(i32 %0) #11
  ret i8* %call
}

; Function Attrs: noreturn
declare void @abort() #4

; Function Attrs: nobuiltin allocsize(0)
declare nonnull i8* @_Znwm(i32) #6

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__217__compressed_pairImNS_9allocatorImEEE5firstEv(%"class.std::__2::__compressed_pair"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__222__compressed_pair_elemImLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %0) #8
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__222__compressed_pair_elemImLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem"*, align 4
  store %"struct.std::__2::__compressed_pair_elem"* %this, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem"*, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem"* %this1, i32 0, i32 0
  ret i32* %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__28distanceINS_14__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb0ELm0EEEEENS_15iterator_traitsIT_E15difference_typeES8_S8_(%"class.std::__2::__bit_iterator"* %__first, %"class.std::__2::__bit_iterator"* %__last) #0 comdat {
entry:
  %agg.tmp = alloca %"class.std::__2::__bit_iterator", align 4
  %agg.tmp1 = alloca %"class.std::__2::__bit_iterator", align 4
  %agg.tmp3 = alloca %"struct.std::__2::random_access_iterator_tag", align 1
  %call = call %"class.std::__2::__bit_iterator"* @_ZNSt3__214__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb0ELm0EEC2ERKS5_(%"class.std::__2::__bit_iterator"* %agg.tmp, %"class.std::__2::__bit_iterator"* nonnull align 4 dereferenceable(8) %__first) #8
  %call2 = call %"class.std::__2::__bit_iterator"* @_ZNSt3__214__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb0ELm0EEC2ERKS5_(%"class.std::__2::__bit_iterator"* %agg.tmp1, %"class.std::__2::__bit_iterator"* nonnull align 4 dereferenceable(8) %__last) #8
  %call4 = call i32 @_ZNSt3__210__distanceINS_14__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb0ELm0EEEEENS_15iterator_traitsIT_E15difference_typeES8_S8_NS_26random_access_iterator_tagE(%"class.std::__2::__bit_iterator"* %agg.tmp, %"class.std::__2::__bit_iterator"* %agg.tmp1)
  ret i32 %call4
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__bit_iterator"* @_ZNSt3__214__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb0ELm0EEC2ERKS5_(%"class.std::__2::__bit_iterator"* returned %this, %"class.std::__2::__bit_iterator"* nonnull align 4 dereferenceable(8) %__it) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__bit_iterator"*, align 4
  %__it.addr = alloca %"class.std::__2::__bit_iterator"*, align 4
  store %"class.std::__2::__bit_iterator"* %this, %"class.std::__2::__bit_iterator"** %this.addr, align 4
  store %"class.std::__2::__bit_iterator"* %__it, %"class.std::__2::__bit_iterator"** %__it.addr, align 4
  %this1 = load %"class.std::__2::__bit_iterator"*, %"class.std::__2::__bit_iterator"** %this.addr, align 4
  %__seg_ = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::__bit_iterator"*, %"class.std::__2::__bit_iterator"** %__it.addr, align 4
  %__seg_2 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %0, i32 0, i32 0
  %1 = load i32*, i32** %__seg_2, align 4
  store i32* %1, i32** %__seg_, align 4
  %__ctz_ = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %this1, i32 0, i32 1
  %2 = load %"class.std::__2::__bit_iterator"*, %"class.std::__2::__bit_iterator"** %__it.addr, align 4
  %__ctz_3 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %2, i32 0, i32 1
  %3 = load i32, i32* %__ctz_3, align 4
  store i32 %3, i32* %__ctz_, align 4
  ret %"class.std::__2::__bit_iterator"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__24copyINS_6vectorIbNS_9allocatorIbEEEELb0EEENS_14__bit_iteratorIT_Lb0EXLi0EEEENS5_IS6_XT0_EXLi0EEEES8_S7_(%"class.std::__2::__bit_iterator"* noalias sret align 4 %agg.result, %"class.std::__2::__bit_iterator"* %__first, %"class.std::__2::__bit_iterator"* %__last, %"class.std::__2::__bit_iterator"* %__result) #0 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %agg.tmp = alloca %"class.std::__2::__bit_iterator", align 4
  %agg.tmp2 = alloca %"class.std::__2::__bit_iterator", align 4
  %agg.tmp4 = alloca %"class.std::__2::__bit_iterator", align 4
  %agg.tmp6 = alloca %"class.std::__2::__bit_iterator", align 4
  %agg.tmp8 = alloca %"class.std::__2::__bit_iterator", align 4
  %agg.tmp10 = alloca %"class.std::__2::__bit_iterator", align 4
  %0 = bitcast %"class.std::__2::__bit_iterator"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  %__ctz_ = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__first, i32 0, i32 1
  %1 = load i32, i32* %__ctz_, align 4
  %__ctz_1 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 1
  %2 = load i32, i32* %__ctz_1, align 4
  %cmp = icmp eq i32 %1, %2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call = call %"class.std::__2::__bit_iterator"* @_ZNSt3__214__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb0ELm0EEC2ERKS5_(%"class.std::__2::__bit_iterator"* %agg.tmp, %"class.std::__2::__bit_iterator"* nonnull align 4 dereferenceable(8) %__first) #8
  %call3 = call %"class.std::__2::__bit_iterator"* @_ZNSt3__214__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb0ELm0EEC2ERKS5_(%"class.std::__2::__bit_iterator"* %agg.tmp2, %"class.std::__2::__bit_iterator"* nonnull align 4 dereferenceable(8) %__last) #8
  %call5 = call %"class.std::__2::__bit_iterator"* @_ZNSt3__214__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb0ELm0EEC2ERKS5_(%"class.std::__2::__bit_iterator"* %agg.tmp4, %"class.std::__2::__bit_iterator"* nonnull align 4 dereferenceable(8) %__result) #8
  call void @_ZNSt3__214__copy_alignedINS_6vectorIbNS_9allocatorIbEEEELb0EEENS_14__bit_iteratorIT_Lb0EXLi0EEEENS5_IS6_XT0_EXLi0EEEES8_S7_(%"class.std::__2::__bit_iterator"* sret align 4 %agg.result, %"class.std::__2::__bit_iterator"* %agg.tmp, %"class.std::__2::__bit_iterator"* %agg.tmp2, %"class.std::__2::__bit_iterator"* %agg.tmp4)
  br label %return

if.end:                                           ; preds = %entry
  %call7 = call %"class.std::__2::__bit_iterator"* @_ZNSt3__214__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb0ELm0EEC2ERKS5_(%"class.std::__2::__bit_iterator"* %agg.tmp6, %"class.std::__2::__bit_iterator"* nonnull align 4 dereferenceable(8) %__first) #8
  %call9 = call %"class.std::__2::__bit_iterator"* @_ZNSt3__214__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb0ELm0EEC2ERKS5_(%"class.std::__2::__bit_iterator"* %agg.tmp8, %"class.std::__2::__bit_iterator"* nonnull align 4 dereferenceable(8) %__last) #8
  %call11 = call %"class.std::__2::__bit_iterator"* @_ZNSt3__214__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb0ELm0EEC2ERKS5_(%"class.std::__2::__bit_iterator"* %agg.tmp10, %"class.std::__2::__bit_iterator"* nonnull align 4 dereferenceable(8) %__result) #8
  call void @_ZNSt3__216__copy_unalignedINS_6vectorIbNS_9allocatorIbEEEELb0EEENS_14__bit_iteratorIT_Lb0EXLi0EEEENS5_IS6_XT0_EXLi0EEEES8_S7_(%"class.std::__2::__bit_iterator"* sret align 4 %agg.result, %"class.std::__2::__bit_iterator"* %agg.tmp6, %"class.std::__2::__bit_iterator"* %agg.tmp8, %"class.std::__2::__bit_iterator"* %agg.tmp10)
  br label %return

return:                                           ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIbNS_9allocatorIbEEE11__make_iterEm(%"class.std::__2::__bit_iterator"* noalias sret align 4 %agg.result, %"class.std::__2::vector"* %this, i32 %__pos) #0 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__pos.addr = alloca i32, align 4
  %0 = bitcast %"class.std::__2::__bit_iterator"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__pos, i32* %__pos.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::vector", %"class.std::__2::vector"* %this1, i32 0, i32 0
  %1 = load i32*, i32** %__begin_, align 4
  %2 = load i32, i32* %__pos.addr, align 4
  %div = udiv i32 %2, 32
  %add.ptr = getelementptr inbounds i32, i32* %1, i32 %div
  %3 = load i32, i32* %__pos.addr, align 4
  %rem = urem i32 %3, 32
  %call = call %"class.std::__2::__bit_iterator"* @_ZNSt3__214__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb0ELm0EEC2EPmj(%"class.std::__2::__bit_iterator"* %agg.result, i32* %add.ptr, i32 %rem) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__210__distanceINS_14__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb0ELm0EEEEENS_15iterator_traitsIT_E15difference_typeES8_S8_NS_26random_access_iterator_tagE(%"class.std::__2::__bit_iterator"* %__first, %"class.std::__2::__bit_iterator"* %__last) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::random_access_iterator_tag", align 1
  %call = call i32 @_ZNSt3__2miERKNS_14__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb0ELm0EEES7_(%"class.std::__2::__bit_iterator"* nonnull align 4 dereferenceable(8) %__last, %"class.std::__2::__bit_iterator"* nonnull align 4 dereferenceable(8) %__first)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__2miERKNS_14__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb0ELm0EEES7_(%"class.std::__2::__bit_iterator"* nonnull align 4 dereferenceable(8) %__x, %"class.std::__2::__bit_iterator"* nonnull align 4 dereferenceable(8) %__y) #0 comdat {
entry:
  %__x.addr = alloca %"class.std::__2::__bit_iterator"*, align 4
  %__y.addr = alloca %"class.std::__2::__bit_iterator"*, align 4
  store %"class.std::__2::__bit_iterator"* %__x, %"class.std::__2::__bit_iterator"** %__x.addr, align 4
  store %"class.std::__2::__bit_iterator"* %__y, %"class.std::__2::__bit_iterator"** %__y.addr, align 4
  %0 = load %"class.std::__2::__bit_iterator"*, %"class.std::__2::__bit_iterator"** %__x.addr, align 4
  %__seg_ = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %0, i32 0, i32 0
  %1 = load i32*, i32** %__seg_, align 4
  %2 = load %"class.std::__2::__bit_iterator"*, %"class.std::__2::__bit_iterator"** %__y.addr, align 4
  %__seg_1 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %2, i32 0, i32 0
  %3 = load i32*, i32** %__seg_1, align 4
  %sub.ptr.lhs.cast = ptrtoint i32* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint i32* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  %mul = mul i32 %sub.ptr.div, 32
  %4 = load %"class.std::__2::__bit_iterator"*, %"class.std::__2::__bit_iterator"** %__x.addr, align 4
  %__ctz_ = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %4, i32 0, i32 1
  %5 = load i32, i32* %__ctz_, align 4
  %add = add i32 %mul, %5
  %6 = load %"class.std::__2::__bit_iterator"*, %"class.std::__2::__bit_iterator"** %__y.addr, align 4
  %__ctz_2 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %6, i32 0, i32 1
  %7 = load i32, i32* %__ctz_2, align 4
  %sub = sub i32 %add, %7
  ret i32 %sub
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__copy_alignedINS_6vectorIbNS_9allocatorIbEEEELb0EEENS_14__bit_iteratorIT_Lb0EXLi0EEEENS5_IS6_XT0_EXLi0EEEES8_S7_(%"class.std::__2::__bit_iterator"* noalias sret align 4 %agg.result, %"class.std::__2::__bit_iterator"* %__first, %"class.std::__2::__bit_iterator"* %__last, %"class.std::__2::__bit_iterator"* %__result) #0 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %__bits_per_word = alloca i32, align 4
  %__n = alloca i32, align 4
  %__clz = alloca i32, align 4
  %__dn = alloca i32, align 4
  %ref.tmp = alloca i32, align 4
  %__m = alloca i32, align 4
  %__b = alloca i32, align 4
  %__nw = alloca i32, align 4
  %__m31 = alloca i32, align 4
  %__b34 = alloca i32, align 4
  %0 = bitcast %"class.std::__2::__bit_iterator"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store i32 32, i32* %__bits_per_word, align 4
  %call = call i32 @_ZNSt3__2miERKNS_14__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb0ELm0EEES7_(%"class.std::__2::__bit_iterator"* nonnull align 4 dereferenceable(8) %__last, %"class.std::__2::__bit_iterator"* nonnull align 4 dereferenceable(8) %__first)
  store i32 %call, i32* %__n, align 4
  %1 = load i32, i32* %__n, align 4
  %cmp = icmp sgt i32 %1, 0
  br i1 %cmp, label %if.then, label %if.end44

if.then:                                          ; preds = %entry
  %__ctz_ = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__first, i32 0, i32 1
  %2 = load i32, i32* %__ctz_, align 4
  %cmp1 = icmp ne i32 %2, 0
  br i1 %cmp1, label %if.then2, label %if.end

if.then2:                                         ; preds = %if.then
  %__ctz_3 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__first, i32 0, i32 1
  %3 = load i32, i32* %__ctz_3, align 4
  %sub = sub i32 32, %3
  store i32 %sub, i32* %__clz, align 4
  %4 = load i32, i32* %__clz, align 4
  store i32 %4, i32* %ref.tmp, align 4
  %call4 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minIlEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %__n)
  %5 = load i32, i32* %call4, align 4
  store i32 %5, i32* %__dn, align 4
  %6 = load i32, i32* %__dn, align 4
  %7 = load i32, i32* %__n, align 4
  %sub5 = sub nsw i32 %7, %6
  store i32 %sub5, i32* %__n, align 4
  %__ctz_6 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__first, i32 0, i32 1
  %8 = load i32, i32* %__ctz_6, align 4
  %shl = shl i32 -1, %8
  %9 = load i32, i32* %__clz, align 4
  %10 = load i32, i32* %__dn, align 4
  %sub7 = sub i32 %9, %10
  %shr = lshr i32 -1, %sub7
  %and = and i32 %shl, %shr
  store i32 %and, i32* %__m, align 4
  %__seg_ = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__first, i32 0, i32 0
  %11 = load i32*, i32** %__seg_, align 4
  %12 = load i32, i32* %11, align 4
  %13 = load i32, i32* %__m, align 4
  %and8 = and i32 %12, %13
  store i32 %and8, i32* %__b, align 4
  %14 = load i32, i32* %__m, align 4
  %neg = xor i32 %14, -1
  %__seg_9 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 0
  %15 = load i32*, i32** %__seg_9, align 4
  %16 = load i32, i32* %15, align 4
  %and10 = and i32 %16, %neg
  store i32 %and10, i32* %15, align 4
  %17 = load i32, i32* %__b, align 4
  %__seg_11 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 0
  %18 = load i32*, i32** %__seg_11, align 4
  %19 = load i32, i32* %18, align 4
  %or = or i32 %19, %17
  store i32 %or, i32* %18, align 4
  %20 = load i32, i32* %__dn, align 4
  %__ctz_12 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 1
  %21 = load i32, i32* %__ctz_12, align 4
  %add = add i32 %20, %21
  %div = udiv i32 %add, 32
  %__seg_13 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 0
  %22 = load i32*, i32** %__seg_13, align 4
  %add.ptr = getelementptr inbounds i32, i32* %22, i32 %div
  store i32* %add.ptr, i32** %__seg_13, align 4
  %23 = load i32, i32* %__dn, align 4
  %__ctz_14 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 1
  %24 = load i32, i32* %__ctz_14, align 4
  %add15 = add i32 %23, %24
  %rem = urem i32 %add15, 32
  %__ctz_16 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 1
  store i32 %rem, i32* %__ctz_16, align 4
  %__seg_17 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__first, i32 0, i32 0
  %25 = load i32*, i32** %__seg_17, align 4
  %incdec.ptr = getelementptr inbounds i32, i32* %25, i32 1
  store i32* %incdec.ptr, i32** %__seg_17, align 4
  br label %if.end

if.end:                                           ; preds = %if.then2, %if.then
  %26 = load i32, i32* %__n, align 4
  %div18 = sdiv i32 %26, 32
  store i32 %div18, i32* %__nw, align 4
  %__seg_19 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 0
  %27 = load i32*, i32** %__seg_19, align 4
  %call20 = call i32* @_ZNSt3__212__to_addressImEEPT_S2_(i32* %27) #8
  %28 = bitcast i32* %call20 to i8*
  %__seg_21 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__first, i32 0, i32 0
  %29 = load i32*, i32** %__seg_21, align 4
  %call22 = call i32* @_ZNSt3__212__to_addressImEEPT_S2_(i32* %29) #8
  %30 = bitcast i32* %call22 to i8*
  %31 = load i32, i32* %__nw, align 4
  %mul = mul i32 %31, 4
  call void @llvm.memmove.p0i8.p0i8.i32(i8* align 4 %28, i8* align 4 %30, i32 %mul, i1 false)
  %32 = load i32, i32* %__nw, align 4
  %mul23 = mul i32 %32, 32
  %33 = load i32, i32* %__n, align 4
  %sub24 = sub i32 %33, %mul23
  store i32 %sub24, i32* %__n, align 4
  %34 = load i32, i32* %__nw, align 4
  %__seg_25 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 0
  %35 = load i32*, i32** %__seg_25, align 4
  %add.ptr26 = getelementptr inbounds i32, i32* %35, i32 %34
  store i32* %add.ptr26, i32** %__seg_25, align 4
  %36 = load i32, i32* %__n, align 4
  %cmp27 = icmp sgt i32 %36, 0
  br i1 %cmp27, label %if.then28, label %if.end43

if.then28:                                        ; preds = %if.end
  %37 = load i32, i32* %__nw, align 4
  %__seg_29 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__first, i32 0, i32 0
  %38 = load i32*, i32** %__seg_29, align 4
  %add.ptr30 = getelementptr inbounds i32, i32* %38, i32 %37
  store i32* %add.ptr30, i32** %__seg_29, align 4
  %39 = load i32, i32* %__n, align 4
  %sub32 = sub nsw i32 32, %39
  %shr33 = lshr i32 -1, %sub32
  store i32 %shr33, i32* %__m31, align 4
  %__seg_35 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__first, i32 0, i32 0
  %40 = load i32*, i32** %__seg_35, align 4
  %41 = load i32, i32* %40, align 4
  %42 = load i32, i32* %__m31, align 4
  %and36 = and i32 %41, %42
  store i32 %and36, i32* %__b34, align 4
  %43 = load i32, i32* %__m31, align 4
  %neg37 = xor i32 %43, -1
  %__seg_38 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 0
  %44 = load i32*, i32** %__seg_38, align 4
  %45 = load i32, i32* %44, align 4
  %and39 = and i32 %45, %neg37
  store i32 %and39, i32* %44, align 4
  %46 = load i32, i32* %__b34, align 4
  %__seg_40 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 0
  %47 = load i32*, i32** %__seg_40, align 4
  %48 = load i32, i32* %47, align 4
  %or41 = or i32 %48, %46
  store i32 %or41, i32* %47, align 4
  %49 = load i32, i32* %__n, align 4
  %__ctz_42 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 1
  store i32 %49, i32* %__ctz_42, align 4
  br label %if.end43

if.end43:                                         ; preds = %if.then28, %if.end
  br label %if.end44

if.end44:                                         ; preds = %if.end43, %entry
  %call45 = call %"class.std::__2::__bit_iterator"* @_ZNSt3__214__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb0ELm0EEC2ERKS5_(%"class.std::__2::__bit_iterator"* %agg.result, %"class.std::__2::__bit_iterator"* nonnull align 4 dereferenceable(8) %__result) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216__copy_unalignedINS_6vectorIbNS_9allocatorIbEEEELb0EEENS_14__bit_iteratorIT_Lb0EXLi0EEEENS5_IS6_XT0_EXLi0EEEES8_S7_(%"class.std::__2::__bit_iterator"* noalias sret align 4 %agg.result, %"class.std::__2::__bit_iterator"* %__first, %"class.std::__2::__bit_iterator"* %__last, %"class.std::__2::__bit_iterator"* %__result) #0 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %__n = alloca i32, align 4
  %__clz_f = alloca i32, align 4
  %__dn = alloca i32, align 4
  %ref.tmp = alloca i32, align 4
  %__m = alloca i32, align 4
  %__b = alloca i32, align 4
  %__clz_r = alloca i32, align 4
  %__ddn = alloca i32, align 4
  %ref.tmp11 = alloca i32, align 4
  %ref.tmp12 = alloca i32, align 4
  %__clz_r58 = alloca i32, align 4
  %__m61 = alloca i32, align 4
  %__b65 = alloca i32, align 4
  %__b88 = alloca i32, align 4
  %__dn91 = alloca i32, align 4
  %ref.tmp92 = alloca i32, align 4
  %0 = bitcast %"class.std::__2::__bit_iterator"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  %call = call i32 @_ZNSt3__2miERKNS_14__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb0ELm0EEES7_(%"class.std::__2::__bit_iterator"* nonnull align 4 dereferenceable(8) %__last, %"class.std::__2::__bit_iterator"* nonnull align 4 dereferenceable(8) %__first)
  store i32 %call, i32* %__n, align 4
  %1 = load i32, i32* %__n, align 4
  %cmp = icmp sgt i32 %1, 0
  br i1 %cmp, label %if.then, label %if.end129

if.then:                                          ; preds = %entry
  %__ctz_ = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__first, i32 0, i32 1
  %2 = load i32, i32* %__ctz_, align 4
  %cmp1 = icmp ne i32 %2, 0
  br i1 %cmp1, label %if.then2, label %if.end57

if.then2:                                         ; preds = %if.then
  %__ctz_3 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__first, i32 0, i32 1
  %3 = load i32, i32* %__ctz_3, align 4
  %sub = sub i32 32, %3
  store i32 %sub, i32* %__clz_f, align 4
  %4 = load i32, i32* %__clz_f, align 4
  store i32 %4, i32* %ref.tmp, align 4
  %call4 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minIlEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %__n)
  %5 = load i32, i32* %call4, align 4
  store i32 %5, i32* %__dn, align 4
  %6 = load i32, i32* %__dn, align 4
  %7 = load i32, i32* %__n, align 4
  %sub5 = sub nsw i32 %7, %6
  store i32 %sub5, i32* %__n, align 4
  %__ctz_6 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__first, i32 0, i32 1
  %8 = load i32, i32* %__ctz_6, align 4
  %shl = shl i32 -1, %8
  %9 = load i32, i32* %__clz_f, align 4
  %10 = load i32, i32* %__dn, align 4
  %sub7 = sub i32 %9, %10
  %shr = lshr i32 -1, %sub7
  %and = and i32 %shl, %shr
  store i32 %and, i32* %__m, align 4
  %__seg_ = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__first, i32 0, i32 0
  %11 = load i32*, i32** %__seg_, align 4
  %12 = load i32, i32* %11, align 4
  %13 = load i32, i32* %__m, align 4
  %and8 = and i32 %12, %13
  store i32 %and8, i32* %__b, align 4
  %__ctz_9 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 1
  %14 = load i32, i32* %__ctz_9, align 4
  %sub10 = sub i32 32, %14
  store i32 %sub10, i32* %__clz_r, align 4
  %15 = load i32, i32* %__dn, align 4
  store i32 %15, i32* %ref.tmp11, align 4
  %16 = load i32, i32* %__clz_r, align 4
  store i32 %16, i32* %ref.tmp12, align 4
  %call13 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp11, i32* nonnull align 4 dereferenceable(4) %ref.tmp12)
  %17 = load i32, i32* %call13, align 4
  store i32 %17, i32* %__ddn, align 4
  %__ctz_14 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 1
  %18 = load i32, i32* %__ctz_14, align 4
  %shl15 = shl i32 -1, %18
  %19 = load i32, i32* %__clz_r, align 4
  %20 = load i32, i32* %__ddn, align 4
  %sub16 = sub i32 %19, %20
  %shr17 = lshr i32 -1, %sub16
  %and18 = and i32 %shl15, %shr17
  store i32 %and18, i32* %__m, align 4
  %21 = load i32, i32* %__m, align 4
  %neg = xor i32 %21, -1
  %__seg_19 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 0
  %22 = load i32*, i32** %__seg_19, align 4
  %23 = load i32, i32* %22, align 4
  %and20 = and i32 %23, %neg
  store i32 %and20, i32* %22, align 4
  %__ctz_21 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 1
  %24 = load i32, i32* %__ctz_21, align 4
  %__ctz_22 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__first, i32 0, i32 1
  %25 = load i32, i32* %__ctz_22, align 4
  %cmp23 = icmp ugt i32 %24, %25
  br i1 %cmp23, label %if.then24, label %if.else

if.then24:                                        ; preds = %if.then2
  %26 = load i32, i32* %__b, align 4
  %__ctz_25 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 1
  %27 = load i32, i32* %__ctz_25, align 4
  %__ctz_26 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__first, i32 0, i32 1
  %28 = load i32, i32* %__ctz_26, align 4
  %sub27 = sub i32 %27, %28
  %shl28 = shl i32 %26, %sub27
  %__seg_29 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 0
  %29 = load i32*, i32** %__seg_29, align 4
  %30 = load i32, i32* %29, align 4
  %or = or i32 %30, %shl28
  store i32 %or, i32* %29, align 4
  br label %if.end

if.else:                                          ; preds = %if.then2
  %31 = load i32, i32* %__b, align 4
  %__ctz_30 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__first, i32 0, i32 1
  %32 = load i32, i32* %__ctz_30, align 4
  %__ctz_31 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 1
  %33 = load i32, i32* %__ctz_31, align 4
  %sub32 = sub i32 %32, %33
  %shr33 = lshr i32 %31, %sub32
  %__seg_34 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 0
  %34 = load i32*, i32** %__seg_34, align 4
  %35 = load i32, i32* %34, align 4
  %or35 = or i32 %35, %shr33
  store i32 %or35, i32* %34, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then24
  %36 = load i32, i32* %__ddn, align 4
  %__ctz_36 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 1
  %37 = load i32, i32* %__ctz_36, align 4
  %add = add i32 %36, %37
  %div = udiv i32 %add, 32
  %__seg_37 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 0
  %38 = load i32*, i32** %__seg_37, align 4
  %add.ptr = getelementptr inbounds i32, i32* %38, i32 %div
  store i32* %add.ptr, i32** %__seg_37, align 4
  %39 = load i32, i32* %__ddn, align 4
  %__ctz_38 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 1
  %40 = load i32, i32* %__ctz_38, align 4
  %add39 = add i32 %39, %40
  %rem = urem i32 %add39, 32
  %__ctz_40 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 1
  store i32 %rem, i32* %__ctz_40, align 4
  %41 = load i32, i32* %__ddn, align 4
  %42 = load i32, i32* %__dn, align 4
  %sub41 = sub i32 %42, %41
  store i32 %sub41, i32* %__dn, align 4
  %43 = load i32, i32* %__dn, align 4
  %cmp42 = icmp sgt i32 %43, 0
  br i1 %cmp42, label %if.then43, label %if.end55

if.then43:                                        ; preds = %if.end
  %44 = load i32, i32* %__dn, align 4
  %sub44 = sub nsw i32 32, %44
  %shr45 = lshr i32 -1, %sub44
  store i32 %shr45, i32* %__m, align 4
  %45 = load i32, i32* %__m, align 4
  %neg46 = xor i32 %45, -1
  %__seg_47 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 0
  %46 = load i32*, i32** %__seg_47, align 4
  %47 = load i32, i32* %46, align 4
  %and48 = and i32 %47, %neg46
  store i32 %and48, i32* %46, align 4
  %48 = load i32, i32* %__b, align 4
  %__ctz_49 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__first, i32 0, i32 1
  %49 = load i32, i32* %__ctz_49, align 4
  %50 = load i32, i32* %__ddn, align 4
  %add50 = add i32 %49, %50
  %shr51 = lshr i32 %48, %add50
  %__seg_52 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 0
  %51 = load i32*, i32** %__seg_52, align 4
  %52 = load i32, i32* %51, align 4
  %or53 = or i32 %52, %shr51
  store i32 %or53, i32* %51, align 4
  %53 = load i32, i32* %__dn, align 4
  %__ctz_54 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 1
  store i32 %53, i32* %__ctz_54, align 4
  br label %if.end55

if.end55:                                         ; preds = %if.then43, %if.end
  %__seg_56 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__first, i32 0, i32 0
  %54 = load i32*, i32** %__seg_56, align 4
  %incdec.ptr = getelementptr inbounds i32, i32* %54, i32 1
  store i32* %incdec.ptr, i32** %__seg_56, align 4
  br label %if.end57

if.end57:                                         ; preds = %if.end55, %if.then
  %__ctz_59 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 1
  %55 = load i32, i32* %__ctz_59, align 4
  %sub60 = sub i32 32, %55
  store i32 %sub60, i32* %__clz_r58, align 4
  %__ctz_62 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 1
  %56 = load i32, i32* %__ctz_62, align 4
  %shl63 = shl i32 -1, %56
  store i32 %shl63, i32* %__m61, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end57
  %57 = load i32, i32* %__n, align 4
  %cmp64 = icmp sge i32 %57, 32
  br i1 %cmp64, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %__seg_66 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__first, i32 0, i32 0
  %58 = load i32*, i32** %__seg_66, align 4
  %59 = load i32, i32* %58, align 4
  store i32 %59, i32* %__b65, align 4
  %60 = load i32, i32* %__m61, align 4
  %neg67 = xor i32 %60, -1
  %__seg_68 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 0
  %61 = load i32*, i32** %__seg_68, align 4
  %62 = load i32, i32* %61, align 4
  %and69 = and i32 %62, %neg67
  store i32 %and69, i32* %61, align 4
  %63 = load i32, i32* %__b65, align 4
  %__ctz_70 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 1
  %64 = load i32, i32* %__ctz_70, align 4
  %shl71 = shl i32 %63, %64
  %__seg_72 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 0
  %65 = load i32*, i32** %__seg_72, align 4
  %66 = load i32, i32* %65, align 4
  %or73 = or i32 %66, %shl71
  store i32 %or73, i32* %65, align 4
  %__seg_74 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 0
  %67 = load i32*, i32** %__seg_74, align 4
  %incdec.ptr75 = getelementptr inbounds i32, i32* %67, i32 1
  store i32* %incdec.ptr75, i32** %__seg_74, align 4
  %68 = load i32, i32* %__m61, align 4
  %__seg_76 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 0
  %69 = load i32*, i32** %__seg_76, align 4
  %70 = load i32, i32* %69, align 4
  %and77 = and i32 %70, %68
  store i32 %and77, i32* %69, align 4
  %71 = load i32, i32* %__b65, align 4
  %72 = load i32, i32* %__clz_r58, align 4
  %shr78 = lshr i32 %71, %72
  %__seg_79 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 0
  %73 = load i32*, i32** %__seg_79, align 4
  %74 = load i32, i32* %73, align 4
  %or80 = or i32 %74, %shr78
  store i32 %or80, i32* %73, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %75 = load i32, i32* %__n, align 4
  %sub81 = sub nsw i32 %75, 32
  store i32 %sub81, i32* %__n, align 4
  %__seg_82 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__first, i32 0, i32 0
  %76 = load i32*, i32** %__seg_82, align 4
  %incdec.ptr83 = getelementptr inbounds i32, i32* %76, i32 1
  store i32* %incdec.ptr83, i32** %__seg_82, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %77 = load i32, i32* %__n, align 4
  %cmp84 = icmp sgt i32 %77, 0
  br i1 %cmp84, label %if.then85, label %if.end128

if.then85:                                        ; preds = %for.end
  %78 = load i32, i32* %__n, align 4
  %sub86 = sub nsw i32 32, %78
  %shr87 = lshr i32 -1, %sub86
  store i32 %shr87, i32* %__m61, align 4
  %__seg_89 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__first, i32 0, i32 0
  %79 = load i32*, i32** %__seg_89, align 4
  %80 = load i32, i32* %79, align 4
  %81 = load i32, i32* %__m61, align 4
  %and90 = and i32 %80, %81
  store i32 %and90, i32* %__b88, align 4
  %82 = load i32, i32* %__clz_r58, align 4
  store i32 %82, i32* %ref.tmp92, align 4
  %call93 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minIlEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %__n, i32* nonnull align 4 dereferenceable(4) %ref.tmp92)
  %83 = load i32, i32* %call93, align 4
  store i32 %83, i32* %__dn91, align 4
  %__ctz_94 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 1
  %84 = load i32, i32* %__ctz_94, align 4
  %shl95 = shl i32 -1, %84
  %85 = load i32, i32* %__clz_r58, align 4
  %86 = load i32, i32* %__dn91, align 4
  %sub96 = sub i32 %85, %86
  %shr97 = lshr i32 -1, %sub96
  %and98 = and i32 %shl95, %shr97
  store i32 %and98, i32* %__m61, align 4
  %87 = load i32, i32* %__m61, align 4
  %neg99 = xor i32 %87, -1
  %__seg_100 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 0
  %88 = load i32*, i32** %__seg_100, align 4
  %89 = load i32, i32* %88, align 4
  %and101 = and i32 %89, %neg99
  store i32 %and101, i32* %88, align 4
  %90 = load i32, i32* %__b88, align 4
  %__ctz_102 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 1
  %91 = load i32, i32* %__ctz_102, align 4
  %shl103 = shl i32 %90, %91
  %__seg_104 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 0
  %92 = load i32*, i32** %__seg_104, align 4
  %93 = load i32, i32* %92, align 4
  %or105 = or i32 %93, %shl103
  store i32 %or105, i32* %92, align 4
  %94 = load i32, i32* %__dn91, align 4
  %__ctz_106 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 1
  %95 = load i32, i32* %__ctz_106, align 4
  %add107 = add i32 %94, %95
  %div108 = udiv i32 %add107, 32
  %__seg_109 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 0
  %96 = load i32*, i32** %__seg_109, align 4
  %add.ptr110 = getelementptr inbounds i32, i32* %96, i32 %div108
  store i32* %add.ptr110, i32** %__seg_109, align 4
  %97 = load i32, i32* %__dn91, align 4
  %__ctz_111 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 1
  %98 = load i32, i32* %__ctz_111, align 4
  %add112 = add i32 %97, %98
  %rem113 = urem i32 %add112, 32
  %__ctz_114 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 1
  store i32 %rem113, i32* %__ctz_114, align 4
  %99 = load i32, i32* %__dn91, align 4
  %100 = load i32, i32* %__n, align 4
  %sub115 = sub i32 %100, %99
  store i32 %sub115, i32* %__n, align 4
  %101 = load i32, i32* %__n, align 4
  %cmp116 = icmp sgt i32 %101, 0
  br i1 %cmp116, label %if.then117, label %if.end127

if.then117:                                       ; preds = %if.then85
  %102 = load i32, i32* %__n, align 4
  %sub118 = sub nsw i32 32, %102
  %shr119 = lshr i32 -1, %sub118
  store i32 %shr119, i32* %__m61, align 4
  %103 = load i32, i32* %__m61, align 4
  %neg120 = xor i32 %103, -1
  %__seg_121 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 0
  %104 = load i32*, i32** %__seg_121, align 4
  %105 = load i32, i32* %104, align 4
  %and122 = and i32 %105, %neg120
  store i32 %and122, i32* %104, align 4
  %106 = load i32, i32* %__b88, align 4
  %107 = load i32, i32* %__dn91, align 4
  %shr123 = lshr i32 %106, %107
  %__seg_124 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 0
  %108 = load i32*, i32** %__seg_124, align 4
  %109 = load i32, i32* %108, align 4
  %or125 = or i32 %109, %shr123
  store i32 %or125, i32* %108, align 4
  %110 = load i32, i32* %__n, align 4
  %__ctz_126 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__result, i32 0, i32 1
  store i32 %110, i32* %__ctz_126, align 4
  br label %if.end127

if.end127:                                        ; preds = %if.then117, %if.then85
  br label %if.end128

if.end128:                                        ; preds = %if.end127, %for.end
  br label %if.end129

if.end129:                                        ; preds = %if.end128, %entry
  %call130 = call %"class.std::__2::__bit_iterator"* @_ZNSt3__214__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb0ELm0EEC2ERKS5_(%"class.std::__2::__bit_iterator"* %agg.result, %"class.std::__2::__bit_iterator"* nonnull align 4 dereferenceable(8) %__result) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minIlEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::__less", align 1
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__a.addr, align 4
  %1 = load i32*, i32** %__b.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minIlNS_6__lessIllEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNSt3__212__to_addressImEEPT_S2_(i32* %__p) #0 comdat {
entry:
  %__p.addr = alloca i32*, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %0 = load i32*, i32** %__p.addr, align 4
  ret i32* %0
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memmove.p0i8.p0i8.i32(i8* nocapture, i8* nocapture readonly, i32, i1 immarg) #1

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minIlNS_6__lessIllEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__comp = alloca %"struct.std::__2::__less", align 1
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__b.addr, align 4
  %1 = load i32*, i32** %__a.addr, align 4
  %call = call zeroext i1 @_ZNKSt3__26__lessIllEclERKlS3_(%"struct.std::__2::__less"* %__comp, i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  br i1 %call, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32*, i32** %__b.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %3 = load i32*, i32** %__a.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi i32* [ %2, %cond.true ], [ %3, %cond.false ]
  ret i32* %cond-lvalue
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__26__lessIllEclERKlS3_(%"struct.std::__2::__less"* %this, i32* nonnull align 4 dereferenceable(4) %__x, i32* nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__less"*, align 4
  %__x.addr = alloca i32*, align 4
  %__y.addr = alloca i32*, align 4
  store %"struct.std::__2::__less"* %this, %"struct.std::__2::__less"** %this.addr, align 4
  store i32* %__x, i32** %__x.addr, align 4
  store i32* %__y, i32** %__y.addr, align 4
  %this1 = load %"struct.std::__2::__less"*, %"struct.std::__2::__less"** %this.addr, align 4
  %0 = load i32*, i32** %__x.addr, align 4
  %1 = load i32, i32* %0, align 4
  %2 = load i32*, i32** %__y.addr, align 4
  %3 = load i32, i32* %2, align 4
  %cmp = icmp slt i32 %1, %3
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::__less.150", align 1
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__a.addr, align 4
  %1 = load i32*, i32** %__b.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__comp = alloca %"struct.std::__2::__less.150", align 1
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__b.addr, align 4
  %1 = load i32*, i32** %__a.addr, align 4
  %call = call zeroext i1 @_ZNKSt3__26__lessImmEclERKmS3_(%"struct.std::__2::__less.150"* %__comp, i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  br i1 %call, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32*, i32** %__b.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %3 = load i32*, i32** %__a.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi i32* [ %2, %cond.true ], [ %3, %cond.false ]
  ret i32* %cond-lvalue
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__26__lessImmEclERKmS3_(%"struct.std::__2::__less.150"* %this, i32* nonnull align 4 dereferenceable(4) %__x, i32* nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__less.150"*, align 4
  %__x.addr = alloca i32*, align 4
  %__y.addr = alloca i32*, align 4
  store %"struct.std::__2::__less.150"* %this, %"struct.std::__2::__less.150"** %this.addr, align 4
  store i32* %__x, i32** %__x.addr, align 4
  store i32* %__y, i32** %__y.addr, align 4
  %this1 = load %"struct.std::__2::__less.150"*, %"struct.std::__2::__less.150"** %this.addr, align 4
  %0 = load i32*, i32** %__x.addr, align 4
  %1 = load i32, i32* %0, align 4
  %2 = load i32*, i32** %__y.addr, align 4
  %3 = load i32, i32* %2, align 4
  %cmp = icmp ult i32 %1, %3
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__bit_iterator"* @_ZNSt3__214__bit_iteratorINS_6vectorIbNS_9allocatorIbEEEELb0ELm0EEC2EPmj(%"class.std::__2::__bit_iterator"* returned %this, i32* %__s, i32 %__ctz) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__bit_iterator"*, align 4
  %__s.addr = alloca i32*, align 4
  %__ctz.addr = alloca i32, align 4
  store %"class.std::__2::__bit_iterator"* %this, %"class.std::__2::__bit_iterator"** %this.addr, align 4
  store i32* %__s, i32** %__s.addr, align 4
  store i32 %__ctz, i32* %__ctz.addr, align 4
  %this1 = load %"class.std::__2::__bit_iterator"*, %"class.std::__2::__bit_iterator"** %this.addr, align 4
  %__seg_ = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__s.addr, align 4
  store i32* %0, i32** %__seg_, align 4
  %__ctz_ = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %this1, i32 0, i32 1
  %1 = load i32, i32* %__ctz.addr, align 4
  store i32 %1, i32* %__ctz_, align 4
  ret %"class.std::__2::__bit_iterator"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::__less.150", align 1
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__a.addr, align 4
  %1 = load i32*, i32** %__b.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__26vectorIbNS_9allocatorIbEEE10__align_itEm(i32 %__new_size) #0 comdat {
entry:
  %__new_size.addr = alloca i32, align 4
  store i32 %__new_size, i32* %__new_size.addr, align 4
  %0 = load i32, i32* %__new_size.addr, align 4
  %add = add i32 %0, 31
  %and = and i32 %add, -32
  ret i32 %and
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__comp = alloca %"struct.std::__2::__less.150", align 1
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__a.addr, align 4
  %1 = load i32*, i32** %__b.addr, align 4
  %call = call zeroext i1 @_ZNKSt3__26__lessImmEclERKmS3_(%"struct.std::__2::__less.150"* %__comp, i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  br i1 %call, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32*, i32** %__b.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %3 = load i32*, i32** %__a.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi i32* [ %2, %cond.true ], [ %3, %cond.false ]
  ret i32* %cond-lvalue
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__24swapIPmEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_(i32** nonnull align 4 dereferenceable(4) %__x, i32** nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %__x.addr = alloca i32**, align 4
  %__y.addr = alloca i32**, align 4
  %__t = alloca i32*, align 4
  store i32** %__x, i32*** %__x.addr, align 4
  store i32** %__y, i32*** %__y.addr, align 4
  %0 = load i32**, i32*** %__x.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__24moveIRPmEEONS_16remove_referenceIT_E4typeEOS4_(i32** nonnull align 4 dereferenceable(4) %0) #8
  %1 = load i32*, i32** %call, align 4
  store i32* %1, i32** %__t, align 4
  %2 = load i32**, i32*** %__y.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__24moveIRPmEEONS_16remove_referenceIT_E4typeEOS4_(i32** nonnull align 4 dereferenceable(4) %2) #8
  %3 = load i32*, i32** %call1, align 4
  %4 = load i32**, i32*** %__x.addr, align 4
  store i32* %3, i32** %4, align 4
  %call2 = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__24moveIRPmEEONS_16remove_referenceIT_E4typeEOS4_(i32** nonnull align 4 dereferenceable(4) %__t) #8
  %5 = load i32*, i32** %call2, align 4
  %6 = load i32**, i32*** %__y.addr, align 4
  store i32* %5, i32** %6, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__24swapImEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS2_EE5valueEvE4typeERS2_S5_(i32* nonnull align 4 dereferenceable(4) %__x, i32* nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %__x.addr = alloca i32*, align 4
  %__y.addr = alloca i32*, align 4
  %__t = alloca i32, align 4
  store i32* %__x, i32** %__x.addr, align 4
  store i32* %__y, i32** %__y.addr, align 4
  %0 = load i32*, i32** %__x.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__24moveIRmEEONS_16remove_referenceIT_E4typeEOS3_(i32* nonnull align 4 dereferenceable(4) %0) #8
  %1 = load i32, i32* %call, align 4
  store i32 %1, i32* %__t, align 4
  %2 = load i32*, i32** %__y.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__24moveIRmEEONS_16remove_referenceIT_E4typeEOS3_(i32* nonnull align 4 dereferenceable(4) %2) #8
  %3 = load i32, i32* %call1, align 4
  %4 = load i32*, i32** %__x.addr, align 4
  store i32 %3, i32* %4, align 4
  %call2 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__24moveIRmEEONS_16remove_referenceIT_E4typeEOS3_(i32* nonnull align 4 dereferenceable(4) %__t) #8
  %5 = load i32, i32* %call2, align 4
  %6 = load i32*, i32** %__y.addr, align 4
  store i32 %5, i32* %6, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216__swap_allocatorINS_9allocatorImEEEEvRT_S4_NS_17integral_constantIbLb0EEE(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %0, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %1) #0 comdat {
entry:
  %2 = alloca %"struct.std::__2::integral_constant.146", align 1
  %.addr = alloca %"class.std::__2::allocator"*, align 4
  %.addr1 = alloca %"class.std::__2::allocator"*, align 4
  store %"class.std::__2::allocator"* %0, %"class.std::__2::allocator"** %.addr, align 4
  store %"class.std::__2::allocator"* %1, %"class.std::__2::allocator"** %.addr1, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNSt3__24moveIRPmEEONS_16remove_referenceIT_E4typeEOS4_(i32** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca i32**, align 4
  store i32** %__t, i32*** %__t.addr, align 4
  %0 = load i32**, i32*** %__t.addr, align 4
  ret i32** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__24moveIRmEEONS_16remove_referenceIT_E4typeEOS3_(i32* nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca i32*, align 4
  store i32* %__t, i32** %__t.addr, align 4
  %0 = load i32*, i32** %__t.addr, align 4
  ret i32* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorImEEE10deallocateERS2_Pmm(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, i32* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i32*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %1 = load i32*, i32** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorImE10deallocateEPmm(%"class.std::__2::allocator"* %0, i32* %1, i32 %2) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorImE10deallocateEPmm(%"class.std::__2::allocator"* %this, i32* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i32*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  %0 = load i32*, i32** %__p.addr, align 4
  %1 = bitcast i32* %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__fill_n_trueINS_6vectorIbNS_9allocatorIbEEEEEEvNS_14__bit_iteratorIT_Lb0EXLi0EEEENS6_9size_typeE(%"class.std::__2::__bit_iterator"* %__first, i32 %__n) #0 comdat {
entry:
  %__n.addr = alloca i32, align 4
  %__bits_per_word = alloca i32, align 4
  %__clz_f = alloca i32, align 4
  %__dn = alloca i32, align 4
  %__m = alloca i32, align 4
  %__nw = alloca i32, align 4
  %__m13 = alloca i32, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store i32 32, i32* %__bits_per_word, align 4
  %__ctz_ = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__first, i32 0, i32 1
  %0 = load i32, i32* %__ctz_, align 4
  %cmp = icmp ne i32 %0, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__ctz_1 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__first, i32 0, i32 1
  %1 = load i32, i32* %__ctz_1, align 4
  %sub = sub i32 32, %1
  store i32 %sub, i32* %__clz_f, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %__clz_f, i32* nonnull align 4 dereferenceable(4) %__n.addr)
  %2 = load i32, i32* %call, align 4
  store i32 %2, i32* %__dn, align 4
  %__ctz_2 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__first, i32 0, i32 1
  %3 = load i32, i32* %__ctz_2, align 4
  %shl = shl i32 -1, %3
  %4 = load i32, i32* %__clz_f, align 4
  %5 = load i32, i32* %__dn, align 4
  %sub3 = sub i32 %4, %5
  %shr = lshr i32 -1, %sub3
  %and = and i32 %shl, %shr
  store i32 %and, i32* %__m, align 4
  %6 = load i32, i32* %__m, align 4
  %__seg_ = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__first, i32 0, i32 0
  %7 = load i32*, i32** %__seg_, align 4
  %8 = load i32, i32* %7, align 4
  %or = or i32 %8, %6
  store i32 %or, i32* %7, align 4
  %9 = load i32, i32* %__dn, align 4
  %10 = load i32, i32* %__n.addr, align 4
  %sub4 = sub i32 %10, %9
  store i32 %sub4, i32* %__n.addr, align 4
  %__seg_5 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__first, i32 0, i32 0
  %11 = load i32*, i32** %__seg_5, align 4
  %incdec.ptr = getelementptr inbounds i32, i32* %11, i32 1
  store i32* %incdec.ptr, i32** %__seg_5, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %12 = load i32, i32* %__n.addr, align 4
  %div = udiv i32 %12, 32
  store i32 %div, i32* %__nw, align 4
  %__seg_6 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__first, i32 0, i32 0
  %13 = load i32*, i32** %__seg_6, align 4
  %call7 = call i32* @_ZNSt3__212__to_addressImEEPT_S2_(i32* %13) #8
  %14 = bitcast i32* %call7 to i8*
  %15 = load i32, i32* %__nw, align 4
  %mul = mul i32 %15, 4
  call void @llvm.memset.p0i8.i32(i8* align 4 %14, i8 -1, i32 %mul, i1 false)
  %16 = load i32, i32* %__nw, align 4
  %mul8 = mul i32 %16, 32
  %17 = load i32, i32* %__n.addr, align 4
  %sub9 = sub i32 %17, %mul8
  store i32 %sub9, i32* %__n.addr, align 4
  %18 = load i32, i32* %__n.addr, align 4
  %cmp10 = icmp ugt i32 %18, 0
  br i1 %cmp10, label %if.then11, label %if.end18

if.then11:                                        ; preds = %if.end
  %19 = load i32, i32* %__nw, align 4
  %__seg_12 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__first, i32 0, i32 0
  %20 = load i32*, i32** %__seg_12, align 4
  %add.ptr = getelementptr inbounds i32, i32* %20, i32 %19
  store i32* %add.ptr, i32** %__seg_12, align 4
  %21 = load i32, i32* %__n.addr, align 4
  %sub14 = sub i32 32, %21
  %shr15 = lshr i32 -1, %sub14
  store i32 %shr15, i32* %__m13, align 4
  %22 = load i32, i32* %__m13, align 4
  %__seg_16 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__first, i32 0, i32 0
  %23 = load i32*, i32** %__seg_16, align 4
  %24 = load i32, i32* %23, align 4
  %or17 = or i32 %24, %22
  store i32 %or17, i32* %23, align 4
  br label %if.end18

if.end18:                                         ; preds = %if.then11, %if.end
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__fill_n_falseINS_6vectorIbNS_9allocatorIbEEEEEEvNS_14__bit_iteratorIT_Lb0EXLi0EEEENS6_9size_typeE(%"class.std::__2::__bit_iterator"* %__first, i32 %__n) #0 comdat {
entry:
  %__n.addr = alloca i32, align 4
  %__bits_per_word = alloca i32, align 4
  %__clz_f = alloca i32, align 4
  %__dn = alloca i32, align 4
  %__m = alloca i32, align 4
  %__nw = alloca i32, align 4
  %__m14 = alloca i32, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store i32 32, i32* %__bits_per_word, align 4
  %__ctz_ = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__first, i32 0, i32 1
  %0 = load i32, i32* %__ctz_, align 4
  %cmp = icmp ne i32 %0, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__ctz_1 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__first, i32 0, i32 1
  %1 = load i32, i32* %__ctz_1, align 4
  %sub = sub i32 32, %1
  store i32 %sub, i32* %__clz_f, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %__clz_f, i32* nonnull align 4 dereferenceable(4) %__n.addr)
  %2 = load i32, i32* %call, align 4
  store i32 %2, i32* %__dn, align 4
  %__ctz_2 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__first, i32 0, i32 1
  %3 = load i32, i32* %__ctz_2, align 4
  %shl = shl i32 -1, %3
  %4 = load i32, i32* %__clz_f, align 4
  %5 = load i32, i32* %__dn, align 4
  %sub3 = sub i32 %4, %5
  %shr = lshr i32 -1, %sub3
  %and = and i32 %shl, %shr
  store i32 %and, i32* %__m, align 4
  %6 = load i32, i32* %__m, align 4
  %neg = xor i32 %6, -1
  %__seg_ = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__first, i32 0, i32 0
  %7 = load i32*, i32** %__seg_, align 4
  %8 = load i32, i32* %7, align 4
  %and4 = and i32 %8, %neg
  store i32 %and4, i32* %7, align 4
  %9 = load i32, i32* %__dn, align 4
  %10 = load i32, i32* %__n.addr, align 4
  %sub5 = sub i32 %10, %9
  store i32 %sub5, i32* %__n.addr, align 4
  %__seg_6 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__first, i32 0, i32 0
  %11 = load i32*, i32** %__seg_6, align 4
  %incdec.ptr = getelementptr inbounds i32, i32* %11, i32 1
  store i32* %incdec.ptr, i32** %__seg_6, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %12 = load i32, i32* %__n.addr, align 4
  %div = udiv i32 %12, 32
  store i32 %div, i32* %__nw, align 4
  %__seg_7 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__first, i32 0, i32 0
  %13 = load i32*, i32** %__seg_7, align 4
  %call8 = call i32* @_ZNSt3__212__to_addressImEEPT_S2_(i32* %13) #8
  %14 = bitcast i32* %call8 to i8*
  %15 = load i32, i32* %__nw, align 4
  %mul = mul i32 %15, 4
  call void @llvm.memset.p0i8.i32(i8* align 4 %14, i8 0, i32 %mul, i1 false)
  %16 = load i32, i32* %__nw, align 4
  %mul9 = mul i32 %16, 32
  %17 = load i32, i32* %__n.addr, align 4
  %sub10 = sub i32 %17, %mul9
  store i32 %sub10, i32* %__n.addr, align 4
  %18 = load i32, i32* %__n.addr, align 4
  %cmp11 = icmp ugt i32 %18, 0
  br i1 %cmp11, label %if.then12, label %if.end20

if.then12:                                        ; preds = %if.end
  %19 = load i32, i32* %__nw, align 4
  %__seg_13 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__first, i32 0, i32 0
  %20 = load i32*, i32** %__seg_13, align 4
  %add.ptr = getelementptr inbounds i32, i32* %20, i32 %19
  store i32* %add.ptr, i32** %__seg_13, align 4
  %21 = load i32, i32* %__n.addr, align 4
  %sub15 = sub i32 32, %21
  %shr16 = lshr i32 -1, %sub15
  store i32 %shr16, i32* %__m14, align 4
  %22 = load i32, i32* %__m14, align 4
  %neg17 = xor i32 %22, -1
  %__seg_18 = getelementptr inbounds %"class.std::__2::__bit_iterator", %"class.std::__2::__bit_iterator"* %__first, i32 0, i32 0
  %23 = load i32*, i32** %__seg_18, align 4
  %24 = load i32, i32* %23, align 4
  %and19 = and i32 %24, %neg17
  store i32 %and19, i32* %23, align 4
  br label %if.end20

if.end20:                                         ; preds = %if.then12, %if.end
  ret void
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #7

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.1"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.1"*, align 4
  store %"class.std::__2::vector.1"* %this, %"class.std::__2::vector.1"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base"*
  %call = call i32 @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::__vector_base"* %0) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType"* @_ZNSt3__26fill_nIPN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEEmS4_EET_S6_T0_RKT1_(%"class.draco::IndexType"* %__first, i32 %__n, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %__value_) #0 comdat {
entry:
  %__first.addr = alloca %"class.draco::IndexType"*, align 4
  %__n.addr = alloca i32, align 4
  %__value_.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.draco::IndexType"* %__first, %"class.draco::IndexType"** %__first.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store %"class.draco::IndexType"* %__value_, %"class.draco::IndexType"** %__value_.addr, align 4
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__first.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNSt3__221__convert_to_integralEm(i32 %1)
  %2 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__value_.addr, align 4
  %call1 = call %"class.draco::IndexType"* @_ZNSt3__28__fill_nIPN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEEmS4_EET_S6_T0_RKT1_(%"class.draco::IndexType"* %0, i32 %call, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %2)
  ret %"class.draco::IndexType"* %call1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE18__construct_at_endEmRKS4_(%"class.std::__2::vector.1"* %this, i32 %__n, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %__x) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.1"*, align 4
  %__n.addr = alloca i32, align 4
  %__x.addr = alloca %"class.draco::IndexType"*, align 4
  %__tx = alloca %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>>>::_ConstructTransaction", align 4
  store %"class.std::__2::vector.1"* %this, %"class.std::__2::vector.1"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store %"class.draco::IndexType"* %__x, %"class.draco::IndexType"** %__x.addr, align 4
  %this1 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %this.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %call = call %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>>>::_ConstructTransaction"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionC2ERS7_m(%"struct.std::__2::vector<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>>>::_ConstructTransaction"* %__tx, %"class.std::__2::vector.1"* nonnull align 4 dereferenceable(12) %this1, i32 %0)
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__pos_, align 4
  %__new_end_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>>>::_ConstructTransaction"* %__tx, i32 0, i32 2
  %2 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__new_end_, align 4
  %cmp = icmp ne %"class.draco::IndexType"* %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.5"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base"* %3) #8
  %__pos_3 = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %4 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__pos_3, align 4
  %call4 = call %"class.draco::IndexType"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType"* %4) #8
  %5 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__x.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEEEEE9constructIS5_JRKS5_EEEvRS6_PT_DpOT0_(%"class.std::__2::allocator.5"* nonnull align 1 dereferenceable(1) %call2, %"class.draco::IndexType"* %call4, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %5)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %__pos_5 = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %6 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__pos_5, align 4
  %incdec.ptr = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %6, i32 1
  store %"class.draco::IndexType"* %incdec.ptr, %"class.draco::IndexType"** %__pos_5, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call6 = call %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>>>::_ConstructTransaction"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>>>::_ConstructTransaction"* %__tx) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_(%"class.std::__2::vector.1"* %this, %"class.draco::IndexType"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.1"*, align 4
  %__new_last.addr = alloca %"class.draco::IndexType"*, align 4
  %__old_size = alloca i32, align 4
  store %"class.std::__2::vector.1"* %this, %"class.std::__2::vector.1"** %this.addr, align 4
  store %"class.draco::IndexType"* %__new_last, %"class.draco::IndexType"** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %this.addr, align 4
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__new_last.addr, align 4
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE27__invalidate_iterators_pastEPS4_(%"class.std::__2::vector.1"* %this1, %"class.draco::IndexType"* %0)
  %call = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.1"* %this1) #8
  store i32 %call, i32* %__old_size, align 4
  %1 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base"*
  %2 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__new_last.addr, align 4
  call void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_(%"class.std::__2::__vector_base"* %1, %"class.draco::IndexType"* %2) #8
  %3 = load i32, i32* %__old_size, align 4
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_shrinkEm(%"class.std::__2::vector.1"* %this1, i32 %3) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE13__vdeallocateEv(%"class.std::__2::vector.1"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.1"*, align 4
  store %"class.std::__2::vector.1"* %this, %"class.std::__2::vector.1"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 0
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  %cmp = icmp ne %"class.draco::IndexType"* %1, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE5clearEv(%"class.std::__2::vector.1"* %this1) #8
  %2 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.5"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base"* %2) #8
  %3 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_2 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %3, i32 0, i32 0
  %4 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_2, align 4
  %call3 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.1"* %this1) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEEEEE10deallocateERS6_PS5_m(%"class.std::__2::allocator.5"* nonnull align 1 dereferenceable(1) %call, %"class.draco::IndexType"* %4, i32 %call3) #8
  %5 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base"*
  %call4 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base"* %5) #8
  store %"class.draco::IndexType"* null, %"class.draco::IndexType"** %call4, align 4
  %6 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %6, i32 0, i32 1
  store %"class.draco::IndexType"* null, %"class.draco::IndexType"** %__end_, align 4
  %7 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_5 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %7, i32 0, i32 0
  store %"class.draco::IndexType"* null, %"class.draco::IndexType"** %__begin_5, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE11__vallocateEm(%"class.std::__2::vector.1"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.1"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector.1"* %this, %"class.std::__2::vector.1"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %this.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE8max_sizeEv(%"class.std::__2::vector.1"* %this1) #8
  %cmp = icmp ugt i32 %0, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base_common"*
  call void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"* %1) #10
  unreachable

if.end:                                           ; preds = %entry
  %2 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.5"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base"* %2) #8
  %3 = load i32, i32* %__n.addr, align 4
  %call3 = call %"class.draco::IndexType"* @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEEEEE8allocateERS6_m(%"class.std::__2::allocator.5"* nonnull align 1 dereferenceable(1) %call2, i32 %3)
  %4 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %4, i32 0, i32 1
  store %"class.draco::IndexType"* %call3, %"class.draco::IndexType"** %__end_, align 4
  %5 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %5, i32 0, i32 0
  store %"class.draco::IndexType"* %call3, %"class.draco::IndexType"** %__begin_, align 4
  %6 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_4 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %6, i32 0, i32 0
  %7 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_4, align 4
  %8 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %7, i32 %8
  %9 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base"*
  %call5 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base"* %9) #8
  store %"class.draco::IndexType"* %add.ptr, %"class.draco::IndexType"** %call5, align 4
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE14__annotate_newEm(%"class.std::__2::vector.1"* %this1, i32 0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE11__recommendEm(%"class.std::__2::vector.1"* %this, i32 %__new_size) #0 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %"class.std::__2::vector.1"*, align 4
  %__new_size.addr = alloca i32, align 4
  %__ms = alloca i32, align 4
  %__cap = alloca i32, align 4
  %ref.tmp = alloca i32, align 4
  store %"class.std::__2::vector.1"* %this, %"class.std::__2::vector.1"** %this.addr, align 4
  store i32 %__new_size, i32* %__new_size.addr, align 4
  %this1 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE8max_sizeEv(%"class.std::__2::vector.1"* %this1) #8
  store i32 %call, i32* %__ms, align 4
  %0 = load i32, i32* %__new_size.addr, align 4
  %1 = load i32, i32* %__ms, align 4
  %cmp = icmp ugt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base_common"*
  call void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"* %2) #10
  unreachable

if.end:                                           ; preds = %entry
  %call2 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.1"* %this1) #8
  store i32 %call2, i32* %__cap, align 4
  %3 = load i32, i32* %__cap, align 4
  %4 = load i32, i32* %__ms, align 4
  %div = udiv i32 %4, 2
  %cmp3 = icmp uge i32 %3, %div
  br i1 %cmp3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %if.end
  %5 = load i32, i32* %__ms, align 4
  store i32 %5, i32* %retval, align 4
  br label %return

if.end5:                                          ; preds = %if.end
  %6 = load i32, i32* %__cap, align 4
  %mul = mul i32 2, %6
  store i32 %mul, i32* %ref.tmp, align 4
  %call6 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %__new_size.addr)
  %7 = load i32, i32* %call6, align 4
  store i32 %7, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end5, %if.then4
  %8 = load i32, i32* %retval, align 4
  ret i32 %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector.1"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.1"*, align 4
  store %"class.std::__2::vector.1"* %this, %"class.std::__2::vector.1"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base"* %this1) #8
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.draco::IndexType"* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.draco::IndexType"* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.2"* %__end_cap_) #8
  ret %"class.draco::IndexType"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.2"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.2"*, align 4
  store %"class.std::__2::__compressed_pair.2"* %this, %"class.std::__2::__compressed_pair.2"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.2"*, %"class.std::__2::__compressed_pair.2"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.2"* %this1 to %"struct.std::__2::__compressed_pair_elem.3"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.3"* %0) #8
  ret %"class.draco::IndexType"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNKSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.3"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.3"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.3"* %this, %"struct.std::__2::__compressed_pair_elem.3"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.3"*, %"struct.std::__2::__compressed_pair_elem.3"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.3", %"struct.std::__2::__compressed_pair_elem.3"* %this1, i32 0, i32 0
  ret %"class.draco::IndexType"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType"* @_ZNSt3__28__fill_nIPN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEEmS4_EET_S6_T0_RKT1_(%"class.draco::IndexType"* %__first, i32 %__n, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %__value_) #0 comdat {
entry:
  %__first.addr = alloca %"class.draco::IndexType"*, align 4
  %__n.addr = alloca i32, align 4
  %__value_.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.draco::IndexType"* %__first, %"class.draco::IndexType"** %__first.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store %"class.draco::IndexType"* %__value_, %"class.draco::IndexType"** %__value_.addr, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %__n.addr, align 4
  %cmp = icmp ugt i32 %0, 0
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__value_.addr, align 4
  %2 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__first.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZN5draco9IndexTypeIjNS_21VertexIndex_tag_type_EEaSERKS2_(%"class.draco::IndexType"* %2, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %1)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %3 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__first.addr, align 4
  %incdec.ptr = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %3, i32 1
  store %"class.draco::IndexType"* %incdec.ptr, %"class.draco::IndexType"** %__first.addr, align 4
  %4 = load i32, i32* %__n.addr, align 4
  %dec = add i32 %4, -1
  store i32 %dec, i32* %__n.addr, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %5 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__first.addr, align 4
  ret %"class.draco::IndexType"* %5
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__221__convert_to_integralEm(i32 %__val) #0 comdat {
entry:
  %__val.addr = alloca i32, align 4
  store i32 %__val, i32* %__val.addr, align 4
  %0 = load i32, i32* %__val.addr, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>>>::_ConstructTransaction"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionC2ERS7_m(%"struct.std::__2::vector<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>>>::_ConstructTransaction"* returned %this, %"class.std::__2::vector.1"* nonnull align 4 dereferenceable(12) %__v, i32 %__n) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>>>::_ConstructTransaction"*, align 4
  %__v.addr = alloca %"class.std::__2::vector.1"*, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>>>::_ConstructTransaction"** %this.addr, align 4
  store %"class.std::__2::vector.1"* %__v, %"class.std::__2::vector.1"** %__v.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>>>::_ConstructTransaction"*, %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>>>::_ConstructTransaction"** %this.addr, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %__v.addr, align 4
  store %"class.std::__2::vector.1"* %0, %"class.std::__2::vector.1"** %__v_, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %1 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %__v.addr, align 4
  %2 = bitcast %"class.std::__2::vector.1"* %1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %2, i32 0, i32 1
  %3 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__end_, align 4
  store %"class.draco::IndexType"* %3, %"class.draco::IndexType"** %__pos_, align 4
  %__new_end_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>>>::_ConstructTransaction"* %this1, i32 0, i32 2
  %4 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %__v.addr, align 4
  %5 = bitcast %"class.std::__2::vector.1"* %4 to %"class.std::__2::__vector_base"*
  %__end_2 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %5, i32 0, i32 1
  %6 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__end_2, align 4
  %7 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %6, i32 %7
  store %"class.draco::IndexType"* %add.ptr, %"class.draco::IndexType"** %__new_end_, align 4
  ret %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEEEEE9constructIS5_JRKS5_EEEvRS6_PT_DpOT0_(%"class.std::__2::allocator.5"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType"* %__p, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %__args) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.5"*, align 4
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  %__args.addr = alloca %"class.draco::IndexType"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_construct", align 1
  store %"class.std::__2::allocator.5"* %__a, %"class.std::__2::allocator.5"** %__a.addr, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  store %"class.draco::IndexType"* %__args, %"class.draco::IndexType"** %__args.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_construct"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.5"*, %"class.std::__2::allocator.5"** %__a.addr, align 4
  %2 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  %3 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZNSt3__27forwardIRKN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %3) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEEEEE11__constructIS5_JRKS5_EEEvNS_17integral_constantIbLb1EEERS6_PT_DpOT0_(%"class.std::__2::allocator.5"* nonnull align 1 dereferenceable(1) %1, %"class.draco::IndexType"* %2, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.5"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.5"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.2"* %__end_cap_) #8
  ret %"class.std::__2::allocator.5"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType"* %__p) #0 comdat {
entry:
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  ret %"class.draco::IndexType"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>>>::_ConstructTransaction"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>>>::_ConstructTransaction"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>>>::_ConstructTransaction"*, align 4
  store %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>>>::_ConstructTransaction"** %this.addr, align 4
  %this1 = load %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>>>::_ConstructTransaction"*, %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>>>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__pos_, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %__v_, align 4
  %2 = bitcast %"class.std::__2::vector.1"* %1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %2, i32 0, i32 1
  store %"class.draco::IndexType"* %0, %"class.draco::IndexType"** %__end_, align 4
  ret %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::VertexIndex_tag_type_>>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEEEEE11__constructIS5_JRKS5_EEEvNS_17integral_constantIbLb1EEERS6_PT_DpOT0_(%"class.std::__2::allocator.5"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType"* %__p, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %__args) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.5"*, align 4
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  %__args.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.std::__2::allocator.5"* %__a, %"class.std::__2::allocator.5"** %__a.addr, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  store %"class.draco::IndexType"* %__args, %"class.draco::IndexType"** %__args.addr, align 4
  %1 = load %"class.std::__2::allocator.5"*, %"class.std::__2::allocator.5"** %__a.addr, align 4
  %2 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  %3 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZNSt3__27forwardIRKN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %3) #8
  call void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEEE9constructIS4_JRKS4_EEEvPT_DpOT0_(%"class.std::__2::allocator.5"* %1, %"class.draco::IndexType"* %2, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZNSt3__27forwardIRKN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.draco::IndexType"* %__t, %"class.draco::IndexType"** %__t.addr, align 4
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__t.addr, align 4
  ret %"class.draco::IndexType"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEEE9constructIS4_JRKS4_EEEvPT_DpOT0_(%"class.std::__2::allocator.5"* %this, %"class.draco::IndexType"* %__p, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %__args) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.5"*, align 4
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  %__args.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.std::__2::allocator.5"* %this, %"class.std::__2::allocator.5"** %this.addr, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  store %"class.draco::IndexType"* %__args, %"class.draco::IndexType"** %__args.addr, align 4
  %this1 = load %"class.std::__2::allocator.5"*, %"class.std::__2::allocator.5"** %this.addr, align 4
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  %1 = bitcast %"class.draco::IndexType"* %0 to i8*
  %2 = bitcast i8* %1 to %"class.draco::IndexType"*
  %3 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZNSt3__27forwardIRKN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %3) #8
  %4 = bitcast %"class.draco::IndexType"* %2 to i8*
  %5 = bitcast %"class.draco::IndexType"* %call to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 4, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.5"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.2"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.2"*, align 4
  store %"class.std::__2::__compressed_pair.2"* %this, %"class.std::__2::__compressed_pair.2"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.2"*, %"class.std::__2::__compressed_pair.2"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.2"* %this1 to %"struct.std::__2::__compressed_pair_elem.4"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.5"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.4"* %0) #8
  ret %"class.std::__2::allocator.5"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.5"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.4"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.4"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.4"* %this, %"struct.std::__2::__compressed_pair_elem.4"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.4"*, %"struct.std::__2::__compressed_pair_elem.4"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.4"* %this1 to %"class.std::__2::allocator.5"*
  ret %"class.std::__2::allocator.5"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE27__invalidate_iterators_pastEPS4_(%"class.std::__2::vector.1"* %this, %"class.draco::IndexType"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.1"*, align 4
  %__new_last.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.std::__2::vector.1"* %this, %"class.std::__2::vector.1"** %this.addr, align 4
  store %"class.draco::IndexType"* %__new_last, %"class.draco::IndexType"** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_(%"class.std::__2::__vector_base"* %this, %"class.draco::IndexType"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  %__new_last.addr = alloca %"class.draco::IndexType"*, align 4
  %__soon_to_be_end = alloca %"class.draco::IndexType"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  store %"class.draco::IndexType"* %__new_last, %"class.draco::IndexType"** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 1
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__end_, align 4
  store %"class.draco::IndexType"* %0, %"class.draco::IndexType"** %__soon_to_be_end, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__new_last.addr, align 4
  %2 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__soon_to_be_end, align 4
  %cmp = icmp ne %"class.draco::IndexType"* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.5"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base"* %this1) #8
  %3 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__soon_to_be_end, align 4
  %incdec.ptr = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %3, i32 -1
  store %"class.draco::IndexType"* %incdec.ptr, %"class.draco::IndexType"** %__soon_to_be_end, align 4
  %call2 = call %"class.draco::IndexType"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType"* %incdec.ptr) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEEEEE7destroyIS5_EEvRS6_PT_(%"class.std::__2::allocator.5"* nonnull align 1 dereferenceable(1) %call, %"class.draco::IndexType"* %call2)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %4 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__new_last.addr, align 4
  %__end_3 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 1
  store %"class.draco::IndexType"* %4, %"class.draco::IndexType"** %__end_3, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_shrinkEm(%"class.std::__2::vector.1"* %this, i32 %__old_size) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.1"*, align 4
  %__old_size.addr = alloca i32, align 4
  store %"class.std::__2::vector.1"* %this, %"class.std::__2::vector.1"** %this.addr, align 4
  store i32 %__old_size, i32* %__old_size.addr, align 4
  %this1 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %this.addr, align 4
  %call = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.1"* %this1) #8
  %0 = bitcast %"class.draco::IndexType"* %call to i8*
  %call2 = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.1"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.1"* %this1) #8
  %add.ptr = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %call2, i32 %call3
  %1 = bitcast %"class.draco::IndexType"* %add.ptr to i8*
  %call4 = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.1"* %this1) #8
  %2 = load i32, i32* %__old_size.addr, align 4
  %add.ptr5 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %call4, i32 %2
  %3 = bitcast %"class.draco::IndexType"* %add.ptr5 to i8*
  %call6 = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.1"* %this1) #8
  %call7 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.1"* %this1) #8
  %add.ptr8 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %call6, i32 %call7
  %4 = bitcast %"class.draco::IndexType"* %add.ptr8 to i8*
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_(%"class.std::__2::vector.1"* %this1, i8* %0, i8* %1, i8* %3, i8* %4) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEEEEE7destroyIS5_EEvRS6_PT_(%"class.std::__2::allocator.5"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType"* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.5"*, align 4
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy.151", align 1
  store %"class.std::__2::allocator.5"* %__a, %"class.std::__2::allocator.5"** %__a.addr, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy.151"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.5"*, %"class.std::__2::allocator.5"** %__a.addr, align 4
  %2 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEEEEE9__destroyIS5_EEvNS_17integral_constantIbLb1EEERS6_PT_(%"class.std::__2::allocator.5"* nonnull align 1 dereferenceable(1) %1, %"class.draco::IndexType"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEEEEE9__destroyIS5_EEvNS_17integral_constantIbLb1EEERS6_PT_(%"class.std::__2::allocator.5"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType"* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.5"*, align 4
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.std::__2::allocator.5"* %__a, %"class.std::__2::allocator.5"** %__a.addr, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.5"*, %"class.std::__2::allocator.5"** %__a.addr, align 4
  %2 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  call void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEEE7destroyEPS4_(%"class.std::__2::allocator.5"* %1, %"class.draco::IndexType"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEEE7destroyEPS4_(%"class.std::__2::allocator.5"* %this, %"class.draco::IndexType"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.5"*, align 4
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.std::__2::allocator.5"* %this, %"class.std::__2::allocator.5"** %this.addr, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.5"*, %"class.std::__2::allocator.5"** %this.addr, align 4
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_(%"class.std::__2::vector.1"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.1"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector.1"* %this, %"class.std::__2::vector.1"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.1"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.1"*, align 4
  store %"class.std::__2::vector.1"* %this, %"class.std::__2::vector.1"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 0
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  %call = call %"class.draco::IndexType"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType"* %1) #8
  ret %"class.draco::IndexType"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE5clearEv(%"class.std::__2::vector.1"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.1"*, align 4
  %__old_size = alloca i32, align 4
  store %"class.std::__2::vector.1"* %this, %"class.std::__2::vector.1"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.1"* %this1) #8
  store i32 %call, i32* %__old_size, align 4
  %0 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base"*
  call void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE5clearEv(%"class.std::__2::__vector_base"* %0) #8
  %1 = load i32, i32* %__old_size, align 4
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_shrinkEm(%"class.std::__2::vector.1"* %this1, i32 %1) #8
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector.1"* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEEEEE10deallocateERS6_PS5_m(%"class.std::__2::allocator.5"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType"* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.5"*, align 4
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.5"* %__a, %"class.std::__2::allocator.5"** %__a.addr, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.5"*, %"class.std::__2::allocator.5"** %__a.addr, align 4
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEEE10deallocateEPS4_m(%"class.std::__2::allocator.5"* %0, %"class.draco::IndexType"* %1, i32 %2) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.2"* %__end_cap_) #8
  ret %"class.draco::IndexType"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE5clearEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  call void @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE17__destruct_at_endEPS4_(%"class.std::__2::__vector_base"* %this1, %"class.draco::IndexType"* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEEE10deallocateEPS4_m(%"class.std::__2::allocator.5"* %this, %"class.draco::IndexType"* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.5"*, align 4
  %__p.addr = alloca %"class.draco::IndexType"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.5"* %this, %"class.std::__2::allocator.5"** %this.addr, align 4
  store %"class.draco::IndexType"* %__p, %"class.draco::IndexType"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.5"*, %"class.std::__2::allocator.5"** %this.addr, align 4
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__p.addr, align 4
  %1 = bitcast %"class.draco::IndexType"* %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.2"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.2"*, align 4
  store %"class.std::__2::__compressed_pair.2"* %this, %"class.std::__2::__compressed_pair.2"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.2"*, %"class.std::__2::__compressed_pair.2"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.2"* %this1 to %"struct.std::__2::__compressed_pair_elem.3"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.3"* %0) #8
  ret %"class.draco::IndexType"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"** @_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.3"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.3"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.3"* %this, %"struct.std::__2::__compressed_pair_elem.3"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.3"*, %"struct.std::__2::__compressed_pair_elem.3"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.3", %"struct.std::__2::__compressed_pair_elem.3"* %this1, i32 0, i32 0
  ret %"class.draco::IndexType"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE8max_sizeEv(%"class.std::__2::vector.1"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.1"*, align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp3 = alloca i32, align 4
  store %"class.std::__2::vector.1"* %this, %"class.std::__2::vector.1"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.5"* @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base"* %0) #8
  %call2 = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEEEEE8max_sizeERKS6_(%"class.std::__2::allocator.5"* nonnull align 1 dereferenceable(1) %call) #8
  store i32 %call2, i32* %ref.tmp, align 4
  %call4 = call i32 @_ZNSt3__214numeric_limitsIlE3maxEv() #8
  store i32 %call4, i32* %ref.tmp3, align 4
  %call5 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %1 = load i32, i32* %call5, align 4
  ret i32 %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType"* @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEEEEE8allocateERS6_m(%"class.std::__2::allocator.5"* nonnull align 1 dereferenceable(1) %__a, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.5"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.5"* %__a, %"class.std::__2::allocator.5"** %__a.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.5"*, %"class.std::__2::allocator.5"** %__a.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call %"class.draco::IndexType"* @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEEE8allocateEmPKv(%"class.std::__2::allocator.5"* %0, i32 %1, i8* null)
  ret %"class.draco::IndexType"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE14__annotate_newEm(%"class.std::__2::vector.1"* %this, i32 %__current_size) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.1"*, align 4
  %__current_size.addr = alloca i32, align 4
  store %"class.std::__2::vector.1"* %this, %"class.std::__2::vector.1"** %this.addr, align 4
  store i32 %__current_size, i32* %__current_size.addr, align 4
  %this1 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %this.addr, align 4
  %call = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.1"* %this1) #8
  %0 = bitcast %"class.draco::IndexType"* %call to i8*
  %call2 = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.1"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.1"* %this1) #8
  %add.ptr = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %call2, i32 %call3
  %1 = bitcast %"class.draco::IndexType"* %add.ptr to i8*
  %call4 = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.1"* %this1) #8
  %call5 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.1"* %this1) #8
  %add.ptr6 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %call4, i32 %call5
  %2 = bitcast %"class.draco::IndexType"* %add.ptr6 to i8*
  %call7 = call %"class.draco::IndexType"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.1"* %this1) #8
  %3 = load i32, i32* %__current_size.addr, align 4
  %add.ptr8 = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %call7, i32 %3
  %4 = bitcast %"class.draco::IndexType"* %add.ptr8 to i8*
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_(%"class.std::__2::vector.1"* %this1, i8* %0, i8* %1, i8* %2, i8* %4) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEEEEE8max_sizeERKS6_(%"class.std::__2::allocator.5"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.5"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_max_size.152", align 1
  store %"class.std::__2::allocator.5"* %__a, %"class.std::__2::allocator.5"** %__a.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_max_size.152"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.5"*, %"class.std::__2::allocator.5"** %__a.addr, align 4
  %call = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS6_(%"class.std::__2::allocator.5"* nonnull align 1 dereferenceable(1) %1) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.5"* @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.5"* @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.2"* %__end_cap_) #8
  ret %"class.std::__2::allocator.5"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__214numeric_limitsIlE3maxEv() #0 comdat {
entry:
  %call = call i32 @_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv() #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS6_(%"class.std::__2::allocator.5"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.5"*, align 4
  store %"class.std::__2::allocator.5"* %__a, %"class.std::__2::allocator.5"** %__a.addr, align 4
  %1 = load %"class.std::__2::allocator.5"*, %"class.std::__2::allocator.5"** %__a.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEEE8max_sizeEv(%"class.std::__2::allocator.5"* %1) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__29allocatorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEEE8max_sizeEv(%"class.std::__2::allocator.5"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.5"*, align 4
  store %"class.std::__2::allocator.5"* %this, %"class.std::__2::allocator.5"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.5"*, %"class.std::__2::allocator.5"** %this.addr, align 4
  ret i32 1073741823
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.5"* @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.2"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.2"*, align 4
  store %"class.std::__2::__compressed_pair.2"* %this, %"class.std::__2::__compressed_pair.2"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.2"*, %"class.std::__2::__compressed_pair.2"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.2"* %this1 to %"struct.std::__2::__compressed_pair_elem.4"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.5"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.4"* %0) #8
  ret %"class.std::__2::allocator.5"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.5"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_21VertexIndex_tag_type_EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.4"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.4"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.4"* %this, %"struct.std::__2::__compressed_pair_elem.4"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.4"*, %"struct.std::__2::__compressed_pair_elem.4"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.4"* %this1 to %"class.std::__2::allocator.5"*
  ret %"class.std::__2::allocator.5"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv() #0 comdat {
entry:
  ret i32 2147483647
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType"* @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEEE8allocateEmPKv(%"class.std::__2::allocator.5"* %this, i32 %__n, i8* %0) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.5"*, align 4
  %__n.addr = alloca i32, align 4
  %.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.5"* %this, %"class.std::__2::allocator.5"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::allocator.5"*, %"class.std::__2::allocator.5"** %this.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEEE8max_sizeEv(%"class.std::__2::allocator.5"* %this1) #8
  %cmp = icmp ugt i32 %1, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__220__throw_length_errorEPKc(i8* getelementptr inbounds ([68 x i8], [68 x i8]* @.str, i32 0, i32 0)) #10
  unreachable

if.end:                                           ; preds = %entry
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  %call2 = call i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %mul, i32 4)
  %3 = bitcast i8* %call2 to %"class.draco::IndexType"*
  ret %"class.draco::IndexType"* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.15"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.15"*, align 4
  store %"class.std::__2::vector.15"* %this, %"class.std::__2::vector.15"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.15"*, %"class.std::__2::vector.15"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.15"* %this1 to %"class.std::__2::__vector_base.16"*
  %call = call i32 @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::__vector_base.16"* %0) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.21"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.16"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.16"*, align 4
  store %"class.std::__2::__vector_base.16"* %this, %"class.std::__2::__vector_base.16"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.16"*, %"class.std::__2::__vector_base.16"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.16", %"class.std::__2::__vector_base.16"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.21"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.18"* %__end_cap_) #8
  ret %"class.std::__2::allocator.21"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.15"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.15"*, align 4
  store %"class.std::__2::vector.15"* %this, %"class.std::__2::vector.15"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.15"*, %"class.std::__2::vector.15"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.15"* %this1 to %"class.std::__2::__vector_base.16"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.16", %"class.std::__2::__vector_base.16"* %0, i32 0, i32 1
  %1 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector.15"* %this1 to %"class.std::__2::__vector_base.16"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.16", %"class.std::__2::__vector_base.16"* %2, i32 0, i32 0
  %3 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.draco::IndexType.17"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.draco::IndexType.17"* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEEC2EmmS7_(%"struct.std::__2::__split_buffer"* returned %this, i32 %__cap, i32 %__start, %"class.std::__2::allocator.21"* nonnull align 1 dereferenceable(1) %__a) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"struct.std::__2::__split_buffer"*, align 4
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__cap.addr = alloca i32, align 4
  %__start.addr = alloca i32, align 4
  %__a.addr = alloca %"class.std::__2::allocator.21"*, align 4
  %ref.tmp = alloca i8*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store i32 %__cap, i32* %__cap.addr, align 4
  store i32 %__start, i32* %__start.addr, align 4
  store %"class.std::__2::allocator.21"* %__a, %"class.std::__2::allocator.21"** %__a.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer"* %this1, %"struct.std::__2::__split_buffer"** %retval, align 4
  %0 = bitcast %"struct.std::__2::__split_buffer"* %this1 to %"class.std::__2::__split_buffer_common"*
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  store i8* null, i8** %ref.tmp, align 4
  %1 = load %"class.std::__2::allocator.21"*, %"class.std::__2::allocator.21"** %__a.addr, align 4
  %call = call %"class.std::__2::__compressed_pair.153"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEEC2IDnS8_EEOT_OT0_(%"class.std::__2::__compressed_pair.153"* %__end_cap_, i8** nonnull align 4 dereferenceable(4) %ref.tmp, %"class.std::__2::allocator.21"* nonnull align 1 dereferenceable(1) %1)
  %2 = load i32, i32* %__cap.addr, align 4
  %cmp = icmp ne i32 %2, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.21"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE7__allocEv(%"struct.std::__2::__split_buffer"* %this1) #8
  %3 = load i32, i32* %__cap.addr, align 4
  %call3 = call %"class.draco::IndexType.17"* @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE8allocateERS6_m(%"class.std::__2::allocator.21"* nonnull align 1 dereferenceable(1) %call2, i32 %3)
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %"class.draco::IndexType.17"* [ %call3, %cond.true ], [ null, %cond.false ]
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  store %"class.draco::IndexType.17"* %cond, %"class.draco::IndexType.17"** %__first_, align 4
  %__first_4 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %4 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %__first_4, align 4
  %5 = load i32, i32* %__start.addr, align 4
  %add.ptr = getelementptr inbounds %"class.draco::IndexType.17", %"class.draco::IndexType.17"* %4, i32 %5
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 2
  store %"class.draco::IndexType.17"* %add.ptr, %"class.draco::IndexType.17"** %__end_, align 4
  %__begin_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 1
  store %"class.draco::IndexType.17"* %add.ptr, %"class.draco::IndexType.17"** %__begin_, align 4
  %__first_5 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %6 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %__first_5, align 4
  %7 = load i32, i32* %__cap.addr, align 4
  %add.ptr6 = getelementptr inbounds %"class.draco::IndexType.17", %"class.draco::IndexType.17"* %6, i32 %7
  %call7 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.17"** @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this1) #8
  store %"class.draco::IndexType.17"* %add.ptr6, %"class.draco::IndexType.17"** %call7, align 4
  %8 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %retval, align 4
  ret %"struct.std::__2::__split_buffer"* %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS4_RS6_EE(%"class.std::__2::vector.15"* %this, %"struct.std::__2::__split_buffer"* nonnull align 4 dereferenceable(20) %__v) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.15"*, align 4
  %__v.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"class.std::__2::vector.15"* %this, %"class.std::__2::vector.15"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer"* %__v, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %this1 = load %"class.std::__2::vector.15"*, %"class.std::__2::vector.15"** %this.addr, align 4
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_deleteEv(%"class.std::__2::vector.15"* %this1) #8
  %0 = bitcast %"class.std::__2::vector.15"* %this1 to %"class.std::__2::__vector_base.16"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.21"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.16"* %0) #8
  %1 = bitcast %"class.std::__2::vector.15"* %this1 to %"class.std::__2::__vector_base.16"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.16", %"class.std::__2::__vector_base.16"* %1, i32 0, i32 0
  %2 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %__begin_, align 4
  %3 = bitcast %"class.std::__2::vector.15"* %this1 to %"class.std::__2::__vector_base.16"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.16", %"class.std::__2::__vector_base.16"* %3, i32 0, i32 1
  %4 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %__end_, align 4
  %5 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__begin_2 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %5, i32 0, i32 1
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE46__construct_backward_with_exception_guaranteesIS5_EENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS6_PT_SC_EE5valuesr31is_trivially_move_constructibleISC_EE5valueEvE4typeERS6_SD_SD_RSD_(%"class.std::__2::allocator.21"* nonnull align 1 dereferenceable(1) %call, %"class.draco::IndexType.17"* %2, %"class.draco::IndexType.17"* %4, %"class.draco::IndexType.17"** nonnull align 4 dereferenceable(4) %__begin_2)
  %6 = bitcast %"class.std::__2::vector.15"* %this1 to %"class.std::__2::__vector_base.16"*
  %__begin_3 = getelementptr inbounds %"class.std::__2::__vector_base.16", %"class.std::__2::__vector_base.16"* %6, i32 0, i32 0
  %7 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__begin_4 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %7, i32 0, i32 1
  call void @_ZNSt3__24swapIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS7_EE5valueEvE4typeERS7_SA_(%"class.draco::IndexType.17"** nonnull align 4 dereferenceable(4) %__begin_3, %"class.draco::IndexType.17"** nonnull align 4 dereferenceable(4) %__begin_4) #8
  %8 = bitcast %"class.std::__2::vector.15"* %this1 to %"class.std::__2::__vector_base.16"*
  %__end_5 = getelementptr inbounds %"class.std::__2::__vector_base.16", %"class.std::__2::__vector_base.16"* %8, i32 0, i32 1
  %9 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__end_6 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %9, i32 0, i32 2
  call void @_ZNSt3__24swapIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS7_EE5valueEvE4typeERS7_SA_(%"class.draco::IndexType.17"** nonnull align 4 dereferenceable(4) %__end_5, %"class.draco::IndexType.17"** nonnull align 4 dereferenceable(4) %__end_6) #8
  %10 = bitcast %"class.std::__2::vector.15"* %this1 to %"class.std::__2::__vector_base.16"*
  %call7 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.17"** @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base.16"* %10) #8
  %11 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %call8 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.17"** @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE9__end_capEv(%"struct.std::__2::__split_buffer"* %11) #8
  call void @_ZNSt3__24swapIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS7_EE5valueEvE4typeERS7_SA_(%"class.draco::IndexType.17"** nonnull align 4 dereferenceable(4) %call7, %"class.draco::IndexType.17"** nonnull align 4 dereferenceable(4) %call8) #8
  %12 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__begin_9 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %12, i32 0, i32 1
  %13 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %__begin_9, align 4
  %14 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %14, i32 0, i32 0
  store %"class.draco::IndexType.17"* %13, %"class.draco::IndexType.17"** %__first_, align 4
  %call10 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.15"* %this1) #8
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE14__annotate_newEm(%"class.std::__2::vector.15"* %this1, i32 %call10) #8
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector.15"* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEED2Ev(%"struct.std::__2::__split_buffer"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"struct.std::__2::__split_buffer"*, align 4
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer"* %this1, %"struct.std::__2::__split_buffer"** %retval, align 4
  call void @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE5clearEv(%"struct.std::__2::__split_buffer"* %this1) #8
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %__first_, align 4
  %tobool = icmp ne %"class.draco::IndexType.17"* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.21"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE7__allocEv(%"struct.std::__2::__split_buffer"* %this1) #8
  %__first_2 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %1 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %__first_2, align 4
  %call3 = call i32 @_ZNKSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE8capacityEv(%"struct.std::__2::__split_buffer"* %this1)
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE10deallocateERS6_PS5_m(%"class.std::__2::allocator.21"* nonnull align 1 dereferenceable(1) %call, %"class.draco::IndexType.17"* %1, i32 %call3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %retval, align 4
  ret %"struct.std::__2::__split_buffer"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::__vector_base.16"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.16"*, align 4
  store %"class.std::__2::__vector_base.16"* %this, %"class.std::__2::__vector_base.16"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.16"*, %"class.std::__2::__vector_base.16"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.17"** @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base.16"* %this1) #8
  %0 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.16", %"class.std::__2::__vector_base.16"* %this1, i32 0, i32 0
  %1 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.draco::IndexType.17"* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.draco::IndexType.17"* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.17"** @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base.16"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.16"*, align 4
  store %"class.std::__2::__vector_base.16"* %this, %"class.std::__2::__vector_base.16"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.16"*, %"class.std::__2::__vector_base.16"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.16", %"class.std::__2::__vector_base.16"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.17"** @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.18"* %__end_cap_) #8
  ret %"class.draco::IndexType.17"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.17"** @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.18"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.18"*, align 4
  store %"class.std::__2::__compressed_pair.18"* %this, %"class.std::__2::__compressed_pair.18"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.18"*, %"class.std::__2::__compressed_pair.18"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.18"* %this1 to %"struct.std::__2::__compressed_pair_elem.19"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.17"** @_ZNKSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.19"* %0) #8
  ret %"class.draco::IndexType.17"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.17"** @_ZNKSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.19"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.19"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.19"* %this, %"struct.std::__2::__compressed_pair_elem.19"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.19"*, %"struct.std::__2::__compressed_pair_elem.19"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.19", %"struct.std::__2::__compressed_pair_elem.19"* %this1, i32 0, i32 0
  ret %"class.draco::IndexType.17"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.21"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.18"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.18"*, align 4
  store %"class.std::__2::__compressed_pair.18"* %this, %"class.std::__2::__compressed_pair.18"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.18"*, %"class.std::__2::__compressed_pair.18"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.18"* %this1 to %"struct.std::__2::__compressed_pair_elem.20"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.21"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.20"* %0) #8
  ret %"class.std::__2::allocator.21"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.21"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.20"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.20"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.20"* %this, %"struct.std::__2::__compressed_pair_elem.20"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.20"*, %"struct.std::__2::__compressed_pair_elem.20"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.20"* %this1 to %"class.std::__2::allocator.21"*
  ret %"class.std::__2::allocator.21"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.153"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEEC2IDnS8_EEOT_OT0_(%"class.std::__2::__compressed_pair.153"* returned %this, i8** nonnull align 4 dereferenceable(4) %__t1, %"class.std::__2::allocator.21"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.153"*, align 4
  %__t1.addr = alloca i8**, align 4
  %__t2.addr = alloca %"class.std::__2::allocator.21"*, align 4
  store %"class.std::__2::__compressed_pair.153"* %this, %"class.std::__2::__compressed_pair.153"** %this.addr, align 4
  store i8** %__t1, i8*** %__t1.addr, align 4
  store %"class.std::__2::allocator.21"* %__t2, %"class.std::__2::allocator.21"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.153"*, %"class.std::__2::__compressed_pair.153"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.153"* %this1 to %"struct.std::__2::__compressed_pair_elem.19"*
  %1 = load i8**, i8*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %1) #8
  %call2 = call %"struct.std::__2::__compressed_pair_elem.19"* @_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.19"* %0, i8** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.153"* %this1 to i8*
  %3 = getelementptr inbounds i8, i8* %2, i32 4
  %4 = bitcast i8* %3 to %"struct.std::__2::__compressed_pair_elem.154"*
  %5 = load %"class.std::__2::allocator.21"*, %"class.std::__2::allocator.21"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.21"* @_ZNSt3__27forwardIRNS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEEEOT_RNS_16remove_referenceIS8_E4typeE(%"class.std::__2::allocator.21"* nonnull align 1 dereferenceable(1) %5) #8
  %call4 = call %"struct.std::__2::__compressed_pair_elem.154"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb0EEC2IS7_vEEOT_(%"struct.std::__2::__compressed_pair_elem.154"* %4, %"class.std::__2::allocator.21"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::__compressed_pair.153"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType.17"* @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE8allocateERS6_m(%"class.std::__2::allocator.21"* nonnull align 1 dereferenceable(1) %__a, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.21"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.21"* %__a, %"class.std::__2::allocator.21"** %__a.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.21"*, %"class.std::__2::allocator.21"** %__a.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call %"class.draco::IndexType.17"* @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE8allocateEmPKv(%"class.std::__2::allocator.21"* %0, i32 %1, i8* null)
  ret %"class.draco::IndexType.17"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.21"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE7__allocEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.21"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.153"* %__end_cap_) #8
  ret %"class.std::__2::allocator.21"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.17"** @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.17"** @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.153"* %__end_cap_) #8
  ret %"class.draco::IndexType.17"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.21"* @_ZNSt3__27forwardIRNS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEEEOT_RNS_16remove_referenceIS8_E4typeE(%"class.std::__2::allocator.21"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::allocator.21"*, align 4
  store %"class.std::__2::allocator.21"* %__t, %"class.std::__2::allocator.21"** %__t.addr, align 4
  %0 = load %"class.std::__2::allocator.21"*, %"class.std::__2::allocator.21"** %__t.addr, align 4
  ret %"class.std::__2::allocator.21"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.154"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb0EEC2IS7_vEEOT_(%"struct.std::__2::__compressed_pair_elem.154"* returned %this, %"class.std::__2::allocator.21"* nonnull align 1 dereferenceable(1) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.154"*, align 4
  %__u.addr = alloca %"class.std::__2::allocator.21"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.154"* %this, %"struct.std::__2::__compressed_pair_elem.154"** %this.addr, align 4
  store %"class.std::__2::allocator.21"* %__u, %"class.std::__2::allocator.21"** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.154"*, %"struct.std::__2::__compressed_pair_elem.154"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.154", %"struct.std::__2::__compressed_pair_elem.154"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::allocator.21"*, %"class.std::__2::allocator.21"** %__u.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.21"* @_ZNSt3__27forwardIRNS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEEEOT_RNS_16remove_referenceIS8_E4typeE(%"class.std::__2::allocator.21"* nonnull align 1 dereferenceable(1) %0) #8
  store %"class.std::__2::allocator.21"* %call, %"class.std::__2::allocator.21"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.154"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType.17"* @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE8allocateEmPKv(%"class.std::__2::allocator.21"* %this, i32 %__n, i8* %0) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.21"*, align 4
  %__n.addr = alloca i32, align 4
  %.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.21"* %this, %"class.std::__2::allocator.21"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::allocator.21"*, %"class.std::__2::allocator.21"** %this.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE8max_sizeEv(%"class.std::__2::allocator.21"* %this1) #8
  %cmp = icmp ugt i32 %1, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__220__throw_length_errorEPKc(i8* getelementptr inbounds ([68 x i8], [68 x i8]* @.str, i32 0, i32 0)) #10
  unreachable

if.end:                                           ; preds = %entry
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  %call2 = call i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %mul, i32 4)
  %3 = bitcast i8* %call2 to %"class.draco::IndexType.17"*
  ret %"class.draco::IndexType.17"* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE8max_sizeEv(%"class.std::__2::allocator.21"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.21"*, align 4
  store %"class.std::__2::allocator.21"* %this, %"class.std::__2::allocator.21"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.21"*, %"class.std::__2::allocator.21"** %this.addr, align 4
  ret i32 1073741823
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.21"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.153"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.153"*, align 4
  store %"class.std::__2::__compressed_pair.153"* %this, %"class.std::__2::__compressed_pair.153"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.153"*, %"class.std::__2::__compressed_pair.153"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.153"* %this1 to i8*
  %add.ptr = getelementptr inbounds i8, i8* %0, i32 4
  %1 = bitcast i8* %add.ptr to %"struct.std::__2::__compressed_pair_elem.154"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.21"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.154"* %1) #8
  ret %"class.std::__2::allocator.21"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.21"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.154"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.154"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.154"* %this, %"struct.std::__2::__compressed_pair_elem.154"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.154"*, %"struct.std::__2::__compressed_pair_elem.154"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.154", %"struct.std::__2::__compressed_pair_elem.154"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::allocator.21"*, %"class.std::__2::allocator.21"** %__value_, align 4
  ret %"class.std::__2::allocator.21"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.17"** @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.153"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.153"*, align 4
  store %"class.std::__2::__compressed_pair.153"* %this, %"class.std::__2::__compressed_pair.153"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.153"*, %"class.std::__2::__compressed_pair.153"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.153"* %this1 to %"struct.std::__2::__compressed_pair_elem.19"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.17"** @_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.19"* %0) #8
  ret %"class.draco::IndexType.17"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.17"** @_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.19"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.19"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.19"* %this, %"struct.std::__2::__compressed_pair_elem.19"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.19"*, %"struct.std::__2::__compressed_pair_elem.19"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.19", %"struct.std::__2::__compressed_pair_elem.19"* %this1, i32 0, i32 0
  ret %"class.draco::IndexType.17"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_deleteEv(%"class.std::__2::vector.15"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.15"*, align 4
  store %"class.std::__2::vector.15"* %this, %"class.std::__2::vector.15"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.15"*, %"class.std::__2::vector.15"** %this.addr, align 4
  %call = call %"class.draco::IndexType.17"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.15"* %this1) #8
  %0 = bitcast %"class.draco::IndexType.17"* %call to i8*
  %call2 = call %"class.draco::IndexType.17"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.15"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.15"* %this1) #8
  %add.ptr = getelementptr inbounds %"class.draco::IndexType.17", %"class.draco::IndexType.17"* %call2, i32 %call3
  %1 = bitcast %"class.draco::IndexType.17"* %add.ptr to i8*
  %call4 = call %"class.draco::IndexType.17"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.15"* %this1) #8
  %call5 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.15"* %this1) #8
  %add.ptr6 = getelementptr inbounds %"class.draco::IndexType.17", %"class.draco::IndexType.17"* %call4, i32 %call5
  %2 = bitcast %"class.draco::IndexType.17"* %add.ptr6 to i8*
  %call7 = call %"class.draco::IndexType.17"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.15"* %this1) #8
  %call8 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.15"* %this1) #8
  %add.ptr9 = getelementptr inbounds %"class.draco::IndexType.17", %"class.draco::IndexType.17"* %call7, i32 %call8
  %3 = bitcast %"class.draco::IndexType.17"* %add.ptr9 to i8*
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_(%"class.std::__2::vector.15"* %this1, i8* %0, i8* %1, i8* %2, i8* %3) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE46__construct_backward_with_exception_guaranteesIS5_EENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS6_PT_SC_EE5valuesr31is_trivially_move_constructibleISC_EE5valueEvE4typeERS6_SD_SD_RSD_(%"class.std::__2::allocator.21"* nonnull align 1 dereferenceable(1) %0, %"class.draco::IndexType.17"* %__begin1, %"class.draco::IndexType.17"* %__end1, %"class.draco::IndexType.17"** nonnull align 4 dereferenceable(4) %__end2) #0 comdat {
entry:
  %.addr = alloca %"class.std::__2::allocator.21"*, align 4
  %__begin1.addr = alloca %"class.draco::IndexType.17"*, align 4
  %__end1.addr = alloca %"class.draco::IndexType.17"*, align 4
  %__end2.addr = alloca %"class.draco::IndexType.17"**, align 4
  %_Np = alloca i32, align 4
  store %"class.std::__2::allocator.21"* %0, %"class.std::__2::allocator.21"** %.addr, align 4
  store %"class.draco::IndexType.17"* %__begin1, %"class.draco::IndexType.17"** %__begin1.addr, align 4
  store %"class.draco::IndexType.17"* %__end1, %"class.draco::IndexType.17"** %__end1.addr, align 4
  store %"class.draco::IndexType.17"** %__end2, %"class.draco::IndexType.17"*** %__end2.addr, align 4
  %1 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %__end1.addr, align 4
  %2 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %__begin1.addr, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.draco::IndexType.17"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.draco::IndexType.17"* %2 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  store i32 %sub.ptr.div, i32* %_Np, align 4
  %3 = load i32, i32* %_Np, align 4
  %4 = load %"class.draco::IndexType.17"**, %"class.draco::IndexType.17"*** %__end2.addr, align 4
  %5 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %4, align 4
  %idx.neg = sub i32 0, %3
  %add.ptr = getelementptr inbounds %"class.draco::IndexType.17", %"class.draco::IndexType.17"* %5, i32 %idx.neg
  store %"class.draco::IndexType.17"* %add.ptr, %"class.draco::IndexType.17"** %4, align 4
  %6 = load i32, i32* %_Np, align 4
  %cmp = icmp sgt i32 %6, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %7 = load %"class.draco::IndexType.17"**, %"class.draco::IndexType.17"*** %__end2.addr, align 4
  %8 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %7, align 4
  %9 = bitcast %"class.draco::IndexType.17"* %8 to i8*
  %10 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %__begin1.addr, align 4
  %11 = bitcast %"class.draco::IndexType.17"* %10 to i8*
  %12 = load i32, i32* %_Np, align 4
  %mul = mul i32 %12, 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %11, i32 %mul, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__24swapIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS7_EE5valueEvE4typeERS7_SA_(%"class.draco::IndexType.17"** nonnull align 4 dereferenceable(4) %__x, %"class.draco::IndexType.17"** nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %__x.addr = alloca %"class.draco::IndexType.17"**, align 4
  %__y.addr = alloca %"class.draco::IndexType.17"**, align 4
  %__t = alloca %"class.draco::IndexType.17"*, align 4
  store %"class.draco::IndexType.17"** %__x, %"class.draco::IndexType.17"*** %__x.addr, align 4
  store %"class.draco::IndexType.17"** %__y, %"class.draco::IndexType.17"*** %__y.addr, align 4
  %0 = load %"class.draco::IndexType.17"**, %"class.draco::IndexType.17"*** %__x.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.17"** @_ZNSt3__24moveIRPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEONS_16remove_referenceIT_E4typeEOS8_(%"class.draco::IndexType.17"** nonnull align 4 dereferenceable(4) %0) #8
  %1 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %call, align 4
  store %"class.draco::IndexType.17"* %1, %"class.draco::IndexType.17"** %__t, align 4
  %2 = load %"class.draco::IndexType.17"**, %"class.draco::IndexType.17"*** %__y.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.17"** @_ZNSt3__24moveIRPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEONS_16remove_referenceIT_E4typeEOS8_(%"class.draco::IndexType.17"** nonnull align 4 dereferenceable(4) %2) #8
  %3 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %call1, align 4
  %4 = load %"class.draco::IndexType.17"**, %"class.draco::IndexType.17"*** %__x.addr, align 4
  store %"class.draco::IndexType.17"* %3, %"class.draco::IndexType.17"** %4, align 4
  %call2 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.17"** @_ZNSt3__24moveIRPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEONS_16remove_referenceIT_E4typeEOS8_(%"class.draco::IndexType.17"** nonnull align 4 dereferenceable(4) %__t) #8
  %5 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %call2, align 4
  %6 = load %"class.draco::IndexType.17"**, %"class.draco::IndexType.17"*** %__y.addr, align 4
  store %"class.draco::IndexType.17"* %5, %"class.draco::IndexType.17"** %6, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.17"** @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base.16"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.16"*, align 4
  store %"class.std::__2::__vector_base.16"* %this, %"class.std::__2::__vector_base.16"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.16"*, %"class.std::__2::__vector_base.16"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.16", %"class.std::__2::__vector_base.16"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.17"** @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.18"* %__end_cap_) #8
  ret %"class.draco::IndexType.17"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE14__annotate_newEm(%"class.std::__2::vector.15"* %this, i32 %__current_size) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.15"*, align 4
  %__current_size.addr = alloca i32, align 4
  store %"class.std::__2::vector.15"* %this, %"class.std::__2::vector.15"** %this.addr, align 4
  store i32 %__current_size, i32* %__current_size.addr, align 4
  %this1 = load %"class.std::__2::vector.15"*, %"class.std::__2::vector.15"** %this.addr, align 4
  %call = call %"class.draco::IndexType.17"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.15"* %this1) #8
  %0 = bitcast %"class.draco::IndexType.17"* %call to i8*
  %call2 = call %"class.draco::IndexType.17"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.15"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.15"* %this1) #8
  %add.ptr = getelementptr inbounds %"class.draco::IndexType.17", %"class.draco::IndexType.17"* %call2, i32 %call3
  %1 = bitcast %"class.draco::IndexType.17"* %add.ptr to i8*
  %call4 = call %"class.draco::IndexType.17"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.15"* %this1) #8
  %call5 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.15"* %this1) #8
  %add.ptr6 = getelementptr inbounds %"class.draco::IndexType.17", %"class.draco::IndexType.17"* %call4, i32 %call5
  %2 = bitcast %"class.draco::IndexType.17"* %add.ptr6 to i8*
  %call7 = call %"class.draco::IndexType.17"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.15"* %this1) #8
  %3 = load i32, i32* %__current_size.addr, align 4
  %add.ptr8 = getelementptr inbounds %"class.draco::IndexType.17", %"class.draco::IndexType.17"* %call7, i32 %3
  %4 = bitcast %"class.draco::IndexType.17"* %add.ptr8 to i8*
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_(%"class.std::__2::vector.15"* %this1, i8* %0, i8* %1, i8* %2, i8* %4) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector.15"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.15"*, align 4
  store %"class.std::__2::vector.15"* %this, %"class.std::__2::vector.15"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.15"*, %"class.std::__2::vector.15"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_(%"class.std::__2::vector.15"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.15"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector.15"* %this, %"class.std::__2::vector.15"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector.15"*, %"class.std::__2::vector.15"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType.17"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.15"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.15"*, align 4
  store %"class.std::__2::vector.15"* %this, %"class.std::__2::vector.15"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.15"*, %"class.std::__2::vector.15"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.15"* %this1 to %"class.std::__2::__vector_base.16"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.16", %"class.std::__2::__vector_base.16"* %0, i32 0, i32 0
  %1 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %__begin_, align 4
  %call = call %"class.draco::IndexType.17"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType.17"* %1) #8
  ret %"class.draco::IndexType.17"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType.17"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType.17"* %__p) #0 comdat {
entry:
  %__p.addr = alloca %"class.draco::IndexType.17"*, align 4
  store %"class.draco::IndexType.17"* %__p, %"class.draco::IndexType.17"** %__p.addr, align 4
  %0 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %__p.addr, align 4
  ret %"class.draco::IndexType.17"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.17"** @_ZNSt3__24moveIRPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEONS_16remove_referenceIT_E4typeEOS8_(%"class.draco::IndexType.17"** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.draco::IndexType.17"**, align 4
  store %"class.draco::IndexType.17"** %__t, %"class.draco::IndexType.17"*** %__t.addr, align 4
  %0 = load %"class.draco::IndexType.17"**, %"class.draco::IndexType.17"*** %__t.addr, align 4
  ret %"class.draco::IndexType.17"** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.17"** @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.18"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.18"*, align 4
  store %"class.std::__2::__compressed_pair.18"* %this, %"class.std::__2::__compressed_pair.18"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.18"*, %"class.std::__2::__compressed_pair.18"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.18"* %this1 to %"struct.std::__2::__compressed_pair_elem.19"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.17"** @_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.19"* %0) #8
  ret %"class.draco::IndexType.17"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE5clearEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 1
  %0 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %__begin_, align 4
  call void @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE17__destruct_at_endEPS4_(%"struct.std::__2::__split_buffer"* %this1, %"class.draco::IndexType.17"* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE10deallocateERS6_PS5_m(%"class.std::__2::allocator.21"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType.17"* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.21"*, align 4
  %__p.addr = alloca %"class.draco::IndexType.17"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.21"* %__a, %"class.std::__2::allocator.21"** %__a.addr, align 4
  store %"class.draco::IndexType.17"* %__p, %"class.draco::IndexType.17"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.21"*, %"class.std::__2::allocator.21"** %__a.addr, align 4
  %1 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE10deallocateEPS4_m(%"class.std::__2::allocator.21"* %0, %"class.draco::IndexType.17"* %1, i32 %2) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE8capacityEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.17"** @_ZNKSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this1) #8
  %0 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %call, align 4
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %1 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %__first_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.draco::IndexType.17"* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.draco::IndexType.17"* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE17__destruct_at_endEPS4_(%"struct.std::__2::__split_buffer"* %this, %"class.draco::IndexType.17"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__new_last.addr = alloca %"class.draco::IndexType.17"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant.146", align 1
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store %"class.draco::IndexType.17"* %__new_last, %"class.draco::IndexType.17"** %__new_last.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %0 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %__new_last.addr, align 4
  call void @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE17__destruct_at_endEPS4_NS_17integral_constantIbLb0EEE(%"struct.std::__2::__split_buffer"* %this1, %"class.draco::IndexType.17"* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE17__destruct_at_endEPS4_NS_17integral_constantIbLb0EEE(%"struct.std::__2::__split_buffer"* %this, %"class.draco::IndexType.17"* %__new_last) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant.146", align 1
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__new_last.addr = alloca %"class.draco::IndexType.17"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store %"class.draco::IndexType.17"* %__new_last, %"class.draco::IndexType.17"** %__new_last.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %__new_last.addr, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 2
  %2 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %__end_, align 4
  %cmp = icmp ne %"class.draco::IndexType.17"* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.21"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE7__allocEv(%"struct.std::__2::__split_buffer"* %this1) #8
  %__end_2 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 2
  %3 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %__end_2, align 4
  %incdec.ptr = getelementptr inbounds %"class.draco::IndexType.17", %"class.draco::IndexType.17"* %3, i32 -1
  store %"class.draco::IndexType.17"* %incdec.ptr, %"class.draco::IndexType.17"** %__end_2, align 4
  %call3 = call %"class.draco::IndexType.17"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType.17"* %incdec.ptr) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE7destroyIS5_EEvRS6_PT_(%"class.std::__2::allocator.21"* nonnull align 1 dereferenceable(1) %call, %"class.draco::IndexType.17"* %call3)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE7destroyIS5_EEvRS6_PT_(%"class.std::__2::allocator.21"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType.17"* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.21"*, align 4
  %__p.addr = alloca %"class.draco::IndexType.17"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy.155", align 1
  store %"class.std::__2::allocator.21"* %__a, %"class.std::__2::allocator.21"** %__a.addr, align 4
  store %"class.draco::IndexType.17"* %__p, %"class.draco::IndexType.17"** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy.155"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.21"*, %"class.std::__2::allocator.21"** %__a.addr, align 4
  %2 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE9__destroyIS5_EEvNS_17integral_constantIbLb1EEERS6_PT_(%"class.std::__2::allocator.21"* nonnull align 1 dereferenceable(1) %1, %"class.draco::IndexType.17"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE9__destroyIS5_EEvNS_17integral_constantIbLb1EEERS6_PT_(%"class.std::__2::allocator.21"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType.17"* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.21"*, align 4
  %__p.addr = alloca %"class.draco::IndexType.17"*, align 4
  store %"class.std::__2::allocator.21"* %__a, %"class.std::__2::allocator.21"** %__a.addr, align 4
  store %"class.draco::IndexType.17"* %__p, %"class.draco::IndexType.17"** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.21"*, %"class.std::__2::allocator.21"** %__a.addr, align 4
  %2 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %__p.addr, align 4
  call void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE7destroyEPS4_(%"class.std::__2::allocator.21"* %1, %"class.draco::IndexType.17"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE7destroyEPS4_(%"class.std::__2::allocator.21"* %this, %"class.draco::IndexType.17"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.21"*, align 4
  %__p.addr = alloca %"class.draco::IndexType.17"*, align 4
  store %"class.std::__2::allocator.21"* %this, %"class.std::__2::allocator.21"** %this.addr, align 4
  store %"class.draco::IndexType.17"* %__p, %"class.draco::IndexType.17"** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.21"*, %"class.std::__2::allocator.21"** %this.addr, align 4
  %0 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %__p.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE10deallocateEPS4_m(%"class.std::__2::allocator.21"* %this, %"class.draco::IndexType.17"* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.21"*, align 4
  %__p.addr = alloca %"class.draco::IndexType.17"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.21"* %this, %"class.std::__2::allocator.21"** %this.addr, align 4
  store %"class.draco::IndexType.17"* %__p, %"class.draco::IndexType.17"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.21"*, %"class.std::__2::allocator.21"** %this.addr, align 4
  %0 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %__p.addr, align 4
  %1 = bitcast %"class.draco::IndexType.17"* %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.17"** @_ZNKSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.17"** @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.153"* %__end_cap_) #8
  ret %"class.draco::IndexType.17"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.17"** @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.153"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.153"*, align 4
  store %"class.std::__2::__compressed_pair.153"* %this, %"class.std::__2::__compressed_pair.153"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.153"*, %"class.std::__2::__compressed_pair.153"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.153"* %this1 to %"struct.std::__2::__compressed_pair_elem.19"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.17"** @_ZNKSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.19"* %0) #8
  ret %"class.draco::IndexType.17"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.7"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.7"*, align 4
  store %"class.std::__2::vector.7"* %this, %"class.std::__2::vector.7"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.7"*, %"class.std::__2::vector.7"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.7"* %this1 to %"class.std::__2::__vector_base.8"*
  %call = call i32 @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::__vector_base.8"* %0) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.13"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.8"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.8"*, align 4
  store %"class.std::__2::__vector_base.8"* %this, %"class.std::__2::__vector_base.8"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.8"*, %"class.std::__2::__vector_base.8"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.8", %"class.std::__2::__vector_base.8"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.13"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.10"* %__end_cap_) #8
  ret %"class.std::__2::allocator.13"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer.156"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEERNS_9allocatorIS4_EEEC2EmmS7_(%"struct.std::__2::__split_buffer.156"* returned %this, i32 %__cap, i32 %__start, %"class.std::__2::allocator.13"* nonnull align 1 dereferenceable(1) %__a) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"struct.std::__2::__split_buffer.156"*, align 4
  %this.addr = alloca %"struct.std::__2::__split_buffer.156"*, align 4
  %__cap.addr = alloca i32, align 4
  %__start.addr = alloca i32, align 4
  %__a.addr = alloca %"class.std::__2::allocator.13"*, align 4
  %ref.tmp = alloca i8*, align 4
  store %"struct.std::__2::__split_buffer.156"* %this, %"struct.std::__2::__split_buffer.156"** %this.addr, align 4
  store i32 %__cap, i32* %__cap.addr, align 4
  store i32 %__start, i32* %__start.addr, align 4
  store %"class.std::__2::allocator.13"* %__a, %"class.std::__2::allocator.13"** %__a.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.156"*, %"struct.std::__2::__split_buffer.156"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer.156"* %this1, %"struct.std::__2::__split_buffer.156"** %retval, align 4
  %0 = bitcast %"struct.std::__2::__split_buffer.156"* %this1 to %"class.std::__2::__split_buffer_common"*
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer.156", %"struct.std::__2::__split_buffer.156"* %this1, i32 0, i32 3
  store i8* null, i8** %ref.tmp, align 4
  %1 = load %"class.std::__2::allocator.13"*, %"class.std::__2::allocator.13"** %__a.addr, align 4
  %call = call %"class.std::__2::__compressed_pair.157"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEERNS_9allocatorIS4_EEEC2IDnS8_EEOT_OT0_(%"class.std::__2::__compressed_pair.157"* %__end_cap_, i8** nonnull align 4 dereferenceable(4) %ref.tmp, %"class.std::__2::allocator.13"* nonnull align 1 dereferenceable(1) %1)
  %2 = load i32, i32* %__cap.addr, align 4
  %cmp = icmp ne i32 %2, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.13"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEERNS_9allocatorIS4_EEE7__allocEv(%"struct.std::__2::__split_buffer.156"* %this1) #8
  %3 = load i32, i32* %__cap.addr, align 4
  %call3 = call %"class.draco::IndexType.9"* @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_21CornerIndex_tag_type_EEEEEE8allocateERS6_m(%"class.std::__2::allocator.13"* nonnull align 1 dereferenceable(1) %call2, i32 %3)
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %"class.draco::IndexType.9"* [ %call3, %cond.true ], [ null, %cond.false ]
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer.156", %"struct.std::__2::__split_buffer.156"* %this1, i32 0, i32 0
  store %"class.draco::IndexType.9"* %cond, %"class.draco::IndexType.9"** %__first_, align 4
  %__first_4 = getelementptr inbounds %"struct.std::__2::__split_buffer.156", %"struct.std::__2::__split_buffer.156"* %this1, i32 0, i32 0
  %4 = load %"class.draco::IndexType.9"*, %"class.draco::IndexType.9"** %__first_4, align 4
  %5 = load i32, i32* %__start.addr, align 4
  %add.ptr = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %4, i32 %5
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer.156", %"struct.std::__2::__split_buffer.156"* %this1, i32 0, i32 2
  store %"class.draco::IndexType.9"* %add.ptr, %"class.draco::IndexType.9"** %__end_, align 4
  %__begin_ = getelementptr inbounds %"struct.std::__2::__split_buffer.156", %"struct.std::__2::__split_buffer.156"* %this1, i32 0, i32 1
  store %"class.draco::IndexType.9"* %add.ptr, %"class.draco::IndexType.9"** %__begin_, align 4
  %__first_5 = getelementptr inbounds %"struct.std::__2::__split_buffer.156", %"struct.std::__2::__split_buffer.156"* %this1, i32 0, i32 0
  %6 = load %"class.draco::IndexType.9"*, %"class.draco::IndexType.9"** %__first_5, align 4
  %7 = load i32, i32* %__cap.addr, align 4
  %add.ptr6 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %6, i32 %7
  %call7 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.9"** @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEERNS_9allocatorIS4_EEE9__end_capEv(%"struct.std::__2::__split_buffer.156"* %this1) #8
  store %"class.draco::IndexType.9"* %add.ptr6, %"class.draco::IndexType.9"** %call7, align 4
  %8 = load %"struct.std::__2::__split_buffer.156"*, %"struct.std::__2::__split_buffer.156"** %retval, align 4
  ret %"struct.std::__2::__split_buffer.156"* %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS4_RS6_EE(%"class.std::__2::vector.7"* %this, %"struct.std::__2::__split_buffer.156"* nonnull align 4 dereferenceable(20) %__v) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.7"*, align 4
  %__v.addr = alloca %"struct.std::__2::__split_buffer.156"*, align 4
  store %"class.std::__2::vector.7"* %this, %"class.std::__2::vector.7"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer.156"* %__v, %"struct.std::__2::__split_buffer.156"** %__v.addr, align 4
  %this1 = load %"class.std::__2::vector.7"*, %"class.std::__2::vector.7"** %this.addr, align 4
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_deleteEv(%"class.std::__2::vector.7"* %this1) #8
  %0 = bitcast %"class.std::__2::vector.7"* %this1 to %"class.std::__2::__vector_base.8"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.13"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.8"* %0) #8
  %1 = bitcast %"class.std::__2::vector.7"* %this1 to %"class.std::__2::__vector_base.8"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.8", %"class.std::__2::__vector_base.8"* %1, i32 0, i32 0
  %2 = load %"class.draco::IndexType.9"*, %"class.draco::IndexType.9"** %__begin_, align 4
  %3 = bitcast %"class.std::__2::vector.7"* %this1 to %"class.std::__2::__vector_base.8"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.8", %"class.std::__2::__vector_base.8"* %3, i32 0, i32 1
  %4 = load %"class.draco::IndexType.9"*, %"class.draco::IndexType.9"** %__end_, align 4
  %5 = load %"struct.std::__2::__split_buffer.156"*, %"struct.std::__2::__split_buffer.156"** %__v.addr, align 4
  %__begin_2 = getelementptr inbounds %"struct.std::__2::__split_buffer.156", %"struct.std::__2::__split_buffer.156"* %5, i32 0, i32 1
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_21CornerIndex_tag_type_EEEEEE46__construct_backward_with_exception_guaranteesIS5_EENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS6_PT_SC_EE5valuesr31is_trivially_move_constructibleISC_EE5valueEvE4typeERS6_SD_SD_RSD_(%"class.std::__2::allocator.13"* nonnull align 1 dereferenceable(1) %call, %"class.draco::IndexType.9"* %2, %"class.draco::IndexType.9"* %4, %"class.draco::IndexType.9"** nonnull align 4 dereferenceable(4) %__begin_2)
  %6 = bitcast %"class.std::__2::vector.7"* %this1 to %"class.std::__2::__vector_base.8"*
  %__begin_3 = getelementptr inbounds %"class.std::__2::__vector_base.8", %"class.std::__2::__vector_base.8"* %6, i32 0, i32 0
  %7 = load %"struct.std::__2::__split_buffer.156"*, %"struct.std::__2::__split_buffer.156"** %__v.addr, align 4
  %__begin_4 = getelementptr inbounds %"struct.std::__2::__split_buffer.156", %"struct.std::__2::__split_buffer.156"* %7, i32 0, i32 1
  call void @_ZNSt3__24swapIPN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS7_EE5valueEvE4typeERS7_SA_(%"class.draco::IndexType.9"** nonnull align 4 dereferenceable(4) %__begin_3, %"class.draco::IndexType.9"** nonnull align 4 dereferenceable(4) %__begin_4) #8
  %8 = bitcast %"class.std::__2::vector.7"* %this1 to %"class.std::__2::__vector_base.8"*
  %__end_5 = getelementptr inbounds %"class.std::__2::__vector_base.8", %"class.std::__2::__vector_base.8"* %8, i32 0, i32 1
  %9 = load %"struct.std::__2::__split_buffer.156"*, %"struct.std::__2::__split_buffer.156"** %__v.addr, align 4
  %__end_6 = getelementptr inbounds %"struct.std::__2::__split_buffer.156", %"struct.std::__2::__split_buffer.156"* %9, i32 0, i32 2
  call void @_ZNSt3__24swapIPN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS7_EE5valueEvE4typeERS7_SA_(%"class.draco::IndexType.9"** nonnull align 4 dereferenceable(4) %__end_5, %"class.draco::IndexType.9"** nonnull align 4 dereferenceable(4) %__end_6) #8
  %10 = bitcast %"class.std::__2::vector.7"* %this1 to %"class.std::__2::__vector_base.8"*
  %call7 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.9"** @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base.8"* %10) #8
  %11 = load %"struct.std::__2::__split_buffer.156"*, %"struct.std::__2::__split_buffer.156"** %__v.addr, align 4
  %call8 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.9"** @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEERNS_9allocatorIS4_EEE9__end_capEv(%"struct.std::__2::__split_buffer.156"* %11) #8
  call void @_ZNSt3__24swapIPN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS7_EE5valueEvE4typeERS7_SA_(%"class.draco::IndexType.9"** nonnull align 4 dereferenceable(4) %call7, %"class.draco::IndexType.9"** nonnull align 4 dereferenceable(4) %call8) #8
  %12 = load %"struct.std::__2::__split_buffer.156"*, %"struct.std::__2::__split_buffer.156"** %__v.addr, align 4
  %__begin_9 = getelementptr inbounds %"struct.std::__2::__split_buffer.156", %"struct.std::__2::__split_buffer.156"* %12, i32 0, i32 1
  %13 = load %"class.draco::IndexType.9"*, %"class.draco::IndexType.9"** %__begin_9, align 4
  %14 = load %"struct.std::__2::__split_buffer.156"*, %"struct.std::__2::__split_buffer.156"** %__v.addr, align 4
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer.156", %"struct.std::__2::__split_buffer.156"* %14, i32 0, i32 0
  store %"class.draco::IndexType.9"* %13, %"class.draco::IndexType.9"** %__first_, align 4
  %call10 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.7"* %this1) #8
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE14__annotate_newEm(%"class.std::__2::vector.7"* %this1, i32 %call10) #8
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector.7"* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer.156"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEERNS_9allocatorIS4_EEED2Ev(%"struct.std::__2::__split_buffer.156"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"struct.std::__2::__split_buffer.156"*, align 4
  %this.addr = alloca %"struct.std::__2::__split_buffer.156"*, align 4
  store %"struct.std::__2::__split_buffer.156"* %this, %"struct.std::__2::__split_buffer.156"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.156"*, %"struct.std::__2::__split_buffer.156"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer.156"* %this1, %"struct.std::__2::__split_buffer.156"** %retval, align 4
  call void @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEERNS_9allocatorIS4_EEE5clearEv(%"struct.std::__2::__split_buffer.156"* %this1) #8
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer.156", %"struct.std::__2::__split_buffer.156"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexType.9"*, %"class.draco::IndexType.9"** %__first_, align 4
  %tobool = icmp ne %"class.draco::IndexType.9"* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.13"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEERNS_9allocatorIS4_EEE7__allocEv(%"struct.std::__2::__split_buffer.156"* %this1) #8
  %__first_2 = getelementptr inbounds %"struct.std::__2::__split_buffer.156", %"struct.std::__2::__split_buffer.156"* %this1, i32 0, i32 0
  %1 = load %"class.draco::IndexType.9"*, %"class.draco::IndexType.9"** %__first_2, align 4
  %call3 = call i32 @_ZNKSt3__214__split_bufferIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEERNS_9allocatorIS4_EEE8capacityEv(%"struct.std::__2::__split_buffer.156"* %this1)
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_21CornerIndex_tag_type_EEEEEE10deallocateERS6_PS5_m(%"class.std::__2::allocator.13"* nonnull align 1 dereferenceable(1) %call, %"class.draco::IndexType.9"* %1, i32 %call3) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"struct.std::__2::__split_buffer.156"*, %"struct.std::__2::__split_buffer.156"** %retval, align 4
  ret %"struct.std::__2::__split_buffer.156"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::__vector_base.8"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.8"*, align 4
  store %"class.std::__2::__vector_base.8"* %this, %"class.std::__2::__vector_base.8"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.8"*, %"class.std::__2::__vector_base.8"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.9"** @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base.8"* %this1) #8
  %0 = load %"class.draco::IndexType.9"*, %"class.draco::IndexType.9"** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.8", %"class.std::__2::__vector_base.8"* %this1, i32 0, i32 0
  %1 = load %"class.draco::IndexType.9"*, %"class.draco::IndexType.9"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.draco::IndexType.9"* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.draco::IndexType.9"* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.9"** @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base.8"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.8"*, align 4
  store %"class.std::__2::__vector_base.8"* %this, %"class.std::__2::__vector_base.8"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.8"*, %"class.std::__2::__vector_base.8"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.8", %"class.std::__2::__vector_base.8"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.9"** @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.10"* %__end_cap_) #8
  ret %"class.draco::IndexType.9"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.9"** @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.10"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.10"*, align 4
  store %"class.std::__2::__compressed_pair.10"* %this, %"class.std::__2::__compressed_pair.10"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.10"*, %"class.std::__2::__compressed_pair.10"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.10"* %this1 to %"struct.std::__2::__compressed_pair_elem.11"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.9"** @_ZNKSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.11"* %0) #8
  ret %"class.draco::IndexType.9"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.9"** @_ZNKSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.11"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.11"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.11"* %this, %"struct.std::__2::__compressed_pair_elem.11"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.11"*, %"struct.std::__2::__compressed_pair_elem.11"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.11", %"struct.std::__2::__compressed_pair_elem.11"* %this1, i32 0, i32 0
  ret %"class.draco::IndexType.9"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.13"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.10"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.10"*, align 4
  store %"class.std::__2::__compressed_pair.10"* %this, %"class.std::__2::__compressed_pair.10"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.10"*, %"class.std::__2::__compressed_pair.10"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.10"* %this1 to %"struct.std::__2::__compressed_pair_elem.12"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.13"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_21CornerIndex_tag_type_EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.12"* %0) #8
  ret %"class.std::__2::allocator.13"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.13"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_21CornerIndex_tag_type_EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.12"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.12"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.12"* %this, %"struct.std::__2::__compressed_pair_elem.12"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.12"*, %"struct.std::__2::__compressed_pair_elem.12"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.12"* %this1 to %"class.std::__2::allocator.13"*
  ret %"class.std::__2::allocator.13"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.157"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEERNS_9allocatorIS4_EEEC2IDnS8_EEOT_OT0_(%"class.std::__2::__compressed_pair.157"* returned %this, i8** nonnull align 4 dereferenceable(4) %__t1, %"class.std::__2::allocator.13"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.157"*, align 4
  %__t1.addr = alloca i8**, align 4
  %__t2.addr = alloca %"class.std::__2::allocator.13"*, align 4
  store %"class.std::__2::__compressed_pair.157"* %this, %"class.std::__2::__compressed_pair.157"** %this.addr, align 4
  store i8** %__t1, i8*** %__t1.addr, align 4
  store %"class.std::__2::allocator.13"* %__t2, %"class.std::__2::allocator.13"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.157"*, %"class.std::__2::__compressed_pair.157"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.157"* %this1 to %"struct.std::__2::__compressed_pair_elem.11"*
  %1 = load i8**, i8*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %1) #8
  %call2 = call %"struct.std::__2::__compressed_pair_elem.11"* @_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEELi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.11"* %0, i8** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.157"* %this1 to i8*
  %3 = getelementptr inbounds i8, i8* %2, i32 4
  %4 = bitcast i8* %3 to %"struct.std::__2::__compressed_pair_elem.158"*
  %5 = load %"class.std::__2::allocator.13"*, %"class.std::__2::allocator.13"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.13"* @_ZNSt3__27forwardIRNS_9allocatorIN5draco9IndexTypeIjNS2_21CornerIndex_tag_type_EEEEEEEOT_RNS_16remove_referenceIS8_E4typeE(%"class.std::__2::allocator.13"* nonnull align 1 dereferenceable(1) %5) #8
  %call4 = call %"struct.std::__2::__compressed_pair_elem.158"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIN5draco9IndexTypeIjNS2_21CornerIndex_tag_type_EEEEELi1ELb0EEC2IS7_vEEOT_(%"struct.std::__2::__compressed_pair_elem.158"* %4, %"class.std::__2::allocator.13"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::__compressed_pair.157"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType.9"* @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_21CornerIndex_tag_type_EEEEEE8allocateERS6_m(%"class.std::__2::allocator.13"* nonnull align 1 dereferenceable(1) %__a, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.13"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.13"* %__a, %"class.std::__2::allocator.13"** %__a.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.13"*, %"class.std::__2::allocator.13"** %__a.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call %"class.draco::IndexType.9"* @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEEE8allocateEmPKv(%"class.std::__2::allocator.13"* %0, i32 %1, i8* null)
  ret %"class.draco::IndexType.9"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.13"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEERNS_9allocatorIS4_EEE7__allocEv(%"struct.std::__2::__split_buffer.156"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.156"*, align 4
  store %"struct.std::__2::__split_buffer.156"* %this, %"struct.std::__2::__split_buffer.156"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.156"*, %"struct.std::__2::__split_buffer.156"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer.156", %"struct.std::__2::__split_buffer.156"* %this1, i32 0, i32 3
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.13"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEERNS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.157"* %__end_cap_) #8
  ret %"class.std::__2::allocator.13"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.9"** @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEERNS_9allocatorIS4_EEE9__end_capEv(%"struct.std::__2::__split_buffer.156"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.156"*, align 4
  store %"struct.std::__2::__split_buffer.156"* %this, %"struct.std::__2::__split_buffer.156"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.156"*, %"struct.std::__2::__split_buffer.156"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer.156", %"struct.std::__2::__split_buffer.156"* %this1, i32 0, i32 3
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.9"** @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEERNS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.157"* %__end_cap_) #8
  ret %"class.draco::IndexType.9"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.13"* @_ZNSt3__27forwardIRNS_9allocatorIN5draco9IndexTypeIjNS2_21CornerIndex_tag_type_EEEEEEEOT_RNS_16remove_referenceIS8_E4typeE(%"class.std::__2::allocator.13"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::allocator.13"*, align 4
  store %"class.std::__2::allocator.13"* %__t, %"class.std::__2::allocator.13"** %__t.addr, align 4
  %0 = load %"class.std::__2::allocator.13"*, %"class.std::__2::allocator.13"** %__t.addr, align 4
  ret %"class.std::__2::allocator.13"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.158"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIN5draco9IndexTypeIjNS2_21CornerIndex_tag_type_EEEEELi1ELb0EEC2IS7_vEEOT_(%"struct.std::__2::__compressed_pair_elem.158"* returned %this, %"class.std::__2::allocator.13"* nonnull align 1 dereferenceable(1) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.158"*, align 4
  %__u.addr = alloca %"class.std::__2::allocator.13"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.158"* %this, %"struct.std::__2::__compressed_pair_elem.158"** %this.addr, align 4
  store %"class.std::__2::allocator.13"* %__u, %"class.std::__2::allocator.13"** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.158"*, %"struct.std::__2::__compressed_pair_elem.158"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.158", %"struct.std::__2::__compressed_pair_elem.158"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::allocator.13"*, %"class.std::__2::allocator.13"** %__u.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.13"* @_ZNSt3__27forwardIRNS_9allocatorIN5draco9IndexTypeIjNS2_21CornerIndex_tag_type_EEEEEEEOT_RNS_16remove_referenceIS8_E4typeE(%"class.std::__2::allocator.13"* nonnull align 1 dereferenceable(1) %0) #8
  store %"class.std::__2::allocator.13"* %call, %"class.std::__2::allocator.13"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.158"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType.9"* @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEEE8allocateEmPKv(%"class.std::__2::allocator.13"* %this, i32 %__n, i8* %0) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.13"*, align 4
  %__n.addr = alloca i32, align 4
  %.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.13"* %this, %"class.std::__2::allocator.13"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::allocator.13"*, %"class.std::__2::allocator.13"** %this.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEEE8max_sizeEv(%"class.std::__2::allocator.13"* %this1) #8
  %cmp = icmp ugt i32 %1, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__220__throw_length_errorEPKc(i8* getelementptr inbounds ([68 x i8], [68 x i8]* @.str, i32 0, i32 0)) #10
  unreachable

if.end:                                           ; preds = %entry
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  %call2 = call i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %mul, i32 4)
  %3 = bitcast i8* %call2 to %"class.draco::IndexType.9"*
  ret %"class.draco::IndexType.9"* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__29allocatorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEEE8max_sizeEv(%"class.std::__2::allocator.13"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.13"*, align 4
  store %"class.std::__2::allocator.13"* %this, %"class.std::__2::allocator.13"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.13"*, %"class.std::__2::allocator.13"** %this.addr, align 4
  ret i32 1073741823
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.13"* @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEERNS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.157"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.157"*, align 4
  store %"class.std::__2::__compressed_pair.157"* %this, %"class.std::__2::__compressed_pair.157"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.157"*, %"class.std::__2::__compressed_pair.157"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.157"* %this1 to i8*
  %add.ptr = getelementptr inbounds i8, i8* %0, i32 4
  %1 = bitcast i8* %add.ptr to %"struct.std::__2::__compressed_pair_elem.158"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.13"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIN5draco9IndexTypeIjNS2_21CornerIndex_tag_type_EEEEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.158"* %1) #8
  ret %"class.std::__2::allocator.13"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.13"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIN5draco9IndexTypeIjNS2_21CornerIndex_tag_type_EEEEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.158"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.158"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.158"* %this, %"struct.std::__2::__compressed_pair_elem.158"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.158"*, %"struct.std::__2::__compressed_pair_elem.158"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.158", %"struct.std::__2::__compressed_pair_elem.158"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::allocator.13"*, %"class.std::__2::allocator.13"** %__value_, align 4
  ret %"class.std::__2::allocator.13"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.9"** @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEERNS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.157"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.157"*, align 4
  store %"class.std::__2::__compressed_pair.157"* %this, %"class.std::__2::__compressed_pair.157"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.157"*, %"class.std::__2::__compressed_pair.157"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.157"* %this1 to %"struct.std::__2::__compressed_pair_elem.11"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.9"** @_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.11"* %0) #8
  ret %"class.draco::IndexType.9"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.9"** @_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.11"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.11"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.11"* %this, %"struct.std::__2::__compressed_pair_elem.11"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.11"*, %"struct.std::__2::__compressed_pair_elem.11"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.11", %"struct.std::__2::__compressed_pair_elem.11"* %this1, i32 0, i32 0
  ret %"class.draco::IndexType.9"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE17__annotate_deleteEv(%"class.std::__2::vector.7"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.7"*, align 4
  store %"class.std::__2::vector.7"* %this, %"class.std::__2::vector.7"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.7"*, %"class.std::__2::vector.7"** %this.addr, align 4
  %call = call %"class.draco::IndexType.9"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.7"* %this1) #8
  %0 = bitcast %"class.draco::IndexType.9"* %call to i8*
  %call2 = call %"class.draco::IndexType.9"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.7"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.7"* %this1) #8
  %add.ptr = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %call2, i32 %call3
  %1 = bitcast %"class.draco::IndexType.9"* %add.ptr to i8*
  %call4 = call %"class.draco::IndexType.9"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.7"* %this1) #8
  %call5 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.7"* %this1) #8
  %add.ptr6 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %call4, i32 %call5
  %2 = bitcast %"class.draco::IndexType.9"* %add.ptr6 to i8*
  %call7 = call %"class.draco::IndexType.9"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.7"* %this1) #8
  %call8 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.7"* %this1) #8
  %add.ptr9 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %call7, i32 %call8
  %3 = bitcast %"class.draco::IndexType.9"* %add.ptr9 to i8*
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_(%"class.std::__2::vector.7"* %this1, i8* %0, i8* %1, i8* %2, i8* %3) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_21CornerIndex_tag_type_EEEEEE46__construct_backward_with_exception_guaranteesIS5_EENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS6_PT_SC_EE5valuesr31is_trivially_move_constructibleISC_EE5valueEvE4typeERS6_SD_SD_RSD_(%"class.std::__2::allocator.13"* nonnull align 1 dereferenceable(1) %0, %"class.draco::IndexType.9"* %__begin1, %"class.draco::IndexType.9"* %__end1, %"class.draco::IndexType.9"** nonnull align 4 dereferenceable(4) %__end2) #0 comdat {
entry:
  %.addr = alloca %"class.std::__2::allocator.13"*, align 4
  %__begin1.addr = alloca %"class.draco::IndexType.9"*, align 4
  %__end1.addr = alloca %"class.draco::IndexType.9"*, align 4
  %__end2.addr = alloca %"class.draco::IndexType.9"**, align 4
  %_Np = alloca i32, align 4
  store %"class.std::__2::allocator.13"* %0, %"class.std::__2::allocator.13"** %.addr, align 4
  store %"class.draco::IndexType.9"* %__begin1, %"class.draco::IndexType.9"** %__begin1.addr, align 4
  store %"class.draco::IndexType.9"* %__end1, %"class.draco::IndexType.9"** %__end1.addr, align 4
  store %"class.draco::IndexType.9"** %__end2, %"class.draco::IndexType.9"*** %__end2.addr, align 4
  %1 = load %"class.draco::IndexType.9"*, %"class.draco::IndexType.9"** %__end1.addr, align 4
  %2 = load %"class.draco::IndexType.9"*, %"class.draco::IndexType.9"** %__begin1.addr, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.draco::IndexType.9"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.draco::IndexType.9"* %2 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  store i32 %sub.ptr.div, i32* %_Np, align 4
  %3 = load i32, i32* %_Np, align 4
  %4 = load %"class.draco::IndexType.9"**, %"class.draco::IndexType.9"*** %__end2.addr, align 4
  %5 = load %"class.draco::IndexType.9"*, %"class.draco::IndexType.9"** %4, align 4
  %idx.neg = sub i32 0, %3
  %add.ptr = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %5, i32 %idx.neg
  store %"class.draco::IndexType.9"* %add.ptr, %"class.draco::IndexType.9"** %4, align 4
  %6 = load i32, i32* %_Np, align 4
  %cmp = icmp sgt i32 %6, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %7 = load %"class.draco::IndexType.9"**, %"class.draco::IndexType.9"*** %__end2.addr, align 4
  %8 = load %"class.draco::IndexType.9"*, %"class.draco::IndexType.9"** %7, align 4
  %9 = bitcast %"class.draco::IndexType.9"* %8 to i8*
  %10 = load %"class.draco::IndexType.9"*, %"class.draco::IndexType.9"** %__begin1.addr, align 4
  %11 = bitcast %"class.draco::IndexType.9"* %10 to i8*
  %12 = load i32, i32* %_Np, align 4
  %mul = mul i32 %12, 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %11, i32 %mul, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__24swapIPN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEEEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS7_EE5valueEvE4typeERS7_SA_(%"class.draco::IndexType.9"** nonnull align 4 dereferenceable(4) %__x, %"class.draco::IndexType.9"** nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %__x.addr = alloca %"class.draco::IndexType.9"**, align 4
  %__y.addr = alloca %"class.draco::IndexType.9"**, align 4
  %__t = alloca %"class.draco::IndexType.9"*, align 4
  store %"class.draco::IndexType.9"** %__x, %"class.draco::IndexType.9"*** %__x.addr, align 4
  store %"class.draco::IndexType.9"** %__y, %"class.draco::IndexType.9"*** %__y.addr, align 4
  %0 = load %"class.draco::IndexType.9"**, %"class.draco::IndexType.9"*** %__x.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.9"** @_ZNSt3__24moveIRPN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEEEEONS_16remove_referenceIT_E4typeEOS8_(%"class.draco::IndexType.9"** nonnull align 4 dereferenceable(4) %0) #8
  %1 = load %"class.draco::IndexType.9"*, %"class.draco::IndexType.9"** %call, align 4
  store %"class.draco::IndexType.9"* %1, %"class.draco::IndexType.9"** %__t, align 4
  %2 = load %"class.draco::IndexType.9"**, %"class.draco::IndexType.9"*** %__y.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.9"** @_ZNSt3__24moveIRPN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEEEEONS_16remove_referenceIT_E4typeEOS8_(%"class.draco::IndexType.9"** nonnull align 4 dereferenceable(4) %2) #8
  %3 = load %"class.draco::IndexType.9"*, %"class.draco::IndexType.9"** %call1, align 4
  %4 = load %"class.draco::IndexType.9"**, %"class.draco::IndexType.9"*** %__x.addr, align 4
  store %"class.draco::IndexType.9"* %3, %"class.draco::IndexType.9"** %4, align 4
  %call2 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.9"** @_ZNSt3__24moveIRPN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEEEEONS_16remove_referenceIT_E4typeEOS8_(%"class.draco::IndexType.9"** nonnull align 4 dereferenceable(4) %__t) #8
  %5 = load %"class.draco::IndexType.9"*, %"class.draco::IndexType.9"** %call2, align 4
  %6 = load %"class.draco::IndexType.9"**, %"class.draco::IndexType.9"*** %__y.addr, align 4
  store %"class.draco::IndexType.9"* %5, %"class.draco::IndexType.9"** %6, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.9"** @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base.8"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.8"*, align 4
  store %"class.std::__2::__vector_base.8"* %this, %"class.std::__2::__vector_base.8"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.8"*, %"class.std::__2::__vector_base.8"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.8", %"class.std::__2::__vector_base.8"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.9"** @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.10"* %__end_cap_) #8
  ret %"class.draco::IndexType.9"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE14__annotate_newEm(%"class.std::__2::vector.7"* %this, i32 %__current_size) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.7"*, align 4
  %__current_size.addr = alloca i32, align 4
  store %"class.std::__2::vector.7"* %this, %"class.std::__2::vector.7"** %this.addr, align 4
  store i32 %__current_size, i32* %__current_size.addr, align 4
  %this1 = load %"class.std::__2::vector.7"*, %"class.std::__2::vector.7"** %this.addr, align 4
  %call = call %"class.draco::IndexType.9"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.7"* %this1) #8
  %0 = bitcast %"class.draco::IndexType.9"* %call to i8*
  %call2 = call %"class.draco::IndexType.9"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.7"* %this1) #8
  %call3 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.7"* %this1) #8
  %add.ptr = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %call2, i32 %call3
  %1 = bitcast %"class.draco::IndexType.9"* %add.ptr to i8*
  %call4 = call %"class.draco::IndexType.9"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.7"* %this1) #8
  %call5 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.7"* %this1) #8
  %add.ptr6 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %call4, i32 %call5
  %2 = bitcast %"class.draco::IndexType.9"* %add.ptr6 to i8*
  %call7 = call %"class.draco::IndexType.9"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.7"* %this1) #8
  %3 = load i32, i32* %__current_size.addr, align 4
  %add.ptr8 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %call7, i32 %3
  %4 = bitcast %"class.draco::IndexType.9"* %add.ptr8 to i8*
  call void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_(%"class.std::__2::vector.7"* %this1, i8* %0, i8* %1, i8* %2, i8* %4) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector.7"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.7"*, align 4
  store %"class.std::__2::vector.7"* %this, %"class.std::__2::vector.7"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.7"*, %"class.std::__2::vector.7"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_(%"class.std::__2::vector.7"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.7"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector.7"* %this, %"class.std::__2::vector.7"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector.7"*, %"class.std::__2::vector.7"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType.9"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE4dataEv(%"class.std::__2::vector.7"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.7"*, align 4
  store %"class.std::__2::vector.7"* %this, %"class.std::__2::vector.7"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.7"*, %"class.std::__2::vector.7"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.7"* %this1 to %"class.std::__2::__vector_base.8"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.8", %"class.std::__2::__vector_base.8"* %0, i32 0, i32 0
  %1 = load %"class.draco::IndexType.9"*, %"class.draco::IndexType.9"** %__begin_, align 4
  %call = call %"class.draco::IndexType.9"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType.9"* %1) #8
  ret %"class.draco::IndexType.9"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType.9"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType.9"* %__p) #0 comdat {
entry:
  %__p.addr = alloca %"class.draco::IndexType.9"*, align 4
  store %"class.draco::IndexType.9"* %__p, %"class.draco::IndexType.9"** %__p.addr, align 4
  %0 = load %"class.draco::IndexType.9"*, %"class.draco::IndexType.9"** %__p.addr, align 4
  ret %"class.draco::IndexType.9"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.9"** @_ZNSt3__24moveIRPN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEEEEONS_16remove_referenceIT_E4typeEOS8_(%"class.draco::IndexType.9"** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.draco::IndexType.9"**, align 4
  store %"class.draco::IndexType.9"** %__t, %"class.draco::IndexType.9"*** %__t.addr, align 4
  %0 = load %"class.draco::IndexType.9"**, %"class.draco::IndexType.9"*** %__t.addr, align 4
  ret %"class.draco::IndexType.9"** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.9"** @_ZNSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.10"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.10"*, align 4
  store %"class.std::__2::__compressed_pair.10"* %this, %"class.std::__2::__compressed_pair.10"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.10"*, %"class.std::__2::__compressed_pair.10"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.10"* %this1 to %"struct.std::__2::__compressed_pair_elem.11"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.9"** @_ZNSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.11"* %0) #8
  ret %"class.draco::IndexType.9"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEERNS_9allocatorIS4_EEE5clearEv(%"struct.std::__2::__split_buffer.156"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.156"*, align 4
  store %"struct.std::__2::__split_buffer.156"* %this, %"struct.std::__2::__split_buffer.156"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.156"*, %"struct.std::__2::__split_buffer.156"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"struct.std::__2::__split_buffer.156", %"struct.std::__2::__split_buffer.156"* %this1, i32 0, i32 1
  %0 = load %"class.draco::IndexType.9"*, %"class.draco::IndexType.9"** %__begin_, align 4
  call void @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEERNS_9allocatorIS4_EEE17__destruct_at_endEPS4_(%"struct.std::__2::__split_buffer.156"* %this1, %"class.draco::IndexType.9"* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_21CornerIndex_tag_type_EEEEEE10deallocateERS6_PS5_m(%"class.std::__2::allocator.13"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType.9"* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.13"*, align 4
  %__p.addr = alloca %"class.draco::IndexType.9"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.13"* %__a, %"class.std::__2::allocator.13"** %__a.addr, align 4
  store %"class.draco::IndexType.9"* %__p, %"class.draco::IndexType.9"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.13"*, %"class.std::__2::allocator.13"** %__a.addr, align 4
  %1 = load %"class.draco::IndexType.9"*, %"class.draco::IndexType.9"** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEEE10deallocateEPS4_m(%"class.std::__2::allocator.13"* %0, %"class.draco::IndexType.9"* %1, i32 %2) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__214__split_bufferIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEERNS_9allocatorIS4_EEE8capacityEv(%"struct.std::__2::__split_buffer.156"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.156"*, align 4
  store %"struct.std::__2::__split_buffer.156"* %this, %"struct.std::__2::__split_buffer.156"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.156"*, %"struct.std::__2::__split_buffer.156"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.9"** @_ZNKSt3__214__split_bufferIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEERNS_9allocatorIS4_EEE9__end_capEv(%"struct.std::__2::__split_buffer.156"* %this1) #8
  %0 = load %"class.draco::IndexType.9"*, %"class.draco::IndexType.9"** %call, align 4
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer.156", %"struct.std::__2::__split_buffer.156"* %this1, i32 0, i32 0
  %1 = load %"class.draco::IndexType.9"*, %"class.draco::IndexType.9"** %__first_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.draco::IndexType.9"* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.draco::IndexType.9"* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEERNS_9allocatorIS4_EEE17__destruct_at_endEPS4_(%"struct.std::__2::__split_buffer.156"* %this, %"class.draco::IndexType.9"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.156"*, align 4
  %__new_last.addr = alloca %"class.draco::IndexType.9"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant.146", align 1
  store %"struct.std::__2::__split_buffer.156"* %this, %"struct.std::__2::__split_buffer.156"** %this.addr, align 4
  store %"class.draco::IndexType.9"* %__new_last, %"class.draco::IndexType.9"** %__new_last.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.156"*, %"struct.std::__2::__split_buffer.156"** %this.addr, align 4
  %0 = load %"class.draco::IndexType.9"*, %"class.draco::IndexType.9"** %__new_last.addr, align 4
  call void @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEERNS_9allocatorIS4_EEE17__destruct_at_endEPS4_NS_17integral_constantIbLb0EEE(%"struct.std::__2::__split_buffer.156"* %this1, %"class.draco::IndexType.9"* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEERNS_9allocatorIS4_EEE17__destruct_at_endEPS4_NS_17integral_constantIbLb0EEE(%"struct.std::__2::__split_buffer.156"* %this, %"class.draco::IndexType.9"* %__new_last) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant.146", align 1
  %this.addr = alloca %"struct.std::__2::__split_buffer.156"*, align 4
  %__new_last.addr = alloca %"class.draco::IndexType.9"*, align 4
  store %"struct.std::__2::__split_buffer.156"* %this, %"struct.std::__2::__split_buffer.156"** %this.addr, align 4
  store %"class.draco::IndexType.9"* %__new_last, %"class.draco::IndexType.9"** %__new_last.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.156"*, %"struct.std::__2::__split_buffer.156"** %this.addr, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load %"class.draco::IndexType.9"*, %"class.draco::IndexType.9"** %__new_last.addr, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer.156", %"struct.std::__2::__split_buffer.156"* %this1, i32 0, i32 2
  %2 = load %"class.draco::IndexType.9"*, %"class.draco::IndexType.9"** %__end_, align 4
  %cmp = icmp ne %"class.draco::IndexType.9"* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.13"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEERNS_9allocatorIS4_EEE7__allocEv(%"struct.std::__2::__split_buffer.156"* %this1) #8
  %__end_2 = getelementptr inbounds %"struct.std::__2::__split_buffer.156", %"struct.std::__2::__split_buffer.156"* %this1, i32 0, i32 2
  %3 = load %"class.draco::IndexType.9"*, %"class.draco::IndexType.9"** %__end_2, align 4
  %incdec.ptr = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %3, i32 -1
  store %"class.draco::IndexType.9"* %incdec.ptr, %"class.draco::IndexType.9"** %__end_2, align 4
  %call3 = call %"class.draco::IndexType.9"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType.9"* %incdec.ptr) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_21CornerIndex_tag_type_EEEEEE7destroyIS5_EEvRS6_PT_(%"class.std::__2::allocator.13"* nonnull align 1 dereferenceable(1) %call, %"class.draco::IndexType.9"* %call3)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_21CornerIndex_tag_type_EEEEEE7destroyIS5_EEvRS6_PT_(%"class.std::__2::allocator.13"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType.9"* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.13"*, align 4
  %__p.addr = alloca %"class.draco::IndexType.9"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy.159", align 1
  store %"class.std::__2::allocator.13"* %__a, %"class.std::__2::allocator.13"** %__a.addr, align 4
  store %"class.draco::IndexType.9"* %__p, %"class.draco::IndexType.9"** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy.159"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.13"*, %"class.std::__2::allocator.13"** %__a.addr, align 4
  %2 = load %"class.draco::IndexType.9"*, %"class.draco::IndexType.9"** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_21CornerIndex_tag_type_EEEEEE9__destroyIS5_EEvNS_17integral_constantIbLb1EEERS6_PT_(%"class.std::__2::allocator.13"* nonnull align 1 dereferenceable(1) %1, %"class.draco::IndexType.9"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_21CornerIndex_tag_type_EEEEEE9__destroyIS5_EEvNS_17integral_constantIbLb1EEERS6_PT_(%"class.std::__2::allocator.13"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType.9"* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.13"*, align 4
  %__p.addr = alloca %"class.draco::IndexType.9"*, align 4
  store %"class.std::__2::allocator.13"* %__a, %"class.std::__2::allocator.13"** %__a.addr, align 4
  store %"class.draco::IndexType.9"* %__p, %"class.draco::IndexType.9"** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.13"*, %"class.std::__2::allocator.13"** %__a.addr, align 4
  %2 = load %"class.draco::IndexType.9"*, %"class.draco::IndexType.9"** %__p.addr, align 4
  call void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEEE7destroyEPS4_(%"class.std::__2::allocator.13"* %1, %"class.draco::IndexType.9"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEEE7destroyEPS4_(%"class.std::__2::allocator.13"* %this, %"class.draco::IndexType.9"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.13"*, align 4
  %__p.addr = alloca %"class.draco::IndexType.9"*, align 4
  store %"class.std::__2::allocator.13"* %this, %"class.std::__2::allocator.13"** %this.addr, align 4
  store %"class.draco::IndexType.9"* %__p, %"class.draco::IndexType.9"** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.13"*, %"class.std::__2::allocator.13"** %this.addr, align 4
  %0 = load %"class.draco::IndexType.9"*, %"class.draco::IndexType.9"** %__p.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEEE10deallocateEPS4_m(%"class.std::__2::allocator.13"* %this, %"class.draco::IndexType.9"* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.13"*, align 4
  %__p.addr = alloca %"class.draco::IndexType.9"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.13"* %this, %"class.std::__2::allocator.13"** %this.addr, align 4
  store %"class.draco::IndexType.9"* %__p, %"class.draco::IndexType.9"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.13"*, %"class.std::__2::allocator.13"** %this.addr, align 4
  %0 = load %"class.draco::IndexType.9"*, %"class.draco::IndexType.9"** %__p.addr, align 4
  %1 = bitcast %"class.draco::IndexType.9"* %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.9"** @_ZNKSt3__214__split_bufferIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEERNS_9allocatorIS4_EEE9__end_capEv(%"struct.std::__2::__split_buffer.156"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.156"*, align 4
  store %"struct.std::__2::__split_buffer.156"* %this, %"struct.std::__2::__split_buffer.156"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.156"*, %"struct.std::__2::__split_buffer.156"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer.156", %"struct.std::__2::__split_buffer.156"* %this1, i32 0, i32 3
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.9"** @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEERNS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.157"* %__end_cap_) #8
  ret %"class.draco::IndexType.9"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.9"** @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEERNS_9allocatorIS4_EEE5firstEv(%"class.std::__2::__compressed_pair.157"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.157"*, align 4
  store %"class.std::__2::__compressed_pair.157"* %this, %"class.std::__2::__compressed_pair.157"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.157"*, %"class.std::__2::__compressed_pair.157"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.157"* %this1 to %"struct.std::__2::__compressed_pair_elem.11"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.9"** @_ZNKSt3__222__compressed_pair_elemIPN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.11"* %0) #8
  ret %"class.draco::IndexType.9"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIbNS_9allocatorIbEEE10__make_refEm(%"class.std::__2::__bit_reference"* noalias sret align 4 %agg.result, %"class.std::__2::vector"* %this, i32 %__pos) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__pos.addr = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__pos, i32* %__pos.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::vector", %"class.std::__2::vector"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__begin_, align 4
  %1 = load i32, i32* %__pos.addr, align 4
  %div = udiv i32 %1, 32
  %add.ptr = getelementptr inbounds i32, i32* %0, i32 %div
  %2 = load i32, i32* %__pos.addr, align 4
  %rem = urem i32 %2, 32
  %shl = shl i32 1, %rem
  %call = call %"class.std::__2::__bit_reference"* @_ZNSt3__215__bit_referenceINS_6vectorIbNS_9allocatorIbEEEELb1EEC2EPmm(%"class.std::__2::__bit_reference"* %agg.result, i32* %add.ptr, i32 %shl) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__bit_reference"* @_ZNSt3__215__bit_referenceINS_6vectorIbNS_9allocatorIbEEEELb1EEC2EPmm(%"class.std::__2::__bit_reference"* returned %this, i32* %__s, i32 %__m) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__bit_reference"*, align 4
  %__s.addr = alloca i32*, align 4
  %__m.addr = alloca i32, align 4
  store %"class.std::__2::__bit_reference"* %this, %"class.std::__2::__bit_reference"** %this.addr, align 4
  store i32* %__s, i32** %__s.addr, align 4
  store i32 %__m, i32* %__m.addr, align 4
  %this1 = load %"class.std::__2::__bit_reference"*, %"class.std::__2::__bit_reference"** %this.addr, align 4
  %__seg_ = getelementptr inbounds %"class.std::__2::__bit_reference", %"class.std::__2::__bit_reference"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__s.addr, align 4
  store i32* %0, i32** %__seg_, align 4
  %__mask_ = getelementptr inbounds %"class.std::__2::__bit_reference", %"class.std::__2::__bit_reference"* %this1, i32 0, i32 1
  %1 = load i32, i32* %__m.addr, align 4
  store i32 %1, i32* %__mask_, align 4
  ret %"class.std::__2::__bit_reference"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType"* @_ZN5draco9IndexTypeIjNS_21VertexIndex_tag_type_EEC2Ej(%"class.draco::IndexType"* returned %this, i32 %value) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType"*, align 4
  %value.addr = alloca i32, align 4
  store %"class.draco::IndexType"* %this, %"class.draco::IndexType"** %this.addr, align 4
  store i32 %value, i32* %value.addr, align 4
  %this1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value.addr, align 4
  store i32 %0, i32* %value_, align 4
  ret %"class.draco::IndexType"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK5draco9IndexTypeIjNS_21VertexIndex_tag_type_EEltERKj(%"class.draco::IndexType"* %this, i32* nonnull align 4 dereferenceable(4) %val) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType"*, align 4
  %val.addr = alloca i32*, align 4
  store %"class.draco::IndexType"* %this, %"class.draco::IndexType"** %this.addr, align 4
  store i32* %val, i32** %val.addr, align 4
  %this1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value_, align 4
  %1 = load i32*, i32** %val.addr, align 4
  %2 = load i32, i32* %1, align 4
  %cmp = icmp ult i32 %0, %2
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco11CornerTable14LeftMostCornerENS_9IndexTypeIjNS_21VertexIndex_tag_type_EEE(%"class.draco::CornerTable"* %this, i32 %v.coerce) #0 comdat {
entry:
  %retval = alloca %"class.draco::IndexType.9", align 4
  %v = alloca %"class.draco::IndexType", align 4
  %this.addr = alloca %"class.draco::CornerTable"*, align 4
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %v, i32 0, i32 0
  store i32 %v.coerce, i32* %coerce.dive, align 4
  store %"class.draco::CornerTable"* %this, %"class.draco::CornerTable"** %this.addr, align 4
  %this1 = load %"class.draco::CornerTable"*, %"class.draco::CornerTable"** %this.addr, align 4
  %vertex_corners_ = getelementptr inbounds %"class.draco::CornerTable", %"class.draco::CornerTable"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.9"* @_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_21VertexIndex_tag_type_EEENS1_IjNS_21CornerIndex_tag_type_EEEEixERKS3_(%"class.draco::IndexTypeVector.24"* %vertex_corners_, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %v)
  %0 = bitcast %"class.draco::IndexType.9"* %retval to i8*
  %1 = bitcast %"class.draco::IndexType.9"* %call to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %0, i8* align 4 %1, i32 4, i1 false)
  %coerce.dive2 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %retval, i32 0, i32 0
  %2 = load i32, i32* %coerce.dive2, align 4
  ret i32 %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE9push_backEOS4_(%"class.std::__2::vector.15"* %this, %"class.draco::IndexType.17"* nonnull align 4 dereferenceable(4) %__x) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.15"*, align 4
  %__x.addr = alloca %"class.draco::IndexType.17"*, align 4
  store %"class.std::__2::vector.15"* %this, %"class.std::__2::vector.15"** %this.addr, align 4
  store %"class.draco::IndexType.17"* %__x, %"class.draco::IndexType.17"** %__x.addr, align 4
  %this1 = load %"class.std::__2::vector.15"*, %"class.std::__2::vector.15"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.15"* %this1 to %"class.std::__2::__vector_base.16"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.16", %"class.std::__2::__vector_base.16"* %0, i32 0, i32 1
  %1 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector.15"* %this1 to %"class.std::__2::__vector_base.16"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.17"** @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base.16"* %2) #8
  %3 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %call, align 4
  %cmp = icmp ult %"class.draco::IndexType.17"* %1, %3
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %4 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %__x.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.17"* @_ZNSt3__24moveIRN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEONS_16remove_referenceIT_E4typeEOS7_(%"class.draco::IndexType.17"* nonnull align 4 dereferenceable(4) %4) #8
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE22__construct_one_at_endIJS4_EEEvDpOT_(%"class.std::__2::vector.15"* %this1, %"class.draco::IndexType.17"* nonnull align 4 dereferenceable(4) %call2)
  br label %if.end

if.else:                                          ; preds = %entry
  %5 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %__x.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.17"* @_ZNSt3__24moveIRN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEONS_16remove_referenceIT_E4typeEOS7_(%"class.draco::IndexType.17"* nonnull align 4 dereferenceable(4) %5) #8
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE21__push_back_slow_pathIS4_EEvOT_(%"class.std::__2::vector.15"* %this1, %"class.draco::IndexType.17"* nonnull align 4 dereferenceable(4) %call3)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::IndexType.9"* @_ZN5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEC2Ev(%"class.draco::IndexType.9"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.9"*, align 4
  store %"class.draco::IndexType.9"* %this, %"class.draco::IndexType.9"** %this.addr, align 4
  %this1 = load %"class.draco::IndexType.9"*, %"class.draco::IndexType.9"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %this1, i32 0, i32 0
  store i32 0, i32* %value_, align 4
  ret %"class.draco::IndexType.9"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__215__bit_referenceINS_6vectorIbNS_9allocatorIbEEEELb1EEcvbEv(%"class.std::__2::__bit_reference"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__bit_reference"*, align 4
  store %"class.std::__2::__bit_reference"* %this, %"class.std::__2::__bit_reference"** %this.addr, align 4
  %this1 = load %"class.std::__2::__bit_reference"*, %"class.std::__2::__bit_reference"** %this.addr, align 4
  %__seg_ = getelementptr inbounds %"class.std::__2::__bit_reference", %"class.std::__2::__bit_reference"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__seg_, align 4
  %1 = load i32, i32* %0, align 4
  %__mask_ = getelementptr inbounds %"class.std::__2::__bit_reference", %"class.std::__2::__bit_reference"* %this1, i32 0, i32 1
  %2 = load i32, i32* %__mask_, align 4
  %and = and i32 %1, %2
  %tobool = icmp ne i32 %and, 0
  ret i1 %tobool
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco24MeshAttributeCornerTable9SwingLeftENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::MeshAttributeCornerTable"* %this, i32 %corner.coerce) #0 comdat {
entry:
  %retval = alloca %"class.draco::IndexType.9", align 4
  %corner = alloca %"class.draco::IndexType.9", align 4
  %this.addr = alloca %"class.draco::MeshAttributeCornerTable"*, align 4
  %agg.tmp = alloca %"class.draco::IndexType.9", align 4
  %agg.tmp2 = alloca %"class.draco::IndexType.9", align 4
  %agg.tmp3 = alloca %"class.draco::IndexType.9", align 4
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %corner, i32 0, i32 0
  store i32 %corner.coerce, i32* %coerce.dive, align 4
  store %"class.draco::MeshAttributeCornerTable"* %this, %"class.draco::MeshAttributeCornerTable"** %this.addr, align 4
  %this1 = load %"class.draco::MeshAttributeCornerTable"*, %"class.draco::MeshAttributeCornerTable"** %this.addr, align 4
  %0 = bitcast %"class.draco::IndexType.9"* %agg.tmp3 to i8*
  %1 = bitcast %"class.draco::IndexType.9"* %corner to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %0, i8* align 4 %1, i32 4, i1 false)
  %coerce.dive4 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %agg.tmp3, i32 0, i32 0
  %2 = load i32, i32* %coerce.dive4, align 4
  %call = call i32 @_ZNK5draco24MeshAttributeCornerTable4NextENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::MeshAttributeCornerTable"* %this1, i32 %2)
  %coerce.dive5 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %agg.tmp2, i32 0, i32 0
  store i32 %call, i32* %coerce.dive5, align 4
  %coerce.dive6 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %agg.tmp2, i32 0, i32 0
  %3 = load i32, i32* %coerce.dive6, align 4
  %call7 = call i32 @_ZNK5draco24MeshAttributeCornerTable8OppositeENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::MeshAttributeCornerTable"* %this1, i32 %3)
  %coerce.dive8 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %agg.tmp, i32 0, i32 0
  store i32 %call7, i32* %coerce.dive8, align 4
  %coerce.dive9 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %agg.tmp, i32 0, i32 0
  %4 = load i32, i32* %coerce.dive9, align 4
  %call10 = call i32 @_ZNK5draco24MeshAttributeCornerTable4NextENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::MeshAttributeCornerTable"* %this1, i32 %4)
  %coerce.dive11 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %retval, i32 0, i32 0
  store i32 %call10, i32* %coerce.dive11, align 4
  %coerce.dive12 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %retval, i32 0, i32 0
  %5 = load i32, i32* %coerce.dive12, align 4
  ret i32 %5
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EE5valueEv(%"class.draco::IndexType.17"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.17"*, align 4
  store %"class.draco::IndexType.17"* %this, %"class.draco::IndexType.17"** %this.addr, align 4
  %this1 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.17", %"class.draco::IndexType.17"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value_, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_21VertexIndex_tag_type_EEENS_9allocatorIS4_EEEixEm(%"class.std::__2::vector.1"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.1"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector.1"* %this, %"class.std::__2::vector.1"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 0
  %1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %__begin_, align 4
  %2 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %1, i32 %2
  ret %"class.draco::IndexType"* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE9push_backERKS4_(%"class.std::__2::vector.7"* %this, %"class.draco::IndexType.9"* nonnull align 4 dereferenceable(4) %__x) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.7"*, align 4
  %__x.addr = alloca %"class.draco::IndexType.9"*, align 4
  store %"class.std::__2::vector.7"* %this, %"class.std::__2::vector.7"** %this.addr, align 4
  store %"class.draco::IndexType.9"* %__x, %"class.draco::IndexType.9"** %__x.addr, align 4
  %this1 = load %"class.std::__2::vector.7"*, %"class.std::__2::vector.7"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.7"* %this1 to %"class.std::__2::__vector_base.8"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.8", %"class.std::__2::__vector_base.8"* %0, i32 0, i32 1
  %1 = load %"class.draco::IndexType.9"*, %"class.draco::IndexType.9"** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector.7"* %this1 to %"class.std::__2::__vector_base.8"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.9"** @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base.8"* %2) #8
  %3 = load %"class.draco::IndexType.9"*, %"class.draco::IndexType.9"** %call, align 4
  %cmp = icmp ne %"class.draco::IndexType.9"* %1, %3
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %4 = load %"class.draco::IndexType.9"*, %"class.draco::IndexType.9"** %__x.addr, align 4
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE22__construct_one_at_endIJRKS4_EEEvDpOT_(%"class.std::__2::vector.7"* %this1, %"class.draco::IndexType.9"* nonnull align 4 dereferenceable(4) %4)
  br label %if.end

if.else:                                          ; preds = %entry
  %5 = load %"class.draco::IndexType.9"*, %"class.draco::IndexType.9"** %__x.addr, align 4
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE21__push_back_slow_pathIRKS4_EEvOT_(%"class.std::__2::vector.7"* %this1, %"class.draco::IndexType.9"* nonnull align 4 dereferenceable(4) %5)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco11CornerTable10SwingRightENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::CornerTable"* %this, i32 %corner.coerce) #0 comdat {
entry:
  %retval = alloca %"class.draco::IndexType.9", align 4
  %corner = alloca %"class.draco::IndexType.9", align 4
  %this.addr = alloca %"class.draco::CornerTable"*, align 4
  %agg.tmp = alloca %"class.draco::IndexType.9", align 4
  %agg.tmp2 = alloca %"class.draco::IndexType.9", align 4
  %agg.tmp3 = alloca %"class.draco::IndexType.9", align 4
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %corner, i32 0, i32 0
  store i32 %corner.coerce, i32* %coerce.dive, align 4
  store %"class.draco::CornerTable"* %this, %"class.draco::CornerTable"** %this.addr, align 4
  %this1 = load %"class.draco::CornerTable"*, %"class.draco::CornerTable"** %this.addr, align 4
  %0 = bitcast %"class.draco::IndexType.9"* %agg.tmp3 to i8*
  %1 = bitcast %"class.draco::IndexType.9"* %corner to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %0, i8* align 4 %1, i32 4, i1 false)
  %coerce.dive4 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %agg.tmp3, i32 0, i32 0
  %2 = load i32, i32* %coerce.dive4, align 4
  %call = call i32 @_ZNK5draco11CornerTable8PreviousENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::CornerTable"* %this1, i32 %2)
  %coerce.dive5 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %agg.tmp2, i32 0, i32 0
  store i32 %call, i32* %coerce.dive5, align 4
  %coerce.dive6 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %agg.tmp2, i32 0, i32 0
  %3 = load i32, i32* %coerce.dive6, align 4
  %call7 = call i32 @_ZNK5draco11CornerTable8OppositeENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::CornerTable"* %this1, i32 %3)
  %coerce.dive8 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %agg.tmp, i32 0, i32 0
  store i32 %call7, i32* %coerce.dive8, align 4
  %coerce.dive9 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %agg.tmp, i32 0, i32 0
  %4 = load i32, i32* %coerce.dive9, align 4
  %call10 = call i32 @_ZNK5draco11CornerTable8PreviousENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::CornerTable"* %this1, i32 %4)
  %coerce.dive11 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %retval, i32 0, i32 0
  store i32 %call10, i32* %coerce.dive11, align 4
  %coerce.dive12 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %retval, i32 0, i32 0
  %5 = load i32, i32* %coerce.dive12, align 4
  ret i32 %5
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK5draco24MeshAttributeCornerTable26IsCornerOppositeToSeamEdgeENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::MeshAttributeCornerTable"* %this, i32 %corner.coerce) #0 comdat {
entry:
  %corner = alloca %"class.draco::IndexType.9", align 4
  %this.addr = alloca %"class.draco::MeshAttributeCornerTable"*, align 4
  %ref.tmp = alloca %"class.std::__2::__bit_const_reference", align 4
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %corner, i32 0, i32 0
  store i32 %corner.coerce, i32* %coerce.dive, align 4
  store %"class.draco::MeshAttributeCornerTable"* %this, %"class.draco::MeshAttributeCornerTable"** %this.addr, align 4
  %this1 = load %"class.draco::MeshAttributeCornerTable"*, %"class.draco::MeshAttributeCornerTable"** %this.addr, align 4
  %is_edge_on_seam_ = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 0
  %call = call i32 @_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EE5valueEv(%"class.draco::IndexType.9"* %corner)
  call void @_ZNKSt3__26vectorIbNS_9allocatorIbEEEixEm(%"class.std::__2::__bit_const_reference"* sret align 4 %ref.tmp, %"class.std::__2::vector"* %is_edge_on_seam_, i32 %call)
  %call2 = call zeroext i1 @_ZNKSt3__221__bit_const_referenceINS_6vectorIbNS_9allocatorIbEEEEEcvbEv(%"class.std::__2::__bit_const_reference"* %ref.tmp) #8
  ret i1 %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.17"* @_ZN5draco9IndexTypeIjNS_29AttributeValueIndex_tag_type_EEaSERKS2_(%"class.draco::IndexType.17"* %this, %"class.draco::IndexType.17"* nonnull align 4 dereferenceable(4) %i) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType.17"*, align 4
  %i.addr = alloca %"class.draco::IndexType.17"*, align 4
  store %"class.draco::IndexType.17"* %this, %"class.draco::IndexType.17"** %this.addr, align 4
  store %"class.draco::IndexType.17"* %i, %"class.draco::IndexType.17"** %i.addr, align 4
  %this1 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %this.addr, align 4
  %0 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %i.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType.17", %"class.draco::IndexType.17"* %0, i32 0, i32 0
  %1 = load i32, i32* %value_, align 4
  %value_2 = getelementptr inbounds %"class.draco::IndexType.17", %"class.draco::IndexType.17"* %this1, i32 0, i32 0
  store i32 %1, i32* %value_2, align 4
  ret %"class.draco::IndexType.17"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType"* @_ZN5draco9IndexTypeIjNS_21VertexIndex_tag_type_EEppEv(%"class.draco::IndexType"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.draco::IndexType"* %this, %"class.draco::IndexType"** %this.addr, align 4
  %this1 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %this.addr, align 4
  %value_ = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %this1, i32 0, i32 0
  %0 = load i32, i32* %value_, align 4
  %inc = add i32 %0, 1
  store i32 %inc, i32* %value_, align 4
  ret %"class.draco::IndexType"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.9"* @_ZNK5draco15IndexTypeVectorINS_9IndexTypeIjNS_21VertexIndex_tag_type_EEENS1_IjNS_21CornerIndex_tag_type_EEEEixERKS3_(%"class.draco::IndexTypeVector.24"* %this, %"class.draco::IndexType"* nonnull align 4 dereferenceable(4) %index) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::IndexTypeVector.24"*, align 4
  %index.addr = alloca %"class.draco::IndexType"*, align 4
  store %"class.draco::IndexTypeVector.24"* %this, %"class.draco::IndexTypeVector.24"** %this.addr, align 4
  store %"class.draco::IndexType"* %index, %"class.draco::IndexType"** %index.addr, align 4
  %this1 = load %"class.draco::IndexTypeVector.24"*, %"class.draco::IndexTypeVector.24"** %this.addr, align 4
  %vector_ = getelementptr inbounds %"class.draco::IndexTypeVector.24", %"class.draco::IndexTypeVector.24"* %this1, i32 0, i32 0
  %0 = load %"class.draco::IndexType"*, %"class.draco::IndexType"** %index.addr, align 4
  %call = call i32 @_ZNK5draco9IndexTypeIjNS_21VertexIndex_tag_type_EE5valueEv(%"class.draco::IndexType"* %0)
  %call2 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.9"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEEixEm(%"class.std::__2::vector.7"* %vector_, i32 %call) #8
  ret %"class.draco::IndexType.9"* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE22__construct_one_at_endIJS4_EEEvDpOT_(%"class.std::__2::vector.15"* %this, %"class.draco::IndexType.17"* nonnull align 4 dereferenceable(4) %__args) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.15"*, align 4
  %__args.addr = alloca %"class.draco::IndexType.17"*, align 4
  %__tx = alloca %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction", align 4
  store %"class.std::__2::vector.15"* %this, %"class.std::__2::vector.15"** %this.addr, align 4
  store %"class.draco::IndexType.17"* %__args, %"class.draco::IndexType.17"** %__args.addr, align 4
  %this1 = load %"class.std::__2::vector.15"*, %"class.std::__2::vector.15"** %this.addr, align 4
  %call = call %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionC2ERS7_m(%"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %__tx, %"class.std::__2::vector.15"* nonnull align 4 dereferenceable(12) %this1, i32 1)
  %0 = bitcast %"class.std::__2::vector.15"* %this1 to %"class.std::__2::__vector_base.16"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.21"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.16"* %0) #8
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %1 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %__pos_, align 4
  %call3 = call %"class.draco::IndexType.17"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType.17"* %1) #8
  %2 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %__args.addr, align 4
  %call4 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.17"* @_ZNSt3__27forwardIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"class.draco::IndexType.17"* nonnull align 4 dereferenceable(4) %2) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE9constructIS5_JS5_EEEvRS6_PT_DpOT0_(%"class.std::__2::allocator.21"* nonnull align 1 dereferenceable(1) %call2, %"class.draco::IndexType.17"* %call3, %"class.draco::IndexType.17"* nonnull align 4 dereferenceable(4) %call4)
  %__pos_5 = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %3 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %__pos_5, align 4
  %incdec.ptr = getelementptr inbounds %"class.draco::IndexType.17", %"class.draco::IndexType.17"* %3, i32 1
  store %"class.draco::IndexType.17"* %incdec.ptr, %"class.draco::IndexType.17"** %__pos_5, align 4
  %call6 = call %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %__tx) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.17"* @_ZNSt3__24moveIRN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEONS_16remove_referenceIT_E4typeEOS7_(%"class.draco::IndexType.17"* nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.draco::IndexType.17"*, align 4
  store %"class.draco::IndexType.17"* %__t, %"class.draco::IndexType.17"** %__t.addr, align 4
  %0 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %__t.addr, align 4
  ret %"class.draco::IndexType.17"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE21__push_back_slow_pathIS4_EEvOT_(%"class.std::__2::vector.15"* %this, %"class.draco::IndexType.17"* nonnull align 4 dereferenceable(4) %__x) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.15"*, align 4
  %__x.addr = alloca %"class.draco::IndexType.17"*, align 4
  %__a = alloca %"class.std::__2::allocator.21"*, align 4
  %__v = alloca %"struct.std::__2::__split_buffer", align 4
  store %"class.std::__2::vector.15"* %this, %"class.std::__2::vector.15"** %this.addr, align 4
  store %"class.draco::IndexType.17"* %__x, %"class.draco::IndexType.17"** %__x.addr, align 4
  %this1 = load %"class.std::__2::vector.15"*, %"class.std::__2::vector.15"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.15"* %this1 to %"class.std::__2::__vector_base.16"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.21"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.16"* %0) #8
  store %"class.std::__2::allocator.21"* %call, %"class.std::__2::allocator.21"** %__a, align 4
  %call2 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.15"* %this1) #8
  %add = add i32 %call2, 1
  %call3 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE11__recommendEm(%"class.std::__2::vector.15"* %this1, i32 %add)
  %call4 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.15"* %this1) #8
  %1 = load %"class.std::__2::allocator.21"*, %"class.std::__2::allocator.21"** %__a, align 4
  %call5 = call %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEEC2EmmS7_(%"struct.std::__2::__split_buffer"* %__v, i32 %call3, i32 %call4, %"class.std::__2::allocator.21"* nonnull align 1 dereferenceable(1) %1)
  %2 = load %"class.std::__2::allocator.21"*, %"class.std::__2::allocator.21"** %__a, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %__v, i32 0, i32 2
  %3 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %__end_, align 4
  %call6 = call %"class.draco::IndexType.17"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType.17"* %3) #8
  %4 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %__x.addr, align 4
  %call7 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.17"* @_ZNSt3__27forwardIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"class.draco::IndexType.17"* nonnull align 4 dereferenceable(4) %4) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE9constructIS5_JS5_EEEvRS6_PT_DpOT0_(%"class.std::__2::allocator.21"* nonnull align 1 dereferenceable(1) %2, %"class.draco::IndexType.17"* %call6, %"class.draco::IndexType.17"* nonnull align 4 dereferenceable(4) %call7)
  %__end_8 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %__v, i32 0, i32 2
  %5 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %__end_8, align 4
  %incdec.ptr = getelementptr inbounds %"class.draco::IndexType.17", %"class.draco::IndexType.17"* %5, i32 1
  store %"class.draco::IndexType.17"* %incdec.ptr, %"class.draco::IndexType.17"** %__end_8, align 4
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS4_RS6_EE(%"class.std::__2::vector.15"* %this1, %"struct.std::__2::__split_buffer"* nonnull align 4 dereferenceable(20) %__v)
  %call9 = call %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEED2Ev(%"struct.std::__2::__split_buffer"* %__v) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionC2ERS7_m(%"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* returned %this, %"class.std::__2::vector.15"* nonnull align 4 dereferenceable(12) %__v, i32 %__n) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"*, align 4
  %__v.addr = alloca %"class.std::__2::vector.15"*, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"** %this.addr, align 4
  store %"class.std::__2::vector.15"* %__v, %"class.std::__2::vector.15"** %__v.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"*, %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"** %this.addr, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::vector.15"*, %"class.std::__2::vector.15"** %__v.addr, align 4
  store %"class.std::__2::vector.15"* %0, %"class.std::__2::vector.15"** %__v_, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %1 = load %"class.std::__2::vector.15"*, %"class.std::__2::vector.15"** %__v.addr, align 4
  %2 = bitcast %"class.std::__2::vector.15"* %1 to %"class.std::__2::__vector_base.16"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.16", %"class.std::__2::__vector_base.16"* %2, i32 0, i32 1
  %3 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %__end_, align 4
  store %"class.draco::IndexType.17"* %3, %"class.draco::IndexType.17"** %__pos_, align 4
  %__new_end_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %this1, i32 0, i32 2
  %4 = load %"class.std::__2::vector.15"*, %"class.std::__2::vector.15"** %__v.addr, align 4
  %5 = bitcast %"class.std::__2::vector.15"* %4 to %"class.std::__2::__vector_base.16"*
  %__end_2 = getelementptr inbounds %"class.std::__2::__vector_base.16", %"class.std::__2::__vector_base.16"* %5, i32 0, i32 1
  %6 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %__end_2, align 4
  %7 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds %"class.draco::IndexType.17", %"class.draco::IndexType.17"* %6, i32 %7
  store %"class.draco::IndexType.17"* %add.ptr, %"class.draco::IndexType.17"** %__new_end_, align 4
  ret %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE9constructIS5_JS5_EEEvRS6_PT_DpOT0_(%"class.std::__2::allocator.21"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType.17"* %__p, %"class.draco::IndexType.17"* nonnull align 4 dereferenceable(4) %__args) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.21"*, align 4
  %__p.addr = alloca %"class.draco::IndexType.17"*, align 4
  %__args.addr = alloca %"class.draco::IndexType.17"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_construct.160", align 1
  store %"class.std::__2::allocator.21"* %__a, %"class.std::__2::allocator.21"** %__a.addr, align 4
  store %"class.draco::IndexType.17"* %__p, %"class.draco::IndexType.17"** %__p.addr, align 4
  store %"class.draco::IndexType.17"* %__args, %"class.draco::IndexType.17"** %__args.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_construct.160"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.21"*, %"class.std::__2::allocator.21"** %__a.addr, align 4
  %2 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %__p.addr, align 4
  %3 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.17"* @_ZNSt3__27forwardIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"class.draco::IndexType.17"* nonnull align 4 dereferenceable(4) %3) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE11__constructIS5_JS5_EEEvNS_17integral_constantIbLb1EEERS6_PT_DpOT0_(%"class.std::__2::allocator.21"* nonnull align 1 dereferenceable(1) %1, %"class.draco::IndexType.17"* %2, %"class.draco::IndexType.17"* nonnull align 4 dereferenceable(4) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.17"* @_ZNSt3__27forwardIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"class.draco::IndexType.17"* nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.draco::IndexType.17"*, align 4
  store %"class.draco::IndexType.17"* %__t, %"class.draco::IndexType.17"** %__t.addr, align 4
  %0 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %__t.addr, align 4
  ret %"class.draco::IndexType.17"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"*, align 4
  store %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"** %this.addr, align 4
  %this1 = load %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"*, %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %0 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %__pos_, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::vector.15"*, %"class.std::__2::vector.15"** %__v_, align 4
  %2 = bitcast %"class.std::__2::vector.15"* %1 to %"class.std::__2::__vector_base.16"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.16", %"class.std::__2::__vector_base.16"* %2, i32 0, i32 1
  store %"class.draco::IndexType.17"* %0, %"class.draco::IndexType.17"** %__end_, align 4
  ret %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE11__constructIS5_JS5_EEEvNS_17integral_constantIbLb1EEERS6_PT_DpOT0_(%"class.std::__2::allocator.21"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType.17"* %__p, %"class.draco::IndexType.17"* nonnull align 4 dereferenceable(4) %__args) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.21"*, align 4
  %__p.addr = alloca %"class.draco::IndexType.17"*, align 4
  %__args.addr = alloca %"class.draco::IndexType.17"*, align 4
  store %"class.std::__2::allocator.21"* %__a, %"class.std::__2::allocator.21"** %__a.addr, align 4
  store %"class.draco::IndexType.17"* %__p, %"class.draco::IndexType.17"** %__p.addr, align 4
  store %"class.draco::IndexType.17"* %__args, %"class.draco::IndexType.17"** %__args.addr, align 4
  %1 = load %"class.std::__2::allocator.21"*, %"class.std::__2::allocator.21"** %__a.addr, align 4
  %2 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %__p.addr, align 4
  %3 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.17"* @_ZNSt3__27forwardIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"class.draco::IndexType.17"* nonnull align 4 dereferenceable(4) %3) #8
  call void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE9constructIS4_JS4_EEEvPT_DpOT0_(%"class.std::__2::allocator.21"* %1, %"class.draco::IndexType.17"* %2, %"class.draco::IndexType.17"* nonnull align 4 dereferenceable(4) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE9constructIS4_JS4_EEEvPT_DpOT0_(%"class.std::__2::allocator.21"* %this, %"class.draco::IndexType.17"* %__p, %"class.draco::IndexType.17"* nonnull align 4 dereferenceable(4) %__args) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.21"*, align 4
  %__p.addr = alloca %"class.draco::IndexType.17"*, align 4
  %__args.addr = alloca %"class.draco::IndexType.17"*, align 4
  store %"class.std::__2::allocator.21"* %this, %"class.std::__2::allocator.21"** %this.addr, align 4
  store %"class.draco::IndexType.17"* %__p, %"class.draco::IndexType.17"** %__p.addr, align 4
  store %"class.draco::IndexType.17"* %__args, %"class.draco::IndexType.17"** %__args.addr, align 4
  %this1 = load %"class.std::__2::allocator.21"*, %"class.std::__2::allocator.21"** %this.addr, align 4
  %0 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %__p.addr, align 4
  %1 = bitcast %"class.draco::IndexType.17"* %0 to i8*
  %2 = bitcast i8* %1 to %"class.draco::IndexType.17"*
  %3 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.17"* @_ZNSt3__27forwardIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"class.draco::IndexType.17"* nonnull align 4 dereferenceable(4) %3) #8
  %4 = bitcast %"class.draco::IndexType.17"* %2 to i8*
  %5 = bitcast %"class.draco::IndexType.17"* %call to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 4, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE11__recommendEm(%"class.std::__2::vector.15"* %this, i32 %__new_size) #0 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %"class.std::__2::vector.15"*, align 4
  %__new_size.addr = alloca i32, align 4
  %__ms = alloca i32, align 4
  %__cap = alloca i32, align 4
  %ref.tmp = alloca i32, align 4
  store %"class.std::__2::vector.15"* %this, %"class.std::__2::vector.15"** %this.addr, align 4
  store i32 %__new_size, i32* %__new_size.addr, align 4
  %this1 = load %"class.std::__2::vector.15"*, %"class.std::__2::vector.15"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8max_sizeEv(%"class.std::__2::vector.15"* %this1) #8
  store i32 %call, i32* %__ms, align 4
  %0 = load i32, i32* %__new_size.addr, align 4
  %1 = load i32, i32* %__ms, align 4
  %cmp = icmp ugt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = bitcast %"class.std::__2::vector.15"* %this1 to %"class.std::__2::__vector_base_common"*
  call void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"* %2) #10
  unreachable

if.end:                                           ; preds = %entry
  %call2 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.15"* %this1) #8
  store i32 %call2, i32* %__cap, align 4
  %3 = load i32, i32* %__cap, align 4
  %4 = load i32, i32* %__ms, align 4
  %div = udiv i32 %4, 2
  %cmp3 = icmp uge i32 %3, %div
  br i1 %cmp3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %if.end
  %5 = load i32, i32* %__ms, align 4
  store i32 %5, i32* %retval, align 4
  br label %return

if.end5:                                          ; preds = %if.end
  %6 = load i32, i32* %__cap, align 4
  %mul = mul i32 2, %6
  store i32 %mul, i32* %ref.tmp, align 4
  %call6 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %__new_size.addr)
  %7 = load i32, i32* %call6, align 4
  store i32 %7, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end5, %if.then4
  %8 = load i32, i32* %retval, align 4
  ret i32 %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE8max_sizeEv(%"class.std::__2::vector.15"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.15"*, align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp3 = alloca i32, align 4
  store %"class.std::__2::vector.15"* %this, %"class.std::__2::vector.15"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.15"*, %"class.std::__2::vector.15"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.15"* %this1 to %"class.std::__2::__vector_base.16"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.21"* @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.16"* %0) #8
  %call2 = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE8max_sizeERKS6_(%"class.std::__2::allocator.21"* nonnull align 1 dereferenceable(1) %call) #8
  store i32 %call2, i32* %ref.tmp, align 4
  %call4 = call i32 @_ZNSt3__214numeric_limitsIlE3maxEv() #8
  store i32 %call4, i32* %ref.tmp3, align 4
  %call5 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %1 = load i32, i32* %call5, align 4
  ret i32 %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE8max_sizeERKS6_(%"class.std::__2::allocator.21"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.21"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_max_size.161", align 1
  store %"class.std::__2::allocator.21"* %__a, %"class.std::__2::allocator.21"** %__a.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_max_size.161"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.21"*, %"class.std::__2::allocator.21"** %__a.addr, align 4
  %call = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS6_(%"class.std::__2::allocator.21"* nonnull align 1 dereferenceable(1) %1) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.21"* @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.16"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.16"*, align 4
  store %"class.std::__2::__vector_base.16"* %this, %"class.std::__2::__vector_base.16"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.16"*, %"class.std::__2::__vector_base.16"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.16", %"class.std::__2::__vector_base.16"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.21"* @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.18"* %__end_cap_) #8
  ret %"class.std::__2::allocator.21"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS6_(%"class.std::__2::allocator.21"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.21"*, align 4
  store %"class.std::__2::allocator.21"* %__a, %"class.std::__2::allocator.21"** %__a.addr, align 4
  %1 = load %"class.std::__2::allocator.21"*, %"class.std::__2::allocator.21"** %__a.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE8max_sizeEv(%"class.std::__2::allocator.21"* %1) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.21"* @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.18"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.18"*, align 4
  store %"class.std::__2::__compressed_pair.18"* %this, %"class.std::__2::__compressed_pair.18"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.18"*, %"class.std::__2::__compressed_pair.18"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.18"* %this1 to %"struct.std::__2::__compressed_pair_elem.20"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.21"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.20"* %0) #8
  ret %"class.std::__2::allocator.21"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.21"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.20"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.20"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.20"* %this, %"struct.std::__2::__compressed_pair_elem.20"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.20"*, %"struct.std::__2::__compressed_pair_elem.20"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.20"* %this1 to %"class.std::__2::allocator.21"*
  ret %"class.std::__2::allocator.21"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco24MeshAttributeCornerTable4NextENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::MeshAttributeCornerTable"* %this, i32 %corner.coerce) #0 comdat {
entry:
  %retval = alloca %"class.draco::IndexType.9", align 4
  %corner = alloca %"class.draco::IndexType.9", align 4
  %this.addr = alloca %"class.draco::MeshAttributeCornerTable"*, align 4
  %agg.tmp = alloca %"class.draco::IndexType.9", align 4
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %corner, i32 0, i32 0
  store i32 %corner.coerce, i32* %coerce.dive, align 4
  store %"class.draco::MeshAttributeCornerTable"* %this, %"class.draco::MeshAttributeCornerTable"** %this.addr, align 4
  %this1 = load %"class.draco::MeshAttributeCornerTable"*, %"class.draco::MeshAttributeCornerTable"** %this.addr, align 4
  %corner_table_ = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 6
  %0 = load %"class.draco::CornerTable"*, %"class.draco::CornerTable"** %corner_table_, align 4
  %1 = bitcast %"class.draco::IndexType.9"* %agg.tmp to i8*
  %2 = bitcast %"class.draco::IndexType.9"* %corner to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 4, i1 false)
  %coerce.dive2 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %agg.tmp, i32 0, i32 0
  %3 = load i32, i32* %coerce.dive2, align 4
  %call = call i32 @_ZNK5draco11CornerTable4NextENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::CornerTable"* %0, i32 %3)
  %coerce.dive3 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %retval, i32 0, i32 0
  store i32 %call, i32* %coerce.dive3, align 4
  %coerce.dive4 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %retval, i32 0, i32 0
  %4 = load i32, i32* %coerce.dive4, align 4
  ret i32 %4
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco24MeshAttributeCornerTable8OppositeENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::MeshAttributeCornerTable"* %this, i32 %corner.coerce) #0 comdat {
entry:
  %retval = alloca %"class.draco::IndexType.9", align 4
  %corner = alloca %"class.draco::IndexType.9", align 4
  %this.addr = alloca %"class.draco::MeshAttributeCornerTable"*, align 4
  %agg.tmp = alloca %"class.draco::IndexType.9", align 4
  %agg.tmp4 = alloca %"class.draco::IndexType.9", align 4
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %corner, i32 0, i32 0
  store i32 %corner.coerce, i32* %coerce.dive, align 4
  store %"class.draco::MeshAttributeCornerTable"* %this, %"class.draco::MeshAttributeCornerTable"** %this.addr, align 4
  %this1 = load %"class.draco::MeshAttributeCornerTable"*, %"class.draco::MeshAttributeCornerTable"** %this.addr, align 4
  %call = call zeroext i1 @_ZNK5draco9IndexTypeIjNS_21CornerIndex_tag_type_EEeqERKS2_(%"class.draco::IndexType.9"* %corner, %"class.draco::IndexType.9"* nonnull align 4 dereferenceable(4) @_ZN5dracoL19kInvalidCornerIndexE)
  br i1 %call, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %0 = bitcast %"class.draco::IndexType.9"* %agg.tmp to i8*
  %1 = bitcast %"class.draco::IndexType.9"* %corner to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %0, i8* align 4 %1, i32 4, i1 false)
  %coerce.dive2 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %agg.tmp, i32 0, i32 0
  %2 = load i32, i32* %coerce.dive2, align 4
  %call3 = call zeroext i1 @_ZNK5draco24MeshAttributeCornerTable26IsCornerOppositeToSeamEdgeENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::MeshAttributeCornerTable"* %this1, i32 %2)
  br i1 %call3, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  %3 = bitcast %"class.draco::IndexType.9"* %retval to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 bitcast (%"class.draco::IndexType.9"* @_ZN5dracoL19kInvalidCornerIndexE to i8*), i32 4, i1 false)
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  %corner_table_ = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 6
  %4 = load %"class.draco::CornerTable"*, %"class.draco::CornerTable"** %corner_table_, align 4
  %5 = bitcast %"class.draco::IndexType.9"* %agg.tmp4 to i8*
  %6 = bitcast %"class.draco::IndexType.9"* %corner to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %5, i8* align 4 %6, i32 4, i1 false)
  %coerce.dive5 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %agg.tmp4, i32 0, i32 0
  %7 = load i32, i32* %coerce.dive5, align 4
  %call6 = call i32 @_ZNK5draco11CornerTable8OppositeENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::CornerTable"* %4, i32 %7)
  %coerce.dive7 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %retval, i32 0, i32 0
  store i32 %call6, i32* %coerce.dive7, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %coerce.dive8 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %retval, i32 0, i32 0
  %8 = load i32, i32* %coerce.dive8, align 4
  ret i32 %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE22__construct_one_at_endIJRKS4_EEEvDpOT_(%"class.std::__2::vector.7"* %this, %"class.draco::IndexType.9"* nonnull align 4 dereferenceable(4) %__args) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.7"*, align 4
  %__args.addr = alloca %"class.draco::IndexType.9"*, align 4
  %__tx = alloca %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::CornerIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::CornerIndex_tag_type_>>>::_ConstructTransaction", align 4
  store %"class.std::__2::vector.7"* %this, %"class.std::__2::vector.7"** %this.addr, align 4
  store %"class.draco::IndexType.9"* %__args, %"class.draco::IndexType.9"** %__args.addr, align 4
  %this1 = load %"class.std::__2::vector.7"*, %"class.std::__2::vector.7"** %this.addr, align 4
  %call = call %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::CornerIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::CornerIndex_tag_type_>>>::_ConstructTransaction"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionC2ERS7_m(%"struct.std::__2::vector<draco::IndexType<unsigned int, draco::CornerIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::CornerIndex_tag_type_>>>::_ConstructTransaction"* %__tx, %"class.std::__2::vector.7"* nonnull align 4 dereferenceable(12) %this1, i32 1)
  %0 = bitcast %"class.std::__2::vector.7"* %this1 to %"class.std::__2::__vector_base.8"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.13"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.8"* %0) #8
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::CornerIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::CornerIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::CornerIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::CornerIndex_tag_type_>>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %1 = load %"class.draco::IndexType.9"*, %"class.draco::IndexType.9"** %__pos_, align 4
  %call3 = call %"class.draco::IndexType.9"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType.9"* %1) #8
  %2 = load %"class.draco::IndexType.9"*, %"class.draco::IndexType.9"** %__args.addr, align 4
  %call4 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.9"* @_ZNSt3__27forwardIRKN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"class.draco::IndexType.9"* nonnull align 4 dereferenceable(4) %2) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_21CornerIndex_tag_type_EEEEEE9constructIS5_JRKS5_EEEvRS6_PT_DpOT0_(%"class.std::__2::allocator.13"* nonnull align 1 dereferenceable(1) %call2, %"class.draco::IndexType.9"* %call3, %"class.draco::IndexType.9"* nonnull align 4 dereferenceable(4) %call4)
  %__pos_5 = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::CornerIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::CornerIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::CornerIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::CornerIndex_tag_type_>>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %3 = load %"class.draco::IndexType.9"*, %"class.draco::IndexType.9"** %__pos_5, align 4
  %incdec.ptr = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %3, i32 1
  store %"class.draco::IndexType.9"* %incdec.ptr, %"class.draco::IndexType.9"** %__pos_5, align 4
  %call6 = call %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::CornerIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::CornerIndex_tag_type_>>>::_ConstructTransaction"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<draco::IndexType<unsigned int, draco::CornerIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::CornerIndex_tag_type_>>>::_ConstructTransaction"* %__tx) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE21__push_back_slow_pathIRKS4_EEvOT_(%"class.std::__2::vector.7"* %this, %"class.draco::IndexType.9"* nonnull align 4 dereferenceable(4) %__x) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.7"*, align 4
  %__x.addr = alloca %"class.draco::IndexType.9"*, align 4
  %__a = alloca %"class.std::__2::allocator.13"*, align 4
  %__v = alloca %"struct.std::__2::__split_buffer.156", align 4
  store %"class.std::__2::vector.7"* %this, %"class.std::__2::vector.7"** %this.addr, align 4
  store %"class.draco::IndexType.9"* %__x, %"class.draco::IndexType.9"** %__x.addr, align 4
  %this1 = load %"class.std::__2::vector.7"*, %"class.std::__2::vector.7"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.7"* %this1 to %"class.std::__2::__vector_base.8"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.13"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.8"* %0) #8
  store %"class.std::__2::allocator.13"* %call, %"class.std::__2::allocator.13"** %__a, align 4
  %call2 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.7"* %this1) #8
  %add = add i32 %call2, 1
  %call3 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE11__recommendEm(%"class.std::__2::vector.7"* %this1, i32 %add)
  %call4 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.7"* %this1) #8
  %1 = load %"class.std::__2::allocator.13"*, %"class.std::__2::allocator.13"** %__a, align 4
  %call5 = call %"struct.std::__2::__split_buffer.156"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEERNS_9allocatorIS4_EEEC2EmmS7_(%"struct.std::__2::__split_buffer.156"* %__v, i32 %call3, i32 %call4, %"class.std::__2::allocator.13"* nonnull align 1 dereferenceable(1) %1)
  %2 = load %"class.std::__2::allocator.13"*, %"class.std::__2::allocator.13"** %__a, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer.156", %"struct.std::__2::__split_buffer.156"* %__v, i32 0, i32 2
  %3 = load %"class.draco::IndexType.9"*, %"class.draco::IndexType.9"** %__end_, align 4
  %call6 = call %"class.draco::IndexType.9"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType.9"* %3) #8
  %4 = load %"class.draco::IndexType.9"*, %"class.draco::IndexType.9"** %__x.addr, align 4
  %call7 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.9"* @_ZNSt3__27forwardIRKN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"class.draco::IndexType.9"* nonnull align 4 dereferenceable(4) %4) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_21CornerIndex_tag_type_EEEEEE9constructIS5_JRKS5_EEEvRS6_PT_DpOT0_(%"class.std::__2::allocator.13"* nonnull align 1 dereferenceable(1) %2, %"class.draco::IndexType.9"* %call6, %"class.draco::IndexType.9"* nonnull align 4 dereferenceable(4) %call7)
  %__end_8 = getelementptr inbounds %"struct.std::__2::__split_buffer.156", %"struct.std::__2::__split_buffer.156"* %__v, i32 0, i32 2
  %5 = load %"class.draco::IndexType.9"*, %"class.draco::IndexType.9"** %__end_8, align 4
  %incdec.ptr = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %5, i32 1
  store %"class.draco::IndexType.9"* %incdec.ptr, %"class.draco::IndexType.9"** %__end_8, align 4
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS4_RS6_EE(%"class.std::__2::vector.7"* %this1, %"struct.std::__2::__split_buffer.156"* nonnull align 4 dereferenceable(20) %__v)
  %call9 = call %"struct.std::__2::__split_buffer.156"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEERNS_9allocatorIS4_EEED2Ev(%"struct.std::__2::__split_buffer.156"* %__v) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::CornerIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::CornerIndex_tag_type_>>>::_ConstructTransaction"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionC2ERS7_m(%"struct.std::__2::vector<draco::IndexType<unsigned int, draco::CornerIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::CornerIndex_tag_type_>>>::_ConstructTransaction"* returned %this, %"class.std::__2::vector.7"* nonnull align 4 dereferenceable(12) %__v, i32 %__n) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::CornerIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::CornerIndex_tag_type_>>>::_ConstructTransaction"*, align 4
  %__v.addr = alloca %"class.std::__2::vector.7"*, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::CornerIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::CornerIndex_tag_type_>>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::CornerIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::CornerIndex_tag_type_>>>::_ConstructTransaction"** %this.addr, align 4
  store %"class.std::__2::vector.7"* %__v, %"class.std::__2::vector.7"** %__v.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::CornerIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::CornerIndex_tag_type_>>>::_ConstructTransaction"*, %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::CornerIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::CornerIndex_tag_type_>>>::_ConstructTransaction"** %this.addr, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::CornerIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::CornerIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::CornerIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::CornerIndex_tag_type_>>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::vector.7"*, %"class.std::__2::vector.7"** %__v.addr, align 4
  store %"class.std::__2::vector.7"* %0, %"class.std::__2::vector.7"** %__v_, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::CornerIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::CornerIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::CornerIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::CornerIndex_tag_type_>>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %1 = load %"class.std::__2::vector.7"*, %"class.std::__2::vector.7"** %__v.addr, align 4
  %2 = bitcast %"class.std::__2::vector.7"* %1 to %"class.std::__2::__vector_base.8"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.8", %"class.std::__2::__vector_base.8"* %2, i32 0, i32 1
  %3 = load %"class.draco::IndexType.9"*, %"class.draco::IndexType.9"** %__end_, align 4
  store %"class.draco::IndexType.9"* %3, %"class.draco::IndexType.9"** %__pos_, align 4
  %__new_end_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::CornerIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::CornerIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::CornerIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::CornerIndex_tag_type_>>>::_ConstructTransaction"* %this1, i32 0, i32 2
  %4 = load %"class.std::__2::vector.7"*, %"class.std::__2::vector.7"** %__v.addr, align 4
  %5 = bitcast %"class.std::__2::vector.7"* %4 to %"class.std::__2::__vector_base.8"*
  %__end_2 = getelementptr inbounds %"class.std::__2::__vector_base.8", %"class.std::__2::__vector_base.8"* %5, i32 0, i32 1
  %6 = load %"class.draco::IndexType.9"*, %"class.draco::IndexType.9"** %__end_2, align 4
  %7 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %6, i32 %7
  store %"class.draco::IndexType.9"* %add.ptr, %"class.draco::IndexType.9"** %__new_end_, align 4
  ret %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::CornerIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::CornerIndex_tag_type_>>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_21CornerIndex_tag_type_EEEEEE9constructIS5_JRKS5_EEEvRS6_PT_DpOT0_(%"class.std::__2::allocator.13"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType.9"* %__p, %"class.draco::IndexType.9"* nonnull align 4 dereferenceable(4) %__args) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.13"*, align 4
  %__p.addr = alloca %"class.draco::IndexType.9"*, align 4
  %__args.addr = alloca %"class.draco::IndexType.9"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_construct.162", align 1
  store %"class.std::__2::allocator.13"* %__a, %"class.std::__2::allocator.13"** %__a.addr, align 4
  store %"class.draco::IndexType.9"* %__p, %"class.draco::IndexType.9"** %__p.addr, align 4
  store %"class.draco::IndexType.9"* %__args, %"class.draco::IndexType.9"** %__args.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_construct.162"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.13"*, %"class.std::__2::allocator.13"** %__a.addr, align 4
  %2 = load %"class.draco::IndexType.9"*, %"class.draco::IndexType.9"** %__p.addr, align 4
  %3 = load %"class.draco::IndexType.9"*, %"class.draco::IndexType.9"** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.9"* @_ZNSt3__27forwardIRKN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"class.draco::IndexType.9"* nonnull align 4 dereferenceable(4) %3) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_21CornerIndex_tag_type_EEEEEE11__constructIS5_JRKS5_EEEvNS_17integral_constantIbLb1EEERS6_PT_DpOT0_(%"class.std::__2::allocator.13"* nonnull align 1 dereferenceable(1) %1, %"class.draco::IndexType.9"* %2, %"class.draco::IndexType.9"* nonnull align 4 dereferenceable(4) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.9"* @_ZNSt3__27forwardIRKN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"class.draco::IndexType.9"* nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.draco::IndexType.9"*, align 4
  store %"class.draco::IndexType.9"* %__t, %"class.draco::IndexType.9"** %__t.addr, align 4
  %0 = load %"class.draco::IndexType.9"*, %"class.draco::IndexType.9"** %__t.addr, align 4
  ret %"class.draco::IndexType.9"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::CornerIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::CornerIndex_tag_type_>>>::_ConstructTransaction"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<draco::IndexType<unsigned int, draco::CornerIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::CornerIndex_tag_type_>>>::_ConstructTransaction"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::CornerIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::CornerIndex_tag_type_>>>::_ConstructTransaction"*, align 4
  store %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::CornerIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::CornerIndex_tag_type_>>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::CornerIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::CornerIndex_tag_type_>>>::_ConstructTransaction"** %this.addr, align 4
  %this1 = load %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::CornerIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::CornerIndex_tag_type_>>>::_ConstructTransaction"*, %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::CornerIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::CornerIndex_tag_type_>>>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::CornerIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::CornerIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::CornerIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::CornerIndex_tag_type_>>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %0 = load %"class.draco::IndexType.9"*, %"class.draco::IndexType.9"** %__pos_, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::CornerIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::CornerIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::CornerIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::CornerIndex_tag_type_>>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::vector.7"*, %"class.std::__2::vector.7"** %__v_, align 4
  %2 = bitcast %"class.std::__2::vector.7"* %1 to %"class.std::__2::__vector_base.8"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.8", %"class.std::__2::__vector_base.8"* %2, i32 0, i32 1
  store %"class.draco::IndexType.9"* %0, %"class.draco::IndexType.9"** %__end_, align 4
  ret %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::CornerIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::CornerIndex_tag_type_>>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_21CornerIndex_tag_type_EEEEEE11__constructIS5_JRKS5_EEEvNS_17integral_constantIbLb1EEERS6_PT_DpOT0_(%"class.std::__2::allocator.13"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType.9"* %__p, %"class.draco::IndexType.9"* nonnull align 4 dereferenceable(4) %__args) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.13"*, align 4
  %__p.addr = alloca %"class.draco::IndexType.9"*, align 4
  %__args.addr = alloca %"class.draco::IndexType.9"*, align 4
  store %"class.std::__2::allocator.13"* %__a, %"class.std::__2::allocator.13"** %__a.addr, align 4
  store %"class.draco::IndexType.9"* %__p, %"class.draco::IndexType.9"** %__p.addr, align 4
  store %"class.draco::IndexType.9"* %__args, %"class.draco::IndexType.9"** %__args.addr, align 4
  %1 = load %"class.std::__2::allocator.13"*, %"class.std::__2::allocator.13"** %__a.addr, align 4
  %2 = load %"class.draco::IndexType.9"*, %"class.draco::IndexType.9"** %__p.addr, align 4
  %3 = load %"class.draco::IndexType.9"*, %"class.draco::IndexType.9"** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.9"* @_ZNSt3__27forwardIRKN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"class.draco::IndexType.9"* nonnull align 4 dereferenceable(4) %3) #8
  call void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEEE9constructIS4_JRKS4_EEEvPT_DpOT0_(%"class.std::__2::allocator.13"* %1, %"class.draco::IndexType.9"* %2, %"class.draco::IndexType.9"* nonnull align 4 dereferenceable(4) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEEE9constructIS4_JRKS4_EEEvPT_DpOT0_(%"class.std::__2::allocator.13"* %this, %"class.draco::IndexType.9"* %__p, %"class.draco::IndexType.9"* nonnull align 4 dereferenceable(4) %__args) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.13"*, align 4
  %__p.addr = alloca %"class.draco::IndexType.9"*, align 4
  %__args.addr = alloca %"class.draco::IndexType.9"*, align 4
  store %"class.std::__2::allocator.13"* %this, %"class.std::__2::allocator.13"** %this.addr, align 4
  store %"class.draco::IndexType.9"* %__p, %"class.draco::IndexType.9"** %__p.addr, align 4
  store %"class.draco::IndexType.9"* %__args, %"class.draco::IndexType.9"** %__args.addr, align 4
  %this1 = load %"class.std::__2::allocator.13"*, %"class.std::__2::allocator.13"** %this.addr, align 4
  %0 = load %"class.draco::IndexType.9"*, %"class.draco::IndexType.9"** %__p.addr, align 4
  %1 = bitcast %"class.draco::IndexType.9"* %0 to i8*
  %2 = bitcast i8* %1 to %"class.draco::IndexType.9"*
  %3 = load %"class.draco::IndexType.9"*, %"class.draco::IndexType.9"** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.9"* @_ZNSt3__27forwardIRKN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"class.draco::IndexType.9"* nonnull align 4 dereferenceable(4) %3) #8
  %4 = bitcast %"class.draco::IndexType.9"* %2 to i8*
  %5 = bitcast %"class.draco::IndexType.9"* %call to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 4, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE11__recommendEm(%"class.std::__2::vector.7"* %this, i32 %__new_size) #0 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %"class.std::__2::vector.7"*, align 4
  %__new_size.addr = alloca i32, align 4
  %__ms = alloca i32, align 4
  %__cap = alloca i32, align 4
  %ref.tmp = alloca i32, align 4
  store %"class.std::__2::vector.7"* %this, %"class.std::__2::vector.7"** %this.addr, align 4
  store i32 %__new_size, i32* %__new_size.addr, align 4
  %this1 = load %"class.std::__2::vector.7"*, %"class.std::__2::vector.7"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE8max_sizeEv(%"class.std::__2::vector.7"* %this1) #8
  store i32 %call, i32* %__ms, align 4
  %0 = load i32, i32* %__new_size.addr, align 4
  %1 = load i32, i32* %__ms, align 4
  %cmp = icmp ugt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = bitcast %"class.std::__2::vector.7"* %this1 to %"class.std::__2::__vector_base_common"*
  call void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"* %2) #10
  unreachable

if.end:                                           ; preds = %entry
  %call2 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE8capacityEv(%"class.std::__2::vector.7"* %this1) #8
  store i32 %call2, i32* %__cap, align 4
  %3 = load i32, i32* %__cap, align 4
  %4 = load i32, i32* %__ms, align 4
  %div = udiv i32 %4, 2
  %cmp3 = icmp uge i32 %3, %div
  br i1 %cmp3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %if.end
  %5 = load i32, i32* %__ms, align 4
  store i32 %5, i32* %retval, align 4
  br label %return

if.end5:                                          ; preds = %if.end
  %6 = load i32, i32* %__cap, align 4
  %mul = mul i32 2, %6
  store i32 %mul, i32* %ref.tmp, align 4
  %call6 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %__new_size.addr)
  %7 = load i32, i32* %call6, align 4
  store i32 %7, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end5, %if.then4
  %8 = load i32, i32* %retval, align 4
  ret i32 %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE8max_sizeEv(%"class.std::__2::vector.7"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.7"*, align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp3 = alloca i32, align 4
  store %"class.std::__2::vector.7"* %this, %"class.std::__2::vector.7"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.7"*, %"class.std::__2::vector.7"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.7"* %this1 to %"class.std::__2::__vector_base.8"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.13"* @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.8"* %0) #8
  %call2 = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_21CornerIndex_tag_type_EEEEEE8max_sizeERKS6_(%"class.std::__2::allocator.13"* nonnull align 1 dereferenceable(1) %call) #8
  store i32 %call2, i32* %ref.tmp, align 4
  %call4 = call i32 @_ZNSt3__214numeric_limitsIlE3maxEv() #8
  store i32 %call4, i32* %ref.tmp3, align 4
  %call5 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %1 = load i32, i32* %call5, align 4
  ret i32 %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_21CornerIndex_tag_type_EEEEEE8max_sizeERKS6_(%"class.std::__2::allocator.13"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.13"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_max_size.163", align 1
  store %"class.std::__2::allocator.13"* %__a, %"class.std::__2::allocator.13"** %__a.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_max_size.163"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.13"*, %"class.std::__2::allocator.13"** %__a.addr, align 4
  %call = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_21CornerIndex_tag_type_EEEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS6_(%"class.std::__2::allocator.13"* nonnull align 1 dereferenceable(1) %1) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.13"* @_ZNKSt3__213__vector_baseIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.8"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.8"*, align 4
  store %"class.std::__2::__vector_base.8"* %this, %"class.std::__2::__vector_base.8"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.8"*, %"class.std::__2::__vector_base.8"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.8", %"class.std::__2::__vector_base.8"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.13"* @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.10"* %__end_cap_) #8
  ret %"class.std::__2::allocator.13"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_21CornerIndex_tag_type_EEEEEE10__max_sizeENS_17integral_constantIbLb1EEERKS6_(%"class.std::__2::allocator.13"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.13"*, align 4
  store %"class.std::__2::allocator.13"* %__a, %"class.std::__2::allocator.13"** %__a.addr, align 4
  %1 = load %"class.std::__2::allocator.13"*, %"class.std::__2::allocator.13"** %__a.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEEE8max_sizeEv(%"class.std::__2::allocator.13"* %1) #8
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.13"* @_ZNKSt3__217__compressed_pairIPN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEE6secondEv(%"class.std::__2::__compressed_pair.10"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.10"*, align 4
  store %"class.std::__2::__compressed_pair.10"* %this, %"class.std::__2::__compressed_pair.10"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.10"*, %"class.std::__2::__compressed_pair.10"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.10"* %this1 to %"struct.std::__2::__compressed_pair_elem.12"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.13"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_21CornerIndex_tag_type_EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.12"* %0) #8
  ret %"class.std::__2::allocator.13"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.13"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorIN5draco9IndexTypeIjNS2_21CornerIndex_tag_type_EEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.12"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.12"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.12"* %this, %"struct.std::__2::__compressed_pair_elem.12"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.12"*, %"struct.std::__2::__compressed_pair_elem.12"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.12"* %this1 to %"class.std::__2::allocator.13"*
  ret %"class.std::__2::allocator.13"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIbNS_9allocatorIbEEEixEm(%"class.std::__2::__bit_const_reference"* noalias sret align 4 %agg.result, %"class.std::__2::vector"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  call void @_ZNKSt3__26vectorIbNS_9allocatorIbEEE10__make_refEm(%"class.std::__2::__bit_const_reference"* sret align 4 %agg.result, %"class.std::__2::vector"* %this1, i32 %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__221__bit_const_referenceINS_6vectorIbNS_9allocatorIbEEEEEcvbEv(%"class.std::__2::__bit_const_reference"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__bit_const_reference"*, align 4
  store %"class.std::__2::__bit_const_reference"* %this, %"class.std::__2::__bit_const_reference"** %this.addr, align 4
  %this1 = load %"class.std::__2::__bit_const_reference"*, %"class.std::__2::__bit_const_reference"** %this.addr, align 4
  %__seg_ = getelementptr inbounds %"class.std::__2::__bit_const_reference", %"class.std::__2::__bit_const_reference"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__seg_, align 4
  %1 = load i32, i32* %0, align 4
  %__mask_ = getelementptr inbounds %"class.std::__2::__bit_const_reference", %"class.std::__2::__bit_const_reference"* %this1, i32 0, i32 1
  %2 = load i32, i32* %__mask_, align 4
  %and = and i32 %1, %2
  %tobool = icmp ne i32 %and, 0
  ret i1 %tobool
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIbNS_9allocatorIbEEE10__make_refEm(%"class.std::__2::__bit_const_reference"* noalias sret align 4 %agg.result, %"class.std::__2::vector"* %this, i32 %__pos) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__pos.addr = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__pos, i32* %__pos.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::vector", %"class.std::__2::vector"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__begin_, align 4
  %1 = load i32, i32* %__pos.addr, align 4
  %div = udiv i32 %1, 32
  %add.ptr = getelementptr inbounds i32, i32* %0, i32 %div
  %2 = load i32, i32* %__pos.addr, align 4
  %rem = urem i32 %2, 32
  %shl = shl i32 1, %rem
  %call = call %"class.std::__2::__bit_const_reference"* @_ZNSt3__221__bit_const_referenceINS_6vectorIbNS_9allocatorIbEEEEEC2EPKmm(%"class.std::__2::__bit_const_reference"* %agg.result, i32* %add.ptr, i32 %shl) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__bit_const_reference"* @_ZNSt3__221__bit_const_referenceINS_6vectorIbNS_9allocatorIbEEEEEC2EPKmm(%"class.std::__2::__bit_const_reference"* returned %this, i32* %__s, i32 %__m) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__bit_const_reference"*, align 4
  %__s.addr = alloca i32*, align 4
  %__m.addr = alloca i32, align 4
  store %"class.std::__2::__bit_const_reference"* %this, %"class.std::__2::__bit_const_reference"** %this.addr, align 4
  store i32* %__s, i32** %__s.addr, align 4
  store i32 %__m, i32* %__m.addr, align 4
  %this1 = load %"class.std::__2::__bit_const_reference"*, %"class.std::__2::__bit_const_reference"** %this.addr, align 4
  %__seg_ = getelementptr inbounds %"class.std::__2::__bit_const_reference", %"class.std::__2::__bit_const_reference"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__s.addr, align 4
  store i32* %0, i32** %__seg_, align 4
  %__mask_ = getelementptr inbounds %"class.std::__2::__bit_const_reference", %"class.std::__2::__bit_const_reference"* %this1, i32 0, i32 1
  %1 = load i32, i32* %__m.addr, align 4
  store i32 %1, i32* %__mask_, align 4
  ret %"class.std::__2::__bit_const_reference"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE9push_backERKS4_(%"class.std::__2::vector.15"* %this, %"class.draco::IndexType.17"* nonnull align 4 dereferenceable(4) %__x) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.15"*, align 4
  %__x.addr = alloca %"class.draco::IndexType.17"*, align 4
  store %"class.std::__2::vector.15"* %this, %"class.std::__2::vector.15"** %this.addr, align 4
  store %"class.draco::IndexType.17"* %__x, %"class.draco::IndexType.17"** %__x.addr, align 4
  %this1 = load %"class.std::__2::vector.15"*, %"class.std::__2::vector.15"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.15"* %this1 to %"class.std::__2::__vector_base.16"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.16", %"class.std::__2::__vector_base.16"* %0, i32 0, i32 1
  %1 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector.15"* %this1 to %"class.std::__2::__vector_base.16"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.17"** @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE9__end_capEv(%"class.std::__2::__vector_base.16"* %2) #8
  %3 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %call, align 4
  %cmp = icmp ne %"class.draco::IndexType.17"* %1, %3
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %4 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %__x.addr, align 4
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE22__construct_one_at_endIJRKS4_EEEvDpOT_(%"class.std::__2::vector.15"* %this1, %"class.draco::IndexType.17"* nonnull align 4 dereferenceable(4) %4)
  br label %if.end

if.else:                                          ; preds = %entry
  %5 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %__x.addr, align 4
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE21__push_back_slow_pathIRKS4_EEvOT_(%"class.std::__2::vector.15"* %this1, %"class.draco::IndexType.17"* nonnull align 4 dereferenceable(4) %5)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE22__construct_one_at_endIJRKS4_EEEvDpOT_(%"class.std::__2::vector.15"* %this, %"class.draco::IndexType.17"* nonnull align 4 dereferenceable(4) %__args) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.15"*, align 4
  %__args.addr = alloca %"class.draco::IndexType.17"*, align 4
  %__tx = alloca %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction", align 4
  store %"class.std::__2::vector.15"* %this, %"class.std::__2::vector.15"** %this.addr, align 4
  store %"class.draco::IndexType.17"* %__args, %"class.draco::IndexType.17"** %__args.addr, align 4
  %this1 = load %"class.std::__2::vector.15"*, %"class.std::__2::vector.15"** %this.addr, align 4
  %call = call %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionC2ERS7_m(%"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %__tx, %"class.std::__2::vector.15"* nonnull align 4 dereferenceable(12) %this1, i32 1)
  %0 = bitcast %"class.std::__2::vector.15"* %this1 to %"class.std::__2::__vector_base.16"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.21"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.16"* %0) #8
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %1 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %__pos_, align 4
  %call3 = call %"class.draco::IndexType.17"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType.17"* %1) #8
  %2 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %__args.addr, align 4
  %call4 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.17"* @_ZNSt3__27forwardIRKN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"class.draco::IndexType.17"* nonnull align 4 dereferenceable(4) %2) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE9constructIS5_JRKS5_EEEvRS6_PT_DpOT0_(%"class.std::__2::allocator.21"* nonnull align 1 dereferenceable(1) %call2, %"class.draco::IndexType.17"* %call3, %"class.draco::IndexType.17"* nonnull align 4 dereferenceable(4) %call4)
  %__pos_5 = getelementptr inbounds %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction", %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %3 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %__pos_5, align 4
  %incdec.ptr = getelementptr inbounds %"class.draco::IndexType.17", %"class.draco::IndexType.17"* %3, i32 1
  store %"class.draco::IndexType.17"* %incdec.ptr, %"class.draco::IndexType.17"** %__pos_5, align 4
  %call6 = call %"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>, std::__2::allocator<draco::IndexType<unsigned int, draco::AttributeValueIndex_tag_type_>>>::_ConstructTransaction"* %__tx) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE21__push_back_slow_pathIRKS4_EEvOT_(%"class.std::__2::vector.15"* %this, %"class.draco::IndexType.17"* nonnull align 4 dereferenceable(4) %__x) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.15"*, align 4
  %__x.addr = alloca %"class.draco::IndexType.17"*, align 4
  %__a = alloca %"class.std::__2::allocator.21"*, align 4
  %__v = alloca %"struct.std::__2::__split_buffer", align 4
  store %"class.std::__2::vector.15"* %this, %"class.std::__2::vector.15"** %this.addr, align 4
  store %"class.draco::IndexType.17"* %__x, %"class.draco::IndexType.17"** %__x.addr, align 4
  %this1 = load %"class.std::__2::vector.15"*, %"class.std::__2::vector.15"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.15"* %this1 to %"class.std::__2::__vector_base.16"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.21"* @_ZNSt3__213__vector_baseIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE7__allocEv(%"class.std::__2::__vector_base.16"* %0) #8
  store %"class.std::__2::allocator.21"* %call, %"class.std::__2::allocator.21"** %__a, align 4
  %call2 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.15"* %this1) #8
  %add = add i32 %call2, 1
  %call3 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE11__recommendEm(%"class.std::__2::vector.15"* %this1, i32 %add)
  %call4 = call i32 @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE4sizeEv(%"class.std::__2::vector.15"* %this1) #8
  %1 = load %"class.std::__2::allocator.21"*, %"class.std::__2::allocator.21"** %__a, align 4
  %call5 = call %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEEC2EmmS7_(%"struct.std::__2::__split_buffer"* %__v, i32 %call3, i32 %call4, %"class.std::__2::allocator.21"* nonnull align 1 dereferenceable(1) %1)
  %2 = load %"class.std::__2::allocator.21"*, %"class.std::__2::allocator.21"** %__a, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %__v, i32 0, i32 2
  %3 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %__end_, align 4
  %call6 = call %"class.draco::IndexType.17"* @_ZNSt3__212__to_addressIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEPT_S6_(%"class.draco::IndexType.17"* %3) #8
  %4 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %__x.addr, align 4
  %call7 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.17"* @_ZNSt3__27forwardIRKN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"class.draco::IndexType.17"* nonnull align 4 dereferenceable(4) %4) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE9constructIS5_JRKS5_EEEvRS6_PT_DpOT0_(%"class.std::__2::allocator.21"* nonnull align 1 dereferenceable(1) %2, %"class.draco::IndexType.17"* %call6, %"class.draco::IndexType.17"* nonnull align 4 dereferenceable(4) %call7)
  %__end_8 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %__v, i32 0, i32 2
  %5 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %__end_8, align 4
  %incdec.ptr = getelementptr inbounds %"class.draco::IndexType.17", %"class.draco::IndexType.17"* %5, i32 1
  store %"class.draco::IndexType.17"* %incdec.ptr, %"class.draco::IndexType.17"** %__end_8, align 4
  call void @_ZNSt3__26vectorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEENS_9allocatorIS4_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS4_RS6_EE(%"class.std::__2::vector.15"* %this1, %"struct.std::__2::__split_buffer"* nonnull align 4 dereferenceable(20) %__v)
  %call9 = call %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEERNS_9allocatorIS4_EEED2Ev(%"struct.std::__2::__split_buffer"* %__v) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE9constructIS5_JRKS5_EEEvRS6_PT_DpOT0_(%"class.std::__2::allocator.21"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType.17"* %__p, %"class.draco::IndexType.17"* nonnull align 4 dereferenceable(4) %__args) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.21"*, align 4
  %__p.addr = alloca %"class.draco::IndexType.17"*, align 4
  %__args.addr = alloca %"class.draco::IndexType.17"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_construct.164", align 1
  store %"class.std::__2::allocator.21"* %__a, %"class.std::__2::allocator.21"** %__a.addr, align 4
  store %"class.draco::IndexType.17"* %__p, %"class.draco::IndexType.17"** %__p.addr, align 4
  store %"class.draco::IndexType.17"* %__args, %"class.draco::IndexType.17"** %__args.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_construct.164"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.21"*, %"class.std::__2::allocator.21"** %__a.addr, align 4
  %2 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %__p.addr, align 4
  %3 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.17"* @_ZNSt3__27forwardIRKN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"class.draco::IndexType.17"* nonnull align 4 dereferenceable(4) %3) #8
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE11__constructIS5_JRKS5_EEEvNS_17integral_constantIbLb1EEERS6_PT_DpOT0_(%"class.std::__2::allocator.21"* nonnull align 1 dereferenceable(1) %1, %"class.draco::IndexType.17"* %2, %"class.draco::IndexType.17"* nonnull align 4 dereferenceable(4) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::IndexType.17"* @_ZNSt3__27forwardIRKN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"class.draco::IndexType.17"* nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.draco::IndexType.17"*, align 4
  store %"class.draco::IndexType.17"* %__t, %"class.draco::IndexType.17"** %__t.addr, align 4
  %0 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %__t.addr, align 4
  ret %"class.draco::IndexType.17"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIN5draco9IndexTypeIjNS2_29AttributeValueIndex_tag_type_EEEEEE11__constructIS5_JRKS5_EEEvNS_17integral_constantIbLb1EEERS6_PT_DpOT0_(%"class.std::__2::allocator.21"* nonnull align 1 dereferenceable(1) %__a, %"class.draco::IndexType.17"* %__p, %"class.draco::IndexType.17"* nonnull align 4 dereferenceable(4) %__args) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.21"*, align 4
  %__p.addr = alloca %"class.draco::IndexType.17"*, align 4
  %__args.addr = alloca %"class.draco::IndexType.17"*, align 4
  store %"class.std::__2::allocator.21"* %__a, %"class.std::__2::allocator.21"** %__a.addr, align 4
  store %"class.draco::IndexType.17"* %__p, %"class.draco::IndexType.17"** %__p.addr, align 4
  store %"class.draco::IndexType.17"* %__args, %"class.draco::IndexType.17"** %__args.addr, align 4
  %1 = load %"class.std::__2::allocator.21"*, %"class.std::__2::allocator.21"** %__a.addr, align 4
  %2 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %__p.addr, align 4
  %3 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.17"* @_ZNSt3__27forwardIRKN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"class.draco::IndexType.17"* nonnull align 4 dereferenceable(4) %3) #8
  call void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE9constructIS4_JRKS4_EEEvPT_DpOT0_(%"class.std::__2::allocator.21"* %1, %"class.draco::IndexType.17"* %2, %"class.draco::IndexType.17"* nonnull align 4 dereferenceable(4) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEE9constructIS4_JRKS4_EEEvPT_DpOT0_(%"class.std::__2::allocator.21"* %this, %"class.draco::IndexType.17"* %__p, %"class.draco::IndexType.17"* nonnull align 4 dereferenceable(4) %__args) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.21"*, align 4
  %__p.addr = alloca %"class.draco::IndexType.17"*, align 4
  %__args.addr = alloca %"class.draco::IndexType.17"*, align 4
  store %"class.std::__2::allocator.21"* %this, %"class.std::__2::allocator.21"** %this.addr, align 4
  store %"class.draco::IndexType.17"* %__p, %"class.draco::IndexType.17"** %__p.addr, align 4
  store %"class.draco::IndexType.17"* %__args, %"class.draco::IndexType.17"** %__args.addr, align 4
  %this1 = load %"class.std::__2::allocator.21"*, %"class.std::__2::allocator.21"** %this.addr, align 4
  %0 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %__p.addr, align 4
  %1 = bitcast %"class.draco::IndexType.17"* %0 to i8*
  %2 = bitcast i8* %1 to %"class.draco::IndexType.17"*
  %3 = load %"class.draco::IndexType.17"*, %"class.draco::IndexType.17"** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.17"* @_ZNSt3__27forwardIRKN5draco9IndexTypeIjNS1_29AttributeValueIndex_tag_type_EEEEEOT_RNS_16remove_referenceIS7_E4typeE(%"class.draco::IndexType.17"* nonnull align 4 dereferenceable(4) %3) #8
  %4 = bitcast %"class.draco::IndexType.17"* %2 to i8*
  %5 = bitcast %"class.draco::IndexType.17"* %call to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 4, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco24MeshAttributeCornerTable14LeftMostCornerENS_9IndexTypeIjNS_21VertexIndex_tag_type_EEE(%"class.draco::MeshAttributeCornerTable"* %this, i32 %v.coerce) #0 comdat {
entry:
  %retval = alloca %"class.draco::IndexType.9", align 4
  %v = alloca %"class.draco::IndexType", align 4
  %this.addr = alloca %"class.draco::MeshAttributeCornerTable"*, align 4
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType", %"class.draco::IndexType"* %v, i32 0, i32 0
  store i32 %v.coerce, i32* %coerce.dive, align 4
  store %"class.draco::MeshAttributeCornerTable"* %this, %"class.draco::MeshAttributeCornerTable"** %this.addr, align 4
  %this1 = load %"class.draco::MeshAttributeCornerTable"*, %"class.draco::MeshAttributeCornerTable"** %this.addr, align 4
  %vertex_to_left_most_corner_map_ = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 4
  %call = call i32 @_ZNK5draco9IndexTypeIjNS_21VertexIndex_tag_type_EE5valueEv(%"class.draco::IndexType"* %v)
  %call2 = call nonnull align 4 dereferenceable(4) %"class.draco::IndexType.9"* @_ZNKSt3__26vectorIN5draco9IndexTypeIjNS1_21CornerIndex_tag_type_EEENS_9allocatorIS4_EEEixEm(%"class.std::__2::vector.7"* %vertex_to_left_most_corner_map_, i32 %call) #8
  %0 = bitcast %"class.draco::IndexType.9"* %retval to i8*
  %1 = bitcast %"class.draco::IndexType.9"* %call2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %0, i8* align 4 %1, i32 4, i1 false)
  %coerce.dive3 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %retval, i32 0, i32 0
  %2 = load i32, i32* %coerce.dive3, align 4
  ret i32 %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco24MeshAttributeCornerTable10SwingRightENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::MeshAttributeCornerTable"* %this, i32 %corner.coerce) #0 comdat {
entry:
  %retval = alloca %"class.draco::IndexType.9", align 4
  %corner = alloca %"class.draco::IndexType.9", align 4
  %this.addr = alloca %"class.draco::MeshAttributeCornerTable"*, align 4
  %agg.tmp = alloca %"class.draco::IndexType.9", align 4
  %agg.tmp2 = alloca %"class.draco::IndexType.9", align 4
  %agg.tmp3 = alloca %"class.draco::IndexType.9", align 4
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %corner, i32 0, i32 0
  store i32 %corner.coerce, i32* %coerce.dive, align 4
  store %"class.draco::MeshAttributeCornerTable"* %this, %"class.draco::MeshAttributeCornerTable"** %this.addr, align 4
  %this1 = load %"class.draco::MeshAttributeCornerTable"*, %"class.draco::MeshAttributeCornerTable"** %this.addr, align 4
  %0 = bitcast %"class.draco::IndexType.9"* %agg.tmp3 to i8*
  %1 = bitcast %"class.draco::IndexType.9"* %corner to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %0, i8* align 4 %1, i32 4, i1 false)
  %coerce.dive4 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %agg.tmp3, i32 0, i32 0
  %2 = load i32, i32* %coerce.dive4, align 4
  %call = call i32 @_ZNK5draco24MeshAttributeCornerTable8PreviousENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::MeshAttributeCornerTable"* %this1, i32 %2)
  %coerce.dive5 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %agg.tmp2, i32 0, i32 0
  store i32 %call, i32* %coerce.dive5, align 4
  %coerce.dive6 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %agg.tmp2, i32 0, i32 0
  %3 = load i32, i32* %coerce.dive6, align 4
  %call7 = call i32 @_ZNK5draco24MeshAttributeCornerTable8OppositeENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::MeshAttributeCornerTable"* %this1, i32 %3)
  %coerce.dive8 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %agg.tmp, i32 0, i32 0
  store i32 %call7, i32* %coerce.dive8, align 4
  %coerce.dive9 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %agg.tmp, i32 0, i32 0
  %4 = load i32, i32* %coerce.dive9, align 4
  %call10 = call i32 @_ZNK5draco24MeshAttributeCornerTable8PreviousENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::MeshAttributeCornerTable"* %this1, i32 %4)
  %coerce.dive11 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %retval, i32 0, i32 0
  store i32 %call10, i32* %coerce.dive11, align 4
  %coerce.dive12 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %retval, i32 0, i32 0
  %5 = load i32, i32* %coerce.dive12, align 4
  ret i32 %5
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco24MeshAttributeCornerTable8PreviousENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::MeshAttributeCornerTable"* %this, i32 %corner.coerce) #0 comdat {
entry:
  %retval = alloca %"class.draco::IndexType.9", align 4
  %corner = alloca %"class.draco::IndexType.9", align 4
  %this.addr = alloca %"class.draco::MeshAttributeCornerTable"*, align 4
  %agg.tmp = alloca %"class.draco::IndexType.9", align 4
  %coerce.dive = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %corner, i32 0, i32 0
  store i32 %corner.coerce, i32* %coerce.dive, align 4
  store %"class.draco::MeshAttributeCornerTable"* %this, %"class.draco::MeshAttributeCornerTable"** %this.addr, align 4
  %this1 = load %"class.draco::MeshAttributeCornerTable"*, %"class.draco::MeshAttributeCornerTable"** %this.addr, align 4
  %corner_table_ = getelementptr inbounds %"class.draco::MeshAttributeCornerTable", %"class.draco::MeshAttributeCornerTable"* %this1, i32 0, i32 6
  %0 = load %"class.draco::CornerTable"*, %"class.draco::CornerTable"** %corner_table_, align 4
  %1 = bitcast %"class.draco::IndexType.9"* %agg.tmp to i8*
  %2 = bitcast %"class.draco::IndexType.9"* %corner to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 4, i1 false)
  %coerce.dive2 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %agg.tmp, i32 0, i32 0
  %3 = load i32, i32* %coerce.dive2, align 4
  %call = call i32 @_ZNK5draco11CornerTable8PreviousENS_9IndexTypeIjNS_21CornerIndex_tag_type_EEE(%"class.draco::CornerTable"* %0, i32 %3)
  %coerce.dive3 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %retval, i32 0, i32 0
  store i32 %call, i32* %coerce.dive3, align 4
  %coerce.dive4 = getelementptr inbounds %"class.draco::IndexType.9", %"class.draco::IndexType.9"* %retval, i32 0, i32 0
  %4 = load i32, i32* %coerce.dive4, align 4
  ret i32 %4
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { noreturn "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { noinline noreturn nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nobuiltin allocsize(0) "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { argmemonly nounwind willreturn writeonly }
attributes #8 = { nounwind }
attributes #9 = { builtin nounwind }
attributes #10 = { noreturn }
attributes #11 = { builtin allocsize(0) }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
