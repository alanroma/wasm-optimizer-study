; ModuleID = './draco/src/draco/compression/point_cloud/point_cloud_decoder.cc'
source_filename = "./draco/src/draco/compression/point_cloud/point_cloud_decoder.cc"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%"class.draco::PointCloudDecoder" = type { i32 (...)**, %"class.draco::PointCloud"*, %"class.std::__2::vector.94", %"class.std::__2::vector.87", %"class.draco::DecoderBuffer"*, i8, i8, %"class.draco::DracoOptions"* }
%"class.draco::PointCloud" = type { i32 (...)**, %"class.std::__2::unique_ptr", %"class.std::__2::vector.51", [5 x %"class.std::__2::vector.87"], i32 }
%"class.std::__2::unique_ptr" = type { %"class.std::__2::__compressed_pair" }
%"class.std::__2::__compressed_pair" = type { %"struct.std::__2::__compressed_pair_elem" }
%"struct.std::__2::__compressed_pair_elem" = type { %"class.draco::GeometryMetadata"* }
%"class.draco::GeometryMetadata" = type { %"class.draco::Metadata", %"class.std::__2::vector" }
%"class.draco::Metadata" = type { %"class.std::__2::unordered_map", %"class.std::__2::unordered_map.17" }
%"class.std::__2::unordered_map" = type { %"class.std::__2::__hash_table" }
%"class.std::__2::__hash_table" = type { %"class.std::__2::unique_ptr.0", %"class.std::__2::__compressed_pair.7", %"class.std::__2::__compressed_pair.12", %"class.std::__2::__compressed_pair.14" }
%"class.std::__2::unique_ptr.0" = type { %"class.std::__2::__compressed_pair.1" }
%"class.std::__2::__compressed_pair.1" = type { %"struct.std::__2::__compressed_pair_elem.2", %"struct.std::__2::__compressed_pair_elem.3" }
%"struct.std::__2::__compressed_pair_elem.2" = type { %"struct.std::__2::__hash_node_base"** }
%"struct.std::__2::__hash_node_base" = type { %"struct.std::__2::__hash_node_base"* }
%"struct.std::__2::__compressed_pair_elem.3" = type { %"class.std::__2::__bucket_list_deallocator" }
%"class.std::__2::__bucket_list_deallocator" = type { %"class.std::__2::__compressed_pair.4" }
%"class.std::__2::__compressed_pair.4" = type { %"struct.std::__2::__compressed_pair_elem.5" }
%"struct.std::__2::__compressed_pair_elem.5" = type { i32 }
%"class.std::__2::__compressed_pair.7" = type { %"struct.std::__2::__compressed_pair_elem.8" }
%"struct.std::__2::__compressed_pair_elem.8" = type { %"struct.std::__2::__hash_node_base" }
%"class.std::__2::__compressed_pair.12" = type { %"struct.std::__2::__compressed_pair_elem.5" }
%"class.std::__2::__compressed_pair.14" = type { %"struct.std::__2::__compressed_pair_elem.15" }
%"struct.std::__2::__compressed_pair_elem.15" = type { float }
%"class.std::__2::unordered_map.17" = type { %"class.std::__2::__hash_table.18" }
%"class.std::__2::__hash_table.18" = type { %"class.std::__2::unique_ptr.19", %"class.std::__2::__compressed_pair.29", %"class.std::__2::__compressed_pair.34", %"class.std::__2::__compressed_pair.37" }
%"class.std::__2::unique_ptr.19" = type { %"class.std::__2::__compressed_pair.20" }
%"class.std::__2::__compressed_pair.20" = type { %"struct.std::__2::__compressed_pair_elem.21", %"struct.std::__2::__compressed_pair_elem.23" }
%"struct.std::__2::__compressed_pair_elem.21" = type { %"struct.std::__2::__hash_node_base.22"** }
%"struct.std::__2::__hash_node_base.22" = type { %"struct.std::__2::__hash_node_base.22"* }
%"struct.std::__2::__compressed_pair_elem.23" = type { %"class.std::__2::__bucket_list_deallocator.24" }
%"class.std::__2::__bucket_list_deallocator.24" = type { %"class.std::__2::__compressed_pair.25" }
%"class.std::__2::__compressed_pair.25" = type { %"struct.std::__2::__compressed_pair_elem.5" }
%"class.std::__2::__compressed_pair.29" = type { %"struct.std::__2::__compressed_pair_elem.30" }
%"struct.std::__2::__compressed_pair_elem.30" = type { %"struct.std::__2::__hash_node_base.22" }
%"class.std::__2::__compressed_pair.34" = type { %"struct.std::__2::__compressed_pair_elem.5" }
%"class.std::__2::__compressed_pair.37" = type { %"struct.std::__2::__compressed_pair_elem.15" }
%"class.std::__2::vector" = type { %"class.std::__2::__vector_base" }
%"class.std::__2::__vector_base" = type { %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"*, %"class.std::__2::__compressed_pair.44" }
%"class.std::__2::unique_ptr.40" = type { %"class.std::__2::__compressed_pair.41" }
%"class.std::__2::__compressed_pair.41" = type { %"struct.std::__2::__compressed_pair_elem.42" }
%"struct.std::__2::__compressed_pair_elem.42" = type { %"class.draco::AttributeMetadata"* }
%"class.draco::AttributeMetadata" = type { %"class.draco::Metadata", i32 }
%"class.std::__2::__compressed_pair.44" = type { %"struct.std::__2::__compressed_pair_elem.45" }
%"struct.std::__2::__compressed_pair_elem.45" = type { %"class.std::__2::unique_ptr.40"* }
%"class.std::__2::vector.51" = type { %"class.std::__2::__vector_base.52" }
%"class.std::__2::__vector_base.52" = type { %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"*, %"class.std::__2::__compressed_pair.82" }
%"class.std::__2::unique_ptr.53" = type { %"class.std::__2::__compressed_pair.54" }
%"class.std::__2::__compressed_pair.54" = type { %"struct.std::__2::__compressed_pair_elem.55" }
%"struct.std::__2::__compressed_pair_elem.55" = type { %"class.draco::PointAttribute"* }
%"class.draco::PointAttribute" = type <{ %"class.draco::GeometryAttribute", %"class.std::__2::unique_ptr.63", %"class.draco::IndexTypeVector", i32, i8, [3 x i8], %"class.std::__2::unique_ptr.75", [4 x i8] }>
%"class.draco::GeometryAttribute" = type { %"class.draco::DataBuffer"*, %"struct.draco::DataBufferDescriptor", i8, i32, i8, i64, i64, i32, i32 }
%"class.draco::DataBuffer" = type { %"class.std::__2::vector.56", %"struct.draco::DataBufferDescriptor" }
%"class.std::__2::vector.56" = type { %"class.std::__2::__vector_base.57" }
%"class.std::__2::__vector_base.57" = type { i8*, i8*, %"class.std::__2::__compressed_pair.58" }
%"class.std::__2::__compressed_pair.58" = type { %"struct.std::__2::__compressed_pair_elem.59" }
%"struct.std::__2::__compressed_pair_elem.59" = type { i8* }
%"struct.draco::DataBufferDescriptor" = type { i64, i64 }
%"class.std::__2::unique_ptr.63" = type { %"class.std::__2::__compressed_pair.64" }
%"class.std::__2::__compressed_pair.64" = type { %"struct.std::__2::__compressed_pair_elem.65" }
%"struct.std::__2::__compressed_pair_elem.65" = type { %"class.draco::DataBuffer"* }
%"class.draco::IndexTypeVector" = type { %"class.std::__2::vector.68" }
%"class.std::__2::vector.68" = type { %"class.std::__2::__vector_base.69" }
%"class.std::__2::__vector_base.69" = type { %"class.draco::IndexType"*, %"class.draco::IndexType"*, %"class.std::__2::__compressed_pair.70" }
%"class.draco::IndexType" = type { i32 }
%"class.std::__2::__compressed_pair.70" = type { %"struct.std::__2::__compressed_pair_elem.71" }
%"struct.std::__2::__compressed_pair_elem.71" = type { %"class.draco::IndexType"* }
%"class.std::__2::unique_ptr.75" = type { %"class.std::__2::__compressed_pair.76" }
%"class.std::__2::__compressed_pair.76" = type { %"struct.std::__2::__compressed_pair_elem.77" }
%"struct.std::__2::__compressed_pair_elem.77" = type { %"class.draco::AttributeTransformData"* }
%"class.draco::AttributeTransformData" = type { i32, %"class.draco::DataBuffer" }
%"class.std::__2::__compressed_pair.82" = type { %"struct.std::__2::__compressed_pair_elem.83" }
%"struct.std::__2::__compressed_pair_elem.83" = type { %"class.std::__2::unique_ptr.53"* }
%"class.std::__2::vector.94" = type { %"class.std::__2::__vector_base.95" }
%"class.std::__2::__vector_base.95" = type { %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"*, %"class.std::__2::__compressed_pair.101" }
%"class.std::__2::unique_ptr.96" = type { %"class.std::__2::__compressed_pair.97" }
%"class.std::__2::__compressed_pair.97" = type { %"struct.std::__2::__compressed_pair_elem.98" }
%"struct.std::__2::__compressed_pair_elem.98" = type { %"class.draco::AttributesDecoderInterface"* }
%"class.draco::AttributesDecoderInterface" = type { i32 (...)** }
%"class.std::__2::__compressed_pair.101" = type { %"struct.std::__2::__compressed_pair_elem.102" }
%"struct.std::__2::__compressed_pair_elem.102" = type { %"class.std::__2::unique_ptr.96"* }
%"class.std::__2::vector.87" = type { %"class.std::__2::__vector_base.88" }
%"class.std::__2::__vector_base.88" = type { i32*, i32*, %"class.std::__2::__compressed_pair.89" }
%"class.std::__2::__compressed_pair.89" = type { %"struct.std::__2::__compressed_pair_elem.90" }
%"struct.std::__2::__compressed_pair_elem.90" = type { i32* }
%"class.draco::DecoderBuffer" = type { i8*, i64, i64, %"class.draco::DecoderBuffer::BitDecoder", i8, i16 }
%"class.draco::DecoderBuffer::BitDecoder" = type { i8*, i8*, i32 }
%"class.draco::DracoOptions" = type { %"class.draco::Options", %"class.std::__2::map.113" }
%"class.draco::Options" = type { %"class.std::__2::map" }
%"class.std::__2::map" = type { %"class.std::__2::__tree" }
%"class.std::__2::__tree" = type { %"class.std::__2::__tree_end_node"*, %"class.std::__2::__compressed_pair.106", %"class.std::__2::__compressed_pair.111" }
%"class.std::__2::__tree_end_node" = type { %"class.std::__2::__tree_node_base"* }
%"class.std::__2::__tree_node_base" = type <{ %"class.std::__2::__tree_end_node", %"class.std::__2::__tree_node_base"*, %"class.std::__2::__tree_end_node"*, i8, [3 x i8] }>
%"class.std::__2::__compressed_pair.106" = type { %"struct.std::__2::__compressed_pair_elem.107" }
%"struct.std::__2::__compressed_pair_elem.107" = type { %"class.std::__2::__tree_end_node" }
%"class.std::__2::__compressed_pair.111" = type { %"struct.std::__2::__compressed_pair_elem.5" }
%"class.std::__2::map.113" = type { %"class.std::__2::__tree.114" }
%"class.std::__2::__tree.114" = type { %"class.std::__2::__tree_end_node"*, %"class.std::__2::__compressed_pair.115", %"class.std::__2::__compressed_pair.119" }
%"class.std::__2::__compressed_pair.115" = type { %"struct.std::__2::__compressed_pair_elem.107" }
%"class.std::__2::__compressed_pair.119" = type { %"struct.std::__2::__compressed_pair_elem.5" }
%"class.draco::Status" = type { i32, %"class.std::__2::basic_string" }
%"class.std::__2::basic_string" = type { %"class.std::__2::__compressed_pair.124" }
%"class.std::__2::__compressed_pair.124" = type { %"struct.std::__2::__compressed_pair_elem.125" }
%"struct.std::__2::__compressed_pair_elem.125" = type { %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep" }
%"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep" = type { %union.anon }
%union.anon = type { %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long" }
%"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__long" = type { i8*, i32, i32 }
%"struct.draco::DracoHeader" = type { [5 x i8], i8, i8, i8, i8, i16 }
%"struct.std::__2::__default_init_tag" = type { i8 }
%"class.std::__2::__basic_string_common" = type { i8 }
%"class.draco::MetadataDecoder" = type { %"class.draco::DecoderBuffer"* }
%"struct.std::__2::default_delete.50" = type { i8 }
%"class.std::__2::__wrap_iter" = type { %"class.std::__2::unique_ptr.96"* }
%"struct.std::__2::__compressed_pair_elem.126" = type { i8 }
%"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__raw" = type { [3 x i32] }
%"class.std::__2::allocator.127" = type { i8 }
%"struct.std::__2::__value_init_tag" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.9" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.13" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.16" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.6" = type { i8 }
%"class.std::__2::allocator" = type { i8 }
%"class.std::__2::allocator.10" = type { i8 }
%"class.std::__2::__unordered_map_hasher" = type { i8 }
%"struct.std::__2::hash" = type { i8 }
%"class.std::__2::__unordered_map_equal" = type { i8 }
%"struct.std::__2::equal_to" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.31" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.35" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.38" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.26" = type { i8 }
%"class.std::__2::allocator.27" = type { i8 }
%"class.std::__2::allocator.32" = type { i8 }
%"class.std::__2::__unordered_map_hasher.36" = type { i8 }
%"class.std::__2::__unordered_map_equal.39" = type { i8 }
%"class.std::__2::__vector_base_common" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.46" = type { i8 }
%"class.std::__2::allocator.47" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.49" = type { i8 }
%"struct.std::__2::integral_constant" = type { i8 }
%"struct.std::__2::__has_destroy" = type { i8 }
%"struct.std::__2::default_delete" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.43" = type { i8 }
%"struct.std::__2::__hash_node" = type { %"struct.std::__2::__hash_node_base.22", i32, %"struct.std::__2::__hash_value_type" }
%"struct.std::__2::__hash_value_type" = type { %"struct.std::__2::pair" }
%"struct.std::__2::pair" = type { %"class.std::__2::basic_string", %"class.std::__2::unique_ptr.129" }
%"class.std::__2::unique_ptr.129" = type { %"class.std::__2::__compressed_pair.130" }
%"class.std::__2::__compressed_pair.130" = type { %"struct.std::__2::__compressed_pair_elem.131" }
%"struct.std::__2::__compressed_pair_elem.131" = type { %"class.draco::Metadata"* }
%"struct.std::__2::integral_constant.134" = type { i8 }
%"struct.std::__2::__has_destroy.135" = type { i8 }
%"struct.std::__2::default_delete.133" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.132" = type { i8 }
%"struct.std::__2::__hash_node.136" = type { %"struct.std::__2::__hash_node_base", i32, %"struct.std::__2::__hash_value_type.137" }
%"struct.std::__2::__hash_value_type.137" = type { %"struct.std::__2::pair.138" }
%"struct.std::__2::pair.138" = type { %"class.std::__2::basic_string", %"class.draco::EntryValue" }
%"class.draco::EntryValue" = type { %"class.std::__2::vector.56" }
%"struct.std::__2::__has_destroy.139" = type { i8 }
%"class.std::__2::allocator.61" = type { i8 }
%"struct.std::__2::__has_destroy.140" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.60" = type { i8 }
%"class.std::__2::allocator.92" = type { i8 }
%"struct.std::__2::__has_destroy.141" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.91" = type { i8 }
%"class.std::__2::allocator.104" = type { i8 }
%"struct.std::__2::__has_destroy.142" = type { i8 }
%"struct.std::__2::default_delete.100" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.99" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.103" = type { i8 }
%"struct.std::__2::__split_buffer" = type { i32*, i32*, i32*, %"class.std::__2::__compressed_pair.143" }
%"class.std::__2::__compressed_pair.143" = type { %"struct.std::__2::__compressed_pair_elem.90", %"struct.std::__2::__compressed_pair_elem.144" }
%"struct.std::__2::__compressed_pair_elem.144" = type { %"class.std::__2::allocator.92"* }
%"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction" = type { %"class.std::__2::vector.87"*, i32*, i32* }
%"class.std::__2::__split_buffer_common" = type { i8 }
%"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction" = type { i32*, i32*, i32** }
%"struct.std::__2::__has_construct" = type { i8 }
%"struct.std::__2::__less" = type { i8 }
%"struct.std::__2::__has_max_size" = type { i8 }

$_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEC2Ev = comdat any

$_ZNSt3__26vectorIiNS_9allocatorIiEEEC2Ev = comdat any

$_ZN5draco13DecoderBuffer6DecodeEPvm = comdat any

$_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2IDnEEPKc = comdat any

$_ZN5draco6StatusC2ENS0_4CodeERKNSt3__212basic_stringIcNS2_11char_traitsIcEENS2_9allocatorIcEEEE = comdat any

$_ZN5draco13DecoderBuffer6DecodeIhEEbPT_ = comdat any

$_ZN5draco13DecoderBuffer6DecodeItEEbPT_ = comdat any

$_ZN5draco8OkStatusEv = comdat any

$_ZN5draco16GeometryMetadataC2Ev = comdat any

$_ZNSt3__210unique_ptrIN5draco16GeometryMetadataENS_14default_deleteIS2_EEEC2ILb1EvEEPS2_ = comdat any

$_ZNKSt3__210unique_ptrIN5draco16GeometryMetadataENS_14default_deleteIS2_EEE3getEv = comdat any

$_ZN5draco10PointCloud11AddMetadataENSt3__210unique_ptrINS_16GeometryMetadataENS1_14default_deleteIS3_EEEE = comdat any

$_ZNSt3__24moveIRNS_10unique_ptrIN5draco16GeometryMetadataENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOS9_ = comdat any

$_ZNSt3__210unique_ptrIN5draco16GeometryMetadataENS_14default_deleteIS2_EEEC2EOS5_ = comdat any

$_ZNSt3__210unique_ptrIN5draco16GeometryMetadataENS_14default_deleteIS2_EEED2Ev = comdat any

$_ZNK5draco6Status2okEv = comdat any

$_ZN5draco6StatusD2Ev = comdat any

$_ZN5draco13DecoderBuffer21set_bitstream_versionEt = comdat any

$_ZNK5draco17PointCloudDecoder17bitstream_versionEv = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5beginEv = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE3endEv = comdat any

$_ZNSt3__2neIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEEbRKNS_11__wrap_iterIT_EESC_ = comdat any

$_ZNKSt3__211__wrap_iterIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEdeEv = comdat any

$_ZNKSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEEptEv = comdat any

$_ZNSt3__211__wrap_iterIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEppEv = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEixEm = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEE4sizeEv = comdat any

$_ZNSt3__26vectorIiNS_9allocatorIiEEE6resizeEm = comdat any

$_ZNSt3__26vectorIiNS_9allocatorIiEEEixEm = comdat any

$_ZNK5draco10PointCloud14num_attributesEv = comdat any

$_ZN5draco17PointCloudDecoderD2Ev = comdat any

$_ZN5draco17PointCloudDecoderD0Ev = comdat any

$_ZNK5draco17PointCloudDecoder15GetGeometryTypeEv = comdat any

$_ZN5draco17PointCloudDecoder17InitializeDecoderEv = comdat any

$_ZN5draco17PointCloudDecoder18DecodeGeometryDataEv = comdat any

$_ZN5draco17PointCloudDecoder19OnAttributesDecodedEv = comdat any

$_ZN5draco6StatusC2ENS0_4CodeE = comdat any

$_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2Ev = comdat any

$_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_EC2INS_18__default_init_tagESA_EEOT_OT0_ = comdat any

$_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__zeroEv = comdat any

$_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIcEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__29allocatorIcEC2Ev = comdat any

$_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EE5__getEv = comdat any

$_ZN5draco8MetadataC2Ev = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEC2Ev = comdat any

$_ZNSt3__213unordered_mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueENS_4hashIS6_EENS_8equal_toIS6_EENS4_INS_4pairIKS6_S8_EEEEEC2Ev = comdat any

$_ZNSt3__213unordered_mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteIS9_EEEENS_4hashIS6_EENS_8equal_toIS6_EENS4_INS_4pairIKS6_SC_EEEEEC2Ev = comdat any

$_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEENS_22__unordered_map_hasherIS7_SA_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SA_NS_8equal_toIS7_EELb1EEENS5_ISA_EEEC2Ev = comdat any

$_ZNSt3__210unique_ptrIA_PNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISH_EEEEEC2ILb1EvEEv = comdat any

$_ZNSt3__217__compressed_pairINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS7_ISE_EEEC2ILb1EvEEv = comdat any

$_ZNSt3__217__compressed_pairImNS_22__unordered_map_hasherINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_17__hash_value_typeIS7_N5draco10EntryValueEEENS_4hashIS7_EELb1EEEEC2IiNS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__217__compressed_pairIfNS_21__unordered_map_equalINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_17__hash_value_typeIS7_N5draco10EntryValueEEENS_8equal_toIS7_EELb1EEEEC2IfNS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__217__compressed_pairIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISH_EEEEEC2ISI_NS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__27forwardIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEEEEOT_RNS_16remove_referenceISJ_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEELi0ELb0EEC2ISI_vEEOT_ = comdat any

$_ZNSt3__222__compressed_pair_elemINS_25__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS2_IcEEEEN5draco10EntryValueEEEPvEEEEEEEELi1ELb0EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEEC2Ev = comdat any

$_ZNSt3__217__compressed_pairImNS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEEC2IiNS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__27forwardIiEEOT_RNS_16remove_referenceIS1_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemImLi0ELb0EEC2IivEEOT_ = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__29allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEEN5draco10EntryValueEEEPvEEEEEC2Ev = comdat any

$_ZNSt3__222__compressed_pair_elemINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEELi0ELb0EEC2ENS_16__value_init_tagE = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEELi1ELb1EEC2ENS_16__value_init_tagE = comdat any

$_ZNSt3__216__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEC2Ev = comdat any

$_ZNSt3__29allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEEN5draco10EntryValueEEEPvEEEC2Ev = comdat any

$_ZNSt3__222__compressed_pair_elemINS_22__unordered_map_hasherINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_17__hash_value_typeIS7_N5draco10EntryValueEEENS_4hashIS7_EELb1EEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__222__unordered_map_hasherINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_17__hash_value_typeIS6_N5draco10EntryValueEEENS_4hashIS6_EELb1EEC2Ev = comdat any

$_ZNSt3__27forwardIfEEOT_RNS_16remove_referenceIS1_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIfLi0ELb0EEC2IfvEEOT_ = comdat any

$_ZNSt3__222__compressed_pair_elemINS_21__unordered_map_equalINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_17__hash_value_typeIS7_N5draco10EntryValueEEENS_8equal_toIS7_EELb1EEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__221__unordered_map_equalINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_17__hash_value_typeIS6_N5draco10EntryValueEEENS_8equal_toIS6_EELb1EEC2Ev = comdat any

$_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEEENS_22__unordered_map_hasherIS7_SE_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SE_NS_8equal_toIS7_EELb1EEENS5_ISE_EEEC2Ev = comdat any

$_ZNSt3__210unique_ptrIA_PNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS0_IN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISK_EEEEEC2ILb1EvEEv = comdat any

$_ZNSt3__217__compressed_pairINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEENS7_ISI_EEEC2ILb1EvEEv = comdat any

$_ZNSt3__217__compressed_pairImNS_22__unordered_map_hasherINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_17__hash_value_typeIS7_NS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEENS_4hashIS7_EELb1EEEEC2IiNS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__217__compressed_pairIfNS_21__unordered_map_equalINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_17__hash_value_typeIS7_NS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEENS_8equal_toIS7_EELb1EEEEC2IfNS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__217__compressed_pairIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISL_EEEEEC2ISM_NS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__27forwardIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEOT_RNS_16remove_referenceISN_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEELi0ELb0EEC2ISM_vEEOT_ = comdat any

$_ZNSt3__222__compressed_pair_elemINS_25__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS2_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISD_EEEEEEPvEEEEEEEELi1ELb0EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEEC2Ev = comdat any

$_ZNSt3__217__compressed_pairImNS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEEC2IiNS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__29allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEEEC2Ev = comdat any

$_ZNSt3__222__compressed_pair_elemINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEELi0ELb0EEC2ENS_16__value_init_tagE = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEELi1ELb1EEC2ENS_16__value_init_tagE = comdat any

$_ZNSt3__216__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEC2Ev = comdat any

$_ZNSt3__29allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEEEPvEEEC2Ev = comdat any

$_ZNSt3__222__compressed_pair_elemINS_22__unordered_map_hasherINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_17__hash_value_typeIS7_NS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEENS_4hashIS7_EELb1EEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__222__unordered_map_hasherINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_17__hash_value_typeIS6_NS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEEENS_4hashIS6_EELb1EEC2Ev = comdat any

$_ZNSt3__222__compressed_pair_elemINS_21__unordered_map_equalINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_17__hash_value_typeIS7_NS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEENS_8equal_toIS7_EELb1EEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__221__unordered_map_equalINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_17__hash_value_typeIS6_NS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEEENS_8equal_toIS6_EELb1EEC2Ev = comdat any

$_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEC2Ev = comdat any

$_ZNSt3__220__vector_base_commonILb1EEC2Ev = comdat any

$_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEC2IDnNS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEELi0ELb0EEC2IDnvEEOT_ = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__29allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEC2Ev = comdat any

$_ZNSt3__210unique_ptrIN5draco16GeometryMetadataENS_14default_deleteIS2_EEEaSEOS5_ = comdat any

$_ZNSt3__210unique_ptrIN5draco16GeometryMetadataENS_14default_deleteIS2_EEE5resetEPS2_ = comdat any

$_ZNSt3__210unique_ptrIN5draco16GeometryMetadataENS_14default_deleteIS2_EEE7releaseEv = comdat any

$_ZNSt3__27forwardINS_14default_deleteIN5draco16GeometryMetadataEEEEEOT_RNS_16remove_referenceIS5_E4typeE = comdat any

$_ZNSt3__210unique_ptrIN5draco16GeometryMetadataENS_14default_deleteIS2_EEE11get_deleterEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco16GeometryMetadataENS_14default_deleteIS2_EEE6secondEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco16GeometryMetadataENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNKSt3__214default_deleteIN5draco16GeometryMetadataEEclEPS2_ = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco16GeometryMetadataELi0ELb0EE5__getEv = comdat any

$_ZN5draco16GeometryMetadataD2Ev = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEED2Ev = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__annotate_deleteEv = comdat any

$_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEED2Ev = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE31__annotate_contiguous_containerEPKvSB_SB_SB_ = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv = comdat any

$_ZNSt3__212__to_addressINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEEPT_S8_ = comdat any

$_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5clearEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEEE10deallocateERS8_PS7_m = comdat any

$_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv = comdat any

$_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__destruct_at_endEPS6_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEEE7destroyIS7_EEvRS8_PT_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEEE9__destroyIS7_EEvNS_17integral_constantIbLb1EEERS8_PT_ = comdat any

$_ZNSt3__29allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEE7destroyEPS6_ = comdat any

$_ZNSt3__210unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS2_EEED2Ev = comdat any

$_ZNSt3__210unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS2_EEE5resetEPS2_ = comdat any

$_ZNSt3__217__compressed_pairIPN5draco17AttributeMetadataENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco17AttributeMetadataENS_14default_deleteIS2_EEE6secondEv = comdat any

$_ZNKSt3__214default_deleteIN5draco17AttributeMetadataEEclEPS2_ = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco17AttributeMetadataELi0ELb0EE5__getEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco17AttributeMetadataEEELi1ELb1EE5__getEv = comdat any

$_ZN5draco17AttributeMetadataD2Ev = comdat any

$_ZNSt3__29allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEE10deallocateEPS6_m = comdat any

$_ZNSt3__219__libcpp_deallocateEPvmm = comdat any

$_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm = comdat any

$_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm = comdat any

$_ZNSt3__217_DeallocateCaller9__do_callEPv = comdat any

$_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__213unordered_mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteIS9_EEEENS_4hashIS6_EENS_8equal_toIS6_EENS4_INS_4pairIKS6_SC_EEEEED2Ev = comdat any

$_ZNSt3__213unordered_mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueENS_4hashIS6_EENS_8equal_toIS6_EENS4_INS_4pairIKS6_S8_EEEEED2Ev = comdat any

$_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEEENS_22__unordered_map_hasherIS7_SE_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SE_NS_8equal_toIS7_EELb1EEENS5_ISE_EEED2Ev = comdat any

$_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEEENS_22__unordered_map_hasherIS7_SE_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SE_NS_8equal_toIS7_EELb1EEENS5_ISE_EEE17__deallocate_nodeEPNS_16__hash_node_baseIPNS_11__hash_nodeISE_PvEEEE = comdat any

$_ZNSt3__217__compressed_pairINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEENS7_ISI_EEE5firstEv = comdat any

$_ZNSt3__210unique_ptrIA_PNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS0_IN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISK_EEEEED2Ev = comdat any

$_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEEENS_22__unordered_map_hasherIS7_SE_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SE_NS_8equal_toIS7_EELb1EEENS5_ISE_EEE12__node_allocEv = comdat any

$_ZNSt3__216__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEE8__upcastEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEEE7destroyINS_4pairIKS8_SE_EEEEvRSI_PT_ = comdat any

$_ZNSt3__222__hash_key_value_typesINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEEEE9__get_ptrERSE_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEEE10deallocateERSI_PSH_m = comdat any

$_ZNSt3__217__compressed_pairINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEENS7_ISI_EEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__214pointer_traitsIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEE10pointer_toERSK_ = comdat any

$_ZNSt3__29addressofINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEPT_RSL_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEEE9__destroyINS_4pairIKS8_SE_EEEEvNS_17integral_constantIbLb0EEERSI_PT_ = comdat any

$_ZNSt3__24pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEED2Ev = comdat any

$_ZNSt3__210unique_ptrIN5draco8MetadataENS_14default_deleteIS2_EEED2Ev = comdat any

$_ZNSt3__210unique_ptrIN5draco8MetadataENS_14default_deleteIS2_EEE5resetEPS2_ = comdat any

$_ZNSt3__217__compressed_pairIPN5draco8MetadataENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco8MetadataENS_14default_deleteIS2_EEE6secondEv = comdat any

$_ZNKSt3__214default_deleteIN5draco8MetadataEEclEPS2_ = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco8MetadataELi0ELb0EE5__getEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco8MetadataEEELi1ELb1EE5__getEv = comdat any

$_ZN5draco8MetadataD2Ev = comdat any

$_ZNSt3__29addressofINS_4pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEEEPT_RSG_ = comdat any

$_ZNSt3__217__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteIS9_EEEEE11__get_valueEv = comdat any

$_ZNSt3__29allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEEEPvEEE10deallocateEPSG_m = comdat any

$_ZNSt3__222__compressed_pair_elemINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__210unique_ptrIA_PNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS0_IN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISK_EEEEE5resetEDn = comdat any

$_ZNSt3__217__compressed_pairIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISL_EEEEE5firstEv = comdat any

$_ZNSt3__217__compressed_pairIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISL_EEEEE6secondEv = comdat any

$_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEEclEPSL_ = comdat any

$_ZNSt3__222__compressed_pair_elemIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_25__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS2_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISD_EEEEEEPvEEEEEEEELi1ELb0EE5__getEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEE10deallocateERSM_PSL_m = comdat any

$_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEE7__allocEv = comdat any

$_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEE4sizeEv = comdat any

$_ZNSt3__29allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEEE10deallocateEPSK_m = comdat any

$_ZNSt3__217__compressed_pairImNS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairImNS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEE5firstEv = comdat any

$_ZNSt3__222__compressed_pair_elemImLi0ELb0EE5__getEv = comdat any

$_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEENS_22__unordered_map_hasherIS7_SA_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SA_NS_8equal_toIS7_EELb1EEENS5_ISA_EEED2Ev = comdat any

$_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEENS_22__unordered_map_hasherIS7_SA_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SA_NS_8equal_toIS7_EELb1EEENS5_ISA_EEE17__deallocate_nodeEPNS_16__hash_node_baseIPNS_11__hash_nodeISA_PvEEEE = comdat any

$_ZNSt3__217__compressed_pairINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS7_ISE_EEE5firstEv = comdat any

$_ZNSt3__210unique_ptrIA_PNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISH_EEEEED2Ev = comdat any

$_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEENS_22__unordered_map_hasherIS7_SA_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SA_NS_8equal_toIS7_EELb1EEENS5_ISA_EEE12__node_allocEv = comdat any

$_ZNSt3__216__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEE8__upcastEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEE7destroyINS_4pairIKS8_SA_EEEEvRSE_PT_ = comdat any

$_ZNSt3__222__hash_key_value_typesINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEE9__get_ptrERSA_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEE10deallocateERSE_PSD_m = comdat any

$_ZNSt3__217__compressed_pairINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS7_ISE_EEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__214pointer_traitsIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEEE10pointer_toERSG_ = comdat any

$_ZNSt3__29addressofINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEEEEPT_RSH_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEE9__destroyINS_4pairIKS8_SA_EEEEvNS_17integral_constantIbLb0EEERSE_PT_ = comdat any

$_ZNSt3__24pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEED2Ev = comdat any

$_ZN5draco10EntryValueD2Ev = comdat any

$_ZNSt3__26vectorIhNS_9allocatorIhEEED2Ev = comdat any

$_ZNKSt3__26vectorIhNS_9allocatorIhEEE17__annotate_deleteEv = comdat any

$_ZNSt3__213__vector_baseIhNS_9allocatorIhEEED2Ev = comdat any

$_ZNKSt3__26vectorIhNS_9allocatorIhEEE31__annotate_contiguous_containerEPKvS5_S5_S5_ = comdat any

$_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv = comdat any

$_ZNKSt3__26vectorIhNS_9allocatorIhEEE8capacityEv = comdat any

$_ZNKSt3__26vectorIhNS_9allocatorIhEEE4sizeEv = comdat any

$_ZNSt3__212__to_addressIhEEPT_S2_ = comdat any

$_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPhNS_9allocatorIhEEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPhLi0ELb0EE5__getEv = comdat any

$_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE5clearEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIhEEE10deallocateERS2_Phm = comdat any

$_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE7__allocEv = comdat any

$_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE17__destruct_at_endEPh = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIhEEE7destroyIhEEvRS2_PT_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIhEEE9__destroyIhEEvNS_17integral_constantIbLb1EEERS2_PT_ = comdat any

$_ZNSt3__29allocatorIhE7destroyEPh = comdat any

$_ZNSt3__29allocatorIhE10deallocateEPhm = comdat any

$_ZNSt3__217__compressed_pairIPhNS_9allocatorIhEEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIhEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__29addressofINS_4pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEEEPT_RSC_ = comdat any

$_ZNSt3__217__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEE11__get_valueEv = comdat any

$_ZNSt3__29allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEEN5draco10EntryValueEEEPvEEE10deallocateEPSC_m = comdat any

$_ZNSt3__222__compressed_pair_elemINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__210unique_ptrIA_PNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISH_EEEEE5resetEDn = comdat any

$_ZNSt3__217__compressed_pairIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISH_EEEEE5firstEv = comdat any

$_ZNSt3__217__compressed_pairIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISH_EEEEE6secondEv = comdat any

$_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEEclEPSH_ = comdat any

$_ZNSt3__222__compressed_pair_elemIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_25__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS2_IcEEEEN5draco10EntryValueEEEPvEEEEEEEELi1ELb0EE5__getEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEE10deallocateERSI_PSH_m = comdat any

$_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEE7__allocEv = comdat any

$_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEE4sizeEv = comdat any

$_ZNSt3__29allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEEN5draco10EntryValueEEEPvEEEEE10deallocateEPSG_m = comdat any

$_ZNSt3__217__compressed_pairImNS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairImNS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEE5firstEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco16GeometryMetadataEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__2eqIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEES7_EEbRKNS_11__wrap_iterIT_EERKNS8_IT0_EE = comdat any

$_ZNKSt3__211__wrap_iterIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEE4baseEv = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv = comdat any

$_ZNSt3__26vectorIiNS_9allocatorIiEEED2Ev = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEED2Ev = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEE17__annotate_deleteEv = comdat any

$_ZNSt3__213__vector_baseIiNS_9allocatorIiEEED2Ev = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEE31__annotate_contiguous_containerEPKvS5_S5_S5_ = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEE8capacityEv = comdat any

$_ZNSt3__212__to_addressIiEEPT_S2_ = comdat any

$_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPiNS_9allocatorIiEEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPiLi0ELb0EE5__getEv = comdat any

$_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE5clearEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIiEEE10deallocateERS2_Pim = comdat any

$_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE7__allocEv = comdat any

$_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE17__destruct_at_endEPi = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIiEEE7destroyIiEEvRS2_PT_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIiEEE9__destroyIiEEvNS_17integral_constantIbLb1EEERS2_PT_ = comdat any

$_ZNSt3__29allocatorIiE7destroyEPi = comdat any

$_ZNSt3__29allocatorIiE10deallocateEPim = comdat any

$_ZNSt3__217__compressed_pairIPiNS_9allocatorIiEEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIiEELi1ELb1EE5__getEv = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__annotate_deleteEv = comdat any

$_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEED2Ev = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE31__annotate_contiguous_containerEPKvSB_SB_SB_ = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv = comdat any

$_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv = comdat any

$_ZNSt3__212__to_addressINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEEPT_S8_ = comdat any

$_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEELi0ELb0EE5__getEv = comdat any

$_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5clearEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE10deallocateERS8_PS7_m = comdat any

$_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv = comdat any

$_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__destruct_at_endEPS6_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE7destroyIS7_EEvRS8_PT_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE9__destroyIS7_EEvNS_17integral_constantIbLb1EEERS8_PT_ = comdat any

$_ZNSt3__29allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEE7destroyEPS6_ = comdat any

$_ZNSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEED2Ev = comdat any

$_ZNSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE5resetEPS2_ = comdat any

$_ZNSt3__217__compressed_pairIPN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE6secondEv = comdat any

$_ZNKSt3__214default_deleteIN5draco26AttributesDecoderInterfaceEEclEPS2_ = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco26AttributesDecoderInterfaceELi0ELb0EE5__getEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco26AttributesDecoderInterfaceEEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__29allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEE10deallocateEPS6_m = comdat any

$_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEELi1ELb1EE5__getEv = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco16GeometryMetadataENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPN5draco16GeometryMetadataELi0ELb0EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPN5draco16GeometryMetadataENS_14default_deleteIS2_EEEC2IRS3_NS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__27forwardIRPN5draco16GeometryMetadataEEEOT_RNS_16remove_referenceIS5_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco16GeometryMetadataELi0ELb0EEC2IRS3_vEEOT_ = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco16GeometryMetadataEEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEC2Ev = comdat any

$_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEC2IDnNS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEELi0ELb0EEC2IDnvEEOT_ = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__29allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEC2Ev = comdat any

$_ZNSt3__213__vector_baseIiNS_9allocatorIiEEEC2Ev = comdat any

$_ZNSt3__217__compressed_pairIPiNS_9allocatorIiEEEC2IDnNS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__222__compressed_pair_elemIPiLi0ELb0EEC2IDnvEEOT_ = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIiEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__29allocatorIiEC2Ev = comdat any

$_ZNSt3__211char_traitsIcE6lengthEPKc = comdat any

$_ZN5draco13DecoderBuffer4PeekIhEEbPT_ = comdat any

$_ZN5draco13DecoderBuffer4PeekItEEbPT_ = comdat any

$_ZNSt3__217__compressed_pairIPN5draco16GeometryMetadataENS_14default_deleteIS2_EEEC2IS3_S5_EEOT_OT0_ = comdat any

$_ZNSt3__27forwardIPN5draco16GeometryMetadataEEEOT_RNS_16remove_referenceIS4_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPN5draco16GeometryMetadataELi0ELb0EEC2IS3_vEEOT_ = comdat any

$_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco16GeometryMetadataEEELi1ELb1EEC2IS4_vEEOT_ = comdat any

$_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE11__make_iterEPS6_ = comdat any

$_ZNSt3__211__wrap_iterIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEC2ES7_ = comdat any

$_ZNKSt3__217__compressed_pairIPN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPN5draco26AttributesDecoderInterfaceELi0ELb0EE5__getEv = comdat any

$_ZNSt3__26vectorIiNS_9allocatorIiEEE8__appendEm = comdat any

$_ZNSt3__26vectorIiNS_9allocatorIiEEE17__destruct_at_endEPi = comdat any

$_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE9__end_capEv = comdat any

$_ZNSt3__26vectorIiNS_9allocatorIiEEE18__construct_at_endEm = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEE11__recommendEm = comdat any

$_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEEC2EmmS3_ = comdat any

$_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE18__construct_at_endEm = comdat any

$_ZNSt3__26vectorIiNS_9allocatorIiEEE26__swap_out_circular_bufferERNS_14__split_bufferIiRS2_EE = comdat any

$_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEED2Ev = comdat any

$_ZNSt3__217__compressed_pairIPiNS_9allocatorIiEEE5firstEv = comdat any

$_ZNSt3__222__compressed_pair_elemIPiLi0ELb0EE5__getEv = comdat any

$_ZNSt3__26vectorIiNS_9allocatorIiEEE21_ConstructTransactionC2ERS3_m = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIiEEE9constructIiJEEEvRS2_PT_DpOT0_ = comdat any

$_ZNSt3__26vectorIiNS_9allocatorIiEEE21_ConstructTransactionD2Ev = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIiEEE11__constructIiJEEEvNS_17integral_constantIbLb1EEERS2_PT_DpOT0_ = comdat any

$_ZNSt3__29allocatorIiE9constructIiJEEEvPT_DpOT0_ = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEE8max_sizeEv = comdat any

$_ZNSt3__23maxImEERKT_S3_S3_ = comdat any

$_ZNSt3__23minImEERKT_S3_S3_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIiEEE8max_sizeERKS2_ = comdat any

$_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE7__allocEv = comdat any

$_ZNSt3__214numeric_limitsIlE3maxEv = comdat any

$_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_ = comdat any

$_ZNKSt3__26__lessImmEclERKmS3_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIiEEE10__max_sizeENS_17integral_constantIbLb1EEERKS2_ = comdat any

$_ZNKSt3__29allocatorIiE8max_sizeEv = comdat any

$_ZNKSt3__217__compressed_pairIPiNS_9allocatorIiEEE6secondEv = comdat any

$_ZNKSt3__222__compressed_pair_elemINS_9allocatorIiEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv = comdat any

$_ZNSt3__23maxImNS_6__lessImmEEEERKT_S5_S5_T0_ = comdat any

$_ZNSt3__217__compressed_pairIPiRNS_9allocatorIiEEEC2IDnS4_EEOT_OT0_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIiEEE8allocateERS2_m = comdat any

$_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE7__allocEv = comdat any

$_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE9__end_capEv = comdat any

$_ZNSt3__27forwardIRNS_9allocatorIiEEEEOT_RNS_16remove_referenceIS4_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIiEELi1ELb0EEC2IS3_vEEOT_ = comdat any

$_ZNSt3__29allocatorIiE8allocateEmPKv = comdat any

$_ZNSt3__220__throw_length_errorEPKc = comdat any

$_ZNSt3__217__libcpp_allocateEmm = comdat any

$_ZNSt3__217__compressed_pairIPiRNS_9allocatorIiEEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIiEELi1ELb0EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPiRNS_9allocatorIiEEE5firstEv = comdat any

$_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE21_ConstructTransactionC2EPPim = comdat any

$_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE21_ConstructTransactionD2Ev = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIiEEE46__construct_backward_with_exception_guaranteesIiEENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS2_PT_S8_EE5valuesr31is_trivially_move_constructibleIS8_EE5valueEvE4typeERS2_S9_S9_RS9_ = comdat any

$_ZNSt3__24swapIPiEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_ = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEE14__annotate_newEm = comdat any

$_ZNSt3__26vectorIiNS_9allocatorIiEEE26__invalidate_all_iteratorsEv = comdat any

$_ZNSt3__24moveIRPiEEONS_16remove_referenceIT_E4typeEOS4_ = comdat any

$_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE5clearEv = comdat any

$_ZNKSt3__214__split_bufferIiRNS_9allocatorIiEEE8capacityEv = comdat any

$_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE17__destruct_at_endEPi = comdat any

$_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE17__destruct_at_endEPiNS_17integral_constantIbLb0EEE = comdat any

$_ZNKSt3__214__split_bufferIiRNS_9allocatorIiEEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPiRNS_9allocatorIiEEE5firstEv = comdat any

$_ZNSt3__26vectorIiNS_9allocatorIiEEE27__invalidate_iterators_pastEPi = comdat any

$_ZNKSt3__26vectorIiNS_9allocatorIiEEE17__annotate_shrinkEm = comdat any

@_ZTVN5draco17PointCloudDecoderE = hidden unnamed_addr constant { [11 x i8*] } { [11 x i8*] [i8* null, i8* null, i8* bitcast (%"class.draco::PointCloudDecoder"* (%"class.draco::PointCloudDecoder"*)* @_ZN5draco17PointCloudDecoderD2Ev to i8*), i8* bitcast (void (%"class.draco::PointCloudDecoder"*)* @_ZN5draco17PointCloudDecoderD0Ev to i8*), i8* bitcast (i32 (%"class.draco::PointCloudDecoder"*)* @_ZNK5draco17PointCloudDecoder15GetGeometryTypeEv to i8*), i8* bitcast (i1 (%"class.draco::PointCloudDecoder"*)* @_ZN5draco17PointCloudDecoder17InitializeDecoderEv to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (i1 (%"class.draco::PointCloudDecoder"*)* @_ZN5draco17PointCloudDecoder18DecodeGeometryDataEv to i8*), i8* bitcast (i1 (%"class.draco::PointCloudDecoder"*)* @_ZN5draco17PointCloudDecoder21DecodePointAttributesEv to i8*), i8* bitcast (i1 (%"class.draco::PointCloudDecoder"*)* @_ZN5draco17PointCloudDecoder19DecodeAllAttributesEv to i8*), i8* bitcast (i1 (%"class.draco::PointCloudDecoder"*)* @_ZN5draco17PointCloudDecoder19OnAttributesDecodedEv to i8*)] }, align 4
@__const._ZN5draco17PointCloudDecoder12DecodeHeaderEPNS_13DecoderBufferEPNS_11DracoHeaderE.kIoErrorMsg = private unnamed_addr constant [30 x i8] c"Failed to parse Draco header.\00", align 16
@.str = private unnamed_addr constant [6 x i8] c"DRACO\00", align 1
@.str.1 = private unnamed_addr constant [18 x i8] c"Not a Draco file.\00", align 1
@.str.2 = private unnamed_addr constant [27 x i8] c"Failed to decode metadata.\00", align 1
@.str.3 = private unnamed_addr constant [51 x i8] c"Using incompatible decoder for the input geometry.\00", align 1
@.str.4 = private unnamed_addr constant [23 x i8] c"Unknown major version.\00", align 1
@.str.5 = private unnamed_addr constant [23 x i8] c"Unknown minor version.\00", align 1
@.str.6 = private unnamed_addr constant [34 x i8] c"Failed to initialize the decoder.\00", align 1
@.str.7 = private unnamed_addr constant [32 x i8] c"Failed to decode geometry data.\00", align 1
@.str.8 = private unnamed_addr constant [35 x i8] c"Failed to decode point attributes.\00", align 1
@.str.9 = private unnamed_addr constant [68 x i8] c"allocator<T>::allocate(size_t n) 'n' exceeds maximum supported size\00", align 1

; Function Attrs: noinline nounwind optnone
define hidden %"class.draco::PointCloudDecoder"* @_ZN5draco17PointCloudDecoderC2Ev(%"class.draco::PointCloudDecoder"* returned %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %"class.draco::PointCloudDecoder"*, align 4
  store %"class.draco::PointCloudDecoder"* %this, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::PointCloudDecoder"*, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  %0 = bitcast %"class.draco::PointCloudDecoder"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [11 x i8*] }, { [11 x i8*] }* @_ZTVN5draco17PointCloudDecoderE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %point_cloud_ = getelementptr inbounds %"class.draco::PointCloudDecoder", %"class.draco::PointCloudDecoder"* %this1, i32 0, i32 1
  store %"class.draco::PointCloud"* null, %"class.draco::PointCloud"** %point_cloud_, align 4
  %attributes_decoders_ = getelementptr inbounds %"class.draco::PointCloudDecoder", %"class.draco::PointCloudDecoder"* %this1, i32 0, i32 2
  %call = call %"class.std::__2::vector.94"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEC2Ev(%"class.std::__2::vector.94"* %attributes_decoders_) #9
  %attribute_to_decoder_map_ = getelementptr inbounds %"class.draco::PointCloudDecoder", %"class.draco::PointCloudDecoder"* %this1, i32 0, i32 3
  %call2 = call %"class.std::__2::vector.87"* @_ZNSt3__26vectorIiNS_9allocatorIiEEEC2Ev(%"class.std::__2::vector.87"* %attribute_to_decoder_map_) #9
  %buffer_ = getelementptr inbounds %"class.draco::PointCloudDecoder", %"class.draco::PointCloudDecoder"* %this1, i32 0, i32 4
  store %"class.draco::DecoderBuffer"* null, %"class.draco::DecoderBuffer"** %buffer_, align 4
  %version_major_ = getelementptr inbounds %"class.draco::PointCloudDecoder", %"class.draco::PointCloudDecoder"* %this1, i32 0, i32 5
  store i8 0, i8* %version_major_, align 4
  %version_minor_ = getelementptr inbounds %"class.draco::PointCloudDecoder", %"class.draco::PointCloudDecoder"* %this1, i32 0, i32 6
  store i8 0, i8* %version_minor_, align 1
  %options_ = getelementptr inbounds %"class.draco::PointCloudDecoder", %"class.draco::PointCloudDecoder"* %this1, i32 0, i32 7
  store %"class.draco::DracoOptions"* null, %"class.draco::DracoOptions"** %options_, align 4
  ret %"class.draco::PointCloudDecoder"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector.94"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEC2Ev(%"class.std::__2::vector.94"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %call = call %"class.std::__2::__vector_base.95"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEC2Ev(%"class.std::__2::__vector_base.95"* %0) #9
  ret %"class.std::__2::vector.94"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector.87"* @_ZNSt3__26vectorIiNS_9allocatorIiEEEC2Ev(%"class.std::__2::vector.87"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.87"* %this1 to %"class.std::__2::__vector_base.88"*
  %call = call %"class.std::__2::__vector_base.88"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEEC2Ev(%"class.std::__2::__vector_base.88"* %0) #9
  ret %"class.std::__2::vector.87"* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN5draco17PointCloudDecoder12DecodeHeaderEPNS_13DecoderBufferEPNS_11DracoHeaderE(%"class.draco::Status"* noalias sret align 4 %agg.result, %"class.draco::DecoderBuffer"* %buffer, %"struct.draco::DracoHeader"* %out_header) #0 {
entry:
  %result.ptr = alloca i8*, align 4
  %buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %out_header.addr = alloca %"struct.draco::DracoHeader"*, align 4
  %kIoErrorMsg = alloca [30 x i8], align 16
  %ref.tmp = alloca %"class.std::__2::basic_string", align 4
  %ref.tmp9 = alloca %"class.std::__2::basic_string", align 4
  %ref.tmp16 = alloca %"class.std::__2::basic_string", align 4
  %ref.tmp24 = alloca %"class.std::__2::basic_string", align 4
  %ref.tmp32 = alloca %"class.std::__2::basic_string", align 4
  %ref.tmp40 = alloca %"class.std::__2::basic_string", align 4
  %ref.tmp48 = alloca %"class.std::__2::basic_string", align 4
  %0 = bitcast %"class.draco::Status"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %"class.draco::DecoderBuffer"* %buffer, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  store %"struct.draco::DracoHeader"* %out_header, %"struct.draco::DracoHeader"** %out_header.addr, align 4
  %1 = bitcast [30 x i8]* %kIoErrorMsg to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %1, i8* align 16 getelementptr inbounds ([30 x i8], [30 x i8]* @__const._ZN5draco17PointCloudDecoder12DecodeHeaderEPNS_13DecoderBufferEPNS_11DracoHeaderE.kIoErrorMsg, i32 0, i32 0), i32 30, i1 false)
  %2 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %3 = load %"struct.draco::DracoHeader"*, %"struct.draco::DracoHeader"** %out_header.addr, align 4
  %draco_string = getelementptr inbounds %"struct.draco::DracoHeader", %"struct.draco::DracoHeader"* %3, i32 0, i32 0
  %arraydecay = getelementptr inbounds [5 x i8], [5 x i8]* %draco_string, i32 0, i32 0
  %call = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeEPvm(%"class.draco::DecoderBuffer"* %2, i8* %arraydecay, i32 5)
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  %arraydecay1 = getelementptr inbounds [30 x i8], [30 x i8]* %kIoErrorMsg, i32 0, i32 0
  %call2 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2IDnEEPKc(%"class.std::__2::basic_string"* %ref.tmp, i8* %arraydecay1)
  %call3 = call %"class.draco::Status"* @_ZN5draco6StatusC2ENS0_4CodeERKNSt3__212basic_stringIcNS2_11char_traitsIcEENS2_9allocatorIcEEEE(%"class.draco::Status"* %agg.result, i32 -2, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %ref.tmp)
  %call4 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_string"* %ref.tmp) #9
  br label %return

if.end:                                           ; preds = %entry
  %4 = load %"struct.draco::DracoHeader"*, %"struct.draco::DracoHeader"** %out_header.addr, align 4
  %draco_string5 = getelementptr inbounds %"struct.draco::DracoHeader", %"struct.draco::DracoHeader"* %4, i32 0, i32 0
  %arraydecay6 = getelementptr inbounds [5 x i8], [5 x i8]* %draco_string5, i32 0, i32 0
  %call7 = call i32 @memcmp(i8* %arraydecay6, i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str, i32 0, i32 0), i32 5)
  %cmp = icmp ne i32 %call7, 0
  br i1 %cmp, label %if.then8, label %if.end13

if.then8:                                         ; preds = %if.end
  %call10 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2IDnEEPKc(%"class.std::__2::basic_string"* %ref.tmp9, i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.str.1, i32 0, i32 0))
  %call11 = call %"class.draco::Status"* @_ZN5draco6StatusC2ENS0_4CodeERKNSt3__212basic_stringIcNS2_11char_traitsIcEENS2_9allocatorIcEEEE(%"class.draco::Status"* %agg.result, i32 -1, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %ref.tmp9)
  %call12 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_string"* %ref.tmp9) #9
  br label %return

if.end13:                                         ; preds = %if.end
  %5 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %6 = load %"struct.draco::DracoHeader"*, %"struct.draco::DracoHeader"** %out_header.addr, align 4
  %version_major = getelementptr inbounds %"struct.draco::DracoHeader", %"struct.draco::DracoHeader"* %6, i32 0, i32 1
  %call14 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIhEEbPT_(%"class.draco::DecoderBuffer"* %5, i8* %version_major)
  br i1 %call14, label %if.end21, label %if.then15

if.then15:                                        ; preds = %if.end13
  %arraydecay17 = getelementptr inbounds [30 x i8], [30 x i8]* %kIoErrorMsg, i32 0, i32 0
  %call18 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2IDnEEPKc(%"class.std::__2::basic_string"* %ref.tmp16, i8* %arraydecay17)
  %call19 = call %"class.draco::Status"* @_ZN5draco6StatusC2ENS0_4CodeERKNSt3__212basic_stringIcNS2_11char_traitsIcEENS2_9allocatorIcEEEE(%"class.draco::Status"* %agg.result, i32 -2, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %ref.tmp16)
  %call20 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_string"* %ref.tmp16) #9
  br label %return

if.end21:                                         ; preds = %if.end13
  %7 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %8 = load %"struct.draco::DracoHeader"*, %"struct.draco::DracoHeader"** %out_header.addr, align 4
  %version_minor = getelementptr inbounds %"struct.draco::DracoHeader", %"struct.draco::DracoHeader"* %8, i32 0, i32 2
  %call22 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIhEEbPT_(%"class.draco::DecoderBuffer"* %7, i8* %version_minor)
  br i1 %call22, label %if.end29, label %if.then23

if.then23:                                        ; preds = %if.end21
  %arraydecay25 = getelementptr inbounds [30 x i8], [30 x i8]* %kIoErrorMsg, i32 0, i32 0
  %call26 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2IDnEEPKc(%"class.std::__2::basic_string"* %ref.tmp24, i8* %arraydecay25)
  %call27 = call %"class.draco::Status"* @_ZN5draco6StatusC2ENS0_4CodeERKNSt3__212basic_stringIcNS2_11char_traitsIcEENS2_9allocatorIcEEEE(%"class.draco::Status"* %agg.result, i32 -2, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %ref.tmp24)
  %call28 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_string"* %ref.tmp24) #9
  br label %return

if.end29:                                         ; preds = %if.end21
  %9 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %10 = load %"struct.draco::DracoHeader"*, %"struct.draco::DracoHeader"** %out_header.addr, align 4
  %encoder_type = getelementptr inbounds %"struct.draco::DracoHeader", %"struct.draco::DracoHeader"* %10, i32 0, i32 3
  %call30 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIhEEbPT_(%"class.draco::DecoderBuffer"* %9, i8* %encoder_type)
  br i1 %call30, label %if.end37, label %if.then31

if.then31:                                        ; preds = %if.end29
  %arraydecay33 = getelementptr inbounds [30 x i8], [30 x i8]* %kIoErrorMsg, i32 0, i32 0
  %call34 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2IDnEEPKc(%"class.std::__2::basic_string"* %ref.tmp32, i8* %arraydecay33)
  %call35 = call %"class.draco::Status"* @_ZN5draco6StatusC2ENS0_4CodeERKNSt3__212basic_stringIcNS2_11char_traitsIcEENS2_9allocatorIcEEEE(%"class.draco::Status"* %agg.result, i32 -2, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %ref.tmp32)
  %call36 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_string"* %ref.tmp32) #9
  br label %return

if.end37:                                         ; preds = %if.end29
  %11 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %12 = load %"struct.draco::DracoHeader"*, %"struct.draco::DracoHeader"** %out_header.addr, align 4
  %encoder_method = getelementptr inbounds %"struct.draco::DracoHeader", %"struct.draco::DracoHeader"* %12, i32 0, i32 4
  %call38 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIhEEbPT_(%"class.draco::DecoderBuffer"* %11, i8* %encoder_method)
  br i1 %call38, label %if.end45, label %if.then39

if.then39:                                        ; preds = %if.end37
  %arraydecay41 = getelementptr inbounds [30 x i8], [30 x i8]* %kIoErrorMsg, i32 0, i32 0
  %call42 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2IDnEEPKc(%"class.std::__2::basic_string"* %ref.tmp40, i8* %arraydecay41)
  %call43 = call %"class.draco::Status"* @_ZN5draco6StatusC2ENS0_4CodeERKNSt3__212basic_stringIcNS2_11char_traitsIcEENS2_9allocatorIcEEEE(%"class.draco::Status"* %agg.result, i32 -2, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %ref.tmp40)
  %call44 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_string"* %ref.tmp40) #9
  br label %return

if.end45:                                         ; preds = %if.end37
  %13 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer.addr, align 4
  %14 = load %"struct.draco::DracoHeader"*, %"struct.draco::DracoHeader"** %out_header.addr, align 4
  %flags = getelementptr inbounds %"struct.draco::DracoHeader", %"struct.draco::DracoHeader"* %14, i32 0, i32 5
  %call46 = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeItEEbPT_(%"class.draco::DecoderBuffer"* %13, i16* %flags)
  br i1 %call46, label %if.end53, label %if.then47

if.then47:                                        ; preds = %if.end45
  %arraydecay49 = getelementptr inbounds [30 x i8], [30 x i8]* %kIoErrorMsg, i32 0, i32 0
  %call50 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2IDnEEPKc(%"class.std::__2::basic_string"* %ref.tmp48, i8* %arraydecay49)
  %call51 = call %"class.draco::Status"* @_ZN5draco6StatusC2ENS0_4CodeERKNSt3__212basic_stringIcNS2_11char_traitsIcEENS2_9allocatorIcEEEE(%"class.draco::Status"* %agg.result, i32 -2, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %ref.tmp48)
  %call52 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_string"* %ref.tmp48) #9
  br label %return

if.end53:                                         ; preds = %if.end45
  call void @_ZN5draco8OkStatusEv(%"class.draco::Status"* sret align 4 %agg.result)
  br label %return

return:                                           ; preds = %if.end53, %if.then47, %if.then39, %if.then31, %if.then23, %if.then15, %if.then8, %if.then
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco13DecoderBuffer6DecodeEPvm(%"class.draco::DecoderBuffer"* %this, i8* %out_data, i32 %size_to_decode) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %out_data.addr = alloca i8*, align 4
  %size_to_decode.addr = alloca i32, align 4
  store %"class.draco::DecoderBuffer"* %this, %"class.draco::DecoderBuffer"** %this.addr, align 4
  store i8* %out_data, i8** %out_data.addr, align 4
  store i32 %size_to_decode, i32* %size_to_decode.addr, align 4
  %this1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %this.addr, align 4
  %data_size_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 1
  %0 = load i64, i64* %data_size_, align 8
  %pos_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %1 = load i64, i64* %pos_, align 8
  %2 = load i32, i32* %size_to_decode.addr, align 4
  %conv = zext i32 %2 to i64
  %add = add nsw i64 %1, %conv
  %cmp = icmp slt i64 %0, %add
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %3 = load i8*, i8** %out_data.addr, align 4
  %data_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 0
  %4 = load i8*, i8** %data_, align 8
  %pos_2 = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %5 = load i64, i64* %pos_2, align 8
  %idx.ext = trunc i64 %5 to i32
  %add.ptr = getelementptr inbounds i8, i8* %4, i32 %idx.ext
  %6 = load i32, i32* %size_to_decode.addr, align 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %3, i8* align 1 %add.ptr, i32 %6, i1 false)
  %7 = load i32, i32* %size_to_decode.addr, align 4
  %conv3 = zext i32 %7 to i64
  %pos_4 = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %8 = load i64, i64* %pos_4, align 8
  %add5 = add nsw i64 %8, %conv3
  store i64 %add5, i64* %pos_4, align 8
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %9 = load i1, i1* %retval, align 1
  ret i1 %9
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2IDnEEPKc(%"class.std::__2::basic_string"* returned %this, i8* %__s) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  %__s.addr = alloca i8*, align 4
  %ref.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  %ref.tmp2 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  store i8* %__s, i8** %__s.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::basic_string"* %this1 to %"class.std::__2::__basic_string_common"*
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::__compressed_pair.124"* @_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_EC2INS_18__default_init_tagESA_EEOT_OT0_(%"class.std::__2::__compressed_pair.124"* %__r_, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  %1 = load i8*, i8** %__s.addr, align 4
  %2 = load i8*, i8** %__s.addr, align 4
  %call3 = call i32 @_ZNSt3__211char_traitsIcE6lengthEPKc(i8* %2) #9
  call void @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEPKcm(%"class.std::__2::basic_string"* %this1, i8* %1, i32 %call3)
  ret %"class.std::__2::basic_string"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::Status"* @_ZN5draco6StatusC2ENS0_4CodeERKNSt3__212basic_stringIcNS2_11char_traitsIcEENS2_9allocatorIcEEEE(%"class.draco::Status"* returned %this, i32 %code, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %error_msg) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::Status"*, align 4
  %code.addr = alloca i32, align 4
  %error_msg.addr = alloca %"class.std::__2::basic_string"*, align 4
  store %"class.draco::Status"* %this, %"class.draco::Status"** %this.addr, align 4
  store i32 %code, i32* %code.addr, align 4
  store %"class.std::__2::basic_string"* %error_msg, %"class.std::__2::basic_string"** %error_msg.addr, align 4
  %this1 = load %"class.draco::Status"*, %"class.draco::Status"** %this.addr, align 4
  %code_ = getelementptr inbounds %"class.draco::Status", %"class.draco::Status"* %this1, i32 0, i32 0
  %0 = load i32, i32* %code.addr, align 4
  store i32 %0, i32* %code_, align 4
  %error_msg_ = getelementptr inbounds %"class.draco::Status", %"class.draco::Status"* %this1, i32 0, i32 1
  %1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %error_msg.addr, align 4
  %call = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKS5_(%"class.std::__2::basic_string"* %error_msg_, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %1)
  ret %"class.draco::Status"* %this1
}

; Function Attrs: nounwind
declare %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_string"* returned) unnamed_addr #2

declare i32 @memcmp(i8*, i8*, i32) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIhEEbPT_(%"class.draco::DecoderBuffer"* %this, i8* %out_val) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %out_val.addr = alloca i8*, align 4
  store %"class.draco::DecoderBuffer"* %this, %"class.draco::DecoderBuffer"** %this.addr, align 4
  store i8* %out_val, i8** %out_val.addr, align 4
  %this1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %this.addr, align 4
  %0 = load i8*, i8** %out_val.addr, align 4
  %call = call zeroext i1 @_ZN5draco13DecoderBuffer4PeekIhEEbPT_(%"class.draco::DecoderBuffer"* %this1, i8* %0)
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %pos_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %1 = load i64, i64* %pos_, align 8
  %add = add nsw i64 %1, 1
  store i64 %add, i64* %pos_, align 8
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %2 = load i1, i1* %retval, align 1
  ret i1 %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco13DecoderBuffer6DecodeItEEbPT_(%"class.draco::DecoderBuffer"* %this, i16* %out_val) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %out_val.addr = alloca i16*, align 4
  store %"class.draco::DecoderBuffer"* %this, %"class.draco::DecoderBuffer"** %this.addr, align 4
  store i16* %out_val, i16** %out_val.addr, align 4
  %this1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %this.addr, align 4
  %0 = load i16*, i16** %out_val.addr, align 4
  %call = call zeroext i1 @_ZN5draco13DecoderBuffer4PeekItEEbPT_(%"class.draco::DecoderBuffer"* %this1, i16* %0)
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %pos_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %1 = load i64, i64* %pos_, align 8
  %add = add nsw i64 %1, 2
  store i64 %add, i64* %pos_, align 8
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %2 = load i1, i1* %retval, align 1
  ret i1 %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco8OkStatusEv(%"class.draco::Status"* noalias sret align 4 %agg.result) #0 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %0 = bitcast %"class.draco::Status"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  %call = call %"class.draco::Status"* @_ZN5draco6StatusC2ENS0_4CodeE(%"class.draco::Status"* %agg.result, i32 0)
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN5draco17PointCloudDecoder14DecodeMetadataEv(%"class.draco::Status"* noalias sret align 4 %agg.result, %"class.draco::PointCloudDecoder"* %this) #0 {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %"class.draco::PointCloudDecoder"*, align 4
  %metadata = alloca %"class.std::__2::unique_ptr", align 4
  %metadata_decoder = alloca %"class.draco::MetadataDecoder", align 4
  %ref.tmp = alloca %"class.std::__2::basic_string", align 4
  %cleanup.dest.slot = alloca i32, align 4
  %agg.tmp = alloca %"class.std::__2::unique_ptr", align 4
  %0 = bitcast %"class.draco::Status"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %"class.draco::PointCloudDecoder"* %this, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::PointCloudDecoder"*, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  %call = call noalias nonnull i8* @_Znwm(i32 52) #10
  %1 = bitcast i8* %call to %"class.draco::GeometryMetadata"*
  %call2 = call %"class.draco::GeometryMetadata"* @_ZN5draco16GeometryMetadataC2Ev(%"class.draco::GeometryMetadata"* %1)
  %call3 = call %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrIN5draco16GeometryMetadataENS_14default_deleteIS2_EEEC2ILb1EvEEPS2_(%"class.std::__2::unique_ptr"* %metadata, %"class.draco::GeometryMetadata"* %1) #9
  %call4 = call %"class.draco::MetadataDecoder"* @_ZN5draco15MetadataDecoderC1Ev(%"class.draco::MetadataDecoder"* %metadata_decoder)
  %buffer_ = getelementptr inbounds %"class.draco::PointCloudDecoder", %"class.draco::PointCloudDecoder"* %this1, i32 0, i32 4
  %2 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer_, align 4
  %call5 = call %"class.draco::GeometryMetadata"* @_ZNKSt3__210unique_ptrIN5draco16GeometryMetadataENS_14default_deleteIS2_EEE3getEv(%"class.std::__2::unique_ptr"* %metadata) #9
  %call6 = call zeroext i1 @_ZN5draco15MetadataDecoder22DecodeGeometryMetadataEPNS_13DecoderBufferEPNS_16GeometryMetadataE(%"class.draco::MetadataDecoder"* %metadata_decoder, %"class.draco::DecoderBuffer"* %2, %"class.draco::GeometryMetadata"* %call5)
  br i1 %call6, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  %call7 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2IDnEEPKc(%"class.std::__2::basic_string"* %ref.tmp, i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str.2, i32 0, i32 0))
  %call8 = call %"class.draco::Status"* @_ZN5draco6StatusC2ENS0_4CodeERKNSt3__212basic_stringIcNS2_11char_traitsIcEENS2_9allocatorIcEEEE(%"class.draco::Status"* %agg.result, i32 -1, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %ref.tmp)
  %call9 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_string"* %ref.tmp) #9
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %point_cloud_ = getelementptr inbounds %"class.draco::PointCloudDecoder", %"class.draco::PointCloudDecoder"* %this1, i32 0, i32 1
  %3 = load %"class.draco::PointCloud"*, %"class.draco::PointCloud"** %point_cloud_, align 4
  %call10 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr"* @_ZNSt3__24moveIRNS_10unique_ptrIN5draco16GeometryMetadataENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOS9_(%"class.std::__2::unique_ptr"* nonnull align 4 dereferenceable(4) %metadata) #9
  %call11 = call %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrIN5draco16GeometryMetadataENS_14default_deleteIS2_EEEC2EOS5_(%"class.std::__2::unique_ptr"* %agg.tmp, %"class.std::__2::unique_ptr"* nonnull align 4 dereferenceable(4) %call10) #9
  call void @_ZN5draco10PointCloud11AddMetadataENSt3__210unique_ptrINS_16GeometryMetadataENS1_14default_deleteIS3_EEEE(%"class.draco::PointCloud"* %3, %"class.std::__2::unique_ptr"* %agg.tmp)
  %call12 = call %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrIN5draco16GeometryMetadataENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr"* %agg.tmp) #9
  call void @_ZN5draco8OkStatusEv(%"class.draco::Status"* sret align 4 %agg.result)
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then
  %call13 = call %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrIN5draco16GeometryMetadataENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr"* %metadata) #9
  ret void
}

; Function Attrs: nobuiltin allocsize(0)
declare nonnull i8* @_Znwm(i32) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::GeometryMetadata"* @_ZN5draco16GeometryMetadataC2Ev(%"class.draco::GeometryMetadata"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::GeometryMetadata"*, align 4
  store %"class.draco::GeometryMetadata"* %this, %"class.draco::GeometryMetadata"** %this.addr, align 4
  %this1 = load %"class.draco::GeometryMetadata"*, %"class.draco::GeometryMetadata"** %this.addr, align 4
  %0 = bitcast %"class.draco::GeometryMetadata"* %this1 to %"class.draco::Metadata"*
  %call = call %"class.draco::Metadata"* @_ZN5draco8MetadataC2Ev(%"class.draco::Metadata"* %0)
  %att_metadatas_ = getelementptr inbounds %"class.draco::GeometryMetadata", %"class.draco::GeometryMetadata"* %this1, i32 0, i32 1
  %call2 = call %"class.std::__2::vector"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEC2Ev(%"class.std::__2::vector"* %att_metadatas_) #9
  ret %"class.draco::GeometryMetadata"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrIN5draco16GeometryMetadataENS_14default_deleteIS2_EEEC2ILb1EvEEPS2_(%"class.std::__2::unique_ptr"* returned %this, %"class.draco::GeometryMetadata"* %__p) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  %__p.addr = alloca %"class.draco::GeometryMetadata"*, align 4
  %ref.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  store %"class.draco::GeometryMetadata"* %__p, %"class.draco::GeometryMetadata"** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::__compressed_pair"* @_ZNSt3__217__compressed_pairIPN5draco16GeometryMetadataENS_14default_deleteIS2_EEEC2IRS3_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair"* %__ptr_, %"class.draco::GeometryMetadata"** nonnull align 4 dereferenceable(4) %__p.addr, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp)
  ret %"class.std::__2::unique_ptr"* %this1
}

declare %"class.draco::MetadataDecoder"* @_ZN5draco15MetadataDecoderC1Ev(%"class.draco::MetadataDecoder"* returned) unnamed_addr #3

declare zeroext i1 @_ZN5draco15MetadataDecoder22DecodeGeometryMetadataEPNS_13DecoderBufferEPNS_16GeometryMetadataE(%"class.draco::MetadataDecoder"*, %"class.draco::DecoderBuffer"*, %"class.draco::GeometryMetadata"*) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::GeometryMetadata"* @_ZNKSt3__210unique_ptrIN5draco16GeometryMetadataENS_14default_deleteIS2_EEE3getEv(%"class.std::__2::unique_ptr"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::GeometryMetadata"** @_ZNKSt3__217__compressed_pairIPN5draco16GeometryMetadataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair"* %__ptr_) #9
  %0 = load %"class.draco::GeometryMetadata"*, %"class.draco::GeometryMetadata"** %call, align 4
  ret %"class.draco::GeometryMetadata"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco10PointCloud11AddMetadataENSt3__210unique_ptrINS_16GeometryMetadataENS1_14default_deleteIS3_EEEE(%"class.draco::PointCloud"* %this, %"class.std::__2::unique_ptr"* %metadata) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointCloud"*, align 4
  store %"class.draco::PointCloud"* %this, %"class.draco::PointCloud"** %this.addr, align 4
  %this1 = load %"class.draco::PointCloud"*, %"class.draco::PointCloud"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr"* @_ZNSt3__24moveIRNS_10unique_ptrIN5draco16GeometryMetadataENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOS9_(%"class.std::__2::unique_ptr"* nonnull align 4 dereferenceable(4) %metadata) #9
  %metadata_ = getelementptr inbounds %"class.draco::PointCloud", %"class.draco::PointCloud"* %this1, i32 0, i32 1
  %call2 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrIN5draco16GeometryMetadataENS_14default_deleteIS2_EEEaSEOS5_(%"class.std::__2::unique_ptr"* %metadata_, %"class.std::__2::unique_ptr"* nonnull align 4 dereferenceable(4) %call) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr"* @_ZNSt3__24moveIRNS_10unique_ptrIN5draco16GeometryMetadataENS_14default_deleteIS3_EEEEEEONS_16remove_referenceIT_E4typeEOS9_(%"class.std::__2::unique_ptr"* nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  store %"class.std::__2::unique_ptr"* %__t, %"class.std::__2::unique_ptr"** %__t.addr, align 4
  %0 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %__t.addr, align 4
  ret %"class.std::__2::unique_ptr"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrIN5draco16GeometryMetadataENS_14default_deleteIS2_EEEC2EOS5_(%"class.std::__2::unique_ptr"* returned %this, %"class.std::__2::unique_ptr"* nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  %__u.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  %ref.tmp = alloca %"class.draco::GeometryMetadata"*, align 4
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  store %"class.std::__2::unique_ptr"* %__u, %"class.std::__2::unique_ptr"** %__u.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %__u.addr, align 4
  %call = call %"class.draco::GeometryMetadata"* @_ZNSt3__210unique_ptrIN5draco16GeometryMetadataENS_14default_deleteIS2_EEE7releaseEv(%"class.std::__2::unique_ptr"* %0) #9
  store %"class.draco::GeometryMetadata"* %call, %"class.draco::GeometryMetadata"** %ref.tmp, align 4
  %1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %__u.addr, align 4
  %call2 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.50"* @_ZNSt3__210unique_ptrIN5draco16GeometryMetadataENS_14default_deleteIS2_EEE11get_deleterEv(%"class.std::__2::unique_ptr"* %1) #9
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.50"* @_ZNSt3__27forwardINS_14default_deleteIN5draco16GeometryMetadataEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.50"* nonnull align 1 dereferenceable(1) %call2) #9
  %call4 = call %"class.std::__2::__compressed_pair"* @_ZNSt3__217__compressed_pairIPN5draco16GeometryMetadataENS_14default_deleteIS2_EEEC2IS3_S5_EEOT_OT0_(%"class.std::__2::__compressed_pair"* %__ptr_, %"class.draco::GeometryMetadata"** nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::default_delete.50"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::unique_ptr"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrIN5draco16GeometryMetadataENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  call void @_ZNSt3__210unique_ptrIN5draco16GeometryMetadataENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr"* %this1, %"class.draco::GeometryMetadata"* null) #9
  ret %"class.std::__2::unique_ptr"* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN5draco17PointCloudDecoder6DecodeERKNS_12DracoOptionsINS_17GeometryAttribute4TypeEEEPNS_13DecoderBufferEPNS_10PointCloudE(%"class.draco::Status"* noalias sret align 4 %agg.result, %"class.draco::PointCloudDecoder"* %this, %"class.draco::DracoOptions"* nonnull align 4 dereferenceable(24) %options, %"class.draco::DecoderBuffer"* %in_buffer, %"class.draco::PointCloud"* %out_point_cloud) #0 {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %"class.draco::PointCloudDecoder"*, align 4
  %options.addr = alloca %"class.draco::DracoOptions"*, align 4
  %in_buffer.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %out_point_cloud.addr = alloca %"class.draco::PointCloud"*, align 4
  %header = alloca %"struct.draco::DracoHeader", align 2
  %nrvo = alloca i1, align 1
  %cleanup.dest.slot = alloca i32, align 4
  %ref.tmp = alloca %"class.std::__2::basic_string", align 4
  %max_supported_major_version = alloca i8, align 1
  %max_supported_minor_version = alloca i8, align 1
  %ref.tmp25 = alloca %"class.std::__2::basic_string", align 4
  %ref.tmp39 = alloca %"class.std::__2::basic_string", align 4
  %nrvo57 = alloca i1, align 1
  %ref.tmp73 = alloca %"class.std::__2::basic_string", align 4
  %ref.tmp82 = alloca %"class.std::__2::basic_string", align 4
  %ref.tmp91 = alloca %"class.std::__2::basic_string", align 4
  %0 = bitcast %"class.draco::Status"* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %"class.draco::PointCloudDecoder"* %this, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  store %"class.draco::DracoOptions"* %options, %"class.draco::DracoOptions"** %options.addr, align 4
  store %"class.draco::DecoderBuffer"* %in_buffer, %"class.draco::DecoderBuffer"** %in_buffer.addr, align 4
  store %"class.draco::PointCloud"* %out_point_cloud, %"class.draco::PointCloud"** %out_point_cloud.addr, align 4
  %this1 = load %"class.draco::PointCloudDecoder"*, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  %1 = load %"class.draco::DracoOptions"*, %"class.draco::DracoOptions"** %options.addr, align 4
  %options_ = getelementptr inbounds %"class.draco::PointCloudDecoder", %"class.draco::PointCloudDecoder"* %this1, i32 0, i32 7
  store %"class.draco::DracoOptions"* %1, %"class.draco::DracoOptions"** %options_, align 4
  %2 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %in_buffer.addr, align 4
  %buffer_ = getelementptr inbounds %"class.draco::PointCloudDecoder", %"class.draco::PointCloudDecoder"* %this1, i32 0, i32 4
  store %"class.draco::DecoderBuffer"* %2, %"class.draco::DecoderBuffer"** %buffer_, align 4
  %3 = load %"class.draco::PointCloud"*, %"class.draco::PointCloud"** %out_point_cloud.addr, align 4
  %point_cloud_ = getelementptr inbounds %"class.draco::PointCloudDecoder", %"class.draco::PointCloudDecoder"* %this1, i32 0, i32 1
  store %"class.draco::PointCloud"* %3, %"class.draco::PointCloud"** %point_cloud_, align 4
  store i1 false, i1* %nrvo, align 1
  %buffer_2 = getelementptr inbounds %"class.draco::PointCloudDecoder", %"class.draco::PointCloudDecoder"* %this1, i32 0, i32 4
  %4 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer_2, align 4
  call void @_ZN5draco17PointCloudDecoder12DecodeHeaderEPNS_13DecoderBufferEPNS_11DracoHeaderE(%"class.draco::Status"* sret align 4 %agg.result, %"class.draco::DecoderBuffer"* %4, %"struct.draco::DracoHeader"* %header)
  %call = call zeroext i1 @_ZNK5draco6Status2okEv(%"class.draco::Status"* %agg.result)
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i1 true, i1* %nrvo, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then
  %nrvo.val = load i1, i1* %nrvo, align 1
  br i1 %nrvo.val, label %nrvo.skipdtor, label %nrvo.unused

nrvo.unused:                                      ; preds = %cleanup
  %call3 = call %"class.draco::Status"* @_ZN5draco6StatusD2Ev(%"class.draco::Status"* %agg.result) #9
  br label %nrvo.skipdtor

nrvo.skipdtor:                                    ; preds = %nrvo.unused, %cleanup
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %return
  ]

cleanup.cont:                                     ; preds = %nrvo.skipdtor
  %encoder_type = getelementptr inbounds %"struct.draco::DracoHeader", %"struct.draco::DracoHeader"* %header, i32 0, i32 3
  %5 = load i8, i8* %encoder_type, align 1
  %conv = zext i8 %5 to i32
  %6 = bitcast %"class.draco::PointCloudDecoder"* %this1 to i32 (%"class.draco::PointCloudDecoder"*)***
  %vtable = load i32 (%"class.draco::PointCloudDecoder"*)**, i32 (%"class.draco::PointCloudDecoder"*)*** %6, align 4
  %vfn = getelementptr inbounds i32 (%"class.draco::PointCloudDecoder"*)*, i32 (%"class.draco::PointCloudDecoder"*)** %vtable, i64 2
  %7 = load i32 (%"class.draco::PointCloudDecoder"*)*, i32 (%"class.draco::PointCloudDecoder"*)** %vfn, align 4
  %call4 = call i32 %7(%"class.draco::PointCloudDecoder"* %this1)
  %cmp = icmp ne i32 %conv, %call4
  br i1 %cmp, label %if.then5, label %if.end9

if.then5:                                         ; preds = %cleanup.cont
  %call6 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2IDnEEPKc(%"class.std::__2::basic_string"* %ref.tmp, i8* getelementptr inbounds ([51 x i8], [51 x i8]* @.str.3, i32 0, i32 0))
  %call7 = call %"class.draco::Status"* @_ZN5draco6StatusC2ENS0_4CodeERKNSt3__212basic_stringIcNS2_11char_traitsIcEENS2_9allocatorIcEEEE(%"class.draco::Status"* %agg.result, i32 -1, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %ref.tmp)
  %call8 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_string"* %ref.tmp) #9
  br label %return

if.end9:                                          ; preds = %cleanup.cont
  %version_major = getelementptr inbounds %"struct.draco::DracoHeader", %"struct.draco::DracoHeader"* %header, i32 0, i32 1
  %8 = load i8, i8* %version_major, align 1
  %version_major_ = getelementptr inbounds %"class.draco::PointCloudDecoder", %"class.draco::PointCloudDecoder"* %this1, i32 0, i32 5
  store i8 %8, i8* %version_major_, align 4
  %version_minor = getelementptr inbounds %"struct.draco::DracoHeader", %"struct.draco::DracoHeader"* %header, i32 0, i32 2
  %9 = load i8, i8* %version_minor, align 2
  %version_minor_ = getelementptr inbounds %"class.draco::PointCloudDecoder", %"class.draco::PointCloudDecoder"* %this1, i32 0, i32 6
  store i8 %9, i8* %version_minor_, align 1
  %encoder_type10 = getelementptr inbounds %"struct.draco::DracoHeader", %"struct.draco::DracoHeader"* %header, i32 0, i32 3
  %10 = load i8, i8* %encoder_type10, align 1
  %conv11 = zext i8 %10 to i32
  %cmp12 = icmp eq i32 %conv11, 0
  %11 = zext i1 %cmp12 to i64
  %cond = select i1 %cmp12, i8 2, i8 2
  store i8 %cond, i8* %max_supported_major_version, align 1
  %encoder_type13 = getelementptr inbounds %"struct.draco::DracoHeader", %"struct.draco::DracoHeader"* %header, i32 0, i32 3
  %12 = load i8, i8* %encoder_type13, align 1
  %conv14 = zext i8 %12 to i32
  %cmp15 = icmp eq i32 %conv14, 0
  %13 = zext i1 %cmp15 to i64
  %cond16 = select i1 %cmp15, i8 3, i8 2
  store i8 %cond16, i8* %max_supported_minor_version, align 1
  %version_major_17 = getelementptr inbounds %"class.draco::PointCloudDecoder", %"class.draco::PointCloudDecoder"* %this1, i32 0, i32 5
  %14 = load i8, i8* %version_major_17, align 4
  %conv18 = zext i8 %14 to i32
  %cmp19 = icmp slt i32 %conv18, 1
  br i1 %cmp19, label %if.then24, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end9
  %version_major_20 = getelementptr inbounds %"class.draco::PointCloudDecoder", %"class.draco::PointCloudDecoder"* %this1, i32 0, i32 5
  %15 = load i8, i8* %version_major_20, align 4
  %conv21 = zext i8 %15 to i32
  %16 = load i8, i8* %max_supported_major_version, align 1
  %conv22 = zext i8 %16 to i32
  %cmp23 = icmp sgt i32 %conv21, %conv22
  br i1 %cmp23, label %if.then24, label %if.end29

if.then24:                                        ; preds = %lor.lhs.false, %if.end9
  %call26 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2IDnEEPKc(%"class.std::__2::basic_string"* %ref.tmp25, i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.4, i32 0, i32 0))
  %call27 = call %"class.draco::Status"* @_ZN5draco6StatusC2ENS0_4CodeERKNSt3__212basic_stringIcNS2_11char_traitsIcEENS2_9allocatorIcEEEE(%"class.draco::Status"* %agg.result, i32 -5, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %ref.tmp25)
  %call28 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_string"* %ref.tmp25) #9
  br label %return

if.end29:                                         ; preds = %lor.lhs.false
  %version_major_30 = getelementptr inbounds %"class.draco::PointCloudDecoder", %"class.draco::PointCloudDecoder"* %this1, i32 0, i32 5
  %17 = load i8, i8* %version_major_30, align 4
  %conv31 = zext i8 %17 to i32
  %18 = load i8, i8* %max_supported_major_version, align 1
  %conv32 = zext i8 %18 to i32
  %cmp33 = icmp eq i32 %conv31, %conv32
  br i1 %cmp33, label %land.lhs.true, label %if.end43

land.lhs.true:                                    ; preds = %if.end29
  %version_minor_34 = getelementptr inbounds %"class.draco::PointCloudDecoder", %"class.draco::PointCloudDecoder"* %this1, i32 0, i32 6
  %19 = load i8, i8* %version_minor_34, align 1
  %conv35 = zext i8 %19 to i32
  %20 = load i8, i8* %max_supported_minor_version, align 1
  %conv36 = zext i8 %20 to i32
  %cmp37 = icmp sgt i32 %conv35, %conv36
  br i1 %cmp37, label %if.then38, label %if.end43

if.then38:                                        ; preds = %land.lhs.true
  %call40 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2IDnEEPKc(%"class.std::__2::basic_string"* %ref.tmp39, i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.5, i32 0, i32 0))
  %call41 = call %"class.draco::Status"* @_ZN5draco6StatusC2ENS0_4CodeERKNSt3__212basic_stringIcNS2_11char_traitsIcEENS2_9allocatorIcEEEE(%"class.draco::Status"* %agg.result, i32 -5, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %ref.tmp39)
  %call42 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_string"* %ref.tmp39) #9
  br label %return

if.end43:                                         ; preds = %land.lhs.true, %if.end29
  %buffer_44 = getelementptr inbounds %"class.draco::PointCloudDecoder", %"class.draco::PointCloudDecoder"* %this1, i32 0, i32 4
  %21 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer_44, align 4
  %version_major_45 = getelementptr inbounds %"class.draco::PointCloudDecoder", %"class.draco::PointCloudDecoder"* %this1, i32 0, i32 5
  %22 = load i8, i8* %version_major_45, align 4
  %conv46 = zext i8 %22 to i16
  %conv47 = zext i16 %conv46 to i32
  %shl = shl i32 %conv47, 8
  %version_minor_48 = getelementptr inbounds %"class.draco::PointCloudDecoder", %"class.draco::PointCloudDecoder"* %this1, i32 0, i32 6
  %23 = load i8, i8* %version_minor_48, align 1
  %conv49 = zext i8 %23 to i32
  %or = or i32 %shl, %conv49
  %conv50 = trunc i32 %or to i16
  call void @_ZN5draco13DecoderBuffer21set_bitstream_versionEt(%"class.draco::DecoderBuffer"* %21, i16 zeroext %conv50)
  %call51 = call zeroext i16 @_ZNK5draco17PointCloudDecoder17bitstream_versionEv(%"class.draco::PointCloudDecoder"* %this1)
  %conv52 = zext i16 %call51 to i32
  %cmp53 = icmp sge i32 %conv52, 259
  br i1 %cmp53, label %land.lhs.true54, label %if.end68

land.lhs.true54:                                  ; preds = %if.end43
  %flags = getelementptr inbounds %"struct.draco::DracoHeader", %"struct.draco::DracoHeader"* %header, i32 0, i32 5
  %24 = load i16, i16* %flags, align 2
  %conv55 = zext i16 %24 to i32
  %and = and i32 %conv55, 32768
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then56, label %if.end68

if.then56:                                        ; preds = %land.lhs.true54
  store i1 false, i1* %nrvo57, align 1
  call void @_ZN5draco17PointCloudDecoder14DecodeMetadataEv(%"class.draco::Status"* sret align 4 %agg.result, %"class.draco::PointCloudDecoder"* %this1)
  %call58 = call zeroext i1 @_ZNK5draco6Status2okEv(%"class.draco::Status"* %agg.result)
  br i1 %call58, label %if.end60, label %if.then59

if.then59:                                        ; preds = %if.then56
  store i1 true, i1* %nrvo57, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup61

if.end60:                                         ; preds = %if.then56
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup61

cleanup61:                                        ; preds = %if.end60, %if.then59
  %nrvo.val62 = load i1, i1* %nrvo57, align 1
  br i1 %nrvo.val62, label %nrvo.skipdtor65, label %nrvo.unused63

nrvo.unused63:                                    ; preds = %cleanup61
  %call64 = call %"class.draco::Status"* @_ZN5draco6StatusD2Ev(%"class.draco::Status"* %agg.result) #9
  br label %nrvo.skipdtor65

nrvo.skipdtor65:                                  ; preds = %nrvo.unused63, %cleanup61
  %cleanup.dest66 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest66, label %unreachable [
    i32 0, label %cleanup.cont67
    i32 1, label %return
  ]

cleanup.cont67:                                   ; preds = %nrvo.skipdtor65
  br label %if.end68

if.end68:                                         ; preds = %cleanup.cont67, %land.lhs.true54, %if.end43
  %25 = bitcast %"class.draco::PointCloudDecoder"* %this1 to i1 (%"class.draco::PointCloudDecoder"*)***
  %vtable69 = load i1 (%"class.draco::PointCloudDecoder"*)**, i1 (%"class.draco::PointCloudDecoder"*)*** %25, align 4
  %vfn70 = getelementptr inbounds i1 (%"class.draco::PointCloudDecoder"*)*, i1 (%"class.draco::PointCloudDecoder"*)** %vtable69, i64 3
  %26 = load i1 (%"class.draco::PointCloudDecoder"*)*, i1 (%"class.draco::PointCloudDecoder"*)** %vfn70, align 4
  %call71 = call zeroext i1 %26(%"class.draco::PointCloudDecoder"* %this1)
  br i1 %call71, label %if.end77, label %if.then72

if.then72:                                        ; preds = %if.end68
  %call74 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2IDnEEPKc(%"class.std::__2::basic_string"* %ref.tmp73, i8* getelementptr inbounds ([34 x i8], [34 x i8]* @.str.6, i32 0, i32 0))
  %call75 = call %"class.draco::Status"* @_ZN5draco6StatusC2ENS0_4CodeERKNSt3__212basic_stringIcNS2_11char_traitsIcEENS2_9allocatorIcEEEE(%"class.draco::Status"* %agg.result, i32 -1, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %ref.tmp73)
  %call76 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_string"* %ref.tmp73) #9
  br label %return

if.end77:                                         ; preds = %if.end68
  %27 = bitcast %"class.draco::PointCloudDecoder"* %this1 to i1 (%"class.draco::PointCloudDecoder"*)***
  %vtable78 = load i1 (%"class.draco::PointCloudDecoder"*)**, i1 (%"class.draco::PointCloudDecoder"*)*** %27, align 4
  %vfn79 = getelementptr inbounds i1 (%"class.draco::PointCloudDecoder"*)*, i1 (%"class.draco::PointCloudDecoder"*)** %vtable78, i64 5
  %28 = load i1 (%"class.draco::PointCloudDecoder"*)*, i1 (%"class.draco::PointCloudDecoder"*)** %vfn79, align 4
  %call80 = call zeroext i1 %28(%"class.draco::PointCloudDecoder"* %this1)
  br i1 %call80, label %if.end86, label %if.then81

if.then81:                                        ; preds = %if.end77
  %call83 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2IDnEEPKc(%"class.std::__2::basic_string"* %ref.tmp82, i8* getelementptr inbounds ([32 x i8], [32 x i8]* @.str.7, i32 0, i32 0))
  %call84 = call %"class.draco::Status"* @_ZN5draco6StatusC2ENS0_4CodeERKNSt3__212basic_stringIcNS2_11char_traitsIcEENS2_9allocatorIcEEEE(%"class.draco::Status"* %agg.result, i32 -1, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %ref.tmp82)
  %call85 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_string"* %ref.tmp82) #9
  br label %return

if.end86:                                         ; preds = %if.end77
  %29 = bitcast %"class.draco::PointCloudDecoder"* %this1 to i1 (%"class.draco::PointCloudDecoder"*)***
  %vtable87 = load i1 (%"class.draco::PointCloudDecoder"*)**, i1 (%"class.draco::PointCloudDecoder"*)*** %29, align 4
  %vfn88 = getelementptr inbounds i1 (%"class.draco::PointCloudDecoder"*)*, i1 (%"class.draco::PointCloudDecoder"*)** %vtable87, i64 6
  %30 = load i1 (%"class.draco::PointCloudDecoder"*)*, i1 (%"class.draco::PointCloudDecoder"*)** %vfn88, align 4
  %call89 = call zeroext i1 %30(%"class.draco::PointCloudDecoder"* %this1)
  br i1 %call89, label %if.end95, label %if.then90

if.then90:                                        ; preds = %if.end86
  %call92 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2IDnEEPKc(%"class.std::__2::basic_string"* %ref.tmp91, i8* getelementptr inbounds ([35 x i8], [35 x i8]* @.str.8, i32 0, i32 0))
  %call93 = call %"class.draco::Status"* @_ZN5draco6StatusC2ENS0_4CodeERKNSt3__212basic_stringIcNS2_11char_traitsIcEENS2_9allocatorIcEEEE(%"class.draco::Status"* %agg.result, i32 -1, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12) %ref.tmp91)
  %call94 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_string"* %ref.tmp91) #9
  br label %return

if.end95:                                         ; preds = %if.end86
  call void @_ZN5draco8OkStatusEv(%"class.draco::Status"* sret align 4 %agg.result)
  br label %return

return:                                           ; preds = %if.end95, %if.then90, %if.then81, %if.then72, %nrvo.skipdtor65, %if.then38, %if.then24, %if.then5, %nrvo.skipdtor
  ret void

unreachable:                                      ; preds = %nrvo.skipdtor65, %nrvo.skipdtor
  unreachable
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK5draco6Status2okEv(%"class.draco::Status"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::Status"*, align 4
  store %"class.draco::Status"* %this, %"class.draco::Status"** %this.addr, align 4
  %this1 = load %"class.draco::Status"*, %"class.draco::Status"** %this.addr, align 4
  %code_ = getelementptr inbounds %"class.draco::Status", %"class.draco::Status"* %this1, i32 0, i32 0
  %0 = load i32, i32* %code_, align 4
  %cmp = icmp eq i32 %0, 0
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::Status"* @_ZN5draco6StatusD2Ev(%"class.draco::Status"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::Status"*, align 4
  store %"class.draco::Status"* %this, %"class.draco::Status"** %this.addr, align 4
  %this1 = load %"class.draco::Status"*, %"class.draco::Status"** %this.addr, align 4
  %error_msg_ = getelementptr inbounds %"class.draco::Status", %"class.draco::Status"* %this1, i32 0, i32 1
  %call = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_string"* %error_msg_) #9
  ret %"class.draco::Status"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco13DecoderBuffer21set_bitstream_versionEt(%"class.draco::DecoderBuffer"* %this, i16 zeroext %version) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %version.addr = alloca i16, align 2
  store %"class.draco::DecoderBuffer"* %this, %"class.draco::DecoderBuffer"** %this.addr, align 4
  store i16 %version, i16* %version.addr, align 2
  %this1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %this.addr, align 4
  %0 = load i16, i16* %version.addr, align 2
  %bitstream_version_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 5
  store i16 %0, i16* %bitstream_version_, align 2
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i16 @_ZNK5draco17PointCloudDecoder17bitstream_versionEv(%"class.draco::PointCloudDecoder"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointCloudDecoder"*, align 4
  store %"class.draco::PointCloudDecoder"* %this, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::PointCloudDecoder"*, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  %version_major_ = getelementptr inbounds %"class.draco::PointCloudDecoder", %"class.draco::PointCloudDecoder"* %this1, i32 0, i32 5
  %0 = load i8, i8* %version_major_, align 4
  %conv = zext i8 %0 to i16
  %conv2 = zext i16 %conv to i32
  %shl = shl i32 %conv2, 8
  %version_minor_ = getelementptr inbounds %"class.draco::PointCloudDecoder", %"class.draco::PointCloudDecoder"* %this1, i32 0, i32 6
  %1 = load i8, i8* %version_minor_, align 1
  %conv3 = zext i8 %1 to i32
  %or = or i32 %shl, %conv3
  %conv4 = trunc i32 %or to i16
  ret i16 %conv4
}

; Function Attrs: noinline nounwind optnone
define hidden zeroext i1 @_ZN5draco17PointCloudDecoder21DecodePointAttributesEv(%"class.draco::PointCloudDecoder"* %this) unnamed_addr #0 {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::PointCloudDecoder"*, align 4
  %num_attributes_decoders = alloca i8, align 1
  %i = alloca i32, align 4
  %__range1 = alloca %"class.std::__2::vector.94"*, align 4
  %__begin1 = alloca %"class.std::__2::__wrap_iter", align 4
  %__end1 = alloca %"class.std::__2::__wrap_iter", align 4
  %att_dec = alloca %"class.std::__2::unique_ptr.96"*, align 4
  %i21 = alloca i32, align 4
  %i38 = alloca i32, align 4
  %num_attributes = alloca i32, align 4
  %j = alloca i32, align 4
  %att_id = alloca i32, align 4
  store %"class.draco::PointCloudDecoder"* %this, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::PointCloudDecoder"*, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  %buffer_ = getelementptr inbounds %"class.draco::PointCloudDecoder", %"class.draco::PointCloudDecoder"* %this1, i32 0, i32 4
  %0 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer_, align 4
  %call = call zeroext i1 @_ZN5draco13DecoderBuffer6DecodeIhEEbPT_(%"class.draco::DecoderBuffer"* %0, i8* %num_attributes_decoders)
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %1 = load i32, i32* %i, align 4
  %2 = load i8, i8* %num_attributes_decoders, align 1
  %conv = zext i8 %2 to i32
  %cmp = icmp slt i32 %1, %conv
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load i32, i32* %i, align 4
  %4 = bitcast %"class.draco::PointCloudDecoder"* %this1 to i1 (%"class.draco::PointCloudDecoder"*, i32)***
  %vtable = load i1 (%"class.draco::PointCloudDecoder"*, i32)**, i1 (%"class.draco::PointCloudDecoder"*, i32)*** %4, align 4
  %vfn = getelementptr inbounds i1 (%"class.draco::PointCloudDecoder"*, i32)*, i1 (%"class.draco::PointCloudDecoder"*, i32)** %vtable, i64 4
  %5 = load i1 (%"class.draco::PointCloudDecoder"*, i32)*, i1 (%"class.draco::PointCloudDecoder"*, i32)** %vfn, align 4
  %call2 = call zeroext i1 %5(%"class.draco::PointCloudDecoder"* %this1, i32 %3)
  br i1 %call2, label %if.end4, label %if.then3

if.then3:                                         ; preds = %for.body
  store i1 false, i1* %retval, align 1
  br label %return

if.end4:                                          ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end4
  %6 = load i32, i32* %i, align 4
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %attributes_decoders_ = getelementptr inbounds %"class.draco::PointCloudDecoder", %"class.draco::PointCloudDecoder"* %this1, i32 0, i32 2
  store %"class.std::__2::vector.94"* %attributes_decoders_, %"class.std::__2::vector.94"** %__range1, align 4
  %7 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %__range1, align 4
  %call5 = call %"class.std::__2::unique_ptr.96"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5beginEv(%"class.std::__2::vector.94"* %7) #9
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %__begin1, i32 0, i32 0
  store %"class.std::__2::unique_ptr.96"* %call5, %"class.std::__2::unique_ptr.96"** %coerce.dive, align 4
  %8 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %__range1, align 4
  %call6 = call %"class.std::__2::unique_ptr.96"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE3endEv(%"class.std::__2::vector.94"* %8) #9
  %coerce.dive7 = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %__end1, i32 0, i32 0
  store %"class.std::__2::unique_ptr.96"* %call6, %"class.std::__2::unique_ptr.96"** %coerce.dive7, align 4
  br label %for.cond8

for.cond8:                                        ; preds = %for.inc18, %for.end
  %call9 = call zeroext i1 @_ZNSt3__2neIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEEbRKNS_11__wrap_iterIT_EESC_(%"class.std::__2::__wrap_iter"* nonnull align 4 dereferenceable(4) %__begin1, %"class.std::__2::__wrap_iter"* nonnull align 4 dereferenceable(4) %__end1) #9
  br i1 %call9, label %for.body10, label %for.end20

for.body10:                                       ; preds = %for.cond8
  %call11 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"* @_ZNKSt3__211__wrap_iterIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEdeEv(%"class.std::__2::__wrap_iter"* %__begin1) #9
  store %"class.std::__2::unique_ptr.96"* %call11, %"class.std::__2::unique_ptr.96"** %att_dec, align 4
  %9 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %att_dec, align 4
  %call12 = call %"class.draco::AttributesDecoderInterface"* @_ZNKSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr.96"* %9) #9
  %point_cloud_ = getelementptr inbounds %"class.draco::PointCloudDecoder", %"class.draco::PointCloudDecoder"* %this1, i32 0, i32 1
  %10 = load %"class.draco::PointCloud"*, %"class.draco::PointCloud"** %point_cloud_, align 4
  %11 = bitcast %"class.draco::AttributesDecoderInterface"* %call12 to i1 (%"class.draco::AttributesDecoderInterface"*, %"class.draco::PointCloudDecoder"*, %"class.draco::PointCloud"*)***
  %vtable13 = load i1 (%"class.draco::AttributesDecoderInterface"*, %"class.draco::PointCloudDecoder"*, %"class.draco::PointCloud"*)**, i1 (%"class.draco::AttributesDecoderInterface"*, %"class.draco::PointCloudDecoder"*, %"class.draco::PointCloud"*)*** %11, align 4
  %vfn14 = getelementptr inbounds i1 (%"class.draco::AttributesDecoderInterface"*, %"class.draco::PointCloudDecoder"*, %"class.draco::PointCloud"*)*, i1 (%"class.draco::AttributesDecoderInterface"*, %"class.draco::PointCloudDecoder"*, %"class.draco::PointCloud"*)** %vtable13, i64 2
  %12 = load i1 (%"class.draco::AttributesDecoderInterface"*, %"class.draco::PointCloudDecoder"*, %"class.draco::PointCloud"*)*, i1 (%"class.draco::AttributesDecoderInterface"*, %"class.draco::PointCloudDecoder"*, %"class.draco::PointCloud"*)** %vfn14, align 4
  %call15 = call zeroext i1 %12(%"class.draco::AttributesDecoderInterface"* %call12, %"class.draco::PointCloudDecoder"* %this1, %"class.draco::PointCloud"* %10)
  br i1 %call15, label %if.end17, label %if.then16

if.then16:                                        ; preds = %for.body10
  store i1 false, i1* %retval, align 1
  br label %return

if.end17:                                         ; preds = %for.body10
  br label %for.inc18

for.inc18:                                        ; preds = %if.end17
  %call19 = call nonnull align 4 dereferenceable(4) %"class.std::__2::__wrap_iter"* @_ZNSt3__211__wrap_iterIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEppEv(%"class.std::__2::__wrap_iter"* %__begin1) #9
  br label %for.cond8

for.end20:                                        ; preds = %for.cond8
  store i32 0, i32* %i21, align 4
  br label %for.cond22

for.cond22:                                       ; preds = %for.inc35, %for.end20
  %13 = load i32, i32* %i21, align 4
  %14 = load i8, i8* %num_attributes_decoders, align 1
  %conv23 = zext i8 %14 to i32
  %cmp24 = icmp slt i32 %13, %conv23
  br i1 %cmp24, label %for.body25, label %for.end37

for.body25:                                       ; preds = %for.cond22
  %attributes_decoders_26 = getelementptr inbounds %"class.draco::PointCloudDecoder", %"class.draco::PointCloudDecoder"* %this1, i32 0, i32 2
  %15 = load i32, i32* %i21, align 4
  %call27 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEixEm(%"class.std::__2::vector.94"* %attributes_decoders_26, i32 %15) #9
  %call28 = call %"class.draco::AttributesDecoderInterface"* @_ZNKSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr.96"* %call27) #9
  %buffer_29 = getelementptr inbounds %"class.draco::PointCloudDecoder", %"class.draco::PointCloudDecoder"* %this1, i32 0, i32 4
  %16 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer_29, align 4
  %17 = bitcast %"class.draco::AttributesDecoderInterface"* %call28 to i1 (%"class.draco::AttributesDecoderInterface"*, %"class.draco::DecoderBuffer"*)***
  %vtable30 = load i1 (%"class.draco::AttributesDecoderInterface"*, %"class.draco::DecoderBuffer"*)**, i1 (%"class.draco::AttributesDecoderInterface"*, %"class.draco::DecoderBuffer"*)*** %17, align 4
  %vfn31 = getelementptr inbounds i1 (%"class.draco::AttributesDecoderInterface"*, %"class.draco::DecoderBuffer"*)*, i1 (%"class.draco::AttributesDecoderInterface"*, %"class.draco::DecoderBuffer"*)** %vtable30, i64 3
  %18 = load i1 (%"class.draco::AttributesDecoderInterface"*, %"class.draco::DecoderBuffer"*)*, i1 (%"class.draco::AttributesDecoderInterface"*, %"class.draco::DecoderBuffer"*)** %vfn31, align 4
  %call32 = call zeroext i1 %18(%"class.draco::AttributesDecoderInterface"* %call28, %"class.draco::DecoderBuffer"* %16)
  br i1 %call32, label %if.end34, label %if.then33

if.then33:                                        ; preds = %for.body25
  store i1 false, i1* %retval, align 1
  br label %return

if.end34:                                         ; preds = %for.body25
  br label %for.inc35

for.inc35:                                        ; preds = %if.end34
  %19 = load i32, i32* %i21, align 4
  %inc36 = add nsw i32 %19, 1
  store i32 %inc36, i32* %i21, align 4
  br label %for.cond22

for.end37:                                        ; preds = %for.cond22
  store i32 0, i32* %i38, align 4
  br label %for.cond39

for.cond39:                                       ; preds = %for.inc68, %for.end37
  %20 = load i32, i32* %i38, align 4
  %21 = load i8, i8* %num_attributes_decoders, align 1
  %conv40 = zext i8 %21 to i32
  %cmp41 = icmp slt i32 %20, %conv40
  br i1 %cmp41, label %for.body42, label %for.end70

for.body42:                                       ; preds = %for.cond39
  %attributes_decoders_43 = getelementptr inbounds %"class.draco::PointCloudDecoder", %"class.draco::PointCloudDecoder"* %this1, i32 0, i32 2
  %22 = load i32, i32* %i38, align 4
  %call44 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEixEm(%"class.std::__2::vector.94"* %attributes_decoders_43, i32 %22) #9
  %call45 = call %"class.draco::AttributesDecoderInterface"* @_ZNKSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr.96"* %call44) #9
  %23 = bitcast %"class.draco::AttributesDecoderInterface"* %call45 to i32 (%"class.draco::AttributesDecoderInterface"*)***
  %vtable46 = load i32 (%"class.draco::AttributesDecoderInterface"*)**, i32 (%"class.draco::AttributesDecoderInterface"*)*** %23, align 4
  %vfn47 = getelementptr inbounds i32 (%"class.draco::AttributesDecoderInterface"*)*, i32 (%"class.draco::AttributesDecoderInterface"*)** %vtable46, i64 6
  %24 = load i32 (%"class.draco::AttributesDecoderInterface"*)*, i32 (%"class.draco::AttributesDecoderInterface"*)** %vfn47, align 4
  %call48 = call i32 %24(%"class.draco::AttributesDecoderInterface"* %call45)
  store i32 %call48, i32* %num_attributes, align 4
  store i32 0, i32* %j, align 4
  br label %for.cond49

for.cond49:                                       ; preds = %for.inc65, %for.body42
  %25 = load i32, i32* %j, align 4
  %26 = load i32, i32* %num_attributes, align 4
  %cmp50 = icmp slt i32 %25, %26
  br i1 %cmp50, label %for.body51, label %for.end67

for.body51:                                       ; preds = %for.cond49
  %attributes_decoders_52 = getelementptr inbounds %"class.draco::PointCloudDecoder", %"class.draco::PointCloudDecoder"* %this1, i32 0, i32 2
  %27 = load i32, i32* %i38, align 4
  %call53 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEixEm(%"class.std::__2::vector.94"* %attributes_decoders_52, i32 %27) #9
  %call54 = call %"class.draco::AttributesDecoderInterface"* @_ZNKSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr.96"* %call53) #9
  %28 = load i32, i32* %j, align 4
  %29 = bitcast %"class.draco::AttributesDecoderInterface"* %call54 to i32 (%"class.draco::AttributesDecoderInterface"*, i32)***
  %vtable55 = load i32 (%"class.draco::AttributesDecoderInterface"*, i32)**, i32 (%"class.draco::AttributesDecoderInterface"*, i32)*** %29, align 4
  %vfn56 = getelementptr inbounds i32 (%"class.draco::AttributesDecoderInterface"*, i32)*, i32 (%"class.draco::AttributesDecoderInterface"*, i32)** %vtable55, i64 5
  %30 = load i32 (%"class.draco::AttributesDecoderInterface"*, i32)*, i32 (%"class.draco::AttributesDecoderInterface"*, i32)** %vfn56, align 4
  %call57 = call i32 %30(%"class.draco::AttributesDecoderInterface"* %call54, i32 %28)
  store i32 %call57, i32* %att_id, align 4
  %31 = load i32, i32* %att_id, align 4
  %attribute_to_decoder_map_ = getelementptr inbounds %"class.draco::PointCloudDecoder", %"class.draco::PointCloudDecoder"* %this1, i32 0, i32 3
  %call58 = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4sizeEv(%"class.std::__2::vector.87"* %attribute_to_decoder_map_) #9
  %cmp59 = icmp uge i32 %31, %call58
  br i1 %cmp59, label %if.then60, label %if.end62

if.then60:                                        ; preds = %for.body51
  %attribute_to_decoder_map_61 = getelementptr inbounds %"class.draco::PointCloudDecoder", %"class.draco::PointCloudDecoder"* %this1, i32 0, i32 3
  %32 = load i32, i32* %att_id, align 4
  %add = add nsw i32 %32, 1
  call void @_ZNSt3__26vectorIiNS_9allocatorIiEEE6resizeEm(%"class.std::__2::vector.87"* %attribute_to_decoder_map_61, i32 %add)
  br label %if.end62

if.end62:                                         ; preds = %if.then60, %for.body51
  %33 = load i32, i32* %i38, align 4
  %attribute_to_decoder_map_63 = getelementptr inbounds %"class.draco::PointCloudDecoder", %"class.draco::PointCloudDecoder"* %this1, i32 0, i32 3
  %34 = load i32, i32* %att_id, align 4
  %call64 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIiNS_9allocatorIiEEEixEm(%"class.std::__2::vector.87"* %attribute_to_decoder_map_63, i32 %34) #9
  store i32 %33, i32* %call64, align 4
  br label %for.inc65

for.inc65:                                        ; preds = %if.end62
  %35 = load i32, i32* %j, align 4
  %inc66 = add nsw i32 %35, 1
  store i32 %inc66, i32* %j, align 4
  br label %for.cond49

for.end67:                                        ; preds = %for.cond49
  br label %for.inc68

for.inc68:                                        ; preds = %for.end67
  %36 = load i32, i32* %i38, align 4
  %inc69 = add nsw i32 %36, 1
  store i32 %inc69, i32* %i38, align 4
  br label %for.cond39

for.end70:                                        ; preds = %for.cond39
  %37 = bitcast %"class.draco::PointCloudDecoder"* %this1 to i1 (%"class.draco::PointCloudDecoder"*)***
  %vtable71 = load i1 (%"class.draco::PointCloudDecoder"*)**, i1 (%"class.draco::PointCloudDecoder"*)*** %37, align 4
  %vfn72 = getelementptr inbounds i1 (%"class.draco::PointCloudDecoder"*)*, i1 (%"class.draco::PointCloudDecoder"*)** %vtable71, i64 7
  %38 = load i1 (%"class.draco::PointCloudDecoder"*)*, i1 (%"class.draco::PointCloudDecoder"*)** %vfn72, align 4
  %call73 = call zeroext i1 %38(%"class.draco::PointCloudDecoder"* %this1)
  br i1 %call73, label %if.end75, label %if.then74

if.then74:                                        ; preds = %for.end70
  store i1 false, i1* %retval, align 1
  br label %return

if.end75:                                         ; preds = %for.end70
  %39 = bitcast %"class.draco::PointCloudDecoder"* %this1 to i1 (%"class.draco::PointCloudDecoder"*)***
  %vtable76 = load i1 (%"class.draco::PointCloudDecoder"*)**, i1 (%"class.draco::PointCloudDecoder"*)*** %39, align 4
  %vfn77 = getelementptr inbounds i1 (%"class.draco::PointCloudDecoder"*)*, i1 (%"class.draco::PointCloudDecoder"*)** %vtable76, i64 8
  %40 = load i1 (%"class.draco::PointCloudDecoder"*)*, i1 (%"class.draco::PointCloudDecoder"*)** %vfn77, align 4
  %call78 = call zeroext i1 %40(%"class.draco::PointCloudDecoder"* %this1)
  br i1 %call78, label %if.end80, label %if.then79

if.then79:                                        ; preds = %if.end75
  store i1 false, i1* %retval, align 1
  br label %return

if.end80:                                         ; preds = %if.end75
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end80, %if.then79, %if.then74, %if.then33, %if.then16, %if.then3, %if.then
  %41 = load i1, i1* %retval, align 1
  ret i1 %41
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.96"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5beginEv(%"class.std::__2::vector.94"* %this) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__wrap_iter", align 4
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %0, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__begin_, align 4
  %call = call %"class.std::__2::unique_ptr.96"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE11__make_iterEPS6_(%"class.std::__2::vector.94"* %this1, %"class.std::__2::unique_ptr.96"* %1) #9
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %retval, i32 0, i32 0
  store %"class.std::__2::unique_ptr.96"* %call, %"class.std::__2::unique_ptr.96"** %coerce.dive, align 4
  %coerce.dive2 = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %retval, i32 0, i32 0
  %2 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %coerce.dive2, align 4
  ret %"class.std::__2::unique_ptr.96"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.96"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE3endEv(%"class.std::__2::vector.94"* %this) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__wrap_iter", align 4
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %0, i32 0, i32 1
  %1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__end_, align 4
  %call = call %"class.std::__2::unique_ptr.96"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE11__make_iterEPS6_(%"class.std::__2::vector.94"* %this1, %"class.std::__2::unique_ptr.96"* %1) #9
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %retval, i32 0, i32 0
  store %"class.std::__2::unique_ptr.96"* %call, %"class.std::__2::unique_ptr.96"** %coerce.dive, align 4
  %coerce.dive2 = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %retval, i32 0, i32 0
  %2 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %coerce.dive2, align 4
  ret %"class.std::__2::unique_ptr.96"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNSt3__2neIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEEbRKNS_11__wrap_iterIT_EESC_(%"class.std::__2::__wrap_iter"* nonnull align 4 dereferenceable(4) %__x, %"class.std::__2::__wrap_iter"* nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %__x.addr = alloca %"class.std::__2::__wrap_iter"*, align 4
  %__y.addr = alloca %"class.std::__2::__wrap_iter"*, align 4
  store %"class.std::__2::__wrap_iter"* %__x, %"class.std::__2::__wrap_iter"** %__x.addr, align 4
  store %"class.std::__2::__wrap_iter"* %__y, %"class.std::__2::__wrap_iter"** %__y.addr, align 4
  %0 = load %"class.std::__2::__wrap_iter"*, %"class.std::__2::__wrap_iter"** %__x.addr, align 4
  %1 = load %"class.std::__2::__wrap_iter"*, %"class.std::__2::__wrap_iter"** %__y.addr, align 4
  %call = call zeroext i1 @_ZNSt3__2eqIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEES7_EEbRKNS_11__wrap_iterIT_EERKNS8_IT0_EE(%"class.std::__2::__wrap_iter"* nonnull align 4 dereferenceable(4) %0, %"class.std::__2::__wrap_iter"* nonnull align 4 dereferenceable(4) %1) #9
  %lnot = xor i1 %call, true
  ret i1 %lnot
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"* @_ZNKSt3__211__wrap_iterIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEdeEv(%"class.std::__2::__wrap_iter"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__wrap_iter"*, align 4
  store %"class.std::__2::__wrap_iter"* %this, %"class.std::__2::__wrap_iter"** %this.addr, align 4
  %this1 = load %"class.std::__2::__wrap_iter"*, %"class.std::__2::__wrap_iter"** %this.addr, align 4
  %__i = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__i, align 4
  ret %"class.std::__2::unique_ptr.96"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::AttributesDecoderInterface"* @_ZNKSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr.96"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  store %"class.std::__2::unique_ptr.96"* %this, %"class.std::__2::unique_ptr.96"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributesDecoderInterface"** @_ZNKSt3__217__compressed_pairIPN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.97"* %__ptr_) #9
  %0 = load %"class.draco::AttributesDecoderInterface"*, %"class.draco::AttributesDecoderInterface"** %call, align 4
  ret %"class.draco::AttributesDecoderInterface"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__wrap_iter"* @_ZNSt3__211__wrap_iterIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEppEv(%"class.std::__2::__wrap_iter"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__wrap_iter"*, align 4
  store %"class.std::__2::__wrap_iter"* %this, %"class.std::__2::__wrap_iter"** %this.addr, align 4
  %this1 = load %"class.std::__2::__wrap_iter"*, %"class.std::__2::__wrap_iter"** %this.addr, align 4
  %__i = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__i, align 4
  %incdec.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %0, i32 1
  store %"class.std::__2::unique_ptr.96"* %incdec.ptr, %"class.std::__2::unique_ptr.96"** %__i, align 4
  ret %"class.std::__2::__wrap_iter"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEixEm(%"class.std::__2::vector.94"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %0, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__begin_, align 4
  %2 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %1, i32 %2
  ret %"class.std::__2::unique_ptr.96"* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4sizeEv(%"class.std::__2::vector.87"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.87"* %this1 to %"class.std::__2::__vector_base.88"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %0, i32 0, i32 1
  %1 = load i32*, i32** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector.87"* %this1 to %"class.std::__2::__vector_base.88"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %2, i32 0, i32 0
  %3 = load i32*, i32** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint i32* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint i32* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIiNS_9allocatorIiEEE6resizeEm(%"class.std::__2::vector.87"* %this, i32 %__sz) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  %__sz.addr = alloca i32, align 4
  %__cs = alloca i32, align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  store i32 %__sz, i32* %__sz.addr, align 4
  %this1 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4sizeEv(%"class.std::__2::vector.87"* %this1) #9
  store i32 %call, i32* %__cs, align 4
  %0 = load i32, i32* %__cs, align 4
  %1 = load i32, i32* %__sz.addr, align 4
  %cmp = icmp ult i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %__sz.addr, align 4
  %3 = load i32, i32* %__cs, align 4
  %sub = sub i32 %2, %3
  call void @_ZNSt3__26vectorIiNS_9allocatorIiEEE8__appendEm(%"class.std::__2::vector.87"* %this1, i32 %sub)
  br label %if.end4

if.else:                                          ; preds = %entry
  %4 = load i32, i32* %__cs, align 4
  %5 = load i32, i32* %__sz.addr, align 4
  %cmp2 = icmp ugt i32 %4, %5
  br i1 %cmp2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.else
  %6 = bitcast %"class.std::__2::vector.87"* %this1 to %"class.std::__2::__vector_base.88"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %6, i32 0, i32 0
  %7 = load i32*, i32** %__begin_, align 4
  %8 = load i32, i32* %__sz.addr, align 4
  %add.ptr = getelementptr inbounds i32, i32* %7, i32 %8
  call void @_ZNSt3__26vectorIiNS_9allocatorIiEEE17__destruct_at_endEPi(%"class.std::__2::vector.87"* %this1, i32* %add.ptr) #9
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.else
  br label %if.end4

if.end4:                                          ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIiNS_9allocatorIiEEEixEm(%"class.std::__2::vector.87"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.87"* %this1 to %"class.std::__2::__vector_base.88"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %0, i32 0, i32 0
  %1 = load i32*, i32** %__begin_, align 4
  %2 = load i32, i32* %__n.addr, align 4
  %arrayidx = getelementptr inbounds i32, i32* %1, i32 %2
  ret i32* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define hidden zeroext i1 @_ZN5draco17PointCloudDecoder19DecodeAllAttributesEv(%"class.draco::PointCloudDecoder"* %this) unnamed_addr #0 {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::PointCloudDecoder"*, align 4
  %__range1 = alloca %"class.std::__2::vector.94"*, align 4
  %__begin1 = alloca %"class.std::__2::__wrap_iter", align 4
  %__end1 = alloca %"class.std::__2::__wrap_iter", align 4
  %att_dec = alloca %"class.std::__2::unique_ptr.96"*, align 4
  store %"class.draco::PointCloudDecoder"* %this, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::PointCloudDecoder"*, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  %attributes_decoders_ = getelementptr inbounds %"class.draco::PointCloudDecoder", %"class.draco::PointCloudDecoder"* %this1, i32 0, i32 2
  store %"class.std::__2::vector.94"* %attributes_decoders_, %"class.std::__2::vector.94"** %__range1, align 4
  %0 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %__range1, align 4
  %call = call %"class.std::__2::unique_ptr.96"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5beginEv(%"class.std::__2::vector.94"* %0) #9
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %__begin1, i32 0, i32 0
  store %"class.std::__2::unique_ptr.96"* %call, %"class.std::__2::unique_ptr.96"** %coerce.dive, align 4
  %1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %__range1, align 4
  %call2 = call %"class.std::__2::unique_ptr.96"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE3endEv(%"class.std::__2::vector.94"* %1) #9
  %coerce.dive3 = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %__end1, i32 0, i32 0
  store %"class.std::__2::unique_ptr.96"* %call2, %"class.std::__2::unique_ptr.96"** %coerce.dive3, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %call4 = call zeroext i1 @_ZNSt3__2neIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEEbRKNS_11__wrap_iterIT_EESC_(%"class.std::__2::__wrap_iter"* nonnull align 4 dereferenceable(4) %__begin1, %"class.std::__2::__wrap_iter"* nonnull align 4 dereferenceable(4) %__end1) #9
  br i1 %call4, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %call5 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"* @_ZNKSt3__211__wrap_iterIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEdeEv(%"class.std::__2::__wrap_iter"* %__begin1) #9
  store %"class.std::__2::unique_ptr.96"* %call5, %"class.std::__2::unique_ptr.96"** %att_dec, align 4
  %2 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %att_dec, align 4
  %call6 = call %"class.draco::AttributesDecoderInterface"* @_ZNKSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr.96"* %2) #9
  %buffer_ = getelementptr inbounds %"class.draco::PointCloudDecoder", %"class.draco::PointCloudDecoder"* %this1, i32 0, i32 4
  %3 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %buffer_, align 4
  %4 = bitcast %"class.draco::AttributesDecoderInterface"* %call6 to i1 (%"class.draco::AttributesDecoderInterface"*, %"class.draco::DecoderBuffer"*)***
  %vtable = load i1 (%"class.draco::AttributesDecoderInterface"*, %"class.draco::DecoderBuffer"*)**, i1 (%"class.draco::AttributesDecoderInterface"*, %"class.draco::DecoderBuffer"*)*** %4, align 4
  %vfn = getelementptr inbounds i1 (%"class.draco::AttributesDecoderInterface"*, %"class.draco::DecoderBuffer"*)*, i1 (%"class.draco::AttributesDecoderInterface"*, %"class.draco::DecoderBuffer"*)** %vtable, i64 4
  %5 = load i1 (%"class.draco::AttributesDecoderInterface"*, %"class.draco::DecoderBuffer"*)*, i1 (%"class.draco::AttributesDecoderInterface"*, %"class.draco::DecoderBuffer"*)** %vfn, align 4
  %call7 = call zeroext i1 %5(%"class.draco::AttributesDecoderInterface"* %call6, %"class.draco::DecoderBuffer"* %3)
  br i1 %call7, label %if.end, label %if.then

if.then:                                          ; preds = %for.body
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %call8 = call nonnull align 4 dereferenceable(4) %"class.std::__2::__wrap_iter"* @_ZNSt3__211__wrap_iterIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEppEv(%"class.std::__2::__wrap_iter"* %__begin1) #9
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %for.end, %if.then
  %6 = load i1, i1* %retval, align 1
  ret i1 %6
}

; Function Attrs: noinline nounwind optnone
define hidden %"class.draco::PointAttribute"* @_ZN5draco17PointCloudDecoder20GetPortableAttributeEi(%"class.draco::PointCloudDecoder"* %this, i32 %parent_att_id) #0 {
entry:
  %retval = alloca %"class.draco::PointAttribute"*, align 4
  %this.addr = alloca %"class.draco::PointCloudDecoder"*, align 4
  %parent_att_id.addr = alloca i32, align 4
  %parent_att_decoder_id = alloca i32, align 4
  store %"class.draco::PointCloudDecoder"* %this, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  store i32 %parent_att_id, i32* %parent_att_id.addr, align 4
  %this1 = load %"class.draco::PointCloudDecoder"*, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  %0 = load i32, i32* %parent_att_id.addr, align 4
  %cmp = icmp slt i32 %0, 0
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %1 = load i32, i32* %parent_att_id.addr, align 4
  %point_cloud_ = getelementptr inbounds %"class.draco::PointCloudDecoder", %"class.draco::PointCloudDecoder"* %this1, i32 0, i32 1
  %2 = load %"class.draco::PointCloud"*, %"class.draco::PointCloud"** %point_cloud_, align 4
  %call = call i32 @_ZNK5draco10PointCloud14num_attributesEv(%"class.draco::PointCloud"* %2)
  %cmp2 = icmp sge i32 %1, %call
  br i1 %cmp2, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  store %"class.draco::PointAttribute"* null, %"class.draco::PointAttribute"** %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  %attribute_to_decoder_map_ = getelementptr inbounds %"class.draco::PointCloudDecoder", %"class.draco::PointCloudDecoder"* %this1, i32 0, i32 3
  %3 = load i32, i32* %parent_att_id.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__26vectorIiNS_9allocatorIiEEEixEm(%"class.std::__2::vector.87"* %attribute_to_decoder_map_, i32 %3) #9
  %4 = load i32, i32* %call3, align 4
  store i32 %4, i32* %parent_att_decoder_id, align 4
  %attributes_decoders_ = getelementptr inbounds %"class.draco::PointCloudDecoder", %"class.draco::PointCloudDecoder"* %this1, i32 0, i32 2
  %5 = load i32, i32* %parent_att_decoder_id, align 4
  %call4 = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEixEm(%"class.std::__2::vector.94"* %attributes_decoders_, i32 %5) #9
  %call5 = call %"class.draco::AttributesDecoderInterface"* @_ZNKSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEEptEv(%"class.std::__2::unique_ptr.96"* %call4) #9
  %6 = load i32, i32* %parent_att_id.addr, align 4
  %7 = bitcast %"class.draco::AttributesDecoderInterface"* %call5 to %"class.draco::PointAttribute"* (%"class.draco::AttributesDecoderInterface"*, i32)***
  %vtable = load %"class.draco::PointAttribute"* (%"class.draco::AttributesDecoderInterface"*, i32)**, %"class.draco::PointAttribute"* (%"class.draco::AttributesDecoderInterface"*, i32)*** %7, align 4
  %vfn = getelementptr inbounds %"class.draco::PointAttribute"* (%"class.draco::AttributesDecoderInterface"*, i32)*, %"class.draco::PointAttribute"* (%"class.draco::AttributesDecoderInterface"*, i32)** %vtable, i64 8
  %8 = load %"class.draco::PointAttribute"* (%"class.draco::AttributesDecoderInterface"*, i32)*, %"class.draco::PointAttribute"* (%"class.draco::AttributesDecoderInterface"*, i32)** %vfn, align 4
  %call6 = call %"class.draco::PointAttribute"* %8(%"class.draco::AttributesDecoderInterface"* %call5, i32 %6)
  store %"class.draco::PointAttribute"* %call6, %"class.draco::PointAttribute"** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %9 = load %"class.draco::PointAttribute"*, %"class.draco::PointAttribute"** %retval, align 4
  ret %"class.draco::PointAttribute"* %9
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco10PointCloud14num_attributesEv(%"class.draco::PointCloud"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointCloud"*, align 4
  store %"class.draco::PointCloud"* %this, %"class.draco::PointCloud"** %this.addr, align 4
  %this1 = load %"class.draco::PointCloud"*, %"class.draco::PointCloud"** %this.addr, align 4
  %attributes_ = getelementptr inbounds %"class.draco::PointCloud", %"class.draco::PointCloud"* %this1, i32 0, i32 2
  %call = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.51"* %attributes_) #9
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::PointCloudDecoder"* @_ZN5draco17PointCloudDecoderD2Ev(%"class.draco::PointCloudDecoder"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointCloudDecoder"*, align 4
  store %"class.draco::PointCloudDecoder"* %this, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::PointCloudDecoder"*, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  %0 = bitcast %"class.draco::PointCloudDecoder"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [11 x i8*] }, { [11 x i8*] }* @_ZTVN5draco17PointCloudDecoderE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %attribute_to_decoder_map_ = getelementptr inbounds %"class.draco::PointCloudDecoder", %"class.draco::PointCloudDecoder"* %this1, i32 0, i32 3
  %call = call %"class.std::__2::vector.87"* @_ZNSt3__26vectorIiNS_9allocatorIiEEED2Ev(%"class.std::__2::vector.87"* %attribute_to_decoder_map_) #9
  %attributes_decoders_ = getelementptr inbounds %"class.draco::PointCloudDecoder", %"class.draco::PointCloudDecoder"* %this1, i32 0, i32 2
  %call2 = call %"class.std::__2::vector.94"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEED2Ev(%"class.std::__2::vector.94"* %attributes_decoders_) #9
  ret %"class.draco::PointCloudDecoder"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5draco17PointCloudDecoderD0Ev(%"class.draco::PointCloudDecoder"* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointCloudDecoder"*, align 4
  store %"class.draco::PointCloudDecoder"* %this, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::PointCloudDecoder"*, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  call void @llvm.trap() #11
  unreachable
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5draco17PointCloudDecoder15GetGeometryTypeEv(%"class.draco::PointCloudDecoder"* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointCloudDecoder"*, align 4
  store %"class.draco::PointCloudDecoder"* %this, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::PointCloudDecoder"*, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  ret i32 0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco17PointCloudDecoder17InitializeDecoderEv(%"class.draco::PointCloudDecoder"* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointCloudDecoder"*, align 4
  store %"class.draco::PointCloudDecoder"* %this, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::PointCloudDecoder"*, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  ret i1 true
}

declare void @__cxa_pure_virtual() unnamed_addr

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco17PointCloudDecoder18DecodeGeometryDataEv(%"class.draco::PointCloudDecoder"* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointCloudDecoder"*, align 4
  store %"class.draco::PointCloudDecoder"* %this, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::PointCloudDecoder"*, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  ret i1 true
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco17PointCloudDecoder19OnAttributesDecodedEv(%"class.draco::PointCloudDecoder"* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::PointCloudDecoder"*, align 4
  store %"class.draco::PointCloudDecoder"* %this, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  %this1 = load %"class.draco::PointCloudDecoder"*, %"class.draco::PointCloudDecoder"** %this.addr, align 4
  ret i1 true
}

declare %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKS5_(%"class.std::__2::basic_string"* returned, %"class.std::__2::basic_string"* nonnull align 4 dereferenceable(12)) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::Status"* @_ZN5draco6StatusC2ENS0_4CodeE(%"class.draco::Status"* returned %this, i32 %code) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::Status"*, align 4
  %code.addr = alloca i32, align 4
  store %"class.draco::Status"* %this, %"class.draco::Status"** %this.addr, align 4
  store i32 %code, i32* %code.addr, align 4
  %this1 = load %"class.draco::Status"*, %"class.draco::Status"** %this.addr, align 4
  %code_ = getelementptr inbounds %"class.draco::Status", %"class.draco::Status"* %this1, i32 0, i32 0
  %0 = load i32, i32* %code.addr, align 4
  store i32 %0, i32* %code_, align 4
  %error_msg_ = getelementptr inbounds %"class.draco::Status", %"class.draco::Status"* %this1, i32 0, i32 1
  %call = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2Ev(%"class.std::__2::basic_string"* %error_msg_) #9
  ret %"class.draco::Status"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC2Ev(%"class.std::__2::basic_string"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  %ref.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  %ref.tmp2 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::basic_string"* %this1 to %"class.std::__2::__basic_string_common"*
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::__compressed_pair.124"* @_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_EC2INS_18__default_init_tagESA_EEOT_OT0_(%"class.std::__2::__compressed_pair.124"* %__r_, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  call void @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__zeroEv(%"class.std::__2::basic_string"* %this1) #9
  ret %"class.std::__2::basic_string"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.124"* @_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_EC2INS_18__default_init_tagESA_EEOT_OT0_(%"class.std::__2::__compressed_pair.124"* returned %this, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.124"*, align 4
  %__t1.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  %agg.tmp3 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.124"* %this, %"class.std::__2::__compressed_pair.124"** %this.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t1, %"struct.std::__2::__default_init_tag"** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.124"*, %"class.std::__2::__compressed_pair.124"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.124"* %this1 to %"struct.std::__2::__compressed_pair_elem.125"*
  %1 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t1.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %1) #9
  %call2 = call %"struct.std::__2::__compressed_pair_elem.125"* @_ZNSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.125"* %0)
  %2 = bitcast %"class.std::__2::__compressed_pair.124"* %this1 to %"struct.std::__2::__compressed_pair_elem.126"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call4 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #9
  %call5 = call %"struct.std::__2::__compressed_pair_elem.126"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIcEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.126"* %2)
  ret %"class.std::__2::__compressed_pair.124"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__zeroEv(%"class.std::__2::basic_string"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::basic_string"*, align 4
  %__a = alloca [3 x i32]*, align 4
  %__i = alloca i32, align 4
  store %"class.std::__2::basic_string"* %this, %"class.std::__2::basic_string"** %this.addr, align 4
  %this1 = load %"class.std::__2::basic_string"*, %"class.std::__2::basic_string"** %this.addr, align 4
  %__r_ = getelementptr inbounds %"class.std::__2::basic_string", %"class.std::__2::basic_string"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair.124"* %__r_) #9
  %0 = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call, i32 0, i32 0
  %__r = bitcast %union.anon* %0 to %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__raw"*
  %__words = getelementptr inbounds %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__raw", %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__raw"* %__r, i32 0, i32 0
  store [3 x i32]* %__words, [3 x i32]** %__a, align 4
  store i32 0, i32* %__i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %__i, align 4
  %cmp = icmp ult i32 %1, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load [3 x i32]*, [3 x i32]** %__a, align 4
  %3 = load i32, i32* %__i, align 4
  %arrayidx = getelementptr inbounds [3 x i32], [3 x i32]* %2, i32 0, i32 %3
  store i32 0, i32* %arrayidx, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %4 = load i32, i32* %__i, align 4
  %inc = add i32 %4, 1
  store i32 %inc, i32* %__i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  store %"struct.std::__2::__default_init_tag"* %__t, %"struct.std::__2::__default_init_tag"** %__t.addr, align 4
  %0 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t.addr, align 4
  ret %"struct.std::__2::__default_init_tag"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.125"* @_ZNSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.125"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.125"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.125"* %this, %"struct.std::__2::__compressed_pair_elem.125"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.125"*, %"struct.std::__2::__compressed_pair_elem.125"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.125", %"struct.std::__2::__compressed_pair_elem.125"* %this1, i32 0, i32 0
  ret %"struct.std::__2::__compressed_pair_elem.125"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.126"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIcEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.126"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.126"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.126"* %this, %"struct.std::__2::__compressed_pair_elem.126"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.126"*, %"struct.std::__2::__compressed_pair_elem.126"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.126"* %this1 to %"class.std::__2::allocator.127"*
  %call = call %"class.std::__2::allocator.127"* @_ZNSt3__29allocatorIcEC2Ev(%"class.std::__2::allocator.127"* %1) #9
  ret %"struct.std::__2::__compressed_pair_elem.126"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::allocator.127"* @_ZNSt3__29allocatorIcEC2Ev(%"class.std::__2::allocator.127"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.127"*, align 4
  store %"class.std::__2::allocator.127"* %this, %"class.std::__2::allocator.127"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.127"*, %"class.std::__2::allocator.127"** %this.addr, align 4
  ret %"class.std::__2::allocator.127"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNSt3__217__compressed_pairINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repES5_E5firstEv(%"class.std::__2::__compressed_pair.124"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.124"*, align 4
  store %"class.std::__2::__compressed_pair.124"* %this, %"class.std::__2::__compressed_pair.124"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.124"*, %"class.std::__2::__compressed_pair.124"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.124"* %this1 to %"struct.std::__2::__compressed_pair_elem.125"*
  %call = call nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.125"* %0) #9
  ret %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* @_ZNSt3__222__compressed_pair_elemINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5__repELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.125"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.125"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.125"* %this, %"struct.std::__2::__compressed_pair_elem.125"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.125"*, %"struct.std::__2::__compressed_pair_elem.125"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.125", %"struct.std::__2::__compressed_pair_elem.125"* %this1, i32 0, i32 0
  ret %"struct.std::__2::basic_string<char, std::__2::char_traits<char>, std::__2::allocator<char>>::__rep"* %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::Metadata"* @_ZN5draco8MetadataC2Ev(%"class.draco::Metadata"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::Metadata"*, align 4
  store %"class.draco::Metadata"* %this, %"class.draco::Metadata"** %this.addr, align 4
  %this1 = load %"class.draco::Metadata"*, %"class.draco::Metadata"** %this.addr, align 4
  %entries_ = getelementptr inbounds %"class.draco::Metadata", %"class.draco::Metadata"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::unordered_map"* @_ZNSt3__213unordered_mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueENS_4hashIS6_EENS_8equal_toIS6_EENS4_INS_4pairIKS6_S8_EEEEEC2Ev(%"class.std::__2::unordered_map"* %entries_) #9
  %sub_metadatas_ = getelementptr inbounds %"class.draco::Metadata", %"class.draco::Metadata"* %this1, i32 0, i32 1
  %call2 = call %"class.std::__2::unordered_map.17"* @_ZNSt3__213unordered_mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteIS9_EEEENS_4hashIS6_EENS_8equal_toIS6_EENS4_INS_4pairIKS6_SC_EEEEEC2Ev(%"class.std::__2::unordered_map.17"* %sub_metadatas_) #9
  ret %"class.draco::Metadata"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEC2Ev(%"class.std::__2::vector"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call %"class.std::__2::__vector_base"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEC2Ev(%"class.std::__2::__vector_base"* %0) #9
  ret %"class.std::__2::vector"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unordered_map"* @_ZNSt3__213unordered_mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueENS_4hashIS6_EENS_8equal_toIS6_EENS4_INS_4pairIKS6_S8_EEEEEC2Ev(%"class.std::__2::unordered_map"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unordered_map"*, align 4
  store %"class.std::__2::unordered_map"* %this, %"class.std::__2::unordered_map"** %this.addr, align 4
  %this1 = load %"class.std::__2::unordered_map"*, %"class.std::__2::unordered_map"** %this.addr, align 4
  %__table_ = getelementptr inbounds %"class.std::__2::unordered_map", %"class.std::__2::unordered_map"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::__hash_table"* @_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEENS_22__unordered_map_hasherIS7_SA_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SA_NS_8equal_toIS7_EELb1EEENS5_ISA_EEEC2Ev(%"class.std::__2::__hash_table"* %__table_) #9
  ret %"class.std::__2::unordered_map"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unordered_map.17"* @_ZNSt3__213unordered_mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteIS9_EEEENS_4hashIS6_EENS_8equal_toIS6_EENS4_INS_4pairIKS6_SC_EEEEEC2Ev(%"class.std::__2::unordered_map.17"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unordered_map.17"*, align 4
  store %"class.std::__2::unordered_map.17"* %this, %"class.std::__2::unordered_map.17"** %this.addr, align 4
  %this1 = load %"class.std::__2::unordered_map.17"*, %"class.std::__2::unordered_map.17"** %this.addr, align 4
  %__table_ = getelementptr inbounds %"class.std::__2::unordered_map.17", %"class.std::__2::unordered_map.17"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::__hash_table.18"* @_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEEENS_22__unordered_map_hasherIS7_SE_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SE_NS_8equal_toIS7_EELb1EEENS5_ISE_EEEC2Ev(%"class.std::__2::__hash_table.18"* %__table_) #9
  ret %"class.std::__2::unordered_map.17"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__hash_table"* @_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEENS_22__unordered_map_hasherIS7_SA_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SA_NS_8equal_toIS7_EELb1EEENS5_ISA_EEEC2Ev(%"class.std::__2::__hash_table"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__hash_table"*, align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp3 = alloca %"struct.std::__2::__default_init_tag", align 1
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__hash_table"* %this, %"class.std::__2::__hash_table"** %this.addr, align 4
  %this1 = load %"class.std::__2::__hash_table"*, %"class.std::__2::__hash_table"** %this.addr, align 4
  %__bucket_list_ = getelementptr inbounds %"class.std::__2::__hash_table", %"class.std::__2::__hash_table"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::unique_ptr.0"* @_ZNSt3__210unique_ptrIA_PNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISH_EEEEEC2ILb1EvEEv(%"class.std::__2::unique_ptr.0"* %__bucket_list_) #9
  %__p1_ = getelementptr inbounds %"class.std::__2::__hash_table", %"class.std::__2::__hash_table"* %this1, i32 0, i32 1
  %call2 = call %"class.std::__2::__compressed_pair.7"* @_ZNSt3__217__compressed_pairINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS7_ISE_EEEC2ILb1EvEEv(%"class.std::__2::__compressed_pair.7"* %__p1_)
  %__p2_ = getelementptr inbounds %"class.std::__2::__hash_table", %"class.std::__2::__hash_table"* %this1, i32 0, i32 2
  store i32 0, i32* %ref.tmp, align 4
  %call4 = call %"class.std::__2::__compressed_pair.12"* @_ZNSt3__217__compressed_pairImNS_22__unordered_map_hasherINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_17__hash_value_typeIS7_N5draco10EntryValueEEENS_4hashIS7_EELb1EEEEC2IiNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.12"* %__p2_, i32* nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp3)
  %__p3_ = getelementptr inbounds %"class.std::__2::__hash_table", %"class.std::__2::__hash_table"* %this1, i32 0, i32 3
  store float 1.000000e+00, float* %ref.tmp5, align 4
  %call7 = call %"class.std::__2::__compressed_pair.14"* @_ZNSt3__217__compressed_pairIfNS_21__unordered_map_equalINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_17__hash_value_typeIS7_N5draco10EntryValueEEENS_8equal_toIS7_EELb1EEEEC2IfNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.14"* %__p3_, float* nonnull align 4 dereferenceable(4) %ref.tmp5, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp6)
  ret %"class.std::__2::__hash_table"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.0"* @_ZNSt3__210unique_ptrIA_PNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISH_EEEEEC2ILb1EvEEv(%"class.std::__2::unique_ptr.0"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.0"*, align 4
  %ref.tmp = alloca %"struct.std::__2::__hash_node_base"**, align 4
  %ref.tmp2 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::unique_ptr.0"* %this, %"class.std::__2::unique_ptr.0"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.0"*, %"class.std::__2::unique_ptr.0"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.0", %"class.std::__2::unique_ptr.0"* %this1, i32 0, i32 0
  store %"struct.std::__2::__hash_node_base"** null, %"struct.std::__2::__hash_node_base"*** %ref.tmp, align 4
  %call = call %"class.std::__2::__compressed_pair.1"* @_ZNSt3__217__compressed_pairIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISH_EEEEEC2ISI_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.1"* %__ptr_, %"struct.std::__2::__hash_node_base"*** nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  ret %"class.std::__2::unique_ptr.0"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.7"* @_ZNSt3__217__compressed_pairINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS7_ISE_EEEC2ILb1EvEEv(%"class.std::__2::__compressed_pair.7"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.7"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__value_init_tag", align 1
  %agg.tmp2 = alloca %"struct.std::__2::__value_init_tag", align 1
  store %"class.std::__2::__compressed_pair.7"* %this, %"class.std::__2::__compressed_pair.7"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.7"*, %"class.std::__2::__compressed_pair.7"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.7"* %this1 to %"struct.std::__2::__compressed_pair_elem.8"*
  %call = call %"struct.std::__2::__compressed_pair_elem.8"* @_ZNSt3__222__compressed_pair_elemINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEELi0ELb0EEC2ENS_16__value_init_tagE(%"struct.std::__2::__compressed_pair_elem.8"* %0)
  %1 = bitcast %"class.std::__2::__compressed_pair.7"* %this1 to %"struct.std::__2::__compressed_pair_elem.9"*
  %call3 = call %"struct.std::__2::__compressed_pair_elem.9"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEELi1ELb1EEC2ENS_16__value_init_tagE(%"struct.std::__2::__compressed_pair_elem.9"* %1)
  ret %"class.std::__2::__compressed_pair.7"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.12"* @_ZNSt3__217__compressed_pairImNS_22__unordered_map_hasherINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_17__hash_value_typeIS7_N5draco10EntryValueEEENS_4hashIS7_EELb1EEEEC2IiNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.12"* returned %this, i32* nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.12"*, align 4
  %__t1.addr = alloca i32*, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.12"* %this, %"class.std::__2::__compressed_pair.12"** %this.addr, align 4
  store i32* %__t1, i32** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.12"*, %"class.std::__2::__compressed_pair.12"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.12"* %this1 to %"struct.std::__2::__compressed_pair_elem.5"*
  %1 = load i32*, i32** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__27forwardIiEEOT_RNS_16remove_referenceIS1_E4typeE(i32* nonnull align 4 dereferenceable(4) %1) #9
  %call2 = call %"struct.std::__2::__compressed_pair_elem.5"* @_ZNSt3__222__compressed_pair_elemImLi0ELb0EEC2IivEEOT_(%"struct.std::__2::__compressed_pair_elem.5"* %0, i32* nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.12"* %this1 to %"struct.std::__2::__compressed_pair_elem.13"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #9
  %call4 = call %"struct.std::__2::__compressed_pair_elem.13"* @_ZNSt3__222__compressed_pair_elemINS_22__unordered_map_hasherINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_17__hash_value_typeIS7_N5draco10EntryValueEEENS_4hashIS7_EELb1EEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.13"* %2)
  ret %"class.std::__2::__compressed_pair.12"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.14"* @_ZNSt3__217__compressed_pairIfNS_21__unordered_map_equalINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_17__hash_value_typeIS7_N5draco10EntryValueEEENS_8equal_toIS7_EELb1EEEEC2IfNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.14"* returned %this, float* nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.14"*, align 4
  %__t1.addr = alloca float*, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.14"* %this, %"class.std::__2::__compressed_pair.14"** %this.addr, align 4
  store float* %__t1, float** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.14"*, %"class.std::__2::__compressed_pair.14"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.14"* %this1 to %"struct.std::__2::__compressed_pair_elem.15"*
  %1 = load float*, float** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNSt3__27forwardIfEEOT_RNS_16remove_referenceIS1_E4typeE(float* nonnull align 4 dereferenceable(4) %1) #9
  %call2 = call %"struct.std::__2::__compressed_pair_elem.15"* @_ZNSt3__222__compressed_pair_elemIfLi0ELb0EEC2IfvEEOT_(%"struct.std::__2::__compressed_pair_elem.15"* %0, float* nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.14"* %this1 to %"struct.std::__2::__compressed_pair_elem.16"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #9
  %call4 = call %"struct.std::__2::__compressed_pair_elem.16"* @_ZNSt3__222__compressed_pair_elemINS_21__unordered_map_equalINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_17__hash_value_typeIS7_N5draco10EntryValueEEENS_8equal_toIS7_EELb1EEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.16"* %2)
  ret %"class.std::__2::__compressed_pair.14"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.1"* @_ZNSt3__217__compressed_pairIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISH_EEEEEC2ISI_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.1"* returned %this, %"struct.std::__2::__hash_node_base"*** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.1"*, align 4
  %__t1.addr = alloca %"struct.std::__2::__hash_node_base"***, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.1"* %this, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  store %"struct.std::__2::__hash_node_base"*** %__t1, %"struct.std::__2::__hash_node_base"**** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.1"*, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.1"* %this1 to %"struct.std::__2::__compressed_pair_elem.2"*
  %1 = load %"struct.std::__2::__hash_node_base"***, %"struct.std::__2::__hash_node_base"**** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base"*** @_ZNSt3__27forwardIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEEEEOT_RNS_16remove_referenceISJ_E4typeE(%"struct.std::__2::__hash_node_base"*** nonnull align 4 dereferenceable(4) %1) #9
  %call2 = call %"struct.std::__2::__compressed_pair_elem.2"* @_ZNSt3__222__compressed_pair_elemIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEELi0ELb0EEC2ISI_vEEOT_(%"struct.std::__2::__compressed_pair_elem.2"* %0, %"struct.std::__2::__hash_node_base"*** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.1"* %this1 to i8*
  %3 = getelementptr inbounds i8, i8* %2, i32 4
  %4 = bitcast i8* %3 to %"struct.std::__2::__compressed_pair_elem.3"*
  %5 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %5) #9
  %call4 = call %"struct.std::__2::__compressed_pair_elem.3"* @_ZNSt3__222__compressed_pair_elemINS_25__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS2_IcEEEEN5draco10EntryValueEEEPvEEEEEEEELi1ELb0EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.3"* %4)
  ret %"class.std::__2::__compressed_pair.1"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base"*** @_ZNSt3__27forwardIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEEEEOT_RNS_16remove_referenceISJ_E4typeE(%"struct.std::__2::__hash_node_base"*** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"struct.std::__2::__hash_node_base"***, align 4
  store %"struct.std::__2::__hash_node_base"*** %__t, %"struct.std::__2::__hash_node_base"**** %__t.addr, align 4
  %0 = load %"struct.std::__2::__hash_node_base"***, %"struct.std::__2::__hash_node_base"**** %__t.addr, align 4
  ret %"struct.std::__2::__hash_node_base"*** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.2"* @_ZNSt3__222__compressed_pair_elemIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEELi0ELb0EEC2ISI_vEEOT_(%"struct.std::__2::__compressed_pair_elem.2"* returned %this, %"struct.std::__2::__hash_node_base"*** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.2"*, align 4
  %__u.addr = alloca %"struct.std::__2::__hash_node_base"***, align 4
  store %"struct.std::__2::__compressed_pair_elem.2"* %this, %"struct.std::__2::__compressed_pair_elem.2"** %this.addr, align 4
  store %"struct.std::__2::__hash_node_base"*** %__u, %"struct.std::__2::__hash_node_base"**** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.2"*, %"struct.std::__2::__compressed_pair_elem.2"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.2", %"struct.std::__2::__compressed_pair_elem.2"* %this1, i32 0, i32 0
  %0 = load %"struct.std::__2::__hash_node_base"***, %"struct.std::__2::__hash_node_base"**** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base"*** @_ZNSt3__27forwardIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEEEEOT_RNS_16remove_referenceISJ_E4typeE(%"struct.std::__2::__hash_node_base"*** nonnull align 4 dereferenceable(4) %0) #9
  %1 = load %"struct.std::__2::__hash_node_base"**, %"struct.std::__2::__hash_node_base"*** %call, align 4
  store %"struct.std::__2::__hash_node_base"** %1, %"struct.std::__2::__hash_node_base"*** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.2"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.3"* @_ZNSt3__222__compressed_pair_elemINS_25__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS2_IcEEEEN5draco10EntryValueEEEPvEEEEEEEELi1ELb0EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.3"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.3"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.3"* %this, %"struct.std::__2::__compressed_pair_elem.3"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.3"*, %"struct.std::__2::__compressed_pair_elem.3"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.3", %"struct.std::__2::__compressed_pair_elem.3"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::__bucket_list_deallocator"* @_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEEC2Ev(%"class.std::__2::__bucket_list_deallocator"* %__value_) #9
  ret %"struct.std::__2::__compressed_pair_elem.3"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__bucket_list_deallocator"* @_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEEC2Ev(%"class.std::__2::__bucket_list_deallocator"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__bucket_list_deallocator"*, align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp2 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__bucket_list_deallocator"* %this, %"class.std::__2::__bucket_list_deallocator"** %this.addr, align 4
  %this1 = load %"class.std::__2::__bucket_list_deallocator"*, %"class.std::__2::__bucket_list_deallocator"** %this.addr, align 4
  %__data_ = getelementptr inbounds %"class.std::__2::__bucket_list_deallocator", %"class.std::__2::__bucket_list_deallocator"* %this1, i32 0, i32 0
  store i32 0, i32* %ref.tmp, align 4
  %call = call %"class.std::__2::__compressed_pair.4"* @_ZNSt3__217__compressed_pairImNS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEEC2IiNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.4"* %__data_, i32* nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  ret %"class.std::__2::__bucket_list_deallocator"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.4"* @_ZNSt3__217__compressed_pairImNS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEEC2IiNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.4"* returned %this, i32* nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.4"*, align 4
  %__t1.addr = alloca i32*, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.4"* %this, %"class.std::__2::__compressed_pair.4"** %this.addr, align 4
  store i32* %__t1, i32** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.4"*, %"class.std::__2::__compressed_pair.4"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.4"* %this1 to %"struct.std::__2::__compressed_pair_elem.5"*
  %1 = load i32*, i32** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__27forwardIiEEOT_RNS_16remove_referenceIS1_E4typeE(i32* nonnull align 4 dereferenceable(4) %1) #9
  %call2 = call %"struct.std::__2::__compressed_pair_elem.5"* @_ZNSt3__222__compressed_pair_elemImLi0ELb0EEC2IivEEOT_(%"struct.std::__2::__compressed_pair_elem.5"* %0, i32* nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.4"* %this1 to %"struct.std::__2::__compressed_pair_elem.6"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #9
  %call4 = call %"struct.std::__2::__compressed_pair_elem.6"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.6"* %2)
  ret %"class.std::__2::__compressed_pair.4"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__27forwardIiEEOT_RNS_16remove_referenceIS1_E4typeE(i32* nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca i32*, align 4
  store i32* %__t, i32** %__t.addr, align 4
  %0 = load i32*, i32** %__t.addr, align 4
  ret i32* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.5"* @_ZNSt3__222__compressed_pair_elemImLi0ELb0EEC2IivEEOT_(%"struct.std::__2::__compressed_pair_elem.5"* returned %this, i32* nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.5"*, align 4
  %__u.addr = alloca i32*, align 4
  store %"struct.std::__2::__compressed_pair_elem.5"* %this, %"struct.std::__2::__compressed_pair_elem.5"** %this.addr, align 4
  store i32* %__u, i32** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.5"*, %"struct.std::__2::__compressed_pair_elem.5"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.5", %"struct.std::__2::__compressed_pair_elem.5"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__27forwardIiEEOT_RNS_16remove_referenceIS1_E4typeE(i32* nonnull align 4 dereferenceable(4) %0) #9
  %1 = load i32, i32* %call, align 4
  store i32 %1, i32* %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.5"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.6"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.6"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.6"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.6"* %this, %"struct.std::__2::__compressed_pair_elem.6"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.6"*, %"struct.std::__2::__compressed_pair_elem.6"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.6"* %this1 to %"class.std::__2::allocator"*
  %call = call %"class.std::__2::allocator"* @_ZNSt3__29allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEEN5draco10EntryValueEEEPvEEEEEC2Ev(%"class.std::__2::allocator"* %1) #9
  ret %"struct.std::__2::__compressed_pair_elem.6"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::allocator"* @_ZNSt3__29allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEEN5draco10EntryValueEEEPvEEEEEC2Ev(%"class.std::__2::allocator"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  ret %"class.std::__2::allocator"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.8"* @_ZNSt3__222__compressed_pair_elemINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEELi0ELb0EEC2ENS_16__value_init_tagE(%"struct.std::__2::__compressed_pair_elem.8"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__value_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.8"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.8"* %this, %"struct.std::__2::__compressed_pair_elem.8"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.8"*, %"struct.std::__2::__compressed_pair_elem.8"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.8", %"struct.std::__2::__compressed_pair_elem.8"* %this1, i32 0, i32 0
  %call = call %"struct.std::__2::__hash_node_base"* @_ZNSt3__216__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEC2Ev(%"struct.std::__2::__hash_node_base"* %__value_) #9
  ret %"struct.std::__2::__compressed_pair_elem.8"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.9"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEELi1ELb1EEC2ENS_16__value_init_tagE(%"struct.std::__2::__compressed_pair_elem.9"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__value_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.9"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.9"* %this, %"struct.std::__2::__compressed_pair_elem.9"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.9"*, %"struct.std::__2::__compressed_pair_elem.9"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.9"* %this1 to %"class.std::__2::allocator.10"*
  %call = call %"class.std::__2::allocator.10"* @_ZNSt3__29allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEEN5draco10EntryValueEEEPvEEEC2Ev(%"class.std::__2::allocator.10"* %1) #9
  ret %"struct.std::__2::__compressed_pair_elem.9"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__hash_node_base"* @_ZNSt3__216__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEC2Ev(%"struct.std::__2::__hash_node_base"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__hash_node_base"*, align 4
  store %"struct.std::__2::__hash_node_base"* %this, %"struct.std::__2::__hash_node_base"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__hash_node_base"*, %"struct.std::__2::__hash_node_base"** %this.addr, align 4
  %__next_ = getelementptr inbounds %"struct.std::__2::__hash_node_base", %"struct.std::__2::__hash_node_base"* %this1, i32 0, i32 0
  store %"struct.std::__2::__hash_node_base"* null, %"struct.std::__2::__hash_node_base"** %__next_, align 4
  ret %"struct.std::__2::__hash_node_base"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::allocator.10"* @_ZNSt3__29allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEEN5draco10EntryValueEEEPvEEEC2Ev(%"class.std::__2::allocator.10"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.10"*, align 4
  store %"class.std::__2::allocator.10"* %this, %"class.std::__2::allocator.10"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.10"*, %"class.std::__2::allocator.10"** %this.addr, align 4
  ret %"class.std::__2::allocator.10"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.13"* @_ZNSt3__222__compressed_pair_elemINS_22__unordered_map_hasherINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_17__hash_value_typeIS7_N5draco10EntryValueEEENS_4hashIS7_EELb1EEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.13"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.13"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.13"* %this, %"struct.std::__2::__compressed_pair_elem.13"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.13"*, %"struct.std::__2::__compressed_pair_elem.13"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.13"* %this1 to %"class.std::__2::__unordered_map_hasher"*
  %call = call %"class.std::__2::__unordered_map_hasher"* @_ZNSt3__222__unordered_map_hasherINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_17__hash_value_typeIS6_N5draco10EntryValueEEENS_4hashIS6_EELb1EEC2Ev(%"class.std::__2::__unordered_map_hasher"* %1) #9
  ret %"struct.std::__2::__compressed_pair_elem.13"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__unordered_map_hasher"* @_ZNSt3__222__unordered_map_hasherINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_17__hash_value_typeIS6_N5draco10EntryValueEEENS_4hashIS6_EELb1EEC2Ev(%"class.std::__2::__unordered_map_hasher"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__unordered_map_hasher"*, align 4
  store %"class.std::__2::__unordered_map_hasher"* %this, %"class.std::__2::__unordered_map_hasher"** %this.addr, align 4
  %this1 = load %"class.std::__2::__unordered_map_hasher"*, %"class.std::__2::__unordered_map_hasher"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__unordered_map_hasher"* %this1 to %"struct.std::__2::hash"*
  ret %"class.std::__2::__unordered_map_hasher"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNSt3__27forwardIfEEOT_RNS_16remove_referenceIS1_E4typeE(float* nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca float*, align 4
  store float* %__t, float** %__t.addr, align 4
  %0 = load float*, float** %__t.addr, align 4
  ret float* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.15"* @_ZNSt3__222__compressed_pair_elemIfLi0ELb0EEC2IfvEEOT_(%"struct.std::__2::__compressed_pair_elem.15"* returned %this, float* nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.15"*, align 4
  %__u.addr = alloca float*, align 4
  store %"struct.std::__2::__compressed_pair_elem.15"* %this, %"struct.std::__2::__compressed_pair_elem.15"** %this.addr, align 4
  store float* %__u, float** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.15"*, %"struct.std::__2::__compressed_pair_elem.15"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.15", %"struct.std::__2::__compressed_pair_elem.15"* %this1, i32 0, i32 0
  %0 = load float*, float** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNSt3__27forwardIfEEOT_RNS_16remove_referenceIS1_E4typeE(float* nonnull align 4 dereferenceable(4) %0) #9
  %1 = load float, float* %call, align 4
  store float %1, float* %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.15"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.16"* @_ZNSt3__222__compressed_pair_elemINS_21__unordered_map_equalINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_17__hash_value_typeIS7_N5draco10EntryValueEEENS_8equal_toIS7_EELb1EEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.16"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.16"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.16"* %this, %"struct.std::__2::__compressed_pair_elem.16"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.16"*, %"struct.std::__2::__compressed_pair_elem.16"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.16"* %this1 to %"class.std::__2::__unordered_map_equal"*
  %call = call %"class.std::__2::__unordered_map_equal"* @_ZNSt3__221__unordered_map_equalINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_17__hash_value_typeIS6_N5draco10EntryValueEEENS_8equal_toIS6_EELb1EEC2Ev(%"class.std::__2::__unordered_map_equal"* %1) #9
  ret %"struct.std::__2::__compressed_pair_elem.16"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__unordered_map_equal"* @_ZNSt3__221__unordered_map_equalINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_17__hash_value_typeIS6_N5draco10EntryValueEEENS_8equal_toIS6_EELb1EEC2Ev(%"class.std::__2::__unordered_map_equal"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__unordered_map_equal"*, align 4
  store %"class.std::__2::__unordered_map_equal"* %this, %"class.std::__2::__unordered_map_equal"** %this.addr, align 4
  %this1 = load %"class.std::__2::__unordered_map_equal"*, %"class.std::__2::__unordered_map_equal"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__unordered_map_equal"* %this1 to %"struct.std::__2::equal_to"*
  ret %"class.std::__2::__unordered_map_equal"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__hash_table.18"* @_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEEENS_22__unordered_map_hasherIS7_SE_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SE_NS_8equal_toIS7_EELb1EEENS5_ISE_EEEC2Ev(%"class.std::__2::__hash_table.18"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__hash_table.18"*, align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp3 = alloca %"struct.std::__2::__default_init_tag", align 1
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__hash_table.18"* %this, %"class.std::__2::__hash_table.18"** %this.addr, align 4
  %this1 = load %"class.std::__2::__hash_table.18"*, %"class.std::__2::__hash_table.18"** %this.addr, align 4
  %__bucket_list_ = getelementptr inbounds %"class.std::__2::__hash_table.18", %"class.std::__2::__hash_table.18"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::unique_ptr.19"* @_ZNSt3__210unique_ptrIA_PNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS0_IN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISK_EEEEEC2ILb1EvEEv(%"class.std::__2::unique_ptr.19"* %__bucket_list_) #9
  %__p1_ = getelementptr inbounds %"class.std::__2::__hash_table.18", %"class.std::__2::__hash_table.18"* %this1, i32 0, i32 1
  %call2 = call %"class.std::__2::__compressed_pair.29"* @_ZNSt3__217__compressed_pairINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEENS7_ISI_EEEC2ILb1EvEEv(%"class.std::__2::__compressed_pair.29"* %__p1_)
  %__p2_ = getelementptr inbounds %"class.std::__2::__hash_table.18", %"class.std::__2::__hash_table.18"* %this1, i32 0, i32 2
  store i32 0, i32* %ref.tmp, align 4
  %call4 = call %"class.std::__2::__compressed_pair.34"* @_ZNSt3__217__compressed_pairImNS_22__unordered_map_hasherINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_17__hash_value_typeIS7_NS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEENS_4hashIS7_EELb1EEEEC2IiNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.34"* %__p2_, i32* nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp3)
  %__p3_ = getelementptr inbounds %"class.std::__2::__hash_table.18", %"class.std::__2::__hash_table.18"* %this1, i32 0, i32 3
  store float 1.000000e+00, float* %ref.tmp5, align 4
  %call7 = call %"class.std::__2::__compressed_pair.37"* @_ZNSt3__217__compressed_pairIfNS_21__unordered_map_equalINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_17__hash_value_typeIS7_NS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEENS_8equal_toIS7_EELb1EEEEC2IfNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.37"* %__p3_, float* nonnull align 4 dereferenceable(4) %ref.tmp5, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp6)
  ret %"class.std::__2::__hash_table.18"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.19"* @_ZNSt3__210unique_ptrIA_PNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS0_IN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISK_EEEEEC2ILb1EvEEv(%"class.std::__2::unique_ptr.19"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.19"*, align 4
  %ref.tmp = alloca %"struct.std::__2::__hash_node_base.22"**, align 4
  %ref.tmp2 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::unique_ptr.19"* %this, %"class.std::__2::unique_ptr.19"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.19"*, %"class.std::__2::unique_ptr.19"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.19", %"class.std::__2::unique_ptr.19"* %this1, i32 0, i32 0
  store %"struct.std::__2::__hash_node_base.22"** null, %"struct.std::__2::__hash_node_base.22"*** %ref.tmp, align 4
  %call = call %"class.std::__2::__compressed_pair.20"* @_ZNSt3__217__compressed_pairIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISL_EEEEEC2ISM_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.20"* %__ptr_, %"struct.std::__2::__hash_node_base.22"*** nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  ret %"class.std::__2::unique_ptr.19"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.29"* @_ZNSt3__217__compressed_pairINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEENS7_ISI_EEEC2ILb1EvEEv(%"class.std::__2::__compressed_pair.29"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.29"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__value_init_tag", align 1
  %agg.tmp2 = alloca %"struct.std::__2::__value_init_tag", align 1
  store %"class.std::__2::__compressed_pair.29"* %this, %"class.std::__2::__compressed_pair.29"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.29"*, %"class.std::__2::__compressed_pair.29"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.29"* %this1 to %"struct.std::__2::__compressed_pair_elem.30"*
  %call = call %"struct.std::__2::__compressed_pair_elem.30"* @_ZNSt3__222__compressed_pair_elemINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEELi0ELb0EEC2ENS_16__value_init_tagE(%"struct.std::__2::__compressed_pair_elem.30"* %0)
  %1 = bitcast %"class.std::__2::__compressed_pair.29"* %this1 to %"struct.std::__2::__compressed_pair_elem.31"*
  %call3 = call %"struct.std::__2::__compressed_pair_elem.31"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEELi1ELb1EEC2ENS_16__value_init_tagE(%"struct.std::__2::__compressed_pair_elem.31"* %1)
  ret %"class.std::__2::__compressed_pair.29"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.34"* @_ZNSt3__217__compressed_pairImNS_22__unordered_map_hasherINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_17__hash_value_typeIS7_NS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEENS_4hashIS7_EELb1EEEEC2IiNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.34"* returned %this, i32* nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.34"*, align 4
  %__t1.addr = alloca i32*, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.34"* %this, %"class.std::__2::__compressed_pair.34"** %this.addr, align 4
  store i32* %__t1, i32** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.34"*, %"class.std::__2::__compressed_pair.34"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.34"* %this1 to %"struct.std::__2::__compressed_pair_elem.5"*
  %1 = load i32*, i32** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__27forwardIiEEOT_RNS_16remove_referenceIS1_E4typeE(i32* nonnull align 4 dereferenceable(4) %1) #9
  %call2 = call %"struct.std::__2::__compressed_pair_elem.5"* @_ZNSt3__222__compressed_pair_elemImLi0ELb0EEC2IivEEOT_(%"struct.std::__2::__compressed_pair_elem.5"* %0, i32* nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.34"* %this1 to %"struct.std::__2::__compressed_pair_elem.35"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #9
  %call4 = call %"struct.std::__2::__compressed_pair_elem.35"* @_ZNSt3__222__compressed_pair_elemINS_22__unordered_map_hasherINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_17__hash_value_typeIS7_NS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEENS_4hashIS7_EELb1EEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.35"* %2)
  ret %"class.std::__2::__compressed_pair.34"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.37"* @_ZNSt3__217__compressed_pairIfNS_21__unordered_map_equalINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_17__hash_value_typeIS7_NS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEENS_8equal_toIS7_EELb1EEEEC2IfNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.37"* returned %this, float* nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.37"*, align 4
  %__t1.addr = alloca float*, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.37"* %this, %"class.std::__2::__compressed_pair.37"** %this.addr, align 4
  store float* %__t1, float** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.37"*, %"class.std::__2::__compressed_pair.37"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.37"* %this1 to %"struct.std::__2::__compressed_pair_elem.15"*
  %1 = load float*, float** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNSt3__27forwardIfEEOT_RNS_16remove_referenceIS1_E4typeE(float* nonnull align 4 dereferenceable(4) %1) #9
  %call2 = call %"struct.std::__2::__compressed_pair_elem.15"* @_ZNSt3__222__compressed_pair_elemIfLi0ELb0EEC2IfvEEOT_(%"struct.std::__2::__compressed_pair_elem.15"* %0, float* nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.37"* %this1 to %"struct.std::__2::__compressed_pair_elem.38"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #9
  %call4 = call %"struct.std::__2::__compressed_pair_elem.38"* @_ZNSt3__222__compressed_pair_elemINS_21__unordered_map_equalINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_17__hash_value_typeIS7_NS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEENS_8equal_toIS7_EELb1EEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.38"* %2)
  ret %"class.std::__2::__compressed_pair.37"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.20"* @_ZNSt3__217__compressed_pairIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISL_EEEEEC2ISM_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.20"* returned %this, %"struct.std::__2::__hash_node_base.22"*** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.20"*, align 4
  %__t1.addr = alloca %"struct.std::__2::__hash_node_base.22"***, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.20"* %this, %"class.std::__2::__compressed_pair.20"** %this.addr, align 4
  store %"struct.std::__2::__hash_node_base.22"*** %__t1, %"struct.std::__2::__hash_node_base.22"**** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.20"*, %"class.std::__2::__compressed_pair.20"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.20"* %this1 to %"struct.std::__2::__compressed_pair_elem.21"*
  %1 = load %"struct.std::__2::__hash_node_base.22"***, %"struct.std::__2::__hash_node_base.22"**** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base.22"*** @_ZNSt3__27forwardIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEOT_RNS_16remove_referenceISN_E4typeE(%"struct.std::__2::__hash_node_base.22"*** nonnull align 4 dereferenceable(4) %1) #9
  %call2 = call %"struct.std::__2::__compressed_pair_elem.21"* @_ZNSt3__222__compressed_pair_elemIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEELi0ELb0EEC2ISM_vEEOT_(%"struct.std::__2::__compressed_pair_elem.21"* %0, %"struct.std::__2::__hash_node_base.22"*** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.20"* %this1 to i8*
  %3 = getelementptr inbounds i8, i8* %2, i32 4
  %4 = bitcast i8* %3 to %"struct.std::__2::__compressed_pair_elem.23"*
  %5 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %5) #9
  %call4 = call %"struct.std::__2::__compressed_pair_elem.23"* @_ZNSt3__222__compressed_pair_elemINS_25__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS2_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISD_EEEEEEPvEEEEEEEELi1ELb0EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.23"* %4)
  ret %"class.std::__2::__compressed_pair.20"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base.22"*** @_ZNSt3__27forwardIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEOT_RNS_16remove_referenceISN_E4typeE(%"struct.std::__2::__hash_node_base.22"*** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"struct.std::__2::__hash_node_base.22"***, align 4
  store %"struct.std::__2::__hash_node_base.22"*** %__t, %"struct.std::__2::__hash_node_base.22"**** %__t.addr, align 4
  %0 = load %"struct.std::__2::__hash_node_base.22"***, %"struct.std::__2::__hash_node_base.22"**** %__t.addr, align 4
  ret %"struct.std::__2::__hash_node_base.22"*** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.21"* @_ZNSt3__222__compressed_pair_elemIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEELi0ELb0EEC2ISM_vEEOT_(%"struct.std::__2::__compressed_pair_elem.21"* returned %this, %"struct.std::__2::__hash_node_base.22"*** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.21"*, align 4
  %__u.addr = alloca %"struct.std::__2::__hash_node_base.22"***, align 4
  store %"struct.std::__2::__compressed_pair_elem.21"* %this, %"struct.std::__2::__compressed_pair_elem.21"** %this.addr, align 4
  store %"struct.std::__2::__hash_node_base.22"*** %__u, %"struct.std::__2::__hash_node_base.22"**** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.21"*, %"struct.std::__2::__compressed_pair_elem.21"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.21", %"struct.std::__2::__compressed_pair_elem.21"* %this1, i32 0, i32 0
  %0 = load %"struct.std::__2::__hash_node_base.22"***, %"struct.std::__2::__hash_node_base.22"**** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base.22"*** @_ZNSt3__27forwardIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEOT_RNS_16remove_referenceISN_E4typeE(%"struct.std::__2::__hash_node_base.22"*** nonnull align 4 dereferenceable(4) %0) #9
  %1 = load %"struct.std::__2::__hash_node_base.22"**, %"struct.std::__2::__hash_node_base.22"*** %call, align 4
  store %"struct.std::__2::__hash_node_base.22"** %1, %"struct.std::__2::__hash_node_base.22"*** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.21"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.23"* @_ZNSt3__222__compressed_pair_elemINS_25__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS2_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISD_EEEEEEPvEEEEEEEELi1ELb0EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.23"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.23"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.23"* %this, %"struct.std::__2::__compressed_pair_elem.23"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.23"*, %"struct.std::__2::__compressed_pair_elem.23"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.23", %"struct.std::__2::__compressed_pair_elem.23"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::__bucket_list_deallocator.24"* @_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEEC2Ev(%"class.std::__2::__bucket_list_deallocator.24"* %__value_) #9
  ret %"struct.std::__2::__compressed_pair_elem.23"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__bucket_list_deallocator.24"* @_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEEC2Ev(%"class.std::__2::__bucket_list_deallocator.24"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__bucket_list_deallocator.24"*, align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp2 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__bucket_list_deallocator.24"* %this, %"class.std::__2::__bucket_list_deallocator.24"** %this.addr, align 4
  %this1 = load %"class.std::__2::__bucket_list_deallocator.24"*, %"class.std::__2::__bucket_list_deallocator.24"** %this.addr, align 4
  %__data_ = getelementptr inbounds %"class.std::__2::__bucket_list_deallocator.24", %"class.std::__2::__bucket_list_deallocator.24"* %this1, i32 0, i32 0
  store i32 0, i32* %ref.tmp, align 4
  %call = call %"class.std::__2::__compressed_pair.25"* @_ZNSt3__217__compressed_pairImNS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEEC2IiNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.25"* %__data_, i32* nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  ret %"class.std::__2::__bucket_list_deallocator.24"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.25"* @_ZNSt3__217__compressed_pairImNS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEEC2IiNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.25"* returned %this, i32* nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.25"*, align 4
  %__t1.addr = alloca i32*, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.25"* %this, %"class.std::__2::__compressed_pair.25"** %this.addr, align 4
  store i32* %__t1, i32** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.25"*, %"class.std::__2::__compressed_pair.25"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.25"* %this1 to %"struct.std::__2::__compressed_pair_elem.5"*
  %1 = load i32*, i32** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__27forwardIiEEOT_RNS_16remove_referenceIS1_E4typeE(i32* nonnull align 4 dereferenceable(4) %1) #9
  %call2 = call %"struct.std::__2::__compressed_pair_elem.5"* @_ZNSt3__222__compressed_pair_elemImLi0ELb0EEC2IivEEOT_(%"struct.std::__2::__compressed_pair_elem.5"* %0, i32* nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.25"* %this1 to %"struct.std::__2::__compressed_pair_elem.26"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #9
  %call4 = call %"struct.std::__2::__compressed_pair_elem.26"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.26"* %2)
  ret %"class.std::__2::__compressed_pair.25"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.26"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.26"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.26"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.26"* %this, %"struct.std::__2::__compressed_pair_elem.26"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.26"*, %"struct.std::__2::__compressed_pair_elem.26"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.26"* %this1 to %"class.std::__2::allocator.27"*
  %call = call %"class.std::__2::allocator.27"* @_ZNSt3__29allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEEEC2Ev(%"class.std::__2::allocator.27"* %1) #9
  ret %"struct.std::__2::__compressed_pair_elem.26"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::allocator.27"* @_ZNSt3__29allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEEEC2Ev(%"class.std::__2::allocator.27"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.27"*, align 4
  store %"class.std::__2::allocator.27"* %this, %"class.std::__2::allocator.27"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.27"*, %"class.std::__2::allocator.27"** %this.addr, align 4
  ret %"class.std::__2::allocator.27"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.30"* @_ZNSt3__222__compressed_pair_elemINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEELi0ELb0EEC2ENS_16__value_init_tagE(%"struct.std::__2::__compressed_pair_elem.30"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__value_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.30"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.30"* %this, %"struct.std::__2::__compressed_pair_elem.30"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.30"*, %"struct.std::__2::__compressed_pair_elem.30"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.30", %"struct.std::__2::__compressed_pair_elem.30"* %this1, i32 0, i32 0
  %call = call %"struct.std::__2::__hash_node_base.22"* @_ZNSt3__216__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEC2Ev(%"struct.std::__2::__hash_node_base.22"* %__value_) #9
  ret %"struct.std::__2::__compressed_pair_elem.30"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.31"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEELi1ELb1EEC2ENS_16__value_init_tagE(%"struct.std::__2::__compressed_pair_elem.31"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__value_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.31"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.31"* %this, %"struct.std::__2::__compressed_pair_elem.31"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.31"*, %"struct.std::__2::__compressed_pair_elem.31"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.31"* %this1 to %"class.std::__2::allocator.32"*
  %call = call %"class.std::__2::allocator.32"* @_ZNSt3__29allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEEEPvEEEC2Ev(%"class.std::__2::allocator.32"* %1) #9
  ret %"struct.std::__2::__compressed_pair_elem.31"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__hash_node_base.22"* @_ZNSt3__216__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEC2Ev(%"struct.std::__2::__hash_node_base.22"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__hash_node_base.22"*, align 4
  store %"struct.std::__2::__hash_node_base.22"* %this, %"struct.std::__2::__hash_node_base.22"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__hash_node_base.22"*, %"struct.std::__2::__hash_node_base.22"** %this.addr, align 4
  %__next_ = getelementptr inbounds %"struct.std::__2::__hash_node_base.22", %"struct.std::__2::__hash_node_base.22"* %this1, i32 0, i32 0
  store %"struct.std::__2::__hash_node_base.22"* null, %"struct.std::__2::__hash_node_base.22"** %__next_, align 4
  ret %"struct.std::__2::__hash_node_base.22"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::allocator.32"* @_ZNSt3__29allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEEEPvEEEC2Ev(%"class.std::__2::allocator.32"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.32"*, align 4
  store %"class.std::__2::allocator.32"* %this, %"class.std::__2::allocator.32"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.32"*, %"class.std::__2::allocator.32"** %this.addr, align 4
  ret %"class.std::__2::allocator.32"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.35"* @_ZNSt3__222__compressed_pair_elemINS_22__unordered_map_hasherINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_17__hash_value_typeIS7_NS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEENS_4hashIS7_EELb1EEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.35"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.35"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.35"* %this, %"struct.std::__2::__compressed_pair_elem.35"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.35"*, %"struct.std::__2::__compressed_pair_elem.35"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.35"* %this1 to %"class.std::__2::__unordered_map_hasher.36"*
  %call = call %"class.std::__2::__unordered_map_hasher.36"* @_ZNSt3__222__unordered_map_hasherINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_17__hash_value_typeIS6_NS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEEENS_4hashIS6_EELb1EEC2Ev(%"class.std::__2::__unordered_map_hasher.36"* %1) #9
  ret %"struct.std::__2::__compressed_pair_elem.35"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__unordered_map_hasher.36"* @_ZNSt3__222__unordered_map_hasherINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_17__hash_value_typeIS6_NS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEEENS_4hashIS6_EELb1EEC2Ev(%"class.std::__2::__unordered_map_hasher.36"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__unordered_map_hasher.36"*, align 4
  store %"class.std::__2::__unordered_map_hasher.36"* %this, %"class.std::__2::__unordered_map_hasher.36"** %this.addr, align 4
  %this1 = load %"class.std::__2::__unordered_map_hasher.36"*, %"class.std::__2::__unordered_map_hasher.36"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__unordered_map_hasher.36"* %this1 to %"struct.std::__2::hash"*
  ret %"class.std::__2::__unordered_map_hasher.36"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.38"* @_ZNSt3__222__compressed_pair_elemINS_21__unordered_map_equalINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_17__hash_value_typeIS7_NS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEENS_8equal_toIS7_EELb1EEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.38"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.38"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.38"* %this, %"struct.std::__2::__compressed_pair_elem.38"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.38"*, %"struct.std::__2::__compressed_pair_elem.38"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.38"* %this1 to %"class.std::__2::__unordered_map_equal.39"*
  %call = call %"class.std::__2::__unordered_map_equal.39"* @_ZNSt3__221__unordered_map_equalINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_17__hash_value_typeIS6_NS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEEENS_8equal_toIS6_EELb1EEC2Ev(%"class.std::__2::__unordered_map_equal.39"* %1) #9
  ret %"struct.std::__2::__compressed_pair_elem.38"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__unordered_map_equal.39"* @_ZNSt3__221__unordered_map_equalINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_17__hash_value_typeIS6_NS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEEENS_8equal_toIS6_EELb1EEC2Ev(%"class.std::__2::__unordered_map_equal.39"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__unordered_map_equal.39"*, align 4
  store %"class.std::__2::__unordered_map_equal.39"* %this, %"class.std::__2::__unordered_map_equal.39"** %this.addr, align 4
  %this1 = load %"class.std::__2::__unordered_map_equal.39"*, %"class.std::__2::__unordered_map_equal.39"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__unordered_map_equal.39"* %this1 to %"struct.std::__2::equal_to"*
  ret %"class.std::__2::__unordered_map_equal.39"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEC2Ev(%"class.std::__2::__vector_base"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  %ref.tmp = alloca i8*, align 4
  %ref.tmp2 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__vector_base"* %this1 to %"class.std::__2::__vector_base_common"*
  %call = call %"class.std::__2::__vector_base_common"* @_ZNSt3__220__vector_base_commonILb1EEC2Ev(%"class.std::__2::__vector_base_common"* %0)
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  store %"class.std::__2::unique_ptr.40"* null, %"class.std::__2::unique_ptr.40"** %__begin_, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 1
  store %"class.std::__2::unique_ptr.40"* null, %"class.std::__2::unique_ptr.40"** %__end_, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  store i8* null, i8** %ref.tmp, align 4
  %call3 = call %"class.std::__2::__compressed_pair.44"* @_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEC2IDnNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.44"* %__end_cap_, i8** nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  ret %"class.std::__2::__vector_base"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base_common"* @_ZNSt3__220__vector_base_commonILb1EEC2Ev(%"class.std::__2::__vector_base_common"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base_common"*, align 4
  store %"class.std::__2::__vector_base_common"* %this, %"class.std::__2::__vector_base_common"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base_common"*, %"class.std::__2::__vector_base_common"** %this.addr, align 4
  ret %"class.std::__2::__vector_base_common"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.44"* @_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEC2IDnNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.44"* returned %this, i8** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.44"*, align 4
  %__t1.addr = alloca i8**, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.44"* %this, %"class.std::__2::__compressed_pair.44"** %this.addr, align 4
  store i8** %__t1, i8*** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.44"*, %"class.std::__2::__compressed_pair.44"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.44"* %this1 to %"struct.std::__2::__compressed_pair_elem.45"*
  %1 = load i8**, i8*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %1) #9
  %call2 = call %"struct.std::__2::__compressed_pair_elem.45"* @_ZNSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEELi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.45"* %0, i8** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.44"* %this1 to %"struct.std::__2::__compressed_pair_elem.46"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #9
  %call4 = call %"struct.std::__2::__compressed_pair_elem.46"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.46"* %2)
  ret %"class.std::__2::__compressed_pair.44"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca i8**, align 4
  store i8** %__t, i8*** %__t.addr, align 4
  %0 = load i8**, i8*** %__t.addr, align 4
  ret i8** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.45"* @_ZNSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEELi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.45"* returned %this, i8** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.45"*, align 4
  %__u.addr = alloca i8**, align 4
  store %"struct.std::__2::__compressed_pair_elem.45"* %this, %"struct.std::__2::__compressed_pair_elem.45"** %this.addr, align 4
  store i8** %__u, i8*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.45"*, %"struct.std::__2::__compressed_pair_elem.45"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.45", %"struct.std::__2::__compressed_pair_elem.45"* %this1, i32 0, i32 0
  %0 = load i8**, i8*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %0) #9
  store %"class.std::__2::unique_ptr.40"* null, %"class.std::__2::unique_ptr.40"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.45"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.46"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.46"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.46"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.46"* %this, %"struct.std::__2::__compressed_pair_elem.46"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.46"*, %"struct.std::__2::__compressed_pair_elem.46"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.46"* %this1 to %"class.std::__2::allocator.47"*
  %call = call %"class.std::__2::allocator.47"* @_ZNSt3__29allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEC2Ev(%"class.std::__2::allocator.47"* %1) #9
  ret %"struct.std::__2::__compressed_pair_elem.46"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::allocator.47"* @_ZNSt3__29allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEC2Ev(%"class.std::__2::allocator.47"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.47"*, align 4
  store %"class.std::__2::allocator.47"* %this, %"class.std::__2::allocator.47"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.47"*, %"class.std::__2::allocator.47"** %this.addr, align 4
  ret %"class.std::__2::allocator.47"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr"* @_ZNSt3__210unique_ptrIN5draco16GeometryMetadataENS_14default_deleteIS2_EEEaSEOS5_(%"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"* nonnull align 4 dereferenceable(4) %__u) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  %__u.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  store %"class.std::__2::unique_ptr"* %__u, %"class.std::__2::unique_ptr"** %__u.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %0 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %__u.addr, align 4
  %call = call %"class.draco::GeometryMetadata"* @_ZNSt3__210unique_ptrIN5draco16GeometryMetadataENS_14default_deleteIS2_EEE7releaseEv(%"class.std::__2::unique_ptr"* %0) #9
  call void @_ZNSt3__210unique_ptrIN5draco16GeometryMetadataENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr"* %this1, %"class.draco::GeometryMetadata"* %call) #9
  %1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %__u.addr, align 4
  %call2 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.50"* @_ZNSt3__210unique_ptrIN5draco16GeometryMetadataENS_14default_deleteIS2_EEE11get_deleterEv(%"class.std::__2::unique_ptr"* %1) #9
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.50"* @_ZNSt3__27forwardINS_14default_deleteIN5draco16GeometryMetadataEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.50"* nonnull align 1 dereferenceable(1) %call2) #9
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call4 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.50"* @_ZNSt3__217__compressed_pairIPN5draco16GeometryMetadataENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair"* %__ptr_) #9
  ret %"class.std::__2::unique_ptr"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__210unique_ptrIN5draco16GeometryMetadataENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr"* %this, %"class.draco::GeometryMetadata"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  %__p.addr = alloca %"class.draco::GeometryMetadata"*, align 4
  %__tmp = alloca %"class.draco::GeometryMetadata"*, align 4
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  store %"class.draco::GeometryMetadata"* %__p, %"class.draco::GeometryMetadata"** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::GeometryMetadata"** @_ZNSt3__217__compressed_pairIPN5draco16GeometryMetadataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair"* %__ptr_) #9
  %0 = load %"class.draco::GeometryMetadata"*, %"class.draco::GeometryMetadata"** %call, align 4
  store %"class.draco::GeometryMetadata"* %0, %"class.draco::GeometryMetadata"** %__tmp, align 4
  %1 = load %"class.draco::GeometryMetadata"*, %"class.draco::GeometryMetadata"** %__p.addr, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::GeometryMetadata"** @_ZNSt3__217__compressed_pairIPN5draco16GeometryMetadataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair"* %__ptr_2) #9
  store %"class.draco::GeometryMetadata"* %1, %"class.draco::GeometryMetadata"** %call3, align 4
  %2 = load %"class.draco::GeometryMetadata"*, %"class.draco::GeometryMetadata"** %__tmp, align 4
  %tobool = icmp ne %"class.draco::GeometryMetadata"* %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__ptr_4 = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call5 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.50"* @_ZNSt3__217__compressed_pairIPN5draco16GeometryMetadataENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair"* %__ptr_4) #9
  %3 = load %"class.draco::GeometryMetadata"*, %"class.draco::GeometryMetadata"** %__tmp, align 4
  call void @_ZNKSt3__214default_deleteIN5draco16GeometryMetadataEEclEPS2_(%"struct.std::__2::default_delete.50"* %call5, %"class.draco::GeometryMetadata"* %3) #9
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::GeometryMetadata"* @_ZNSt3__210unique_ptrIN5draco16GeometryMetadataENS_14default_deleteIS2_EEE7releaseEv(%"class.std::__2::unique_ptr"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  %__t = alloca %"class.draco::GeometryMetadata"*, align 4
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::GeometryMetadata"** @_ZNSt3__217__compressed_pairIPN5draco16GeometryMetadataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair"* %__ptr_) #9
  %0 = load %"class.draco::GeometryMetadata"*, %"class.draco::GeometryMetadata"** %call, align 4
  store %"class.draco::GeometryMetadata"* %0, %"class.draco::GeometryMetadata"** %__t, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::GeometryMetadata"** @_ZNSt3__217__compressed_pairIPN5draco16GeometryMetadataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair"* %__ptr_2) #9
  store %"class.draco::GeometryMetadata"* null, %"class.draco::GeometryMetadata"** %call3, align 4
  %1 = load %"class.draco::GeometryMetadata"*, %"class.draco::GeometryMetadata"** %__t, align 4
  ret %"class.draco::GeometryMetadata"* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.50"* @_ZNSt3__27forwardINS_14default_deleteIN5draco16GeometryMetadataEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.50"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"struct.std::__2::default_delete.50"*, align 4
  store %"struct.std::__2::default_delete.50"* %__t, %"struct.std::__2::default_delete.50"** %__t.addr, align 4
  %0 = load %"struct.std::__2::default_delete.50"*, %"struct.std::__2::default_delete.50"** %__t.addr, align 4
  ret %"struct.std::__2::default_delete.50"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.50"* @_ZNSt3__210unique_ptrIN5draco16GeometryMetadataENS_14default_deleteIS2_EEE11get_deleterEv(%"class.std::__2::unique_ptr"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr"*, align 4
  store %"class.std::__2::unique_ptr"* %this, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr"*, %"class.std::__2::unique_ptr"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr", %"class.std::__2::unique_ptr"* %this1, i32 0, i32 0
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.50"* @_ZNSt3__217__compressed_pairIPN5draco16GeometryMetadataENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair"* %__ptr_) #9
  ret %"struct.std::__2::default_delete.50"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.50"* @_ZNSt3__217__compressed_pairIPN5draco16GeometryMetadataENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem.49"*
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.50"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco16GeometryMetadataEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.49"* %0) #9
  ret %"struct.std::__2::default_delete.50"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::GeometryMetadata"** @_ZNSt3__217__compressed_pairIPN5draco16GeometryMetadataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::GeometryMetadata"** @_ZNSt3__222__compressed_pair_elemIPN5draco16GeometryMetadataELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %0) #9
  ret %"class.draco::GeometryMetadata"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__214default_deleteIN5draco16GeometryMetadataEEclEPS2_(%"struct.std::__2::default_delete.50"* %this, %"class.draco::GeometryMetadata"* %__ptr) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::default_delete.50"*, align 4
  %__ptr.addr = alloca %"class.draco::GeometryMetadata"*, align 4
  store %"struct.std::__2::default_delete.50"* %this, %"struct.std::__2::default_delete.50"** %this.addr, align 4
  store %"class.draco::GeometryMetadata"* %__ptr, %"class.draco::GeometryMetadata"** %__ptr.addr, align 4
  %this1 = load %"struct.std::__2::default_delete.50"*, %"struct.std::__2::default_delete.50"** %this.addr, align 4
  %0 = load %"class.draco::GeometryMetadata"*, %"class.draco::GeometryMetadata"** %__ptr.addr, align 4
  %isnull = icmp eq %"class.draco::GeometryMetadata"* %0, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %entry
  %call = call %"class.draco::GeometryMetadata"* @_ZN5draco16GeometryMetadataD2Ev(%"class.draco::GeometryMetadata"* %0) #9
  %1 = bitcast %"class.draco::GeometryMetadata"* %0 to i8*
  call void @_ZdlPv(i8* %1) #12
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::GeometryMetadata"** @_ZNSt3__222__compressed_pair_elemIPN5draco16GeometryMetadataELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem"*, align 4
  store %"struct.std::__2::__compressed_pair_elem"* %this, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem"*, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem"* %this1, i32 0, i32 0
  ret %"class.draco::GeometryMetadata"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::GeometryMetadata"* @_ZN5draco16GeometryMetadataD2Ev(%"class.draco::GeometryMetadata"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::GeometryMetadata"*, align 4
  store %"class.draco::GeometryMetadata"* %this, %"class.draco::GeometryMetadata"** %this.addr, align 4
  %this1 = load %"class.draco::GeometryMetadata"*, %"class.draco::GeometryMetadata"** %this.addr, align 4
  %att_metadatas_ = getelementptr inbounds %"class.draco::GeometryMetadata", %"class.draco::GeometryMetadata"* %this1, i32 0, i32 1
  %call = call %"class.std::__2::vector"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEED2Ev(%"class.std::__2::vector"* %att_metadatas_) #9
  %0 = bitcast %"class.draco::GeometryMetadata"* %this1 to %"class.draco::Metadata"*
  %call2 = call %"class.draco::Metadata"* @_ZN5draco8MetadataD2Ev(%"class.draco::Metadata"* %0) #9
  ret %"class.draco::GeometryMetadata"* %this1
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #5

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEED2Ev(%"class.std::__2::vector"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  call void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__annotate_deleteEv(%"class.std::__2::vector"* %this1) #9
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call %"class.std::__2::__vector_base"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEED2Ev(%"class.std::__2::__vector_base"* %0) #9
  ret %"class.std::__2::vector"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__annotate_deleteEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %call = call %"class.std::__2::unique_ptr.40"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector"* %this1) #9
  %0 = bitcast %"class.std::__2::unique_ptr.40"* %call to i8*
  %call2 = call %"class.std::__2::unique_ptr.40"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector"* %this1) #9
  %call3 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector"* %this1) #9
  %add.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.40", %"class.std::__2::unique_ptr.40"* %call2, i32 %call3
  %1 = bitcast %"class.std::__2::unique_ptr.40"* %add.ptr to i8*
  %call4 = call %"class.std::__2::unique_ptr.40"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector"* %this1) #9
  %call5 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector"* %this1) #9
  %add.ptr6 = getelementptr inbounds %"class.std::__2::unique_ptr.40", %"class.std::__2::unique_ptr.40"* %call4, i32 %call5
  %2 = bitcast %"class.std::__2::unique_ptr.40"* %add.ptr6 to i8*
  %call7 = call %"class.std::__2::unique_ptr.40"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector"* %this1) #9
  %call8 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector"* %this1) #9
  %add.ptr9 = getelementptr inbounds %"class.std::__2::unique_ptr.40", %"class.std::__2::unique_ptr.40"* %call7, i32 %call8
  %3 = bitcast %"class.std::__2::unique_ptr.40"* %add.ptr9 to i8*
  call void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE31__annotate_contiguous_containerEPKvSB_SB_SB_(%"class.std::__2::vector"* %this1, i8* %0, i8* %1, i8* %2, i8* %3) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEED2Ev(%"class.std::__2::__vector_base"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__vector_base"*, align 4
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  store %"class.std::__2::__vector_base"* %this1, %"class.std::__2::__vector_base"** %retval, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__begin_, align 4
  %cmp = icmp ne %"class.std::__2::unique_ptr.40"* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5clearEv(%"class.std::__2::__vector_base"* %this1) #9
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.47"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base"* %this1) #9
  %__begin_2 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__begin_2, align 4
  %call3 = call i32 @_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::__vector_base"* %this1) #9
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEEE10deallocateERS8_PS7_m(%"class.std::__2::allocator.47"* nonnull align 1 dereferenceable(1) %call, %"class.std::__2::unique_ptr.40"* %1, i32 %call3) #9
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %retval, align 4
  ret %"class.std::__2::__vector_base"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE31__annotate_contiguous_containerEPKvSB_SB_SB_(%"class.std::__2::vector"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.40"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__begin_, align 4
  %call = call %"class.std::__2::unique_ptr.40"* @_ZNSt3__212__to_addressINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEEPT_S8_(%"class.std::__2::unique_ptr.40"* %1) #9
  ret %"class.std::__2::unique_ptr.40"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call i32 @_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::__vector_base"* %0) #9
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 1
  %1 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %2, i32 0, i32 0
  %3 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.std::__2::unique_ptr.40"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.std::__2::unique_ptr.40"* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.40"* @_ZNSt3__212__to_addressINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEEPT_S8_(%"class.std::__2::unique_ptr.40"* %__p) #0 comdat {
entry:
  %__p.addr = alloca %"class.std::__2::unique_ptr.40"*, align 4
  store %"class.std::__2::unique_ptr.40"* %__p, %"class.std::__2::unique_ptr.40"** %__p.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__p.addr, align 4
  ret %"class.std::__2::unique_ptr.40"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.40"** @_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE9__end_capEv(%"class.std::__2::__vector_base"* %this1) #9
  %0 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.std::__2::unique_ptr.40"* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.std::__2::unique_ptr.40"* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.40"** @_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE9__end_capEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.40"** @_ZNKSt3__217__compressed_pairIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.44"* %__end_cap_) #9
  ret %"class.std::__2::unique_ptr.40"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.40"** @_ZNKSt3__217__compressed_pairIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.44"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.44"*, align 4
  store %"class.std::__2::__compressed_pair.44"* %this, %"class.std::__2::__compressed_pair.44"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.44"*, %"class.std::__2::__compressed_pair.44"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.44"* %this1 to %"struct.std::__2::__compressed_pair_elem.45"*
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.40"** @_ZNKSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.45"* %0) #9
  ret %"class.std::__2::unique_ptr.40"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.40"** @_ZNKSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.45"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.45"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.45"* %this, %"struct.std::__2::__compressed_pair_elem.45"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.45"*, %"struct.std::__2::__compressed_pair_elem.45"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.45", %"struct.std::__2::__compressed_pair_elem.45"* %this1, i32 0, i32 0
  ret %"class.std::__2::unique_ptr.40"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5clearEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__begin_, align 4
  call void @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__destruct_at_endEPS6_(%"class.std::__2::__vector_base"* %this1, %"class.std::__2::unique_ptr.40"* %0) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEEE10deallocateERS8_PS7_m(%"class.std::__2::allocator.47"* nonnull align 1 dereferenceable(1) %__a, %"class.std::__2::unique_ptr.40"* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.47"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.40"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.47"* %__a, %"class.std::__2::allocator.47"** %__a.addr, align 4
  store %"class.std::__2::unique_ptr.40"* %__p, %"class.std::__2::unique_ptr.40"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.47"*, %"class.std::__2::allocator.47"** %__a.addr, align 4
  %1 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEE10deallocateEPS6_m(%"class.std::__2::allocator.47"* %0, %"class.std::__2::unique_ptr.40"* %1, i32 %2) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.47"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.47"* @_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE6secondEv(%"class.std::__2::__compressed_pair.44"* %__end_cap_) #9
  ret %"class.std::__2::allocator.47"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__destruct_at_endEPS6_(%"class.std::__2::__vector_base"* %this, %"class.std::__2::unique_ptr.40"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  %__new_last.addr = alloca %"class.std::__2::unique_ptr.40"*, align 4
  %__soon_to_be_end = alloca %"class.std::__2::unique_ptr.40"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.40"* %__new_last, %"class.std::__2::unique_ptr.40"** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 1
  %0 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__end_, align 4
  store %"class.std::__2::unique_ptr.40"* %0, %"class.std::__2::unique_ptr.40"** %__soon_to_be_end, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__new_last.addr, align 4
  %2 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__soon_to_be_end, align 4
  %cmp = icmp ne %"class.std::__2::unique_ptr.40"* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.47"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base"* %this1) #9
  %3 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__soon_to_be_end, align 4
  %incdec.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.40", %"class.std::__2::unique_ptr.40"* %3, i32 -1
  store %"class.std::__2::unique_ptr.40"* %incdec.ptr, %"class.std::__2::unique_ptr.40"** %__soon_to_be_end, align 4
  %call2 = call %"class.std::__2::unique_ptr.40"* @_ZNSt3__212__to_addressINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEEEPT_S8_(%"class.std::__2::unique_ptr.40"* %incdec.ptr) #9
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEEE7destroyIS7_EEvRS8_PT_(%"class.std::__2::allocator.47"* nonnull align 1 dereferenceable(1) %call, %"class.std::__2::unique_ptr.40"* %call2)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %4 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__new_last.addr, align 4
  %__end_3 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 1
  store %"class.std::__2::unique_ptr.40"* %4, %"class.std::__2::unique_ptr.40"** %__end_3, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEEE7destroyIS7_EEvRS8_PT_(%"class.std::__2::allocator.47"* nonnull align 1 dereferenceable(1) %__a, %"class.std::__2::unique_ptr.40"* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.47"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.40"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy", align 1
  store %"class.std::__2::allocator.47"* %__a, %"class.std::__2::allocator.47"** %__a.addr, align 4
  store %"class.std::__2::unique_ptr.40"* %__p, %"class.std::__2::unique_ptr.40"** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.47"*, %"class.std::__2::allocator.47"** %__a.addr, align 4
  %2 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEEE9__destroyIS7_EEvNS_17integral_constantIbLb1EEERS8_PT_(%"class.std::__2::allocator.47"* nonnull align 1 dereferenceable(1) %1, %"class.std::__2::unique_ptr.40"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEEE9__destroyIS7_EEvNS_17integral_constantIbLb1EEERS8_PT_(%"class.std::__2::allocator.47"* nonnull align 1 dereferenceable(1) %__a, %"class.std::__2::unique_ptr.40"* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.47"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.40"*, align 4
  store %"class.std::__2::allocator.47"* %__a, %"class.std::__2::allocator.47"** %__a.addr, align 4
  store %"class.std::__2::unique_ptr.40"* %__p, %"class.std::__2::unique_ptr.40"** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.47"*, %"class.std::__2::allocator.47"** %__a.addr, align 4
  %2 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__p.addr, align 4
  call void @_ZNSt3__29allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEE7destroyEPS6_(%"class.std::__2::allocator.47"* %1, %"class.std::__2::unique_ptr.40"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEE7destroyEPS6_(%"class.std::__2::allocator.47"* %this, %"class.std::__2::unique_ptr.40"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.47"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.40"*, align 4
  store %"class.std::__2::allocator.47"* %this, %"class.std::__2::allocator.47"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.40"* %__p, %"class.std::__2::unique_ptr.40"** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.47"*, %"class.std::__2::allocator.47"** %this.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__p.addr, align 4
  %call = call %"class.std::__2::unique_ptr.40"* @_ZNSt3__210unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.40"* %0) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.40"* @_ZNSt3__210unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.40"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.40"*, align 4
  store %"class.std::__2::unique_ptr.40"* %this, %"class.std::__2::unique_ptr.40"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %this.addr, align 4
  call void @_ZNSt3__210unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.40"* %this1, %"class.draco::AttributeMetadata"* null) #9
  ret %"class.std::__2::unique_ptr.40"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__210unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.40"* %this, %"class.draco::AttributeMetadata"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.40"*, align 4
  %__p.addr = alloca %"class.draco::AttributeMetadata"*, align 4
  %__tmp = alloca %"class.draco::AttributeMetadata"*, align 4
  store %"class.std::__2::unique_ptr.40"* %this, %"class.std::__2::unique_ptr.40"** %this.addr, align 4
  store %"class.draco::AttributeMetadata"* %__p, %"class.draco::AttributeMetadata"** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.40", %"class.std::__2::unique_ptr.40"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributeMetadata"** @_ZNSt3__217__compressed_pairIPN5draco17AttributeMetadataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.41"* %__ptr_) #9
  %0 = load %"class.draco::AttributeMetadata"*, %"class.draco::AttributeMetadata"** %call, align 4
  store %"class.draco::AttributeMetadata"* %0, %"class.draco::AttributeMetadata"** %__tmp, align 4
  %1 = load %"class.draco::AttributeMetadata"*, %"class.draco::AttributeMetadata"** %__p.addr, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr.40", %"class.std::__2::unique_ptr.40"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::AttributeMetadata"** @_ZNSt3__217__compressed_pairIPN5draco17AttributeMetadataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.41"* %__ptr_2) #9
  store %"class.draco::AttributeMetadata"* %1, %"class.draco::AttributeMetadata"** %call3, align 4
  %2 = load %"class.draco::AttributeMetadata"*, %"class.draco::AttributeMetadata"** %__tmp, align 4
  %tobool = icmp ne %"class.draco::AttributeMetadata"* %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__ptr_4 = getelementptr inbounds %"class.std::__2::unique_ptr.40", %"class.std::__2::unique_ptr.40"* %this1, i32 0, i32 0
  %call5 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete"* @_ZNSt3__217__compressed_pairIPN5draco17AttributeMetadataENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.41"* %__ptr_4) #9
  %3 = load %"class.draco::AttributeMetadata"*, %"class.draco::AttributeMetadata"** %__tmp, align 4
  call void @_ZNKSt3__214default_deleteIN5draco17AttributeMetadataEEclEPS2_(%"struct.std::__2::default_delete"* %call5, %"class.draco::AttributeMetadata"* %3) #9
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::AttributeMetadata"** @_ZNSt3__217__compressed_pairIPN5draco17AttributeMetadataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.41"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.41"*, align 4
  store %"class.std::__2::__compressed_pair.41"* %this, %"class.std::__2::__compressed_pair.41"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.41"*, %"class.std::__2::__compressed_pair.41"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.41"* %this1 to %"struct.std::__2::__compressed_pair_elem.42"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributeMetadata"** @_ZNSt3__222__compressed_pair_elemIPN5draco17AttributeMetadataELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.42"* %0) #9
  ret %"class.draco::AttributeMetadata"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete"* @_ZNSt3__217__compressed_pairIPN5draco17AttributeMetadataENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.41"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.41"*, align 4
  store %"class.std::__2::__compressed_pair.41"* %this, %"class.std::__2::__compressed_pair.41"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.41"*, %"class.std::__2::__compressed_pair.41"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.41"* %this1 to %"struct.std::__2::__compressed_pair_elem.43"*
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco17AttributeMetadataEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.43"* %0) #9
  ret %"struct.std::__2::default_delete"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__214default_deleteIN5draco17AttributeMetadataEEclEPS2_(%"struct.std::__2::default_delete"* %this, %"class.draco::AttributeMetadata"* %__ptr) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::default_delete"*, align 4
  %__ptr.addr = alloca %"class.draco::AttributeMetadata"*, align 4
  store %"struct.std::__2::default_delete"* %this, %"struct.std::__2::default_delete"** %this.addr, align 4
  store %"class.draco::AttributeMetadata"* %__ptr, %"class.draco::AttributeMetadata"** %__ptr.addr, align 4
  %this1 = load %"struct.std::__2::default_delete"*, %"struct.std::__2::default_delete"** %this.addr, align 4
  %0 = load %"class.draco::AttributeMetadata"*, %"class.draco::AttributeMetadata"** %__ptr.addr, align 4
  %isnull = icmp eq %"class.draco::AttributeMetadata"* %0, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %entry
  %call = call %"class.draco::AttributeMetadata"* @_ZN5draco17AttributeMetadataD2Ev(%"class.draco::AttributeMetadata"* %0) #9
  %1 = bitcast %"class.draco::AttributeMetadata"* %0 to i8*
  call void @_ZdlPv(i8* %1) #12
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::AttributeMetadata"** @_ZNSt3__222__compressed_pair_elemIPN5draco17AttributeMetadataELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.42"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.42"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.42"* %this, %"struct.std::__2::__compressed_pair_elem.42"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.42"*, %"struct.std::__2::__compressed_pair_elem.42"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.42", %"struct.std::__2::__compressed_pair_elem.42"* %this1, i32 0, i32 0
  ret %"class.draco::AttributeMetadata"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco17AttributeMetadataEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.43"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.43"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.43"* %this, %"struct.std::__2::__compressed_pair_elem.43"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.43"*, %"struct.std::__2::__compressed_pair_elem.43"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.43"* %this1 to %"struct.std::__2::default_delete"*
  ret %"struct.std::__2::default_delete"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::AttributeMetadata"* @_ZN5draco17AttributeMetadataD2Ev(%"class.draco::AttributeMetadata"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::AttributeMetadata"*, align 4
  store %"class.draco::AttributeMetadata"* %this, %"class.draco::AttributeMetadata"** %this.addr, align 4
  %this1 = load %"class.draco::AttributeMetadata"*, %"class.draco::AttributeMetadata"** %this.addr, align 4
  %0 = bitcast %"class.draco::AttributeMetadata"* %this1 to %"class.draco::Metadata"*
  %call = call %"class.draco::Metadata"* @_ZN5draco8MetadataD2Ev(%"class.draco::Metadata"* %0) #9
  ret %"class.draco::AttributeMetadata"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEEE10deallocateEPS6_m(%"class.std::__2::allocator.47"* %this, %"class.std::__2::unique_ptr.40"* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.47"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.40"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.47"* %this, %"class.std::__2::allocator.47"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.40"* %__p, %"class.std::__2::unique_ptr.40"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.47"*, %"class.std::__2::allocator.47"** %this.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.40"*, %"class.std::__2::unique_ptr.40"** %__p.addr, align 4
  %1 = bitcast %"class.std::__2::unique_ptr.40"* %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %__ptr, i32 %__size, i32 %__align) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  %1 = load i32, i32* %__size.addr, align 4
  %2 = load i32, i32* %__align.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm(i8* %0, i32 %1, i32 %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm(i8* %__ptr, i32 %__size, i32 %__align) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  %1 = load i32, i32* %__size.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm(i8* %0, i32 %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm(i8* %__ptr, i32 %__size) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller9__do_callEPv(i8* %0)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller9__do_callEPv(i8* %__ptr) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  call void @_ZdlPv(i8* %0) #12
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.47"* @_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE6secondEv(%"class.std::__2::__compressed_pair.44"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.44"*, align 4
  store %"class.std::__2::__compressed_pair.44"* %this, %"class.std::__2::__compressed_pair.44"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.44"*, %"class.std::__2::__compressed_pair.44"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.44"* %this1 to %"struct.std::__2::__compressed_pair_elem.46"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.47"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.46"* %0) #9
  ret %"class.std::__2::allocator.47"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.47"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_10unique_ptrIN5draco17AttributeMetadataENS_14default_deleteIS4_EEEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.46"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.46"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.46"* %this, %"struct.std::__2::__compressed_pair_elem.46"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.46"*, %"struct.std::__2::__compressed_pair_elem.46"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.46"* %this1 to %"class.std::__2::allocator.47"*
  ret %"class.std::__2::allocator.47"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unordered_map.17"* @_ZNSt3__213unordered_mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteIS9_EEEENS_4hashIS6_EENS_8equal_toIS6_EENS4_INS_4pairIKS6_SC_EEEEED2Ev(%"class.std::__2::unordered_map.17"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unordered_map.17"*, align 4
  store %"class.std::__2::unordered_map.17"* %this, %"class.std::__2::unordered_map.17"** %this.addr, align 4
  %this1 = load %"class.std::__2::unordered_map.17"*, %"class.std::__2::unordered_map.17"** %this.addr, align 4
  %__table_ = getelementptr inbounds %"class.std::__2::unordered_map.17", %"class.std::__2::unordered_map.17"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::__hash_table.18"* @_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEEENS_22__unordered_map_hasherIS7_SE_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SE_NS_8equal_toIS7_EELb1EEENS5_ISE_EEED2Ev(%"class.std::__2::__hash_table.18"* %__table_) #9
  ret %"class.std::__2::unordered_map.17"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unordered_map"* @_ZNSt3__213unordered_mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueENS_4hashIS6_EENS_8equal_toIS6_EENS4_INS_4pairIKS6_S8_EEEEED2Ev(%"class.std::__2::unordered_map"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unordered_map"*, align 4
  store %"class.std::__2::unordered_map"* %this, %"class.std::__2::unordered_map"** %this.addr, align 4
  %this1 = load %"class.std::__2::unordered_map"*, %"class.std::__2::unordered_map"** %this.addr, align 4
  %__table_ = getelementptr inbounds %"class.std::__2::unordered_map", %"class.std::__2::unordered_map"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::__hash_table"* @_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEENS_22__unordered_map_hasherIS7_SA_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SA_NS_8equal_toIS7_EELb1EEENS5_ISA_EEED2Ev(%"class.std::__2::__hash_table"* %__table_) #9
  ret %"class.std::__2::unordered_map"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__hash_table.18"* @_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEEENS_22__unordered_map_hasherIS7_SE_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SE_NS_8equal_toIS7_EELb1EEENS5_ISE_EEED2Ev(%"class.std::__2::__hash_table.18"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__hash_table.18"*, align 4
  store %"class.std::__2::__hash_table.18"* %this, %"class.std::__2::__hash_table.18"** %this.addr, align 4
  %this1 = load %"class.std::__2::__hash_table.18"*, %"class.std::__2::__hash_table.18"** %this.addr, align 4
  %__p1_ = getelementptr inbounds %"class.std::__2::__hash_table.18", %"class.std::__2::__hash_table.18"* %this1, i32 0, i32 1
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base.22"* @_ZNSt3__217__compressed_pairINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEENS7_ISI_EEE5firstEv(%"class.std::__2::__compressed_pair.29"* %__p1_) #9
  %__next_ = getelementptr inbounds %"struct.std::__2::__hash_node_base.22", %"struct.std::__2::__hash_node_base.22"* %call, i32 0, i32 0
  %0 = load %"struct.std::__2::__hash_node_base.22"*, %"struct.std::__2::__hash_node_base.22"** %__next_, align 4
  call void @_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEEENS_22__unordered_map_hasherIS7_SE_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SE_NS_8equal_toIS7_EELb1EEENS5_ISE_EEE17__deallocate_nodeEPNS_16__hash_node_baseIPNS_11__hash_nodeISE_PvEEEE(%"class.std::__2::__hash_table.18"* %this1, %"struct.std::__2::__hash_node_base.22"* %0) #9
  %__bucket_list_ = getelementptr inbounds %"class.std::__2::__hash_table.18", %"class.std::__2::__hash_table.18"* %this1, i32 0, i32 0
  %call2 = call %"class.std::__2::unique_ptr.19"* @_ZNSt3__210unique_ptrIA_PNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS0_IN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISK_EEEEED2Ev(%"class.std::__2::unique_ptr.19"* %__bucket_list_) #9
  ret %"class.std::__2::__hash_table.18"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEEENS_22__unordered_map_hasherIS7_SE_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SE_NS_8equal_toIS7_EELb1EEENS5_ISE_EEE17__deallocate_nodeEPNS_16__hash_node_baseIPNS_11__hash_nodeISE_PvEEEE(%"class.std::__2::__hash_table.18"* %this, %"struct.std::__2::__hash_node_base.22"* %__np) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__hash_table.18"*, align 4
  %__np.addr = alloca %"struct.std::__2::__hash_node_base.22"*, align 4
  %__na = alloca %"class.std::__2::allocator.32"*, align 4
  %__next = alloca %"struct.std::__2::__hash_node_base.22"*, align 4
  %__real_np = alloca %"struct.std::__2::__hash_node"*, align 4
  store %"class.std::__2::__hash_table.18"* %this, %"class.std::__2::__hash_table.18"** %this.addr, align 4
  store %"struct.std::__2::__hash_node_base.22"* %__np, %"struct.std::__2::__hash_node_base.22"** %__np.addr, align 4
  %this1 = load %"class.std::__2::__hash_table.18"*, %"class.std::__2::__hash_table.18"** %this.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.32"* @_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEEENS_22__unordered_map_hasherIS7_SE_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SE_NS_8equal_toIS7_EELb1EEENS5_ISE_EEE12__node_allocEv(%"class.std::__2::__hash_table.18"* %this1) #9
  store %"class.std::__2::allocator.32"* %call, %"class.std::__2::allocator.32"** %__na, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %0 = load %"struct.std::__2::__hash_node_base.22"*, %"struct.std::__2::__hash_node_base.22"** %__np.addr, align 4
  %cmp = icmp ne %"struct.std::__2::__hash_node_base.22"* %0, null
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %1 = load %"struct.std::__2::__hash_node_base.22"*, %"struct.std::__2::__hash_node_base.22"** %__np.addr, align 4
  %__next_ = getelementptr inbounds %"struct.std::__2::__hash_node_base.22", %"struct.std::__2::__hash_node_base.22"* %1, i32 0, i32 0
  %2 = load %"struct.std::__2::__hash_node_base.22"*, %"struct.std::__2::__hash_node_base.22"** %__next_, align 4
  store %"struct.std::__2::__hash_node_base.22"* %2, %"struct.std::__2::__hash_node_base.22"** %__next, align 4
  %3 = load %"struct.std::__2::__hash_node_base.22"*, %"struct.std::__2::__hash_node_base.22"** %__np.addr, align 4
  %call2 = call %"struct.std::__2::__hash_node"* @_ZNSt3__216__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEE8__upcastEv(%"struct.std::__2::__hash_node_base.22"* %3) #9
  store %"struct.std::__2::__hash_node"* %call2, %"struct.std::__2::__hash_node"** %__real_np, align 4
  %4 = load %"class.std::__2::allocator.32"*, %"class.std::__2::allocator.32"** %__na, align 4
  %5 = load %"struct.std::__2::__hash_node"*, %"struct.std::__2::__hash_node"** %__real_np, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__hash_node", %"struct.std::__2::__hash_node"* %5, i32 0, i32 2
  %call3 = call %"struct.std::__2::pair"* @_ZNSt3__222__hash_key_value_typesINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEEEE9__get_ptrERSE_(%"struct.std::__2::__hash_value_type"* nonnull align 4 dereferenceable(16) %__value_)
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEEE7destroyINS_4pairIKS8_SE_EEEEvRSI_PT_(%"class.std::__2::allocator.32"* nonnull align 1 dereferenceable(1) %4, %"struct.std::__2::pair"* %call3)
  %6 = load %"class.std::__2::allocator.32"*, %"class.std::__2::allocator.32"** %__na, align 4
  %7 = load %"struct.std::__2::__hash_node"*, %"struct.std::__2::__hash_node"** %__real_np, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEEE10deallocateERSI_PSH_m(%"class.std::__2::allocator.32"* nonnull align 1 dereferenceable(1) %6, %"struct.std::__2::__hash_node"* %7, i32 1) #9
  %8 = load %"struct.std::__2::__hash_node_base.22"*, %"struct.std::__2::__hash_node_base.22"** %__next, align 4
  store %"struct.std::__2::__hash_node_base.22"* %8, %"struct.std::__2::__hash_node_base.22"** %__np.addr, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base.22"* @_ZNSt3__217__compressed_pairINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEENS7_ISI_EEE5firstEv(%"class.std::__2::__compressed_pair.29"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.29"*, align 4
  store %"class.std::__2::__compressed_pair.29"* %this, %"class.std::__2::__compressed_pair.29"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.29"*, %"class.std::__2::__compressed_pair.29"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.29"* %this1 to %"struct.std::__2::__compressed_pair_elem.30"*
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base.22"* @_ZNSt3__222__compressed_pair_elemINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.30"* %0) #9
  ret %"struct.std::__2::__hash_node_base.22"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.19"* @_ZNSt3__210unique_ptrIA_PNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS0_IN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISK_EEEEED2Ev(%"class.std::__2::unique_ptr.19"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.19"*, align 4
  store %"class.std::__2::unique_ptr.19"* %this, %"class.std::__2::unique_ptr.19"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.19"*, %"class.std::__2::unique_ptr.19"** %this.addr, align 4
  call void @_ZNSt3__210unique_ptrIA_PNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS0_IN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISK_EEEEE5resetEDn(%"class.std::__2::unique_ptr.19"* %this1, i8* null) #9
  ret %"class.std::__2::unique_ptr.19"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.32"* @_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEEENS_22__unordered_map_hasherIS7_SE_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SE_NS_8equal_toIS7_EELb1EEENS5_ISE_EEE12__node_allocEv(%"class.std::__2::__hash_table.18"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__hash_table.18"*, align 4
  store %"class.std::__2::__hash_table.18"* %this, %"class.std::__2::__hash_table.18"** %this.addr, align 4
  %this1 = load %"class.std::__2::__hash_table.18"*, %"class.std::__2::__hash_table.18"** %this.addr, align 4
  %__p1_ = getelementptr inbounds %"class.std::__2::__hash_table.18", %"class.std::__2::__hash_table.18"* %this1, i32 0, i32 1
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.32"* @_ZNSt3__217__compressed_pairINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEENS7_ISI_EEE6secondEv(%"class.std::__2::__compressed_pair.29"* %__p1_) #9
  ret %"class.std::__2::allocator.32"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__hash_node"* @_ZNSt3__216__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEE8__upcastEv(%"struct.std::__2::__hash_node_base.22"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__hash_node_base.22"*, align 4
  store %"struct.std::__2::__hash_node_base.22"* %this, %"struct.std::__2::__hash_node_base.22"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__hash_node_base.22"*, %"struct.std::__2::__hash_node_base.22"** %this.addr, align 4
  %call = call %"struct.std::__2::__hash_node_base.22"* @_ZNSt3__214pointer_traitsIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEE10pointer_toERSK_(%"struct.std::__2::__hash_node_base.22"* nonnull align 4 dereferenceable(4) %this1) #9
  %0 = bitcast %"struct.std::__2::__hash_node_base.22"* %call to %"struct.std::__2::__hash_node"*
  ret %"struct.std::__2::__hash_node"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEEE7destroyINS_4pairIKS8_SE_EEEEvRSI_PT_(%"class.std::__2::allocator.32"* nonnull align 1 dereferenceable(1) %__a, %"struct.std::__2::pair"* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.32"*, align 4
  %__p.addr = alloca %"struct.std::__2::pair"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant.134", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy.135", align 1
  store %"class.std::__2::allocator.32"* %__a, %"class.std::__2::allocator.32"** %__a.addr, align 4
  store %"struct.std::__2::pair"* %__p, %"struct.std::__2::pair"** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy.135"* %ref.tmp to %"struct.std::__2::integral_constant.134"*
  %1 = load %"class.std::__2::allocator.32"*, %"class.std::__2::allocator.32"** %__a.addr, align 4
  %2 = load %"struct.std::__2::pair"*, %"struct.std::__2::pair"** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEEE9__destroyINS_4pairIKS8_SE_EEEEvNS_17integral_constantIbLb0EEERSI_PT_(%"class.std::__2::allocator.32"* nonnull align 1 dereferenceable(1) %1, %"struct.std::__2::pair"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::pair"* @_ZNSt3__222__hash_key_value_typesINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEEEE9__get_ptrERSE_(%"struct.std::__2::__hash_value_type"* nonnull align 4 dereferenceable(16) %__n) #0 comdat {
entry:
  %__n.addr = alloca %"struct.std::__2::__hash_value_type"*, align 4
  store %"struct.std::__2::__hash_value_type"* %__n, %"struct.std::__2::__hash_value_type"** %__n.addr, align 4
  %0 = load %"struct.std::__2::__hash_value_type"*, %"struct.std::__2::__hash_value_type"** %__n.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %"struct.std::__2::pair"* @_ZNSt3__217__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteIS9_EEEEE11__get_valueEv(%"struct.std::__2::__hash_value_type"* %0)
  %call1 = call %"struct.std::__2::pair"* @_ZNSt3__29addressofINS_4pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEEEPT_RSG_(%"struct.std::__2::pair"* nonnull align 4 dereferenceable(16) %call) #9
  ret %"struct.std::__2::pair"* %call1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEEE10deallocateERSI_PSH_m(%"class.std::__2::allocator.32"* nonnull align 1 dereferenceable(1) %__a, %"struct.std::__2::__hash_node"* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.32"*, align 4
  %__p.addr = alloca %"struct.std::__2::__hash_node"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.32"* %__a, %"class.std::__2::allocator.32"** %__a.addr, align 4
  store %"struct.std::__2::__hash_node"* %__p, %"struct.std::__2::__hash_node"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.32"*, %"class.std::__2::allocator.32"** %__a.addr, align 4
  %1 = load %"struct.std::__2::__hash_node"*, %"struct.std::__2::__hash_node"** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEEEPvEEE10deallocateEPSG_m(%"class.std::__2::allocator.32"* %0, %"struct.std::__2::__hash_node"* %1, i32 %2) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.32"* @_ZNSt3__217__compressed_pairINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEENS7_ISI_EEE6secondEv(%"class.std::__2::__compressed_pair.29"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.29"*, align 4
  store %"class.std::__2::__compressed_pair.29"* %this, %"class.std::__2::__compressed_pair.29"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.29"*, %"class.std::__2::__compressed_pair.29"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.29"* %this1 to %"struct.std::__2::__compressed_pair_elem.31"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.32"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.31"* %0) #9
  ret %"class.std::__2::allocator.32"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.32"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.31"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.31"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.31"* %this, %"struct.std::__2::__compressed_pair_elem.31"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.31"*, %"struct.std::__2::__compressed_pair_elem.31"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.31"* %this1 to %"class.std::__2::allocator.32"*
  ret %"class.std::__2::allocator.32"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__hash_node_base.22"* @_ZNSt3__214pointer_traitsIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEE10pointer_toERSK_(%"struct.std::__2::__hash_node_base.22"* nonnull align 4 dereferenceable(4) %__r) #0 comdat {
entry:
  %__r.addr = alloca %"struct.std::__2::__hash_node_base.22"*, align 4
  store %"struct.std::__2::__hash_node_base.22"* %__r, %"struct.std::__2::__hash_node_base.22"** %__r.addr, align 4
  %0 = load %"struct.std::__2::__hash_node_base.22"*, %"struct.std::__2::__hash_node_base.22"** %__r.addr, align 4
  %call = call %"struct.std::__2::__hash_node_base.22"* @_ZNSt3__29addressofINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEPT_RSL_(%"struct.std::__2::__hash_node_base.22"* nonnull align 4 dereferenceable(4) %0) #9
  ret %"struct.std::__2::__hash_node_base.22"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__hash_node_base.22"* @_ZNSt3__29addressofINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEPT_RSL_(%"struct.std::__2::__hash_node_base.22"* nonnull align 4 dereferenceable(4) %__x) #0 comdat {
entry:
  %__x.addr = alloca %"struct.std::__2::__hash_node_base.22"*, align 4
  store %"struct.std::__2::__hash_node_base.22"* %__x, %"struct.std::__2::__hash_node_base.22"** %__x.addr, align 4
  %0 = load %"struct.std::__2::__hash_node_base.22"*, %"struct.std::__2::__hash_node_base.22"** %__x.addr, align 4
  ret %"struct.std::__2::__hash_node_base.22"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEEE9__destroyINS_4pairIKS8_SE_EEEEvNS_17integral_constantIbLb0EEERSI_PT_(%"class.std::__2::allocator.32"* nonnull align 1 dereferenceable(1) %0, %"struct.std::__2::pair"* %__p) #0 comdat {
entry:
  %1 = alloca %"struct.std::__2::integral_constant.134", align 1
  %.addr = alloca %"class.std::__2::allocator.32"*, align 4
  %__p.addr = alloca %"struct.std::__2::pair"*, align 4
  store %"class.std::__2::allocator.32"* %0, %"class.std::__2::allocator.32"** %.addr, align 4
  store %"struct.std::__2::pair"* %__p, %"struct.std::__2::pair"** %__p.addr, align 4
  %2 = load %"struct.std::__2::pair"*, %"struct.std::__2::pair"** %__p.addr, align 4
  %call = call %"struct.std::__2::pair"* @_ZNSt3__24pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEED2Ev(%"struct.std::__2::pair"* %2) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::pair"* @_ZNSt3__24pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEED2Ev(%"struct.std::__2::pair"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::pair"*, align 4
  store %"struct.std::__2::pair"* %this, %"struct.std::__2::pair"** %this.addr, align 4
  %this1 = load %"struct.std::__2::pair"*, %"struct.std::__2::pair"** %this.addr, align 4
  %second = getelementptr inbounds %"struct.std::__2::pair", %"struct.std::__2::pair"* %this1, i32 0, i32 1
  %call = call %"class.std::__2::unique_ptr.129"* @_ZNSt3__210unique_ptrIN5draco8MetadataENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.129"* %second) #9
  %first = getelementptr inbounds %"struct.std::__2::pair", %"struct.std::__2::pair"* %this1, i32 0, i32 0
  %call2 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_string"* %first) #9
  ret %"struct.std::__2::pair"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.129"* @_ZNSt3__210unique_ptrIN5draco8MetadataENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.129"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.129"*, align 4
  store %"class.std::__2::unique_ptr.129"* %this, %"class.std::__2::unique_ptr.129"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.129"*, %"class.std::__2::unique_ptr.129"** %this.addr, align 4
  call void @_ZNSt3__210unique_ptrIN5draco8MetadataENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.129"* %this1, %"class.draco::Metadata"* null) #9
  ret %"class.std::__2::unique_ptr.129"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__210unique_ptrIN5draco8MetadataENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.129"* %this, %"class.draco::Metadata"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.129"*, align 4
  %__p.addr = alloca %"class.draco::Metadata"*, align 4
  %__tmp = alloca %"class.draco::Metadata"*, align 4
  store %"class.std::__2::unique_ptr.129"* %this, %"class.std::__2::unique_ptr.129"** %this.addr, align 4
  store %"class.draco::Metadata"* %__p, %"class.draco::Metadata"** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.129"*, %"class.std::__2::unique_ptr.129"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.129", %"class.std::__2::unique_ptr.129"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::Metadata"** @_ZNSt3__217__compressed_pairIPN5draco8MetadataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.130"* %__ptr_) #9
  %0 = load %"class.draco::Metadata"*, %"class.draco::Metadata"** %call, align 4
  store %"class.draco::Metadata"* %0, %"class.draco::Metadata"** %__tmp, align 4
  %1 = load %"class.draco::Metadata"*, %"class.draco::Metadata"** %__p.addr, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr.129", %"class.std::__2::unique_ptr.129"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::Metadata"** @_ZNSt3__217__compressed_pairIPN5draco8MetadataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.130"* %__ptr_2) #9
  store %"class.draco::Metadata"* %1, %"class.draco::Metadata"** %call3, align 4
  %2 = load %"class.draco::Metadata"*, %"class.draco::Metadata"** %__tmp, align 4
  %tobool = icmp ne %"class.draco::Metadata"* %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__ptr_4 = getelementptr inbounds %"class.std::__2::unique_ptr.129", %"class.std::__2::unique_ptr.129"* %this1, i32 0, i32 0
  %call5 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.133"* @_ZNSt3__217__compressed_pairIPN5draco8MetadataENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.130"* %__ptr_4) #9
  %3 = load %"class.draco::Metadata"*, %"class.draco::Metadata"** %__tmp, align 4
  call void @_ZNKSt3__214default_deleteIN5draco8MetadataEEclEPS2_(%"struct.std::__2::default_delete.133"* %call5, %"class.draco::Metadata"* %3) #9
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::Metadata"** @_ZNSt3__217__compressed_pairIPN5draco8MetadataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.130"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.130"*, align 4
  store %"class.std::__2::__compressed_pair.130"* %this, %"class.std::__2::__compressed_pair.130"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.130"*, %"class.std::__2::__compressed_pair.130"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.130"* %this1 to %"struct.std::__2::__compressed_pair_elem.131"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::Metadata"** @_ZNSt3__222__compressed_pair_elemIPN5draco8MetadataELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.131"* %0) #9
  ret %"class.draco::Metadata"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.133"* @_ZNSt3__217__compressed_pairIPN5draco8MetadataENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.130"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.130"*, align 4
  store %"class.std::__2::__compressed_pair.130"* %this, %"class.std::__2::__compressed_pair.130"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.130"*, %"class.std::__2::__compressed_pair.130"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.130"* %this1 to %"struct.std::__2::__compressed_pair_elem.132"*
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.133"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco8MetadataEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.132"* %0) #9
  ret %"struct.std::__2::default_delete.133"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__214default_deleteIN5draco8MetadataEEclEPS2_(%"struct.std::__2::default_delete.133"* %this, %"class.draco::Metadata"* %__ptr) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::default_delete.133"*, align 4
  %__ptr.addr = alloca %"class.draco::Metadata"*, align 4
  store %"struct.std::__2::default_delete.133"* %this, %"struct.std::__2::default_delete.133"** %this.addr, align 4
  store %"class.draco::Metadata"* %__ptr, %"class.draco::Metadata"** %__ptr.addr, align 4
  %this1 = load %"struct.std::__2::default_delete.133"*, %"struct.std::__2::default_delete.133"** %this.addr, align 4
  %0 = load %"class.draco::Metadata"*, %"class.draco::Metadata"** %__ptr.addr, align 4
  %isnull = icmp eq %"class.draco::Metadata"* %0, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %entry
  %call = call %"class.draco::Metadata"* @_ZN5draco8MetadataD2Ev(%"class.draco::Metadata"* %0) #9
  %1 = bitcast %"class.draco::Metadata"* %0 to i8*
  call void @_ZdlPv(i8* %1) #12
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::Metadata"** @_ZNSt3__222__compressed_pair_elemIPN5draco8MetadataELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.131"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.131"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.131"* %this, %"struct.std::__2::__compressed_pair_elem.131"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.131"*, %"struct.std::__2::__compressed_pair_elem.131"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.131", %"struct.std::__2::__compressed_pair_elem.131"* %this1, i32 0, i32 0
  ret %"class.draco::Metadata"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.133"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco8MetadataEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.132"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.132"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.132"* %this, %"struct.std::__2::__compressed_pair_elem.132"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.132"*, %"struct.std::__2::__compressed_pair_elem.132"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.132"* %this1 to %"struct.std::__2::default_delete.133"*
  ret %"struct.std::__2::default_delete.133"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::Metadata"* @_ZN5draco8MetadataD2Ev(%"class.draco::Metadata"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::Metadata"*, align 4
  store %"class.draco::Metadata"* %this, %"class.draco::Metadata"** %this.addr, align 4
  %this1 = load %"class.draco::Metadata"*, %"class.draco::Metadata"** %this.addr, align 4
  %sub_metadatas_ = getelementptr inbounds %"class.draco::Metadata", %"class.draco::Metadata"* %this1, i32 0, i32 1
  %call = call %"class.std::__2::unordered_map.17"* @_ZNSt3__213unordered_mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteIS9_EEEENS_4hashIS6_EENS_8equal_toIS6_EENS4_INS_4pairIKS6_SC_EEEEED2Ev(%"class.std::__2::unordered_map.17"* %sub_metadatas_) #9
  %entries_ = getelementptr inbounds %"class.draco::Metadata", %"class.draco::Metadata"* %this1, i32 0, i32 0
  %call2 = call %"class.std::__2::unordered_map"* @_ZNSt3__213unordered_mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueENS_4hashIS6_EENS_8equal_toIS6_EENS4_INS_4pairIKS6_S8_EEEEED2Ev(%"class.std::__2::unordered_map"* %entries_) #9
  ret %"class.draco::Metadata"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::pair"* @_ZNSt3__29addressofINS_4pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEEEPT_RSG_(%"struct.std::__2::pair"* nonnull align 4 dereferenceable(16) %__x) #0 comdat {
entry:
  %__x.addr = alloca %"struct.std::__2::pair"*, align 4
  store %"struct.std::__2::pair"* %__x, %"struct.std::__2::pair"** %__x.addr, align 4
  %0 = load %"struct.std::__2::pair"*, %"struct.std::__2::pair"** %__x.addr, align 4
  ret %"struct.std::__2::pair"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %"struct.std::__2::pair"* @_ZNSt3__217__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteIS9_EEEEE11__get_valueEv(%"struct.std::__2::__hash_value_type"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__hash_value_type"*, align 4
  store %"struct.std::__2::__hash_value_type"* %this, %"struct.std::__2::__hash_value_type"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__hash_value_type"*, %"struct.std::__2::__hash_value_type"** %this.addr, align 4
  %__cc = getelementptr inbounds %"struct.std::__2::__hash_value_type", %"struct.std::__2::__hash_value_type"* %this1, i32 0, i32 0
  ret %"struct.std::__2::pair"* %__cc
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISA_EEEEEEPvEEE10deallocateEPSG_m(%"class.std::__2::allocator.32"* %this, %"struct.std::__2::__hash_node"* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.32"*, align 4
  %__p.addr = alloca %"struct.std::__2::__hash_node"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.32"* %this, %"class.std::__2::allocator.32"** %this.addr, align 4
  store %"struct.std::__2::__hash_node"* %__p, %"struct.std::__2::__hash_node"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.32"*, %"class.std::__2::allocator.32"** %this.addr, align 4
  %0 = load %"struct.std::__2::__hash_node"*, %"struct.std::__2::__hash_node"** %__p.addr, align 4
  %1 = bitcast %"struct.std::__2::__hash_node"* %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 24
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base.22"* @_ZNSt3__222__compressed_pair_elemINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.30"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.30"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.30"* %this, %"struct.std::__2::__compressed_pair_elem.30"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.30"*, %"struct.std::__2::__compressed_pair_elem.30"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.30", %"struct.std::__2::__compressed_pair_elem.30"* %this1, i32 0, i32 0
  ret %"struct.std::__2::__hash_node_base.22"* %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__210unique_ptrIA_PNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS0_IN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISK_EEEEE5resetEDn(%"class.std::__2::unique_ptr.19"* %this, i8* %0) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.19"*, align 4
  %.addr = alloca i8*, align 4
  %__tmp = alloca %"struct.std::__2::__hash_node_base.22"**, align 4
  store %"class.std::__2::unique_ptr.19"* %this, %"class.std::__2::unique_ptr.19"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.19"*, %"class.std::__2::unique_ptr.19"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.19", %"class.std::__2::unique_ptr.19"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base.22"*** @_ZNSt3__217__compressed_pairIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISL_EEEEE5firstEv(%"class.std::__2::__compressed_pair.20"* %__ptr_) #9
  %1 = load %"struct.std::__2::__hash_node_base.22"**, %"struct.std::__2::__hash_node_base.22"*** %call, align 4
  store %"struct.std::__2::__hash_node_base.22"** %1, %"struct.std::__2::__hash_node_base.22"*** %__tmp, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr.19", %"class.std::__2::unique_ptr.19"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base.22"*** @_ZNSt3__217__compressed_pairIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISL_EEEEE5firstEv(%"class.std::__2::__compressed_pair.20"* %__ptr_2) #9
  store %"struct.std::__2::__hash_node_base.22"** null, %"struct.std::__2::__hash_node_base.22"*** %call3, align 4
  %2 = load %"struct.std::__2::__hash_node_base.22"**, %"struct.std::__2::__hash_node_base.22"*** %__tmp, align 4
  %tobool = icmp ne %"struct.std::__2::__hash_node_base.22"** %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__ptr_4 = getelementptr inbounds %"class.std::__2::unique_ptr.19", %"class.std::__2::unique_ptr.19"* %this1, i32 0, i32 0
  %call5 = call nonnull align 4 dereferenceable(4) %"class.std::__2::__bucket_list_deallocator.24"* @_ZNSt3__217__compressed_pairIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISL_EEEEE6secondEv(%"class.std::__2::__compressed_pair.20"* %__ptr_4) #9
  %3 = load %"struct.std::__2::__hash_node_base.22"**, %"struct.std::__2::__hash_node_base.22"*** %__tmp, align 4
  call void @_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEEclEPSL_(%"class.std::__2::__bucket_list_deallocator.24"* %call5, %"struct.std::__2::__hash_node_base.22"** %3) #9
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base.22"*** @_ZNSt3__217__compressed_pairIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISL_EEEEE5firstEv(%"class.std::__2::__compressed_pair.20"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.20"*, align 4
  store %"class.std::__2::__compressed_pair.20"* %this, %"class.std::__2::__compressed_pair.20"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.20"*, %"class.std::__2::__compressed_pair.20"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.20"* %this1 to %"struct.std::__2::__compressed_pair_elem.21"*
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base.22"*** @_ZNSt3__222__compressed_pair_elemIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.21"* %0) #9
  ret %"struct.std::__2::__hash_node_base.22"*** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__bucket_list_deallocator.24"* @_ZNSt3__217__compressed_pairIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISL_EEEEE6secondEv(%"class.std::__2::__compressed_pair.20"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.20"*, align 4
  store %"class.std::__2::__compressed_pair.20"* %this, %"class.std::__2::__compressed_pair.20"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.20"*, %"class.std::__2::__compressed_pair.20"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.20"* %this1 to i8*
  %add.ptr = getelementptr inbounds i8, i8* %0, i32 4
  %1 = bitcast i8* %add.ptr to %"struct.std::__2::__compressed_pair_elem.23"*
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::__bucket_list_deallocator.24"* @_ZNSt3__222__compressed_pair_elemINS_25__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS2_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISD_EEEEEEPvEEEEEEEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.23"* %1) #9
  ret %"class.std::__2::__bucket_list_deallocator.24"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEEclEPSL_(%"class.std::__2::__bucket_list_deallocator.24"* %this, %"struct.std::__2::__hash_node_base.22"** %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__bucket_list_deallocator.24"*, align 4
  %__p.addr = alloca %"struct.std::__2::__hash_node_base.22"**, align 4
  store %"class.std::__2::__bucket_list_deallocator.24"* %this, %"class.std::__2::__bucket_list_deallocator.24"** %this.addr, align 4
  store %"struct.std::__2::__hash_node_base.22"** %__p, %"struct.std::__2::__hash_node_base.22"*** %__p.addr, align 4
  %this1 = load %"class.std::__2::__bucket_list_deallocator.24"*, %"class.std::__2::__bucket_list_deallocator.24"** %this.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.27"* @_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEE7__allocEv(%"class.std::__2::__bucket_list_deallocator.24"* %this1) #9
  %0 = load %"struct.std::__2::__hash_node_base.22"**, %"struct.std::__2::__hash_node_base.22"*** %__p.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEE4sizeEv(%"class.std::__2::__bucket_list_deallocator.24"* %this1) #9
  %1 = load i32, i32* %call2, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEE10deallocateERSM_PSL_m(%"class.std::__2::allocator.27"* nonnull align 1 dereferenceable(1) %call, %"struct.std::__2::__hash_node_base.22"** %0, i32 %1) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base.22"*** @_ZNSt3__222__compressed_pair_elemIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.21"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.21"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.21"* %this, %"struct.std::__2::__compressed_pair_elem.21"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.21"*, %"struct.std::__2::__compressed_pair_elem.21"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.21", %"struct.std::__2::__compressed_pair_elem.21"* %this1, i32 0, i32 0
  ret %"struct.std::__2::__hash_node_base.22"*** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__bucket_list_deallocator.24"* @_ZNSt3__222__compressed_pair_elemINS_25__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS2_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISD_EEEEEEPvEEEEEEEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.23"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.23"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.23"* %this, %"struct.std::__2::__compressed_pair_elem.23"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.23"*, %"struct.std::__2::__compressed_pair_elem.23"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.23", %"struct.std::__2::__compressed_pair_elem.23"* %this1, i32 0, i32 0
  ret %"class.std::__2::__bucket_list_deallocator.24"* %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEE10deallocateERSM_PSL_m(%"class.std::__2::allocator.27"* nonnull align 1 dereferenceable(1) %__a, %"struct.std::__2::__hash_node_base.22"** %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.27"*, align 4
  %__p.addr = alloca %"struct.std::__2::__hash_node_base.22"**, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.27"* %__a, %"class.std::__2::allocator.27"** %__a.addr, align 4
  store %"struct.std::__2::__hash_node_base.22"** %__p, %"struct.std::__2::__hash_node_base.22"*** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.27"*, %"class.std::__2::allocator.27"** %__a.addr, align 4
  %1 = load %"struct.std::__2::__hash_node_base.22"**, %"struct.std::__2::__hash_node_base.22"*** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEEE10deallocateEPSK_m(%"class.std::__2::allocator.27"* %0, %"struct.std::__2::__hash_node_base.22"** %1, i32 %2) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.27"* @_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEE7__allocEv(%"class.std::__2::__bucket_list_deallocator.24"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__bucket_list_deallocator.24"*, align 4
  store %"class.std::__2::__bucket_list_deallocator.24"* %this, %"class.std::__2::__bucket_list_deallocator.24"** %this.addr, align 4
  %this1 = load %"class.std::__2::__bucket_list_deallocator.24"*, %"class.std::__2::__bucket_list_deallocator.24"** %this.addr, align 4
  %__data_ = getelementptr inbounds %"class.std::__2::__bucket_list_deallocator.24", %"class.std::__2::__bucket_list_deallocator.24"* %this1, i32 0, i32 0
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.27"* @_ZNSt3__217__compressed_pairImNS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEE6secondEv(%"class.std::__2::__compressed_pair.25"* %__data_) #9
  ret %"class.std::__2::allocator.27"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEE4sizeEv(%"class.std::__2::__bucket_list_deallocator.24"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__bucket_list_deallocator.24"*, align 4
  store %"class.std::__2::__bucket_list_deallocator.24"* %this, %"class.std::__2::__bucket_list_deallocator.24"** %this.addr, align 4
  %this1 = load %"class.std::__2::__bucket_list_deallocator.24"*, %"class.std::__2::__bucket_list_deallocator.24"** %this.addr, align 4
  %__data_ = getelementptr inbounds %"class.std::__2::__bucket_list_deallocator.24", %"class.std::__2::__bucket_list_deallocator.24"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__217__compressed_pairImNS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEE5firstEv(%"class.std::__2::__compressed_pair.25"* %__data_) #9
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISB_EEEEEEPvEEEEE10deallocateEPSK_m(%"class.std::__2::allocator.27"* %this, %"struct.std::__2::__hash_node_base.22"** %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.27"*, align 4
  %__p.addr = alloca %"struct.std::__2::__hash_node_base.22"**, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.27"* %this, %"class.std::__2::allocator.27"** %this.addr, align 4
  store %"struct.std::__2::__hash_node_base.22"** %__p, %"struct.std::__2::__hash_node_base.22"*** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.27"*, %"class.std::__2::allocator.27"** %this.addr, align 4
  %0 = load %"struct.std::__2::__hash_node_base.22"**, %"struct.std::__2::__hash_node_base.22"*** %__p.addr, align 4
  %1 = bitcast %"struct.std::__2::__hash_node_base.22"** %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.27"* @_ZNSt3__217__compressed_pairImNS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEE6secondEv(%"class.std::__2::__compressed_pair.25"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.25"*, align 4
  store %"class.std::__2::__compressed_pair.25"* %this, %"class.std::__2::__compressed_pair.25"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.25"*, %"class.std::__2::__compressed_pair.25"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.25"* %this1 to %"struct.std::__2::__compressed_pair_elem.26"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.27"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.26"* %0) #9
  ret %"class.std::__2::allocator.27"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.27"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.26"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.26"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.26"* %this, %"struct.std::__2::__compressed_pair_elem.26"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.26"*, %"struct.std::__2::__compressed_pair_elem.26"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.26"* %this1 to %"class.std::__2::allocator.27"*
  ret %"class.std::__2::allocator.27"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__217__compressed_pairImNS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEENS_10unique_ptrIN5draco8MetadataENS_14default_deleteISC_EEEEEEPvEEEEEEE5firstEv(%"class.std::__2::__compressed_pair.25"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.25"*, align 4
  store %"class.std::__2::__compressed_pair.25"* %this, %"class.std::__2::__compressed_pair.25"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.25"*, %"class.std::__2::__compressed_pair.25"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.25"* %this1 to %"struct.std::__2::__compressed_pair_elem.5"*
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__222__compressed_pair_elemImLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.5"* %0) #9
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__222__compressed_pair_elemImLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.5"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.5"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.5"* %this, %"struct.std::__2::__compressed_pair_elem.5"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.5"*, %"struct.std::__2::__compressed_pair_elem.5"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.5", %"struct.std::__2::__compressed_pair_elem.5"* %this1, i32 0, i32 0
  ret i32* %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__hash_table"* @_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEENS_22__unordered_map_hasherIS7_SA_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SA_NS_8equal_toIS7_EELb1EEENS5_ISA_EEED2Ev(%"class.std::__2::__hash_table"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__hash_table"*, align 4
  store %"class.std::__2::__hash_table"* %this, %"class.std::__2::__hash_table"** %this.addr, align 4
  %this1 = load %"class.std::__2::__hash_table"*, %"class.std::__2::__hash_table"** %this.addr, align 4
  %__p1_ = getelementptr inbounds %"class.std::__2::__hash_table", %"class.std::__2::__hash_table"* %this1, i32 0, i32 1
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base"* @_ZNSt3__217__compressed_pairINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS7_ISE_EEE5firstEv(%"class.std::__2::__compressed_pair.7"* %__p1_) #9
  %__next_ = getelementptr inbounds %"struct.std::__2::__hash_node_base", %"struct.std::__2::__hash_node_base"* %call, i32 0, i32 0
  %0 = load %"struct.std::__2::__hash_node_base"*, %"struct.std::__2::__hash_node_base"** %__next_, align 4
  call void @_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEENS_22__unordered_map_hasherIS7_SA_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SA_NS_8equal_toIS7_EELb1EEENS5_ISA_EEE17__deallocate_nodeEPNS_16__hash_node_baseIPNS_11__hash_nodeISA_PvEEEE(%"class.std::__2::__hash_table"* %this1, %"struct.std::__2::__hash_node_base"* %0) #9
  %__bucket_list_ = getelementptr inbounds %"class.std::__2::__hash_table", %"class.std::__2::__hash_table"* %this1, i32 0, i32 0
  %call2 = call %"class.std::__2::unique_ptr.0"* @_ZNSt3__210unique_ptrIA_PNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISH_EEEEED2Ev(%"class.std::__2::unique_ptr.0"* %__bucket_list_) #9
  ret %"class.std::__2::__hash_table"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEENS_22__unordered_map_hasherIS7_SA_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SA_NS_8equal_toIS7_EELb1EEENS5_ISA_EEE17__deallocate_nodeEPNS_16__hash_node_baseIPNS_11__hash_nodeISA_PvEEEE(%"class.std::__2::__hash_table"* %this, %"struct.std::__2::__hash_node_base"* %__np) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__hash_table"*, align 4
  %__np.addr = alloca %"struct.std::__2::__hash_node_base"*, align 4
  %__na = alloca %"class.std::__2::allocator.10"*, align 4
  %__next = alloca %"struct.std::__2::__hash_node_base"*, align 4
  %__real_np = alloca %"struct.std::__2::__hash_node.136"*, align 4
  store %"class.std::__2::__hash_table"* %this, %"class.std::__2::__hash_table"** %this.addr, align 4
  store %"struct.std::__2::__hash_node_base"* %__np, %"struct.std::__2::__hash_node_base"** %__np.addr, align 4
  %this1 = load %"class.std::__2::__hash_table"*, %"class.std::__2::__hash_table"** %this.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.10"* @_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEENS_22__unordered_map_hasherIS7_SA_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SA_NS_8equal_toIS7_EELb1EEENS5_ISA_EEE12__node_allocEv(%"class.std::__2::__hash_table"* %this1) #9
  store %"class.std::__2::allocator.10"* %call, %"class.std::__2::allocator.10"** %__na, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %0 = load %"struct.std::__2::__hash_node_base"*, %"struct.std::__2::__hash_node_base"** %__np.addr, align 4
  %cmp = icmp ne %"struct.std::__2::__hash_node_base"* %0, null
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %1 = load %"struct.std::__2::__hash_node_base"*, %"struct.std::__2::__hash_node_base"** %__np.addr, align 4
  %__next_ = getelementptr inbounds %"struct.std::__2::__hash_node_base", %"struct.std::__2::__hash_node_base"* %1, i32 0, i32 0
  %2 = load %"struct.std::__2::__hash_node_base"*, %"struct.std::__2::__hash_node_base"** %__next_, align 4
  store %"struct.std::__2::__hash_node_base"* %2, %"struct.std::__2::__hash_node_base"** %__next, align 4
  %3 = load %"struct.std::__2::__hash_node_base"*, %"struct.std::__2::__hash_node_base"** %__np.addr, align 4
  %call2 = call %"struct.std::__2::__hash_node.136"* @_ZNSt3__216__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEE8__upcastEv(%"struct.std::__2::__hash_node_base"* %3) #9
  store %"struct.std::__2::__hash_node.136"* %call2, %"struct.std::__2::__hash_node.136"** %__real_np, align 4
  %4 = load %"class.std::__2::allocator.10"*, %"class.std::__2::allocator.10"** %__na, align 4
  %5 = load %"struct.std::__2::__hash_node.136"*, %"struct.std::__2::__hash_node.136"** %__real_np, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__hash_node.136", %"struct.std::__2::__hash_node.136"* %5, i32 0, i32 2
  %call3 = call %"struct.std::__2::pair.138"* @_ZNSt3__222__hash_key_value_typesINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEE9__get_ptrERSA_(%"struct.std::__2::__hash_value_type.137"* nonnull align 4 dereferenceable(24) %__value_)
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEE7destroyINS_4pairIKS8_SA_EEEEvRSE_PT_(%"class.std::__2::allocator.10"* nonnull align 1 dereferenceable(1) %4, %"struct.std::__2::pair.138"* %call3)
  %6 = load %"class.std::__2::allocator.10"*, %"class.std::__2::allocator.10"** %__na, align 4
  %7 = load %"struct.std::__2::__hash_node.136"*, %"struct.std::__2::__hash_node.136"** %__real_np, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEE10deallocateERSE_PSD_m(%"class.std::__2::allocator.10"* nonnull align 1 dereferenceable(1) %6, %"struct.std::__2::__hash_node.136"* %7, i32 1) #9
  %8 = load %"struct.std::__2::__hash_node_base"*, %"struct.std::__2::__hash_node_base"** %__next, align 4
  store %"struct.std::__2::__hash_node_base"* %8, %"struct.std::__2::__hash_node_base"** %__np.addr, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base"* @_ZNSt3__217__compressed_pairINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS7_ISE_EEE5firstEv(%"class.std::__2::__compressed_pair.7"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.7"*, align 4
  store %"class.std::__2::__compressed_pair.7"* %this, %"class.std::__2::__compressed_pair.7"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.7"*, %"class.std::__2::__compressed_pair.7"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.7"* %this1 to %"struct.std::__2::__compressed_pair_elem.8"*
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base"* @_ZNSt3__222__compressed_pair_elemINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.8"* %0) #9
  ret %"struct.std::__2::__hash_node_base"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.0"* @_ZNSt3__210unique_ptrIA_PNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISH_EEEEED2Ev(%"class.std::__2::unique_ptr.0"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.0"*, align 4
  store %"class.std::__2::unique_ptr.0"* %this, %"class.std::__2::unique_ptr.0"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.0"*, %"class.std::__2::unique_ptr.0"** %this.addr, align 4
  call void @_ZNSt3__210unique_ptrIA_PNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISH_EEEEE5resetEDn(%"class.std::__2::unique_ptr.0"* %this1, i8* null) #9
  ret %"class.std::__2::unique_ptr.0"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.10"* @_ZNSt3__212__hash_tableINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEENS_22__unordered_map_hasherIS7_SA_NS_4hashIS7_EELb1EEENS_21__unordered_map_equalIS7_SA_NS_8equal_toIS7_EELb1EEENS5_ISA_EEE12__node_allocEv(%"class.std::__2::__hash_table"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__hash_table"*, align 4
  store %"class.std::__2::__hash_table"* %this, %"class.std::__2::__hash_table"** %this.addr, align 4
  %this1 = load %"class.std::__2::__hash_table"*, %"class.std::__2::__hash_table"** %this.addr, align 4
  %__p1_ = getelementptr inbounds %"class.std::__2::__hash_table", %"class.std::__2::__hash_table"* %this1, i32 0, i32 1
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.10"* @_ZNSt3__217__compressed_pairINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS7_ISE_EEE6secondEv(%"class.std::__2::__compressed_pair.7"* %__p1_) #9
  ret %"class.std::__2::allocator.10"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__hash_node.136"* @_ZNSt3__216__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEE8__upcastEv(%"struct.std::__2::__hash_node_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__hash_node_base"*, align 4
  store %"struct.std::__2::__hash_node_base"* %this, %"struct.std::__2::__hash_node_base"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__hash_node_base"*, %"struct.std::__2::__hash_node_base"** %this.addr, align 4
  %call = call %"struct.std::__2::__hash_node_base"* @_ZNSt3__214pointer_traitsIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEEE10pointer_toERSG_(%"struct.std::__2::__hash_node_base"* nonnull align 4 dereferenceable(4) %this1) #9
  %0 = bitcast %"struct.std::__2::__hash_node_base"* %call to %"struct.std::__2::__hash_node.136"*
  ret %"struct.std::__2::__hash_node.136"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEE7destroyINS_4pairIKS8_SA_EEEEvRSE_PT_(%"class.std::__2::allocator.10"* nonnull align 1 dereferenceable(1) %__a, %"struct.std::__2::pair.138"* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.10"*, align 4
  %__p.addr = alloca %"struct.std::__2::pair.138"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant.134", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy.139", align 1
  store %"class.std::__2::allocator.10"* %__a, %"class.std::__2::allocator.10"** %__a.addr, align 4
  store %"struct.std::__2::pair.138"* %__p, %"struct.std::__2::pair.138"** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy.139"* %ref.tmp to %"struct.std::__2::integral_constant.134"*
  %1 = load %"class.std::__2::allocator.10"*, %"class.std::__2::allocator.10"** %__a.addr, align 4
  %2 = load %"struct.std::__2::pair.138"*, %"struct.std::__2::pair.138"** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEE9__destroyINS_4pairIKS8_SA_EEEEvNS_17integral_constantIbLb0EEERSE_PT_(%"class.std::__2::allocator.10"* nonnull align 1 dereferenceable(1) %1, %"struct.std::__2::pair.138"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::pair.138"* @_ZNSt3__222__hash_key_value_typesINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEE9__get_ptrERSA_(%"struct.std::__2::__hash_value_type.137"* nonnull align 4 dereferenceable(24) %__n) #0 comdat {
entry:
  %__n.addr = alloca %"struct.std::__2::__hash_value_type.137"*, align 4
  store %"struct.std::__2::__hash_value_type.137"* %__n, %"struct.std::__2::__hash_value_type.137"** %__n.addr, align 4
  %0 = load %"struct.std::__2::__hash_value_type.137"*, %"struct.std::__2::__hash_value_type.137"** %__n.addr, align 4
  %call = call nonnull align 4 dereferenceable(24) %"struct.std::__2::pair.138"* @_ZNSt3__217__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEE11__get_valueEv(%"struct.std::__2::__hash_value_type.137"* %0)
  %call1 = call %"struct.std::__2::pair.138"* @_ZNSt3__29addressofINS_4pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEEEPT_RSC_(%"struct.std::__2::pair.138"* nonnull align 4 dereferenceable(24) %call) #9
  ret %"struct.std::__2::pair.138"* %call1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEE10deallocateERSE_PSD_m(%"class.std::__2::allocator.10"* nonnull align 1 dereferenceable(1) %__a, %"struct.std::__2::__hash_node.136"* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.10"*, align 4
  %__p.addr = alloca %"struct.std::__2::__hash_node.136"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.10"* %__a, %"class.std::__2::allocator.10"** %__a.addr, align 4
  store %"struct.std::__2::__hash_node.136"* %__p, %"struct.std::__2::__hash_node.136"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.10"*, %"class.std::__2::allocator.10"** %__a.addr, align 4
  %1 = load %"struct.std::__2::__hash_node.136"*, %"struct.std::__2::__hash_node.136"** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEEN5draco10EntryValueEEEPvEEE10deallocateEPSC_m(%"class.std::__2::allocator.10"* %0, %"struct.std::__2::__hash_node.136"* %1, i32 %2) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.10"* @_ZNSt3__217__compressed_pairINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS7_ISE_EEE6secondEv(%"class.std::__2::__compressed_pair.7"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.7"*, align 4
  store %"class.std::__2::__compressed_pair.7"* %this, %"class.std::__2::__compressed_pair.7"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.7"*, %"class.std::__2::__compressed_pair.7"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.7"* %this1 to %"struct.std::__2::__compressed_pair_elem.9"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.10"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.9"* %0) #9
  ret %"class.std::__2::allocator.10"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.10"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.9"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.9"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.9"* %this, %"struct.std::__2::__compressed_pair_elem.9"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.9"*, %"struct.std::__2::__compressed_pair_elem.9"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.9"* %this1 to %"class.std::__2::allocator.10"*
  ret %"class.std::__2::allocator.10"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__hash_node_base"* @_ZNSt3__214pointer_traitsIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEEE10pointer_toERSG_(%"struct.std::__2::__hash_node_base"* nonnull align 4 dereferenceable(4) %__r) #0 comdat {
entry:
  %__r.addr = alloca %"struct.std::__2::__hash_node_base"*, align 4
  store %"struct.std::__2::__hash_node_base"* %__r, %"struct.std::__2::__hash_node_base"** %__r.addr, align 4
  %0 = load %"struct.std::__2::__hash_node_base"*, %"struct.std::__2::__hash_node_base"** %__r.addr, align 4
  %call = call %"struct.std::__2::__hash_node_base"* @_ZNSt3__29addressofINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEEEEPT_RSH_(%"struct.std::__2::__hash_node_base"* nonnull align 4 dereferenceable(4) %0) #9
  ret %"struct.std::__2::__hash_node_base"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__hash_node_base"* @_ZNSt3__29addressofINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEEEEPT_RSH_(%"struct.std::__2::__hash_node_base"* nonnull align 4 dereferenceable(4) %__x) #0 comdat {
entry:
  %__x.addr = alloca %"struct.std::__2::__hash_node_base"*, align 4
  store %"struct.std::__2::__hash_node_base"* %__x, %"struct.std::__2::__hash_node_base"** %__x.addr, align 4
  %0 = load %"struct.std::__2::__hash_node_base"*, %"struct.std::__2::__hash_node_base"** %__x.addr, align 4
  ret %"struct.std::__2::__hash_node_base"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEE9__destroyINS_4pairIKS8_SA_EEEEvNS_17integral_constantIbLb0EEERSE_PT_(%"class.std::__2::allocator.10"* nonnull align 1 dereferenceable(1) %0, %"struct.std::__2::pair.138"* %__p) #0 comdat {
entry:
  %1 = alloca %"struct.std::__2::integral_constant.134", align 1
  %.addr = alloca %"class.std::__2::allocator.10"*, align 4
  %__p.addr = alloca %"struct.std::__2::pair.138"*, align 4
  store %"class.std::__2::allocator.10"* %0, %"class.std::__2::allocator.10"** %.addr, align 4
  store %"struct.std::__2::pair.138"* %__p, %"struct.std::__2::pair.138"** %__p.addr, align 4
  %2 = load %"struct.std::__2::pair.138"*, %"struct.std::__2::pair.138"** %__p.addr, align 4
  %call = call %"struct.std::__2::pair.138"* @_ZNSt3__24pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEED2Ev(%"struct.std::__2::pair.138"* %2) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::pair.138"* @_ZNSt3__24pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEED2Ev(%"struct.std::__2::pair.138"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::pair.138"*, align 4
  store %"struct.std::__2::pair.138"* %this, %"struct.std::__2::pair.138"** %this.addr, align 4
  %this1 = load %"struct.std::__2::pair.138"*, %"struct.std::__2::pair.138"** %this.addr, align 4
  %second = getelementptr inbounds %"struct.std::__2::pair.138", %"struct.std::__2::pair.138"* %this1, i32 0, i32 1
  %call = call %"class.draco::EntryValue"* @_ZN5draco10EntryValueD2Ev(%"class.draco::EntryValue"* %second) #9
  %first = getelementptr inbounds %"struct.std::__2::pair.138", %"struct.std::__2::pair.138"* %this1, i32 0, i32 0
  %call2 = call %"class.std::__2::basic_string"* @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev(%"class.std::__2::basic_string"* %first) #9
  ret %"struct.std::__2::pair.138"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.draco::EntryValue"* @_ZN5draco10EntryValueD2Ev(%"class.draco::EntryValue"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.draco::EntryValue"*, align 4
  store %"class.draco::EntryValue"* %this, %"class.draco::EntryValue"** %this.addr, align 4
  %this1 = load %"class.draco::EntryValue"*, %"class.draco::EntryValue"** %this.addr, align 4
  %data_ = getelementptr inbounds %"class.draco::EntryValue", %"class.draco::EntryValue"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::vector.56"* @_ZNSt3__26vectorIhNS_9allocatorIhEEED2Ev(%"class.std::__2::vector.56"* %data_) #9
  ret %"class.draco::EntryValue"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector.56"* @_ZNSt3__26vectorIhNS_9allocatorIhEEED2Ev(%"class.std::__2::vector.56"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.56"*, align 4
  store %"class.std::__2::vector.56"* %this, %"class.std::__2::vector.56"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.56"*, %"class.std::__2::vector.56"** %this.addr, align 4
  call void @_ZNKSt3__26vectorIhNS_9allocatorIhEEE17__annotate_deleteEv(%"class.std::__2::vector.56"* %this1) #9
  %0 = bitcast %"class.std::__2::vector.56"* %this1 to %"class.std::__2::__vector_base.57"*
  %call = call %"class.std::__2::__vector_base.57"* @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEED2Ev(%"class.std::__2::__vector_base.57"* %0) #9
  ret %"class.std::__2::vector.56"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIhNS_9allocatorIhEEE17__annotate_deleteEv(%"class.std::__2::vector.56"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.56"*, align 4
  store %"class.std::__2::vector.56"* %this, %"class.std::__2::vector.56"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.56"*, %"class.std::__2::vector.56"** %this.addr, align 4
  %call = call i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector.56"* %this1) #9
  %call2 = call i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector.56"* %this1) #9
  %call3 = call i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::vector.56"* %this1) #9
  %add.ptr = getelementptr inbounds i8, i8* %call2, i32 %call3
  %call4 = call i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector.56"* %this1) #9
  %call5 = call i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4sizeEv(%"class.std::__2::vector.56"* %this1) #9
  %add.ptr6 = getelementptr inbounds i8, i8* %call4, i32 %call5
  %call7 = call i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector.56"* %this1) #9
  %call8 = call i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::vector.56"* %this1) #9
  %add.ptr9 = getelementptr inbounds i8, i8* %call7, i32 %call8
  call void @_ZNKSt3__26vectorIhNS_9allocatorIhEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector.56"* %this1, i8* %call, i8* %add.ptr, i8* %add.ptr6, i8* %add.ptr9) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base.57"* @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEED2Ev(%"class.std::__2::__vector_base.57"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__vector_base.57"*, align 4
  %this.addr = alloca %"class.std::__2::__vector_base.57"*, align 4
  store %"class.std::__2::__vector_base.57"* %this, %"class.std::__2::__vector_base.57"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.57"*, %"class.std::__2::__vector_base.57"** %this.addr, align 4
  store %"class.std::__2::__vector_base.57"* %this1, %"class.std::__2::__vector_base.57"** %retval, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.57", %"class.std::__2::__vector_base.57"* %this1, i32 0, i32 0
  %0 = load i8*, i8** %__begin_, align 4
  %cmp = icmp ne i8* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE5clearEv(%"class.std::__2::__vector_base.57"* %this1) #9
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.61"* @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE7__allocEv(%"class.std::__2::__vector_base.57"* %this1) #9
  %__begin_2 = getelementptr inbounds %"class.std::__2::__vector_base.57", %"class.std::__2::__vector_base.57"* %this1, i32 0, i32 0
  %1 = load i8*, i8** %__begin_2, align 4
  %call3 = call i32 @_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::__vector_base.57"* %this1) #9
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE10deallocateERS2_Phm(%"class.std::__2::allocator.61"* nonnull align 1 dereferenceable(1) %call, i8* %1, i32 %call3) #9
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"class.std::__2::__vector_base.57"*, %"class.std::__2::__vector_base.57"** %retval, align 4
  ret %"class.std::__2::__vector_base.57"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIhNS_9allocatorIhEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector.56"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.56"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector.56"* %this, %"class.std::__2::vector.56"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector.56"*, %"class.std::__2::vector.56"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4dataEv(%"class.std::__2::vector.56"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.56"*, align 4
  store %"class.std::__2::vector.56"* %this, %"class.std::__2::vector.56"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.56"*, %"class.std::__2::vector.56"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.56"* %this1 to %"class.std::__2::__vector_base.57"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.57", %"class.std::__2::__vector_base.57"* %0, i32 0, i32 0
  %1 = load i8*, i8** %__begin_, align 4
  %call = call i8* @_ZNSt3__212__to_addressIhEEPT_S2_(i8* %1) #9
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::vector.56"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.56"*, align 4
  store %"class.std::__2::vector.56"* %this, %"class.std::__2::vector.56"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.56"*, %"class.std::__2::vector.56"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.56"* %this1 to %"class.std::__2::__vector_base.57"*
  %call = call i32 @_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::__vector_base.57"* %0) #9
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIhNS_9allocatorIhEEE4sizeEv(%"class.std::__2::vector.56"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.56"*, align 4
  store %"class.std::__2::vector.56"* %this, %"class.std::__2::vector.56"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.56"*, %"class.std::__2::vector.56"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.56"* %this1 to %"class.std::__2::__vector_base.57"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.57", %"class.std::__2::__vector_base.57"* %0, i32 0, i32 1
  %1 = load i8*, i8** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector.56"* %this1 to %"class.std::__2::__vector_base.57"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.57", %"class.std::__2::__vector_base.57"* %2, i32 0, i32 0
  %3 = load i8*, i8** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint i8* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  ret i32 %sub.ptr.sub
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__212__to_addressIhEEPT_S2_(i8* %__p) #0 comdat {
entry:
  %__p.addr = alloca i8*, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %0 = load i8*, i8** %__p.addr, align 4
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE8capacityEv(%"class.std::__2::__vector_base.57"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.57"*, align 4
  store %"class.std::__2::__vector_base.57"* %this, %"class.std::__2::__vector_base.57"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.57"*, %"class.std::__2::__vector_base.57"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE9__end_capEv(%"class.std::__2::__vector_base.57"* %this1) #9
  %0 = load i8*, i8** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.57", %"class.std::__2::__vector_base.57"* %this1, i32 0, i32 0
  %1 = load i8*, i8** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint i8* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  ret i32 %sub.ptr.sub
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__213__vector_baseIhNS_9allocatorIhEEE9__end_capEv(%"class.std::__2::__vector_base.57"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.57"*, align 4
  store %"class.std::__2::__vector_base.57"* %this, %"class.std::__2::__vector_base.57"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.57"*, %"class.std::__2::__vector_base.57"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.57", %"class.std::__2::__vector_base.57"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__217__compressed_pairIPhNS_9allocatorIhEEE5firstEv(%"class.std::__2::__compressed_pair.58"* %__end_cap_) #9
  ret i8** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__217__compressed_pairIPhNS_9allocatorIhEEE5firstEv(%"class.std::__2::__compressed_pair.58"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.58"*, align 4
  store %"class.std::__2::__compressed_pair.58"* %this, %"class.std::__2::__compressed_pair.58"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.58"*, %"class.std::__2::__compressed_pair.58"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.58"* %this1 to %"struct.std::__2::__compressed_pair_elem.59"*
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__222__compressed_pair_elemIPhLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.59"* %0) #9
  ret i8** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__222__compressed_pair_elemIPhLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.59"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.59"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.59"* %this, %"struct.std::__2::__compressed_pair_elem.59"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.59"*, %"struct.std::__2::__compressed_pair_elem.59"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.59", %"struct.std::__2::__compressed_pair_elem.59"* %this1, i32 0, i32 0
  ret i8** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE5clearEv(%"class.std::__2::__vector_base.57"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.57"*, align 4
  store %"class.std::__2::__vector_base.57"* %this, %"class.std::__2::__vector_base.57"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.57"*, %"class.std::__2::__vector_base.57"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.57", %"class.std::__2::__vector_base.57"* %this1, i32 0, i32 0
  %0 = load i8*, i8** %__begin_, align 4
  call void @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE17__destruct_at_endEPh(%"class.std::__2::__vector_base.57"* %this1, i8* %0) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE10deallocateERS2_Phm(%"class.std::__2::allocator.61"* nonnull align 1 dereferenceable(1) %__a, i8* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.61"*, align 4
  %__p.addr = alloca i8*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.61"* %__a, %"class.std::__2::allocator.61"** %__a.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.61"*, %"class.std::__2::allocator.61"** %__a.addr, align 4
  %1 = load i8*, i8** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorIhE10deallocateEPhm(%"class.std::__2::allocator.61"* %0, i8* %1, i32 %2) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.61"* @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE7__allocEv(%"class.std::__2::__vector_base.57"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.57"*, align 4
  store %"class.std::__2::__vector_base.57"* %this, %"class.std::__2::__vector_base.57"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.57"*, %"class.std::__2::__vector_base.57"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.57", %"class.std::__2::__vector_base.57"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.61"* @_ZNSt3__217__compressed_pairIPhNS_9allocatorIhEEE6secondEv(%"class.std::__2::__compressed_pair.58"* %__end_cap_) #9
  ret %"class.std::__2::allocator.61"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE17__destruct_at_endEPh(%"class.std::__2::__vector_base.57"* %this, i8* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.57"*, align 4
  %__new_last.addr = alloca i8*, align 4
  %__soon_to_be_end = alloca i8*, align 4
  store %"class.std::__2::__vector_base.57"* %this, %"class.std::__2::__vector_base.57"** %this.addr, align 4
  store i8* %__new_last, i8** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.57"*, %"class.std::__2::__vector_base.57"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.57", %"class.std::__2::__vector_base.57"* %this1, i32 0, i32 1
  %0 = load i8*, i8** %__end_, align 4
  store i8* %0, i8** %__soon_to_be_end, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load i8*, i8** %__new_last.addr, align 4
  %2 = load i8*, i8** %__soon_to_be_end, align 4
  %cmp = icmp ne i8* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.61"* @_ZNSt3__213__vector_baseIhNS_9allocatorIhEEE7__allocEv(%"class.std::__2::__vector_base.57"* %this1) #9
  %3 = load i8*, i8** %__soon_to_be_end, align 4
  %incdec.ptr = getelementptr inbounds i8, i8* %3, i32 -1
  store i8* %incdec.ptr, i8** %__soon_to_be_end, align 4
  %call2 = call i8* @_ZNSt3__212__to_addressIhEEPT_S2_(i8* %incdec.ptr) #9
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE7destroyIhEEvRS2_PT_(%"class.std::__2::allocator.61"* nonnull align 1 dereferenceable(1) %call, i8* %call2)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %4 = load i8*, i8** %__new_last.addr, align 4
  %__end_3 = getelementptr inbounds %"class.std::__2::__vector_base.57", %"class.std::__2::__vector_base.57"* %this1, i32 0, i32 1
  store i8* %4, i8** %__end_3, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE7destroyIhEEvRS2_PT_(%"class.std::__2::allocator.61"* nonnull align 1 dereferenceable(1) %__a, i8* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.61"*, align 4
  %__p.addr = alloca i8*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy.140", align 1
  store %"class.std::__2::allocator.61"* %__a, %"class.std::__2::allocator.61"** %__a.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy.140"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.61"*, %"class.std::__2::allocator.61"** %__a.addr, align 4
  %2 = load i8*, i8** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE9__destroyIhEEvNS_17integral_constantIbLb1EEERS2_PT_(%"class.std::__2::allocator.61"* nonnull align 1 dereferenceable(1) %1, i8* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIhEEE9__destroyIhEEvNS_17integral_constantIbLb1EEERS2_PT_(%"class.std::__2::allocator.61"* nonnull align 1 dereferenceable(1) %__a, i8* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.61"*, align 4
  %__p.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.61"* %__a, %"class.std::__2::allocator.61"** %__a.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.61"*, %"class.std::__2::allocator.61"** %__a.addr, align 4
  %2 = load i8*, i8** %__p.addr, align 4
  call void @_ZNSt3__29allocatorIhE7destroyEPh(%"class.std::__2::allocator.61"* %1, i8* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIhE7destroyEPh(%"class.std::__2::allocator.61"* %this, i8* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.61"*, align 4
  %__p.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.61"* %this, %"class.std::__2::allocator.61"** %this.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.61"*, %"class.std::__2::allocator.61"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIhE10deallocateEPhm(%"class.std::__2::allocator.61"* %this, i8* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.61"*, align 4
  %__p.addr = alloca i8*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.61"* %this, %"class.std::__2::allocator.61"** %this.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.61"*, %"class.std::__2::allocator.61"** %this.addr, align 4
  %0 = load i8*, i8** %__p.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %1, 1
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %0, i32 %mul, i32 1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.61"* @_ZNSt3__217__compressed_pairIPhNS_9allocatorIhEEE6secondEv(%"class.std::__2::__compressed_pair.58"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.58"*, align 4
  store %"class.std::__2::__compressed_pair.58"* %this, %"class.std::__2::__compressed_pair.58"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.58"*, %"class.std::__2::__compressed_pair.58"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.58"* %this1 to %"struct.std::__2::__compressed_pair_elem.60"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.61"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIhEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.60"* %0) #9
  ret %"class.std::__2::allocator.61"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.61"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIhEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.60"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.60"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.60"* %this, %"struct.std::__2::__compressed_pair_elem.60"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.60"*, %"struct.std::__2::__compressed_pair_elem.60"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.60"* %this1 to %"class.std::__2::allocator.61"*
  ret %"class.std::__2::allocator.61"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::pair.138"* @_ZNSt3__29addressofINS_4pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEEEPT_RSC_(%"struct.std::__2::pair.138"* nonnull align 4 dereferenceable(24) %__x) #0 comdat {
entry:
  %__x.addr = alloca %"struct.std::__2::pair.138"*, align 4
  store %"struct.std::__2::pair.138"* %__x, %"struct.std::__2::pair.138"** %__x.addr, align 4
  %0 = load %"struct.std::__2::pair.138"*, %"struct.std::__2::pair.138"** %__x.addr, align 4
  ret %"struct.std::__2::pair.138"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(24) %"struct.std::__2::pair.138"* @_ZNSt3__217__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEE11__get_valueEv(%"struct.std::__2::__hash_value_type.137"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__hash_value_type.137"*, align 4
  store %"struct.std::__2::__hash_value_type.137"* %this, %"struct.std::__2::__hash_value_type.137"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__hash_value_type.137"*, %"struct.std::__2::__hash_value_type.137"** %this.addr, align 4
  %__cc = getelementptr inbounds %"struct.std::__2::__hash_value_type.137", %"struct.std::__2::__hash_value_type.137"* %this1, i32 0, i32 0
  ret %"struct.std::__2::pair.138"* %__cc
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorINS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEEN5draco10EntryValueEEEPvEEE10deallocateEPSC_m(%"class.std::__2::allocator.10"* %this, %"struct.std::__2::__hash_node.136"* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.10"*, align 4
  %__p.addr = alloca %"struct.std::__2::__hash_node.136"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.10"* %this, %"class.std::__2::allocator.10"** %this.addr, align 4
  store %"struct.std::__2::__hash_node.136"* %__p, %"struct.std::__2::__hash_node.136"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.10"*, %"class.std::__2::allocator.10"** %this.addr, align 4
  %0 = load %"struct.std::__2::__hash_node.136"*, %"struct.std::__2::__hash_node.136"** %__p.addr, align 4
  %1 = bitcast %"struct.std::__2::__hash_node.136"* %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 32
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base"* @_ZNSt3__222__compressed_pair_elemINS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.8"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.8"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.8"* %this, %"struct.std::__2::__compressed_pair_elem.8"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.8"*, %"struct.std::__2::__compressed_pair_elem.8"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.8", %"struct.std::__2::__compressed_pair_elem.8"* %this1, i32 0, i32 0
  ret %"struct.std::__2::__hash_node_base"* %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__210unique_ptrIA_PNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISH_EEEEE5resetEDn(%"class.std::__2::unique_ptr.0"* %this, i8* %0) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.0"*, align 4
  %.addr = alloca i8*, align 4
  %__tmp = alloca %"struct.std::__2::__hash_node_base"**, align 4
  store %"class.std::__2::unique_ptr.0"* %this, %"class.std::__2::unique_ptr.0"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.0"*, %"class.std::__2::unique_ptr.0"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.0", %"class.std::__2::unique_ptr.0"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base"*** @_ZNSt3__217__compressed_pairIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISH_EEEEE5firstEv(%"class.std::__2::__compressed_pair.1"* %__ptr_) #9
  %1 = load %"struct.std::__2::__hash_node_base"**, %"struct.std::__2::__hash_node_base"*** %call, align 4
  store %"struct.std::__2::__hash_node_base"** %1, %"struct.std::__2::__hash_node_base"*** %__tmp, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr.0", %"class.std::__2::unique_ptr.0"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base"*** @_ZNSt3__217__compressed_pairIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISH_EEEEE5firstEv(%"class.std::__2::__compressed_pair.1"* %__ptr_2) #9
  store %"struct.std::__2::__hash_node_base"** null, %"struct.std::__2::__hash_node_base"*** %call3, align 4
  %2 = load %"struct.std::__2::__hash_node_base"**, %"struct.std::__2::__hash_node_base"*** %__tmp, align 4
  %tobool = icmp ne %"struct.std::__2::__hash_node_base"** %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__ptr_4 = getelementptr inbounds %"class.std::__2::unique_ptr.0", %"class.std::__2::unique_ptr.0"* %this1, i32 0, i32 0
  %call5 = call nonnull align 4 dereferenceable(4) %"class.std::__2::__bucket_list_deallocator"* @_ZNSt3__217__compressed_pairIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISH_EEEEE6secondEv(%"class.std::__2::__compressed_pair.1"* %__ptr_4) #9
  %3 = load %"struct.std::__2::__hash_node_base"**, %"struct.std::__2::__hash_node_base"*** %__tmp, align 4
  call void @_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEEclEPSH_(%"class.std::__2::__bucket_list_deallocator"* %call5, %"struct.std::__2::__hash_node_base"** %3) #9
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base"*** @_ZNSt3__217__compressed_pairIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISH_EEEEE5firstEv(%"class.std::__2::__compressed_pair.1"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.1"*, align 4
  store %"class.std::__2::__compressed_pair.1"* %this, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.1"*, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.1"* %this1 to %"struct.std::__2::__compressed_pair_elem.2"*
  %call = call nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base"*** @_ZNSt3__222__compressed_pair_elemIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.2"* %0) #9
  ret %"struct.std::__2::__hash_node_base"*** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__bucket_list_deallocator"* @_ZNSt3__217__compressed_pairIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEENS_25__bucket_list_deallocatorINS7_ISH_EEEEE6secondEv(%"class.std::__2::__compressed_pair.1"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.1"*, align 4
  store %"class.std::__2::__compressed_pair.1"* %this, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.1"*, %"class.std::__2::__compressed_pair.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.1"* %this1 to i8*
  %add.ptr = getelementptr inbounds i8, i8* %0, i32 4
  %1 = bitcast i8* %add.ptr to %"struct.std::__2::__compressed_pair_elem.3"*
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::__bucket_list_deallocator"* @_ZNSt3__222__compressed_pair_elemINS_25__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS2_IcEEEEN5draco10EntryValueEEEPvEEEEEEEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.3"* %1) #9
  ret %"class.std::__2::__bucket_list_deallocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEEclEPSH_(%"class.std::__2::__bucket_list_deallocator"* %this, %"struct.std::__2::__hash_node_base"** %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__bucket_list_deallocator"*, align 4
  %__p.addr = alloca %"struct.std::__2::__hash_node_base"**, align 4
  store %"class.std::__2::__bucket_list_deallocator"* %this, %"class.std::__2::__bucket_list_deallocator"** %this.addr, align 4
  store %"struct.std::__2::__hash_node_base"** %__p, %"struct.std::__2::__hash_node_base"*** %__p.addr, align 4
  %this1 = load %"class.std::__2::__bucket_list_deallocator"*, %"class.std::__2::__bucket_list_deallocator"** %this.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEE7__allocEv(%"class.std::__2::__bucket_list_deallocator"* %this1) #9
  %0 = load %"struct.std::__2::__hash_node_base"**, %"struct.std::__2::__hash_node_base"*** %__p.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEE4sizeEv(%"class.std::__2::__bucket_list_deallocator"* %this1) #9
  %1 = load i32, i32* %call2, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEE10deallocateERSI_PSH_m(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call, %"struct.std::__2::__hash_node_base"** %0, i32 %1) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.std::__2::__hash_node_base"*** @_ZNSt3__222__compressed_pair_elemIPPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEN5draco10EntryValueEEEPvEEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.2"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.2"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.2"* %this, %"struct.std::__2::__compressed_pair_elem.2"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.2"*, %"struct.std::__2::__compressed_pair_elem.2"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.2", %"struct.std::__2::__compressed_pair_elem.2"* %this1, i32 0, i32 0
  ret %"struct.std::__2::__hash_node_base"*** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::__bucket_list_deallocator"* @_ZNSt3__222__compressed_pair_elemINS_25__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS2_IcEEEEN5draco10EntryValueEEEPvEEEEEEEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.3"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.3"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.3"* %this, %"struct.std::__2::__compressed_pair_elem.3"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.3"*, %"struct.std::__2::__compressed_pair_elem.3"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.3", %"struct.std::__2::__compressed_pair_elem.3"* %this1, i32 0, i32 0
  ret %"class.std::__2::__bucket_list_deallocator"* %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEE10deallocateERSI_PSH_m(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, %"struct.std::__2::__hash_node_base"** %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca %"struct.std::__2::__hash_node_base"**, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store %"struct.std::__2::__hash_node_base"** %__p, %"struct.std::__2::__hash_node_base"*** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %1 = load %"struct.std::__2::__hash_node_base"**, %"struct.std::__2::__hash_node_base"*** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEEN5draco10EntryValueEEEPvEEEEE10deallocateEPSG_m(%"class.std::__2::allocator"* %0, %"struct.std::__2::__hash_node_base"** %1, i32 %2) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEE7__allocEv(%"class.std::__2::__bucket_list_deallocator"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__bucket_list_deallocator"*, align 4
  store %"class.std::__2::__bucket_list_deallocator"* %this, %"class.std::__2::__bucket_list_deallocator"** %this.addr, align 4
  %this1 = load %"class.std::__2::__bucket_list_deallocator"*, %"class.std::__2::__bucket_list_deallocator"** %this.addr, align 4
  %__data_ = getelementptr inbounds %"class.std::__2::__bucket_list_deallocator", %"class.std::__2::__bucket_list_deallocator"* %this1, i32 0, i32 0
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__217__compressed_pairImNS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEE6secondEv(%"class.std::__2::__compressed_pair.4"* %__data_) #9
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__225__bucket_list_deallocatorINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEE4sizeEv(%"class.std::__2::__bucket_list_deallocator"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__bucket_list_deallocator"*, align 4
  store %"class.std::__2::__bucket_list_deallocator"* %this, %"class.std::__2::__bucket_list_deallocator"** %this.addr, align 4
  %this1 = load %"class.std::__2::__bucket_list_deallocator"*, %"class.std::__2::__bucket_list_deallocator"** %this.addr, align 4
  %__data_ = getelementptr inbounds %"class.std::__2::__bucket_list_deallocator", %"class.std::__2::__bucket_list_deallocator"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__217__compressed_pairImNS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEE5firstEv(%"class.std::__2::__compressed_pair.4"* %__data_) #9
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS0_IcEEEEN5draco10EntryValueEEEPvEEEEE10deallocateEPSG_m(%"class.std::__2::allocator"* %this, %"struct.std::__2::__hash_node_base"** %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca %"struct.std::__2::__hash_node_base"**, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  store %"struct.std::__2::__hash_node_base"** %__p, %"struct.std::__2::__hash_node_base"*** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  %0 = load %"struct.std::__2::__hash_node_base"**, %"struct.std::__2::__hash_node_base"*** %__p.addr, align 4
  %1 = bitcast %"struct.std::__2::__hash_node_base"** %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__217__compressed_pairImNS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEE6secondEv(%"class.std::__2::__compressed_pair.4"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.4"*, align 4
  store %"class.std::__2::__compressed_pair.4"* %this, %"class.std::__2::__compressed_pair.4"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.4"*, %"class.std::__2::__compressed_pair.4"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.4"* %this1 to %"struct.std::__2::__compressed_pair_elem.6"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.6"* %0) #9
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.6"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.6"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.6"* %this, %"struct.std::__2::__compressed_pair_elem.6"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.6"*, %"struct.std::__2::__compressed_pair_elem.6"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.6"* %this1 to %"class.std::__2::allocator"*
  ret %"class.std::__2::allocator"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__217__compressed_pairImNS_9allocatorIPNS_16__hash_node_baseIPNS_11__hash_nodeINS_17__hash_value_typeINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEN5draco10EntryValueEEEPvEEEEEEE5firstEv(%"class.std::__2::__compressed_pair.4"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.4"*, align 4
  store %"class.std::__2::__compressed_pair.4"* %this, %"class.std::__2::__compressed_pair.4"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.4"*, %"class.std::__2::__compressed_pair.4"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.4"* %this1 to %"struct.std::__2::__compressed_pair_elem.5"*
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__222__compressed_pair_elemImLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.5"* %0) #9
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.50"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco16GeometryMetadataEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.49"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.49"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.49"* %this, %"struct.std::__2::__compressed_pair_elem.49"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.49"*, %"struct.std::__2::__compressed_pair_elem.49"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.49"* %this1 to %"struct.std::__2::default_delete.50"*
  ret %"struct.std::__2::default_delete.50"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNSt3__2eqIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEES7_EEbRKNS_11__wrap_iterIT_EERKNS8_IT0_EE(%"class.std::__2::__wrap_iter"* nonnull align 4 dereferenceable(4) %__x, %"class.std::__2::__wrap_iter"* nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %__x.addr = alloca %"class.std::__2::__wrap_iter"*, align 4
  %__y.addr = alloca %"class.std::__2::__wrap_iter"*, align 4
  store %"class.std::__2::__wrap_iter"* %__x, %"class.std::__2::__wrap_iter"** %__x.addr, align 4
  store %"class.std::__2::__wrap_iter"* %__y, %"class.std::__2::__wrap_iter"** %__y.addr, align 4
  %0 = load %"class.std::__2::__wrap_iter"*, %"class.std::__2::__wrap_iter"** %__x.addr, align 4
  %call = call %"class.std::__2::unique_ptr.96"* @_ZNKSt3__211__wrap_iterIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEE4baseEv(%"class.std::__2::__wrap_iter"* %0) #9
  %1 = load %"class.std::__2::__wrap_iter"*, %"class.std::__2::__wrap_iter"** %__y.addr, align 4
  %call1 = call %"class.std::__2::unique_ptr.96"* @_ZNKSt3__211__wrap_iterIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEE4baseEv(%"class.std::__2::__wrap_iter"* %1) #9
  %cmp = icmp eq %"class.std::__2::unique_ptr.96"* %call, %call1
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.96"* @_ZNKSt3__211__wrap_iterIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEE4baseEv(%"class.std::__2::__wrap_iter"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__wrap_iter"*, align 4
  store %"class.std::__2::__wrap_iter"* %this, %"class.std::__2::__wrap_iter"** %this.addr, align 4
  %this1 = load %"class.std::__2::__wrap_iter"*, %"class.std::__2::__wrap_iter"** %this.addr, align 4
  %__i = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__i, align 4
  ret %"class.std::__2::unique_ptr.96"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco14PointAttributeENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.51"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.51"*, align 4
  store %"class.std::__2::vector.51"* %this, %"class.std::__2::vector.51"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.51"*, %"class.std::__2::vector.51"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.51"* %this1 to %"class.std::__2::__vector_base.52"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.52", %"class.std::__2::__vector_base.52"* %0, i32 0, i32 1
  %1 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector.51"* %this1 to %"class.std::__2::__vector_base.52"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.52", %"class.std::__2::__vector_base.52"* %2, i32 0, i32 0
  %3 = load %"class.std::__2::unique_ptr.53"*, %"class.std::__2::unique_ptr.53"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.std::__2::unique_ptr.53"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.std::__2::unique_ptr.53"* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector.87"* @_ZNSt3__26vectorIiNS_9allocatorIiEEED2Ev(%"class.std::__2::vector.87"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  call void @_ZNKSt3__26vectorIiNS_9allocatorIiEEE17__annotate_deleteEv(%"class.std::__2::vector.87"* %this1) #9
  %0 = bitcast %"class.std::__2::vector.87"* %this1 to %"class.std::__2::__vector_base.88"*
  %call = call %"class.std::__2::__vector_base.88"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEED2Ev(%"class.std::__2::__vector_base.88"* %0) #9
  ret %"class.std::__2::vector.87"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector.94"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEED2Ev(%"class.std::__2::vector.94"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  call void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__annotate_deleteEv(%"class.std::__2::vector.94"* %this1) #9
  %0 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %call = call %"class.std::__2::__vector_base.95"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEED2Ev(%"class.std::__2::__vector_base.95"* %0) #9
  ret %"class.std::__2::vector.94"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIiNS_9allocatorIiEEE17__annotate_deleteEv(%"class.std::__2::vector.87"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  %call = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector.87"* %this1) #9
  %0 = bitcast i32* %call to i8*
  %call2 = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector.87"* %this1) #9
  %call3 = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::vector.87"* %this1) #9
  %add.ptr = getelementptr inbounds i32, i32* %call2, i32 %call3
  %1 = bitcast i32* %add.ptr to i8*
  %call4 = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector.87"* %this1) #9
  %call5 = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4sizeEv(%"class.std::__2::vector.87"* %this1) #9
  %add.ptr6 = getelementptr inbounds i32, i32* %call4, i32 %call5
  %2 = bitcast i32* %add.ptr6 to i8*
  %call7 = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector.87"* %this1) #9
  %call8 = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::vector.87"* %this1) #9
  %add.ptr9 = getelementptr inbounds i32, i32* %call7, i32 %call8
  %3 = bitcast i32* %add.ptr9 to i8*
  call void @_ZNKSt3__26vectorIiNS_9allocatorIiEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector.87"* %this1, i8* %0, i8* %1, i8* %2, i8* %3) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base.88"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEED2Ev(%"class.std::__2::__vector_base.88"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__vector_base.88"*, align 4
  %this.addr = alloca %"class.std::__2::__vector_base.88"*, align 4
  store %"class.std::__2::__vector_base.88"* %this, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.88"*, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  store %"class.std::__2::__vector_base.88"* %this1, %"class.std::__2::__vector_base.88"** %retval, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__begin_, align 4
  %cmp = icmp ne i32* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE5clearEv(%"class.std::__2::__vector_base.88"* %this1) #9
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE7__allocEv(%"class.std::__2::__vector_base.88"* %this1) #9
  %__begin_2 = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %this1, i32 0, i32 0
  %1 = load i32*, i32** %__begin_2, align 4
  %call3 = call i32 @_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::__vector_base.88"* %this1) #9
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE10deallocateERS2_Pim(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %call, i32* %1, i32 %call3) #9
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"class.std::__2::__vector_base.88"*, %"class.std::__2::__vector_base.88"** %retval, align 4
  ret %"class.std::__2::__vector_base.88"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIiNS_9allocatorIiEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector.87"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector.87"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.87"* %this1 to %"class.std::__2::__vector_base.88"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %0, i32 0, i32 0
  %1 = load i32*, i32** %__begin_, align 4
  %call = call i32* @_ZNSt3__212__to_addressIiEEPT_S2_(i32* %1) #9
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::vector.87"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.87"* %this1 to %"class.std::__2::__vector_base.88"*
  %call = call i32 @_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::__vector_base.88"* %0) #9
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNSt3__212__to_addressIiEEPT_S2_(i32* %__p) #0 comdat {
entry:
  %__p.addr = alloca i32*, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %0 = load i32*, i32** %__p.addr, align 4
  ret i32* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::__vector_base.88"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.88"*, align 4
  store %"class.std::__2::__vector_base.88"* %this, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.88"*, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE9__end_capEv(%"class.std::__2::__vector_base.88"* %this1) #9
  %0 = load i32*, i32** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %this1, i32 0, i32 0
  %1 = load i32*, i32** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint i32* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint i32* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE9__end_capEv(%"class.std::__2::__vector_base.88"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.88"*, align 4
  store %"class.std::__2::__vector_base.88"* %this, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.88"*, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__217__compressed_pairIPiNS_9allocatorIiEEE5firstEv(%"class.std::__2::__compressed_pair.89"* %__end_cap_) #9
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__217__compressed_pairIPiNS_9allocatorIiEEE5firstEv(%"class.std::__2::__compressed_pair.89"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.89"*, align 4
  store %"class.std::__2::__compressed_pair.89"* %this, %"class.std::__2::__compressed_pair.89"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.89"*, %"class.std::__2::__compressed_pair.89"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.89"* %this1 to %"struct.std::__2::__compressed_pair_elem.90"*
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__222__compressed_pair_elemIPiLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.90"* %0) #9
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__222__compressed_pair_elemIPiLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.90"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.90"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.90"* %this, %"struct.std::__2::__compressed_pair_elem.90"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.90"*, %"struct.std::__2::__compressed_pair_elem.90"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.90", %"struct.std::__2::__compressed_pair_elem.90"* %this1, i32 0, i32 0
  ret i32** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE5clearEv(%"class.std::__2::__vector_base.88"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.88"*, align 4
  store %"class.std::__2::__vector_base.88"* %this, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.88"*, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__begin_, align 4
  call void @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE17__destruct_at_endEPi(%"class.std::__2::__vector_base.88"* %this1, i32* %0) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE10deallocateERS2_Pim(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %__a, i32* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.92"*, align 4
  %__p.addr = alloca i32*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.92"* %__a, %"class.std::__2::allocator.92"** %__a.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.92"*, %"class.std::__2::allocator.92"** %__a.addr, align 4
  %1 = load i32*, i32** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorIiE10deallocateEPim(%"class.std::__2::allocator.92"* %0, i32* %1, i32 %2) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE7__allocEv(%"class.std::__2::__vector_base.88"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.88"*, align 4
  store %"class.std::__2::__vector_base.88"* %this, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.88"*, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__217__compressed_pairIPiNS_9allocatorIiEEE6secondEv(%"class.std::__2::__compressed_pair.89"* %__end_cap_) #9
  ret %"class.std::__2::allocator.92"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE17__destruct_at_endEPi(%"class.std::__2::__vector_base.88"* %this, i32* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.88"*, align 4
  %__new_last.addr = alloca i32*, align 4
  %__soon_to_be_end = alloca i32*, align 4
  store %"class.std::__2::__vector_base.88"* %this, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  store i32* %__new_last, i32** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.88"*, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %this1, i32 0, i32 1
  %0 = load i32*, i32** %__end_, align 4
  store i32* %0, i32** %__soon_to_be_end, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load i32*, i32** %__new_last.addr, align 4
  %2 = load i32*, i32** %__soon_to_be_end, align 4
  %cmp = icmp ne i32* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE7__allocEv(%"class.std::__2::__vector_base.88"* %this1) #9
  %3 = load i32*, i32** %__soon_to_be_end, align 4
  %incdec.ptr = getelementptr inbounds i32, i32* %3, i32 -1
  store i32* %incdec.ptr, i32** %__soon_to_be_end, align 4
  %call2 = call i32* @_ZNSt3__212__to_addressIiEEPT_S2_(i32* %incdec.ptr) #9
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE7destroyIiEEvRS2_PT_(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %call, i32* %call2)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %4 = load i32*, i32** %__new_last.addr, align 4
  %__end_3 = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %this1, i32 0, i32 1
  store i32* %4, i32** %__end_3, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE7destroyIiEEvRS2_PT_(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %__a, i32* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.92"*, align 4
  %__p.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy.141", align 1
  store %"class.std::__2::allocator.92"* %__a, %"class.std::__2::allocator.92"** %__a.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy.141"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.92"*, %"class.std::__2::allocator.92"** %__a.addr, align 4
  %2 = load i32*, i32** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE9__destroyIiEEvNS_17integral_constantIbLb1EEERS2_PT_(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %1, i32* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE9__destroyIiEEvNS_17integral_constantIbLb1EEERS2_PT_(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %__a, i32* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.92"*, align 4
  %__p.addr = alloca i32*, align 4
  store %"class.std::__2::allocator.92"* %__a, %"class.std::__2::allocator.92"** %__a.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.92"*, %"class.std::__2::allocator.92"** %__a.addr, align 4
  %2 = load i32*, i32** %__p.addr, align 4
  call void @_ZNSt3__29allocatorIiE7destroyEPi(%"class.std::__2::allocator.92"* %1, i32* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIiE7destroyEPi(%"class.std::__2::allocator.92"* %this, i32* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.92"*, align 4
  %__p.addr = alloca i32*, align 4
  store %"class.std::__2::allocator.92"* %this, %"class.std::__2::allocator.92"** %this.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.92"*, %"class.std::__2::allocator.92"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIiE10deallocateEPim(%"class.std::__2::allocator.92"* %this, i32* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.92"*, align 4
  %__p.addr = alloca i32*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.92"* %this, %"class.std::__2::allocator.92"** %this.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.92"*, %"class.std::__2::allocator.92"** %this.addr, align 4
  %0 = load i32*, i32** %__p.addr, align 4
  %1 = bitcast i32* %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__217__compressed_pairIPiNS_9allocatorIiEEE6secondEv(%"class.std::__2::__compressed_pair.89"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.89"*, align 4
  store %"class.std::__2::__compressed_pair.89"* %this, %"class.std::__2::__compressed_pair.89"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.89"*, %"class.std::__2::__compressed_pair.89"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.89"* %this1 to %"struct.std::__2::__compressed_pair_elem.91"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIiEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.91"* %0) #9
  ret %"class.std::__2::allocator.92"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIiEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.91"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.91"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.91"* %this, %"struct.std::__2::__compressed_pair_elem.91"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.91"*, %"struct.std::__2::__compressed_pair_elem.91"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.91"* %this1 to %"class.std::__2::allocator.92"*
  ret %"class.std::__2::allocator.92"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__annotate_deleteEv(%"class.std::__2::vector.94"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  %call = call %"class.std::__2::unique_ptr.96"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.94"* %this1) #9
  %0 = bitcast %"class.std::__2::unique_ptr.96"* %call to i8*
  %call2 = call %"class.std::__2::unique_ptr.96"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.94"* %this1) #9
  %call3 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector.94"* %this1) #9
  %add.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %call2, i32 %call3
  %1 = bitcast %"class.std::__2::unique_ptr.96"* %add.ptr to i8*
  %call4 = call %"class.std::__2::unique_ptr.96"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.94"* %this1) #9
  %call5 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.94"* %this1) #9
  %add.ptr6 = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %call4, i32 %call5
  %2 = bitcast %"class.std::__2::unique_ptr.96"* %add.ptr6 to i8*
  %call7 = call %"class.std::__2::unique_ptr.96"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.94"* %this1) #9
  %call8 = call i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector.94"* %this1) #9
  %add.ptr9 = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %call7, i32 %call8
  %3 = bitcast %"class.std::__2::unique_ptr.96"* %add.ptr9 to i8*
  call void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE31__annotate_contiguous_containerEPKvSB_SB_SB_(%"class.std::__2::vector.94"* %this1, i8* %0, i8* %1, i8* %2, i8* %3) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base.95"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEED2Ev(%"class.std::__2::__vector_base.95"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__vector_base.95"*, align 4
  %this.addr = alloca %"class.std::__2::__vector_base.95"*, align 4
  store %"class.std::__2::__vector_base.95"* %this, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.95"*, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  store %"class.std::__2::__vector_base.95"* %this1, %"class.std::__2::__vector_base.95"** %retval, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__begin_, align 4
  %cmp = icmp ne %"class.std::__2::unique_ptr.96"* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5clearEv(%"class.std::__2::__vector_base.95"* %this1) #9
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base.95"* %this1) #9
  %__begin_2 = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__begin_2, align 4
  %call3 = call i32 @_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::__vector_base.95"* %this1) #9
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE10deallocateERS8_PS7_m(%"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %call, %"class.std::__2::unique_ptr.96"* %1, i32 %call3) #9
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"class.std::__2::__vector_base.95"*, %"class.std::__2::__vector_base.95"** %retval, align 4
  ret %"class.std::__2::__vector_base.95"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE31__annotate_contiguous_containerEPKvSB_SB_SB_(%"class.std::__2::vector.94"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.96"* @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4dataEv(%"class.std::__2::vector.94"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %0, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__begin_, align 4
  %call = call %"class.std::__2::unique_ptr.96"* @_ZNSt3__212__to_addressINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEEPT_S8_(%"class.std::__2::unique_ptr.96"* %1) #9
  ret %"class.std::__2::unique_ptr.96"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::vector.94"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %call = call i32 @_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::__vector_base.95"* %0) #9
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE4sizeEv(%"class.std::__2::vector.94"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %0, i32 0, i32 1
  %1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector.94"* %this1 to %"class.std::__2::__vector_base.95"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %2, i32 0, i32 0
  %3 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.std::__2::unique_ptr.96"* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.std::__2::unique_ptr.96"* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.96"* @_ZNSt3__212__to_addressINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEEPT_S8_(%"class.std::__2::unique_ptr.96"* %__p) #0 comdat {
entry:
  %__p.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  store %"class.std::__2::unique_ptr.96"* %__p, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  ret %"class.std::__2::unique_ptr.96"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE8capacityEv(%"class.std::__2::__vector_base.95"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.95"*, align 4
  store %"class.std::__2::__vector_base.95"* %this, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.95"*, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"** @_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE9__end_capEv(%"class.std::__2::__vector_base.95"* %this1) #9
  %0 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint %"class.std::__2::unique_ptr.96"* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint %"class.std::__2::unique_ptr.96"* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"** @_ZNKSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE9__end_capEv(%"class.std::__2::__vector_base.95"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.95"*, align 4
  store %"class.std::__2::__vector_base.95"* %this, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.95"*, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"** @_ZNKSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.101"* %__end_cap_) #9
  ret %"class.std::__2::unique_ptr.96"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"** @_ZNKSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5firstEv(%"class.std::__2::__compressed_pair.101"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.101"*, align 4
  store %"class.std::__2::__compressed_pair.101"* %this, %"class.std::__2::__compressed_pair.101"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.101"*, %"class.std::__2::__compressed_pair.101"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.101"* %this1 to %"struct.std::__2::__compressed_pair_elem.102"*
  %call = call nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"** @_ZNKSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.102"* %0) #9
  ret %"class.std::__2::unique_ptr.96"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.std::__2::unique_ptr.96"** @_ZNKSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.102"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.102"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.102"* %this, %"struct.std::__2::__compressed_pair_elem.102"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.102"*, %"struct.std::__2::__compressed_pair_elem.102"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.102", %"struct.std::__2::__compressed_pair_elem.102"* %this1, i32 0, i32 0
  ret %"class.std::__2::unique_ptr.96"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE5clearEv(%"class.std::__2::__vector_base.95"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.95"*, align 4
  store %"class.std::__2::__vector_base.95"* %this, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.95"*, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__begin_, align 4
  call void @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__destruct_at_endEPS6_(%"class.std::__2::__vector_base.95"* %this1, %"class.std::__2::unique_ptr.96"* %0) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE10deallocateERS8_PS7_m(%"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %__a, %"class.std::__2::unique_ptr.96"* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.104"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.104"* %__a, %"class.std::__2::allocator.104"** %__a.addr, align 4
  store %"class.std::__2::unique_ptr.96"* %__p, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.104"*, %"class.std::__2::allocator.104"** %__a.addr, align 4
  %1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEE10deallocateEPS6_m(%"class.std::__2::allocator.104"* %0, %"class.std::__2::unique_ptr.96"* %1, i32 %2) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base.95"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.95"*, align 4
  store %"class.std::__2::__vector_base.95"* %this, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.95"*, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE6secondEv(%"class.std::__2::__compressed_pair.101"* %__end_cap_) #9
  ret %"class.std::__2::allocator.104"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE17__destruct_at_endEPS6_(%"class.std::__2::__vector_base.95"* %this, %"class.std::__2::unique_ptr.96"* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.95"*, align 4
  %__new_last.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  %__soon_to_be_end = alloca %"class.std::__2::unique_ptr.96"*, align 4
  store %"class.std::__2::__vector_base.95"* %this, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.96"* %__new_last, %"class.std::__2::unique_ptr.96"** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.95"*, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %this1, i32 0, i32 1
  %0 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__end_, align 4
  store %"class.std::__2::unique_ptr.96"* %0, %"class.std::__2::unique_ptr.96"** %__soon_to_be_end, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__new_last.addr, align 4
  %2 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__soon_to_be_end, align 4
  %cmp = icmp ne %"class.std::__2::unique_ptr.96"* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE7__allocEv(%"class.std::__2::__vector_base.95"* %this1) #9
  %3 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__soon_to_be_end, align 4
  %incdec.ptr = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %3, i32 -1
  store %"class.std::__2::unique_ptr.96"* %incdec.ptr, %"class.std::__2::unique_ptr.96"** %__soon_to_be_end, align 4
  %call2 = call %"class.std::__2::unique_ptr.96"* @_ZNSt3__212__to_addressINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEEPT_S8_(%"class.std::__2::unique_ptr.96"* %incdec.ptr) #9
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE7destroyIS7_EEvRS8_PT_(%"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %call, %"class.std::__2::unique_ptr.96"* %call2)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %4 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__new_last.addr, align 4
  %__end_3 = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %this1, i32 0, i32 1
  store %"class.std::__2::unique_ptr.96"* %4, %"class.std::__2::unique_ptr.96"** %__end_3, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE7destroyIS7_EEvRS8_PT_(%"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %__a, %"class.std::__2::unique_ptr.96"* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.104"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy.142", align 1
  store %"class.std::__2::allocator.104"* %__a, %"class.std::__2::allocator.104"** %__a.addr, align 4
  store %"class.std::__2::unique_ptr.96"* %__p, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy.142"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.104"*, %"class.std::__2::allocator.104"** %__a.addr, align 4
  %2 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE9__destroyIS7_EEvNS_17integral_constantIbLb1EEERS8_PT_(%"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %1, %"class.std::__2::unique_ptr.96"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEEE9__destroyIS7_EEvNS_17integral_constantIbLb1EEERS8_PT_(%"class.std::__2::allocator.104"* nonnull align 1 dereferenceable(1) %__a, %"class.std::__2::unique_ptr.96"* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.104"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  store %"class.std::__2::allocator.104"* %__a, %"class.std::__2::allocator.104"** %__a.addr, align 4
  store %"class.std::__2::unique_ptr.96"* %__p, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.104"*, %"class.std::__2::allocator.104"** %__a.addr, align 4
  %2 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  call void @_ZNSt3__29allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEE7destroyEPS6_(%"class.std::__2::allocator.104"* %1, %"class.std::__2::unique_ptr.96"* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEE7destroyEPS6_(%"class.std::__2::allocator.104"* %this, %"class.std::__2::unique_ptr.96"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.104"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  store %"class.std::__2::allocator.104"* %this, %"class.std::__2::allocator.104"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.96"* %__p, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.104"*, %"class.std::__2::allocator.104"** %this.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  %call = call %"class.std::__2::unique_ptr.96"* @_ZNSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.96"* %0) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.96"* @_ZNSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEED2Ev(%"class.std::__2::unique_ptr.96"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  store %"class.std::__2::unique_ptr.96"* %this, %"class.std::__2::unique_ptr.96"** %this.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %this.addr, align 4
  call void @_ZNSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.96"* %this1, %"class.draco::AttributesDecoderInterface"* null) #9
  ret %"class.std::__2::unique_ptr.96"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__210unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE5resetEPS2_(%"class.std::__2::unique_ptr.96"* %this, %"class.draco::AttributesDecoderInterface"* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  %__p.addr = alloca %"class.draco::AttributesDecoderInterface"*, align 4
  %__tmp = alloca %"class.draco::AttributesDecoderInterface"*, align 4
  store %"class.std::__2::unique_ptr.96"* %this, %"class.std::__2::unique_ptr.96"** %this.addr, align 4
  store %"class.draco::AttributesDecoderInterface"* %__p, %"class.draco::AttributesDecoderInterface"** %__p.addr, align 4
  %this1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %this.addr, align 4
  %__ptr_ = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributesDecoderInterface"** @_ZNSt3__217__compressed_pairIPN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.97"* %__ptr_) #9
  %0 = load %"class.draco::AttributesDecoderInterface"*, %"class.draco::AttributesDecoderInterface"** %call, align 4
  store %"class.draco::AttributesDecoderInterface"* %0, %"class.draco::AttributesDecoderInterface"** %__tmp, align 4
  %1 = load %"class.draco::AttributesDecoderInterface"*, %"class.draco::AttributesDecoderInterface"** %__p.addr, align 4
  %__ptr_2 = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) %"class.draco::AttributesDecoderInterface"** @_ZNSt3__217__compressed_pairIPN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.97"* %__ptr_2) #9
  store %"class.draco::AttributesDecoderInterface"* %1, %"class.draco::AttributesDecoderInterface"** %call3, align 4
  %2 = load %"class.draco::AttributesDecoderInterface"*, %"class.draco::AttributesDecoderInterface"** %__tmp, align 4
  %tobool = icmp ne %"class.draco::AttributesDecoderInterface"* %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %__ptr_4 = getelementptr inbounds %"class.std::__2::unique_ptr.96", %"class.std::__2::unique_ptr.96"* %this1, i32 0, i32 0
  %call5 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.100"* @_ZNSt3__217__compressed_pairIPN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.97"* %__ptr_4) #9
  %3 = load %"class.draco::AttributesDecoderInterface"*, %"class.draco::AttributesDecoderInterface"** %__tmp, align 4
  call void @_ZNKSt3__214default_deleteIN5draco26AttributesDecoderInterfaceEEclEPS2_(%"struct.std::__2::default_delete.100"* %call5, %"class.draco::AttributesDecoderInterface"* %3) #9
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::AttributesDecoderInterface"** @_ZNSt3__217__compressed_pairIPN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.97"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.97"*, align 4
  store %"class.std::__2::__compressed_pair.97"* %this, %"class.std::__2::__compressed_pair.97"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.97"*, %"class.std::__2::__compressed_pair.97"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.97"* %this1 to %"struct.std::__2::__compressed_pair_elem.98"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributesDecoderInterface"** @_ZNSt3__222__compressed_pair_elemIPN5draco26AttributesDecoderInterfaceELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.98"* %0) #9
  ret %"class.draco::AttributesDecoderInterface"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.100"* @_ZNSt3__217__compressed_pairIPN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE6secondEv(%"class.std::__2::__compressed_pair.97"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.97"*, align 4
  store %"class.std::__2::__compressed_pair.97"* %this, %"class.std::__2::__compressed_pair.97"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.97"*, %"class.std::__2::__compressed_pair.97"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.97"* %this1 to %"struct.std::__2::__compressed_pair_elem.99"*
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.100"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco26AttributesDecoderInterfaceEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.99"* %0) #9
  ret %"struct.std::__2::default_delete.100"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__214default_deleteIN5draco26AttributesDecoderInterfaceEEclEPS2_(%"struct.std::__2::default_delete.100"* %this, %"class.draco::AttributesDecoderInterface"* %__ptr) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::default_delete.100"*, align 4
  %__ptr.addr = alloca %"class.draco::AttributesDecoderInterface"*, align 4
  store %"struct.std::__2::default_delete.100"* %this, %"struct.std::__2::default_delete.100"** %this.addr, align 4
  store %"class.draco::AttributesDecoderInterface"* %__ptr, %"class.draco::AttributesDecoderInterface"** %__ptr.addr, align 4
  %this1 = load %"struct.std::__2::default_delete.100"*, %"struct.std::__2::default_delete.100"** %this.addr, align 4
  %0 = load %"class.draco::AttributesDecoderInterface"*, %"class.draco::AttributesDecoderInterface"** %__ptr.addr, align 4
  %isnull = icmp eq %"class.draco::AttributesDecoderInterface"* %0, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %entry
  %1 = bitcast %"class.draco::AttributesDecoderInterface"* %0 to void (%"class.draco::AttributesDecoderInterface"*)***
  %vtable = load void (%"class.draco::AttributesDecoderInterface"*)**, void (%"class.draco::AttributesDecoderInterface"*)*** %1, align 4
  %vfn = getelementptr inbounds void (%"class.draco::AttributesDecoderInterface"*)*, void (%"class.draco::AttributesDecoderInterface"*)** %vtable, i64 1
  %2 = load void (%"class.draco::AttributesDecoderInterface"*)*, void (%"class.draco::AttributesDecoderInterface"*)** %vfn, align 4
  call void %2(%"class.draco::AttributesDecoderInterface"* %0) #9
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::AttributesDecoderInterface"** @_ZNSt3__222__compressed_pair_elemIPN5draco26AttributesDecoderInterfaceELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.98"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.98"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.98"* %this, %"struct.std::__2::__compressed_pair_elem.98"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.98"*, %"struct.std::__2::__compressed_pair_elem.98"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.98", %"struct.std::__2::__compressed_pair_elem.98"* %this1, i32 0, i32 0
  ret %"class.draco::AttributesDecoderInterface"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.100"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco26AttributesDecoderInterfaceEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.99"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.99"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.99"* %this, %"struct.std::__2::__compressed_pair_elem.99"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.99"*, %"struct.std::__2::__compressed_pair_elem.99"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.99"* %this1 to %"struct.std::__2::default_delete.100"*
  ret %"struct.std::__2::default_delete.100"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEE10deallocateEPS6_m(%"class.std::__2::allocator.104"* %this, %"class.std::__2::unique_ptr.96"* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.104"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.104"* %this, %"class.std::__2::allocator.104"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.96"* %__p, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.104"*, %"class.std::__2::allocator.104"** %this.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  %1 = bitcast %"class.std::__2::unique_ptr.96"* %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE6secondEv(%"class.std::__2::__compressed_pair.101"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.101"*, align 4
  store %"class.std::__2::__compressed_pair.101"* %this, %"class.std::__2::__compressed_pair.101"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.101"*, %"class.std::__2::__compressed_pair.101"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.101"* %this1 to %"struct.std::__2::__compressed_pair_elem.103"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.103"* %0) #9
  ret %"class.std::__2::allocator.104"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.104"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.103"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.103"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.103"* %this, %"struct.std::__2::__compressed_pair_elem.103"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.103"*, %"struct.std::__2::__compressed_pair_elem.103"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.103"* %this1 to %"class.std::__2::allocator.104"*
  ret %"class.std::__2::allocator.104"* %0
}

; Function Attrs: cold noreturn nounwind
declare void @llvm.trap() #6

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::GeometryMetadata"** @_ZNKSt3__217__compressed_pairIPN5draco16GeometryMetadataENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::GeometryMetadata"** @_ZNKSt3__222__compressed_pair_elemIPN5draco16GeometryMetadataELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %0) #9
  ret %"class.draco::GeometryMetadata"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::GeometryMetadata"** @_ZNKSt3__222__compressed_pair_elemIPN5draco16GeometryMetadataELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem"*, align 4
  store %"struct.std::__2::__compressed_pair_elem"* %this, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem"*, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem"* %this1, i32 0, i32 0
  ret %"class.draco::GeometryMetadata"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair"* @_ZNSt3__217__compressed_pairIPN5draco16GeometryMetadataENS_14default_deleteIS2_EEEC2IRS3_NS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair"* returned %this, %"class.draco::GeometryMetadata"** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  %__t1.addr = alloca %"class.draco::GeometryMetadata"**, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  store %"class.draco::GeometryMetadata"** %__t1, %"class.draco::GeometryMetadata"*** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %1 = load %"class.draco::GeometryMetadata"**, %"class.draco::GeometryMetadata"*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::GeometryMetadata"** @_ZNSt3__27forwardIRPN5draco16GeometryMetadataEEEOT_RNS_16remove_referenceIS5_E4typeE(%"class.draco::GeometryMetadata"** nonnull align 4 dereferenceable(4) %1) #9
  %call2 = call %"struct.std::__2::__compressed_pair_elem"* @_ZNSt3__222__compressed_pair_elemIPN5draco16GeometryMetadataELi0ELb0EEC2IRS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem"* %0, %"class.draco::GeometryMetadata"** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem.49"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #9
  %call4 = call %"struct.std::__2::__compressed_pair_elem.49"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco16GeometryMetadataEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.49"* %2)
  ret %"class.std::__2::__compressed_pair"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::GeometryMetadata"** @_ZNSt3__27forwardIRPN5draco16GeometryMetadataEEEOT_RNS_16remove_referenceIS5_E4typeE(%"class.draco::GeometryMetadata"** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.draco::GeometryMetadata"**, align 4
  store %"class.draco::GeometryMetadata"** %__t, %"class.draco::GeometryMetadata"*** %__t.addr, align 4
  %0 = load %"class.draco::GeometryMetadata"**, %"class.draco::GeometryMetadata"*** %__t.addr, align 4
  ret %"class.draco::GeometryMetadata"** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem"* @_ZNSt3__222__compressed_pair_elemIPN5draco16GeometryMetadataELi0ELb0EEC2IRS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem"* returned %this, %"class.draco::GeometryMetadata"** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem"*, align 4
  %__u.addr = alloca %"class.draco::GeometryMetadata"**, align 4
  store %"struct.std::__2::__compressed_pair_elem"* %this, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  store %"class.draco::GeometryMetadata"** %__u, %"class.draco::GeometryMetadata"*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem"*, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem"* %this1, i32 0, i32 0
  %0 = load %"class.draco::GeometryMetadata"**, %"class.draco::GeometryMetadata"*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::GeometryMetadata"** @_ZNSt3__27forwardIRPN5draco16GeometryMetadataEEEOT_RNS_16remove_referenceIS5_E4typeE(%"class.draco::GeometryMetadata"** nonnull align 4 dereferenceable(4) %0) #9
  %1 = load %"class.draco::GeometryMetadata"*, %"class.draco::GeometryMetadata"** %call, align 4
  store %"class.draco::GeometryMetadata"* %1, %"class.draco::GeometryMetadata"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.49"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco16GeometryMetadataEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.49"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.49"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.49"* %this, %"struct.std::__2::__compressed_pair_elem.49"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.49"*, %"struct.std::__2::__compressed_pair_elem.49"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.49"* %this1 to %"struct.std::__2::default_delete.50"*
  ret %"struct.std::__2::__compressed_pair_elem.49"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base.95"* @_ZNSt3__213__vector_baseINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEC2Ev(%"class.std::__2::__vector_base.95"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.95"*, align 4
  %ref.tmp = alloca i8*, align 4
  %ref.tmp2 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__vector_base.95"* %this, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.95"*, %"class.std::__2::__vector_base.95"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__vector_base.95"* %this1 to %"class.std::__2::__vector_base_common"*
  %call = call %"class.std::__2::__vector_base_common"* @_ZNSt3__220__vector_base_commonILb1EEC2Ev(%"class.std::__2::__vector_base_common"* %0)
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %this1, i32 0, i32 0
  store %"class.std::__2::unique_ptr.96"* null, %"class.std::__2::unique_ptr.96"** %__begin_, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %this1, i32 0, i32 1
  store %"class.std::__2::unique_ptr.96"* null, %"class.std::__2::unique_ptr.96"** %__end_, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.95", %"class.std::__2::__vector_base.95"* %this1, i32 0, i32 2
  store i8* null, i8** %ref.tmp, align 4
  %call3 = call %"class.std::__2::__compressed_pair.101"* @_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEC2IDnNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.101"* %__end_cap_, i8** nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  ret %"class.std::__2::__vector_base.95"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.101"* @_ZNSt3__217__compressed_pairIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEEC2IDnNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.101"* returned %this, i8** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.101"*, align 4
  %__t1.addr = alloca i8**, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.101"* %this, %"class.std::__2::__compressed_pair.101"** %this.addr, align 4
  store i8** %__t1, i8*** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.101"*, %"class.std::__2::__compressed_pair.101"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.101"* %this1 to %"struct.std::__2::__compressed_pair_elem.102"*
  %1 = load i8**, i8*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %1) #9
  %call2 = call %"struct.std::__2::__compressed_pair_elem.102"* @_ZNSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEELi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.102"* %0, i8** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.101"* %this1 to %"struct.std::__2::__compressed_pair_elem.103"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #9
  %call4 = call %"struct.std::__2::__compressed_pair_elem.103"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.103"* %2)
  ret %"class.std::__2::__compressed_pair.101"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.102"* @_ZNSt3__222__compressed_pair_elemIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEELi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.102"* returned %this, i8** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.102"*, align 4
  %__u.addr = alloca i8**, align 4
  store %"struct.std::__2::__compressed_pair_elem.102"* %this, %"struct.std::__2::__compressed_pair_elem.102"** %this.addr, align 4
  store i8** %__u, i8*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.102"*, %"struct.std::__2::__compressed_pair_elem.102"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.102", %"struct.std::__2::__compressed_pair_elem.102"* %this1, i32 0, i32 0
  %0 = load i8**, i8*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %0) #9
  store %"class.std::__2::unique_ptr.96"* null, %"class.std::__2::unique_ptr.96"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.102"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.103"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS4_EEEEEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.103"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.103"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.103"* %this, %"struct.std::__2::__compressed_pair_elem.103"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.103"*, %"struct.std::__2::__compressed_pair_elem.103"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.103"* %this1 to %"class.std::__2::allocator.104"*
  %call = call %"class.std::__2::allocator.104"* @_ZNSt3__29allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEC2Ev(%"class.std::__2::allocator.104"* %1) #9
  ret %"struct.std::__2::__compressed_pair_elem.103"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::allocator.104"* @_ZNSt3__29allocatorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEC2Ev(%"class.std::__2::allocator.104"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.104"*, align 4
  store %"class.std::__2::allocator.104"* %this, %"class.std::__2::allocator.104"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.104"*, %"class.std::__2::allocator.104"** %this.addr, align 4
  ret %"class.std::__2::allocator.104"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base.88"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEEC2Ev(%"class.std::__2::__vector_base.88"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.88"*, align 4
  %ref.tmp = alloca i8*, align 4
  %ref.tmp2 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__vector_base.88"* %this, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.88"*, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__vector_base.88"* %this1 to %"class.std::__2::__vector_base_common"*
  %call = call %"class.std::__2::__vector_base_common"* @_ZNSt3__220__vector_base_commonILb1EEC2Ev(%"class.std::__2::__vector_base_common"* %0)
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %this1, i32 0, i32 0
  store i32* null, i32** %__begin_, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %this1, i32 0, i32 1
  store i32* null, i32** %__end_, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %this1, i32 0, i32 2
  store i8* null, i8** %ref.tmp, align 4
  %call3 = call %"class.std::__2::__compressed_pair.89"* @_ZNSt3__217__compressed_pairIPiNS_9allocatorIiEEEC2IDnNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.89"* %__end_cap_, i8** nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  ret %"class.std::__2::__vector_base.88"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.89"* @_ZNSt3__217__compressed_pairIPiNS_9allocatorIiEEEC2IDnNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair.89"* returned %this, i8** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.89"*, align 4
  %__t1.addr = alloca i8**, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair.89"* %this, %"class.std::__2::__compressed_pair.89"** %this.addr, align 4
  store i8** %__t1, i8*** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.89"*, %"class.std::__2::__compressed_pair.89"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.89"* %this1 to %"struct.std::__2::__compressed_pair_elem.90"*
  %1 = load i8**, i8*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %1) #9
  %call2 = call %"struct.std::__2::__compressed_pair_elem.90"* @_ZNSt3__222__compressed_pair_elemIPiLi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.90"* %0, i8** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.89"* %this1 to %"struct.std::__2::__compressed_pair_elem.91"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #9
  %call4 = call %"struct.std::__2::__compressed_pair_elem.91"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIiEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.91"* %2)
  ret %"class.std::__2::__compressed_pair.89"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.90"* @_ZNSt3__222__compressed_pair_elemIPiLi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.90"* returned %this, i8** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.90"*, align 4
  %__u.addr = alloca i8**, align 4
  store %"struct.std::__2::__compressed_pair_elem.90"* %this, %"struct.std::__2::__compressed_pair_elem.90"** %this.addr, align 4
  store i8** %__u, i8*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.90"*, %"struct.std::__2::__compressed_pair_elem.90"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.90", %"struct.std::__2::__compressed_pair_elem.90"* %this1, i32 0, i32 0
  %0 = load i8**, i8*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %0) #9
  store i32* null, i32** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.90"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.91"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIiEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.91"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.91"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.91"* %this, %"struct.std::__2::__compressed_pair_elem.91"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.91"*, %"struct.std::__2::__compressed_pair_elem.91"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.91"* %this1 to %"class.std::__2::allocator.92"*
  %call = call %"class.std::__2::allocator.92"* @_ZNSt3__29allocatorIiEC2Ev(%"class.std::__2::allocator.92"* %1) #9
  ret %"struct.std::__2::__compressed_pair_elem.91"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::allocator.92"* @_ZNSt3__29allocatorIiEC2Ev(%"class.std::__2::allocator.92"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.92"*, align 4
  store %"class.std::__2::allocator.92"* %this, %"class.std::__2::allocator.92"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.92"*, %"class.std::__2::allocator.92"** %this.addr, align 4
  ret %"class.std::__2::allocator.92"* %this1
}

declare void @_ZNSt3__212basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEPKcm(%"class.std::__2::basic_string"*, i8*, i32) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__211char_traitsIcE6lengthEPKc(i8* %__s) #0 comdat {
entry:
  %__s.addr = alloca i8*, align 4
  store i8* %__s, i8** %__s.addr, align 4
  %0 = load i8*, i8** %__s.addr, align 4
  %call = call i32 @strlen(i8* %0) #9
  ret i32 %call
}

; Function Attrs: nounwind
declare i32 @strlen(i8*) #2

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco13DecoderBuffer4PeekIhEEbPT_(%"class.draco::DecoderBuffer"* %this, i8* %out_val) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %out_val.addr = alloca i8*, align 4
  %size_to_decode = alloca i32, align 4
  store %"class.draco::DecoderBuffer"* %this, %"class.draco::DecoderBuffer"** %this.addr, align 4
  store i8* %out_val, i8** %out_val.addr, align 4
  %this1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %this.addr, align 4
  store i32 1, i32* %size_to_decode, align 4
  %data_size_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 1
  %0 = load i64, i64* %data_size_, align 8
  %pos_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %1 = load i64, i64* %pos_, align 8
  %add = add nsw i64 %1, 1
  %cmp = icmp slt i64 %0, %add
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %2 = load i8*, i8** %out_val.addr, align 4
  %data_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 0
  %3 = load i8*, i8** %data_, align 8
  %pos_2 = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %4 = load i64, i64* %pos_2, align 8
  %idx.ext = trunc i64 %4 to i32
  %add.ptr = getelementptr inbounds i8, i8* %3, i32 %idx.ext
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %2, i8* align 1 %add.ptr, i32 1, i1 false)
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %5 = load i1, i1* %retval, align 1
  ret i1 %5
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco13DecoderBuffer4PeekItEEbPT_(%"class.draco::DecoderBuffer"* %this, i16* %out_val) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::DecoderBuffer"*, align 4
  %out_val.addr = alloca i16*, align 4
  %size_to_decode = alloca i32, align 4
  store %"class.draco::DecoderBuffer"* %this, %"class.draco::DecoderBuffer"** %this.addr, align 4
  store i16* %out_val, i16** %out_val.addr, align 4
  %this1 = load %"class.draco::DecoderBuffer"*, %"class.draco::DecoderBuffer"** %this.addr, align 4
  store i32 2, i32* %size_to_decode, align 4
  %data_size_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 1
  %0 = load i64, i64* %data_size_, align 8
  %pos_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %1 = load i64, i64* %pos_, align 8
  %add = add nsw i64 %1, 2
  %cmp = icmp slt i64 %0, %add
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %2 = load i16*, i16** %out_val.addr, align 4
  %3 = bitcast i16* %2 to i8*
  %data_ = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 0
  %4 = load i8*, i8** %data_, align 8
  %pos_2 = getelementptr inbounds %"class.draco::DecoderBuffer", %"class.draco::DecoderBuffer"* %this1, i32 0, i32 2
  %5 = load i64, i64* %pos_2, align 8
  %idx.ext = trunc i64 %5 to i32
  %add.ptr = getelementptr inbounds i8, i8* %4, i32 %idx.ext
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %3, i8* align 1 %add.ptr, i32 2, i1 false)
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %6 = load i1, i1* %retval, align 1
  ret i1 %6
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair"* @_ZNSt3__217__compressed_pairIPN5draco16GeometryMetadataENS_14default_deleteIS2_EEEC2IS3_S5_EEOT_OT0_(%"class.std::__2::__compressed_pair"* returned %this, %"class.draco::GeometryMetadata"** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::default_delete.50"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  %__t1.addr = alloca %"class.draco::GeometryMetadata"**, align 4
  %__t2.addr = alloca %"struct.std::__2::default_delete.50"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  store %"class.draco::GeometryMetadata"** %__t1, %"class.draco::GeometryMetadata"*** %__t1.addr, align 4
  store %"struct.std::__2::default_delete.50"* %__t2, %"struct.std::__2::default_delete.50"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %1 = load %"class.draco::GeometryMetadata"**, %"class.draco::GeometryMetadata"*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::GeometryMetadata"** @_ZNSt3__27forwardIPN5draco16GeometryMetadataEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::GeometryMetadata"** nonnull align 4 dereferenceable(4) %1) #9
  %call2 = call %"struct.std::__2::__compressed_pair_elem"* @_ZNSt3__222__compressed_pair_elemIPN5draco16GeometryMetadataELi0ELb0EEC2IS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem"* %0, %"class.draco::GeometryMetadata"** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem.49"*
  %3 = load %"struct.std::__2::default_delete.50"*, %"struct.std::__2::default_delete.50"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.50"* @_ZNSt3__27forwardINS_14default_deleteIN5draco16GeometryMetadataEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.50"* nonnull align 1 dereferenceable(1) %3) #9
  %call4 = call %"struct.std::__2::__compressed_pair_elem.49"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco16GeometryMetadataEEELi1ELb1EEC2IS4_vEEOT_(%"struct.std::__2::__compressed_pair_elem.49"* %2, %"struct.std::__2::default_delete.50"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::__compressed_pair"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::GeometryMetadata"** @_ZNSt3__27forwardIPN5draco16GeometryMetadataEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::GeometryMetadata"** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.draco::GeometryMetadata"**, align 4
  store %"class.draco::GeometryMetadata"** %__t, %"class.draco::GeometryMetadata"*** %__t.addr, align 4
  %0 = load %"class.draco::GeometryMetadata"**, %"class.draco::GeometryMetadata"*** %__t.addr, align 4
  ret %"class.draco::GeometryMetadata"** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem"* @_ZNSt3__222__compressed_pair_elemIPN5draco16GeometryMetadataELi0ELb0EEC2IS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem"* returned %this, %"class.draco::GeometryMetadata"** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem"*, align 4
  %__u.addr = alloca %"class.draco::GeometryMetadata"**, align 4
  store %"struct.std::__2::__compressed_pair_elem"* %this, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  store %"class.draco::GeometryMetadata"** %__u, %"class.draco::GeometryMetadata"*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem"*, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem"* %this1, i32 0, i32 0
  %0 = load %"class.draco::GeometryMetadata"**, %"class.draco::GeometryMetadata"*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::GeometryMetadata"** @_ZNSt3__27forwardIPN5draco16GeometryMetadataEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.draco::GeometryMetadata"** nonnull align 4 dereferenceable(4) %0) #9
  %1 = load %"class.draco::GeometryMetadata"*, %"class.draco::GeometryMetadata"** %call, align 4
  store %"class.draco::GeometryMetadata"* %1, %"class.draco::GeometryMetadata"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.49"* @_ZNSt3__222__compressed_pair_elemINS_14default_deleteIN5draco16GeometryMetadataEEELi1ELb1EEC2IS4_vEEOT_(%"struct.std::__2::__compressed_pair_elem.49"* returned %this, %"struct.std::__2::default_delete.50"* nonnull align 1 dereferenceable(1) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.49"*, align 4
  %__u.addr = alloca %"struct.std::__2::default_delete.50"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.49"* %this, %"struct.std::__2::__compressed_pair_elem.49"** %this.addr, align 4
  store %"struct.std::__2::default_delete.50"* %__u, %"struct.std::__2::default_delete.50"** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.49"*, %"struct.std::__2::__compressed_pair_elem.49"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.49"* %this1 to %"struct.std::__2::default_delete.50"*
  %1 = load %"struct.std::__2::default_delete.50"*, %"struct.std::__2::default_delete.50"** %__u.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"struct.std::__2::default_delete.50"* @_ZNSt3__27forwardINS_14default_deleteIN5draco16GeometryMetadataEEEEEOT_RNS_16remove_referenceIS5_E4typeE(%"struct.std::__2::default_delete.50"* nonnull align 1 dereferenceable(1) %1) #9
  ret %"struct.std::__2::__compressed_pair_elem.49"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::unique_ptr.96"* @_ZNSt3__26vectorINS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEENS_9allocatorIS6_EEE11__make_iterEPS6_(%"class.std::__2::vector.94"* %this, %"class.std::__2::unique_ptr.96"* %__p) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__wrap_iter", align 4
  %this.addr = alloca %"class.std::__2::vector.94"*, align 4
  %__p.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  store %"class.std::__2::vector.94"* %this, %"class.std::__2::vector.94"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.96"* %__p, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  %this1 = load %"class.std::__2::vector.94"*, %"class.std::__2::vector.94"** %this.addr, align 4
  %0 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__p.addr, align 4
  %call = call %"class.std::__2::__wrap_iter"* @_ZNSt3__211__wrap_iterIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEC2ES7_(%"class.std::__2::__wrap_iter"* %retval, %"class.std::__2::unique_ptr.96"* %0) #9
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %retval, i32 0, i32 0
  %1 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %coerce.dive, align 4
  ret %"class.std::__2::unique_ptr.96"* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__wrap_iter"* @_ZNSt3__211__wrap_iterIPNS_10unique_ptrIN5draco26AttributesDecoderInterfaceENS_14default_deleteIS3_EEEEEC2ES7_(%"class.std::__2::__wrap_iter"* returned %this, %"class.std::__2::unique_ptr.96"* %__x) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__wrap_iter"*, align 4
  %__x.addr = alloca %"class.std::__2::unique_ptr.96"*, align 4
  store %"class.std::__2::__wrap_iter"* %this, %"class.std::__2::__wrap_iter"** %this.addr, align 4
  store %"class.std::__2::unique_ptr.96"* %__x, %"class.std::__2::unique_ptr.96"** %__x.addr, align 4
  %this1 = load %"class.std::__2::__wrap_iter"*, %"class.std::__2::__wrap_iter"** %this.addr, align 4
  %__i = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::unique_ptr.96"*, %"class.std::__2::unique_ptr.96"** %__x.addr, align 4
  store %"class.std::__2::unique_ptr.96"* %0, %"class.std::__2::unique_ptr.96"** %__i, align 4
  ret %"class.std::__2::__wrap_iter"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::AttributesDecoderInterface"** @_ZNKSt3__217__compressed_pairIPN5draco26AttributesDecoderInterfaceENS_14default_deleteIS2_EEE5firstEv(%"class.std::__2::__compressed_pair.97"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.97"*, align 4
  store %"class.std::__2::__compressed_pair.97"* %this, %"class.std::__2::__compressed_pair.97"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.97"*, %"class.std::__2::__compressed_pair.97"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.97"* %this1 to %"struct.std::__2::__compressed_pair_elem.98"*
  %call = call nonnull align 4 dereferenceable(4) %"class.draco::AttributesDecoderInterface"** @_ZNKSt3__222__compressed_pair_elemIPN5draco26AttributesDecoderInterfaceELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.98"* %0) #9
  ret %"class.draco::AttributesDecoderInterface"** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"class.draco::AttributesDecoderInterface"** @_ZNKSt3__222__compressed_pair_elemIPN5draco26AttributesDecoderInterfaceELi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.98"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.98"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.98"* %this, %"struct.std::__2::__compressed_pair_elem.98"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.98"*, %"struct.std::__2::__compressed_pair_elem.98"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.98", %"struct.std::__2::__compressed_pair_elem.98"* %this1, i32 0, i32 0
  ret %"class.draco::AttributesDecoderInterface"** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIiNS_9allocatorIiEEE8__appendEm(%"class.std::__2::vector.87"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  %__n.addr = alloca i32, align 4
  %__a = alloca %"class.std::__2::allocator.92"*, align 4
  %__v = alloca %"struct.std::__2::__split_buffer", align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.87"* %this1 to %"class.std::__2::__vector_base.88"*
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE9__end_capEv(%"class.std::__2::__vector_base.88"* %0) #9
  %1 = load i32*, i32** %call, align 4
  %2 = bitcast %"class.std::__2::vector.87"* %this1 to %"class.std::__2::__vector_base.88"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %2, i32 0, i32 1
  %3 = load i32*, i32** %__end_, align 4
  %sub.ptr.lhs.cast = ptrtoint i32* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint i32* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  %4 = load i32, i32* %__n.addr, align 4
  %cmp = icmp uge i32 %sub.ptr.div, %4
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %5 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__26vectorIiNS_9allocatorIiEEE18__construct_at_endEm(%"class.std::__2::vector.87"* %this1, i32 %5)
  br label %if.end

if.else:                                          ; preds = %entry
  %6 = bitcast %"class.std::__2::vector.87"* %this1 to %"class.std::__2::__vector_base.88"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE7__allocEv(%"class.std::__2::__vector_base.88"* %6) #9
  store %"class.std::__2::allocator.92"* %call2, %"class.std::__2::allocator.92"** %__a, align 4
  %call3 = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4sizeEv(%"class.std::__2::vector.87"* %this1) #9
  %7 = load i32, i32* %__n.addr, align 4
  %add = add i32 %call3, %7
  %call4 = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE11__recommendEm(%"class.std::__2::vector.87"* %this1, i32 %add)
  %call5 = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4sizeEv(%"class.std::__2::vector.87"* %this1) #9
  %8 = load %"class.std::__2::allocator.92"*, %"class.std::__2::allocator.92"** %__a, align 4
  %call6 = call %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEEC2EmmS3_(%"struct.std::__2::__split_buffer"* %__v, i32 %call4, i32 %call5, %"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %8)
  %9 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE18__construct_at_endEm(%"struct.std::__2::__split_buffer"* %__v, i32 %9)
  call void @_ZNSt3__26vectorIiNS_9allocatorIiEEE26__swap_out_circular_bufferERNS_14__split_bufferIiRS2_EE(%"class.std::__2::vector.87"* %this1, %"struct.std::__2::__split_buffer"* nonnull align 4 dereferenceable(20) %__v)
  %call7 = call %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEED2Ev(%"struct.std::__2::__split_buffer"* %__v) #9
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIiNS_9allocatorIiEEE17__destruct_at_endEPi(%"class.std::__2::vector.87"* %this, i32* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  %__new_last.addr = alloca i32*, align 4
  %__old_size = alloca i32, align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  store i32* %__new_last, i32** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  %0 = load i32*, i32** %__new_last.addr, align 4
  call void @_ZNSt3__26vectorIiNS_9allocatorIiEEE27__invalidate_iterators_pastEPi(%"class.std::__2::vector.87"* %this1, i32* %0)
  %call = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4sizeEv(%"class.std::__2::vector.87"* %this1) #9
  store i32 %call, i32* %__old_size, align 4
  %1 = bitcast %"class.std::__2::vector.87"* %this1 to %"class.std::__2::__vector_base.88"*
  %2 = load i32*, i32** %__new_last.addr, align 4
  call void @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE17__destruct_at_endEPi(%"class.std::__2::__vector_base.88"* %1, i32* %2) #9
  %3 = load i32, i32* %__old_size, align 4
  call void @_ZNKSt3__26vectorIiNS_9allocatorIiEEE17__annotate_shrinkEm(%"class.std::__2::vector.87"* %this1, i32 %3) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE9__end_capEv(%"class.std::__2::__vector_base.88"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.88"*, align 4
  store %"class.std::__2::__vector_base.88"* %this, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.88"*, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__217__compressed_pairIPiNS_9allocatorIiEEE5firstEv(%"class.std::__2::__compressed_pair.89"* %__end_cap_) #9
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIiNS_9allocatorIiEEE18__construct_at_endEm(%"class.std::__2::vector.87"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  %__n.addr = alloca i32, align 4
  %__tx = alloca %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction", align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %call = call %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* @_ZNSt3__26vectorIiNS_9allocatorIiEEE21_ConstructTransactionC2ERS3_m(%"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* %__tx, %"class.std::__2::vector.87"* nonnull align 4 dereferenceable(12) %this1, i32 %0)
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction", %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %1 = load i32*, i32** %__pos_, align 4
  %__new_end_ = getelementptr inbounds %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction", %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* %__tx, i32 0, i32 2
  %2 = load i32*, i32** %__new_end_, align 4
  %cmp = icmp ne i32* %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = bitcast %"class.std::__2::vector.87"* %this1 to %"class.std::__2::__vector_base.88"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE7__allocEv(%"class.std::__2::__vector_base.88"* %3) #9
  %__pos_3 = getelementptr inbounds %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction", %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %4 = load i32*, i32** %__pos_3, align 4
  %call4 = call i32* @_ZNSt3__212__to_addressIiEEPT_S2_(i32* %4) #9
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE9constructIiJEEEvRS2_PT_DpOT0_(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %call2, i32* %call4)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %__pos_5 = getelementptr inbounds %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction", %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %5 = load i32*, i32** %__pos_5, align 4
  %incdec.ptr = getelementptr inbounds i32, i32* %5, i32 1
  store i32* %incdec.ptr, i32** %__pos_5, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call6 = call %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* @_ZNSt3__26vectorIiNS_9allocatorIiEEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* %__tx) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE11__recommendEm(%"class.std::__2::vector.87"* %this, i32 %__new_size) #0 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  %__new_size.addr = alloca i32, align 4
  %__ms = alloca i32, align 4
  %__cap = alloca i32, align 4
  %ref.tmp = alloca i32, align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  store i32 %__new_size, i32* %__new_size.addr, align 4
  %this1 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE8max_sizeEv(%"class.std::__2::vector.87"* %this1) #9
  store i32 %call, i32* %__ms, align 4
  %0 = load i32, i32* %__new_size.addr, align 4
  %1 = load i32, i32* %__ms, align 4
  %cmp = icmp ugt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = bitcast %"class.std::__2::vector.87"* %this1 to %"class.std::__2::__vector_base_common"*
  call void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"* %2) #13
  unreachable

if.end:                                           ; preds = %entry
  %call2 = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::vector.87"* %this1) #9
  store i32 %call2, i32* %__cap, align 4
  %3 = load i32, i32* %__cap, align 4
  %4 = load i32, i32* %__ms, align 4
  %div = udiv i32 %4, 2
  %cmp3 = icmp uge i32 %3, %div
  br i1 %cmp3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %if.end
  %5 = load i32, i32* %__ms, align 4
  store i32 %5, i32* %retval, align 4
  br label %return

if.end5:                                          ; preds = %if.end
  %6 = load i32, i32* %__cap, align 4
  %mul = mul i32 2, %6
  store i32 %mul, i32* %ref.tmp, align 4
  %call6 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %__new_size.addr)
  %7 = load i32, i32* %call6, align 4
  store i32 %7, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end5, %if.then4
  %8 = load i32, i32* %retval, align 4
  ret i32 %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEEC2EmmS3_(%"struct.std::__2::__split_buffer"* returned %this, i32 %__cap, i32 %__start, %"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %__a) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"struct.std::__2::__split_buffer"*, align 4
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__cap.addr = alloca i32, align 4
  %__start.addr = alloca i32, align 4
  %__a.addr = alloca %"class.std::__2::allocator.92"*, align 4
  %ref.tmp = alloca i8*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store i32 %__cap, i32* %__cap.addr, align 4
  store i32 %__start, i32* %__start.addr, align 4
  store %"class.std::__2::allocator.92"* %__a, %"class.std::__2::allocator.92"** %__a.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer"* %this1, %"struct.std::__2::__split_buffer"** %retval, align 4
  %0 = bitcast %"struct.std::__2::__split_buffer"* %this1 to %"class.std::__2::__split_buffer_common"*
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  store i8* null, i8** %ref.tmp, align 4
  %1 = load %"class.std::__2::allocator.92"*, %"class.std::__2::allocator.92"** %__a.addr, align 4
  %call = call %"class.std::__2::__compressed_pair.143"* @_ZNSt3__217__compressed_pairIPiRNS_9allocatorIiEEEC2IDnS4_EEOT_OT0_(%"class.std::__2::__compressed_pair.143"* %__end_cap_, i8** nonnull align 4 dereferenceable(4) %ref.tmp, %"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %1)
  %2 = load i32, i32* %__cap.addr, align 4
  %cmp = icmp ne i32 %2, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE7__allocEv(%"struct.std::__2::__split_buffer"* %this1) #9
  %3 = load i32, i32* %__cap.addr, align 4
  %call3 = call i32* @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE8allocateERS2_m(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %call2, i32 %3)
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32* [ %call3, %cond.true ], [ null, %cond.false ]
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  store i32* %cond, i32** %__first_, align 4
  %__first_4 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %4 = load i32*, i32** %__first_4, align 4
  %5 = load i32, i32* %__start.addr, align 4
  %add.ptr = getelementptr inbounds i32, i32* %4, i32 %5
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 2
  store i32* %add.ptr, i32** %__end_, align 4
  %__begin_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 1
  store i32* %add.ptr, i32** %__begin_, align 4
  %__first_5 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %6 = load i32*, i32** %__first_5, align 4
  %7 = load i32, i32* %__cap.addr, align 4
  %add.ptr6 = getelementptr inbounds i32, i32* %6, i32 %7
  %call7 = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this1) #9
  store i32* %add.ptr6, i32** %call7, align 4
  %8 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %retval, align 4
  ret %"struct.std::__2::__split_buffer"* %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE18__construct_at_endEm(%"struct.std::__2::__split_buffer"* %this, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__n.addr = alloca i32, align 4
  %__tx = alloca %"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction", align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 2
  %0 = load i32, i32* %__n.addr, align 4
  %call = call %"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE21_ConstructTransactionC2EPPim(%"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction"* %__tx, i32** %__end_, i32 %0) #9
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %__pos_ = getelementptr inbounds %"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction"* %__tx, i32 0, i32 0
  %1 = load i32*, i32** %__pos_, align 4
  %__end_2 = getelementptr inbounds %"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %2 = load i32*, i32** %__end_2, align 4
  %cmp = icmp ne i32* %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %call3 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE7__allocEv(%"struct.std::__2::__split_buffer"* %this1) #9
  %__pos_4 = getelementptr inbounds %"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction"* %__tx, i32 0, i32 0
  %3 = load i32*, i32** %__pos_4, align 4
  %call5 = call i32* @_ZNSt3__212__to_addressIiEEPT_S2_(i32* %3) #9
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE9constructIiJEEEvRS2_PT_DpOT0_(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %call3, i32* %call5)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %__pos_6 = getelementptr inbounds %"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction"* %__tx, i32 0, i32 0
  %4 = load i32*, i32** %__pos_6, align 4
  %incdec.ptr = getelementptr inbounds i32, i32* %4, i32 1
  store i32* %incdec.ptr, i32** %__pos_6, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call7 = call %"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE21_ConstructTransactionD2Ev(%"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction"* %__tx) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIiNS_9allocatorIiEEE26__swap_out_circular_bufferERNS_14__split_bufferIiRS2_EE(%"class.std::__2::vector.87"* %this, %"struct.std::__2::__split_buffer"* nonnull align 4 dereferenceable(20) %__v) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  %__v.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer"* %__v, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %this1 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  call void @_ZNKSt3__26vectorIiNS_9allocatorIiEEE17__annotate_deleteEv(%"class.std::__2::vector.87"* %this1) #9
  %0 = bitcast %"class.std::__2::vector.87"* %this1 to %"class.std::__2::__vector_base.88"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE7__allocEv(%"class.std::__2::__vector_base.88"* %0) #9
  %1 = bitcast %"class.std::__2::vector.87"* %this1 to %"class.std::__2::__vector_base.88"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %1, i32 0, i32 0
  %2 = load i32*, i32** %__begin_, align 4
  %3 = bitcast %"class.std::__2::vector.87"* %this1 to %"class.std::__2::__vector_base.88"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %3, i32 0, i32 1
  %4 = load i32*, i32** %__end_, align 4
  %5 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__begin_2 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %5, i32 0, i32 1
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE46__construct_backward_with_exception_guaranteesIiEENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS2_PT_S8_EE5valuesr31is_trivially_move_constructibleIS8_EE5valueEvE4typeERS2_S9_S9_RS9_(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %call, i32* %2, i32* %4, i32** nonnull align 4 dereferenceable(4) %__begin_2)
  %6 = bitcast %"class.std::__2::vector.87"* %this1 to %"class.std::__2::__vector_base.88"*
  %__begin_3 = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %6, i32 0, i32 0
  %7 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__begin_4 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %7, i32 0, i32 1
  call void @_ZNSt3__24swapIPiEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_(i32** nonnull align 4 dereferenceable(4) %__begin_3, i32** nonnull align 4 dereferenceable(4) %__begin_4) #9
  %8 = bitcast %"class.std::__2::vector.87"* %this1 to %"class.std::__2::__vector_base.88"*
  %__end_5 = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %8, i32 0, i32 1
  %9 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__end_6 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %9, i32 0, i32 2
  call void @_ZNSt3__24swapIPiEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_(i32** nonnull align 4 dereferenceable(4) %__end_5, i32** nonnull align 4 dereferenceable(4) %__end_6) #9
  %10 = bitcast %"class.std::__2::vector.87"* %this1 to %"class.std::__2::__vector_base.88"*
  %call7 = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__213__vector_baseIiNS_9allocatorIiEEE9__end_capEv(%"class.std::__2::__vector_base.88"* %10) #9
  %11 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %call8 = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE9__end_capEv(%"struct.std::__2::__split_buffer"* %11) #9
  call void @_ZNSt3__24swapIPiEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_(i32** nonnull align 4 dereferenceable(4) %call7, i32** nonnull align 4 dereferenceable(4) %call8) #9
  %12 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__begin_9 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %12, i32 0, i32 1
  %13 = load i32*, i32** %__begin_9, align 4
  %14 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %14, i32 0, i32 0
  store i32* %13, i32** %__first_, align 4
  %call10 = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4sizeEv(%"class.std::__2::vector.87"* %this1) #9
  call void @_ZNKSt3__26vectorIiNS_9allocatorIiEEE14__annotate_newEm(%"class.std::__2::vector.87"* %this1, i32 %call10) #9
  call void @_ZNSt3__26vectorIiNS_9allocatorIiEEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector.87"* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEED2Ev(%"struct.std::__2::__split_buffer"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"struct.std::__2::__split_buffer"*, align 4
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer"* %this1, %"struct.std::__2::__split_buffer"** %retval, align 4
  call void @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE5clearEv(%"struct.std::__2::__split_buffer"* %this1) #9
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__first_, align 4
  %tobool = icmp ne i32* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE7__allocEv(%"struct.std::__2::__split_buffer"* %this1) #9
  %__first_2 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %1 = load i32*, i32** %__first_2, align 4
  %call3 = call i32 @_ZNKSt3__214__split_bufferIiRNS_9allocatorIiEEE8capacityEv(%"struct.std::__2::__split_buffer"* %this1)
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE10deallocateERS2_Pim(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %call, i32* %1, i32 %call3) #9
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %retval, align 4
  ret %"struct.std::__2::__split_buffer"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNSt3__217__compressed_pairIPiNS_9allocatorIiEEE5firstEv(%"class.std::__2::__compressed_pair.89"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.89"*, align 4
  store %"class.std::__2::__compressed_pair.89"* %this, %"class.std::__2::__compressed_pair.89"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.89"*, %"class.std::__2::__compressed_pair.89"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.89"* %this1 to %"struct.std::__2::__compressed_pair_elem.90"*
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__222__compressed_pair_elemIPiLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.90"* %0) #9
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNSt3__222__compressed_pair_elemIPiLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.90"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.90"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.90"* %this, %"struct.std::__2::__compressed_pair_elem.90"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.90"*, %"struct.std::__2::__compressed_pair_elem.90"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.90", %"struct.std::__2::__compressed_pair_elem.90"* %this1, i32 0, i32 0
  ret i32** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* @_ZNSt3__26vectorIiNS_9allocatorIiEEE21_ConstructTransactionC2ERS3_m(%"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* returned %this, %"class.std::__2::vector.87"* nonnull align 4 dereferenceable(12) %__v, i32 %__n) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"*, align 4
  %__v.addr = alloca %"class.std::__2::vector.87"*, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"** %this.addr, align 4
  store %"class.std::__2::vector.87"* %__v, %"class.std::__2::vector.87"** %__v.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"*, %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"** %this.addr, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction", %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %__v.addr, align 4
  store %"class.std::__2::vector.87"* %0, %"class.std::__2::vector.87"** %__v_, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction", %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %1 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %__v.addr, align 4
  %2 = bitcast %"class.std::__2::vector.87"* %1 to %"class.std::__2::__vector_base.88"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %2, i32 0, i32 1
  %3 = load i32*, i32** %__end_, align 4
  store i32* %3, i32** %__pos_, align 4
  %__new_end_ = getelementptr inbounds %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction", %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* %this1, i32 0, i32 2
  %4 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %__v.addr, align 4
  %5 = bitcast %"class.std::__2::vector.87"* %4 to %"class.std::__2::__vector_base.88"*
  %__end_2 = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %5, i32 0, i32 1
  %6 = load i32*, i32** %__end_2, align 4
  %7 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds i32, i32* %6, i32 %7
  store i32* %add.ptr, i32** %__new_end_, align 4
  ret %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE9constructIiJEEEvRS2_PT_DpOT0_(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %__a, i32* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.92"*, align 4
  %__p.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_construct", align 1
  store %"class.std::__2::allocator.92"* %__a, %"class.std::__2::allocator.92"** %__a.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_construct"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.92"*, %"class.std::__2::allocator.92"** %__a.addr, align 4
  %2 = load i32*, i32** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE11__constructIiJEEEvNS_17integral_constantIbLb1EEERS2_PT_DpOT0_(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %1, i32* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* @_ZNSt3__26vectorIiNS_9allocatorIiEEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"*, align 4
  store %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"** %this.addr, align 4
  %this1 = load %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"*, %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction", %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %0 = load i32*, i32** %__pos_, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction", %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %__v_, align 4
  %2 = bitcast %"class.std::__2::vector.87"* %1 to %"class.std::__2::__vector_base.88"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %2, i32 0, i32 1
  store i32* %0, i32** %__end_, align 4
  ret %"struct.std::__2::vector<int, std::__2::allocator<int>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE11__constructIiJEEEvNS_17integral_constantIbLb1EEERS2_PT_DpOT0_(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %__a, i32* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.92"*, align 4
  %__p.addr = alloca i32*, align 4
  store %"class.std::__2::allocator.92"* %__a, %"class.std::__2::allocator.92"** %__a.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.92"*, %"class.std::__2::allocator.92"** %__a.addr, align 4
  %2 = load i32*, i32** %__p.addr, align 4
  call void @_ZNSt3__29allocatorIiE9constructIiJEEEvPT_DpOT0_(%"class.std::__2::allocator.92"* %1, i32* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIiE9constructIiJEEEvPT_DpOT0_(%"class.std::__2::allocator.92"* %this, i32* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.92"*, align 4
  %__p.addr = alloca i32*, align 4
  store %"class.std::__2::allocator.92"* %this, %"class.std::__2::allocator.92"** %this.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.92"*, %"class.std::__2::allocator.92"** %this.addr, align 4
  %0 = load i32*, i32** %__p.addr, align 4
  %1 = bitcast i32* %0 to i8*
  %2 = bitcast i8* %1 to i32*
  store i32 0, i32* %2, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE8max_sizeEv(%"class.std::__2::vector.87"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp3 = alloca i32, align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.87"* %this1 to %"class.std::__2::__vector_base.88"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE7__allocEv(%"class.std::__2::__vector_base.88"* %0) #9
  %call2 = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE8max_sizeERKS2_(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %call) #9
  store i32 %call2, i32* %ref.tmp, align 4
  %call4 = call i32 @_ZNSt3__214numeric_limitsIlE3maxEv() #9
  store i32 %call4, i32* %ref.tmp3, align 4
  %call5 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %1 = load i32, i32* %call5, align 4
  ret i32 %1
}

; Function Attrs: noreturn
declare void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"*) #7

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::__less", align 1
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__a.addr, align 4
  %1 = load i32*, i32** %__b.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::__less", align 1
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__a.addr, align 4
  %1 = load i32*, i32** %__b.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE8max_sizeERKS2_(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.92"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_max_size", align 1
  store %"class.std::__2::allocator.92"* %__a, %"class.std::__2::allocator.92"** %__a.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_max_size"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.92"*, %"class.std::__2::allocator.92"** %__a.addr, align 4
  %call = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE10__max_sizeENS_17integral_constantIbLb1EEERKS2_(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %1) #9
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNKSt3__213__vector_baseIiNS_9allocatorIiEEE7__allocEv(%"class.std::__2::__vector_base.88"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.88"*, align 4
  store %"class.std::__2::__vector_base.88"* %this, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.88"*, %"class.std::__2::__vector_base.88"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.88", %"class.std::__2::__vector_base.88"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNKSt3__217__compressed_pairIPiNS_9allocatorIiEEE6secondEv(%"class.std::__2::__compressed_pair.89"* %__end_cap_) #9
  ret %"class.std::__2::allocator.92"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__214numeric_limitsIlE3maxEv() #0 comdat {
entry:
  %call = call i32 @_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv() #9
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__comp = alloca %"struct.std::__2::__less", align 1
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__b.addr, align 4
  %1 = load i32*, i32** %__a.addr, align 4
  %call = call zeroext i1 @_ZNKSt3__26__lessImmEclERKmS3_(%"struct.std::__2::__less"* %__comp, i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  br i1 %call, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32*, i32** %__b.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %3 = load i32*, i32** %__a.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi i32* [ %2, %cond.true ], [ %3, %cond.false ]
  ret i32* %cond-lvalue
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__26__lessImmEclERKmS3_(%"struct.std::__2::__less"* %this, i32* nonnull align 4 dereferenceable(4) %__x, i32* nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__less"*, align 4
  %__x.addr = alloca i32*, align 4
  %__y.addr = alloca i32*, align 4
  store %"struct.std::__2::__less"* %this, %"struct.std::__2::__less"** %this.addr, align 4
  store i32* %__x, i32** %__x.addr, align 4
  store i32* %__y, i32** %__y.addr, align 4
  %this1 = load %"struct.std::__2::__less"*, %"struct.std::__2::__less"** %this.addr, align 4
  %0 = load i32*, i32** %__x.addr, align 4
  %1 = load i32, i32* %0, align 4
  %2 = load i32*, i32** %__y.addr, align 4
  %3 = load i32, i32* %2, align 4
  %cmp = icmp ult i32 %1, %3
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE10__max_sizeENS_17integral_constantIbLb1EEERKS2_(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.92"*, align 4
  store %"class.std::__2::allocator.92"* %__a, %"class.std::__2::allocator.92"** %__a.addr, align 4
  %1 = load %"class.std::__2::allocator.92"*, %"class.std::__2::allocator.92"** %__a.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorIiE8max_sizeEv(%"class.std::__2::allocator.92"* %1) #9
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__29allocatorIiE8max_sizeEv(%"class.std::__2::allocator.92"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.92"*, align 4
  store %"class.std::__2::allocator.92"* %this, %"class.std::__2::allocator.92"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.92"*, %"class.std::__2::allocator.92"** %this.addr, align 4
  ret i32 1073741823
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNKSt3__217__compressed_pairIPiNS_9allocatorIiEEE6secondEv(%"class.std::__2::__compressed_pair.89"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.89"*, align 4
  store %"class.std::__2::__compressed_pair.89"* %this, %"class.std::__2::__compressed_pair.89"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.89"*, %"class.std::__2::__compressed_pair.89"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.89"* %this1 to %"struct.std::__2::__compressed_pair_elem.91"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorIiEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.91"* %0) #9
  ret %"class.std::__2::allocator.92"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorIiEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.91"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.91"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.91"* %this, %"struct.std::__2::__compressed_pair_elem.91"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.91"*, %"struct.std::__2::__compressed_pair_elem.91"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.91"* %this1 to %"class.std::__2::allocator.92"*
  ret %"class.std::__2::allocator.92"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv() #0 comdat {
entry:
  ret i32 2147483647
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__comp = alloca %"struct.std::__2::__less", align 1
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__a.addr, align 4
  %1 = load i32*, i32** %__b.addr, align 4
  %call = call zeroext i1 @_ZNKSt3__26__lessImmEclERKmS3_(%"struct.std::__2::__less"* %__comp, i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  br i1 %call, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32*, i32** %__b.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %3 = load i32*, i32** %__a.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi i32* [ %2, %cond.true ], [ %3, %cond.false ]
  ret i32* %cond-lvalue
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.143"* @_ZNSt3__217__compressed_pairIPiRNS_9allocatorIiEEEC2IDnS4_EEOT_OT0_(%"class.std::__2::__compressed_pair.143"* returned %this, i8** nonnull align 4 dereferenceable(4) %__t1, %"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.143"*, align 4
  %__t1.addr = alloca i8**, align 4
  %__t2.addr = alloca %"class.std::__2::allocator.92"*, align 4
  store %"class.std::__2::__compressed_pair.143"* %this, %"class.std::__2::__compressed_pair.143"** %this.addr, align 4
  store i8** %__t1, i8*** %__t1.addr, align 4
  store %"class.std::__2::allocator.92"* %__t2, %"class.std::__2::allocator.92"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.143"*, %"class.std::__2::__compressed_pair.143"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.143"* %this1 to %"struct.std::__2::__compressed_pair_elem.90"*
  %1 = load i8**, i8*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %1) #9
  %call2 = call %"struct.std::__2::__compressed_pair_elem.90"* @_ZNSt3__222__compressed_pair_elemIPiLi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.90"* %0, i8** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.143"* %this1 to i8*
  %3 = getelementptr inbounds i8, i8* %2, i32 4
  %4 = bitcast i8* %3 to %"struct.std::__2::__compressed_pair_elem.144"*
  %5 = load %"class.std::__2::allocator.92"*, %"class.std::__2::allocator.92"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__27forwardIRNS_9allocatorIiEEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %5) #9
  %call4 = call %"struct.std::__2::__compressed_pair_elem.144"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIiEELi1ELb0EEC2IS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.144"* %4, %"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::__compressed_pair.143"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE8allocateERS2_m(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %__a, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.92"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.92"* %__a, %"class.std::__2::allocator.92"** %__a.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.92"*, %"class.std::__2::allocator.92"** %__a.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call i32* @_ZNSt3__29allocatorIiE8allocateEmPKv(%"class.std::__2::allocator.92"* %0, i32 %1, i8* null)
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE7__allocEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__217__compressed_pairIPiRNS_9allocatorIiEEE6secondEv(%"class.std::__2::__compressed_pair.143"* %__end_cap_) #9
  ret %"class.std::__2::allocator.92"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__217__compressed_pairIPiRNS_9allocatorIiEEE5firstEv(%"class.std::__2::__compressed_pair.143"* %__end_cap_) #9
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__27forwardIRNS_9allocatorIiEEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::allocator.92"*, align 4
  store %"class.std::__2::allocator.92"* %__t, %"class.std::__2::allocator.92"** %__t.addr, align 4
  %0 = load %"class.std::__2::allocator.92"*, %"class.std::__2::allocator.92"** %__t.addr, align 4
  ret %"class.std::__2::allocator.92"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.144"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIiEELi1ELb0EEC2IS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.144"* returned %this, %"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.144"*, align 4
  %__u.addr = alloca %"class.std::__2::allocator.92"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.144"* %this, %"struct.std::__2::__compressed_pair_elem.144"** %this.addr, align 4
  store %"class.std::__2::allocator.92"* %__u, %"class.std::__2::allocator.92"** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.144"*, %"struct.std::__2::__compressed_pair_elem.144"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.144", %"struct.std::__2::__compressed_pair_elem.144"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::allocator.92"*, %"class.std::__2::allocator.92"** %__u.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__27forwardIRNS_9allocatorIiEEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %0) #9
  store %"class.std::__2::allocator.92"* %call, %"class.std::__2::allocator.92"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.144"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNSt3__29allocatorIiE8allocateEmPKv(%"class.std::__2::allocator.92"* %this, i32 %__n, i8* %0) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.92"*, align 4
  %__n.addr = alloca i32, align 4
  %.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.92"* %this, %"class.std::__2::allocator.92"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::allocator.92"*, %"class.std::__2::allocator.92"** %this.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorIiE8max_sizeEv(%"class.std::__2::allocator.92"* %this1) #9
  %cmp = icmp ugt i32 %1, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__220__throw_length_errorEPKc(i8* getelementptr inbounds ([68 x i8], [68 x i8]* @.str.9, i32 0, i32 0)) #13
  unreachable

if.end:                                           ; preds = %entry
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  %call2 = call i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %mul, i32 4)
  %3 = bitcast i8* %call2 to i32*
  ret i32* %3
}

; Function Attrs: noinline noreturn nounwind optnone
define linkonce_odr hidden void @_ZNSt3__220__throw_length_errorEPKc(i8* %__msg) #8 comdat {
entry:
  %__msg.addr = alloca i8*, align 4
  store i8* %__msg, i8** %__msg.addr, align 4
  call void @abort() #13
  unreachable
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %__size, i32 %__align) #0 comdat {
entry:
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i32, i32* %__size.addr, align 4
  %call = call noalias nonnull i8* @_Znwm(i32 %0) #10
  ret i8* %call
}

; Function Attrs: noreturn
declare void @abort() #7

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__217__compressed_pairIPiRNS_9allocatorIiEEE6secondEv(%"class.std::__2::__compressed_pair.143"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.143"*, align 4
  store %"class.std::__2::__compressed_pair.143"* %this, %"class.std::__2::__compressed_pair.143"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.143"*, %"class.std::__2::__compressed_pair.143"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.143"* %this1 to i8*
  %add.ptr = getelementptr inbounds i8, i8* %0, i32 4
  %1 = bitcast i8* %add.ptr to %"struct.std::__2::__compressed_pair_elem.144"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIiEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.144"* %1) #9
  ret %"class.std::__2::allocator.92"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIiEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.144"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.144"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.144"* %this, %"struct.std::__2::__compressed_pair_elem.144"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.144"*, %"struct.std::__2::__compressed_pair_elem.144"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.144", %"struct.std::__2::__compressed_pair_elem.144"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::allocator.92"*, %"class.std::__2::allocator.92"** %__value_, align 4
  ret %"class.std::__2::allocator.92"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNSt3__217__compressed_pairIPiRNS_9allocatorIiEEE5firstEv(%"class.std::__2::__compressed_pair.143"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.143"*, align 4
  store %"class.std::__2::__compressed_pair.143"* %this, %"class.std::__2::__compressed_pair.143"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.143"*, %"class.std::__2::__compressed_pair.143"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.143"* %this1 to %"struct.std::__2::__compressed_pair_elem.90"*
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__222__compressed_pair_elemIPiLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.90"* %0) #9
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE21_ConstructTransactionC2EPPim(%"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction"* returned %this, i32** %__p, i32 %__n) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction"*, align 4
  %__p.addr = alloca i32**, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction"* %this, %"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction"** %this.addr, align 4
  store i32** %__p, i32*** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction"*, %"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load i32**, i32*** %__p.addr, align 4
  %1 = load i32*, i32** %0, align 4
  store i32* %1, i32** %__pos_, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction"* %this1, i32 0, i32 1
  %2 = load i32**, i32*** %__p.addr, align 4
  %3 = load i32*, i32** %2, align 4
  %4 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds i32, i32* %3, i32 %4
  store i32* %add.ptr, i32** %__end_, align 4
  %__dest_ = getelementptr inbounds %"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction"* %this1, i32 0, i32 2
  %5 = load i32**, i32*** %__p.addr, align 4
  store i32** %5, i32*** %__dest_, align 4
  ret %"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE21_ConstructTransactionD2Ev(%"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction"*, align 4
  store %"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction"* %this, %"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction"*, %"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__pos_, align 4
  %__dest_ = getelementptr inbounds %"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction"* %this1, i32 0, i32 2
  %1 = load i32**, i32*** %__dest_, align 4
  store i32* %0, i32** %1, align 4
  ret %"struct.std::__2::__split_buffer<int, std::__2::allocator<int> &>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE46__construct_backward_with_exception_guaranteesIiEENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS2_PT_S8_EE5valuesr31is_trivially_move_constructibleIS8_EE5valueEvE4typeERS2_S9_S9_RS9_(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %0, i32* %__begin1, i32* %__end1, i32** nonnull align 4 dereferenceable(4) %__end2) #0 comdat {
entry:
  %.addr = alloca %"class.std::__2::allocator.92"*, align 4
  %__begin1.addr = alloca i32*, align 4
  %__end1.addr = alloca i32*, align 4
  %__end2.addr = alloca i32**, align 4
  %_Np = alloca i32, align 4
  store %"class.std::__2::allocator.92"* %0, %"class.std::__2::allocator.92"** %.addr, align 4
  store i32* %__begin1, i32** %__begin1.addr, align 4
  store i32* %__end1, i32** %__end1.addr, align 4
  store i32** %__end2, i32*** %__end2.addr, align 4
  %1 = load i32*, i32** %__end1.addr, align 4
  %2 = load i32*, i32** %__begin1.addr, align 4
  %sub.ptr.lhs.cast = ptrtoint i32* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint i32* %2 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  store i32 %sub.ptr.div, i32* %_Np, align 4
  %3 = load i32, i32* %_Np, align 4
  %4 = load i32**, i32*** %__end2.addr, align 4
  %5 = load i32*, i32** %4, align 4
  %idx.neg = sub i32 0, %3
  %add.ptr = getelementptr inbounds i32, i32* %5, i32 %idx.neg
  store i32* %add.ptr, i32** %4, align 4
  %6 = load i32, i32* %_Np, align 4
  %cmp = icmp sgt i32 %6, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %7 = load i32**, i32*** %__end2.addr, align 4
  %8 = load i32*, i32** %7, align 4
  %9 = bitcast i32* %8 to i8*
  %10 = load i32*, i32** %__begin1.addr, align 4
  %11 = bitcast i32* %10 to i8*
  %12 = load i32, i32* %_Np, align 4
  %mul = mul i32 %12, 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %11, i32 %mul, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__24swapIPiEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_(i32** nonnull align 4 dereferenceable(4) %__x, i32** nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %__x.addr = alloca i32**, align 4
  %__y.addr = alloca i32**, align 4
  %__t = alloca i32*, align 4
  store i32** %__x, i32*** %__x.addr, align 4
  store i32** %__y, i32*** %__y.addr, align 4
  %0 = load i32**, i32*** %__x.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__24moveIRPiEEONS_16remove_referenceIT_E4typeEOS4_(i32** nonnull align 4 dereferenceable(4) %0) #9
  %1 = load i32*, i32** %call, align 4
  store i32* %1, i32** %__t, align 4
  %2 = load i32**, i32*** %__y.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__24moveIRPiEEONS_16remove_referenceIT_E4typeEOS4_(i32** nonnull align 4 dereferenceable(4) %2) #9
  %3 = load i32*, i32** %call1, align 4
  %4 = load i32**, i32*** %__x.addr, align 4
  store i32* %3, i32** %4, align 4
  %call2 = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__24moveIRPiEEONS_16remove_referenceIT_E4typeEOS4_(i32** nonnull align 4 dereferenceable(4) %__t) #9
  %5 = load i32*, i32** %call2, align 4
  %6 = load i32**, i32*** %__y.addr, align 4
  store i32* %5, i32** %6, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIiNS_9allocatorIiEEE14__annotate_newEm(%"class.std::__2::vector.87"* %this, i32 %__current_size) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  %__current_size.addr = alloca i32, align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  store i32 %__current_size, i32* %__current_size.addr, align 4
  %this1 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  %call = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector.87"* %this1) #9
  %0 = bitcast i32* %call to i8*
  %call2 = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector.87"* %this1) #9
  %call3 = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::vector.87"* %this1) #9
  %add.ptr = getelementptr inbounds i32, i32* %call2, i32 %call3
  %1 = bitcast i32* %add.ptr to i8*
  %call4 = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector.87"* %this1) #9
  %call5 = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::vector.87"* %this1) #9
  %add.ptr6 = getelementptr inbounds i32, i32* %call4, i32 %call5
  %2 = bitcast i32* %add.ptr6 to i8*
  %call7 = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector.87"* %this1) #9
  %3 = load i32, i32* %__current_size.addr, align 4
  %add.ptr8 = getelementptr inbounds i32, i32* %call7, i32 %3
  %4 = bitcast i32* %add.ptr8 to i8*
  call void @_ZNKSt3__26vectorIiNS_9allocatorIiEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector.87"* %this1, i8* %0, i8* %1, i8* %2, i8* %4) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIiNS_9allocatorIiEEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector.87"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNSt3__24moveIRPiEEONS_16remove_referenceIT_E4typeEOS4_(i32** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca i32**, align 4
  store i32** %__t, i32*** %__t.addr, align 4
  %0 = load i32**, i32*** %__t.addr, align 4
  ret i32** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE5clearEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 1
  %0 = load i32*, i32** %__begin_, align 4
  call void @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE17__destruct_at_endEPi(%"struct.std::__2::__split_buffer"* %this1, i32* %0) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__214__split_bufferIiRNS_9allocatorIiEEE8capacityEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__214__split_bufferIiRNS_9allocatorIiEEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this1) #9
  %0 = load i32*, i32** %call, align 4
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %1 = load i32*, i32** %__first_, align 4
  %sub.ptr.lhs.cast = ptrtoint i32* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint i32* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE17__destruct_at_endEPi(%"struct.std::__2::__split_buffer"* %this, i32* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__new_last.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant.134", align 1
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store i32* %__new_last, i32** %__new_last.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %0 = load i32*, i32** %__new_last.addr, align 4
  call void @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE17__destruct_at_endEPiNS_17integral_constantIbLb0EEE(%"struct.std::__2::__split_buffer"* %this1, i32* %0) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE17__destruct_at_endEPiNS_17integral_constantIbLb0EEE(%"struct.std::__2::__split_buffer"* %this, i32* %__new_last) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant.134", align 1
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__new_last.addr = alloca i32*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store i32* %__new_last, i32** %__new_last.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load i32*, i32** %__new_last.addr, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 2
  %2 = load i32*, i32** %__end_, align 4
  %cmp = icmp ne i32* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.92"* @_ZNSt3__214__split_bufferIiRNS_9allocatorIiEEE7__allocEv(%"struct.std::__2::__split_buffer"* %this1) #9
  %__end_2 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 2
  %3 = load i32*, i32** %__end_2, align 4
  %incdec.ptr = getelementptr inbounds i32, i32* %3, i32 -1
  store i32* %incdec.ptr, i32** %__end_2, align 4
  %call3 = call i32* @_ZNSt3__212__to_addressIiEEPT_S2_(i32* %incdec.ptr) #9
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIiEEE7destroyIiEEvRS2_PT_(%"class.std::__2::allocator.92"* nonnull align 1 dereferenceable(1) %call, i32* %call3)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__214__split_bufferIiRNS_9allocatorIiEEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__217__compressed_pairIPiRNS_9allocatorIiEEE5firstEv(%"class.std::__2::__compressed_pair.143"* %__end_cap_) #9
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__217__compressed_pairIPiRNS_9allocatorIiEEE5firstEv(%"class.std::__2::__compressed_pair.143"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.143"*, align 4
  store %"class.std::__2::__compressed_pair.143"* %this, %"class.std::__2::__compressed_pair.143"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.143"*, %"class.std::__2::__compressed_pair.143"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.143"* %this1 to %"struct.std::__2::__compressed_pair_elem.90"*
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__222__compressed_pair_elemIPiLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.90"* %0) #9
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIiNS_9allocatorIiEEE27__invalidate_iterators_pastEPi(%"class.std::__2::vector.87"* %this, i32* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  %__new_last.addr = alloca i32*, align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  store i32* %__new_last, i32** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIiNS_9allocatorIiEEE17__annotate_shrinkEm(%"class.std::__2::vector.87"* %this, i32 %__old_size) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.87"*, align 4
  %__old_size.addr = alloca i32, align 4
  store %"class.std::__2::vector.87"* %this, %"class.std::__2::vector.87"** %this.addr, align 4
  store i32 %__old_size, i32* %__old_size.addr, align 4
  %this1 = load %"class.std::__2::vector.87"*, %"class.std::__2::vector.87"** %this.addr, align 4
  %call = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector.87"* %this1) #9
  %0 = bitcast i32* %call to i8*
  %call2 = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector.87"* %this1) #9
  %call3 = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE8capacityEv(%"class.std::__2::vector.87"* %this1) #9
  %add.ptr = getelementptr inbounds i32, i32* %call2, i32 %call3
  %1 = bitcast i32* %add.ptr to i8*
  %call4 = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector.87"* %this1) #9
  %2 = load i32, i32* %__old_size.addr, align 4
  %add.ptr5 = getelementptr inbounds i32, i32* %call4, i32 %2
  %3 = bitcast i32* %add.ptr5 to i8*
  %call6 = call i32* @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4dataEv(%"class.std::__2::vector.87"* %this1) #9
  %call7 = call i32 @_ZNKSt3__26vectorIiNS_9allocatorIiEEE4sizeEv(%"class.std::__2::vector.87"* %this1) #9
  %add.ptr8 = getelementptr inbounds i32, i32* %call6, i32 %call7
  %4 = bitcast i32* %add.ptr8 to i8*
  call void @_ZNKSt3__26vectorIiNS_9allocatorIiEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector.87"* %this1, i8* %0, i8* %1, i8* %3, i8* %4) #9
  ret void
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nobuiltin allocsize(0) "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { cold noreturn nounwind }
attributes #7 = { noreturn "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #8 = { noinline noreturn nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #9 = { nounwind }
attributes #10 = { builtin allocsize(0) }
attributes #11 = { noreturn nounwind }
attributes #12 = { builtin nounwind }
attributes #13 = { noreturn }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
