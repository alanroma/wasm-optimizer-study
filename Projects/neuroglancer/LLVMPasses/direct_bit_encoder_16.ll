; ModuleID = './draco/src/draco/compression/bit_coders/direct_bit_encoder.cc'
source_filename = "./draco/src/draco/compression/bit_coders/direct_bit_encoder.cc"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%"class.draco::DirectBitEncoder" = type { %"class.std::__2::vector", i32, i32 }
%"class.std::__2::vector" = type { %"class.std::__2::__vector_base" }
%"class.std::__2::__vector_base" = type { i32*, i32*, %"class.std::__2::__compressed_pair" }
%"class.std::__2::__compressed_pair" = type { %"struct.std::__2::__compressed_pair_elem" }
%"struct.std::__2::__compressed_pair_elem" = type { i32* }
%"class.draco::EncoderBuffer" = type <{ %"class.std::__2::vector.1", %"class.std::__2::unique_ptr", i64, i8, [7 x i8] }>
%"class.std::__2::vector.1" = type { %"class.std::__2::__vector_base.2" }
%"class.std::__2::__vector_base.2" = type { i8*, i8*, %"class.std::__2::__compressed_pair.3" }
%"class.std::__2::__compressed_pair.3" = type { %"struct.std::__2::__compressed_pair_elem.4" }
%"struct.std::__2::__compressed_pair_elem.4" = type { i8* }
%"class.std::__2::unique_ptr" = type { %"class.std::__2::__compressed_pair.8" }
%"class.std::__2::__compressed_pair.8" = type { %"struct.std::__2::__compressed_pair_elem.9" }
%"struct.std::__2::__compressed_pair_elem.9" = type { %"class.draco::EncoderBuffer::BitEncoder"* }
%"class.draco::EncoderBuffer::BitEncoder" = type { i8*, i32 }
%"class.std::__2::__wrap_iter" = type { i8* }
%"class.std::__2::__wrap_iter.11" = type { i8* }
%"class.std::__2::allocator.6" = type { i8 }
%"struct.std::__2::__split_buffer" = type { i8*, i8*, i8*, %"class.std::__2::__compressed_pair.12" }
%"class.std::__2::__compressed_pair.12" = type { %"struct.std::__2::__compressed_pair_elem.4", %"struct.std::__2::__compressed_pair_elem.13" }
%"struct.std::__2::__compressed_pair_elem.13" = type { %"class.std::__2::allocator.6"* }
%"struct.std::__2::random_access_iterator_tag" = type { i8 }
%"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction" = type { %"class.std::__2::vector.1"*, i8*, i8* }
%"class.std::__2::__vector_base_common" = type { i8 }
%"class.std::__2::__split_buffer_common" = type { i8 }
%"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction" = type { i8*, i8*, i8** }
%"struct.std::__2::integral_constant" = type { i8 }
%"struct.std::__2::__has_construct" = type { i8 }
%"struct.std::__2::__has_construct.14" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.5" = type { i8 }
%"struct.std::__2::__less" = type { i8 }
%"struct.std::__2::__has_max_size" = type { i8 }
%"struct.std::__2::integral_constant.15" = type { i8 }
%"struct.std::__2::__has_destroy" = type { i8 }
%"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction" = type { %"class.std::__2::vector"*, i32*, i32* }
%"class.std::__2::allocator" = type { i8 }
%"struct.std::__2::__split_buffer.17" = type { i32*, i32*, i32*, %"class.std::__2::__compressed_pair.18" }
%"class.std::__2::__compressed_pair.18" = type { %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem.19" }
%"struct.std::__2::__compressed_pair_elem.19" = type { %"class.std::__2::allocator"* }
%"struct.std::__2::__has_construct.16" = type { i8 }
%"struct.std::__2::__compressed_pair_elem.0" = type { i8 }
%"struct.std::__2::__has_max_size.20" = type { i8 }
%"struct.std::__2::__has_destroy.21" = type { i8 }
%"struct.std::__2::__default_init_tag" = type { i8 }

$_ZNSt3__26vectorIjNS_9allocatorIjEEEC2Ev = comdat any

$_ZNSt3__26vectorIjNS_9allocatorIjEEED2Ev = comdat any

$_ZNSt3__26vectorIjNS_9allocatorIjEEE9push_backERKj = comdat any

$_ZNKSt3__26vectorIjNS_9allocatorIjEEE4sizeEv = comdat any

$_ZN5draco13EncoderBuffer6EncodeIjEEbRKT_ = comdat any

$_ZN5draco13EncoderBuffer6EncodeEPKvm = comdat any

$_ZNSt3__26vectorIjNS_9allocatorIjEEE4dataEv = comdat any

$_ZNSt3__26vectorIjNS_9allocatorIjEEE5clearEv = comdat any

$_ZNK5draco13EncoderBuffer18bit_encoder_activeEv = comdat any

$_ZNSt3__26vectorIcNS_9allocatorIcEEE6insertIPKhEENS_9enable_ifIXaasr27__is_cpp17_forward_iteratorIT_EE5valuesr16is_constructibleIcNS_15iterator_traitsIS8_E9referenceEEE5valueENS_11__wrap_iterIPcEEE4typeENSC_IPKcEES8_S8_ = comdat any

$_ZNSt3__26vectorIcNS_9allocatorIcEEE3endEv = comdat any

$_ZNSt3__211__wrap_iterIPKcEC2IPcEERKNS0_IT_EEPNS_9enable_ifIXsr14is_convertibleIS6_S2_EE5valueEvE4typeE = comdat any

$_ZNSt3__2miIPKcPcEEDTmicldtfp_4baseEcldtfp0_4baseEERKNS_11__wrap_iterIT_EERKNS5_IT0_EE = comdat any

$_ZNSt3__26vectorIcNS_9allocatorIcEEE5beginEv = comdat any

$_ZNSt3__28distanceIPKhEENS_15iterator_traitsIT_E15difference_typeES4_S4_ = comdat any

$_ZNSt3__213__vector_baseIcNS_9allocatorIcEEE9__end_capEv = comdat any

$_ZNSt3__27advanceIPKhEEvRT_NS_15iterator_traitsIS3_E15difference_typeE = comdat any

$_ZNSt3__26vectorIcNS_9allocatorIcEEE18__construct_at_endIPKhEENS_9enable_ifIXsr27__is_cpp17_forward_iteratorIT_EE5valueEvE4typeES8_S8_m = comdat any

$_ZNSt3__26vectorIcNS_9allocatorIcEEE12__move_rangeEPcS4_S4_ = comdat any

$_ZNSt3__24copyIPKhPcEET0_T_S5_S4_ = comdat any

$_ZNSt3__213__vector_baseIcNS_9allocatorIcEEE7__allocEv = comdat any

$_ZNKSt3__26vectorIcNS_9allocatorIcEEE11__recommendEm = comdat any

$_ZNKSt3__26vectorIcNS_9allocatorIcEEE4sizeEv = comdat any

$_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEEC2EmmS3_ = comdat any

$_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE18__construct_at_endIPKhEENS_9enable_ifIXsr27__is_cpp17_forward_iteratorIT_EE5valueEvE4typeES9_S9_ = comdat any

$_ZNSt3__26vectorIcNS_9allocatorIcEEE26__swap_out_circular_bufferERNS_14__split_bufferIcRS2_EEPc = comdat any

$_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEED2Ev = comdat any

$_ZNSt3__26vectorIcNS_9allocatorIcEEE11__make_iterEPc = comdat any

$_ZNKSt3__211__wrap_iterIPKcE4baseEv = comdat any

$_ZNKSt3__211__wrap_iterIPcE4baseEv = comdat any

$_ZNSt3__210__distanceIPKhEENS_15iterator_traitsIT_E15difference_typeES4_S4_NS_26random_access_iterator_tagE = comdat any

$_ZNSt3__217__compressed_pairIPcNS_9allocatorIcEEE5firstEv = comdat any

$_ZNSt3__222__compressed_pair_elemIPcLi0ELb0EE5__getEv = comdat any

$_ZNSt3__29__advanceIPKhEEvRT_NS_15iterator_traitsIS3_E15difference_typeENS_26random_access_iterator_tagE = comdat any

$_ZNSt3__26vectorIcNS_9allocatorIcEEE21_ConstructTransactionC2ERS3_m = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIcEEE25__construct_range_forwardIPKhPcEEvRS2_T_S9_RT0_ = comdat any

$_ZNSt3__26vectorIcNS_9allocatorIcEEE21_ConstructTransactionD2Ev = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIcEEE9constructIcJRKhEEEvRS2_PT_DpOT0_ = comdat any

$_ZNSt3__212__to_addressIcEEPT_S2_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIcEEE11__constructIcJRKhEEEvNS_17integral_constantIbLb1EEERS2_PT_DpOT0_ = comdat any

$_ZNSt3__27forwardIRKhEEOT_RNS_16remove_referenceIS3_E4typeE = comdat any

$_ZNSt3__29allocatorIcE9constructIcJRKhEEEvPT_DpOT0_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIcEEE9constructIcJcEEEvRS2_PT_DpOT0_ = comdat any

$_ZNSt3__24moveIRcEEONS_16remove_referenceIT_E4typeEOS3_ = comdat any

$_ZNSt3__213move_backwardIPcS1_EET0_T_S3_S2_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIcEEE11__constructIcJcEEEvNS_17integral_constantIbLb1EEERS2_PT_DpOT0_ = comdat any

$_ZNSt3__27forwardIcEEOT_RNS_16remove_referenceIS1_E4typeE = comdat any

$_ZNSt3__29allocatorIcE9constructIcJcEEEvPT_DpOT0_ = comdat any

$_ZNSt3__215__move_backwardIccEENS_9enable_ifIXaasr7is_sameINS_12remove_constIT_E4typeET0_EE5valuesr28is_trivially_copy_assignableIS6_EE5valueEPS6_E4typeEPS3_SA_S7_ = comdat any

$_ZNSt3__213__unwrap_iterIPcEET_S2_ = comdat any

$_ZNSt3__26__copyIPKhPcEET0_T_S5_S4_ = comdat any

$_ZNSt3__213__unwrap_iterIPKhEET_S3_ = comdat any

$_ZNSt3__216__copy_constexprIPKhPcEET0_T_S5_S4_ = comdat any

$_ZNSt3__217__compressed_pairIPcNS_9allocatorIcEEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIcEELi1ELb1EE5__getEv = comdat any

$_ZNKSt3__26vectorIcNS_9allocatorIcEEE8max_sizeEv = comdat any

$_ZNKSt3__26vectorIcNS_9allocatorIcEEE8capacityEv = comdat any

$_ZNSt3__23maxImEERKT_S3_S3_ = comdat any

$_ZNSt3__23minImEERKT_S3_S3_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIcEEE8max_sizeERKS2_ = comdat any

$_ZNKSt3__213__vector_baseIcNS_9allocatorIcEEE7__allocEv = comdat any

$_ZNSt3__214numeric_limitsIlE3maxEv = comdat any

$_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_ = comdat any

$_ZNKSt3__26__lessImmEclERKmS3_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIcEEE10__max_sizeENS_17integral_constantIbLb1EEERKS2_ = comdat any

$_ZNKSt3__29allocatorIcE8max_sizeEv = comdat any

$_ZNKSt3__217__compressed_pairIPcNS_9allocatorIcEEE6secondEv = comdat any

$_ZNKSt3__222__compressed_pair_elemINS_9allocatorIcEELi1ELb1EE5__getEv = comdat any

$_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv = comdat any

$_ZNKSt3__213__vector_baseIcNS_9allocatorIcEEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseIcNS_9allocatorIcEEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPcNS_9allocatorIcEEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPcLi0ELb0EE5__getEv = comdat any

$_ZNSt3__23maxImNS_6__lessImmEEEERKT_S5_S5_T0_ = comdat any

$_ZNSt3__217__compressed_pairIPcRNS_9allocatorIcEEEC2IDnS4_EEOT_OT0_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIcEEE8allocateERS2_m = comdat any

$_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE7__allocEv = comdat any

$_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE9__end_capEv = comdat any

$_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIPcLi0ELb0EEC2IDnvEEOT_ = comdat any

$_ZNSt3__27forwardIRNS_9allocatorIcEEEEOT_RNS_16remove_referenceIS4_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIcEELi1ELb0EEC2IS3_vEEOT_ = comdat any

$_ZNSt3__29allocatorIcE8allocateEmPKv = comdat any

$_ZNSt3__220__throw_length_errorEPKc = comdat any

$_ZNSt3__217__libcpp_allocateEmm = comdat any

$_ZNSt3__217__compressed_pairIPcRNS_9allocatorIcEEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIcEELi1ELb0EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPcRNS_9allocatorIcEEE5firstEv = comdat any

$_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE21_ConstructTransactionC2EPPcm = comdat any

$_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE21_ConstructTransactionD2Ev = comdat any

$_ZNKSt3__26vectorIcNS_9allocatorIcEEE17__annotate_deleteEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIcEEE46__construct_backward_with_exception_guaranteesIcEENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS2_PT_S8_EE5valuesr31is_trivially_move_constructibleIS8_EE5valueEvE4typeERS2_S9_S9_RS9_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIcEEE45__construct_forward_with_exception_guaranteesIcEENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS2_PT_S8_EE5valuesr31is_trivially_move_constructibleIS8_EE5valueEvE4typeERS2_S9_S9_RS9_ = comdat any

$_ZNSt3__24swapIPcEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_ = comdat any

$_ZNKSt3__26vectorIcNS_9allocatorIcEEE14__annotate_newEm = comdat any

$_ZNSt3__26vectorIcNS_9allocatorIcEEE26__invalidate_all_iteratorsEv = comdat any

$_ZNKSt3__26vectorIcNS_9allocatorIcEEE31__annotate_contiguous_containerEPKvS5_S5_S5_ = comdat any

$_ZNKSt3__26vectorIcNS_9allocatorIcEEE4dataEv = comdat any

$_ZNSt3__24moveIRPcEEONS_16remove_referenceIT_E4typeEOS4_ = comdat any

$_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE5clearEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIcEEE10deallocateERS2_Pcm = comdat any

$_ZNKSt3__214__split_bufferIcRNS_9allocatorIcEEE8capacityEv = comdat any

$_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE17__destruct_at_endEPc = comdat any

$_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE17__destruct_at_endEPcNS_17integral_constantIbLb0EEE = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIcEEE7destroyIcEEvRS2_PT_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIcEEE9__destroyIcEEvNS_17integral_constantIbLb1EEERS2_PT_ = comdat any

$_ZNSt3__29allocatorIcE7destroyEPc = comdat any

$_ZNSt3__29allocatorIcE10deallocateEPcm = comdat any

$_ZNSt3__219__libcpp_deallocateEPvmm = comdat any

$_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm = comdat any

$_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm = comdat any

$_ZNSt3__217_DeallocateCaller9__do_callEPv = comdat any

$_ZNKSt3__214__split_bufferIcRNS_9allocatorIcEEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPcRNS_9allocatorIcEEE5firstEv = comdat any

$_ZNSt3__211__wrap_iterIPcEC2ES1_ = comdat any

$_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE9__end_capEv = comdat any

$_ZNSt3__26vectorIjNS_9allocatorIjEEE22__construct_one_at_endIJRKjEEEvDpOT_ = comdat any

$_ZNSt3__26vectorIjNS_9allocatorIjEEE21__push_back_slow_pathIRKjEEvOT_ = comdat any

$_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEE5firstEv = comdat any

$_ZNSt3__222__compressed_pair_elemIPjLi0ELb0EE5__getEv = comdat any

$_ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionC2ERS3_m = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIjEEE9constructIjJRKjEEEvRS2_PT_DpOT0_ = comdat any

$_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv = comdat any

$_ZNSt3__212__to_addressIjEEPT_S2_ = comdat any

$_ZNSt3__27forwardIRKjEEOT_RNS_16remove_referenceIS3_E4typeE = comdat any

$_ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionD2Ev = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIjEEE11__constructIjJRKjEEEvNS_17integral_constantIbLb1EEERS2_PT_DpOT0_ = comdat any

$_ZNSt3__29allocatorIjE9constructIjJRKjEEEvPT_DpOT0_ = comdat any

$_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EE5__getEv = comdat any

$_ZNKSt3__26vectorIjNS_9allocatorIjEEE11__recommendEm = comdat any

$_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEEC2EmmS3_ = comdat any

$_ZNSt3__26vectorIjNS_9allocatorIjEEE26__swap_out_circular_bufferERNS_14__split_bufferIjRS2_EE = comdat any

$_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEED2Ev = comdat any

$_ZNKSt3__26vectorIjNS_9allocatorIjEEE8max_sizeEv = comdat any

$_ZNKSt3__26vectorIjNS_9allocatorIjEEE8capacityEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIjEEE8max_sizeERKS2_ = comdat any

$_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIjEEE10__max_sizeENS_17integral_constantIbLb1EEERKS2_ = comdat any

$_ZNKSt3__29allocatorIjE8max_sizeEv = comdat any

$_ZNKSt3__217__compressed_pairIPjNS_9allocatorIjEEE6secondEv = comdat any

$_ZNKSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EE5__getEv = comdat any

$_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE8capacityEv = comdat any

$_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPjNS_9allocatorIjEEE5firstEv = comdat any

$_ZNKSt3__222__compressed_pair_elemIPjLi0ELb0EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEEC2IDnS4_EEOT_OT0_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIjEEE8allocateERS2_m = comdat any

$_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE7__allocEv = comdat any

$_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE9__end_capEv = comdat any

$_ZNSt3__222__compressed_pair_elemIPjLi0ELb0EEC2IDnvEEOT_ = comdat any

$_ZNSt3__27forwardIRNS_9allocatorIjEEEEOT_RNS_16remove_referenceIS4_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIjEELi1ELb0EEC2IS3_vEEOT_ = comdat any

$_ZNSt3__29allocatorIjE8allocateEmPKv = comdat any

$_ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEE6secondEv = comdat any

$_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIjEELi1ELb0EE5__getEv = comdat any

$_ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEE5firstEv = comdat any

$_ZNKSt3__26vectorIjNS_9allocatorIjEEE17__annotate_deleteEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIjEEE46__construct_backward_with_exception_guaranteesIjEENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS2_PT_S8_EE5valuesr31is_trivially_move_constructibleIS8_EE5valueEvE4typeERS2_S9_S9_RS9_ = comdat any

$_ZNSt3__24swapIPjEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_ = comdat any

$_ZNKSt3__26vectorIjNS_9allocatorIjEEE14__annotate_newEm = comdat any

$_ZNSt3__26vectorIjNS_9allocatorIjEEE26__invalidate_all_iteratorsEv = comdat any

$_ZNKSt3__26vectorIjNS_9allocatorIjEEE31__annotate_contiguous_containerEPKvS5_S5_S5_ = comdat any

$_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv = comdat any

$_ZNSt3__24moveIRPjEEONS_16remove_referenceIT_E4typeEOS4_ = comdat any

$_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE5clearEv = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIjEEE10deallocateERS2_Pjm = comdat any

$_ZNKSt3__214__split_bufferIjRNS_9allocatorIjEEE8capacityEv = comdat any

$_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE17__destruct_at_endEPj = comdat any

$_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE17__destruct_at_endEPjNS_17integral_constantIbLb0EEE = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIjEEE7destroyIjEEvRS2_PT_ = comdat any

$_ZNSt3__216allocator_traitsINS_9allocatorIjEEE9__destroyIjEEvNS_17integral_constantIbLb1EEERS2_PT_ = comdat any

$_ZNSt3__29allocatorIjE7destroyEPj = comdat any

$_ZNSt3__29allocatorIjE10deallocateEPjm = comdat any

$_ZNKSt3__214__split_bufferIjRNS_9allocatorIjEEE9__end_capEv = comdat any

$_ZNKSt3__217__compressed_pairIPjRNS_9allocatorIjEEE5firstEv = comdat any

$_ZNSt3__213__vector_baseIjNS_9allocatorIjEEEC2Ev = comdat any

$_ZNSt3__220__vector_base_commonILb1EEC2Ev = comdat any

$_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEEC2IDnNS_18__default_init_tagEEEOT_OT0_ = comdat any

$_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE = comdat any

$_ZNSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EEC2ENS_18__default_init_tagE = comdat any

$_ZNSt3__29allocatorIjEC2Ev = comdat any

$_ZNSt3__213__vector_baseIjNS_9allocatorIjEEED2Ev = comdat any

$_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE5clearEv = comdat any

$_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE17__destruct_at_endEPj = comdat any

$_ZNKSt3__26vectorIjNS_9allocatorIjEEE17__annotate_shrinkEm = comdat any

@.str = private unnamed_addr constant [68 x i8] c"allocator<T>::allocate(size_t n) 'n' exceeds maximum supported size\00", align 1

@_ZN5draco16DirectBitEncoderC1Ev = hidden unnamed_addr alias %"class.draco::DirectBitEncoder"* (%"class.draco::DirectBitEncoder"*), %"class.draco::DirectBitEncoder"* (%"class.draco::DirectBitEncoder"*)* @_ZN5draco16DirectBitEncoderC2Ev
@_ZN5draco16DirectBitEncoderD1Ev = hidden unnamed_addr alias %"class.draco::DirectBitEncoder"* (%"class.draco::DirectBitEncoder"*), %"class.draco::DirectBitEncoder"* (%"class.draco::DirectBitEncoder"*)* @_ZN5draco16DirectBitEncoderD2Ev

; Function Attrs: noinline nounwind optnone
define hidden %"class.draco::DirectBitEncoder"* @_ZN5draco16DirectBitEncoderC2Ev(%"class.draco::DirectBitEncoder"* returned %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %"class.draco::DirectBitEncoder"*, align 4
  store %"class.draco::DirectBitEncoder"* %this, %"class.draco::DirectBitEncoder"** %this.addr, align 4
  %this1 = load %"class.draco::DirectBitEncoder"*, %"class.draco::DirectBitEncoder"** %this.addr, align 4
  %bits_ = getelementptr inbounds %"class.draco::DirectBitEncoder", %"class.draco::DirectBitEncoder"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEEC2Ev(%"class.std::__2::vector"* %bits_) #6
  %local_bits_ = getelementptr inbounds %"class.draco::DirectBitEncoder", %"class.draco::DirectBitEncoder"* %this1, i32 0, i32 1
  store i32 0, i32* %local_bits_, align 4
  %num_local_bits_ = getelementptr inbounds %"class.draco::DirectBitEncoder", %"class.draco::DirectBitEncoder"* %this1, i32 0, i32 2
  store i32 0, i32* %num_local_bits_, align 4
  ret %"class.draco::DirectBitEncoder"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEEC2Ev(%"class.std::__2::vector"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call %"class.std::__2::__vector_base"* @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEEC2Ev(%"class.std::__2::__vector_base"* %0) #6
  ret %"class.std::__2::vector"* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden %"class.draco::DirectBitEncoder"* @_ZN5draco16DirectBitEncoderD2Ev(%"class.draco::DirectBitEncoder"* returned %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %"class.draco::DirectBitEncoder"*, align 4
  store %"class.draco::DirectBitEncoder"* %this, %"class.draco::DirectBitEncoder"** %this.addr, align 4
  %this1 = load %"class.draco::DirectBitEncoder"*, %"class.draco::DirectBitEncoder"** %this.addr, align 4
  call void @_ZN5draco16DirectBitEncoder5ClearEv(%"class.draco::DirectBitEncoder"* %this1)
  %bits_ = getelementptr inbounds %"class.draco::DirectBitEncoder", %"class.draco::DirectBitEncoder"* %this1, i32 0, i32 0
  %call = call %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEED2Ev(%"class.std::__2::vector"* %bits_) #6
  ret %"class.draco::DirectBitEncoder"* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN5draco16DirectBitEncoder5ClearEv(%"class.draco::DirectBitEncoder"* %this) #0 {
entry:
  %this.addr = alloca %"class.draco::DirectBitEncoder"*, align 4
  store %"class.draco::DirectBitEncoder"* %this, %"class.draco::DirectBitEncoder"** %this.addr, align 4
  %this1 = load %"class.draco::DirectBitEncoder"*, %"class.draco::DirectBitEncoder"** %this.addr, align 4
  %bits_ = getelementptr inbounds %"class.draco::DirectBitEncoder", %"class.draco::DirectBitEncoder"* %this1, i32 0, i32 0
  call void @_ZNSt3__26vectorIjNS_9allocatorIjEEE5clearEv(%"class.std::__2::vector"* %bits_) #6
  %local_bits_ = getelementptr inbounds %"class.draco::DirectBitEncoder", %"class.draco::DirectBitEncoder"* %this1, i32 0, i32 1
  store i32 0, i32* %local_bits_, align 4
  %num_local_bits_ = getelementptr inbounds %"class.draco::DirectBitEncoder", %"class.draco::DirectBitEncoder"* %this1, i32 0, i32 2
  store i32 0, i32* %num_local_bits_, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::vector"* @_ZNSt3__26vectorIjNS_9allocatorIjEEED2Ev(%"class.std::__2::vector"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  call void @_ZNKSt3__26vectorIjNS_9allocatorIjEEE17__annotate_deleteEv(%"class.std::__2::vector"* %this1) #6
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call %"class.std::__2::__vector_base"* @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEED2Ev(%"class.std::__2::__vector_base"* %0) #6
  ret %"class.std::__2::vector"* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN5draco16DirectBitEncoder13StartEncodingEv(%"class.draco::DirectBitEncoder"* %this) #0 {
entry:
  %this.addr = alloca %"class.draco::DirectBitEncoder"*, align 4
  store %"class.draco::DirectBitEncoder"* %this, %"class.draco::DirectBitEncoder"** %this.addr, align 4
  %this1 = load %"class.draco::DirectBitEncoder"*, %"class.draco::DirectBitEncoder"** %this.addr, align 4
  call void @_ZN5draco16DirectBitEncoder5ClearEv(%"class.draco::DirectBitEncoder"* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN5draco16DirectBitEncoder11EndEncodingEPNS_13EncoderBufferE(%"class.draco::DirectBitEncoder"* %this, %"class.draco::EncoderBuffer"* %target_buffer) #0 {
entry:
  %this.addr = alloca %"class.draco::DirectBitEncoder"*, align 4
  %target_buffer.addr = alloca %"class.draco::EncoderBuffer"*, align 4
  %size_in_byte = alloca i32, align 4
  store %"class.draco::DirectBitEncoder"* %this, %"class.draco::DirectBitEncoder"** %this.addr, align 4
  store %"class.draco::EncoderBuffer"* %target_buffer, %"class.draco::EncoderBuffer"** %target_buffer.addr, align 4
  %this1 = load %"class.draco::DirectBitEncoder"*, %"class.draco::DirectBitEncoder"** %this.addr, align 4
  %bits_ = getelementptr inbounds %"class.draco::DirectBitEncoder", %"class.draco::DirectBitEncoder"* %this1, i32 0, i32 0
  %local_bits_ = getelementptr inbounds %"class.draco::DirectBitEncoder", %"class.draco::DirectBitEncoder"* %this1, i32 0, i32 1
  call void @_ZNSt3__26vectorIjNS_9allocatorIjEEE9push_backERKj(%"class.std::__2::vector"* %bits_, i32* nonnull align 4 dereferenceable(4) %local_bits_)
  %bits_2 = getelementptr inbounds %"class.draco::DirectBitEncoder", %"class.draco::DirectBitEncoder"* %this1, i32 0, i32 0
  %call = call i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4sizeEv(%"class.std::__2::vector"* %bits_2) #6
  %mul = mul i32 %call, 4
  store i32 %mul, i32* %size_in_byte, align 4
  %0 = load %"class.draco::EncoderBuffer"*, %"class.draco::EncoderBuffer"** %target_buffer.addr, align 4
  %call3 = call zeroext i1 @_ZN5draco13EncoderBuffer6EncodeIjEEbRKT_(%"class.draco::EncoderBuffer"* %0, i32* nonnull align 4 dereferenceable(4) %size_in_byte)
  %1 = load %"class.draco::EncoderBuffer"*, %"class.draco::EncoderBuffer"** %target_buffer.addr, align 4
  %bits_4 = getelementptr inbounds %"class.draco::DirectBitEncoder", %"class.draco::DirectBitEncoder"* %this1, i32 0, i32 0
  %call5 = call i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEE4dataEv(%"class.std::__2::vector"* %bits_4) #6
  %2 = bitcast i32* %call5 to i8*
  %3 = load i32, i32* %size_in_byte, align 4
  %call6 = call zeroext i1 @_ZN5draco13EncoderBuffer6EncodeEPKvm(%"class.draco::EncoderBuffer"* %1, i8* %2, i32 %3)
  call void @_ZN5draco16DirectBitEncoder5ClearEv(%"class.draco::DirectBitEncoder"* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIjNS_9allocatorIjEEE9push_backERKj(%"class.std::__2::vector"* %this, i32* nonnull align 4 dereferenceable(4) %__x) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__x.addr = alloca i32*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32* %__x, i32** %__x.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 1
  %1 = load i32*, i32** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE9__end_capEv(%"class.std::__2::__vector_base"* %2) #6
  %3 = load i32*, i32** %call, align 4
  %cmp = icmp ne i32* %1, %3
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %4 = load i32*, i32** %__x.addr, align 4
  call void @_ZNSt3__26vectorIjNS_9allocatorIjEEE22__construct_one_at_endIJRKjEEEvDpOT_(%"class.std::__2::vector"* %this1, i32* nonnull align 4 dereferenceable(4) %4)
  br label %if.end

if.else:                                          ; preds = %entry
  %5 = load i32*, i32** %__x.addr, align 4
  call void @_ZNSt3__26vectorIjNS_9allocatorIjEEE21__push_back_slow_pathIRKjEEvOT_(%"class.std::__2::vector"* %this1, i32* nonnull align 4 dereferenceable(4) %5)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4sizeEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 1
  %1 = load i32*, i32** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %2, i32 0, i32 0
  %3 = load i32*, i32** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint i32* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint i32* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco13EncoderBuffer6EncodeIjEEbRKT_(%"class.draco::EncoderBuffer"* %this, i32* nonnull align 4 dereferenceable(4) %data) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::EncoderBuffer"*, align 4
  %data.addr = alloca i32*, align 4
  %src_data = alloca i8*, align 4
  %agg.tmp = alloca %"class.std::__2::__wrap_iter", align 4
  %ref.tmp = alloca %"class.std::__2::__wrap_iter.11", align 4
  %coerce = alloca %"class.std::__2::__wrap_iter.11", align 4
  store %"class.draco::EncoderBuffer"* %this, %"class.draco::EncoderBuffer"** %this.addr, align 4
  store i32* %data, i32** %data.addr, align 4
  %this1 = load %"class.draco::EncoderBuffer"*, %"class.draco::EncoderBuffer"** %this.addr, align 4
  %call = call zeroext i1 @_ZNK5draco13EncoderBuffer18bit_encoder_activeEv(%"class.draco::EncoderBuffer"* %this1)
  br i1 %call, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %0 = load i32*, i32** %data.addr, align 4
  %1 = bitcast i32* %0 to i8*
  store i8* %1, i8** %src_data, align 4
  %buffer_ = getelementptr inbounds %"class.draco::EncoderBuffer", %"class.draco::EncoderBuffer"* %this1, i32 0, i32 0
  %buffer_2 = getelementptr inbounds %"class.draco::EncoderBuffer", %"class.draco::EncoderBuffer"* %this1, i32 0, i32 0
  %call3 = call i8* @_ZNSt3__26vectorIcNS_9allocatorIcEEE3endEv(%"class.std::__2::vector.1"* %buffer_2) #6
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter.11", %"class.std::__2::__wrap_iter.11"* %ref.tmp, i32 0, i32 0
  store i8* %call3, i8** %coerce.dive, align 4
  %call4 = call %"class.std::__2::__wrap_iter"* @_ZNSt3__211__wrap_iterIPKcEC2IPcEERKNS0_IT_EEPNS_9enable_ifIXsr14is_convertibleIS6_S2_EE5valueEvE4typeE(%"class.std::__2::__wrap_iter"* %agg.tmp, %"class.std::__2::__wrap_iter.11"* nonnull align 4 dereferenceable(4) %ref.tmp, i8* null) #6
  %2 = load i8*, i8** %src_data, align 4
  %3 = load i8*, i8** %src_data, align 4
  %add.ptr = getelementptr inbounds i8, i8* %3, i32 4
  %coerce.dive5 = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %agg.tmp, i32 0, i32 0
  %4 = load i8*, i8** %coerce.dive5, align 4
  %call6 = call i8* @_ZNSt3__26vectorIcNS_9allocatorIcEEE6insertIPKhEENS_9enable_ifIXaasr27__is_cpp17_forward_iteratorIT_EE5valuesr16is_constructibleIcNS_15iterator_traitsIS8_E9referenceEEE5valueENS_11__wrap_iterIPcEEE4typeENSC_IPKcEES8_S8_(%"class.std::__2::vector.1"* %buffer_, i8* %4, i8* %2, i8* %add.ptr)
  %coerce.dive7 = getelementptr inbounds %"class.std::__2::__wrap_iter.11", %"class.std::__2::__wrap_iter.11"* %coerce, i32 0, i32 0
  store i8* %call6, i8** %coerce.dive7, align 4
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %5 = load i1, i1* %retval, align 1
  ret i1 %5
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN5draco13EncoderBuffer6EncodeEPKvm(%"class.draco::EncoderBuffer"* %this, i8* %data, i32 %data_size) #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"class.draco::EncoderBuffer"*, align 4
  %data.addr = alloca i8*, align 4
  %data_size.addr = alloca i32, align 4
  %src_data = alloca i8*, align 4
  %agg.tmp = alloca %"class.std::__2::__wrap_iter", align 4
  %ref.tmp = alloca %"class.std::__2::__wrap_iter.11", align 4
  %coerce = alloca %"class.std::__2::__wrap_iter.11", align 4
  store %"class.draco::EncoderBuffer"* %this, %"class.draco::EncoderBuffer"** %this.addr, align 4
  store i8* %data, i8** %data.addr, align 4
  store i32 %data_size, i32* %data_size.addr, align 4
  %this1 = load %"class.draco::EncoderBuffer"*, %"class.draco::EncoderBuffer"** %this.addr, align 4
  %call = call zeroext i1 @_ZNK5draco13EncoderBuffer18bit_encoder_activeEv(%"class.draco::EncoderBuffer"* %this1)
  br i1 %call, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %0 = load i8*, i8** %data.addr, align 4
  store i8* %0, i8** %src_data, align 4
  %buffer_ = getelementptr inbounds %"class.draco::EncoderBuffer", %"class.draco::EncoderBuffer"* %this1, i32 0, i32 0
  %buffer_2 = getelementptr inbounds %"class.draco::EncoderBuffer", %"class.draco::EncoderBuffer"* %this1, i32 0, i32 0
  %call3 = call i8* @_ZNSt3__26vectorIcNS_9allocatorIcEEE3endEv(%"class.std::__2::vector.1"* %buffer_2) #6
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter.11", %"class.std::__2::__wrap_iter.11"* %ref.tmp, i32 0, i32 0
  store i8* %call3, i8** %coerce.dive, align 4
  %call4 = call %"class.std::__2::__wrap_iter"* @_ZNSt3__211__wrap_iterIPKcEC2IPcEERKNS0_IT_EEPNS_9enable_ifIXsr14is_convertibleIS6_S2_EE5valueEvE4typeE(%"class.std::__2::__wrap_iter"* %agg.tmp, %"class.std::__2::__wrap_iter.11"* nonnull align 4 dereferenceable(4) %ref.tmp, i8* null) #6
  %1 = load i8*, i8** %src_data, align 4
  %2 = load i8*, i8** %src_data, align 4
  %3 = load i32, i32* %data_size.addr, align 4
  %add.ptr = getelementptr inbounds i8, i8* %2, i32 %3
  %coerce.dive5 = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %agg.tmp, i32 0, i32 0
  %4 = load i8*, i8** %coerce.dive5, align 4
  %call6 = call i8* @_ZNSt3__26vectorIcNS_9allocatorIcEEE6insertIPKhEENS_9enable_ifIXaasr27__is_cpp17_forward_iteratorIT_EE5valuesr16is_constructibleIcNS_15iterator_traitsIS8_E9referenceEEE5valueENS_11__wrap_iterIPcEEE4typeENSC_IPKcEES8_S8_(%"class.std::__2::vector.1"* %buffer_, i8* %4, i8* %1, i8* %add.ptr)
  %coerce.dive7 = getelementptr inbounds %"class.std::__2::__wrap_iter.11", %"class.std::__2::__wrap_iter.11"* %coerce, i32 0, i32 0
  store i8* %call6, i8** %coerce.dive7, align 4
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %5 = load i1, i1* %retval, align 1
  ret i1 %5
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNSt3__26vectorIjNS_9allocatorIjEEE4dataEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 0
  %1 = load i32*, i32** %__begin_, align 4
  %call = call i32* @_ZNSt3__212__to_addressIjEEPT_S2_(i32* %1) #6
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIjNS_9allocatorIjEEE5clearEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__old_size = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4sizeEv(%"class.std::__2::vector"* %this1) #6
  store i32 %call, i32* %__old_size, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  call void @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE5clearEv(%"class.std::__2::__vector_base"* %0) #6
  %1 = load i32, i32* %__old_size, align 4
  call void @_ZNKSt3__26vectorIjNS_9allocatorIjEEE17__annotate_shrinkEm(%"class.std::__2::vector"* %this1, i32 %1) #6
  call void @_ZNSt3__26vectorIjNS_9allocatorIjEEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector"* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK5draco13EncoderBuffer18bit_encoder_activeEv(%"class.draco::EncoderBuffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.draco::EncoderBuffer"*, align 4
  store %"class.draco::EncoderBuffer"* %this, %"class.draco::EncoderBuffer"** %this.addr, align 4
  %this1 = load %"class.draco::EncoderBuffer"*, %"class.draco::EncoderBuffer"** %this.addr, align 4
  %bit_encoder_reserved_bytes_ = getelementptr inbounds %"class.draco::EncoderBuffer", %"class.draco::EncoderBuffer"* %this1, i32 0, i32 2
  %0 = load i64, i64* %bit_encoder_reserved_bytes_, align 8
  %cmp = icmp sgt i64 %0, 0
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__26vectorIcNS_9allocatorIcEEE6insertIPKhEENS_9enable_ifIXaasr27__is_cpp17_forward_iteratorIT_EE5valuesr16is_constructibleIcNS_15iterator_traitsIS8_E9referenceEEE5valueENS_11__wrap_iterIPcEEE4typeENSC_IPKcEES8_S8_(%"class.std::__2::vector.1"* %this, i8* %__position.coerce, i8* %__first, i8* %__last) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__wrap_iter.11", align 4
  %__position = alloca %"class.std::__2::__wrap_iter", align 4
  %this.addr = alloca %"class.std::__2::vector.1"*, align 4
  %__first.addr = alloca i8*, align 4
  %__last.addr = alloca i8*, align 4
  %__p = alloca i8*, align 4
  %ref.tmp = alloca %"class.std::__2::__wrap_iter.11", align 4
  %__n = alloca i32, align 4
  %__old_n = alloca i32, align 4
  %__old_last = alloca i8*, align 4
  %__m = alloca i8*, align 4
  %__dx = alloca i32, align 4
  %__diff = alloca i32, align 4
  %__a = alloca %"class.std::__2::allocator.6"*, align 4
  %__v = alloca %"struct.std::__2::__split_buffer", align 4
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %__position, i32 0, i32 0
  store i8* %__position.coerce, i8** %coerce.dive, align 4
  store %"class.std::__2::vector.1"* %this, %"class.std::__2::vector.1"** %this.addr, align 4
  store i8* %__first, i8** %__first.addr, align 4
  store i8* %__last, i8** %__last.addr, align 4
  %this1 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base.2"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %0, i32 0, i32 0
  %1 = load i8*, i8** %__begin_, align 4
  %call = call i8* @_ZNSt3__26vectorIcNS_9allocatorIcEEE5beginEv(%"class.std::__2::vector.1"* %this1) #6
  %coerce.dive2 = getelementptr inbounds %"class.std::__2::__wrap_iter.11", %"class.std::__2::__wrap_iter.11"* %ref.tmp, i32 0, i32 0
  store i8* %call, i8** %coerce.dive2, align 4
  %call3 = call i32 @_ZNSt3__2miIPKcPcEEDTmicldtfp_4baseEcldtfp0_4baseEERKNS_11__wrap_iterIT_EERKNS5_IT0_EE(%"class.std::__2::__wrap_iter"* nonnull align 4 dereferenceable(4) %__position, %"class.std::__2::__wrap_iter.11"* nonnull align 4 dereferenceable(4) %ref.tmp) #6
  %add.ptr = getelementptr inbounds i8, i8* %1, i32 %call3
  store i8* %add.ptr, i8** %__p, align 4
  %2 = load i8*, i8** %__first.addr, align 4
  %3 = load i8*, i8** %__last.addr, align 4
  %call4 = call i32 @_ZNSt3__28distanceIPKhEENS_15iterator_traitsIT_E15difference_typeES4_S4_(i8* %2, i8* %3)
  store i32 %call4, i32* %__n, align 4
  %4 = load i32, i32* %__n, align 4
  %cmp = icmp sgt i32 %4, 0
  br i1 %cmp, label %if.then, label %if.end35

if.then:                                          ; preds = %entry
  %5 = load i32, i32* %__n, align 4
  %6 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base.2"*
  %call5 = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__213__vector_baseIcNS_9allocatorIcEEE9__end_capEv(%"class.std::__2::__vector_base.2"* %6) #6
  %7 = load i8*, i8** %call5, align 4
  %8 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base.2"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %8, i32 0, i32 1
  %9 = load i8*, i8** %__end_, align 4
  %sub.ptr.lhs.cast = ptrtoint i8* %7 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %9 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %cmp6 = icmp sle i32 %5, %sub.ptr.sub
  br i1 %cmp6, label %if.then7, label %if.else

if.then7:                                         ; preds = %if.then
  %10 = load i32, i32* %__n, align 4
  store i32 %10, i32* %__old_n, align 4
  %11 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base.2"*
  %__end_8 = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %11, i32 0, i32 1
  %12 = load i8*, i8** %__end_8, align 4
  store i8* %12, i8** %__old_last, align 4
  %13 = load i8*, i8** %__last.addr, align 4
  store i8* %13, i8** %__m, align 4
  %14 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base.2"*
  %__end_9 = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %14, i32 0, i32 1
  %15 = load i8*, i8** %__end_9, align 4
  %16 = load i8*, i8** %__p, align 4
  %sub.ptr.lhs.cast10 = ptrtoint i8* %15 to i32
  %sub.ptr.rhs.cast11 = ptrtoint i8* %16 to i32
  %sub.ptr.sub12 = sub i32 %sub.ptr.lhs.cast10, %sub.ptr.rhs.cast11
  store i32 %sub.ptr.sub12, i32* %__dx, align 4
  %17 = load i32, i32* %__n, align 4
  %18 = load i32, i32* %__dx, align 4
  %cmp13 = icmp sgt i32 %17, %18
  br i1 %cmp13, label %if.then14, label %if.end

if.then14:                                        ; preds = %if.then7
  %19 = load i8*, i8** %__first.addr, align 4
  store i8* %19, i8** %__m, align 4
  %20 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base.2"*
  %__end_15 = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %20, i32 0, i32 1
  %21 = load i8*, i8** %__end_15, align 4
  %22 = load i8*, i8** %__p, align 4
  %sub.ptr.lhs.cast16 = ptrtoint i8* %21 to i32
  %sub.ptr.rhs.cast17 = ptrtoint i8* %22 to i32
  %sub.ptr.sub18 = sub i32 %sub.ptr.lhs.cast16, %sub.ptr.rhs.cast17
  store i32 %sub.ptr.sub18, i32* %__diff, align 4
  %23 = load i32, i32* %__diff, align 4
  call void @_ZNSt3__27advanceIPKhEEvRT_NS_15iterator_traitsIS3_E15difference_typeE(i8** nonnull align 4 dereferenceable(4) %__m, i32 %23)
  %24 = load i8*, i8** %__m, align 4
  %25 = load i8*, i8** %__last.addr, align 4
  %26 = load i32, i32* %__n, align 4
  %27 = load i32, i32* %__diff, align 4
  %sub = sub nsw i32 %26, %27
  call void @_ZNSt3__26vectorIcNS_9allocatorIcEEE18__construct_at_endIPKhEENS_9enable_ifIXsr27__is_cpp17_forward_iteratorIT_EE5valueEvE4typeES8_S8_m(%"class.std::__2::vector.1"* %this1, i8* %24, i8* %25, i32 %sub)
  %28 = load i32, i32* %__dx, align 4
  store i32 %28, i32* %__n, align 4
  br label %if.end

if.end:                                           ; preds = %if.then14, %if.then7
  %29 = load i32, i32* %__n, align 4
  %cmp19 = icmp sgt i32 %29, 0
  br i1 %cmp19, label %if.then20, label %if.end23

if.then20:                                        ; preds = %if.end
  %30 = load i8*, i8** %__p, align 4
  %31 = load i8*, i8** %__old_last, align 4
  %32 = load i8*, i8** %__p, align 4
  %33 = load i32, i32* %__old_n, align 4
  %add.ptr21 = getelementptr inbounds i8, i8* %32, i32 %33
  call void @_ZNSt3__26vectorIcNS_9allocatorIcEEE12__move_rangeEPcS4_S4_(%"class.std::__2::vector.1"* %this1, i8* %30, i8* %31, i8* %add.ptr21)
  %34 = load i8*, i8** %__first.addr, align 4
  %35 = load i8*, i8** %__m, align 4
  %36 = load i8*, i8** %__p, align 4
  %call22 = call i8* @_ZNSt3__24copyIPKhPcEET0_T_S5_S4_(i8* %34, i8* %35, i8* %36)
  br label %if.end23

if.end23:                                         ; preds = %if.then20, %if.end
  br label %if.end34

if.else:                                          ; preds = %if.then
  %37 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base.2"*
  %call24 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNSt3__213__vector_baseIcNS_9allocatorIcEEE7__allocEv(%"class.std::__2::__vector_base.2"* %37) #6
  store %"class.std::__2::allocator.6"* %call24, %"class.std::__2::allocator.6"** %__a, align 4
  %call25 = call i32 @_ZNKSt3__26vectorIcNS_9allocatorIcEEE4sizeEv(%"class.std::__2::vector.1"* %this1) #6
  %38 = load i32, i32* %__n, align 4
  %add = add i32 %call25, %38
  %call26 = call i32 @_ZNKSt3__26vectorIcNS_9allocatorIcEEE11__recommendEm(%"class.std::__2::vector.1"* %this1, i32 %add)
  %39 = load i8*, i8** %__p, align 4
  %40 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base.2"*
  %__begin_27 = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %40, i32 0, i32 0
  %41 = load i8*, i8** %__begin_27, align 4
  %sub.ptr.lhs.cast28 = ptrtoint i8* %39 to i32
  %sub.ptr.rhs.cast29 = ptrtoint i8* %41 to i32
  %sub.ptr.sub30 = sub i32 %sub.ptr.lhs.cast28, %sub.ptr.rhs.cast29
  %42 = load %"class.std::__2::allocator.6"*, %"class.std::__2::allocator.6"** %__a, align 4
  %call31 = call %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEEC2EmmS3_(%"struct.std::__2::__split_buffer"* %__v, i32 %call26, i32 %sub.ptr.sub30, %"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %42)
  %43 = load i8*, i8** %__first.addr, align 4
  %44 = load i8*, i8** %__last.addr, align 4
  call void @_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE18__construct_at_endIPKhEENS_9enable_ifIXsr27__is_cpp17_forward_iteratorIT_EE5valueEvE4typeES9_S9_(%"struct.std::__2::__split_buffer"* %__v, i8* %43, i8* %44)
  %45 = load i8*, i8** %__p, align 4
  %call32 = call i8* @_ZNSt3__26vectorIcNS_9allocatorIcEEE26__swap_out_circular_bufferERNS_14__split_bufferIcRS2_EEPc(%"class.std::__2::vector.1"* %this1, %"struct.std::__2::__split_buffer"* nonnull align 4 dereferenceable(20) %__v, i8* %45)
  store i8* %call32, i8** %__p, align 4
  %call33 = call %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEED2Ev(%"struct.std::__2::__split_buffer"* %__v) #6
  br label %if.end34

if.end34:                                         ; preds = %if.else, %if.end23
  br label %if.end35

if.end35:                                         ; preds = %if.end34, %entry
  %46 = load i8*, i8** %__p, align 4
  %call36 = call i8* @_ZNSt3__26vectorIcNS_9allocatorIcEEE11__make_iterEPc(%"class.std::__2::vector.1"* %this1, i8* %46) #6
  %coerce.dive37 = getelementptr inbounds %"class.std::__2::__wrap_iter.11", %"class.std::__2::__wrap_iter.11"* %retval, i32 0, i32 0
  store i8* %call36, i8** %coerce.dive37, align 4
  %coerce.dive38 = getelementptr inbounds %"class.std::__2::__wrap_iter.11", %"class.std::__2::__wrap_iter.11"* %retval, i32 0, i32 0
  %47 = load i8*, i8** %coerce.dive38, align 4
  ret i8* %47
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__26vectorIcNS_9allocatorIcEEE3endEv(%"class.std::__2::vector.1"* %this) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__wrap_iter.11", align 4
  %this.addr = alloca %"class.std::__2::vector.1"*, align 4
  store %"class.std::__2::vector.1"* %this, %"class.std::__2::vector.1"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base.2"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %0, i32 0, i32 1
  %1 = load i8*, i8** %__end_, align 4
  %call = call i8* @_ZNSt3__26vectorIcNS_9allocatorIcEEE11__make_iterEPc(%"class.std::__2::vector.1"* %this1, i8* %1) #6
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter.11", %"class.std::__2::__wrap_iter.11"* %retval, i32 0, i32 0
  store i8* %call, i8** %coerce.dive, align 4
  %coerce.dive2 = getelementptr inbounds %"class.std::__2::__wrap_iter.11", %"class.std::__2::__wrap_iter.11"* %retval, i32 0, i32 0
  %2 = load i8*, i8** %coerce.dive2, align 4
  ret i8* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__wrap_iter"* @_ZNSt3__211__wrap_iterIPKcEC2IPcEERKNS0_IT_EEPNS_9enable_ifIXsr14is_convertibleIS6_S2_EE5valueEvE4typeE(%"class.std::__2::__wrap_iter"* returned %this, %"class.std::__2::__wrap_iter.11"* nonnull align 4 dereferenceable(4) %__u, i8* %0) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__wrap_iter"*, align 4
  %__u.addr = alloca %"class.std::__2::__wrap_iter.11"*, align 4
  %.addr = alloca i8*, align 4
  store %"class.std::__2::__wrap_iter"* %this, %"class.std::__2::__wrap_iter"** %this.addr, align 4
  store %"class.std::__2::__wrap_iter.11"* %__u, %"class.std::__2::__wrap_iter.11"** %__u.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::__wrap_iter"*, %"class.std::__2::__wrap_iter"** %this.addr, align 4
  %__i = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::__wrap_iter.11"*, %"class.std::__2::__wrap_iter.11"** %__u.addr, align 4
  %call = call i8* @_ZNKSt3__211__wrap_iterIPcE4baseEv(%"class.std::__2::__wrap_iter.11"* %1) #6
  store i8* %call, i8** %__i, align 4
  ret %"class.std::__2::__wrap_iter"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__2miIPKcPcEEDTmicldtfp_4baseEcldtfp0_4baseEERKNS_11__wrap_iterIT_EERKNS5_IT0_EE(%"class.std::__2::__wrap_iter"* nonnull align 4 dereferenceable(4) %__x, %"class.std::__2::__wrap_iter.11"* nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %__x.addr = alloca %"class.std::__2::__wrap_iter"*, align 4
  %__y.addr = alloca %"class.std::__2::__wrap_iter.11"*, align 4
  store %"class.std::__2::__wrap_iter"* %__x, %"class.std::__2::__wrap_iter"** %__x.addr, align 4
  store %"class.std::__2::__wrap_iter.11"* %__y, %"class.std::__2::__wrap_iter.11"** %__y.addr, align 4
  %0 = load %"class.std::__2::__wrap_iter"*, %"class.std::__2::__wrap_iter"** %__x.addr, align 4
  %call = call i8* @_ZNKSt3__211__wrap_iterIPKcE4baseEv(%"class.std::__2::__wrap_iter"* %0) #6
  %1 = load %"class.std::__2::__wrap_iter.11"*, %"class.std::__2::__wrap_iter.11"** %__y.addr, align 4
  %call1 = call i8* @_ZNKSt3__211__wrap_iterIPcE4baseEv(%"class.std::__2::__wrap_iter.11"* %1) #6
  %sub.ptr.lhs.cast = ptrtoint i8* %call to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %call1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  ret i32 %sub.ptr.sub
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__26vectorIcNS_9allocatorIcEEE5beginEv(%"class.std::__2::vector.1"* %this) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__wrap_iter.11", align 4
  %this.addr = alloca %"class.std::__2::vector.1"*, align 4
  store %"class.std::__2::vector.1"* %this, %"class.std::__2::vector.1"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base.2"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %0, i32 0, i32 0
  %1 = load i8*, i8** %__begin_, align 4
  %call = call i8* @_ZNSt3__26vectorIcNS_9allocatorIcEEE11__make_iterEPc(%"class.std::__2::vector.1"* %this1, i8* %1) #6
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter.11", %"class.std::__2::__wrap_iter.11"* %retval, i32 0, i32 0
  store i8* %call, i8** %coerce.dive, align 4
  %coerce.dive2 = getelementptr inbounds %"class.std::__2::__wrap_iter.11", %"class.std::__2::__wrap_iter.11"* %retval, i32 0, i32 0
  %2 = load i8*, i8** %coerce.dive2, align 4
  ret i8* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__28distanceIPKhEENS_15iterator_traitsIT_E15difference_typeES4_S4_(i8* %__first, i8* %__last) #0 comdat {
entry:
  %__first.addr = alloca i8*, align 4
  %__last.addr = alloca i8*, align 4
  %agg.tmp = alloca %"struct.std::__2::random_access_iterator_tag", align 1
  store i8* %__first, i8** %__first.addr, align 4
  store i8* %__last, i8** %__last.addr, align 4
  %0 = load i8*, i8** %__first.addr, align 4
  %1 = load i8*, i8** %__last.addr, align 4
  %call = call i32 @_ZNSt3__210__distanceIPKhEENS_15iterator_traitsIT_E15difference_typeES4_S4_NS_26random_access_iterator_tagE(i8* %0, i8* %1)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNSt3__213__vector_baseIcNS_9allocatorIcEEE9__end_capEv(%"class.std::__2::__vector_base.2"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.2"*, align 4
  store %"class.std::__2::__vector_base.2"* %this, %"class.std::__2::__vector_base.2"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.2"*, %"class.std::__2::__vector_base.2"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__217__compressed_pairIPcNS_9allocatorIcEEE5firstEv(%"class.std::__2::__compressed_pair.3"* %__end_cap_) #6
  ret i8** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__27advanceIPKhEEvRT_NS_15iterator_traitsIS3_E15difference_typeE(i8** nonnull align 4 dereferenceable(4) %__i, i32 %__n) #0 comdat {
entry:
  %__i.addr = alloca i8**, align 4
  %__n.addr = alloca i32, align 4
  %agg.tmp = alloca %"struct.std::__2::random_access_iterator_tag", align 1
  store i8** %__i, i8*** %__i.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load i8**, i8*** %__i.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29__advanceIPKhEEvRT_NS_15iterator_traitsIS3_E15difference_typeENS_26random_access_iterator_tagE(i8** nonnull align 4 dereferenceable(4) %0, i32 %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIcNS_9allocatorIcEEE18__construct_at_endIPKhEENS_9enable_ifIXsr27__is_cpp17_forward_iteratorIT_EE5valueEvE4typeES8_S8_m(%"class.std::__2::vector.1"* %this, i8* %__first, i8* %__last, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.1"*, align 4
  %__first.addr = alloca i8*, align 4
  %__last.addr = alloca i8*, align 4
  %__n.addr = alloca i32, align 4
  %__tx = alloca %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction", align 4
  store %"class.std::__2::vector.1"* %this, %"class.std::__2::vector.1"** %this.addr, align 4
  store i8* %__first, i8** %__first.addr, align 4
  store i8* %__last, i8** %__last.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %this.addr, align 4
  %0 = load i32, i32* %__n.addr, align 4
  %call = call %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"* @_ZNSt3__26vectorIcNS_9allocatorIcEEE21_ConstructTransactionC2ERS3_m(%"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"* %__tx, %"class.std::__2::vector.1"* nonnull align 4 dereferenceable(12) %this1, i32 %0)
  %1 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base.2"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNSt3__213__vector_baseIcNS_9allocatorIcEEE7__allocEv(%"class.std::__2::__vector_base.2"* %1) #6
  %2 = load i8*, i8** %__first.addr, align 4
  %3 = load i8*, i8** %__last.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction", %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE25__construct_range_forwardIPKhPcEEvRS2_T_S9_RT0_(%"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %call2, i8* %2, i8* %3, i8** nonnull align 4 dereferenceable(4) %__pos_)
  %call3 = call %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"* @_ZNSt3__26vectorIcNS_9allocatorIcEEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"* %__tx) #6
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIcNS_9allocatorIcEEE12__move_rangeEPcS4_S4_(%"class.std::__2::vector.1"* %this, i8* %__from_s, i8* %__from_e, i8* %__to) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.1"*, align 4
  %__from_s.addr = alloca i8*, align 4
  %__from_e.addr = alloca i8*, align 4
  %__to.addr = alloca i8*, align 4
  %__old_last = alloca i8*, align 4
  %__n = alloca i32, align 4
  %__i = alloca i8*, align 4
  %__tx = alloca %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction", align 4
  store %"class.std::__2::vector.1"* %this, %"class.std::__2::vector.1"** %this.addr, align 4
  store i8* %__from_s, i8** %__from_s.addr, align 4
  store i8* %__from_e, i8** %__from_e.addr, align 4
  store i8* %__to, i8** %__to.addr, align 4
  %this1 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base.2"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %0, i32 0, i32 1
  %1 = load i8*, i8** %__end_, align 4
  store i8* %1, i8** %__old_last, align 4
  %2 = load i8*, i8** %__old_last, align 4
  %3 = load i8*, i8** %__to.addr, align 4
  %sub.ptr.lhs.cast = ptrtoint i8* %2 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  store i32 %sub.ptr.sub, i32* %__n, align 4
  %4 = load i8*, i8** %__from_s.addr, align 4
  %5 = load i32, i32* %__n, align 4
  %add.ptr = getelementptr inbounds i8, i8* %4, i32 %5
  store i8* %add.ptr, i8** %__i, align 4
  %6 = load i8*, i8** %__from_e.addr, align 4
  %7 = load i8*, i8** %__i, align 4
  %sub.ptr.lhs.cast2 = ptrtoint i8* %6 to i32
  %sub.ptr.rhs.cast3 = ptrtoint i8* %7 to i32
  %sub.ptr.sub4 = sub i32 %sub.ptr.lhs.cast2, %sub.ptr.rhs.cast3
  %call = call %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"* @_ZNSt3__26vectorIcNS_9allocatorIcEEE21_ConstructTransactionC2ERS3_m(%"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"* %__tx, %"class.std::__2::vector.1"* nonnull align 4 dereferenceable(12) %this1, i32 %sub.ptr.sub4)
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %8 = load i8*, i8** %__i, align 4
  %9 = load i8*, i8** %__from_e.addr, align 4
  %cmp = icmp ult i8* %8, %9
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %10 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base.2"*
  %call5 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNSt3__213__vector_baseIcNS_9allocatorIcEEE7__allocEv(%"class.std::__2::__vector_base.2"* %10) #6
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction", %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %11 = load i8*, i8** %__pos_, align 4
  %call6 = call i8* @_ZNSt3__212__to_addressIcEEPT_S2_(i8* %11) #6
  %12 = load i8*, i8** %__i, align 4
  %call7 = call nonnull align 1 dereferenceable(1) i8* @_ZNSt3__24moveIRcEEONS_16remove_referenceIT_E4typeEOS3_(i8* nonnull align 1 dereferenceable(1) %12) #6
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE9constructIcJcEEEvRS2_PT_DpOT0_(%"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %call5, i8* %call6, i8* nonnull align 1 dereferenceable(1) %call7)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %13 = load i8*, i8** %__i, align 4
  %incdec.ptr = getelementptr inbounds i8, i8* %13, i32 1
  store i8* %incdec.ptr, i8** %__i, align 4
  %__pos_8 = getelementptr inbounds %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction", %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %14 = load i8*, i8** %__pos_8, align 4
  %incdec.ptr9 = getelementptr inbounds i8, i8* %14, i32 1
  store i8* %incdec.ptr9, i8** %__pos_8, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call10 = call %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"* @_ZNSt3__26vectorIcNS_9allocatorIcEEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"* %__tx) #6
  %15 = load i8*, i8** %__from_s.addr, align 4
  %16 = load i8*, i8** %__from_s.addr, align 4
  %17 = load i32, i32* %__n, align 4
  %add.ptr11 = getelementptr inbounds i8, i8* %16, i32 %17
  %18 = load i8*, i8** %__old_last, align 4
  %call12 = call i8* @_ZNSt3__213move_backwardIPcS1_EET0_T_S3_S2_(i8* %15, i8* %add.ptr11, i8* %18)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__24copyIPKhPcEET0_T_S5_S4_(i8* %__first, i8* %__last, i8* %__result) #0 comdat {
entry:
  %__first.addr = alloca i8*, align 4
  %__last.addr = alloca i8*, align 4
  %__result.addr = alloca i8*, align 4
  store i8* %__first, i8** %__first.addr, align 4
  store i8* %__last, i8** %__last.addr, align 4
  store i8* %__result, i8** %__result.addr, align 4
  %0 = load i8*, i8** %__first.addr, align 4
  %call = call i8* @_ZNSt3__213__unwrap_iterIPKhEET_S3_(i8* %0)
  %1 = load i8*, i8** %__last.addr, align 4
  %call1 = call i8* @_ZNSt3__213__unwrap_iterIPKhEET_S3_(i8* %1)
  %2 = load i8*, i8** %__result.addr, align 4
  %call2 = call i8* @_ZNSt3__213__unwrap_iterIPcEET_S2_(i8* %2)
  %call3 = call i8* @_ZNSt3__26__copyIPKhPcEET0_T_S5_S4_(i8* %call, i8* %call1, i8* %call2)
  ret i8* %call3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNSt3__213__vector_baseIcNS_9allocatorIcEEE7__allocEv(%"class.std::__2::__vector_base.2"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.2"*, align 4
  store %"class.std::__2::__vector_base.2"* %this, %"class.std::__2::__vector_base.2"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.2"*, %"class.std::__2::__vector_base.2"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNSt3__217__compressed_pairIPcNS_9allocatorIcEEE6secondEv(%"class.std::__2::__compressed_pair.3"* %__end_cap_) #6
  ret %"class.std::__2::allocator.6"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIcNS_9allocatorIcEEE11__recommendEm(%"class.std::__2::vector.1"* %this, i32 %__new_size) #0 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %"class.std::__2::vector.1"*, align 4
  %__new_size.addr = alloca i32, align 4
  %__ms = alloca i32, align 4
  %__cap = alloca i32, align 4
  %ref.tmp = alloca i32, align 4
  store %"class.std::__2::vector.1"* %this, %"class.std::__2::vector.1"** %this.addr, align 4
  store i32 %__new_size, i32* %__new_size.addr, align 4
  %this1 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIcNS_9allocatorIcEEE8max_sizeEv(%"class.std::__2::vector.1"* %this1) #6
  store i32 %call, i32* %__ms, align 4
  %0 = load i32, i32* %__new_size.addr, align 4
  %1 = load i32, i32* %__ms, align 4
  %cmp = icmp ugt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base_common"*
  call void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"* %2) #7
  unreachable

if.end:                                           ; preds = %entry
  %call2 = call i32 @_ZNKSt3__26vectorIcNS_9allocatorIcEEE8capacityEv(%"class.std::__2::vector.1"* %this1) #6
  store i32 %call2, i32* %__cap, align 4
  %3 = load i32, i32* %__cap, align 4
  %4 = load i32, i32* %__ms, align 4
  %div = udiv i32 %4, 2
  %cmp3 = icmp uge i32 %3, %div
  br i1 %cmp3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %if.end
  %5 = load i32, i32* %__ms, align 4
  store i32 %5, i32* %retval, align 4
  br label %return

if.end5:                                          ; preds = %if.end
  %6 = load i32, i32* %__cap, align 4
  %mul = mul i32 2, %6
  store i32 %mul, i32* %ref.tmp, align 4
  %call6 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %__new_size.addr)
  %7 = load i32, i32* %call6, align 4
  store i32 %7, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end5, %if.then4
  %8 = load i32, i32* %retval, align 4
  ret i32 %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIcNS_9allocatorIcEEE4sizeEv(%"class.std::__2::vector.1"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.1"*, align 4
  store %"class.std::__2::vector.1"* %this, %"class.std::__2::vector.1"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base.2"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %0, i32 0, i32 1
  %1 = load i8*, i8** %__end_, align 4
  %2 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base.2"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %2, i32 0, i32 0
  %3 = load i8*, i8** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint i8* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %3 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  ret i32 %sub.ptr.sub
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEEC2EmmS3_(%"struct.std::__2::__split_buffer"* returned %this, i32 %__cap, i32 %__start, %"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %__a) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"struct.std::__2::__split_buffer"*, align 4
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__cap.addr = alloca i32, align 4
  %__start.addr = alloca i32, align 4
  %__a.addr = alloca %"class.std::__2::allocator.6"*, align 4
  %ref.tmp = alloca i8*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store i32 %__cap, i32* %__cap.addr, align 4
  store i32 %__start, i32* %__start.addr, align 4
  store %"class.std::__2::allocator.6"* %__a, %"class.std::__2::allocator.6"** %__a.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer"* %this1, %"struct.std::__2::__split_buffer"** %retval, align 4
  %0 = bitcast %"struct.std::__2::__split_buffer"* %this1 to %"class.std::__2::__split_buffer_common"*
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  store i8* null, i8** %ref.tmp, align 4
  %1 = load %"class.std::__2::allocator.6"*, %"class.std::__2::allocator.6"** %__a.addr, align 4
  %call = call %"class.std::__2::__compressed_pair.12"* @_ZNSt3__217__compressed_pairIPcRNS_9allocatorIcEEEC2IDnS4_EEOT_OT0_(%"class.std::__2::__compressed_pair.12"* %__end_cap_, i8** nonnull align 4 dereferenceable(4) %ref.tmp, %"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %1)
  %2 = load i32, i32* %__cap.addr, align 4
  %cmp = icmp ne i32 %2, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE7__allocEv(%"struct.std::__2::__split_buffer"* %this1) #6
  %3 = load i32, i32* %__cap.addr, align 4
  %call3 = call i8* @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE8allocateERS2_m(%"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %call2, i32 %3)
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i8* [ %call3, %cond.true ], [ null, %cond.false ]
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  store i8* %cond, i8** %__first_, align 4
  %__first_4 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %4 = load i8*, i8** %__first_4, align 4
  %5 = load i32, i32* %__start.addr, align 4
  %add.ptr = getelementptr inbounds i8, i8* %4, i32 %5
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 2
  store i8* %add.ptr, i8** %__end_, align 4
  %__begin_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 1
  store i8* %add.ptr, i8** %__begin_, align 4
  %__first_5 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %6 = load i8*, i8** %__first_5, align 4
  %7 = load i32, i32* %__cap.addr, align 4
  %add.ptr6 = getelementptr inbounds i8, i8* %6, i32 %7
  %call7 = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this1) #6
  store i8* %add.ptr6, i8** %call7, align 4
  %8 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %retval, align 4
  ret %"struct.std::__2::__split_buffer"* %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE18__construct_at_endIPKhEENS_9enable_ifIXsr27__is_cpp17_forward_iteratorIT_EE5valueEvE4typeES9_S9_(%"struct.std::__2::__split_buffer"* %this, i8* %__first, i8* %__last) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__first.addr = alloca i8*, align 4
  %__last.addr = alloca i8*, align 4
  %__tx = alloca %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction", align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store i8* %__first, i8** %__first.addr, align 4
  store i8* %__last, i8** %__last.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 2
  %0 = load i8*, i8** %__first.addr, align 4
  %1 = load i8*, i8** %__last.addr, align 4
  %call = call i32 @_ZNSt3__28distanceIPKhEENS_15iterator_traitsIT_E15difference_typeES4_S4_(i8* %0, i8* %1)
  %call2 = call %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE21_ConstructTransactionC2EPPcm(%"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"* %__tx, i8** %__end_, i32 %call) #6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %__pos_ = getelementptr inbounds %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"* %__tx, i32 0, i32 0
  %2 = load i8*, i8** %__pos_, align 4
  %__end_3 = getelementptr inbounds %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %3 = load i8*, i8** %__end_3, align 4
  %cmp = icmp ne i8* %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %call4 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE7__allocEv(%"struct.std::__2::__split_buffer"* %this1) #6
  %__pos_5 = getelementptr inbounds %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"* %__tx, i32 0, i32 0
  %4 = load i8*, i8** %__pos_5, align 4
  %call6 = call i8* @_ZNSt3__212__to_addressIcEEPT_S2_(i8* %4) #6
  %5 = load i8*, i8** %__first.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE9constructIcJRKhEEEvRS2_PT_DpOT0_(%"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %call4, i8* %call6, i8* nonnull align 1 dereferenceable(1) %5)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %__pos_7 = getelementptr inbounds %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"* %__tx, i32 0, i32 0
  %6 = load i8*, i8** %__pos_7, align 4
  %incdec.ptr = getelementptr inbounds i8, i8* %6, i32 1
  store i8* %incdec.ptr, i8** %__pos_7, align 4
  %7 = load i8*, i8** %__first.addr, align 4
  %incdec.ptr8 = getelementptr inbounds i8, i8* %7, i32 1
  store i8* %incdec.ptr8, i8** %__first.addr, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call9 = call %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE21_ConstructTransactionD2Ev(%"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"* %__tx) #6
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__26vectorIcNS_9allocatorIcEEE26__swap_out_circular_bufferERNS_14__split_bufferIcRS2_EEPc(%"class.std::__2::vector.1"* %this, %"struct.std::__2::__split_buffer"* nonnull align 4 dereferenceable(20) %__v, i8* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.1"*, align 4
  %__v.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__p.addr = alloca i8*, align 4
  %__r = alloca i8*, align 4
  store %"class.std::__2::vector.1"* %this, %"class.std::__2::vector.1"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer"* %__v, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %this1 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %this.addr, align 4
  call void @_ZNKSt3__26vectorIcNS_9allocatorIcEEE17__annotate_deleteEv(%"class.std::__2::vector.1"* %this1) #6
  %0 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__begin_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %0, i32 0, i32 1
  %1 = load i8*, i8** %__begin_, align 4
  store i8* %1, i8** %__r, align 4
  %2 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base.2"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNSt3__213__vector_baseIcNS_9allocatorIcEEE7__allocEv(%"class.std::__2::__vector_base.2"* %2) #6
  %3 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base.2"*
  %__begin_2 = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %3, i32 0, i32 0
  %4 = load i8*, i8** %__begin_2, align 4
  %5 = load i8*, i8** %__p.addr, align 4
  %6 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__begin_3 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %6, i32 0, i32 1
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE46__construct_backward_with_exception_guaranteesIcEENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS2_PT_S8_EE5valuesr31is_trivially_move_constructibleIS8_EE5valueEvE4typeERS2_S9_S9_RS9_(%"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %call, i8* %4, i8* %5, i8** nonnull align 4 dereferenceable(4) %__begin_3)
  %7 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base.2"*
  %call4 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNSt3__213__vector_baseIcNS_9allocatorIcEEE7__allocEv(%"class.std::__2::__vector_base.2"* %7) #6
  %8 = load i8*, i8** %__p.addr, align 4
  %9 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base.2"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %9, i32 0, i32 1
  %10 = load i8*, i8** %__end_, align 4
  %11 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__end_5 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %11, i32 0, i32 2
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE45__construct_forward_with_exception_guaranteesIcEENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS2_PT_S8_EE5valuesr31is_trivially_move_constructibleIS8_EE5valueEvE4typeERS2_S9_S9_RS9_(%"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %call4, i8* %8, i8* %10, i8** nonnull align 4 dereferenceable(4) %__end_5)
  %12 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base.2"*
  %__begin_6 = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %12, i32 0, i32 0
  %13 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__begin_7 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %13, i32 0, i32 1
  call void @_ZNSt3__24swapIPcEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_(i8** nonnull align 4 dereferenceable(4) %__begin_6, i8** nonnull align 4 dereferenceable(4) %__begin_7) #6
  %14 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base.2"*
  %__end_8 = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %14, i32 0, i32 1
  %15 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__end_9 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %15, i32 0, i32 2
  call void @_ZNSt3__24swapIPcEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_(i8** nonnull align 4 dereferenceable(4) %__end_8, i8** nonnull align 4 dereferenceable(4) %__end_9) #6
  %16 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base.2"*
  %call10 = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__213__vector_baseIcNS_9allocatorIcEEE9__end_capEv(%"class.std::__2::__vector_base.2"* %16) #6
  %17 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE9__end_capEv(%"struct.std::__2::__split_buffer"* %17) #6
  call void @_ZNSt3__24swapIPcEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_(i8** nonnull align 4 dereferenceable(4) %call10, i8** nonnull align 4 dereferenceable(4) %call11) #6
  %18 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__begin_12 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %18, i32 0, i32 1
  %19 = load i8*, i8** %__begin_12, align 4
  %20 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %__v.addr, align 4
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %20, i32 0, i32 0
  store i8* %19, i8** %__first_, align 4
  %call13 = call i32 @_ZNKSt3__26vectorIcNS_9allocatorIcEEE4sizeEv(%"class.std::__2::vector.1"* %this1) #6
  call void @_ZNKSt3__26vectorIcNS_9allocatorIcEEE14__annotate_newEm(%"class.std::__2::vector.1"* %this1, i32 %call13) #6
  call void @_ZNSt3__26vectorIcNS_9allocatorIcEEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector.1"* %this1)
  %21 = load i8*, i8** %__r, align 4
  ret i8* %21
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer"* @_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEED2Ev(%"struct.std::__2::__split_buffer"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"struct.std::__2::__split_buffer"*, align 4
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer"* %this1, %"struct.std::__2::__split_buffer"** %retval, align 4
  call void @_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE5clearEv(%"struct.std::__2::__split_buffer"* %this1) #6
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %0 = load i8*, i8** %__first_, align 4
  %tobool = icmp ne i8* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE7__allocEv(%"struct.std::__2::__split_buffer"* %this1) #6
  %__first_2 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %1 = load i8*, i8** %__first_2, align 4
  %call3 = call i32 @_ZNKSt3__214__split_bufferIcRNS_9allocatorIcEEE8capacityEv(%"struct.std::__2::__split_buffer"* %this1)
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE10deallocateERS2_Pcm(%"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %call, i8* %1, i32 %call3) #6
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %retval, align 4
  ret %"struct.std::__2::__split_buffer"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__26vectorIcNS_9allocatorIcEEE11__make_iterEPc(%"class.std::__2::vector.1"* %this, i8* %__p) #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__wrap_iter.11", align 4
  %this.addr = alloca %"class.std::__2::vector.1"*, align 4
  %__p.addr = alloca i8*, align 4
  store %"class.std::__2::vector.1"* %this, %"class.std::__2::vector.1"** %this.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %this1 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %this.addr, align 4
  %0 = load i8*, i8** %__p.addr, align 4
  %call = call %"class.std::__2::__wrap_iter.11"* @_ZNSt3__211__wrap_iterIPcEC2ES1_(%"class.std::__2::__wrap_iter.11"* %retval, i8* %0) #6
  %coerce.dive = getelementptr inbounds %"class.std::__2::__wrap_iter.11", %"class.std::__2::__wrap_iter.11"* %retval, i32 0, i32 0
  %1 = load i8*, i8** %coerce.dive, align 4
  ret i8* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__211__wrap_iterIPKcE4baseEv(%"class.std::__2::__wrap_iter"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__wrap_iter"*, align 4
  store %"class.std::__2::__wrap_iter"* %this, %"class.std::__2::__wrap_iter"** %this.addr, align 4
  %this1 = load %"class.std::__2::__wrap_iter"*, %"class.std::__2::__wrap_iter"** %this.addr, align 4
  %__i = getelementptr inbounds %"class.std::__2::__wrap_iter", %"class.std::__2::__wrap_iter"* %this1, i32 0, i32 0
  %0 = load i8*, i8** %__i, align 4
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__211__wrap_iterIPcE4baseEv(%"class.std::__2::__wrap_iter.11"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__wrap_iter.11"*, align 4
  store %"class.std::__2::__wrap_iter.11"* %this, %"class.std::__2::__wrap_iter.11"** %this.addr, align 4
  %this1 = load %"class.std::__2::__wrap_iter.11"*, %"class.std::__2::__wrap_iter.11"** %this.addr, align 4
  %__i = getelementptr inbounds %"class.std::__2::__wrap_iter.11", %"class.std::__2::__wrap_iter.11"* %this1, i32 0, i32 0
  %0 = load i8*, i8** %__i, align 4
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__210__distanceIPKhEENS_15iterator_traitsIT_E15difference_typeES4_S4_NS_26random_access_iterator_tagE(i8* %__first, i8* %__last) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::random_access_iterator_tag", align 1
  %__first.addr = alloca i8*, align 4
  %__last.addr = alloca i8*, align 4
  store i8* %__first, i8** %__first.addr, align 4
  store i8* %__last, i8** %__last.addr, align 4
  %1 = load i8*, i8** %__last.addr, align 4
  %2 = load i8*, i8** %__first.addr, align 4
  %sub.ptr.lhs.cast = ptrtoint i8* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %2 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  ret i32 %sub.ptr.sub
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNSt3__217__compressed_pairIPcNS_9allocatorIcEEE5firstEv(%"class.std::__2::__compressed_pair.3"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.3"*, align 4
  store %"class.std::__2::__compressed_pair.3"* %this, %"class.std::__2::__compressed_pair.3"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.3"*, %"class.std::__2::__compressed_pair.3"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.3"* %this1 to %"struct.std::__2::__compressed_pair_elem.4"*
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__222__compressed_pair_elemIPcLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.4"* %0) #6
  ret i8** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNSt3__222__compressed_pair_elemIPcLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.4"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.4"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.4"* %this, %"struct.std::__2::__compressed_pair_elem.4"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.4"*, %"struct.std::__2::__compressed_pair_elem.4"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.4", %"struct.std::__2::__compressed_pair_elem.4"* %this1, i32 0, i32 0
  ret i8** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29__advanceIPKhEEvRT_NS_15iterator_traitsIS3_E15difference_typeENS_26random_access_iterator_tagE(i8** nonnull align 4 dereferenceable(4) %__i, i32 %__n) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::random_access_iterator_tag", align 1
  %__i.addr = alloca i8**, align 4
  %__n.addr = alloca i32, align 4
  store i8** %__i, i8*** %__i.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %2 = load i8**, i8*** %__i.addr, align 4
  %3 = load i8*, i8** %2, align 4
  %add.ptr = getelementptr inbounds i8, i8* %3, i32 %1
  store i8* %add.ptr, i8** %2, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"* @_ZNSt3__26vectorIcNS_9allocatorIcEEE21_ConstructTransactionC2ERS3_m(%"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"* returned %this, %"class.std::__2::vector.1"* nonnull align 4 dereferenceable(12) %__v, i32 %__n) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"*, align 4
  %__v.addr = alloca %"class.std::__2::vector.1"*, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"** %this.addr, align 4
  store %"class.std::__2::vector.1"* %__v, %"class.std::__2::vector.1"** %__v.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"*, %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"** %this.addr, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction", %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %__v.addr, align 4
  store %"class.std::__2::vector.1"* %0, %"class.std::__2::vector.1"** %__v_, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction", %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %1 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %__v.addr, align 4
  %2 = bitcast %"class.std::__2::vector.1"* %1 to %"class.std::__2::__vector_base.2"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %2, i32 0, i32 1
  %3 = load i8*, i8** %__end_, align 4
  store i8* %3, i8** %__pos_, align 4
  %__new_end_ = getelementptr inbounds %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction", %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"* %this1, i32 0, i32 2
  %4 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %__v.addr, align 4
  %5 = bitcast %"class.std::__2::vector.1"* %4 to %"class.std::__2::__vector_base.2"*
  %__end_2 = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %5, i32 0, i32 1
  %6 = load i8*, i8** %__end_2, align 4
  %7 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds i8, i8* %6, i32 %7
  store i8* %add.ptr, i8** %__new_end_, align 4
  ret %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE25__construct_range_forwardIPKhPcEEvRS2_T_S9_RT0_(%"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %__a, i8* %__begin1, i8* %__end1, i8** nonnull align 4 dereferenceable(4) %__begin2) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.6"*, align 4
  %__begin1.addr = alloca i8*, align 4
  %__end1.addr = alloca i8*, align 4
  %__begin2.addr = alloca i8**, align 4
  store %"class.std::__2::allocator.6"* %__a, %"class.std::__2::allocator.6"** %__a.addr, align 4
  store i8* %__begin1, i8** %__begin1.addr, align 4
  store i8* %__end1, i8** %__end1.addr, align 4
  store i8** %__begin2, i8*** %__begin2.addr, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i8*, i8** %__begin1.addr, align 4
  %1 = load i8*, i8** %__end1.addr, align 4
  %cmp = icmp ne i8* %0, %1
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load %"class.std::__2::allocator.6"*, %"class.std::__2::allocator.6"** %__a.addr, align 4
  %3 = load i8**, i8*** %__begin2.addr, align 4
  %4 = load i8*, i8** %3, align 4
  %call = call i8* @_ZNSt3__212__to_addressIcEEPT_S2_(i8* %4) #6
  %5 = load i8*, i8** %__begin1.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE9constructIcJRKhEEEvRS2_PT_DpOT0_(%"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %2, i8* %call, i8* nonnull align 1 dereferenceable(1) %5)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i8*, i8** %__begin1.addr, align 4
  %incdec.ptr = getelementptr inbounds i8, i8* %6, i32 1
  store i8* %incdec.ptr, i8** %__begin1.addr, align 4
  %7 = load i8**, i8*** %__begin2.addr, align 4
  %8 = load i8*, i8** %7, align 4
  %incdec.ptr1 = getelementptr inbounds i8, i8* %8, i32 1
  store i8* %incdec.ptr1, i8** %7, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"* @_ZNSt3__26vectorIcNS_9allocatorIcEEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"*, align 4
  store %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"** %this.addr, align 4
  %this1 = load %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"*, %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction", %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %0 = load i8*, i8** %__pos_, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction", %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %__v_, align 4
  %2 = bitcast %"class.std::__2::vector.1"* %1 to %"class.std::__2::__vector_base.2"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %2, i32 0, i32 1
  store i8* %0, i8** %__end_, align 4
  ret %"struct.std::__2::vector<char, std::__2::allocator<char>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE9constructIcJRKhEEEvRS2_PT_DpOT0_(%"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %__a, i8* %__p, i8* nonnull align 1 dereferenceable(1) %__args) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.6"*, align 4
  %__p.addr = alloca i8*, align 4
  %__args.addr = alloca i8*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_construct", align 1
  store %"class.std::__2::allocator.6"* %__a, %"class.std::__2::allocator.6"** %__a.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  store i8* %__args, i8** %__args.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_construct"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.6"*, %"class.std::__2::allocator.6"** %__a.addr, align 4
  %2 = load i8*, i8** %__p.addr, align 4
  %3 = load i8*, i8** %__args.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) i8* @_ZNSt3__27forwardIRKhEEOT_RNS_16remove_referenceIS3_E4typeE(i8* nonnull align 1 dereferenceable(1) %3) #6
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE11__constructIcJRKhEEEvNS_17integral_constantIbLb1EEERS2_PT_DpOT0_(%"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %1, i8* %2, i8* nonnull align 1 dereferenceable(1) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__212__to_addressIcEEPT_S2_(i8* %__p) #0 comdat {
entry:
  %__p.addr = alloca i8*, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %0 = load i8*, i8** %__p.addr, align 4
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE11__constructIcJRKhEEEvNS_17integral_constantIbLb1EEERS2_PT_DpOT0_(%"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %__a, i8* %__p, i8* nonnull align 1 dereferenceable(1) %__args) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.6"*, align 4
  %__p.addr = alloca i8*, align 4
  %__args.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.6"* %__a, %"class.std::__2::allocator.6"** %__a.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  store i8* %__args, i8** %__args.addr, align 4
  %1 = load %"class.std::__2::allocator.6"*, %"class.std::__2::allocator.6"** %__a.addr, align 4
  %2 = load i8*, i8** %__p.addr, align 4
  %3 = load i8*, i8** %__args.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) i8* @_ZNSt3__27forwardIRKhEEOT_RNS_16remove_referenceIS3_E4typeE(i8* nonnull align 1 dereferenceable(1) %3) #6
  call void @_ZNSt3__29allocatorIcE9constructIcJRKhEEEvPT_DpOT0_(%"class.std::__2::allocator.6"* %1, i8* %2, i8* nonnull align 1 dereferenceable(1) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) i8* @_ZNSt3__27forwardIRKhEEOT_RNS_16remove_referenceIS3_E4typeE(i8* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca i8*, align 4
  store i8* %__t, i8** %__t.addr, align 4
  %0 = load i8*, i8** %__t.addr, align 4
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIcE9constructIcJRKhEEEvPT_DpOT0_(%"class.std::__2::allocator.6"* %this, i8* %__p, i8* nonnull align 1 dereferenceable(1) %__args) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.6"*, align 4
  %__p.addr = alloca i8*, align 4
  %__args.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.6"* %this, %"class.std::__2::allocator.6"** %this.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  store i8* %__args, i8** %__args.addr, align 4
  %this1 = load %"class.std::__2::allocator.6"*, %"class.std::__2::allocator.6"** %this.addr, align 4
  %0 = load i8*, i8** %__p.addr, align 4
  %1 = load i8*, i8** %__args.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) i8* @_ZNSt3__27forwardIRKhEEOT_RNS_16remove_referenceIS3_E4typeE(i8* nonnull align 1 dereferenceable(1) %1) #6
  %2 = load i8, i8* %call, align 1
  store i8 %2, i8* %0, align 1
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE9constructIcJcEEEvRS2_PT_DpOT0_(%"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %__a, i8* %__p, i8* nonnull align 1 dereferenceable(1) %__args) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.6"*, align 4
  %__p.addr = alloca i8*, align 4
  %__args.addr = alloca i8*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_construct.14", align 1
  store %"class.std::__2::allocator.6"* %__a, %"class.std::__2::allocator.6"** %__a.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  store i8* %__args, i8** %__args.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_construct.14"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.6"*, %"class.std::__2::allocator.6"** %__a.addr, align 4
  %2 = load i8*, i8** %__p.addr, align 4
  %3 = load i8*, i8** %__args.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) i8* @_ZNSt3__27forwardIcEEOT_RNS_16remove_referenceIS1_E4typeE(i8* nonnull align 1 dereferenceable(1) %3) #6
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE11__constructIcJcEEEvNS_17integral_constantIbLb1EEERS2_PT_DpOT0_(%"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %1, i8* %2, i8* nonnull align 1 dereferenceable(1) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) i8* @_ZNSt3__24moveIRcEEONS_16remove_referenceIT_E4typeEOS3_(i8* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca i8*, align 4
  store i8* %__t, i8** %__t.addr, align 4
  %0 = load i8*, i8** %__t.addr, align 4
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__213move_backwardIPcS1_EET0_T_S3_S2_(i8* %__first, i8* %__last, i8* %__result) #0 comdat {
entry:
  %__first.addr = alloca i8*, align 4
  %__last.addr = alloca i8*, align 4
  %__result.addr = alloca i8*, align 4
  store i8* %__first, i8** %__first.addr, align 4
  store i8* %__last, i8** %__last.addr, align 4
  store i8* %__result, i8** %__result.addr, align 4
  %0 = load i8*, i8** %__first.addr, align 4
  %call = call i8* @_ZNSt3__213__unwrap_iterIPcEET_S2_(i8* %0)
  %1 = load i8*, i8** %__last.addr, align 4
  %call1 = call i8* @_ZNSt3__213__unwrap_iterIPcEET_S2_(i8* %1)
  %2 = load i8*, i8** %__result.addr, align 4
  %call2 = call i8* @_ZNSt3__213__unwrap_iterIPcEET_S2_(i8* %2)
  %call3 = call i8* @_ZNSt3__215__move_backwardIccEENS_9enable_ifIXaasr7is_sameINS_12remove_constIT_E4typeET0_EE5valuesr28is_trivially_copy_assignableIS6_EE5valueEPS6_E4typeEPS3_SA_S7_(i8* %call, i8* %call1, i8* %call2)
  ret i8* %call3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE11__constructIcJcEEEvNS_17integral_constantIbLb1EEERS2_PT_DpOT0_(%"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %__a, i8* %__p, i8* nonnull align 1 dereferenceable(1) %__args) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.6"*, align 4
  %__p.addr = alloca i8*, align 4
  %__args.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.6"* %__a, %"class.std::__2::allocator.6"** %__a.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  store i8* %__args, i8** %__args.addr, align 4
  %1 = load %"class.std::__2::allocator.6"*, %"class.std::__2::allocator.6"** %__a.addr, align 4
  %2 = load i8*, i8** %__p.addr, align 4
  %3 = load i8*, i8** %__args.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) i8* @_ZNSt3__27forwardIcEEOT_RNS_16remove_referenceIS1_E4typeE(i8* nonnull align 1 dereferenceable(1) %3) #6
  call void @_ZNSt3__29allocatorIcE9constructIcJcEEEvPT_DpOT0_(%"class.std::__2::allocator.6"* %1, i8* %2, i8* nonnull align 1 dereferenceable(1) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) i8* @_ZNSt3__27forwardIcEEOT_RNS_16remove_referenceIS1_E4typeE(i8* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca i8*, align 4
  store i8* %__t, i8** %__t.addr, align 4
  %0 = load i8*, i8** %__t.addr, align 4
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIcE9constructIcJcEEEvPT_DpOT0_(%"class.std::__2::allocator.6"* %this, i8* %__p, i8* nonnull align 1 dereferenceable(1) %__args) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.6"*, align 4
  %__p.addr = alloca i8*, align 4
  %__args.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.6"* %this, %"class.std::__2::allocator.6"** %this.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  store i8* %__args, i8** %__args.addr, align 4
  %this1 = load %"class.std::__2::allocator.6"*, %"class.std::__2::allocator.6"** %this.addr, align 4
  %0 = load i8*, i8** %__p.addr, align 4
  %1 = load i8*, i8** %__args.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) i8* @_ZNSt3__27forwardIcEEOT_RNS_16remove_referenceIS1_E4typeE(i8* nonnull align 1 dereferenceable(1) %1) #6
  %2 = load i8, i8* %call, align 1
  store i8 %2, i8* %0, align 1
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__215__move_backwardIccEENS_9enable_ifIXaasr7is_sameINS_12remove_constIT_E4typeET0_EE5valuesr28is_trivially_copy_assignableIS6_EE5valueEPS6_E4typeEPS3_SA_S7_(i8* %__first, i8* %__last, i8* %__result) #0 comdat {
entry:
  %__first.addr = alloca i8*, align 4
  %__last.addr = alloca i8*, align 4
  %__result.addr = alloca i8*, align 4
  %__n = alloca i32, align 4
  store i8* %__first, i8** %__first.addr, align 4
  store i8* %__last, i8** %__last.addr, align 4
  store i8* %__result, i8** %__result.addr, align 4
  %0 = load i8*, i8** %__last.addr, align 4
  %1 = load i8*, i8** %__first.addr, align 4
  %sub.ptr.lhs.cast = ptrtoint i8* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  store i32 %sub.ptr.sub, i32* %__n, align 4
  %2 = load i32, i32* %__n, align 4
  %cmp = icmp ugt i32 %2, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %3 = load i32, i32* %__n, align 4
  %4 = load i8*, i8** %__result.addr, align 4
  %idx.neg = sub i32 0, %3
  %add.ptr = getelementptr inbounds i8, i8* %4, i32 %idx.neg
  store i8* %add.ptr, i8** %__result.addr, align 4
  %5 = load i8*, i8** %__result.addr, align 4
  %6 = load i8*, i8** %__first.addr, align 4
  %7 = load i32, i32* %__n, align 4
  %mul = mul i32 %7, 1
  call void @llvm.memmove.p0i8.p0i8.i32(i8* align 1 %5, i8* align 1 %6, i32 %mul, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %8 = load i8*, i8** %__result.addr, align 4
  ret i8* %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__213__unwrap_iterIPcEET_S2_(i8* %__i) #0 comdat {
entry:
  %__i.addr = alloca i8*, align 4
  store i8* %__i, i8** %__i.addr, align 4
  %0 = load i8*, i8** %__i.addr, align 4
  ret i8* %0
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memmove.p0i8.p0i8.i32(i8* nocapture, i8* nocapture readonly, i32, i1 immarg) #1

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__26__copyIPKhPcEET0_T_S5_S4_(i8* %__first, i8* %__last, i8* %__result) #0 comdat {
entry:
  %__first.addr = alloca i8*, align 4
  %__last.addr = alloca i8*, align 4
  %__result.addr = alloca i8*, align 4
  store i8* %__first, i8** %__first.addr, align 4
  store i8* %__last, i8** %__last.addr, align 4
  store i8* %__result, i8** %__result.addr, align 4
  %0 = load i8*, i8** %__first.addr, align 4
  %1 = load i8*, i8** %__last.addr, align 4
  %2 = load i8*, i8** %__result.addr, align 4
  %call = call i8* @_ZNSt3__216__copy_constexprIPKhPcEET0_T_S5_S4_(i8* %0, i8* %1, i8* %2)
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__213__unwrap_iterIPKhEET_S3_(i8* %__i) #0 comdat {
entry:
  %__i.addr = alloca i8*, align 4
  store i8* %__i, i8** %__i.addr, align 4
  %0 = load i8*, i8** %__i.addr, align 4
  ret i8* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__216__copy_constexprIPKhPcEET0_T_S5_S4_(i8* %__first, i8* %__last, i8* %__result) #0 comdat {
entry:
  %__first.addr = alloca i8*, align 4
  %__last.addr = alloca i8*, align 4
  %__result.addr = alloca i8*, align 4
  store i8* %__first, i8** %__first.addr, align 4
  store i8* %__last, i8** %__last.addr, align 4
  store i8* %__result, i8** %__result.addr, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i8*, i8** %__first.addr, align 4
  %1 = load i8*, i8** %__last.addr, align 4
  %cmp = icmp ne i8* %0, %1
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load i8*, i8** %__first.addr, align 4
  %3 = load i8, i8* %2, align 1
  %4 = load i8*, i8** %__result.addr, align 4
  store i8 %3, i8* %4, align 1
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i8*, i8** %__first.addr, align 4
  %incdec.ptr = getelementptr inbounds i8, i8* %5, i32 1
  store i8* %incdec.ptr, i8** %__first.addr, align 4
  %6 = load i8*, i8** %__result.addr, align 4
  %incdec.ptr1 = getelementptr inbounds i8, i8* %6, i32 1
  store i8* %incdec.ptr1, i8** %__result.addr, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = load i8*, i8** %__result.addr, align 4
  ret i8* %7
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNSt3__217__compressed_pairIPcNS_9allocatorIcEEE6secondEv(%"class.std::__2::__compressed_pair.3"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.3"*, align 4
  store %"class.std::__2::__compressed_pair.3"* %this, %"class.std::__2::__compressed_pair.3"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.3"*, %"class.std::__2::__compressed_pair.3"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.3"* %this1 to %"struct.std::__2::__compressed_pair_elem.5"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIcEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.5"* %0) #6
  ret %"class.std::__2::allocator.6"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIcEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.5"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.5"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.5"* %this, %"struct.std::__2::__compressed_pair_elem.5"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.5"*, %"struct.std::__2::__compressed_pair_elem.5"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.5"* %this1 to %"class.std::__2::allocator.6"*
  ret %"class.std::__2::allocator.6"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIcNS_9allocatorIcEEE8max_sizeEv(%"class.std::__2::vector.1"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.1"*, align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp3 = alloca i32, align 4
  store %"class.std::__2::vector.1"* %this, %"class.std::__2::vector.1"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base.2"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNKSt3__213__vector_baseIcNS_9allocatorIcEEE7__allocEv(%"class.std::__2::__vector_base.2"* %0) #6
  %call2 = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE8max_sizeERKS2_(%"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %call) #6
  store i32 %call2, i32* %ref.tmp, align 4
  %call4 = call i32 @_ZNSt3__214numeric_limitsIlE3maxEv() #6
  store i32 %call4, i32* %ref.tmp3, align 4
  %call5 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %1 = load i32, i32* %call5, align 4
  ret i32 %1
}

; Function Attrs: noreturn
declare void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"*) #2

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIcNS_9allocatorIcEEE8capacityEv(%"class.std::__2::vector.1"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.1"*, align 4
  store %"class.std::__2::vector.1"* %this, %"class.std::__2::vector.1"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base.2"*
  %call = call i32 @_ZNKSt3__213__vector_baseIcNS_9allocatorIcEEE8capacityEv(%"class.std::__2::__vector_base.2"* %0) #6
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::__less", align 1
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__a.addr, align 4
  %1 = load i32*, i32** %__b.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::__less", align 1
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__a.addr, align 4
  %1 = load i32*, i32** %__b.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE8max_sizeERKS2_(%"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.6"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_max_size", align 1
  store %"class.std::__2::allocator.6"* %__a, %"class.std::__2::allocator.6"** %__a.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_max_size"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.6"*, %"class.std::__2::allocator.6"** %__a.addr, align 4
  %call = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE10__max_sizeENS_17integral_constantIbLb1EEERKS2_(%"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %1) #6
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNKSt3__213__vector_baseIcNS_9allocatorIcEEE7__allocEv(%"class.std::__2::__vector_base.2"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.2"*, align 4
  store %"class.std::__2::__vector_base.2"* %this, %"class.std::__2::__vector_base.2"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.2"*, %"class.std::__2::__vector_base.2"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNKSt3__217__compressed_pairIPcNS_9allocatorIcEEE6secondEv(%"class.std::__2::__compressed_pair.3"* %__end_cap_) #6
  ret %"class.std::__2::allocator.6"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__214numeric_limitsIlE3maxEv() #0 comdat {
entry:
  %call = call i32 @_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv() #6
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__comp = alloca %"struct.std::__2::__less", align 1
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__b.addr, align 4
  %1 = load i32*, i32** %__a.addr, align 4
  %call = call zeroext i1 @_ZNKSt3__26__lessImmEclERKmS3_(%"struct.std::__2::__less"* %__comp, i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  br i1 %call, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32*, i32** %__b.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %3 = load i32*, i32** %__a.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi i32* [ %2, %cond.true ], [ %3, %cond.false ]
  ret i32* %cond-lvalue
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNKSt3__26__lessImmEclERKmS3_(%"struct.std::__2::__less"* %this, i32* nonnull align 4 dereferenceable(4) %__x, i32* nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__less"*, align 4
  %__x.addr = alloca i32*, align 4
  %__y.addr = alloca i32*, align 4
  store %"struct.std::__2::__less"* %this, %"struct.std::__2::__less"** %this.addr, align 4
  store i32* %__x, i32** %__x.addr, align 4
  store i32* %__y, i32** %__y.addr, align 4
  %this1 = load %"struct.std::__2::__less"*, %"struct.std::__2::__less"** %this.addr, align 4
  %0 = load i32*, i32** %__x.addr, align 4
  %1 = load i32, i32* %0, align 4
  %2 = load i32*, i32** %__y.addr, align 4
  %3 = load i32, i32* %2, align 4
  %cmp = icmp ult i32 %1, %3
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE10__max_sizeENS_17integral_constantIbLb1EEERKS2_(%"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.6"*, align 4
  store %"class.std::__2::allocator.6"* %__a, %"class.std::__2::allocator.6"** %__a.addr, align 4
  %1 = load %"class.std::__2::allocator.6"*, %"class.std::__2::allocator.6"** %__a.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorIcE8max_sizeEv(%"class.std::__2::allocator.6"* %1) #6
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__29allocatorIcE8max_sizeEv(%"class.std::__2::allocator.6"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.6"*, align 4
  store %"class.std::__2::allocator.6"* %this, %"class.std::__2::allocator.6"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator.6"*, %"class.std::__2::allocator.6"** %this.addr, align 4
  ret i32 -1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNKSt3__217__compressed_pairIPcNS_9allocatorIcEEE6secondEv(%"class.std::__2::__compressed_pair.3"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.3"*, align 4
  store %"class.std::__2::__compressed_pair.3"* %this, %"class.std::__2::__compressed_pair.3"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.3"*, %"class.std::__2::__compressed_pair.3"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.3"* %this1 to %"struct.std::__2::__compressed_pair_elem.5"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorIcEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.5"* %0) #6
  ret %"class.std::__2::allocator.6"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorIcEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.5"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.5"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.5"* %this, %"struct.std::__2::__compressed_pair_elem.5"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.5"*, %"struct.std::__2::__compressed_pair_elem.5"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.5"* %this1 to %"class.std::__2::allocator.6"*
  ret %"class.std::__2::allocator.6"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__223__libcpp_numeric_limitsIlLb1EE3maxEv() #0 comdat {
entry:
  ret i32 2147483647
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseIcNS_9allocatorIcEEE8capacityEv(%"class.std::__2::__vector_base.2"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.2"*, align 4
  store %"class.std::__2::__vector_base.2"* %this, %"class.std::__2::__vector_base.2"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.2"*, %"class.std::__2::__vector_base.2"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__213__vector_baseIcNS_9allocatorIcEEE9__end_capEv(%"class.std::__2::__vector_base.2"* %this1) #6
  %0 = load i8*, i8** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %this1, i32 0, i32 0
  %1 = load i8*, i8** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint i8* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  ret i32 %sub.ptr.sub
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__213__vector_baseIcNS_9allocatorIcEEE9__end_capEv(%"class.std::__2::__vector_base.2"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base.2"*, align 4
  store %"class.std::__2::__vector_base.2"* %this, %"class.std::__2::__vector_base.2"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base.2"*, %"class.std::__2::__vector_base.2"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__217__compressed_pairIPcNS_9allocatorIcEEE5firstEv(%"class.std::__2::__compressed_pair.3"* %__end_cap_) #6
  ret i8** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__217__compressed_pairIPcNS_9allocatorIcEEE5firstEv(%"class.std::__2::__compressed_pair.3"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.3"*, align 4
  store %"class.std::__2::__compressed_pair.3"* %this, %"class.std::__2::__compressed_pair.3"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.3"*, %"class.std::__2::__compressed_pair.3"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.3"* %this1 to %"struct.std::__2::__compressed_pair_elem.4"*
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__222__compressed_pair_elemIPcLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.4"* %0) #6
  ret i8** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__222__compressed_pair_elemIPcLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.4"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.4"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.4"* %this, %"struct.std::__2::__compressed_pair_elem.4"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.4"*, %"struct.std::__2::__compressed_pair_elem.4"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.4", %"struct.std::__2::__compressed_pair_elem.4"* %this1, i32 0, i32 0
  ret i8** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImNS_6__lessImmEEEERKT_S5_S5_T0_(i32* nonnull align 4 dereferenceable(4) %__a, i32* nonnull align 4 dereferenceable(4) %__b) #0 comdat {
entry:
  %__comp = alloca %"struct.std::__2::__less", align 1
  %__a.addr = alloca i32*, align 4
  %__b.addr = alloca i32*, align 4
  store i32* %__a, i32** %__a.addr, align 4
  store i32* %__b, i32** %__b.addr, align 4
  %0 = load i32*, i32** %__a.addr, align 4
  %1 = load i32*, i32** %__b.addr, align 4
  %call = call zeroext i1 @_ZNKSt3__26__lessImmEclERKmS3_(%"struct.std::__2::__less"* %__comp, i32* nonnull align 4 dereferenceable(4) %0, i32* nonnull align 4 dereferenceable(4) %1)
  br i1 %call, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32*, i32** %__b.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %3 = load i32*, i32** %__a.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi i32* [ %2, %cond.true ], [ %3, %cond.false ]
  ret i32* %cond-lvalue
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.12"* @_ZNSt3__217__compressed_pairIPcRNS_9allocatorIcEEEC2IDnS4_EEOT_OT0_(%"class.std::__2::__compressed_pair.12"* returned %this, i8** nonnull align 4 dereferenceable(4) %__t1, %"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.12"*, align 4
  %__t1.addr = alloca i8**, align 4
  %__t2.addr = alloca %"class.std::__2::allocator.6"*, align 4
  store %"class.std::__2::__compressed_pair.12"* %this, %"class.std::__2::__compressed_pair.12"** %this.addr, align 4
  store i8** %__t1, i8*** %__t1.addr, align 4
  store %"class.std::__2::allocator.6"* %__t2, %"class.std::__2::allocator.6"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.12"*, %"class.std::__2::__compressed_pair.12"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.12"* %this1 to %"struct.std::__2::__compressed_pair_elem.4"*
  %1 = load i8**, i8*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %1) #6
  %call2 = call %"struct.std::__2::__compressed_pair_elem.4"* @_ZNSt3__222__compressed_pair_elemIPcLi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.4"* %0, i8** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.12"* %this1 to i8*
  %3 = getelementptr inbounds i8, i8* %2, i32 4
  %4 = bitcast i8* %3 to %"struct.std::__2::__compressed_pair_elem.13"*
  %5 = load %"class.std::__2::allocator.6"*, %"class.std::__2::allocator.6"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNSt3__27forwardIRNS_9allocatorIcEEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %5) #6
  %call4 = call %"struct.std::__2::__compressed_pair_elem.13"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIcEELi1ELb0EEC2IS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.13"* %4, %"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::__compressed_pair.12"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE8allocateERS2_m(%"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %__a, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.6"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.6"* %__a, %"class.std::__2::allocator.6"** %__a.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.6"*, %"class.std::__2::allocator.6"** %__a.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call i8* @_ZNSt3__29allocatorIcE8allocateEmPKv(%"class.std::__2::allocator.6"* %0, i32 %1, i8* null)
  ret i8* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE7__allocEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNSt3__217__compressed_pairIPcRNS_9allocatorIcEEE6secondEv(%"class.std::__2::__compressed_pair.12"* %__end_cap_) #6
  ret %"class.std::__2::allocator.6"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__217__compressed_pairIPcRNS_9allocatorIcEEE5firstEv(%"class.std::__2::__compressed_pair.12"* %__end_cap_) #6
  ret i8** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca i8**, align 4
  store i8** %__t, i8*** %__t.addr, align 4
  %0 = load i8**, i8*** %__t.addr, align 4
  ret i8** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.4"* @_ZNSt3__222__compressed_pair_elemIPcLi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem.4"* returned %this, i8** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.4"*, align 4
  %__u.addr = alloca i8**, align 4
  store %"struct.std::__2::__compressed_pair_elem.4"* %this, %"struct.std::__2::__compressed_pair_elem.4"** %this.addr, align 4
  store i8** %__u, i8*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.4"*, %"struct.std::__2::__compressed_pair_elem.4"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.4", %"struct.std::__2::__compressed_pair_elem.4"* %this1, i32 0, i32 0
  %0 = load i8**, i8*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %0) #6
  store i8* null, i8** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.4"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNSt3__27forwardIRNS_9allocatorIcEEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::allocator.6"*, align 4
  store %"class.std::__2::allocator.6"* %__t, %"class.std::__2::allocator.6"** %__t.addr, align 4
  %0 = load %"class.std::__2::allocator.6"*, %"class.std::__2::allocator.6"** %__t.addr, align 4
  ret %"class.std::__2::allocator.6"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.13"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIcEELi1ELb0EEC2IS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.13"* returned %this, %"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.13"*, align 4
  %__u.addr = alloca %"class.std::__2::allocator.6"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.13"* %this, %"struct.std::__2::__compressed_pair_elem.13"** %this.addr, align 4
  store %"class.std::__2::allocator.6"* %__u, %"class.std::__2::allocator.6"** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.13"*, %"struct.std::__2::__compressed_pair_elem.13"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.13", %"struct.std::__2::__compressed_pair_elem.13"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::allocator.6"*, %"class.std::__2::allocator.6"** %__u.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNSt3__27forwardIRNS_9allocatorIcEEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %0) #6
  store %"class.std::__2::allocator.6"* %call, %"class.std::__2::allocator.6"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.13"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__29allocatorIcE8allocateEmPKv(%"class.std::__2::allocator.6"* %this, i32 %__n, i8* %0) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.6"*, align 4
  %__n.addr = alloca i32, align 4
  %.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.6"* %this, %"class.std::__2::allocator.6"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::allocator.6"*, %"class.std::__2::allocator.6"** %this.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorIcE8max_sizeEv(%"class.std::__2::allocator.6"* %this1) #6
  %cmp = icmp ugt i32 %1, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__220__throw_length_errorEPKc(i8* getelementptr inbounds ([68 x i8], [68 x i8]* @.str, i32 0, i32 0)) #7
  unreachable

if.end:                                           ; preds = %entry
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 1
  %call2 = call i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %mul, i32 1)
  ret i8* %call2
}

; Function Attrs: noinline noreturn nounwind optnone
define linkonce_odr hidden void @_ZNSt3__220__throw_length_errorEPKc(i8* %__msg) #3 comdat {
entry:
  %__msg.addr = alloca i8*, align 4
  store i8* %__msg, i8** %__msg.addr, align 4
  call void @abort() #7
  unreachable
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %__size, i32 %__align) #0 comdat {
entry:
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i32, i32* %__size.addr, align 4
  %call = call noalias nonnull i8* @_Znwm(i32 %0) #8
  ret i8* %call
}

; Function Attrs: noreturn
declare void @abort() #2

; Function Attrs: nobuiltin allocsize(0)
declare nonnull i8* @_Znwm(i32) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNSt3__217__compressed_pairIPcRNS_9allocatorIcEEE6secondEv(%"class.std::__2::__compressed_pair.12"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.12"*, align 4
  store %"class.std::__2::__compressed_pair.12"* %this, %"class.std::__2::__compressed_pair.12"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.12"*, %"class.std::__2::__compressed_pair.12"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.12"* %this1 to i8*
  %add.ptr = getelementptr inbounds i8, i8* %0, i32 4
  %1 = bitcast i8* %add.ptr to %"struct.std::__2::__compressed_pair_elem.13"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIcEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.13"* %1) #6
  ret %"class.std::__2::allocator.6"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIcEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.13"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.13"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.13"* %this, %"struct.std::__2::__compressed_pair_elem.13"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.13"*, %"struct.std::__2::__compressed_pair_elem.13"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.13", %"struct.std::__2::__compressed_pair_elem.13"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::allocator.6"*, %"class.std::__2::allocator.6"** %__value_, align 4
  ret %"class.std::__2::allocator.6"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNSt3__217__compressed_pairIPcRNS_9allocatorIcEEE5firstEv(%"class.std::__2::__compressed_pair.12"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.12"*, align 4
  store %"class.std::__2::__compressed_pair.12"* %this, %"class.std::__2::__compressed_pair.12"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.12"*, %"class.std::__2::__compressed_pair.12"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.12"* %this1 to %"struct.std::__2::__compressed_pair_elem.4"*
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__222__compressed_pair_elemIPcLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.4"* %0) #6
  ret i8** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE21_ConstructTransactionC2EPPcm(%"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"* returned %this, i8** %__p, i32 %__n) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"*, align 4
  %__p.addr = alloca i8**, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"* %this, %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"** %this.addr, align 4
  store i8** %__p, i8*** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"*, %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load i8**, i8*** %__p.addr, align 4
  %1 = load i8*, i8** %0, align 4
  store i8* %1, i8** %__pos_, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"* %this1, i32 0, i32 1
  %2 = load i8**, i8*** %__p.addr, align 4
  %3 = load i8*, i8** %2, align 4
  %4 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds i8, i8* %3, i32 %4
  store i8* %add.ptr, i8** %__end_, align 4
  %__dest_ = getelementptr inbounds %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"* %this1, i32 0, i32 2
  %5 = load i8**, i8*** %__p.addr, align 4
  store i8** %5, i8*** %__dest_, align 4
  ret %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"* @_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE21_ConstructTransactionD2Ev(%"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"*, align 4
  store %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"* %this, %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"*, %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load i8*, i8** %__pos_, align 4
  %__dest_ = getelementptr inbounds %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction", %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"* %this1, i32 0, i32 2
  %1 = load i8**, i8*** %__dest_, align 4
  store i8* %0, i8** %1, align 4
  ret %"struct.std::__2::__split_buffer<char, std::__2::allocator<char> &>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIcNS_9allocatorIcEEE17__annotate_deleteEv(%"class.std::__2::vector.1"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.1"*, align 4
  store %"class.std::__2::vector.1"* %this, %"class.std::__2::vector.1"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %this.addr, align 4
  %call = call i8* @_ZNKSt3__26vectorIcNS_9allocatorIcEEE4dataEv(%"class.std::__2::vector.1"* %this1) #6
  %call2 = call i8* @_ZNKSt3__26vectorIcNS_9allocatorIcEEE4dataEv(%"class.std::__2::vector.1"* %this1) #6
  %call3 = call i32 @_ZNKSt3__26vectorIcNS_9allocatorIcEEE8capacityEv(%"class.std::__2::vector.1"* %this1) #6
  %add.ptr = getelementptr inbounds i8, i8* %call2, i32 %call3
  %call4 = call i8* @_ZNKSt3__26vectorIcNS_9allocatorIcEEE4dataEv(%"class.std::__2::vector.1"* %this1) #6
  %call5 = call i32 @_ZNKSt3__26vectorIcNS_9allocatorIcEEE4sizeEv(%"class.std::__2::vector.1"* %this1) #6
  %add.ptr6 = getelementptr inbounds i8, i8* %call4, i32 %call5
  %call7 = call i8* @_ZNKSt3__26vectorIcNS_9allocatorIcEEE4dataEv(%"class.std::__2::vector.1"* %this1) #6
  %call8 = call i32 @_ZNKSt3__26vectorIcNS_9allocatorIcEEE8capacityEv(%"class.std::__2::vector.1"* %this1) #6
  %add.ptr9 = getelementptr inbounds i8, i8* %call7, i32 %call8
  call void @_ZNKSt3__26vectorIcNS_9allocatorIcEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector.1"* %this1, i8* %call, i8* %add.ptr, i8* %add.ptr6, i8* %add.ptr9) #6
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE46__construct_backward_with_exception_guaranteesIcEENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS2_PT_S8_EE5valuesr31is_trivially_move_constructibleIS8_EE5valueEvE4typeERS2_S9_S9_RS9_(%"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %0, i8* %__begin1, i8* %__end1, i8** nonnull align 4 dereferenceable(4) %__end2) #0 comdat {
entry:
  %.addr = alloca %"class.std::__2::allocator.6"*, align 4
  %__begin1.addr = alloca i8*, align 4
  %__end1.addr = alloca i8*, align 4
  %__end2.addr = alloca i8**, align 4
  %_Np = alloca i32, align 4
  store %"class.std::__2::allocator.6"* %0, %"class.std::__2::allocator.6"** %.addr, align 4
  store i8* %__begin1, i8** %__begin1.addr, align 4
  store i8* %__end1, i8** %__end1.addr, align 4
  store i8** %__end2, i8*** %__end2.addr, align 4
  %1 = load i8*, i8** %__end1.addr, align 4
  %2 = load i8*, i8** %__begin1.addr, align 4
  %sub.ptr.lhs.cast = ptrtoint i8* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %2 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  store i32 %sub.ptr.sub, i32* %_Np, align 4
  %3 = load i32, i32* %_Np, align 4
  %4 = load i8**, i8*** %__end2.addr, align 4
  %5 = load i8*, i8** %4, align 4
  %idx.neg = sub i32 0, %3
  %add.ptr = getelementptr inbounds i8, i8* %5, i32 %idx.neg
  store i8* %add.ptr, i8** %4, align 4
  %6 = load i32, i32* %_Np, align 4
  %cmp = icmp sgt i32 %6, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %7 = load i8**, i8*** %__end2.addr, align 4
  %8 = load i8*, i8** %7, align 4
  %9 = load i8*, i8** %__begin1.addr, align 4
  %10 = load i32, i32* %_Np, align 4
  %mul = mul i32 %10, 1
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %8, i8* align 1 %9, i32 %mul, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE45__construct_forward_with_exception_guaranteesIcEENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS2_PT_S8_EE5valuesr31is_trivially_move_constructibleIS8_EE5valueEvE4typeERS2_S9_S9_RS9_(%"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %0, i8* %__begin1, i8* %__end1, i8** nonnull align 4 dereferenceable(4) %__begin2) #0 comdat {
entry:
  %.addr = alloca %"class.std::__2::allocator.6"*, align 4
  %__begin1.addr = alloca i8*, align 4
  %__end1.addr = alloca i8*, align 4
  %__begin2.addr = alloca i8**, align 4
  %_Np = alloca i32, align 4
  store %"class.std::__2::allocator.6"* %0, %"class.std::__2::allocator.6"** %.addr, align 4
  store i8* %__begin1, i8** %__begin1.addr, align 4
  store i8* %__end1, i8** %__end1.addr, align 4
  store i8** %__begin2, i8*** %__begin2.addr, align 4
  %1 = load i8*, i8** %__end1.addr, align 4
  %2 = load i8*, i8** %__begin1.addr, align 4
  %sub.ptr.lhs.cast = ptrtoint i8* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %2 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  store i32 %sub.ptr.sub, i32* %_Np, align 4
  %3 = load i32, i32* %_Np, align 4
  %cmp = icmp sgt i32 %3, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = load i8**, i8*** %__begin2.addr, align 4
  %5 = load i8*, i8** %4, align 4
  %6 = load i8*, i8** %__begin1.addr, align 4
  %7 = load i32, i32* %_Np, align 4
  %mul = mul i32 %7, 1
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %5, i8* align 1 %6, i32 %mul, i1 false)
  %8 = load i32, i32* %_Np, align 4
  %9 = load i8**, i8*** %__begin2.addr, align 4
  %10 = load i8*, i8** %9, align 4
  %add.ptr = getelementptr inbounds i8, i8* %10, i32 %8
  store i8* %add.ptr, i8** %9, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__24swapIPcEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_(i8** nonnull align 4 dereferenceable(4) %__x, i8** nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %__x.addr = alloca i8**, align 4
  %__y.addr = alloca i8**, align 4
  %__t = alloca i8*, align 4
  store i8** %__x, i8*** %__x.addr, align 4
  store i8** %__y, i8*** %__y.addr, align 4
  %0 = load i8**, i8*** %__x.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__24moveIRPcEEONS_16remove_referenceIT_E4typeEOS4_(i8** nonnull align 4 dereferenceable(4) %0) #6
  %1 = load i8*, i8** %call, align 4
  store i8* %1, i8** %__t, align 4
  %2 = load i8**, i8*** %__y.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__24moveIRPcEEONS_16remove_referenceIT_E4typeEOS4_(i8** nonnull align 4 dereferenceable(4) %2) #6
  %3 = load i8*, i8** %call1, align 4
  %4 = load i8**, i8*** %__x.addr, align 4
  store i8* %3, i8** %4, align 4
  %call2 = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__24moveIRPcEEONS_16remove_referenceIT_E4typeEOS4_(i8** nonnull align 4 dereferenceable(4) %__t) #6
  %5 = load i8*, i8** %call2, align 4
  %6 = load i8**, i8*** %__y.addr, align 4
  store i8* %5, i8** %6, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIcNS_9allocatorIcEEE14__annotate_newEm(%"class.std::__2::vector.1"* %this, i32 %__current_size) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.1"*, align 4
  %__current_size.addr = alloca i32, align 4
  store %"class.std::__2::vector.1"* %this, %"class.std::__2::vector.1"** %this.addr, align 4
  store i32 %__current_size, i32* %__current_size.addr, align 4
  %this1 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %this.addr, align 4
  %call = call i8* @_ZNKSt3__26vectorIcNS_9allocatorIcEEE4dataEv(%"class.std::__2::vector.1"* %this1) #6
  %call2 = call i8* @_ZNKSt3__26vectorIcNS_9allocatorIcEEE4dataEv(%"class.std::__2::vector.1"* %this1) #6
  %call3 = call i32 @_ZNKSt3__26vectorIcNS_9allocatorIcEEE8capacityEv(%"class.std::__2::vector.1"* %this1) #6
  %add.ptr = getelementptr inbounds i8, i8* %call2, i32 %call3
  %call4 = call i8* @_ZNKSt3__26vectorIcNS_9allocatorIcEEE4dataEv(%"class.std::__2::vector.1"* %this1) #6
  %call5 = call i32 @_ZNKSt3__26vectorIcNS_9allocatorIcEEE8capacityEv(%"class.std::__2::vector.1"* %this1) #6
  %add.ptr6 = getelementptr inbounds i8, i8* %call4, i32 %call5
  %call7 = call i8* @_ZNKSt3__26vectorIcNS_9allocatorIcEEE4dataEv(%"class.std::__2::vector.1"* %this1) #6
  %0 = load i32, i32* %__current_size.addr, align 4
  %add.ptr8 = getelementptr inbounds i8, i8* %call7, i32 %0
  call void @_ZNKSt3__26vectorIcNS_9allocatorIcEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector.1"* %this1, i8* %call, i8* %add.ptr, i8* %add.ptr6, i8* %add.ptr8) #6
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIcNS_9allocatorIcEEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector.1"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.1"*, align 4
  store %"class.std::__2::vector.1"* %this, %"class.std::__2::vector.1"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIcNS_9allocatorIcEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector.1"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.1"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector.1"* %this, %"class.std::__2::vector.1"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNKSt3__26vectorIcNS_9allocatorIcEEE4dataEv(%"class.std::__2::vector.1"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector.1"*, align 4
  store %"class.std::__2::vector.1"* %this, %"class.std::__2::vector.1"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector.1"*, %"class.std::__2::vector.1"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector.1"* %this1 to %"class.std::__2::__vector_base.2"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base.2", %"class.std::__2::__vector_base.2"* %0, i32 0, i32 0
  %1 = load i8*, i8** %__begin_, align 4
  %call = call i8* @_ZNSt3__212__to_addressIcEEPT_S2_(i8* %1) #6
  ret i8* %call
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNSt3__24moveIRPcEEONS_16remove_referenceIT_E4typeEOS4_(i8** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca i8**, align 4
  store i8** %__t, i8*** %__t.addr, align 4
  %0 = load i8**, i8*** %__t.addr, align 4
  ret i8** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE5clearEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 1
  %0 = load i8*, i8** %__begin_, align 4
  call void @_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE17__destruct_at_endEPc(%"struct.std::__2::__split_buffer"* %this1, i8* %0) #6
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE10deallocateERS2_Pcm(%"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %__a, i8* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.6"*, align 4
  %__p.addr = alloca i8*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.6"* %__a, %"class.std::__2::allocator.6"** %__a.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator.6"*, %"class.std::__2::allocator.6"** %__a.addr, align 4
  %1 = load i8*, i8** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorIcE10deallocateEPcm(%"class.std::__2::allocator.6"* %0, i8* %1, i32 %2) #6
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__214__split_bufferIcRNS_9allocatorIcEEE8capacityEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__214__split_bufferIcRNS_9allocatorIcEEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this1) #6
  %0 = load i8*, i8** %call, align 4
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 0
  %1 = load i8*, i8** %__first_, align 4
  %sub.ptr.lhs.cast = ptrtoint i8* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  ret i32 %sub.ptr.sub
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE17__destruct_at_endEPc(%"struct.std::__2::__split_buffer"* %this, i8* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__new_last.addr = alloca i8*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant.15", align 1
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store i8* %__new_last, i8** %__new_last.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %0 = load i8*, i8** %__new_last.addr, align 4
  call void @_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE17__destruct_at_endEPcNS_17integral_constantIbLb0EEE(%"struct.std::__2::__split_buffer"* %this1, i8* %0) #6
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE17__destruct_at_endEPcNS_17integral_constantIbLb0EEE(%"struct.std::__2::__split_buffer"* %this, i8* %__new_last) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant.15", align 1
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  %__new_last.addr = alloca i8*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  store i8* %__new_last, i8** %__new_last.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load i8*, i8** %__new_last.addr, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 2
  %2 = load i8*, i8** %__end_, align 4
  %cmp = icmp ne i8* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator.6"* @_ZNSt3__214__split_bufferIcRNS_9allocatorIcEEE7__allocEv(%"struct.std::__2::__split_buffer"* %this1) #6
  %__end_2 = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 2
  %3 = load i8*, i8** %__end_2, align 4
  %incdec.ptr = getelementptr inbounds i8, i8* %3, i32 -1
  store i8* %incdec.ptr, i8** %__end_2, align 4
  %call3 = call i8* @_ZNSt3__212__to_addressIcEEPT_S2_(i8* %incdec.ptr) #6
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE7destroyIcEEvRS2_PT_(%"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %call, i8* %call3)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE7destroyIcEEvRS2_PT_(%"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %__a, i8* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator.6"*, align 4
  %__p.addr = alloca i8*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy", align 1
  store %"class.std::__2::allocator.6"* %__a, %"class.std::__2::allocator.6"** %__a.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator.6"*, %"class.std::__2::allocator.6"** %__a.addr, align 4
  %2 = load i8*, i8** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE9__destroyIcEEvNS_17integral_constantIbLb1EEERS2_PT_(%"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %1, i8* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIcEEE9__destroyIcEEvNS_17integral_constantIbLb1EEERS2_PT_(%"class.std::__2::allocator.6"* nonnull align 1 dereferenceable(1) %__a, i8* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator.6"*, align 4
  %__p.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.6"* %__a, %"class.std::__2::allocator.6"** %__a.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator.6"*, %"class.std::__2::allocator.6"** %__a.addr, align 4
  %2 = load i8*, i8** %__p.addr, align 4
  call void @_ZNSt3__29allocatorIcE7destroyEPc(%"class.std::__2::allocator.6"* %1, i8* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIcE7destroyEPc(%"class.std::__2::allocator.6"* %this, i8* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.6"*, align 4
  %__p.addr = alloca i8*, align 4
  store %"class.std::__2::allocator.6"* %this, %"class.std::__2::allocator.6"** %this.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator.6"*, %"class.std::__2::allocator.6"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIcE10deallocateEPcm(%"class.std::__2::allocator.6"* %this, i8* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator.6"*, align 4
  %__p.addr = alloca i8*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator.6"* %this, %"class.std::__2::allocator.6"** %this.addr, align 4
  store i8* %__p, i8** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator.6"*, %"class.std::__2::allocator.6"** %this.addr, align 4
  %0 = load i8*, i8** %__p.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %1, 1
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %0, i32 %mul, i32 1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %__ptr, i32 %__size, i32 %__align) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  %1 = load i32, i32* %__size.addr, align 4
  %2 = load i32, i32* %__align.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm(i8* %0, i32 %1, i32 %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller33__do_deallocate_handle_size_alignEPvmm(i8* %__ptr, i32 %__size, i32 %__align) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  %__align.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  store i32 %__align, i32* %__align.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  %1 = load i32, i32* %__size.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm(i8* %0, i32 %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller27__do_deallocate_handle_sizeEPvm(i8* %__ptr, i32 %__size) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  %__size.addr = alloca i32, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  store i32 %__size, i32* %__size.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  call void @_ZNSt3__217_DeallocateCaller9__do_callEPv(i8* %0)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__217_DeallocateCaller9__do_callEPv(i8* %__ptr) #0 comdat {
entry:
  %__ptr.addr = alloca i8*, align 4
  store i8* %__ptr, i8** %__ptr.addr, align 4
  %0 = load i8*, i8** %__ptr.addr, align 4
  call void @_ZdlPv(i8* %0) #9
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #5

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__214__split_bufferIcRNS_9allocatorIcEEE9__end_capEv(%"struct.std::__2::__split_buffer"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer"*, align 4
  store %"struct.std::__2::__split_buffer"* %this, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer"*, %"struct.std::__2::__split_buffer"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer", %"struct.std::__2::__split_buffer"* %this1, i32 0, i32 3
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__217__compressed_pairIPcRNS_9allocatorIcEEE5firstEv(%"class.std::__2::__compressed_pair.12"* %__end_cap_) #6
  ret i8** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__217__compressed_pairIPcRNS_9allocatorIcEEE5firstEv(%"class.std::__2::__compressed_pair.12"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.12"*, align 4
  store %"class.std::__2::__compressed_pair.12"* %this, %"class.std::__2::__compressed_pair.12"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.12"*, %"class.std::__2::__compressed_pair.12"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.12"* %this1 to %"struct.std::__2::__compressed_pair_elem.4"*
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNKSt3__222__compressed_pair_elemIPcLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.4"* %0) #6
  ret i8** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__wrap_iter.11"* @_ZNSt3__211__wrap_iterIPcEC2ES1_(%"class.std::__2::__wrap_iter.11"* returned %this, i8* %__x) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__wrap_iter.11"*, align 4
  %__x.addr = alloca i8*, align 4
  store %"class.std::__2::__wrap_iter.11"* %this, %"class.std::__2::__wrap_iter.11"** %this.addr, align 4
  store i8* %__x, i8** %__x.addr, align 4
  %this1 = load %"class.std::__2::__wrap_iter.11"*, %"class.std::__2::__wrap_iter.11"** %this.addr, align 4
  %__i = getelementptr inbounds %"class.std::__2::__wrap_iter.11", %"class.std::__2::__wrap_iter.11"* %this1, i32 0, i32 0
  %0 = load i8*, i8** %__x.addr, align 4
  store i8* %0, i8** %__i, align 4
  ret %"class.std::__2::__wrap_iter.11"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE9__end_capEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEE5firstEv(%"class.std::__2::__compressed_pair"* %__end_cap_) #6
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIjNS_9allocatorIjEEE22__construct_one_at_endIJRKjEEEvDpOT_(%"class.std::__2::vector"* %this, i32* nonnull align 4 dereferenceable(4) %__args) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__args.addr = alloca i32*, align 4
  %__tx = alloca %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction", align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32* %__args, i32** %__args.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %call = call %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* @_ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionC2ERS3_m(%"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %__tx, %"class.std::__2::vector"* nonnull align 4 dereferenceable(12) %this1, i32 1)
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv(%"class.std::__2::__vector_base"* %0) #6
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction", %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %1 = load i32*, i32** %__pos_, align 4
  %call3 = call i32* @_ZNSt3__212__to_addressIjEEPT_S2_(i32* %1) #6
  %2 = load i32*, i32** %__args.addr, align 4
  %call4 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__27forwardIRKjEEOT_RNS_16remove_referenceIS3_E4typeE(i32* nonnull align 4 dereferenceable(4) %2) #6
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE9constructIjJRKjEEEvRS2_PT_DpOT0_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call2, i32* %call3, i32* nonnull align 4 dereferenceable(4) %call4)
  %__pos_5 = getelementptr inbounds %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction", %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %__tx, i32 0, i32 1
  %3 = load i32*, i32** %__pos_5, align 4
  %incdec.ptr = getelementptr inbounds i32, i32* %3, i32 1
  store i32* %incdec.ptr, i32** %__pos_5, align 4
  %call6 = call %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* @_ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %__tx) #6
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIjNS_9allocatorIjEEE21__push_back_slow_pathIRKjEEvOT_(%"class.std::__2::vector"* %this, i32* nonnull align 4 dereferenceable(4) %__x) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__x.addr = alloca i32*, align 4
  %__a = alloca %"class.std::__2::allocator"*, align 4
  %__v = alloca %"struct.std::__2::__split_buffer.17", align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32* %__x, i32** %__x.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv(%"class.std::__2::__vector_base"* %0) #6
  store %"class.std::__2::allocator"* %call, %"class.std::__2::allocator"** %__a, align 4
  %call2 = call i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4sizeEv(%"class.std::__2::vector"* %this1) #6
  %add = add i32 %call2, 1
  %call3 = call i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE11__recommendEm(%"class.std::__2::vector"* %this1, i32 %add)
  %call4 = call i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4sizeEv(%"class.std::__2::vector"* %this1) #6
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a, align 4
  %call5 = call %"struct.std::__2::__split_buffer.17"* @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEEC2EmmS3_(%"struct.std::__2::__split_buffer.17"* %__v, i32 %call3, i32 %call4, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %1)
  %2 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer.17", %"struct.std::__2::__split_buffer.17"* %__v, i32 0, i32 2
  %3 = load i32*, i32** %__end_, align 4
  %call6 = call i32* @_ZNSt3__212__to_addressIjEEPT_S2_(i32* %3) #6
  %4 = load i32*, i32** %__x.addr, align 4
  %call7 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__27forwardIRKjEEOT_RNS_16remove_referenceIS3_E4typeE(i32* nonnull align 4 dereferenceable(4) %4) #6
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE9constructIjJRKjEEEvRS2_PT_DpOT0_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %2, i32* %call6, i32* nonnull align 4 dereferenceable(4) %call7)
  %__end_8 = getelementptr inbounds %"struct.std::__2::__split_buffer.17", %"struct.std::__2::__split_buffer.17"* %__v, i32 0, i32 2
  %5 = load i32*, i32** %__end_8, align 4
  %incdec.ptr = getelementptr inbounds i32, i32* %5, i32 1
  store i32* %incdec.ptr, i32** %__end_8, align 4
  call void @_ZNSt3__26vectorIjNS_9allocatorIjEEE26__swap_out_circular_bufferERNS_14__split_bufferIjRS2_EE(%"class.std::__2::vector"* %this1, %"struct.std::__2::__split_buffer.17"* nonnull align 4 dereferenceable(20) %__v)
  %call9 = call %"struct.std::__2::__split_buffer.17"* @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEED2Ev(%"struct.std::__2::__split_buffer.17"* %__v) #6
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEE5firstEv(%"class.std::__2::__compressed_pair"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__222__compressed_pair_elemIPjLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %0) #6
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNSt3__222__compressed_pair_elemIPjLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem"*, align 4
  store %"struct.std::__2::__compressed_pair_elem"* %this, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem"*, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem"* %this1, i32 0, i32 0
  ret i32** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* @_ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionC2ERS3_m(%"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* returned %this, %"class.std::__2::vector"* nonnull align 4 dereferenceable(12) %__v, i32 %__n) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"*, align 4
  %__v.addr = alloca %"class.std::__2::vector"*, align 4
  %__n.addr = alloca i32, align 4
  store %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"** %this.addr, align 4
  store %"class.std::__2::vector"* %__v, %"class.std::__2::vector"** %__v.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"*, %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"** %this.addr, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction", %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__v.addr, align 4
  store %"class.std::__2::vector"* %0, %"class.std::__2::vector"** %__v_, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction", %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__v.addr, align 4
  %2 = bitcast %"class.std::__2::vector"* %1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %2, i32 0, i32 1
  %3 = load i32*, i32** %__end_, align 4
  store i32* %3, i32** %__pos_, align 4
  %__new_end_ = getelementptr inbounds %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction", %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %this1, i32 0, i32 2
  %4 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__v.addr, align 4
  %5 = bitcast %"class.std::__2::vector"* %4 to %"class.std::__2::__vector_base"*
  %__end_2 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %5, i32 0, i32 1
  %6 = load i32*, i32** %__end_2, align 4
  %7 = load i32, i32* %__n.addr, align 4
  %add.ptr = getelementptr inbounds i32, i32* %6, i32 %7
  store i32* %add.ptr, i32** %__new_end_, align 4
  ret %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE9constructIjJRKjEEEvRS2_PT_DpOT0_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, i32* %__p, i32* nonnull align 4 dereferenceable(4) %__args) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i32*, align 4
  %__args.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_construct.16", align 1
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  store i32* %__args, i32** %__args.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_construct.16"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %2 = load i32*, i32** %__p.addr, align 4
  %3 = load i32*, i32** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__27forwardIRKjEEOT_RNS_16remove_referenceIS3_E4typeE(i32* nonnull align 4 dereferenceable(4) %3) #6
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE11__constructIjJRKjEEEvNS_17integral_constantIbLb1EEERS2_PT_DpOT0_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %1, i32* %2, i32* nonnull align 4 dereferenceable(4) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEE6secondEv(%"class.std::__2::__compressed_pair"* %__end_cap_) #6
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNSt3__212__to_addressIjEEPT_S2_(i32* %__p) #0 comdat {
entry:
  %__p.addr = alloca i32*, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %0 = load i32*, i32** %__p.addr, align 4
  ret i32* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNSt3__27forwardIRKjEEOT_RNS_16remove_referenceIS3_E4typeE(i32* nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca i32*, align 4
  store i32* %__t, i32** %__t.addr, align 4
  %0 = load i32*, i32** %__t.addr, align 4
  ret i32* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* @_ZNSt3__26vectorIjNS_9allocatorIjEEE21_ConstructTransactionD2Ev(%"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"*, align 4
  store %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %this, %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"** %this.addr, align 4
  %this1 = load %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"*, %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"** %this.addr, align 4
  %__pos_ = getelementptr inbounds %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction", %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %this1, i32 0, i32 1
  %0 = load i32*, i32** %__pos_, align 4
  %__v_ = getelementptr inbounds %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction", %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %this1, i32 0, i32 0
  %1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %__v_, align 4
  %2 = bitcast %"class.std::__2::vector"* %1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %2, i32 0, i32 1
  store i32* %0, i32** %__end_, align 4
  ret %"struct.std::__2::vector<unsigned int, std::__2::allocator<unsigned int>>::_ConstructTransaction"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE11__constructIjJRKjEEEvNS_17integral_constantIbLb1EEERS2_PT_DpOT0_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, i32* %__p, i32* nonnull align 4 dereferenceable(4) %__args) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i32*, align 4
  %__args.addr = alloca i32*, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  store i32* %__args, i32** %__args.addr, align 4
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %2 = load i32*, i32** %__p.addr, align 4
  %3 = load i32*, i32** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__27forwardIRKjEEOT_RNS_16remove_referenceIS3_E4typeE(i32* nonnull align 4 dereferenceable(4) %3) #6
  call void @_ZNSt3__29allocatorIjE9constructIjJRKjEEEvPT_DpOT0_(%"class.std::__2::allocator"* %1, i32* %2, i32* nonnull align 4 dereferenceable(4) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIjE9constructIjJRKjEEEvPT_DpOT0_(%"class.std::__2::allocator"* %this, i32* %__p, i32* nonnull align 4 dereferenceable(4) %__args) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i32*, align 4
  %__args.addr = alloca i32*, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  store i32* %__args, i32** %__args.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  %0 = load i32*, i32** %__p.addr, align 4
  %1 = bitcast i32* %0 to i8*
  %2 = bitcast i8* %1 to i32*
  %3 = load i32*, i32** %__args.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__27forwardIRKjEEOT_RNS_16remove_referenceIS3_E4typeE(i32* nonnull align 4 dereferenceable(4) %3) #6
  %4 = load i32, i32* %call, align 4
  store i32 %4, i32* %2, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEE6secondEv(%"class.std::__2::__compressed_pair"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem.0"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.0"* %0) #6
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.0"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.0"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.0"* %this, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.0"*, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.0"* %this1 to %"class.std::__2::allocator"*
  ret %"class.std::__2::allocator"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE11__recommendEm(%"class.std::__2::vector"* %this, i32 %__new_size) #0 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__new_size.addr = alloca i32, align 4
  %__ms = alloca i32, align 4
  %__cap = alloca i32, align 4
  %ref.tmp = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__new_size, i32* %__new_size.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %call = call i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE8max_sizeEv(%"class.std::__2::vector"* %this1) #6
  store i32 %call, i32* %__ms, align 4
  %0 = load i32, i32* %__new_size.addr, align 4
  %1 = load i32, i32* %__ms, align 4
  %cmp = icmp ugt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base_common"*
  call void @_ZNKSt3__220__vector_base_commonILb1EE20__throw_length_errorEv(%"class.std::__2::__vector_base_common"* %2) #7
  unreachable

if.end:                                           ; preds = %entry
  %call2 = call i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE8capacityEv(%"class.std::__2::vector"* %this1) #6
  store i32 %call2, i32* %__cap, align 4
  %3 = load i32, i32* %__cap, align 4
  %4 = load i32, i32* %__ms, align 4
  %div = udiv i32 %4, 2
  %cmp3 = icmp uge i32 %3, %div
  br i1 %cmp3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %if.end
  %5 = load i32, i32* %__ms, align 4
  store i32 %5, i32* %retval, align 4
  br label %return

if.end5:                                          ; preds = %if.end
  %6 = load i32, i32* %__cap, align 4
  %mul = mul i32 2, %6
  store i32 %mul, i32* %ref.tmp, align 4
  %call6 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23maxImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %__new_size.addr)
  %7 = load i32, i32* %call6, align 4
  store i32 %7, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end5, %if.then4
  %8 = load i32, i32* %retval, align 4
  ret i32 %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer.17"* @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEEC2EmmS3_(%"struct.std::__2::__split_buffer.17"* returned %this, i32 %__cap, i32 %__start, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"struct.std::__2::__split_buffer.17"*, align 4
  %this.addr = alloca %"struct.std::__2::__split_buffer.17"*, align 4
  %__cap.addr = alloca i32, align 4
  %__start.addr = alloca i32, align 4
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %ref.tmp = alloca i8*, align 4
  store %"struct.std::__2::__split_buffer.17"* %this, %"struct.std::__2::__split_buffer.17"** %this.addr, align 4
  store i32 %__cap, i32* %__cap.addr, align 4
  store i32 %__start, i32* %__start.addr, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.17"*, %"struct.std::__2::__split_buffer.17"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer.17"* %this1, %"struct.std::__2::__split_buffer.17"** %retval, align 4
  %0 = bitcast %"struct.std::__2::__split_buffer.17"* %this1 to %"class.std::__2::__split_buffer_common"*
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer.17", %"struct.std::__2::__split_buffer.17"* %this1, i32 0, i32 3
  store i8* null, i8** %ref.tmp, align 4
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %call = call %"class.std::__2::__compressed_pair.18"* @_ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEEC2IDnS4_EEOT_OT0_(%"class.std::__2::__compressed_pair.18"* %__end_cap_, i8** nonnull align 4 dereferenceable(4) %ref.tmp, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %1)
  %2 = load i32, i32* %__cap.addr, align 4
  %cmp = icmp ne i32 %2, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %call2 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE7__allocEv(%"struct.std::__2::__split_buffer.17"* %this1) #6
  %3 = load i32, i32* %__cap.addr, align 4
  %call3 = call i32* @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE8allocateERS2_m(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call2, i32 %3)
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32* [ %call3, %cond.true ], [ null, %cond.false ]
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer.17", %"struct.std::__2::__split_buffer.17"* %this1, i32 0, i32 0
  store i32* %cond, i32** %__first_, align 4
  %__first_4 = getelementptr inbounds %"struct.std::__2::__split_buffer.17", %"struct.std::__2::__split_buffer.17"* %this1, i32 0, i32 0
  %4 = load i32*, i32** %__first_4, align 4
  %5 = load i32, i32* %__start.addr, align 4
  %add.ptr = getelementptr inbounds i32, i32* %4, i32 %5
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer.17", %"struct.std::__2::__split_buffer.17"* %this1, i32 0, i32 2
  store i32* %add.ptr, i32** %__end_, align 4
  %__begin_ = getelementptr inbounds %"struct.std::__2::__split_buffer.17", %"struct.std::__2::__split_buffer.17"* %this1, i32 0, i32 1
  store i32* %add.ptr, i32** %__begin_, align 4
  %__first_5 = getelementptr inbounds %"struct.std::__2::__split_buffer.17", %"struct.std::__2::__split_buffer.17"* %this1, i32 0, i32 0
  %6 = load i32*, i32** %__first_5, align 4
  %7 = load i32, i32* %__cap.addr, align 4
  %add.ptr6 = getelementptr inbounds i32, i32* %6, i32 %7
  %call7 = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE9__end_capEv(%"struct.std::__2::__split_buffer.17"* %this1) #6
  store i32* %add.ptr6, i32** %call7, align 4
  %8 = load %"struct.std::__2::__split_buffer.17"*, %"struct.std::__2::__split_buffer.17"** %retval, align 4
  ret %"struct.std::__2::__split_buffer.17"* %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIjNS_9allocatorIjEEE26__swap_out_circular_bufferERNS_14__split_bufferIjRS2_EE(%"class.std::__2::vector"* %this, %"struct.std::__2::__split_buffer.17"* nonnull align 4 dereferenceable(20) %__v) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__v.addr = alloca %"struct.std::__2::__split_buffer.17"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer.17"* %__v, %"struct.std::__2::__split_buffer.17"** %__v.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  call void @_ZNKSt3__26vectorIjNS_9allocatorIjEEE17__annotate_deleteEv(%"class.std::__2::vector"* %this1) #6
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv(%"class.std::__2::__vector_base"* %0) #6
  %1 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %1, i32 0, i32 0
  %2 = load i32*, i32** %__begin_, align 4
  %3 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %3, i32 0, i32 1
  %4 = load i32*, i32** %__end_, align 4
  %5 = load %"struct.std::__2::__split_buffer.17"*, %"struct.std::__2::__split_buffer.17"** %__v.addr, align 4
  %__begin_2 = getelementptr inbounds %"struct.std::__2::__split_buffer.17", %"struct.std::__2::__split_buffer.17"* %5, i32 0, i32 1
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE46__construct_backward_with_exception_guaranteesIjEENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS2_PT_S8_EE5valuesr31is_trivially_move_constructibleIS8_EE5valueEvE4typeERS2_S9_S9_RS9_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call, i32* %2, i32* %4, i32** nonnull align 4 dereferenceable(4) %__begin_2)
  %6 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_3 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %6, i32 0, i32 0
  %7 = load %"struct.std::__2::__split_buffer.17"*, %"struct.std::__2::__split_buffer.17"** %__v.addr, align 4
  %__begin_4 = getelementptr inbounds %"struct.std::__2::__split_buffer.17", %"struct.std::__2::__split_buffer.17"* %7, i32 0, i32 1
  call void @_ZNSt3__24swapIPjEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_(i32** nonnull align 4 dereferenceable(4) %__begin_3, i32** nonnull align 4 dereferenceable(4) %__begin_4) #6
  %8 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__end_5 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %8, i32 0, i32 1
  %9 = load %"struct.std::__2::__split_buffer.17"*, %"struct.std::__2::__split_buffer.17"** %__v.addr, align 4
  %__end_6 = getelementptr inbounds %"struct.std::__2::__split_buffer.17", %"struct.std::__2::__split_buffer.17"* %9, i32 0, i32 2
  call void @_ZNSt3__24swapIPjEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_(i32** nonnull align 4 dereferenceable(4) %__end_5, i32** nonnull align 4 dereferenceable(4) %__end_6) #6
  %10 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call7 = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE9__end_capEv(%"class.std::__2::__vector_base"* %10) #6
  %11 = load %"struct.std::__2::__split_buffer.17"*, %"struct.std::__2::__split_buffer.17"** %__v.addr, align 4
  %call8 = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE9__end_capEv(%"struct.std::__2::__split_buffer.17"* %11) #6
  call void @_ZNSt3__24swapIPjEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_(i32** nonnull align 4 dereferenceable(4) %call7, i32** nonnull align 4 dereferenceable(4) %call8) #6
  %12 = load %"struct.std::__2::__split_buffer.17"*, %"struct.std::__2::__split_buffer.17"** %__v.addr, align 4
  %__begin_9 = getelementptr inbounds %"struct.std::__2::__split_buffer.17", %"struct.std::__2::__split_buffer.17"* %12, i32 0, i32 1
  %13 = load i32*, i32** %__begin_9, align 4
  %14 = load %"struct.std::__2::__split_buffer.17"*, %"struct.std::__2::__split_buffer.17"** %__v.addr, align 4
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer.17", %"struct.std::__2::__split_buffer.17"* %14, i32 0, i32 0
  store i32* %13, i32** %__first_, align 4
  %call10 = call i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4sizeEv(%"class.std::__2::vector"* %this1) #6
  call void @_ZNKSt3__26vectorIjNS_9allocatorIjEEE14__annotate_newEm(%"class.std::__2::vector"* %this1, i32 %call10) #6
  call void @_ZNSt3__26vectorIjNS_9allocatorIjEEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector"* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__split_buffer.17"* @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEED2Ev(%"struct.std::__2::__split_buffer.17"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"struct.std::__2::__split_buffer.17"*, align 4
  %this.addr = alloca %"struct.std::__2::__split_buffer.17"*, align 4
  store %"struct.std::__2::__split_buffer.17"* %this, %"struct.std::__2::__split_buffer.17"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.17"*, %"struct.std::__2::__split_buffer.17"** %this.addr, align 4
  store %"struct.std::__2::__split_buffer.17"* %this1, %"struct.std::__2::__split_buffer.17"** %retval, align 4
  call void @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE5clearEv(%"struct.std::__2::__split_buffer.17"* %this1) #6
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer.17", %"struct.std::__2::__split_buffer.17"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__first_, align 4
  %tobool = icmp ne i32* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE7__allocEv(%"struct.std::__2::__split_buffer.17"* %this1) #6
  %__first_2 = getelementptr inbounds %"struct.std::__2::__split_buffer.17", %"struct.std::__2::__split_buffer.17"* %this1, i32 0, i32 0
  %1 = load i32*, i32** %__first_2, align 4
  %call3 = call i32 @_ZNKSt3__214__split_bufferIjRNS_9allocatorIjEEE8capacityEv(%"struct.std::__2::__split_buffer.17"* %this1)
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE10deallocateERS2_Pjm(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call, i32* %1, i32 %call3) #6
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"struct.std::__2::__split_buffer.17"*, %"struct.std::__2::__split_buffer.17"** %retval, align 4
  ret %"struct.std::__2::__split_buffer.17"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE8max_sizeEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp3 = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv(%"class.std::__2::__vector_base"* %0) #6
  %call2 = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE8max_sizeERKS2_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call) #6
  store i32 %call2, i32* %ref.tmp, align 4
  %call4 = call i32 @_ZNSt3__214numeric_limitsIlE3maxEv() #6
  store i32 %call4, i32* %ref.tmp3, align 4
  %call5 = call nonnull align 4 dereferenceable(4) i32* @_ZNSt3__23minImEERKT_S3_S3_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %1 = load i32, i32* %call5, align 4
  ret i32 %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE8capacityEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %call = call i32 @_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE8capacityEv(%"class.std::__2::__vector_base"* %0) #6
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE8max_sizeERKS2_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_max_size.20", align 1
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_max_size.20"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %call = call i32 @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE10__max_sizeENS_17integral_constantIbLb1EEERKS2_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %1) #6
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNKSt3__217__compressed_pairIPjNS_9allocatorIjEEE6secondEv(%"class.std::__2::__compressed_pair"* %__end_cap_) #6
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE10__max_sizeENS_17integral_constantIbLb1EEERKS2_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorIjE8max_sizeEv(%"class.std::__2::allocator"* %1) #6
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__29allocatorIjE8max_sizeEv(%"class.std::__2::allocator"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  ret i32 1073741823
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNKSt3__217__compressed_pairIPjNS_9allocatorIjEEE6secondEv(%"class.std::__2::__compressed_pair"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem.0"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.0"* %0) #6
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNKSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EE5__getEv(%"struct.std::__2::__compressed_pair_elem.0"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.0"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.0"* %this, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.0"*, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %0 = bitcast %"struct.std::__2::__compressed_pair_elem.0"* %this1 to %"class.std::__2::allocator"*
  ret %"class.std::__2::allocator"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE8capacityEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE9__end_capEv(%"class.std::__2::__vector_base"* %this1) #6
  %0 = load i32*, i32** %call, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  %1 = load i32*, i32** %__begin_, align 4
  %sub.ptr.lhs.cast = ptrtoint i32* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint i32* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE9__end_capEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__217__compressed_pairIPjNS_9allocatorIjEEE5firstEv(%"class.std::__2::__compressed_pair"* %__end_cap_) #6
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__217__compressed_pairIPjNS_9allocatorIjEEE5firstEv(%"class.std::__2::__compressed_pair"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__222__compressed_pair_elemIPjLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %0) #6
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__222__compressed_pair_elemIPjLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem"*, align 4
  store %"struct.std::__2::__compressed_pair_elem"* %this, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem"*, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem"* %this1, i32 0, i32 0
  ret i32** %__value_
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair.18"* @_ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEEC2IDnS4_EEOT_OT0_(%"class.std::__2::__compressed_pair.18"* returned %this, i8** nonnull align 4 dereferenceable(4) %__t1, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.18"*, align 4
  %__t1.addr = alloca i8**, align 4
  %__t2.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"class.std::__2::__compressed_pair.18"* %this, %"class.std::__2::__compressed_pair.18"** %this.addr, align 4
  store i8** %__t1, i8*** %__t1.addr, align 4
  store %"class.std::__2::allocator"* %__t2, %"class.std::__2::allocator"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.18"*, %"class.std::__2::__compressed_pair.18"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.18"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %1 = load i8**, i8*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %1) #6
  %call2 = call %"struct.std::__2::__compressed_pair_elem"* @_ZNSt3__222__compressed_pair_elemIPjLi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem"* %0, i8** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair.18"* %this1 to i8*
  %3 = getelementptr inbounds i8, i8* %2, i32 4
  %4 = bitcast i8* %3 to %"struct.std::__2::__compressed_pair_elem.19"*
  %5 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__27forwardIRNS_9allocatorIjEEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %5) #6
  %call4 = call %"struct.std::__2::__compressed_pair_elem.19"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIjEELi1ELb0EEC2IS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.19"* %4, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call3)
  ret %"class.std::__2::__compressed_pair.18"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE8allocateERS2_m(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call i32* @_ZNSt3__29allocatorIjE8allocateEmPKv(%"class.std::__2::allocator"* %0, i32 %1, i8* null)
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE7__allocEv(%"struct.std::__2::__split_buffer.17"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.17"*, align 4
  store %"struct.std::__2::__split_buffer.17"* %this, %"struct.std::__2::__split_buffer.17"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.17"*, %"struct.std::__2::__split_buffer.17"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer.17", %"struct.std::__2::__split_buffer.17"* %this1, i32 0, i32 3
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEE6secondEv(%"class.std::__2::__compressed_pair.18"* %__end_cap_) #6
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE9__end_capEv(%"struct.std::__2::__split_buffer.17"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.17"*, align 4
  store %"struct.std::__2::__split_buffer.17"* %this, %"struct.std::__2::__split_buffer.17"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.17"*, %"struct.std::__2::__split_buffer.17"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer.17", %"struct.std::__2::__split_buffer.17"* %this1, i32 0, i32 3
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEE5firstEv(%"class.std::__2::__compressed_pair.18"* %__end_cap_) #6
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem"* @_ZNSt3__222__compressed_pair_elemIPjLi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem"* returned %this, i8** nonnull align 4 dereferenceable(4) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem"*, align 4
  %__u.addr = alloca i8**, align 4
  store %"struct.std::__2::__compressed_pair_elem"* %this, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  store i8** %__u, i8*** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem"*, %"struct.std::__2::__compressed_pair_elem"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem", %"struct.std::__2::__compressed_pair_elem"* %this1, i32 0, i32 0
  %0 = load i8**, i8*** %__u.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %0) #6
  store i32* null, i32** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__27forwardIRNS_9allocatorIjEEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"class.std::__2::allocator"* %__t, %"class.std::__2::allocator"** %__t.addr, align 4
  %0 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__t.addr, align 4
  ret %"class.std::__2::allocator"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.19"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIjEELi1ELb0EEC2IS3_vEEOT_(%"struct.std::__2::__compressed_pair_elem.19"* returned %this, %"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__u) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.19"*, align 4
  %__u.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.19"* %this, %"struct.std::__2::__compressed_pair_elem.19"** %this.addr, align 4
  store %"class.std::__2::allocator"* %__u, %"class.std::__2::allocator"** %__u.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.19"*, %"struct.std::__2::__compressed_pair_elem.19"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.19", %"struct.std::__2::__compressed_pair_elem.19"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__u.addr, align 4
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__27forwardIRNS_9allocatorIjEEEEOT_RNS_16remove_referenceIS4_E4typeE(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %0) #6
  store %"class.std::__2::allocator"* %call, %"class.std::__2::allocator"** %__value_, align 4
  ret %"struct.std::__2::__compressed_pair_elem.19"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNSt3__29allocatorIjE8allocateEmPKv(%"class.std::__2::allocator"* %this, i32 %__n, i8* %0) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  %__n.addr = alloca i32, align 4
  %.addr = alloca i8*, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  store i8* %0, i8** %.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  %1 = load i32, i32* %__n.addr, align 4
  %call = call i32 @_ZNKSt3__29allocatorIjE8max_sizeEv(%"class.std::__2::allocator"* %this1) #6
  %cmp = icmp ugt i32 %1, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__220__throw_length_errorEPKc(i8* getelementptr inbounds ([68 x i8], [68 x i8]* @.str, i32 0, i32 0)) #7
  unreachable

if.end:                                           ; preds = %entry
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  %call2 = call i8* @_ZNSt3__217__libcpp_allocateEmm(i32 %mul, i32 4)
  %3 = bitcast i8* %call2 to i32*
  ret i32* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEE6secondEv(%"class.std::__2::__compressed_pair.18"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.18"*, align 4
  store %"class.std::__2::__compressed_pair.18"* %this, %"class.std::__2::__compressed_pair.18"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.18"*, %"class.std::__2::__compressed_pair.18"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.18"* %this1 to i8*
  %add.ptr = getelementptr inbounds i8, i8* %0, i32 4
  %1 = bitcast i8* %add.ptr to %"struct.std::__2::__compressed_pair_elem.19"*
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIjEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.19"* %1) #6
  ret %"class.std::__2::allocator"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__222__compressed_pair_elemIRNS_9allocatorIjEELi1ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem.19"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.19"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.19"* %this, %"struct.std::__2::__compressed_pair_elem.19"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.19"*, %"struct.std::__2::__compressed_pair_elem.19"** %this.addr, align 4
  %__value_ = getelementptr inbounds %"struct.std::__2::__compressed_pair_elem.19", %"struct.std::__2::__compressed_pair_elem.19"* %this1, i32 0, i32 0
  %0 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__value_, align 4
  ret %"class.std::__2::allocator"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNSt3__217__compressed_pairIPjRNS_9allocatorIjEEE5firstEv(%"class.std::__2::__compressed_pair.18"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.18"*, align 4
  store %"class.std::__2::__compressed_pair.18"* %this, %"class.std::__2::__compressed_pair.18"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.18"*, %"class.std::__2::__compressed_pair.18"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.18"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__222__compressed_pair_elemIPjLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %0) #6
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIjNS_9allocatorIjEEE17__annotate_deleteEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %call = call i32* @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv(%"class.std::__2::vector"* %this1) #6
  %0 = bitcast i32* %call to i8*
  %call2 = call i32* @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv(%"class.std::__2::vector"* %this1) #6
  %call3 = call i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE8capacityEv(%"class.std::__2::vector"* %this1) #6
  %add.ptr = getelementptr inbounds i32, i32* %call2, i32 %call3
  %1 = bitcast i32* %add.ptr to i8*
  %call4 = call i32* @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv(%"class.std::__2::vector"* %this1) #6
  %call5 = call i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4sizeEv(%"class.std::__2::vector"* %this1) #6
  %add.ptr6 = getelementptr inbounds i32, i32* %call4, i32 %call5
  %2 = bitcast i32* %add.ptr6 to i8*
  %call7 = call i32* @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv(%"class.std::__2::vector"* %this1) #6
  %call8 = call i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE8capacityEv(%"class.std::__2::vector"* %this1) #6
  %add.ptr9 = getelementptr inbounds i32, i32* %call7, i32 %call8
  %3 = bitcast i32* %add.ptr9 to i8*
  call void @_ZNKSt3__26vectorIjNS_9allocatorIjEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector"* %this1, i8* %0, i8* %1, i8* %2, i8* %3) #6
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE46__construct_backward_with_exception_guaranteesIjEENS_9enable_ifIXaaooL_ZNS_17integral_constantIbLb1EE5valueEEntsr15__has_constructIS2_PT_S8_EE5valuesr31is_trivially_move_constructibleIS8_EE5valueEvE4typeERS2_S9_S9_RS9_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %0, i32* %__begin1, i32* %__end1, i32** nonnull align 4 dereferenceable(4) %__end2) #0 comdat {
entry:
  %.addr = alloca %"class.std::__2::allocator"*, align 4
  %__begin1.addr = alloca i32*, align 4
  %__end1.addr = alloca i32*, align 4
  %__end2.addr = alloca i32**, align 4
  %_Np = alloca i32, align 4
  store %"class.std::__2::allocator"* %0, %"class.std::__2::allocator"** %.addr, align 4
  store i32* %__begin1, i32** %__begin1.addr, align 4
  store i32* %__end1, i32** %__end1.addr, align 4
  store i32** %__end2, i32*** %__end2.addr, align 4
  %1 = load i32*, i32** %__end1.addr, align 4
  %2 = load i32*, i32** %__begin1.addr, align 4
  %sub.ptr.lhs.cast = ptrtoint i32* %1 to i32
  %sub.ptr.rhs.cast = ptrtoint i32* %2 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  store i32 %sub.ptr.div, i32* %_Np, align 4
  %3 = load i32, i32* %_Np, align 4
  %4 = load i32**, i32*** %__end2.addr, align 4
  %5 = load i32*, i32** %4, align 4
  %idx.neg = sub i32 0, %3
  %add.ptr = getelementptr inbounds i32, i32* %5, i32 %idx.neg
  store i32* %add.ptr, i32** %4, align 4
  %6 = load i32, i32* %_Np, align 4
  %cmp = icmp sgt i32 %6, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %7 = load i32**, i32*** %__end2.addr, align 4
  %8 = load i32*, i32** %7, align 4
  %9 = bitcast i32* %8 to i8*
  %10 = load i32*, i32** %__begin1.addr, align 4
  %11 = bitcast i32* %10 to i8*
  %12 = load i32, i32* %_Np, align 4
  %mul = mul i32 %12, 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %11, i32 %mul, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__24swapIPjEENS_9enable_ifIXaasr21is_move_constructibleIT_EE5valuesr18is_move_assignableIS3_EE5valueEvE4typeERS3_S6_(i32** nonnull align 4 dereferenceable(4) %__x, i32** nonnull align 4 dereferenceable(4) %__y) #0 comdat {
entry:
  %__x.addr = alloca i32**, align 4
  %__y.addr = alloca i32**, align 4
  %__t = alloca i32*, align 4
  store i32** %__x, i32*** %__x.addr, align 4
  store i32** %__y, i32*** %__y.addr, align 4
  %0 = load i32**, i32*** %__x.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__24moveIRPjEEONS_16remove_referenceIT_E4typeEOS4_(i32** nonnull align 4 dereferenceable(4) %0) #6
  %1 = load i32*, i32** %call, align 4
  store i32* %1, i32** %__t, align 4
  %2 = load i32**, i32*** %__y.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__24moveIRPjEEONS_16remove_referenceIT_E4typeEOS4_(i32** nonnull align 4 dereferenceable(4) %2) #6
  %3 = load i32*, i32** %call1, align 4
  %4 = load i32**, i32*** %__x.addr, align 4
  store i32* %3, i32** %4, align 4
  %call2 = call nonnull align 4 dereferenceable(4) i32** @_ZNSt3__24moveIRPjEEONS_16remove_referenceIT_E4typeEOS4_(i32** nonnull align 4 dereferenceable(4) %__t) #6
  %5 = load i32*, i32** %call2, align 4
  %6 = load i32**, i32*** %__y.addr, align 4
  store i32* %5, i32** %6, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIjNS_9allocatorIjEEE14__annotate_newEm(%"class.std::__2::vector"* %this, i32 %__current_size) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__current_size.addr = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__current_size, i32* %__current_size.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %call = call i32* @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv(%"class.std::__2::vector"* %this1) #6
  %0 = bitcast i32* %call to i8*
  %call2 = call i32* @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv(%"class.std::__2::vector"* %this1) #6
  %call3 = call i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE8capacityEv(%"class.std::__2::vector"* %this1) #6
  %add.ptr = getelementptr inbounds i32, i32* %call2, i32 %call3
  %1 = bitcast i32* %add.ptr to i8*
  %call4 = call i32* @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv(%"class.std::__2::vector"* %this1) #6
  %call5 = call i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE8capacityEv(%"class.std::__2::vector"* %this1) #6
  %add.ptr6 = getelementptr inbounds i32, i32* %call4, i32 %call5
  %2 = bitcast i32* %add.ptr6 to i8*
  %call7 = call i32* @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv(%"class.std::__2::vector"* %this1) #6
  %3 = load i32, i32* %__current_size.addr, align 4
  %add.ptr8 = getelementptr inbounds i32, i32* %call7, i32 %3
  %4 = bitcast i32* %add.ptr8 to i8*
  call void @_ZNKSt3__26vectorIjNS_9allocatorIjEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector"* %this1, i8* %0, i8* %1, i8* %2, i8* %4) #6
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__26vectorIjNS_9allocatorIjEEE26__invalidate_all_iteratorsEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIjNS_9allocatorIjEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector"* %this, i8* %0, i8* %1, i8* %2, i8* %3) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %.addr = alloca i8*, align 4
  %.addr1 = alloca i8*, align 4
  %.addr2 = alloca i8*, align 4
  %.addr3 = alloca i8*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i8* %0, i8** %.addr, align 4
  store i8* %1, i8** %.addr1, align 4
  store i8* %2, i8** %.addr2, align 4
  store i8* %3, i8** %.addr3, align 4
  %this4 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32* @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv(%"class.std::__2::vector"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::vector"* %this1 to %"class.std::__2::__vector_base"*
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %0, i32 0, i32 0
  %1 = load i32*, i32** %__begin_, align 4
  %call = call i32* @_ZNSt3__212__to_addressIjEEPT_S2_(i32* %1) #6
  ret i32* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNSt3__24moveIRPjEEONS_16remove_referenceIT_E4typeEOS4_(i32** nonnull align 4 dereferenceable(4) %__t) #0 comdat {
entry:
  %__t.addr = alloca i32**, align 4
  store i32** %__t, i32*** %__t.addr, align 4
  %0 = load i32**, i32*** %__t.addr, align 4
  ret i32** %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE5clearEv(%"struct.std::__2::__split_buffer.17"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.17"*, align 4
  store %"struct.std::__2::__split_buffer.17"* %this, %"struct.std::__2::__split_buffer.17"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.17"*, %"struct.std::__2::__split_buffer.17"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"struct.std::__2::__split_buffer.17", %"struct.std::__2::__split_buffer.17"* %this1, i32 0, i32 1
  %0 = load i32*, i32** %__begin_, align 4
  call void @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE17__destruct_at_endEPj(%"struct.std::__2::__split_buffer.17"* %this1, i32* %0) #6
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE10deallocateERS2_Pjm(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, i32* %__p, i32 %__n) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i32*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %0 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %1 = load i32*, i32** %__p.addr, align 4
  %2 = load i32, i32* %__n.addr, align 4
  call void @_ZNSt3__29allocatorIjE10deallocateEPjm(%"class.std::__2::allocator"* %0, i32* %1, i32 %2) #6
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNKSt3__214__split_bufferIjRNS_9allocatorIjEEE8capacityEv(%"struct.std::__2::__split_buffer.17"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.17"*, align 4
  store %"struct.std::__2::__split_buffer.17"* %this, %"struct.std::__2::__split_buffer.17"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.17"*, %"struct.std::__2::__split_buffer.17"** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__214__split_bufferIjRNS_9allocatorIjEEE9__end_capEv(%"struct.std::__2::__split_buffer.17"* %this1) #6
  %0 = load i32*, i32** %call, align 4
  %__first_ = getelementptr inbounds %"struct.std::__2::__split_buffer.17", %"struct.std::__2::__split_buffer.17"* %this1, i32 0, i32 0
  %1 = load i32*, i32** %__first_, align 4
  %sub.ptr.lhs.cast = ptrtoint i32* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint i32* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 4
  ret i32 %sub.ptr.div
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE17__destruct_at_endEPj(%"struct.std::__2::__split_buffer.17"* %this, i32* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.17"*, align 4
  %__new_last.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant.15", align 1
  store %"struct.std::__2::__split_buffer.17"* %this, %"struct.std::__2::__split_buffer.17"** %this.addr, align 4
  store i32* %__new_last, i32** %__new_last.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.17"*, %"struct.std::__2::__split_buffer.17"** %this.addr, align 4
  %0 = load i32*, i32** %__new_last.addr, align 4
  call void @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE17__destruct_at_endEPjNS_17integral_constantIbLb0EEE(%"struct.std::__2::__split_buffer.17"* %this1, i32* %0) #6
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE17__destruct_at_endEPjNS_17integral_constantIbLb0EEE(%"struct.std::__2::__split_buffer.17"* %this, i32* %__new_last) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant.15", align 1
  %this.addr = alloca %"struct.std::__2::__split_buffer.17"*, align 4
  %__new_last.addr = alloca i32*, align 4
  store %"struct.std::__2::__split_buffer.17"* %this, %"struct.std::__2::__split_buffer.17"** %this.addr, align 4
  store i32* %__new_last, i32** %__new_last.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.17"*, %"struct.std::__2::__split_buffer.17"** %this.addr, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load i32*, i32** %__new_last.addr, align 4
  %__end_ = getelementptr inbounds %"struct.std::__2::__split_buffer.17", %"struct.std::__2::__split_buffer.17"* %this1, i32 0, i32 2
  %2 = load i32*, i32** %__end_, align 4
  %cmp = icmp ne i32* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__214__split_bufferIjRNS_9allocatorIjEEE7__allocEv(%"struct.std::__2::__split_buffer.17"* %this1) #6
  %__end_2 = getelementptr inbounds %"struct.std::__2::__split_buffer.17", %"struct.std::__2::__split_buffer.17"* %this1, i32 0, i32 2
  %3 = load i32*, i32** %__end_2, align 4
  %incdec.ptr = getelementptr inbounds i32, i32* %3, i32 -1
  store i32* %incdec.ptr, i32** %__end_2, align 4
  %call3 = call i32* @_ZNSt3__212__to_addressIjEEPT_S2_(i32* %incdec.ptr) #6
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE7destroyIjEEvRS2_PT_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call, i32* %call3)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE7destroyIjEEvRS2_PT_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, i32* %__p) #0 comdat {
entry:
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i32*, align 4
  %agg.tmp = alloca %"struct.std::__2::integral_constant", align 1
  %ref.tmp = alloca %"struct.std::__2::__has_destroy.21", align 1
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %0 = bitcast %"struct.std::__2::__has_destroy.21"* %ref.tmp to %"struct.std::__2::integral_constant"*
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %2 = load i32*, i32** %__p.addr, align 4
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE9__destroyIjEEvNS_17integral_constantIbLb1EEERS2_PT_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %1, i32* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE9__destroyIjEEvNS_17integral_constantIbLb1EEERS2_PT_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %__a, i32* %__p) #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::integral_constant", align 1
  %__a.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i32*, align 4
  store %"class.std::__2::allocator"* %__a, %"class.std::__2::allocator"** %__a.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %__a.addr, align 4
  %2 = load i32*, i32** %__p.addr, align 4
  call void @_ZNSt3__29allocatorIjE7destroyEPj(%"class.std::__2::allocator"* %1, i32* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIjE7destroyEPj(%"class.std::__2::allocator"* %this, i32* %__p) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i32*, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__29allocatorIjE10deallocateEPjm(%"class.std::__2::allocator"* %this, i32* %__p, i32 %__n) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  %__p.addr = alloca i32*, align 4
  %__n.addr = alloca i32, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  store i32* %__p, i32** %__p.addr, align 4
  store i32 %__n, i32* %__n.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  %0 = load i32*, i32** %__p.addr, align 4
  %1 = bitcast i32* %0 to i8*
  %2 = load i32, i32* %__n.addr, align 4
  %mul = mul i32 %2, 4
  call void @_ZNSt3__219__libcpp_deallocateEPvmm(i8* %1, i32 %mul, i32 4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__214__split_bufferIjRNS_9allocatorIjEEE9__end_capEv(%"struct.std::__2::__split_buffer.17"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.std::__2::__split_buffer.17"*, align 4
  store %"struct.std::__2::__split_buffer.17"* %this, %"struct.std::__2::__split_buffer.17"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__split_buffer.17"*, %"struct.std::__2::__split_buffer.17"** %this.addr, align 4
  %__end_cap_ = getelementptr inbounds %"struct.std::__2::__split_buffer.17", %"struct.std::__2::__split_buffer.17"* %this1, i32 0, i32 3
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__217__compressed_pairIPjRNS_9allocatorIjEEE5firstEv(%"class.std::__2::__compressed_pair.18"* %__end_cap_) #6
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__217__compressed_pairIPjRNS_9allocatorIjEEE5firstEv(%"class.std::__2::__compressed_pair.18"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair.18"*, align 4
  store %"class.std::__2::__compressed_pair.18"* %this, %"class.std::__2::__compressed_pair.18"** %this.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair.18"*, %"class.std::__2::__compressed_pair.18"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair.18"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %call = call nonnull align 4 dereferenceable(4) i32** @_ZNKSt3__222__compressed_pair_elemIPjLi0ELb0EE5__getEv(%"struct.std::__2::__compressed_pair_elem"* %0) #6
  ret i32** %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base"* @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEEC2Ev(%"class.std::__2::__vector_base"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  %ref.tmp = alloca i8*, align 4
  %ref.tmp2 = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__vector_base"* %this1 to %"class.std::__2::__vector_base_common"*
  %call = call %"class.std::__2::__vector_base_common"* @_ZNSt3__220__vector_base_commonILb1EEC2Ev(%"class.std::__2::__vector_base_common"* %0)
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  store i32* null, i32** %__begin_, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 1
  store i32* null, i32** %__end_, align 4
  %__end_cap_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 2
  store i8* null, i8** %ref.tmp, align 4
  %call3 = call %"class.std::__2::__compressed_pair"* @_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEEC2IDnNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair"* %__end_cap_, i8** nonnull align 4 dereferenceable(4) %ref.tmp, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %ref.tmp2)
  ret %"class.std::__2::__vector_base"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base_common"* @_ZNSt3__220__vector_base_commonILb1EEC2Ev(%"class.std::__2::__vector_base_common"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base_common"*, align 4
  store %"class.std::__2::__vector_base_common"* %this, %"class.std::__2::__vector_base_common"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base_common"*, %"class.std::__2::__vector_base_common"** %this.addr, align 4
  ret %"class.std::__2::__vector_base_common"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__compressed_pair"* @_ZNSt3__217__compressed_pairIPjNS_9allocatorIjEEEC2IDnNS_18__default_init_tagEEEOT_OT0_(%"class.std::__2::__compressed_pair"* returned %this, i8** nonnull align 4 dereferenceable(4) %__t1, %"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__compressed_pair"*, align 4
  %__t1.addr = alloca i8**, align 4
  %__t2.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  %agg.tmp = alloca %"struct.std::__2::__default_init_tag", align 1
  store %"class.std::__2::__compressed_pair"* %this, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  store i8** %__t1, i8*** %__t1.addr, align 4
  store %"struct.std::__2::__default_init_tag"* %__t2, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %this1 = load %"class.std::__2::__compressed_pair"*, %"class.std::__2::__compressed_pair"** %this.addr, align 4
  %0 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem"*
  %1 = load i8**, i8*** %__t1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i8** @_ZNSt3__27forwardIDnEEOT_RNS_16remove_referenceIS1_E4typeE(i8** nonnull align 4 dereferenceable(4) %1) #6
  %call2 = call %"struct.std::__2::__compressed_pair_elem"* @_ZNSt3__222__compressed_pair_elemIPjLi0ELb0EEC2IDnvEEOT_(%"struct.std::__2::__compressed_pair_elem"* %0, i8** nonnull align 4 dereferenceable(4) %call)
  %2 = bitcast %"class.std::__2::__compressed_pair"* %this1 to %"struct.std::__2::__compressed_pair_elem.0"*
  %3 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t2.addr, align 4
  %call3 = call nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %3) #6
  %call4 = call %"struct.std::__2::__compressed_pair_elem.0"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.0"* %2)
  ret %"class.std::__2::__compressed_pair"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) %"struct.std::__2::__default_init_tag"* @_ZNSt3__27forwardINS_18__default_init_tagEEEOT_RNS_16remove_referenceIS2_E4typeE(%"struct.std::__2::__default_init_tag"* nonnull align 1 dereferenceable(1) %__t) #0 comdat {
entry:
  %__t.addr = alloca %"struct.std::__2::__default_init_tag"*, align 4
  store %"struct.std::__2::__default_init_tag"* %__t, %"struct.std::__2::__default_init_tag"** %__t.addr, align 4
  %0 = load %"struct.std::__2::__default_init_tag"*, %"struct.std::__2::__default_init_tag"** %__t.addr, align 4
  ret %"struct.std::__2::__default_init_tag"* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.std::__2::__compressed_pair_elem.0"* @_ZNSt3__222__compressed_pair_elemINS_9allocatorIjEELi1ELb1EEC2ENS_18__default_init_tagE(%"struct.std::__2::__compressed_pair_elem.0"* returned %this) unnamed_addr #0 comdat {
entry:
  %0 = alloca %"struct.std::__2::__default_init_tag", align 1
  %this.addr = alloca %"struct.std::__2::__compressed_pair_elem.0"*, align 4
  store %"struct.std::__2::__compressed_pair_elem.0"* %this, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %this1 = load %"struct.std::__2::__compressed_pair_elem.0"*, %"struct.std::__2::__compressed_pair_elem.0"** %this.addr, align 4
  %1 = bitcast %"struct.std::__2::__compressed_pair_elem.0"* %this1 to %"class.std::__2::allocator"*
  %call = call %"class.std::__2::allocator"* @_ZNSt3__29allocatorIjEC2Ev(%"class.std::__2::allocator"* %1) #6
  ret %"struct.std::__2::__compressed_pair_elem.0"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::allocator"* @_ZNSt3__29allocatorIjEC2Ev(%"class.std::__2::allocator"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::allocator"*, align 4
  store %"class.std::__2::allocator"* %this, %"class.std::__2::allocator"** %this.addr, align 4
  %this1 = load %"class.std::__2::allocator"*, %"class.std::__2::allocator"** %this.addr, align 4
  ret %"class.std::__2::allocator"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.std::__2::__vector_base"* @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEED2Ev(%"class.std::__2::__vector_base"* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %"class.std::__2::__vector_base"*, align 4
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  store %"class.std::__2::__vector_base"* %this1, %"class.std::__2::__vector_base"** %retval, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__begin_, align 4
  %cmp = icmp ne i32* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE5clearEv(%"class.std::__2::__vector_base"* %this1) #6
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv(%"class.std::__2::__vector_base"* %this1) #6
  %__begin_2 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  %1 = load i32*, i32** %__begin_2, align 4
  %call3 = call i32 @_ZNKSt3__213__vector_baseIjNS_9allocatorIjEEE8capacityEv(%"class.std::__2::__vector_base"* %this1) #6
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE10deallocateERS2_Pjm(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call, i32* %1, i32 %call3) #6
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %retval, align 4
  ret %"class.std::__2::__vector_base"* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE5clearEv(%"class.std::__2::__vector_base"* %this) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__begin_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 0
  %0 = load i32*, i32** %__begin_, align 4
  call void @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE17__destruct_at_endEPj(%"class.std::__2::__vector_base"* %this1, i32* %0) #6
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE17__destruct_at_endEPj(%"class.std::__2::__vector_base"* %this, i32* %__new_last) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::__vector_base"*, align 4
  %__new_last.addr = alloca i32*, align 4
  %__soon_to_be_end = alloca i32*, align 4
  store %"class.std::__2::__vector_base"* %this, %"class.std::__2::__vector_base"** %this.addr, align 4
  store i32* %__new_last, i32** %__new_last.addr, align 4
  %this1 = load %"class.std::__2::__vector_base"*, %"class.std::__2::__vector_base"** %this.addr, align 4
  %__end_ = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 1
  %0 = load i32*, i32** %__end_, align 4
  store i32* %0, i32** %__soon_to_be_end, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load i32*, i32** %__new_last.addr, align 4
  %2 = load i32*, i32** %__soon_to_be_end, align 4
  %cmp = icmp ne i32* %1, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call = call nonnull align 1 dereferenceable(1) %"class.std::__2::allocator"* @_ZNSt3__213__vector_baseIjNS_9allocatorIjEEE7__allocEv(%"class.std::__2::__vector_base"* %this1) #6
  %3 = load i32*, i32** %__soon_to_be_end, align 4
  %incdec.ptr = getelementptr inbounds i32, i32* %3, i32 -1
  store i32* %incdec.ptr, i32** %__soon_to_be_end, align 4
  %call2 = call i32* @_ZNSt3__212__to_addressIjEEPT_S2_(i32* %incdec.ptr) #6
  call void @_ZNSt3__216allocator_traitsINS_9allocatorIjEEE7destroyIjEEvRS2_PT_(%"class.std::__2::allocator"* nonnull align 1 dereferenceable(1) %call, i32* %call2)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %4 = load i32*, i32** %__new_last.addr, align 4
  %__end_3 = getelementptr inbounds %"class.std::__2::__vector_base", %"class.std::__2::__vector_base"* %this1, i32 0, i32 1
  store i32* %4, i32** %__end_3, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNKSt3__26vectorIjNS_9allocatorIjEEE17__annotate_shrinkEm(%"class.std::__2::vector"* %this, i32 %__old_size) #0 comdat {
entry:
  %this.addr = alloca %"class.std::__2::vector"*, align 4
  %__old_size.addr = alloca i32, align 4
  store %"class.std::__2::vector"* %this, %"class.std::__2::vector"** %this.addr, align 4
  store i32 %__old_size, i32* %__old_size.addr, align 4
  %this1 = load %"class.std::__2::vector"*, %"class.std::__2::vector"** %this.addr, align 4
  %call = call i32* @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv(%"class.std::__2::vector"* %this1) #6
  %0 = bitcast i32* %call to i8*
  %call2 = call i32* @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv(%"class.std::__2::vector"* %this1) #6
  %call3 = call i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE8capacityEv(%"class.std::__2::vector"* %this1) #6
  %add.ptr = getelementptr inbounds i32, i32* %call2, i32 %call3
  %1 = bitcast i32* %add.ptr to i8*
  %call4 = call i32* @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv(%"class.std::__2::vector"* %this1) #6
  %2 = load i32, i32* %__old_size.addr, align 4
  %add.ptr5 = getelementptr inbounds i32, i32* %call4, i32 %2
  %3 = bitcast i32* %add.ptr5 to i8*
  %call6 = call i32* @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4dataEv(%"class.std::__2::vector"* %this1) #6
  %call7 = call i32 @_ZNKSt3__26vectorIjNS_9allocatorIjEEE4sizeEv(%"class.std::__2::vector"* %this1) #6
  %add.ptr8 = getelementptr inbounds i32, i32* %call6, i32 %call7
  %4 = bitcast i32* %add.ptr8 to i8*
  call void @_ZNKSt3__26vectorIjNS_9allocatorIjEEE31__annotate_contiguous_containerEPKvS5_S5_S5_(%"class.std::__2::vector"* %this1, i8* %0, i8* %1, i8* %3, i8* %4) #6
  ret void
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { noreturn "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { noinline noreturn nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nobuiltin allocsize(0) "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nounwind }
attributes #7 = { noreturn }
attributes #8 = { builtin allocsize(0) }
attributes #9 = { builtin nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
